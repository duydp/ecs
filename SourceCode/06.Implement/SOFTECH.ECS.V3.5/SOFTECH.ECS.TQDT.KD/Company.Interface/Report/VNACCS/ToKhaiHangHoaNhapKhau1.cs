using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using DevExpress.XtraPrinting.BarCode;
//using BarcodeLib;
using System.Drawing.Imaging;
using System.Windows.Forms;
using BarcodeLib;

namespace Company.Interface.Report.VNACCS
{
    public partial class ToKhaiHangHoaNhapKhau1 : DevExpress.XtraReports.UI.XtraReport
    {
        public bool IsKhaiBaoSua;
        public ToKhaiHangHoaNhapKhau1()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 30,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        public void BindingReport(VAD1AC0 vad1ac,string maNV)
        {
            pictureBox1.Image = GenerateBarCode(vad1ac.ICN.GetValue().ToString());
            lblTenThongTinXuat.Text = EnumThongBao.GetTenNV(maNV);
            if (string.IsNullOrEmpty(vad1ac.A06.GetValue().ToString()))
                lblTenThongTinXuat.Text = "Bản sao thông tin tờ khai nhập khẩu(Chưa đăng ký)";
            lblSoTrang.Text = "1/" + vad1ac.B12.GetValue().ToString().ToUpper();
            lblSoToKhai.Text = vad1ac.ICN.GetValue().ToString().ToUpper();
            lblSoToKhaiDauTien.Text = vad1ac.FIC.GetValue().ToString().ToUpper();
            lblSoNhanhToKhaiChiaNho.Text = vad1ac.BNO.GetValue().ToString().ToUpper();
            lblTongSoToKhaiChiaNho.Text = vad1ac.DNO.GetValue().ToString().ToUpper();
            lblSoToKhaiTamNhapTaiXuat.Text = vad1ac.TDN.GetValue().ToString().ToUpper();
            lblMaPhanLoaiKiemTra.Text = vad1ac.A06.GetValue().ToString().ToUpper();
            lblMaLoaiHinh.Text = vad1ac.ICB.GetValue().ToString().ToUpper();
            lblMaPhanLoaiHangHoa.Text = vad1ac.CCC.GetValue().ToString().ToUpper();
            lblMaHieuPhuongThucVanChuyen.Text = vad1ac.MTC.GetValue().ToString().ToUpper();
            lblPhanLoaiCaNhanToChuc.Text = vad1ac.SKB.GetValue().ToString().ToUpper();
            lblMaSoHangHoaDaiDienToKhai.Text = vad1ac.A00.GetValue().ToString().ToUpper();
            lblTenCoQuanHaiQuanTiepNhanToKhai.Text = vad1ac.A07.GetValue().ToString().ToUpper();
            lblMaBoPhanXuLyToKhai.Text = vad1ac.CHB.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ac.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDangKy.Text = Convert.ToDateTime(vad1ac.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDangKy.Text = "";
                
            }
            if (vad1ac.AD1.GetValue().ToString().ToUpper() != "" && vad1ac.AD1.GetValue().ToString().ToUpper() != "0")
            {
                lblGioDangKy.Text = vad1ac.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ac.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayThayDoiDangKy.Text = Convert.ToDateTime(vad1ac.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayThayDoiDangKy.Text = "";
            }
            if (vad1ac.AD3.GetValue().ToString().ToUpper() != "" && vad1ac.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioThayDoiDangKy.Text = vad1ac.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioThayDoiDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ac.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(vad1ac.RED.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanTaiNhapTaiXuat.Text = "";
            }
            lblBieuThiTHHetHan.Text = vad1ac.AAA.GetValue().ToString().ToUpper();
            lblMaNguoiNhapKhau.Text = vad1ac.IMC.GetValue().ToString().ToUpper();
            lblTenNguoiNhapKhau.Text = vad1ac.IMN.GetValue().ToString().ToUpper();
            lblMaBuuChinhNhapKhau.Text = vad1ac.IMY.GetValue().ToString().ToUpper();
            lblDiaChiNguoiNhapKhau.Text = vad1ac.IMA.GetValue().ToString().ToUpper();
            lblSoDienThoaiNguoiNhapKhau.Text = vad1ac.IMT.GetValue().ToString().ToUpper();
            lblMaNguoiUyThacNhapKhau.Text = vad1ac.NMC.GetValue().ToString().ToUpper();
            lblTenNguoiUyThacNhapKhau.Text = vad1ac.NMN.GetValue().ToString().ToUpper();
            lblMaNguoiXuatKhau.Text = vad1ac.EPC.GetValue().ToString().ToUpper();
            lblTenNguoiXuatKhau.Text = vad1ac.EPN.GetValue().ToString().ToUpper();
            lblMaBuuChinhXuatKhau.Text = vad1ac.EPY.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau1.Text = vad1ac.EPA.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau2.Text = vad1ac.EP2.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau3.Text = vad1ac.EP3.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau4.Text = vad1ac.EP4.GetValue().ToString().ToUpper();
            lblMaNuoc.Text = vad1ac.EPO.GetValue().ToString().ToUpper();
            lblNguoiUyThacXuatKhau.Text = vad1ac.ENM.GetValue().ToString().ToUpper();
            lblMaDaiLyHaiQuan.Text = vad1ac.A37.GetValue().ToString().ToUpper();
            lblTenDaiLyHaiQuan.Text = vad1ac.A38.GetValue().ToString().ToUpper();
            lblMaNhanVienHaiQuan.Text = vad1ac.A39.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ac.BL_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblSoVanDon1.Text = vad1ac.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblSoVanDon2.Text = vad1ac.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblSoVanDon3.Text = vad1ac.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblSoVanDon4.Text = vad1ac.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblSoVanDon5.Text = vad1ac.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblSoLuong.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(vad1ac.NO.GetValue().ToString().ToUpper().ToString().Replace(".", ",")));
            lblMaDonViTinh.Text = vad1ac.NOT.GetValue().ToString().ToUpper();
            //string a = vad1ac.GW.GetValue().ToString().Replace(".", ",");
            lblTongTrongLuongHang.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(vad1ac.GW.GetValue().ToString().Replace(".", ",")));
            lblMaDonViTinhTrongLuong.Text = vad1ac.GWT.GetValue().ToString().ToUpper();
            lblSoLuongContainer.Text = vad1ac.COC.GetValue().ToString().ToUpper();
            lblMaDiaDiemLuuKhoHang.Text = vad1ac.ST.GetValue().ToString().ToUpper();
            lblTenDiaDiemLuuKhoHang.Text = vad1ac.A51.GetValue().ToString().ToUpper();
            lblMaDiaDiemDoHang.Text = vad1ac.DST.GetValue().ToString().ToUpper();
            lblTenDiaDiemDoHang.Text = vad1ac.DSN.GetValue().ToString().ToUpper();
            lblMaDiaDiemXepHang.Text = vad1ac.PSC.GetValue().ToString().ToUpper();
            lblTenDiaDiemXepHang.Text = vad1ac.PSN.GetValue().ToString().ToUpper();
            lblMaPhuongTienVanChuyen.Text = vad1ac.VSC.GetValue().ToString().ToUpper();
            lblTenPhuongTienVanChuyen.Text = vad1ac.VSN.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ac.ARR.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHangDen.Text = Convert.ToDateTime(vad1ac.ARR.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHangDen.Text = "";
            }
            lblKyHieuSoHieu.Text = vad1ac.MRK.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ac.ISD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuocPhepNhapKho.Text = Convert.ToDateTime(vad1ac.ISD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuocPhepNhapKho.Text = "";
            }
            for (int i = 0; i < vad1ac.OL_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaVanBanPhapQuyKhac1.Text = vad1ac.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaVanBanPhapQuyKhac2.Text = vad1ac.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaVanBanPhapQuyKhac3.Text = vad1ac.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblMaVanBanPhapQuyKhac4.Text = vad1ac.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblMaVanBanPhapQuyKhac5.Text = vad1ac.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblPhanLoaiHinhThucHoaDon.Text = vad1ac.IV1.GetValue().ToString().ToUpper();
            lblSoHoaDon.Text = vad1ac.IV3.GetValue().ToString().ToUpper();
            lblSoTiepNhanHoaDonDienTu.Text = vad1ac.IV2.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ac.IVD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayPhatHanh.Text = Convert.ToDateTime(vad1ac.IVD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayPhatHanh.Text = "";
            }
            lblPhuongThucThanhToan.Text = vad1ac.IVP.GetValue().ToString().ToUpper();
            lblMaPhanLoaiGiaHoaDon.Text = vad1ac.IP1.GetValue().ToString().ToUpper();
            lblMaDieuKienGiaHoaDon.Text = vad1ac.IP2.GetValue().ToString().ToUpper();
            lblMaDongTienHoaDon.Text = vad1ac.IP3.GetValue().ToString().ToUpper();
            lblTongTriGiaHoaDon.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.IP4.GetValue().ToString().Replace(".", ",")));
            lblTongTriGiaTinhThue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.A86.GetValue().ToString().Replace(".", ",")));

            //minhnd Fix Trị giá Tờ khai
            //KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(lblSoToKhai.Text);
            //decimal triGiaHang = 0;
            //TKMD.LoadFull();
            //foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
            //{
            //    triGiaHang += hmd.TriGiaHoaDon;
            //}
            //lblTongHeSoPhanBoTriGia.Text = triGiaHang.ToString("#,#.0000#;(#,#.0000#)");
            //minhnd Fix Trị giá Tờ khai
            lblTongHeSoPhanBoTriGia.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.TP.GetValue().ToString().Replace(".", ",")));
            
            lblMaPhanLoaiNhapLieu.Text = vad1ac.A97.GetValue().ToString().ToUpper();
            lblMaKetQuaKiemTraNoiDung.Text = vad1ac.N4.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ac.SS_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPhanLoaiGiayPhepNhapKhau1.Text = vad1ac.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep1.Text = vad1ac.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblPhanLoaiGiayPhepNhapKhau2.Text = vad1ac.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep2.Text = vad1ac.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblPhanLoaiGiayPhepNhapKhau3.Text = vad1ac.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep3.Text = vad1ac.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblPhanLoaiGiayPhepNhapKhau4.Text = vad1ac.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep4.Text = vad1ac.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblPhanLoaiGiayPhepNhapKhau5.Text = vad1ac.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep5.Text = vad1ac.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblMaPhanLoaiKhaiTriGia.Text = vad1ac.VD1.GetValue().ToString().ToUpper();
            lblSoTiepNhanKhaiTriGiaTongHop.Text = vad1ac.VD2.GetValue().ToString().ToUpper() == "0" ? "" : vad1ac.VD2.GetValue().ToString().ToUpper();
            lblPhanLoaiCongThucChuan.Text = vad1ac.A93.GetValue().ToString().ToUpper();
            lblMaPhanLoaiDieuChinhTriGia.Text = vad1ac.A94.GetValue().ToString().ToUpper();
            lblPhuongPhapDieuChinhTriGia.Text = vad1ac.A95.GetValue().ToString().ToUpper();
            lblMaTienTe.Text = vad1ac.VCC.GetValue().ToString().ToUpper();
            lblGiaCoSo.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VPC.GetValue().ToString().Replace(".", ",")));
            lblMaPhanLoaiPhiVanChuyen.Text = vad1ac.FR1.GetValue().ToString().ToUpper();
            lblMaTienTePhiVanChuyen.Text = vad1ac.FR2.GetValue().ToString().ToUpper();
            lblPhiVanChuyen.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.FR3.GetValue().ToString().Replace(".", ",")));
            lblMaPhanLoaiBaoHiem.Text = vad1ac.IN1.GetValue().ToString().ToUpper();
            lblMaTienTeCuaTienBaoHiem.Text = vad1ac.IN2.GetValue().ToString().ToUpper();
            lblPhiBaoHiem.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.IN3.GetValue().ToString().Replace(".", ",")));
            lblSoDangKyBaoHiemTongHop.Text = vad1ac.IN4.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ac.VR_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaTenKhoanDieuChinh1.Text = vad1ac.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh1.Text = vad1ac.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia1.Text = vad1ac.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 1:
                        lblMaTenKhoanDieuChinh2.Text = vad1ac.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh2.Text = vad1ac.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia2.Text = vad1ac.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 2:
                        lblMaTenKhoanDieuChinh3.Text = vad1ac.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh3.Text = vad1ac.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia3.Text = vad1ac.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 3:
                        lblMaTenKhoanDieuChinh4.Text = vad1ac.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh4.Text = vad1ac.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia4.Text = vad1ac.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 4:
                        lblMaTenKhoanDieuChinh5.Text = vad1ac.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh5.Text = vad1ac.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia5.Text = vad1ac.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[3].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.VR_.listAttribute[4].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                }
            }
            lblChiTietKhaiTriGia.Text = vad1ac.VLD.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ac.KF1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaSacThue1.Text = vad1ac.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue1.Text = vad1ac.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong1.Text = vad1ac.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaSacThue2.Text = vad1ac.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue2.Text = vad1ac.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong2.Text = vad1ac.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaSacThue3.Text = vad1ac.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue3.Text = vad1ac.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong3.Text = vad1ac.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblMaSacThue4.Text = vad1ac.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue4.Text = vad1ac.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue4.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong4.Text = vad1ac.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblMaSacThue5.Text = vad1ac.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue5.Text = vad1ac.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue5.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong5.Text = vad1ac.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 5:
                        lblMaSacThue6.Text = vad1ac.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue6.Text = vad1ac.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue6.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KF1.listAttribute[2].GetValueCollection(i).ToString().Replace(".", ",")));
                        lblSoDongTong6.Text = vad1ac.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblTongTienThuePhaiNop.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.B02.GetValue().ToString().Replace(".", ",")));
            lblSoTienBaoLanh.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.B03.GetValue().ToString().Replace(".", ",")));
            for (int i = 0; i < vad1ac.KJ1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaDongTienTyGiaTinhThue1.Text = vad1ac.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 1:
                        lblMaDongTienTyGiaTinhThue2.Text = vad1ac.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 2:
                        lblMaDongTienTyGiaTinhThue3.Text = vad1ac.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue3.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(vad1ac.KJ1.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                }
            }
            lblMaXacDinhThoiHanNopThue.Text = vad1ac.ENC.GetValue().ToString().ToUpper();
            lblNguoiNopThue.Text = vad1ac.TPM.GetValue().ToString().ToUpper();
            lblMaLyDoDeNghi.Text = vad1ac.BP.GetValue().ToString().ToUpper();
            lblPhanLoaiNopThue.Text = vad1ac.B08.GetValue().ToString().ToUpper();
            lblTongSoTrangToKhai.Text = vad1ac.B12.GetValue().ToString().ToUpper();
            lblTongSoDongHang.Text = vad1ac.B13.GetValue().ToString().ToUpper();

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }

        public XRBarCode CreateCode39BarCode(string BarCodeText)
        {
            // Create a bar code control.
            XRBarCode barCode = new XRBarCode();

            // Set the bar code's type to Code 39.
            barCode.Symbology = new Code39Generator();

            // Adjust the bar code's main properties.
            barCode.Text = BarCodeText;
            barCode.Width = 250;
            barCode.Height = 30;

            // Adjust the properties specific to the bar code type.
            ((Code39Generator)barCode.Symbology).CalcCheckSum = false;
            ((Code39Generator)barCode.Symbology).WideNarrowRatio = 2.5F;

            return barCode;
        }

        private void lblTenThongTinXuat_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            e.Brick.Text = "Tờ khai hàng hóa nhập khẩu (thông quan)";
            e.PreviewControl.Refresh();
        }
    }
}
