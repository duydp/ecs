﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{

    [Serializable]
    [XmlRoot("Header")]
    public class MessageHeader
    {
        /// <summary>
        /// Loại thủ tục áp dụng
        /// Áp dụng cho thủ tục HQĐT, thủ công (1 - truyền thống, 2 - điện tử,...)
        /// </summary>
        [XmlElement("procedureType")]
        public string ProcedureType { get; set; }

        [XmlElement("Reference")]        
        public Reference Reference { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("SendApplication")]        
        public SendApplication SendApplication { get; set; }
        /// <summary>
        /// Doanh nghiệp gửi
        /// </summary>
        [XmlElement("From")]
        public NameBase From { get; set; }

        /// <summary>
        /// Chi cục gửi
        /// </summary>
        [XmlElement("To")]
        public NameBase To { get; set; }
        /// <summary>
        /// Vận đơn nếu có sử dụng
        /// </summary>
        [XmlElement("VAN")]
        public NameBase Van { get; set; }
        /// <summary>
        /// Chức năng khai báo
        /// </summary>
        [XmlElement("Subject")]        
        public Subject Subject { get; set; }
        
    }
}
