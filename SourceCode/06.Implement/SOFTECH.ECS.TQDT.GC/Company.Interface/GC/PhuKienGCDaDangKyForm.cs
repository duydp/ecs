﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.KDT.GC.PCTFormTemplate;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using Company.Interface.KDT.GC;

namespace Company.Interface.GC
{
    public partial class PhuKienGCDaDangKyForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public PhuKienDangKy pkdk = new PhuKienDangKy();
        public bool boolFlag = false ;
        //-----------------------------------------------------------------------------------------
        public PhuKienGCDaDangKyForm()
        {
            InitializeComponent();
            createLoaiPhuKien();                              
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll();
            this._DonViTinh = DonViTinh.SelectAll();
            HD.Load();            
            txtSoHopDong.Text = HD.SoHopDong;
            createLoaiPhuKien();
        }
     
        private void setCommandStatus()
        {
               cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True ;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True ;
                XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Đã duyệt","Approved");
           
           
        }
        //-----------------------------------------------------------------------------------------
        private void PhuKienGCSendForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();

            if (pkdk.ID>0)
            {                
                txtSoPhuKien.Text = pkdk.SoPhuKien.Trim();
                ccNgayPhuKien.Value = pkdk.NgayPhuKien;
                txtGhiChu.Text = pkdk.GhiChu;
                txtVBCP.Text = pkdk.VanBanChoPhep;
                txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                txtNguoiDuyet.Text = pkdk.NguoiDuyet;
            }    
            dgList.DataSource = pkdk.PKCollection;
            setCommandStatus();
            if (pkdk.ID > 0)
            {
                Company.GC.BLL.KDT.SXXK.MsgSend msg = new Company.GC.BLL.KDT.SXXK.MsgSend();
                msg.master_id = this.pkdk.ID;
                msg.LoaiHS = "PK";
                if (msg.Load())
                {
                    lblTrangThai.Text = setText("Chờ xác nhận", "Wait for approval");
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
            }            
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo thông tin về loại phụ kiện.
        /// </summary>
        private void createLoaiPhuKien()
        {
            cbLoaiPhuKien.DataSource = Company.GC.BLL.DuLieuChuan.LoaiPhuKien.SelectAll();
            cbLoaiPhuKien.DisplayMember = "TenLoaiPhuKien";
            cbLoaiPhuKien.ValueMember = "ID_LoaiPhuKien";
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
           
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //ShowMessage("Phụ kiện này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.", false);
                    return;
                }
            }

            if (pkdk.PKCollection.Count == 0)
            {
                showMsg("MSG_2702015");
                //ShowMessage("Chưa nhập phụ kiện", false);
                return;
            }
            //try
            //{
            //    pkdk.InsertUpDateFull();
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage("Lưu thông tin vào cơ sở dữ liệu bị lỗi. Không khai báo thông tin tới hải quan được.Lỗi : " + ex.Message, false);
            //    return;
            //}
            string xmlCurrent = "";
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = pkdk.WSSend(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;                
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            showMsg("MSG_WRN13",ex.Message);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //ShowMessage(ex.Message, false);
                            showMsg("MSG_2702004", ex.Message);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Phụ kiện . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }     
        }
        private void LaySoTiepNhanDT()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //ShowMessage("Phụ kiện không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    return;
                }
            }
            string xmlCurrent = "";
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {               
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = pkdk.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                    if (showMsg("MSG_STN02", true) == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    else
                    {
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;                        
                    }
                    return;
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02");
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkdk.SoTiepNhan, false);
                    lblTrangThai.Text = setText("Chờ duyệt","Wait for approval");
                    txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702017");
                    //ShowMessage("Đã hủy phụ kiện này", false);
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet" );
                    txtSoTiepNhan.Text = "";
                }
                else if (sendXML.func == 2)
                {
                    if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        showMsg("MSG_2702018", "", false);
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        showMsg("MSG_SEN04");
                        //ShowMessage("Hải quan chưa xử lý danh sách sản phẩm này!", false);
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                        lblTrangThai.Text = setText("Không phê duyệt", "Not approved");
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //ShowMessage(ex.Message, false);
                            showMsg("MSG_2702004", ex.Message);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            string xmlCurrent = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {

                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = pkdk.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    if (showMsg("MSG_STN02", true) == "Yes")
                    //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    else
                    {
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;                       
                    }
                    return;
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", pkdk.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkdk.SoTiepNhan, false);
                    lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                    txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702019");
                    //ShowMessage("Đã hủy danh sách phụ kiện này", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        showMsg("MSG_2702018", "", false);
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", false);
                        txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        showMsg("MSG_SEN04");
                        //ShowMessage("Hải quan chưa xử lý danh sách sản phẩm này!", false);
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                        lblTrangThai.Text = setText("Không phê duyệt", "Not approved");
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới phụ kiện.
        /// </summary>
        private void add()
        {
            if (pkdk.ID == 0)
            {
                showMsg("MSG_WRN20");
                //ShowMessage("Lưu thông tin trước khi nhập thông tin cho từng loại phụ kiện.", false);
                return;
            }
            cvError.Validate();
            if (!cvError.IsValid) return;
            
            if (cbLoaiPhuKien.Value == null || cbLoaiPhuKien.Value.ToString() == "")
            {
                showMsg("MSG_2702020");
                //ShowMessage("Bạn chưa chọn loại phụ kiện.", false);
                return;
            }
            string id_loaiphukien = cbLoaiPhuKien.Value.ToString();
            long id = pkdk.checkPhuKienCungLoai(id_loaiphukien, pkdk.ID, pkdk.HopDong_ID);
            if (id > 0)
            {
                showMsg("MSG_240201", id);
                //ShowMessage("Có phụ kiện cùng loại này chưa được hải quan duyệt nằm trong danh sách phụ kiện có ID=" + id.ToString(), false);
                return;
            }
            switch (id_loaiphukien.Trim())
            {
                case "H06":
                {                        
                        showPKGiaHanHD(true); 
                } 
                    break;
                case "H10":
                    {                       
                        showPKHuyHD(true); 
                    } break;
                case "H11": 
                    {                       
                        showPKMoPhuKienDocLap(true); 
                    } break;
                case "N01": { showPKBoSungNPL(true); } break;
                case "N05": { showPKDieuChinhSoLuongNPL(true); } break;
                case "N06": { showPKDieuChinhDVTNPL(true); } break;
                case "N11": { showPKBoSungNPLVietNam(true); } break;
                case "S06": { showPKDieuChinhDVTSP(true); } break;
                case "S10": { showPKDieuChinhMaSP(true); } break;
                case "S13": { showPKDieuChinhChiTietMaSP(true); } break;
                case "S15": { showPKDieuChinhNhomSP(true); } break;
                case "T01": { showPKDieuChinhThietBi(true); } break;
                case "T07": { showPKBoSungThietBi(true); } break;
            }
        }
     
        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {           
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            string id_loaiphukien = e.Row.Cells[1].Text.Trim();
            switch (id_loaiphukien.Trim())
            {
                case "H06": { showPKGiaHanHD(false); } break;
                case "H10": { showPKHuyHD(false); } break;
                case "H11": { showPKMoPhuKienDocLap(false); } break;
                case "N01": { showPKBoSungNPL(false); } break;
                case "N05": { showPKDieuChinhSoLuongNPL(false); } break;
                case "N06": { showPKDieuChinhDVTNPL(false); } break;
                case "N11": { showPKBoSungNPLVietNam(false); } break;
                case "S06": { showPKDieuChinhDVTSP(false); } break;
                case "S10": { showPKDieuChinhMaSP(false); } break;
                case "S13": { showPKDieuChinhChiTietMaSP(false); } break;
                case "S15": { showPKDieuChinhNhomSP(false); } break;
                case "T01": { showPKDieuChinhThietBi(false); } break;
                case "T07": { showPKBoSungThietBi(false); } break;
                case "S09": { showPKDieuChinhSLSanPham(false); } break;
            }
        }
        private void showPKDieuChinhSLSanPham(bool isAdd)
        {
            FormDieuChinhSLSP f = new FormDieuChinhSLSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void save()
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            try
            {
                HD.Load();
                pkdk.HopDong_ID = this.HD.ID;
                long id=pkdk.checkExitsSoPhuKien(txtSoPhuKien.Text.Trim());
                if(id>0)
                {
                    showMsg("MSG_240202");
                    //ShowMessage("Số phụ kiện này đã được đăng ký trong danh sách phụ kiện có ID="+id.ToString(), false);
                    return;
                }               
                this.Cursor = Cursors.WaitCursor;               
                {
                    pkdk.GhiChu = txtGhiChu.Text.Trim();
                    
                    pkdk.SoPhuKien = txtSoPhuKien.Text.Trim();
                    pkdk.TrangThaiXuLy = 1;
                    pkdk.VanBanChoPhep = txtVBCP.Text.Trim();
                    pkdk.NgayPhuKien = ccNgayPhuKien.Value;
                    pkdk.NguoiDuyet = txtNguoiDuyet.Text.Trim();
                    pkdk.MaHaiQuan = HD.MaHaiQuan;
                    //pkdk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    pkdk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    if (pkdk.ID == 0)
                        pkdk.Insert();
                    else
                        pkdk.Update();
                    //pkdk.InsertUpDateFull();
                    showMsg("MSG_2702001");
                    //ShowMessage("Cập nhật thành công", false);
                    setCommandStatus();                    
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Có lỗi:" + ex.Message);
                showMsg("MSG_2702004", ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {                       
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSend":
                    this.send();
                    break;
                case "HuyKhaiBao":
                    this.Huy();
                    break;
                case "NhanDuLieuPK":
                    this.NhanDuLieuPK12();
                    break;
                case "XacNhanThongTin":
                     this.LaySoTiepNhanDT();
                    break;
            }
        }
        public void NhanDuLieuPK12()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //ShowMessage("Phụ kiện này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.", false);
                    return;
                }
            }
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string xmlCurrent = pkdk.WSDownLoad(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);

            }
            catch (Exception ex)
            {                
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            showMsg("MSG_WRN13",ex.Message);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        public void Huy()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //ShowMessage("Phụ kiện này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.", false);
                    return;
                }
            }

            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string xmlCurrent = pkdk.WSCancel(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            showMsg("MSG_WRN13",ex.Message);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void showPKGiaHanHD(bool isAdd)
        {
            
            PKGiahanHDForm f = new PKGiahanHDForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void showPKHuyHD(bool isAdd)
        {                             
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "H10")
                {
                    return;
                }
            }
            if (isAdd)
            {
                try
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "H10";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;                    
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    else
                        LoaiPK.Update();
                    pkdk.PKCollection.Add(LoaiPK);                  
                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKMoPhuKienDocLap(bool isAdd)
        {                       
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "H11")
                {
                    return;
                }
            }
            if (isAdd)
            {
                try
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "H11";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;                 
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    else
                        LoaiPK.Update();
                    pkdk.PKCollection.Add(LoaiPK);                    
                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKBoSungNPL(bool isAdd)
        {
            NguyenPhuLieuGCBoSungForm f = new NguyenPhuLieuGCBoSungForm();                               
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();            
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }          
        }
        private void showPKDieuChinhSoLuongNPL(bool isAdd)
        {
            FormDieuChinhSLSP f = new FormDieuChinhSLSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();           
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }                   
        }
        private void showPKDieuChinhDVTNPL(bool isAdd)
        {
            FormDieuChinhDVTNPL f = new FormDieuChinhDVTNPL();
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }          
        }
        private void showPKBoSungNPLVietNam(bool isAdd)
        {
            FormNPLNhapVN f = new FormNPLNhapVN();
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }          
        }
        private void showPKDieuChinhDVTSP(bool isAdd)
        {
            FormDieuChinhDVTSP f = new FormDieuChinhDVTSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }          
        }
        private void showPKDieuChinhMaSP(bool isAdd)
        {
            FormDieuChinhMaSP f = new FormDieuChinhMaSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }          
        }
        private void showPKDieuChinhChiTietMaSP(bool isAdd)
        {
            SanPhamGCBoSungForm f = new SanPhamGCBoSungForm();        
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }          
        }
        private void showPKDieuChinhNhomSP(bool isAdd)
        {

            FormBoSungNhomSP f = new FormBoSungNhomSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }          
        }
        private void showPKBoSungThietBi(bool isAdd)
        {
            ThietBiGCBoSungForm f = new ThietBiGCBoSungForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }          
        }
        private void showPKDieuChinhThietBi(bool isAdd)
        {
            FormDieuChinhSLThietBi f = new FormDieuChinhSLThietBi();
            f.HD = HD;
            f.pkdk = pkdk;
            f.boolFlag = this.boolFlag;  
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }          
        }
        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (showMsg("MSG_DEL01",true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa phụ kiện này không?", true) == "Yes")
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = (Company.GC.BLL.KDT.GC.LoaiPhuKien)e.Row.DataRow;
                    if (LoaiPK.ID > 0)
                    {
                        LoaiPK.Delete(this.pkdk.HopDong_ID);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

    
       


        private void txtSoPhuKien_TextChanged(object sender, EventArgs e)
        {
           // this.isUpdate = true;
        }

        private void PhuKienGCSendForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void uiCommandBar1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }
    }
}
