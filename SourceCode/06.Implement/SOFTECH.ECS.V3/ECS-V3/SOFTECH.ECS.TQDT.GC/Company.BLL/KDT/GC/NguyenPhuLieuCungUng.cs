﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.KDT.GC
{
    public partial class NguyenPhuLieuCungUng
    {
        public DataSet SelectNPLCungUng()
        {
            string sql = "select ";
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(cmd);
        }
        public bool Load(SqlTransaction trans)
        {
            string spName = "p_KDT_GC_NguyenPhuLieuCungUng_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this._Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) this._MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCung"))) this._LuongCung = reader.GetDecimal(reader.GetOrdinal("LuongCung"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) this._TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucCungUng"))) this._DinhMucCungUng = reader.GetDouble(reader.GetOrdinal("DinhMucCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHH"))) this._TyLeHH = reader.GetDouble(reader.GetOrdinal("TyLeHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("HinhThuCungUng"))) this._HinhThuCungUng = reader.GetString(reader.GetOrdinal("HinhThuCungUng"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool Load(SqlTransaction trans, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_NguyenPhuLieuCungUng_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);

            IDataReader reader = null;
            if (trans != null)
                reader = this.db.ExecuteReader(dbCommand, trans);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this._Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) this._MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCung"))) this._LuongCung = reader.GetDecimal(reader.GetOrdinal("LuongCung"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) this._TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucCungUng"))) this._DinhMucCungUng = reader.GetDouble(reader.GetOrdinal("DinhMucCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHH"))) this._TyLeHH = reader.GetDouble(reader.GetOrdinal("TyLeHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("HinhThuCungUng"))) this._HinhThuCungUng = reader.GetString(reader.GetOrdinal("HinhThuCungUng"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public void Delete(HopDong HD, SanPhanCungUng sp)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                    NPLDuyet.HopDong_ID = HD.ID;
                    NPLDuyet.Ma = this.MaNguyenPhuLieu;
                    NPLDuyet.Load(transaction);
                    NPLDuyet.SoLuongDaDung -= this.LuongCung;
                    decimal DinhMucChung = (new Company.GC.BLL.GC.DinhMuc()).GetDinhMucChungOfMaSPAndMaNPL(sp.MaSanPham, this.MaNguyenPhuLieu, HD.ID, transaction);
                    decimal LuongSuDung = DinhMucChung * sp.LuongCUSanPham;
                    NPLDuyet.SoLuongDaDung += LuongSuDung;
                    NPLDuyet.UpdateTransaction(transaction);
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void Delete(HopDong HD, SqlTransaction transaction, SanPhanCungUng sp)
        {
            Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLDuyet.HopDong_ID = HD.ID;
            NPLDuyet.Ma = this.MaNguyenPhuLieu;
            NPLDuyet.Load(transaction);
            // NPLDuyet.SoLuongDaDung -= this.LuongCung;
            NPLDuyet.SoLuongCungUng -= this.LuongCung;
            //decimal DinhMucChung = (new Company.GC.BLL.GC.DinhMuc()).GetDinhMucChungOfMaSPAndMaNPL(sp.MaSanPham, this.MaNguyenPhuLieu, HD.ID, transaction);
            //decimal LuongSuDung = DinhMucChung * sp.LuongCUSanPham;
            //NPLDuyet.SoLuongDaDung += LuongSuDung;
            NPLDuyet.UpdateTransaction(transaction);
            this.DeleteTransaction(transaction);
        }

        public NguyenPhuLieuCungUngCollection SelectCollectionBy_Master_ID(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_NguyenPhuLieuCungUng_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            NguyenPhuLieuCungUngCollection collection = new NguyenPhuLieuCungUngCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand, transaction);
            while (reader.Read())
            {
                NguyenPhuLieuCungUng entity = new NguyenPhuLieuCungUng();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCung"))) entity.LuongCung = reader.GetDecimal(reader.GetOrdinal("LuongCung"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucCungUng"))) entity.DinhMucCungUng = reader.GetDouble(reader.GetOrdinal("DinhMucCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHH"))) entity.TyLeHH = reader.GetDouble(reader.GetOrdinal("TyLeHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("HinhThuCungUng"))) entity.HinhThuCungUng = reader.GetString(reader.GetOrdinal("HinhThuCungUng"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public NguyenPhuLieuCungUngCollection SelectCollectionBy_Master_ID(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_NguyenPhuLieuCungUng_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            NguyenPhuLieuCungUngCollection collection = new NguyenPhuLieuCungUngCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                NguyenPhuLieuCungUng entity = new NguyenPhuLieuCungUng();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCung"))) entity.LuongCung = reader.GetDecimal(reader.GetOrdinal("LuongCung"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucCungUng"))) entity.DinhMucCungUng = reader.GetDouble(reader.GetOrdinal("DinhMucCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHH"))) entity.TyLeHH = reader.GetDouble(reader.GetOrdinal("TyLeHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("HinhThuCungUng"))) entity.HinhThuCungUng = reader.GetString(reader.GetOrdinal("HinhThuCungUng"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //kiem tra xem cung ung cua npl cho mot san pham trong mot to khai xuat da dc khai bao chua
        public long checkExitNPLCungUngTKMD(string MaSanPham, string MaNPL, long TKMD_ID, long BKCungUng_ID)
        {
            string sql = "select id from t_View_KDT_NPLCungUng where TKMD_ID=@TKMD_ID and MaSanPham=@MaSanPham and MaNguyenPhuLieu=@MaNguyenPhuLieu and ID<>@ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, BKCungUng_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNPL);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            object o = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);

        }
        public long checkExitNPLCungUngTKCT(string MaSanPham, string MaNPL, long TKCT_ID, long BKCungUng_ID)
        {
            string sql = "select id from t_View_KDT_NPLCungUng where TKCT_ID=@TKCT_ID and MaSanPham=@MaSanPham and MaNguyenPhuLieu=@MaNguyenPhuLieu and ID<>@ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, BKCungUng_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNPL);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, TKCT_ID);
            object o = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);

        }

        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NguyenPhuLieuCungUng_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
            this.db.AddInParameter(dbCommand, "@LuongCung", SqlDbType.Decimal, this._LuongCung);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@DinhMucCungUng", SqlDbType.Float, this._DinhMucCungUng);
            this.db.AddInParameter(dbCommand, "@TyLeHH", SqlDbType.Float, this._TyLeHH);
            this.db.AddInParameter(dbCommand, "@HinhThuCungUng", SqlDbType.VarChar, this._HinhThuCungUng);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_NguyenPhuLieuCungUng_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, this._MaNguyenPhuLieu);
            this.db.AddInParameter(dbCommand, "@LuongCung", SqlDbType.Decimal, this._LuongCung);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@DinhMucCungUng", SqlDbType.Float, this._DinhMucCungUng);
            this.db.AddInParameter(dbCommand, "@TyLeHH", SqlDbType.Float, this._TyLeHH);
            this.db.AddInParameter(dbCommand, "@HinhThuCungUng", SqlDbType.VarChar, this._HinhThuCungUng);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public  DataSet ViewNPLCungUng(long HopDongID)
        {
            string spName = "p_NPLCungUngDaDangKy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDongID", SqlDbType.BigInt, HopDongID);

            return db.ExecuteDataSet(dbCommand);
        }
    }
}
