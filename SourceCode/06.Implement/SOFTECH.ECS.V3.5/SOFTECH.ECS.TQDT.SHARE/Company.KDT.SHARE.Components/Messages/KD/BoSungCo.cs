﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class BoSungCo : DeclarationBase
    {
        [XmlElement("DeclarationDocument")]
        public DeclarationBase DeclarationDocument { get; set; }
        [XmlArray("CertificateOfOrigins")]
        [XmlArrayItem("CertificateOfOrigin")]
        public List<CertificateOfOrigin> CertificateOfOrigins { get; set; }

    }
}
