﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.Send;
using System.Runtime.InteropServices;
using System.Threading;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.Components;
using SignRemote;
using Company.KDT.SHARE.VNACCS.Controls;
using Helper.Controls;
using Company.KDT.SHARE.Components.Messages.SmartCA;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Asn1.Pkcs;
//using VnptHashSignatures.Interface;
//using VnptHashSignatures.Common;
//using VnptHashSignatures.Cms;

namespace Company.Interface
{
    public partial class SendmsgVNACCFrm : BaseForm
    {
        public string msgSend;
        public ResponseVNACCS feedback;
        MessagesSend messagesSend;
        public bool result;
        public string inputMSGID;
        public List<ColumnError> columnErrors;
        /// <summary>
        /// Số TK VNACCS gan dung
        /// </summary>
        public string SoToKhai;
        public SendmsgVNACCFrm(MessagesSend MessagesSend)
            : this(MessagesSend, "")
        {

        }
        public SendmsgVNACCFrm(MessagesSend MessagesSend, string _msgSend)
        {
            try
            {
                InitializeComponent();
                if (MessagesSend != null && string.IsNullOrEmpty(_msgSend))
                {
                    this.messagesSend = MessagesSend;
                    this.msgSend = HelperVNACCS.BuildEdiMessages(MessagesSend).ToString();
                }
                else if (!string.IsNullOrEmpty(_msgSend))
                {
                    this.messagesSend = MessagesSend;
                    this.msgSend = _msgSend;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Close();
            }

        }

        public bool isRep { get; set; }
        public bool isSend { get; set; }

        #region     Send messages
        public string msgFeedBack;
        public void setCaption(string str)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblStatus.Text = str; }));
            }
            else
                lblStatus.Text = str;
        }
        private byte[] postData;
        public void SendMessEdi(object obj)
        {
            try
            {

                setCaption("Đang ngừng phản hồi tự động");
                while (GlobalVNACC.isResponding)
                {
                    Thread.Sleep(500);
                    GlobalVNACC.isStopRespone = true;
                }

                setCaption("đính kèm chữ ký số vào messages");
                System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = null;
                MIME mess = new MIME();
                if (Company.KDT.SHARE.Components.Globals.IsSignature)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName))
                        {
                            x509Certificate2 = Company.KDT.SHARE.Components.Cryptography.GetStoreX509Certificate2New(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName);
                        }
                        else
                        {
                            msgFeedBack = "Chưa cấu hình chữ ký số";
                            this.DialogResult = DialogResult.Cancel;
                            return;
                        }
                        if (x509Certificate2 == null)
                        {
                            msgFeedBack = "Không tìm thấy chữ ký số ";
                            this.DialogResult = DialogResult.Cancel;
                            return;
                        }
                        mess = HelperVNACCS.GetMIME(msgSend, x509Certificate2);
                    }
                    catch (System.Exception ex)
                    {
                        msgFeedBack = "Lỗi load chữ ký số: " + ex.Message;
                        this.DialogResult = DialogResult.Cancel;
                        return;
                    }
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                {

                    this.setCaption("Đang thực hiện ký số qua mạng nội bộ");
                    mess = HelperVNACCS.GetMIME(msgSend, GetSignatureOnLan(msgSend));
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
                {
                    this.setCaption("Đang thực hiện ký số Online");
                    mess = HelperVNACCS.GetMIME(msgSend, GetSignatureRemote(msgSend));
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignSmartCA)
                {
                    this.setCaption("Đang thực hiện ký số Online");
                    mess = HelperVNACCS.GetMIME(msgSend, GetSignSmartCA(msgSend));
                }
                else
                {
                    msgFeedBack = "Chưa cấu hình chữ ký số";

                    this.DialogResult = DialogResult.Cancel;
                }



                Thread.Sleep(500);
                setCaption("Đóng gói messages VNACCS EDI");
                string boundary = mess.boundary;
                //boundary = mime.Boundary;
                //mess = mime.GetMime();
                //mess = mime.GetEntireBody();
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(GlobalVNACC.Url);

                if (string.IsNullOrEmpty(boundary))
                {
                    msgFeedBack = "Boundary bị rỗng";
                    this.DialogResult = DialogResult.Cancel;
                    return;
                }
                postData = UTF8Encoding.UTF8.GetBytes(mess.GetMessages().ToString());
                //string txt = string.Empty;
                //StreamReader streamReader = new StreamReader(Application.StartupPath + "\\Test\\HYS.txt");
                //txt = streamReader.ReadToEnd();
                //streamReader.Close();
                //postData = UTF8Encoding.UTF8.GetBytes(txt);
                string result = System.Text.Encoding.UTF8.GetString(postData);
                try
                {
                    if (GlobalVNACC.IsGenerateTextDataVNACC)
                    {
                        GlobalVNACC.GenerateTextData(messagesSend.Header.MaNghiepVu.GetValue(true).ToString(), UTF8Encoding.UTF8.GetBytes(mess.GetMessages().ToString()));
                    }
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("SendmsgVNACCFrm.GenerateTextDataVNACC", ex); }

                boundary = boundary.Substring(2, boundary.Length - 2);
                httpWebRequest.ContentType = "multipart/signed; micalg=\"sha256\";boundary=\"" + boundary + "\"; protocol=\"application/x-pkcs7-signature\"";

                httpWebRequest.Method = "POST";
                httpWebRequest.KeepAlive = false;
                httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
                httpWebRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                httpWebRequest.UserAgent = "SOFTECH";
                httpWebRequest.ProtocolVersion = HttpVersion.Version11;
                httpWebRequest.ContentLength = postData.Length;
                // httpWebRequest.Expect = "100-continue";
                httpWebRequest.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), httpWebRequest);
                //LoadingProcess();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                msgFeedBack = ex.Message;
                this.DialogResult = DialogResult.Cancel;
            }
        }
        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                setCaption("Đang gửi dữ liệu...");
                Thread.Sleep(500);
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

                // End the operation
                Stream postStream = request.EndGetRequestStream(asynchronousResult);

                //Console.WriteLine("Please enter the input data to be posted:");
                //string postData = Console.ReadLine();

                // Convert the string into a byte array. 
                // byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Write to the request stream.
                postStream.Write(postData, 0, postData.Length);
                postStream.Close();

                // Start the asynchronous operation to get the response
                request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
            }
            catch (Exception e)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                        {
                            this.DialogResult = DialogResult.Cancel;
                            this.msgFeedBack = e.Message;
                        }));
                }
                else
                {
                    this.msgFeedBack = e.Message;
                    this.DialogResult = DialogResult.Cancel;
                }

            }

        }

        private void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                setCaption("Đang nhận dữ liệu...");
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

                // End the operation
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse, Encoding.UTF8);
                string responseString = streamRead.ReadToEnd();
                // Company.KDT.SHARE.VNACCS.HelperVNACCS.SaveFileEdi(Application.StartupPath, new StringBuilder(responseString), "Return" + DateTime.Now.ToString("yyyyMMddhhmmss"));
                //                 if (InvokeRequired)
                //                 {
                //                     this.Invoke(new MethodInvoker(delegate { txtResult.Text = responseString; UnLoadingProcess(); }));
                //                 }
                //                 else
                //                     textBox2.Text = responseString;
                //Console.WriteLine(responseString);
                // Close the stream object
                streamResponse.Close();
                streamRead.Close();

                // Release the HttpWebResponse
                response.Close();
                //allDone.Set();
                //msgFeedBack = responseString;0000000
                MsgLog.SaveMessages(responseString, this.messagesSend.Header.MaNghiepVu.GetValue().ToString() + " Return", 0, EnumThongBao.ReturnMess, "", this.messagesSend.Header.MessagesTag.GetValue().ToString(), this.messagesSend.Header.InputMessagesID.GetValue().ToString(), this.messagesSend.Header.IndexTag.GetValue().ToString());
                feedback = new ResponseVNACCS(responseString);
                msgFeedBack = feedback.GetError();
                if (feedback.Error != null && feedback.Error.Count > 0)
                {
                    //this.DialogResult = DialogResult.No;
                    Helper.Controls.ErrorMessageBoxForm_VNACC.ShowMessage(messagesSend.Header.MaNghiepVu.GetValue().ToString(), feedback.Error, false, Helper.Controls.MessageType.Error);
                    columnErrors = ErrorMessageBoxForm_VNACC.columnErrors;
                    this.DialogResult = DialogResult.No;
                }
                else
                {
                    if (isRep)
                    {
                        SetGif(false);
                        ucRespone1.DoWork(null);

                    }
                    else
                        this.DialogResult = DialogResult.Yes;
                }

            }
            catch (Exception e)
            {
                msgFeedBack = e.Message;
                this.DialogResult = DialogResult.Cancel;
                Logger.LocalLogger.Instance().WriteMessage(e);
            }


        }
        #endregion

        private void SendVNACCFrm_Load(object sender, EventArgs e)
        {
            //progressBar1.MarqueeAnimationSpeed = 50;
            ucRespone1.InputMsgID = this.inputMSGID;
            ucRespone1.bindingThongBao();
            ucRespone1.UcEvent += new EventHandler<Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs>(ucRespone1_UcEvent);
            ucRespone1.HandlerID += new EventHandler<Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs>(ucRespone1_HandlerID);
            ucRespone1.HandlerXuLyMSG += new EventHandler<Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs>(ucRespone1_HandlerXuLyMSG);
            ucRespone1.USER = GlobalSettings.USER;
            ucRespone1.PASS = GlobalSettings.PASS;
            ucRespone1.MA_DON_VI = GlobalSettings.MA_DON_VI;
            ucRespone1.SERVER_NAME = GlobalSettings.SERVER_NAME;
            if (isSend)
            {
                grbSend.Visible = true;
                ThreadPool.QueueUserWorkItem(SendMessEdi);
            }
            else
                grbSend.Visible = false;

            if (isRep)
                grbRequest.Visible = true;
            else
                grbRequest.Visible = false;
            if (!grbSend.Visible)
            {
                grbRequest.Dock = DockStyle.Fill;
                ucRespone1.InputMsgID = this.inputMSGID;
                ucRespone1.SoToKhai = this.SoToKhai;
                ucRespone1.bindingThongBao();
                ControlBox = true;
            }
            else if (!grbRequest.Visible)
            {
                this.Size = grbSend.Size;
                this.Height = grbSend.Size.Height + 40;

                grbSend.Dock = DockStyle.Fill;
            }
            SetGif(isSend);

        }

        void ucRespone1_HandlerXuLyMSG(object sender, Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs e)
        {

            try
            {

                if (e.Message == "TKMD")
                {
                    KDT_VNACC_ToKhaiMauDich Tkmd = (KDT_VNACC_ToKhaiMauDich)e.Data;

                    if (Tkmd != null)
                    {
#if SXXK_V4
                        Company.BLL.VNACCS.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(Tkmd);
#elif GC_V4
                        Company.GC.BLL.VNACC.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(Tkmd,GlobalSettings.MA_DON_VI != "4000395355");
#endif
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        void ucRespone1_HandlerID(object sender, Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs e)
        {
            ProcessReport.ShowReport(Convert.ToInt64(e.Data), e.Message);
        }
        void ucRespone1_UcEvent(object sender, Company.KDT.SHARE.VNACCS.Controls.ucResponeEventArgs e)
        {
            setCaption(e.Message);
            if (e.isFinish)
                setFormClose(e.isError);
            result = !e.isError;
        }
        private void SetGif(bool send)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                    {
                        pbSend.Visible = send;
                        pbRequest.Visible = !send;
                    }));
            }
            else
            {
                pbSend.Visible = send;
                pbRequest.Visible = !send;
            }
        }
        private void setFormClose(bool isError)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                    {
                        this.ControlBox = true;
                        if (isError)
                        {
                            pbRequest.Visible = false;
                            pbSend.Visible = false;
                        }
                        else
                        {
                            grbSend.Visible = false;
                            grbRequest.Dock = DockStyle.Fill;
                            result = true;
                        }
                        ControlBox = true;
                    }
                ));
            }
        }

        #region ký số từ xa
        private int CountRequest(decimal len)
        {
            int timeRequest = 0;
            Random rnd1 = new Random();

            if (len < 1024)
                timeRequest = rnd1.Next(3, 7);
            else if (len > 1024 & len < 1048576)
                timeRequest = rnd1.Next(5, 9);
            else if (len > 1048576 & len < 107341824)
                timeRequest = rnd1.Next(10, 20);
            else timeRequest = rnd1.Next(30, 60);
            return timeRequest;
        }
        private string GetSignatureRemote(string ediMess)
        {
            ISignMessage signRemote = WebService.SignMessage();
            string guidStr = Guid.NewGuid().ToString();
            MessageSend msgSend = new MessageSend()
            {
                From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                SendStatus = SendStatusInfo.SendVNACCS,
                Message = new SignRemote.Message()
                {
                    Content = Helpers.ConvertToBase64VNACCS(ediMess),
                    GuidStr = guidStr
                },
                To = GlobalSettings.MA_DON_VI.Trim()
            };

            string msg = Helpers.Serializer(msgSend, true, true);
            setCaption("Gửi ký thông tin");
            string msgRet = signRemote.ClientSend(msg);
            msgSend = Helpers.Deserialize<MessageSend>(msgRet);
            if (msgSend.SendStatus == SendStatusInfo.Error)
            {
                throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
            }
            MessageSend msgFeedBack = new MessageSend()
            {
                From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                SendStatus = SendStatusInfo.Request,
                Message = new SignRemote.Message()
                {
                    GuidStr = guidStr
                },
                To = GlobalSettings.MA_DON_VI.Trim()
            };
            msg = Helpers.Serializer(msgFeedBack, true, true);

            bool isWait = true;
            int countSend = 1;
            int countRequest = CountRequest(msg.Length);
            while (isWait && countSend < 15)
            {
                int count = countRequest;
                //string title = this._title;
                string title = string.Format("Chờ phản hồi ký số lần thứ {0}", countSend);
                while (count > 0)
                {
                    setCaption(string.Format(title + ": {0} giây", count));
                    Thread.Sleep(1000);
                    count--;
                }
                //this._title = title;
                msgRet = signRemote.ClientSend(msg);
                msgSend = Helpers.Deserialize<MessageSend>(msgRet);
                if (msgSend.SendStatus == SendStatusInfo.Error)
                {
                    throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
                }
                else if (msgSend.SendStatus == SendStatusInfo.Sign || msgSend.SendStatus == SendStatusInfo.SignVNACCS)
                {
                    return msgSend.Message.SignData;
                }
                else if (msgSend.SendStatus == SendStatusInfo.Wait)
                    countSend++;
            }
            throw new Exception(string.Format("ID={0}. Ký số từ xa thất bại\nMáy chủ đã không phản hồi thông tin trong thời gian giao dịch", guidStr));
        }
        private string GetSignatureOnLan(string content)
        {
            //try
            //{
            string guidStr = Guid.NewGuid().ToString();
            string cnnString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { GlobalSettings.SERVER_NAME, Company.KDT.SHARE.Components.Globals.DataSignLan, GlobalSettings.USER, GlobalSettings.PASS });
            string MSG_FROM = System.Environment.MachineName.ToString();
            INBOX.InsertSignContent(cnnString, guidStr, Helpers.ConvertToBase64VNACCS(content), SendStatusInfo.SendVNACCS, MSG_FROM);
            int count = 0;
            int timeWait = 15;
            while (count < timeWait)
            {
                if (Company.KDT.SHARE.Components.Globals.MA_DON_VI != "4000395355")
                {
                    OUTBOX outbox = OUTBOX.GetContentSignLocal(cnnString, guidStr);
                    if (outbox != null && outbox.MSG_SIGN_DATA != null)
                    {
                        return outbox.MSG_SIGN_DATA;
                    }
                    else
                    {
                        count++;
                        setCaption(string.Format("Chờ phản hồi ký số : {0} giây", count));
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    //OUTBOX outbox = OUTBOX.GetContentSignLocal(cnnString, guidStr);
                    List<OUTBOX> outBoxCollection = OUTBOX.GetCollectionContentSignLocal(cnnString, guidStr);
                    foreach (OUTBOX item in outBoxCollection)
                    {
                        if (item != null && item.MSG_SIGN_DATA != null)
                        {
                            return item.MSG_SIGN_DATA;
                        }
                        else
                        {
                            count++;
                            setCaption(string.Format("Chờ phản hồi ký số : {0} giây", count));
                            Thread.Sleep(1000);
                        }
                    }
                }

            }
            throw new Exception("Đã gửi yêu cầu ký để khai báo\r\nTuy nhiên máy chủ đã không ký thông tin yêu cầu\r\nVui lòng xem thiết lập chữ ký số trên máy chủ");
            //}
            //catch (Exception ex)
            //{
            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi GetSignatureOnLan. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    return null;
            //}

        }
        //public const string URL_CREDENTIALSLIST = "https://rmgateway.vnptit.vn/csc/credentials/list";
        //public const string URL_CREDENTIALSINFO = "https://rmgateway.vnptit.vn/csc/credentials/info";
        //public const string URL_SIGNHASH = "https://rmgateway.vnptit.vn/csc/signature/signhash";
        //public const string URL_SIGN = "https://rmgateway.vnptit.vn/csc/signature/sign";
        //public const string URL_GETTRANINFO = "https://rmgateway.vnptit.vn/csc/credentials/gettraninfo";
        public string GetSignSmartCA(string ediMess)
        {
            //var customerEmail = "0400392263123";
            //var customerPass = "Tchq@12345";
            var refresh_token = Company.KDT.SHARE.Components.Globals.REFRESH_TOKEN;
            var access_token = Company.KDT.SHARE.Components.Globals.ACCESS_TOKEN;
            DateTime expires_in = Convert.ToDateTime(Company.KDT.SHARE.Components.Globals.EXPIRES_TOKEN);
            setCaption("Kiểm tra hiệu lực của Token");
            Thread.Sleep(300);
            if (!String.IsNullOrEmpty(access_token))
            {
                if (DateTime.Now > expires_in)
                {
                    setCaption("Lấy thông tin Token mới");
                    Thread.Sleep(300);
                    access_token = HelperVNACCS.GetAccessToken(Company.KDT.SHARE.Components.Globals.UserNameSmartCA, Company.KDT.SHARE.Components.Globals.PaswordSmartCA, refresh_token);
                    if (access_token == null)
                    {
                        throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được chuỗi Access Token"));
                    }
                }
            }
            else
            {
                setCaption("Lấy thông tin Token mới");
                Thread.Sleep(300);
                access_token = HelperVNACCS.GetAccessToken(Company.KDT.SHARE.Components.Globals.UserNameSmartCA, Company.KDT.SHARE.Components.Globals.PaswordSmartCA, refresh_token);
                if (access_token == null)
                {
                    throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được chuỗi Access Token"));
                }
            }
            setCaption("Lấy danh sách Chữ ký số");
            Thread.Sleep(300);
            String credential = HelperVNACCS.GetCredentialSmartCA(access_token, Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSLIST);
            if (credential == null)
            {
                throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được danh sách Chữ ký số"));
            }
            setCaption("Lấy Thông tin Chữ ký số");
            Thread.Sleep(300);
            String certBase64 = HelperVNACCS.GetAccoutSmartCACert(access_token, Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSINFO, credential);
            if (certBase64 == null)
            {
                throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được Chữ ký số"));
            }
            //var data1 = Encoding.UTF8.GetBytes(certBase64);
            //X509Certificate2 cert = new X509Certificate2(data1);

            //var data = Encoding.UTF8.GetBytes(ediMess);
            //var digestOid = new Oid("1.2.840.113549.1.7.2"); //pkcs7 signed

            //var content = new ContentInfo(digestOid, data);

            //var signedCms = new SignedCms(SubjectIdentifierType.IssuerAndSerialNumber, content, true); //detached=true
            //var singer = new CmsSigner(cert) { DigestAlgorithm = new Oid("1.3.14.3.2.26") };
            //signedCms.ComputeSignature(singer, false);
            //var signEnv = signedCms.Encode();
            //return Convert.ToBase64String(signEnv);

            //duydp Comment 18/02/2022 
            //IHashSigner signers = HashSignerFactory.GenerateSigner(Encoding.UTF8.GetBytes(ediMess), certBase64, null, HashSignerFactory.CMS);

            //((CmsHashSigner)signers).SetHashAlgorithm(MessageDigestAlgorithm.SHA256);

            //var hashValues = signers.GetSecondHashAsBase64();

            var data = Encoding.UTF8.GetBytes(ediMess);
            var hashValues = Convert.ToBase64String(data);

            setCaption("Lấy Thông tin giao dịch Chữ ký số");
            Thread.Sleep(300);
            var tranId = HelperVNACCS.SignHash(access_token, Company.KDT.SHARE.Components.Globals.URL_SIGNHASH, hashValues, credential);
            if (tranId == null)
            {
                throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được Thông tin giao dịch Chữ ký số"));
            }
            bool isWait = true;
            int countSend = 1;
            var datasigned = "";
            int countRequest = 60;
            while (isWait && countSend < 6)
            {
                int count = countRequest;
                string title = string.Format("Chờ người dùng xác nhận lần {0}", countSend);
                while (count > 0)
                {
                    setCaption(string.Format(title + ": {0} giây", count));
                    var getTranInfo = HelperVNACCS.GetTranInfo(access_token, Company.KDT.SHARE.Components.Globals.URL_GETTRANINFO, tranId);
                    if (getTranInfo != null)
                    {
                        if (getTranInfo.Content.TranStatus == TranStatus.WAITING_FOR_SIGNER_CONFIRM.ToString())
                        {
                            Thread.Sleep(1000);
                            count--;
                        }
                        else
                        {
                            switch (getTranInfo.Content.TranStatus)
                            {
                                case "1":
                                    //SHA512Managed HashTool = new SHA512Managed();
                                    //Byte[] PhraseAsByte = System.Text.Encoding.UTF8.GetBytes(string.Concat(getTranInfo.Content.Documents[0].Sig));
                                    //Byte[] EncryptedBytes = HashTool.ComputeHash(PhraseAsByte);
                                    //HashTool.Clear();
                                    //var d = Convert.ToBase64String(EncryptedBytes);
                                    //var e = Encoding.UTF8.GetBytes(getTranInfo.Content.Documents[0].Sig);
                                    //var f = Convert.ToBase64String(e);
                                    //var b = signers.Sign(getTranInfo.Content.Documents[0].Sig);
                                    //var data2 = Encoding.UTF8.GetBytes(getTranInfo.Content.Documents[0].Sig);
                                    //var abc = Convert.ToBase64String(b);
                                    datasigned = getTranInfo.Content.Documents[0].Sig;

                                    //duydp Comment 18/02/2022 

                                    //if (!signers.CheckHashSignature(datasigned))
                                    //{
                                    //    return null;
                                    //}
                                    //byte[] signed = signers.Sign(datasigned);

                                    byte[] signed = Encoding.UTF8.GetBytes(datasigned);
                                    File.WriteAllBytes(@"C:\Users\dangp\Downloads\" + getTranInfo.Content.TranId + ".txt", signed);

                                    return datasigned = Convert.ToBase64String(signed);//"Rfb5K1ci52GaK5tUZpveKMhvslpYEQlcXd34aAHEm98DfDbfDqxEcnHiIa52xn19b0W5Ps+EdNWbfEzZq2TkHQTnL4oaPob730jZEkB27g27puVprR843E13ofOlEtcgW5xoB8S11p8JrVvJNxtu7y6WrlXAWV2BOZ1bAIUNInQ=";// Convert.ToBase64String(abc);//getTranInfo.Content.Documents[0].Sig;//Convert.ToBase64String(data1);
                                    break;
                                case "4001":
                                    throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.EXPIRED));
                                    break;
                                case "4002":
                                    throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.SIGNER_REJECTED));
                                    break;
                                case "4003":
                                    throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.AUTHORIZE_KEY_FAILED));
                                    break;
                                case "4004":
                                    throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.SIGN_FAILED));
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(string.Format("ID={0}. Ký số không thành công\nKhông xác nhận được Thông tin chữ ký số giao dịch", tranId));
                    }
                }
                countSend++;
            }
            return datasigned;
        }
        #endregion
    }

}
