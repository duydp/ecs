﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.GC
{
    public partial class AutoUpdateLenhSanXuatForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_LenhSanXuat_QuyTac Config = new KDT_LenhSanXuat_QuyTac();

        public KDT_LenhSanXuat lenhSanXuat = new KDT_LenhSanXuat();
        public KDT_LenhSanXuat_SP lenhSanXuatSP = new KDT_LenhSanXuat_SP();

        public List<SXXK_DinhMuc> DinhMucCollection = new List<SXXK_DinhMuc>();
        public DinhMucCollection SPCollection = new DinhMucCollection();
        public DinhMucCollection LenhSXCollection = new DinhMucCollection();

        List<HoSoThanhLyDangKy> HSTLCollection = new List<HoSoThanhLyDangKy>();
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public AutoUpdateLenhSanXuatForm()
        {
            InitializeComponent();
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            QuyTacTaoLenhSanXuatForm f = new QuyTacTaoLenhSanXuatForm();
            f.Show(this);
        }
        private void BindData()
        {
            try
            {
                string where = " MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                this.HSTLCollection = HoSoThanhLyDangKy.SelectCollectionDynamic(where, "ID desc");
                dgList.DataSource = this.HSTLCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void AutoUpdateLenhSanXuatForm_Load(object sender, EventArgs e)
        {
            cbbSoPO.SelectedValue = "0";
            cbbTinhTrang.SelectedValue = "2";
            BindData();
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private string GetConfigAuto(String TinhTrang)
        {
            try
            {
                String LenhSanXuat = "";
                String FirstStringLenhSanXuat = "";
                String LastStringLenhSanXuat = "";
                int lenghtFirst;
                int lenghtLast;
                int lenghtIndex;
                List<KDT_LenhSanXuat_QuyTac> ConfigCollection = KDT_LenhSanXuat_QuyTac.SelectCollectionAll();

                foreach (KDT_LenhSanXuat_QuyTac item in ConfigCollection)
                {
                    string Value = "";
                    switch (TinhTrang)
                    {
                        case "0":
                            Value = "M";
                            break;
                        case "1":
                            Value = "Đ";
                            break;
                        case "2":
                            Value = "H";
                            break;
                        default:
                            break;
                    }
                    if (item.TinhTrang == Value)
                    {
                        lenghtFirst = item.TienTo.Length + item.TinhTrang.Length + item.LoaiHinh.Length + Convert.ToInt32(item.DoDaiSo) - 1;
                        lenghtLast = item.HienThi.Length - lenghtFirst;
                        FirstStringLenhSanXuat = item.HienThi.Substring(0, lenghtFirst - 1);
                        LastStringLenhSanXuat = item.HienThi.Substring(lenghtFirst, lenghtLast);
                        string GetLenhSanXuat = KDT_LenhSanXuat_QuyTac.GetLenhSanXuat(TinhTrang);
                        if (!String.IsNullOrEmpty(GetLenhSanXuat))
                        {
                            if (GetLenhSanXuat.Length >= item.HienThi.Length)
                            {
                                if (GetLenhSanXuat.Substring(0, lenghtFirst - 1) != FirstStringLenhSanXuat)
                                {
                                    LenhSanXuat = item.HienThi;
                                }
                                else
                                {
                                    string[] Temp = GetLenhSanXuat.Split(new string[] { "-" }, StringSplitOptions.None);
                                    string First = Temp[0].ToString();
                                    int LastIndex = Convert.ToInt32(First.Substring(lenghtFirst - 1, First.Length - (lenghtFirst - 1)));
                                    LenhSanXuat = FirstStringLenhSanXuat + (LastIndex + 1) + LastStringLenhSanXuat;
                                }
                            }
                            else
                            {
                                LenhSanXuat = item.HienThi;
                            }
                        }
                        else
                        {
                            LenhSanXuat = item.HienThi;
                        }
                    }
                }
                return LenhSanXuat;
            }
            catch (Exception ex)
            {
                return "";
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HSTL = new HoSoThanhLyDangKy();
                        HSTL = (HoSoThanhLyDangKy)i.GetRow().DataRow;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(cbbSoPO, errorProvider, "Số đơn hàng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbTinhTrang, errorProvider, "Tình trạng", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void bttAuto_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(false))
                return;
            bttAuto.Enabled = false;
            btnClose.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void DoWork(object obj)
        {
            try
            {
                SetError(string.Empty);
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    HSTL = (HoSoThanhLyDangKy)item.DataRow;
                    HSTL.LoadBKCollection();
                    foreach (BangKeHoSoThanhLy bk in HSTL.BKCollection)
                    {
                        if (bk.MaBangKe == "DTLTKX")
                        {
                            try
                            {
                                int k = HSTL.getBKToKhaiNhap();
                                HSTL.BKCollection[k].LoadChiTietBangKe();

                                IList<Company.BLL.KDT.HangMauDich> HMDHSTLCollection = new List<Company.BLL.KDT.HangMauDich>();
                                HMDHSTLCollection = Company.BLL.KDT.HangMauDich.SelectDistinctHMDHSTLCollectionDynamic(HSTL.BKCollection[k].ID);
                                LenhSXCollection = DinhMuc.SelectCollectionDynamic(" Master_ID IN (SELECT ID FROM dbo.t_KDT_SXXK_DinhMucDangKy) AND MaSanPham NOT IN (SELECT SP.MaSanPham FROM dbo.t_KDT_LenhSanXuat LSX INNER JOIN dbo.t_KDT_LenhSanXuat_SP SP ON SP.LenhSanXuat_ID = LSX.ID ) AND MaDinhDanhLenhSX IS NOT NULL", "");
                                DinhMucCollection LenhSXHSTL = new DinhMucCollection();
                                foreach (Company.BLL.KDT.HangMauDich HMD in HMDHSTLCollection)
                                {
                                    foreach (DinhMuc lenh in LenhSXCollection)
                                    {
                                        if (HMD.MaPhu == lenh.MaSanPham)
                                        {
                                            DinhMuc lenhSX = new DinhMuc();
                                            lenhSX.MaSanPham = lenh.MaSanPham;
                                            lenhSX.MaDinhDanhLenhSX = lenh.MaDinhDanhLenhSX;
                                            LenhSXHSTL.Add(lenh);
                                        }
                                    }
                                }
                                foreach (DinhMuc lenh in LenhSXHSTL)
                                {
                                    lenhSanXuat = new KDT_LenhSanXuat();
                                    lenhSanXuat.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                    lenhSanXuat.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                                    lenhSanXuat.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                    if (String.IsNullOrEmpty( GetConfigAuto(cbbTinhTrang.SelectedValue.ToString())))
                                    {
                                        KDT_LenhSanXuat_QuyTac  Config = new KDT_LenhSanXuat_QuyTac();
                                        Config.TienTo = "LSX";
                                        Config.TinhTrang = cbbTinhTrang.SelectedValue.ToString();
                                        Config.LoaiHinh = "SX";
                                        Config.GiaTriPhanSo = 1000;
                                        Config.DoDaiSo = 5;
                                        Config.Nam = HSTL.NgayBatDau;
                                        Config.SoDonHang = HSTL.SoQuyetDinh == null ? "" :  HSTL.SoQuyetDinh.ToString();
                                        string Prefix = "";
                                        Prefix = Config.TienTo.ToString().Trim() + Config.TinhTrang.ToString() + Config.LoaiHinh.ToString() + Convert.ToInt64(Config.GiaTriPhanSo + 1) + "-" + Config.Nam.Year + "-" + Config.SoDonHang;
                                        Config.HienThi = Prefix;
                                        Config.Insert();
                                    }
                                    lenhSanXuat.SoLenhSanXuat = GetConfigAuto(cbbTinhTrang.SelectedValue.ToString());
                                    lenhSanXuat.TuNgay = new DateTime (HSTL.NgayBatDau.Year,HSTL.NgayBatDau.Month,HSTL.NgayBatDau.Day,00,00,00);
                                    lenhSanXuat.DenNgay = new DateTime(HSTL.NgayKetThuc.Year, HSTL.NgayKetThuc.Month, HSTL.NgayKetThuc.Day, 23, 59, 59);
                                    lenhSanXuat.SoDonHang = cbbSoPO.SelectedValue == "0" ? HSTL.SoHoSo.ToString() : HSTL.SoQuyetDinh;
                                    lenhSanXuat.TinhTrang = Convert.ToInt32(cbbTinhTrang.SelectedValue.ToString());
                                    lenhSanXuat.GhiChu = txtGhiChu.Text.ToString();
                                    SPCollection = DinhMuc.SelectDistinctCollectionDynamicSP(" Master_ID IN (SELECT ID FROM dbo.t_KDT_SXXK_DinhMucDangKy ) AND MaDinhDanhLenhSX = '" + lenh.MaDinhDanhLenhSX + "'", "");
                                    foreach (DinhMuc items in SPCollection)
                                    {
                                        lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                                        lenhSanXuatSP.MaSanPham = items.MaSanPham;
                                        lenhSanXuatSP.TenSanPham = items.TenSP;
                                        lenhSanXuat.SPCollection.Add(lenhSanXuatSP);
                                    }
                                    if (lenhSanXuat.SPCollection.Count >= 1)
                                    {
                                        foreach (SXXK_SanPham ite in SXXK_SanPham.SelectCollectionDynamic(" MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "'", ""))
                                        {
                                            foreach (KDT_LenhSanXuat_SP items in lenhSanXuat.SPCollection)
                                            {
                                                if (ite.Ma == items.MaSanPham)
                                                {
                                                    items.TenSanPham = ite.Ten;
                                                    items.MaHS = ite.MaHS;
                                                    items.DVT_ID = ite.DVT_ID;
                                                }
                                            }
                                        }
                                        lenhSanXuat.InsertUpdateFull();
                                        DinhMucCollection = SXXK_DinhMuc.SelectCollectionDynamic("LenhSanXuat_ID = 0 ", "");
                                        foreach (SXXK_DinhMuc items in DinhMucCollection)
                                        {
                                            foreach (KDT_LenhSanXuat_SP sp in lenhSanXuat.SPCollection)
                                            {
                                                if (items.MaSanPham == sp.MaSanPham)
                                                {
                                                    items.LenhSanXuat_ID = lenhSanXuat.ID;
                                                    items.Update();
                                                }
                                            }
                                        }
                                    }
                                }
                                //Tạo lệnh sản xuất cho những định mức đã khai báo trước đây nhưng chưa có lệnh
                                List<SXXK_DinhMuc> DinhMucHSTLCollection = new List<SXXK_DinhMuc>();
                                DinhMucCollection = SXXK_DinhMuc.SelectCollectionDynamic("LenhSanXuat_ID = 0 ", "");
                                foreach (Company.BLL.KDT.HangMauDich HMD in HMDHSTLCollection)
                                {
                                    foreach (SXXK_DinhMuc dinhmuc in DinhMucCollection)
                                    {
                                        if (HMD.MaPhu == dinhmuc.MaSanPham)
                                        {
                                            SXXK_DinhMuc entity = new SXXK_DinhMuc();
                                            entity.MaHaiQuan = dinhmuc.MaHaiQuan;
                                            entity.MaDoanhNghiep = dinhmuc.MaDoanhNghiep;
                                            entity.MaSanPham = dinhmuc.MaSanPham;
                                            entity.MaNguyenPhuLieu = dinhmuc.MaNguyenPhuLieu;
                                            entity.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                            entity.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                            entity.DinhMucChung = dinhmuc.DinhMucChung;
                                            entity.GhiChu = dinhmuc.GhiChu;
                                            entity.IsFromVietNam = dinhmuc.IsFromVietNam;
                                            entity.MaDinhDanh = dinhmuc.MaDinhDanh;
                                            entity.SoTiepNhan = dinhmuc.SoTiepNhan;
                                            entity.NgayTiepNhan = dinhmuc.NgayTiepNhan;
                                            entity.TrangThaiXuLy = dinhmuc.TrangThaiXuLy;
                                            entity.LenhSanXuat_ID = dinhmuc.LenhSanXuat_ID;
                                            entity.GuidString = dinhmuc.GuidString;
                                            DinhMucHSTLCollection.Add(entity);
                                        }
                                    }
                                }
                                if (DinhMucHSTLCollection.Count >= 1)
                                {
                                    lenhSanXuat = new KDT_LenhSanXuat();
                                    lenhSanXuat.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                    lenhSanXuat.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                                    lenhSanXuat.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                    if (String.IsNullOrEmpty(GetConfigAuto(cbbTinhTrang.SelectedValue.ToString())))
                                    {
                                        KDT_LenhSanXuat_QuyTac Config = new KDT_LenhSanXuat_QuyTac();
                                        Config.TienTo = "LSX";
                                        Config.TinhTrang = cbbTinhTrang.SelectedValue.ToString();
                                        Config.LoaiHinh = "SX";
                                        Config.GiaTriPhanSo = 1000;
                                        Config.DoDaiSo = 5;
                                        Config.Nam = HSTL.NgayBatDau;
                                        Config.SoDonHang = HSTL.SoQuyetDinh == null ? "" : HSTL.SoQuyetDinh.ToString();
                                        string Prefix = "";
                                        Prefix = Config.TienTo.ToString().Trim() + Config.TinhTrang.ToString() + Config.LoaiHinh.ToString() + Convert.ToInt64(Config.GiaTriPhanSo + 1) + "-" + Config.Nam.Year + "-" + Config.SoDonHang;
                                        Config.HienThi = Prefix;
                                        Config.Insert();
                                    }
                                    lenhSanXuat.SoLenhSanXuat = GetConfigAuto(cbbTinhTrang.SelectedValue.ToString());
                                    lenhSanXuat.TuNgay = new DateTime(HSTL.NgayBatDau.Year, HSTL.NgayBatDau.Month, HSTL.NgayBatDau.Day, 00, 00, 00);
                                    lenhSanXuat.DenNgay = new DateTime(HSTL.NgayKetThuc.Year, HSTL.NgayKetThuc.Month, HSTL.NgayKetThuc.Day, 23, 59, 59);
                                    lenhSanXuat.SoDonHang = cbbSoPO.SelectedValue == "0" ? HSTL.SoHoSo.ToString() : HSTL.SoQuyetDinh;
                                    lenhSanXuat.TinhTrang = Convert.ToInt32(cbbTinhTrang.SelectedValue.ToString());
                                    lenhSanXuat.GhiChu = txtGhiChu.Text.ToString();
                                    string MaSPCheck = String.Empty;
                                    foreach (SXXK_DinhMuc itemss in DinhMucHSTLCollection)
                                    {
                                        if (String.IsNullOrEmpty(MaSPCheck))
                                        {
                                            lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                                            lenhSanXuatSP.MaSanPham = itemss.MaSanPham;
                                            lenhSanXuatSP.TenSanPham = itemss.TenSanPham;
                                            lenhSanXuat.SPCollection.Add(lenhSanXuatSP);
                                            MaSPCheck = itemss.MaSanPham;
                                        }
                                        else if (itemss.MaSanPham != MaSPCheck)
                                        {
                                            lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                                            lenhSanXuatSP.MaSanPham = itemss.MaSanPham;
                                            lenhSanXuatSP.TenSanPham = itemss.TenSanPham;
                                            lenhSanXuat.SPCollection.Add(lenhSanXuatSP);
                                            MaSPCheck = itemss.MaSanPham;
                                        }
                                    }
                                    if (lenhSanXuat.SPCollection.Count >= 1)
                                    {
                                        foreach (SXXK_SanPham ite in SXXK_SanPham.SelectCollectionDynamic(" MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "'", ""))
                                        {
                                            foreach (KDT_LenhSanXuat_SP items in lenhSanXuat.SPCollection)
                                            {
                                                if (ite.Ma == items.MaSanPham)
                                                {
                                                    items.MaHS = ite.MaHS;
                                                    items.DVT_ID = ite.DVT_ID;
                                                }
                                            }
                                        }
                                        lenhSanXuat.InsertUpdateFull();
                                        foreach (SXXK_DinhMuc items in DinhMucHSTLCollection)
                                        {
                                            foreach (KDT_LenhSanXuat_SP sp in lenhSanXuat.SPCollection)
                                            {
                                                if (items.MaSanPham == sp.MaSanPham)
                                                {
                                                    items.LenhSanXuat_ID = lenhSanXuat.ID;
                                                    items.Update();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }                    
                    SetProcessBar((i * 100 / listChecked.Length));
                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        BindData();
                        bttAuto.Enabled = true; btnClose.Enabled = true;

                    }));
                }
                else
                {
                    BindData();
                    bttAuto.Enabled = true; btnClose.Enabled = true;
                }
                SetProcessBar(0);
                SetError("Hoàn thành");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SetError(string.Empty);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
