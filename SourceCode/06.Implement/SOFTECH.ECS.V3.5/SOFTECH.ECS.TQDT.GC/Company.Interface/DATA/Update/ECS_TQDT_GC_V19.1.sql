
/****** Object:  Table [dbo].[t_KDT_GC_CungUngDangKy]    Script Date: 08/22/2014 16:18:01 ******/
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_GC_CungUngDangKy]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_GC_CungUngDangKy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [bigint] NULL,
	[NgayTiepNhan] [datetime] NULL,
	[HopDong_ID] [bigint] NULL,
	[SoHopDong] [varchar](100) NULL,
	[NgayHopDong] [datetime] NULL,
	[NgayHetHan] [datetime] NULL,
	[TrangThaiXuLy] [int] NULL,
	[MaHQ] [varchar](6) NULL,
	[MaDoanhNghiep] [varchar](50) NULL,
	[GuidStr] [varchar](100) NULL,
	[DeXuatKhac] [nvarchar](250) NULL,
	[LyDoSua] [nvarchar](250) NULL,
 CONSTRAINT [PK_t_KDT_GC_CungUngDangKy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

Go

/****** Object:  Table [dbo].[t_KDT_GC_CungUng]    Script Date: 08/22/2014 16:22:04 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_GC_CungUng]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_GC_CungUng](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[MaNPL] [nvarchar](100) NULL,
	[TenNPL] [nvarchar](255) NULL,
	[MaHS] [varchar](10) NULL,
	[DVT_ID] [varchar](6) NULL,
	[LuongCungUng] [numeric](24, 4) NULL,
 CONSTRAINT [PK_t_KDT_GC_CungUngNPL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END


GO
/****** Object:  Table [dbo].[t_KDT_GC_CungUng_Detail]    Script Date: 08/22/2014 16:22:51 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_GC_CungUng_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_GC_CungUng_Detail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[SoToKhaiXuat] [varchar](50) NULL,
	[SoToKhaiVNACCS] [varchar](50) NULL,
	[MaLoaiHinh] [varchar](6) NULL,
	[NgayDangKy] [datetime] NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaSanPham] [nvarchar](100) NULL,
	[TenSanPham] [nvarchar](255) NULL,
	[DVT_ID] [varchar](6) NULL,
	[SoLuong] [numeric](18, 4) NULL,
 CONSTRAINT [PK_t_KDT_GC_CungUngDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END

GO

/****** Object:  Table [dbo].[t_KDT_GC_CungUng_ChungTu]    Script Date: 08/22/2014 16:23:45 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_GC_CungUng_ChungTu]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_GC_CungUng_ChungTu](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[SoChungTu] [varchar](50) NULL,
	[NgayChungTu] [datetime] NULL,
	[LoaiChungTu] [int] NULL,
	[NoiCap] [nvarchar](255) NULL,
	[SoLuong] [numeric](24, 4) NULL,
 CONSTRAINT [PK_t_KDT_GC_CungUngChungTu] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUngDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUngDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUngDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUngDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUngDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUngDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUngDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUngDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUngDangKy_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@HopDong_ID bigint,
	@SoHopDong varchar(100),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_CungUngDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@HopDong_ID,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHan,
	@TrangThaiXuLy,
	@MaHQ,
	@MaDoanhNghiep,
	@GuidStr,
	@DeXuatKhac,
	@LyDoSua
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUngDangKy_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@HopDong_ID bigint,
	@SoHopDong varchar(100),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_GC_CungUngDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[HopDong_ID] = @HopDong_ID,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHan] = @NgayHetHan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[GuidStr] = @GuidStr,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUngDangKy_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@HopDong_ID bigint,
	@SoHopDong varchar(100),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_CungUngDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_CungUngDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[HopDong_ID] = @HopDong_ID,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHan] = @NgayHetHan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[GuidStr] = @GuidStr,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_CungUngDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[HopDong_ID],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHan],
			[TrangThaiXuLy],
			[MaHQ],
			[MaDoanhNghiep],
			[GuidStr],
			[DeXuatKhac],
			[LyDoSua]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@HopDong_ID,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHan,
			@TrangThaiXuLy,
			@MaHQ,
			@MaDoanhNghiep,
			@GuidStr,
			@DeXuatKhac,
			@LyDoSua
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUngDangKy_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_CungUngDangKy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUngDangKy_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_CungUngDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUngDangKy_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua]
FROM
	[dbo].[t_KDT_GC_CungUngDangKy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUngDangKy_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua]
FROM [dbo].[t_KDT_GC_CungUngDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUngDangKy_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUngDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua]
FROM
	[dbo].[t_KDT_GC_CungUngDangKy]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Insert]
	@Master_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(6),
	@LuongCungUng numeric(24, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_CungUng]
(
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongCungUng]
)
VALUES 
(
	@Master_ID,
	@MaNPL,
	@TenNPL,
	@MaHS,
	@DVT_ID,
	@LuongCungUng
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(6),
	@LuongCungUng numeric(24, 4)
AS

UPDATE
	[dbo].[t_KDT_GC_CungUng]
SET
	[Master_ID] = @Master_ID,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[LuongCungUng] = @LuongCungUng
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(6),
	@LuongCungUng numeric(24, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_CungUng] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_CungUng] 
		SET
			[Master_ID] = @Master_ID,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[LuongCungUng] = @LuongCungUng
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_CungUng]
		(
			[Master_ID],
			[MaNPL],
			[TenNPL],
			[MaHS],
			[DVT_ID],
			[LuongCungUng]
		)
		VALUES 
		(
			@Master_ID,
			@MaNPL,
			@TenNPL,
			@MaHS,
			@DVT_ID,
			@LuongCungUng
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_CungUng]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_CungUng] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongCungUng]
FROM
	[dbo].[t_KDT_GC_CungUng]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongCungUng]
FROM [dbo].[t_KDT_GC_CungUng] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongCungUng]
FROM
	[dbo].[t_KDT_GC_CungUng]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Detail_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_Insert]
	@Master_ID bigint,
	@SoToKhaiXuat varchar(50),
	@SoToKhaiVNACCS varchar(50),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaSanPham nvarchar(100),
	@TenSanPham nvarchar(255),
	@DVT_ID varchar(6),
	@SoLuong numeric(18, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_CungUng_Detail]
(
	[Master_ID],
	[SoToKhaiXuat],
	[SoToKhaiVNACCS],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaSanPham],
	[TenSanPham],
	[DVT_ID],
	[SoLuong]
)
VALUES 
(
	@Master_ID,
	@SoToKhaiXuat,
	@SoToKhaiVNACCS,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaHaiQuan,
	@MaSanPham,
	@TenSanPham,
	@DVT_ID,
	@SoLuong
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Detail_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoToKhaiXuat varchar(50),
	@SoToKhaiVNACCS varchar(50),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaSanPham nvarchar(100),
	@TenSanPham nvarchar(255),
	@DVT_ID varchar(6),
	@SoLuong numeric(18, 4)
AS

UPDATE
	[dbo].[t_KDT_GC_CungUng_Detail]
SET
	[Master_ID] = @Master_ID,
	[SoToKhaiXuat] = @SoToKhaiXuat,
	[SoToKhaiVNACCS] = @SoToKhaiVNACCS,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaHaiQuan] = @MaHaiQuan,
	[MaSanPham] = @MaSanPham,
	[TenSanPham] = @TenSanPham,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Detail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoToKhaiXuat varchar(50),
	@SoToKhaiVNACCS varchar(50),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaSanPham nvarchar(100),
	@TenSanPham nvarchar(255),
	@DVT_ID varchar(6),
	@SoLuong numeric(18, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_CungUng_Detail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_CungUng_Detail] 
		SET
			[Master_ID] = @Master_ID,
			[SoToKhaiXuat] = @SoToKhaiXuat,
			[SoToKhaiVNACCS] = @SoToKhaiVNACCS,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaHaiQuan] = @MaHaiQuan,
			[MaSanPham] = @MaSanPham,
			[TenSanPham] = @TenSanPham,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_CungUng_Detail]
		(
			[Master_ID],
			[SoToKhaiXuat],
			[SoToKhaiVNACCS],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaHaiQuan],
			[MaSanPham],
			[TenSanPham],
			[DVT_ID],
			[SoLuong]
		)
		VALUES 
		(
			@Master_ID,
			@SoToKhaiXuat,
			@SoToKhaiVNACCS,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaHaiQuan,
			@MaSanPham,
			@TenSanPham,
			@DVT_ID,
			@SoLuong
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Detail_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_CungUng_Detail]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Detail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_CungUng_Detail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Detail_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoToKhaiXuat],
	[SoToKhaiVNACCS],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaSanPham],
	[TenSanPham],
	[DVT_ID],
	[SoLuong]
FROM
	[dbo].[t_KDT_GC_CungUng_Detail]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Detail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoToKhaiXuat],
	[SoToKhaiVNACCS],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaSanPham],
	[TenSanPham],
	[DVT_ID],
	[SoLuong]
FROM [dbo].[t_KDT_GC_CungUng_Detail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Detail_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_Detail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoToKhaiXuat],
	[SoToKhaiVNACCS],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaSanPham],
	[TenSanPham],
	[DVT_ID],
	[SoLuong]
FROM
	[dbo].[t_KDT_GC_CungUng_Detail]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_ChungTu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_ChungTu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_ChungTu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_ChungTu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_ChungTu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_ChungTu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_ChungTu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_ChungTu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Insert]
	@Master_ID bigint,
	@SoChungTu varchar(50),
	@NgayChungTu datetime,
	@LoaiChungTu int,
	@NoiCap nvarchar(255),
	@SoLuong numeric(24, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_CungUng_ChungTu]
(
	[Master_ID],
	[SoChungTu],
	[NgayChungTu],
	[LoaiChungTu],
	[NoiCap],
	[SoLuong]
)
VALUES 
(
	@Master_ID,
	@SoChungTu,
	@NgayChungTu,
	@LoaiChungTu,
	@NoiCap,
	@SoLuong
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoChungTu varchar(50),
	@NgayChungTu datetime,
	@LoaiChungTu int,
	@NoiCap nvarchar(255),
	@SoLuong numeric(24, 4)
AS

UPDATE
	[dbo].[t_KDT_GC_CungUng_ChungTu]
SET
	[Master_ID] = @Master_ID,
	[SoChungTu] = @SoChungTu,
	[NgayChungTu] = @NgayChungTu,
	[LoaiChungTu] = @LoaiChungTu,
	[NoiCap] = @NoiCap,
	[SoLuong] = @SoLuong
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoChungTu varchar(50),
	@NgayChungTu datetime,
	@LoaiChungTu int,
	@NoiCap nvarchar(255),
	@SoLuong numeric(24, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_CungUng_ChungTu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_CungUng_ChungTu] 
		SET
			[Master_ID] = @Master_ID,
			[SoChungTu] = @SoChungTu,
			[NgayChungTu] = @NgayChungTu,
			[LoaiChungTu] = @LoaiChungTu,
			[NoiCap] = @NoiCap,
			[SoLuong] = @SoLuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_CungUng_ChungTu]
		(
			[Master_ID],
			[SoChungTu],
			[NgayChungTu],
			[LoaiChungTu],
			[NoiCap],
			[SoLuong]
		)
		VALUES 
		(
			@Master_ID,
			@SoChungTu,
			@NgayChungTu,
			@LoaiChungTu,
			@NoiCap,
			@SoLuong
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_CungUng_ChungTu]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_CungUng_ChungTu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoChungTu],
	[NgayChungTu],
	[LoaiChungTu],
	[NoiCap],
	[SoLuong]
FROM
	[dbo].[t_KDT_GC_CungUng_ChungTu]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoChungTu],
	[NgayChungTu],
	[LoaiChungTu],
	[NoiCap],
	[SoLuong]
FROM [dbo].[t_KDT_GC_CungUng_ChungTu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoChungTu],
	[NgayChungTu],
	[LoaiChungTu],
	[NoiCap],
	[SoLuong]
FROM
	[dbo].[t_KDT_GC_CungUng_ChungTu]	

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDich_SelectByHopDong]    Script Date: 08/22/2014 16:30:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectByHopDong]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectByHopDong]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDich_SelectByHopDong]    Script Date: 08/22/2014 16:30:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectByHopDong]       
 -- Add the parameters for the stored procedure here      
 @IDHopDong BIGINT,
 @MaNPL Varchar(100)     
AS      
BEGIN 
select SoToKhai,(case when MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = SoToKhai) else SoToKhai end ) as SoTokhaiVNACCS,
MaHaiQuan,NgayDangKy,MaLoaiHinh,hang.MaPhu as MaSanPham,hang.SoLuong
from t_KDT_ToKhaiMauDich 
Inner Join t_KDT_HangMauDich as hang on hang.TKMD_ID =  t_KDT_ToKhaiMauDich.ID  
and hang.MaPhu in (select MaSanPham from  t_GC_DinhMuc where HopDong_ID =@IDHopDong and MaNguyenPhuLieu =@MaNPL)
where IDHopDong = @IDHopDong and MaLoaiHinh like 'X%' order by SoToKhai
end
GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.1',GETDATE(), N' Cập nhật NPL cung ứng')
END	



