﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;
using System.Threading;
using Company.Interface.DanhMucChuan;
using Company.Interface.VNACCS;
#if KD_V4
using Company.Interface.SXXK;
using System.Diagnostics;
using System.IO;
#elif SXXK_V4
using Company.Interface.SXXK;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using System.IO;
using System.Diagnostics;
#elif GC_V4
using Company.Interface.GC;
using Company.GC.BLL.GC;
//using Company.Interface.VNACCS;
using System.IO;
using System.Diagnostics;
#endif

namespace Company.Interface
{
    public partial class VNACC_HangMauDichNhapForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public KDT_VNACC_HangMauDich HMD = null;
        private KDT_VNACC_HangMauDich_ThueThuKhac ThuThuKhac = null;
        DataTable dt = new DataTable();
        DataSet dsMienGiam = new DataSet();
        DataSet dsMaThuKhac = new DataSet();
        DataTable dtThuKhac = new DataTable();
        public string MaTienTe = "";
        private double soLuong;
        private double triGiaHD;
        private double donGiaHD;
        public String Caption;
        public bool IsChange;
        public string loaiHangHoa = "N";
#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#elif GC_V4
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#endif




        public VNACC_HangMauDichNhapForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.IDA.ToString());
            // SetIDControl();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void LoadData()
        {
            // A521:Mã miễn / Giảm / Không chịu thuế và thu khác
            // A522: Mã áp dụng thuế suất / Mức thuế và thu khác
            dsMaThuKhac = VNACC_Category_Common.SelectDynamic("ReferenceDB='A522' ", "");
            dsMienGiam = VNACC_Category_Common.SelectDynamic("ReferenceDB='A521' ", "");
            /*e.Row.*/
            grListThueThuKhac.DropDowns["drpMaApDungMucThue"].DataSource = dsMaThuKhac.Tables[0];
            grListThueThuKhac.DropDowns["drpMaMienGiam"].DataSource = dsMienGiam.Tables[0];


            grList.DataSource = TKMD.HangCollection;
            SetThueThuKhac();

            ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTDonGia.Code = MaTienTe;
            ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrMaTTDonGia.Code = MaTienTe;
            //khoitao_DuLieuChuan();

        }
        bool isAddNew = true;

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;

                if (HMD == null)
                {
                    HMD = new KDT_VNACC_HangMauDich();
                    //HMD.tkxuat = false;
                    isAddNew = true;
                }
                GetHangMD();
                //#if GC_V4 || SXXK_V4
                //                if(!checkMaHang(HMD.MaHangHoa,loaiHangHoa))
                //                {
                //                    MLMessages("Nguyên phụ liệu này chưa được khai báo den HQ. Xin kiểm tra lại thông tin ", "MSG_PUB06", "", false);
                //                    return;
                //                }
                //#endif
#if GC_V4
                //ctrMaMienGiamThue
                if (string.IsNullOrEmpty(HMD.MaMienGiamThue) || HMD.MaMienGiamThue.Trim() == "")
                {
                    this.ShowMessage("Dòng hàng chưa nhập Mã Miễn Giảm Thuế.", false);
                }
#endif
                if (isAddNew)
                    TKMD.HangCollection.Add(HMD);
                grList.DataSource = TKMD.HangCollection;
                grList.Refetch();
                HMD = new KDT_VNACC_HangMauDich();
                //SetHangMD();
                //HMD.tkxuat = false;
                HMD = null;
                txtSoLuong1.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong1.Text = "0";
                txtSoLuong1.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong2.Text = "0";
                txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoLuong1.Text = "0";
                this.SetChange(true);

            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi khi lưu hàng: " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }
        private void check()
        {
            txtThueSuat.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (ctrMaBieuThueNK.Code != "B30")
                if (txtThueSuat.Text == "0")
                {
                    txtThueSuat.TextChanged -= new EventHandler(txt_TextChanged);
                    txtThueSuat.Text = "";
                    txtThueSuat.TextChanged += new EventHandler(txt_TextChanged);

                    //txtThueSuat.Text = "";
                }
            txtThueSuatTuyetDoi.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtThueSuatTuyetDoi.Text == "0")
            {
                txtThueSuatTuyetDoi.TextChanged -= new EventHandler(txt_TextChanged);
                txtThueSuatTuyetDoi.Text = "";
                txtThueSuatTuyetDoi.TextChanged += new EventHandler(txt_TextChanged);

                //txtThueSuatTuyetDoi.Text = "";
            }
            txtSoLuong1.NullBehavior = txtSoLuong2.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtSoLuong1.Text == "0")
            {
                txtSoLuong1.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong1.Text = "";
                txtSoLuong1.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoLuong1.Text = "";
            }
            if (txtSoLuong2.Text == "0")
            {
                txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong2.Text = "";
                txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoLuong2.Text = "";
            }
            txtDonGiaTinhThue.NullBehavior = txtTriGiaTinhThueS.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtDonGiaTinhThue.Text == "0")
            {
                txtDonGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
                txtDonGiaTinhThue.Text = "";
                txtDonGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

                //txtDonGiaTinhThue.Text = "";
            }
            if (txtTriGiaTinhThueS.Text == "0")
            {
                txtTriGiaTinhThueS.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGiaTinhThueS.Text = "";
                txtTriGiaTinhThueS.TextChanged += new EventHandler(txt_TextChanged);

                //txtTriGiaTinhThueS.Text = "";
            }
            txtTriGiaHoaDon.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtTriGiaHoaDon.Text == "0")
            {
                txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGiaHoaDon.Text = "";
                txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                //txtTriGiaHoaDon.Text = "";
            }
            txtDonGiaHoaDon.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtDonGiaHoaDon.Text == "0")
            {
                txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                txtDonGiaHoaDon.Text = "";
                txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                //txtDonGiaHoaDon.Text = "";
            }
            //txtSoDongDMMienThue.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtSoDongDMMienThue.Text == "0")
            {
                txtSoDongDMMienThue.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDongDMMienThue.Text = "";
                txtSoDongDMMienThue.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoDongDMMienThue.Text = "";
            }
            //txtSoTTDongHangTKTNTX.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtSoTTDongHangTKTNTX.Text == "0")
            {
                txtSoTTDongHangTKTNTX.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoTTDongHangTKTNTX.Text = "";
                txtSoTTDongHangTKTNTX.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoTTDongHangTKTNTX.Text = "";
            }
            txtTriGiaTinhThue.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtTriGiaTinhThue.Text == "0")
            {
                txtTriGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGiaTinhThue.Text = "";
                txtTriGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

                //txtTriGiaTinhThue.Text = "";
            }
            if (txtSoDongDMMienThue.Text == "0")
            {
                txtSoDongDMMienThue.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDongDMMienThue.Text = "";
                txtSoDongDMMienThue.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoDongDMMienThue.Text = "";
            }
            if (txtSoDMMienThue.Text == "0")
            {
                txtSoDMMienThue.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDMMienThue.Text = "";
                txtSoDMMienThue.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoDMMienThue.Text = "";
            }
            txtSoLuongTinhThue.NullBehavior = txtSoTienThue.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtSoLuongTinhThue.Text == "0")
            {
                txtSoLuongTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuongTinhThue.Text = "";
                txtSoLuongTinhThue.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoLuongTinhThue.Text = "";
            }
            if (txtSoTienThue.Text == "0")
            {
                txtSoTienThue.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoTienThue.Text = "";
                txtSoTienThue.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoTienThue.Text = "";
            }

        }
        private void GetHangMD()
        {
            //HMD.tkxuat = false;
            HMD.MaSoHang = ctrMaSoHang.Code;
            HMD.MaHangHoa = txtMaHangHoa.Text;
            HMD.MaQuanLy = txtMaQuanLy.Text;
            HMD.TenHang = txtTenHang.Text;
            if (grList.SelectedItems.Count == 1)
            {
                HMD.SoDong = grList.RowCount >= 10 ? (grList.GetRow().RowIndex + 1).ToString() : "0" + (grList.GetRow().RowIndex + 1).ToString();
            }
            else
            {
                HMD.SoDong = grList.RowCount >= 10 ? (grList.RowCount + 1).ToString() : "0" + (grList.RowCount + 1).ToString();
            }
            if (string.IsNullOrEmpty(txtThueSuat.Text))
                HMD.ThueSuat = 0;
            else
                HMD.ThueSuat = Convert.ToDecimal(txtThueSuat.Text);
            //HMD.ThueSuatTuyetDoi = Convert.ToDecimal(txtThueSuatTuyetDoi.Value);
            HMD.ThueSuatTuyetDoi = string.IsNullOrEmpty(txtThueSuatTuyetDoi.Text) ? Convert.ToDecimal(txtThueSuatTuyetDoi.Value) : 0;
            HMD.MaDVTTuyetDoi = HMD.ThueSuatTuyetDoi > 0 ? ctrMaDVTTuyetDoi.Code : string.Empty;
            HMD.MaTTTuyetDoi = HMD.ThueSuatTuyetDoi > 0 ? ctrMaTTTuyetDoi.Code : string.Empty;
#if GC_V4
            if (string.IsNullOrEmpty(ctrMaMienGiam.Code))
                ShowMessage("Mã Miễn giảm thuế chưa được nhập",false);
                //MessageBox.Show("Mã Miễn giảm thuế chưa được nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
                HMD.MaMienGiamThue = ctrMaMienGiam.Code;
#else
            HMD.MaMienGiamThue = ctrMaMienGiam.Code;
#endif

            HMD.SoTienGiamThue = Convert.ToDecimal(txtSoTienMienGiam.Value);
            HMD.MaThueNKTheoLuong = ctrMaThueNKTheoLuong.Code;
            HMD.SoLuong1 = Convert.ToDecimal(txtSoLuong1.Value);
            HMD.DVTLuong1 = ctrDVTLuong1.Code;
            HMD.SoLuong2 = Convert.ToDecimal(txtSoLuong2.Value);
            HMD.DVTLuong2 = ctrDVTLuong2.Code;
            HMD.TriGiaHoaDon = Convert.ToDecimal(txtTriGiaHoaDon.Value);
            HMD.DonGiaHoaDon = Convert.ToDecimal(txtDonGiaHoaDon.Value);
            // duydp 09/11/2015 Fix khai báo hàng FOC không có đơn giá và trị giá
            if (HMD.DonGiaHoaDon == 0)
            {
                HMD.DVTDonGia = String.Empty;
                HMD.MaTTDonGia = String.Empty;
            }
            else
            {
                HMD.DVTDonGia = ctrDVTDonGia.Code;
                HMD.MaTTDonGia = ctrMaTTDonGia.Code;
            }
            // duydp 09/11/2015
            HMD.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Value);
            HMD.MaTTTriGiaTinhThue = ctrMaTTTriGiaTinhThue.Code;
            HMD.SoTTDongHangTKTNTX = txtSoTTDongHangTKTNTX.Text;
            HMD.NuocXuatXu = ctrNuocXuatXu.Code;
            HMD.MaBieuThueNK = ctrMaBieuThueNK.Code;
            HMD.MaHanNgach = txtMaHanNgach.Text;
            HMD.SoDMMienThue = txtSoDMMienThue.Text;
            HMD.SoDongDMMienThue = txtSoDongDMMienThue.Text;
            HMD.SoMucKhaiKhoanDC = txtSoMucKhaiKhoanDC1.Text + txtSoMucKhaiKhoanDC2.Text + txtSoMucKhaiKhoanDC3.Text + txtSoMucKhaiKhoanDC4.Text + txtSoMucKhaiKhoanDC5.Text;
            //So cua muc khaon dieu chinh

            // thue va thu khac
            GetThueThuKhac();





        }

        private void SetHangMD()
        {
            ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaSoHang.Code = HMD.MaSoHang;
            ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaHangHoa.Text = HMD.MaHangHoa;
            txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

            txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaQuanLy.Text = HMD.MaQuanLy;
            txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

            txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenHang.Text = HMD.TenHang;
            txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

            txtThueSuat.TextChanged -= new EventHandler(txt_TextChanged);
            txtThueSuat.Text = HMD.ThueSuat.ToString();
            txtThueSuat.TextChanged += new EventHandler(txt_TextChanged);

            txtThueSuatTuyetDoi.TextChanged -= new EventHandler(txt_TextChanged);
            txtThueSuatTuyetDoi.Text = HMD.ThueSuatTuyetDoi.ToString();
            txtThueSuatTuyetDoi.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaDVTTuyetDoi.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDVTTuyetDoi.Code = HMD.MaDVTTuyetDoi;
            ctrMaDVTTuyetDoi.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaTTTuyetDoi.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTTuyetDoi.Code = HMD.MaTTTuyetDoi;
            ctrMaTTTuyetDoi.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaMienGiam.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaMienGiam.Code = HMD.MaMienGiamThue;
            ctrMaMienGiam.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtSoTienMienGiam.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoTienMienGiam.Text = HMD.SoTienGiamThue.ToString();
            txtSoTienMienGiam.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaThueNKTheoLuong.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrMaThueNKTheoLuong.Code = HMD.MaThueNKTheoLuong;
            ctrMaThueNKTheoLuong.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            txtSoLuong1.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong1.Text = HMD.SoLuong1.ToString();
            txtSoLuong1.TextChanged += new EventHandler(txt_TextChanged);

            ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTLuong1.Code = HMD.DVTLuong1;
            ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong2.Text = HMD.SoLuong2.ToString();
            txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);

            ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTLuong2.Code = HMD.DVTLuong2;
            ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGiaHoaDon.Text = HMD.TriGiaHoaDon.ToString();
            txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

            txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonGiaHoaDon.Text = HMD.DonGiaHoaDon.ToString();
            txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

            ctrDVTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTDonGia.Code = HMD.DVTDonGia;
            ctrDVTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTDonGia.Code = HMD.MaTTDonGia;
            ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTriGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGiaTinhThue.Text = HMD.TriGiaTinhThue.ToString();
            txtTriGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaTTTriGiaTinhThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTTriGiaTinhThue.Code = HMD.MaTTTriGiaTinhThue;
            ctrMaTTTriGiaTinhThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtSoTTDongHangTKTNTX.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoTTDongHangTKTNTX.Text = HMD.SoTTDongHangTKTNTX;
            txtSoTTDongHangTKTNTX.TextChanged += new EventHandler(txt_TextChanged);

            ctrNuocXuatXu.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrNuocXuatXu.Code = HMD.NuocXuatXu;
            ctrNuocXuatXu.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrMaBieuThueNK.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrMaBieuThueNK.Code = HMD.MaBieuThueNK;
            ctrMaBieuThueNK.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            txtMaHanNgach.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaHanNgach.Text = HMD.MaHanNgach;
            txtMaHanNgach.TextChanged += new EventHandler(txt_TextChanged);

            txtSoDMMienThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoDMMienThue.Text = HMD.SoDMMienThue;
            txtSoDMMienThue.TextChanged += new EventHandler(txt_TextChanged);

            txtSoDongDMMienThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoDongDMMienThue.Text = HMD.SoDongDMMienThue;
            txtSoDongDMMienThue.TextChanged += new EventHandler(txt_TextChanged);

            if (HMD.SoMucKhaiKhoanDC == null)
                HMD.SoMucKhaiKhoanDC = "00000";

            txtSoMucKhaiKhoanDC1.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoMucKhaiKhoanDC1.Text = HMD.SoMucKhaiKhoanDC.Substring(0, 1);
            txtSoMucKhaiKhoanDC1.TextChanged += new EventHandler(txt_TextChanged);

            txtSoMucKhaiKhoanDC2.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoMucKhaiKhoanDC2.Text = HMD.SoMucKhaiKhoanDC.Substring(1, 1);
            txtSoMucKhaiKhoanDC2.TextChanged += new EventHandler(txt_TextChanged);

            txtSoMucKhaiKhoanDC3.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoMucKhaiKhoanDC3.Text = HMD.SoMucKhaiKhoanDC.Substring(2, 1);
            txtSoMucKhaiKhoanDC3.TextChanged += new EventHandler(txt_TextChanged);

            txtSoMucKhaiKhoanDC4.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoMucKhaiKhoanDC4.Text = HMD.SoMucKhaiKhoanDC.Substring(3, 1);
            txtSoMucKhaiKhoanDC4.TextChanged += new EventHandler(txt_TextChanged);

            txtSoMucKhaiKhoanDC5.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoMucKhaiKhoanDC5.Text = HMD.SoMucKhaiKhoanDC.Substring(4, 1);
            txtSoMucKhaiKhoanDC5.TextChanged += new EventHandler(txt_TextChanged);

            txtDonGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonGiaTinhThue.Text = HMD.DonGiaTinhThue.ToString();
            txtDonGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

            ctrDV_SL_TrongDonGiaTinhThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDV_SL_TrongDonGiaTinhThue.Code = HMD.DV_SL_TrongDonGiaTinhThue;
            ctrDV_SL_TrongDonGiaTinhThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTriGiaTinhThueS.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGiaTinhThueS.Text = HMD.TriGiaTinhThueS.ToString();
            txtTriGiaTinhThueS.TextChanged += new EventHandler(txt_TextChanged);

            txtSoLuongTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuongTinhThue.Text = HMD.SoLuongTinhThue.ToString();
            txtSoLuongTinhThue.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaDVTDanhThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDVTDanhThue.Code = HMD.MaDVTDanhThue;
            ctrMaDVTDanhThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrMaSoHang.Code = HMD.MaSoHang;
            //txtMaHangHoa.Text = HMD.MaHangHoa;
            //txtMaQuanLy.Text = HMD.MaQuanLy;
            //txtTenHang.Text = HMD.TenHang;
            //txtThueSuat.Text = HMD.ThueSuat.ToString();
            //txtThueSuatTuyetDoi.Text = HMD.ThueSuatTuyetDoi.ToString();
            //ctrMaDVTTuyetDoi.Code = HMD.MaDVTTuyetDoi;
            //ctrMaTTTuyetDoi.Code = HMD.MaTTTuyetDoi;
            //ctrMaMienGiam.Code = HMD.MaMienGiamThue;
            //txtSoTienMienGiam.Text = HMD.SoTienGiamThue.ToString();
            //ctrMaThueNKTheoLuong.Code = HMD.MaThueNKTheoLuong;
            //txtSoLuong1.Text = HMD.SoLuong1.ToString();
            //ctrDVTLuong1.Code = HMD.DVTLuong1;
            //txtSoLuong2.Text = HMD.SoLuong2.ToString();
            //ctrDVTLuong2.Code = HMD.DVTLuong2;
            //txtTriGiaHoaDon.Text = HMD.TriGiaHoaDon.ToString();
            //txtDonGiaHoaDon.Text = HMD.DonGiaHoaDon.ToString();
            //ctrDVTDonGia.Code = HMD.DVTDonGia;
            //ctrMaTTDonGia.Code = HMD.MaTTDonGia;
            //txtTriGiaTinhThue.Text = HMD.TriGiaTinhThue.ToString();
            //ctrMaTTTriGiaTinhThue.Code = HMD.MaTTTriGiaTinhThue;
            //txtSoTTDongHangTKTNTX.Text = HMD.SoTTDongHangTKTNTX;

            //ctrNuocXuatXu.Code = HMD.NuocXuatXu;
            //ctrMaBieuThueNK.Code = HMD.MaBieuThueNK;
            //txtMaHanNgach.Text = HMD.MaHanNgach;
            //txtSoDMMienThue.Text = HMD.SoDMMienThue;
            //txtSoDongDMMienThue.Text = HMD.SoDongDMMienThue;
            //if (HMD.SoMucKhaiKhoanDC == null)
            //    HMD.SoMucKhaiKhoanDC = "00000";
            //txtSoMucKhaiKhoanDC1.Text = HMD.SoMucKhaiKhoanDC.Substring(0, 1);
            //txtSoMucKhaiKhoanDC2.Text = HMD.SoMucKhaiKhoanDC.Substring(1, 1);
            //txtSoMucKhaiKhoanDC3.Text = HMD.SoMucKhaiKhoanDC.Substring(2, 1);
            //txtSoMucKhaiKhoanDC4.Text = HMD.SoMucKhaiKhoanDC.Substring(3, 1);
            //txtSoMucKhaiKhoanDC5.Text = HMD.SoMucKhaiKhoanDC.Substring(4, 1);


            //txtDonGiaTinhThue.Text = HMD.DonGiaTinhThue.ToString();
            //ctrDV_SL_TrongDonGiaTinhThue.Code = HMD.DV_SL_TrongDonGiaTinhThue;
            //txtTriGiaTinhThueS.Text = HMD.TriGiaTinhThueS.ToString();
            //txtSoLuongTinhThue.Text = HMD.SoLuongTinhThue.ToString();
            //ctrMaDVTDanhThue.Code = HMD.MaDVTDanhThue;
            //thue va thu khac
            SetThueThuKhac();
        }

        private void VNACC_HangMauDichNhapForm_Load(object sender, EventArgs e)
        {

            Caption = this.Text;
            grList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
#if GC_V4
            ctrMaBieuThueNK.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrMaBieuThueNK.Code = "B30";
            ctrMaBieuThueNK.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrMaMienGiam.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaMienGiam.Code = "XNG81";
            ctrMaMienGiam.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrMaBieuThueNK.Code = "B30";
            //ctrMaMienGiam.Code = "XNG81";
#endif
#if SXXK_V4
            ctrMaBieuThueNK.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrMaBieuThueNK.Code = "B01";
            ctrMaBieuThueNK.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrMaMienGiam.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaMienGiam.Code = "XN210";
            ctrMaMienGiam.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrMaBieuThueNK.Code = "B01";
            //ctrMaMienGiam.Code = "XN210";
#endif
            ctrMaMienGiam_Leave(null, null);
            LoadData();

            ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTDonGia.Code = MaTienTe;
            ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrMaTTDonGia.Code = MaTienTe;
#if KD_V4
            lblMaHang.Visible = false;
            txtMaHangHoa.Visible = false;
            grList.RootTable.Columns["MaHangHoa"].Visible = false;
            grList.RootTable.Columns["MaQuanLyRieng"].Visible = true;
#else
            grList.RootTable.Columns["MaHangHoa"].Visible = true;
            grList.RootTable.Columns["MaQuanLyRieng"].Visible = false;
#endif
            if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
            {
                btnGhi.Enabled = false;
                btnXoa.Enabled = false;
            }
            check();
            SetIDControl();

            SetMaxLengthControl();

            //ValidateForm(true);

            //SetAutoRemoveUnicodeAndUpperCaseControl();
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_HangMauDich> ItemColl = new List<KDT_VNACC_HangMauDich>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_HangMauDich)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangMauDich item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKMD.HangCollection.Remove(item);
                    }
                    this.SetChange(true);
                    grList.DataSource = TKMD.HangCollection;
                    try { grList.Refetch(); }
                    catch { grList.Refresh(); }

                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void SetThueThuKhac()
        {
            try
            {

                if (HMD == null) HMD = new KDT_VNACC_HangMauDich();
                if (HMD.ThueThuKhacCollection == null) HMD.ThueThuKhacCollection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                //dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(HMD.ThueThuKhacCollection);
                //grListThueThuKhac.DataSource = dt;
                setdtThuKhac(HMD.ThueThuKhacCollection);
                grListThueThuKhac.Refetch();
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        private void GetThueThuKhac()
        {
            //HMD.ThueThuKhacCollection.Clear();
            //foreach (DataRow row in dt.Rows)
            //{
            //    ThuThuKhac = new KDT_VNACC_HangMauDich_ThueThuKhac();
            //    if (row["ID"].ToString().Length != 0)
            //    {
            //        ThuThuKhac.ID = Convert.ToInt64(row["ID"].ToString());
            //    }
            //    ThuThuKhac.MaTSThueThuKhac = row["MaTSThueThuKhac"].ToString();
            //    ThuThuKhac.MaMGThueThuKhac = row["MaMGThueThuKhac"].ToString();
            //    ThuThuKhac.SoTienGiamThueThuKhac = row["SoTienGiamThueThuKhac"] == null ? 0 : Convert.ToDecimal(row["SoTienGiamThueThuKhac"]);
            //    HMD.ThueThuKhacCollection.Add(ThuThuKhac);
            //}
            if (HMD == null) HMD = new KDT_VNACC_HangMauDich();
            if (HMD.ThueThuKhacCollection == null) HMD.ThueThuKhacCollection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
            else
                KDT_VNACC_HangMauDich_ThueThuKhac.DeleteCollection(HMD.ThueThuKhacCollection);
            HMD.ThueThuKhacCollection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
            getThueThuKhac(HMD.ThueThuKhacCollection);
        }

        private void grList_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grList.GetRows().Length < 0) return;

                HMD = (KDT_VNACC_HangMauDich)grList.CurrentRow.DataRow;
                SetHangMD();
                isAddNew = false;
                check();
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void grListThueThuKhac_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                long ID = Convert.ToInt64(grListThueThuKhac.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa thuế và thu khác này không?", true) == "Yes")
                    {
                        KDT_VNACC_HangMauDich_ThueThuKhac.DeleteDynamic("ID=" + ID);
                        grListThueThuKhac.CurrentRow.Delete();
                        this.SetChange(true);
                    }
                }
                else
                {
                    grList.CurrentRow.Delete();
                    this.SetChange(true);
                }
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                ctrMaSoHang.TagName = "CMD"; //Mã số hàng hóa
                txtMaQuanLy.Tag = "GZC"; //Mã quản lý riêng
                txtThueSuat.Tag = "TXA"; //Thuế suất
                txtThueSuatTuyetDoi.Tag = "TXB"; //Mức thuế tuyệt đối
                ctrMaDVTTuyetDoi.TagName = "TXC"; //Mã đơn vị tính thuế tuyệt đối
                ctrMaTTTuyetDoi.TagName = "TXD"; //Mã đồng tiền của mức thuế tuyệt đối
                txtTenHang.Tag = "CMN"; //Mô tả hàng hóa
                ctrNuocXuatXu.TagCode = "OR"; //Mã nước xuất xứ
                ctrMaBieuThueNK.TagCode = "ORS"; //Mã biểu thuế nhập khẩu 
                txtMaHanNgach.Tag = "KWS"; //Mã ngoài hạn ngạch
                ctrMaThueNKTheoLuong.TagCode = "SPD"; //Mã xác định mức thuế nhập khẩu theo lượng
                txtSoLuong1.Tag = "QN1"; //Số lượng (1)
                ctrDVTLuong1.TagName = "QT1"; //Mã đơn vị tính (1)
                txtSoLuong2.Tag = "QN2"; //Số lượng (2)
                ctrDVTLuong2.TagName = "QT2"; //Mã đơn vị tính (2)
                txtTriGiaHoaDon.Tag = "BPR"; //Trị giá hóa đơn
                txtDonGiaHoaDon.Tag = "UPR"; //Đơn giá hóa đơn
                ctrMaTTDonGia.TagName = "UPC"; //Mã đồng tiền của đơn giá hóa đơn
                ctrDVTDonGia.TagName = "TSC"; //Đơn vị của đơn giá hóa đơn và số lượng
                ctrMaTTTriGiaTinhThue.TagName = "BPC"; //Mã đồng tiền trị giá tính thuế
                txtTriGiaTinhThue.Tag = "DPR"; //Trị giá tính thuế
                txtSoMucKhaiKhoanDC1.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoMucKhaiKhoanDC2.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoMucKhaiKhoanDC3.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoMucKhaiKhoanDC4.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoMucKhaiKhoanDC5.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoTTDongHangTKTNTX.Tag = "TDL"; //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
                txtSoDMMienThue.Tag = "TXN"; //Số đăng ký danh mục miễn thuế nhập khẩu
                txtSoDongDMMienThue.Tag = "TXR"; //Số dòng tương ứng trong danh mục miễn thuế nhập khẩu
                ctrMaMienGiam.TagName = "RE"; //Mã miễn / Giảm / Không chịu thuế nhập khẩu
                txtSoTienMienGiam.Tag = "REG"; //Số tiền giảm thuế nhập khẩu
                //grListThueThuKhac.Tag = "TX_";

                grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Tag = "TX_"; //Mã áp dụng thuế suất / Mức thuế và thu khác
                grListThueThuKhac.DropDowns["drpMaMienGiam"].Tag = "TR_"; //Mã miễn / Giảm / Không chịu thuế và thu khác
                //txtMaTSThueThuKhac1.Tag = "TX_"; //Mã áp dụng thuế suất / Mức thuế và thu khác
                //txtMaMGThueThuKhac1.Tag = "TR_"; //Mã miễn / Giảm / Không chịu thuế và thu khác
                //txtSoTienMienGiamThuKhac1.Tag = "TG_"; //Số tiền giảm thuế và thu khác


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                //ctrMaSoHang.MaxLength = 12; //Mã số hàng hóa
                txtMaQuanLy.MaxLength = 12; //Mã quản lý riêng
                //txtThueSuat. = 10; //Thuế suất
                txtThueSuatTuyetDoi.MaxLength = 10; //Mức thuế tuyệt đối
                //ctrMaDVTTuyetDoi.MaxLength = 4; //Mã đơn vị tính thuế tuyệt đối
                //ctrMaTTTuyetDoi.MaxLength = 3; //Mã đồng tiền của mức thuế tuyệt đối
                txtTenHang.MaxLength = 200; //Mô tả hàng hóa
                //ctrNuocXuatXu.MaxLength = 2; //Mã nước xuất xứ
                //ctrMaBieuThueNK.MaxLength = 3; //Mã biểu thuế nhập khẩu 
                txtMaHanNgach.MaxLength = 1; //Mã ngoài hạn ngạch
                //txtMaThueNKTheoLuong.MaxLength = 10; //Mã xác định mức thuế nhập khẩu theo lượng
                txtSoLuong1.MaxLength = 20; //Số lượng (1)
                //ctrDVTLuong1.MaxLength = 4; //Mã đơn vị tính (1)
                txtSoLuong2.MaxLength = 20; //Số lượng (2)
                //ctrDVTLuong2.MaxLength = 4; //Mã đơn vị tính (2)
                txtTriGiaHoaDon.MaxLength = 20; //Trị giá hóa đơn
                txtDonGiaHoaDon.MaxLength = 9; //Đơn giá hóa đơn
                //ctrMaTTDonGia.MaxLength = 3; //Mã đồng tiền của đơn giá hóa đơn
                //ctrDVTDonGia.MaxLength = 4; //Đơn vị của đơn giá hóa đơn và số lượng
                //ctrMaTTTriGiaTinhThue.MaxLength = 3; //Mã đồng tiền trị giá tính thuế
                txtTriGiaTinhThue.MaxLength = 20; //Trị giá tính thuế
                //txtSoMucKhaiKhoanDC1.MaxLength = 1; //Số của mục khai khoản điều chỉnh
                txtSoTTDongHangTKTNTX.MaxLength = 2; //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
                txtSoDMMienThue.MaxLength = 12; //Số đăng ký danh mục miễn thuế nhập khẩu
                txtSoDongDMMienThue.MaxLength = 3; //Số dòng tương ứng trong danh mục miễn thuế nhập khẩu
                //ctrMaMienGiam.MaxLength = 5; //Mã miễn / Giảm / Không chịu thuế nhập khẩu
                txtSoTienMienGiam.MaxLength = 16; //Số tiền giảm thuế nhập khẩu
                //txtMaTSThueThuKhac1.MaxLength = 10; //Mã áp dụng thuế suất / Mức thuế và thu khác
                //txtMaMGThueThuKhac1.MaxLength = 5; //Mã miễn / Giảm / Không chịu thuế và thu khác
                //txtSoTienGiamThueThuKhac1.MaxLength = 16; //Số tiền giảm thuế và thu khác
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ctrMaSoHang.SetValidate = !isOnlyWarning;
                ctrMaSoHang.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaSoHang.IsValidate; //"Mã số hàng hóa";
                //Đơn vị tính số lượng
                ctrDVTLuong1.SetValidate = !isOnlyWarning; ctrDVTLuong1.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrDVTLuong1.IsValidate;

                ctrDVTLuong2.SetValidate = !isOnlyWarning; ctrDVTLuong2.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrDVTLuong2.IsValidate;
                //Đồng tiền giá hóa đơn
                //ctrMaTTDonGia.SetValidate = !isOnlyWarning; ctrMaTTDonGia.IsOnlyWarning = isOnlyWarning;
                //isValid &= ctrMaTTDonGia.IsValidate;
                //Đơn vị tính đơn giá HD
                //ctrDVTDonGia.SetValidate = !isOnlyWarning; ctrDVTDonGia.IsOnlyWarning = isOnlyWarning;
                //isValid &= ctrDVTDonGia.IsValidate;
                //Mã nước xuất xứ
                ctrNuocXuatXu.SetValidate = !isOnlyWarning; ctrNuocXuatXu.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrNuocXuatXu.IsValidate;
                //Biểu thuế XNK
                ctrMaBieuThueNK.SetValidate = !isOnlyWarning; ctrMaBieuThueNK.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaBieuThueNK.IsValidate;
#if GC_V4 || SXXK_V4
                isValid &= ValidateControl.ValidateNull(txtMaHangHoa, errorProvider, "Mã hàng hoá", isOnlyWarning);
#endif
                //isValid &= ValidateControl.ValidateNull(txtMaHangHoa, errorProvider, "Mã hàng hoá", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHang, errorProvider, "Tên hàng");
                isValid &= ValidateControl.ValidateNull(txtSoLuong1, errorProvider, "Số lượng 1", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuong2, errorProvider, "Số lượng 2", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }



        private void uiTab1_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            if (uiTab1.SelectedIndex == 1)
            {
                SetThueThuKhac();
            }
        }

        private void txtTriGiaHoaDon_Leave(object sender, EventArgs e)
        {
            if (GlobalSettings.TinhThueTGNT == "1")
            {
                this.soLuong = Convert.ToDouble(txtSoLuong1.Value);
                if (soLuong != 0)
                {
                    this.triGiaHD = Convert.ToDouble(txtTriGiaHoaDon.Value);
#if KD_V4
                    txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
#elif SXXK_V4||GC_V4

                    txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.DonGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.DonGiaNT, Thread.CurrentThread.CurrentCulture);
#endif
                }
                ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTTDonGia.Code = this.MaTienTe;
                ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                //ctrMaTTDonGia.Code = this.MaTienTe;
            }
        }

        private void txtDonGiaHoaDon_Leave(object sender, EventArgs e)
        {
            if (GlobalSettings.TinhThueTGNT == "0")
            {
                this.soLuong = Convert.ToDouble(txtSoLuong1.Value);
                if (soLuong != 0)
                {
                    this.donGiaHD = Convert.ToDouble(txtDonGiaHoaDon.Value);
#if KD_V4
                    txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
#elif SXXK_V4||GC_V4
                    txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);
#endif
                }
                ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTTDonGia.Code = this.MaTienTe;
                ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                //ctrMaTTDonGia.Code = this.MaTienTe;
            }
        }

        private void txtSoLuong1_Leave(object sender, EventArgs e)
        {

            this.soLuong = Convert.ToDouble(txtSoLuong1.Value);
            if (soLuong > 0)
            {
                if (GlobalSettings.TinhThueTGNT == "1")
                {
                    this.triGiaHD = Convert.ToDouble(txtTriGiaHoaDon.Value);
#if KD_V4
                    txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
#elif SXXK_V4 || GC_V4

                    txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.DonGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.DonGiaNT, Thread.CurrentThread.CurrentCulture);

#endif

                }
                if (GlobalSettings.TinhThueTGNT == "0")
                {
                    this.donGiaHD = Convert.ToDouble(txtDonGiaHoaDon.Value);
#if KD_V4
                    txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
#elif SXXK_V4||GC_V4
                    txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);
#endif
                }
                ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTTDonGia.Code = this.MaTienTe;
                ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong2.Text = txtSoLuong1.Text;
                txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);
                //ctrMaTTDonGia.Code = this.MaTienTe;
            }

        }

        private void ctrDVTLuong1_Leave(object sender, EventArgs e)
        {
            ctrDVTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTDonGia.Code = ctrDVTLuong1.Code;
            ctrDVTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTLuong2.Code = ctrDVTLuong1.Code;
            ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrDVTDonGia.Code = ctrDVTLuong1.Code;
        }

        private void ctrDVTDonGia_Leave(object sender, EventArgs e)
        {
            ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTLuong1.Code = ctrDVTDonGia.Code;
            ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrDVTLuong1.Code = ctrDVTDonGia.Code;
        }

        private void grListThueThuKhac_AddingRecord(object sender, CancelEventArgs e)
        {

            //if (grListThueThuKhac.CurrentRow.Cells["SoTienGiamThueThuKhac"].Value.ToString() == "")
            //{
            //    grListThueThuKhac.CurrentRow.Cells["SoTienGiamThueThuKhac"].Value = 0;
            //}

        }

        private void txtMaQuanLy_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;//VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4
            if (this.loaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    //txtTenHang.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                    //ctrMaSoHang.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                }
                txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);
                //txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);

            }
            else if (this.loaiHangHoa == "N")
            {
                if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm.isBrowser = true;
                this.NPLRegistedForm.ShowDialog(this);
                if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    //txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                    //ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                }
                txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                //txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
            }
            else
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    //txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                    //ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                }
                txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                //txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
            }
#elif GC_V4
            switch (this.loaiHangHoa)

            {
                case "N":
                    //if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.isBrower = true;
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = TKMD.HopDong_ID;
                    this.NPLRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        //txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        //ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                case "S":
                    //if (this.SPRegistedForm == null)
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.SanPhamSelected.HopDong_ID = TKMD.HopDong_ID;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        //txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        //ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                    
                case "T":
                    ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                    ftb.ThietBiSelected.HopDong_ID = TKMD.HopDong_ID;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (!string.IsNullOrEmpty(ftb.ThietBiSelected.Ma))
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = ftb.ThietBiSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = ftb.ThietBiSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = ftb.ThietBiSelected.Ma;
                        //txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        //ctrMaSoHang.Code = ftb.ThietBiSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                case "H":
                    HangMauRegistedForm hangmauForm = new HangMauRegistedForm();
                    hangmauForm.isBrower = true;
                    hangmauForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    hangmauForm.HangMauSelected.HopDong_ID = TKMD.HopDong_ID;
                    hangmauForm.ShowDialog();
                    if (!string.IsNullOrEmpty(hangmauForm.HangMauSelected.Ma))
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = hangmauForm.HangMauSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = hangmauForm.HangMauSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = hangmauForm.HangMauSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = hangmauForm.HangMauSelected.Ma;
                        //txtTenHang.Text = hangmauForm.HangMauSelected.Ten;
                        //ctrMaSoHang.Code = hangmauForm.HangMauSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "4" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "4" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
            }
#endif


        }

        private DataTable NewdtThuKhac()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("STT", typeof(int));
            dt.Columns.Add("MaLoaiThue", typeof(string));
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("LoaiThue", typeof(string));
            dt.Columns.Add("MaTSThueThuKhac", typeof(string));
            dt.Columns.Add("MaMGThueThuKhac", typeof(string));
            dt.Columns.Add("SoTienGiamThueThuKhac", typeof(decimal));
            dt.Columns.Add("TenKhoanMucThueVaThuKhac", typeof(string));
            dt.Columns.Add("TriGiaTinhThueVaThuKhac", typeof(decimal));
            dt.Columns.Add("SoLuongTinhThueVaThuKhac", typeof(decimal));
            dt.Columns.Add("MaDVTDanhThueVaThuKhac", typeof(string));
            dt.Columns.Add("ThueSuatThueVaThuKhac", typeof(string));
            dt.Columns.Add("SoTienThueVaThuKhac", typeof(string));
            dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac", typeof(string));

            for (int i = 0; i < 5; i++)
            {
                DataRow dr = dt.NewRow();
                switch (i)
                {
                    case 3:
                        dr["MaLoaiThue"] = "VAT";
                        dr["LoaiThue"] = "THUẾ GIÁ TRỊ GIA TĂNG";
                        break;
                    case 1:
                        dr["MaLoaiThue"] = "TTDB";
                        dr["LoaiThue"] = "THUẾ TIÊU THỤ ĐẶC BIỆT";
                        break;
                    case 2:
                        dr["MaLoaiThue"] = "BVMT";
                        dr["LoaiThue"] = "THUẾ BẢO VỆ MÔI TRƯỜNG";
                        break;
                    case 0:
                        dr["MaLoaiThue"] = "TUVE";
                        dr["LoaiThue"] = "THUẾ TỰ VỆ/CHỐNG PHÁ GIÁ";
                        break;
                    case 4:
                        dr["MaLoaiThue"] = "KHAC";
                        dr["LoaiThue"] = "THUẾ KHÁC";
                        break;
                }
                dr["STT"] = i;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        private void setdtThuKhac(List<KDT_VNACC_HangMauDich_ThueThuKhac> listThuKhac)
        {
            dtThuKhac = NewdtThuKhac();
            foreach (KDT_VNACC_HangMauDich_ThueThuKhac thukhac in listThuKhac)
            {
                DataRow dr = dtThuKhac.Rows[4];
                if (thukhac.MaTSThueThuKhac.StartsWith("V"))
                {
                    dr = dtThuKhac.Rows[3];
                }
                else if (thukhac.MaTSThueThuKhac.StartsWith("T"))
                {
                    dr = dtThuKhac.Rows[1];
                }
                else if (thukhac.MaTSThueThuKhac.StartsWith("M"))
                {
                    dr = dtThuKhac.Rows[2];
                }
                else if (thukhac.MaTSThueThuKhac.StartsWith("BB") || thukhac.MaTSThueThuKhac.StartsWith("GB") || thukhac.MaTSThueThuKhac.StartsWith("CB") || thukhac.MaTSThueThuKhac.StartsWith("PB") || thukhac.MaTSThueThuKhac.StartsWith("DB") || thukhac.MaTSThueThuKhac.StartsWith("EB"))
                {
                    dr = dtThuKhac.Rows[0];
                }

                dr["ID"] = thukhac.ID;
                dr["MaTSThueThuKhac"] = thukhac.MaTSThueThuKhac;
                dr["MaMGThueThuKhac"] = thukhac.MaMGThueThuKhac;
                dr["SoTienGiamThueThuKhac"] = thukhac.SoTienGiamThueThuKhac;
                dr["TenKhoanMucThueVaThuKhac"] = thukhac.TenKhoanMucThueVaThuKhac;
                dr["TriGiaTinhThueVaThuKhac"] = thukhac.TriGiaTinhThueVaThuKhac;
                dr["SoLuongTinhThueVaThuKhac"] = thukhac.SoLuongTinhThueVaThuKhac;
                dr["MaDVTDanhThueVaThuKhac"] = thukhac.MaDVTDanhThueVaThuKhac;
                dr["ThueSuatThueVaThuKhac"] = thukhac.ThueSuatThueVaThuKhac;
                dr["SoTienThueVaThuKhac"] = thukhac.SoTienThueVaThuKhac;
                dr["DieuKhoanMienGiamThueVaThuKhac"] = thukhac.DieuKhoanMienGiamThueVaThuKhac;
            }
            grListThueThuKhac.DataSource = dtThuKhac;


        }
        private void GetRowThuKhac(KDT_VNACC_HangMauDich_ThueThuKhac thukhac, DataRow dr)
        {
            thukhac.MaTSThueThuKhac = dr["MaTSThueThuKhac"].ToString();
            thukhac.MaMGThueThuKhac = dr["MaMGThueThuKhac"].ToString();
            thukhac.SoTienGiamThueThuKhac = Convert.ToDecimal(dr["SoTienGiamThueThuKhac"] == null || string.IsNullOrEmpty(dr["SoTienGiamThueThuKhac"].ToString().Trim()) ? 0 : dr["SoTienGiamThueThuKhac"]);
            //             
            //             thukhac.TenKhoanMucThueVaThuKhac = dr["TenKhoanMucThueVaThuKhac"].ToString();
            //             thukhac.TriGiaTinhThueVaThuKhac = Convert.ToDecimal(dr["TriGiaTinhThueVaThuKhac"]);
            //             thukhac.SoLuongTinhThueVaThuKhac = Convert.ToDecimal(dr["SoLuongTinhThueVaThuKhac"]);
            //             thukhac.MaDVTDanhThueVaThuKhac = dr["MaDVTDanhThueVaThuKhac"].ToString();
            //             thukhac.ThueSuatThueVaThuKhac = dr["ThueSuatThueVaThuKhac"].ToString();
            //             thukhac.SoTienThueVaThuKhac = Convert.ToDecimal(dr["SoTienThueVaThuKhac"]);
            //             thukhac.DieuKhoanMienGiamThueVaThuKhac = dr["DieuKhoanMienGiamThueVaThuKhac"].ToString();

        }
        private void getThueThuKhac(List<KDT_VNACC_HangMauDich_ThueThuKhac> listThuKhac)
        {
            foreach (DataRow dr in dtThuKhac.Rows)
            {
                if (!string.IsNullOrEmpty(dr["MaTSThueThuKhac"].ToString().Trim()))
                {
                    int index = 0;
                    if (dr["MaTSThueThuKhac"].ToString().StartsWith("V"))
                        index = HMD.ThueThuKhacCollection.FindIndex(x => x.MaTSThueThuKhac.StartsWith("V"));
                    else if (dr["MaTSThueThuKhac"].ToString().StartsWith("T"))
                        index = HMD.ThueThuKhacCollection.FindIndex(x => x.MaTSThueThuKhac.StartsWith("T"));
                    else if (dr["MaTSThueThuKhac"].ToString().StartsWith("M"))
                        index = HMD.ThueThuKhacCollection.FindIndex(x => x.MaTSThueThuKhac.StartsWith("M"));
                    else if (dr["MaTSThueThuKhac"].ToString().StartsWith("B") || dr["MaTSThueThuKhac"].ToString().StartsWith("G") || dr["MaTSThueThuKhac"].ToString().Contains("CB") || dr["MaTSThueThuKhac"].ToString().Contains("PB") || dr["MaTSThueThuKhac"].ToString().Contains("DB") || dr["MaTSThueThuKhac"].ToString().Contains("EB"))
                        index = HMD.ThueThuKhacCollection.FindIndex(x => x.MaTSThueThuKhac.Contains("BB") || x.MaTSThueThuKhac.Contains("G") || x.MaTSThueThuKhac.Contains("CB") || x.MaTSThueThuKhac.Contains("PD") || x.MaTSThueThuKhac.Contains("DB") || x.MaTSThueThuKhac.Contains("EB"));
                    if (index > 0)
                    {
                        GetRowThuKhac(listThuKhac[index], dr);
                    }
                    else
                    {
                        listThuKhac.Add(new KDT_VNACC_HangMauDich_ThueThuKhac());
                        GetRowThuKhac(listThuKhac[listThuKhac.Count - 1], dr);
                    }
                }
            }
        }
        private void grListThueThuKhac_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //DataRowView dr = (DataRowView)e.Row.DataRow;
            //switch (dr["MaLoaiThue"].ToString())
            //{
            //    case "VAT":

            //        GridEXFilterCondition filter = new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.Contains,"VB");
            //        e.Row.Cells["MaTSThueThuKhac"].Column.DropDown = grListThueThuKhac.DropDowns["drpMaApDungMucThue"];
            //        e.Row.Cells["MaTSThueThuKhac"].Column.DropDown.ApplyFilter(filter);
            //        break;
            //    case "TTDB":
            //        break;
            //    case "BVMT":
            //        break;
            //    case "TUVE":
            //        break;
            //    case "KHAC":
            //        break;

            //}

        }

        private void grListThueThuKhac_DropDown(object sender, ColumnActionEventArgs e)
        {
            if (grListThueThuKhac.SelectedItems[0].RowType == RowType.Record)
            {
                DataRowView dr = (DataRowView)grListThueThuKhac.SelectedItems[0].GetRow().DataRow;
                if (e.Column.Key == "MaTSThueThuKhac" || e.Column.Key == "MaMGThueThuKhac")
                {
                    switch (dr["MaLoaiThue"].ToString())
                    {
                        case "VAT":
                            GridEXFilterCondition filter;
                            if (e.Column.Key == "MaTSThueThuKhac")
                                filter = new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.BeginsWith, "V");
                            else
                                filter = new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaMienGiam"].Columns["Code"], ConditionOperator.BeginsWith, "V");
                            // e.Column.DropDown = grListThueThuKhac.DropDowns["drpMaApDungMucThue"];
                            e.Column.DropDown.ApplyFilter(filter);
                            break;
                        case "TTDB":
                            GridEXFilterCondition filter2;
                            if (e.Column.Key == "MaTSThueThuKhac")
                                filter2 = new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.BeginsWith, "T");
                            else
                                filter2 = new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaMienGiam"].Columns["Code"], ConditionOperator.BeginsWith, "T");
                            // e.Column.DropDown = grListThueThuKhac.DropDowns["drpMaApDungMucThue"];
                            e.Column.DropDown.ApplyFilter(filter2);
                            break;
                        case "BVMT":
                            GridEXFilterCondition filter3;
                            e.Column.DropDown.Refresh();
                            if (e.Column.Key == "MaTSThueThuKhac")
                                filter3 = new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.BeginsWith, "M");
                            else
                                filter3 = new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaMienGiam"].Columns["Code"], ConditionOperator.BeginsWith, "M");
                            // e.Column.DropDown = grListThueThuKhac.DropDowns["drpMaApDungMucThue"];
                            e.Column.DropDown.ApplyFilter(filter3);
                            break;
                        case "TUVE":
                            if (e.Column.Key == "MaTSThueThuKhac")
                            {
                                GridEXFilterCondition filter4 = new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.BeginsWith, "B");
                                filter4.AddCondition(LogicalOperator.Or, new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.BeginsWith, "G"));
                                filter4.AddCondition(LogicalOperator.Or, new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.BeginsWith, "C"));
                                //filter4.AddCondition(LogicalOperator.Or, new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"],ConditionOperator.BeginsWith, "T"));
                                filter4.AddCondition(LogicalOperator.Or, new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.BeginsWith, "P"));
                                filter4.AddCondition(LogicalOperator.Or, new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.BeginsWith, "D"));
                                filter4.AddCondition(LogicalOperator.Or, new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.BeginsWith, "E"));

                                // e.Column.DropDown = grListThueThuKhac.DropDowns["drpMaApDungMucThue"];
                                e.Column.DropDown.ApplyFilter(filter4);
                            }
                            break;
                        case "KHAC":
                            if (e.Column.Key == "MaTSThueThuKhac")
                            {
                                GridEXFilterCondition filter5 = new GridEXFilterCondition(grListThueThuKhac.DropDowns["drpMaApDungMucThue"].Columns["Code"], ConditionOperator.NotIsEmpty, "");
                                // e.Column.DropDown = grListThueThuKhac.DropDowns["drpMaApDungMucThue"];
                                e.Column.DropDown.ApplyFilter(filter5);
                            }
                            break;

                    }
                }
            }
        }

        private void grListThueThuKhac_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void txtMaHangHoa_Leave(object sender, EventArgs e)
        {
#if GC_V4
            bool isNotFound = false;
            if (this.loaiHangHoa == "N")
            {

                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = TKMD.HopDong_ID;
                npl.Ma = txtMaHangHoa.Text.Trim();
                if (npl.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = npl.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = npl.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = npl.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    //txtMaHangHoa.Text = npl.Ma;
                    //ctrMaSoHang.Code = npl.MaHS;
                    //txtTenHang.Text = npl.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));

                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.loaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKMD.HopDong_ID;
                sp.Ma = txtMaHangHoa.Text.Trim();
                if (sp.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = sp.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = sp.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = sp.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    //txtMaHangHoa.Text = sp.Ma;
                    //ctrMaSoHang.Code = sp.MaHS;
                    //txtTenHang.Text = sp.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.loaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = TKMD.HopDong_ID;
                tb.Ma = txtMaHangHoa.Text.Trim();
                if (tb.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = tb.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = tb.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = tb.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    //txtMaHangHoa.Text = tb.Ma;
                    //ctrMaSoHang.Code = tb.MaHS;
                    //txtTenHang.Text = tb.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.loaiHangHoa == "H")
            {
                HangMau hangmau = HangMau.Load(TKMD.HopDong_ID, txtMaHangHoa.Text.Trim());
                if (hangmau != null)
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = hangmau.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = hangmau.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = hangmau.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(hangmau.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(hangmau.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    //txtMaHangHoa.Text = hangmau.Ma;
                    //ctrMaSoHang.Code = hangmau.MaHS;
                    //txtTenHang.Text = hangmau.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(hangmau.DVT_ID.PadRight(3));

                }
                else isNotFound = true;

            }
            if (isNotFound)
            {

                txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenHang.Text = string.Empty;
                txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaSoHang.Code = string.Empty;
                ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                //txtTenHang.Text = ctrMaSoHang.Code = string.Empty;
                return;
            }
#elif SXXK_V4

            if (loaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                IList<HangDuaVao> listTB = HangDuaVao.SelectCollectionDynamic(string.Format("Ma = '{0}' and LoaiNPL = '3'", txtMaHangHoa.Text.Trim()), null);
                if (listTB != null && listTB.Count > 0)
                {

                    HangDuaVao tb = listTB[0];

                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = tb.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = tb.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = tb.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtTenHang.Text = tb.Ten;
                    //ctrMaSoHang.Code = tb.MaHS;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                }
                else
                {
                }
            }
            else if (loaiHangHoa == "N")
            {
                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.Ma = txtMaHangHoa.Text;
                if (npl.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = npl.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = npl.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = npl.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = npl.Ma;
                    //ctrMaSoHang.Code = npl.MaHS;
                    //txtTenHang.Text = npl.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                }


            }
            else if (loaiHangHoa == "S")
            {
                Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = txtMaHangHoa.Text;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (sp.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = sp.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = sp.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = sp.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = sp.Ma;
                    //ctrMaSoHang.Code = sp.MaHS;
                    //txtTenHang.Text = sp.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));

                }
            }
#endif
        }
        public long IdTKTamNhapTX;
        private void txtSoTTDongHangTKTNTX_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (IdTKTamNhapTX > 0)
                {
                    VNACC_ListHangHMD listForm = new VNACC_ListHangHMD(Company.Interface.VNACCS.VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { IdTKTamNhapTX });
                    listForm.ShowDialog();
                    if (listForm.DialogResult == DialogResult.OK)
                    {
                        if (listForm.HangMD != null)
                        {
                            KDT_VNACC_HangMauDich hmdTNTX = listForm.HangMD;

                            ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrMaSoHang.Code = hmdTNTX.MaSoHang;
                            ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                            txtMaHangHoa.Text = hmdTNTX.MaHangHoa;
                            txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                            txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                            txtMaQuanLy.Text = hmdTNTX.MaQuanLy;
                            txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                            txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                            txtTenHang.Text = hmdTNTX.TenHang;
                            txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                            txtSoLuong1.TextChanged -= new EventHandler(txt_TextChanged);
                            txtSoLuong1.Text = hmdTNTX.SoLuong1.ToString();
                            txtSoLuong1.TextChanged += new EventHandler(txt_TextChanged);

                            ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrDVTLuong1.Code = hmdTNTX.DVTLuong1;
                            ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
                            txtSoLuong2.Text = hmdTNTX.SoLuong2.ToString();
                            txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);

                            ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrDVTLuong2.Code = hmdTNTX.DVTLuong2;
                            ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                            txtTriGiaHoaDon.Text = hmdTNTX.TriGiaHoaDon.ToString();
                            txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                            txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                            txtDonGiaHoaDon.Text = hmdTNTX.DonGiaHoaDon.ToString();
                            txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                            ctrDVTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrDVTDonGia.Code = hmdTNTX.DVTDonGia;
                            ctrDVTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrMaTTDonGia.Code = hmdTNTX.MaTTDonGia;
                            ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            txtSoTTDongHangTKTNTX.TextChanged -= new EventHandler(txt_TextChanged);
                            txtSoTTDongHangTKTNTX.Text = hmdTNTX.SoDong.Length == 1 ? "0" + hmdTNTX.SoDong : hmdTNTX.SoDong;
                            txtSoTTDongHangTKTNTX.TextChanged += new EventHandler(txt_TextChanged);

                            ctrNuocXuatXu.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
                            ctrNuocXuatXu.Code = hmdTNTX.NuocXuatXu;
                            ctrNuocXuatXu.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

                            //ctrMaSoHang.Code = hmdTNTX.MaSoHang;
                            //txtMaQuanLy.Text = hmdTNTX.MaQuanLy;
                            //txtMaHangHoa.Text = hmdTNTX.MaHangHoa;
                            //txtTenHang.Text = hmdTNTX.TenHang;
                            //txtSoLuong1.Text = hmdTNTX.SoLuong1.ToString();
                            //ctrDVTLuong1.Code = hmdTNTX.DVTLuong1;
                            //txtSoLuong2.Text = hmdTNTX.SoLuong2.ToString();
                            //ctrDVTLuong2.Code = hmdTNTX.DVTLuong2;
                            //txtTriGiaHoaDon.Text = hmdTNTX.TriGiaHoaDon.ToString();
                            //txtDonGiaHoaDon.Text = hmdTNTX.DonGiaHoaDon.ToString();
                            //ctrDVTDonGia.Code = hmdTNTX.DVTDonGia;
                            //ctrMaTTDonGia.Code = hmdTNTX.MaTTDonGia;
                            //txtSoTTDongHangTKTNTX.Text = hmdTNTX.SoDong.Length == 1 ? "0" + hmdTNTX.SoDong : hmdTNTX.SoDong;
                            //ctrNuocXuatXu.Code = hmdTNTX.NuocXuatXu;
                        }
                    }

                }
                else
                {
                    this.ShowMessage("Bạn chưa chọn tờ khai tạm xuất hoặc tờ khai tạm xuất không tồn tại", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }


        private void btnDVT1_Click(object sender, EventArgs e)
        {
            MapperForm f = new MapperForm();
            f.TKMD = this.TKMD;
            f.LoaiMapper = "DVT";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                ctrDVTLuong1.Code = f.mapper.CodeV5.Trim();
            }
        }

        private void btnDVT2_Click(object sender, EventArgs e)
        {
            MapperForm f = new MapperForm();
            f.TKMD = this.TKMD;
            f.LoaiMapper = "DVT";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                ctrDVTLuong2.Code = f.mapper.CodeV5.Trim();
            }
        }

        private void CopyBieuThue_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.MaBieuThueNK = ctrMaBieuThueNK.Code;
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }
        private void CopyDanhMuc_LinkkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.SoDMMienThue = txtSoDMMienThue.Text;
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }
        private void CopyMienGiam_LinkkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.MaMienGiamThue = ctrMaMienGiam.Code;
                item.DieuKhoanMienGiam = txtDieuKhoanMienGiam.Text;
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }

        private void uiTab1_SelectedTabChanged_1(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            if (e.Page == TabThueNhapKhau)
                CopyThueVaThuKhac.Visible = false;
            else if (e.Page == TabThueVaThuKhac)
                CopyThueVaThuKhac.Visible = true;
        }
        private void CopyThuKhac_LinkkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
                {
                    item.ThueThuKhacCollection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                    getThueThuKhac(item.ThueThuKhacCollection);
                }
                this.ShowMessage("Copy thành công", false);
            }
            catch (System.Exception ex)
            {
                this.ShowMessage("Copy thất bại. Vui lòng ghi lại thông tin trước khi copy", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.MaQuanLy = txtMaQuanLy.Text.Trim();
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }

        private void linkCopyDongTienThanhToan_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.MaTTDonGia = ctrMaTTDonGia.Code;
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.RestoreDirectory = true;
            sfNPL.InitialDirectory = Application.StartupPath;
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.FileName = "DanhSachHangHoaTKHQNK_" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + ".xls";
            sfNPL.ShowDialog();
            if (sfNPL.FileName != "")
            {
                Janus.Windows.GridEX.Export.GridEXExporter gridEXExporterNPL = new Janus.Windows.GridEX.Export.GridEXExporter();
                gridEXExporterNPL.GridEX = grList;
                Stream str = sfNPL.OpenFile();
                gridEXExporterNPL.Export(str);
                str.Close();
                //if (showMsg("MSG_MAL08", true) == "Yes")
                if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
        }

        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            KDT_VNACC_HangMauDich hmd = (KDT_VNACC_HangMauDich)e.Row.DataRow;
            if (hmd != null)
            {
                if (hmd == null) hmd = new KDT_VNACC_HangMauDich();
                if (hmd.ThueThuKhacCollection == null) hmd.ThueThuKhacCollection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                decimal TongTienThue = hmd.SoTienThue;
                foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in hmd.ThueThuKhacCollection)
                {
                    TongTienThue += item.SoTienThueVaThuKhac;
                    if (item.MaTSThueThuKhac.StartsWith("V"))
                        e.Row.Cells["ThueVAT"].Text = item.SoTienThueVaThuKhac.ToString("###,###,###,###.####");
                    if (item.MaTSThueThuKhac.StartsWith("T"))
                        e.Row.Cells["ThueTTDB"].Text = item.SoTienThueVaThuKhac.ToString("###,###,###,###.####");
                    if (item.MaTSThueThuKhac.StartsWith("M"))
                        e.Row.Cells["ThueBVMT"].Text = item.SoTienThueVaThuKhac.ToString("###,###,###,###.####");
                }
                e.Row.Cells["TongTienThue"].Text = TongTienThue.ToString("###,###,###,###.####");
            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Company.Interface.CauHinhToKhaiForm f = new Company.Interface.CauHinhToKhaiForm();
            f.Listlable.Add(f.lblPhuongThucTT);
            f.ShowDialog();
            this.txtDonGiaHoaDon.Focus();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.DVTLuong2 = string.Empty;
                item.SoLuong2 = 0;
            }
            this.SetChange(true);
            this.ShowMessage("Xoá thành công", false);
        }

        private void ckboxNhapThue_CheckedChanged(object sender, EventArgs e)
        {
            if (ckboxNhapThue.Checked == true)
            {
                txtDonGiaTinhThue.ReadOnly = false;
                txtTriGiaTinhThueS.ReadOnly = false;
                txtSoLuongTinhThue.ReadOnly = false;
                txtSoTienThue.ReadOnly = false;

            }
            else
            {
                txtDonGiaTinhThue.ReadOnly = true;
                txtTriGiaTinhThueS.ReadOnly = true;
                txtSoLuongTinhThue.ReadOnly = true;
                txtSoTienThue.ReadOnly = true;
            }
        }

        private void txtMaHangHoa_TextChanged(object sender, EventArgs e)
        {

        }

        private void ctrMaMienGiam_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ctrMaMienGiam.Code))
            {
                try
                {
                    List<VNACC_Category_Common> result = VNACC_Category_Common.SelectCollectionDynamic("ReferenceDB='A520' AND Code='" + ctrMaMienGiam.Code.Trim() + "'", "ID");
                    if (result.Count >= 1)
                    {
                        txtDieuKhoanMienGiam.Text = result[0].Name_VN;
                    }
                    else
                    {
                        txtDieuKhoanMienGiam.Text = String.Empty;
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            else
            {
                txtDieuKhoanMienGiam.Text = String.Empty;
            }
        }

        private void linkSuaSoLuong_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            VNACC_DieuChinhHangMauDichForm f = new VNACC_DieuChinhHangMauDichForm();
            f.TKMD = TKMD;
            f.loaiHangHoa = loaiHangHoa;
            f.ShowDialog(this);
            grList.DataSource = TKMD.HangCollection;
            grList.Refetch();
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.SoLuong2 = item.SoLuong1;
                item.DVTLuong2 = item.DVTLuong1;
            }
            this.SetChange(true);
            this.ShowMessage("Coppy thành công", false);
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ctrMaSoHang.Code.ToString()))
                {
                    System.Diagnostics.Process.Start("https://caselaw.vn/chi-tiet-ma-hs/" + ctrMaSoHang.Code.ToString() + "");
                }
                else
                {
                    ShowMessageTQDT("Doanh nghiệp chưa chọn Mã HS .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }
    }
}
