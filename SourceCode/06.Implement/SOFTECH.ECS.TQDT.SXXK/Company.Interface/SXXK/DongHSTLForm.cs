﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.SXXK
{
    public partial class DongHSTLForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public DongHSTLForm()
        {
            InitializeComponent();
        }

        private void DongHSTLForm_Load(object sender, EventArgs e)
        {
            txtSoHoSo.Value = this.HSTL.SoHoSo;
            txtSoQD.Text = this.HSTL.SoQuyetDinh;
            if (this.HSTL.NgayKetThuc.Year > 1900)
                ccNgayKetThuc.Value = this.HSTL.NgayKetThuc;
            if (this.HSTL.NgayQuyetDinh.Year > 1900)
                ccNgayQD.Value = this.HSTL.NgayQuyetDinh;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.HSTL.SoHoSo = Convert.ToInt32(txtSoHoSo.Value);
            this.HSTL.NgayKetThuc = ccNgayKetThuc.Value;
            this.HSTL.SoQuyetDinh = txtSoQD.Text;
            this.HSTL.NgayQuyetDinh = ccNgayQD.Value;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.HSTL.DongHoSo();
                this.Cursor = Cursors.Default;
                ShowMessage("Hồ sơ đã đóng thành công.", false);
                this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage("Đóng hồ sơ không thành công.\n Lỗi: " + ex, false);

            }
        }

    }
}