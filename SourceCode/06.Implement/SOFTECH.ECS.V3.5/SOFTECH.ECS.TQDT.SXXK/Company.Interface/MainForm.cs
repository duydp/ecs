﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.Interface.KDT.SXXK;
using Company.Interface.SXXK;
using Janus.Windows.ExplorerBar;
using Company.BLL.KDT;
using Company.Interface.SXXK.BangKe;
using Company.Interface.Report.SXXK;
using DevExpress.LookAndFeel;
using Company.BLL.KDT.SXXK;
using Company.Interface.Report;
using Company.Interface.DanhMucChuan;
using System.IO;
using System.Xml.Serialization;
using Company.QuanTri;
using Company.Interface.QuanTri;
using System.Threading;
using System.Globalization;
using Company.Interface.PhongKhai;
using System.Xml;
using System.Security.Cryptography;
using System.Text;
using Company.KDT.SHARE.Components;
using Company.Interface.DongBoDuLieu;
using Company.KDT.SHARE.VNACCS;
using Company.Interface.KDT.DNCX;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using Company.Interface.VNACCS.PhieuKho;
using Company.Interface.VNACCS.Vouchers;
using IWshRuntimeLibrary;
using Company.Interface.VNACCS.WareHouse;
using Company.Interface.VNACCS.ExportTotalData;
using Company.Interface.VNACCS;
using System.Drawing;
using Company.Interface.ProdInfo;
using Company.Interface.GC.WareHouse;
using Company.Interface.CauHinh;
using Company.Interface.VNACCS.TollManagement;
using Company.Interface.KDT.GC;

namespace Company.Interface
{
    public partial class MainForm : BaseForm
    {
        //TODO: VNACCS---------------------------------------------------------------------------------------------
        public static bool isUseSettingVNACC = true;
        public ThongBaoFormVNACCS frmThongBao = new ThongBaoFormVNACCS();
        public TrangThaiPhanHoiForm frmTrangThaiPhanHoi = new TrangThaiPhanHoiForm();

        public static int flag = 0;
        public static bool isLoginSuccess = false;

        HtmlDocument docHTML = null;
        WebBrowser wbManin = new WebBrowser();
        public static int versionHD = 0;
        public static int typeLogin = 0;
        public int soLanLayTyGia = 0;
        private FrmQuerySQLUpdate frmQuery;

        #region SXXK
        private static QueueForm queueForm = new QueueForm();
        private static NhanNhoForm NhacNhoForm = new NhanNhoForm();
        // Danh muc hang hoa.
        //private DMHHSendForm dmhhSendForm;
        //private DMHHManageForm dmhhManageForm;
        // Nguyên phụ liệu.
        private NguyenPhuLieuSendForm nplSendForm;
        private Company.Interface.KDT.SXXK.DNCX.NguyenPhuLieuSendForm_CX nplSendForm_CX;
        //private NguyenPhuLieu_Receive_Form nplReceiveForm;
        private NguyenPhuLieuManageForm nplManageForm;
        private NguyenPhuLieuManageForm_CX nplManageForm_CX;
        private NguyenPhuLieuRegistedForm nplRegistedForm;
        // Sản phẩm.
        //private SanPhamForm spForm;
        private SanPhamSendForm spSendForm;
        private SanPhamSendForm_CX spSendForm_CX;
        //private SanPham_Receive_Form spReceiveForm;
        private SanPhamManageForm spManageForm;
        private SanPhamManageForm_CX spManageForm_CX;
        private SanPhamRegistedForm spRegistedForm;
        private TheoDoiPhanBoTKNhap theodoiPhanBoTKN;

        // Định mức.
        //private DinhMuc_Form dmForm;
        private DinhMucSendForm_OLD dmSendForm;
        private DinhMucSendForm_NEW_HOATHO dmSendForm_NEW_HOATHO;
        private DinhMucManageForm_OLD dmManageForm;
        private DinhMucRegistedForm dmRegistedForm;
        // Định mức for HOATHO.
        private DinhMuc_New dmSendForm_new;
        private DinhMucManageForm_New dmManageForm_new;
        private DinhMucRegistedForm_NEW dmRegistedForm_new;
        // Tờ khai mậu dịch.
        private ToKhaiMauDichForm tkmdForm;
        //private ToKhaiMauDichSendForm tkmdSendForm;
        private ToKhaiMauDichManageForm tkmdManageForm;
        private ToKhaiSapHetHanForm tkHetHanForm;
        private ToKhaiQuaHanTKForm tkQuaHanForm;
        private HangTKXuatForm hangTKXForm;
        //Hồ sơ thanh lý
        private TaoHoSoThanhLyForm taoHSTLForm;
        private CapNhatHoSoThanhLyForm capNhatHSTLForm;
        private HSTLDaDongForm hstlDaDongForm;
        private HSTLManageForm hstlManageForm;
        private QuyetDinhToKhaiNhapForm quyetDinhTKNForm;

        private ToKhaiMauDichRegisterForm tkmdRegisterForm;
        private ToKhaiThuocLoaiHinhKhacForm tkLoaiHinhKhacForm;

        private PhanBoTKNForm pbTKNForm;
        private PhanBoTKXForm pbTKXForm;
        private ChiPhiXNKForm chiPhiXNKForm;
        private ThueTonToKhaiNhapForm thueTonTKNForm;
        private TriGiaHangToKhaiXuatForm triGiaHangTKXForm;
        private TriGiaHangToKhaiNhapForm triGiaHangTKNForm;
        private KiemTraDuLieuTKForm ktDuLieuTKForm;
        private KiemTraDuLieuDMForm ktDuLieuDMForm;

        private ThanhKhoanSendForm thanhkhoanSendForm;
        private Company.Interface.VNACCS.QuanLyMessageForm quanLyMsg = new Company.Interface.VNACCS.QuanLyMessageForm();
        private Company.BLL.SXXK.NguyenPhuLieuCollection tmpNPLCollection = new Company.BLL.SXXK.NguyenPhuLieuCollection();
        private Company.BLL.SXXK.SanPhamCollection tmpSPCollection = new Company.BLL.SXXK.SanPhamCollection();
        private Company.BLL.SXXK.DinhMucCollection tmpDMCollection = new Company.BLL.SXXK.DinhMucCollection();

        private BaseForm baseForm;

        // Thanh ly Tai San Co Dinh

        private TaiSanCoDinhEditFrm TaiSanCoDinhEditForm;
        private TaiSanCoDinhManagerFrm TaiSanCoDinhManagerForm;

        //hang ton
        private HangTon_ManagerForm HangTonManager;

        #endregion

        //-----------------------------------------------------------------------------------------------

        #region Du lieu Chuan
        private MaHSForm maHSForm;
        private PTTTForm ptttForm;
        private PTVTForm ptvtForm;
        private DonViTinhForm dvtForm;
        private DonViHaiQuanForm dvhqForm;
        private CuaKhauForm ckForm;
        private NguyenTeForm ntForm;
        private DKGHForm dkghForm;
        private NuocForm nuocForm;
        private LoaiPhiChungTuThanhToanForm loaiPhiCTTTForm;
        private NhomCuaKhauForm nhomCuaKhauForm;

        private SyncDataForm dongBoForm;
        private FrmDongBoDuLieu DBDLForm;
        private FrmDongBoDuLieu_VNACCS DBDLForm_VNACCS;
        //private DongBoDuLieu_New.QuanTri.QuanLyNguoiDung QuanLyDaiLy;
        #endregion

        #region Quản trị
        public static ECSPrincipal EcsQuanTri;
        QuanLyNguoiDung NguoiDung;
        QuanLyNhomNguoiDung NhomNguoiDung;
        #endregion Quản trị

        #region Hien thi ngay thang nam, gio

        private System.Windows.Forms.Timer timerDateTime;

        private void SetTimerDateTimeStatus()
        {
            timerDateTime = new System.Windows.Forms.Timer();
            timerDateTime.Interval = 1000;
            timerDateTime.Tick += new EventHandler(timerDateTime_Tick);

            timerDateTime.Enabled = true;
            timerDateTime.Start();
            statusBar.Panels["DateTime"].AutoSize = StatusBarPanelAutoSize.Contents;

            statusBar.ImageList = ilSmall;
            statusBar.Panels["DateTime"].ImageIndex = 65;

            timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = 1000;
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Enabled = true;
            timer1.Start();
            statusBar.Panels["Support"].AutoSize = StatusBarPanelAutoSize.Spring;

        }

        void timerDateTime_Tick(object sender, EventArgs e)
        {
            statusBar.Panels["DateTime"].ToolTipText = statusBar.Panels["DateTime"].Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt");
        }

        #endregion

        QuanLyMessage quanlyMess;
        private void khoitao_GiaTriMacDinh()
        {
            try
            {
                GlobalSettings.KhoiTao_GiaTriMacDinh();

                statusBar.Panels["DoanhNghiep"].AutoSize = StatusBarPanelAutoSize.Spring;
                statusBar.Panels["HaiQuan"].AutoSize = StatusBarPanelAutoSize.Contents;
                statusBar.Panels["HaiQuan"].Width = 250;
                statusBar.Panels["Service"].AutoSize = StatusBarPanelAutoSize.None;
                statusBar.Panels["Service"].Width = 250;
                statusBar.Panels["Terminal"].AutoSize = StatusBarPanelAutoSize.Contents;

                if (versionHD == 0)
                {
                    string statusStr1 = setText(string.Format("Mã/ tên người khai HQ: {0}/ {1}", ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.HO_TEN) + " - " + string.Format("Mã/ tên Doanh nghiệp: {0}/ {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI), "User: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Company : {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI));
                    //string statusStr2 = setText(string.Format("Hải quan tiếp nhận: {0}/ {1}.", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN), string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN));
                    statusBar.Panels["HaiQuan"].Text = setText(string.Format("Hải quan tiếp nhận: {0}", GlobalSettings.MA_HAI_QUAN), string.Format("Customs : {0}", GlobalSettings.MA_HAI_QUAN));
                    string statusStr3 = string.Format("Service khai báo: {0}", GlobalSettings.DiaChiWS);
                    statusBar.Panels["DoanhNghiep"].ToolTipText = statusBar.Panels["DoanhNghiep"].Text = statusStr1;
                    statusBar.Panels["HaiQuan"].ToolTipText = setText(string.Format("Hải quan tiếp nhận: {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN), string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN));
                    statusBar.Panels["Service"].ToolTipText = statusBar.Panels["Service"].Text = statusStr3;
                    statusBar.Panels["Terminal"].ToolTipText = statusBar.Panels["Terminal"].Text = string.Format("Terminal ID: {0}", GlobalVNACC.TerminalID != null && GlobalVNACC.TerminalID != "" ? GlobalVNACC.TerminalID : "?");
                }
                else
                {
                    string diaChiKhaibao = "http://" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(GlobalSettings.MA_DON_VI, "DIA_CHI_HQ").Value_Config.ToString() +
                                        "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(GlobalSettings.MA_DON_VI, "TEN_DICH_VU").Value_Config.ToString();
                    //this.Text = "Hệ thống hỗ trợ khai báo từ xa loại hình Kinh Doanh - Đầu Tư version " + Application.ProductVersion;
                    statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                    //statusBar.Panels["HaiQuan"].Text = statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Hải quan tiếp nhận: {0}/ {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                    statusBar.Panels["HaiQuan"].Text = string.Format("Hải quan tiếp nhận: {0}", GlobalSettings.MA_HAI_QUAN);
                    statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Hải quan tiếp nhận: {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                    statusBar.Panels["Service"].Text = statusBar.Panels["Service"].ToolTipText = string.Format("Service khai báo: {0}", GlobalSettings.DiaChiWS);
                    statusBar.Panels["Terminal"].ToolTipText = statusBar.Panels["Terminal"].Text = string.Format("Terminal ID: {0}", GlobalVNACC.TerminalID != null && GlobalVNACC.TerminalID != "" ? GlobalVNACC.TerminalID : "?");
                }

                statusBar.Panels["Terminal"].FormatStyle.ForeColor = GlobalVNACC.TerminalID != null && GlobalVNACC.TerminalID != "" ? System.Drawing.Color.Blue : System.Drawing.Color.Red;


                //TODO: Hungtq updaetd 7/2/2013. Khoitao_giatriMacDinh()
                string Version = "V?";
                bool CKS = false;

                CKS = Company.KDT.SHARE.Components.Globals.IsSignOnLan || Company.KDT.SHARE.Components.Globals.IsSignRemote || Company.KDT.SHARE.Components.Globals.IsSignature;

                if (Company.KDT.SHARE.Components.Globals.IsKTX)
                    Version = string.Format("V{0}", 1);
                else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                    Version = string.Format("V{0}", 2);
                else if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00" && GlobalSettings.SendV4 == false)
                    Version = string.Format("V{0}", 3);
                else if (GlobalSettings.SendV4) //Mac dinh V4
                    Version = string.Format("V{0}", 4);
                if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS) //khai báo VNACCS
                    Version = string.Format("V{0}", 5);

                statusBar.Panels["Version"].ToolTipText = statusBar.Panels["Version"].Text = Version;
                statusBar.ImageList = ilSmall;
                statusBar.Panels["CKS"].ToolTipText = (CKS ? "CKS" : "");
                statusBar.Panels["CKS"].Text = "";
                statusBar.Panels["CKS"].ImageIndex = (CKS ? 60 : -1);

                //statusBar.Panels["DoanhNghiep"].ImageIndex = 37;
                statusBar.Panels["HaiQuan"].ImageIndex = 61;
                statusBar.Panels["Terminal"].ImageIndex = 62;
                //statusBar.Panels["Service"].ImageIndex = 63;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void LoadDongBoDuLieuDN()
        {
            LoadDongBoDuLieuForm dongboDNForm = new LoadDongBoDuLieuForm();
            dongboDNForm.ShowDialog(this);
        }
        void wbManin_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                //docHTML = wbManin.Document;
                //HtmlElement itemTyGia = null;
                //foreach (HtmlElement item in docHTML.GetElementsByTagName("table"))
                //{
                //    if (item.GetAttribute("id") == "AutoNumber6")
                //    {
                //        itemTyGia = item;
                //        break;
                //    }
                //}
                //if (itemTyGia == null)
                //{
                //    MainForm.flag = 0;
                //    return;
                //}
                //int i = 1;
                //string stUSD = "";
                //try
                //{
                //    foreach (HtmlElement itemTD in docHTML.GetElementsByTagName("TD"))
                //    {
                //        if (itemTD.InnerText != null)
                //        {

                //        }
                //        if (itemTD.InnerText != null && itemTD.InnerText.IndexOf("1 USD=") > -1)
                //        {
                //            stUSD = itemTD.InnerText.Trim();
                //        }
                //    }
                //    if (stUSD != "")
                //    {
                //        stUSD = stUSD.Substring(6).Trim();
                //        stUSD = stUSD.Substring(0, stUSD.Length - 3).Trim();
                //        Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.Update(null, "USD", Convert.ToDecimal(stUSD));

                //    }
                //}
                //catch { }
                //XmlDocument doc = new XmlDocument();
                //XmlElement root = doc.CreateElement("Root");
                //doc.AppendChild(root);
                //foreach (HtmlElement itemTD in itemTyGia.GetElementsByTagName("TR"))
                //{
                //    if (i == 1)
                //    {
                //        i++;
                //        continue;
                //    }
                //    HtmlElementCollection collection = itemTD.GetElementsByTagName("TD");
                //    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                //    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                //    itemMaNT.InnerText = collection[1].InnerText;

                //    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                //    itemTenNT.InnerText = collection[2].InnerText;

                //    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                //    itemTyNT.InnerText = collection[3].InnerText;

                //    itemNT.AppendChild(itemMaNT);
                //    itemNT.AppendChild(itemTenNT);
                //    itemNT.AppendChild(itemTyNT);
                //    root.AppendChild(itemNT);
                //}
                //Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.UpdateTyGia(root);
                //doc.Save("TyGia.xml");
                //timer1.Enabled = false;
            }
            catch { MainForm.flag = 0; }
            //timer1.Enabled = false;
        }
        public static void LoadECSPrincipal(long id)
        {
            EcsQuanTri = new ECSPrincipal(id);
        }
        public static void ShowQueueForm()
        {
            queueForm.Show();
        }
        public static void AddToQueueForm(HangDoi hd)
        {
            queueForm.HDCollection.Add(hd);
            queueForm.RefreshQueue();
        }
        public MainForm()
        {
            //try
            //{
            //    Company.KDT.SHARE.Components.Globals.CreateShortcut(
            //        AppDomain.CurrentDomain.BaseDirectory + "SOFTECH.ECS.TQDT.SXXK.exe",
            //        Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\SOFTECH.ECS.TQDT.SXXK v5.0 - VNACCS.lnk",
            //        "", null, "", AppDomain.CurrentDomain.BaseDirectory, "");
            //}

            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage("Lỗi tạo tự động shortcut chương trình.", ex);
            //}

            InitializeComponent();
            wbManin.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbManin_DocumentCompleted);

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            //timer1.Enabled = false;
            //timer1.Stop();

            statusBar.PanelClick += new Janus.Windows.UI.StatusBar.StatusBarEventHandler(statusBar_PanelClick);
            statusBar.MouseHover += new EventHandler(statusBar_MouseHover);
            statusBar.MouseLeave += new EventHandler(statusBar_MouseLeave);

            SetTimerDateTimeStatus();
        }
        private void ShowLoginForm()
        {
            // this.Hide();
        }
        private string HashCode(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {

            try
            {


                //minhnd Định mức mới dành cho HOATHO
                if (GlobalSettings.MA_DON_VI == "0400101556")
                {
                    expSXXK.Groups["grpDinhMucNew"].Visible = true;
                    expSXXK.Groups["grpDinhMuc"].Items["dmKhaiBao_New"].Visible = true;
                }
                else
                {
                    expSXXK.Groups["grpDinhMucNew"].Visible = false;
                    expSXXK.Groups["grpDinhMuc"].Items["dmKhaiBao_New"].Visible = false;
                }

                //minhnd Định mức mới dành cho HOATHO

                //Hungtq complemented 14/12/2010
                //timer1.Enabled = false; //Disable timer get Exchange Rate

                // UpdateOnline.AutoUpdate.RemotePath = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("RemotePath");
                // UpdateOnline.AutoUpdate.AssemblyName = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("AssemblyName");

                bool installCASuccess = Company.KDT.SHARE.VNACCS.ImportCertNet.InstallCA();
                string cer = installCASuccess ? " - Certified" : "- Not certified";

                string strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                string dateRelease = Company.KDT.SHARE.Components.DownloadUpdate.GetDateRelease();
                this.Text += " - Build " + strVersion + (dateRelease != "" ? string.Format(" ({0})", dateRelease) : "") + cer;
                this.Text += " - SĐT HỖ TRỢ : (0236) 3.840.888 . ĐỊA CHỈ EMAIL : ecs@softech.vn ";
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Application.StartupPath + "\\Config.xml");
                    XmlNode node = doc.SelectSingleNode("Root/Type");
                    MainForm.versionHD = Convert.ToInt32(node.InnerText);

                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                GlobalSettings.IsDaiLy = MainForm.versionHD == 1 ? true : false;
                if (MainForm.versionHD == 2)
                {
                    cmdNhapDuLieu.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdXuatDuLieu.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdXuatDuLieuDN.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhapXML.Visible = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdXuatDuLieu.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdNhapDuLieu.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdXuatDuLieuDN.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdNhapXML.Visible = Janus.Windows.UI.InheritableBoolean.True;
                }
                if (GlobalSettings.NGON_NGU == "0")
                {
                    try
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecsv.png");
                        this.BackgroundImageLayout = ImageLayout.Stretch;
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecse.jpg");
                        this.BackgroundImageLayout = ImageLayout.Stretch;
                    }
                    catch { }
                }

                this.Hide();

                if (versionHD == 0)
                {
                    GlobalSettings.ISKHAIBAO = true;
                    Login login = new Login();
                    login.ShowDialog(this);
                }
                else if (versionHD == 1)
                {
                    ThongTinDaiLy DLinfo = new ThongTinDaiLy();
                    try
                    {
                        DLinfo = Company.KDT.SHARE.Components.HeThongPhongKhai.GetThongTinDL();
                        if (DLinfo == null)
                        {
                            Company.Interface.TTDaiLy.FrmInfo finfo = new Company.Interface.TTDaiLy.FrmInfo();
                            finfo.ShowDialog(this);
                            while (!finfo.isSuccess)
                            {
                                if (ShowMessage("Bạn chưa nhập thông tin đại lý. Vui lòng nhập đầy đủ thông tin đại lý khai báo trước khi sử dụng chương trình. Xin cảm ơn!\r\n\r\n Nhập lại thông tin đại lý ?", true) == "Yes")
                                {
                                    finfo = new Company.Interface.TTDaiLy.FrmInfo();
                                    finfo.ShowDialog(this);
                                }
                                else
                                {
                                    Application.ExitThread();
                                    Application.Exit();
                                    return;
                                }
                            }
                        }
                        else
                        {
                            GlobalSettings.MA_DAI_LY = DLinfo.MaDL;
                            GlobalSettings.TEN_DAI_LY = DLinfo.TenDL;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("Không kết nối được với cơ sở dữ liệu. " + ex.Message);
                        ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
                        f.ShowDialog(this);
                    }

                    Company.Interface.TTDaiLy.Login login = new Company.Interface.TTDaiLy.Login();
                    login.ShowDialog(this);

                }
                else if (versionHD == 2)
                {
                    Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                    login.ShowDialog(this);
                }

                if (isLoginSuccess)
                {
                    this.Show();

                    SetLeftGroupName(Company.KDT.SHARE.Components.Globals.LaDNCX);

                    //Hungtq updated 20/12/2011. Cau hinh Ecs Express
                    Express();

                    this.khoitao_GiaTriMacDinh();

                    //Hungtq updated 11/12/2012.
                    Company.KDT.SHARE.Components.Globals.KhoiTao_DanhMucChuanHQ();

                    if (versionHD == 0)
                    {
                        if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                        {
                            QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                            ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmbMenu.Commands[0].Commands[0].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmbMenu.Commands[0].Commands[1].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmbMenu.Commands[0].Commands[2].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                    }
                    else if (versionHD == 2)
                    {
                        switch (typeLogin)
                        {
                            case 1: // User Da Cau Hinh
                                this.Show();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 2: // User Chua Cau Hinh
                                this.Show();
                                DangKyForm dangKyForm = new DangKyForm();
                                dangKyForm.ShowDialog(this);
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 3:// Admin
                                this.Hide();
                                CreatAccountForm fAdminForm = new CreatAccountForm();
                                fAdminForm.ShowDialog(this);
                                this.LoginUserKhac();
                                break;
                        }
                    }

                    vsmMain.DefaultColorScheme = GlobalSettings.GiaoDien.Id;
                    if (GlobalSettings.GiaoDien.Id == "Office2007")
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    }

                    #region active mới
                    try
                    {
                        this.requestActivate_new();

                        int dayInt = 7;
                        int dateTrial = KeySecurity.Active.Install.License.KeyInfo.TrialDays;
                        //try
                        //{
                        //    string day = new Company.KDT.SHARE.Components.Utils.License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ExpiredDate"));
                        //    dayInt = int.Parse(day);
                        //}
                        //catch (Exception) { }
                        if (!string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Key))
                        {
                            cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            if (dayInt > dateTrial)
                                ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty Cổ phẩn SOFTECH để tiếp tục sử dụng", false);
                            //CultureInfo en = new CultureInfo("en-US");
                            //for (int k = 0; k < dayInt; k++)
                            //{
                            //    //Kiem tra Culture, neu la vi-VN -> chuyen doi ve cung 1 chuan en-US.
                            //    string dateString = DateTime.Now.Date.ToString("MM/dd/yyyy");

                            //    if (DateTime.Parse(dateString, en).AddDays(k) == DateTime.Parse(Program.lic.dayExpires, en).Date)
                            //    {
                            //        int ngayConLai = k + 1;
                            //        ShowMessage("Phần mềm còn " + ngayConLai + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", false);
                            //        break;
                            //    }
                            //}
                            this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");
                        }
                        else
                        {
                            this.Text += setText(" - Bản dùng thử.", " - Trial.");
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        ShowMessageTQDT("Lỗi kiểm tra bản quyền", "Thông báo", false);
                        Application.ExitThread();
                        Application.Exit();
                    }




                    #endregion

                    if (string.IsNullOrEmpty(GlobalSettings.MA_DON_VI) || GlobalSettings.MA_DON_VI == "?")
                    {
                        ShowThietLapThongTinDNAndHQ();
                    }

                    Show_Leftpanel();

                    //Hungtq complemented 14/12/2010
                    //timer1.Enabled = true; //Enable timer get Exchange Rate

                    backgroundWorker1.RunWorkerAsync();

                    #region  active cũ

                    //                    int dayInt = 7;
                    //                    int dateTrial = int.Parse(Program.lic.dateTrial);
                    //                    try
                    //                    {
                    //                        string day = new Company.KDT.SHARE.Components.Utils.License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ExpiredDate"));
                    //                        dayInt = int.Parse(day);
                    //                    }
                    //                    catch (Exception) { }
                    //                    if (Program.isActivated)
                    //                    {
                    //                        cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    //                        //CultureInfo vn = new CultureInfo("vi-VN");
                    //                        CultureInfo vn = Thread.CurrentThread.CurrentCulture;
                    //                        for (int k = 0; k < dayInt; k++)
                    //                        {
                    //                            //Kiem tra Culture, neu la vi-VN -> chuyen doi ve cung 1 chuan en-US.
                    //                            string dateString = DateTime.Now.Date.ToShortDateString();

                    //                            if (DateTime.Parse(dateString, vn.DateTimeFormat).AddDays(k) == DateTime.Parse(Program.lic.dayExpires, vn.DateTimeFormat).Date)
                    //                            {
                    //                                int ngayConLai = k + 1;
                    //                                ShowMessage("Phần mềm còn " + ngayConLai + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", false);
                    //                                break;
                    //                            }
                    //                        }
                    //                        this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");
                    //                    }
                    //                    else
                    //                    {
                    //                        this.Text += setText(" - Bản dùng thử.", " - Trial.");

                    //                        if (dateTrial <= 0)
                    //                            this.requestActivate();
                    //                        else if (dateTrial <= dayInt)
                    //                            ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", false);
                    //                    }


                    #endregion

                    //Sao luu du lieu
                    if (GlobalSettings.NGAYSAOLUU != null && GlobalSettings.NGAYSAOLUU != "")
                    {
                        string st = GlobalSettings.NGAYSAOLUU;
                        DateTime time = Convert.ToDateTime(GlobalSettings.NGAYSAOLUU);
                        int NHAC_NHO_SAO_LUU = Convert.ToInt32(GlobalSettings.NHAC_NHO_SAO_LUU);
                        TimeSpan time1 = DateTime.Today.Subtract(time);
                        int ngay = NHAC_NHO_SAO_LUU - time1.Days;
                        if (ngay <= 0)
                            ShowBackupAndRestore(true);
                    }
                    else
                        ShowBackupAndRestore(true);

                    if (!(GlobalSettings.LastBackup.Equals("")) && !(GlobalSettings.NHAC_NHO_SAO_LUU.Equals("")))
                        if (Convert.ToDateTime(GlobalSettings.LastBackup).Add(TimeSpan.Parse(GlobalSettings.NHAC_NHO_SAO_LUU)) == DateTime.Today)
                        {
                            ShowBackupAndRestore(true);
                        }

                    //TODO: VNACCS, Hungtq 15/11/2013
                    //frmTrangThaiPhanHoi.Show();

                    //LanNT dung cho ban express.
                    //Company.KDT.SHARE.Components.DoanhNghiepCfgs.Load(@"ConfigDoanhNghiep\\ConfigDoanhNghiep.xml");
                    //Company.KDT.SHARE.Components.Config cfg = Company.KDT.SHARE.Components.DoanhNghiepCfgs.Install.Find(cf => cf.Key == "Express");
                    //if (cfg == null ||cfg.Value != "yJSXAArLTPqZ1q1U5L0WOw==")
                    //{
                    //    if (cfg == null)
                    //    {
                    //        Company.KDT.SHARE.Components.DoanhNghiepCfgs.Install.Add(new Company.KDT.SHARE.Components.Config { Key = "Express", Value = "false", Note = "Phiên bản express" });
                    //        Company.KDT.SHARE.Components.DoanhNghiepCfgs.Install.SaveConfig(@"ConfigDoanhNghiep\\ConfigDoanhNghiep.xml");
                    //    }
                    //    foreach (ExplorerBarItem item in  expSXXK.Groups[4].Items)
                    //    {
                    //        item.Enabled = false;
                    //    }

                    //}
                    //else if ()
                    //{
                    //    expSXXK.Groups[3].Items[0].Enabled = false;
                    //}
                    if (!Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThongBaoVNACCS", "false")))
                    {
                        FrmThongBaoVNACCS frmThongBaoVNACCS = new FrmThongBaoVNACCS();
                        frmThongBaoVNACCS.ShowDialog();
                        //ShowMessageTQDT("ĐỂ THUẬN LỢI KHI KHAI DỊCH VỤ CÔNG TRỰC TUYẾN TRÊN TRANG WEBSITE HẢI QUAN : http://pus.customs.gov.vn \n.SOFTECH ĐÃ TẠO PHẦN MỀM KÝ CHỮ KÝ SỐ CHO FILE ĐÍNH KÈM GỬI LÊN (HỖ TRỢ ĐỊNH DẠNG pdf,docx,xlsx) \n .ĐỂ CÀI ĐẶT PHẦN MỀM DOANH NGHIỆP VÀO : MENU TRỢ GIÚP - CÔNG CỤ HỖ TRỢ - PHẦN MỀM HỖ TRỢ KÝ CHỮ KÝ SỐ CHO FILE ĐỂ CÀI ĐẶT.", false);
                    }
                    CreateShorcut();
                }
                else
                    Application.Exit();
                //}
                //else
                //{
                //    ShowMessage("Chương trình đã hết hạn sử dụng. \nLiên hệ với Softech để được cung cấp phiên bản chính thức. \nSố điện thoại: (0511)3.810535 hoặc FAX: (0511)3.810278.\nXin cám ơn quí khách hàng đã sử dụng chương trình của chúng tôi.", false);
                //    Application.Exit();
                //}
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            //minhnd check cert    
            if (!GlobalSettings.iSignRemote)
                if (GlobalSettings.CheckChuKySo() != -1)
                    MessageBox.Show("Chữ ký số có MST:" + GlobalSettings.MA_DON_VI + " còn " + GlobalSettings.CheckChuKySo() + " ngày nữa hết hạn sử dụng.", "Expiration Date Certificate", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void requestActivate()
        {
            if (ShowMessage("Đã hết hạn dùng thử.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", true) == "Yes")
            {
                ProdInfo.frmRegister_old obj = new Company.Interface.ProdInfo.frmRegister_old();
                obj.ShowDialog(this);
                requestActivate();
            }
            else
            {
                Application.Exit();
            }
        }
        private void Show_Leftpanel()
        {
            try
            {
                //Kiem tra permission nguyen phu lieu
                if (versionHD == 0)
                {
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.XemDuLieu)))
                    {
                        this.expSXXK.Groups[0].Enabled = false;
                        this.expSXXK.Groups[0].Expanded = false;
                    }
                    else
                    {
                        this.expSXXK.Groups[0].Enabled = true;
                        this.expSXXK.Groups[0].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.KhaiDienTu)))
                        this.expSXXK.Groups[0].Items[0].Enabled = false;
                    else
                        this.expSXXK.Groups[0].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.CapNhatDuLieu)))
                        this.expSXXK.Groups[0].Items[1].Enabled = false;
                    else
                        this.expSXXK.Groups[0].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.XemDuLieu)))
                        this.expSXXK.Groups[0].Items[2].Enabled = false;
                    else
                        this.expSXXK.Groups[0].Items[2].Enabled = true;
                    //Ket thuc kiem tra nguyen phu lieu

                    //Kiem tra permission san pham
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.KhaiDienTu))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.CapNhatDuLieu))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.XemDuLieu)))
                    {
                        this.expSXXK.Groups[1].Enabled = false;
                        this.expSXXK.Groups[1].Expanded = false;
                    }
                    else
                    {
                        this.expSXXK.Groups[1].Enabled = true;
                        this.expSXXK.Groups[1].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.KhaiDienTu)))
                        this.expSXXK.Groups[1].Items[0].Enabled = false;
                    else
                        this.expSXXK.Groups[1].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.CapNhatDuLieu)))
                        this.expSXXK.Groups[1].Items[1].Enabled = false;
                    else
                        this.expSXXK.Groups[1].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.XemDuLieu)))
                        this.expSXXK.Groups[1].Items[2].Enabled = false;
                    else
                        this.expSXXK.Groups[1].Items[2].Enabled = true;
                    //Ket thuc kiem tra san pham

                    //Kiem tra permission dinh muc
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.CapNhatDuLieu))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.XemDuLieu)))
                    {
                        this.expSXXK.Groups[2].Enabled = false;
                        this.expSXXK.Groups[2].Expanded = false;
                    }
                    else
                    {
                        this.expSXXK.Groups[2].Enabled = true;
                        this.expSXXK.Groups[2].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)))
                        this.expSXXK.Groups[2].Items[0].Enabled = false;
                    else
                        this.expSXXK.Groups[2].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.CapNhatDuLieu)))
                        this.expSXXK.Groups[2].Items[1].Enabled = false;
                    else
                        this.expSXXK.Groups[2].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.XemDuLieu)))
                        this.expSXXK.Groups[2].Items[2].Enabled = false;
                    else
                        this.expSXXK.Groups[2].Items[2].Enabled = true;
                    //Ket thuc kiem tra dinh muc

                    //Kiem tra permission to khai
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuXuat))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.XemDuLieu))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.ToKhaiSapHetHan))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CchiPhiXNK)))
                    {
                        this.expSXXK.Groups[3].Enabled = false;
                        this.expSXXK.Groups[3].Expanded = false;
                    }
                    else
                    {
                        this.expSXXK.Groups[3].Enabled = true;
                        this.expSXXK.Groups[3].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)))
                        this.expSXXK.Groups[3].Items[0].Enabled = false;
                    else
                        this.expSXXK.Groups[3].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuXuat)))
                        this.expSXXK.Groups[3].Items[1].Enabled = false;
                    else
                        this.expSXXK.Groups[3].Items[1].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
                        this.expSXXK.Groups[3].Items[2].Enabled = false;
                    else
                        this.expSXXK.Groups[3].Items[2].Enabled = true;

                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.XemDuLieu)))
                        this.expSXXK.Groups[3].Items[3].Enabled = false;
                    else
                        this.expSXXK.Groups[3].Items[3].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.ToKhaiSapHetHan)))
                        this.expSXXK.Groups[3].Items[4].Enabled = false;
                    else
                        this.expSXXK.Groups[3].Items[4].Enabled = true;

                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CchiPhiXNK)))
                        this.expSXXK.Groups[3].Items[5].Enabled = false;
                    else
                        this.expSXXK.Groups[3].Items[5].Enabled = true;
                    //Ket thuc kiem tra to khai

                    //Kiem tra permission thanh khoan
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleThanhKhoan.ThanhKhoan)))
                    {
                        this.expSXXK.Groups[4].Enabled = false;
                        this.expSXXK.Groups[4].Expanded = false;
                    }
                    else
                    {
                        this.expSXXK.Groups[4].Enabled = true;
                        this.expSXXK.Groups[4].Expanded = true;
                    }
                    //Ket thuc kiem tra thanh khoan

                    //Kiem tra permission nguyen phu lieu ton
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTon.TheoDoiNPLTon))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTon.ThongKeNPLTon)))
                    {
                        this.expKhaiBao_TheoDoi.Groups[0].Enabled = false;
                        this.expKhaiBao_TheoDoi.Groups[0].Expanded = false;
                    }
                    else
                    {
                        this.expKhaiBao_TheoDoi.Groups[0].Enabled = true;
                        this.expKhaiBao_TheoDoi.Groups[0].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTon.TheoDoiNPLTon)))
                        this.expKhaiBao_TheoDoi.Groups[0].Items[0].Enabled = false;
                    else
                        this.expKhaiBao_TheoDoi.Groups[0].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTon.ThongKeNPLTon)))
                        this.expKhaiBao_TheoDoi.Groups[0].Items[1].Enabled = false;
                    else
                        this.expKhaiBao_TheoDoi.Groups[0].Items[1].Enabled = true;
                    //Ket thuc kiem tra nguyen phu lieu ton

                    //Kiem tra permission to khai nhap tu HT khac
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacNhap))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacXuat)))
                    {
                        this.expKhaiBao_TheoDoi.Groups[1].Enabled = false;
                        this.expKhaiBao_TheoDoi.Groups[1].Expanded = false;
                    }
                    else
                    {
                        this.expKhaiBao_TheoDoi.Groups[1].Enabled = true;
                        this.expKhaiBao_TheoDoi.Groups[1].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacNhap)))
                        this.expKhaiBao_TheoDoi.Groups[1].Items[0].Enabled = false;
                    else
                        this.expKhaiBao_TheoDoi.Groups[1].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacXuat)))
                        this.expKhaiBao_TheoDoi.Groups[1].Items[1].Enabled = false;
                    else
                        this.expKhaiBao_TheoDoi.Groups[1].Items[1].Enabled = true;
                    //Ket thuc kiem tra to khai nhap tu HT khac

                    //Kiem tra permission quan ly to khai
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleQuanLyToKhai.ThueTonKhoTKNhap))
                        && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacXuat)))
                    {
                        this.expKhaiBao_TheoDoi.Groups[2].Enabled = false;
                        this.expKhaiBao_TheoDoi.Groups[2].Expanded = false;
                    }
                    else
                    {
                        this.expKhaiBao_TheoDoi.Groups[2].Enabled = true;
                        this.expKhaiBao_TheoDoi.Groups[2].Expanded = true;
                    }
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleQuanLyToKhai.ThueTonKhoTKNhap)))
                        this.expKhaiBao_TheoDoi.Groups[2].Items[0].Enabled = false;
                    else
                        this.expKhaiBao_TheoDoi.Groups[2].Items[0].Enabled = true;
                    if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacXuat)))
                        this.expKhaiBao_TheoDoi.Groups[2].Items[1].Enabled = false;
                    else
                        this.expKhaiBao_TheoDoi.Groups[2].Items[1].Enabled = true;
                    //Ket thuc kiem tra quan ly to khai
                }
            }
            catch
            {
            }
        }
        private void expSXXK_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                // Mẫu 15.
                case "cmdMau15":
                    this.showMau15();
                    break;
                // Mẫu 15.
                case "cmdTheoDoi":
                    this.GoodItemsManagerForm();
                    break;
                // Báo cáo quyết toán
                case "cmdKhaiBaoBCQT":
                    this.KhaiBaoBCQT();
                    break;
                // Theo dõi báo cáo quyết toán
                case "cmdTheoDoiBCQT":
                    this.TheoDoiBCQT();
                    break;
                // Khai báo DMHH.
                case "dmhhKhaiBao":
                    this.show_DMHHSendForm();
                    break;
                // Khai báo DMHH.
                case "dmhhTheoDoi":
                    this.show_DMHHManageForm();
                    break;

                // Khai báo NPL.
                case "nplKhaiBao":
                    this.show_NguyenPhuLieuSendForm();
                    break;
                case "nplKhaiBaoSua":
                    this.show_NguyenPhuLieuEditForm();
                    break;
                case "nplKhaiBaoHuy":
                    this.show_NguyenPhuLieuCancelForm();
                    break;
                // Theo dõi NPL.
                case "nplTheoDoi":
                    this.show_NguyenPhuLieuManageForm();
                    break;
                // NPL đã đăng ký.
                case "nplDaDangKy":
                    this.show_NguyenPhuLieuRegistedForm();
                    break;

                // Khai báo SP.
                case "spKhaiBao":
                    this.show_SanPhamSendForm();
                    break;

                case "spKhaiBaoSua":
                    this.show_SanPhamSendEditForm();
                    break;
                case "spKhaiBaoHuy":
                    this.show_SanPhamSendCancelForm();
                    break;
                // Theo dõi SP.
                case "spTheoDoi":
                    this.show_SanPhamManageForm();
                    break;

                // SP đã đăng ký.
                case "spDaDangKy":
                    this.show_SanPhamRegistedForm();
                    break;

                // Khai báo ĐM.
                case "dmDangKy":
                    this.show_DinhMucRegisterForm();
                    break;
                case "dmKhaiBao":
                    this.show_DinhMucSendForm();
                    break;
                case "dmKhaiBaoSua":
                    this.show_DinhMucSendEditForm();
                    break;
                case "dmKhaiBaoHuy":
                    this.show_DinhMucSendCancelForm();
                    break;
                case "dmKhaiBao_New":
                    this.show_DinhMucSendForm_New();
                    break;
                case "dmKhaiBao_new":
                    this.show_DinhMuc_New();
                    break;
                // Định mức thực tế
                case "dmTTRegister":
                    this.show_DinhMucThucTe();
                    break;
                case "dmTTEdit":
                    this.show_DinhMucThucTeEdit();
                    break;
                case "dmTTCancel":
                    this.show_DinhMucThucTeCancel();
                    break;
                case "dmTTManagement":
                    this.show_DinhMucThucTeTheoDoi();
                    break;
                //Lệnh sản xuất
                case "LSXRegister":
                    this.show_DangKyLenhSanXuat();
                    break;
                case "LSXUpdate":
                    this.show_CapNhatLenhSanXuat();
                    break;
                case "LSXManagement":
                    this.show_TheoDoiLenhSanXuat();
                    break;
                case "LSXConfig":
                    this.show_CauHinhLenhSanXuat();
                    break;  
                // Theo dõi ĐM.
                case "dmTheoDoi":
                    this.show_DinhMucManageForm();
                    break;
                case "dmTheoDoi_new":
                    this.show_DinhMucManageForm_new();
                    break;
                case "dmDaThanhKhoan":
                    this.show_DinhMucRegistedForm_NEW();
                    break;

                // ĐM đã đăng ký
                case "dmDaDangKy":
                    this.show_DinhMucRegistedForm();
                    break;

                // Tờ khai mậu dịch.
                case "tkNhapKhau":
                    this.show_ToKhaiMauDichForm("NSX");
                    break;
                case "tkXuatKhau":
                    this.show_ToKhaiMauDichForm("XSX");
                    break;

                case "TheoDoiTKSXXK":
                    show_ToKhaiSXXKManage();
                    break;
                case "ToKhaiSXXKDangKy":
                    show_ToKhaiSXXKDangKy();
                    break;
                case "tkHetHan":
                    show_ToKhaiHetHan();
                    break;
                //Tờ khai quá hạn thanh khoản
                case "tkNopThue":
                    show_ToKhaiQuaHanTK();
                    break;
                case "TKHangTKX":
                    show_HangTKX();
                    break;

                //Hồ sơ thanh lý
                case "AddHSTL":
                    this.show_TaoHoSoThanhLyForm();
                    break;
                case "UpdateHSTL":
                    HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
                    int id = HSTL.getHSTLMoiNhat(EcsQuanTri.Identity.Name, GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                    if (id == 0)
                    {
                        DialogResult result = MessageBox.Show("Bạn có muốn thanh lý hồ sơ theo PO?", "Thanh Khoản", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.OK)
                            HSTL.HSTL_TheoMaHang = true;
                        this.show_TaoHoSoThanhLyForm();
                    }
                    else
                    {
                        HSTL.ID = id;
                        HSTL = HoSoThanhLyDangKy.Load(id);
                        HSTL.LoadBKCollection();
                        //minhnd Thanh khoản theo định múc mới dành cho HOATHO
                        if (GlobalSettings.MA_DON_VI == "0400101556")
                        {
                            DialogResult result = MessageBox.Show("Bạn có muốn thanh lý hồ sơ theo PO?", "Thanh Khoản", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                                HSTL.HSTL_TheoMaHang = true;
                        }
                        //minhnd Thanh khoản theo định múc mới dành cho HOATHO
                        this.show_CapNhatHoSoThanhLyForm(HSTL);
                    }
                    break;
                case "HSTLManage":
                    show_HSTLManageForm();
                    break;
                case "HSTLClosed":
                    show_HSTLDaDongForm();
                    break;
                case "ChiPhiXNK":
                    show_ChiPhiXNKForm();
                    break;
                case "QDThanhKhoanTKN":
                    show_QDThanhThoanTKN();
                    break;
                case "ThanhKhoanSend":
                    show_KhaiBaoThanhKhoanForm();
                    break;
                case "ThanhKhoanManager":
                    break;

                //Chung tu thanh toan
                case "TriGiaHopDong":
                    Show_TriGiaHopDongForm();
                    break;
                case "TaoMoiCTTT":
                    Show_TaoMoiCTTTForm(true);
                    break;
                case "TaoMoiCTTTNhap":
                    Show_TaoMoiCTTTForm(false);
                    break;
                case "TheoDoiCTTT":
                    Show_TheoDoiCTTTForm();
                    break;
                case "AddTLTSCD":
                    Show_AddThanhLyTSCD();
                    break;
                case "TLTSCDManager":
                    Show_TLTSCDManager();
                    break;
                case "htKhaiBao":
                    ShowKhaiBaoHangTon();
                    break;
                case "htTheoDoi":
                    ShowHangTonManager();
                    break;
                case "tieuHuyKhaiBao":
                    ShowKhaiBaoTieuHuy();
                    break;
                case "tieuHuyTheoDoi":
                    ShowTieuHuyManager();
                    break;
                case "tieuHuyDaDangKy":
                    ShowTieuHuyRegisted();
                    break;
                case "KhaiBao_ChotTon":
                    ShowBaoCaoChotTon();
                    break;
                case "TheoDoi_ChotTon":
                    ShowChotTonManager();
                    break;
            }
        }

        private void show_DinhMucRegisterForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucGCRegisterForm f = new DinhMucGCRegisterForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_DinhMucThucTeEdit()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucThucTeGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucThucTeSendForm f = new DinhMucThucTeSendForm();
            f.isEdit = true;
            f.MdiParent = this;
            f.Show();
        }
        private void show_DinhMucThucTeCancel()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucThucTeGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucThucTeSendForm f = new DinhMucThucTeSendForm();
            f.isCancel = true;
            f.MdiParent = this;
            f.Show();
        }
        private void show_CauHinhLenhSanXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuyTacTaoLenhSanXuatForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            QuyTacTaoLenhSanXuatForm f = new QuyTacTaoLenhSanXuatForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_TheoDoiLenhSanXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("LenhSanXuatSPManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            LenhSanXuatSPManagerForm f = new LenhSanXuatSPManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_CapNhatLenhSanXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CapNhatLenhSanXuatGCForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            CapNhatLenhSanXuatGCForm f = new CapNhatLenhSanXuatGCForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_DangKyLenhSanXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("LenhSanXuatSPForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            LenhSanXuatSPForm f = new LenhSanXuatSPForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_DinhMucThucTeTheoDoi()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucThucTeManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucThucTeManagerForm f = new DinhMucThucTeManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_DinhMucThucTe()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucThucTeGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucThucTeSendForm f = new DinhMucThucTeSendForm();
            f.MdiParent = this;
            f.Show();
        }

        private void TheoDoiBCQT()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("BaoCaoQuyetToanManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                BaoCaoQuyetToanManagerForm f = new BaoCaoQuyetToanManagerForm();
                f.MdiParent = this;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void KhaiBaoBCQT()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("BaoCaoQuyetToanForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                BaoCaoQuyetToanForm f = new BaoCaoQuyetToanForm();
                f.MdiParent = this;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void ShowChotTonManager()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("BaoCaoChotTonManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaoCaoChotTonManagerForm f = new BaoCaoChotTonManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowBaoCaoChotTon()
        {
                        Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("BaoCaoChotTonForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaoCaoChotTonForm f = new BaoCaoChotTonForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowTieuHuyRegisted()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongTinTieuHuyManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ThongTinTieuHuyManagerForm f = new ThongTinTieuHuyManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowTieuHuyManager()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongTinTieuHuyManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ThongTinTieuHuyManagerForm f = new ThongTinTieuHuyManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowKhaiBaoTieuHuy()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongTinTieuHuySendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ThongTinTieuHuySendForm f = new ThongTinTieuHuySendForm();
            f.MdiParent = this;
            f.Show();
        }
        private void showKhaiBao_TKVC()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TransportEquipmentForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_TransportEquipmentForm f = new VNACC_TransportEquipmentForm();
            f.MdiParent = this;
            f.Show();
        }
        private void showTheoDoiTKVC()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TransportEquipmentManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TransportEquipmentManagerForm f = new TransportEquipmentManagerForm();
            f.MdiParent = this;
            f.Show();
        }
        #region hang ton
        private void ShowHangTonManager()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HangTon_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HangTonManager = new HangTon_ManagerForm();
            HangTonManager.MdiParent = this;
            HangTonManager.Show();

        }
        private void ShowKhaiBaoHangTon()
        {
            HangTonDetailForm frm = new HangTonDetailForm();
            frm.MdiParent = this;
            frm.Show();
        }
        #endregion
        #region Thanh Ly Tai san co dinh - che xuat
        private void Show_AddThanhLyTSCD()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TaiSanCoDinhEditFrm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TaiSanCoDinhEditForm = new TaiSanCoDinhEditFrm();
            TaiSanCoDinhEditForm.MdiParent = this;
            TaiSanCoDinhEditForm.Show();
        }
        private void Show_TLTSCDManager()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TaiSanCoDinhManagerFrm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TaiSanCoDinhManagerForm = new TaiSanCoDinhManagerFrm();
            TaiSanCoDinhManagerForm.MdiParent = this;
            TaiSanCoDinhManagerForm.Show();
        }
        #endregion

        #region Chung tu thanh toan Form

        private void Show_TriGiaHopDongForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("FrmQuanLyHopDongXuatKhau"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            baseForm = new FrmQuanLyHopDongXuatKhau();
            baseForm.MdiParent = this;
            baseForm.Show();
        }

        private void Show_TaoMoiCTTTForm(bool isThanhToanXuat)
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("FrmChungTuThanhToanChiTiet"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            //baseForm = new ChungTuThanhToanForm_Modified();
            //baseForm.MdiParent = this;
            //baseForm.Show();

            FrmChungTuThanhToanChiTiet frm = new FrmChungTuThanhToanChiTiet();
            frm.HSTL = new HoSoThanhLyDangKy();
            frm.IsOpenFromChayThanhLy = false;
            frm.ChungTuThanhToan = new Company.KDT.SHARE.QuanLyChungTu.CTTT.CTTT_ChungTu();
            frm.LoaiChungTu = isThanhToanXuat ? Company.KDT.SHARE.QuanLyChungTu.CTTT.enumLoaiChungTuTT.Xuat : Company.KDT.SHARE.QuanLyChungTu.CTTT.enumLoaiChungTuTT.Nhap;
            frm.MdiParent = this;
            frm.Show();
        }

        private void Show_TheoDoiCTTTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("FormQuanLyChungTuThanhToan"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            baseForm = new FormQuanLyChungTuThanhToan();
            baseForm.MdiParent = this;
            baseForm.Show();
        }

        #endregion

        private void show_QDThanhThoanTKN()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuyetDinhToKhaiNhapForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            quyetDinhTKNForm = new QuyetDinhToKhaiNhapForm();
            quyetDinhTKNForm.MdiParent = this;
            quyetDinhTKNForm.Show();
        }
        private void show_ChiPhiXNKForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ChiPhiXNKForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            chiPhiXNKForm = new ChiPhiXNKForm();
            chiPhiXNKForm.MdiParent = this;
            chiPhiXNKForm.Show();
        }

        private void show_ToKhaiHetHan()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiSapHetHanForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkHetHanForm = new ToKhaiSapHetHanForm();
            tkHetHanForm.MdiParent = this;
            tkHetHanForm.Show();
        }

        private void show_ToKhaiQuaHanTK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiQuaHanTKForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkQuaHanForm = new ToKhaiQuaHanTKForm();
            tkQuaHanForm.MdiParent = this;
            tkQuaHanForm.Show();
        }
        private void show_HangTKX()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HangTKXuatForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hangTKXForm = new HangTKXuatForm();
            hangTKXForm.MdiParent = this;
            hangTKXForm.Show();
        }

        private void show_HSTLManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HSTLManageForm"))
                {
                    forms[i].Activate();
                    return;

                }
            }
            hstlManageForm = new HSTLManageForm();
            hstlManageForm.MdiParent = this;
            hstlManageForm.Show();
        }

        private void show_ToKhaiSXXKDangKy()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new ToKhaiMauDichRegisterForm();
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.NhomLoaiHinh = "SX";
            tkmdRegisterForm.Text = "Tờ khai SXXK đã đăng ký";
            tkmdRegisterForm.Show();
        }

        private void show_ToKhaiSXXKManage()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.nhomLoaiHinh = "SX";
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Text = "Theo dõi tờ khai SXXK";
            tkmdManageForm.Show();
        }

        private void ShowBKTKX()
        {
        }

        private void ShowBKTKN()
        {
        }

        private void ShowBCThueXNK()
        {
            Company.Interface.Report.SXXK.BCThueXNK bc = new Company.Interface.Report.SXXK.BCThueXNK();
            bc.BindReport(GlobalSettings.MA_HAI_QUAN);
            bc.ShowPreview();
        }

        private void ShowBCNPLXuatNhapTon()
        {
            BCNPLXuatNhapTon bc = new BCNPLXuatNhapTon();
            bc.BindReport(GlobalSettings.MA_HAI_QUAN);
            bc.ShowPreview();
        }

        private void show_TaoHoSoThanhLyForm()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TaoHoSoThanhLyForm") || forms[i].Name.ToString().Equals("BK02WizardForm") || forms[i].Name.ToString().Equals("BK01WizardForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            taoHSTLForm = new TaoHoSoThanhLyForm();
            taoHSTLForm.MdiParent = this;

            taoHSTLForm.Show();
        }
        private void show_CapNhatHoSoThanhLyForm(HoSoThanhLyDangKy HSTL)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CapNhatHoSoThanhLyForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            capNhatHSTLForm = new CapNhatHoSoThanhLyForm();
            capNhatHSTLForm.HSTL = HSTL;
            capNhatHSTLForm.MdiParent = this;
            capNhatHSTLForm.Show();
        }

        private void show_HSTLDaDongForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HSTLDaDongForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hstlDaDongForm = new HSTLDaDongForm();
            hstlDaDongForm.MdiParent = this;
            hstlDaDongForm.Show();
        }
        //private void uiButton1_Click(object sender, EventArgs e)
        //{
        //    new NguyenPhuLieu_Form().ShowDialog(this);
        //}

        //private void uiButton2_Click(object sender, EventArgs e)
        //{
        //    new SanPham_Form().ShowDialog(this);
        //}

        //private void explorerBar1_ItemClick(object sender, ItemEventArgs e)
        //{
        //    this.Cursor = Cursors.WaitCursor;
        //    switch (e.Item.Key)
        //    {
        //            // Nguyên phụ liệu.
        //        case "nplNhap":
        //            this.show_NguyenPhuLieu_Form();
        //            break;
        //        case "nplSend":
        //            this.show_NguyenPhuLieu_Send_Form();
        //            break;
        //        case "nplManage":
        //            this.show_NguyenPhuLieu_Manage_Form();
        //            break;

        //            // Sản phẩm.
        //        case "spNhap":
        //            this.show_SanPham_Form();
        //            break;
        //        case "spSend":
        //            this.show_SanPham_Send_Form();
        //            break;
        //        case "spManage":
        //            this.show_SanPham_Manage_Form();
        //            break;

        //            // Định mức.
        //        case "dmNhap":
        //            this.show_DinhMuc_Form();
        //            break;
        //        case "dmSend":
        //            this.show_DinhMuc_Send_Form();
        //            break;
        //        case "dmManage":
        //            this.show_DinhMuc_Manage_Form();
        //            break;

        //            // Tờ khai mậu dịch.
        //        case "tkNhapKhau_SXXK":
        //            this.show_ToKhaiNhapKhauSXXK_Form();
        //            break;

        //        case "tkSend_SXXK":
        //            this.show_ToKhaiMauDich_Send_Form();
        //            break;

        //        case "tkManage_SXXK":
        //            this.show_ToKhaiMauDich_Manage_Form();					
        //            break;

        //    }
        //    this.Cursor = Cursors.Default;
        //}

        //-----------------------------------------------------------------------------------------
        private void show_DMHHSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DMHHSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DMHHSendForm form = new DMHHSendForm();
            form.MdiParent = this;
            form.Show();
        }

        //-----------------------------------------------------------------------------------------------
        private void show_DMHHManageForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("DMHHManageForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            //dmhhManageForm = new DMHHManageForm();
            //dmhhManageForm.MdiParent = this;
            //dmhhManageForm.Show();
        }
        //-----------------------------------------------------------------------------------------
        private void showMau15()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("Mau15ChiTietForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Mau15ChiTietForm f = new Mau15ChiTietForm();
                f.MdiParent = this;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void GoodItemsManagerForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("GoodItemsManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                GoodItemsManagerForm f = new GoodItemsManagerForm();
                f.MdiParent = this;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------
        private void show_NguyenPhuLieuSendForm()
        {
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("NguyenPhuLieuSendForm_CX"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                nplSendForm_CX = new Company.Interface.KDT.SXXK.DNCX.NguyenPhuLieuSendForm_CX();
                nplSendForm_CX.MdiParent = this;
                nplSendForm_CX.Show();
            }
            else
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("NguyenPhuLieuSendForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                nplSendForm = new NguyenPhuLieuSendForm();
                nplSendForm.MdiParent = this;
                nplSendForm.Show();
            }
        }
        private void show_NguyenPhuLieuEditForm()
        {
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("NguyenPhuLieuSendForm_CX"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                nplSendForm_CX = new Company.Interface.KDT.SXXK.DNCX.NguyenPhuLieuSendForm_CX();
                nplSendForm_CX.MdiParent = this;
                nplSendForm_CX.isEdit = true;
                nplSendForm_CX.Show();
            }
            else
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("NguyenPhuLieuSendForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                nplSendForm = new NguyenPhuLieuSendForm();
                nplSendForm.MdiParent = this;
                nplSendForm.isEdit = true;
                nplSendForm.Show();
            }
        }
        private void show_NguyenPhuLieuCancelForm()
        {
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("NguyenPhuLieuSendForm_CX"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                nplSendForm_CX = new Company.Interface.KDT.SXXK.DNCX.NguyenPhuLieuSendForm_CX();
                nplSendForm_CX.MdiParent = this;
                nplSendForm_CX.isCancel = true;
                nplSendForm_CX.Show();
            }
            else
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("NguyenPhuLieuSendForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                nplSendForm = new NguyenPhuLieuSendForm();
                nplSendForm.MdiParent = this;
                nplSendForm.isCancel = true;
                nplSendForm.Show();
            }
        }
        //-----------------------------------------------------------------------------------------------
        private void show_NguyenPhuLieuManageForm()
        {
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("NguyenPhuLieuManageForm_CX"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                nplManageForm_CX = new NguyenPhuLieuManageForm_CX();
                nplManageForm_CX.MdiParent = this;
                nplManageForm_CX.Show();
            }
            else
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("NguyenPhuLieuManageForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                nplManageForm = new NguyenPhuLieuManageForm();
                nplManageForm.MdiParent = this;
                nplManageForm.Show();
            }

        }
        //-----------------------------------------------------------------------------------------------
        private void show_NguyenPhuLieuRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenPhuLieuRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplRegistedForm = new NguyenPhuLieuRegistedForm();
            nplRegistedForm.MdiParent = this;
            nplRegistedForm.Show();
        }

        ////-----------------------------------------------------------------------------------------------
        private void show_SanPhamManageForm()
        {
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("SanPhamManageForm_CX"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                spManageForm_CX = new SanPhamManageForm_CX();
                spManageForm_CX.MdiParent = this;
                spManageForm_CX.Show();
            }
            else
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("SanPhamManageForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                spManageForm = new SanPhamManageForm();
                spManageForm.MdiParent = this;
                spManageForm.Show();
            }

        }

        //-----------------------------------------------------------------------------------------------
        private void show_TheoDoiPhanBoTKNForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TheoDoiPhanBoTKNhap"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            theodoiPhanBoTKN = new TheoDoiPhanBoTKNhap();
            theodoiPhanBoTKN.MdiParent = this;
            theodoiPhanBoTKN.Show();
        }
        //
        private void show_SanPhamRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SanPhamRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            spRegistedForm = new SanPhamRegistedForm();
            spRegistedForm.MdiParent = this;
            spRegistedForm.Show();

        }

        ////-----------------------------------------------------------------------------------------
        private void show_SanPhamSendForm()
        {
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("SanPhamSendForm_CX"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                spSendForm_CX = new SanPhamSendForm_CX();
                spSendForm_CX.MdiParent = this;
                spSendForm_CX.Show();
            }
            else
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("SanPhamSendForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                spSendForm = new SanPhamSendForm();
                spSendForm.MdiParent = this;
                spSendForm.Show();
            }

        }
        private void show_SanPhamSendEditForm()
        {
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("SanPhamSendForm_CX"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                spSendForm_CX = new SanPhamSendForm_CX();
                spSendForm_CX.isEdit = true;
                spSendForm_CX.MdiParent = this;
                spSendForm_CX.Show();
            }
            else
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("SanPhamSendForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                spSendForm = new SanPhamSendForm();
                spSendForm.MdiParent = this;
                spSendForm.isEdit = true;
                spSendForm.Show();
            }
        }
        private void show_SanPhamSendCancelForm()
        {
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("SanPhamSendForm_CX"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                spSendForm_CX = new SanPhamSendForm_CX();
                spSendForm_CX.isCancel = true;
                spSendForm_CX.MdiParent = this;
                spSendForm_CX.Show();
            }
            else
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("SanPhamSendForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                spSendForm = new SanPhamSendForm();
                spSendForm.MdiParent = this;
                spSendForm.isCancel = true;
                spSendForm.Show();
            }
        }
        ////-----------------------------------------------------------------------------------------

        private void show_DinhMuc_Form()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMuc_Form"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmSendForm_new = new DinhMuc_New();
            dmSendForm_new.MdiParent = this;
            dmSendForm_new.Show();
        }

        //-----------------------------------------------------------------------------------------
        private void show_DinhMucSendForm_New()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dmSendForm_NEW_HOATHO = new DinhMucSendForm_NEW_HOATHO();
            dmSendForm_NEW_HOATHO.MdiParent = this;
            dmSendForm_NEW_HOATHO.Show();


        }

        //-----------------------------------------------------------------------------------------
        private void show_DinhMucSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucSendForm_OLD"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dmSendForm = new DinhMucSendForm_OLD();
            dmSendForm.MdiParent = this;
            dmSendForm.Show();


        }
        private void show_DinhMucSendEditForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucSendForm_OLD"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dmSendForm = new DinhMucSendForm_OLD();
            dmSendForm.MdiParent = this;
            dmSendForm.isEdit = true;
            dmSendForm.Show();


        }
        private void show_DinhMucSendCancelForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucSendForm_OLD"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dmSendForm = new DinhMucSendForm_OLD();
            dmSendForm.MdiParent = this;
            dmSendForm.isCancel = true;
            dmSendForm.Show();


        }
        private void show_DinhMuc_New()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMuc_New"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmSendForm_new = new DinhMuc_New();
            dmSendForm_new.MdiParent = this;
            dmSendForm_new.Name = "DinhMuc_New";
            dmSendForm_new.Show();
        }

        //-----------------------------------------------------------------------------------------------
        private void show_DinhMucManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucManageForm_OLD"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmManageForm = new DinhMucManageForm_OLD();
            dmManageForm.MdiParent = this;
            dmManageForm.Show();

        }

        private void show_DinhMucManageForm_new()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucManageForm_new"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmManageForm_new = new DinhMucManageForm_New();
            dmManageForm_new.MdiParent = this;
            dmManageForm_new.Name = "DinhMucManageForm_new";
            dmManageForm_new.Show();

        }
        private void show_DinhMucRegistedForm_NEW()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucRegistedForm_NEW"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmRegistedForm_new = new DinhMucRegistedForm_NEW();
            dmRegistedForm_new.MdiParent = this;
            dmRegistedForm_new.Show();

        }
        private void show_DinhMucRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmRegistedForm = new DinhMucRegistedForm();
            dmRegistedForm.MdiParent = this;
            dmRegistedForm.Show();

        }
        private void show_KhaiBaoThanhKhoanForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThanhKhoanSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            thanhkhoanSendForm = new ThanhKhoanSendForm();
            thanhkhoanSendForm.MdiParent = this;
            thanhkhoanSendForm.Show();
        }
        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiMauDichForm(string nhomLoaiHinh)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals(nhomLoaiHinh))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdForm = new ToKhaiMauDichForm();
            tkmdForm.OpenType = OpenFormType.Insert;
            // tkmdForm.Name = nhomLoaiHinh;
            tkmdForm.NhomLoaiHinh = nhomLoaiHinh;
            tkmdForm.MdiParent = this;
            tkmdForm.Show();
        }

        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiMauDichSendForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("ToKhaiMauDichSendForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            //tkmdSendForm = new ToKhaiMauDichSendForm();
            //tkmdSendForm.MdiParent = this;
            //tkmdSendForm.Show();
        }

        //-----------------------------------------------------------------------------------------

        private void show_ToKhaiMauDichManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Show();
        }
        public void show_QuanLyMessageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyMessageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            quanLyMsg = new Company.Interface.VNACCS.QuanLyMessageForm();
            quanLyMsg.MdiParent = this;
            quanLyMsg.Show();
        }
        private void expKhaiBao_TheoDoi_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "tkSend":
                    this.show_ToKhaiMauDichSendForm();
                    break;
                case "tkManage":
                    this.show_ToKhaiMauDichManageForm();
                    break;
                case "tkDaDangKy":
                    this.show_ToKhaiMauDichRegisterForm();
                    break;
                case "tdNPLton":
                    this.show_HangtonForm();
                    break;
                case "tkNPLton":
                    this.show_ThongkeForm();
                    break;
                case "tkLHKNhap":
                    this.show_ToKhaiThuocLoaiHinhKhacForm("N");
                    break;
                case "tkLHKXuat":
                    this.show_ToKhaiThuocLoaiHinhKhacForm("X");
                    break;
                case "tkNhap":
                    this.show_PhanBoTKNForm();
                    break;
                case "tkXuat":
                    this.show_PhanBoTKXForm();
                    break;
                case "triGiaTKXuat":
                    this.show_TriGiaToKhaiXuatForm();
                    break;
                case "triGiaTKNhap":
                    this.show_TriGiaToKhaiNhapForm();
                    break;
                case "thueTonTKNhap":
                    this.show_ThueTonToKhaiNhapForm();
                    break;

                case "tdPBTKN":
                    this.show_TheoDoiPhanBoTKNForm();
                    break;
                case "itemKTToKhai":
                    this.show_KiemTraDuLieuTKForm();
                    break;

                case "itemKTDinhMuc":
                    this.show_KiemTraDuLieuDinhMucForm();
                    break;
            }
        }

        private void show_KiemTraDuLieuTKForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("KiemTraDuLieuTKForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ktDuLieuTKForm = new KiemTraDuLieuTKForm();
            ktDuLieuTKForm.MdiParent = this;
            ktDuLieuTKForm.Show();
        }

        private void show_KiemTraDuLieuDinhMucForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("KiemTraDuLieuDMForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ktDuLieuDMForm = new KiemTraDuLieuDMForm();
            ktDuLieuDMForm.MdiParent = this;
            ktDuLieuDMForm.Show();
        }

        private void show_ThueTonToKhaiNhapForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThueTonToKhaiNhapForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            thueTonTKNForm = new ThueTonToKhaiNhapForm();
            thueTonTKNForm.MdiParent = this;
            thueTonTKNForm.Show();
        }

        private void show_TriGiaToKhaiXuatForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TriGiaHangToKhaiXuatForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            triGiaHangTKXForm = new TriGiaHangToKhaiXuatForm();
            triGiaHangTKXForm.MdiParent = this;
            triGiaHangTKXForm.Show();
        }
        private void show_TriGiaToKhaiNhapForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TriGiaHangToKhaiNhapForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            triGiaHangTKNForm = new TriGiaHangToKhaiNhapForm();
            triGiaHangTKNForm.MdiParent = this;
            triGiaHangTKNForm.Show();
        }
        private void show_PhanBoTKXForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("PhanBoTKXForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            pbTKXForm = new PhanBoTKXForm();
            //pbTKXForm.MdiParent = this;
            pbTKXForm.Show();
        }

        private void show_PhanBoTKNForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("PhanBoTKNForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            pbTKNForm = new PhanBoTKNForm();
            //pbTKNForm.MdiParent = this;
            pbTKNForm.Show();
        }

        private void expKD_ItemClick(object sender, ItemEventArgs e)
        {

        }

        private void expDT_ItemClick(object sender, ItemEventArgs e)
        {

        }

        private void expGiaCong_ItemClick(object sender, ItemEventArgs e)
        {

        }

        private void show_ToKhaiThuocLoaiHinhKhacForm(string nhomLoaiHinh)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals(nhomLoaiHinh))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkLoaiHinhKhacForm = new ToKhaiThuocLoaiHinhKhacForm();
            tkLoaiHinhKhacForm.NhomLoaiHinh = nhomLoaiHinh;
            // tkLoaiHinhKhacForm.Name = nhomLoaiHinh;
            tkLoaiHinhKhacForm.OpenType = OpenFormType.Insert;
            tkLoaiHinhKhacForm.MdiParent = this;
            tkLoaiHinhKhacForm.Show();


        }

        private void show_ToKhaiMauDichRegisterForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new ToKhaiMauDichRegisterForm();
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.Show();
        }

        //-----------------------------------------------------------------------------------------
        //hangton
        private void show_HangtonForm()
        {


            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HangTon"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            HangTon_OLD HangtonForm = new HangTon_OLD();
            HangtonForm.MdiParent = this;
            HangtonForm.Show();
        }

        private void show_ThongkeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongkeHangTonForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ThongkeHangTonForm thongkeHangtonForm = new ThongkeHangTonForm();
            thongkeHangtonForm.MdiParent = this;
            thongkeHangtonForm.Show();
        }

        private void pnlMain_SelectedPanelChanged(object sender, Janus.Windows.UI.Dock.PanelActionEventArgs e)
        {

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (queueForm.HDCollection.Count > 0)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(HangDoiCollection));
                FileStream fs = new FileStream("HangDoi.xml", FileMode.Create);
                serializer.Serialize(fs, queueForm.HDCollection);
            }
            queueForm.Dispose();
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            statusBar.Panels["Support"].Visible = true;
            Int32 red;
            Int32 green;
            Int32 blue;
            Random rnd = new Random();
            red = rnd.Next(0, 255);
            green = rnd.Next(0, 255);
            blue = rnd.Next(0, 255);
            Color clr = Color.FromArgb(red, green, blue);
            statusBar.Panels["Support"].Alignment = HorizontalAlignment.Center;
            statusBar.Panels["Support"].FormatStyle.ForeColor = clr;
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdDataVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != "" ? dataVersion : "?"));

                Company.BLL.SXXK.ToKhai.ToKhaiMauDich tokhai = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                //NhanNhoForm f = new NhanNhoForm();
                uint hannhacnho = Convert.ToUInt32(GlobalSettings.HanThanhKhoan);//Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["HanThanhKhoan"]);
                uint thoigianTK = Convert.ToUInt32(GlobalSettings.ThongBaoHetHan);
                System.Data.DataSet ds = tokhai.GetThongBao(hannhacnho, thoigianTK);
                NhacNhoForm.ds = ds;
                if (ds == null || ds.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    queueForm.notifyIcon1.ShowBalloonTip(5000, "Thông báo ", "Có " + ds.Tables[0].Rows.Count + " tờ khai sắp hết hạn chưa được thanh khoản.\nVào phần nhắc nhở để xem chi tiết.", ToolTipIcon.Warning);
                    //f.Show();                   
                }

                long sotk = ToKhaiMauDich.SelectCountSoTK(GlobalSettings.MA_DON_VI);
                //if (sotk % 10 == 0 && sotk > 0)
                //{
                //    WebServiceConnection.sendThongTinDN(GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, "SoTK=" + sotk, "ECS-SXXK");
                //}


                string error = KeySecurity.Active.Install.ECS_updateInfo(UpdateInfo(sotk.ToString()));
                if (!string.IsNullOrEmpty(error))
                    Logger.LocalLogger.Instance().WriteMessage("Lỗi cập nhật thông tin doanh nghiệp: " + error, new Exception(""));

                //Hungtq updated 23/05/2013.
                Company.KDT.SHARE.Components.Globals.KhoiTao_DanhMucChuanHQ_MaHS();

                //Test webservice
                string msgError = "";
                bool webServiceConnection = Company.KDT.SHARE.Components.Globals.ServiceExists(GlobalSettings.DiaChiWS, false, out msgError);
                if (webServiceConnection)
                    statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Blue;
                else
                    statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Red;

                AutoUpdate(string.Empty);

                if (!e.Cancel)
                {
                    //Hungtq complemented 14/12/2010
                    if (Company.KDT.SHARE.Components.Globals.GetTyGia() == true)
                    {
                        //timer1.Enabled = false;
                        MainForm.flag = 1;
                    }
                    else
                    {
                        MainForm.flag = 0;
                        soLanLayTyGia = 0;
                        Thread.Sleep(15000);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //Hungtq, 21/11/2013.
                case "cmdHDSDCKS":
                    HDSD_CKS();
                    break;

                case "cmdHelpVideo":
                    try
                    {
                        this.Cursor = Cursors.WaitCursor;
                        FrmHelpVideo frm = new FrmHelpVideo();
                        frm.Show();
                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        this.Cursor = Cursors.Default;
                    }
                    break;
                case "cmdHDSDVNACCS":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\VNACCS_TaiLieuHuongDanSuDung.pdf");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                    }
                    break;

                case "NhacNho":
                    {
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich tokhai = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        NhanNhoForm f = new NhanNhoForm();
                        uint hannhacnho = Convert.ToUInt32(GlobalSettings.HanThanhKhoan);//Convert.ToUInt32(System.Configuration.ConfigurationManager.AppSettings["HanThanhKhoan"]);
                        uint thoigianTK = Convert.ToUInt32(GlobalSettings.ThongBaoHetHan);
                        System.Data.DataSet ds = tokhai.GetThongBao(hannhacnho, thoigianTK);
                        f.ds = ds;
                        if (f.ds == null || f.ds.Tables[0].Rows.Count == 0)
                        {
                            BaseForm baseForm = new BaseForm();
                            ShowMessage("Không có tờ khai sắp hết hạn.", false);
                            return;
                        }
                        else
                        {
                            f.ShowDialog(this);
                        }
                    }
                    break;
                case "cmdThoat":
                    {
                        this.Close();
                    }
                    break;
                case "DongBoDuLieu":
                    {
                        this.ShowDongBoDuLieuForm();
                    }
                    break;
                case "cmdQuanLyDL":
                    {
                        this.ShowQuanLyNguoiDungDBDL();
                    }
                    break;
                case "cmdImportXml":
                    {
                        new ImportAll4Xml().ShowDialog(this);
                        break;
                    }
                case "cmd2007":
                    if (cmd20071.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    GlobalSettings.Luu_GiaoDien("Office2007");
                    UpdateStyleForAllForm("Office2007");
                    //this.UpdateStyles();

                    break;
                case "cmd2003":
                    if (cmd20031.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    GlobalSettings.Luu_GiaoDien("Office2003");
                    UpdateStyleForAllForm("Office2003");
                    //this.UpdateStyles();
                    break;
                case "cmdExportExccel":
                    ShowExportExcelFile();
                    break;
                case "cmdImportExcel":
                    ShowImportExcelFile();
                    break;

                #region Nhập dữ liệu cho doanh nghiep từ file excel cua hai quan
                case "cmdImportNPL":
                    ShowImportNPLForm();
                    break;
                case "cmdImportSP":
                    ShowImportSPForm();
                    break;
                case "cmdImportDM":
                    ShowImportDMForm();
                    break;
                case "cmdImportTTDM":
                    ShowImportTTDMForm();
                    break;
                case "cmdImportToKhai":
                    ShowImportTKForm();
                    break;
                case "cmdImportHangHoa":
                    ShowImportHMDForm();
                    break;
                case "cmdNPLNhapTon":
                    ShowImportNPLNhapTonForm();
                    break;
                #endregion Nhập dữ liệu cho doanh nghiep từ file excel cua hai quan

                case "cmdHelp":
                    try
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\Huong dan su dung phan mem ECS_TQDT_SXXK.pdf");
                        }
                        else
                        {
                            System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\Huong dan su dung phan mem ECS_TQDT_SXXK.pdf");
                        }
                    }
                    catch { }
                    break;
                case "cmdBieuThueXNK2018":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\BIEU THUE XNK2018 - Update.xlsx");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                    }
                    break;
                case "cmdMaHS":
                    ShowMaHSForm();
                    break;
                    //Cảng trong nước
                case "cmdBerth":
                    ShowBerthForm();
                    break;
                    //Địa điểm lưu kho hàng chờ thông quan
                case "cmdCargo":
                    ShowCargoForm();
                    break;
                    //Địa điểm xếp hàng 
                case "cmdCityUNLOCODE":
                    ShowCityUNLOCODEForm();
                    break;
                    //Tổng hợp danh mục
                case "cmdCommon":
                    ShowCommonForm();
                    break;
                    //Container size
                case "cmdContainerSize":
                    ShowContainerSizeForm();
                    break;
                    // Đội thủ tục HQ
                case "cmdCustomsSubSection":
                    ShowCustomsSubSectionForm();
                    break;
                    // Mã kiểm dịch động vật
                case "cmdOGAUser":
                    ShowOGAUserForm();
                    break;
                    //ĐVT Lượng kiện
                case "cmdPackagesUnit":
                    ShowPackagesUnitForm();
                    break;
                    // Kho ngoại quan
                case "cmdStations":
                    ShowStationsForm();
                    break;
                // Biểu thuế XNK
                case "cmdTaxClassificationCode":
                    ShowTaxClassificationCodeForm();
                    break;
                case "cmdHaiQuan":
                    ShowDVHQForm();
                    break;
                case "cmdNuoc":
                    ShowNuocForm();
                    break;
                case "cmdNguyenTe":
                    ShowNguyenTeForm();
                    break;
                case "cmdPTTT":
                    ShowPTTTForm();
                    break;
                case "cmdPTVT":
                    ShowPTVTForm();
                    break;
                case "cmdDKGH":
                    ShowDKGHForm();
                    break;
                case "cmdDVT":
                    ShowDVTForm();
                    break;
                case "cmdCuaKhau":
                    ShowCuaKhauForm();
                    break;
                case "cmdReloadData":
                    Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
                    ucCategory.ReLoadData();
                    Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucCategoryAllowEmpty = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
                    ucCategoryAllowEmpty.ReLoadData();
                    break;
                case "cmdRestore":
                    ShowRestoreForm();
                    break;
                case "cmdBackUp":
                    ShowBackupAndRestore(true);
                    break;
                case "ThongSoKetNoi":
                    ShowThietLapThongSoKetNoi();
                    break;
                case "TLThongTinDNHQ":
                    ShowThietLapThongTinDNAndHQ();
                    break;
                case "cmdThietLapIn":
                    ShowThietLapInBaoCao();
                    break;
                case "cmdCauHinhToKhai":
                    ShowCauHinhToKhai();
                    break;
                case "cmdConfigReadExcel":
                    ShowThietLapReadExcel();
                    break;    
                case "QuanLyNguoiDung":
                    ShowQuanLyNguoiDung();
                    break;
                case "QuanLyNhom":
                    ShowQuanLyNhomNguoiDung();
                    break;
                case "LoginUser":
                    LoginUserKhac();
                    break;
                case "cmdChangePass":
                    ChangePassForm();
                    break;
                case "cmdDoiMatKhauHQ":
                    ChangePassHaiQuanForm();
                    break;
                case "cmdCloseMe":
                    this.doCloseMe();
                    break;
                case "cmdCloseAllButMe":
                    this.doCloseAllButMe();
                    break;
                case "cmdCloseAll":
                    this.doCloseAll();
                    break;
                case "cmdAutoUpdate":
                    AutoUpdate("MSG");
                    break;
                case "TraCuuMaHS":
                    TraCuuMaHSForm();
                    break;
                case "cmdNhomCuaKhau":
                    ShowNhomCuaKhauForm();
                    break;
                case "cmdLoaiPhiChungTuThanhToan":
                    ShowLoaiPhiChungTuThanhToanForm();
                    break;
                case "cmdDaily":
                    ShowDaiLy();
                    break;
                case "cmdThongBaoVNACCS":
                    FrmThongBaoVNACCS frmThongBaoVNACCS = new FrmThongBaoVNACCS();
                    frmThongBaoVNACCS.Show();
                    break;

                #region Nhập dữ liệu cho doanh nghiep từ đại lý
                case "cmdNPL":
                    this.ImportNPLXML();
                    break;
                case "cmdSP":
                    this.ImportSPXML();
                    break;
                case "cmdDM":
                    this.ImportDMXML();
                    break;
                case "cmdTK":
                    this.ImportTKXML();
                    break;

                #endregion Nhập dữ liệu cho doanh nghiep từ đại lý

                #region Xuat du lieu tu dai ly,doanh nghiep cho phong khai
                case "cmdHUNGNPL":
                    XuatNPLChoPhongKhai();
                    break;
                case "cmdHUNGSP":
                    XuatSanPhamChoPhongKhai();
                    break;
                case "cmdHUNGDM":
                    XuatDinhMucChoPhongKhai();
                    break;
                case "cmdHUNGTK":
                    XuatToKhaiChoPhongKhai();
                    break;
                #endregion Xuat du lieu tu dai ly,doanh nghiep cho phong khai

                case "cmdEL":
                    // if (cmdVN1.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    // Vietnamese();
                    // this.VietNameseLanguge();
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        try
                        {
                            this.BackgroundImage = System.Drawing.Image.FromFile("ecse.jpg");
                            this.BackgroundImageLayout = ImageLayout.Stretch;
                        }
                        catch { }
                        GlobalSettings.NGON_NGU = "1";
                        GlobalSettings.ActiveStatus = "0"; //load login form
                        GlobalSettings.RefreshKey();
                        CultureInfo culture = new CultureInfo("en-US");

                        Thread.CurrentThread.CurrentCulture = culture;
                        Thread.CurrentThread.CurrentUICulture = culture;
                        Form[] forms = this.MdiChildren;
                        for (int i = 0; i < forms.Length; i++)
                        {
                            ((BaseForm)forms[i]).InitCulture("en-US", "Company.Interface.Resources.Language");
                        }
                        this.InitCulture("en-US", "Company.Interface.Resources.Language");
                    }
                    break;
                case "cmdVN":

                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        if (MLMessages("Chương trình sẽ khởi động lại và chuyển giao diện sang tiếng Việt,hãy lưu lại các công việc đang làm.\nBạn muốn chuyển giao diện không?", "MSG_0203092", "", true) == "Yes")
                        {
                            GlobalSettings.NGON_NGU = "0";
                            GlobalSettings.ActiveStatus = "1"; //load login form
                            GlobalSettings.RefreshKey();
                            Application.Restart();
                        }

                    }
                    break;
                case "cmdAbout":
                    this.dispInfo();
                    break;
                case "cmdActivate":
                    this.dispActivate();
                    break;
                #region Xuat du lieu tu dai ly cho doanh nghiep
                case "cmdXuatNPLDN":
                    this.ExportNPL();
                    break;
                case "cmdXuatSPDN":
                    this.ExportSP();
                    break;
                case "cmdXuatDMDN":
                    this.ExportDM();
                    break;
                case "cmdXuatTKDN":
                    this.ExportTK();
                    break;
                #endregion Xuat du lieu tu dai ly cho doanh nghiep

                #region nhap du lieu cho phong khai
                case "cmdNhapNPL":
                    NhapNPLTuDoanhNghiep();
                    break;
                case "cmdNhapDinhMuc":
                    NhapDinhMucTuDoanhNghiep();
                    break;
                case "cmdNhapSanPham":
                    NhapSanPhamTuDoanhNghiep();
                    break;
                case "cmdNhapToKhai":
                    NhapToKhaiTuDoanhNghiep();
                    break;
                case "QuanlyMess":
                    WSForm2 wsform2 = new WSForm2();
                    wsform2.ShowDialog(this);
                    if (WSForm2.IsSuccess == true)
                    {
                        ShowQuanlyMess();
                    }
                    break;
                #endregion nhap du lieu cho phong khai
                case "cmdConfig":
                    {
                        this.ShowConfigForm();
                    }
                    break;
                case "cmdLog":
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\Error.log");
                    break;
                case "cmdChuKySo":
                    ChuKySo();
                    break;
                case "cmdTimer":
                    new FrmCauHinhThoiGian().ShowDialog(this);
                    break;
                case "cmdSignFile":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\PDFSignedDigitalSetup.msi");
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                case "cmdHelpSignFile":
                   try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\HelpSignFile.pdf");
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                case "cmdUpdateCategoryOnline":
                    UpdateCategoryOnline();
                    break;
                case "cmdPerformanceDatabase":
                    PerformanceDatabase();
                    break;
                    
                case "cmdNhanPhanHoi":
                    var mainForm = Application.OpenForms["MainForm"] as MainForm;
                    //mainForm.ShowFormThongBao(null, null);
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate { mainForm.frmThongBao.Show(); }));

                    }
                    else
                        mainForm.frmThongBao.Show();
                    break;
            }

            //TODO: HUNGTQ updated 02/08/2012.
            cmQuerySQL(e);

            //TODO: HUNGTQ updated 20/08/2012.
            cmdBoSungCommand(e);
        }

        private void PerformanceDatabase()
        {
            PerformanceDatabaseForm f = new PerformanceDatabaseForm();
            f.Show(this);
        }
        private void ShowThietLapReadExcel()
        {
            FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
            f.ShowDialog(this);
        }
        private void UpdateCategoryOnline()
        {
            try
            {
                ProcessUpdateCategoryOnline f = new ProcessUpdateCategoryOnline();
                f.Show(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void ShowTaxClassificationCodeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_TaxClassificationCode"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_TaxClassificationCode f = new Company.Interface.VNACCS.Category.VNACC_Category_TaxClassificationCode();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowStationsForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Stations"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Stations f = new Company.Interface.VNACCS.Category.VNACC_Category_Stations();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowPackagesUnitForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_PackagesUnit"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_PackagesUnit f = new Company.Interface.VNACCS.Category.VNACC_Category_PackagesUnit();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowOGAUserForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_OGAUser"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_OGAUser f = new Company.Interface.VNACCS.Category.VNACC_Category_OGAUser();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCustomsSubSectionForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CustomsSubSection"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CustomsSubSection f = new Company.Interface.VNACCS.Category.VNACC_Category_CustomsSubSection();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowContainerSizeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_ContainerSize"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_ContainerSize f = new Company.Interface.VNACCS.Category.VNACC_Category_ContainerSize();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCommonForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Common"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Common f = new Company.Interface.VNACCS.Category.VNACC_Category_Common();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCityUNLOCODEForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CityUNLOCODE"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CityUNLOCODE f = new Company.Interface.VNACCS.Category.VNACC_Category_CityUNLOCODE();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowCargoForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Cargo"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Cargo f = new Company.Interface.VNACCS.Category.VNACC_Category_Cargo();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowBerthForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_Berth"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_Berth f = new Company.Interface.VNACCS.Category.VNACC_Category_Berth();
            f.MdiParent = this;
            f.Show();
        }

        private void HDSD_CKS()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Company.KDT.SHARE.Components.Globals.HDSD_CKS();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        //private double CheckChuKySo() 
        //{
        //    TimeSpan ts = new TimeSpan();
        //    List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = Cryptography.GetX509CertificatedNames();
        //    //cbSigns.Items.Clear();
        //    foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
        //    {

        //        string name = item.GetName();
        //        if (name.Contains(GlobalSettings.MA_DON_VI))
        //        {
        //            DateTime date = DateTime.Parse(item.GetExpirationDateString());
        //            ts = date - DateTime.Now;
        //            return Math.Round(ts.TotalDays);
        //            //if(ts.TotalDays<=30)
        //            //    MessageBox.Show("Chữ ký số có MST:" + GlobalSettings.MA_DON_VI+" còn "+Math.Round(ts.TotalDays)+" nữa hết hạn sử dụng.", "Expiration Date Certificate", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        }
        //    }
        //    return 0;
        //}
        private void ChuKySo()
        {
            FrmCauHinhChuKySo cks = new FrmCauHinhChuKySo();
            cks.Show(this);
            //TODO: Hungtq updated, 7/2/2013. ChuKySo(): Reload sau khi dong form cau hinh CKS
            khoitao_GiaTriMacDinh();
        }

        #region Query SQL

        //TODO: HUNGTQ updated 02/08/2012.

        /// <summary>
        ///  Bo sung dong lenh: CreateQuerySQLCommand(); vao ham khoi dung MainForm()
        /// </summary>
        //private void CreateQuerySQLCommand()
        //{
        //    Janus.Windows.UI.CommandBars.UICommand mnuFormSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuFormSQL");
        //    Janus.Windows.UI.CommandBars.UICommand mnuMiniSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuMiniSQL");
        //    // 
        //    // mnuFormSQL
        //    // 
        //    this.mnuFormSQL.ImageIndex = 42;
        //    this.mnuFormSQL.Key = "mnuFormSQL";
        //    this.mnuFormSQL.Name = "mnuFormSQL";
        //    this.mnuFormSQL.Text = "Form SQL";
        //    // 
        //    // mnuMiniSQL
        //    // 
        //    this.mnuMiniSQL.ImageIndex = 42;
        //    this.mnuMiniSQL.Key = "mnuMiniSQL";
        //    this.mnuMiniSQL.Name = "mnuMiniSQL";
        //    this.mnuMiniSQL.Text = "MiniSQL";

        //    // 
        //    // mnuQuerySQL
        //    // 
        //    this.mnuQuerySQL.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
        //    this.mnuFormSQL,
        //    this.mnuMiniSQL});
        //    this.mnuQuerySQL.ImageIndex = 42;
        //    this.mnuQuerySQL.Key = "mnuQuerySQL";
        //    this.mnuQuerySQL.Name = "mnuQuerySQL";
        //    this.mnuQuerySQL.Text = "Thực hiện truy vấn SQL";
        //}

        /// <summary>
        /// Bo sung dong lenh: cmQuerySQL(e); vao cuoi dong cua ham cmMain_CommandClick(...)
        /// </summary>
        /// <param name="e"></param>
        private void cmQuerySQL(Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "mnuFormSQL":
                    FormSQL();
                    break;
                case "mnuMiniSQL":
                    MiniSQL();
                    break;
            }
        }

        private void FormSQL()
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("FrmQuerySQLUpdate"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                frmQuery = new FrmQuerySQLUpdate();
                frmQuery.MdiParent = this;
                frmQuery.Show();
            }
        }

        private void MiniSQL()
        {
            //WSForm2 login2 = new WSForm2();
            //login2.ShowDialog(this);
            //if (WSForm2.IsSuccess == true)
            //{
            //    string fileName = Application.StartupPath + "\\MiniSQL\\MiniSqlQuery.exe";
            //    if (System.IO.File.Exists(fileName))
            //    {

            //        MiniSqlQuery.Core.DbConnectionDefinition conn = new MiniSqlQuery.Core.DbConnectionDefinition();
            //        conn.ConnectionString = "Data Source=" + GlobalSettings.SERVER_NAME + ";Initial Catalog=" + GlobalSettings.DATABASE_NAME +
            //                                ";User ID=" + GlobalSettings.USER + ";Password=" + GlobalSettings.PASS;
            //        conn.Name = GlobalSettings.DATABASE_NAME;
            //        System.Diagnostics.Process.Start(fileName);
            //    }
            //}
        }

        #endregion

        #region Bo sung command

        //TODO: HUNGTQ updated 16/08/2012.

        /// <summary>
        /// Bo sung dong lenh: cmdBoSungCommand(e); vao cuoi dong cua ham cmMain_CommandClick(...)
        /// </summary>
        /// <param name="e"></param>
        private void cmdBoSungCommand(Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            Company.KDT.SHARE.Components.Globals.OpenCommandBieuThue(e.Command.Key);

            if (e.Command.Key == "cmdGetCategoryOnline")
            {
                //string msg = Company.KDT.SHARE.Components.Globals.UpdateCategoryOnline();

                //if (msg != "")
                //{
                //    if (msg.Contains("E|"))
                //        ShowMessage("Có lỗi trong quá trình thực hiện cập nhật danh mục trực tuyến:\r\n" + msg.Replace("E|", ""), true, true, "");
                //    else
                //    {
                //        Logger.LocalLogger.Instance().WriteMessage(msg, new Exception(""));
                //        ShowMessage(msg, false);
                //    }
                //}
                //else
                //    ShowMessage("Không có bản ghi nào được cập nhật mới. Thông tin danh mục trên hệ thống hiện tại là mới nhất.", false);
            }
            else if (e.Command.Key == "cmdGopY")
            {
            }
            else if (e.Command.Key == "cmdTeamview")
            {
                try
                {
                    System.Diagnostics.Process.Start("https://www.teamviewer.com/vi/tai-xuong-tu-dong-teamviewer/");
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            else if (e.Command.Key == "cmdCapNhatHS8SoAuto")
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Bạn có chắc chắn muốn chuyển mã HS danh mục Nguyên phụ liệu, Sản phẩm thành 8 số không?");
                sb.AppendLine();
                sb.AppendLine("Quy tắc chuyển mã:");
                sb.AppendLine("   - Chương trình sẽ tự động cắt bỏ 2 số '00' bên phải của mã HS, chỉ lấy 8 số.");
                sb.AppendLine();
                sb.AppendLine("Vì vậy mã HS sau chuyển đổi có thể không đúng với danh mục chuẩn mã HS 8 số, bạn phải kiểm tra lại danh mục mã HS vừa chuyển đổi.");

                if (ShowMessageTQDT("Chuyên đổi mã Biểu thuế (HS)", sb.ToString(), true) == "Yes")
                {
                    if (Company.KDT.SHARE.Components.Globals.ConvertHS8Auto(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, "SXXK") == true)
                    {
                        ShowMessage("Đã thực hiện thành công.", false);
                    }
                }
            }
            else if (e.Command.Key == "cmdCapNhatHS8SoManual")
            {
                HS08SO.ChuyenMaHS08So f = new Company.Interface.HS08SO.ChuyenMaHS08So();
                f.ShowDialog();
            }
            else if (e.Command.Key == "cmdImageResize")
            {
                Cursor = Cursors.WaitCursor;

                string msg = Company.KDT.SHARE.Components.Globals.DownloadToolImageResize();

                if (msg.Length != 0)
                    ShowMessage(msg, false);
            }
            else if (e.Command.Key == "cmdImageResizeHelp")
            {
                Cursor = Cursors.WaitCursor;

                string msg = Company.KDT.SHARE.Components.Globals.HDSDImageResize();

                if (msg.Length != 0)
                    ShowMessage(msg, false);
            }
            else if (e.Command.Key == "cmdUpdateDatabase")
            {
                UpdateNewDataVersion();
            }
        }

        private void UpdateNewDataVersion()
        {
            try
            {
                //  Cursor = Cursors.WaitCursor;

                Helper.Controls.UpdateDatabaseSQLForm.Instance(true).ShowDialog();

                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdDataVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != "" ? dataVersion : "?"));

                //TODO: Reload lai du lieu sau khi Nang cap du lieu moi. Hungtq, 21/11/2013.
                //  Company.KDT.SHARE.VNACCS.Controls.ucBase.InitialData(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            //finally { Cursor = Cursors.Default; }
        }

        #endregion

        private void ShowConfigForm()
        {
            DangKyForm dangKyForm = new DangKyForm();
            dangKyForm.ShowDialog(this);

        }
        #region Doanh nghiệp khai đại lý
        private void ShowDaiLy()
        {
            WSForm2 login = new WSForm2();
            login.ShowDialog(this);
            if (WSForm2.IsSuccess == true)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("FrmDaiLy"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }

                FrmDaiLy frmDaiLy = new FrmDaiLy();
                frmDaiLy.MdiParent = this;
                frmDaiLy.Show();
            }
        }
        #endregion
        private void ExportNPL()
        {
            try
            {
                //  NguyenPhuLieuDangKyCollection col = new NguyenPhuLieuDangKyCollection();
                Company.BLL.SXXK.NguyenPhuLieuCollection col = new Company.BLL.SXXK.NguyenPhuLieuCollection();
                col = Company.BLL.SXXK.NguyenPhuLieu.GetNPLXML(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.NguyenPhuLieuCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }


        }
        private void ExportSP()
        {
            try
            {

                Company.BLL.SXXK.SanPhamCollection col = new Company.BLL.SXXK.SanPhamCollection();
                col = Company.BLL.SXXK.SanPham.GetSanPhamXML(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.SanPhamCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
        private void ExportDM()
        {
            DinhMucDaDangKyForm dmdk = new DinhMucDaDangKyForm();
            dmdk.ShowDialog(this);

        }
        private void ExportTK()
        {
            ToKhaiMauDichDaDangKyForm tkmdddkForm = new ToKhaiMauDichDaDangKyForm();
            tkmdddkForm.ShowDialog(this);
        }
        private void ShowQuanlyMess()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CuaKhauForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            quanlyMess = new QuanLyMessage();
            quanlyMess.MdiParent = this;
            quanlyMess.Show();
        }
        private void NhapToKhaiTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    ToKhaiMauDichCollection tkDKCollection = (ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    int soTK = 0;
                    foreach (ToKhaiMauDich tkDK in tkDKCollection)
                    {
                        try
                        {
                            tkDK.InsertFull();
                            soTK++;
                        }
                        catch { }
                    }
                    if (soTK > 0)
                        ShowMessage("Nhập thành công " + soTK + " tờ khai.", false);

                    fs.Close();

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private void NhapSanPhamTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    SanPhamDangKyCollection spDKCollection = (SanPhamDangKyCollection)serializer.Deserialize(fs);
                    int soSP = 0;
                    foreach (SanPhamDangKy spDK in spDKCollection)
                    {
                        if (spDK.InsertUpdateFull())
                            soSP++;
                    }
                    if (soSP > 0)
                        ShowMessage("Nhập thành công " + soSP + " sản phẩm.", false);

                    fs.Close();

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private void NhapDinhMucTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    DinhMucDangKyCollection dmDKCollection = (DinhMucDangKyCollection)serializer.Deserialize(fs);
                    int soDM = 0;
                    foreach (DinhMucDangKy dmDK in dmDKCollection)
                    {
                        if (dmDK.InsertUpdateFull())
                            soDM++;
                    }
                    if (soDM > 0)
                        ShowMessage("Nhập thành công " + soDM + " định mức.", false);

                    fs.Close();

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private void NhapNPLTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(NguyenPhuLieuDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    NguyenPhuLieuDangKyCollection nplDKCollection = (NguyenPhuLieuDangKyCollection)serializer.Deserialize(fs);
                    int soNPL = 0;
                    foreach (NguyenPhuLieuDangKy nplDK in nplDKCollection)
                    {
                        if (nplDK.InsertUpdateFull())
                            soNPL++;
                    }
                    if (soNPL > 0)
                        ShowMessage("Nhập thành công " + soNPL + " nguyên phụ liệu.", false);

                    fs.Close();

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }
        private void dispActivate()
        {
            ProdInfo.frmRegister_old obj = new Company.Interface.ProdInfo.frmRegister_old();
            obj.Show();
        }

        private void dispInfo()
        {
            ProdInfo.frmInfo obj = new Company.Interface.ProdInfo.frmInfo();
            obj.ShowDialog(this);
        }

        public void English()
        {
            langCheck = 2;
            CultureInfo c = CultureInfo.CurrentCulture;
            if (!c.ToString().Equals("en-US"))
            {
                ;// this.InitCulture("en-US");
            }
        }

        public void Vietnamese()
        {
            langCheck = 1;
            CultureInfo c = CultureInfo.CurrentCulture;
            if (!c.ToString().Equals("vi-VN"))
            {
                ;// this.InitCulture("vi-VN");
            }
        }

        private void XuatToKhaiChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ToKhaiMauDichManageForm f = new ToKhaiMauDichManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void XuatDinhMucChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            DinhMucManageForm_OLD f = new DinhMucManageForm_OLD();
            f.MdiParent = this;
            f.Show();
        }

        private void XuatSanPhamChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SanPhamManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            SanPhamManageForm f = new SanPhamManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void XuatNPLChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenPhuLieuManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            NguyenPhuLieuManageForm f = new NguyenPhuLieuManageForm();
            f.MdiParent = this;
            f.Show();
        }

        #region Nhập dữ liệu
        //

        private void ImportNPLXML()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.NguyenPhuLieuCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.BLL.SXXK.NguyenPhuLieuCollection nplDKCollection = (Company.BLL.SXXK.NguyenPhuLieuCollection)serializer.Deserialize(fs);
                    fs.Close();
                    Company.BLL.SXXK.NguyenPhuLieu.NhapNPLXML(nplDKCollection);
                    ShowMessage("Import thành công", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }

        }

        private void ImportSPXML()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.SanPhamCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.BLL.SXXK.SanPhamCollection SPDKCollection = (Company.BLL.SXXK.SanPhamCollection)serializer.Deserialize(fs);
                    fs.Close();
                    Company.BLL.SXXK.SanPham.NhapSPXML(SPDKCollection);
                    ShowMessage("Import thành công", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }

        }
        private string CheckDataDinhMucImport(Company.BLL.SXXK.DinhMucCollection DinhMucDKCollection)
        {
            string st = "";
            string masanpham = "";
            Company.BLL.SXXK.DinhMucCollection DinhMucDKCollectionExit = new Company.BLL.SXXK.DinhMucCollection();
            foreach (Company.BLL.SXXK.DinhMuc DinhMucDK in DinhMucDKCollection)
            {
                if (Company.BLL.SXXK.DinhMuc.checkIsExist(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, DinhMucDK.MaSanPHam))
                {
                    DinhMucDKCollectionExit.Add(DinhMucDK);
                    if (masanpham.Trim().ToUpper() != DinhMucDK.MaSanPHam.Trim().ToUpper())
                    {
                        masanpham = DinhMucDK.MaSanPHam;
                        st += "Sản phẩm " + DinhMucDK.MaSanPHam + "\n";
                    }
                }
            }
            foreach (Company.BLL.SXXK.DinhMuc DinhMucDK in DinhMucDKCollectionExit)
            {
                DinhMucDKCollection.Remove(DinhMucDK);
            }
            return st;
        }
        private void ImportDMXML()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.DinhMucCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.BLL.SXXK.DinhMucCollection DinhMucDKCollection = (Company.BLL.SXXK.DinhMucCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = CheckDataDinhMucImport(DinhMucDKCollection);
                    if (st != "")
                    {
                        ShowMessage("Các " + st + " đã có định mức trong hệ thống nên sẽ được bỏ qua.", false);
                    }
                    Company.BLL.SXXK.DinhMuc.NhapDMXML(DinhMucDKCollection, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                    ShowMessage("Import thành công", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }

        }


        private void ImportTKXML()
        {
            Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollectionImport = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
            Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollectionExits = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection));
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKMDCollectionImport = (Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(TKMDCollectionImport, TKMDCollectionExits);
                    if (st == "")
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich.NhapDuLieuXML(TKMDCollectionImport);
                    else
                    {
                        ShowMessage(st + "đã có trong hệ thống.Chương trình sẽ bỏ qua những tờ khai đã có trong hệ thống.", false);
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich.NhapDuLieuXML(TKMDCollectionImport);
                    }
                    ShowMessage("Import thành công", false);
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }

        }
        private string checkDataImport(Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollectionImport, Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollectionExit)
        {
            string st = "";
            foreach (Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD in TKMDCollectionImport)
            {
                Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMDData = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                TKMDData.SoToKhai = TKMD.SoToKhai;
                TKMDData.MaHaiQuan = TKMD.MaHaiQuan;
                TKMDData.MaLoaiHinh = TKMD.MaLoaiHinh;
                TKMDData.NamDangKy = TKMD.NamDangKy;
                if (TKMDData.Load())
                {
                    st += "Tờ khai " + TKMDData.SoToKhai + "/" + TKMDData.MaLoaiHinh + "/" + TKMDData.MaHaiQuan + "/" + TKMDData.NamDangKy + "\n";
                    TKMDCollectionExit.Add(TKMD);
                }
            }
            foreach (Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD in TKMDCollectionExit)
            {
                TKMDCollectionImport.Remove(TKMD);
            }
            return st;
        }
        //private void ImportData()
        //{

        //}

        //
        #endregion Nhập dữ liệu

        private void ChangePassHaiQuanForm()
        {
            ChangePasswordHQForm f = new ChangePasswordHQForm();
            f.ShowDialog(this);
        }
        private void ChangePassForm()
        {
            ChangePassForm f = new ChangePassForm();
            f.ShowDialog(this);
        }
        private void LoginUserKhac()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
            MainForm.EcsQuanTri = null;
            isLoginSuccess = false;
            if (versionHD == 0)
            {
                Login login = new Login();
                login.ShowDialog(this);
            }
            else if (versionHD == 1)
            {
                Company.Interface.TTDaiLy.Login login = new Company.Interface.TTDaiLy.Login();
                login.ShowDialog(this);
            }
            else if (versionHD == 2)
            {
                Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                login.ShowDialog(this);
            }
            if (isLoginSuccess)
            {
                //statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = "Người dùng: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                this.Show();
                if (versionHD == 0)
                {
                    if (MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    else
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)))
                    {
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else if (versionHD == 1)
                {
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                }
                else if (versionHD == 2)
                {
                    switch (typeLogin)
                    {
                        case 1: // User Da Cau Hinh
                            this.Show();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 2: // User Chua Cau Hinh
                            this.Show();
                            DangKyForm dangKyForm = new DangKyForm();
                            dangKyForm.ShowDialog(this);
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 3:// Admin
                            this.Hide();
                            CreatAccountForm fAdminForm = new CreatAccountForm();
                            fAdminForm.ShowDialog(this);
                            this.ShowLoginForm();
                            break;
                    }
                }
                khoitao_GiaTriMacDinh();
            }
            else
                Application.Exit();
        }
        private void ShowQuanLyNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            NguoiDung = new QuanLyNguoiDung();
            NguoiDung.MdiParent = this;
            NguoiDung.Show();
        }
        private void ShowQuanLyNhomNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNhomNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            NhomNguoiDung = new QuanLyNhomNguoiDung();
            NhomNguoiDung.MdiParent = this;
            NhomNguoiDung.Show();
        }
        private void ShowCauHinhToKhai()
        {
            CauHinhToKhaiForm f = new CauHinhToKhaiForm();
            f.ShowDialog(this);
        }

        private void ShowExportExcelFile()
        {
            ExportToExcelForm export = new ExportToExcelForm();
            export.ShowDialog(this);

        }
        private void ShowImportExcelFile()
        {
            ImportFromExcelForm import = new ImportFromExcelForm();
            import.ShowDialog(this);

        }

        private void ShowThietLapInBaoCao()
        {
            CauHinhInForm f = new CauHinhInForm();
            f.ShowDialog(this);
        }
        private void ShowRestoreForm()
        {
            RestoreForm f = new RestoreForm();
            f.ShowDialog(this);
        }
        private void ShowThietLapThongTinDNAndHQ()
        {
            ThongTinDNAndHQForm f = new ThongTinDNAndHQForm();
            f.ShowDialog(this);
            khoitao_GiaTriMacDinh();

            //Test webservice
            string msgError = "";
            bool webServiceConnection = Company.KDT.SHARE.Components.Globals.ServiceExists(GlobalSettings.DiaChiWS, false, out msgError);
            if (webServiceConnection)
                statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Blue;
            else
                statusBar.Panels["Service"].FormatStyle.ForeColor = System.Drawing.Color.Red;
        }
        private void ShowThietLapThongSoKetNoi()
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog(this);

        }
        private void ShowBackupAndRestore(bool isBackUp)
        {
            BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            f.isBackUp = isBackUp;
            f.ShowDialog(this);
        }
        private void ShowLoaiPhiChungTuThanhToanForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("LoaiPhiChungTuThanhToanForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            loaiPhiCTTTForm = new LoaiPhiChungTuThanhToanForm();
            loaiPhiCTTTForm.MdiParent = this;
            loaiPhiCTTTForm.Show();
        }
        private void ShowNhomCuaKhauForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NhomCuaKhau"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            nhomCuaKhauForm = new NhomCuaKhauForm();
            nhomCuaKhauForm.MdiParent = this;
            nhomCuaKhauForm.Show();
        }
        private void ShowCuaKhauForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("CuaKhauForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //ckForm = new CuaKhauForm();
            //ckForm.MdiParent = this;
            //ckForm.Show();
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_BorderGate"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_BorderGate f = new Company.Interface.VNACCS.Category.VNACC_Category_BorderGate();
            f.MdiParent = this;
            f.Show();
        }
        private void ShowDVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_QuantityUnit"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_QuantityUnit f = new Company.Interface.VNACCS.Category.VNACC_Category_QuantityUnit();
            f.MdiParent = this;
            f.Show();

            //dvtForm = new DonViTinhForm();
            //dvtForm.MdiParent = this;
            //dvtForm.Show();
        }

        private void ShowDKGHForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DKGHForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dkghForm = new DKGHForm();
            dkghForm.MdiParent = this;
            dkghForm.Show();
        }
        private void ShowPTVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_TransportMeans"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_TransportMeans f = new Company.Interface.VNACCS.Category.VNACC_Category_TransportMeans();
            f.MdiParent = this;
            f.Show();

            //ptvtForm = new PTVTForm();
            //ptvtForm.MdiParent = this;
            //ptvtForm.Show();
        }
        private void ShowPTTTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PTTTForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ptttForm = new PTTTForm();
            ptttForm.MdiParent = this;
            ptttForm.Show();
        }

        private void ShowNguyenTeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CurrencyExchange"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CurrencyExchange f = new Company.Interface.VNACCS.Category.VNACC_Category_CurrencyExchange();
            f.MdiParent = this;
            f.Show();

            //ntForm = new NguyenTeForm();
            //ntForm.MdiParent = this;
            //ntForm.Show();
        }

        private void ShowNuocForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NuocForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            nuocForm = new NuocForm();
            nuocForm.MdiParent = this;
            nuocForm.Show();
        }

        private void ShowDVHQForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_CustomsOffice"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_CustomsOffice f = new Company.Interface.VNACCS.Category.VNACC_Category_CustomsOffice();
            f.MdiParent = this;
            f.Show();

            //dvhqForm = new DonViHaiQuanForm();
            //dvhqForm.MdiParent = this;
            //dvhqForm.Show();
        }

        private void ShowMaHSForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_Category_HSCode"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            Company.Interface.VNACCS.Category.VNACC_Category_HSCode f = new Company.Interface.VNACCS.Category.VNACC_Category_HSCode();
            f.MdiParent = this;
            f.Show();
            //maHSForm = new MaHSForm();
            //maHSForm.MdiParent = this;
            //maHSForm.Show();
        }
        private void TraCuuMaHSForm()
        {
            //HaiQuan.HS.MainForm f = new HaiQuan.HS.MainForm();
            //f.Show();
            ShowMaHSForm();
        }
        private void ShowImportNPLNhapTonForm()
        {
            ImportNPLNhapTonForm f = new ImportNPLNhapTonForm();
            f.ShowDialog(this);
        }

        private void ShowImportHMDForm()
        {
            ImportHMDForm f = new ImportHMDForm();
            f.ShowDialog(this);
        }

        private void ShowImportTKForm()
        {
            ImportTKForm f = new ImportTKForm();
            f.ShowDialog(this);
        }

        private void ShowImportTTDMForm()
        {
            ImportTTDMForm f = new ImportTTDMForm();
            f.TTDMCollection = new Company.BLL.SXXK.ThongTinDinhMuc().SelectCollectionAll();
            f.ShowDialog(this);
        }

        private void ShowImportDMForm()
        {
            ImportDMForm f = new ImportDMForm();
            f.DMCollection = new Company.BLL.SXXK.DinhMuc().SelectCollectionAll();
            //f.TTDMCollection = new Company.BLL.SXXK.ThongTinDinhMuc().SelectCollectionAll(); 
            f.ShowDialog(this);
        }

        private void ShowImportSPForm()
        {
            ImportSPForm f = new ImportSPForm();
            f.SPCollection = new Company.BLL.SXXK.SanPham().SelectCollectionAll();
            f.ShowDialog(this);
        }

        private void ShowImportNPLForm()
        {
            ImportNPLForm f = new ImportNPLForm();
            f.NPLCollection = new Company.BLL.SXXK.NguyenPhuLieu().SelectCollectionAll();
            f.ShowDialog(this);
        }

        #region Đồng bộ dữ liệu
        private void DongBoDuLieuDN()
        {
            BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            f.ShowDialog(this);
        }
        private void ShowDongBoDuLieuForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("FrmDongBoDuLieu_VNACCS"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DBDLForm_VNACCS = new FrmDongBoDuLieu_VNACCS();
            DBDLForm_VNACCS.MdiParent = this;
            DBDLForm_VNACCS.Show();
        }
        private void ShowQuanLyNguoiDungDBDL()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("QuanLyNguoiDungDBDL"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //QuanLyDaiLy = new DongBoDuLieu_New.QuanTri.QuanLyNguoiDungDBDL();
            //QuanLyDaiLy.MdiParent = this;
            //QuanLyDaiLy.Show();
        }
        #endregion
        private void UpdateStyleForAllForm(string style)
        {
            this.vsmMain.DefaultColorScheme = style;
            queueForm.vsmMain.DefaultColorScheme = style;
            if (nplSendForm != null)
                nplSendForm.vsmMain.DefaultColorScheme = style;
            if (nplManageForm != null)
                nplManageForm.vsmMain.DefaultColorScheme = style;
            if (nplRegistedForm != null)
                nplRegistedForm.vsmMain.DefaultColorScheme = style;
            if (spSendForm != null)
                spSendForm.vsmMain.DefaultColorScheme = style;
            if (spManageForm != null)
                spManageForm.vsmMain.DefaultColorScheme = style;
            if (spRegistedForm != null)
                spRegistedForm.vsmMain.DefaultColorScheme = style;
            if (dmSendForm != null)
                dmSendForm.vsmMain.DefaultColorScheme = style;
            if (dmManageForm != null)
                dmManageForm.vsmMain.DefaultColorScheme = style;
            if (dmRegistedForm != null)
                dmRegistedForm.vsmMain.DefaultColorScheme = style;
            if (tkmdForm != null)
                tkmdForm.vsmMain.DefaultColorScheme = style;
            //if (tkmdSendForm != null)
            //    tkmdSendForm.vsmMain.DefaultColorScheme = style;
            if (tkmdManageForm != null)
                tkmdManageForm.vsmMain.DefaultColorScheme = style;
            if (taoHSTLForm != null)
                taoHSTLForm.vsmMain.DefaultColorScheme = style;
            if (capNhatHSTLForm != null)
                capNhatHSTLForm.vsmMain.DefaultColorScheme = style;
            if (hstlDaDongForm != null)
                hstlDaDongForm.vsmMain.DefaultColorScheme = style;
            if (tkmdRegisterForm != null)
                tkmdRegisterForm.vsmMain.DefaultColorScheme = style;
            if (maHSForm != null)
                maHSForm.vsmMain.DefaultColorScheme = style;
            if (pbTKNForm != null)
                pbTKNForm.vsmMain.DefaultColorScheme = style;
            if (pbTKXForm != null)
                pbTKXForm.vsmMain.DefaultColorScheme = style;
            if (ptttForm != null)
                ptttForm.vsmMain.DefaultColorScheme = style;
            if (ptvtForm != null)
                ptvtForm.vsmMain.DefaultColorScheme = style;
            if (dvtForm != null)
                dvtForm.vsmMain.DefaultColorScheme = style;
            if (dvhqForm != null)
                dvhqForm.vsmMain.DefaultColorScheme = style;
            if (ckForm != null)
                ckForm.vsmMain.DefaultColorScheme = style;
            if (ntForm != null)
                ntForm.vsmMain.DefaultColorScheme = style;
            if (dkghForm != null)
                dkghForm.vsmMain.DefaultColorScheme = style;
            if (nuocForm != null)
                nuocForm.vsmMain.DefaultColorScheme = style;

        }

        private void cmbMenu_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }
        private void closeAllButThisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
        }
        private void CreateShorcutAutoUpdate()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar +"AUTO UPDATE "+ Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = AppDomain.CurrentDomain.BaseDirectory + "\\AppAutoUpdate.exe";
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
        private void CreateShorcut()
        {
            string link =Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
        #region AutoUpdate ONLINE

        private void AutoUpdate(string args)
        {
            try
            {
                CreateShorcutAutoUpdate();
                //Kiem tra phien ban du lieu truoc khi cap nhat phien ban chuong trinh moi
                if (Helper.Controls.UpdateDatabaseSQLForm.Instance(true).IsHasNewDataVersion)
                {
                    UpdateNewDataVersion();
                }

                Company.KDT.SHARE.Components.DownloadUpdate dl = new DownloadUpdate(args);
                dl.DoDownload();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        #region TẠO COMMAND TOOLBAR

        private delegate void CreateCommandCallback(string lastestVersion);
        private void CreateCommand(string lastestVersion)
        {
            try
            {
                this.cmbMenu.CommandManager = this.cmMain;
                this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                    this.cmdNewVersion
                });

                if (!this.cmMain.Commands.Contains("cmdNewVersion"))
                    this.cmMain.Commands.Add(this.cmdNewVersion);

                // 
                // cmdNewVersion
                // 
                this.cmdNewVersion.Key = "cmdNewVersion";
                this.cmdNewVersion.Name = "cmdNewVersion";
                this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;
                this.cmdNewVersion.TextAlignment = Janus.Windows.UI.CommandBars.ContentAlignment.MiddleCenter;
                this.cmdNewVersion.TextImageRelation = Janus.Windows.UI.CommandBars.TextImageRelation.ImageBeforeText;
                this.cmdNewVersion.ImageReplaceableColor = System.Drawing.Color.Pink;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //HUNGTQ, Update 11/06/2010.

        private Janus.Windows.UI.CommandBars.UICommand cmdNewVersion;

        /// <summary>
        /// Tạo command chứng từ đính kèm bổ sung.
        /// </summary>
        private void CreateCommandBosung(string lastestVersion)
        {
            #region Initialize
            //Tao nut command tren toolbar.
            this.cmdNewVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdNewVersion");

            //this.cmbMenu.CommandManager = this.cmMain;
            //this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            //    this.cmdNewVersion
            //});

            //if (!this.cmMain.Commands.Contains("cmdNewVersion"))
            //    this.cmMain.Commands.Add(this.cmdNewVersion);

            //// 
            //// cmdNewVersion
            //// 
            //this.cmdNewVersion.Key = "cmdNewVersion";
            //this.cmdNewVersion.Name = "cmdNewVersion";
            //this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;

            // Invoke the method that updates the form's label
            this.Invoke(new CreateCommandCallback(this.CreateCommand), new object[] { lastestVersion });

            #endregion

            cmdNewVersion.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdNewVersion_Click);

        }

        private void cmdNewVersion_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                switch (e.Command.Key)
                {
                    case "cmdNewVersion":
                        AutoUpdate("MSG");
                        break;
                }
            }
            catch (Exception ex) { }
        }

        #endregion

        #endregion

        #region Begin ECS Express

        private void Express()
        {
            try
            {
                string val = KeySecurity.KeyCode.Decrypt(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Ep"));

                if (!string.IsNullOrEmpty(val) && bool.Parse(val) == true)
                {
                    SetExpress(false);
                }
                else
                    SetExpress(true);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void SetExpress(bool visible)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Janus.Windows.UI.InheritableBoolean status = visible == true ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

                /*Menu He thong*/
                cmdConfig.Visible = status;             //Thiet lap thong tin doanh nghiep
                cmdImportXml.Visible = status;            //Nhap du lieu XML
                cmdNhapXML.Visible = status;            //Nhap du lieu tu dai ly
                cmdDongBoPhongKhai.Visible = cmdNhapDuLieu.Visible = status;    //Nhap du lieu tu doanh nghiep
                cmdRestore.Visible = status;            //Phuc hoi du lieu
                cmdXuatDuLieu.Visible = status;         //Xuat du lieu cho phong khai
                cmdXuatDuLieuDN.Visible = status;       //Xuat du lieu cho doanh nghiep
                cmdNhapXuat.Visible = status;       //Nhập du lieu cho doanh nghiep

                /*Menu Giao dien*/
                cmdEL.Visible = status;

                //NPL Ton
                expKhaiBao_TheoDoi.Groups["grpNPLTon"].Items["tkNPLTon"].Visible = visible;
                expKhaiBao_TheoDoi.Groups["grpNPLTon"].Items["tdPBTKN"].Visible = visible;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        #endregion End ECS Express

        #region Active
        private void requestActivate_new()
        {

            string sfmtMsg = "{0}\nVui lòng liên hệ với Công ty cổ phẩn SOFTECH để tiếp tục sử dụng";
            bool isShowActive = false;

            if (KeySecurity.Active.Install.License.KeyInfo == null)
            {
                sfmtMsg = string.Format(sfmtMsg, "Bạn chưa đăng ký sử dụng phần mềm.");
                isShowActive = true;
            }
            //else if (Helpers.GetMD5Value(Security.Active.Install.License.KeyInfo.ProductId) != "ECS_TQDT_KD")
            //{
            //    sfmtMsg = string.Format(sfmtMsg, "Mã sản phẩm không hợp lệ.");
            //    isShowActive = true;
            //}
            else if (KeySecurity.Active.Install.License.KeyInfo.TrialDays == 0)
            {
                sfmtMsg = string.Format(sfmtMsg, "Đã hết hạn dùng phần mềm");
                isShowActive = true;
            }

            if (isShowActive)
            {
                if (ShowMessage(sfmtMsg, true) == "Yes")
                {
                    ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
                    if (obj.ShowDialog(this) == DialogResult.OK)
                    {
                        ShowMessage("Kích hoạt thành công. Chương trình sẽ tự khởi động lại", false);
                        Application.ExitThread();
                        Application.Restart();
                    }
                    else
                    {
                        ShowMessage("Kích hoạt không thành công, chương trình sẽ tự đóng", false);
                        Application.ExitThread();
                        Application.Exit();
                    }

                }
                else
                {
                    Application.ExitThread();
                    Application.Exit();
                }
            }
            if (KeySecurity.Active.Install.License.KeyInfo.IsShow
                && !string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Notice))
            {
                InfoOnline inf = new InfoOnline();
                inf.SetRTF(KeySecurity.Active.Install.License.KeyInfo.Notice);
                inf.ShowDialog();
            }
        }
        #endregion

        #region Cập nhật thông tin

        private string UpdateInfo(string soTK)
        {
            Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer info = new Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer();
            #region Tạo thông tin update
            if (GlobalSettings.IsDaiLy)
            {
                string ListDoanhNghiep = Company.KDT.SHARE.Components.HeThongPhongKhai.GetDoanhNghiep();
                ThongTinDaiLy DLinfo = new ThongTinDaiLy();
                DLinfo = Company.KDT.SHARE.Components.HeThongPhongKhai.GetThongTinDL();
                info.Id = DLinfo.MaDL;
                info.Name = DLinfo.TenDL;
                info.Address = DLinfo.DiaChiDL;
                info.Phone = DLinfo.SoDienThoaiDL;
                info.Contact_Person = DLinfo.NguoiLienHeDL;
                info.Email = DLinfo.EmailDL;
                info.Id_Customs = GlobalSettings.MA_HAI_QUAN;
                info.IP_Customs = GlobalSettings.DiaChiWS;
                info.ServerName = GlobalSettings.SERVER_NAME;
                info.Data_Name = GlobalSettings.DATABASE_NAME;
                info.Data_User = GlobalSettings.USER;
                info.Data_Pass = GlobalSettings.PASS;
                info.Data_Version = Company.KDT.SHARE.Components.Version.GetVersion();
                info.App_Version = Application.ProductVersion;
                info.LastCheck = DateTime.Now;
                info.temp1 = ListDoanhNghiep;
                info.MachineCode = KeySecurity.KeyCode.ProcessInfo();
                info.Product_ID = "ECS_TQDT_SXXK_V4";
                info.RecordCount = soTK;
            }
            else
            {
                string SignRemote = string.Empty; // Thông tin của doanh nghiệp sử dụng chữ kí số
                if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
                {
                    SignRemote = "Sữ dụng chữ kí số online";
                    SignRemote += ". User: " + Company.KDT.SHARE.Components.Globals.UserNameSignRemote;
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                {
                    SignRemote = "Sử dụng chữ kí số qua mạng nội bộ";
                    SignRemote += ". Database chữ kí số: " + Company.KDT.SHARE.Components.Globals.DataSignLan;
                }
                info.Id = GlobalSettings.MA_DON_VI;
                info.Name = GlobalSettings.TEN_DON_VI;
                info.Address = GlobalSettings.DIA_CHI;
                info.Phone = GlobalSettings.SoDienThoaiDN;
                info.Contact_Person = GlobalSettings.NguoiLienHe;
                info.Email = GlobalSettings.MailDoanhNghiep;
                info.Id_Customs = GlobalSettings.MA_HAI_QUAN;
                info.IP_Customs = GlobalSettings.DiaChiWS;
                info.ServerName = GlobalSettings.SERVER_NAME;
                info.Data_Name = GlobalSettings.DATABASE_NAME;
                info.Data_User = GlobalSettings.USER;
                info.Data_Pass = GlobalSettings.PASS;
                info.Data_Version = Company.KDT.SHARE.Components.Version.GetVersion();
                info.App_Version = Application.ProductVersion;
                info.LastCheck = DateTime.Now;
                info.temp1 = SignRemote;
                //info.temp2 = string.Empty;
                info.MachineCode = KeySecurity.KeyCode.ProcessInfo();
                info.Product_ID = "ECS_TQDT_SXXK_V4";
                info.RecordCount = soTK;
            }
            #endregion
            return Helpers.Serializer(info);
        }
        #endregion

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        #region Cai dat CSDL

        /// <summary>
        /// Bo sung dong lenh: cmdCaiDatCSDL(e); vao cuoi dong cua ham cmMain_CommandClick(...)
        /// </summary>
        /// <param name="e"></param>
        private void cmdCaiDatCSDL(Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string msg = "";

                //Cai dat MS SQL Server Express
                if (e.Command.Key == "cmdInstallSQLServer")
                {
                    msg = InstallSQLServer();
                }
                //Cai dat SQL Server Management Studio Express
                else if (e.Command.Key == "cmdInstallSQLManagement")
                {
                    msg = InstallSQLManagement();
                }
                //Cai dat file du lieu
                else if (e.Command.Key == "cmdAttachDatabase")
                {
                    if (Company.KDT.SHARE.Components.SQL.TestConnection(Company.KDT.SHARE.Components.Globals.ReadNodeXmlConnectionStrings2()))
                        Helper.Controls.MessageBoxControlV.ShowMessage("Phát hiện dữ liệu mặc định đã được thiết lập.", false);
                    else
                        msg = AttachDatabase();
                }

                if (msg.Length != 0)
                    ShowMessage(msg, false);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }

        private string InstallSQLServer()
        {
            try
            {
                //Tim thu muc cai dat SQLEXPR2005. Thu muc nay se co khi cai file SETUP ECS EXPRESS
                string folderApp = AppDomain.CurrentDomain.BaseDirectory;
                string fileSQLServer = "SQLEXPR.EXE";

                if (System.IO.Directory.Exists(folderApp))
                {
                    System.IO.DirectoryInfo di = new DirectoryInfo(folderApp);

                    string folderSQl = di.Root.Name + "SOFTECH\\ECS\\TQDT\\SQLEXPR2005";
                    string[] files = System.IO.Directory.GetFiles(folderSQl, fileSQLServer, SearchOption.AllDirectories);

                    //Tim file cai dat
                    if (files.Length == 0)
                    {
                        return "Không tìm thấy file cài đặt SQL Server Express";
                    }
                    else
                    {
                        //Kiem tra xem co ket noi duoc den SQl voi instance mac dinh la .\ECSExpress khong?.
                        if (!Company.KDT.SHARE.Components.SQL.TestConnection(Company.KDT.SHARE.Components.Globals.ReadNodeXmlConnectionStrings2()))
                        {
                            //Mo thu muc chua file cai dat
                            System.Diagnostics.Process.Start(folderSQl);
                        }
                        else
                            ShowMessage("Hệ quản trị Cơ sở dữ liệu (Microsoft SQL Server Express) đã được cài trên máy bạn.", false);

                    }
                }
                else
                    return "Không tìm thấy thư mục cài đặt file SQL Server Express";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return ex.Message; }

            return "";
        }

        private string InstallSQLManagement()
        {
            try
            {
                //Tim thu muc cai dat SQLEXPR2005. Thu muc nay se co khi cai file SETUP ECS EXPRESS
                string folderApp = AppDomain.CurrentDomain.BaseDirectory;
                string fileSQLServerSSMSEE = "SQLServer2005_SSMSEE.msi";

                if (System.IO.Directory.Exists(folderApp))
                {
                    System.IO.DirectoryInfo di = new DirectoryInfo(folderApp);

                    string folderSQl = di.Root.Name + "SOFTECH\\ECS\\TQDT\\SQLEXPR2005";
                    string[] files = System.IO.Directory.GetFiles(folderSQl, fileSQLServerSSMSEE, SearchOption.AllDirectories);

                    //Tim file cai dat
                    if (files.Length == 0)
                    {
                        return "Không tìm thấy file cài đặt SQL Server Management Studio Express";
                    }
                    else
                    {
                        //Mo thu muc chua file cai dat
                        System.Diagnostics.Process.Start(folderSQl);
                    }
                }
                else
                    return "Không tìm thấy thư mục cài đặt file SQL Server Management Studio Express";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return ex.Message; }

            return "";
        }

        private string AttachDatabase()
        {
            try
            {
                //Attach data file
                Company.KDT.SHARE.Components.SQL.AttachDatabase(GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS, "KD", 4);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return ex.Message; }

            return "";
        }

        #endregion

        #region Danh cho DN Che xuat

        delegate void SetLeftGroupNameCallback(bool laDNCX);

        /// <summary>
        /// Cap nhat lai ten hien trhi cac GROUP thong tin danh cho Doanh nghiep che xuat
        /// </summary>
        /// <param name="laDNCX"></param>
        public void SetLeftGroupName(bool laDNCX)
        {
            try
            {
                if (uiPanel0.InvokeRequired)
                {
                    SetLeftGroupNameCallback d = new SetLeftGroupNameCallback(SetLeftGroupName);
                    this.Invoke(d, new object[] { laDNCX });
                }
                else
                {
                    uiPanel1.Text = laDNCX ? "Loại hình SXXK - KCX" : "Khai báo Thông quan điện tử";
                    uiPanel2.Text = laDNCX ? "Quản lý NPL tồn - KCX" : "Quản lý Nguyên phụ liệu tồn";
                    //uiPanel0.Width = laDNCX ? 230 : 250;

                    expSXXK.Groups["grpNguyenPhuLieu"].Text = laDNCX ? "Hàng đưa vào KCX" : "Nguyên phụ liệu";
                    expSXXK.Groups["grpSanPham"].Text = laDNCX ? "Hàng đưa ra KCX" : "Sản phẩm";
                    expSXXK.Groups["grpDinhMuc"].Text = laDNCX ? "Định mức sản phẩm" : "Định mức";
                    expSXXK.Groups["grpThanhLy"].Text = laDNCX ? "Hồ sơ thanh khoản" : "Hồ sơ thanh khoản";
                    expSXXK.Groups["grpTLTSCD"].Visible = laDNCX ? true : false;

                    expSXXK.Groups["grpHangTon"].Visible = false;
                    expSXXK.Groups["grpTieuHuy"].Visible = true;
                    expSXXK.Groups["grpGiaiTrinh"].Visible = false;
                    expSXXK.Groups["grpQDKT"].Visible = false;
                    expSXXK.Groups["grpCTNX"].Visible = false;

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #endregion

        private void pmMain_MdiTabMouseDown(object sender, Janus.Windows.UI.Dock.MdiTabMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                e.Tab.Form.Activate();
                if (this.MdiChildren.Length == 1)
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                mnuRightClick.Show(this, e.X + pmMain.MdiTabGroups[0].Location.X, e.Y + 6);
            }
        }

        private void doCloseMe()
        {
            Form form = pmMain.MdiTabGroups[0].SelectedTab.Form;
            form.Close();
        }

        private void doCloseAllButMe()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i] != pmMain.MdiTabGroups[0].SelectedTab.Form)
                {
                    forms[i].Close();
                }
            }
        }

        private void doCloseAll()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
        }

        private void statusBar_PanelClick(object sender, Janus.Windows.UI.StatusBar.StatusBarEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                switch (e.Panel.Key)
                {
                    case "DoanhNghiep":
                    case "HaiQuan":
                    case "Terminal":
                    case "Service":
                    case "Version":
                        ShowThietLapThongTinDNAndHQ();
                        break;
                    case "CKS":
                        ChuKySo();
                        break;
                    case "Support":
                        Support();
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { this.Cursor = Cursors.Default; }

        }

        private void Support()
        {
            frmSendSupport f = new frmSendSupport();
            f.ShowDialog(this);
        }

        private void statusBar_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void statusBar_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        /***VNACCS***/
        //Hoa Don
        private VNACC_HoaDonForm khaiBaoHoaDon;
        private VNACC_GiayPhepForm_SEA khaiBaoGiayPhep;
        private VNACC_GiayPhepManageForm giayPhepManage;
        private VNACC_HoaDonManageForm hoaDonManage;
        //to khai tri gia thap
        private VNACC_TKNhapTriGiaForm toKhaiNhapTriGia;
        private VNACC_TKXuatTriGiaForm toKhaiXuatTriGia;
        private VNACC_TKTriGiaManager toKhaiTriGiaManager;
        //To khai VNACC
        private VNACC_ToKhaiMauDichXuatForm toKhaiXuat;
        private VNACC_ToKhaiMauDichNhapForm toKhaiNhap;
        private VNACC_ToKhaiManager toKhaiManager;
        //To khai van chuyen
        private VNACC_ChuyenCuaKhau toKhaiVC;
        private VNACC_ChuyenCuaKhau_ManagerForm toKhaiVCManager;
        // Dang ky hang miem thue TEA
        private VNACC_TEA_ManagerForm TEAManager;
        //Dang ky hang Tam nhap tai xuat
        private VNACC_TIA_ManagerForm TIAManager;

        private void explorerBarVNACCS_TKMD_ItemClick(object sender, ItemEventArgs e)
        {
            try
            {
                if (!GlobalSettings.iSignRemote)
                {
                    double date = GlobalSettings.CheckChuKySo();
                    //MessageBox.Show("Chữ ký số còn:" + date + " ngày sử dụng.(Hãy gia hạn chữ ký số!!!)", "Hạn sử dụng CKS", MessageBoxButtons.OK,MessageBoxIcon.Warning);
                    if (date == -1)
                    {
                        //MessageBox.Show("Không có chữ ký số", "Hạn sử dụng CKS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (date <= 30)
                    {
                        if (date <= 0)
                        {
                            MessageBox.Show("Chữ ký số đã hết hạn sử dụng. Vui lòng gia hạn thêm", "Hạn sử dụng CKS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        else
                            MessageBox.Show("Chữ ký số còn:" + date + " ngày sử dụng.(Hãy gia hạn chữ ký số!!!)", "Hạn sử dụng CKS", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                switch (e.Item.Key)
                {
                    //To khai VNACC
                    case "ToKhaiNhap":
                        this.showToKhaiNhap();
                        break;
                    case "ToKhaiXuat":
                        this.showToKhaiXuat();
                        break;
                    case "TheoDoiToKhai":
                        this.showTheoDoiToKhai();
                        break;
                    case "TheoDoiChungTu":
                        this.showTheoDoiChungTuToKhai();
                        break; 
                    // To khai van chuyen
                    case "KhaiBao_TKVC":
                        this.showToKhaiVC();
                        break;
                    case "TheoDoi_TKVC":
                        this.showTheoDoiToKhaiVC();
                        break;
                    // To khai tri gia thap
                    case "ToKhaiNhapTriGiaThap":
                        this.showToKhaiNhapTriGia();
                        break;
                    case "ToKhaiXuatTriGiaThap":
                        this.showToKhaiXuatTriGia();
                        break;
                    case "TheoDoiTKTG":
                        this.showTheoDoiToKhaiTriGia();
                        break;
                    //Quản lý kho
                    case "KhaiBao_PK":
                        this.showKhaiBao_PK();
                        break;
                    case "TheoDoi_PK":
                        this.showTheoDoiPK();
                        break;
                    // Dang ky hang mien thue
                    case "KhaiBaoTEA":
                        this.showTEA();
                        break;
                    case "TheoDoiTEA":
                        this.showManagerTEA();
                        break;

                    // Dang ky hang mien thue
                    case "KhaiBaoTIA":
                        this.showTIA();
                        break;
                    case "TheoDoiTIA":
                        this.showManagerTIA();
                        break;

                    //To khai sua bo sung thue cua hang hoa
                    case "KhaiBao_TK_KhaiBoSung_ThueHangHoa":
                        this.show_VNACC_TKBoSung_ThueHangHoaForm();
                        break;
                    case "TheoDoi_TK_KhaiBoSung_ThueHangHoa":
                        this.show_VNACC_TKBoSung_ThueHangHoaManageForm();
                        break;

                    //Chung tu thanh toan
                    case "KhaiBao_CTTT_IAS":
                        this.Show_VNACC_CTTT_IAS();
                        break;
                    case "KhaiBao_CTTT_IBA":
                        this.Show_VNACC_CTTT_IBA();
                        break;
                    case "TheoDoi_ChungTuThanhToan":
                        this.Show_VNACC_CTTTManageForm();
                        break;

                    //Chung tu dinh kem
                    case "KhaiBao_ChungTuKem_MSB":
                        this.show_VNACC_ChungTuKem_MSBForm();
                        break;
                    case "KhaiBao_ChungTuKem_HYS":
                        this.show_VNACC_ChungTuKem_HYSForm();
                        break;
                    case "TheoDoi_ChungTuKem":
                        this.show_VNACC_ChungTuKemManageForm();
                        break;
                    case "cmdSignFile":
                        this.show_VNACC_SignFile();
                        break;
                    //Tờ khai vận chuyển đủ điều kiện qua KVGS
                    case "KhaiBao_TKVCQKVGS":
                        this.showKhaiBao_TKVC();
                        break;
                    case "TheoDoi_TKVCQKVGS":
                        this.showTheoDoiTKVC();
                        break;
                    //Đinh danh hàng hóa
                    case "cmdKhaiBaoDinhDanh":
                        this.showKhaiBao_DinhDanhHH();
                        break;
                    case "cmdTheoDoiDinhDanh":
                        this.showTheoDoiDinhDanhHH();
                        break;

                    //Tách vận đơn
                    case "cmdKhaiBaoTachVanDon":
                        this.showKhaiBao_TachVanDon();
                        break;
                    case "cmdTheoDoiTachVanDon":
                        this.showTheoDoiTachVanDon();
                        break;

                    //Quản lý thu phí
                    case "cmdHangContainer":
                        this.showToKhaiNP_HangContainer();
                        break;
                    case "cmdHangRoi":
                        this.showToKhaiNP_HangRoi();
                        break;
                    case "cmdDanhSachTKNP":
                        this.showDanhSachTKNP();
                        break;
                    case "cmdTraCuuBL":
                        this.showTraCuuBL();
                        break;
                    case "cmdRegisterInformation":
                        this.showRegisterInformation();
                        break;
                    case "cmdRegisterManagement":
                        this.showRegisterManagement();
                        break;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void showRegisterManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("RegisterManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            RegisterManagementForm f = new RegisterManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showRegisterInformation()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_Register_Information"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_Register_Information f = new HP_Register_Information();
            f.MdiParent = this;
            f.Show();
        }

        private void showTraCuuBL()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_SearchInfomationForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_SearchInfomationForm f = new HP_SearchInfomationForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showDanhSachTKNP()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_ToKhaiNopPhiManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_ToKhaiNopPhiManagementForm f = new HP_ToKhaiNopPhiManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showToKhaiNP_HangRoi()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_ToKhaiNopPhiForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_ToKhaiNopPhiForm f = new HP_ToKhaiNopPhiForm();
            f.IsContainer = false;
            f.MdiParent = this;
            f.Show();
        }

        private void showToKhaiNP_HangContainer()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HP_ToKhaiNopPhiForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HP_ToKhaiNopPhiForm f = new HP_ToKhaiNopPhiForm();
            f.IsContainer = true;
            f.MdiParent = this;
            f.Show();
        }
        private void showTheoDoiTachVanDon()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_BillOfLadingManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_BillOfLadingManagementForm f = new VNACC_BillOfLadingManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showKhaiBao_TachVanDon()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_BillOfLadingForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_BillOfLadingForm f = new VNACC_BillOfLadingForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showTheoDoiDinhDanhHH()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhDanhHangHoaManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhDanhHangHoaManagerForm f = new DinhDanhHangHoaManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showKhaiBao_DinhDanhHH()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CapSoDinhDanhHangHoaForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            CapSoDinhDanhHangHoaForm f = new CapSoDinhDanhHangHoaForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_SignFile()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SignDigitalDocumentForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            SignDigitalDocumentForm f = new SignDigitalDocumentForm();
            f.MdiParent = this;
            f.Show();
        }

        private void showTheoDoiPK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("Theo dõi phiếu kho"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            PhieuKhoManager f = new PhieuKhoManager();
            f.MdiParent = this;
            f.Show();
        }

        private void showKhaiBao_PK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("Phiếu nhập kho"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            PhieuKho f = new PhieuKho();
            f.MdiParent = this;
            f.Show();


        }

        private void explorerBarVNACCS_GiayPhep_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Giay phep
                case "KhaiBao_GiayPhep_SEA":
                    this.show_VNACC_GiayPhepForm_SEA();
                    break;
                case "TheoDoi_GiayPhep_SEA":
                    this.show_VNACC_GiayPhepManageForm_SEA();
                    break;
                case "KhaiBao_GiayPhep_SFA":
                    this.show_VNACC_GiayPhepForm_SFA();
                    break;
                case "TheoDoi_GiayPhep_SFA":
                    this.show_VNACC_GiayPhepManageForm_SFA();
                    break;
                case "KhaiBao_GiayPhep_SAA":
                    this.show_VNACC_GiayPhepForm_SAA();
                    break;
                case "TheoDoi_GiayPhep_SAA":
                    this.show_VNACC_GiayPhepManageForm_SAA();
                    break;
                case "KhaiBao_GiayPhep_SMA":
                    this.show_VNACC_GiayPhepForm_SMA();
                    break;
                case "TheoDoi_GiayPhep_SMA":
                    this.show_VNACC_GiayPhepManageForm_SMA();
                    break;
            }
        }

        private void explorerBarVNACCS_HoaDon_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Hoa don
                case "KhaiBaoHoaDon":
                    this.show_VNACC_HoaDonForm();
                    break;
                case "TheoDoiHoaDon":
                    this.show_VNACC_HoaDonManageForm();
                    break;
            }
        }

        #region Khai bao sua doi/ bo sung thue hang hoa

        private void show_VNACC_TKBoSung_ThueHangHoaManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TKBoSung_ThueHangHoaManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_TKBoSung_ThueHangHoaManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_TKBoSung_ThueHangHoaForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TKBoSung_ThueHangHoaForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_TKBoSung_ThueHangHoaForm();
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        #region VNACC Chung tu kem
        private void show_VNACC_ChungTuKemManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuKemManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuKemManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_ChungTuKem_MSBForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("VNACC_ChungTuDinhKemForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            BaseForm f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.MSB);
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_ChungTuKem_HYSForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("VNACC_ChungTuDinhKemForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            BaseForm f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.HYS);
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        // Hoa don
        #region Hoa Don
        private void show_VNACC_HoaDonForm()
        {

            khaiBaoHoaDon = new VNACC_HoaDonForm();
            khaiBaoHoaDon.MdiParent = this;
            khaiBaoHoaDon.Show();
        }
        private void show_VNACC_HoaDonManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_HoaDonManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hoaDonManage = new VNACC_HoaDonManageForm();
            hoaDonManage.MdiParent = this;
            hoaDonManage.Show();
        }
        #endregion

        //giay phep
        #region Giay Phep
        private void show_VNACC_GiayPhepForm_SEA()
        {
            khaiBaoGiayPhep = new VNACC_GiayPhepForm_SEA();
            khaiBaoGiayPhep.MdiParent = this;
            khaiBaoGiayPhep.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SEA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            giayPhepManage = new VNACC_GiayPhepManageForm();
            giayPhepManage.MdiParent = this;
            giayPhepManage.Show();
        }

        private void show_VNACC_GiayPhepForm_SFA()
        {

            BaseForm f = new VNACC_GiayPhepForm_SFA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SFA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm_SFA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepManageForm_SFA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepForm_SAA()
        {

            BaseForm f = new VNACC_GiayPhepForm_SAA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SAA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm_SAA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepManageForm_SAA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepForm_SMA()
        {

            BaseForm f = new VNACC_GiayPhepForm_SMA();
            f.MdiParent = this;
            f.Show();
        }

        private void show_VNACC_GiayPhepManageForm_SMA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_GiayPhepManageForm_SMA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_GiayPhepManageForm_SMA();
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        #region Chung tu thanh toan

        private void Show_VNACC_CTTT_IAS()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuThanhToanForm_IAS"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuThanhToanForm_IAS();
            f.MdiParent = this;
            f.Show();
        }

        private void Show_VNACC_CTTT_IBA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuThanhToanForm_IBA"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuThanhToanForm_IBA();
            f.MdiParent = this;
            f.Show();
        }

        private void Show_VNACC_CTTTManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChungTuThanhToanManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            BaseForm f = new VNACC_ChungTuThanhToanManageForm();
            f.MdiParent = this;
            f.Show();
        }

        #endregion

        #region To khai VNACC
        private void showToKhaiNhap()
        {
            //string str = "javascript:selIt('R7','466592','1','#FFFFEB', '1'); javascript:doOpendoc('466592',2,'2');";
            //if (str.Contains("doOpendoc('"))
            //{
            //    string[] split = str.Split(new string[] { "'('" }, StringSplitOptions.None);
            //    MessageBox.Show(split[1]);
            //}
            toKhaiNhap = new VNACC_ToKhaiMauDichNhapForm();
            toKhaiNhap.MdiParent = this;
            toKhaiNhap.Show();
        }

        private void showToKhaiXuat()
        {
            toKhaiXuat = new VNACC_ToKhaiMauDichXuatForm();
            toKhaiXuat.MdiParent = this;
            toKhaiXuat.Show();
        }


        private void showTheoDoiToKhai()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ToKhaiManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiManager = new VNACC_ToKhaiManager();
            toKhaiManager.MdiParent = this;
            toKhaiManager.Show();
        }
        private void showTheoDoiChungTuToKhai()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_VouchersToKhaiManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            VNACC_VouchersToKhaiManager toKhaiManager = new VNACC_VouchersToKhaiManager();
            toKhaiManager.MdiParent = this;
            toKhaiManager.Show();

        }
        private void showToKhaiVC()
        {


            toKhaiVC = new VNACC_ChuyenCuaKhau();
            toKhaiVC.MdiParent = this;
            toKhaiVC.Show();
        }
        private void showTheoDoiToKhaiVC()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_ChuyenCuaKhau_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiVCManager = new VNACC_ChuyenCuaKhau_ManagerForm();
            toKhaiVCManager.MdiParent = this;
            toKhaiVCManager.Show();

        }

        private void showTEA()
        {

            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("KhaiBaoTEA"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            //TEAForm = new VNACC_TEAForm();
            //TEAForm.MdiParent = this;
            //TEAForm.Show();
            VNACC_TEAForm f = new VNACC_TEAForm();
            f.ShowDialog();
        }
        private void showManagerTEA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TEA_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TEAManager = new VNACC_TEA_ManagerForm();
            TEAManager.MdiParent = this;
            TEAManager.Show();
        }
        private void showTIA()
        {
            VNACC_TIAForm f = new VNACC_TIAForm();
            f.ShowDialog();
        }
        private void showManagerTIA()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TIA_ManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TIAManager = new VNACC_TIA_ManagerForm();
            TIAManager.MdiParent = this;
            TIAManager.Show();
        }
        #endregion

        #region To khai tri gia thap
        private void showToKhaiNhapTriGia()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiNhapTriGiaThap"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiNhapTriGia = new VNACC_TKNhapTriGiaForm();
            toKhaiNhapTriGia.MdiParent = this;
            toKhaiNhapTriGia.Show();
        }
        private void showToKhaiXuatTriGia()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiXuat"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiXuatTriGia = new VNACC_TKXuatTriGiaForm();
            toKhaiXuatTriGia.MdiParent = this;
            toKhaiXuatTriGia.Show();
        }
        private void showTheoDoiToKhaiTriGia()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("VNACC_TKTriGiaManager"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            toKhaiTriGiaManager = new VNACC_TKTriGiaManager();
            toKhaiTriGiaManager.MdiParent = this;
            toKhaiTriGiaManager.Show();
        }
        #endregion

        public void ShowFormThongBao(object sender, EventArgs e)
        {
            if (!frmThongBao.Visible)
            {
                frmThongBao.TopMost = true;
                frmThongBao.BringToFront();
                frmThongBao.Visible = true;
            }
        }

        private void uiPanel0_SelectedPanelChanged(object sender, Janus.Windows.UI.Dock.PanelActionEventArgs e)
        {

        }

        private void explorerBar1_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Hoa don
                case "KhaiBao":
                    this.StorageAreasProduction();
                    break;
                case "TheoDoi":
                    this.ManagerStorageAreasProduction();
                    break;
                case "cmdPhieuNhapKho":
                    this.PhieuNhapKho();
                    break;
                case "cmdPhieuXuatKho":
                    this.PhieuXuatKho();
                    break;
                case "cmdTheoDoiKho":
                    this.TheoDoiKho();
                    break;
                case "cmdTheoDoiMessage":
                    this.showTheoDoiMessage();
                    break;
            }
        }
        private void showTheoDoiMessage()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyMessage"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            QuanLyMessage f = new QuanLyMessage();
            f.MdiParent = this;
            f.Show();
        }
        private void TheoDoiKho()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_WareHouseManagementForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_WareHouseManagementForm f = new VNACC_WareHouseManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void PhieuXuatKho()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_WareHouseExportForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_WareHouseExportForm f = new VNACC_WareHouseExportForm();
            f.MdiParent = this;
            f.Show();
        }

        private void PhieuNhapKho()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_WareHouseImportForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_WareHouseImportForm f = new VNACC_WareHouseImportForm();
            f.MdiParent = this;
            f.Show();
        }
        private void StorageAreasProduction()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("ManufactureFactories"))
                {
                    form[i].Activate();
                    return;
                }
            }

            ManufactureFactories f = new ManufactureFactories();
            f.MdiParent = this;
            f.Show();
        }
        private void ManagerStorageAreasProduction()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("StorageAreasProductionManagerForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            StorageAreasProductionManagerForm f = new StorageAreasProductionManagerForm();
            f.MdiParent = this;
            f.Show();
        }

        private void explorerBarBCTongHopDuLieu_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Tờ khai VNACCS
                case "ToKhaiXuatNhapKhau":
                    this.ThongKeToKhaiXNK();
                    break;
                case "ToKhaiAMA":
                    this.ThongKeToKhaiAMA();
                    break;
                case "TongHangHoaXNK":
                    this.BaoCaoTongHopHHXNK();
                    break;
                case "ChiTietHangHoaXNK":
                    this.BaoCaoChiTietHHXNK();
                    break;
                // Báo cáo tờ khai VNACCS
                case "TongHopXNT":
                    this.BaoCaoTongHopXNT();
                    break;
                case "QuyDoiNPLSP":
                    this.BaoCaoTongHopQuyDoiNPLSP();
                    break;

                //Tờ khai mậu dịch V4
                case "ThongKeTKXNK":
                    this.ThongKeToKhaiXNKV4();
                    break;
                case "BaoCaoTongHopHHXNK":
                    this.BaoCaoTongHopHHXNKV4();
                    break;
                case "BaoCaoChiTietHHXNK":
                    this.BaoCaoChiTietHHXNKV4();
                    break;


            }
        }
        private void BaoCaoChiTietHHXNKV4()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("V4_BaoCaoChiTietHHXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            V4_BaoCaoChiTietHHXNKForm f = new V4_BaoCaoChiTietHHXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTongHopHHXNKV4()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("V4_BaoCaoTongHopHHXNK"))
                {
                    form[i].Activate();
                    return;
                }
            }

            V4_BaoCaoTongHopHHXNK f = new V4_BaoCaoTongHopHHXNK();
            f.MdiParent = this;
            f.Show();
        }

        private void ThongKeToKhaiXNKV4()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("V4_ThongKeTKXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            V4_ThongKeTKXNKForm f = new V4_ThongKeTKXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTongHopQuyDoiNPLSP()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_BaoCaoTongHopQuyDoiNPLSPForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_BaoCaoTongHopQuyDoiNPLSPForm f = new VNACC_BaoCaoTongHopQuyDoiNPLSPForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTongHopXNT()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_BaoCaoTongHopXNTForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_BaoCaoTongHopXNTForm f = new VNACC_BaoCaoTongHopXNTForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoChiTietHHXNK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_BaoCaoChiTietHHXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_BaoCaoChiTietHHXNKForm f = new VNACC_BaoCaoChiTietHHXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTongHopHHXNK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_BaoCaoTongHHXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_BaoCaoTongHHXNKForm f = new VNACC_BaoCaoTongHHXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ThongKeToKhaiAMA()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_ThongKeToKhaiAMAForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_ThongKeToKhaiAMAForm f = new VNACC_ThongKeToKhaiAMAForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ThongKeToKhaiXNK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("VNACC_ThongKeToKhaiXNKForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            VNACC_ThongKeToKhaiXNKForm f = new VNACC_ThongKeToKhaiXNKForm();
            f.MdiParent = this;
            f.Show();
        }

        private void explorerBarKhoKeToan_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                // Cấu hình
                case "cmdCauHinh":
                    this.CauHinhKhoKeToan();
                    break;
                // Danh sách kho 
                case "cmdDanhSachKho":
                    this.DanhSachKhoKeToan();
                    break;
                case "cmdTheoDoiKho":
                    this.TheoDoiKhoKeToan();
                    break;
                // Danh mục kho
                case "cmdKhoNPL":
                    this.KhoNPL();
                    break;
                case "cmdKhoSanPham":
                    this.KhoSanPham();
                    break;
                case "cmdKhoThietBi":
                    this.KhoThietBi();
                    break;
                case "cmdKhoHangMau":
                    this.KhoHangMau();
                    break;
                // Phiếu xuất nhập kho
                case "cmdPhieuNhapKho":
                    this.PhieuNhapKhoKeToan();
                    break;
                case "cmdTheoDoiPNK":
                    this.TheoDoiPNK();
                    break;
                case "cmdPhieuXuatKho":
                    this.PhieuXuatKhoKeToan();
                    break;
                case "cmdTheoDoiPXK":
                    this.cmdTheoDoiPXK();
                    break;
                // Định mức
                case "cmdTheoDoiDinhMuc":
                    this.TheoDoiDM();
                    break;
                case "cmdThemMoiDM":
                    this.ThemMoiDM();
                    break;
                case "cmdTinhToan":
                    this.TinhToanDinhMucSPNPL();
                    break;

                // Báo cáo 
                case "cmdTheKho":
                    this.BaoCaoTheKho();
                    break;
                case "cmdBaoCaoQT":
                    this.BaoCaoQuyetToan();
                    break;
                case "cmdCapNhatTonDK":
                    this.CapNhatTonDauKy();
                    break;

            }
        }
        private void CapNhatTonDauKy()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseUpdateQuantityForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseUpdateQuantityForm f = new WareHouseUpdateQuantityForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoQuyetToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseSettlementReportForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseSettlementReportForm f = new WareHouseSettlementReportForm();
            f.MdiParent = this;
            f.Show();
        }

        private void BaoCaoTheKho()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WarehouseCardReportForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WarehouseCardReportForm f = new WarehouseCardReportForm();
            f.MdiParent = this;
            f.Show();
        }

        private void TinhToanDinhMucSPNPL()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseCalculateSampleDataForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseCalculateSampleDataForm f = new WareHouseCalculateSampleDataForm();
            f.MdiParent = this;
            f.Show();
        }

        private void TheoDoiDM()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseDinhMucManagementForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseDinhMucManagementForm f = new WareHouseDinhMucManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ThemMoiDM()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseDinhMucMaper"))
                {
                    form[i].Activate();
                    return;
                }
            }
            WareHouseDinhMucMaper frm = new WareHouseDinhMucMaper();
            frm.MdiParent = this;
            frm.Show();

        }

        private void CauHinhKhoKeToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("ConfigPrefix"))
                {
                    form[i].Activate();
                    return;
                }
            }

            ConfigPrefix f = new ConfigPrefix();
            f.MdiParent = this;
            f.Show();
        }

        private void DanhSachKhoKeToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseRegisterForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseRegisterForm f = new WareHouseRegisterForm();
            f.MdiParent = this;
            f.Show();
        }

        private void TheoDoiKhoKeToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseRegisterManagementForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseRegisterManagementForm f = new WareHouseRegisterManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void KhoNPL()
        {


            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseNPLMaperManagement"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseNPLMaperManagement f = new WareHouseNPLMaperManagement();
            f.MdiParent = this;
            f.Show();
        }

        private void KhoSanPham()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseSPMaperManagementForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseSPMaperManagementForm f = new WareHouseSPMaperManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void KhoThietBi()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseNPLMaperManagement"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseNPLMaperManagement f = new WareHouseNPLMaperManagement();
            f.MdiParent = this;
            f.Show();
        }

        private void KhoHangMau()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseNPLMaperManagement"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseNPLMaperManagement f = new WareHouseNPLMaperManagement();
            f.MdiParent = this;
            f.Show();
        }

        private void PhieuNhapKhoKeToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseExportForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseExportForm frm = new WareHouseExportForm();
            frm.MdiParent = this;
            frm.LOAICT = "N";
            frm.Show();
        }

        private void TheoDoiPNK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseManager"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseManager f = new WareHouseManager();
            f.MdiParent = this;
            f.Show();
        }

        private void PhieuXuatKhoKeToan()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseExportForm"))
                {
                    form[i].Activate();
                    return;
                }
            }

            WareHouseExportForm frm = new WareHouseExportForm();
            frm.MdiParent = this;
            frm.LOAICT = "X";
            frm.Show();
        }

        private void cmdTheoDoiPXK()
        {
            Form[] form = this.MdiChildren;
            for (int i = 0; i < form.Length; i++)
            {
                if (form[i].Name.ToString().Equals("WareHouseManager"))
                {
                    form[i].Activate();
                    return;
                }
            }
            WareHouseManager f = new WareHouseManager();
            f.MdiParent = this;
            f.Show();
        }
    }
}
