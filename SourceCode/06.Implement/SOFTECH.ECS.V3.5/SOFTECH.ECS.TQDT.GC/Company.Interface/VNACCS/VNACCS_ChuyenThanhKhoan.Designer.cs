﻿namespace Company.Interface.VNACCS
{
    partial class VNACCS_ChuyenThanhKhoan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACCS_ChuyenThanhKhoan));
            this.btnThucHien = new Janus.Windows.EditControls.UIButton();
            this.btnDong = new Janus.Windows.EditControls.UIButton();
            this.uiProgressBar1 = new Janus.Windows.EditControls.UIProgressBar();
            this.lblError = new System.Windows.Forms.Label();
            this.checkAMAIsUpdate = new System.Windows.Forms.CheckBox();
            this.checkVNACCS = new System.Windows.Forms.CheckBox();
            this.checkVNACCSValid = new System.Windows.Forms.CheckBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1062, 493);
            // 
            // btnThucHien
            // 
            this.btnThucHien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThucHien.Image = ((System.Drawing.Image)(resources.GetObject("btnThucHien.Image")));
            this.btnThucHien.Location = new System.Drawing.Point(854, 47);
            this.btnThucHien.Name = "btnThucHien";
            this.btnThucHien.Size = new System.Drawing.Size(99, 23);
            this.btnThucHien.TabIndex = 4;
            this.btnThucHien.Text = "Thực hiện";
            this.btnThucHien.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnThucHien.Click += new System.EventHandler(this.btnThucHien_Click);
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDong.Image = ((System.Drawing.Image)(resources.GetObject("btnDong.Image")));
            this.btnDong.Location = new System.Drawing.Point(959, 47);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(80, 23);
            this.btnDong.TabIndex = 4;
            this.btnDong.Text = "Đóng";
            this.btnDong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // uiProgressBar1
            // 
            this.uiProgressBar1.Location = new System.Drawing.Point(10, 50);
            this.uiProgressBar1.Name = "uiProgressBar1";
            this.uiProgressBar1.Size = new System.Drawing.Size(539, 23);
            this.uiProgressBar1.TabIndex = 5;
            this.uiProgressBar1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.BackColor = System.Drawing.Color.Transparent;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(562, 54);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(35, 13);
            this.lblError.TabIndex = 6;
            this.lblError.Text = "label1";
            // 
            // checkAMAIsUpdate
            // 
            this.checkAMAIsUpdate.AutoSize = true;
            this.checkAMAIsUpdate.BackColor = System.Drawing.Color.Transparent;
            this.checkAMAIsUpdate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkAMAIsUpdate.Location = new System.Drawing.Point(557, 23);
            this.checkAMAIsUpdate.Name = "checkAMAIsUpdate";
            this.checkAMAIsUpdate.Size = new System.Drawing.Size(201, 17);
            this.checkAMAIsUpdate.TabIndex = 7;
            this.checkAMAIsUpdate.Text = "Hiển thị các tờ khai đã cập nhật AMA";
            this.checkAMAIsUpdate.UseVisualStyleBackColor = false;
            this.checkAMAIsUpdate.CheckedChanged += new System.EventHandler(this.checkAMAIsUpdate_CheckedChanged);
            // 
            // checkVNACCS
            // 
            this.checkVNACCS.AutoSize = true;
            this.checkVNACCS.BackColor = System.Drawing.Color.Transparent;
            this.checkVNACCS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkVNACCS.Location = new System.Drawing.Point(359, 23);
            this.checkVNACCS.Name = "checkVNACCS";
            this.checkVNACCS.Size = new System.Drawing.Size(159, 17);
            this.checkVNACCS.TabIndex = 7;
            this.checkVNACCS.Text = "Hiển thị các tờ khai VNACCS";
            this.checkVNACCS.UseVisualStyleBackColor = false;
            this.checkVNACCS.CheckedChanged += new System.EventHandler(this.checkVNACCS_CheckedChanged);
            // 
            // checkVNACCSValid
            // 
            this.checkVNACCSValid.AutoSize = true;
            this.checkVNACCSValid.BackColor = System.Drawing.Color.Transparent;
            this.checkVNACCSValid.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkVNACCSValid.Location = new System.Drawing.Point(10, 23);
            this.checkVNACCSValid.Name = "checkVNACCSValid";
            this.checkVNACCSValid.Size = new System.Drawing.Size(296, 17);
            this.checkVNACCSValid.TabIndex = 7;
            this.checkVNACCSValid.Text = "Hiển thị các tờ khai VNACCS sai lệch số liệu với các TK V4";
            this.checkVNACCSValid.UseVisualStyleBackColor = false;
            this.checkVNACCSValid.CheckedChanged += new System.EventHandler(this.checkVNACCSValid_CheckedChanged);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.checkAMAIsUpdate);
            this.uiGroupBox1.Controls.Add(this.checkVNACCSValid);
            this.uiGroupBox1.Controls.Add(this.btnThucHien);
            this.uiGroupBox1.Controls.Add(this.checkVNACCS);
            this.uiGroupBox1.Controls.Add(this.btnDong);
            this.uiGroupBox1.Controls.Add(this.uiProgressBar1);
            this.uiGroupBox1.Controls.Add(this.lblError);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 407);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1062, 86);
            this.uiGroupBox1.TabIndex = 8;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.grList);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1062, 407);
            this.uiGroupBox3.TabIndex = 9;
            this.uiGroupBox3.Text = "Danh sách tờ chưa chuyển thanh khoản";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic;
            this.grList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Hierarchical = true;
            this.grList.Location = new System.Drawing.Point(3, 17);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(1056, 387);
            this.grList.TabIndex = 0;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grList_LoadingRow);
            // 
            // VNACCS_ChuyenThanhKhoan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 493);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACCS_ChuyenThanhKhoan";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cập nhật chuyển thanh khoản";
            this.Load += new System.EventHandler(this.VNACCS_ChuyenThanhKhoan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnThucHien;
        private Janus.Windows.EditControls.UIButton btnDong;
        private System.Windows.Forms.Label lblError;
        private Janus.Windows.EditControls.UIProgressBar uiProgressBar1;
        private System.Windows.Forms.CheckBox checkAMAIsUpdate;
        private System.Windows.Forms.CheckBox checkVNACCS;
        private System.Windows.Forms.CheckBox checkVNACCSValid;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX grList;
    }
}