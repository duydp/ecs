﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class ChungTuKem
    {
        #region Properties.

        public long ID { set; get; }
        public string SO_CT { set; get; }
        public DateTime NGAY_CT { set; get; }
        public string MA_LOAI_CT { set; get; }
        public string DIENGIAI { set; get; }
        public string LoaiKB { set; get; }
        public string TrangThaiXuLy { set; get; }
        public decimal Tempt { set; get; }
        public string MessageID { set; get; }
        public string GUIDSTR { set; get; }
        public string KDT_WAITING { set; get; }
        public string KDT_LASTINFO { set; get; }
        public decimal SOTN { set; get; }
        public DateTime NGAYTN { set; get; }
        public decimal TotalSize { set; get; }
        public string Phanluong { set; get; }
        public string HuongDan { set; get; }
        public long TKMDID { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<ChungTuKem> ConvertToCollection(IDataReader reader)
        {
            IList<ChungTuKem> collection = new List<ChungTuKem>();
            while (reader.Read())
            {
                ChungTuKem entity = new ChungTuKem();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SO_CT"))) entity.SO_CT = reader.GetString(reader.GetOrdinal("SO_CT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_CT"))) entity.NGAY_CT = reader.GetDateTime(reader.GetOrdinal("NGAY_CT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_LOAI_CT"))) entity.MA_LOAI_CT = reader.GetString(reader.GetOrdinal("MA_LOAI_CT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DIENGIAI"))) entity.DIENGIAI = reader.GetString(reader.GetOrdinal("DIENGIAI"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetString(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("Tempt"))) entity.Tempt = reader.GetDecimal(reader.GetOrdinal("Tempt"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageID"))) entity.MessageID = reader.GetString(reader.GetOrdinal("MessageID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("KDT_WAITING"))) entity.KDT_WAITING = reader.GetString(reader.GetOrdinal("KDT_WAITING"));
                if (!reader.IsDBNull(reader.GetOrdinal("KDT_LASTINFO"))) entity.KDT_LASTINFO = reader.GetString(reader.GetOrdinal("KDT_LASTINFO"));
                if (!reader.IsDBNull(reader.GetOrdinal("SOTN"))) entity.SOTN = reader.GetDecimal(reader.GetOrdinal("SOTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAYTN"))) entity.NGAYTN = reader.GetDateTime(reader.GetOrdinal("NGAYTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("TotalSize"))) entity.TotalSize = reader.GetDecimal(reader.GetOrdinal("TotalSize"));
                if (!reader.IsDBNull(reader.GetOrdinal("Phanluong"))) entity.Phanluong = reader.GetString(reader.GetOrdinal("Phanluong"));
                if (!reader.IsDBNull(reader.GetOrdinal("HuongDan"))) entity.HuongDan = reader.GetString(reader.GetOrdinal("HuongDan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMDID"))) entity.TKMDID = reader.GetInt64(reader.GetOrdinal("TKMDID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static ChungTuKem Load(long id)
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<ChungTuKem> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<ChungTuKem> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<ChungTuKem> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<ChungTuKem> SelectCollectionBy_TKMDID(long tKMDID)
        {
            IDataReader reader = SelectReaderBy_TKMDID(tKMDID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_TKMDID(long tKMDID)
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_SelectBy_TKMDID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, tKMDID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_TKMDID(long tKMDID)
        {
            const string spName = "p_KDT_ChungTuKem_SelectBy_TKMDID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, tKMDID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertChungTuKem(string sO_CT, DateTime nGAY_CT, string mA_LOAI_CT, string dIENGIAI, string loaiKB, string trangThaiXuLy, decimal tempt, string messageID, string gUIDSTR, string kDT_WAITING, string kDT_LASTINFO, decimal sOTN, DateTime nGAYTN, decimal totalSize, string phanluong, string huongDan, long tKMDID)
        {
            ChungTuKem entity = new ChungTuKem();
            entity.SO_CT = sO_CT;
            entity.NGAY_CT = nGAY_CT;
            entity.MA_LOAI_CT = mA_LOAI_CT;
            entity.DIENGIAI = dIENGIAI;
            entity.LoaiKB = loaiKB;
            entity.TrangThaiXuLy = trangThaiXuLy;
            entity.Tempt = tempt;
            entity.MessageID = messageID;
            entity.GUIDSTR = gUIDSTR;
            entity.KDT_WAITING = kDT_WAITING;
            entity.KDT_LASTINFO = kDT_LASTINFO;
            entity.SOTN = sOTN;
            entity.NGAYTN = nGAYTN;
            entity.TotalSize = totalSize;
            entity.Phanluong = phanluong;
            entity.HuongDan = huongDan;
            entity.TKMDID = tKMDID;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, SO_CT);
            db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, NGAY_CT.Year <= 1753 ? DBNull.Value : (object)NGAY_CT);
            db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, MA_LOAI_CT);
            db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.VarChar, DIENGIAI);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.VarChar, LoaiKB);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@Tempt", SqlDbType.Decimal, Tempt);
            db.AddInParameter(dbCommand, "@MessageID", SqlDbType.VarChar, MessageID);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.VarChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@KDT_WAITING", SqlDbType.VarChar, KDT_WAITING);
            db.AddInParameter(dbCommand, "@KDT_LASTINFO", SqlDbType.VarChar, KDT_LASTINFO);
            db.AddInParameter(dbCommand, "@SOTN", SqlDbType.Decimal, SOTN);
            db.AddInParameter(dbCommand, "@NGAYTN", SqlDbType.DateTime, NGAYTN.Year <= 1753 ? DBNull.Value : (object)NGAYTN);
            db.AddInParameter(dbCommand, "@TotalSize", SqlDbType.Decimal, TotalSize);
            db.AddInParameter(dbCommand, "@Phanluong", SqlDbType.NVarChar, Phanluong);
            db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, TKMDID);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<ChungTuKem> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ChungTuKem item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateChungTuKem(long id, string sO_CT, DateTime nGAY_CT, string mA_LOAI_CT, string dIENGIAI, string loaiKB, string trangThaiXuLy, decimal tempt, string messageID, string gUIDSTR, string kDT_WAITING, string kDT_LASTINFO, decimal sOTN, DateTime nGAYTN, decimal totalSize, string phanluong, string huongDan, long tKMDID)
        {
            ChungTuKem entity = new ChungTuKem();
            entity.ID = id;
            entity.SO_CT = sO_CT;
            entity.NGAY_CT = nGAY_CT;
            entity.MA_LOAI_CT = mA_LOAI_CT;
            entity.DIENGIAI = dIENGIAI;
            entity.LoaiKB = loaiKB;
            entity.TrangThaiXuLy = trangThaiXuLy;
            entity.Tempt = tempt;
            entity.MessageID = messageID;
            entity.GUIDSTR = gUIDSTR;
            entity.KDT_WAITING = kDT_WAITING;
            entity.KDT_LASTINFO = kDT_LASTINFO;
            entity.SOTN = sOTN;
            entity.NGAYTN = nGAYTN;
            entity.TotalSize = totalSize;
            entity.Phanluong = phanluong;
            entity.HuongDan = huongDan;
            entity.TKMDID = tKMDID;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_ChungTuKem_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, SO_CT);
            db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, NGAY_CT.Year <= 1753 ? DBNull.Value : (object)NGAY_CT);
            db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, MA_LOAI_CT);
            db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.VarChar, DIENGIAI);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.VarChar, LoaiKB);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@Tempt", SqlDbType.Decimal, Tempt);
            db.AddInParameter(dbCommand, "@MessageID", SqlDbType.VarChar, MessageID);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.VarChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@KDT_WAITING", SqlDbType.VarChar, KDT_WAITING);
            db.AddInParameter(dbCommand, "@KDT_LASTINFO", SqlDbType.VarChar, KDT_LASTINFO);
            db.AddInParameter(dbCommand, "@SOTN", SqlDbType.Decimal, SOTN);
            db.AddInParameter(dbCommand, "@NGAYTN", SqlDbType.DateTime, NGAYTN.Year <= 1753 ? DBNull.Value : (object)NGAYTN);
            db.AddInParameter(dbCommand, "@TotalSize", SqlDbType.Decimal, TotalSize);
            db.AddInParameter(dbCommand, "@Phanluong", SqlDbType.NVarChar, Phanluong);
            db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, TKMDID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<ChungTuKem> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ChungTuKem item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateChungTuKem(long id, string sO_CT, DateTime nGAY_CT, string mA_LOAI_CT, string dIENGIAI, string loaiKB, string trangThaiXuLy, decimal tempt, string messageID, string gUIDSTR, string kDT_WAITING, string kDT_LASTINFO, decimal sOTN, DateTime nGAYTN, decimal totalSize, string phanluong, string huongDan, long tKMDID)
        {
            ChungTuKem entity = new ChungTuKem();
            entity.ID = id;
            entity.SO_CT = sO_CT;
            entity.NGAY_CT = nGAY_CT;
            entity.MA_LOAI_CT = mA_LOAI_CT;
            entity.DIENGIAI = dIENGIAI;
            entity.LoaiKB = loaiKB;
            entity.TrangThaiXuLy = trangThaiXuLy;
            entity.Tempt = tempt;
            entity.MessageID = messageID;
            entity.GUIDSTR = gUIDSTR;
            entity.KDT_WAITING = kDT_WAITING;
            entity.KDT_LASTINFO = kDT_LASTINFO;
            entity.SOTN = sOTN;
            entity.NGAYTN = nGAYTN;
            entity.TotalSize = totalSize;
            entity.Phanluong = phanluong;
            entity.HuongDan = huongDan;
            entity.TKMDID = tKMDID;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, SO_CT);
            db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, NGAY_CT.Year <= 1753 ? DBNull.Value : (object)NGAY_CT);
            db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, MA_LOAI_CT);
            db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.VarChar, DIENGIAI);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.VarChar, LoaiKB);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@Tempt", SqlDbType.Decimal, Tempt);
            db.AddInParameter(dbCommand, "@MessageID", SqlDbType.VarChar, MessageID);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.VarChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@KDT_WAITING", SqlDbType.VarChar, KDT_WAITING);
            db.AddInParameter(dbCommand, "@KDT_LASTINFO", SqlDbType.VarChar, KDT_LASTINFO);
            db.AddInParameter(dbCommand, "@SOTN", SqlDbType.Decimal, SOTN);
            db.AddInParameter(dbCommand, "@NGAYTN", SqlDbType.DateTime, NGAYTN.Year <= 1753 ? DBNull.Value : (object)NGAYTN);
            db.AddInParameter(dbCommand, "@TotalSize", SqlDbType.Decimal, TotalSize);
            db.AddInParameter(dbCommand, "@Phanluong", SqlDbType.NVarChar, Phanluong);
            db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, TKMDID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<ChungTuKem> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ChungTuKem item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteChungTuKem(long id)
        {
            ChungTuKem entity = new ChungTuKem();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMDID(long tKMDID)
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_DeleteBy_TKMDID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, tKMDID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<ChungTuKem> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ChungTuKem item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}