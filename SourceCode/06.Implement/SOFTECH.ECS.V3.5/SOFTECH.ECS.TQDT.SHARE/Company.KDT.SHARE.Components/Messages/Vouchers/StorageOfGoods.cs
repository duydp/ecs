﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.CSSX;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class StorageOfGoods
    {
        /// <summary>
        /// Tên 
        /// </summary>
       [XmlElement("StorageOfGood")]
       public List<StorageOfGood> StorageOfGood { get; set; }
    }
}
