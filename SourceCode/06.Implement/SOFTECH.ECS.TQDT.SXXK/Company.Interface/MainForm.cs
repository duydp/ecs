﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.Interface.KDT.SXXK;
using Company.Interface.SXXK;
using Janus.Windows.ExplorerBar;
using Company.BLL.KDT;
using Company.Interface.SXXK.BangKe;
using Company.Interface.Report.SXXK;
using DevExpress.LookAndFeel;
using Company.BLL.KDT.SXXK;
using Company.Interface.Report;
using Company.Interface.DanhMucChuan;
using System.IO;
using System.Xml.Serialization;
using Company.QuanTri;
using Company.Interface.QuanTri;
using System.Threading;
using System.Globalization;
using Company.Interface.PhongKhai;
using System.Xml;
using System.Security.Cryptography;
using System.Text;

namespace Company.Interface
{
    public partial class MainForm : BaseForm
    {
        public static int flag = 0;
        public static bool isLoginSuccess = false;
        HtmlDocument docHTML = null;
        WebBrowser wbManin = new WebBrowser();
        public static int versionHD = 0;
        public static int typeLogin = 0;
        public int soLanLayTyGia = 0;
        private FrmQuerySQL frmQuery;

        #region SXXK
        private static QueueForm queueForm = new QueueForm();
        // Nguyên phụ liệu.
        private NguyenPhuLieuSendForm nplSendForm;
        //private NguyenPhuLieu_Receive_Form nplReceiveForm;
        private NguyenPhuLieuManageForm nplManageForm;
        private NguyenPhuLieuRegistedForm nplRegistedForm;
        // Sản phẩm.
        //private SanPhamForm spForm;
        private SanPhamSendForm spSendForm;
        //private SanPham_Receive_Form spReceiveForm;
        private SanPhamManageForm spManageForm;
        private SanPhamRegistedForm spRegistedForm;
        private TheoDoiPhanBoTKNhap theodoiPhanBoTKN;

        // Định mức.
        //private DinhMuc_Form dmForm;
        private DinhMucSendForm dmSendForm;
        private DinhMucManageForm dmManageForm;
        private DinhMucRegistedForm dmRegistedForm;
        // Tờ khai mậu dịch.
        private ToKhaiMauDichForm tkmdForm;
        //private ToKhaiMauDichSendForm tkmdSendForm;
        private ToKhaiMauDichManageForm tkmdManageForm;
        private ToKhaiSapHetHanForm tkHetHanForm;
        private ToKhaiQuaHanTKForm tkQuaHanForm;
        private HangTKXuatForm hangTKXForm;
        //Hồ sơ thanh lý
        private TaoHoSoThanhLyForm taoHSTLForm;
        private CapNhatHoSoThanhLyForm capNhatHSTLForm;
        private HSTLDaDongForm hstlDaDongForm;
        private HSTLManageForm hstlManageForm;
        private QuyetDinhToKhaiNhapForm quyetDinhTKNForm;

        private ToKhaiMauDichRegisterForm tkmdRegisterForm;
        private ToKhaiThuocLoaiHinhKhacForm tkLoaiHinhKhacForm;

        private PhanBoTKNForm pbTKNForm;
        private PhanBoTKXForm pbTKXForm;
        private ChiPhiXNKForm chiPhiXNKForm;
        private ThueTonToKhaiNhapForm thueTonTKNForm;
        private TriGiaHangToKhaiXuatForm triGiaHangTKXForm;
        private TriGiaHangToKhaiNhapForm triGiaHangTKNForm;
        private KiemTraDuLieuTKForm ktDuLieuTKForm;
        private KiemTraDuLieuDMForm ktDuLieuDMForm;

        private Company.BLL.SXXK.NguyenPhuLieuCollection tmpNPLCollection = new Company.BLL.SXXK.NguyenPhuLieuCollection();
        private Company.BLL.SXXK.SanPhamCollection tmpSPCollection = new Company.BLL.SXXK.SanPhamCollection();
        private Company.BLL.SXXK.DinhMucCollection tmpDMCollection = new Company.BLL.SXXK.DinhMucCollection();

        #endregion

        //-----------------------------------------------------------------------------------------------

        #region Du lieu Chuan
        private MaHSForm maHSForm;
        private PTTTForm ptttForm;
        private PTVTForm ptvtForm;
        private DonViTinhForm dvtForm;
        private DonViHaiQuanForm dvhqForm;
        private CuaKhauForm ckForm;
        private NguyenTeForm ntForm;
        private DKGHForm dkghForm;
        private NuocForm nuocForm;
        #endregion

        #region Quản trị
        public static ECSPrincipal EcsQuanTri;
        QuanLyNguoiDung NguoiDung;
        QuanLyNhomNguoiDung NhomNguoiDung;
        #endregion Quản trị

        QuanLyMessage quanlyMess;
        private void khoitao_GiaTriMacDinh()
        {

            GlobalSettings.KhoiTao_GiaTriMacDinh();
            if (versionHD == 0)
            {
                string statusStr1 = setText("Người dùng : " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI), "User : " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Company : {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI));
                string statusStr2 = setText(string.Format("Đơn vị hải quan : {0} - {1}. Service khai báo: {2}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN, GlobalSettings.DiaChiWS), string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN));
                statusBar.Panels["DoanhNghiep"].ToolTipText = statusBar.Panels["DoanhNghiep"].Text = statusStr1;
                statusBar.Panels["HaiQuan"].ToolTipText = statusBar.Panels["HaiQuan"].Text = statusStr2;
            }
            else
            {
                //this.Text = "Hệ thống hỗ trợ khai báo từ xa loại hình SXXK ";
                statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                statusBar.Panels["HaiQuan"].Text = statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Đơn vị Hải quan: {0} - {1}. Service khai báo: {2}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN, GlobalSettings.DiaChiWS);
            }

        }
        private void LoadDongBoDuLieuDN()
        {
            LoadDongBoDuLieuForm dongboDNForm = new LoadDongBoDuLieuForm();
            dongboDNForm.ShowDialog();
        }
        void wbManin_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                //docHTML = wbManin.Document;
                //HtmlElement itemTyGia = null;
                //foreach (HtmlElement item in docHTML.GetElementsByTagName("table"))
                //{
                //    if (item.GetAttribute("id") == "AutoNumber6")
                //    {
                //        itemTyGia = item;
                //        break;
                //    }
                //}
                //if (itemTyGia == null)
                //{
                //    MainForm.flag = 0;
                //    return;
                //}
                //int i = 1;
                //string stUSD = "";
                //try
                //{
                //    foreach (HtmlElement itemTD in docHTML.GetElementsByTagName("TD"))
                //    {
                //        if (itemTD.InnerText != null)
                //        {

                //        }
                //        if (itemTD.InnerText != null && itemTD.InnerText.IndexOf("1 USD=") > -1)
                //        {
                //            stUSD = itemTD.InnerText.Trim();
                //        }
                //    }
                //    if (stUSD != "")
                //    {
                //        stUSD = stUSD.Substring(6).Trim();
                //        stUSD = stUSD.Substring(0, stUSD.Length - 3).Trim();
                //        Company.BLL.DuLieuChuan.NguyenTe.Update(null, "USD", Convert.ToDecimal(stUSD));

                //    }
                //}
                //catch { }
                //XmlDocument doc = new XmlDocument();
                //XmlElement root = doc.CreateElement("Root");
                //doc.AppendChild(root);
                //foreach (HtmlElement itemTD in itemTyGia.GetElementsByTagName("TR"))
                //{
                //    if (i == 1)
                //    {
                //        i++;
                //        continue;
                //    }
                //    HtmlElementCollection collection = itemTD.GetElementsByTagName("TD");
                //    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                //    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                //    itemMaNT.InnerText = collection[1].InnerText;

                //    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                //    itemTenNT.InnerText = collection[2].InnerText;

                //    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                //    itemTyNT.InnerText = collection[3].InnerText;

                //    itemNT.AppendChild(itemMaNT);
                //    itemNT.AppendChild(itemTenNT);
                //    itemNT.AppendChild(itemTyNT);
                //    root.AppendChild(itemNT);
                //}
                //Company.BLL.DuLieuChuan.NguyenTe.UpdateTyGia(root);
                //doc.Save("TyGia.xml");
                //timer1.Enabled = false;
            }
            catch { MainForm.flag = 0; }
            //timer1.Enabled = false;
        }
        public static void LoadECSPrincipal(long id)
        {
            EcsQuanTri = new ECSPrincipal(id);
        }
        public static void ShowQueueForm()
        {
            queueForm.Show();
        }
        public static void AddToQueueForm(HangDoi hd)
        {
            queueForm.HDCollection.Add(hd);
            queueForm.RefreshQueue();
        }
        public MainForm()
        {


            //
            // Required for Windows Form Designer support
            //

            InitializeComponent();
            wbManin.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbManin_DocumentCompleted);

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            timer1.Enabled = false;
            timer1.Stop();
        }
        private void ShowLoginForm()
        {
            // this.Hide();
        }
        private string HashCode(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {

                //expSXXK.Groups["grpThanhLy"].Visible = false;

                //Hungtq complemented 14/12/2010
                timer1.Enabled = false; //Disable timer get Exchange Rate

                // UpdateOnline.AutoUpdate.RemotePath = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("RemotePath");
                // UpdateOnline.AutoUpdate.AssemblyName = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("AssemblyName");

                string strVersion = //UpdateOnline.AutoUpdate.ConvertVersion(System.Diagnostics.FileVersionInfo.GetVersionInfo(Application.StartupPath + "\\" + UpdateOnline.AutoUpdate.AssemblyName + ".exe").ProductVersion);
                    System.Diagnostics.FileVersionInfo.GetVersionInfo(Application.ExecutablePath).ProductVersion.ToString();
                this.Text += " - Build " + strVersion;


                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Application.StartupPath + "\\Config.xml");
                    XmlNode node = doc.SelectSingleNode("Root/Type");
                    MainForm.versionHD = Convert.ToInt32(node.InnerText);

                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                GlobalSettings.IsDaiLy = MainForm.versionHD == 1 ? true : false;
                if (MainForm.versionHD == 2)
                {
                    cmdNhapDuLieu.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdXuatDuLieu.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdXuatDuLieuDN.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhapXML.Visible = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdXuatDuLieu.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdNhapDuLieu.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdXuatDuLieuDN.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdNhapXML.Visible = Janus.Windows.UI.InheritableBoolean.True;
                }
                if (GlobalSettings.NGON_NGU == "0")
                {
                    try
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecsv.png");
                        this.BackgroundImageLayout = ImageLayout.Stretch;
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecse.jpg");
                        this.BackgroundImageLayout = ImageLayout.Stretch;
                    }
                    catch { }
                }
                //try
                //{
                //    this.BackgroundImage = System.Drawing.Image.FromFile("ecs.jpg");
                //    this.BackgroundImageLayout = ImageLayout.Stretch;
                //}
                //catch { }
                this.Hide();
                //Login login = new Login();
                //login.ShowDialog();
                if (versionHD == 0)
                {
                    Login login = new Login();
                    login.ShowDialog();
                }
                else if (versionHD == 1)
                {
                    Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                    login.ShowDialog();
                }
                else if (versionHD == 2)
                {
                    Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                    login.ShowDialog();
                }

                if (isLoginSuccess)
                {
                    this.Show();

                    //Hungtq updated 20/12/2011. Cau hinh Ecs Express
                    Express();

                    if (versionHD == 0)
                    {
                        if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                        {
                            QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                            ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmbMenu.Commands[0].Commands[0].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmbMenu.Commands[0].Commands[1].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmbMenu.Commands[0].Commands[2].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[0].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[1].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[2].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[3].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                    }
                    else if (versionHD == 2)
                    {
                        switch (typeLogin)
                        {
                            case 1: // User Da Cau Hinh
                                this.Show();
                                this.khoitao_GiaTriMacDinh();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 2: // User Chua Cau Hinh
                                this.Show();
                                DangKyForm dangKyForm = new DangKyForm();
                                dangKyForm.ShowDialog();
                                this.khoitao_GiaTriMacDinh();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 3:// Admin
                                this.Hide();
                                CreatAccountForm fAdminForm = new CreatAccountForm();
                                fAdminForm.ShowDialog();
                                this.LoginUserKhac();
                                break;
                        }
                    }
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    vsmMain.DefaultColorScheme = GlobalSettings.GiaoDien.Id;
                    if (GlobalSettings.GiaoDien.Id == "Office2007")
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    this.khoitao_GiaTriMacDinh();

                    //Hungtq complemented 14/12/2010
                    timer1.Enabled = true; //Enable timer get Exchange Rate

                    backgroundWorker1.RunWorkerAsync();

                    Show_Leftpanel();

                    #region active mới
                    try
                    {
                        this.requestActivate_new();
                        int dayInt = 7;
                        int dateTrial = KeySecurity.Active.Install.License.KeyInfo.TrialDays;
                        if (!string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Key))
                        {
                            cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            if (dayInt > dateTrial)
                                ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty Cổ phẩn SOFTECH để tiếp tục sử dụng", false);
                            this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");
                        }
                        else
                        {
                            this.Text += setText(" - Bản dùng thử.", " - Trial.");
                        }

                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        ShowMessageTQDT("Thông Báo", "Lỗi khi kiểm tra bản quyền: " + ex.Message, false);
                        Application.ExitThread();
                        Application.Exit();
                    }



                    #endregion

                    #region Active Cũ

                    //int dayInt = 7;
                    //int dateTrial = int.Parse(Program.lic.dateTrial);
                    //try
                    //{
                    //    string day = new Company.KDT.SHARE.Components.Utils.License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ExpiredDate"));
                    //    dayInt = int.Parse(day);
                    //}
                    //catch (Exception) { }
                    //if (Program.isActivated)
                    //{
                    //    cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    //    //CultureInfo vn = new CultureInfo("vi-VN");
                    //    CultureInfo vn = Thread.CurrentThread.CurrentCulture;
                    //    for (int k = 0; k < dayInt; k++)
                    //    {
                    //        //Kiem tra Culture, neu la vi-VN -> chuyen doi ve cung 1 chuan en-US.
                    //        string dateString = DateTime.Now.Date.ToShortDateString();

                    //        if (DateTime.Parse(dateString, vn.DateTimeFormat).AddDays(k) == DateTime.Parse(Program.lic.dayExpires, vn.DateTimeFormat).Date)
                    //        {
                    //            int ngayConLai = k + 1;
                    //            ShowMessage("Phần mềm còn " + ngayConLai + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", false);
                    //            break;
                    //        }
                    //    }
                    //    this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");
                    //}
                    //else
                    //{

                    //    this.Text += setText(" - Bản dùng thử.", " - Trial.");
                        

                    //    if (dateTrial <= 0)
                    //        this.requestActivate();
                    //    else if (dateTrial <= dayInt)
                    //        ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", false);
                    //}
                    #endregion


                    if (GlobalSettings.MA_DON_VI == "" || GlobalSettings.MA_DON_VI == "?")
                    {
                        ShowThietLapThongTinDNAndHQ();
                    }
                    //if (BackUpAndReStoreForm.lastBackUp.Add(TimeSpan.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHAC_NHO_SAO_LUU"))) == DateTime.Now)
                    if (!(GlobalSettings.LastBackup.Equals("")) && !(GlobalSettings.NHAC_NHO_SAO_LUU.Equals("")))
                        if (Convert.ToDateTime(GlobalSettings.LastBackup).Add(TimeSpan.Parse(GlobalSettings.NHAC_NHO_SAO_LUU)) == DateTime.Today)
                        {
                            ShowBackupAndRestore(true);
                        }
                    //LanNT dung cho ban express.
                    //Company.KDT.SHARE.Components.DoanhNghiepCfgs.Load(@"ConfigDoanhNghiep\\ConfigDoanhNghiep.xml");
                    //Company.KDT.SHARE.Components.Config cfg = Company.KDT.SHARE.Components.DoanhNghiepCfgs.Install.Find(cf => cf.Key == "Express");
                    //if (cfg == null ||cfg.Value != "yJSXAArLTPqZ1q1U5L0WOw==")
                    //{
                    //    if (cfg == null)
                    //    {
                    //        Company.KDT.SHARE.Components.DoanhNghiepCfgs.Install.Add(new Company.KDT.SHARE.Components.Config { Key = "Express", Value = "false", Note = "Phiên bản express" });
                    //        Company.KDT.SHARE.Components.DoanhNghiepCfgs.Install.SaveConfig(@"ConfigDoanhNghiep\\ConfigDoanhNghiep.xml");
                    //    }
                    //    foreach (ExplorerBarItem item in  expSXXK.Groups[4].Items)
                    //    {
                    //        item.Enabled = false;
                    //    }

                    //}
                    //else if ()
                    //{
                    //    expSXXK.Groups[3].Items[0].Enabled = false;
                    //}

                }
                else
                    Application.Exit();
                //}
                //else
                //{
                //    ShowMessage("Chương trình đã hết hạn sử dụng. \nLiên hệ với Softech để được cung cấp phiên bản chính thức. \nSố điện thoại: (0511)3.810535 hoặc FAX: (0511)3.810278.\nXin cám ơn quí khách hàng đã sử dụng chương trình của chúng tôi.", false);
                //    Application.Exit();
                //}


            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void requestActivate()
        {
            if (ShowMessage("Đã hết hạn dùng thử.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", true) == "Yes")
            {
                ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
                obj.ShowDialog();
                requestActivate();
            }
            else
            {
                Application.Exit();
            }
        }
        private void Show_Leftpanel()
        {
            //Kiem tra permission nguyen phu lieu
            if (versionHD == 0)
            {
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.XemDuLieu)))
                {
                    this.expSXXK.Groups[0].Enabled = false;
                    this.expSXXK.Groups[0].Expanded = false;
                }
                else
                {
                    this.expSXXK.Groups[0].Enabled = true;
                    this.expSXXK.Groups[0].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.KhaiDienTu)))
                    this.expSXXK.Groups[0].Items[0].Enabled = false;
                else
                    this.expSXXK.Groups[0].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.CapNhatDuLieu)))
                    this.expSXXK.Groups[0].Items[1].Enabled = false;
                else
                    this.expSXXK.Groups[0].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.XemDuLieu)))
                    this.expSXXK.Groups[0].Items[2].Enabled = false;
                else
                    this.expSXXK.Groups[0].Items[2].Enabled = true;
                //Ket thuc kiem tra nguyen phu lieu

                //Kiem tra permission san pham
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.XemDuLieu)))
                {
                    this.expSXXK.Groups[1].Enabled = false;
                    this.expSXXK.Groups[1].Expanded = false;
                }
                else
                {
                    this.expSXXK.Groups[1].Enabled = true;
                    this.expSXXK.Groups[1].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.KhaiDienTu)))
                    this.expSXXK.Groups[1].Items[0].Enabled = false;
                else
                    this.expSXXK.Groups[1].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.CapNhatDuLieu)))
                    this.expSXXK.Groups[1].Items[1].Enabled = false;
                else
                    this.expSXXK.Groups[1].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.XemDuLieu)))
                    this.expSXXK.Groups[1].Items[2].Enabled = false;
                else
                    this.expSXXK.Groups[1].Items[2].Enabled = true;
                //Ket thuc kiem tra san pham

                //Kiem tra permission dinh muc
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.XemDuLieu)))
                {
                    this.expSXXK.Groups[2].Enabled = false;
                    this.expSXXK.Groups[2].Expanded = false;
                }
                else
                {
                    this.expSXXK.Groups[2].Enabled = true;
                    this.expSXXK.Groups[2].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)))
                    this.expSXXK.Groups[2].Items[0].Enabled = false;
                else
                    this.expSXXK.Groups[2].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.CapNhatDuLieu)))
                    this.expSXXK.Groups[2].Items[1].Enabled = false;
                else
                    this.expSXXK.Groups[2].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.XemDuLieu)))
                    this.expSXXK.Groups[2].Items[2].Enabled = false;
                else
                    this.expSXXK.Groups[2].Items[2].Enabled = true;
                //Ket thuc kiem tra dinh muc

                //Kiem tra permission to khai
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap))
                    && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuXuat))
                    && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu))
                    && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.XemDuLieu))
                    && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.ToKhaiSapHetHan))
                    && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CchiPhiXNK)))
                {
                    this.expSXXK.Groups[3].Enabled = false;
                    this.expSXXK.Groups[3].Expanded = false;
                }
                else
                {
                    this.expSXXK.Groups[3].Enabled = true;
                    this.expSXXK.Groups[3].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)))
                    this.expSXXK.Groups[3].Items[0].Enabled = false;
                else
                    this.expSXXK.Groups[3].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuXuat)))
                    this.expSXXK.Groups[3].Items[1].Enabled = false;
                else
                    this.expSXXK.Groups[3].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
                    this.expSXXK.Groups[3].Items[2].Enabled = false;
                else
                    this.expSXXK.Groups[3].Items[2].Enabled = true;

                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.XemDuLieu)))
                    this.expSXXK.Groups[3].Items[3].Enabled = false;
                else
                    this.expSXXK.Groups[3].Items[3].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.ToKhaiSapHetHan)))
                    this.expSXXK.Groups[3].Items[4].Enabled = false;
                else
                    this.expSXXK.Groups[3].Items[4].Enabled = true;

                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CchiPhiXNK)))
                    this.expSXXK.Groups[3].Items[5].Enabled = false;
                else
                    this.expSXXK.Groups[3].Items[5].Enabled = true;
                //Ket thuc kiem tra to khai

                //Kiem tra permission thanh khoan
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleThanhKhoan.ThanhKhoan)))
                {
                    this.expSXXK.Groups[4].Enabled = false;
                    this.expSXXK.Groups[4].Expanded = false;
                }
                else
                {
                    this.expSXXK.Groups[4].Enabled = true;
                    this.expSXXK.Groups[4].Expanded = true;
                }
                //Ket thuc kiem tra thanh khoan

                //Kiem tra permission nguyen phu lieu ton
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTon.TheoDoiNPLTon))
                    && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTon.ThongKeNPLTon)))
                {
                    this.expKhaiBao_TheoDoi.Groups[0].Enabled = false;
                    this.expKhaiBao_TheoDoi.Groups[0].Expanded = false;
                }
                else
                {
                    this.expKhaiBao_TheoDoi.Groups[0].Enabled = true;
                    this.expKhaiBao_TheoDoi.Groups[0].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTon.TheoDoiNPLTon)))
                    this.expKhaiBao_TheoDoi.Groups[0].Items[0].Enabled = false;
                else
                    this.expKhaiBao_TheoDoi.Groups[0].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTon.ThongKeNPLTon)))
                    this.expKhaiBao_TheoDoi.Groups[0].Items[1].Enabled = false;
                else
                    this.expKhaiBao_TheoDoi.Groups[0].Items[1].Enabled = true;
                //Ket thuc kiem tra nguyen phu lieu ton

                //Kiem tra permission to khai nhap tu HT khac
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacNhap))
                    && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacXuat)))
                {
                    this.expKhaiBao_TheoDoi.Groups[1].Enabled = false;
                    this.expKhaiBao_TheoDoi.Groups[1].Expanded = false;
                }
                else
                {
                    this.expKhaiBao_TheoDoi.Groups[1].Enabled = true;
                    this.expKhaiBao_TheoDoi.Groups[1].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacNhap)))
                    this.expKhaiBao_TheoDoi.Groups[1].Items[0].Enabled = false;
                else
                    this.expKhaiBao_TheoDoi.Groups[1].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacXuat)))
                    this.expKhaiBao_TheoDoi.Groups[1].Items[1].Enabled = false;
                else
                    this.expKhaiBao_TheoDoi.Groups[1].Items[1].Enabled = true;
                //Ket thuc kiem tra to khai nhap tu HT khac

                //Kiem tra permission quan ly to khai
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleQuanLyToKhai.ThueTonKhoTKNhap))
                    && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacXuat)))
                {
                    this.expKhaiBao_TheoDoi.Groups[2].Enabled = false;
                    this.expKhaiBao_TheoDoi.Groups[2].Expanded = false;
                }
                else
                {
                    this.expKhaiBao_TheoDoi.Groups[2].Enabled = true;
                    this.expKhaiBao_TheoDoi.Groups[2].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleQuanLyToKhai.ThueTonKhoTKNhap)))
                    this.expKhaiBao_TheoDoi.Groups[2].Items[0].Enabled = false;
                else
                    this.expKhaiBao_TheoDoi.Groups[2].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiHTKhac.ToKhaiHTKhacXuat)))
                    this.expKhaiBao_TheoDoi.Groups[2].Items[1].Enabled = false;
                else
                    this.expKhaiBao_TheoDoi.Groups[2].Items[1].Enabled = true;
                //Ket thuc kiem tra quan ly to khai
            }
        }
        private void expSXXK_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                // Khai báo NPL.
                case "nplKhaiBao":
                    this.show_NguyenPhuLieuSendForm();
                    break;
                // Theo dõi NPL.
                case "nplTheoDoi":
                    this.show_NguyenPhuLieuManageForm();
                    break;
                // NPL đã đăng ký.
                case "nplDaDangKy":
                    this.show_NguyenPhuLieuRegistedForm();
                    break;

                // Khai báo SP.
                case "spKhaiBao":
                    this.show_SanPhamSendForm();
                    break;

                // Theo dõi SP.
                case "spTheoDoi":
                    this.show_SanPhamManageForm();
                    break;

                // SP đã đăng ký.
                case "spDaDangKy":
                    this.show_SanPhamRegistedForm();
                    break;

                // Khai báo ĐM.
                case "dmKhaiBao":
                    this.show_DinhMucSendForm();
                    break;
                // Theo dõi ĐM.
                case "dmTheoDoi":
                    this.show_DinhMucManageForm();
                    break;
                // ĐM đã đăng ký
                case "dmDaDangKy":
                    this.show_DinhMucRegistedForm();
                    break;

                // Tờ khai mậu dịch.
                case "tkNhapKhau":
                    this.show_ToKhaiMauDichForm("NSX");
                    break;
                case "tkXuatKhau":
                    this.show_ToKhaiMauDichForm("XSX");
                    break;

                case "TheoDoiTKSXXK":
                    show_ToKhaiSXXKManage();
                    break;
                case "ToKhaiSXXKDangKy":
                    show_ToKhaiSXXKDangKy();
                    break;
                case "tkHetHan":
                    show_ToKhaiHetHan();
                    break;
                //Tờ khai quá hạn thanh khoản
                case "tkNopThue":
                    show_ToKhaiQuaHanTK();
                    break;
                case "TKHangTKX":
                    show_HangTKX();
                    break;

                //Hồ sơ thanh lý
                case "AddHSTL":
                    this.show_TaoHoSoThanhLyForm();
                    break;
                case "UpdateHSTL":
                    HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
                    int id = HSTL.getHSTLMoiNhat(EcsQuanTri.Identity.Name, GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                    if (id == 0)
                    {
                        this.show_TaoHoSoThanhLyForm();
                    }
                    else
                    {
                        HSTL.ID = id;
                        HSTL.Load();
                        HSTL.LoadBKCollection();
                        this.show_CapNhatHoSoThanhLyForm(HSTL);
                    }
                    break;
                case "HSTLManage":
                    show_HSTLManageForm();
                    break;
                case "HSTLClosed":
                    show_HSTLDaDongForm();
                    break;
                case "ChiPhiXNK":
                    show_ChiPhiXNKForm();
                    break;
                case "QDThanhKhoanTKN":
                    show_QDThanhThoanTKN();
                    break;
            }
        }
        private void show_QDThanhThoanTKN()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuyetDinhToKhaiNhapForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            quyetDinhTKNForm = new QuyetDinhToKhaiNhapForm();
            quyetDinhTKNForm.MdiParent = this;
            quyetDinhTKNForm.Show();
        }
        private void show_ChiPhiXNKForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ChiPhiXNKForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            chiPhiXNKForm = new ChiPhiXNKForm();
            chiPhiXNKForm.MdiParent = this;
            chiPhiXNKForm.Show();
        }

        private void show_ToKhaiHetHan()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiSapHetHanForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkHetHanForm = new ToKhaiSapHetHanForm();
            tkHetHanForm.MdiParent = this;
            tkHetHanForm.Show();
        }

        private void show_ToKhaiQuaHanTK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiQuaHanTKForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkQuaHanForm = new ToKhaiQuaHanTKForm();
            tkQuaHanForm.MdiParent = this;
            tkQuaHanForm.Show();
        }
        private void show_HangTKX()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HangTKXuatForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hangTKXForm = new HangTKXuatForm();
            hangTKXForm.MdiParent = this;
            hangTKXForm.Show();
        }

        private void show_HSTLManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HSTLManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hstlManageForm = new HSTLManageForm();
            hstlManageForm.MdiParent = this;
            hstlManageForm.Show();
        }

        private void show_ToKhaiSXXKDangKy()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new ToKhaiMauDichRegisterForm();
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.NhomLoaiHinh = "SX";
            tkmdRegisterForm.Text = "Tờ khai SXXK đã đăng ký";
            tkmdRegisterForm.Show();
        }

        private void show_ToKhaiSXXKManage()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.nhomLoaiHinh = "SX";
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Text = "Theo dõi tờ khai SXXK";
            tkmdManageForm.Show();
        }

        private void ShowBKTKX()
        {
        }

        private void ShowBKTKN()
        {
        }

        private void ShowBCThueXNK()
        {
            Company.Interface.Report.SXXK.BCThueXNK bc = new Company.Interface.Report.SXXK.BCThueXNK();
            bc.BindReport(GlobalSettings.MA_HAI_QUAN);
            bc.ShowPreview();
        }

        private void ShowBCNPLXuatNhapTon()
        {
            BCNPLXuatNhapTon bc = new BCNPLXuatNhapTon();
            bc.BindReport(GlobalSettings.MA_HAI_QUAN);
            bc.ShowPreview();
        }

        private void show_TaoHoSoThanhLyForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TaoHoSoThanhLyForm") || forms[i].Name.ToString().Equals("BK02WizardForm") || forms[i].Name.ToString().Equals("BK01WizardForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            taoHSTLForm = new TaoHoSoThanhLyForm();
            taoHSTLForm.MdiParent = this;
            taoHSTLForm.Show();
        }
        private void show_CapNhatHoSoThanhLyForm(HoSoThanhLyDangKy HSTL)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CapNhatHoSoThanhLyForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            capNhatHSTLForm = new CapNhatHoSoThanhLyForm();
            capNhatHSTLForm.HSTL = HSTL;
            capNhatHSTLForm.MdiParent = this;
            capNhatHSTLForm.Show();
        }

        private void show_HSTLDaDongForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HSTLDaDongForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hstlDaDongForm = new HSTLDaDongForm();
            hstlDaDongForm.MdiParent = this;
            hstlDaDongForm.Show();
        }
        //private void uiButton1_Click(object sender, EventArgs e)
        //{
        //    new NguyenPhuLieu_Form().ShowDialog();
        //}

        //private void uiButton2_Click(object sender, EventArgs e)
        //{
        //    new SanPham_Form().ShowDialog();
        //}

        //private void explorerBar1_ItemClick(object sender, ItemEventArgs e)
        //{
        //    this.Cursor = Cursors.WaitCursor;
        //    switch (e.Item.Key)
        //    {
        //            // Nguyên phụ liệu.
        //        case "nplNhap":
        //            this.show_NguyenPhuLieu_Form();
        //            break;
        //        case "nplSend":
        //            this.show_NguyenPhuLieu_Send_Form();
        //            break;
        //        case "nplManage":
        //            this.show_NguyenPhuLieu_Manage_Form();
        //            break;

        //            // Sản phẩm.
        //        case "spNhap":
        //            this.show_SanPham_Form();
        //            break;
        //        case "spSend":
        //            this.show_SanPham_Send_Form();
        //            break;
        //        case "spManage":
        //            this.show_SanPham_Manage_Form();
        //            break;

        //            // Định mức.
        //        case "dmNhap":
        //            this.show_DinhMuc_Form();
        //            break;
        //        case "dmSend":
        //            this.show_DinhMuc_Send_Form();
        //            break;
        //        case "dmManage":
        //            this.show_DinhMuc_Manage_Form();
        //            break;

        //            // Tờ khai mậu dịch.
        //        case "tkNhapKhau_SXXK":
        //            this.show_ToKhaiNhapKhauSXXK_Form();
        //            break;

        //        case "tkSend_SXXK":
        //            this.show_ToKhaiMauDich_Send_Form();
        //            break;

        //        case "tkManage_SXXK":
        //            this.show_ToKhaiMauDich_Manage_Form();					
        //            break;

        //    }
        //    this.Cursor = Cursors.Default;
        //}


        //-----------------------------------------------------------------------------------------
        private void show_NguyenPhuLieuSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenPhuLieuSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplSendForm = new NguyenPhuLieuSendForm();
            nplSendForm.MdiParent = this;
            nplSendForm.Show();
        }

        //-----------------------------------------------------------------------------------------------
        private void show_NguyenPhuLieuManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenPhuLieuManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplManageForm = new NguyenPhuLieuManageForm();
            nplManageForm.MdiParent = this;
            nplManageForm.Show();
        }
        //-----------------------------------------------------------------------------------------------
        private void show_NguyenPhuLieuRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenPhuLieuRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplRegistedForm = new NguyenPhuLieuRegistedForm();
            nplRegistedForm.MdiParent = this;
            nplRegistedForm.Show();
        }

        ////-----------------------------------------------------------------------------------------------
        private void show_SanPhamManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SanPhamManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            spManageForm = new SanPhamManageForm();
            spManageForm.MdiParent = this;
            spManageForm.Show();

        }

        //-----------------------------------------------------------------------------------------------
        private void show_TheoDoiPhanBoTKNForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TheoDoiPhanBoTKNhap"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            theodoiPhanBoTKN = new TheoDoiPhanBoTKNhap();
            theodoiPhanBoTKN.MdiParent = this;
            theodoiPhanBoTKN.Show();
        }
        //
        private void show_SanPhamRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SanPhamRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            spRegistedForm = new SanPhamRegistedForm();
            spRegistedForm.MdiParent = this;
            spRegistedForm.Show();
        }

        ////-----------------------------------------------------------------------------------------
        private void show_SanPhamSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SanPhamSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            spSendForm = new SanPhamSendForm();
            spSendForm.MdiParent = this;
            spSendForm.Show();

        }

        ////-----------------------------------------------------------------------------------------

        //private void show_DinhMuc_Form()
        //{
        //    Form[] forms = this.MdiChildren;
        //    for (int i = 0; i < forms.Length; i++)
        //    {
        //        if (forms[i].Name.ToString().Equals("DinhMuc_Form"))
        //        {
        //            forms[i].Activate();
        //            return;
        //        }
        //    }
        //    dmForm = new DinhMuc_Form();
        //    dmForm.MdiParent = this;
        //    dmForm.Show();
        //}

        //-----------------------------------------------------------------------------------------
        private void show_DinhMucSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmSendForm = new DinhMucSendForm();
            dmSendForm.MdiParent = this;
            dmSendForm.Show();
        }

        //-----------------------------------------------------------------------------------------------
        private void show_DinhMucManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmManageForm = new DinhMucManageForm();
            dmManageForm.MdiParent = this;
            dmManageForm.Show();

        }
        private void show_DinhMucRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmRegistedForm = new DinhMucRegistedForm();
            dmRegistedForm.MdiParent = this;
            dmRegistedForm.Show();

        }

        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiMauDichForm(string nhomLoaiHinh)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals(nhomLoaiHinh))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdForm = new ToKhaiMauDichForm();
            tkmdForm.OpenType = OpenFormType.Insert;
            // tkmdForm.Name = nhomLoaiHinh;
            tkmdForm.NhomLoaiHinh = nhomLoaiHinh;
            tkmdForm.MdiParent = this;
            tkmdForm.Show();
        }

        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiMauDichSendForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("ToKhaiMauDichSendForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            //tkmdSendForm = new ToKhaiMauDichSendForm();
            //tkmdSendForm.MdiParent = this;
            //tkmdSendForm.Show();
        }

        //-----------------------------------------------------------------------------------------

        private void show_ToKhaiMauDichManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Show();
        }

        private void expKhaiBao_TheoDoi_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "tkSend":
                    this.show_ToKhaiMauDichSendForm();
                    break;
                case "tkManage":
                    this.show_ToKhaiMauDichManageForm();
                    break;
                case "tkDaDangKy":
                    this.show_ToKhaiMauDichRegisterForm();
                    break;
                case "tdNPLton":
                    this.show_HangtonForm();
                    break;
                case "tkNPLton":
                    this.show_ThongkeForm();
                    break;
                case "tkLHKNhap":
                    this.show_ToKhaiThuocLoaiHinhKhacForm("N");
                    break;
                case "tkLHKXuat":
                    this.show_ToKhaiThuocLoaiHinhKhacForm("X");
                    break;
                case "tkNhap":
                    this.show_PhanBoTKNForm();
                    break;
                case "tkXuat":
                    this.show_PhanBoTKXForm();
                    break;
                case "triGiaTKXuat":
                    this.show_TriGiaToKhaiXuatForm();
                    break;
                case "triGiaTKNhap":
                    this.show_TriGiaToKhaiNhapForm();
                    break;
                case "thueTonTKNhap":
                    this.show_ThueTonToKhaiNhapForm();
                    break;

                case "tdPBTKN":
                    this.show_TheoDoiPhanBoTKNForm();
                    break;
                case "itemKTToKhai":
                    this.show_KiemTraDuLieuTKForm();
                    break;

                case "itemKTDinhMuc":
                    this.show_KiemTraDuLieuDinhMucForm();
                    break;
            }
        }

        private void show_KiemTraDuLieuTKForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("KiemTraDuLieuTKForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ktDuLieuTKForm = new KiemTraDuLieuTKForm();
            ktDuLieuTKForm.MdiParent = this;
            ktDuLieuTKForm.Show();
        }

        private void show_KiemTraDuLieuDinhMucForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("KiemTraDuLieuDMForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ktDuLieuDMForm = new KiemTraDuLieuDMForm();
            ktDuLieuDMForm.MdiParent = this;
            ktDuLieuDMForm.Show();
        }

        private void show_ThueTonToKhaiNhapForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThueTonToKhaiNhapForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            thueTonTKNForm = new ThueTonToKhaiNhapForm();
            thueTonTKNForm.MdiParent = this;
            thueTonTKNForm.Show();
        }

        private void show_TriGiaToKhaiXuatForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TriGiaHangToKhaiXuatForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            triGiaHangTKXForm = new TriGiaHangToKhaiXuatForm();
            triGiaHangTKXForm.MdiParent = this;
            triGiaHangTKXForm.Show();
        }
        private void show_TriGiaToKhaiNhapForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TriGiaHangToKhaiNhapForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            triGiaHangTKNForm = new TriGiaHangToKhaiNhapForm();
            triGiaHangTKNForm.MdiParent = this;
            triGiaHangTKNForm.Show();
        }
        private void show_PhanBoTKXForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("PhanBoTKXForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            pbTKXForm = new PhanBoTKXForm();
            //pbTKXForm.MdiParent = this;
            pbTKXForm.Show();
        }

        private void show_PhanBoTKNForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("PhanBoTKNForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            pbTKNForm = new PhanBoTKNForm();
            //pbTKNForm.MdiParent = this;
            pbTKNForm.Show();
        }

        private void expKD_ItemClick(object sender, ItemEventArgs e)
        {

        }

        private void expDT_ItemClick(object sender, ItemEventArgs e)
        {

        }

        private void expGiaCong_ItemClick(object sender, ItemEventArgs e)
        {

        }

        private void show_ToKhaiThuocLoaiHinhKhacForm(string nhomLoaiHinh)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals(nhomLoaiHinh))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkLoaiHinhKhacForm = new ToKhaiThuocLoaiHinhKhacForm();
            tkLoaiHinhKhacForm.NhomLoaiHinh = nhomLoaiHinh;
            // tkLoaiHinhKhacForm.Name = nhomLoaiHinh;
            tkLoaiHinhKhacForm.OpenType = OpenFormType.Insert;
            tkLoaiHinhKhacForm.MdiParent = this;
            tkLoaiHinhKhacForm.Show();


        }

        private void show_ToKhaiMauDichRegisterForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new ToKhaiMauDichRegisterForm();
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.Show();
        }

        //-----------------------------------------------------------------------------------------
        //hangton
        private void show_HangtonForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HangTon"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            HangTon HangtonForm = new HangTon();
            HangtonForm.MdiParent = this;
            HangtonForm.Show();
        }

        private void show_ThongkeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongkeHangTonForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ThongkeHangTonForm thongkeHangtonForm = new ThongkeHangTonForm();
            thongkeHangtonForm.MdiParent = this;
            thongkeHangtonForm.Show();
        }

        private void pnlMain_SelectedPanelChanged(object sender, Janus.Windows.UI.Dock.PanelActionEventArgs e)
        {

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (queueForm.HDCollection.Count > 0)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(HangDoiCollection));
                FileStream fs = new FileStream("HangDoi.xml", FileMode.Create);
                serializer.Serialize(fs, queueForm.HDCollection);
            }
            queueForm.Dispose();
            //Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (MainForm.flag == 0 && soLanLayTyGia <= 3)
            {
                soLanLayTyGia++;

                if (!backgroundWorker1.IsBusy)
                    backgroundWorker1.RunWorkerAsync();
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                //Check new version
                //  CheckNewVersion();
                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdDataVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != "" ? dataVersion : "?"));
                RunUpdate(string.Empty);
                Company.BLL.SXXK.ToKhai.ToKhaiMauDich tokhai = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                NhanNhoForm f = new NhanNhoForm();
                uint hannhacnho = Convert.ToUInt32(System.Configuration.ConfigurationManager.AppSettings["HanThanhKhoan"]);
                uint thoigianTK = Convert.ToUInt32(GlobalSettings.ThongBaoHetHan);
                System.Data.DataSet ds = tokhai.GetThongBao(hannhacnho, thoigianTK);
                if (ds == null || ds.Tables[0].Rows.Count == 0)
                {
                }
                else
                {
                    queueForm.notifyIcon1.ShowBalloonTip(5000, "Thông báo ", "Có " + ds.Tables[0].Rows.Count + " tờ khai sắp hết hạn chưa được thanh khoản.\nVào phần nhắc nhở để xem chi tiết.", ToolTipIcon.Warning);
                    //f.Show();                   
                }

                long sotk = ToKhaiMauDich.SelectCountSoTK(GlobalSettings.MA_DON_VI);
                if (sotk % 10 == 0 && sotk > 0)
                {
                    WebServiceConnection.sendThongTinDN(GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, "SoTK=" + sotk, "ECS-SXXK");
                }
                string error = KeySecurity.Active.Install.ECS_updateInfo(UpdateInfo(sotk.ToString()));
                if (!string.IsNullOrEmpty(error))
                    Logger.LocalLogger.Instance().WriteMessage("Lỗi cập nhật thông tin doanh nghiệp: " + error, null);
                if (!e.Cancel)
                {
                    //Hungtq complemented 14/12/2010
                    if (Company.KDT.SHARE.Components.Globals.GetTyGia() == true)
                    {
                        timer1.Enabled = false;
                        MainForm.flag = 1;
                    }
                    else
                    {
                        MainForm.flag = 0;
                        soLanLayTyGia = 0;
                        Thread.Sleep(15000);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "NhacNho":
                    {
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich tokhai = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        NhanNhoForm f = new NhanNhoForm();
                        uint hannhacnho = Convert.ToUInt32(System.Configuration.ConfigurationManager.AppSettings["HanThanhKhoan"]);
                        uint thoigianTK = Convert.ToUInt32(GlobalSettings.ThongBaoHetHan);
                        System.Data.DataSet ds = tokhai.GetThongBao(hannhacnho, thoigianTK);
                        f.ds = ds;
                        if (f.ds == null || f.ds.Tables[0].Rows.Count == 0)
                        {
                            BaseForm baseForm = new BaseForm();
                            // baseForm.ShowMessage("Không có nhắc nhở nào cả", false);
                            return;
                        }
                        else
                        {
                            f.ShowDialog();
                        }
                    }
                    break;
                case "cmdThoat":
                    {
                        this.Close();
                    }
                    break;
                case "DongBoDuLieu":
                    {
                        this.DongBoDuLieuDN();
                    }
                    break;
                case "cmdImportXml":
                    {
                        new ImportAll4Xml().ShowDialog();
                        break;
                    }
                case "cmd2007":
                    if (cmd20071.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    GlobalSettings.Luu_GiaoDien("Office2007");
                    UpdateStyleForAllForm("Office2007");
                    //this.UpdateStyles();

                    break;
                case "cmd2003":
                    if (cmd20031.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    GlobalSettings.Luu_GiaoDien("Office2003");
                    UpdateStyleForAllForm("Office2003");
                    //this.UpdateStyles();
                    break;
                case "cmdExportExccel":
                    ShowExportExcelFile();
                    break;
                case "cmdImportExcel":
                    ShowImportExcelFile();
                    break;

                #region Nhập dữ liệu cho doanh nghiep từ file excel cua hai quan
                case "cmdImportNPL":
                    ShowImportNPLForm();
                    break;
                case "cmdImportSP":
                    ShowImportSPForm();
                    break;
                case "cmdImportDM":
                    ShowImportDMForm();
                    break;
                case "cmdImportTTDM":
                    ShowImportTTDMForm();
                    break;
                case "cmdImportToKhai":
                    ShowImportTKForm();
                    break;
                case "cmdImportHangHoa":
                    ShowImportHMDForm();
                    break;
                case "cmdNPLNhapTon":
                    ShowImportNPLNhapTonForm();
                    break;
                #endregion Nhập dữ liệu cho doanh nghiep từ file excel cua hai quan

                case "cmdHelp":
                    try
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\Huong dan su dung phan mem ECS_TQDT_SXXK.pdf");
                        }
                        else
                        {
                            System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\Huong dan su dung phan mem ECS_TQDT_SXXK.pdf");
                        }
                    }
                    catch { }
                    break;
                case "cmdMaHS":
                    ShowMaHSForm();
                    break;
                case "cmdHaiQuan":
                    ShowDVHQForm();
                    break;
                case "cmdNuoc":
                    ShowNuocForm();
                    break;
                case "cmdNguyenTe":
                    ShowNguyenTeForm();
                    break;
                case "cmdPTTT":
                    ShowPTTTForm();
                    break;
                case "cmdPTVT":
                    ShowPTVTForm();
                    break;
                case "cmdDKGH":
                    ShowDKGHForm();
                    break;
                case "cmdDVT":
                    ShowDVTForm();
                    break;
                case "cmdCuaKhau":
                    ShowCuaKhauForm();
                    break;
                case "cmdRestore":
                    ShowRestoreForm();
                    break;
                case "cmdBackUp":
                    ShowBackupAndRestore(true);
                    break;
                case "ThongSoKetNoi":
                    ShowThietLapThongSoKetNoi();
                    break;
                case "TLThongTinDNHQ":
                    ShowThietLapThongTinDNAndHQ();
                    break;
                case "cmdThietLapIn":
                    ShowThietLapInBaoCao();
                    break;
                case "cmdCauHinhToKhai":
                    ShowCauHinhToKhai();
                    break;
                case "QuanLyNguoiDung":
                    ShowQuanLyNguoiDung();
                    break;
                case "QuanLyNhom":
                    ShowQuanLyNhomNguoiDung();
                    break;
                case "LoginUser":
                    LoginUserKhac();
                    break;
                case "cmdChangePass":
                    ChangePassForm();
                    break;
                case "cmdDoiMatKhauHQ":
                    ChangePassHaiQuanForm();
                    break;
                case "cmdCloseAll":
                    CloseAll();
                    break;
                case "cmdCloseAllButThis":
                    CloseAllButThis();
                    break;
                case "cmdAutoUpdate":
                    RunUpdate("MSG");
                    break;
                case "TraCuuMaHS":
                    MaHSForm();
                    break;
                case "cmdNhomCuaKhau":
                    new SOFTECH.ECS.TQDT.SXXK.KDT.SXXK.NhomCuaKhauForm().ShowDialog();
                    break;
                case "cmdLoaiPhiChungTuThanhToan":
                    new Company.Interface.DanhMucChuan.LoaiPhiChungTuThanhToanForm().ShowDialog();
                    break;
                #region Nhập dữ liệu cho doanh nghiep từ đại lý
                case "cmdNPL":
                    this.ImportNPLXML();
                    break;
                case "cmdSP":
                    this.ImportSPXML();
                    break;
                case "cmdDM":
                    this.ImportDMXML();
                    break;
                case "cmdTK":
                    this.ImportTKXML();
                    break;

                #endregion Nhập dữ liệu cho doanh nghiep từ đại lý

                #region Xuat du lieu tu dai ly,doanh nghiep cho phong khai
                case "cmdHUNGNPL":
                    XuatNPLChoPhongKhai();
                    break;
                case "cmdHUNGSP":
                    XuatSanPhamChoPhongKhai();
                    break;
                case "cmdHUNGDM":
                    XuatDinhMucChoPhongKhai();
                    break;
                case "cmdHUNGTK":
                    XuatToKhaiChoPhongKhai();
                    break;
                #endregion Xuat du lieu tu dai ly,doanh nghiep cho phong khai

                case "cmdEL":
                    // if (cmdVN1.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    // Vietnamese();
                    // this.VietNameseLanguge();
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        try
                        {
                            this.BackgroundImage = System.Drawing.Image.FromFile("ecse.jpg");
                            this.BackgroundImageLayout = ImageLayout.Stretch;
                        }
                        catch { }
                        GlobalSettings.NGON_NGU = "1";
                        GlobalSettings.ActiveStatus = "0"; //load login form
                        GlobalSettings.RefreshKey();
                        CultureInfo culture = new CultureInfo("en-US");

                        Thread.CurrentThread.CurrentCulture = culture;
                        Thread.CurrentThread.CurrentUICulture = culture;
                        Form[] forms = this.MdiChildren;
                        for (int i = 0; i < forms.Length; i++)
                        {
                            ((BaseForm)forms[i]).InitCulture("en-US", "Company.Interface.Resources.Language");
                        }
                        this.InitCulture("en-US", "Company.Interface.Resources.Language");
                    }
                    break;
                case "cmdVN":

                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        if (MLMessages("Chương trình sẽ khởi động lại và chuyển giao diện sang tiếng Việt,hãy lưu lại các công việc đang làm.\nBạn muốn chuyển giao diện không?", "MSG_0203092", "", true) == "Yes")
                        {
                            GlobalSettings.NGON_NGU = "0";
                            GlobalSettings.ActiveStatus = "1"; //load login form
                            GlobalSettings.RefreshKey();
                            Application.Restart();
                        }

                    }
                    break;
                case "cmdAbout":
                    this.dispInfo();
                    break;
                case "cmdActivate":
                    this.dispActivate();
                    break;
                #region Xuat du lieu tu dai ly cho doanh nghiep
                case "cmdXuatNPLDN":
                    this.ExportNPL();
                    break;
                case "cmdXuatSPDN":
                    this.ExportSP();
                    break;
                case "cmdXuatDMDN":
                    this.ExportDM();
                    break;
                case "cmdXuatTKDN":
                    this.ExportTK();
                    break;
                #endregion Xuat du lieu tu dai ly cho doanh nghiep

                #region nhap du lieu cho phong khai
                case "cmdNhapNPL":
                    NhapNPLTuDoanhNghiep();
                    break;
                case "cmdNhapDinhMuc":
                    NhapDinhMucTuDoanhNghiep();
                    break;
                case "cmdNhapSanPham":
                    NhapSanPhamTuDoanhNghiep();
                    break;
                case "cmdNhapToKhai":
                    NhapToKhaiTuDoanhNghiep();
                    break;
                case "QuanlyMess":
                    WSForm2 wsform2 = new WSForm2();
                    wsform2.ShowDialog();
                    if (WSForm2.IsSuccess == true)
                    {
                        ShowQuanlyMess();
                    }
                    break;
                #endregion nhap du lieu cho phong khai
                case "cmdConfig":
                    {
                        this.ShowConfigForm();
                    }
                    break;
                case "mnuQuerySQL":
                    //DATLMQ comment ngày 02/04/2011
                    //Form[] frms = this.MdiChildren;
                    //for (int i = 0; i < frms.Length; i++)
                    //{
                    //    if (frms[i].Name.ToString().Equals("FrmQuerySQL"))
                    //    {
                    //        frms[i].Activate();
                    //        return;
                    //    }
                    //}

                    //FrmQuerySQL frmQuerySQL = new FrmQuerySQL();
                    //frmQuerySQL.MdiParent = this;
                    //frmQuerySQL.Show();
                    WSForm2 login = new WSForm2();
                    login.ShowDialog();
                    if (WSForm2.IsSuccess == true)
                    {
                        //string fileName = Application.StartupPath + "\\MiniSQL\\MiniSqlQuery.exe";
                        //if (System.IO.File.Exists(fileName))
                        //{

                        //    MiniSqlQuery.Core.DbConnectionDefinition conn = new MiniSqlQuery.Core.DbConnectionDefinition();
                        //    conn.ConnectionString = "Data Source=" + GlobalSettings.SERVER_NAME + ";Initial Catalog=" + GlobalSettings.DATABASE_NAME +
                        //                            ";User ID=" + GlobalSettings.USER + ";Password=" + GlobalSettings.PASS;
                        //    conn.Name = GlobalSettings.DATABASE_NAME;
                        //    System.Diagnostics.Process.Start(fileName);
                        //}

                        QuerySQL();
                    }
                    break;
                case "cmdLog":
                    if (System.IO.File.Exists(Application.StartupPath + "\\Error.log"))
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Error.log");
                    break;
            }
        }

        private void QuerySQL()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("FrmQuerySQL"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            frmQuery = new FrmQuerySQL();
            frmQuery.MdiParent = this;
            frmQuery.Show();
        }

        private void ShowConfigForm()
        {
            DangKyForm dangKyForm = new DangKyForm();
            dangKyForm.ShowDialog();

        }
        private void ExportNPL()
        {
            try
            {
                //  NguyenPhuLieuDangKyCollection col = new NguyenPhuLieuDangKyCollection();
                Company.BLL.SXXK.NguyenPhuLieuCollection col = new Company.BLL.SXXK.NguyenPhuLieuCollection();
                col = Company.BLL.SXXK.NguyenPhuLieu.GetNPLXML(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.NguyenPhuLieuCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }


        }
        private void ExportSP()
        {
            try
            {

                Company.BLL.SXXK.SanPhamCollection col = new Company.BLL.SXXK.SanPhamCollection();
                col = Company.BLL.SXXK.SanPham.GetSanPhamXML(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.SanPhamCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
        private void ExportDM()
        {
            DinhMucDaDangKyForm dmdk = new DinhMucDaDangKyForm();
            dmdk.ShowDialog();

        }
        private void ExportTK()
        {
            ToKhaiMauDichDaDangKyForm tkmdddkForm = new ToKhaiMauDichDaDangKyForm();
            tkmdddkForm.ShowDialog();
        }
        private void ShowQuanlyMess()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CuaKhauForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            quanlyMess = new QuanLyMessage();
            quanlyMess.MdiParent = this;
            quanlyMess.Show();
        }
        private void NhapToKhaiTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    ToKhaiMauDichCollection tkDKCollection = (ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    int soTK = 0;
                    foreach (ToKhaiMauDich tkDK in tkDKCollection)
                    {
                        try
                        {
                            tkDK.InsertFull();
                            soTK++;
                        }
                        catch { }
                    }
                    if (soTK > 0)
                        ShowMessage("Nhập thành công " + soTK + " tờ khai.", false);

                    fs.Close();

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private void NhapSanPhamTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    SanPhamDangKyCollection spDKCollection = (SanPhamDangKyCollection)serializer.Deserialize(fs);
                    int soSP = 0;
                    foreach (SanPhamDangKy spDK in spDKCollection)
                    {
                        if (spDK.InsertUpdateFull())
                            soSP++;
                    }
                    if (soSP > 0)
                        ShowMessage("Nhập thành công " + soSP + " sản phẩm.", false);

                    fs.Close();

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private void NhapDinhMucTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    DinhMucDangKyCollection dmDKCollection = (DinhMucDangKyCollection)serializer.Deserialize(fs);
                    int soDM = 0;
                    foreach (DinhMucDangKy dmDK in dmDKCollection)
                    {
                        if (dmDK.InsertUpdateFull())
                            soDM++;
                    }
                    if (soDM > 0)
                        ShowMessage("Nhập thành công " + soDM + " định mức.", false);

                    fs.Close();

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private void NhapNPLTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(NguyenPhuLieuDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    NguyenPhuLieuDangKyCollection nplDKCollection = (NguyenPhuLieuDangKyCollection)serializer.Deserialize(fs);
                    int soNPL = 0;
                    foreach (NguyenPhuLieuDangKy nplDK in nplDKCollection)
                    {
                        if (nplDK.InsertUpdateFull())
                            soNPL++;
                    }
                    if (soNPL > 0)
                        ShowMessage("Nhập thành công " + soNPL + " nguyên phụ liệu.", false);

                    fs.Close();

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }
        private void dispActivate()
        {
            ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
            obj.Show();
        }

        private void dispInfo()
        {
            ProdInfo.frmInfo obj = new Company.Interface.ProdInfo.frmInfo();
            obj.ShowDialog();
        }

        public void English()
        {
            langCheck = 2;
            CultureInfo c = CultureInfo.CurrentCulture;
            if (!c.ToString().Equals("en-US"))
            {
                ;// this.InitCulture("en-US");
            }
        }

        public void Vietnamese()
        {
            langCheck = 1;
            CultureInfo c = CultureInfo.CurrentCulture;
            if (!c.ToString().Equals("vi-VN"))
            {
                ;// this.InitCulture("vi-VN");
            }
        }

        private void XuatToKhaiChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ToKhaiMauDichManageForm f = new ToKhaiMauDichManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void XuatDinhMucChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            DinhMucManageForm f = new DinhMucManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void XuatSanPhamChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SanPhamManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            SanPhamManageForm f = new SanPhamManageForm();
            f.MdiParent = this;
            f.Show();
        }

        private void XuatNPLChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenPhuLieuManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            NguyenPhuLieuManageForm f = new NguyenPhuLieuManageForm();
            f.MdiParent = this;
            f.Show();
        }

        #region Nhập dữ liệu
        //

        private void ImportNPLXML()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.NguyenPhuLieuCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.BLL.SXXK.NguyenPhuLieuCollection nplDKCollection = (Company.BLL.SXXK.NguyenPhuLieuCollection)serializer.Deserialize(fs);
                    fs.Close();
                    Company.BLL.SXXK.NguyenPhuLieu.NhapNPLXML(nplDKCollection);
                    ShowMessage("Import thành công", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }

        }

        private void ImportSPXML()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.SanPhamCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.BLL.SXXK.SanPhamCollection SPDKCollection = (Company.BLL.SXXK.SanPhamCollection)serializer.Deserialize(fs);
                    fs.Close();
                    Company.BLL.SXXK.SanPham.NhapSPXML(SPDKCollection);
                    ShowMessage("Import thành công", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }

        }
        private string CheckDataDinhMucImport(Company.BLL.SXXK.DinhMucCollection DinhMucDKCollection)
        {
            string st = "";
            string masanpham = "";
            Company.BLL.SXXK.DinhMucCollection DinhMucDKCollectionExit = new Company.BLL.SXXK.DinhMucCollection();
            foreach (Company.BLL.SXXK.DinhMuc DinhMucDK in DinhMucDKCollection)
            {
                if (Company.BLL.SXXK.DinhMuc.checkIsExist(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, DinhMucDK.MaSanPHam))
                {
                    DinhMucDKCollectionExit.Add(DinhMucDK);
                    if (masanpham.Trim().ToUpper() != DinhMucDK.MaSanPHam.Trim().ToUpper())
                    {
                        masanpham = DinhMucDK.MaSanPHam;
                        st += "Sản phẩm " + DinhMucDK.MaSanPHam + "\n";
                    }
                }
            }
            foreach (Company.BLL.SXXK.DinhMuc DinhMucDK in DinhMucDKCollectionExit)
            {
                DinhMucDKCollection.Remove(DinhMucDK);
            }
            return st;
        }
        private void ImportDMXML()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.DinhMucCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.BLL.SXXK.DinhMucCollection DinhMucDKCollection = (Company.BLL.SXXK.DinhMucCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = CheckDataDinhMucImport(DinhMucDKCollection);
                    if (st != "")
                    {
                        ShowMessage("Các " + st + " đã có định mức trong hệ thống nên sẽ được bỏ qua.", false);
                    }
                    Company.BLL.SXXK.DinhMuc.NhapDMXML(DinhMucDKCollection, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                    ShowMessage("Import thành công", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }

        }


        private void ImportTKXML()
        {
            Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollectionImport = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
            Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollectionExits = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKMDCollectionImport = (Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(TKMDCollectionImport, TKMDCollectionExits);
                    if (st == "")
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich.NhapDuLieuXML(TKMDCollectionImport);
                    else
                    {
                        ShowMessage(st + "đã có trong hệ thống.Chương trình sẽ bỏ qua những tờ khai đã có trong hệ thống.", false);
                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich.NhapDuLieuXML(TKMDCollectionImport);
                    }
                    ShowMessage("Import thành công", false);
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }

        }
        private string checkDataImport(Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollectionImport, Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollectionExit)
        {
            string st = "";
            foreach (Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD in TKMDCollectionImport)
            {
                Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMDData = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                TKMDData.SoToKhai = TKMD.SoToKhai;
                TKMDData.MaHaiQuan = TKMD.MaHaiQuan;
                TKMDData.MaLoaiHinh = TKMD.MaLoaiHinh;
                TKMDData.NamDangKy = TKMD.NamDangKy;
                if (TKMDData.Load())
                {
                    st += "Tờ khai " + TKMDData.SoToKhai + "/" + TKMDData.MaLoaiHinh + "/" + TKMDData.MaHaiQuan + "/" + TKMDData.NamDangKy + "\n";
                    TKMDCollectionExit.Add(TKMD);
                }
            }
            foreach (Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD in TKMDCollectionExit)
            {
                TKMDCollectionImport.Remove(TKMD);
            }
            return st;
        }
        //private void ImportData()
        //{

        //}

        //
        #endregion Nhập dữ liệu

        private void CloseAllButThis()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (!forms[i].Name.ToString().Equals(this.ActiveMdiChild.Name))
                {
                    forms[i].Close();
                    forms[i].Dispose();
                }
            }
        }

        private void CloseAll()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
                forms[i].Dispose();
            }
        }
        private void ChangePassHaiQuanForm()
        {
            ChangePasswordHQForm f = new ChangePasswordHQForm();
            f.ShowDialog();
        }
        private void ChangePassForm()
        {
            ChangePassForm f = new ChangePassForm();
            f.ShowDialog();
        }
        private void LoginUserKhac()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
            MainForm.EcsQuanTri = null;
            isLoginSuccess = false;
            if (versionHD == 0)
            {
                Login login = new Login();
                login.ShowDialog();
            }
            else if (versionHD == 1)
            {
                Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                login.ShowDialog();
            }
            else if (versionHD == 2)
            {
                Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                login.ShowDialog();
            }
            if (isLoginSuccess)
            {
                //statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = "Người dùng: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                this.Show();
                if (versionHD == 0)
                {
                    if (MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    else
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)))
                    {
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else if (versionHD == 1)
                {
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                }
                else if (versionHD == 2)
                {
                    switch (typeLogin)
                    {
                        case 1: // User Da Cau Hinh
                            this.Show();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 2: // User Chua Cau Hinh
                            this.Show();
                            DangKyForm dangKyForm = new DangKyForm();
                            dangKyForm.ShowDialog();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 3:// Admin
                            this.Hide();
                            CreatAccountForm fAdminForm = new CreatAccountForm();
                            fAdminForm.ShowDialog();
                            this.ShowLoginForm();
                            break;
                    }
                }
                khoitao_GiaTriMacDinh();
            }
            else
                Application.Exit();
        }
        private void ShowQuanLyNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            NguoiDung = new QuanLyNguoiDung();
            NguoiDung.MdiParent = this;
            NguoiDung.Show();
        }
        private void ShowQuanLyNhomNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNhomNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            NhomNguoiDung = new QuanLyNhomNguoiDung();
            NhomNguoiDung.MdiParent = this;
            NhomNguoiDung.Show();
        }
        private void ShowCauHinhToKhai()
        {
            CauHinhToKhaiForm f = new CauHinhToKhaiForm();
            f.ShowDialog();
        }

        private void ShowExportExcelFile()
        {
            ExportToExcelForm export = new ExportToExcelForm();
            export.ShowDialog();

        }
        private void ShowImportExcelFile()
        {
            ImportFromExcelForm import = new ImportFromExcelForm();
            import.ShowDialog();

        }

        private void ShowThietLapInBaoCao()
        {
            CauHinhInForm f = new CauHinhInForm();
            f.ShowDialog();
        }
        private void ShowRestoreForm()
        {
            RestoreForm f = new RestoreForm();
            f.ShowDialog();
        }
        private void ShowThietLapThongTinDNAndHQ()
        {
            ThongTinDNAndHQForm f = new ThongTinDNAndHQForm();
            f.ShowDialog();
            khoitao_GiaTriMacDinh();
        }
        private void ShowThietLapThongSoKetNoi()
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();

        }
        private void ShowBackupAndRestore(bool isBackUp)
        {
            BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            f.isBackUp = isBackUp;
            f.ShowDialog();
        }
        private void ShowCuaKhauForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CuaKhauForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ckForm = new CuaKhauForm();
            ckForm.MdiParent = this;
            ckForm.Show();
        }

        private void ShowDVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DonViTinhForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dvtForm = new DonViTinhForm();
            dvtForm.MdiParent = this;
            dvtForm.Show();
        }

        private void ShowDKGHForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DKGHForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dkghForm = new DKGHForm();
            dkghForm.MdiParent = this;
            dkghForm.Show();
        }
        private void ShowPTVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PTVTForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ptvtForm = new PTVTForm();
            ptvtForm.MdiParent = this;
            ptvtForm.Show();
        }
        private void ShowPTTTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PTTTForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ptttForm = new PTTTForm();
            ptttForm.MdiParent = this;
            ptttForm.Show();
        }

        private void ShowNguyenTeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenTeForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ntForm = new NguyenTeForm();
            ntForm.MdiParent = this;
            ntForm.Show();
        }

        private void ShowNuocForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NuocForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            nuocForm = new NuocForm();
            nuocForm.MdiParent = this;
            nuocForm.Show();
        }

        private void ShowDVHQForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DonViHaiQuanForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dvhqForm = new DonViHaiQuanForm();
            dvhqForm.MdiParent = this;
            dvhqForm.Show();
        }

        private void ShowMaHSForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("MaHSForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            maHSForm = new MaHSForm();
            maHSForm.MdiParent = this;
            maHSForm.Show();
        }
        private void MaHSForm()
        {
            HaiQuan.HS.MainForm f = new HaiQuan.HS.MainForm();
            f.Show();
        }
        private void ShowImportNPLNhapTonForm()
        {
            ImportNPLNhapTonForm f = new ImportNPLNhapTonForm();
            f.ShowDialog();
        }

        private void ShowImportHMDForm()
        {
            ImportHMDForm f = new ImportHMDForm();
            f.ShowDialog();
        }

        private void ShowImportTKForm()
        {
            ImportTKForm f = new ImportTKForm();
            f.ShowDialog();
        }

        private void ShowImportTTDMForm()
        {
            ImportTTDMForm f = new ImportTTDMForm();
            f.TTDMCollection = new Company.BLL.SXXK.ThongTinDinhMuc().SelectCollectionAll();
            f.ShowDialog();
        }

        private void ShowImportDMForm()
        {
            ImportDMForm f = new ImportDMForm();
            f.DMCollection = new Company.BLL.SXXK.DinhMuc().SelectCollectionAll();
            //f.TTDMCollection = new Company.BLL.SXXK.ThongTinDinhMuc().SelectCollectionAll(); 
            f.ShowDialog();
        }

        private void ShowImportSPForm()
        {
            ImportSPForm f = new ImportSPForm();
            f.SPCollection = new Company.BLL.SXXK.SanPham().SelectCollectionAll();
            f.ShowDialog();
        }

        private void ShowImportNPLForm()
        {
            ImportNPLForm f = new ImportNPLForm();
            f.NPLCollection = new Company.BLL.SXXK.NguyenPhuLieu().SelectCollectionAll();
            f.ShowDialog();
        }
        private void DongBoDuLieuDN()
        {
            BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            f.ShowDialog();
        }
        private void UpdateStyleForAllForm(string style)
        {
            this.vsmMain.DefaultColorScheme = style;
            queueForm.vsmMain.DefaultColorScheme = style;
            if (nplSendForm != null)
                nplSendForm.vsmMain.DefaultColorScheme = style;
            if (nplManageForm != null)
                nplManageForm.vsmMain.DefaultColorScheme = style;
            if (nplRegistedForm != null)
                nplRegistedForm.vsmMain.DefaultColorScheme = style;
            if (spSendForm != null)
                spSendForm.vsmMain.DefaultColorScheme = style;
            if (spManageForm != null)
                spManageForm.vsmMain.DefaultColorScheme = style;
            if (spRegistedForm != null)
                spRegistedForm.vsmMain.DefaultColorScheme = style;
            if (dmSendForm != null)
                dmSendForm.vsmMain.DefaultColorScheme = style;
            if (dmManageForm != null)
                dmManageForm.vsmMain.DefaultColorScheme = style;
            if (dmRegistedForm != null)
                dmRegistedForm.vsmMain.DefaultColorScheme = style;
            if (tkmdForm != null)
                tkmdForm.vsmMain.DefaultColorScheme = style;
            //if (tkmdSendForm != null)
            //    tkmdSendForm.vsmMain.DefaultColorScheme = style;
            if (tkmdManageForm != null)
                tkmdManageForm.vsmMain.DefaultColorScheme = style;
            if (taoHSTLForm != null)
                taoHSTLForm.vsmMain.DefaultColorScheme = style;
            if (capNhatHSTLForm != null)
                capNhatHSTLForm.vsmMain.DefaultColorScheme = style;
            if (hstlDaDongForm != null)
                hstlDaDongForm.vsmMain.DefaultColorScheme = style;
            if (tkmdRegisterForm != null)
                tkmdRegisterForm.vsmMain.DefaultColorScheme = style;
            if (maHSForm != null)
                maHSForm.vsmMain.DefaultColorScheme = style;
            if (pbTKNForm != null)
                pbTKNForm.vsmMain.DefaultColorScheme = style;
            if (pbTKXForm != null)
                pbTKXForm.vsmMain.DefaultColorScheme = style;
            if (ptttForm != null)
                ptttForm.vsmMain.DefaultColorScheme = style;
            if (ptvtForm != null)
                ptvtForm.vsmMain.DefaultColorScheme = style;
            if (dvtForm != null)
                dvtForm.vsmMain.DefaultColorScheme = style;
            if (dvhqForm != null)
                dvhqForm.vsmMain.DefaultColorScheme = style;
            if (ckForm != null)
                ckForm.vsmMain.DefaultColorScheme = style;
            if (ntForm != null)
                ntForm.vsmMain.DefaultColorScheme = style;
            if (dkghForm != null)
                dkghForm.vsmMain.DefaultColorScheme = style;
            if (nuocForm != null)
                nuocForm.vsmMain.DefaultColorScheme = style;

        }

        private void cmbMenu_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void closeAllButThisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
        }

        #region AutoUpdate ONLINE

        private void CheckNewVersion()
        {
            //try
            //{
            //    Logger.LocalLogger.Instance().WriteMessage("Kiểm tra phiên bản mới.", new Exception());

            //    //Cap nhat moi file ECS_AuotoUpdate trong UPDATE folder.
            //    if (System.IO.File.Exists(Application.StartupPath + "\\Update\\ECS_AutoUpdate.exe"))
            //    {
            //        Logger.LocalLogger.Instance().WriteMessage("Copy " + Application.StartupPath + "\\Update\\ECS_AutoUpdate.exe", new Exception());

            //        System.IO.File.Copy(Application.StartupPath + "\\Update\\ECS_AutoUpdate.exe", Application.StartupPath + "\\ECS_AutoUpdate.exe", true);
            //    }

            //    UpdateOnline.AutoUpdate.RemotePath = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("RemotePath");
            //    UpdateOnline.AutoUpdate.AssemblyName = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("AssemblyName");

            //    if (System.IO.File.Exists(Application.StartupPath + "\\" + UpdateOnline.AutoUpdate.AssemblyName + ".exe"))
            //    {
            //        string currentVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(Application.StartupPath + "\\" + UpdateOnline.AutoUpdate.AssemblyName + ".exe").ProductVersion;
            //        string lastestVersion = UpdateOnline.AutoUpdate.CheckNewVersion(UpdateOnline.AutoUpdate.RemotePath, UpdateOnline.AutoUpdate.AssemblyName);

            //        if (lastestVersion != "" && (System.Convert.ToDecimal(UpdateOnline.AutoUpdate.GetVersion(lastestVersion)) > System.Convert.ToDecimal(UpdateOnline.AutoUpdate.GetVersion(currentVersion))))
            //        {
            //            //Add NewVersion command on Menu bar.
            //            CreateCommandBosung(lastestVersion);

            //            AutoUpdate();

            //            Logger.LocalLogger.Instance().WriteMessage("Có phiên bản mới: " + lastestVersion + ".\r\nĐã thực hiện cập nhật ngày: " + System.DateTime.Now.ToString(), new Exception());
            //        }
            //    }
            //}
            //catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void RunUpdate(string args)
        {
            Company.KDT.SHARE.Components.DownloadUpdate dl = new Company.KDT.SHARE.Components.DownloadUpdate(args);
            dl.DoDownload();
        }
        #region TẠO COMMAND TOOLBAR

        private delegate void CreateCommandCallback(string lastestVersion);
        private void CreateCommand(string lastestVersion)
        {
            try
            {
                this.cmbMenu.CommandManager = this.cmMain;
                this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                    this.cmdNewVersion
                });

                if (!this.cmMain.Commands.Contains("cmdNewVersion"))
                    this.cmMain.Commands.Add(this.cmdNewVersion);

                // 
                // cmdNewVersion
                // 
                this.cmdNewVersion.Key = "cmdNewVersion";
                this.cmdNewVersion.Name = "cmdNewVersion";
                this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;
                this.cmdNewVersion.TextAlignment = Janus.Windows.UI.CommandBars.ContentAlignment.MiddleCenter;
                this.cmdNewVersion.TextImageRelation = Janus.Windows.UI.CommandBars.TextImageRelation.ImageBeforeText;
                this.cmdNewVersion.ImageReplaceableColor = System.Drawing.Color.Pink;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //HUNGTQ, Update 11/06/2010.

        private Janus.Windows.UI.CommandBars.UICommand cmdNewVersion;

        /// <summary>
        /// Tạo command chứng từ đính kèm bổ sung.
        /// </summary>
        private void CreateCommandBosung(string lastestVersion)
        {
            #region Initialize
            //Tao nut command tren toolbar.
            this.cmdNewVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdNewVersion");

            //this.cmbMenu.CommandManager = this.cmMain;
            //this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            //    this.cmdNewVersion
            //});

            //if (!this.cmMain.Commands.Contains("cmdNewVersion"))
            //    this.cmMain.Commands.Add(this.cmdNewVersion);

            //// 
            //// cmdNewVersion
            //// 
            //this.cmdNewVersion.Key = "cmdNewVersion";
            //this.cmdNewVersion.Name = "cmdNewVersion";
            //this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;

            // Invoke the method that updates the form's label
            this.Invoke(new CreateCommandCallback(this.CreateCommand), new object[] { lastestVersion });

            #endregion

            cmdNewVersion.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdNewVersion_Click);

        }

        private void cmdNewVersion_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                switch (e.Command.Key)
                {
                    case "cmdNewVersion":
                        RunUpdate("MSG");
                        break;
                }
            }
            catch (Exception ex) { }
        }

        #endregion

        private void btnHDXK_Click(object sender, EventArgs e)
        {
            FrmQuanLyHopDongXuatKhau f = new FrmQuanLyHopDongXuatKhau();
            f.Show();
        }

        #endregion

        #region Begin ECS Express

        private void Express()
        {
            try
            {
                string val = new Company.KDT.SHARE.Components.Utils.License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Ep"));

                if (!string.IsNullOrEmpty(val) && bool.Parse(val) == true)
                {
                    SetExpress(false);
                }
                else
                    SetExpress(true);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void SetExpress(bool visible)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Janus.Windows.UI.InheritableBoolean status = visible == true ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

                /*Menu He thong*/
                cmdConfig.Visible = status;             //Thiet lap thong tin doanh nghiep
                cmdImportXml.Visible = status;            //Nhap du lieu XML
                cmdNhapXML.Visible = status;            //Nhap du lieu tu dai ly
                cmdDongBoPhongKhai.Visible = cmdNhapDuLieu.Visible = status;    //Nhap du lieu tu doanh nghiep
                cmdRestore.Visible = status;            //Phuc hoi du lieu
                cmdXuatDuLieu.Visible = status;         //Xuat du lieu cho phong khai
                cmdXuatDuLieuDN.Visible = status;       //Xuat du lieu cho doanh nghiep

                /*Menu Giao dien*/
                cmdEL.Visible = status;

                //NPL Ton
                expKhaiBao_TheoDoi.Groups["grpNPLTon"].Items["tkNPLTon"].Visible = visible;
                expKhaiBao_TheoDoi.Groups["grpNPLTon"].Items["tdPBTKN"].Visible = visible;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        #endregion End ECS Express


        private void requestActivate_new()
        {

            string sfmtMsg = "{0}\nVui lòng liên hệ với Công ty cổ phẩn SOFTECH để tiếp tục sử dụng";
            bool isShowActive = false;

            if (KeySecurity.Active.Install.License.KeyInfo == null)
            {
                sfmtMsg = string.Format(sfmtMsg, "Bạn chưa đăng ký sử dụng phần mềm.");
                isShowActive = true;
            }
            //else if (Helpers.GetMD5Value(Security.Active.Install.License.KeyInfo.ProductId) != "ECS_TQDT_KD")
            //{
            //    sfmtMsg = string.Format(sfmtMsg, "Mã sản phẩm không hợp lệ.");
            //    isShowActive = true;
            //}
            else if (KeySecurity.Active.Install.License.KeyInfo.TrialDays == 0)
            {
                sfmtMsg = string.Format(sfmtMsg, "Đã hết hạn dùng phần mềm");
                isShowActive = true;
            }

            if (isShowActive)
            {
                if (ShowMessage(sfmtMsg, true) == "Yes")
                {
                    ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
                    if (obj.ShowDialog(this) == DialogResult.OK)
                    {
                        //Company.Interface.ProdInfo.frmInfo f = new Company.Interface.ProdInfo.frmInfo();
                        //f.ShowDialog();
                        ShowMessage("Kích hoạt thành công. Chương trình sẽ tự khởi động lại", false);
                        Application.ExitThread();
                        Application.Restart();
                    }
                    else
                    {
                        ShowMessage("Kích hoạt không thành công, chương trình sẽ tự đóng", false);
                        Application.ExitThread();
                        Application.Exit();
                    }

                }
                else
                {
                    Application.ExitThread();
                    Application.Exit();
                }
            }
            if (KeySecurity.Active.Install.License.KeyInfo.IsShow
                && !string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Notice))
            {
                InfoOnline inf = new InfoOnline();
                inf.SetRTF(KeySecurity.Active.Install.License.KeyInfo.Notice);
                inf.ShowDialog();
            }
        }
        
        #region Cập nhật thông tin

        private string UpdateInfo(string soTK)
        {
            Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer info = new Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer();
            #region Tạo thông tin update
            info.Id = GlobalSettings.MA_DON_VI;
            info.Name = GlobalSettings.TEN_DON_VI;
            info.Address = GlobalSettings.DIA_CHI;
            info.Phone = GlobalSettings.DienThoai;
            info.Contact_Person = GlobalSettings.NguoiLienHe;
            info.Email = GlobalSettings.MailDoanhNghiep;
            info.Id_Customs = GlobalSettings.MA_HAI_QUAN;
            info.IP_Customs = GlobalSettings.DiaChiWS;
            info.ServerName = GlobalSettings.SERVER_NAME;
            info.Data_Name = GlobalSettings.DATABASE_NAME;
            info.Data_User = GlobalSettings.USER;
            info.Data_Pass = GlobalSettings.PASS;
            info.Data_Version = Company.KDT.SHARE.Components.Version.GetVersion();
            info.App_Version = Application.ProductVersion;
            info.LastCheck = DateTime.Now;
            info.temp1 = GlobalSettings.NguoiLienHe;
            info.MachineCode = KeySecurity.KeyCode.ProcessInfo();
            info.Product_ID = "ECS_TQDT_SXXK_V2";
            info.RecordCount = soTK;
            #endregion
            return Company.KDT.SHARE.Components.Helpers.Serializer(info);
        }
        #endregion
    }
}
