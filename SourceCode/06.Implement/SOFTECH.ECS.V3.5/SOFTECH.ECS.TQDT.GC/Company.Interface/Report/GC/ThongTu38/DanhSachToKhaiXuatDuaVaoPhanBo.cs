﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Logger;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.GC.ThongTu38
{
    public partial class DanhSachToKhaiXuatDuaVaoPhanBo : BaseForm
    {
        //hop dong duoc chon
        private HopDong hopDong = new HopDong();

        public HopDong HopDong
        {
            get { return hopDong; }
            set { hopDong = value; }
        }

        public DanhSachToKhaiXuatDuaVaoPhanBo()
        {
            InitializeComponent();
        }

        private void DanhSachToKhaiXuatDuaVaoPhanBo_Load(object sender, EventArgs e)
        {
            dgList.DataSource = GetDataSource(HopDong.ID);
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private DataTable GetDataSource(long IDHopDong)
        {
            try
            {
                string sql = "SELECT  SoToKhai , " +
                                            "CASE WHEN MaLoaiHinh LIKE '%V%' THEN ( SELECT TOP 1SoTKVNACCS " +                                                                                            
                                                                                   "FROM t_VNACCS_CapSoToKhai " + "WHERE SoTK = SoToKhai) " +
                                                 "ELSE SoToKhai " +
                                            "END AS SoToKhaiVNACCS, " +
                                             "NgayDangKy , " +
                                            "CASE WHEN MaLoaiHinh LIKE '%V%' THEN SUBSTRING(MaLoaiHinh, 3, 3) " +
                                                 "ELSE MaLoaiHinh " +
                                            "END AS MaLoaiHinh " +                                            
                                    "FROM    v_GC_ToKhaiXuatDaPhanBo " +
                                    "WHERE   IDHopDong = " + IDHopDong + " ORDER BY NgayDangKy";

                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                dbCommand.CommandTimeout = Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60;
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void btnBKToKhaiXuat_Click(object sender, EventArgs e)
        {
            

            BKToKhaiXuatTT38 bangKe = new BKToKhaiXuatTT38();
            bangKe.HopDong = this.HopDong;
            bangKe.DSTK = LayDSToKhaiDuocChon();
          //MessageBox.Show(bangKe.DSTK);
            bangKe.BindReport();
            bangKe.ShowPreview();
        }

        private string LayDSToKhaiDuocChon()
        {
            string temp = "";
        
                foreach (var row in dgList.GetCheckedRows())
                {
                    string soTK = row.Cells["SoToKhai"].Value.ToString();
                    temp += soTK + ", ";
                }
            return temp.Substring(0, temp.Length - 2);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //, ConditionOperator.Equal, Convert.ToDecimal(txtSoToKhaiTL.Value
                if (txtSoToKhai.Text != "0")
                {
                    if (dgList.FindAll(dgList.Tables[0].Columns["LoaiVanDon"], Janus.Windows.GridEX.ConditionOperator.Equal, Convert.ToDecimal(txtSoToKhai.Value)) > 0)
                    {

                        foreach (GridEXSelectedItem item in dgList.SelectedItems)
                            item.GetRow().IsChecked = true;
                    }
                    else
                    {
                        MLMessages("Không có tờ khai này trong danh sách", "MSG_THK79", "", false);
                    }
                }
                else
                {

                    DateTime fromDate = ccFromDate.Value;
                    DateTime toDate = ccToDate.Value;
                    if (fromDate > toDate)
                    {
                        MLMessages("Từ ngày phải nhỏ hơn hoặc bằng đến ngày.", "MSG_SEA01", "", false);
                        return;
                    }
                    fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                    toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

                    int soLuongToKhai = 0;
                    foreach (GridEXRow row in dgList.GetRows())
                    {
                        if (Convert.ToDateTime(row.Cells["NgayDangKy"].Value) >= fromDate && Convert.ToDateTime(row.Cells["NgayDangKy"].Value) <= toDate)
                        {
                            row.IsChecked = true;
                            soLuongToKhai++;
                        }
                    }
                    if (soLuongToKhai > 0)
                    {
                    }
                    else
                    {
                        MLMessages("Không có tờ khai nào được đăng ký trong thời gian đã cho", "MSG_THK79", "", false);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
