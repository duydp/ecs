﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ThanhKhoan;
using Janus.Windows.GridEX;
using Company.BLL;
using Company.Interface.Report;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;
namespace Company.Interface.SXXK
{
    public partial class HSTLDaDongForm : BaseForm
    {
        List<HoSoThanhLyDangKy> HSTLCollection = new List<HoSoThanhLyDangKy>();
        public HSTLDaDongForm()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "TrangThaiThanhKhoan = " + (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo + " AND MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoHoSo = " + txtSoTiepNhan.Value;
            }

            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND Year(NgayBatDau) = " + txtNamTiepNhan.Value;
            }

            // Thực hiện tìm kiếm.            
            this.HSTLCollection = HoSoThanhLyDangKy.SelectCollectionDynamic(where, "");
            dgList.DataSource = this.HSTLCollection;
        }

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {
            //WSForm wsForm = new WSForm();
            //wsForm.ShowDialog(this);
            //if (!wsForm.IsReady) return;
            //try
            //{
            //    this.Cursor = Cursors.WaitCursor;                
            //    LanThanhLyBase.DongBoDuLieuHaiQuan(donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI);
            //    this.Cursor = Cursors.Default;               
            //    this.ShowMessage("Cập nhật thành công", false);
            //    this.btnSearch_Click(null, null);
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    this.ShowMessage("Có lỗi: " + ex.Message, false);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
        }


        private void TheoDoiHoSoThanhLyForm_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Value = DateTime.Today.Year;
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.KhaiDienTu)))
            {
                CapNhatDongHSTK.Enabled = false;
            }
            btnSearch_Click(null, null);
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //XemHoSoThanhLyForm f = new XemHoSoThanhLyForm();
            //f.Name="XemTL"+e.Row.Cells["LanThanhLy"].Text;
            //Form[] forms = this.ParentForm.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals(f.Name))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}

            //f.LanTL = new LanThanhLyBase();
            //f.LanTL.LanThanhLy = Convert.ToInt32(e.Row.Cells["LanThanhLy"].Text);
            //f.LanTL.MaHaiQuan = donViHaiQuanControl1.Ma;
            //f.LanTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //f.MdiParent = this.ParentForm;
            //f.Show();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["NgayKetThuc"].Text != "")
            {
                try
                {
                    DateTime date = Convert.ToDateTime(e.Row.Cells["NgayKetThuc"].Text);
                    if (date.Year <= 1900)
                        e.Row.Cells["NgayKetThuc"].Text = "";
                }
                catch { }

            }
        }


        private void XemBaoCao_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                HoSoThanhLyDangKy HSTL = (HoSoThanhLyDangKy)row.DataRow;
                if (HSTL.TrangThaiThanhKhoan < (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaChayThanhKhoan)
                {
                    // ShowMessage("Hồ sơ chưa chạy thanh khoản thành công, không thể xem báo cáo.", false);
                    MLMessages("Hồ sơ chưa chạy thanh khoản thành công, không thể xem báo cáo.", "MSG_THK86", "", false);
                    return;
                }
                HSTL.LoadBKCollection();
                if (GlobalSettings.SoThapPhan.TachLam2 == 1)
                {
                    ChonToKhaiNhapForm1 f = new ChonToKhaiNhapForm1();
                    f.HSTL = HSTL;
                    f.ShowDialog(this);
                    PreviewFormITG f1 = new PreviewFormITG();
                    f1.SoHoSo = HSTL.SoHoSo;
                    f1.LanThanhLy = HSTL.LanThanhLy;
                    f1.TKNHoanThueCollection = f.TKNHoanThueCollection;
                    f1.TKNKhongThuCollection = f.TKNKhongThuCollection;
                    f1.Show();
                }
                else
                {
                    PreviewForm f = new PreviewForm();
                    f.SoHoSo = HSTL.SoHoSo;
                    f.LanThanhLy = HSTL.LanThanhLy;
                    f.Show();
                }
            }
        }

        private void CapNhatDongHSTK_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                HoSoThanhLyDangKy HSTL = (HoSoThanhLyDangKy)row.DataRow;
                CapNhapDongHSTLForm f = new CapNhapDongHSTLForm();
                f.HSTL = HSTL;
                f.ShowDialog(this);
                btnSearch_Click(null, null);
            }

        }

        private void xemCTTT_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                HoSoThanhLyDangKy HSTL = (HoSoThanhLyDangKy)row.DataRow;
                HSTL.LoadBKCollection();
                ChungTuThanhToanForm_Modified f = new ChungTuThanhToanForm_Modified();
                f.HSTL = HSTL;
                f.Show();

            }
        }

        private void xemBaoCao929_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                HoSoThanhLyDangKy HSTL = (HoSoThanhLyDangKy)row.DataRow;
                if (HSTL.TrangThaiThanhKhoan < (int)Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaChayThanhKhoan)
                {
                    //ShowMessage("Hồ sơ chưa chạy thanh khoản thành công, không thể xem báo cáo.", false);
                    MLMessages("Hồ sơ chưa chạy thanh khoản thành công, không thể xem báo cáo.", "MSG_THK86", "", false);
                    return;
                }
                HSTL.LoadBKCollection();
                if (GlobalSettings.SoThapPhan.TachLam2 == 1)
                {
                    ChonToKhaiNhapForm1 f = new ChonToKhaiNhapForm1();
                    f.HSTL = HSTL;
                    f.ShowDialog(this);
                    PreviewFormBC929 f1 = new PreviewFormBC929();
                    f1.SoHoSo = HSTL.SoHoSo;
                    f1.LanThanhLy = HSTL.LanThanhLy;
                    f1.TKNHoanThueCollection = f.TKNHoanThueCollection;
                    f1.TKNKhongThuCollection = f.TKNKhongThuCollection;
                    f1.Show();
                }
                else
                {
                    PreviewForm929 f = new PreviewForm929();
                    f.SoHoSo = HSTL.SoHoSo;
                    f.LanThanhLy = HSTL.LanThanhLy;
                    f.Show();
                }
            }
        }




    }
}