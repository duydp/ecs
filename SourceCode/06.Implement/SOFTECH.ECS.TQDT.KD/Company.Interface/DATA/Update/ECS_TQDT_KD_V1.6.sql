/*
Run this script on:

        ecsteam.ECS_TQDT_KD_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_KD

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 01/18/2012 5:39:01 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_KDT_Message_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_Insert]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_Message_Insert]
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@MessageFrom varchar(50),
	@MessageTo varchar(50),
	@MessageType int,
	@MessageFunction int,
	@MessageContent nvarchar(max),
	@CreatedTime datetime,
	@TieuDeThongBao nvarchar(100),
	@NoiDungThongBao nvarchar(max),
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_Messages]
(
	[ReferenceID],
	[ItemID],
	[MessageFrom],
	[MessageTo],
	[MessageType],
	[MessageFunction],
	[MessageContent],
	[CreatedTime],
	[TieuDeThongBao],
	[NoiDungThongBao]
)
VALUES 
(
	@ReferenceID,
	@ItemID,
	@MessageFrom,
	@MessageTo,
	@MessageType,
	@MessageFunction,
	@MessageContent,
	@CreatedTime,
	@TieuDeThongBao,
	@NoiDungThongBao
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_Message_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_Update]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_Message_Update]
	@ID int,
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@MessageFrom varchar(50),
	@MessageTo varchar(50),
	@MessageType int,
	@MessageFunction int,
	@MessageContent nvarchar(max),
	@CreatedTime datetime,
	@TieuDeThongBao nvarchar(100),
	@NoiDungThongBao nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT_Messages]
SET
	[ReferenceID] = @ReferenceID,
	[ItemID] = @ItemID,
	[MessageFrom] = @MessageFrom,
	[MessageTo] = @MessageTo,
	[MessageType] = @MessageType,
	[MessageFunction] = @MessageFunction,
	[MessageContent] = @MessageContent,
	[CreatedTime] = @CreatedTime,
	[TieuDeThongBao] = @TieuDeThongBao,
	[NoiDungThongBao] = @NoiDungThongBao
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_Message_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_InsertUpdate]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_Message_InsertUpdate]
	@ID int,
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@MessageFrom varchar(50),
	@MessageTo varchar(50),
	@MessageType int,
	@MessageFunction int,
	@MessageContent nvarchar(max),
	@CreatedTime datetime,
	@TieuDeThongBao nvarchar(100),
	@NoiDungThongBao nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_Messages] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_Messages] 
		SET
			[ReferenceID] = @ReferenceID,
			[ItemID] = @ItemID,
			[MessageFrom] = @MessageFrom,
			[MessageTo] = @MessageTo,
			[MessageType] = @MessageType,
			[MessageFunction] = @MessageFunction,
			[MessageContent] = @MessageContent,
			[CreatedTime] = @CreatedTime,
			[TieuDeThongBao] = @TieuDeThongBao,
			[NoiDungThongBao] = @NoiDungThongBao
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_Messages]
		(
			[ReferenceID],
			[ItemID],
			[MessageFrom],
			[MessageTo],
			[MessageType],
			[MessageFunction],
			[MessageContent],
			[CreatedTime],
			[TieuDeThongBao],
			[NoiDungThongBao]
		)
		VALUES 
		(
			@ReferenceID,
			@ItemID,
			@MessageFrom,
			@MessageTo,
			@MessageType,
			@MessageFunction,
			@MessageContent,
			@CreatedTime,
			@TieuDeThongBao,
			@NoiDungThongBao
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE dbo.t_HaiQuan_Version SET [Version] ='1.6', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.6', getdate(), null)
	end