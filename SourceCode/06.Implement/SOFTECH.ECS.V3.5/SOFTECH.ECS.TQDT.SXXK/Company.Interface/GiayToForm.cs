﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.BLL;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using Company.KDT.SHARE.Components;
namespace Company.Interface
{
    public partial class GiayToForm : BaseForm
    {
        public ChungTuCollection collection = new ChungTuCollection();

        //Ngonnt 25/02
        public int countHoaDon = 0, countHopDong = 0, countVanTai = 0, countGiayPhep = 0, countCO = 0, countChuyenCK = 0, countGKT = 0, countCTNo, countCTGD = 0, countBanKe = 0;
        //Ngonnt 25/02

        public string GiayTo = "";
        private Image img;
        public GiayToForm()
        {
            InitializeComponent();

        }
        private byte[] GetArrayFromImagen(Image imagen)
        {
            MemoryStream ms = new MemoryStream();
            imagen.Save(ms, ImageFormat.Jpeg);
            byte[] matriz = ms.ToArray();

            return matriz;
        }
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            FileStream file = null;
            Image img = null;

            #region Hop Dong
            if (txtBCHopDong.Text != "0" || txtBSHopDong.Text != "0")
            {
                GiayTo += "Hop Dong TM";
                if (txtBCHopDong.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCHopDong.Text + " ";
                if (txtBSHopDong.Text != "0")
                    GiayTo += " Ban Sao " + txtBSHopDong.Text + " ";
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 1)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 1;
                ctHD.TenChungTu = "Hợp đồng thương mại";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCHopDong.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSHopDong.Text);
                //if (FileHopDong.Text != "")
                //{
                //    //img = Image.FromFile(FileHopDong.Text);
                //    //ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileHopDong.Text;                 
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 1)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region Hoa Don
            if (txtBCHoaDon.Text != "0" || txtBSHoaDon.Text != "0")
            {
                GiayTo += "Hoa Don TM";
                if (txtBCHoaDon.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCHoaDon.Text + " "; ;
                if (txtBSHoaDon.Text != "0")
                    GiayTo += " Ban Sao " + txtBSHoaDon.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 2)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 2;
                ctHD.TenChungTu = "Hóa đơn thương mại";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCHoaDon.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSHoaDon.Text);
                //if (FileHoaDon.Text.Trim() != "")
                //{
                //    //img = Image.FromFile(FileHoaDon.Text);
                //    //ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileHoaDon.Text;                  
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 2)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region Ban Ke
            if (txtBCBanKe.Text != "0" || txtBSBanKe.Text != "0")
            {
                GiayTo += " Ban ke";
                if (txtBCBanKe.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCBanKe.Text + " "; ;
                if (txtBSBanKe.Text != "0")
                    GiayTo += " Ban Sao " + txtBSBanKe.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 3)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 3;
                ctHD.TenChungTu = "Bản kê chi tiết";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCBanKe.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSBanKe.Text);
                //if (FileBanKe.Text.Trim() != "")
                //{
                //    //img = Image.FromFile(FileHoaDon.Text);
                //    //ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileBanKe.Text;                    
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 3)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region Van Tai
            if (txtBCVanTai.Text != "0" || txtBSVanTai.Text != "0")
            {
                GiayTo += "Van tai don";
                if (txtBCVanTai.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCVanTai.Text + " "; ;
                if (txtBSVanTai.Text != "0")
                    GiayTo += " Ban Sao " + txtBSVanTai.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 4)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 4;
                ctHD.TenChungTu = "Vận tải đơn";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCVanTai.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSVanTai.Text);
                //if (FileVanTai.Text.Trim() != "")
                //{
                //    //img = Image.FromFile(FileVanTai.Text);
                //    //ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileVanTai.Text;                    
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 4)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region Loai Khac
            if (txtLoaiKhac1.Text.Trim() != "")
            {
                GiayTo += txtLoaiKhac1.Text.Trim();
                if (txtBCLoaiKhac1.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCLoaiKhac1.Text + " "; ;
                if (txtBSLoaiKhac1.Text != "0")
                    GiayTo += " Ban Sao " + txtBSLoaiKhac1.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 5)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 5;
                ctHD.TenChungTu = txtLoaiKhac1.Text.Trim();
                ctHD.SoBanChinh = Convert.ToInt16(txtBCLoaiKhac1.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSLoaiKhac1.Text);
                //if (FileLoaiKhac.Text.Trim() != "")
                //{
                //    img = Image.FromFile(FileLoaiKhac.Text);
                //    ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileLoaiKhac.Text;                    
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 5)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region Giay Phep
            if (txtBCGiayPhep.Text != "0" || txtBSGiayPhep.Text != "0")
            {
                GiayTo += "Giay Phep";
                if (txtBCGiayPhep.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCGiayPhep.Text + " "; ;
                if (txtBSGiayPhep.Text != "0")
                    GiayTo += " Ban Sao " + txtBSGiayPhep.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 6)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 6;
                ctHD.TenChungTu = "Giấy phép";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCGiayPhep.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSGiayPhep.Text);
                //if (FileGiayPhep.Text.Trim() != "")
                //{
                //    img = Image.FromFile(FileGiayPhep.Text);
                //    ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileGiayPhep.Text;                    
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 6)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region CO
            if (txtBCCO.Text != "0" || txtBSCO.Text != "0")
            {
                GiayTo += "CO";
                if (txtBCCO.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCCO.Text + " "; ;
                if (txtBSCO.Text != "0")
                    GiayTo += " Ban Sao " + txtBSCO.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 7)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 7;
                ctHD.TenChungTu = "CO";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCCO.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSCO.Text);
                //if (FileCO.Text.Trim() != "")
                //{
                //    img = Image.FromFile(FileCO.Text);
                //    ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileCO.Text;                    
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 7)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region Chuyen Cua Khau
            if (txtBCChuyenCuaKhau.Text != "0" || txtBSChuyenCuaKhau.Text != "0")
            {
                GiayTo += "Chuyen Cua Khau";
                if (txtBCChuyenCuaKhau.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCChuyenCuaKhau.Text + " "; ;
                if (txtBSChuyenCuaKhau.Text != "0")
                    GiayTo += " Ban Sao " + txtBSChuyenCuaKhau.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 8)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 8;
                ctHD.TenChungTu = "Chuyển cửa khẩu";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCChuyenCuaKhau.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSChuyenCuaKhau.Text);
                //if (FileChuyen Cua Khau.Text.Trim() != "")
                //{
                //    img = Image.FromFile(FileChuyen Cua Khau.Text);
                //    ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileChuyen Cua Khau.Text;                    
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 8)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region Giay Kiem Tra
            if (txtBCGiayKiemTra.Text != "0" || txtBSGiayKiemTra.Text != "0")
            {
                GiayTo += "Giay Kiem Tra";
                if (txtBCGiayKiemTra.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCGiayKiemTra.Text + " "; ;
                if (txtBSGiayKiemTra.Text != "0")
                    GiayTo += " Ban Sao " + txtBSGiayKiemTra.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 9)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 9;
                ctHD.TenChungTu = "Giấy kiểm tra";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCGiayKiemTra.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSGiayKiemTra.Text);
                //if (FileGiayKiemTra.Text.Trim() != "")
                //{
                //    img = Image.FromFile(FileGiayKiemTra.Text);
                //    ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileGiayKiemTra.Text;                    
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 9)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region Chung Thu Giam Dinh
            if (txtBCChungThuGiamDinh.Text != "0" || txtBSChungThuGiamDinh.Text != "0")
            {
                GiayTo += "Chung Thu Giam Dinh";
                if (txtBCChungThuGiamDinh.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCChungThuGiamDinh.Text + " "; ;
                if (txtBSChungThuGiamDinh.Text != "0")
                    GiayTo += " Ban Sao " + txtBSChungThuGiamDinh.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 10)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 10;
                ctHD.TenChungTu = "Chứng thư giám định";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCChungThuGiamDinh.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSChungThuGiamDinh.Text);
                //if (FileChungThuGiamDinh.Text.Trim() != "")
                //{
                //    img = Image.FromFile(FileChungThuGiamDinh.Text);
                //    ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileChungThuGiamDinh.Text;                    
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 10)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            #region Chung Tu No
            if (txtBCChungTuNo.Text != "0" || txtBSChungTuNo.Text != "0")
            {
                GiayTo += "Chung Tu No";
                if (txtBCChungTuNo.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCChungTuNo.Text + " "; ;
                if (txtBSChungTuNo.Text != "0")
                    GiayTo += " Ban Sao " + txtBSChungTuNo.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 11)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 11;
                ctHD.TenChungTu = "Chứng từ nợ";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCChungTuNo.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSChungTuNo.Text);
                //if (FileChungThuGiamDinh.Text.Trim() != "")
                //{
                //    img = Image.FromFile(FileChungThuGiamDinh.Text);
                //    ctHD.NoiDung = GetArrayFromImagen(img);
                //    ctHD.FileUpLoad = FileChungThuGiamDinh.Text;                    
                //}
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 11)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            #endregion

            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChungTuForm_Load(object sender, EventArgs e)
        {
            //Ngonnt 25/02
            txtBCHoaDon.Text = countHoaDon.ToString();
            txtBCHopDong.Text = countHopDong.ToString();
            txtBCVanTai.Text = countVanTai.ToString();
            txtBCGiayPhep.Text = countGiayPhep.ToString();
            txtBCCO.Text = countCO.ToString();
            txtBCChuyenCuaKhau.Text = countChuyenCK.ToString();
            txtBCChungTuNo.Text = countCTNo.ToString();
            txtBCGiayKiemTra.Text = countGKT.ToString();
            txtBCChungThuGiamDinh.Text = countCTGD.ToString();
            txtBCBanKe.Text = countBanKe.ToString();
            //Ngonnt 25/02

            if (this.OpenType == OpenFormType.View)
            {
                btnAddNew.Visible = false;
            }
            foreach (ChungTu ct in collection)
            {
                //hợp đồng
                if (ct.LoaiCT == 1)
                {
                    txtBCHopDong.Text = ct.SoBanChinh.ToString();
                    txtBSHopDong.Text = ct.SoBanSao.ToString();
                    //FileHopDong.Text = ct.FileUpLoad;
                }
                //hóa đơn
                else if (ct.LoaiCT == 2)
                {
                    txtBCHoaDon.Text = ct.SoBanChinh.ToString();
                    txtBSHoaDon.Text = ct.SoBanSao.ToString();
                    //FileHoaDon.Text = ct.FileUpLoad;
                }
                else if (ct.LoaiCT == 3)
                {
                    txtBCBanKe.Text = ct.SoBanChinh.ToString();
                    txtBSBanKe.Text = ct.SoBanSao.ToString();
                    //FileBanKe.Text = ct.FileUpLoad;
                }
                else if (ct.LoaiCT == 4)
                {
                    txtBCVanTai.Text = ct.SoBanChinh.ToString();
                    txtBSVanTai.Text = ct.SoBanSao.ToString();
                    //FileVanTai.Text = ct.FileUpLoad;
                }
                else if (ct.LoaiCT == 5)
                {
                    txtBCLoaiKhac1.Text = ct.SoBanChinh.ToString();
                    txtBSLoaiKhac1.Text = ct.SoBanSao.ToString();
                    txtLoaiKhac1.Text = ct.TenChungTu;
                    //FileLoaiKhac.Text = ct.FileUpLoad;
                }
                else if (ct.LoaiCT == 6)
                {
                    txtBCGiayPhep.Text = ct.SoBanChinh.ToString();
                    txtBSGiayPhep.Text = ct.SoBanSao.ToString();
                    //FileBanKe.Text = ct.FileUpLoad;
                }
                else if (ct.LoaiCT == 7)
                {
                    txtBCCO.Text = ct.SoBanChinh.ToString();
                    txtBSCO.Text = ct.SoBanSao.ToString();
                    //FileBanKe.Text = ct.FileUpLoad;
                }
                else if (ct.LoaiCT == 8)
                {
                    txtBCChuyenCuaKhau.Text = ct.SoBanChinh.ToString();
                    txtBSChuyenCuaKhau.Text = ct.SoBanSao.ToString();
                    //FileBanKe.Text = ct.FileUpLoad;
                }
                else if (ct.LoaiCT == 9)
                {
                    txtBCGiayKiemTra.Text = ct.SoBanChinh.ToString();
                    txtBSGiayKiemTra.Text = ct.SoBanSao.ToString();
                    //FileBanKe.Text = ct.FileUpLoad;
                }
                else if (ct.LoaiCT == 10)
                {
                    txtBCChungThuGiamDinh.Text = ct.SoBanChinh.ToString();
                    txtBSChungThuGiamDinh.Text = ct.SoBanSao.ToString();
                    //FileBanKe.Text = ct.FileUpLoad;
                }
                else if (ct.LoaiCT == 11)
                {
                    txtBCChungTuNo.Text = ct.SoBanChinh.ToString();
                    txtBSChungTuNo.Text = ct.SoBanSao.ToString();
                    //FileBanKe.Text = ct.FileUpLoad;
                }
            }
            openFile.InitialDirectory = Application.StartupPath;
        }

        private void editBox1_ButtonClick(object sender, EventArgs e)
        {
            EditBox txtChungTu = (EditBox)sender;
            DialogResult kq = openFile.ShowDialog(this);
            if (kq == DialogResult.OK)
            {
                txtChungTu.Text = openFile.FileName;
            }
        }



    }
}
