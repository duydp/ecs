﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.Send;
using System.Runtime.InteropServices;
using System.Threading;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;

namespace Company.Interface
{
    public partial class SendVNACCFrm : BaseForm
    {
        public string msgSend;
        public ResponseVNACCS feedback;
        MessagesSend messagesSend;
        public SendVNACCFrm(MessagesSend MessagesSend)
        {
            try
            {
                InitializeComponent();
                this.messagesSend = MessagesSend;
                this.msgSend = HelperVNACCS.BuildEdiMessages(MessagesSend).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
                this.Close();
            }
            
        }
//        public string MessagesSend;
//        public string result = string.Empty;
//        public string boundary;
//        private string sendRequest(string url, string method, string postdata)
//        {
//            WebRequest rqst = HttpWebRequest.Create(url);
//            rqst.Method = method;
//            if (!String.IsNullOrEmpty(postdata))
//            {
//                rqst.ContentType = "multipart/signed";
//                rqst.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                
//                byte[] byteData = UTF8Encoding.UTF8.GetBytes(postdata);
//                rqst.ContentLength = byteData.Length;
//                using (Stream postStream = rqst.GetRequestStream())
//                {
//                    postStream.Write(byteData, 0, byteData.Length);
//                    postStream.Close();
//                }
//            }
//            ((HttpWebRequest)rqst).KeepAlive = false;
//            StreamReader rsps = new StreamReader(rqst.GetResponse().GetResponseStream());
//            string strRsps = rsps.ReadToEnd();
//            return strRsps;

//        }
//        private void LoadServices()
//        {
//            string url = "https://ediconn.vnaccs.customs.gov.vn/test";
//            HttpWebRequest httprequest = (HttpWebRequest)WebRequest.Create(url);
//            string st = sendRequest(url, "POST", this.MessagesSend);
//            string st2 = PostMultipleFiles(url, this.MessagesSend, boundary);
            
//        }

//        void client_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
//        {
//            if (e.Error == null)
//            {
//                result = e.Result;
//                this.Close();
//            }
//            else
//            {
//                result = string.Empty;
//                MessageBox.Show(e.Error.Message);
//                this.Close();
//            }
//        }


//        void client_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
//        {
//            lblTrangThai.Text = string.Format("{0}    uploaded {1} of {2} bytes. {3} % complete...",
//                (string)e.UserState,
//                e.BytesSent,
//                e.TotalBytesToSend,
//                (e.ProgressPercentage - 50) * 2);
//            prcbar.EditValue = (e.ProgressPercentage - 50) * 2;
//        }

//        private void SendVNACCFrm_Load(object sender, EventArgs e)
//        {
//            if (string.IsNullOrEmpty(MessagesSend))
//            {
//                this.Close();
//            }
//            else
//            {
//                LoadServices();
//            }
//        }
//        private void LoadServicesHttp()
//        {

//        }
//        #region test
//        public void PostMultipleFiles(string url, string[] files)
//        {
//            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
//            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//            httpWebRequest.ContentType = "multipart/form-data; boundary=" + boundary;
//            httpWebRequest.Method = "POST";
//            httpWebRequest.KeepAlive = true;
//            httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
//            Stream memStream = new System.IO.MemoryStream();
//            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
//            string formdataTemplate = "\r\n--" + boundary + "\r\nContent-Disposition:  form-data; name=\"{0}\";\r\n\r\n{1}";
//            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n Content-Type: application/octet-stream\r\n\r\n";
//            memStream.Write(boundarybytes, 0, boundarybytes.Length);
//            for (int i = 0; i < files.Length; i++)
//            {
//                string header = string.Format(headerTemplate, "file" + i, files[i]);
//                byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
//                memStream.Write(headerbytes, 0, headerbytes.Length);
//                FileStream fileStream = new FileStream(files[i], FileMode.Open,
//                FileAccess.Read);
//                byte[] buffer = new byte[1024];
//                int bytesRead = 0;
//                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
//                {
//                    memStream.Write(buffer, 0, bytesRead);
//                }
//                memStream.Write(boundarybytes, 0, boundarybytes.Length);
//                fileStream.Close();
//            }
//            httpWebRequest.ContentLength = memStream.Length;
//            Stream requestStream = httpWebRequest.GetRequestStream();
//            memStream.Position = 0;
//            byte[] tempBuffer = new byte[memStream.Length];
//            memStream.Read(tempBuffer, 0, tempBuffer.Length);
//            memStream.Close();
//            requestStream.Write(tempBuffer, 0, tempBuffer.Length);
//            requestStream.Close();
//            try
//            {
//                WebResponse webResponse = httpWebRequest.GetResponse();
//                Stream stream = webResponse.GetResponseStream();
//                StreamReader reader = new StreamReader(stream);
//                string var = reader.ReadToEnd();

//            }
//            catch (Exception ex)
//            {
//                Logger.LocalLogger.Instance().WriteMessage(ex);
//                throw ex;
//            }
//            httpWebRequest = null;
//        }
//        public string PostMultipleFiles(string url, string mess, string boundary)
//        {
//            //string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
//            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//            //WebRequest httpWebRequest = WebRequest.Create(url);
//            httpWebRequest.ContentType = "multipart/signed; micalg = sha1;boundary=" + boundary + ";protocol=application/x-pkcs7-signature";
//            httpWebRequest.Method = "POST";
//            httpWebRequest.KeepAlive = false;
//            httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
//            httpWebRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
//            httpWebRequest.UserAgent = Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma;
//            httpWebRequest.ProtocolVersion = HttpVersion.Version10;
//;            Stream memStream = new System.IO.MemoryStream();
//            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
//            //string formdataTemplate = "\r\n--" + boundary + "\r\nContent-Disposition:  form-data; name=\"{0}\";\r\n\r\n{1}";
//            //string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n Content-Type: application/octet-stream\r\n\r\n";
//            memStream.Write(boundarybytes, 0, boundarybytes.Length);
//            //for (int i = 0; i < files.Length; i++)
//            //{
//            //    string header = string.Format(headerTemplate, "file" + i, files[i]);
//            //    //string header = string.Format(headerTemplate, "uplTheFile", files[i]);
//            //    byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
//            //    memStream.Write(headerbytes, 0, headerbytes.Length);
//            //    FileStream fileStream = new FileStream(files[i], FileMode.Open,
//            //    FileAccess.Read);
//            //    byte[] buffer = new byte[1024];
//            //    int bytesRead = 0;
//            //    while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
//            //    {
//            //        memStream.Write(buffer, 0, bytesRead);
//            //    }
//            //    memStream.Write(boundarybytes, 0, boundarybytes.Length);
//            //    fileStream.Close();
//            //}
//            byte[] data =  UTF8Encoding.UTF8.GetBytes(mess);
//            memStream.Write(data, 0, data.Length);
            
           
//            httpWebRequest.ContentLength = memStream.Length;
//            Stream requestStream = httpWebRequest.GetRequestStream();
//            memStream.Position = 0;
//            byte[] tempBuffer = new byte[memStream.Length];
//            memStream.Read(tempBuffer, 0, tempBuffer.Length);
//            memStream.Close();
//            requestStream.Write(tempBuffer, 0, tempBuffer.Length);
//            requestStream.Close();
//            try
//            {
//                WebResponse webResponse = httpWebRequest.GetResponse();
//                Stream stream = webResponse.GetResponseStream();
//                StreamReader reader = new StreamReader(stream);
//                string var = reader.ReadToEnd();
//                return var;
//            }
//            catch (Exception ex)
//            {
//                Logger.LocalLogger.Instance().WriteMessage(ex);
//                throw ex;
//            }
//            //httpWebRequest = null;
//        }

//        #endregion


        #region     Send messages
        public string msgFeedBack;
        public void setCaption(string str)
        {
            if(this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblStatus.Text = str; }));
            }
            else
                lblStatus.Text = str;
        }
        private byte[] postData;
        public void SendMessEdi(object obj)
        {
            setCaption("đính kèm chữ ký số vào messages");
            System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = null;
             if (Company.KDT.SHARE.Components.Globals.IsSignature)
             {
                 try
                 {
                     if (!string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName))
                     {
                         x509Certificate2 = Company.KDT.SHARE.Components.Cryptography.GetStoreX509Certificate2(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName);
                     }
                     else
                     {
                         msgFeedBack = "Chưa cấu hình chữ ký số";
                         this.DialogResult = DialogResult.Cancel;
                         return;
                     }
                 }
                 catch (System.Exception ex)
                 {
                     msgFeedBack = "Lỗi load chữ ký số: " + ex.Message;
                     this.DialogResult = DialogResult.Cancel;
                     return;
                 }

             }
             else
             {
                 msgFeedBack = "Chưa cấu hình chữ ký số";
                 this.DialogResult = DialogResult.Cancel;
             }
             if (x509Certificate2 == null)
             {
                 msgFeedBack = "Không tìm thấy chữ ký số ";
                 this.DialogResult = DialogResult.Cancel;
                 return;
             }
             try
             {

       
            MIME mess = HelperVNACCS.GetMIME(msgSend, x509Certificate2);
            Thread.Sleep(300)   ;
            setCaption("Đóng gói messages VNACCS EDI");
            string boundary = mess.boundary;
            //boundary = mime.Boundary;
            //mess = mime.GetMime();
            //mess = mime.GetEntireBody();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(GlobalVNACC.Url);
   
            if (string.IsNullOrEmpty(boundary))
            {
                msgFeedBack = "Boundary bị rỗng";
                this.DialogResult = DialogResult.Cancel;
                return;
            }
            postData = UTF8Encoding.UTF8.GetBytes(mess.GetMessages().ToString());
            boundary = boundary.Substring(2, boundary.Length - 2);
            httpWebRequest.ContentType = "multipart/signed; micalg = \"sha1\";boundary=\"" + boundary + "\";protocol=\"application/x-pkcs7-signature\"";
            httpWebRequest.Method = "POST";
            httpWebRequest.KeepAlive = false;
            httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
            httpWebRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
            httpWebRequest.UserAgent = "SOFTECH";
            httpWebRequest.ProtocolVersion = HttpVersion.Version10;
            httpWebRequest.ContentLength = postData.Length;
            httpWebRequest.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), httpWebRequest);
            //LoadingProcess(); 
             }
             catch (Exception ex)
             {
                 msgFeedBack = ex.Message;
                 this.DialogResult = DialogResult.Cancel;
                 return;
             }
        }
        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                setCaption("Đang gửi dữ liệu...");
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
                
                // End the operation
                Stream postStream = request.EndGetRequestStream(asynchronousResult);

                //Console.WriteLine("Please enter the input data to be posted:");
                //string postData = Console.ReadLine();

                // Convert the string into a byte array. 
                // byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Write to the request stream.
                postStream.Write(postData, 0, postData.Length);
                postStream.Close();

                // Start the asynchronous operation to get the response
                request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
            }
            catch (Exception e)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { /*button1.Enabled = true;*/ MessageBox.Show(e.Message); /*UnLoadingProcess();*/ }));
                }

            }

        }

        private void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                setCaption("Đang nhận dữ liệu...");
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

                // End the operation
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse,Encoding.UTF8);
                string responseString = streamRead.ReadToEnd();
                Company.KDT.SHARE.VNACCS.HelperVNACCS.SaveFileEdi(Application.StartupPath, new StringBuilder(responseString), "Return" + DateTime.Now.ToString("yyyyMMddhhmmss"));
//                 if (InvokeRequired)
//                 {
//                     this.Invoke(new MethodInvoker(delegate { txtResult.Text = responseString; UnLoadingProcess(); }));
//                 }
//                 else
//                     textBox2.Text = responseString;
                //Console.WriteLine(responseString);
                // Close the stream object
                streamResponse.Close();
                streamRead.Close();

                // Release the HttpWebResponse
                response.Close();
                //allDone.Set();
                //msgFeedBack = responseString;
                MsgLog.SaveMessages(responseString, this.messagesSend.Header.MaNghiepVu.GetValue().ToString() + " Return", 0, EnumThongBao.ReturnMess, "", this.messagesSend.Header.MessagesTag.GetValue().ToString(), this.messagesSend.Header.InputMessagesID.GetValue().ToString(), this.messagesSend.Header.IndexTag.GetValue().ToString());
                feedback = new ResponseVNACCS(responseString);
                msgFeedBack = feedback.GetError();
                if (feedback.Error != null && feedback.Error.Count > 0)
                {
                    Helper.Controls.ErrorMessageBoxForm_VNACC.ShowMessage(messagesSend.Header.MaNghiepVu.GetValue().ToString(),feedback.Error, false, Helper.Controls.MessageType.Error);
                    this.DialogResult = DialogResult.No;
                }
                this.DialogResult = DialogResult.Yes;
            }
            catch (Exception e)
            {
                msgFeedBack = e.Message;
                this.DialogResult = DialogResult.Cancel;
            }
            //if (InvokeRequired)
            //{
            //    this.Invoke(new MethodInvoker(delegate { button1.Enabled = true; UnLoadingProcess(); }));
            //}
            //else
            //    button1.Enabled = true;

        }
        #endregion

        private void SendVNACCFrm_Load(object sender, EventArgs e)
        {
            progressBar1.MarqueeAnimationSpeed = 50;
            ThreadPool.QueueUserWorkItem(SendMessEdi);
        }
      
    }
   
}
