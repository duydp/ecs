﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class GC_HopDong:DeclarationBase
    {
        /// <summary>
        /// chi tiết hợp đồng
        /// </summary>
        [XmlElement("ContractDocument")]
        public ContractDocument ContractDocument { set; get; }

    }
}
