﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
namespace ECS.AutoUpdate
{
    public sealed class UnzipFile
    {
        static UnzipFile install = null;
        private IUnZip iUnzip;
        static UnzipFile()
        {
            install = new UnzipFile();
            if (install.iUnzip == null)
                install.iUnzip = (IUnZip)Activator.CreateInstance
                         (Type.GetType(install.GetType().Namespace + "." + Config.Install.ClassUnZip));

        }
        public static bool Unzip(DownloadFileInfo fileInfo)
        {
            return install.iUnzip.UnZip(fileInfo);
        }
        public static void BeforeUnzip()
        {
            install.iUnzip.BeforeUnzip();
        }
        public static void AfterUnzip()
        {
            install.iUnzip.AfterUnzip();
        }
    }
    class EcsUnzip : IUnZip
    {
        string folderConfig = string.Empty;

        public EcsUnzip()
        {
            folderConfig = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ConfigDoanhNghiep\\");
        }
        private void KillProcess(string[] arrProc)
        {
            foreach (string strProcname in arrProc)
            {
                string strTemp = strProcname.ToLower().Trim().Replace(".exe", string.Empty);
                foreach (Process process in Process.GetProcesses())
                {
                    if (process.ProcessName.ToLower() == strTemp)
                    {
                        process.Kill();
                        break;
                    }
                }
            }
        }
        private bool ExistProcess(string procName)
        {

            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName(procName.Replace(".exe", string.Empty));
            return process.Length > 0;

        }
        public void BeforeUnzip()
        {
            try
            {
                string[] arrProc = new string[]{
                    "SOFTECH.ECS.TQDT.SXXK.exe",
                    "SOFTECH.ECS.TQDT.GC.exe",
                    "SOFTECH.ECS.TQDT.KD.exe",
                    "MiniSqlQuery.exe"
                };

                BackupFile(folderConfig + "ConfigDoanhNghiep.xml");
                BackupFile(CommonUnitity.SystemBinUrl + "Config.xml");
                //Updaetd by Hungtq, 14/09/2012.
                if (System.IO.File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile))
                {
                    BackupFile(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                }

                foreach (string strProcName in arrProc)
                {
                    if (ExistProcess(strProcName))
                    {
                        //System.Windows.Forms.MessageBox.Show("Phần mềm sẽ tự động đóng các ứng dụng đang thực thi",
                        //                                     "Thông báo",
                        //                                      System.Windows.Forms.MessageBoxButtons.OK,
                        //                                      System.Windows.Forms.MessageBoxIcon.Warning);
                        KillProcess(arrProc);
                        break;
                    }
                }


                //if (System.IO.File.Exists(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config"))
                //{
                //    BackupFile(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config");
                //    //pathFileExecute = CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config";
                //}
                //else if (System.IO.File.Exists(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.KD.exe.Config"))
                //{
                //    BackupFile(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.KD.exe.Config");
                //    //pathFileExecute = CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.KD.exe.Config";
                //}
                //else if (System.IO.File.Exists(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config"))
                //{
                //    BackupFile(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config");
                //    //pathFileExecute = CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.KD.exe.Config";
                //}
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("BeforeUnzip:\r\n", ex); }
        }

        private void SyncConfig(string oldPath, string newPath)
        {
            if (File.Exists(oldPath) && File.Exists(newPath))
            {
                System.Xml.XmlDocument newCfg = new System.Xml.XmlDocument();
                newCfg.Load(newPath);

                System.Xml.XmlDocument oldCfg = new System.Xml.XmlDocument();
                oldCfg.Load(oldPath);

                XmlNode oldNode = oldCfg.SelectSingleNode("configuration/connectionStrings/add");
                XmlNode newNode = newCfg.SelectSingleNode("configuration/connectionStrings/add");
                if (oldNode != null && newNode != null)
                {
                    newNode.Attributes["connectionString"].Value = oldNode.Attributes["connectionString"].Value;
                }
                XmlNodeList newNodes = newCfg.SelectNodes("configuration/applicationSettings");
                XmlNodeList oldNodes = oldCfg.SelectNodes("configuration/applicationSettings");
                for (int i = 0; i < ((newNodes[0]).ChildNodes).Count; i++)
                {
                    newNode = newNodes[0].ChildNodes[i];

                    for (int j = 0; j < newNode.ChildNodes.Count; j++)
                    {
                        XmlNode newNodeChild = newNode.ChildNodes[j];
                        XmlNode oldNodeChild = oldNodes[0].SelectSingleNode(newNode.Name + "/" + newNodeChild.Name + "[@name='" + newNodeChild.Attributes[0].Value + "']");
                        if (oldNodeChild != null)
                        {
                            if (newNodeChild.SelectSingleNode("//value") != null && oldNodeChild.SelectSingleNode("//value") != null)
                                newNodeChild.SelectSingleNode("//value").InnerText = oldNodeChild.SelectSingleNode("//value").InnerText;
                            else
                                newNodeChild.InnerText = oldNodeChild.InnerText;
                        }
                    }
                }
                newNodes = newCfg.SelectNodes("configuration/appSettings");
                oldNodes = oldCfg.SelectNodes("configuration/appSettings");
                for (int i = 0; i < ((newNodes[0]).ChildNodes).Count; i++)
                {
                    XmlNode newNodeChild = newNodes[0].ChildNodes[i];
                    if (newNodeChild.Name == "add")
                    {
                        if (newNodeChild.Attributes[0].Value == "WS_SyncData")
                        {
                            continue;
                        }

                        XmlNode oldNodeChild = oldNodes[0].SelectSingleNode(newNodeChild.Name + "[@key='" + newNodeChild.Attributes[0].Value + "']");
                        if (oldNodeChild != null)
                        {
                            for (int k = 0; k < newNodeChild.Attributes.Count; k++)
                            {
                                try
                                {
                                    string name = newNodeChild.Attributes[k].Name.Trim();
                                    newNodeChild.Attributes[name].Value = oldNodeChild.Attributes[name].Value;
                                }
                                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                            }
                        }
                    }
                }
                newCfg.Save(newPath);
            }
        }
        public void AfterUnzip()
        {
            try
            {
                if (File.Exists(folderConfig + "\\ConfigDoanhNghiep.xml.old") && File.Exists(folderConfig + "\\ConfigDoanhNghiep.xml"))
                {
                    XmlDocument configOld = new XmlDocument();
                    configOld.Load(folderConfig + "\\ConfigDoanhNghiep.xml.old");
                    XmlDocument configNew = new XmlDocument();

                    configNew.Load(folderConfig + "\\ConfigDoanhNghiep.xml");
                    foreach (XmlNode node in configNew.GetElementsByTagName("Config"))
                    {
                        XmlNode nodeOld = configOld.SelectSingleNode("/Configs/Config[Key='" + node["Key"].InnerText + "']");
                        if (nodeOld != null)
                            node["Value"].InnerText = nodeOld["Value"].InnerText;
                    }
                    configNew.Save(folderConfig + "\\ConfigDoanhNghiep.xml");
                }
                //SyncConfig(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.KD.exe.Config.old", CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.KD.exe.Config");
                //SyncConfig(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config.old", CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config");
                //SyncConfig(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config.old", CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config");

                //Updated by Hungtq, 14/09/2012.
                if (File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + ".old")
                    && File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile))
                    SyncConfig(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + ".old", AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            if (File.Exists(CommonUnitity.SystemBinUrl + "Config.xml.old") && File.Exists(CommonUnitity.SystemBinUrl + "Config.xml"))

                try
                {
                    XmlDocument docOld = new XmlDocument();
                    XmlDocument docNew = new XmlDocument();
                    docOld.Load(CommonUnitity.SystemBinUrl + "Config.xml.old");
                    docNew.Load(CommonUnitity.SystemBinUrl + "Config.xml");
                    XmlNode node = docOld.SelectSingleNode("Root/Type");
                    XmlNode nodeNew = docNew.SelectSingleNode("Root/Type");
                    nodeNew.InnerText = node.InnerText;
                    docNew.Save(CommonUnitity.SystemBinUrl + "Config.xml");
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            if (!File.Exists(CommonUnitity.SystemBinUrl + "Config.xml")) RollbackFile(CommonUnitity.SystemBinUrl + "Config.xml.old", CommonUnitity.SystemBinUrl + "Config.xml");
            if (!File.Exists(folderConfig + "\\ConfigDoanhNghiep.xml")) RollbackFile(folderConfig + "\\ConfigDoanhNghiep.xml.old", folderConfig + "\\ConfigDoanhNghiep.xml");
            //if (!File.Exists(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config")) RollbackFile(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config.old", CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config");
            //if (!File.Exists(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config")) RollbackFile(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config.old", CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config");
            //if (!File.Exists(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.KD.exe.Config")) RollbackFile(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.KD.exe.Config.old", CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.KD.exe.Config");

            if (!File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile)) RollbackFile(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + ".old", AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            //Update date release
            if (File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile))
                CommonUnitity.SaveNodeXmlAppSettings(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, "LastUpdated", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt"));

        }
        void BackupFile(string fileName)
        {
            if (File.Exists(fileName + ".old"))
                File.Delete(fileName + ".old");
            if (File.Exists(fileName)) File.Move(fileName, fileName + ".old");
        }
        private void RollbackFile(string oldFile, string newFile)
        {
            if (File.Exists(oldFile))
                System.IO.File.Move(oldFile, newFile);
        }
        public bool UnZip(DownloadFileInfo fileInfo)
        {
            try
            {
                string directoryUnzip = CommonUnitity.GetSubPath(fileInfo, CommonUnitity.SystemBinUrl);
                string pathFile = directoryUnzip + fileInfo.FileName.Replace(".zip", "");

                //Hungtq updated, 3/2/2013
                //Don't Delete data file
                try
                {
                    if (fileInfo.FileName.Equals("Company.KD.BLL.dll")
                        || fileInfo.FileName.Equals("Company.GC.BLL.dll")
                        || fileInfo.FileName.Equals("Company.BLL.dll")
                        || fileInfo.FileName.Equals("Company.KDT.SHARE.Components.dll")
                        || fileInfo.FileName.Equals("Company.KDT.SHARE.QuanLyChungTu.dll")
                        || fileInfo.FileName.Equals("SOFTECH.ECS.TQDT.KD.exe")
                        || fileInfo.FileName.Equals("SOFTECH.ECS.TQDT.GC.exe")
                        || fileInfo.FileName.Equals("SOFTECH.ECS.TQDT.SXXK.exe"))
                    {
                        if (System.IO.File.Exists(pathFile))
                        {
                            System.IO.File.Delete(pathFile);
                        }
                    }
                }
                catch (Exception e) { Logger.LocalLogger.Instance().WriteMessage("Delete file in ECS.AutoUpdater.AutoUpdateHelper.UnzipFile.Unzip:\r\n" + pathFile, e); }


                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("unzip.exe",
                                                            "-o \"" + CommonUnitity.GetZipPath(fileInfo) + "\" -d \"" + directoryUnzip);
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process p = System.Diagnostics.Process.Start(psi);
                p.WaitForExit();
                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("Unzip file", ex); return false; }
        }
    }

    class EcsKTXUnzip : IUnZip
    {
        string folderConfig = string.Empty;

        public EcsKTXUnzip()
        {
            folderConfig = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ConfigDoanhNghiep\\");
        }
        private void KillProcess(string[] arrProc)
        {
            foreach (string strProcname in arrProc)
            {
                string strTemp = strProcname.ToLower().Trim().Replace(".exe", string.Empty);
                foreach (Process process in Process.GetProcesses())
                {
                    if (process.ProcessName.ToLower() == strTemp)
                    {
                        process.Kill();
                        break;
                    }
                }
            }
        }
        private bool ExistProcess(string procName)
        {

            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName(procName.Replace(".exe", string.Empty));
            return process.Length > 0;

        }
        public void BeforeUnzip()
        {
            string[] arrProc = new string[]{
                "ECS_SXXK.exe",
                "ECS_GC.exe",
                "ECS_KD.exe",
                "MiniSqlQuery.exe"
            };

            foreach (string strProcName in arrProc)
            {
                if (ExistProcess(strProcName))
                {

                    System.Windows.Forms.MessageBox.Show("Phần mềm sẽ tự động đóng các ứng dụng đang thực thi",
                                                         "Thông báo",
                                                          System.Windows.Forms.MessageBoxButtons.OK,
                                                          System.Windows.Forms.MessageBoxIcon.Warning);
                    KillProcess(arrProc);
                    break;
                }
            }

            BackupFile(folderConfig + "ConfigDoanhNghiep.xml");
            BackupFile(CommonUnitity.SystemBinUrl + "Config.xml");
            //Updaetd by Hungtq, 14/09/2012.
            if (System.IO.File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile))
            {
                BackupFile(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            }
        }
        private void SyncConfig(string oldPath, string newPath)
        {
            if (File.Exists(oldPath) && File.Exists(newPath))
            {
                System.Xml.XmlDocument newCfg = new System.Xml.XmlDocument();
                newCfg.Load(newPath);

                System.Xml.XmlDocument oldCfg = new System.Xml.XmlDocument();
                oldCfg.Load(oldPath);

                XmlNode oldNode = oldCfg.SelectSingleNode("configuration/connectionStrings/add");
                XmlNode newNode = newCfg.SelectSingleNode("configuration/connectionStrings/add");
                if (oldNode != null && newNode != null)
                {
                    newNode.Attributes["connectionString"].Value = oldNode.Attributes["connectionString"].Value;
                }
                XmlNodeList newNodes = newCfg.SelectNodes("configuration/applicationSettings");
                XmlNodeList oldNodes = oldCfg.SelectNodes("configuration/applicationSettings");
                for (int i = 0; i < ((newNodes[0]).ChildNodes).Count; i++)
                {
                    newNode = newNodes[0].ChildNodes[i];

                    for (int j = 0; j < newNode.ChildNodes.Count; j++)
                    {
                        XmlNode newNodeChild = newNode.ChildNodes[j];
                        XmlNode oldNodeChild = oldNodes[0].SelectSingleNode(newNode.Name + "/" + newNodeChild.Name + "[@name='" + newNodeChild.Attributes[0].Value + "']");
                        if (oldNodeChild != null)
                        {
                            if (newNodeChild.SelectSingleNode("//value") != null && oldNodeChild.SelectSingleNode("//value") != null)
                                newNodeChild.SelectSingleNode("//value").InnerText = oldNodeChild.SelectSingleNode("//value").InnerText;
                            else
                                newNodeChild.InnerText = oldNodeChild.InnerText;
                        }
                    }
                }
                newNodes = newCfg.SelectNodes("configuration/appSettings");
                oldNodes = oldCfg.SelectNodes("configuration/appSettings");
                for (int i = 0; i < ((newNodes[0]).ChildNodes).Count; i++)
                {
                    XmlNode newNodeChild = newNodes[0].ChildNodes[i];
                    if (newNodeChild.Name == "add")
                    {
                        if (newNodeChild.Attributes[0].Value == "WS_SyncData")
                        {
                            continue;
                        }

                        XmlNode oldNodeChild = oldNodes[0].SelectSingleNode(newNodeChild.Name + "[@key='" + newNodeChild.Attributes[0].Value + "']");
                        if (oldNodeChild != null)
                        {
                            for (int k = 0; k < newNodeChild.Attributes.Count; k++)
                            {
                                try
                                {
                                    string name = newNodeChild.Attributes[k].Name.Trim();
                                    newNodeChild.Attributes[name].Value = oldNodeChild.Attributes[name].Value;
                                }
                                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                            }
                        }
                    }
                }
                newCfg.Save(newPath);
            }
        }
        public void AfterUnzip()
        {
            try
            {
                if (File.Exists(folderConfig + "\\ConfigDoanhNghiep.xml.old") && File.Exists(folderConfig + "\\ConfigDoanhNghiep.xml"))
                {
                    XmlDocument configOld = new XmlDocument();
                    configOld.Load(folderConfig + "\\ConfigDoanhNghiep.xml.old");
                    XmlDocument configNew = new XmlDocument();

                    configNew.Load(folderConfig + "\\ConfigDoanhNghiep.xml");
                    foreach (XmlNode node in configNew.GetElementsByTagName("Config"))
                    {
                        XmlNode nodeOld = configOld.SelectSingleNode("/Configs/Config[Key='" + node["Key"].InnerText + "']");
                        if (nodeOld != null)
                            node["Value"].InnerText = nodeOld["Value"].InnerText;
                    }
                    configNew.Save(folderConfig + "\\ConfigDoanhNghiep.xml");
                }

                //Updated by Hungtq, 14/09/2012.
                SyncConfig(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + ".old", AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            if (File.Exists(CommonUnitity.SystemBinUrl + "Config.xml.old") && File.Exists(CommonUnitity.SystemBinUrl + "Config.xml"))

                try
                {
                    XmlDocument docOld = new XmlDocument();
                    XmlDocument docNew = new XmlDocument();
                    docOld.Load(CommonUnitity.SystemBinUrl + "Config.xml.old");
                    docNew.Load(CommonUnitity.SystemBinUrl + "Config.xml");
                    XmlNode node = docOld.SelectSingleNode("Root/Type");
                    XmlNode nodeNew = docNew.SelectSingleNode("Root/Type");
                    nodeNew.InnerText = node.InnerText;
                    docNew.Save(CommonUnitity.SystemBinUrl + "Config.xml");
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            if (!File.Exists(CommonUnitity.SystemBinUrl + "Config.xml")) RollbackFile(CommonUnitity.SystemBinUrl + "Config.xml.old", CommonUnitity.SystemBinUrl + "Config.xml");
            if (!File.Exists(folderConfig + "\\ConfigDoanhNghiep.xml")) RollbackFile(folderConfig + "\\ConfigDoanhNghiep.xml.old", folderConfig + "\\ConfigDoanhNghiep.xml");

            if (!File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile)) RollbackFile(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + ".old", AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            //Update date release
            if (File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile))
                CommonUnitity.SaveNodeXmlAppSettings(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, "LastUpdated", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt"));

        }
        void BackupFile(string fileName)
        {
            if (File.Exists(fileName + ".old"))
                File.Delete(fileName + ".old");
            if (File.Exists(fileName)) File.Move(fileName, fileName + ".old");
        }
        private void RollbackFile(string oldFile, string newFile)
        {
            if (File.Exists(oldFile))
                System.IO.File.Move(oldFile, newFile);
        }
        public bool UnZip(DownloadFileInfo fileInfo)
        {
            try
            {
                string directoryUnzip = CommonUnitity.GetSubPath(fileInfo, CommonUnitity.SystemBinUrl);
                string pathFile = directoryUnzip + fileInfo.FileName.Replace(".zip", "");

                //Hungtq updated, 3/2/2013
                //Don't Delete data file
                try
                {
                    if ((fileInfo.FileName.Contains(".mdf")
                        || fileInfo.FileName.Contains(".ldf")))
                    {
                        return false;
                    }
                }
                catch (Exception e) { Logger.LocalLogger.Instance().WriteMessage(pathFile, e); }

                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("unzip.exe",
                                                            "-o \"" + CommonUnitity.GetZipPath(fileInfo) + "\" -d \"" + directoryUnzip);
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process p = System.Diagnostics.Process.Start(psi);
                p.WaitForExit();
                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return false; }
        }
    }

    class HRSOnlineUnzip : IUnZip
    {
        public HRSOnlineUnzip()
        {
        }
        private void KillProcess(string[] arrProc)
        {

            foreach (string strProcname in arrProc)
            {
                string strTemp = strProcname.ToLower().Trim().Replace(".exe", string.Empty);
                foreach (Process process in Process.GetProcesses())
                {
                    if (process.ProcessName.ToLower() == strTemp)
                    {
                        process.Kill();
                        break;
                    }
                }
            }
        }
        private bool ExistProcess(string procName)
        {

            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName(procName.Replace(".exe", string.Empty));
            return process.Length > 0;

        }
        public void BeforeUnzip()
        {
            string[] arrProc = new string[]{
                "Softech.HRS.Online.exe"               
            };

            foreach (string strProcName in arrProc)
            {
                if (ExistProcess(strProcName))
                {

                    System.Windows.Forms.MessageBox.Show("Phần mềm sẽ tự động đóng các ứng dụng đang thực thi",
                                                         "Thông báo",
                                                          System.Windows.Forms.MessageBoxButtons.OK,
                                                          System.Windows.Forms.MessageBoxIcon.Warning);
                    KillProcess(arrProc);
                    break;
                }
            }
            //BackupFile(CommonUnitity.SystemBinUrl + "Config.xml");
            BackupFile(CommonUnitity.SystemBinUrl + "Softech.HRS.Online.exe.Config");

        }
        private void SyncConfig(string oldPath, string newPath)
        {
            if (File.Exists(oldPath) && File.Exists(newPath))
            {
                System.Xml.XmlDocument newCfg = new System.Xml.XmlDocument();
                newCfg.Load(newPath);

                System.Xml.XmlDocument oldCfg = new System.Xml.XmlDocument();
                oldCfg.Load(oldPath);

                XmlNode oldNode = oldCfg.SelectSingleNode("configuration/connectionStrings/add");
                XmlNode newNode = newCfg.SelectSingleNode("configuration/connectionStrings/add");
                if (oldNode != null && newNode != null)
                {
                    newNode.Attributes["connectionString"].Value = oldNode.Attributes["connectionString"].Value;
                }
                XmlNodeList newNodes = newCfg.SelectNodes("configuration/applicationSettings");
                XmlNodeList oldNodes = oldCfg.SelectNodes("configuration/applicationSettings");
                for (int i = 0; i < ((newNodes[0]).ChildNodes).Count; i++)
                {
                    newNode = newNodes[0].ChildNodes[i];

                    for (int j = 0; j < newNode.ChildNodes.Count; j++)
                    {
                        XmlNode newNodeChild = newNode.ChildNodes[j];
                        XmlNode oldNodeChild = oldNodes[0].SelectSingleNode(newNode.Name + "/" + newNodeChild.Name + "[@name='" + newNodeChild.Attributes[0].Value + "']");
                        if (oldNodeChild != null)
                        {
                            if (newNodeChild.SelectSingleNode("//value") != null && oldNodeChild.SelectSingleNode("//value") != null)
                                newNodeChild.SelectSingleNode("//value").InnerText = oldNodeChild.SelectSingleNode("//value").InnerText;
                            else
                                newNodeChild.InnerText = oldNodeChild.InnerText;
                        }
                    }
                }
                newNodes = newCfg.SelectNodes("configuration/appSettings");
                oldNodes = oldCfg.SelectNodes("configuration/appSettings");
                for (int i = 0; i < ((newNodes[0]).ChildNodes).Count; i++)
                {
                    XmlNode newNodeChild = newNodes[0].ChildNodes[i];
                    if (newNodeChild.Name == "add")
                    {
                        XmlNode oldNodeChild = oldNodes[0].SelectSingleNode(newNodeChild.Name + "[@key='" + newNodeChild.Attributes[0].Value + "']");
                        if (oldNodeChild != null)
                        {
                            for (int k = 0; k < newNodeChild.Attributes.Count; k++)
                            {
                                try
                                {
                                    string name = newNodeChild.Attributes[k].Name.Trim();
                                    newNodeChild.Attributes[name].Value = oldNodeChild.Attributes[name].Value;
                                }
                                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                            }
                        }
                    }
                }
                newCfg.Save(newPath);
            }
        }
        public void AfterUnzip()
        {
            try
            {
                SyncConfig(CommonUnitity.SystemBinUrl + "Softech.HRS.Online.exe.Config.old", CommonUnitity.SystemBinUrl + "Softech.HRS.Online.exe.Config");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            if (!File.Exists(CommonUnitity.SystemBinUrl + "Softech.HRS.Online.exe.Config")) RollbackFile(CommonUnitity.SystemBinUrl + "Softech.HRS.Online.exe.Config.old", CommonUnitity.SystemBinUrl + "Softech.HRS.Online.exe.Config");

        }
        void BackupFile(string fileName)
        {
            if (File.Exists(fileName + ".old"))
                File.Delete(fileName + ".old");
            if (File.Exists(fileName)) File.Move(fileName, fileName + ".old");
        }
        private void RollbackFile(string oldFile, string newFile)
        {
            if (File.Exists(oldFile))
                System.IO.File.Move(oldFile, newFile);
        }
        public bool UnZip(DownloadFileInfo fileInfo)
        {


            try
            {

                string directoryUnzip = CommonUnitity.GetSubPath(fileInfo, CommonUnitity.SystemBinUrl);
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("unzip.exe",
                                                            "-o \"" + CommonUnitity.GetZipPath(fileInfo) + "\" -d \"" + directoryUnzip);
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process p = System.Diagnostics.Process.Start(psi);
                p.WaitForExit();
                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return false; }
        }
    }
}
