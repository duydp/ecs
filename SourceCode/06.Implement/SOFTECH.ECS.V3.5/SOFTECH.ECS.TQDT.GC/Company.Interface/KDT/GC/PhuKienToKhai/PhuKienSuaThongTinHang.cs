﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;
using Company.Interface.VNACCS;

namespace Company.Interface.KDT.GC
{
    public partial class PhuKienSuaThongTinHang : BaseForm
    {
        public PhuKienSuaThongTinHang()
        {
            InitializeComponent();
                    }
        public KDT_SXXK_LoaiPhuKien loaiPK = new KDT_SXXK_LoaiPhuKien();
        //public List<KDT_SXXK_PhuKienDetail> loaiPK.ListPhuKienDeTail = new List<KDT_SXXK_PhuKienDetail>();
        private KDT_SXXK_PhuKienDetail detail = new KDT_SXXK_PhuKienDetail();
        public string SoTKVNACCS;
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GetPhuKien()
        {
            if (detail == null)
                detail = new KDT_SXXK_PhuKienDetail();
            txtSoTT.Value = detail.STTHang;
            txtMaHangCu.Text = detail.MaHangCu;
            if (loaiPK.MaPhuKien.Trim() == "504")
            {
                txtSoLuongCu.Value = detail.SoLuongCu;
                txtSoLuongMoi.Value = detail.SoLuongMoi;
            }
            else
            {
                ctrDVT_Old.Code = detail.DVTCu;
                ctrDVT_New.Code = detail.DVTMoi;
            }
        }

        private void SetPhuKien()
        {
            if (detail == null)
                detail = new KDT_SXXK_PhuKienDetail();
            detail.STTHang = Convert.ToInt32(txtSoTT.Value);
            detail.MaHangCu = txtMaHangCu.Text;
            if (loaiPK.MaPhuKien.Trim() == "504")
            {
                detail.SoLuongCu = Convert.ToDecimal(txtSoLuongCu.Value);
                detail.SoLuongMoi = Convert.ToDecimal(txtSoLuongMoi.Value);
            }
            else
            {
                detail.DVTCu = ctrDVT_Old.Code;
                detail.DVTMoi = ctrDVT_New.Code;
            }
            detail.Master_ID = loaiPK.ID;
            
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
            
            if (string.IsNullOrEmpty(txtSoTT.Text) || Convert.ToInt32(txtSoTT.Value) == 0)
            {
                this.ShowMessage("Vui lòng chọn số thứ tự hàng muốn khai sửa đổi", false);
                return;
            }
           
            detail = loaiPK.ListPhuKienDeTail.Find(x => Convert.ToInt32(x.STTHang) == Convert.ToInt32(txtSoTT.Value));

            if (detail != null )
            {
                SetPhuKien();
                if (detail.ID > 0)
                    detail.Update();
                else
                    detail.Insert();
            }
            else
            {
                SetPhuKien();
                detail.Insert();
                loaiPK.ListPhuKienDeTail.Add(detail);
            }
            grvHang.DataSource = loaiPK.ListPhuKienDeTail;
            grvHang.Refetch();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void txtSoTT_Click(object sender, EventArgs e)
        {
            try
            {
            
            KDT_VNACC_ToKhaiMauDich tkmd = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(SoTKVNACCS);
            if(tkmd == null)
            {
                this.ShowMessage("Số tờ khai VNACCS không tồn tại trên hệ thống. Vui lòng kiểm tra lại",false);
                return;
            }
            VNACC_ListHangHMD fhmd = new VNACC_ListHangHMD(VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { tkmd.ID });
            fhmd.ShowDialog(this);
            if (fhmd.DialogResult == DialogResult.OK)
            {
                detail = convertFromHangVNACCS(fhmd.HangMD);
                GetPhuKien();
            }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private KDT_SXXK_PhuKienDetail convertFromHangVNACCS(KDT_VNACC_HangMauDich hmd)
        {
            KDT_SXXK_PhuKienDetail newdetail = new KDT_SXXK_PhuKienDetail()
            {
                STTHang = string.IsNullOrEmpty(hmd.SoDong) ? 0 : System.Convert.ToInt16(hmd.SoDong),
                MaHangCu = hmd.MaHangHoa,
                SoLuongCu = hmd.SoLuong1,
                DVTCu = hmd.DVTLuong1,
                MaHQDangKyMoi = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(GlobalSettings.MA_HAI_QUAN)
            };
            if (!string.IsNullOrEmpty(hmd.MaQuanLy) && hmd.MaQuanLy.Length > 5)
            {
                newdetail.LoaiHangCu = hmd.MaQuanLy.Substring(0, 1);
                newdetail.MaHQDangKyCu = hmd.MaQuanLy.Substring(1, 6);
            }
            return newdetail;

        }

        private void grvHang_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
            
            if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                detail = (KDT_SXXK_PhuKienDetail)e.Row.DataRow;
            GetPhuKien();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grvHang_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {
           
             Janus.Windows.GridEX.GridEXSelectedItemCollection items = grvHang.SelectedItems;
            if (items.Count > 0)
            {
                if (ShowMessage("Bạn có muốn xóa các dòng hàng này không?", true) == "Yes")
                {
                    foreach (Janus.Windows.GridEX.GridEXSelectedItem item in items)
                    {
                        if(item.RowType == Janus.Windows.GridEX.RowType.Record)
                            {
                                KDT_SXXK_PhuKienDetail pkdetail = (KDT_SXXK_PhuKienDetail)item.GetRow().DataRow;
                                if (detail.ID > 0)
                                    detail.Delete();
                                loaiPK.ListPhuKienDeTail.RemoveAt(loaiPK.ListPhuKienDeTail.FindIndex(x => x.STTHang == detail.STTHang));
                            }
                    }
                }
                grvHang.DataSource = loaiPK.ListPhuKienDeTail;
                grvHang.Refetch();
            }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void PhuKienSuaMaHang_Load(object sender, EventArgs e)
        {
           
            if (loaiPK == null)
                loaiPK = new KDT_SXXK_LoaiPhuKien();
            else
            {
                grvHang.DataSource = loaiPK.ListPhuKienDeTail;
            }
            if (loaiPK.MaPhuKien.Trim() == "504")
            {
                this.Text = "Phụ kiện sửa thông tin số lượng hàng của tờ khai";
                ctrDVT_Old.Enabled = ctrDVT_New.Enabled = false;
                txtSoLuongCu.Enabled = txtSoLuongMoi.Enabled = true;
                grvHang.Tables[0].Columns["DVTCu"].Visible =grvHang.Tables[0].Columns["DVTMoi"].Visible = false;
            }
            else
            {
                this.Text = "Phụ kiện sửa thông tin đơn vị tính hàng của tờ khai";
                ctrDVT_Old.Enabled = ctrDVT_New.Enabled = true ;
                txtSoLuongCu.Enabled = txtSoLuongMoi.Enabled = false;
                grvHang.Tables[0].Columns["SoLuongCu"].Visible = grvHang.Tables[0].Columns["SoLuongMoi"].Visible = false;
            }
        }
        
    }
}
