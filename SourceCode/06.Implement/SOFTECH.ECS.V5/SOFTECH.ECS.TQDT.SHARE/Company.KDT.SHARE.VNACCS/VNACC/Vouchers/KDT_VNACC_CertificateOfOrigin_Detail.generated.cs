﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_CertificateOfOrigin_Detail : ICloneable
	{
		#region Properties.
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string SoCO { set; get; }
		public string LoaiCO { set; get; }
		public string ToChucCap { set; get; }
		public DateTime NgayCap { set; get; }
		public string NuocCap { set; get; }
		public string NguoiCap { set; get; }
        public List<KDT_VNACC_CertificateOfOrigin_Detail> ListCertificateOfOrigin = new List<KDT_VNACC_CertificateOfOrigin_Detail>();
		#endregion
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_CertificateOfOrigin_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_CertificateOfOrigin_Detail> collection = new List<KDT_VNACC_CertificateOfOrigin_Detail>();
			while (reader.Read())
			{
				KDT_VNACC_CertificateOfOrigin_Detail entity = new KDT_VNACC_CertificateOfOrigin_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
				if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCap"))) entity.NgayCap = reader.GetDateTime(reader.GetOrdinal("NgayCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocCap"))) entity.NuocCap = reader.GetString(reader.GetOrdinal("NuocCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiCap"))) entity.NguoiCap = reader.GetString(reader.GetOrdinal("NguoiCap"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_CertificateOfOrigin_Detail> collection, long id)
        {
            foreach (KDT_VNACC_CertificateOfOrigin_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_CertificateOfOrigin_Details VALUES(@Master_ID, @SoCO, @LoaiCO, @ToChucCap, @NgayCap, @NuocCap, @NguoiCap)";
            string update = "UPDATE t_KDT_VNACC_CertificateOfOrigin_Details SET Master_ID = @Master_ID, SoCO = @SoCO, LoaiCO = @LoaiCO, ToChucCap = @ToChucCap, NgayCap = @NgayCap, NuocCap = @NuocCap, NguoiCap = @NguoiCap WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_CertificateOfOrigin_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCO", SqlDbType.NVarChar, "SoCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiCO", SqlDbType.NVarChar, "LoaiCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ToChucCap", SqlDbType.NVarChar, "ToChucCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocCap", SqlDbType.VarChar, "NuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiCap", SqlDbType.NVarChar, "NguoiCap", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCO", SqlDbType.NVarChar, "SoCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiCO", SqlDbType.NVarChar, "LoaiCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ToChucCap", SqlDbType.NVarChar, "ToChucCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocCap", SqlDbType.VarChar, "NuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiCap", SqlDbType.NVarChar, "NguoiCap", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_CertificateOfOrigin_Details VALUES(@Master_ID, @SoCO, @LoaiCO, @ToChucCap, @NgayCap, @NuocCap, @NguoiCap)";
            string update = "UPDATE t_KDT_VNACC_CertificateOfOrigin_Details SET Master_ID = @Master_ID, SoCO = @SoCO, LoaiCO = @LoaiCO, ToChucCap = @ToChucCap, NgayCap = @NgayCap, NuocCap = @NuocCap, NguoiCap = @NguoiCap WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_CertificateOfOrigin_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCO", SqlDbType.NVarChar, "SoCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiCO", SqlDbType.NVarChar, "LoaiCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ToChucCap", SqlDbType.NVarChar, "ToChucCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocCap", SqlDbType.VarChar, "NuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiCap", SqlDbType.NVarChar, "NguoiCap", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCO", SqlDbType.NVarChar, "SoCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiCO", SqlDbType.NVarChar, "LoaiCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ToChucCap", SqlDbType.NVarChar, "ToChucCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocCap", SqlDbType.VarChar, "NuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiCap", SqlDbType.NVarChar, "NguoiCap", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_CertificateOfOrigin_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_CertificateOfOrigin_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_CertificateOfOrigin_Detail(long master_ID, string soCO, string loaiCO, string toChucCap, DateTime ngayCap, string nuocCap, string NguoiCap)
		{
			KDT_VNACC_CertificateOfOrigin_Detail entity = new KDT_VNACC_CertificateOfOrigin_Detail();	
			entity.Master_ID = master_ID;
			entity.SoCO = soCO;
			entity.LoaiCO = loaiCO;
			entity.ToChucCap = toChucCap;
			entity.NgayCap = ngayCap;
			entity.NuocCap = nuocCap;
			entity.NguoiCap = NguoiCap;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoCO", SqlDbType.NVarChar, SoCO);
			db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.NVarChar, LoaiCO);
			db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
			db.AddInParameter(dbCommand, "@NgayCap", SqlDbType.DateTime, NgayCap.Year <= 1753 ? DBNull.Value : (object) NgayCap);
			db.AddInParameter(dbCommand, "@NuocCap", SqlDbType.VarChar, NuocCap);
			db.AddInParameter(dbCommand, "@NguoiCap", SqlDbType.NVarChar, NguoiCap);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_CertificateOfOrigin_Detail(long id, long master_ID, string soCO, string loaiCO, string toChucCap, DateTime ngayCap, string nuocCap, string NguoiCap)
		{
			KDT_VNACC_CertificateOfOrigin_Detail entity = new KDT_VNACC_CertificateOfOrigin_Detail();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoCO = soCO;
			entity.LoaiCO = loaiCO;
			entity.ToChucCap = toChucCap;
			entity.NgayCap = ngayCap;
			entity.NuocCap = nuocCap;
			entity.NguoiCap = NguoiCap;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_CertificateOfOrigin_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoCO", SqlDbType.NVarChar, SoCO);
			db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.NVarChar, LoaiCO);
			db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
			db.AddInParameter(dbCommand, "@NgayCap", SqlDbType.DateTime, NgayCap.Year <= 1753 ? DBNull.Value : (object) NgayCap);
			db.AddInParameter(dbCommand, "@NuocCap", SqlDbType.VarChar, NuocCap);
			db.AddInParameter(dbCommand, "@NguoiCap", SqlDbType.NVarChar, NguoiCap);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_CertificateOfOrigin_Detail(long id, long master_ID, string soCO, string loaiCO, string toChucCap, DateTime ngayCap, string nuocCap, string NguoiCap)
		{
			KDT_VNACC_CertificateOfOrigin_Detail entity = new KDT_VNACC_CertificateOfOrigin_Detail();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SoCO = soCO;
			entity.LoaiCO = loaiCO;
			entity.ToChucCap = toChucCap;
			entity.NgayCap = ngayCap;
			entity.NuocCap = nuocCap;
			entity.NguoiCap = NguoiCap;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SoCO", SqlDbType.NVarChar, SoCO);
			db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.NVarChar, LoaiCO);
			db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
			db.AddInParameter(dbCommand, "@NgayCap", SqlDbType.DateTime, NgayCap.Year <= 1753 ? DBNull.Value : (object) NgayCap);
			db.AddInParameter(dbCommand, "@NuocCap", SqlDbType.VarChar, NuocCap);
			db.AddInParameter(dbCommand, "@NguoiCap", SqlDbType.NVarChar, NguoiCap);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_CertificateOfOrigin_Detail(long id)
		{
			KDT_VNACC_CertificateOfOrigin_Detail entity = new KDT_VNACC_CertificateOfOrigin_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}