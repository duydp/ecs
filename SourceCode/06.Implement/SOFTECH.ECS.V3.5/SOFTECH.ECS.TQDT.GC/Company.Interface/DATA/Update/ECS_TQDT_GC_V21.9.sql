﻿
GO

/****** Object:  StoredProcedure [dbo].[p_GC_BC01HSTK_GC_TT38]    Script Date: 06/01/2016 7:51:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

     
create PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT38]       
 -- Add the parameters for the stored procedure here      
    @IDHopDong BIGINT ,
    @MaHaiQuan CHAR(6) ,
    @MaDoanhNghiep VARCHAR(14),
    @TuNgay DATETIME,
    @DenNgay DATETIME
AS 
    BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
        SET NOCOUNT ON ;    
        SELECT  ROW_NUMBER() OVER ( ORDER BY ToKhai.MaNguyenPhuLieu ) AS 'STT' ,
                ToKhai.IDHopDong AS ID ,
                ToKhai.MaDoanhNghiep ,
                ToKhai.MaHaiQuan ,
                ToKhai.SoToKhai ,
                ToKhai.MaLoaiHinh ,
                ToKhai.NgayDangKy ,
                ToKhai.TenHang AS TenNguyenPhuLieu ,
                ToKhai.MaNguyenPhuLieu ,
                ToKhai.SoLuong ,
                t_HaiQuan_DonViTinh.Ten AS DVT ,
                ToKhai.SoLuong AS TongLuong
        FROM    ( SELECT    CONVERT(VARCHAR(20), ( CASE WHEN t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%V%'
                                                        THEN ( SELECT TOP 1
                                                              SoTKVNACCS
                                                              FROM
                                                              t_VNACCS_CapSoToKhai
                                                              WHERE
                                                              SoTK = t_KDT_ToKhaiMauDich.SoToKhai
                                                             )
                                                        ELSE t_KDT_ToKhaiMauDich.SoToKhai
                                                   END )) AS SoToKhai ,
                            t_KDT_ToKhaiMauDich.MaLoaiHinh ,
                            t_KDT_ToKhaiMauDich.NgayDangKy ,
                            t_KDT_ToKhaiMauDich.IDHopDong ,
                            t_KDT_ToKhaiMauDich.MaHaiQuan ,
                            t_KDT_HangMauDich.MaPhu AS MaNguyenPhuLieu ,
                            t_KDT_HangMauDich.SoLuong ,
                            t_KDT_HangMauDich.TenHang ,
                            t_KDT_HangMauDich.DVT_ID ,
                            t_KDT_ToKhaiMauDich.MaDoanhNghiep
                  FROM      t_KDT_ToKhaiMauDich
                            INNER JOIN t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
                  WHERE     ( t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 )
                            AND ( t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong )
                            AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'N%' )
                            AND ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N' )
                  UNION
                  SELECT    CONVERT(VARCHAR(20), ( CASE WHEN t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE '%V%'
                                                        THEN ( SELECT TOP 1
                                                              SoTKVNACCS
                                                              FROM
                                                              t_VNACCS_CapSoToKhai
                                                              WHERE
                                                              SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai
                                                             )
                                                        ELSE t_KDT_GC_ToKhaiChuyenTiep.SoToKhai
                                                   END )) AS SoToKhai ,
                            t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,
                            t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                            t_KDT_GC_ToKhaiChuyenTiep.IDHopDong ,
                            t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan ,
                            t_KDT_GC_HangChuyenTiep.MaHang AS MaNguyenPhuLieu ,
                            t_KDT_GC_HangChuyenTiep.SoLuong ,
                            t_KDT_GC_HangChuyenTiep.TenHang ,
                            t_KDT_GC_HangChuyenTiep.ID_DVT ,
                            t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                  FROM      t_KDT_GC_ToKhaiChuyenTiep
                            INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                  WHERE     ( t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 )
                            AND ( t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong )
                            AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh IN (
                                  'PHPLN', 'PHSPN', 'NGC18', 'NGC19', 'PHTBN',
                                  'NVE23' ) )
                            AND ( t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N' )
                ) AS ToKhai
                INNER JOIN t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID
        WHERE   ( ToKhai.IDHopDong = @IDHopDong ) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan) AND ToKhai.NgayDangKy >= @TuNgay AND ToKhai.NgayDangKy <= @DenNgay
ORDER BY MaNguyenPhuLieu,ToKhai.NgayDangKy
    END
    
    SELECT * FROM dbo.t_KDT_ToKhaiMauDich
GO



GO

/****** Object:  StoredProcedure [dbo].[p_GC_BC02HSTK_GC_TT38]    Script Date: 06/01/2016 7:53:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[p_GC_BC02HSTK_GC_TT38]       
 -- Add the parameters for the stored procedure here      
    @IDHopDong BIGINT ,
    @MaHaiQuan CHAR(6) ,
    @MaDoanhNghiep VARCHAR(14) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
    BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
        SET NOCOUNT ON ;      
 -- p_GC_BC02HSTK_GC_TT117 710, 'C34C','0400101556'      
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',      
    --ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',   
        SELECT DISTINCT
                ROW_NUMBER() OVER ( ORDER BY v_KDT_SP_Xuat_HD.MaPhu ) AS 'STT' ,
                CONVERT(VARCHAR(50), ( CASE WHEN ToKhai.MaLoaiHinh LIKE '%V%'
                                            THEN ( SELECT TOP 1
                                                            SoTKVNACCS
                                                   FROM     t_VNACCS_CapSoToKhai
                                                   WHERE    SoTK = ToKhai.SoToKhai
                                                 )
                                            ELSE ToKhai.SoToKhai
                                       END )) AS SoToKhai ,
                         ToKhai.MaLoaiHinh,
                ToKhai.NgayDangKy ,
                v_KDT_SP_Xuat_HD.MaPhu AS MaHang ,
                ToKhai.TenHang ,
                t_HaiQuan_DonViTinh.Ten AS DVT ,
                ToKhai.SoLuong ,
                ToKhai.SoLuong AS TongCong
        FROM    v_KDT_SP_Xuat_HD
                INNER JOIN ( SELECT t_KDT_ToKhaiMauDich.SoToKhai ,
                                    t_KDT_ToKhaiMauDich.MaLoaiHinh ,
                                    t_KDT_ToKhaiMauDich.NgayDangKy ,
                                    t_KDT_ToKhaiMauDich.IDHopDong ,
                                    t_KDT_ToKhaiMauDich.MaHaiQuan ,
                                    t_KDT_HangMauDich.MaPhu AS MaHang ,
                                    t_KDT_HangMauDich.TenHang ,
                                    t_KDT_HangMauDich.SoLuong ,
                                    t_KDT_ToKhaiMauDich.MaDoanhNghiep
                             FROM   t_KDT_ToKhaiMauDich
                                    INNER JOIN t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                             WHERE  ( t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 )
                                    AND ( t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong )
                                    AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' )
                                    AND ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' )
                             UNION
                             SELECT t_KDT_GC_ToKhaiChuyenTiep.SoToKhai ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,
                                    t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                                    t_KDT_GC_ToKhaiChuyenTiep.IDHopDong ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan ,
                                    t_KDT_GC_HangChuyenTiep.MaHang ,
                                    t_KDT_GC_HangChuyenTiep.TenHang ,
                                    t_KDT_GC_HangChuyenTiep.SoLuong ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                             FROM   t_KDT_GC_ToKhaiChuyenTiep
                                    INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                             WHERE  ( t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 )
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong )
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX'
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19'
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XVE54'
                                        )
                           ) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong
                                          AND v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang
                INNER JOIN t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID
                INNER JOIN t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
        WHERE   ( v_KDT_SP_Xuat_HD.IDHopDong = @IDHopDong )
                AND ( ToKhai.MaDoanhNghiep = @MaDoanhNghiep )
                AND ( ToKhai.MaHaiQuan = @MaHaiQuan )
                AND ToKhai.NgayDangKy>= @TuNgay
                AND ToKhai.NgayDangKy<=@DenNgay
        ORDER BY MaHang,ToKhai.NgayDangKy   
      
    END   
  
GO


GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '21.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('21.9',GETDATE(),'Cập nhật thanh khoản mẫu 01,02 lấy dữ liệu theo ngày')
END	