﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Feedback.Components;
using Khendys.Controls;

namespace Company.Interface.Feedback
{
    public partial class CauHoiForm : BaseForm
    {
        private DataTable dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];
        public string MaLinhVuc;

        public CauHoiForm()
        {
            InitializeComponent();

            this.FormClosing += new FormClosingEventHandler(Form_FormClosing);

        }

        void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (base.IsChanged)
            {
                if (Globals.ShowMessage("Bạn chưa lưu thay đổi thông tin trên form, bạn có muốn lưu không?", true) == "Yes")
                {
                    Save();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtTieuDe.Text = txtNoiDung.Text = txtTraLoi.Text = "";

            SetChanged(false);
        }

        private void CauHoiForm_Load(object sender, EventArgs e)
        {
            Title = Text;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm())
                    return;

                Save();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void Save()
        {
            try
            {
                ThuVienCauHoi cauHoi = new ThuVienCauHoi();
                cauHoi.TieuDe = txtTieuDe.Text;
                cauHoi.NoiDungCauHoi = txtNoiDung.Text;
                cauHoi.TraLoi = txtTraLoi.Text;
                cauHoi.Phanloai = phanLoaiUserControl1.GetItemChecked();
                cauHoi.LinhVuc = linhVucUserControl1.GetMa();
                cauHoi.NgayTao = cauHoi.NgayCapNhat = DateTime.Now;
                cauHoi.HienThi = true;

                string msg = FeedbackGlobalSettings.TaoSuaCauHoi(ref cauHoi);
                if (msg == "")
                {
                    Globals.ShowMessage("Lưu thông tin thành công.", false);

                    SetChanged(false);
                }
                else
                    Globals.ShowMessage("Lỗi lưu thông tin:\r\n" + msg, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage("Lỗi lưu thông tin:\r\n" + ex.Message, false);
            }
        }

        private bool ValidateForm()
        {
            bool valid = true;

            valid &= Globals.ValidateNull(txtTieuDe, errorProvider1, "Tiêu đề");

            if (ThuVienCauHoi.SelectCollectionDynamic("TieuDe = N'" + txtTieuDe.Text + "'", "").Count > 0)
            {
                valid = false;
                Globals.ShowMessage("Tiêu đề này đã tồn tại.", false);
            }

            valid &= Globals.ValidateNull(txtNoiDung, errorProvider1, "Nội dung cầu hỏi");

            string msgErr = "Thông tin 'Trả lời' không được để trống.";
            errorProvider1.SetError(txtTraLoi, "");
            if (txtTraLoi.Text.Trim().Length == 0)
            {
                valid = false;
                errorProvider1.SetError(txtTraLoi, msgErr);
                txtTraLoi.Focus();
            }

            if (linhVucUserControl1.GetMa() == "")
            {
                Globals.ShowMessage("Bạn chưa chọn thông tin 'Lĩnh vực' cho câu hỏi.", false);
                valid = false;
            }

            return valid;
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            SetChanged(true);
        }
    }
}
