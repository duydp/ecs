﻿namespace Company.Interface
{
    partial class CauHinhBieuThueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.Common.JanusColorScheme janusColorScheme2 = new Janus.Windows.Common.JanusColorScheme();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbBieuThueGTGT = new Janus.Windows.EditControls.UIComboBox();
            this.cmbBieuThueBVMT = new Janus.Windows.EditControls.UIComboBox();
            this.cmbBieuThueTTDB = new Janus.Windows.EditControls.UIComboBox();
            this.cmbBieuThueXK = new Janus.Windows.EditControls.UIComboBox();
            this.cbBieuThueNK = new Janus.Windows.EditControls.UIComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // visualStyleManager1
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office2003";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme2.Name = "Office2007";
            janusColorScheme2.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme1);
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme2);
            this.visualStyleManager1.DefaultColorScheme = "Office2003";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox1.Controls.Add(this.uiButton2);
            this.uiGroupBox1.Controls.Add(this.uiButton1);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.cmbBieuThueGTGT);
            this.uiGroupBox1.Controls.Add(this.cmbBieuThueBVMT);
            this.uiGroupBox1.Controls.Add(this.cmbBieuThueTTDB);
            this.uiGroupBox1.Controls.Add(this.cmbBieuThueXK);
            this.uiGroupBox1.Controls.Add(this.cbBieuThueNK);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(334, 210);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiButton2
            // 
            this.uiButton2.Location = new System.Drawing.Point(166, 181);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 3;
            this.uiButton2.Text = "Lưu";
            this.uiButton2.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiButton1
            // 
            this.uiButton1.Location = new System.Drawing.Point(247, 181);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(75, 23);
            this.uiButton1.TabIndex = 3;
            this.uiButton1.Text = "Đóng";
            this.uiButton1.VisualStyleManager = this.visualStyleManager1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Biểu thuế giá trị gia tăng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Biểu thuế bảo vệ môi trường";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Biểu thuế tiêu thụ đặc biệt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Biểu thuế xuất khẩu ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Biểu thuế nhập khẩu";
            // 
            // cmbBieuThueGTGT
            // 
            this.cmbBieuThueGTGT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbBieuThueGTGT.DisplayMember = "Ten";
            this.cmbBieuThueGTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBieuThueGTGT.Location = new System.Drawing.Point(166, 140);
            this.cmbBieuThueGTGT.Name = "cmbBieuThueGTGT";
            this.cmbBieuThueGTGT.Size = new System.Drawing.Size(156, 21);
            this.cmbBieuThueGTGT.TabIndex = 1;
            this.cmbBieuThueGTGT.ValueMember = "ID";
            this.cmbBieuThueGTGT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cmbBieuThueBVMT
            // 
            this.cmbBieuThueBVMT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbBieuThueBVMT.DisplayMember = "Ten";
            this.cmbBieuThueBVMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBieuThueBVMT.Location = new System.Drawing.Point(166, 113);
            this.cmbBieuThueBVMT.Name = "cmbBieuThueBVMT";
            this.cmbBieuThueBVMT.Size = new System.Drawing.Size(156, 21);
            this.cmbBieuThueBVMT.TabIndex = 1;
            this.cmbBieuThueBVMT.ValueMember = "ID";
            this.cmbBieuThueBVMT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cmbBieuThueTTDB
            // 
            this.cmbBieuThueTTDB.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbBieuThueTTDB.DisplayMember = "Ten";
            this.cmbBieuThueTTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBieuThueTTDB.Location = new System.Drawing.Point(166, 86);
            this.cmbBieuThueTTDB.Name = "cmbBieuThueTTDB";
            this.cmbBieuThueTTDB.Size = new System.Drawing.Size(156, 21);
            this.cmbBieuThueTTDB.TabIndex = 1;
            this.cmbBieuThueTTDB.ValueMember = "ID";
            this.cmbBieuThueTTDB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cmbBieuThueXK
            // 
            this.cmbBieuThueXK.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbBieuThueXK.DisplayMember = "Ten";
            this.cmbBieuThueXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBieuThueXK.Location = new System.Drawing.Point(166, 32);
            this.cmbBieuThueXK.Name = "cmbBieuThueXK";
            this.cmbBieuThueXK.Size = new System.Drawing.Size(156, 21);
            this.cmbBieuThueXK.TabIndex = 1;
            this.cmbBieuThueXK.ValueMember = "ID";
            this.cmbBieuThueXK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbBieuThueNK
            // 
            this.cbBieuThueNK.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbBieuThueNK.DisplayMember = "Ten";
            this.cbBieuThueNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBieuThueNK.Location = new System.Drawing.Point(166, 59);
            this.cbBieuThueNK.Name = "cbBieuThueNK";
            this.cbBieuThueNK.Size = new System.Drawing.Size(156, 21);
            this.cbBieuThueNK.TabIndex = 1;
            this.cbBieuThueNK.ValueMember = "ID";
            this.cbBieuThueNK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // CauHinhBieuThueForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 210);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "CauHinhBieuThueForm";
            this.Text = "Cấu hình biểu thuế mặc định";
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIComboBox cmbBieuThueGTGT;
        private Janus.Windows.EditControls.UIComboBox cmbBieuThueBVMT;
        private Janus.Windows.EditControls.UIComboBox cmbBieuThueTTDB;
        private Janus.Windows.EditControls.UIComboBox cbBieuThueNK;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIComboBox cmbBieuThueXK;
    }
}