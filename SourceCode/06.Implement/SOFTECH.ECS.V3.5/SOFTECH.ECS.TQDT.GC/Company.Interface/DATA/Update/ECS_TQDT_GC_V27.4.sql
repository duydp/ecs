GO
 IF OBJECT_ID(N'[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]') IS NOT NULL
	DROP TABLE [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
GO
CREATE TABLE [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
(
[STTHang] [int] NOT NULL,
[HopDong_ID] [bigint] NOT NULL,
[MaNPL] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenNPL] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LuongNhap] [numeric] (18, 6) NULL,
[LuongXuat] [numeric] (18, 6) NULL,
[LuongCungUng] [numeric] (18, 6) NULL,
[LuongTon] [numeric] (18, 6) NULL,
[TongNhuCau] [numeric] (18, 6) NULL,
[TrangThai] [int] NOT NULL,
[TuNgay] DATETIME NOT NULL,
[DenNgay] DATETIME NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] ADD CONSTRAINT [PK_t_KDT_GC_BCXNT_NguyenPhuLieu] PRIMARY KEY CLUSTERED ([HopDong_ID], [MaNPL]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] ADD CONSTRAINT [FK_t_KDT_GC_BCXNT_NguyenPhuLieu_t_KDT_GC_HopDong] FOREIGN KEY ([HopDong_ID]) REFERENCES [dbo].[t_KDT_GC_HopDong] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Insert]
	@STTHang int,
	@HopDong_ID bigint,
	@MaNPL varchar(30),
	@TenNPL nvarchar(80),
	@MaHS varchar(12),
	@DVT char(3),
	@LuongNhap numeric(18, 6),
	@LuongXuat numeric(18, 6),
	@LuongCungUng numeric(18, 6),
	@LuongTon numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@TrangThai int,
	@TuNgay datetime,
	@DenNgay datetime
AS
INSERT INTO [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
(
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
)
VALUES
(
	@STTHang,
	@HopDong_ID,
	@MaNPL,
	@TenNPL,
	@MaHS,
	@DVT,
	@LuongNhap,
	@LuongXuat,
	@LuongCungUng,
	@LuongTon,
	@TongNhuCau,
	@TrangThai,
	@TuNgay,
	@DenNgay
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Update]
	@STTHang int,
	@HopDong_ID bigint,
	@MaNPL varchar(30),
	@TenNPL nvarchar(80),
	@MaHS varchar(12),
	@DVT char(3),
	@LuongNhap numeric(18, 6),
	@LuongXuat numeric(18, 6),
	@LuongCungUng numeric(18, 6),
	@LuongTon numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@TrangThai int,
	@TuNgay datetime,
	@DenNgay datetime
AS

UPDATE
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
SET
	[STTHang] = @STTHang,
	[TenNPL] = @TenNPL,
	[MaHS] = @MaHS,
	[DVT] = @DVT,
	[LuongNhap] = @LuongNhap,
	[LuongXuat] = @LuongXuat,
	[LuongCungUng] = @LuongCungUng,
	[LuongTon] = @LuongTon,
	[TongNhuCau] = @TongNhuCau,
	[TrangThai] = @TrangThai,
	[TuNgay] = @TuNgay,
	[DenNgay] = @DenNgay
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaNPL] = @MaNPL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_InsertUpdate]
	@STTHang int,
	@HopDong_ID bigint,
	@MaNPL varchar(30),
	@TenNPL nvarchar(80),
	@MaHS varchar(12),
	@DVT char(3),
	@LuongNhap numeric(18, 6),
	@LuongXuat numeric(18, 6),
	@LuongCungUng numeric(18, 6),
	@LuongTon numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@TrangThai int,
	@TuNgay datetime,
	@DenNgay datetime
AS
IF EXISTS(SELECT [HopDong_ID], [MaNPL] FROM [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] WHERE [HopDong_ID] = @HopDong_ID AND [MaNPL] = @MaNPL)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] 
		SET
			[STTHang] = @STTHang,
			[TenNPL] = @TenNPL,
			[MaHS] = @MaHS,
			[DVT] = @DVT,
			[LuongNhap] = @LuongNhap,
			[LuongXuat] = @LuongXuat,
			[LuongCungUng] = @LuongCungUng,
			[LuongTon] = @LuongTon,
			[TongNhuCau] = @TongNhuCau,
			[TrangThai] = @TrangThai,
			[TuNgay] = @TuNgay,
			[DenNgay] = @DenNgay
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [MaNPL] = @MaNPL
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
	(
			[STTHang],
			[HopDong_ID],
			[MaNPL],
			[TenNPL],
			[MaHS],
			[DVT],
			[LuongNhap],
			[LuongXuat],
			[LuongCungUng],
			[LuongTon],
			[TongNhuCau],
			[TrangThai],
			[TuNgay],
			[DenNgay]
	)
	VALUES
	(
			@STTHang,
			@HopDong_ID,
			@MaNPL,
			@TenNPL,
			@MaHS,
			@DVT,
			@LuongNhap,
			@LuongXuat,
			@LuongCungUng,
			@LuongTon,
			@TongNhuCau,
			@TrangThai,
			@TuNgay,
			@DenNgay
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Delete]
	@HopDong_ID bigint,
	@MaNPL varchar(30)
AS

DELETE FROM 
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaNPL] = @MaNPL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Load]
	@HopDong_ID bigint,
	@MaNPL varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
FROM
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaNPL] = @MaNPL
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
FROM
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
FROM [dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[STTHang],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT],
	[LuongNhap],
	[LuongXuat],
	[LuongCungUng],
	[LuongTon],
	[TongNhuCau],
	[TrangThai],
	[TuNgay],
	[DenNgay]
FROM
	[dbo].[t_KDT_GC_BCXNT_NguyenPhuLieu]	

GO
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]      
-- Database: ECS_GC      
-- Author: Dang Phuoc Duy    
-- Time created: Tuesday, October 20, 2015    
------------------------------------------------------------------------------------------------------------------------      
      
ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]      
 @WhereCondition NVARCHAR(500),      
 @OrderByExpression NVARCHAR(250) = NULL      
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL nvarchar(3250)      
      
SET @SQL = 'SELECT      
  tkmd.ID,      
 [SoTiepNhan],      
 [NgayTiepNhan],      
 [MaHaiQuan],      
 [SoToKhai],      
 [MaLoaiHinh],      
 [NgayDangKy],      
 [MaDoanhNghiep],      
 [TenDoanhNghiep],      
 [MaDaiLyTTHQ],      
 [TenDaiLyTTHQ],      
 [TenDonViDoiTac],      
 [ChiTietDonViDoiTac],      
 [SoGiayPhep],      
 [NgayGiayPhep],      
 [NgayHetHanGiayPhep],      
 [SoHopDong],      
 [NgayHopDong],      
 [NgayHetHanHopDong],      
 [SoHoaDonThuongMai],      
 [NgayHoaDonThuongMai],      
 [PTVT_ID],      
 [SoHieuPTVT],      
 [NgayDenPTVT],      
 [QuocTichPTVT_ID],      
 [LoaiVanDon],      
 [SoVanDon],      
 [NgayVanDon],      
 [NuocXK_ID],      
 [NuocNK_ID],      
 [DiaDiemXepHang],      
 [CuaKhau_ID],      
 [DKGH_ID],      
 [NguyenTe_ID],      
 [TyGiaTinhThue],      
 [TyGiaUSD],      
 [PTTT_ID],      
 [SoHang],      
 [SoLuongPLTK],      
 [TenChuHang],      
 [ChucVu],      
 [SoContainer20],      
 [SoContainer40],      
 [SoKien],      
 [TongTriGiaKhaiBao],      
 [TongTriGiaTinhThue],      
 [LoaiToKhaiGiaCong],      
 [LePhiHaiQuan],      
 [PhiBaoHiem],      
 [PhiVanChuyen],      
 [PhiXepDoHang],      
 [PhiKhac],      
 [CanBoDangKy],      
 [QuanLyMay],      
 [TrangThaiXuLy],      
 [LoaiHangHoa],      
 [GiayTo],      
 [PhanLuong],      
 [MaDonViUT],      
 [TenDonViUT],      
 [TrongLuongNet],      
 [SoTienKhoan],  
 [HeSoNhan],             
 [GUIDSTR],      
 [DeXuatKhac],      
 [LyDoSua],      
 [ActionStatus],      
 [GuidReference],      
 [NamDK],      
 [HUONGDAN] ,    
 hmd.SoThuTuHang,    
 hmd.MaPhu,  
 hmd.MaHS,    
 hmd.TenHang,    
 hmd.NuocXX_ID,    
 hmd.SoLuong,    
 hmd.DonGiaKB,    
 hmd.TriGiaKB,    
 hmd.DonGiaTT,    
 hmd.TriGiaTT,    
 CASE WHEN tkmd.ID <>0 THEN (SELECT dvt.Ten FROM dbo.t_HaiQuan_DonViTinh dvt WHERE dvt.ID=hmd.DVT_ID) ELSE NULL END AS DVT    
 FROM [dbo].[t_KDT_ToKhaiMauDich] tkmd INNER JOIN dbo.t_KDT_HangMauDich hmd ON hmd.TKMD_ID = tkmd.ID  WHERE ' + @WhereCondition      
      
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0      
BEGIN      
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression      
END      
      
EXEC sp_executesql @SQL      


GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '27.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('27.4',GETDATE(), N'CẬP NHẬT PROCEDURE KẾT XUẤT DỮ LIỆU')
END