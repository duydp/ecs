﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace ECS.AutoUpdate
{
    /// <summary>
    /// Thực hiện cập nhật database
    /// </summary>
    public sealed class UpdateSql
    {
        private static UpdateSql install;
        private IUpdate iUpdate;
        static UpdateSql()
        {
            install = new UpdateSql();
            if (install.iUpdate == null)
            {
                install.iUpdate = (IUpdate)Activator.CreateInstance
                                (Type.GetType(install.GetType().Namespace + "." + Config.Install.ClassUpdateSql));
            }
        }
        public static bool Update(string fileName)
        {
            try
            {
                //int posVersion = fileName.IndexOf("_V");
                //string version = fileName.Replace(".sql", string.Empty);
                //version = version.Remove(0, posVersion + 2);
                return install.iUpdate.Update(fileName);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

        }
        public static bool BeforeUpdate()
        {
            return install.iUpdate.BeforeUpdate();
        }
        public static string VersionDataBase()
        {
            return install.iUpdate.VersionDatabase;
        }
        public static bool AfterUpdate()
        {
            return install.iUpdate.AfterUpdate();
        }
    }

    class EcsUpdate : IUpdate
    {
        string servername, dbName, dbUser, dbPassWord;
        string lasVersion = string.Empty;
        public string VersionDatabase
        {
            get
            {
                try
                {
                    using (SqlConnection cnn = new SqlConnection())
                    {
                        cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { servername, dbName, dbUser, dbPassWord });
                        cnn.Open();

                        SqlCommand sqlCmd = new SqlCommand();
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Connection = cnn;

                        string cmdText = @"SELECT COUNT(*)
                                           FROM   INFORMATION_SCHEMA.TABLES 
                                           WHERE  TABLE_NAME = N't_haiquan_version' 
                                           AND TABLE_TYPE = 'BASE TABLE'";

                        sqlCmd.CommandText = cmdText;
                        object objRet = sqlCmd.ExecuteScalar();
                        if (objRet != null && int.Parse(objRet.ToString()) != 0)
                        {
                            //cmdText = "select max([version]) as [version] from t_haiquan_version";
                            cmdText = "select max(cast([version] as decimal(5,2))) as [version] from t_haiquan_version"; //TODO: Hungtq updated 14/11/2013.
                            sqlCmd.CommandText = cmdText;
                            objRet = sqlCmd.ExecuteScalar();
                            if (objRet != null)
                                lasVersion = System.Convert.ToDecimal(objRet).ToString("##0.0#", new System.Globalization.CultureInfo("en-US")); //TODO: Hungtq, 14/11/2013
                            else
                            {
                                cmdText = @"INSERT INTO [dbo].[t_HaiQuan_Version]
                                               ([Version]
                                               ,[Date]
                                               ,[Notes])
                                         VALUES
                                               ('1.0'
                                               ,GETDATE()
                                               ,'AUTOUPDATE-DEFAULT V1.0')";
                                lasVersion = "1.0";
                                sqlCmd.CommandText = cmdText;
                                sqlCmd.ExecuteNonQuery();
                            }
                        }
                        else lasVersion = string.Empty;
                    }
                }
                catch { lasVersion = string.Empty; }
                return lasVersion;
            }
        }
        public bool BeforeUpdate()
        {
            try
            {
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Data\\Update\\LogUpdate"))
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Data\\Update\\LogUpdate");
                if (string.IsNullOrEmpty(servername) || string.IsNullOrEmpty(dbName) || string.IsNullOrEmpty(dbUser))
                    return false;
                else
                {
                    bool connection = false;
                    try
                    {
                        using (SqlConnection cnn = new SqlConnection())
                        {
                            //Kiem tra ket noi den CSDL truoc khi thuc hien backup.
                            cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout={4}", new object[] { servername, dbName, dbUser, dbPassWord, 0 }); //0s
                            cnn.Open();
                            cnn.Close();

                            //Neu da ket noi duoc den CSDL thi thuc hien tien trinh backup
                            cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout={4}", new object[] { servername, dbName, dbUser, dbPassWord, 30 * 5 }); //150 giay = 5 phut = 30s * 5
                            cnn.Open();
                            connection = true;
                            string cmdText = @"
			                                --Default backup database LanNT
                                            --Create 20-09-2011
                                            --Modifier 21-12-2011
                                            --Backup database at folder data *.mdf with format dbName_dd_MM_yyyy.bak
                                            BEGIN

                                            DECLARE @filedata  AS NVARCHAR(255)
                                            DECLARE @filename  AS NVARCHAR(255)
                                            DECLARE @dir  AS NVARCHAR(255)
                                            DECLARE @dbName AS NVARCHAR(255)

                                            SET @filedata=(
                                            SELECT TOP(1) 	filename
                                                from sysfiles
                                                order by fileid)
                                            SET @dbName = (select TOP(1) name from sys.sysdatabases  WHERE filename=@filedata)
                                            SET @filename =
                                            LTRIM(
                                              RTRIM(
                                               REVERSE(
                                                SUBSTRING(
                                                 REVERSE(@filedata),
                                                 0,
                                                 CHARINDEX('\', REVERSE(@filedata),0)
                                                )
                                               )
                                              )
                                             )
                                            SET @dir = REPLACE(@filedata,@filename,'')
                                            SET @filename = @dbName+ '_' +
                                                             REPLACE(CONVERT(VARCHAR(255), GETDATE(), 103),'/','_') +'.bak'

                                            SET @filename = @dir + @filename			                                
                                            BACKUP DATABASE @dbName
                                            TO DISK =@filename
                                            WITH FORMAT,INIT,MEDIANAME = 'Z_SQLServerBackups',
                                            NAME = 'Full Backup of AdventureWorks',
                                            SKIP,NOREWIND,NOUNLOAD,STATS = 10;
                                            END
                                         ";
                            SqlCommand sqlCmd = new SqlCommand(cmdText);
                            sqlCmd.CommandType = CommandType.Text;
                            sqlCmd.Connection = cnn;
                            sqlCmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage("Lỗi thực hiện sao lưu dữ liệu", ex);
                        lasVersion = string.Empty;
                        return false;
                    }
                    if (!System.IO.File.Exists("osql.exe"))
                    {
                        Logger.LocalLogger.Instance().WriteMessage(new Exception("Thiếu tệp tin OSQL"));
                        return false;
                    }
                    else return connection;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return false; }
        }
        public bool AfterUpdate()
        {
            try
            {
                string[] paras = new string[3] { string.Empty, string.Empty, string.Empty };
                string[] files = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.mdf", System.IO.SearchOption.AllDirectories);
                SearchFile(paras, files);
                files = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.ldf", System.IO.SearchOption.AllDirectories);
                SearchFile(paras, files);
                if (paras[0] == string.Empty || paras[1] == string.Empty)
                    return false;

                try
                {
                    using (SqlConnection cnn = new SqlConnection())
                    {
                        cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { servername, dbName, dbUser, dbPassWord });
                        cnn.Open();
                        cnn.Close();
                    }
                }
                catch (SqlException)
                {
                    if (System.Windows.Forms.MessageBox.Show("Phát hiện dữ liệu mặc định chưa thiết lập\nBạn có muốn thiết lập tự động không?", "Cài đặt dữ liệu",
                         System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return false;
                    try
                    {
                        paras[2] = dbName;

                        using (SqlConnection cnn = new SqlConnection())
                        {
                            cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { servername, "master", dbUser, dbPassWord });
                            cnn.Open();
                            string sfmtCreateData = string.Format(@"
                                                                    CREATE DATABASE [{0}] ON 
                                                                    ( FILENAME = N'{1}' ),
                                                                    ( FILENAME = N'{2}' )
                                                                     FOR ATTACH
                                                                    ", new object[]{paras[2],paras[0],paras[1]                                                                     
                                                                 });
                            SqlCommand sqlCmd = new SqlCommand(sfmtCreateData);
                            sqlCmd.CommandType = CommandType.Text;
                            sqlCmd.Connection = cnn;
                            sqlCmd.ExecuteNonQuery();
                        }
                        System.Xml.XmlDocument docApp = new System.Xml.XmlDocument();
                        string cfgName = string.Empty;
                        if (File.Exists(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config"))
                            cfgName = CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config";
                        else if (File.Exists(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config"))
                            cfgName = CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.GC.exe.Config";
                        else if (File.Exists(CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config"))
                            cfgName = CommonUnitity.SystemBinUrl + "SOFTECH.ECS.TQDT.SXXK.exe.Config";

                        if (cfgName != string.Empty)
                        {
                            docApp.Load(cfgName);
                            XmlNode nodeCnn = docApp.SelectSingleNode("configuration/connectionStrings/add[@name=\"MSSQL\"]");
                            if (nodeCnn != null/* && !string.IsNullOrEmpty(nodeCnn.Attributes["name"].Value) && nodeCnn.Attributes["name"].Value.ToUpper() == "MSSQL"*/)
                            {
                                nodeCnn.Attributes["connectionString"].Value = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { servername, dbName, dbUser, dbPassWord });
                            }
                            docApp.Save(cfgName);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Thông báo lỗi",
                             System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
                finally
                {
                }
                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            return false;
        }

        private static void SearchFile(string[] paras, string[] files)
        {
            foreach (string item in files)
            {
                if (Path.GetExtension(item).ToLower() == ".mdf")
                {
                    paras[0] = item;
                }
                else if (Path.GetExtension(item).ToLower() == ".ldf")
                {
                    paras[1] = item;
                }
                if (paras[0] != string.Empty && paras[1] != string.Empty)
                    break;
            }
        }
        public EcsUpdate()
        {
            try
            {
                if (!System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "ConfigDoanhNghiep\\ConfigDoanhNghiep.xml")) return;
                string sfmt = string.Empty;
                XmlDocument doc = new XmlDocument();
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + "ConfigDoanhNghiep\\ConfigDoanhNghiep.xml");

                servername = GetNode(doc, "Server").InnerText;
                dbName = GetNode(doc, "Database").InnerText;
                dbUser = GetNode(doc, "UserName").InnerText;
                dbPassWord = GetNode(doc, "Password").InnerText;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

        }
        private XmlNode GetNode(XmlDocument doc, string key)
        {
            foreach (XmlNode node in doc.GetElementsByTagName("Config"))
            {
                if (node["Key"].InnerText.Trim().ToLower() == key.Trim().ToLower())
                {
                    return node["Value"];
                }
            }
            return null;
        }
        public bool Update(string fileName)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection())
                {
                    cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { servername, dbName, dbUser, dbPassWord });
                    cnn.Open();
                    cnn.Close();
                }

                #region Update theo Osql
                string sfmtArgs = string.Format(@"-S {0} -U {1} -P {2} -d {3} -i ""{4}"" -o ""Data\Update\LogUpdate\{5}.log""", new object[] { servername, dbUser, dbPassWord, dbName, fileName, Path.GetFileNameWithoutExtension(fileName) });
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("Osql.exe", sfmtArgs);
                psi.CreateNoWindow = true;
                psi.ErrorDialog = true;
                psi.UseShellExecute = false;
                System.Diagnostics.Process p = System.Diagnostics.Process.Start(psi);
                p.WaitForExit();
                return true;
                #endregion
                #region Update theo SqlCommand
                //using (SqlConnection cnn = new SqlConnection())
                //{
                //    cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout=1500", new object[] { servername, dbName, dbUser, dbPassWord });
                //    cnn.Open();
                //    string script = ReadText(fileName);
                //    SqlCommand sqlCmd = new SqlCommand(script);
                //    sqlCmd.CommandType = CommandType.Text;
                //    sqlCmd.Connection = cnn;
                //    sqlCmd.ExecuteNonQuery();
                //    return true;
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Lỗi thực hiện cập nhật Script SQL '" + fileName + "'", ex);
                return false;
            }

        }
        private string ReadText(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            string script = file.OpenText().ReadToEnd();
            return script;
        }
    }

    class EcsKTXUpdate : IUpdate
    {
        string servername, dbName, dbUser, dbPassWord;
        string lasVersion = "1.0";
        public string VersionDatabase
        {
            get
            {
                return lasVersion;
            }
        }
        public bool BeforeUpdate()
        {
            return true;
        }
        public bool AfterUpdate()
        {
            try
            {
                try
                {

                    using (SqlConnection cnn = new SqlConnection())
                    {
                        cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { servername, dbName, dbUser, dbPassWord });
                        cnn.Open();
                        cnn.Close();
                    }
                }
                catch (SqlException)
                {
                    try
                    {
                        System.Xml.XmlDocument docApp = new System.Xml.XmlDocument();
                        string cfgName = string.Empty;
                        //if (File.Exists(CommonUnitity.SystemBinUrl + "ECS_KD.exe.Config"))
                        //    cfgName = CommonUnitity.SystemBinUrl + "ECS_KD.exe.Config";
                        //else if (File.Exists(CommonUnitity.SystemBinUrl + "ECS_GC.exe.Config"))
                        //    cfgName = CommonUnitity.SystemBinUrl + "ECS_GC.exe.Config";
                        //else if (File.Exists(CommonUnitity.SystemBinUrl + "ECS_SXXK.exe.Config"))
                        //    cfgName = CommonUnitity.SystemBinUrl + "ECS_SXXK.exe.Config";
                        cfgName = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;

                        if (cfgName != string.Empty)
                        {
                            docApp.Load(cfgName);
                            XmlNode nodeCnn = docApp.SelectSingleNode("configuration/connectionStrings/add[@name=\"MSSQL\"]");
                            if (nodeCnn != null/* && !string.IsNullOrEmpty(nodeCnn.Attributes["name"].Value) && nodeCnn.Attributes["name"].Value.ToUpper() == "MSSQL"*/)
                            {
                                nodeCnn.Attributes["connectionString"].Value = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { servername, dbName, dbUser, dbPassWord });
                            }
                            docApp.Save(cfgName);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Thông báo lỗi",
                             System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            return false;
        }

        public EcsKTXUpdate()
        {
            try
            {
                if (!System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "ConfigDoanhNghiep\\ConfigDoanhNghiep.xml")) return;
                string sfmt = string.Empty;
                XmlDocument doc = new XmlDocument();
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + "ConfigDoanhNghiep\\ConfigDoanhNghiep.xml");

                servername = GetNode(doc, "Server").InnerText;
                dbName = GetNode(doc, "Database").InnerText;
                dbUser = GetNode(doc, "UserName").InnerText;
                dbPassWord = GetNode(doc, "Password").InnerText;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        public bool Update(string fileName)
        {
            return true;
        }

        private string ReadText(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            string script = file.OpenText().ReadToEnd();
            return script;
        }
        private XmlNode GetNode(XmlDocument doc, string key)
        {
            foreach (XmlNode node in doc.GetElementsByTagName("Config"))
            {
                if (node["Key"].InnerText.Trim().ToLower() == key.Trim().ToLower())
                {
                    return node["Value"];
                }
            }
            return null;
        }
    }

    class HRSOnlineUpdate : IUpdate
    {
        //string servername, dbName, dbUser, dbPassWord;
        string lasVersion = "1.0";
        public string VersionDatabase
        {
            get
            {
                return lasVersion;
            }
        }
        public bool BeforeUpdate()
        {
            return true;
        }
        public bool AfterUpdate()
        {
            return true;
        }

        public HRSOnlineUpdate()
        {
        }
        public bool Update(string fileName)
        {
            return true;
        }
        private string ReadText(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            string script = file.OpenText().ReadToEnd();
            return script;
        }
    }

}
