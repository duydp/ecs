using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace WindowsFormsApplication1
{
	public partial class INBOX : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string InputMSGID { set; get; }
		public string RTPTag { set; get; }
		public string Messages { set; get; }
		public string temp1 { set; get; }
		public string temp2 { set; get; }
		public string temp3 { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<INBOX> ConvertToCollection(IDataReader reader)
		{
			IList<INBOX> collection = new List<INBOX>();
			while (reader.Read())
			{
				INBOX entity = new INBOX();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMSGID"))) entity.InputMSGID = reader.GetString(reader.GetOrdinal("InputMSGID"));
				if (!reader.IsDBNull(reader.GetOrdinal("RTPTag"))) entity.RTPTag = reader.GetString(reader.GetOrdinal("RTPTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("Messages"))) entity.Messages = reader.GetString(reader.GetOrdinal("Messages"));
				if (!reader.IsDBNull(reader.GetOrdinal("temp1"))) entity.temp1 = reader.GetString(reader.GetOrdinal("temp1"));
				if (!reader.IsDBNull(reader.GetOrdinal("temp2"))) entity.temp2 = reader.GetString(reader.GetOrdinal("temp2"));
				if (!reader.IsDBNull(reader.GetOrdinal("temp3"))) entity.temp3 = reader.GetString(reader.GetOrdinal("temp3"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<INBOX> collection, long id)
        {
            foreach (INBOX item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_MSG_INBOX VALUES(@InputMSGID, @RTPTag, @Messages, @temp1, @temp2, @temp3)";
            string update = "UPDATE t_MSG_INBOX SET InputMSGID = @InputMSGID, RTPTag = @RTPTag, Messages = @Messages, temp1 = @temp1, temp2 = @temp2, temp3 = @temp3 WHERE ID = @ID";
            string delete = "DELETE FROM t_MSG_INBOX WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMSGID", SqlDbType.VarChar, "InputMSGID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RTPTag", SqlDbType.VarChar, "RTPTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Messages", SqlDbType.VarChar, "Messages", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@temp1", SqlDbType.VarChar, "temp1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@temp2", SqlDbType.VarChar, "temp2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@temp3", SqlDbType.VarChar, "temp3", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMSGID", SqlDbType.VarChar, "InputMSGID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RTPTag", SqlDbType.VarChar, "RTPTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Messages", SqlDbType.VarChar, "Messages", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@temp1", SqlDbType.VarChar, "temp1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@temp2", SqlDbType.VarChar, "temp2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@temp3", SqlDbType.VarChar, "temp3", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_MSG_INBOX VALUES(@InputMSGID, @RTPTag, @Messages, @temp1, @temp2, @temp3)";
            string update = "UPDATE t_MSG_INBOX SET InputMSGID = @InputMSGID, RTPTag = @RTPTag, Messages = @Messages, temp1 = @temp1, temp2 = @temp2, temp3 = @temp3 WHERE ID = @ID";
            string delete = "DELETE FROM t_MSG_INBOX WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMSGID", SqlDbType.VarChar, "InputMSGID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RTPTag", SqlDbType.VarChar, "RTPTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Messages", SqlDbType.VarChar, "Messages", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@temp1", SqlDbType.VarChar, "temp1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@temp2", SqlDbType.VarChar, "temp2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@temp3", SqlDbType.VarChar, "temp3", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMSGID", SqlDbType.VarChar, "InputMSGID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RTPTag", SqlDbType.VarChar, "RTPTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Messages", SqlDbType.VarChar, "Messages", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@temp1", SqlDbType.VarChar, "temp1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@temp2", SqlDbType.VarChar, "temp2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@temp3", SqlDbType.VarChar, "temp3", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static INBOX Load(long id)
		{
			const string spName = "[dbo].[p_MSG_INBOX_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<INBOX> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<INBOX> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<INBOX> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_MSG_INBOX_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_MSG_INBOX_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_MSG_INBOX_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_MSG_INBOX_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertINBOX(string inputMSGID, string rTPTag, string messages, string temp1, string temp2, string temp3)
		{
			INBOX entity = new INBOX();	
			entity.InputMSGID = inputMSGID;
			entity.RTPTag = rTPTag;
			entity.Messages = messages;
			entity.temp1 = temp1;
			entity.temp2 = temp2;
			entity.temp3 = temp3;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_MSG_INBOX_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@InputMSGID", SqlDbType.VarChar, InputMSGID);
			db.AddInParameter(dbCommand, "@RTPTag", SqlDbType.VarChar, RTPTag);
			db.AddInParameter(dbCommand, "@Messages", SqlDbType.VarChar, Messages);
			db.AddInParameter(dbCommand, "@temp1", SqlDbType.VarChar, temp1);
			db.AddInParameter(dbCommand, "@temp2", SqlDbType.VarChar, temp2);
			db.AddInParameter(dbCommand, "@temp3", SqlDbType.VarChar, temp3);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<INBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (INBOX item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateINBOX(long id, string inputMSGID, string rTPTag, string messages, string temp1, string temp2, string temp3)
		{
			INBOX entity = new INBOX();			
			entity.ID = id;
			entity.InputMSGID = inputMSGID;
			entity.RTPTag = rTPTag;
			entity.Messages = messages;
			entity.temp1 = temp1;
			entity.temp2 = temp2;
			entity.temp3 = temp3;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_MSG_INBOX_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@InputMSGID", SqlDbType.VarChar, InputMSGID);
			db.AddInParameter(dbCommand, "@RTPTag", SqlDbType.VarChar, RTPTag);
			db.AddInParameter(dbCommand, "@Messages", SqlDbType.VarChar, Messages);
			db.AddInParameter(dbCommand, "@temp1", SqlDbType.VarChar, temp1);
			db.AddInParameter(dbCommand, "@temp2", SqlDbType.VarChar, temp2);
			db.AddInParameter(dbCommand, "@temp3", SqlDbType.VarChar, temp3);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<INBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (INBOX item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateINBOX(long id, string inputMSGID, string rTPTag, string messages, string temp1, string temp2, string temp3)
		{
			INBOX entity = new INBOX();			
			entity.ID = id;
			entity.InputMSGID = inputMSGID;
			entity.RTPTag = rTPTag;
			entity.Messages = messages;
			entity.temp1 = temp1;
			entity.temp2 = temp2;
			entity.temp3 = temp3;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_MSG_INBOX_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@InputMSGID", SqlDbType.VarChar, InputMSGID);
			db.AddInParameter(dbCommand, "@RTPTag", SqlDbType.VarChar, RTPTag);
			db.AddInParameter(dbCommand, "@Messages", SqlDbType.VarChar, Messages);
			db.AddInParameter(dbCommand, "@temp1", SqlDbType.VarChar, temp1);
			db.AddInParameter(dbCommand, "@temp2", SqlDbType.VarChar, temp2);
			db.AddInParameter(dbCommand, "@temp3", SqlDbType.VarChar, temp3);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<INBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (INBOX item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteINBOX(long id)
		{
			INBOX entity = new INBOX();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_MSG_INBOX_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_MSG_INBOX_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<INBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (INBOX item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}