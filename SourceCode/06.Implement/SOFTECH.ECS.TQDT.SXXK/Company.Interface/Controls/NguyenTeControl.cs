using System;
using System.Data;
using System.Windows.Forms;
using Company.BLL.DuLieuChuan;

namespace Company.Interface.Controls
{
    public partial class NguyenTeControl : UserControl
    {
        public NguyenTeControl()
        {
            this.InitializeComponent();
        }
        public decimal TyGia = 0;
        public string Ma
        {
            set
            {
                this.txtMa.Text = value;
                this.cbTen.Value = this.txtMa.Text.PadRight(3);
            }
            get { return this.txtMa.Text; }
        }
        public bool ReadOnly
        {
            set
            {
                this.txtMa.ReadOnly = value;
                this.cbTen.ReadOnly = value;
            }
            get { return this.cbTen.ReadOnly; }
        }
        public string ErrorMessage
        {
            get { return this.rfvTen.ErrorMessage; }
            set { this.rfvTen.ErrorMessage = value; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.txtMa.VisualStyleManager = value;
                this.cbTen.VisualStyleManager = value;
            }
            get
            {
                return this.txtMa.VisualStyleManager;
            }
        }
        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);

        private void loadData()
        {
            if (this.txtMa.Text.Trim().Length == 0) this.txtMa.Text = GlobalSettings.NGUYEN_TE_MAC_DINH;
            dtNguyenTe.Rows.Clear();
            DataTable dt = NguyenTe.SelectAll().Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                this.dtNguyenTe.ImportRow(row);
            }
            this.cbTen.Value = this.txtMa.Text.PadRight(3);
        }

        private void NguyenTeControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.loadData();
            }
        }

        private void cbList_ValueChanged(object sender, EventArgs e)
        {
            this.txtMa.Text = this.cbTen.Value.ToString().Trim();
            TyGia = NguyenTe.GetTyGia(txtMa.Text.Trim());
            if (this.ValueChanged != null) this.ValueChanged(sender, e);
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {
            this.txtMa.Text = this.txtMa.Text.Trim();
            if (this.dtNguyenTe.Select("ID='" + txtMa.Text.Trim() + "'").Length == 0)
            {
                this.txtMa.Text = GlobalSettings.NGUYEN_TE_MAC_DINH;
            }
            TyGia = NguyenTe.GetTyGia(txtMa.Text.Trim());
            this.cbTen.Value = this.txtMa.Text.PadRight(3);
        }
    }
}