﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.WareHouse
{
	public partial class T_KHOKETOAN_QUYTACDANHSO : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string LOAICHUNGTU { set; get; }
		public string TIENTO { set; get; }
		public long GIATRIPHANSO { set; get; }
		public long DODAISO { set; get; }
		public string HIENTHI { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KHOKETOAN_QUYTACDANHSO> ConvertToCollection(IDataReader reader)
		{
			List<T_KHOKETOAN_QUYTACDANHSO> collection = new List<T_KHOKETOAN_QUYTACDANHSO>();
			while (reader.Read())
			{
				T_KHOKETOAN_QUYTACDANHSO entity = new T_KHOKETOAN_QUYTACDANHSO();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LOAICHUNGTU"))) entity.LOAICHUNGTU = reader.GetString(reader.GetOrdinal("LOAICHUNGTU"));
				if (!reader.IsDBNull(reader.GetOrdinal("TIENTO"))) entity.TIENTO = reader.GetString(reader.GetOrdinal("TIENTO"));
				if (!reader.IsDBNull(reader.GetOrdinal("GIATRIPHANSO"))) entity.GIATRIPHANSO = reader.GetInt64(reader.GetOrdinal("GIATRIPHANSO"));
				if (!reader.IsDBNull(reader.GetOrdinal("DODAISO"))) entity.DODAISO = reader.GetInt64(reader.GetOrdinal("DODAISO"));
				if (!reader.IsDBNull(reader.GetOrdinal("HIENTHI"))) entity.HIENTHI = reader.GetString(reader.GetOrdinal("HIENTHI"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KHOKETOAN_QUYTACDANHSO> collection, long id)
        {
            foreach (T_KHOKETOAN_QUYTACDANHSO item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KHOKETOAN_QUYTACDANHSO VALUES(@LOAICHUNGTU, @TIENTO, @GIATRIPHANSO, @DODAISO, @HIENTHI)";
            string update = "UPDATE T_KHOKETOAN_QUYTACDANHSO SET LOAICHUNGTU = @LOAICHUNGTU, TIENTO = @TIENTO, GIATRIPHANSO = @GIATRIPHANSO, DODAISO = @DODAISO, HIENTHI = @HIENTHI WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_QUYTACDANHSO WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAICHUNGTU", SqlDbType.NVarChar, "LOAICHUNGTU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TIENTO", SqlDbType.NVarChar, "TIENTO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GIATRIPHANSO", SqlDbType.BigInt, "GIATRIPHANSO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DODAISO", SqlDbType.BigInt, "DODAISO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HIENTHI", SqlDbType.NVarChar, "HIENTHI", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAICHUNGTU", SqlDbType.NVarChar, "LOAICHUNGTU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TIENTO", SqlDbType.NVarChar, "TIENTO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GIATRIPHANSO", SqlDbType.BigInt, "GIATRIPHANSO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DODAISO", SqlDbType.BigInt, "DODAISO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HIENTHI", SqlDbType.NVarChar, "HIENTHI", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KHOKETOAN_QUYTACDANHSO VALUES(@LOAICHUNGTU, @TIENTO, @GIATRIPHANSO, @DODAISO, @HIENTHI)";
            string update = "UPDATE T_KHOKETOAN_QUYTACDANHSO SET LOAICHUNGTU = @LOAICHUNGTU, TIENTO = @TIENTO, GIATRIPHANSO = @GIATRIPHANSO, DODAISO = @DODAISO, HIENTHI = @HIENTHI WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_QUYTACDANHSO WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAICHUNGTU", SqlDbType.NVarChar, "LOAICHUNGTU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TIENTO", SqlDbType.NVarChar, "TIENTO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GIATRIPHANSO", SqlDbType.BigInt, "GIATRIPHANSO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DODAISO", SqlDbType.BigInt, "DODAISO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HIENTHI", SqlDbType.NVarChar, "HIENTHI", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAICHUNGTU", SqlDbType.NVarChar, "LOAICHUNGTU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TIENTO", SqlDbType.NVarChar, "TIENTO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GIATRIPHANSO", SqlDbType.BigInt, "GIATRIPHANSO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DODAISO", SqlDbType.BigInt, "DODAISO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HIENTHI", SqlDbType.NVarChar, "HIENTHI", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KHOKETOAN_QUYTACDANHSO Load(long id)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KHOKETOAN_QUYTACDANHSO> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KHOKETOAN_QUYTACDANHSO> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KHOKETOAN_QUYTACDANHSO> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KHOKETOAN_QUYTACDANHSO(string lOAICHUNGTU, string tIENTO, long gIATRIPHANSO, long dODAISO, string hIENTHI)
		{
			T_KHOKETOAN_QUYTACDANHSO entity = new T_KHOKETOAN_QUYTACDANHSO();	
			entity.LOAICHUNGTU = lOAICHUNGTU;
			entity.TIENTO = tIENTO;
			entity.GIATRIPHANSO = gIATRIPHANSO;
			entity.DODAISO = dODAISO;
			entity.HIENTHI = hIENTHI;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LOAICHUNGTU", SqlDbType.NVarChar, LOAICHUNGTU);
			db.AddInParameter(dbCommand, "@TIENTO", SqlDbType.NVarChar, TIENTO);
			db.AddInParameter(dbCommand, "@GIATRIPHANSO", SqlDbType.BigInt, GIATRIPHANSO);
			db.AddInParameter(dbCommand, "@DODAISO", SqlDbType.BigInt, DODAISO);
			db.AddInParameter(dbCommand, "@HIENTHI", SqlDbType.NVarChar, HIENTHI);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KHOKETOAN_QUYTACDANHSO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_QUYTACDANHSO item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KHOKETOAN_QUYTACDANHSO(long id, string lOAICHUNGTU, string tIENTO, long gIATRIPHANSO, long dODAISO, string hIENTHI)
		{
			T_KHOKETOAN_QUYTACDANHSO entity = new T_KHOKETOAN_QUYTACDANHSO();			
			entity.ID = id;
			entity.LOAICHUNGTU = lOAICHUNGTU;
			entity.TIENTO = tIENTO;
			entity.GIATRIPHANSO = gIATRIPHANSO;
			entity.DODAISO = dODAISO;
			entity.HIENTHI = hIENTHI;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KHOKETOAN_QUYTACDANHSO_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LOAICHUNGTU", SqlDbType.NVarChar, LOAICHUNGTU);
			db.AddInParameter(dbCommand, "@TIENTO", SqlDbType.NVarChar, TIENTO);
			db.AddInParameter(dbCommand, "@GIATRIPHANSO", SqlDbType.BigInt, GIATRIPHANSO);
			db.AddInParameter(dbCommand, "@DODAISO", SqlDbType.BigInt, DODAISO);
			db.AddInParameter(dbCommand, "@HIENTHI", SqlDbType.NVarChar, HIENTHI);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KHOKETOAN_QUYTACDANHSO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_QUYTACDANHSO item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KHOKETOAN_QUYTACDANHSO(long id, string lOAICHUNGTU, string tIENTO, long gIATRIPHANSO, long dODAISO, string hIENTHI)
		{
			T_KHOKETOAN_QUYTACDANHSO entity = new T_KHOKETOAN_QUYTACDANHSO();			
			entity.ID = id;
			entity.LOAICHUNGTU = lOAICHUNGTU;
			entity.TIENTO = tIENTO;
			entity.GIATRIPHANSO = gIATRIPHANSO;
			entity.DODAISO = dODAISO;
			entity.HIENTHI = hIENTHI;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LOAICHUNGTU", SqlDbType.NVarChar, LOAICHUNGTU);
			db.AddInParameter(dbCommand, "@TIENTO", SqlDbType.NVarChar, TIENTO);
			db.AddInParameter(dbCommand, "@GIATRIPHANSO", SqlDbType.BigInt, GIATRIPHANSO);
			db.AddInParameter(dbCommand, "@DODAISO", SqlDbType.BigInt, DODAISO);
			db.AddInParameter(dbCommand, "@HIENTHI", SqlDbType.NVarChar, HIENTHI);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KHOKETOAN_QUYTACDANHSO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_QUYTACDANHSO item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KHOKETOAN_QUYTACDANHSO(long id)
		{
			T_KHOKETOAN_QUYTACDANHSO entity = new T_KHOKETOAN_QUYTACDANHSO();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KHOKETOAN_QUYTACDANHSO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_QUYTACDANHSO item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}