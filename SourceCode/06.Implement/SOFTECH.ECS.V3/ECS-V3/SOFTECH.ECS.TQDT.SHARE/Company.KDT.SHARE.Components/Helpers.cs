﻿
using System;
using System.Collections.Generic;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Text;
using System.ServiceModel;
using System.Linq;
using System.Threading;
using System.Globalization;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using Company.KDT.SHARE.Components.Utils;
using System.Runtime.Serialization.Formatters.Binary;

namespace Company.KDT.SHARE.Components
{

    public class Helpers
    {

        /// <summary>
        /// Message phong bì đóng gói thông tin khai báo của doanh nghiệp gửi lên Hải quan, tất cả các message gửi đến Hải quan đều được đóng gói bên ngoài bởi message này
        /// Chú ý các tính năng format kiểu dữ liệu xem tại
        /// http://msdn.microsoft.com/en-us/library/system.string.format.aspx
        /// http://msdn.microsoft.com/en-us/library/26etazsy.aspx
        /// </summary>
        /// <param name="sendFrom">Nơi gửi</param>
        /// <param name="sendTo">Nơi đến</param>
        /// <param name="subject">Tiêu đề</param>
        /// <param name="content">Nội dung</param>
        /// <param name="signature">Chữ ký số</param>
        /// <returns></returns>
        public static string BuildSend(NameBase sendFrom, NameBase sendTo, SubjectBase subject, object objContent, MessageSignature signature)
        {

            return BuildSend(sendFrom, sendTo, subject, objContent, signature, false);

        }
        public static MessageSignature GetSignature(string content)
        {
            MessageSignature ret = null;
            if (Globals.IsSignature)
                try
                {
                    if (!string.IsNullOrEmpty(Globals.GetX509CertificatedName))
                    {
                        ret = new MessageSignature();

                        System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = Cryptography.GetStoreX509Certificate2(Globals.GetX509CertificatedName);
                        if (x509Certificate2 != null)
                        {
                            RSACryptographyHelper rsacryptography = new RSACryptographyHelper(x509Certificate2);
#if DEBUG_SIGN
                            
                            #region Test Compare Softech And Thai Son --LanNT
                            XmlDocument doc = new XmlDocument();
                            doc.Load(@"C:\Users\LanNT\Desktop\Errors\28-04-2012-08-04-04_8b30549d-e687-4c78-b26a-17f18f04789d.xml");
                            string nodeContent = doc.SelectSingleNode("Envelope/Body/Content").InnerXml;
                            string nodeSing = doc.SelectSingleNode("Envelope/Body/Signature/data").InnerText;
                            string nodeCert = doc.SelectSingleNode("Envelope/Body/Signature/fileCert").InnerText;
                            nodeContent = ConvertFromBase64(nodeContent);
                            string s = nodeContent;;
                            s = s.Substring(0, s.Length - 1);
                            //XmlDocument docNew = new XmlDocument();
                           //docNew.LoadXml(s);
                            string sSign = rsacryptography.SignHash(s, Globals.PasswordSign);
                           // string sSingProduct = rsacryptography.SignHash(s);  
                            #endregion
#else

                            string sData = rsacryptography.SignHash(content, Globals.PasswordSign);
                            string sCert = rsacryptography.RawData;
                            ret.Data = sData;
                            ret.FileCert = sCert;
#endif
                        }
                        else
                            throw new Exception("Bạn đã thiết lập cấu hình chữ ký số\r\nLỗi là do thiết bị (Usb token) này chưa được gắn vào");
                    }
                    else throw new Exception("Bạn đã thiết lập cấu hình chữ ký số\r\nLỗi là do loại chữ ký số chưa được xác định");
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("wrong PIN") || ex.Message.Contains("PIN is incorrect"))
                    {
                        throw new Exception("Mật khẩu xác nhận bị sai\r\nVui lòng thử lại");
                    }
                    else
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        throw new Exception("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin");
                    }
                }
            return ret;
        }
        public static string BuildSend(NameBase sendFrom, NameBase sendTo, SubjectBase subject, object objContent, MessageSignature signature, bool removeNameSpace)
        {
            MessageSend<string> msgSend = Envelope<string>(sendFrom, sendTo, subject);
            string content = Serializer(objContent, true, removeNameSpace);
            signature = GetSignature(content);
            // content = ConvertToBase64(content);
            // content = MultipleLines(content);
            msgSend.MessageBody.MessageContent.Text = content;

            if (signature != null)
            {
                msgSend.MessageBody.MessageSignature = signature;
            }
            return Serializer(msgSend);
        }
        /// <summary>
        /// Phong bì message
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sendFrom"></param>
        /// <param name="sendTo"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        
        public static MessageSend<T> Envelope<T>(NameBase sendFrom, NameBase sendTo, SubjectBase subject)
        {
            if (subject == null) throw new ArgumentNullException("Tiêu chí gửi không được null");
            if (sendFrom == null) throw new ArgumentNullException("Nơi gửi không được rỗng");
            if (sendTo == null) throw new ArgumentNullException("Nơi đến không được null");

            MessageSend<T> msgSend = new MessageSend<T>()
            {
                MessageHeader = new MessageHeader()
                {
                    Reference = new Reference()
                    {
                        Version = Globals.VersionSend,
                        ReplyTo = string.Empty,
                        MessageID = Guid.NewGuid().ToString().Replace("-", string.Empty)

                    },
                    SendApplication = new SendApplication()
                    {
                        Name = Globals.AppSend,
                        Version = Globals.VersionSend,
                        CompanyName = "SOFTECHDANANG",
                        CompanyIdentity = "0400392263",
                        //CompanyIdentity = 
                        CreateMessageIssue = Guid.NewGuid().ToString(),
                    },
                    From = sendFrom,
                    To = sendTo,
                    Subject = new Subject()

                }
                ,
                MessageBody = new MessageBody<T>()

            };

            msgSend.MessageHeader.ProcedureType = "2";//TQDT , KTX is 1
            if (!string.IsNullOrEmpty(subject.Type))
                msgSend.MessageHeader.Subject.Type = subject.Type;
            msgSend.MessageHeader.Subject.Function = subject.Function;
            msgSend.MessageHeader.Subject.Reference = subject.Reference;
            #region KhanhHN - Bổ sung cho đồng bộ dữ liệu
            if (!string.IsNullOrEmpty(subject.DeclarationOffice))
                msgSend.MessageHeader.Subject.DeclarationOffice = subject.DeclarationOffice;
            if (!string.IsNullOrEmpty(subject.Issue))
                msgSend.MessageHeader.Subject.Issue = subject.Issue;
            if (!string.IsNullOrEmpty(subject.IssueLocation))
                msgSend.MessageHeader.Subject.IssueLocation = subject.IssueLocation;
            if (!string.IsNullOrEmpty(subject.Issuer))
                msgSend.MessageHeader.Subject.Issuer = subject.Issuer;
            #endregion
            msgSend.MessageBody = new MessageBody<T>();
            msgSend.MessageBody.MessageContent = new MessageContent<T>();
           
             

#if DEBUG
            //msgSend.MessageHeader.SendApplication.CompanyIdentity = "0101300842";

#endif
            return msgSend;
        }
        /// <summary>
        /// Lấy phản hồi từ phía hải quan
        /// </summary>
        /// <param name="sendFrom">Nơi gửi</param>
        /// <param name="sendTo">Nơi đến</param>
        /// <param name="subject">Tiêu đề gửi</param>
        /// <param name="signature">Chữ ký số</param>
        /// <returns></returns>
        public static string BuildFeedBack(NameBase sendFrom, NameBase sendTo, SubjectBase subject, MessageSignature signature)
        {
            string ret = GetResourceString("Company.KDT.SHARE.Components.PhongBi.PhongBiV3.xml");
            string subType = subject.Type;
            subject.Type = null;
            string content = Serializer(subject, Encoding.UTF8, true, true);
            subject.Type = subType;
            content = ConvertToBase64(content);
            signature = GetSignature(content);
            //content = MultipleLines(content);
            MessageSend<string> msgSend = Envelope<string>(sendFrom, sendTo, subject);
            ret = string.Format(ret, new object[]{
                msgSend.MessageHeader.ProcedureType,
                msgSend.MessageHeader.Reference.Version,
                msgSend.MessageHeader.Reference.MessageID,
                msgSend.MessageHeader.Reference.ReplyTo,
                msgSend.MessageHeader.SendApplication.Name,
                msgSend.MessageHeader.SendApplication.Version,
                msgSend.MessageHeader.SendApplication.CompanyName,
                msgSend.MessageHeader.SendApplication.CompanyIdentity,
                //929 DateTime
                msgSend.MessageHeader.SendApplication.CreateMessageIssue,
                msgSend.MessageHeader.From.Name,
                msgSend.MessageHeader.From.Identity,
                msgSend.MessageHeader.To.Name,
                msgSend.MessageHeader.To.Identity.Trim(),
                msgSend.MessageHeader.Subject.Type,
                msgSend.MessageHeader.Subject.Function,
                msgSend.MessageHeader.Subject.Reference,
                msgSend.MessageHeader.Subject.SendApplication,
                msgSend.MessageHeader.Subject.ReceiveApplication,
                content,
                signature!=null? signature.Data:string.Empty,
                signature!=null? signature.FileCert:string.Empty

            });
            return ret;
        }

        /// <summary>
        /// Chia ra nhiều dòng mỗi dòng đạt 80 ký tự
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string MultipleLines(string source)
        {
            int pos = 0;
            string ret = source;
            while (pos <= source.Length)
            {
                ret = ret.Insert(pos, "\r\n");
                pos += 84;
            }
            return ret;
        }
        static Helpers()
        {
        }
        /// <summary>
        /// Chuyển chuổi format xml sang object T
        /// </summary>
        /// <typeparam name="T">object</typeparam>
        /// <param name="sfmtObjectXml">format xml</param>
        /// <returns>T</returns>
        public static T Deserialize<T>(string sfmtObjectXml)
        {
            if (string.IsNullOrEmpty(sfmtObjectXml)) throw new ArgumentNullException("sfmtObjectXml không được rỗng");
            using (StringReader reader = new StringReader(sfmtObjectXml))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }
        /// <summary>
        /// Chuyển format xml sang object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding"></param>
        /// <param name="removeDeclartion">Gở bỏ format <?xml version="1.0" encoding="utf-8"?></param>
        /// <param name="removeNameSpace">Gở bỏ namespace</param>
        /// <returns></returns>
        public static string Serializer(object obj, Encoding encoding, bool removeDeclartion, bool removeNameSpace)
        {

            XmlWriterSettings xmlWS = new XmlWriterSettings();
            if (removeDeclartion)
                xmlWS.OmitXmlDeclaration = true;
            xmlWS.Encoding = encoding;

            // xmlWS.Encoding = new UTF7Encoding(true);

            UtfStringWriter sWriter = new UtfStringWriter(encoding);
            using (XmlWriter xmlWriter = XmlWriter.Create(sWriter, xmlWS))
            {
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                if (removeNameSpace)
                    namespaces.Add("", "");//Remove NameSpace
                else
                    namespaces.Add("dt", "urn:schemas-microsoft-com:datatypes");// Add Namespace
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(xmlWriter, obj, namespaces);
                return sWriter.ToString();
            }
        }
        public static string Serializer(object obj)
        {
            return Serializer(obj, Encoding.UTF8, false, false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="removeDeclartion">Gở bỏ format <?xml version="1.0" encoding="utf-8"?></param>
        /// <param name="removeNameSpace">Gở bỏ namespace</param>
        /// <returns></returns>
        public static string Serializer(object obj, bool removeDeclartion, bool removeNameSpace)
        {
            return Serializer(obj, Encoding.UTF8, removeDeclartion, removeNameSpace);
        }
        /// <summary>
        /// Thực hiện hành động và đóng kết nối
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="action"></param>
        public static void DoActionAndClose<T>(T client, Action<T> action) where T : ICommunicationObject
        {
            try
            {
                action(client);
                client.Close();
            }
            catch
            {
                client.Abort();
                throw;
            }
        }
        public static void DoAction<T>(T client, Action<T> action) where T : ICommunicationObject
        {
            try
            {
                action(client);
            }
            catch
            {
                client.Abort();
                throw;
            }
        }
        public static int GetLoading(int count)
        {

            int item = count / 3;
            if (count % 3 > 0 && item > 3)
                item++;
            return item;

        }
        public static string FormatNumeric(object obj)
        {
            return FormatNumeric(obj, 0, CultureInfo.CreateSpecificCulture("en-US"));
        }
        public static string FormatNumeric(object obj, int decimalFormat)
        {
            return FormatNumeric(obj, decimalFormat, CultureInfo.CreateSpecificCulture("en-US"));
        }
        public static string Format(object obj, int decimalFormat)
        {
            return Helpers.FormatNumeric(obj, decimalFormat, Thread.CurrentThread.CurrentCulture);
        }
        /// <summary>
        /// Format dữ liệu chuẩn tiếng anh.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="decimalFormat"></param>
        /// <returns></returns>
        public static string FormatNumeric(object obj, int decimalFormat, CultureInfo cultureInfo)
        {
            string ret = string.Empty;
            if (obj == null) return ret;

            string sFormat = "{0:0" + Globals.GetDot(decimalFormat, false) + "}";

            if (obj is bool)
            {
                bool b = Convert.ToBoolean(obj, cultureInfo);
                ret = b ? "1" : "0";
            }
            else
            {
                ret = string.Format(cultureInfo, sFormat, new object[] { obj });
            }
            return ret;
        }


        public static string ConvertFromBase64(string strSource)
        {
            return Encoding.Unicode.GetString(Convert.FromBase64String(strSource));
        }
        static public string GetResourceString(string resourceName)
        {
            Stream stream = System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream(resourceName);
            if (stream == null)
                throw new FileNotFoundException("Resource not found.");
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
        public static string ConvertToBase64(string strSource)
        {
            return Convert.ToBase64String(Encoding.Unicode.GetBytes(strSource));
        }
        public static void Demo()
        {
            ActivationInfo.ClientDetailInfo clientinf = new ActivationInfo.ClientDetailInfo()
            {
                DBName = "192.4985745",
                DiaChiHaiQuan = "NONE",
                NgayOnline = DateTime.Now
            };
            string sfmt = Serializer(clientinf);
            ActivationInfo.ClientDetailInfo clientinf1 = Deserialize<ActivationInfo.ClientDetailInfo>(sfmt);

        }

        public static FeedBackContent GetFeedBackContent(string sfmtFeedBack)
        {
            MessageSend<FeedBackContent> msg = Helpers.Deserialize<MessageSend<FeedBackContent>>(sfmtFeedBack);
            FeedBackContent fback = null;
            if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
            {
                string contentError = ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                fback = Helpers.Deserialize<FeedBackContent>(contentError);
            }
            else if (msg.MessageBody.MessageContent.Declaration != null)
                fback = msg.MessageBody.MessageContent.Declaration;
            if (fback == null)
                throw new Exception("Nội dung hải quan trả về không hợp lệ với chuẩn 3\r\nNội dung: " + sfmtFeedBack);
            if (Company.KDT.SHARE.Components.Globals.FontName == "TCVN3")
                fback.AdditionalInformations[0].Content.Text = FontConverter.TCVN2Unicode(fback.AdditionalInformations[0].Content.Text);
            return fback;
        }
        #region Encrypt/Decrypt String
        public static string EncryptString(string plainText, string keyword)
        {
            byte[] text = Encoding.ASCII.GetBytes(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] salt = Encoding.ASCII.GetBytes(keyword);

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(keyword, salt);

            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            CryptoStream encStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            encStream.Write(text, 0, text.Length);

            encStream.FlushFinalBlock();

            byte[] CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            encStream.Close();

            return Convert.ToBase64String(CipherBytes);
        }
        public static string DecryptString(string plainText, string keyWord)
        {
            byte[] text = Convert.FromBase64String(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] salt = Encoding.ASCII.GetBytes(keyWord);

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(keyWord, salt);

            ICryptoTransform decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(text);

            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            text = new byte[text.Length];
            int DecryptedCount = cryptoStream.Read(text, 0, text.Length);

            memoryStream.Close();
            cryptoStream.Close();


            return Encoding.ASCII.GetString(text);
        }
        public static string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }
        #endregion

    }
    class UtfStringWriter : StringWriter
    {
        Encoding _enCoding = Encoding.UTF8;
        public UtfStringWriter(Encoding encoding)
        {
            _enCoding = encoding;
        }
        public override Encoding Encoding
        {
            get { return _enCoding; }
        }
    }


}
