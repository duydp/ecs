﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml;

namespace ECS.AutoUpdate
{
    class CommonUnitity
    {
        public static string SystemBinUrl = AppDomain.CurrentDomain.BaseDirectory;

        public static void RestartApplication()
        {
            Process.Start(Application.ExecutablePath);
            Environment.Exit(0);
        }
        public static string GetSubPath(DownloadFileInfo fileInfo, string startDirectory)
        {
            string strSubPath = startDirectory;
            if (fileInfo.FileFullName.IndexOf("/") != -1)
            {

                string[] exeGroup = fileInfo.FileFullName.Split('/');
                string tempPath = string.Empty;

                for (int i = 0; i < exeGroup.Length - 1; i++)
                    tempPath += exeGroup[i] + "\\";

                strSubPath += tempPath;

                if (!Directory.Exists(strSubPath))
                    Directory.CreateDirectory(strSubPath);
            }

            return strSubPath;
        }
        public static string GetZipPath(DownloadFileInfo dFileInfo)
        {
            string strSubPath = GetSubPath(dFileInfo, SystemBinUrl + ConstFile.TEMPFOLDERNAME + "\\");
            return Path.Combine(strSubPath, dFileInfo.FileName);

        }
        public static string GetExcuteFile(DownloadFileInfo dFileInfo)
        {
            string strSubpath = GetSubPath(dFileInfo, SystemBinUrl);
            return Path.Combine(strSubpath, dFileInfo.FileName.Replace(".zip", string.Empty));
        }
        public static string GetFolderUrl(DownloadFileInfo file)
        {
            string folderPathUrl = string.Empty;
            int folderPathPoint = file.DownloadUrl.IndexOf("/", 15) + 1;
            string filepathstring = file.DownloadUrl.Substring(folderPathPoint);
            int folderPathPoint1 = filepathstring.IndexOf("/");
            string filepathstring1 = filepathstring.Substring(folderPathPoint1 + 1);
            if (filepathstring1.IndexOf("/") != -1)
            {
                string[] ExeGroup = filepathstring1.Split('/');
                for (int i = 0; i < ExeGroup.Length - 1; i++)
                {
                    folderPathUrl += "\\" + ExeGroup[i];
                }
                if (!Directory.Exists(SystemBinUrl + ConstFile.TEMPFOLDERNAME + folderPathUrl))
                {
                    Directory.CreateDirectory(SystemBinUrl + ConstFile.TEMPFOLDERNAME + folderPathUrl);
                }
            }
            return folderPathUrl;
        }

        //Hungtq 20/11/2013.
        public static bool IsAutoUpdateMultiThread()
        {
            string appPath = "";
            string appPathConfig = "";
            string[] arrProc = new string[]{
                    "SOFTECH.ECS.TQDT.SXXK.exe",
                    "SOFTECH.ECS.TQDT.GC.exe",
                    "SOFTECH.ECS.TQDT.KD.exe",
                    "SOFTECH.ECS.TQDT.SIGNREMOTE.exe"
                };

            foreach (string item in arrProc)
            {
                appPath = SystemBinUrl + item;
                appPathConfig = appPath + ".config"; //SystemBinUrl + item + ".Config";

                if (System.IO.File.Exists(appPath) || System.IO.File.Exists(appPathConfig))
                {
                    break;
                }
                else
                {
                    appPath = "";
                    appPathConfig = "";
                }
            }

            string val = "";
            //Updaetd by Hungtq, 14/09/2012.
            if (System.IO.File.Exists(appPathConfig))
            {
                val = ReadNodeXmlAppSettings(appPathConfig, "IsAutoUpdateMultiThread", "false");
                //Logger.LocalLogger.Instance().WriteMessage("IsAutoUpdateMultiThread = " + val, new Exception(""));
            }
            else
                Logger.LocalLogger.Instance().WriteMessage("ECS.AutoUpdate.CommonUnitity: Khong tim thay file " + appPathConfig, new Exception(""));

            return string.IsNullOrEmpty(val) ? false : Convert.ToBoolean(val);
        }

        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXmlAppSettings(string pathFileExecute, string key, object value)
        {
            try
            {
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(pathFileExecute);
                string groupSettingName = "appSettings";

                doc.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
                doc.Save(pathFileExecute);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }

        public static string ReadNodeXmlAppSettings(string pathFileExecute, string key, string defaultValue)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(pathFileExecute);
            string groupSettingName = "appSettings";

            return ReadNodeXml(pathFileExecute, doc, groupSettingName, key, defaultValue);
        }

        public static string ReadNodeXml(string pathFileExecute, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, string default_Value)
        {
            string result = "";

            try
            {
                XmlNode Node = xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']");
                if (Node != null)
                {

                    result = Node.SelectSingleNode("@value").Value;
                    if (string.IsNullOrEmpty(result))
                    {
                        result = default_Value;
                    }
                }
                else
                {
                    result = default_Value;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
            if (string.IsNullOrEmpty(result))
                result = default_Value;
            return result;
        }

    }
}
