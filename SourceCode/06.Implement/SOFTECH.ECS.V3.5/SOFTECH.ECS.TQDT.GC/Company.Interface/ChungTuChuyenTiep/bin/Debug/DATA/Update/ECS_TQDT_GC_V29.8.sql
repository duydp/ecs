GO
IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_Export]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Export]
GO	  
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_Export]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Export]
@HopDong_ID NVARCHAR(50)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT  
Ma AS N''MA_NPL'' , 
Ten AS N''TEN_NPL'',
MaHS AS N''MA_HS'' , 
CASE WHEN DVT_ID  > 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh  WHERE ID = DVT_ID ) ELSE DVT_ID END AS N''ĐVT'' 
FROM dbo.t_GC_NguyenPhuLieu WHERE HopDong_ID =' + @HopDong_ID

EXEC sp_executesql @SQL  
GO

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_Export]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_Export]
GO	   
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_SanPham_Export]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_GC_SanPham_Export]
@HopDong_ID NVARCHAR(50)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT  
Ma AS ''MA_SAN_PHAM'' ,
Ten AS ''TEN_SAN_PHAM'',
MaHS AS ''MA_HS'' , 
CASE WHEN DVT_ID  > 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh  WHERE ID = DVT_ID ) ELSE DVT_ID END AS ''ĐVT'' 
FROM dbo.t_GC_SanPham WHERE HopDong_ID =' + @HopDong_ID
  
EXEC sp_executesql @SQL  

GO

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_Export]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_Export]
GO	   
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_ThietBi_Export]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_GC_ThietBi_Export]  
@HopDong_ID NVARCHAR(50)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT  
Ma AS N''MA_THIET_BI'' ,
Ten AS N''TEN_THIET_BI'',
MaHS AS N''MA_HS'' ,
CASE WHEN DVT_ID  > 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh  WHERE ID = DVT_ID ) ELSE DVT_ID END AS N''ĐVT'' 
FROM dbo.t_GC_ThietBi WHERE HopDong_ID =' + @HopDong_ID  
  
EXEC sp_executesql @SQL  


GO
IF OBJECT_ID(N'[dbo].[p_GC_HangMau_Export]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_Export]
GO	   
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_HangMau_Export]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_GC_HangMau_Export]  
@HopDong_ID NVARCHAR(50) 
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT  
Ma AS N''MA_HANG_MAU'' ,
Ten AS N''TEN_HANG_MAU'',
MaHS AS N''MA_HS'' ,
CASE WHEN DVT_ID  > 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh  WHERE ID = DVT_ID ) ELSE DVT_ID END AS N''ĐVT'' 
FROM dbo.t_GC_HangMau WHERE HopDong_ID =' + @HopDong_ID  
  
EXEC sp_executesql @SQL     
GO

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_NKExport]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_NKExport]
GO	   
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_NKExport]  
-- Database: ECS_GCTQ  
-- Author: Ngo Thanh Tung  
-- Time created: Wednesday, January 06, 2010  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_NKExport]  
@HopDong_ID NVARCHAR(50)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(3250)  
  
SET @SQL = 'SELECT 
SoToKhai AS N''SO_TO_KHAI_V4'',
LoaiVanDon AS N''SO_TO_KHAI_VNACCS'',
MaLoaiHinh AS N''MA_LOAI_HINH'',
MaHaiQuan AS N''MA_HAI_QUAN'',
NamDK AS N''NAM_DANG_KY'',
NgayDangKy AS N''NGAY_DANG_KY'',
HMD.MaPhu AS N''MA_HANG_HOA'',
HMD.TenHang AS N''TEN_HANG_HOA'',
HMD.MaHS AS N''MA_HS'',
CASE WHEN HMD.DVT_ID  > 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh  WHERE ID = HMD.DVT_ID ) ELSE DVT_ID END AS N''ĐVT'',
HMD.SoLuong AS N''SO_LUONG'',
HMD.DonGiaKB AS N''DON_GIA'',
HMD.TriGiaKB AS N''TRI_GIA'',
TKMD.TyGiaUSD AS N''TY_GIA'' 
FROM dbo.t_KDT_ToKhaiMauDich TKMD INNER JOIN dbo.t_KDT_HangMauDich HMD ON HMD.TKMD_ID = TKMD.ID WHERE TKMD.MaLoaiHinh Like ''N%'' AND IDHopDong = ' + @HopDong_ID 
  
EXEC sp_executesql @SQL  

GO
IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_XKExport]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_XKExport]
GO	 
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_XKExport]  
-- Database: ECS_GCTQ  
-- Author: Ngo Thanh Tung  
-- Time created: Wednesday, January 06, 2010  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_XKExport]  
@HopDong_ID NVARCHAR(50) 
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(3250)  
  
SET @SQL = 'SELECT 
SoToKhai AS N''SO_TO_KHAI_V4'',
LoaiVanDon AS N''SO_TO_KHAI_VNACCS'',
MaLoaiHinh AS N''MA_LOAI_HINH'',
MaHaiQuan AS N''MA_HAI_QUAN'',
NamDK AS N''NAM_DANG_KY'',
NgayDangKy AS N''NGAY_DANG_KY'',
HMD.MaPhu AS N''MA_HANG_HOA'',
HMD.TenHang AS N''TEN_HANG_HOA'',
HMD.MaHS AS N''MA_HS'',
CASE WHEN HMD.DVT_ID  > 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh  WHERE ID = HMD.DVT_ID ) ELSE DVT_ID END AS N''ĐVT'',
HMD.SoLuong AS N''SO_LUONG'',
HMD.DonGiaKB AS N''DON_GIA'',
HMD.TriGiaKB AS N''TRI_GIA'',
TKMD.TyGiaUSD AS N''TY_GIA'' 
FROM dbo.t_KDT_ToKhaiMauDich TKMD INNER JOIN dbo.t_KDT_HangMauDich HMD ON HMD.TKMD_ID = TKMD.ID WHERE TKMD.MaLoaiHinh Like ''X%'' AND IDHopDong = ' + @HopDong_ID 
  
EXEC sp_executesql @SQL    
GO

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ToKhaiChuyenTiep_NKExport]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_NKExport]
GO	    
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_NKExport]  
-- Database: ECS_TQDT_GC_V3  
-- Author: Ngo Thanh Tung  
-- Time created: Tuesday, March 20, 2012  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_NKExport]  
@HopDong_ID NVARCHAR(50)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT 
SoToKhai AS N''SO_TO_KHAI_V4'',
TKCT.Huongdan_PL AS N''SO_TO_KHAI_VNACCS'',
MaLoaiHinh AS N''MA_LOAI_HINH'',
TKCT.MaHaiQuanTiepNhan AS N''MA_HAI_QUAN'',
NamDK AS N''NAM_DANG_KY'',
NgayDangKy AS N''NGAY_DANG_KY'',
HCT.MaHang AS N''MA_HANG_HOA'',
HCT.TenHang AS N''TEN_HANG_HOA'',
HCT.MaHS AS N''MA_HS'',
CASE WHEN HCT.ID_DVT  > 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh  WHERE ID = HCT.ID_DVT ) ELSE ID_DVT END AS N''ĐVT'',
HCT.SoLuong AS N''SO_LUONG'',
HCT.DonGia AS N''DON_GIA'',
HCT.TriGia AS N''TRI_GIA'',
TKCT.TyGiaUSD AS N''TY_GIA'' 
FROM dbo.t_KDT_GC_ToKhaiChuyenTiep TKCT INNER JOIN dbo.t_KDT_GC_HangChuyenTiep HCT ON HCT.Master_ID = TKCT.ID WHERE TKCT.MaLoaiHinh Like ''N%'' AND IDHopDong = ' + @HopDong_ID  
  
EXEC sp_executesql @SQL  
GO
IF OBJECT_ID(N'[dbo].[p_KDT_GC_ToKhaiChuyenTiep_XKExport]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_XKExport]
GO	 
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_XKExport]  
-- Database: ECS_TQDT_GC_V3  
-- Author: Ngo Thanh Tung  
-- Time created: Tuesday, March 20, 2012  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_XKExport]  
@HopDong_ID NVARCHAR(50)  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT 
SoToKhai AS N''SO_TO_KHAI_V4'',
TKCT.Huongdan_PL AS N''SO_TO_KHAI_VNACCS'',
MaLoaiHinh AS N''MA_LOAI_HINH'',
TKCT.MaHaiQuanTiepNhan AS N''MA_HAI_QUAN'',
NamDK AS N''NAM_DANG_KY'',
NgayDangKy AS N''NGAY_DANG_KY'',
HCT.MaHang AS N''MA_HANG_HOA'',
HCT.TenHang AS N''TEN_HANG_HOA'',
HCT.MaHS AS N''MA_HS'',
CASE WHEN HCT.ID_DVT  > 0 THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh  WHERE ID = HCT.ID_DVT ) ELSE ID_DVT END AS N''ĐVT'',
HCT.SoLuong AS N''SO_LUONG'',
HCT.DonGia AS N''DON_GIA'',
HCT.TriGia AS N''TRI_GIA'',
TKCT.TyGiaUSD AS N''TY_GIA'' 
FROM dbo.t_KDT_GC_ToKhaiChuyenTiep TKCT INNER JOIN dbo.t_KDT_GC_HangChuyenTiep HCT ON HCT.Master_ID = TKCT.ID WHERE TKCT.MaLoaiHinh Like ''X%'' AND IDHopDong = ' + @HopDong_ID 
  
EXEC sp_executesql @SQL  

GO

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Export]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Export]
GO	   
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Export]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Export]  
@HopDong_ID NVARCHAR(50)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT MaSanPham AS N''MA_SAN_PHAM'',
TenSanPham AS N''TEN_SAN_PHAM'',
MaNguyenPhuLieu AS N''MA_NPL'',
TenNPL AS N''TEN_NPL'',
DinhMucSuDung AS N''DINH_MUC_SU_DUNG'',
TyLeHaoHut AS N''TY_LE_HAO_HUT'' 
FROM dbo.t_GC_DinhMuc WHERE HopDong_ID =' + @HopDong_ID  

EXEC sp_executesql @SQL  

GO

IF OBJECT_ID(N'[dbo].[p_KDT_GC_PhuKienDangKy_Export]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_PhuKienDangKy_Export]
GO	   
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_PhuKienDangKy_Export]  
-- Database: ECS_GCTQ  
-- Author: Ngo Thanh Tung  
-- Time created: Friday, March 05, 2010  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_GC_PhuKienDangKy_Export]  
@HopDong_ID NVARCHAR(50)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(3250)  
  
SET @SQL = 'SELECT 
SoTiepNhan AS N''SO_TIEP_NHAN'',
NgayTiepNhan AS N''NGAY_TIEP_NHAN'',
MaHaiQuan AS N''MA_HQ'',
NgayPhuKien AS N''NGAY_PHU_KIEN'',
SoPhuKien AS N''SO_PHU_KIEN'',
GhiChu AS N''GHI_CHU'',
NoiDung AS N''LOAI_PHU_KIEN''
FROM dbo.t_KDT_GC_PhuKienDangKy  INNER JOIN dbo.t_KDT_GC_LoaiPhuKien ON t_KDT_GC_LoaiPhuKien.Master_ID = t_KDT_GC_PhuKienDangKy.ID WHERE  HopDong_ID = ' + @HopDong_ID  
  
  
EXEC sp_executesql @SQL  


--SELECT 
--SoTiepNhan AS N'Số tiếp nhận',
--NgayTiepNhan AS N'Ngày tiếp nhận',
--MaHaiQuan AS N'Mã HQ',
--NgayPhuKien AS N'Ngày phụ kiện',
--SoPhuKien AS N'Số phụ kiện',
--GhiChu AS N'Ghi chú',
--NoiDung AS N'Loại phụ kiện'
--FROM dbo.t_KDT_GC_PhuKienDangKy  INNER JOIN dbo.t_KDT_GC_LoaiPhuKien ON t_KDT_GC_LoaiPhuKien.Master_ID = t_KDT_GC_PhuKienDangKy.ID WHERE HopDong_ID


GO
IF OBJECT_ID(N'[dbo].[p_KDT_GC_HopDong_Export]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HopDong_Export]
GO	 
  
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_Export]  
-- Database: ECS_TQDT_GC_V4  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_Export]  
@HopDong_ID NVARCHAR(50)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT   
 [SoHopDong] AS ''SO_HOP_DONG'',   
 [SoTiepNhan]  AS ''SO_TIEP_NHAN'', 
 [NgayTiepNhan] AS ''NGAY_TIEP_NHAN'',    
 [MaHaiQuan] AS ''MA_HAI_QUAN'',  
 [MaDoanhNghiep] AS ''MA_DON_VI'',
 [TenDoanhNghiep] AS ''TEN_DON_VI'',
 [DiaChiDoanhNghiep] AS ''DIA_CHI_DON_VI'',       
 [NgayDangKy]  AS ''NGAY_DANG_KY'',  
 [NgayHetHan]  AS ''NGAY_HET_HAN'',  
 [NgayGiaHan]  AS ''NGAY_GIA_HAN'',  
 [NuocThue_ID]  AS ''NUOC_THUE'',  
 [NguyenTe_ID]  AS ''NGUYEN_TE'',   
 [DonViDoiTac]  AS ''MA_DOI_TAC'',
 [TenDonViDoiTac] AS ''TEN_DOI_TAC'' ,   
 [DiaChiDoiTac]  AS ''DIA_CHI_DOI_TAC'',
 [TongTriGiaSP] AS ''TONG_TRI_GIA'',  
 [TongTriGiaTienCong] AS ''TONG_TIEN_CONG'' 
FROM [dbo].[t_KDT_GC_HopDong]   
WHERE ID = ' + @HopDong_ID     
  
EXEC sp_executesql @SQL  


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '29.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('29.8',GETDATE(), N'CẬP NHẬT PROCEDURE KẾT XUẤT EXCEL HỢP ĐỒNG')
END