using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class BCNPLXuatNhapTon_KCX_194_TongNPL : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        private int STTBanLuu = 0;
        public int SoHoSo;
        private DataTable DtGhiChu = new DataTable();
        private int SoToKhaiNhap = 0;
        public string SoHoSoString;
        public int namThanhLy;
        public long BangKeNhapID;
        private string MaNPLTemp = "";
        private string MaNPLBanLuu = "";

        private string MaSPTemp = ""; // Dùng để phân biệt các tờ khai xuất chuyển tiếp cho tờ khai nhập tiếp theo
        private string MaSPBanLuu = "";


        private int cnt = 1; //Gia tri phai thiet lap mac dinh ban dau = 1.
        private DataTable dtMaNPLTemp;
        private decimal _TongLuongNhap = 0;
        private decimal _TongLuongTonDau = 0;
        private decimal _TongLuongNPLSuDung = 0;
        private decimal _TongLuongTonCuoi = 0;
        private decimal _TongLuongNPLTaiXuat = 0;

        public BCNPLXuatNhapTon_KCX_194_TongNPL()
        {
            InitializeComponent();
        }

        public void BindReport(string where)
        {
            this.Font = new Font(FontFamily.GenericSansSerif, GlobalSettings.FontBCXNT);

            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                order = " ngayhoanthanhnhap,ngaydangkynhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }

            dtMaNPLTemp = BLL.SXXK.ThanhKhoan.BCXuatNhapTon.GetSoLanNPLThamGiaThanhKhoan(GlobalSettings.MA_DON_VI, this.LanThanhLy);

            DataTable dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, order).Tables[0];
            //Tạo table báo cáo nhập xuất tồn
            //DataTable dtBCXuatNhapTon = dt.Clone();
            DataTable dtBCXuatNhapTon = Company.BLL.KDT.SXXK.BKToKhaiNhap.getTKNhapKoThamGiaTK(this.LanThanhLy, (int)BangKeNhapID);
            //foreach (DataRow drNhapTon in dtBCXuatNhapTon.Rows)
            //{
            //    DataRow dr = dt.NewRow();

            //    dr["LanThanhLy"] = this.LanThanhLy;
            //    dr["MaDoanhNghiep"] = GlobalSettings.MA_DON_VI;
            //    dr["STT"] = 0;
            //    dr["MaNPL"] = drNhapTon["MaNPL"].ToString().Trim();
            //    dr["TenNPL"] = drNhapTon["TenNPL"].ToString();
            //    dr["SoToKhaiNhap"] = drNhapTon["SoToKhai"].ToString();
            //    dr["NgayDangKyNhap"] = Convert.ToDateTime(drNhapTon["NgayDangKy"]);
            //    dr["NgayHoanThanhNhap"] = Convert.ToDateTime(drNhapTon["NgayHoanThanh"]);
            //    dr["MaLoaiHinhNhap"] = Convert.ToString(drNhapTon["MaLoaiHinh"]);
            //    dr["LuongNhap"] = Convert.ToDecimal(drNhapTon["Luong"]);
            //    dr["LuongTonDau"] = Convert.ToDecimal(drNhapTon["TonDau"]);
            //    dr["TenDVT_NPL"] = Convert.ToString(drNhapTon["TenDVT_NPL"]);
            //    dr["DonGiaTT"] = Convert.ToDouble(drNhapTon["DonGiaTT"]);
            //    dr["TyGiaTT"] = Convert.ToDecimal(drNhapTon["TyGiaTT"]);
            //    dr["ThueSuat"] = Convert.ToDecimal(drNhapTon["ThueSuat"]);
            //    dr["ThueXNK"] = Convert.ToDouble(drNhapTon["ThueXNK"]);
            //    dr["ThueXNKTon"] = Convert.ToDouble(drNhapTon["TonDauThueXNK"]);

            //    dr["MaSP"] = "";
            //    dr["TenSP"] = "";
            //    dr["SoToKhaiXuat"] = 0;
            //    dr["NgayDangKyXuat"] = "01/01/1900";
            //    //
            //    dr["NgayHoanThanhXuat"] = "01/01/1900";
            //    dr["NgayThucXuat"] = "01/01/1900";

            //    dr["MaLoaiHinhXuat"] = "";
            //    dr["LuongSPXuat"] = 0;
            //    dr["LuongNPLSuDung"] = 0;
            //    dr["TenDVT_SP"] = "";
            //    dr["DinhMuc"] = 0;
            //    dr["LuongTonCuoi"] = 0;
            //    dt.Rows.Add(dr);

            //}
            ///
            int i = 1;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                {
                    dr["STT"] = i;
                }
                else
                {
                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    i++;
                    dr["STT"] = i;
                }
            }
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap")});
                //new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                //new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                //new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")
            }
            else
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});

            }
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;

            if (SoHoSoString != "")
                lblSHSTK.Text = SoHoSoString;
            else if (SoHoSo > 0)
                lblSHSTK.Text = SoHoSo + "";
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            //lblMaNPL.DataBindings.Add("Text", this.DataSource,  dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblMaSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            if (GlobalSettings.SoThapPhan.MauBC04 == 0)
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            else
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            lblLuongSPXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSPXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
            lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
            lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
            lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep", "{0:n3}");
            //lblChuyenMucDichKhac.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChuyenMucDichKhac", "{0:n3}");

            //lblTongLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            //lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            //lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //TODO: Updated by Hungtq, 15/09/2012
            //lblTongLuongNhap.Summary.Func = SummaryFunc.Custom;
            //lblTongLuongNhap.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
            //lblTongTonDau.Summary.Func = SummaryFunc.Custom;
            //lblTongTonDau.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
            //lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Custom;
            //lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
            //lblTongTonCuoi.Summary.Func = SummaryFunc.Custom;
            //lblTongTonCuoi.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");
            lblNgayThangHQ.Text = lblNgayThangDN.Text = GlobalSettings.TieudeNgay;
        }

        public void BindReportByMaNPL(string where, string MaNPL)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {

                order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                order = " ngayhoanthanhnhap,ngaydangkynhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";

            }

            dtMaNPLTemp = BLL.SXXK.ThanhKhoan.BCXuatNhapTon.GetSoLanNPLThamGiaThanhKhoan(GlobalSettings.MA_DON_VI, this.LanThanhLy);

            DataTable dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaNPL ='" + MaNPL + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, order).Tables[0];
            int i = 1;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                {
                    dr["STT"] = i;
                }
                else
                {
                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    i++;
                    dr["STT"] = i;
                }
            }
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap")});
                //new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                //new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                //new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")
            }
            else
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});

            }

            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (SoHoSoString != "")
                lblSHSTK.Text = SoHoSoString;
            else if (SoHoSo > 0)
                lblSHSTK.Text = SoHoSo + "";
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            //lblMaNPL.DataBindings.Add("Text", this.DataSource,  dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblMaSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            if (GlobalSettings.SoThapPhan.MauBC04 == 0)
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            else
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            lblLuongSPXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSPXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
            lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
            lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
            lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep", "{0:n3}");
            //lblChuyenMucDichKhac.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChuyenMucDichKhac", "{0:n3}");
            //lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");


            //TODO: Updated by Hungtq, 15/09/2012
            //lblTongLuongNhap.Summary.Func = SummaryFunc.Custom;
            //lblTongLuongNhap.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
            //lblTongTonDau.Summary.Func = SummaryFunc.Custom;
            //lblTongTonDau.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
            //lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Custom;
            //lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
            //lblTongTonCuoi.Summary.Func = SummaryFunc.Custom;
            //lblTongTonCuoi.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            lblNgayThangHQ.Text = lblNgayThangDN.Text = GlobalSettings.TieudeNgay;
        }

        private decimal temp = 0;
        private void lblMaSP_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (GlobalSettings.MA_DON_VI == "0400101242")//Rieng cong ty Luoi
            //{
            //    string[] temp = GetCurrentColumnValue("TenSP").ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            //    if (temp.Length > 0)
            //        lblMaSP.Text = temp[0] + " " + GetCurrentColumnValue("MaSP").ToString();
            //}
            //else
            //{
            lblMaSP.Text = GetCurrentColumnValue("TenSP").ToString();
            if (GetCurrentColumnValue("MaSP").ToString().Trim().Length > 0)
                lblMaSP.Text = lblMaSP.Text + " / " + GetCurrentColumnValue("MaSP").ToString();
            //}
        }

        private void lblSoToKhaiTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0)
                lblSoToKhaiTaiXuat1.Text = "";
        }

        private void lblLuongNPLTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0)
                lblTongLuongNPLTaiXuat.Text = "";
        }

        private void lblToKhaiNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblToKhaiNhap.Text = "Tổng lượng tờ khai " + Convert.ToString(GetCurrentColumnValue("SoToKhaiNhap")) + "/" + GetCurrentColumnValue("MaLoaiHinhNhap").ToString() + "/" + Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyNhap")).Year;
            lblToKhaiNhap.Text = "Tổng lượng nguyên phụ liệu " + GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("TenNPL") == null || GetCurrentColumnValue("MaNPL") == null) return;
            lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        }

        private void BCNPLXuatNhapTon_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                this.STT = 0;
                this.DtGhiChu = new DataTable();
                DataColumn[] cols = new DataColumn[3];
                cols[0] = new DataColumn("MaNPL", typeof(string));
                cols[1] = new DataColumn("From", typeof(int));
                cols[2] = new DataColumn("To", typeof(int));
                this.DtGhiChu.Columns.AddRange(cols);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (MaNPLBanLuu != GetCurrentColumnValue("MaNPL").ToString())
            {
                this.STT++;
            }

            temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            if (lblSoToKhaiNhap.Text != "")
                SoToKhaiNhap = Convert.ToInt32(lblSoToKhaiNhap.Text);
        }

        private void GroupHeader1_AfterPrint(object sender, EventArgs e)
        {
            //TODO: Updated by HungTQ, 17/09/2012.
            //Visible Group Footer
            MaNPLTemp = GetCurrentColumnValue("MaNPL").ToString(); //Da su dung dong nay tai vi tri Detail_BeforePrint
            if (MaNPLBanLuu != MaNPLTemp)
            {
                MaNPLBanLuu = MaNPLTemp;
                cnt = 1;

                _TongLuongNhap = (System.Decimal)GetCurrentColumnValue("LuongNhap");
                _TongLuongTonDau = (System.Decimal)GetCurrentColumnValue("LuongTonDau");

                _TongLuongNPLTaiXuat = 0;
                _TongLuongNPLSuDung = 0;
                _TongLuongTonCuoi = 0;
            }
            else
            {
                cnt += 1;

                _TongLuongNhap += (System.Decimal)GetCurrentColumnValue("LuongNhap");
                _TongLuongTonDau += (System.Decimal)GetCurrentColumnValue("LuongTonDau");
            }

            //Visible Group Footer            
            GroupFooter1.Visible = (cnt == LaySoLanMaNPLThamGiaThanhKhoan(MaNPLTemp));
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (STTBanLuu != STT)
            {
                lblSTT.Text = this.STT + "";

                STTBanLuu = STT;
            }
            else
                lblSTT.Text = "";
        }

        private void lblSoToKhaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) > 0) ((XRTableCell)sender).Text = "";
            if (((XRTableCell)sender).Name == "lblLuongSPXuat")
            {
                if (Convert.ToDecimal(GetCurrentColumnValue("LuongSPXuat")) == 0) ((XRTableCell)sender).Text = "";
            }
        }

        private void lblSoToKhaiTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToInt32(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0) lblSoToKhaiTaiXuat.Text = "";
        }

        private void lblNgayTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDateTime(GetCurrentColumnValue("NgayTaiXuat")).Year == 1900) lblNgayTaiXuat.Text = "";
        }

        private void lblLuongNPLTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0) lblLuongNPLTaiXuat.Text = "";
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("MaSP") == null) e.Cancel = true;
            if (GetCurrentColumnValue("MaSP").ToString() == "") e.Cancel = true;

            MaSPTemp = GetCurrentColumnValue("MaSP").ToString() + "/" + GetCurrentColumnValue("SoToKhaiXuat").ToString();
            //TODO: Updated by HungTQ, 17/09/2012.
            //Visible Group Footer            
            if (MaNPLBanLuu != MaNPLTemp)
            {
                MaNPLBanLuu = MaNPLTemp;

                _TongLuongNPLSuDung = (System.Decimal)GetCurrentColumnValue("LuongNPLSuDung");

                _TongLuongNPLTaiXuat = (System.Decimal)GetCurrentColumnValue("LuongNPLTaiXuat");
            }
            else
            {
                if (MaSPBanLuu != MaSPTemp)
                {
                    MaSPBanLuu = MaSPTemp;
                    _TongLuongNPLSuDung += (System.Decimal)GetCurrentColumnValue("LuongNPLSuDung");
                }
                else
                {
                    //Updated by HungTQ, 23/01/2013.
                    //Kiem tra truong hop dong tinh tong NPL cuoi cung cua bao cao do luon luon = 0.
                    if (cnt == 1)
                    {
                        _TongLuongNPLSuDung += (System.Decimal)GetCurrentColumnValue("LuongNPLSuDung");
                    }
                }
                _TongLuongNPLTaiXuat += (System.Decimal)GetCurrentColumnValue("LuongNPLTaiXuat");

            }
        }

        private void lblThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if ((temp - Convert.ToDecimal(GetCurrentColumnValue("LuongNPLSuDung"))) != Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi")))
            //{
            //    int from = GetFrom(GetCurrentColumnValue("MaNPL").ToString(), Convert.ToInt32(GetCurrentColumnValue("SoToKhaiNhap")));
            //    if (from > 0)
            //        lblThanhKhoanTiep.Text = "Chuyển từ TK " + from;
            //    else
            //        lblThanhKhoanTiep.Text = "";
            //}
            //else
            //    lblThanhKhoanTiep.Text = "";
            //temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
        }

        private int GetFrom(string maNPL, int to)
        {
            foreach (DataRow dr in this.DtGhiChu.Rows)
            {
                if (dr["MaNPL"].ToString() == maNPL && to == Convert.ToInt32(dr["To"]))
                    return Convert.ToInt32(dr["From"]);
            }
            return 0;
        }

        private void lblTongThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (lblTongThanhKhoanTiep.Text.Contains("Chuyển TK"))
            //{
            //    DataRow dr = this.DtGhiChu.NewRow();
            //    dr["MaNPL"] = GetCurrentColumnValue("MaNPL").ToString();
            //    dr["From"] = Convert.ToInt32(GetCurrentColumnValue("SoToKhaiNhap"));
            //    dr["To"] = Convert.ToInt32(lblTongThanhKhoanTiep.Text.Replace("Chuyển TK ", ""));
            //    this.DtGhiChu.Rows.Add(dr);
            //}
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //TODO: Updated by HungTQ, 17/09/2012.
            lblTongLuongNhap.Text = _TongLuongNhap.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            lblTongTonDau.Text = _TongLuongTonDau.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            lblTongLuongNPLSuDung.Text = _TongLuongNPLSuDung.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            lblTongTonCuoi.Text = (_TongLuongTonDau - _TongLuongNPLSuDung - _TongLuongNPLTaiXuat).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {

        }

        private int LaySoLanMaNPLThamGiaThanhKhoan(string maNPL)
        {
            return dtMaNPLTemp.Select("MaNPL = '" + maNPL + "'").Length;
        }

    }
}
