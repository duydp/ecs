-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]
	@Master_ID bigint,
	@LanDieuChinhGP_GCN int,
	@ChungNhanDieuChinhSo varchar(20),
	@NgayChungNhanDieuChinh datetime,
	@DieuChinhBoi nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TEADieuChinh]
(
	[Master_ID],
	[LanDieuChinhGP_GCN],
	[ChungNhanDieuChinhSo],
	[NgayChungNhanDieuChinh],
	[DieuChinhBoi],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@Master_ID,
	@LanDieuChinhGP_GCN,
	@ChungNhanDieuChinhSo,
	@NgayChungNhanDieuChinh,
	@DieuChinhBoi,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Update]
	@ID bigint,
	@Master_ID bigint,
	@LanDieuChinhGP_GCN int,
	@ChungNhanDieuChinhSo varchar(20),
	@NgayChungNhanDieuChinh datetime,
	@DieuChinhBoi nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TEADieuChinh]
SET
	[Master_ID] = @Master_ID,
	[LanDieuChinhGP_GCN] = @LanDieuChinhGP_GCN,
	[ChungNhanDieuChinhSo] = @ChungNhanDieuChinhSo,
	[NgayChungNhanDieuChinh] = @NgayChungNhanDieuChinh,
	[DieuChinhBoi] = @DieuChinhBoi,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@LanDieuChinhGP_GCN int,
	@ChungNhanDieuChinhSo varchar(20),
	@NgayChungNhanDieuChinh datetime,
	@DieuChinhBoi nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TEADieuChinh] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TEADieuChinh] 
		SET
			[Master_ID] = @Master_ID,
			[LanDieuChinhGP_GCN] = @LanDieuChinhGP_GCN,
			[ChungNhanDieuChinhSo] = @ChungNhanDieuChinhSo,
			[NgayChungNhanDieuChinh] = @NgayChungNhanDieuChinh,
			[DieuChinhBoi] = @DieuChinhBoi,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TEADieuChinh]
		(
			[Master_ID],
			[LanDieuChinhGP_GCN],
			[ChungNhanDieuChinhSo],
			[NgayChungNhanDieuChinh],
			[DieuChinhBoi],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@Master_ID,
			@LanDieuChinhGP_GCN,
			@ChungNhanDieuChinhSo,
			@NgayChungNhanDieuChinh,
			@DieuChinhBoi,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TEADieuChinh]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TEADieuChinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LanDieuChinhGP_GCN],
	[ChungNhanDieuChinhSo],
	[NgayChungNhanDieuChinh],
	[DieuChinhBoi],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEADieuChinh]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[LanDieuChinhGP_GCN],
	[ChungNhanDieuChinhSo],
	[NgayChungNhanDieuChinh],
	[DieuChinhBoi],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TEADieuChinh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LanDieuChinhGP_GCN],
	[ChungNhanDieuChinhSo],
	[NgayChungNhanDieuChinh],
	[DieuChinhBoi],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEADieuChinh]	

GO

