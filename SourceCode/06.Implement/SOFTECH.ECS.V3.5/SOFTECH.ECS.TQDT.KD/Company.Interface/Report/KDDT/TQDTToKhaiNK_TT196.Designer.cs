namespace Company.Interface.Report
{
    partial class TQDTToKhaiNK_TT196
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TQDTToKhaiNK_TT196));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.GroupHeader = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoThamChieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblChiCucHQDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaySoThamChieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenChiCucHQCuaKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoPhuLucToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNguoiXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiayPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTNguoiXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHoaDonThuongMai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lableNguoiN = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHoaDonThuongMai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHetHanGiayPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHetHanHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNguoiNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTNguoiNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTNguoiNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVanTaiDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCangXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaCangdoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenNguoiUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayVanTaiDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCangXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCangDoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTNguoiUT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongTienVanTai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNuocXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSoHieuPTVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDenPTVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNuoc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenDaiLy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKienGiaoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongThucThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTDaiLy = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDongTienThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTyGiaTinhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTTTDBHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTTDBHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueTTDBHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTBVMTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatBVMTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueBVMTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBangChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblLePhi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTGNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader3 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter3 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongCont = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongKien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChungTuDiKem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayIn = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblKetQuaPhanLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblGhiChepKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 0F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader
            // 
            this.GroupHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrLabel4,
            this.xrLabel9,
            this.xrLabel8,
            this.xrTable3,
            this.lblCucHaiQuan,
            this.winControlContainer1,
            this.xrLabel3});
            this.GroupHeader.HeightF = 430.496F;
            this.GroupHeader.Name = "GroupHeader";
            this.GroupHeader.StylePriority.UseFont = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(31.95833F, 37.5F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(108F, 23F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Cục Hải Quan:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(681.9583F, 45.49999F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(83F, 17F);
            this.xrLabel4.Text = "HQ/2012-NK";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(31.95833F, 12.5F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(208F, 23F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "HẢI QUAN VIỆT NAM";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(240.9583F, 12.5F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(358F, 25F);
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "TỜ KHAI HẢI QUAN ĐIỆN TỬ";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(33F, 62.5F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow18,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow20,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow1,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow37});
            this.xrTable3.SizeF = new System.Drawing.SizeF(745F, 367.996F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell90,
            this.lblSoThamChieu,
            this.xrTableCell71,
            this.lblSoToKhai,
            this.xrTableCell92});
            this.xrTableRow25.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.StylePriority.UseFont = false;
            this.xrTableRow25.Weight = 1.0725002185319696;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell89.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.StylePriority.UseFont = false;
            this.xrTableCell89.Text = " Chi cục Hải Quan đăng ký tờ khai:";
            this.xrTableCell89.Weight = 0.98812664907651715;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell90.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.StylePriority.UseFont = false;
            this.xrTableCell90.StylePriority.UseTextAlignment = false;
            this.xrTableCell90.Text = " Số tham chiếu:";
            this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell90.Weight = 0.387342289467653;
            // 
            // lblSoThamChieu
            // 
            this.lblSoThamChieu.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoThamChieu.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoThamChieu.Name = "lblSoThamChieu";
            this.lblSoThamChieu.StylePriority.UseBorders = false;
            this.lblSoThamChieu.StylePriority.UseFont = false;
            this.lblSoThamChieu.StylePriority.UseTextAlignment = false;
            this.lblSoThamChieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoThamChieu.Weight = 0.33366443059457607;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.Text = " Số tờ khai:";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell71.Weight = 0.25260803255705838;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoToKhai.StylePriority.UseBorders = false;
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.StylePriority.UsePadding = false;
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = "999999";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSoToKhai.Weight = 0.41978143871771073;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell92.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell92.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorderColor = false;
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.StylePriority.UseFont = false;
            this.xrTableCell92.StylePriority.UseForeColor = false;
            this.xrTableCell92.Text = " Công chức đăng ký tờ khai";
            this.xrTableCell92.Weight = 0.61847715958648475;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblChiCucHQDangKy,
            this.xrTableCell94,
            this.xrTableCell72,
            this.xrTableCell96});
            this.xrTableRow26.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.StylePriority.UseFont = false;
            this.xrTableRow26.Weight = 0.66920162450057852;
            // 
            // lblChiCucHQDangKy
            // 
            this.lblChiCucHQDangKy.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblChiCucHQDangKy.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChiCucHQDangKy.Name = "lblChiCucHQDangKy";
            this.lblChiCucHQDangKy.StylePriority.UseBorders = false;
            this.lblChiCucHQDangKy.StylePriority.UseFont = false;
            this.lblChiCucHQDangKy.StylePriority.UseTextAlignment = false;
            this.lblChiCucHQDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblChiCucHQDangKy.Weight = 0.98812664907651715;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell94.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.StylePriority.UseFont = false;
            this.xrTableCell94.StylePriority.UseTextAlignment = false;
            this.xrTableCell94.Text = " Ngày, giờ gửi:";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell94.Weight = 0.72100671683512751;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.StylePriority.UseFont = false;
            this.xrTableCell72.StylePriority.UseTextAlignment = false;
            this.xrTableCell72.Text = " Ngày, giờ đăng ký:";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell72.Weight = 0.67240267867712433;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell96.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.StylePriority.UseFont = false;
            this.xrTableCell96.StylePriority.UsePadding = false;
            this.xrTableCell96.StylePriority.UseTextAlignment = false;
            this.xrTableCell96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell96.Weight = 0.61846395541123123;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.lblNgaySoThamChieu,
            this.lblNgayDangKy,
            this.xrTableCell100});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.StylePriority.UseBorders = false;
            this.xrTableRow27.Weight = 1.2405250242549131;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell97.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.StylePriority.UseFont = false;
            this.xrTableCell97.StylePriority.UseTextAlignment = false;
            this.xrTableCell97.Text = " Chi cục Hải quan cửa khẩu nhập:";
            this.xrTableCell97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell97.Weight = 0.98810740940014075;
            // 
            // lblNgaySoThamChieu
            // 
            this.lblNgaySoThamChieu.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaySoThamChieu.Name = "lblNgaySoThamChieu";
            this.lblNgaySoThamChieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgaySoThamChieu.StylePriority.UseFont = false;
            this.lblNgaySoThamChieu.StylePriority.UsePadding = false;
            this.lblNgaySoThamChieu.StylePriority.UseTextAlignment = false;
            this.lblNgaySoThamChieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgaySoThamChieu.Weight = 0.721006722347325;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgayDangKy.StylePriority.UseBorders = false;
            this.lblNgayDangKy.StylePriority.UseFont = false;
            this.lblNgayDangKy.StylePriority.UsePadding = false;
            this.lblNgayDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayDangKy.Weight = 0.67240267867712433;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.StylePriority.UsePadding = false;
            this.xrTableCell100.StylePriority.UseTextAlignment = false;
            this.xrTableCell100.Text = "Hệ thống xử lý dữ liệu điện tử Hải quan";
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell100.Weight = 0.61848318957540993;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenChiCucHQCuaKhau,
            this.xrTableCell17,
            this.xrTableCell6,
            this.lblSoPhuLucToKhai,
            this.xrTableCell20});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.StylePriority.UseBorders = false;
            this.xrTableRow18.Weight = 1.115400196080973;
            // 
            // lblTenChiCucHQCuaKhau
            // 
            this.lblTenChiCucHQCuaKhau.Name = "lblTenChiCucHQCuaKhau";
            this.lblTenChiCucHQCuaKhau.Weight = 0.98812664907651715;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Weight = 0.72104696122589829;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 5, 100F);
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = " Số lượng phụ lục tờ khai:";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell6.Weight = 0.54115694602492415;
            // 
            // lblSoPhuLucToKhai
            // 
            this.lblSoPhuLucToKhai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoPhuLucToKhai.Name = "lblSoPhuLucToKhai";
            this.lblSoPhuLucToKhai.StylePriority.UseBorders = false;
            this.lblSoPhuLucToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoPhuLucToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSoPhuLucToKhai.Weight = 0.13123490689924783;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Weight = 0.61843453677341265;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell1,
            this.lblLoaiHinh});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1.2959184373702635;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.StylePriority.UseFont = false;
            this.xrTableCell101.Text = " 1.Người xuất khẩu:";
            this.xrTableCell101.Weight = 0.98812664907651715;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = " 5. Mã loại hình:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.5270448548812664;
            // 
            // lblLoaiHinh
            // 
            this.lblLoaiHinh.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLoaiHinh.Name = "lblLoaiHinh";
            this.lblLoaiHinh.StylePriority.UseBorders = false;
            this.lblLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblLoaiHinh.Text = "NDT01 - Nhập đầu tư";
            this.lblLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblLoaiHinh.Weight = 1.4848284960422165;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNguoiXK,
            this.xrTableCell7,
            this.xrTableCell74,
            this.lblGiayPhep,
            this.xrTableCell9,
            this.lblHopDong});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1;
            // 
            // lblNguoiXK
            // 
            this.lblNguoiXK.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNguoiXK.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiXK.Name = "lblNguoiXK";
            this.lblNguoiXK.StylePriority.UseBorders = false;
            this.lblNguoiXK.StylePriority.UseFont = false;
            this.lblNguoiXK.Weight = 0.98812664907651715;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = " 6.Hóa đơn thương mại";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.655610703510897;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.StylePriority.UseFont = false;
            this.xrTableCell74.StylePriority.UseTextAlignment = false;
            this.xrTableCell74.Text = " 7. Giấy phép số:";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell74.Weight = 0.36846581363949882;
            // 
            // lblGiayPhep
            // 
            this.lblGiayPhep.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblGiayPhep.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiayPhep.Name = "lblGiayPhep";
            this.lblGiayPhep.StylePriority.UseBorders = false;
            this.lblGiayPhep.StylePriority.UseFont = false;
            this.lblGiayPhep.StylePriority.UseTextAlignment = false;
            this.lblGiayPhep.Text = "So GP";
            this.lblGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblGiayPhep.Weight = 0.37224728843231109;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = " 8. Hợp đồng:";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.272741732481078;
            // 
            // lblHopDong
            // 
            this.lblHopDong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblHopDong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHopDong.Name = "lblHopDong";
            this.lblHopDong.StylePriority.UseBorders = false;
            this.lblHopDong.StylePriority.UseFont = false;
            this.lblHopDong.StylePriority.UseTextAlignment = false;
            this.lblHopDong.Text = "368/HD-TVD1-P2";
            this.lblHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblHopDong.Weight = 0.34280781285969775;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell107,
            this.lblHoaDonThuongMai,
            this.xrTableCell5,
            this.lblNgayGiayPhep,
            this.xrTableCell14,
            this.lblNgayHopDong});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.99985508506506693;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "MST";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.30077473393423165;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.BorderWidth = 1;
            this.xrTableCell107.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMSTNguoiXuat});
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.StylePriority.UseBorderWidth = false;
            this.xrTableCell107.StylePriority.UseFont = false;
            this.xrTableCell107.Weight = 0.68734226831054246;
            // 
            // lblMSTNguoiXuat
            // 
            this.lblMSTNguoiXuat.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMSTNguoiXuat.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lblMSTNguoiXuat.Name = "lblMSTNguoiXuat";
            this.lblMSTNguoiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.lblMSTNguoiXuat.SizeF = new System.Drawing.SizeF(170.69F, 24.7475F);
            this.lblMSTNguoiXuat.StylePriority.UseBorders = false;
            this.lblMSTNguoiXuat.StylePriority.UsePadding = false;
            // 
            // lblHoaDonThuongMai
            // 
            this.lblHoaDonThuongMai.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblHoaDonThuongMai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoaDonThuongMai.Name = "lblHoaDonThuongMai";
            this.lblHoaDonThuongMai.StylePriority.UseBorders = false;
            this.lblHoaDonThuongMai.StylePriority.UseFont = false;
            this.lblHoaDonThuongMai.StylePriority.UseTextAlignment = false;
            this.lblHoaDonThuongMai.Text = "368/HD-TVD1-P2-110901";
            this.lblHoaDonThuongMai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblHoaDonThuongMai.Weight = 0.65561070253815623;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = " Ngày:";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 0.31801372001918016;
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgayGiayPhep.StylePriority.UseBorders = false;
            this.lblNgayGiayPhep.StylePriority.UsePadding = false;
            this.lblNgayGiayPhep.StylePriority.UseTextAlignment = false;
            this.lblNgayGiayPhep.Text = "19/08/2010";
            this.lblNgayGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayGiayPhep.Weight = 0.42284706557373958;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = " Ngày:";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 0.26795529315906114;
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgayHopDong.StylePriority.UseBorders = false;
            this.lblNgayHopDong.StylePriority.UsePadding = false;
            this.lblNgayHopDong.StylePriority.UseTextAlignment = false;
            this.lblNgayHopDong.Text = "19/08/2010";
            this.lblNgayHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayHopDong.Weight = 0.34745621646508817;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lableNguoiN,
            this.lblNgayHoaDonThuongMai,
            this.xrTableCell8,
            this.lblNgayHetHanGiayPhep,
            this.xrTableCell75,
            this.lblNgayHetHanHopDong});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.StylePriority.UseBorders = false;
            this.xrTableRow31.Weight = 0.79798858070312717;
            // 
            // lableNguoiN
            // 
            this.lableNguoiN.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lableNguoiN.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lableNguoiN.Name = "lableNguoiN";
            this.lableNguoiN.StylePriority.UseBorders = false;
            this.lableNguoiN.StylePriority.UseFont = false;
            this.lableNguoiN.Text = " 2.Người nhập khẩu:";
            this.lableNguoiN.Weight = 0.98810737719161512;
            // 
            // lblNgayHoaDonThuongMai
            // 
            this.lblNgayHoaDonThuongMai.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNgayHoaDonThuongMai.Name = "lblNgayHoaDonThuongMai";
            this.lblNgayHoaDonThuongMai.StylePriority.UseBorders = false;
            this.lblNgayHoaDonThuongMai.StylePriority.UseTextAlignment = false;
            this.lblNgayHoaDonThuongMai.Text = "29/08/2010";
            this.lblNgayHoaDonThuongMai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayHoaDonThuongMai.Weight = 0.6556230331074766;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = " Ngày hết hạn:";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell8.Weight = 0.31794195250659635;
            // 
            // lblNgayHetHanGiayPhep
            // 
            this.lblNgayHetHanGiayPhep.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblNgayHetHanGiayPhep.Name = "lblNgayHetHanGiayPhep";
            this.lblNgayHetHanGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgayHetHanGiayPhep.StylePriority.UseBorders = false;
            this.lblNgayHetHanGiayPhep.StylePriority.UsePadding = false;
            this.lblNgayHetHanGiayPhep.StylePriority.UseTextAlignment = false;
            this.lblNgayHetHanGiayPhep.Text = "19/08/2010";
            this.lblNgayHetHanGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayHetHanGiayPhep.Weight = 0.42277806307662347;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.StylePriority.UseTextAlignment = false;
            this.xrTableCell75.Text = " Ngày hết hạn:";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell75.Weight = 0.27000318747675783;
            // 
            // lblNgayHetHanHopDong
            // 
            this.lblNgayHetHanHopDong.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblNgayHetHanHopDong.Name = "lblNgayHetHanHopDong";
            this.lblNgayHetHanHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgayHetHanHopDong.StylePriority.UseBorders = false;
            this.lblNgayHetHanHopDong.StylePriority.UsePadding = false;
            this.lblNgayHetHanHopDong.StylePriority.UseTextAlignment = false;
            this.lblNgayHetHanHopDong.Text = "19/08/2010";
            this.lblNgayHetHanHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayHetHanHopDong.Weight = 0.34554638664093068;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNguoiNK,
            this.xrTableCell19,
            this.xrTableCell42,
            this.xrTableCell44});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.99999999999999989;
            // 
            // lblNguoiNK
            // 
            this.lblNguoiNK.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNguoiNK.Name = "lblNguoiNK";
            this.lblNguoiNK.StylePriority.UseBorders = false;
            this.lblNguoiNK.Weight = 0.98810737719161512;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.Weight = 0.6556230331074766;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Weight = 0.74072001558321987;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Weight = 0.61554957411768851;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.lblMSTNguoiNhapKhau,
            this.xrTableCell10,
            this.xrTableCell114,
            this.xrTableCell115});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UsePadding = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "MST";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.30076512263454536;
            // 
            // lblMSTNguoiNhapKhau
            // 
            this.lblMSTNguoiNhapKhau.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMSTNguoiNhapKhau.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMSTNguoiNK});
            this.lblMSTNguoiNhapKhau.Name = "lblMSTNguoiNhapKhau";
            this.lblMSTNguoiNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblMSTNguoiNhapKhau.StylePriority.UseBorders = false;
            this.lblMSTNguoiNhapKhau.StylePriority.UsePadding = false;
            this.lblMSTNguoiNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblMSTNguoiNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMSTNguoiNhapKhau.Weight = 0.68734231405637647;
            // 
            // lblMSTNguoiNK
            // 
            this.lblMSTNguoiNK.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMSTNguoiNK.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lblMSTNguoiNK.Name = "lblMSTNguoiNK";
            this.lblMSTNguoiNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMSTNguoiNK.SizeF = new System.Drawing.SizeF(170.69F, 24.75107F);
            this.lblMSTNguoiNK.StylePriority.UseBorders = false;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = " 9.Vận đơn:(số/ngày)";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.65576109760101586;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell114.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorderColor = false;
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.StylePriority.UseForeColor = false;
            this.xrTableCell114.StylePriority.UseTextAlignment = false;
            this.xrTableCell114.Text = " 10.Cảng xếp hàng:";
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell114.Weight = 0.74072001558321976;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell115.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorderColor = false;
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.StylePriority.UseForeColor = false;
            this.xrTableCell115.StylePriority.UseTextAlignment = false;
            this.xrTableCell115.Text = " 11.Cảng dỡ hàng:";
            this.xrTableCell115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell115.Weight = 0.61541145012484266;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell116,
            this.lblVanTaiDon,
            this.lblCangXepHang,
            this.lblMaCangdoHang});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.79784366576819421;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Text = " 3.Người ủy thác/ người được ủy quyền:";
            this.xrTableCell116.Weight = 0.98812664907651693;
            // 
            // lblVanTaiDon
            // 
            this.lblVanTaiDon.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblVanTaiDon.Name = "lblVanTaiDon";
            this.lblVanTaiDon.StylePriority.UseBorders = false;
            this.lblVanTaiDon.StylePriority.UseTextAlignment = false;
            this.lblVanTaiDon.Text = "QLMD02/03";
            this.lblVanTaiDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblVanTaiDon.Weight = 0.65567282321899745;
            // 
            // lblCangXepHang
            // 
            this.lblCangXepHang.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblCangXepHang.Name = "lblCangXepHang";
            this.lblCangXepHang.StylePriority.UseBorders = false;
            this.lblCangXepHang.StylePriority.UseTextAlignment = false;
            this.lblCangXepHang.Text = "SHANGHAI";
            this.lblCangXepHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblCangXepHang.Weight = 0.74072001558321976;
            // 
            // lblMaCangdoHang
            // 
            this.lblMaCangdoHang.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblMaCangdoHang.Name = "lblMaCangdoHang";
            this.lblMaCangdoHang.StylePriority.UseBorders = false;
            this.lblMaCangdoHang.StylePriority.UseTextAlignment = false;
            this.lblMaCangdoHang.Text = "C021";
            this.lblMaCangdoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaCangdoHang.Weight = 0.61548051212126564;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenNguoiUyThac,
            this.lblNgayVanTaiDon,
            this.lblTenCangXepHang,
            this.lblTenCangDoHang});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1.0000000000000002;
            // 
            // lblTenNguoiUyThac
            // 
            this.lblTenNguoiUyThac.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTenNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiUyThac.Name = "lblTenNguoiUyThac";
            this.lblTenNguoiUyThac.StylePriority.UseBorders = false;
            this.lblTenNguoiUyThac.StylePriority.UseFont = false;
            this.lblTenNguoiUyThac.Weight = 0.98812664907651715;
            // 
            // lblNgayVanTaiDon
            // 
            this.lblNgayVanTaiDon.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayVanTaiDon.Name = "lblNgayVanTaiDon";
            this.lblNgayVanTaiDon.StylePriority.UseBorders = false;
            this.lblNgayVanTaiDon.StylePriority.UseTextAlignment = false;
            this.lblNgayVanTaiDon.Text = "17/10/2010";
            this.lblNgayVanTaiDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayVanTaiDon.Weight = 0.65560111812394239;
            // 
            // lblTenCangXepHang
            // 
            this.lblTenCangXepHang.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenCangXepHang.Name = "lblTenCangXepHang";
            this.lblTenCangXepHang.StylePriority.UseBorders = false;
            this.lblTenCangXepHang.Weight = 0.74072284301621627;
            // 
            // lblTenCangDoHang
            // 
            this.lblTenCangDoHang.Name = "lblTenCangDoHang";
            this.lblTenCangDoHang.StylePriority.UseTextAlignment = false;
            this.lblTenCangDoHang.Text = "Cảng Tiên sa";
            this.lblTenCangDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTenCangDoHang.Weight = 0.615549389783324;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.lblMSTNguoiUT,
            this.xrTableCell3,
            this.lblPhuongTienVanTai,
            this.xrTableCell2,
            this.lblNuocXK});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.99999999999999978;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.StylePriority.UsePadding = false;
            this.xrTableCell119.StylePriority.UseTextAlignment = false;
            this.xrTableCell119.Text = "MST ";
            this.xrTableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell119.Weight = 0.30076511369073489;
            // 
            // lblMSTNguoiUT
            // 
            this.lblMSTNguoiUT.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMSTNguoiUT.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMSTNguoiUyThac});
            this.lblMSTNguoiUT.Name = "lblMSTNguoiUT";
            this.lblMSTNguoiUT.StylePriority.UseBorders = false;
            this.lblMSTNguoiUT.StylePriority.UseTextAlignment = false;
            this.lblMSTNguoiUT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMSTNguoiUT.Weight = 0.68734230511256611;
            // 
            // lblMSTNguoiUyThac
            // 
            this.lblMSTNguoiUyThac.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMSTNguoiUyThac.LocationFloat = new DevExpress.Utils.PointFloat(0.002384186F, 0.001017253F);
            this.lblMSTNguoiUyThac.Name = "lblMSTNguoiUyThac";
            this.lblMSTNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMSTNguoiUyThac.SizeF = new System.Drawing.SizeF(170.69F, 24.75006F);
            this.lblMSTNguoiUyThac.StylePriority.UseBorders = false;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = " 12.Phương tiện vận tải:";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.52904297697506331;
            // 
            // lblPhuongTienVanTai
            // 
            this.lblPhuongTienVanTai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblPhuongTienVanTai.Name = "lblPhuongTienVanTai";
            this.lblPhuongTienVanTai.StylePriority.UseBorders = false;
            this.lblPhuongTienVanTai.StylePriority.UseTextAlignment = false;
            this.lblPhuongTienVanTai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPhuongTienVanTai.Weight = 0.86730008960325411;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = " 13.Nước xuất khẩu:";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 0.41064257123151066;
            // 
            // lblNuocXK
            // 
            this.lblNuocXK.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNuocXK.Name = "lblNuocXK";
            this.lblNuocXK.StylePriority.UseBorders = false;
            this.lblNuocXK.StylePriority.UseTextAlignment = false;
            this.lblNuocXK.Text = "CN";
            this.lblNuocXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNuocXK.Weight = 0.204906943386871;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xrTableCell77,
            this.lblTenSoHieuPTVT,
            this.xrTableCell78,
            this.lblNgayDenPTVT,
            this.lblTenNuoc});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 0.7978436657681941;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.Text = " 4.Đại lý hải quan:";
            this.xrTableCell123.Weight = 0.98812664907651715;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.StylePriority.UseTextAlignment = false;
            this.xrTableCell77.Text = " Tên, số hiệu:";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell77.Weight = 0.29518469656992091;
            // 
            // lblTenSoHieuPTVT
            // 
            this.lblTenSoHieuPTVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenSoHieuPTVT.Name = "lblTenSoHieuPTVT";
            this.lblTenSoHieuPTVT.StylePriority.UseBorders = false;
            this.lblTenSoHieuPTVT.StylePriority.UseTextAlignment = false;
            this.lblTenSoHieuPTVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTenSoHieuPTVT.Weight = 0.47328496042216367;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseBorders = false;
            this.xrTableCell78.StylePriority.UseTextAlignment = false;
            this.xrTableCell78.Text = " Ngày đến:";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell78.Weight = 0.23779683377308714;
            // 
            // lblNgayDenPTVT
            // 
            this.lblNgayDenPTVT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayDenPTVT.Name = "lblNgayDenPTVT";
            this.lblNgayDenPTVT.StylePriority.UseBorders = false;
            this.lblNgayDenPTVT.StylePriority.UseTextAlignment = false;
            this.lblNgayDenPTVT.Text = "19/08/2011";
            this.lblNgayDenPTVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayDenPTVT.Weight = 0.3901263480370456;
            // 
            // lblTenNuoc
            // 
            this.lblTenNuoc.Name = "lblTenNuoc";
            this.lblTenNuoc.StylePriority.UseTextAlignment = false;
            this.lblTenNuoc.Text = "China";
            this.lblTenNuoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTenNuoc.Weight = 0.61548051212126564;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenDaiLy,
            this.xrTableCell4,
            this.lblDieuKienGiaoHang,
            this.xrTableCell12,
            this.lblPhuongThucThanhToan});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1;
            // 
            // lblTenDaiLy
            // 
            this.lblTenDaiLy.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTenDaiLy.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDaiLy.Name = "lblTenDaiLy";
            this.lblTenDaiLy.StylePriority.UseBorders = false;
            this.lblTenDaiLy.StylePriority.UseFont = false;
            this.lblTenDaiLy.Weight = 0.98812664907651715;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = " 14.Điều kiện giao hàng:";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.56068601583113464;
            // 
            // lblDieuKienGiaoHang
            // 
            this.lblDieuKienGiaoHang.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDieuKienGiaoHang.Name = "lblDieuKienGiaoHang";
            this.lblDieuKienGiaoHang.StylePriority.UseBorders = false;
            this.lblDieuKienGiaoHang.StylePriority.UseTextAlignment = false;
            this.lblDieuKienGiaoHang.Text = "CIF";
            this.lblDieuKienGiaoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDieuKienGiaoHang.Weight = 0.36343964158594688;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = " 15.Phương thức thanh toán:";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 0.64143719785376541;
            // 
            // lblPhuongThucThanhToan
            // 
            this.lblPhuongThucThanhToan.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPhuongThucThanhToan.Name = "lblPhuongThucThanhToan";
            this.lblPhuongThucThanhToan.StylePriority.UseBorders = false;
            this.lblPhuongThucThanhToan.StylePriority.UseTextAlignment = false;
            this.lblPhuongThucThanhToan.Text = "CASH";
            this.lblPhuongThucThanhToan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPhuongThucThanhToan.Weight = 0.44631049565263581;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.lblMSTDaiLy,
            this.xrTableCell11,
            this.lblDongTienThanhToan,
            this.xrTableCell76,
            this.lblTyGiaTinhThue});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.99995564456691277;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.StylePriority.UsePadding = false;
            this.xrTableCell129.StylePriority.UseTextAlignment = false;
            this.xrTableCell129.Text = "MST ";
            this.xrTableCell129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell129.Weight = 0.30076511369073489;
            // 
            // lblMSTDaiLy
            // 
            this.lblMSTDaiLy.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMSTHaiQuan});
            this.lblMSTDaiLy.Name = "lblMSTDaiLy";
            this.lblMSTDaiLy.StylePriority.UseTextAlignment = false;
            this.lblMSTDaiLy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMSTDaiLy.Weight = 0.68734230511256611;
            // 
            // lblMSTHaiQuan
            // 
            this.lblMSTHaiQuan.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMSTHaiQuan.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lblMSTHaiQuan.Name = "lblMSTHaiQuan";
            this.lblMSTHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMSTHaiQuan.SizeF = new System.Drawing.SizeF(170.69F, 24.74997F);
            this.lblMSTHaiQuan.StylePriority.UseBorders = false;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = " 16.Đồng tiền thanh toán:";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell11.Weight = 0.56466302974551175;
            // 
            // lblDongTienThanhToan
            // 
            this.lblDongTienThanhToan.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDongTienThanhToan.Name = "lblDongTienThanhToan";
            this.lblDongTienThanhToan.StylePriority.UseBorders = false;
            this.lblDongTienThanhToan.StylePriority.UseTextAlignment = false;
            this.lblDongTienThanhToan.Text = "USD";
            this.lblDongTienThanhToan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDongTienThanhToan.Weight = 0.359412795948363;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseBorders = false;
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.Text = " 17.Tỷ giá tính thuế:";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell76.Weight = 0.63938924403676212;
            // 
            // lblTyGiaTinhThue
            // 
            this.lblTyGiaTinhThue.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTyGiaTinhThue.Name = "lblTyGiaTinhThue";
            this.lblTyGiaTinhThue.StylePriority.UseBorders = false;
            this.lblTyGiaTinhThue.StylePriority.UseTextAlignment = false;
            this.lblTyGiaTinhThue.Text = "20803";
            this.lblTyGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTyGiaTinhThue.Weight = 0.44842751146606208;
            // 
            // lblCucHaiQuan
            // 
            this.lblCucHaiQuan.BorderWidth = 0;
            this.lblCucHaiQuan.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCucHaiQuan.LocationFloat = new DevExpress.Utils.PointFloat(140.9583F, 37.5F);
            this.lblCucHaiQuan.Name = "lblCucHaiQuan";
            this.lblCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCucHaiQuan.SizeF = new System.Drawing.SizeF(225F, 23F);
            this.lblCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(606.9583F, 12.5F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(173F, 25F);
            this.winControlContainer1.WinControl = this.label1;
            // 
            // label1
            // 
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 24);
            this.label1.TabIndex = 0;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BorderWidth = 0;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(373.9583F, 37.5F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Nhập khẩu";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader1,
            this.ReportFooter1});
            this.DetailReport1.Level = 0;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.HeightF = 20F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(745F, 20F);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang1,
            this.lblMoTaHang1,
            this.lblMaHSHang1,
            this.lblXuatXuHang1,
            this.lblCheDoUuDaiHang1,
            this.lblLuongHang1,
            this.lblDVTHang1,
            this.lblDonGiaHang1,
            this.lblTGNTHang1});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow2.Weight = 0.51489361702127678;
            // 
            // lblSTTHang1
            // 
            this.lblSTTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTHang1.Name = "lblSTTHang1";
            this.lblSTTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTTHang1.StylePriority.UseBorders = false;
            this.lblSTTHang1.Text = "1";
            this.lblSTTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTHang1.Weight = 0.032981530343007916;
            // 
            // lblMoTaHang1
            // 
            this.lblMoTaHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMoTaHang1.Multiline = true;
            this.lblMoTaHang1.Name = "lblMoTaHang1";
            this.lblMoTaHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoTaHang1.StylePriority.UseBorders = false;
            this.lblMoTaHang1.StylePriority.UseTextAlignment = false;
            this.lblMoTaHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang1.Weight = 0.250636609941386;
            // 
            // lblMaHSHang1
            // 
            this.lblMaHSHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaHSHang1.Multiline = true;
            this.lblMaHSHang1.Name = "lblMaHSHang1";
            this.lblMaHSHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHSHang1.StylePriority.UseBorders = false;
            this.lblMaHSHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang1.Weight = 0.09586867595757112;
            // 
            // lblXuatXuHang1
            // 
            this.lblXuatXuHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblXuatXuHang1.Name = "lblXuatXuHang1";
            this.lblXuatXuHang1.StylePriority.UseBorders = false;
            this.lblXuatXuHang1.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang1.Weight = 0.076125799082715023;
            // 
            // lblCheDoUuDaiHang1
            // 
            this.lblCheDoUuDaiHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCheDoUuDaiHang1.Multiline = true;
            this.lblCheDoUuDaiHang1.Name = "lblCheDoUuDaiHang1";
            this.lblCheDoUuDaiHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCheDoUuDaiHang1.StylePriority.UseBorders = false;
            this.lblCheDoUuDaiHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblCheDoUuDaiHang1.Weight = 0.0803368100440934;
            // 
            // lblLuongHang1
            // 
            this.lblLuongHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongHang1.Multiline = true;
            this.lblLuongHang1.Name = "lblLuongHang1";
            this.lblLuongHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang1.StylePriority.UseBorders = false;
            this.lblLuongHang1.StylePriority.UseTextAlignment = false;
            this.lblLuongHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang1.Weight = 0.10146804149998071;
            // 
            // lblDVTHang1
            // 
            this.lblDVTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVTHang1.Name = "lblDVTHang1";
            this.lblDVTHang1.StylePriority.UseBorders = false;
            this.lblDVTHang1.StylePriority.UseTextAlignment = false;
            this.lblDVTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang1.Weight = 0.0868259501063305;
            // 
            // lblDonGiaHang1
            // 
            this.lblDonGiaHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang1.Name = "lblDonGiaHang1";
            this.lblDonGiaHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblDonGiaHang1.StylePriority.UseBorders = false;
            this.lblDonGiaHang1.StylePriority.UsePadding = false;
            this.lblDonGiaHang1.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDonGiaHang1.Weight = 0.076156345734979036;
            // 
            // lblTGNTHang1
            // 
            this.lblTGNTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTGNTHang1.Name = "lblTGNTHang1";
            this.lblTGNTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGNTHang1.StylePriority.UseBorders = false;
            this.lblTGNTHang1.StylePriority.UsePadding = false;
            this.lblTGNTHang1.StylePriority.UseTextAlignment = false;
            this.lblTGNTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang1.Weight = 0.19960023728993642;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.ReportHeader1.HeightF = 30F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrTable11
            // 
            this.xrTable11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable11.SizeF = new System.Drawing.SizeF(745F, 30F);
            this.xrTable11.StylePriority.UseFont = false;
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell15,
            this.xrTableCell67,
            this.xrTableCell68});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow14.StylePriority.UseTextAlignment = false;
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow14.Weight = 0.45053191489361721;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "Số TT";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell61.Weight = 0.032981530343007916;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.Multiline = true;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "18.Mô tả hàng hóa";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 0.250636609941386;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Multiline = true;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "19.Mã số hàng hóa";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.0958686759575711;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.Text = "20.Xuất xứ";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell64.Weight = 0.076125799082715065;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Multiline = true;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.Text = "21.Chế độ ưu đãi";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell65.Weight = 0.080336810044093432;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Multiline = true;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "22.Lượng hàng";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell66.Weight = 0.10146804149998072;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "23.Đơn vị tính";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.0868259501063305;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.Text = "24.Đơn giá ";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell67.Weight = 0.076486161038409051;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.Text = "25.Trị giá nguyên tệ";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell68.Weight = 0.19927042198650641;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.xrTable8});
            this.ReportFooter1.HeightF = 180F;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(33F, 20F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow10,
            this.xrTableRow9,
            this.xrTableRow8,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow11});
            this.xrTable2.SizeF = new System.Drawing.SizeF(745F, 160F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell33});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow3.Weight = 0.45421966647498579;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Text = "Loại thuế";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell26.Weight = 0.28496042216358841;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Multiline = true;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 0.25233128508437963;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Multiline = true;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell30.Weight = 0.098760425705229235;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.Text = "Tiền thuế";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell33.Weight = 0.36394786704680282;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.lblTGTTNKHang1,
            this.lblThueSuatNKHang1,
            this.lblTienThueNKHang1});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.StylePriority.UseBorders = false;
            this.xrTableRow10.StylePriority.UseTextAlignment = false;
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow10.Weight = 0.30204772857964363;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Text = " 26. Thuế nhập khẩu";
            this.xrTableCell53.Weight = 0.28496042216358841;
            // 
            // lblTGTTNKHang1
            // 
            this.lblTGTTNKHang1.Name = "lblTGTTNKHang1";
            this.lblTGTTNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGTTNKHang1.StylePriority.UsePadding = false;
            this.lblTGTTNKHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang1.Weight = 0.25233128508437963;
            // 
            // lblThueSuatNKHang1
            // 
            this.lblThueSuatNKHang1.Name = "lblThueSuatNKHang1";
            this.lblThueSuatNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatNKHang1.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatNKHang1.Weight = 0.098760425705229235;
            // 
            // lblTienThueNKHang1
            // 
            this.lblTienThueNKHang1.Name = "lblTienThueNKHang1";
            this.lblTienThueNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTienThueNKHang1.StylePriority.UseBorders = false;
            this.lblTienThueNKHang1.StylePriority.UsePadding = false;
            this.lblTienThueNKHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang1.Weight = 0.36394786704680282;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.lblTGTTTTDBHang1,
            this.lblThueSuatTTDBHang1,
            this.lblTienThueTTDBHang1});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 0.30343933294997127;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = " 27.Thuế TTĐB/Tự vệ chông bán phá giá ";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell49.Weight = 0.28496042216358841;
            // 
            // lblTGTTTTDBHang1
            // 
            this.lblTGTTTTDBHang1.Name = "lblTGTTTTDBHang1";
            this.lblTGTTTTDBHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGTTTTDBHang1.StylePriority.UsePadding = false;
            this.lblTGTTTTDBHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTTTDBHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTTTDBHang1.Weight = 0.25233128508437963;
            // 
            // lblThueSuatTTDBHang1
            // 
            this.lblThueSuatTTDBHang1.Name = "lblThueSuatTTDBHang1";
            this.lblThueSuatTTDBHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatTTDBHang1.StylePriority.UsePadding = false;
            this.lblThueSuatTTDBHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTTDBHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatTTDBHang1.Weight = 0.098760425705229235;
            // 
            // lblTienThueTTDBHang1
            // 
            this.lblTienThueTTDBHang1.Name = "lblTienThueTTDBHang1";
            this.lblTienThueTTDBHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTienThueTTDBHang1.StylePriority.UseBorders = false;
            this.lblTienThueTTDBHang1.StylePriority.UsePadding = false;
            this.lblTienThueTTDBHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueTTDBHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueTTDBHang1.Weight = 0.36394786704680282;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.lblTGTTBVMTHang1,
            this.lblThueSuatBVMTHang1,
            this.lblTienThueBVMTHang1});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.StylePriority.UseBorders = false;
            this.xrTableRow8.Weight = 0.30343933294997139;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.Text = " 28.Thuế  BVMT";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell45.Weight = 0.28496042216358841;
            // 
            // lblTGTTBVMTHang1
            // 
            this.lblTGTTBVMTHang1.Name = "lblTGTTBVMTHang1";
            this.lblTGTTBVMTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGTTBVMTHang1.StylePriority.UsePadding = false;
            this.lblTGTTBVMTHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTBVMTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTBVMTHang1.Weight = 0.25233128508437963;
            // 
            // lblThueSuatBVMTHang1
            // 
            this.lblThueSuatBVMTHang1.Name = "lblThueSuatBVMTHang1";
            this.lblThueSuatBVMTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatBVMTHang1.StylePriority.UsePadding = false;
            this.lblThueSuatBVMTHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatBVMTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatBVMTHang1.Weight = 0.098760425705229235;
            // 
            // lblTienThueBVMTHang1
            // 
            this.lblTienThueBVMTHang1.Name = "lblTienThueBVMTHang1";
            this.lblTienThueBVMTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTienThueBVMTHang1.StylePriority.UseBorders = false;
            this.lblTienThueBVMTHang1.StylePriority.UsePadding = false;
            this.lblTienThueBVMTHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueBVMTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueBVMTHang1.Weight = 0.36394786704680282;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.lblTGTTVATHang1,
            this.lblThueSuatVATHang1,
            this.lblTienThueVATHang1});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.StylePriority.UseBorders = false;
            this.xrTableRow7.Weight = 0.30343933294997127;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.Text = " 29.Thuế GTGT";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.28496042216358841;
            // 
            // lblTGTTVATHang1
            // 
            this.lblTGTTVATHang1.Name = "lblTGTTVATHang1";
            this.lblTGTTVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGTTVATHang1.StylePriority.UsePadding = false;
            this.lblTGTTVATHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang1.Weight = 0.25233128508437963;
            // 
            // lblThueSuatVATHang1
            // 
            this.lblThueSuatVATHang1.Name = "lblThueSuatVATHang1";
            this.lblThueSuatVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatVATHang1.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatVATHang1.Weight = 0.098760425705229235;
            // 
            // lblTienThueVATHang1
            // 
            this.lblTienThueVATHang1.Name = "lblTienThueVATHang1";
            this.lblTienThueVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTienThueVATHang1.StylePriority.UsePadding = false;
            this.lblTienThueVATHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang1.Weight = 0.36394786704680282;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.lblTongTienThue});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.38018631397354818;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBorders = false;
            this.xrTableCell79.StylePriority.UseTextAlignment = false;
            this.xrTableCell79.Text = " 30.Tổng số tiền thuế(ô 26+27+28+29):";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell79.Weight = 0.29683377308707126;
            // 
            // lblTongTienThue
            // 
            this.lblTongTienThue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTongTienThue.Name = "lblTongTienThue";
            this.lblTongTienThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTongTienThue.StylePriority.UseBorders = false;
            this.lblTongTienThue.StylePriority.UsePadding = false;
            this.lblTongTienThue.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue.Text = "0";
            this.lblTongTienThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTongTienThue.Weight = 0.70316622691292874;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell80,
            this.lblBangChu});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.380186313973548;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.StylePriority.UseTextAlignment = false;
            this.xrTableCell80.Text = " Bằng chữ:";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell80.Weight = 0.098944591029023782;
            // 
            // lblBangChu
            // 
            this.lblBangChu.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblBangChu.Name = "lblBangChu";
            this.lblBangChu.StylePriority.UseBorders = false;
            this.lblBangChu.StylePriority.UseTextAlignment = false;
            this.lblBangChu.Text = "Không đồng chẵn";
            this.lblBangChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblBangChu.Weight = 0.90105540897097625;
            // 
            // xrTable8
            // 
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable8.SizeF = new System.Drawing.SizeF(745F, 20F);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblLePhi,
            this.xrTableCell37,
            this.lblTongTGNT});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow38.StylePriority.UseTextAlignment = false;
            this.xrTableRow38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow38.Weight = 0.51489361702127678;
            // 
            // lblLePhi
            // 
            this.lblLePhi.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLePhi.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLePhi.Multiline = true;
            this.lblLePhi.Name = "lblLePhi";
            this.lblLePhi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLePhi.StylePriority.UseBorders = false;
            this.lblLePhi.StylePriority.UseFont = false;
            this.lblLePhi.StylePriority.UseTextAlignment = false;
            this.lblLePhi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblLePhi.Weight = 0.63778222450461308;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UsePadding = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.Text = "Cộng:";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.16294731640963095;
            // 
            // lblTongTGNT
            // 
            this.lblTongTGNT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTGNT.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTGNT.Name = "lblTongTGNT";
            this.lblTongTGNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongTGNT.StylePriority.UseBorders = false;
            this.lblTongTGNT.StylePriority.UseFont = false;
            this.lblTongTGNT.StylePriority.UsePadding = false;
            this.lblTongTGNT.StylePriority.UseTextAlignment = false;
            this.lblTongTGNT.Text = "0";
            this.lblTongTGNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTGNT.Weight = 0.19927045908575611;
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.ReportHeader3,
            this.ReportFooter3});
            this.DetailReport3.Level = 1;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.Detail3.HeightF = 80F;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow19});
            this.xrTable4.SizeF = new System.Drawing.SizeF(745F, 80F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont1,
            this.lblSoHieuCont1,
            this.lblSoLuongKienCont1,
            this.lblTrongLuongCont1,
            this.lblDiaDiem1});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 1.6800000000000002;
            // 
            // lblSTTCont1
            // 
            this.lblSTTCont1.Name = "lblSTTCont1";
            this.lblSTTCont1.StylePriority.UseTextAlignment = false;
            this.lblSTTCont1.Text = "1";
            this.lblSTTCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont1.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont1
            // 
            this.lblSoHieuCont1.Name = "lblSoHieuCont1";
            this.lblSoHieuCont1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoHieuCont1.StylePriority.UsePadding = false;
            this.lblSoHieuCont1.Weight = 0.75490782879708163;
            // 
            // lblSoLuongKienCont1
            // 
            this.lblSoLuongKienCont1.Name = "lblSoLuongKienCont1";
            this.lblSoLuongKienCont1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongKienCont1.StylePriority.UsePadding = false;
            this.lblSoLuongKienCont1.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont1.Weight = 0.64356572399992884;
            // 
            // lblTrongLuongCont1
            // 
            this.lblTrongLuongCont1.Name = "lblTrongLuongCont1";
            this.lblTrongLuongCont1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrongLuongCont1.StylePriority.UsePadding = false;
            this.lblTrongLuongCont1.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont1.Weight = 0.81908767331904864;
            // 
            // lblDiaDiem1
            // 
            this.lblDiaDiem1.Name = "lblDiaDiem1";
            this.lblDiaDiem1.Weight = 0.68217492164119642;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont2,
            this.lblSoHieuCont2,
            this.lblSoLuongKienCont2,
            this.lblTrongLuongCont2,
            this.lblDiaDiem2});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.Weight = 1.6800000000000002;
            // 
            // lblSTTCont2
            // 
            this.lblSTTCont2.Name = "lblSTTCont2";
            this.lblSTTCont2.StylePriority.UseTextAlignment = false;
            this.lblSTTCont2.Text = "2";
            this.lblSTTCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont2.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont2
            // 
            this.lblSoHieuCont2.Name = "lblSoHieuCont2";
            this.lblSoHieuCont2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoHieuCont2.StylePriority.UsePadding = false;
            this.lblSoHieuCont2.Weight = 0.75490782879708163;
            // 
            // lblSoLuongKienCont2
            // 
            this.lblSoLuongKienCont2.Name = "lblSoLuongKienCont2";
            this.lblSoLuongKienCont2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongKienCont2.StylePriority.UsePadding = false;
            this.lblSoLuongKienCont2.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont2.Weight = 0.64356572399992884;
            // 
            // lblTrongLuongCont2
            // 
            this.lblTrongLuongCont2.Name = "lblTrongLuongCont2";
            this.lblTrongLuongCont2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrongLuongCont2.StylePriority.UsePadding = false;
            this.lblTrongLuongCont2.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont2.Weight = 0.81908767331904864;
            // 
            // lblDiaDiem2
            // 
            this.lblDiaDiem2.Name = "lblDiaDiem2";
            this.lblDiaDiem2.Weight = 0.68217492164119642;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont3,
            this.lblSoHieuCont3,
            this.lblSoLuongKienCont3,
            this.lblTrongLuongCont3,
            this.lblDiaDiem3});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.StylePriority.UseBorders = false;
            this.xrTableRow17.Weight = 1.6800000000000002;
            // 
            // lblSTTCont3
            // 
            this.lblSTTCont3.Name = "lblSTTCont3";
            this.lblSTTCont3.StylePriority.UseTextAlignment = false;
            this.lblSTTCont3.Text = "3";
            this.lblSTTCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont3.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont3
            // 
            this.lblSoHieuCont3.Name = "lblSoHieuCont3";
            this.lblSoHieuCont3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoHieuCont3.StylePriority.UsePadding = false;
            this.lblSoHieuCont3.Weight = 0.75490782879708163;
            // 
            // lblSoLuongKienCont3
            // 
            this.lblSoLuongKienCont3.Name = "lblSoLuongKienCont3";
            this.lblSoLuongKienCont3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongKienCont3.StylePriority.UsePadding = false;
            this.lblSoLuongKienCont3.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont3.Weight = 0.64356572399992884;
            // 
            // lblTrongLuongCont3
            // 
            this.lblTrongLuongCont3.Name = "lblTrongLuongCont3";
            this.lblTrongLuongCont3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrongLuongCont3.StylePriority.UsePadding = false;
            this.lblTrongLuongCont3.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont3.Weight = 0.81908767331904864;
            // 
            // lblDiaDiem3
            // 
            this.lblDiaDiem3.Name = "lblDiaDiem3";
            this.lblDiaDiem3.Weight = 0.68217492164119642;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.lblSoHieuCont4,
            this.lblSoLuongKienCont4,
            this.lblTrongLuongCont4,
            this.lblDiaDiem4});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1.6800000000000002;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "4";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell22.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont4
            // 
            this.lblSoHieuCont4.Multiline = true;
            this.lblSoHieuCont4.Name = "lblSoHieuCont4";
            this.lblSoHieuCont4.Text = "\r\n";
            this.lblSoHieuCont4.Weight = 0.75490782879708163;
            // 
            // lblSoLuongKienCont4
            // 
            this.lblSoLuongKienCont4.Name = "lblSoLuongKienCont4";
            this.lblSoLuongKienCont4.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont4.Weight = 0.64356572399992884;
            // 
            // lblTrongLuongCont4
            // 
            this.lblTrongLuongCont4.Name = "lblTrongLuongCont4";
            this.lblTrongLuongCont4.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont4.Weight = 0.81908767331904864;
            // 
            // lblDiaDiem4
            // 
            this.lblDiaDiem4.Name = "lblDiaDiem4";
            this.lblDiaDiem4.Weight = 0.68217492164119642;
            // 
            // ReportHeader3
            // 
            this.ReportHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.ReportHeader3.HeightF = 47F;
            this.ReportHeader3.Name = "ReportHeader3";
            // 
            // xrTable6
            // 
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow5});
            this.xrTable6.SizeF = new System.Drawing.SizeF(745F, 47F);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.StylePriority.UseFont = false;
            this.xrTableRow4.Weight = 0.79999999999999993;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "31.Lượng hàng, số hiệu container";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell25.Weight = 3;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell31,
            this.xrTableCell16,
            this.xrTableCell32});
            this.xrTableRow5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseFont = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow5.Weight = 1.08;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "Số\r\n TT";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell27.Weight = 0.10026385224274405;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Text = "a.Số hiệu container";
            this.xrTableCell28.Weight = 0.75490782879708174;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "b.Số lượng kiện trong container";
            this.xrTableCell31.Weight = 0.64356572399992884;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "c.Trọng lượng hàng trong container";
            this.xrTableCell16.Weight = 0.81908767331904864;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "d. Địa điểm đóng hàng";
            this.xrTableCell32.Weight = 0.68217492164119642;
            // 
            // ReportFooter3
            // 
            this.ReportFooter3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter3.HeightF = 20F;
            this.ReportFooter3.Name = "ReportFooter3";
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable5.SizeF = new System.Drawing.SizeF(745F, 20F);
            this.xrTable5.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.lblTongCont,
            this.xrTableCell21,
            this.lblTongKien,
            this.xrTableCell23,
            this.lblTongTrongLuong,
            this.xrTableCell39});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1.6800000000000002;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell35.Weight = 0.10026385224274405;
            // 
            // lblTongCont
            // 
            this.lblTongCont.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.lblTongCont.Name = "lblTongCont";
            this.lblTongCont.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTongCont.StylePriority.UseFont = false;
            this.lblTongCont.StylePriority.UsePadding = false;
            this.lblTongCont.StylePriority.UseTextAlignment = false;
            this.lblTongCont.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTongCont.Weight = 0.75490782879708163;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "Cộng";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.15668219085902477;
            // 
            // lblTongKien
            // 
            this.lblTongKien.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongKien.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongKien.Name = "lblTongKien";
            this.lblTongKien.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongKien.StylePriority.UseBorders = false;
            this.lblTongKien.StylePriority.UseFont = false;
            this.lblTongKien.StylePriority.UsePadding = false;
            this.lblTongKien.StylePriority.UseTextAlignment = false;
            this.lblTongKien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongKien.Weight = 0.48688353314090393;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "Cộng";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell23.Weight = 0.1467921588071755;
            // 
            // lblTongTrongLuong
            // 
            this.lblTongTrongLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTrongLuong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTrongLuong.Name = "lblTongTrongLuong";
            this.lblTongTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongTrongLuong.StylePriority.UseBorders = false;
            this.lblTongTrongLuong.StylePriority.UseFont = false;
            this.lblTongTrongLuong.StylePriority.UsePadding = false;
            this.lblTongTrongLuong.StylePriority.UseTextAlignment = false;
            this.lblTongTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTrongLuong.Weight = 0.67229594462539044;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell39.Weight = 0.68217449152767939;
            // 
            // GroupFooter
            // 
            this.GroupFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.GroupFooter.HeightF = 268F;
            this.GroupFooter.Name = "GroupFooter";
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow13,
            this.xrTableRow24,
            this.xrTableRow21});
            this.xrTable7.SizeF = new System.Drawing.SizeF(746F, 265.2918F);
            this.xrTable7.StylePriority.UseFont = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 5.6828164342678154;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.lblChungTuDiKem});
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Weight = 0.99217474455915411;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(2F, 1F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(240F, 25F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = " 32.Chứng từ đi kèm:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblChungTuDiKem
            // 
            this.lblChungTuDiKem.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblChungTuDiKem.LocationFloat = new DevExpress.Utils.PointFloat(2F, 26F);
            this.lblChungTuDiKem.Name = "lblChungTuDiKem";
            this.lblChungTuDiKem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblChungTuDiKem.SizeF = new System.Drawing.SizeF(240F, 82F);
            this.lblChungTuDiKem.StylePriority.UseBorders = false;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell82.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.lblNgayIn,
            this.xrLabel7});
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseBorders = false;
            this.xrTableCell82.Weight = 2.0118521010784294;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(240F, 28F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "33.Tôi xin cam đoan, chịu trách nhiệm trước         pháp luật về nội dung khai tr" +
                "ên tờ khai";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayIn
            // 
            this.lblNgayIn.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayIn.LocationFloat = new DevExpress.Utils.PointFloat(98F, 28F);
            this.lblNgayIn.Name = "lblNgayIn";
            this.lblNgayIn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayIn.SizeF = new System.Drawing.SizeF(250F, 15F);
            this.lblNgayIn.StylePriority.UseBorders = false;
            this.lblNgayIn.StylePriority.UseTextAlignment = false;
            this.lblNgayIn.Text = "Ngày...tháng...năm.....";
            this.lblNgayIn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Italic);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(100F, 43F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(250F, 20F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "(Người khai ký, ghi rõ họ tên, đóng dấu)";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell84,
            this.xrTableCell69,
            this.xrTableCell85});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 1.0799977718062477;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.Text = "34. Kết quả phân luồng và hướng dẫn làm thủ tục hải quan";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell40.Weight = 0.99217477541665122;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.StylePriority.UseTextAlignment = false;
            this.xrTableCell84.Text = "36. Xác nhận của hải quan giám sát";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell84.Weight = 0.689223641469892;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.StylePriority.UseTextAlignment = false;
            this.xrTableCell69.Text = "37.Xác nhận giải phóng hàng/ đưa hàng về bảo quản/ chuyển cửa khẩu";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell69.Weight = 0.78990765171503952;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.StylePriority.UseTextAlignment = false;
            this.xrTableCell85.Text = " 38. Xác nhận thông quan";
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell85.Weight = 0.53272077703600074;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblKetQuaPhanLuong,
            this.xrTableCell83,
            this.xrTableCell91,
            this.xrTableCell93});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1.6771857939259365;
            // 
            // lblKetQuaPhanLuong
            // 
            this.lblKetQuaPhanLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblKetQuaPhanLuong.Multiline = true;
            this.lblKetQuaPhanLuong.Name = "lblKetQuaPhanLuong";
            this.lblKetQuaPhanLuong.StylePriority.UseBorders = false;
            this.lblKetQuaPhanLuong.Weight = 0.99217477541665122;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.Weight = 0.689223641469892;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Weight = 0.78990765171503952;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.53272077703600074;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell70,
            this.xrTableCell88});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.68000000000000149;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.Text = "35. Ghi chép khác:";
            this.xrTableCell86.Weight = 0.99217481605559643;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.Weight = 0.68922353938615866;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Weight = 0.78990759027025137;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Weight = 0.532720899925577;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblGhiChepKhac,
            this.xrTableCell43,
            this.xrTableCell46,
            this.xrTableCell47});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.StylePriority.UseBorders = false;
            this.xrTableRow21.Weight = 1.2105978665431592;
            // 
            // lblGhiChepKhac
            // 
            this.lblGhiChepKhac.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblGhiChepKhac.Name = "lblGhiChepKhac";
            this.lblGhiChepKhac.StylePriority.UseBorders = false;
            this.lblGhiChepKhac.Weight = 0.99217493894517272;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.Weight = 0.68922329360700607;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Weight = 0.789907467380675;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Weight = 0.53272114570472973;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 10F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 1F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // TQDTToKhaiNK_TT196
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader,
            this.DetailReport1,
            this.DetailReport3,
            this.GroupFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ExportOptions.Xlsx.SheetName = "Tờ khai chính";
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(1, 1, 10, 1);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell lblChiCucHQDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaySoThamChieu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiHinh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoiXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell lblCangXepHang;
        private DevExpress.XtraReports.UI.XRTableCell lblMaCangdoHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiUyThac;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCangXepHang;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCangDoHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongTienVanTai;
        private DevExpress.XtraReports.UI.XRTableCell lblNuocXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDenPTVT;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNuoc;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDaiLy;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKienGiaoHang;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongThucThanhToan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell lblMSTDaiLy;
        private DevExpress.XtraReports.UI.XRTableCell lblDongTienThanhToan;
        private DevExpress.XtraReports.UI.XRTableCell lblTyGiaTinhThue;
        private DevExpress.XtraReports.UI.XRTableCell lblMSTNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell lblVanTaiDon;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayVanTaiDon;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSoHieuPTVT;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueTTDBHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueBVMTHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThue;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTTTDBHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTTDBHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTBVMTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatBVMTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell lblBangChu;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter3;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell lblTongCont;
        private DevExpress.XtraReports.UI.XRTableCell lblTongKien;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel lblNgayIn;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblCucHaiQuan;
        private DevExpress.XtraReports.UI.XRTableCell lblGiayPhep;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell lblHoaDonThuongMai;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell lblHopDong;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell lableNguoiN;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHetHanGiayPhep;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHetHanHopDong;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHoaDonThuongMai;
        private DevExpress.XtraReports.UI.XRTableCell lblSoThamChieu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell lblKetQuaPhanLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblChungTuDiKem;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRLabel lblMSTNguoiNK;
        private DevExpress.XtraReports.UI.XRLabel lblMSTHaiQuan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell lblTenChiCucHQCuaKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblSoPhuLucToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem1;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem2;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont4;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont4;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoiNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRLabel lblMSTNguoiXuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChepKhac;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell lblLePhi;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTGNT;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRTableCell lblMSTNguoiUT;
        private DevExpress.XtraReports.UI.XRLabel lblMSTNguoiUyThac;
    }
}
