﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.Interface;

/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT;
using Company.BLL;
using Company.BLL.Utils;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif

namespace Company.Interface
{
    public partial class VanTaiDonForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        public VanTaiDonForm()
        {
            InitializeComponent();
        }

        private void VanTaiDonForm_Load(object sender, EventArgs e)
        {
            //if (TKMD.VanTaiDon != null && TKMD.VanTaiDon.SoVanDon == "." && TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "X")
            //{
            //    dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
            //    BindData();
            //    try { dgList.Refetch(); }
            //    catch { dgList.Refresh(); }
            //    return;
            //}
            //if (TKMD.VanTaiDon == null && TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "X")
            //    return;
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            //DATLMQ bổ sung Lấy thông tin từ TKMD đưa vào Vận đơn 13/12/2010
            if (TKMD.SoVanDon.Length >= 0 && TKMD.VanTaiDon == null)
            {
                txtSoVanDon.Text = TKMD.SoVanDon;
                if (TKMD.NgayVanDon.Year == 1900)
                    ccNgayVanDon.Text = DateTime.Now.ToShortDateString();
                else
                    ccNgayVanDon.Text = TKMD.NgayVanDon.ToShortDateString();
                try
                {
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        txtMaNguoiNhanHang.Text = TKMD.MaDoanhNghiep;
                        txtTenNguoiNhanHang.Text = TKMD.TenDoanhNghiep;
                        txtTenNguoiGiaoHang.Text = TKMD.TenDonViDoiTac;
                    }
                    else
                    {
                        txtTenNguoiNhanHang.Text = TKMD.TenDonViDoiTac;
                        txtMaNguoiGiaoHang.Text = TKMD.MaDoanhNghiep;
                        txtTenNguoiGiaoHang.Text = TKMD.TenDoanhNghiep;

                        uiGroupBox5.Text = "Cửa khẩu xuất";
                        uiGroupBox4.Text = "Cửa khẩu nhập";
                        uiGroupBox8.Enabled = false;
                    }
                    cuaKhauControl1.Ma = TKMD.CuaKhau_ID;
                    txtMaDiaDiemDoHang.Text = TKMD.CuaKhau_ID;
                    txtTenDiaDiemDoHang.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);
                    txtTenDiaDiemXepHang.Text = TKMD.DiaDiemXepHang;
                    txtTongSoKien.Text = txtLoaiKien.Text = string.Empty;

                    //DATLMQ update NgayTauDen 17/01/2011
                    ccNgayDen.Text = TKMD.NgayDenPTVT.Year > 1900 ? TKMD.NgayDenPTVT.ToShortDateString() : string.Empty;
                    //DATLMQ update NoiDi, CuaKhauXuat 17/01/2011
                    txtNoiDi.Text = txtCuaKhauXuat.Text = txtTenDiaDiemXepHang.Text;
                    //DATLMQ update NgayKhoiHanh 17/01/2011
                    ccNgayKhoiHanh.Text = TKMD.NgayVanDon.Year > 1900 ? TKMD.NgayVanDon.ToShortDateString(): string.Empty;
                    //DATLMQ update DiaDiemGiaoHang 17/01/2011
                    txtDiaDiemGiaoHang.Text = txtTenDiaDiemDoHang.Text;
                    if (TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "N")
                        chkQuocTich.Visible = false;
                }
                catch
                {
                    ShowMessage("Cần lưu thông tin tờ khai trước khi mở Vận đơn", false);
                    this.Dispose();
                }
            }

            if (TKMD.VanTaiDon != null)
            {
                cuaKhauControl1.Ma = TKMD.VanTaiDon.CuaKhauNhap_ID;
                txtCuaKhauXuat.Text = TKMD.VanTaiDon.CuaKhauXuat;
                cbDKGH.SelectedValue = TKMD.VanTaiDon.DKGH_ID;
                chkHangRoi.Checked = TKMD.VanTaiDon.HangRoi;
                txtMaDiaDiemDoHang.Text = TKMD.VanTaiDon.MaCangDoHang;
                txtMaDiaDiemXepHang.Text = TKMD.VanTaiDon.MaCangXepHang;
                txtMaHangVT.Text = TKMD.VanTaiDon.MaHangVT;
                txtMaNguoiGiaoHang.Text = TKMD.VanTaiDon.MaNguoiGiaoHang;
                txtMaNguoiNhanHang.Text = TKMD.VanTaiDon.MaNguoiNhanHang;
                txtMaNguoiNhanHangTG.Text = TKMD.VanTaiDon.MaNguoiNhanHangTrungGian;
                //ccNgayDen.Value = TKMD.VanTaiDon.NgayDenPTVT;
                ccNgayDen.Text = TKMD.VanTaiDon.NgayDenPTVT.Year > 1900 ? TKMD.VanTaiDon.NgayDenPTVT.ToShortDateString() : string.Empty;
                //ccNgayVanDon.Value = TKMD.VanTaiDon.NgayVanDon;
                ccNgayVanDon.Text =  TKMD.VanTaiDon.NgayVanDon.Year>1900 ? TKMD.VanTaiDon.NgayVanDon.ToShortDateString(): string.Empty;
                ctrNuocXuat.Ma = TKMD.VanTaiDon.NuocXuat_ID;
                ctrQuocTichPTVT.Ma = TKMD.VanTaiDon.QuocTichPTVT;
                txtSoHieuPTVT.Text = TKMD.VanTaiDon.SoHieuPTVT;
                txtSoVanDon.Text = TKMD.VanTaiDon.SoVanDon;
                txtTenDiaDiemDoHang.Text = TKMD.VanTaiDon.TenCangDoHang;
                txtTenDiaDiemXepHang.Text = TKMD.VanTaiDon.TenCangXepHang;
                txtTenHangVT.Text = TKMD.VanTaiDon.TenHangVT;
                txtTenNguoiGiaoHang.Text = TKMD.VanTaiDon.TenNguoiGiaoHang;
                //txtTenNguoiNhanHang.Text = TKMD.TenDonViDoiTac;
                txtTenNguoiNhanHang.Text = TKMD.VanTaiDon.TenNguoiNhanHang;
                txtTenNguoiNhanHangTG.Text = TKMD.VanTaiDon.TenNguoiNhanHangTrungGian;
                txtTenPTVT.Text = TKMD.VanTaiDon.TenPTVT;
                txtNoiDi.Text = TKMD.VanTaiDon.NoiDi;
                txtSoHieuChuyenDi.Text = TKMD.VanTaiDon.SoHieuChuyenDi;
                //ccNgayKhoiHanh.Value = TKMD.VanTaiDon.NgayKhoiHanh;
                ccNgayKhoiHanh.Text = TKMD.VanTaiDon.NgayKhoiHanh.Year > 1900 ? TKMD.VanTaiDon.NgayKhoiHanh.ToShortDateString() : string.Empty;
                txtDiaDiemGiaoHang.Text = TKMD.VanTaiDon.DiaDiemGiaoHang;
                txtTongSoKien.Value = TKMD.VanTaiDon.TongSoKien;
                txtLoaiKien.Text = TKMD.VanTaiDon.LoaiKien;
                if (TKMD.VanTaiDon.ContainerCollection == null)
                    TKMD.VanTaiDon.ContainerCollection = new List<Container>();
                dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
                BindData();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }

            }
            else
            {
                txtMaNguoiNhanHang.Text = txtMaNguoiNhanHangTG.Text = TKMD.MaDoanhNghiep;
                txtTenNguoiNhanHang.Text = txtTenNguoiNhanHangTG.Text = TKMD.TenDoanhNghiep;
                txtSoHieuPTVT.Text = TKMD.SoHieuPTVT;

            }

            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                Ghi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXoaVanDon.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemContainerExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                TaoContainer.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Xoa.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                )
            {
                Ghi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXoaVanDon.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemContainerExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                TaoContainer.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Xoa.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            }

            if (Xoa.Enabled == Janus.Windows.UI.InheritableBoolean.True)
            {
                thêmHàngĐóngGóiToolStripMenuItem.Enabled = true;
                xóaHàngĐãChọnToolStripMenuItem.Enabled = true;
                this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
                //this.dgHang.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgHang_DeletingRecords);
            }

        }
        protected override void OnClosed(EventArgs e)
        {
            //if (TKMD.VanTaiDon != null && string.IsNullOrEmpty(TKMD.VanTaiDon.SoVanDon))
            //    TKMD.VanTaiDon = null;
            base.OnClosed(e);
        }
        private void Save()
        {
            try
            {
                if (TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "N")
                {
                    cvError.Validate();
                    if (!cvError.IsValid)
                    {
                        return;
                    }
                    if (!ValidateVanDon())
                        return;
                    if (string.IsNullOrEmpty(ctrQuocTichPTVT.Ma))
                    {
                        ShowMessage("Không được để trống quốc tịch PTVT", false);
                        return;
                    }
                }

                if (TKMD.VanTaiDon == null)
                    TKMD.VanTaiDon = new VanDon();

                if (TKMD.VanTaiDon.ListHangOfVanDon.Count > 0)
                {

                    GridEXRow[] rows = dgHang.GetRows();
                    foreach (GridEXRow row in rows)
                    {
                        DataRowView rowview = (DataRowView)row.DataRow;
                        string msg = string.Empty;
                        double dTrongLuong = Convert.ToDouble(row.Cells["TrongLuong"].Value);
                        if (dTrongLuong <= 0)
                        {
                            msg = string.Format("Dòng hàng thứ {0} \r\nTrọng lượng phải lớn hơn không", row.Position + 1);

                        }
                        else if (string.IsNullOrEmpty(row.Cells["LoaiKien"].Text))
                        {

                            msg = string.Format("Dòng hàng thứ {0} \r\nLoại kiện trong hàng đóng gói chưa nhập đầy đủ", row.Position + 1);
                        }
                        else
                            if (string.IsNullOrEmpty(row.Cells["SoHieuKien"].Text))
                            {
                                msg = string.Format("Dòng hàng thứ {0} \r\nSố hiệu kiện trong hàng đóng gói chưa nhập đầy đủ", row.Position + 1);

                            }
                            else
                                if (string.IsNullOrEmpty(row.Cells["SoHieuContainer"].Text))
                                {
                                    msg = string.Format("Dòng hàng thứ {0} \r\nSố hiệu container trong hàng đóng gói chưa nhập đầy đủ", row.Position + 1);
                                }
                        if (msg != string.Empty)
                        {
                            ShowMessage(msg, false);
                            uiPanel1.Show();
                            return;
                        }
                        string idHang = rowview["HMD_ID"].ToString().Trim();
                        HangVanDonDetail hangVanDon = TKMD.VanTaiDon.ListHangOfVanDon.Find(hang => hang.HMD_ID.ToString() == idHang);
                        if (hangVanDon != null)
                        {
                            hangVanDon.LoaiKien = row.Cells["LoaiKien"].Text;
                            hangVanDon.SoHieuKien = row.Cells["SoHieuKien"].Text;
                            hangVanDon.SoHieuContainer = row.Cells["SoHieuContainer"].Text;
                            hangVanDon.GhiChu = row.Cells["GhiChu"].Text;
                            hangVanDon.TrongLuong = dTrongLuong;
                        }

                    }
                }
                //fill vận đơn ảo cho tờ khai xuất
//                 if (string.IsNullOrEmpty(txtSoVanDon.Text) && TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "X")
//                 {
//                     //TKMD.VanTaiDon.SoVanDon = ".";
//                     //TKMD.VanTaiDon.NgayDenPTVT = new DateTime(1900, 1, 1);
//                     //TKMD.VanTaiDon.NgayKhoiHanh = new DateTime(1900, 1, 1);
//                     //TKMD.VanTaiDon.NgayVanDon = new DateTime(1900, 1, 1);
//                     //TKMD.VanTaiDon.SoHieuPTVT = TKMD.SoHieuPTVT;
//                     //TKMD.VanTaiDon.TKMD_ID = TKMD.ID;
//                     //TKMD.NgayVanDon = new DateTime(1900, 1, 1);
//                     //TKMD.VanTaiDon.QuocTichPTVT = string.IsNullOrEmpty(TKMD.QuocTichPTVT_ID) ? "1" : TKMD.QuocTichPTVT_ID;
//                     //TKMD.VanTaiDon.CuaKhauNhap_ID = string.IsNullOrEmpty(TKMD.CuaKhau_ID) ? "A001" : TKMD.CuaKhau_ID;
// //                 }
//                 else
//                 {

                    TKMD.CuaKhau_ID = TKMD.VanTaiDon.CuaKhauNhap_ID = cuaKhauControl1.Ma;
                    TKMD.VanTaiDon.CuaKhauXuat = txtCuaKhauXuat.Text.Trim();
                    TKMD.DKGH_ID = TKMD.VanTaiDon.DKGH_ID = cbDKGH.SelectedValue.ToString();
                    TKMD.VanTaiDon.HangRoi = chkHangRoi.Checked;
                    TKMD.VanTaiDon.MaCangDoHang = txtMaDiaDiemDoHang.Text.Trim();
                    TKMD.VanTaiDon.MaCangXepHang = txtMaDiaDiemXepHang.Text.Trim();
                    TKMD.VanTaiDon.MaHangVT = txtMaHangVT.Text.Trim();
                    TKMD.VanTaiDon.MaNguoiGiaoHang = txtMaNguoiGiaoHang.Text.Trim();
                    TKMD.VanTaiDon.MaNguoiNhanHang = txtMaNguoiNhanHang.Text.Trim();
                    TKMD.VanTaiDon.MaNguoiNhanHangTrungGian = txtMaNguoiNhanHangTG.Text.Trim();
                    if (!string.IsNullOrEmpty(ccNgayDen.Text))
                        TKMD.NgayDenPTVT = TKMD.VanTaiDon.NgayDenPTVT = ccNgayDen.Value;
                    else TKMD.NgayDenPTVT = TKMD.VanTaiDon.NgayDenPTVT = new DateTime(1900,1,1);

                    if (!string.IsNullOrEmpty(ccNgayVanDon.Text))
                        TKMD.NgayVanDon = TKMD.VanTaiDon.NgayVanDon = ccNgayVanDon.Value;
                    else TKMD.NgayVanDon = TKMD.VanTaiDon.NgayVanDon = new DateTime(1900,1,1);
                    TKMD.VanTaiDon.NuocXuat_ID = ctrNuocXuat.Ma;
                    if (!string.IsNullOrEmpty(TKMD.MaLoaiHinh) && TKMD.MaLoaiHinh.StartsWith("N"))
                        TKMD.NuocXK_ID = TKMD.VanTaiDon.NuocXuat_ID;
                    if (chkQuocTich.Checked)
                        TKMD.VanTaiDon.QuocTichPTVT = string.Empty;
                    else
                        TKMD.VanTaiDon.QuocTichPTVT = ctrQuocTichPTVT.Ma;
                    TKMD.VanTaiDon.SoHieuPTVT = txtSoHieuPTVT.Text.Trim();
                    TKMD.SoVanDon = TKMD.VanTaiDon.SoVanDon =  txtSoVanDon.Text.Trim();
                    TKMD.VanTaiDon.TenCangDoHang = txtTenDiaDiemDoHang.Text.Trim();
                    TKMD.DiaDiemXepHang = TKMD.VanTaiDon.TenCangXepHang = txtTenDiaDiemXepHang.Text.Trim();
                    TKMD.VanTaiDon.TenHangVT = txtTenHangVT.Text.Trim();
                    TKMD.VanTaiDon.TenNguoiGiaoHang = txtTenNguoiGiaoHang.Text.Trim();
                    TKMD.VanTaiDon.TenNguoiNhanHang = txtTenNguoiNhanHang.Text.Trim();
                    TKMD.VanTaiDon.TenNguoiNhanHangTrungGian = txtTenNguoiNhanHangTG.Text;
                    TKMD.SoHieuPTVT = txtSoHieuPTVT.Text.Trim();
                    TKMD.VanTaiDon.TenPTVT = txtTenPTVT.Text.Trim();
                    TKMD.VanTaiDon.TKMD_ID = TKMD.ID;
                    TKMD.VanTaiDon.DiaDiemGiaoHang = txtDiaDiemGiaoHang.Text.Trim();
                    TKMD.VanTaiDon.NoiDi = txtNoiDi.Text.Trim();
                    TKMD.VanTaiDon.SoHieuChuyenDi = txtSoHieuChuyenDi.Text.Trim();
                    if (!string.IsNullOrEmpty(ccNgayKhoiHanh.Text))
                        TKMD.VanTaiDon.NgayKhoiHanh = ccNgayKhoiHanh.Value;
                    else
                        TKMD.VanTaiDon.NgayKhoiHanh = new DateTime(1900,1,1);
                    TKMD.VanTaiDon.TenCangXepHang = txtTenDiaDiemXepHang.Text;
                    TKMD.VanTaiDon.TongSoKien = Convert.ToInt32(txtTongSoKien.Value);
                    TKMD.VanTaiDon.LoaiKien = txtLoaiKien.Text;
              /*  }*/
                if (TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count > 0)
                {
                    TKMD.SoContainer20 = 0;
                    TKMD.SoContainer40 = 0;
                    foreach (Container c in TKMD.VanTaiDon.ContainerCollection)
                    {
                        if (c.LoaiContainer == "2")
                            TKMD.SoContainer20++;
                        else
                            TKMD.SoContainer40++;
                    }
                }

                ShowMessage("Đã ghi vận tải đơn.\nLưu ý: Nhớ lưu lại thông tin tờ khai.", false);
                ToKhaiMauDichForm.tenPTVT = txtTenPTVT.Text;
                this.Close();
            }
            catch (System.Exception ex)
            {
                ShowMessage("Lưu vận đơn thất bại. Vui lòng nhập đầy đủ thông tin trên form", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void XoaVanDon()
        {
            try
            {
                if (TKMD.VanTaiDon == null) return;

                if (ShowMessage("Bạn có muốn xóa vận đơn này không?", true) == "Yes")
                {
                    //Xoa container
                    foreach (Container c in TKMD.VanTaiDon.ContainerCollection)
                    {
                        try
                        {
                            if (c.ID > 0)
                            {
                                c.Delete();
                            }
                            TKMD.VanTaiDon.ContainerCollection.Remove(c);
                        }
                        catch (Exception ex) { throw ex; }
                    }

                    //Xoa van don
                    TKMD.VanTaiDon.Delete();
                    TKMD.VanTaiDon = null;
                    ShowMessage("Xóa vận đơn thành công.", false);

                    this.Close();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); ShowMessage(ex.Message, false, true, ex.StackTrace); }
        }

        private void XoaContainer()
        {
            List<Container> ContainerCollection = new List<Container>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa Container này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Container container = (Container)i.GetRow().DataRow;
                        ContainerCollection.Add(container);
                    }
                }
                foreach (Container c in ContainerCollection)
                {
                    try
                    {
                        if (c.ID > 0)
                        {
                            c.Delete();
                        }
                        TKMD.VanTaiDon.ContainerCollection.Remove(c);
                    }
                    catch { }
                }
            }
            dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void AddContainer()
        {
            AddContainerForm f = new AddContainerForm();
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
            if (TKMD.VanTaiDon == null) return;
            dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

        }
        private void AddContainerExcel()
        {
#if KD_V3
            ReadExcContainerForm rexcelForm = new ReadExcContainerForm();
            rexcelForm.TKMD = TKMD;
            if (TKMD.VanTaiDon == null) return;
            dgList.DataSource = rexcelForm.TKMD.VanTaiDon.ContainerCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            rexcelForm.ShowDialog(this);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
#endif
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Ghi": Save();
                    break;
                case "cmdXoaVanDon": XoaVanDon();
                    break;
                case "Xoa": XoaContainer();
                    break;
                case "TaoContainer": AddContainer();
                    break;
                case "ThemContainerExcel": AddContainerExcel();
                    break;
                case "ThemHang":
                    ThemHang_Click();
                    break;
            }
        }
        private void ThemHang_Click()
        {

            if (TKMD.VanTaiDon == null)
            {
                TKMD.VanTaiDon = new VanDon();
            }
            try
            {
                SelectHangMauDichForm f = new SelectHangMauDichForm();
                f.TKMD = TKMD;
                f.ShowDialog(this);

                if (f.HMDTMPCollection.Count > 0)
                {
                    foreach (HangMauDich HMD in f.HMDTMPCollection)
                    {

                        HangVanDonDetail hangVanDon = TKMD.VanTaiDon.ListHangOfVanDon.Find(hang => hang.HMD_ID == HMD.ID);
                        if (hangVanDon == null)
                        {
                            hangVanDon = new HangVanDonDetail();
                            hangVanDon.HMD_ID = HMD.ID;
                            hangVanDon.VanDon_ID = TKMD.VanTaiDon.ID;
                            hangVanDon.MaPhu = HMD.MaPhu;
                            hangVanDon.MaHS = HMD.MaHS;
                            hangVanDon.TenHang = HMD.TenHang;
                            hangVanDon.DVT_ID = HMD.DVT_ID;
                            hangVanDon.SoThuTuHang = HMD.SoThuTuHang;
                            hangVanDon.SoLuong = HMD.SoLuong;
                            hangVanDon.NuocXX_ID = HMD.NuocXX_ID;
                            hangVanDon.MaNguyenTe = TKMD.NguyenTe_ID;
                            hangVanDon.DonGiaKB = Convert.ToDouble(HMD.DonGiaKB);
                            hangVanDon.TriGiaKB = Convert.ToDouble(HMD.TriGiaKB);
                            hangVanDon.TrongLuong = Convert.ToDouble(HMD.TrongLuong);
                            TKMD.VanTaiDon.ListHangOfVanDon.Add(hangVanDon);

                        }

                    }
                }
                try
                {
                    BindData();
                }
                catch
                {
                    throw;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
        private void BindData()
        {
            dgHang.DataSource = TKMD.VanTaiDon.ConvertListToDataSet(HangMauDich.SelectBy_TKMD_ID(TKMD.ID).Tables[0]);
            try { dgHang.Refetch(); }
            catch { dgHang.Refresh(); }
        }
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            List<Container> ContainerCollection = new List<Container>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa Container này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Container ContainerTMP = (Container)i.GetRow().DataRow;
                        ContainerCollection.Add(ContainerTMP);
                    }
                }
                foreach (Container item in ContainerCollection)
                {
                    try
                    {
                        if (item.ID > 0)
                        {
                            item.Delete();
                        }
                        TKMD.VanTaiDon.ContainerCollection.Remove(item);
                    }
                    catch { }
                }
                dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
            else
                e.Cancel = true;

        }

        private void chkQuocTich_CheckedChanged(object sender, EventArgs e)
        {
            if (chkQuocTich.Checked)
                ctrQuocTichPTVT.Enabled = false;
            else
                ctrQuocTichPTVT.Enabled = true;
        }
        private void dgListHang_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["LoaiContainer"].Text == "2")
                e.Row.Cells["LoaiContainer"].Text = "Container20";
            if (e.Row.Cells["LoaiContainer"].Text == "4")
                e.Row.Cells["LoaiContainer"].Text = "Container40";
            if (e.Row.Cells["Trang_thai"].Text == "0")
                e.Row.Cells["Trang_thai"].Text = "Rỗng";
            if (e.Row.Cells["Trang_thai"].Text == "1")
                e.Row.Cells["Trang_thai"].Text = "Đầy";
        }
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            Container cont = (Container)e.Row.DataRow;
            if (e != null)
            {
                AddContainerForm f = new AddContainerForm();
                f.TKMD = this.TKMD;
                f.container = cont;
                f.ShowDialog(this);
                if (TKMD.VanTaiDon == null) return;
                dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }
        #region Begin VALIDATE VAN DON

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateVanDon()
        {
            bool isValid = true;
            if (TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "X")
            {
                try
                {
                    //So_Chung_Tu(35)
                    isValid = Globals.ValidateLength(txtSoVanDon, 35, err, "Số vận đơn");

                    //Ma_PTVT	char(3)

                    //So_Hieu_PTVT	varchar(25)
                    isValid &= Globals.ValidateLength(txtSoHieuPTVT, 25, err, "Số hiệu phương tiện vận tải");

                    //Ten_PTVT varchar(255)
                    isValid &= Globals.ValidateLength(txtTenPTVT, 255, err, "Tên phương tiện vận tải");


                    //Ma_Hang_Van_Tai	varchar(17)
                    isValid &= Globals.ValidateLength(txtMaHangVT, 17, err, "Mã hãng vận tải");

                    //Ten_Hang_Van_Tai	varchar(35)
                    isValid &= Globals.ValidateLength(txtTenHangVT, 35, err, "Tên hãng vận tải");

                    //Ma_Nguoi_Nhan_Hang	varchar(17)
                    isValid &= Globals.ValidateLength(txtMaNguoiNhanHang, 17, err, "Mã người nhận hàng");

                    //Ma_Nguoi_Giao_Hang	varchar(17)
                    isValid &= Globals.ValidateLength(txtMaNguoiGiaoHang, 17, err, "Mã người giao hàng");

                    //Ma_Nguoi_Nhan_Hang_TG	varchar(17)
                    isValid &= Globals.ValidateLength(txtMaNguoiNhanHangTG, 17, err, "Mã người nhận hàng trung gian");

                    //Ma_DKGH	varchar(7)
                    isValid &= Globals.ValidateLength(cbDKGH, 7, err, "Điều kiện giao hàng");

                    //Dia diem giao hang	nvarchar(60)
                    isValid &= Globals.ValidateLength(txtDiaDiemGiaoHang, 60, err, "Địa điểm giao hàng");

                    //Ma_Cang_Do_Hang	nvarchar(50)
                    isValid &= Globals.ValidateLength(txtMaDiaDiemDoHang, 50, err, "Mã địa điểm dỡ hàng");

                    //Ma_Cang_Xep_Hang	varchar(11)
                    isValid &= Globals.ValidateLength(txtMaDiaDiemXepHang, 11, err, "Mã địa điểm xếp hàng");

                    //Ten_Cang_Xep_Hang	varchar(40)
                    isValid &= Globals.ValidateLength(txtTenDiaDiemXepHang, 40, err, "Tên địa điểm xếp hàng");
                    isValid &= Globals.ValidateDate(ccNgayKhoiHanh, err, "Ngày khởi hành");
                    isValid &= Globals.ValidateNull(txtMaNguoiNhanHangTG, err, "Người nhận hàng trung gian");
                    epError.Clear();
                    if (txtLoaiKien.Text.Length == 0)
                    {
                        epError.SetError(txtLoaiKien, "Loại kiện bắt buộc nhập");
                        isValid = false;
                    }
                    isValid &= txtTongSoKien.Value != null;
                    if (!isValid)
                    {
                        epError.SetError(txtTongSoKien, "Tổng số kiện không để trống");
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

            }
            return isValid;
        }

        #endregion End VALIDATE VAN DONs

        private void dgHang_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            List<HangVanDonDetail> HangsInVanDon = new List<HangVanDonDetail>();
            GridEXSelectedItemCollection items = dgHang.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangVanDonDetail hangCo = new HangVanDonDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hangCo.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hangCo.ID = Convert.ToInt64(row["ID"]);
                        HangsInVanDon.Add(hangCo);
                    }
                }
                foreach (HangVanDonDetail hangVandon in HangsInVanDon)
                {
                    try
                    {
                        if (hangVandon.ID > 0)
                        {
                            hangVandon.Delete();
                        }
                        foreach (HangVanDonDetail item in TKMD.VanTaiDon.ListHangOfVanDon)
                        {
                            if (item.HMD_ID == hangVandon.HMD_ID)
                            {
                                TKMD.VanTaiDon.ListHangOfVanDon.Remove(item);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void xóaHàngĐãChọnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgHang_DeletingRecords(null, null);
        }

        private void thêmHàngĐóngGóiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ThemHang_Click();
        }


    }
}

