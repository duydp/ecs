﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class CuaKhauForm : Company.Interface.BaseForm
    {
        DataSet ds = new DataSet();

        public CuaKhauForm()
        {
            InitializeComponent();
        }

        private void CuaKhauForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            ds = CuaKhau.SelectAll();
            dgList.DataSource = ds;
            dgList.DataMember = ds.Tables[0].TableName;

            CreateColumnCombo_CucHQ(dgList);
        }
        
        /// <summary>
        /// Tao 1 cot thong tin Cuc HQ ComboBox tren luoi. 
        /// De ap dung ham nay, tren luoi phai tao truoc 1 cot co key: MaCuc.
        /// </summary>
        /// <param name="grid"></param>
        public static void CreateColumnCombo_CucHQ(Janus.Windows.GridEX.GridEX grid)
        {
            System.Data.DataSet dsCucHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.SelectAll();

            //Create multiplevalues column

            //When using a many-to-manyn relation, the DataMember of the MultipleValues
            //column must be the name of the relation between the parent table and the 
            //relation table.

            //Janus.Windows.GridEX.GridEXColumn col = new Janus.Windows.GridEX.GridEXColumn();
            Janus.Windows.GridEX.GridEXColumn col = grid.RootTable.Columns["MaCuc"];
            col.EditType = Janus.Windows.GridEX.EditType.Combo;
            col.Caption = "Cục Hải quan";
            col.Width = 100;
            //Since the column will be bound to a list containing DataRowView objects, we must specify which
            //field in the DataRowView will be used as value 
            col.MultipleValueDataMember = "ID";

            //Fill the ValueList with the categories table
            col.HasValueList = true;
            col.ValueList.PopulateValueList(dsCucHQ.Tables[0].DefaultView, "Id", "Ten");
            col.DefaultGroupInterval = Janus.Windows.GridEX.GroupInterval.Text;
            col.CompareTarget = Janus.Windows.GridEX.ColumnCompareTarget.Text;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region GetChanges

                //Updated by Hungtq, 10/08/2012.
                if (ds.Tables[0].GetChanges() != null)
                {
                    DataRow[] rows;

                    foreach (DataRow dr in ds.Tables[0].GetChanges().Rows)
                    {
                        if (dr.RowState == DataRowState.Added)
                        {
                            rows = ds.Tables[0].Select(" ID= '" + dr["ID"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateCreated"] = System.DateTime.Now;
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                        else if (dr.RowState == DataRowState.Modified)
                        {
                            rows = ds.Tables[0].Select(" ID= '" + dr["ID"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                    }
                }

                #endregion

                CuaKhau.Update(ds);
                ShowMessage(setText("Cập nhật thành công.", "Save successfully"), false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (ShowMessage(setText("Bạn có muốn xóa các cửa khẩu này không?", "Do you want to delete ?"), true) != "Yes") e.Cancel = true;
        }
        private bool CheckID(string Id)
        {
            if (ds.Tables[0].Select(" ID= '" + Id.Trim() + "'").Length == 0) return false;
            else return true;
            //foreach (DataRow dr in ds.Tables[0].Rows)
            //{
            //    if (dr["Id"].ToString().Trim().ToUpper() == Id.Trim().ToUpper()) return true;
            //}
            //return false;
        }
        private void dgList_UpdatingCell(object sender, Janus.Windows.GridEX.UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "ID")
            {
                string s = e.Value.ToString();
                if (CheckID(s))
                {
                    ShowMessage(setText("Mã cửa khẩu này đã có.", "This code is exist"), false);
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    ShowMessage(setText("Mã cửa khẩu không được rỗng.", "Border-Gate code must be filled"), false);
                    e.Cancel = true;
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            LoadData();
        }

    }
}

