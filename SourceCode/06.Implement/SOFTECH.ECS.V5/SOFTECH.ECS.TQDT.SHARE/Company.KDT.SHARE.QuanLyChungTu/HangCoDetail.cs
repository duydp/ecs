﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class HangCoDetail
    {
        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_CO_ID(long CO_ID, SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HangCoDetail_Delete]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, CO_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_HangCoDetail_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@Co_ID", SqlDbType.BigInt, Co_ID);
            db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
            db.AddInParameter(dbCommand, "@MaChuyenNganh", SqlDbType.VarChar, MaChuyenNganh);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.NVarChar, SoHoaDon);
            db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
            db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year == 1753 ? DBNull.Value : (object)NgayHoaDon);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
    }
}
