﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.Interface.SXXK;
using Company.Interface.Report.SXXK;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
#endif
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.ExportTotalData
{
    public partial class VNACC_BaoCaoTongHopXNTForm : BaseForm
    {
        public int ChenhLechNgay;
        public int AmXuLyTiep;
        public DateTime fromDate ;
        public DateTime toDate;
        public VNACC_BaoCaoTongHopXNTForm()
        {
            InitializeComponent();
        }

        private void VNACC_BaoCaoTongHopXNTForm_Load(object sender, EventArgs e)
        {
#if GC_V4            
            BindHopDong();
            cbHopDong.SelectedIndex = 0;
#endif
#if SXXK_V4
            cbHopDong.Visible = true;
            cbHopDong.Enabled = false;
#endif
            clcDenNgay.Value = clcTuNgay.Value.AddDays(30);
            grbtimkiem.Enabled = true;
        }
#if GC_V4
        private void BindHopDong()
        {
            try
            {
                HopDong hd = new HopDong();
                hd.SoHopDong = "";
                hd.ID = 0;
                List<HopDong> collection;
                {
                    string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                    collection = HopDong.SelectCollectionDynamic(where, "ID");
                }
                collection.Add(hd);
                cbHopDong.DataSource = collection;
                cbHopDong.DisplayMember = "SoHopDong";
                cbHopDong.ValueMember = "ID";
                cbHopDong.SelectedIndex = collection.Count - 1;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
#endif
        private string GetSeachWhere()
        {
            try
            {

                //this.Cursor = Cursors.WaitCursor;

                string where = String.Empty;
                where = " TKMD.MaDonVi='" + GlobalSettings.MA_DON_VI + "' AND TKMD.TrangThaiXuLy IN ('2','3') ";
#if GC_V4
                if (!String.IsNullOrEmpty(cbHopDong.Text.Trim()))
                    where = where + " AND TKMD.HopDong_ID = " + cbHopDong.Value.ToString() + "";
#endif
                fromDate = clcTuNgay.Value;
                toDate = clcDenNgay.Value;
                where = where + " AND TKMD.NgayDangKy Between '" + fromDate.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + toDate.ToString("yyyy-MM-dd 23:59:59") + "' ";

                if (!String.IsNullOrEmpty(txtMaHangHoa.Text.Trim()))
                    where = where + " AND HMD.MaHangHoa LIKE '%" + txtMaHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtTenHangHoa.Text.Trim()))
                    where = where + " AND HMD.TenHang LIKE '%" + txtTenHangHoa.Text.Trim() + "%' ";
                return where;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        private void BindDataXNT()
        {
            try
            {
#if GC_V4
                grList.RootTable.Columns["Ma"].Visible = false;
                grList.RootTable.Columns["Ten"].Visible = false;
                grList.RootTable.Columns["DVT_ID"].Visible = false;

                grList.RootTable.Columns["MaNPL"].Visible = true;
                grList.RootTable.Columns["TenNPL"].Visible = true;
                grList.RootTable.Columns["DVT"].Visible = true;

                string where = " HopDong_ID = " + cbHopDong.Value.ToString() + " AND TuNgay ='" + clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:00") + "' AND DenNgay = '" + clcDenNgay.Value.ToString("yyyy-MM-dd 00:00:00") + "'";
                grList.Refresh();
                grList.DataSource = KDT_GC_BCXNT_NguyenPhuLieu.SelectDynamic(GetSeachWhereXNT(), "").Tables[0];
                grList.Refetch();
#elif SXXK_V4
                grList.RootTable.Columns["Ma"].Visible = true;
                grList.RootTable.Columns["Ten"].Visible = true;
                grList.RootTable.Columns["DVT_ID"].Visible = true;

                grList.RootTable.Columns["MaNPL"].Visible = false;
                grList.RootTable.Columns["TenNPL"].Visible = false;
                grList.RootTable.Columns["DVT"].Visible = false;

                List<SXXK_NguyenPhuLieu_Ton> NPLCollection = SXXK_NguyenPhuLieu_Ton.SelectCollectionDynamic(GetSeachWhereXNT(),"");
                List<SXXK_NguyenPhuLieu_Ton> NPLCollectionNew = new List<SXXK_NguyenPhuLieu_Ton>();
                foreach (SXXK_NguyenPhuLieu_Ton item in NPLCollection)
                {
                    if (item.LuongNhap != 0 || item.LuongXuat != 0)
                    {
                        NPLCollectionNew.Add(item);
                    }
                }
                grList.Refresh();
                grList.DataSource = NPLCollectionNew;
                grList.Refetch();
#endif
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
#if GC_V4
            try
            {
                Company.Interface.GC.XuLyHopDongFrm fx = new Company.Interface.GC.XuLyHopDongFrm();
                fx.dateFrom = clcTuNgay.Value;
                fx.dateTo = clcDenNgay.Value;
                fx.StartPosition = FormStartPosition.CenterScreen;
                fx.HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
                fx.isXNT = true;
                fx.ShowDialog(this);
                if (fx.DialogResult == DialogResult.OK)
                {
                    //showMsg("MSG_2702010");
                }
                else
                {
                    this.ShowMessage("Lỗi xử lý hợp đồng + " + fx.ExceptionForm.Message, false);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
#endif
            try
            {
#if GC_V4
                ChayThanhLyForm f = new ChayThanhLyForm();
                if (!String.IsNullOrEmpty(txtChenhLechNgay.Text.ToString()))
                {
                    ChenhLechNgay = Convert.ToInt32(txtChenhLechNgay.Text.ToString());
                }
                if (ckbAmXuLyTiep.Checked)
                {
                    AmXuLyTiep = 1;
                }
                else
                {
                    AmXuLyTiep = 0;
                }
                f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                f.DateFrom = fromDate;
                f.DateTo = toDate;
                f.HopDong_ID = Convert.ToInt64(cbHopDong.Value.ToString());
                f.AmXuLyTiep = AmXuLyTiep;
                f.ChenhLechNgay = ChenhLechNgay;
                f.WhereCondition = GetSeachWhere();
                f.ShowDialog(this);
#endif
#if SXXK_V4
                //ChayThanhLyForm f = new ChayThanhLyForm();
                //f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //f.DateFrom = fromDate;
                //f.DateTo = toDate;
                //f.BCXNT = true;
                //f.WhereCondition = GetSeachWhere();
                //f.ShowDialog(this);

                ProcessForm processForm = new ProcessForm();
                processForm.StartPosition = FormStartPosition.CenterScreen;
                processForm.dateFrom = clcTuNgay.Value;
                processForm.dateTo = clcDenNgay.Value;
                processForm.ShowDialog(this);
                if (processForm.DialogResult == DialogResult.OK)
                    ShowMessage("Xử lý thành công ", false);
                else
                {
                    this.ShowMessage("Lỗi xử lý  + " + processForm.ExceptionForm.Message, false);
                }
#endif
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            BindDataXNT();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Báo cáo Tổng hợp xuất nhập tồn  Từ ngày_" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + " Đến ngày _" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
#if GC_V4
                BCNPLXuatNhapTon_TT128_2013 bc = new BCNPLXuatNhapTon_TT128_2013();
                bc.DateFrom = fromDate;
                bc.DateTo = toDate;
                bc.HopDong_ID = Convert.ToInt64(cbHopDong.Value.ToString());
                bc.BindReport("");
                bc.ShowPreview();
#endif
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private string GetSeachWhereXNT()
        {
            try
            {

                //this.Cursor = Cursors.WaitCursor;
#if GC_V4
                string where = String.Empty;
                if (!String.IsNullOrEmpty(cbHopDong.Text.Trim()))
                    where = where + " HopDong_ID = " + cbHopDong.Value.ToString() + "";
                fromDate = clcTuNgay.Value;
                toDate = clcDenNgay.Value;
                where = where + " AND TuNgay ='" + fromDate.ToString("yyyy-MM-dd 00:00:00") + "' AND DenNgay ='" + toDate.ToString("yyyy-MM-dd 00:00:00") + "' ";

                if (!String.IsNullOrEmpty(txtMaHangHoa.Text.Trim()))
                    where = where + " AND MaNPL like '%" + txtMaHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtTenHangHoa.Text.Trim()))
                    where = where + " AND TenNPL like '%" + txtTenHangHoa.Text.Trim() + "%' ";
                return where;
#elif SXXK_V4
                string where = "1 = 1 ";
                if (!String.IsNullOrEmpty(txtMaHangHoa.Text.Trim()))
                    where = where + " AND Ma like '%" + txtMaHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtTenHangHoa.Text.Trim()))
                    where = where + " AND Ten like '%" + txtTenHangHoa.Text.Trim() + "%' ";
                return where;
#endif
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindDataXNT();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {

        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
#if SXXK_V4
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                }
#endif
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

    }
}
