﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components.Messages.Send;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using NguyenTe = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
using NguyenTe = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe;
#elif GC_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.DuLieuChuan;
using NguyenTe = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe;
#endif


namespace Company.Interface
{
    public partial class GiayPhepForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        public GiayPhep giayPhep = new GiayPhep();
        public bool isKhaiBoSung = false;
        private FeedBackContent feedbackContent;
        private string msgInfor = string.Empty;
        public GiayPhepForm()
        {
            InitializeComponent();
            giayPhep.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
        }

        private void BindData()
        {
            //Company.KD.BLL.KDT.HangMauDich hmd = new Company.KD.BLL.KDT.HangMauDich();
            //hmd.TKMD_ID = TKMD.ID;

            //dgList.DataSource = giayPhep.ConvertListToDataSet(HangMauDich.SelectBy_TKMD_ID(TKMD.ID).Tables[0]);
            dgList.DataSource = giayPhep.ListHMDofGiayPhep;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void GiayPhepForm_Load(object sender, EventArgs e)
        {
            if (this._NguyenTe == null)
                _NguyenTe = NguyenTe.SelectAll().Tables[0];
            foreach (DataRow rowNuoc in this._NguyenTe.Rows)
                dgList.RootTable.Columns["MaNguyenTe"].ValueList.Add(rowNuoc["ID"].ToString(), rowNuoc["Ten"].ToString());

            if (giayPhep.ID <= 0)
            {
                //DATLMQ bổ sung GP lấy từ TKMD 13/12/2010
                giayPhep.SoGiayPhep = TKMD.SoGiayPhep;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                giayPhep.NgayGiayPhep = TKMD.NgayGiayPhep;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                giayPhep.NgayHetHan = TKMD.NgayHetHanGiayPhep;
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                //DATLMQ bổ sung Mã đơn vị được cấp 17/01/2011
                txtMaDVdcCap.Text = TKMD.MaDoanhNghiep;
                txtTenDVdcCap.Text = TKMD.TenDoanhNghiep;
            }
            else if (giayPhep.ID > 0)
            {
                txtMaCQCap.Text = giayPhep.MaCoQuanCap;
                txtMaDVdcCap.Text = giayPhep.MaDonViDuocCap;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                txtNguoiCap.Text = giayPhep.NguoiCap;
                txtNoiCap.Text = giayPhep.NoiCap;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                txtTenDVdcCap.Text = giayPhep.TenDonViDuocCap;
                txtTenCQCap.Text = giayPhep.TenQuanCap;
                txtThongTinKhac.Text = giayPhep.ThongTinKhac;
                giayPhep.LoadListHMDofGiayPhep();
                BindData();
            }
            if (TKMD.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            if (giayPhep.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                ccNgayTiepNhan.Text = giayPhep.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = giayPhep.SoTiepNhan + "";
            }
            else if (TKMD.SoToKhai > 0 && int.Parse(TKMD.PhanLuong != "" ? TKMD.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            SetButtonStateGIAYPHEP(TKMD, giayPhep);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HangGiayPhepDetail> HangGiayPhepDetailCollection = new List<HangGiayPhepDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangGiayPhepDetail hgpDetail = new HangGiayPhepDetail();
                        hgpDetail = (HangGiayPhepDetail)i.GetRow().DataRow;
                        HangGiayPhepDetailCollection.Add(hgpDetail);
                    }
                }
                foreach (HangGiayPhepDetail hgpDetailTMP in HangGiayPhepDetailCollection)
                {
                    try
                    {
                        if (hgpDetailTMP.ID > 0)
                        {
                            hgpDetailTMP.Delete();
                        }
                        foreach (HangGiayPhepDetail hgpDetail in giayPhep.ListHMDofGiayPhep)
                        {
                            if (hgpDetail.HMD_ID == hgpDetailTMP.HMD_ID)
                            {
                                giayPhep.ListHMDofGiayPhep.Remove(hgpDetail);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private bool checkSoGiayPhep(string soGiayPhep)
        {
            foreach (GiayPhep gpTMP in TKMD.GiayPhepCollection)
            {
                if (gpTMP.SoGiayPhep.Trim().ToUpper() == soGiayPhep.Trim().ToUpper())
                    return true;
            }
            return false;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (!Globals.ValidateDate(ccNgayHHGiayPhep, ccNgayGiayPhep.Value.AddDays(1), new DateTime(9999, 1, 1),epError,"hết hạn"))
                return;
            TKMD.GiayPhepCollection.Remove(giayPhep);

            if (checkSoGiayPhep(txtSoGiayPhep.Text))
            {
                if (giayPhep.ID > 0)
                    TKMD.GiayPhepCollection.Add(giayPhep);
                ShowMessage("Số giấy phép này đã tồn tại.", false);
                return;
            }

            giayPhep.MaCoQuanCap = txtMaCQCap.Text.Trim();
            giayPhep.MaDonViDuocCap = txtMaDVdcCap.Text.Trim();
            giayPhep.NgayGiayPhep = ccNgayGiayPhep.Value;
            giayPhep.NgayHetHan = ccNgayHHGiayPhep.Value;
            giayPhep.NguoiCap = txtNguoiCap.Text.Trim();
            giayPhep.NoiCap = txtNoiCap.Text.Trim();
            giayPhep.SoGiayPhep = txtSoGiayPhep.Text.Trim();
            giayPhep.TenDonViDuocCap = txtTenDVdcCap.Text.Trim();
            giayPhep.TenQuanCap = txtTenCQCap.Text.Trim();
            giayPhep.ThongTinKhac = txtThongTinKhac.Text;
            giayPhep.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            giayPhep.TKMD_ID = TKMD.ID;
            if (string.IsNullOrEmpty(giayPhep.GuidStr)) giayPhep.GuidStr = Guid.NewGuid().ToString();
            if (isKhaiBoSung)
                giayPhep.LoaiKB = 1;
            else
                giayPhep.LoaiKB = 0;
            GridEXRow[] rowCollection = dgList.GetRows();
            foreach (GridEXRow row in rowCollection)
            {
                HangGiayPhepDetail rowview = (HangGiayPhepDetail)row.DataRow;
                foreach (HangGiayPhepDetail item in giayPhep.ListHMDofGiayPhep)
                {
                    if (rowview.HMD_ID.ToString().Trim() == item.HMD_ID.ToString().Trim())
                    {
                        item.GhiChu = row.Cells["GhiChu"].Text;
                        item.MaChuyenNganh = row.Cells["MaChuyenNganh"].Value != null ? row.Cells["MaChuyenNganh"].Value.ToString() : "";
                        if (row.Cells["MaNguyenTe"].Text != null)
                            try
                            {
                                item.MaNguyenTe = row.Cells["MaNguyenTe"].Value.ToString();
                            }
                            catch { item.MaNguyenTe = "VND"; }
                        else
                        {
                            ShowMessage("Chưa chọn nguyên tệ cho hàng : " + (row.RowIndex + 1), false);
                            return;
                        }
                        break;
                    }
                }
            }

            try
            {
                giayPhep.InsertUpdateFull();
                TKMD.GiayPhepCollection.Add(giayPhep);
                TKMD.SoGiayPhep = giayPhep.SoGiayPhep;
                TKMD.NgayGiayPhep = giayPhep.NgayGiayPhep;
                TKMD.NgayHetHanGiayPhep = giayPhep.NgayHetHan;
                BindData();
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
                return;
            }

        }

        public List<HangMauDich> ConvertListToHangMauDichCollection(List<HangGiayPhepDetail> listHangGiayPhep, List<HangMauDich> hangCollection)
        {

            List<HangMauDich> tmp = new List<HangMauDich>();

            foreach (HangGiayPhepDetail item in listHangGiayPhep)
            {
                foreach (HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất hàng khi kích nút Chọn hàng
            //if (giayPhep.ListHMDofGiayPhep.Count > 0)
            //{
            //    f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(giayPhep.ListHMDofGiayPhep,TKMD.HMDCollection);
            //}
            f.ShowDialog(this);
            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (HangMauDich HMD in f.HMDTMPCollection)
                {
                    bool ok = false;
                    foreach (HangGiayPhepDetail hangGiayPhepDetail in giayPhep.ListHMDofGiayPhep)
                    {
                        if (hangGiayPhepDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HangGiayPhepDetail hgpDetail = new HangGiayPhepDetail();
                        hgpDetail.HMD_ID = HMD.ID;
                        hgpDetail.GiayPhep_ID = giayPhep.ID;
                        hgpDetail.MaPhu = HMD.MaPhu;
                        hgpDetail.MaHS = HMD.MaHS;
                        hgpDetail.TenHang = HMD.TenHang;
                        hgpDetail.DVT_ID = HMD.DVT_ID;
                        hgpDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hgpDetail.SoLuong = HMD.SoLuong;
                        hgpDetail.NuocXX_ID = HMD.NuocXX_ID;
                        hgpDetail.MaNguyenTe = TKMD.NguyenTe_ID;
                        hgpDetail.DonGiaKB = Convert.ToDouble(HMD.DonGiaKB);
                        hgpDetail.TriGiaKB = Convert.ToDouble(HMD.TriGiaKB);
                        giayPhep.ListHMDofGiayPhep.Add(hgpDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void btnChonGP_Click(object sender, EventArgs e)
        {
            ManageGiayPhepForm f = new ManageGiayPhepForm();
            f.isBrower = true;
            f.TKMD_ID = TKMD.ID;
            f.ShowDialog(this);
            if (f.giayPhep != null)
            {
                giayPhep = f.giayPhep;
                txtMaCQCap.Text = giayPhep.MaCoQuanCap;
                txtMaDVdcCap.Text = giayPhep.MaDonViDuocCap;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                txtNguoiCap.Text = giayPhep.NguoiCap;
                txtNoiCap.Text = giayPhep.NoiCap;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                txtTenDVdcCap.Text = giayPhep.TenDonViDuocCap;
                txtTenCQCap.Text = giayPhep.TenQuanCap;
                txtThongTinKhac.Text = giayPhep.ThongTinKhac;
                giayPhep.ID = 0;
                giayPhep.GuidStr = "";
                giayPhep.SoTiepNhan = 0;
                giayPhep.NamTiepNhan = 0;
                giayPhep.NgayTiepNhan = new DateTime(1900, 1, 1);
                BindData();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (giayPhep.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }

            try
            {
                if (giayPhep.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO || giayPhep.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
                    giayPhep.GuidStr = Guid.NewGuid().ToString();
                else if (giayPhep.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    ShowMessage("Bạn đã gửi yêu cầu khai báo bổ sung đến hải quan\r\nVui lòng nhận phản hồi thông tin", false);
                    return;
                }
                #region V3

                ObjectSend msgSend = SingleMessage.BoSungGiayPhep(TKMD, giayPhep);

                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);
                sendMessageForm.Message.XmlSaveMessage(giayPhep.ID, MessageTitle.KhaiBaoBoSungGiayPhep);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    giayPhep.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    sendMessageForm.Message.XmlSaveMessage(giayPhep.ID, MessageTitle.KhaiBaoBoSungGiayPhep);
                    giayPhep.Update();
                    btnLayPhanHoi_Click(null, null);
                }
                else if (isSend && feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    ShowMessageTQDT(msgInfor, false);
                }

                SetButtonStateGIAYPHEP(TKMD, giayPhep);

                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }


        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBack(TKMD, giayPhep.GuidStr);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                isFeedBack = sendMessageForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetButtonStateGIAYPHEP(TKMD, giayPhep);
                }


            }
//             ObjectSend msgSend = SingleMessage.FeedBack(TKMD, giayPhep.GuidStr);
//             SendMessageForm sendMessageForm = new SendMessageForm();
//             sendMessageForm.Send += SendMessage;
//             sendMessageForm.DoSend(msgSend);

        }

        private void LayPhanHoiKhaiBao(string password)
        {
//         StartInvoke:
//             try
//             {
//                 bool thanhcong = giayPhep.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
// 
//                 // Thực hiện kiểm tra.  
//                 if (thanhcong == false)
//                 {
//                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
//                     {
//                         this.Refresh();
//                         goto StartInvoke;
//                     }
// 
//                     btnKhaiBao.Enabled = true;
//                     btnLayPhanHoi.Enabled = true;
//                 }
//                 else
//                 {
//                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.giayPhep.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungGiayPhepThanhCong);
//                     this.ShowMessage(message, false);
// 
//                     btnKhaiBao.Enabled = false;
//                     txtSoTiepNhan.Text = this.giayPhep.SoTiepNhan.ToString("N0");
//                     ccNgayTiepNhan.Value = this.giayPhep.NgayTiepNhan;
//                     ccNgayTiepNhan.Text = this.giayPhep.NgayTiepNhan.ToShortDateString();
//                 }
//             }
//             catch (Exception ex)
//             {
//                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
//             }
        }

        private void LayPhanHoiDuyet(string password)
        {
//         StartInvoke:
//             try
//             {
//                 bool thanhcong = giayPhep.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
// 
//                 // Thực hiện kiểm tra.  
//                 if (thanhcong == false)
//                 {
//                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
//                     {
//                         this.Refresh();
//                         goto StartInvoke;
//                     }
// 
//                     btnLayPhanHoi.Enabled = true;
//                 }
//                 else
//                 {
//                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.giayPhep.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungGiayPhepDuocChapNhan);
//                     if (message.Length == 0)
//                     {
//                         message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.giayPhep.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
//                         txtSoTiepNhan.Text = "";
//                         ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
//                         ccNgayTiepNhan.Text = "";
//                     }
//                     else
//                     {
//                         btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
//                     }
//                     this.ShowMessage(message, false);
// 
//                 }
//             }
//             catch (Exception ex)
//             {
//                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
//             }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (giayPhep.GuidStr != null && giayPhep.GuidStr != "")
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = giayPhep.ID;
                form.DeclarationIssuer = TKMD.MaLoaiHinh.StartsWith("N") ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                form.ShowDialog(this);
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form GIAY PHEP.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private void SetButtonStateGIAYPHEP(ToKhaiMauDich tkmd, Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep)
        {
            if (giayphep == null)
                return;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                     || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                      || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnChonGP.Enabled = status;
                btnChonHang.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            else if (tkmd.SoToKhai > 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {
                btnKetQuaXuLy.Enabled = true;
                //Re set button declaration LanNT


                bool khaiBaoEnable = (giayphep.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     giayphep.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnChonHang.Enabled = btnXoa.Enabled = btnChonGP.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = giayphep.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   giayphep.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   giayphep.TrangThai == TrangThaiXuLy.CHO_DUYET || giayphep.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;

                if (giayphep.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || giayphep.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    txtSoTiepNhan.Text = string.Empty;
                    ccNgayTiepNhan.Text = string.Empty;
                }
                else
                {
                    txtSoTiepNhan.Text = giayphep.SoTiepNhan.ToString();
                    ccNgayTiepNhan.Value = giayphep.NgayTiepNhan;
                }
                if (giayphep.LoaiKB == 1)
                {
                    switch (giayphep.TrangThai)
                    {
                        case -1:
                            lbTrangThai.Text = "Chưa Khai Báo";
                            break;
                        case -3:
                            lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                            break;
                        case 0:
                            lbTrangThai.Text = "Chờ Duyệt";
                            break;
                        case 1:
                            lbTrangThai.Text = "Đã Duyệt";
                            break;
                        case 2:
                            lbTrangThai.Text = "Hải quan không phê duyệt";
                            break;
                    }
                }
                else
                    lbTrangThai.Text = string.Empty;
            }

        }

        #endregion
        
        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);

                                e.FeedBackMessage.XmlSaveMessage(giayPhep.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                giayPhep.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                //this.ShowMessageTQDT("Hải quan từ chối tiếp nhận", noidung, false);
                                noidung = "Hải quan từ chối tiếp nhận.\r\nLý do: " + noidung;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                           // noidung = noidung.Replace(string.Format("Message [{0}]", giayPhep.GuidStr), string.Empty);
                            //this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            btnKhaiBao.Enabled = false;
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                giayPhep.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);
                                //giayPhep.NamTiepNhan = int.Parse(vals[1].Trim());
                                //giayPhep.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);
                                if (feedbackContent.Acceptance.Length == 10)
                                    giayPhep.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd", null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    giayPhep.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                else
                                    giayPhep.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);

                                giayPhep.NamTiepNhan = giayPhep.NgayTiepNhan.Year;
                                noidung = "Hải quan cấp số tiếp nhận\r\nSố tiếp nhận: " + giayPhep.SoTiepNhan + "\r\nNgày tiếp nhận:" + giayPhep.NgayTiepNhan.ToLongTimeString();
                                e.FeedBackMessage.XmlSaveMessage(giayPhep.ID, MessageTitle.KhaiBaoBoSungGiayPhepDuocCapSoTN, noidung);
                                giayPhep.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(giayPhep.ID, MessageTitle.KhaiBaoBoSungGiayPhepDuocChapNhan, noidung);
                                giayPhep.TrangThai = TrangThaiXuLy.DA_DUYET;
                                noidung = "Giấy phép đã được duyệt.\n Thông tin trả về từ hải quan: " + noidung;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(giayPhep.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }
                    if (isUpdate)
                    {
                        giayPhep.Update();
                        SetButtonStateGIAYPHEP(TKMD, giayPhep);
                    }
                    msgInfor = noidung;
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion
    }
}

