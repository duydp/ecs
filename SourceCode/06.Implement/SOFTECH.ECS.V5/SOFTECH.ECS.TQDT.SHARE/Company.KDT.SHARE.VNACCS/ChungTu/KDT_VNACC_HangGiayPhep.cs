using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class KDT_VNACC_HangGiayPhep
    {
        public static List<KDT_VNACC_HangGiayPhep> SelectCollectionDynamic(string whereCondition, string orderByExpression, EGiayPhep giayPhepType)
        {
            string where = string.Format("{0} and GiayPhepType = '{1}'", whereCondition != "" ? whereCondition : "1 = 1", giayPhepType.ToString());
            IDataReader reader = SelectReaderDynamic(where, orderByExpression);
            return ConvertToCollection(reader);
        }

    }
}