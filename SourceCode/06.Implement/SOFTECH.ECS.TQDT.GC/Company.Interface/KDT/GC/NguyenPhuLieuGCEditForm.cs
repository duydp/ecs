﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;

namespace Company.Interface.KDT.GC
{
    public partial class NguyenPhuLieuGCEditForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public NguyenPhuLieu npl = new NguyenPhuLieu();
        public bool isBrower = false;
        public NguyenPhuLieuGCEditForm()
        {
            InitializeComponent();            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void NguyenPhuLieuGCEditForm_Load(object sender, EventArgs e)
        {
            revMaHS.ValidationExpression = @"\d{8}";

            if (!isBrower)
            {
                txtMa.Focus();
                this._DonViTinh = DonViTinh.SelectAll();
                cbDonViTinh.DataSource = this._DonViTinh;
                cbDonViTinh.DisplayMember = "Ten";
                cbDonViTinh.ValueMember = "ID";
                cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
                txtMa.Text = npl.Ma.Trim();
                txtMaHS.Text = npl.MaHS.Trim();
                txtSoLuong.Text = npl.SoLuongDangKy.ToString();
                txtTen.Text = npl.Ten.Trim();
                txtDonGia.Text = npl.DonGia.ToString();
                if (npl.Ma != "")
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                if (this.OpenType == OpenFormType.Edit)
                {
                    ;
                }
                else
                {
                    btnAdd.Enabled = false;
                    btnXoa.Enabled = false;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                }
            }
            else
            {
                uiGroupBox2.Visible = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                this.Width = dgList.Width;
                this.Height = dgList.Height;
                btnAdd.Visible = false;
                btnClose.Visible = false;
                HD.Load();
                HD.LoadCollection();
            }
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            dgList.DataSource = HD.NPLCollection;

            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtDonGia.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;

            dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["DonGia"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {

            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                error.SetIconPadding(txtMaHS, -8);
                error.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                txtMaHS.Focus();
            }
            else
            {
                error.SetError(txtMaHS, null);
            }

        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtMaHS, null);
            error.SetError(txtTen, null);

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {
                //if (!MaHS.Validate(txtMaHS.Text.Trim(), 10))
                //{
                //    error.SetIconPadding(txtMaHS, -8);
                //    error.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                //    return;
                //}


                checkExitsNPLAndSTTHang();
            }
        }
        private void reset()
        {
            txtMa.Text = "";
            txtMaHS.Text = "";
            txtSoLuong.Text = "0";
            txtTen.Text = "";
            txtDonGia.Text = "0";

            error.SetError(txtMaHS, null);
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            npl = new NguyenPhuLieu();

        }
        private void checkExitsNPLAndSTTHang()
        {
            HD.NPLCollection.Remove(npl);
            foreach (NguyenPhuLieu nguyenPL in HD.NPLCollection)
            {
                if (nguyenPL.Ma.Trim().ToUpper() == txtMa.Text.Trim().ToUpper())
                {
                    showMsg("MSG_2702057");
                    //MLMessages("Đã có nguyên phụ liệu này trong danh sách.","MSG_WRN06","",false);
                    if (npl.Ma != "")
                        HD.NPLCollection.Add(npl);
                    return;
                }
            }
            npl.SoLuongDangKy = Convert.ToDecimal(txtSoLuong.Text);
            npl.Ten = txtTen.Text.Trim();
            npl.Ma = txtMa.Text.Trim();
            npl.MaHS = txtMaHS.Text.Trim();
            npl.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            npl.HopDong_ID = HD.ID;
            npl.DonGia = Convert.ToDecimal(txtDonGia.Text);
            HD.NPLCollection.Add(npl);
            reset();
            this.setErro();
        }




        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            npl = (NguyenPhuLieu)e.Row.DataRow;
            if (!isBrower)
            {
                txtMa.Text = npl.Ma.Trim();
                txtMaHS.Text = npl.MaHS.Trim();
                txtSoLuong.Text = npl.SoLuongDangKy.ToString();
                txtTen.Text = npl.Ten.Trim();
                cbDonViTinh.SelectedValue = npl.DVT_ID;
            }
            else
            {
                this.Close();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.NguyenPhuLieu nplDelete = (Company.GC.BLL.KDT.GC.NguyenPhuLieu)row.GetRow().DataRow;
                    if (NguyenPhuLieu.CheckExitNPLinDinhMuc(nplDelete.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI, this.HD.ID))
                    {
                        if (showMsg("MSG_0203077", nplDelete.Ma, true) != "Yes")
                        {
                            e.Cancel = true;
                            return;
                        }
                        else
                            continue;
                    }
                    nplDelete.Delete();
                }
            }
            else
            {
                e.Cancel = true;
            }
        }


        private void txtSoLuong_Leave(object sender, EventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                Company.GC.BLL.KDT.GC.NguyenPhuLieuCollection NPLCollectionTMP = new NguyenPhuLieuCollection();
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.NguyenPhuLieu nplDelete = (Company.GC.BLL.KDT.GC.NguyenPhuLieu)row.GetRow().DataRow;
                    try
                    {
                        if (NguyenPhuLieu.CheckExitNPLinDinhMuc(nplDelete.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI, this.HD.ID))
                        {
                            if (ShowMessage("nguyên phụ liệu " + nplDelete.Ma + " đã khai định mức. Bạn có muốn xóa không", true) != "Yes")
                            {
                                return;
                            }
                            else
                                continue;
                        }
                        nplDelete.Delete();
                        NPLCollectionTMP.Add(nplDelete);
                    }
                    catch { }
                }
                foreach (NguyenPhuLieu NPL in NPLCollectionTMP)
                    HD.NPLCollection.Remove(NPL);

                dgList.DataSource = HD.NPLCollection;
                this.setErro();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }
    }
}