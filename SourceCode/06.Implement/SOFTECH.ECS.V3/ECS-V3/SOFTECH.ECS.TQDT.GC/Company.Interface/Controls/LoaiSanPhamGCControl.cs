using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface;

namespace Company.Interface.Controls
{
    public partial class LoaiSanPhamGCControl : UserControl
    {
        public LoaiSanPhamGCControl()
        {
            this.InitializeComponent();
        }
        DataTable dt;
        public string Ma
        {
            set
            {
                this.txtMa.Text = value;
                this.cbTen.Value = this.txtMa.Text.Trim();
            }
            get { return this.txtMa.Text; }
        }
        public string Ten
        {
            get { return this.cbTen.Text; }
        }
        public bool ReadOnly
        {
            set
            {
                this.cbTen.ReadOnly = value;
                this.txtMa.ReadOnly = value;
            }
            get { return this.cbTen.ReadOnly; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.cbTen.VisualStyleManager = value;
                this.txtMa.VisualStyleManager = value;
            }
            get
            {
                return this.txtMa.VisualStyleManager;
            }
        }
        public int SelectIndex
        {
            set { cbTen.SelectedIndex = value; }
            get { return cbTen.SelectedIndex; }
        }
        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);       

        private void loadData()
        {            
           
            dt = Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham.SelectAll().Tables[0];
            cbTen.DataSource = dt;
            cbTen.ValueMember = "MaSanPham";
            cbTen.DisplayMember = "TenSanPham";
            cbTen.SelectedIndex = 0;                 
        }

        private void DonViHaiQuanControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.loadData();
            }
        }

        private void cbTen_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtMa.Text = this.cbTen.Value.ToString().Trim();
                if (this.ValueChanged != null) this.ValueChanged(sender, e);
            }
            catch { }
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {
            this.txtMa.Text = this.txtMa.Text.Trim();
            if (dt.Select("MaSanPham='" + txtMa.Text.Trim() + "'").Length == 0)
            {
                cbTen.SelectedIndex = cbTen.SelectedIndex + 1;
                return;
            }
            this.cbTen.Value = this.txtMa.Text.Trim();
        }
    }
}