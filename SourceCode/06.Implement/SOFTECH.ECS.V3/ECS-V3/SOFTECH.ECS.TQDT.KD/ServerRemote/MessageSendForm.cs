﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using System.Threading;
using SignRemote;
using System.Data.SqlClient;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.Interface.VNACCS;
using System.Xml;
using Company.Interface;

namespace ServerRemote
{
    public partial class MessageSendForm : Company.Interface.BaseForm
    {
        public static bool IsAbort = false;
        public ISignMessage _service = WebService.SignMessage();
        private bool _isSignLocal = true;
        private DateTime _lastStartTime;
        private List<MessageSend> _queueMessages = new List<MessageSend>();
        private bool _flagSign = false;
        public bool isSignOnLan;
        public string msgFeedBack;
        public MessageSendForm()
        {
            InitializeComponent();
        }
        private string GetSearchWhere()
        {
            string where = " 1 = 1";
            if (!String.IsNullOrEmpty(txtNguoiGui.Text.ToString().Trim()))
                where += " AND MSG_FROM LIKE '%" + txtNguoiGui.Text.ToString().Trim() + "%'";
            DateTime dateFrom = clcTuNgay.Value;
            DateTime dateTo = clcDenNgay.Value;

            where += " AND CREATE_TIME BETWEEN '" + dateFrom.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + dateTo.ToString("yyyy-MM-dd 23:59:59") + "'";
            return where;
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = MSG_INBOX.SelectCollectionDynamic(GetSearchWhere(), "ID");
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void DoProcess()
        {
            if (!_isSignLocal)
                DoGetContent();
            else
                DoGetContentLocal();

        }
        private void DoGetContent()
        {
            try
            {
                //_service = WebService.SignMessage();
                MessageSend msgSend = new MessageSend()
                {
                    From = Globals.UserNameLogin,
                    Password = Globals.PasswordLogin,
                    SendStatus = SendStatusInfo.Request
                };
                string MSG = Helpers.Serializer(msgSend);
                string msgRequest = _service.ServerSend(Helpers.Serializer(msgSend));
                msgSend = Helpers.Deserialize<MessageSend>(msgRequest);

                if (msgSend.SendStatus == SendStatusInfo.NoSign || msgSend.SendStatus == SendStatusInfo.NoSignVNACCS)
                {
                    msgSend.TypeMessages = msgSend.SendStatus == SendStatusInfo.NoSignVNACCS ? "VNACCS" : string.Empty;
                    _queueMessages.Add(msgSend);
                    if (!_flagSign)
                    {
                        ThreadPool.QueueUserWorkItem(DoSignContent);
                        Thread.Sleep(new TimeSpan(0, 0, 5));
                    }

                }
                else
                    Thread.Sleep(new TimeSpan(0, 0, 5));

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            #region DisConnect to server

            MessageSend msg = new MessageSend()
            {
                From = Globals.UserNameLogin,
                Password = Globals.PasswordLogin,
                SendStatus = SendStatusInfo.ServerLogout
            };
            _service = WebService.SignMessage();
            _service.ServerSend(Helpers.Serializer(msg));
            #endregion
        }
        private void DoGetContentLocal()
        {
            try
            {
                #region Code
                List<MSG_INBOX> MSG_INBOX_Collection = MSG_INBOX.SelectCollectionAll();
                foreach (MSG_INBOX INBOX in MSG_INBOX_Collection)
                {
                    if (INBOX != null)
                    {
                        MessageSend msgSend = new MessageSend()
                        {
                            From = Globals.UserNameLogin,
                            Password = Globals.PasswordLogin,
                            SendStatus = SendStatusInfo.Request,
                            Message = new SignRemote.Message()
                            {
                                Content = INBOX.MSG_ORIGIN,
                                GuidStr = INBOX.GUIDSTR,
                                From = INBOX.MSG_FROM
                            },
                            TypeMessages = INBOX.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
                        };
                        _queueMessages.Add(msgSend);
                        if (!_flagSign)
                        {
                            ThreadPool.QueueUserWorkItem(DoSignContent);
                            Thread.Sleep(new TimeSpan(0, 0, 5));
                        }
                    }
                    else
                        Thread.Sleep(new TimeSpan(0, 0, 5));
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }


        private void DoSignContent(object obj)
        {
            _flagSign = _queueMessages.Count != 0;
            while (!IsAbort && _flagSign)
            {
                try
                {
                    bool isSignVNACCS = false;
                    int index = 0;
                    MessageSend msgSend = _queueMessages[index];
                    this._lastStartTime = DateTime.Now;

                    #region SignData
                    isSignVNACCS = msgSend.TypeMessages == "VNACCS";

                    string content;
                    MessageSignature signInfo;
                    if (!isSignVNACCS)
                    {
                        content = msgSend.Message.Content;
                        //Kiểm tra Content có phải là Base64String
                        if (content.Length % 4 == 0)
                        {
                            if (!content.TrimStart().StartsWith("<"))
                            {
                                content = Helpers.ConvertFromBase64VNACCS(msgSend.Message.Content);
                            }
                        }
                        signInfo = Helpers.GetSignature(content);
                    }
                    else
                    {
                        content = Helpers.ConvertFromBase64VNACCS(msgSend.Message.Content);
                        System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = null;
                        if (!string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName))
                        {
                            x509Certificate2 = Company.KDT.SHARE.Components.Cryptography.GetStoreX509Certificate2New(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName);
                        }
                        else
                        {
                            msgFeedBack = "Chưa cấu hình chữ ký số để thực hiện ký số . Doanh nghiệp hãy cấu hình chữ ký số để thực hiện ký số";
                            MessageBoxControl.ShowMessage(msgFeedBack);
                            IsAbort = true;
                            return;
                        }
                        if (x509Certificate2 == null)
                        {
                            msgFeedBack = "Không tìm thấy chữ ký số . Doanh nghiệp hãy kiểm tra lại Thiết bị USB chữ ký số ";
                            MessageBoxControl.ShowMessage(msgFeedBack);
                            IsAbort = true;
                            return;
                        }
                        signInfo = new MessageSignature()
                        {
                            Data = SignRemoteVNACCS.SignVNACCS.signEdi(content, x509Certificate2, Globals.PasswordSign),
                        };
                    }

                    if (!_isSignLocal)
                    {
                        MessageSend msgReturn = new MessageSend()
                        {
                            From = Globals.UserNameLogin,
                            Password = Globals.PasswordLogin,
                            To = msgSend.From,
                            SendStatus = SendStatusInfo.Send,
                            Message = new SignRemote.Message()
                            {
                                SignData = signInfo.Data,
                                SignCert = signInfo.FileCert,
                                GuidStr = msgSend.Message.GuidStr
                            }
                        };
                        _service.ServerSend(Helpers.Serializer(msgReturn));
                        #region Comment
                        if (this.InvokeRequired)
                            this.Invoke(new MethodInvoker(delegate
                            {
                                PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ : '{0}'", msgReturn.From));
                            }));
                        else
                        {
                            PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ : '{0}'", msgReturn.From));
                        }  
                        #endregion 
                        
                    }
                    else
                    {
                        #region Code New
                        MSG_OUTBOX MSG_OUTBOX = new MSG_OUTBOX();
                        MSG_OUTBOX.MSG_FROM = msgSend.From;
                        MSG_OUTBOX.MSG_TO = msgSend.To;
                        MSG_OUTBOX.CREATE_TIME = DateTime.Now;
                        // 1 : VNACCS ; 0 : V5
                        MSG_OUTBOX.STATUS = msgSend.TypeMessages == "VNACCS" ? 1 : 0;
                        MSG_OUTBOX.MSG_ORIGIN = msgSend.Message.Content;
                        MSG_OUTBOX.GUIDSTR = msgSend.Message.GuidStr;
                        MSG_OUTBOX.MSG_SIGN_DATA = signInfo.Data;
                        MSG_OUTBOX.MSG_SIGN_CERT = signInfo.FileCert;
                        MSG_OUTBOX.InsertUpdate();

                        MSG_INBOX.DeleteDynamic("GUIDSTR = '" + MSG_OUTBOX.GUIDSTR + "'");
                        #region Comment
                        if (this.InvokeRequired)
                            this.Invoke(new MethodInvoker(delegate
                            {
                                PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ : '{0}'", MSG_OUTBOX.MSG_FROM));
                            }));
                        else
                        {
                            PopUpThongBao(string.Format("Đã ký thông tin yêu cầu từ : '{0}'", MSG_OUTBOX.MSG_FROM));
                        } 
                        #endregion  
                        //if (f == null)
                        //{
                        //    f = new MainForm();
                        //}
                        //setNotifyIcon(string.Format("Đã ký thông tin yêu cầu từ '{0}'", MSG_OUTBOX.MSG_FROM));
                        #endregion
                        //OUTBOX.InsertDataSignLocal(msgSend.Message.GuidStr, signInfo.Data, signInfo.FileCert);
                        #region
                    }
                        #endregion
                    _queueMessages.RemoveAt(index);
                    if (_queueMessages.Count == 0) _flagSign = false;
                    #endregion
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }


        }
        public void GetNewFileClientSend()
        {
            try
            {
                DataTable dt = _service.GetNewFileClientSend(Globals.UserNameLogin);
                if (dt != null && dt.Rows.Count >= 1)
                {
                    FileSendForm f = new FileSendForm();
                    f._isSignLocal = false;
                    f.DoProcessFile();
                    #region Comment
                    //Thread.Sleep(5000);
                    //string contentThongBao = f.contentThongBao;
                    //if (this.InvokeRequired)
                    //    this.Invoke(new MethodInvoker(delegate
                    //    {
                    //        PopUpThongBao(contentThongBao);
                    //    }));
                    //else
                    //{
                    //    PopUpThongBao(contentThongBao);
                    //}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void GetNewMessageClientSend()
        {
            try
            {
                DataTable dt = _service.GetNewMessageClientSend(Globals.UserNameLogin);
                if (dt != null && dt.Rows.Count >= 1)
                {
                    _isSignLocal = false;
                    DoProcess();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void GetNewMessageSend()
        {
            try
            {
                List<INBOX> InboxCollection = INBOX.SelectCollectionAll();
                if (InboxCollection.Count >= 1)
                {
                    DoProcess();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void GetNewFileSend()
        {
            try
            {
                List<MSG_FILE_INBOX> InboxCollection = MSG_FILE_INBOX.SelectCollectionAll();
                if (InboxCollection.Count >= 1)
                {
                    FileSendForm f = new FileSendForm();
                    f.DoProcessFile();
                    #region Comment
                    //Thread.Sleep(2000);
                    //string contentThongBao = f.contentThongBao;
                    //if (this.InvokeRequired)
                    //    this.Invoke(new MethodInvoker(delegate
                    //    {
                    //        PopUpThongBao(contentThongBao);
                    //    }));
                    //else
                    //{
                    //    PopUpThongBao(contentThongBao);
                    //}
                    #endregion
  
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void MessageSendForm_Load(object sender, EventArgs e)
        {
            isSignOnLan = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsSignOnLan").ToString());
            clcTuNgay.Value = DateTime.Now;
            clcDenNgay.Value = DateTime.Now;

            //timer1.Interval = 2000;
            //timer1.Enabled = true;
            //timer1_Tick(null, null);

            //timer1.Tick += new EventHandler(timer1_Tick);
        }
        private void setNotifyIcon(string text)
        {
            notifyIcon1.Visible = true;
            string sfmtCurr = notifyIcon1.BalloonTipText;
            notifyIcon1.BalloonTipText = text;
            notifyIcon1.ShowBalloonTip(500);
            notifyIcon1.BalloonTipText = sfmtCurr;
        }
        public void PopUpThongBao(string contentThongBao)
        {
            popupNotifier1.TitleText = "THÔNG BÁO TỪ ECS-SIGNREMOTE";
            popupNotifier1.ContentText = contentThongBao;
            popupNotifier1.ShowCloseButton = true;
            popupNotifier1.ShowOptionsButton = false;
            popupNotifier1.ShowGrip = false;
            popupNotifier1.Delay = 3000;
            popupNotifier1.AnimationInterval = 10;
            popupNotifier1.AnimationDuration = 1000;
            popupNotifier1.Popup();
        }
        private void MessageSendForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<MSG_INBOX> ItemColl = new List<MSG_INBOX>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ItemColl.Add((MSG_INBOX)i.GetRow().DataRow);
                    }

                }
                foreach (MSG_INBOX item in ItemColl)
                {
                    if (item.ID > 0)
                        item.Delete();
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {            
            //if (!isSignOnLan)
            //{
            //    GetNewMessageClientSend();
            //    GetNewFileClientSend();
            //}
            //else
            //{
            //    GetNewMessageSend();
            //    GetNewFileSend();
            //}
        }

        private void grList_ColumnButtonClick(object sender, ColumnActionEventArgs ev)
        {
            string ColumnName = ev.Column.ButtonText.ToString();
            switch (ColumnName)
            {
                case "Xem":
                    try
                    {
                        GridEXSelectedItem e = grList.SelectedItems[0];
                        if (e.GetRow().RowType == Janus.Windows.GridEX.RowType.Record)
                        {
                            MSG_INBOX msg = new MSG_INBOX();
                            msg = (MSG_INBOX)e.GetRow().DataRow;
                            string MsgXML = "";
                            string MsgEDI = "";
                            if (msg != null)
                            {
                                if (msg.STATUS == 50)
                                {
                                    if (msg.MSG_ORIGIN.Length % 4 == 0)
                                    {
                                        if (!msg.MSG_ORIGIN.TrimStart().StartsWith("<"))
                                        {
                                            MsgEDI = Helpers.ConvertFromBase64VNACCS(msg.MSG_ORIGIN);
                                        }
                                        else
                                        {
                                            MsgEDI = msg.MSG_ORIGIN;
                                        }
                                    }
                                    else
                                    {
                                        MsgEDI = msg.MSG_ORIGIN;
                                    }
                                    ShowMessagesEDI f = new ShowMessagesEDI(MsgEDI);
                                    f.ShowDialog(this);
                                }
                                else
                                {
                                    if (msg.MSG_ORIGIN.Length % 4 == 0)
                                    {
                                        if (!msg.MSG_ORIGIN.TrimStart().StartsWith("<"))
                                        {
                                            MsgXML = Helpers.ConvertFromBase64VNACCS(msg.MSG_ORIGIN);
                                        }
                                        else
                                        {
                                            MsgXML = msg.MSG_ORIGIN;
                                        }
                                    }
                                    else
                                    {
                                        MsgXML = msg.MSG_ORIGIN;
                                    } 
                                    XmlDocument doc = new XmlDocument();
                                    doc.LoadXml(MsgXML);
                                    ViewMessagesForm f = new ViewMessagesForm();
                                    f.doc = doc;
                                    f.ShowDialog(this);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);

                    }
                    break;
                case "Ký":
                    try
                    {
                        GridEXSelectedItem e = grList.SelectedItems[0];
                        if (e.GetRow().RowType == Janus.Windows.GridEX.RowType.Record)
                        {
                            MSG_INBOX msg = new MSG_INBOX();
                            msg = (MSG_INBOX)e.GetRow().DataRow;
                            if (msg != null)
                            {
                                MessageSend msgSend = new MessageSend()
                                {
                                    From = Globals.UserNameLogin,
                                    Password = Globals.PasswordLogin,
                                    SendStatus = SendStatusInfo.Request,
                                    Message = new SignRemote.Message()
                                    {
                                        Content = msg.MSG_ORIGIN,
                                        GuidStr = msg.GUIDSTR,
                                        From = msg.MSG_FROM
                                    },
                                    TypeMessages = msg.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
                                };
                                _queueMessages.Add(msgSend);
                                if (!_flagSign)
                                {
                                    ThreadPool.QueueUserWorkItem(DoSignContent);
                                }
                            }
                            btnSearch_Click(null, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    break;
                case "Từ chối":
                    break;
                default:
                    break;
            }
        }

        private void grList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    MSG_INBOX msg = new MSG_INBOX();
                    msg = (MSG_INBOX)e.Row.DataRow;
                    string MsgXML = "";
                    string MsgEDI = "";
                    if (msg != null)
                    {
                        if (msg.STATUS == 50)
                        {
                            if (msg.MSG_ORIGIN.Length % 4 == 0)
                            {
                                if (!msg.MSG_ORIGIN.TrimStart().StartsWith("<"))
                                {
                                    MsgEDI = Helpers.ConvertFromBase64VNACCS(msg.MSG_ORIGIN);
                                }
                                else
                                {
                                    MsgEDI = msg.MSG_ORIGIN;
                                }
                            }
                            else
                            {
                                MsgEDI = msg.MSG_ORIGIN;
                            }
                            ShowMessagesEDI f = new ShowMessagesEDI(MsgEDI);
                            f.ShowDialog(this);
                        }
                        else
                        {
                            if (msg.MSG_ORIGIN.Length % 4 == 0)
                            {
                                if (!msg.MSG_ORIGIN.TrimStart().StartsWith("<"))
                                {
                                    MsgXML = Helpers.ConvertFromBase64VNACCS(msg.MSG_ORIGIN);
                                }
                                else
                                {
                                    MsgXML = msg.MSG_ORIGIN;
                                }
                            }
                            else
                            {
                                MsgXML = msg.MSG_ORIGIN;
                            }
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(MsgXML);
                            ViewMessagesForm f = new ViewMessagesForm();
                            f.doc = doc;
                            f.ShowDialog(this);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                string TrangThai = e.Row.Cells["STATUS"].Value.ToString();
                switch (TrangThai)
                {
                    case "50":
                        e.Row.Cells["STATUS"].Text = "VNACCS";
                        break;
                    case "-1":
                        e.Row.Cells["STATUS"].Text = "V5";
                        break;
                }
            }
            catch (Exception )
            {
            }
        }
    }
}
