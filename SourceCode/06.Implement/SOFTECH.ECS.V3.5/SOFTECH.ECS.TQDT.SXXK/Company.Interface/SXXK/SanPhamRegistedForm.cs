using System;
using System.Data;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.Report.SXXK;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;

namespace Company.Interface.SXXK
{
    public partial class SanPhamRegistedForm : BaseForm
    {
        public Company.BLL.SXXK.SanPham SanPhamSelected = new Company.BLL.SXXK.SanPham();
        public Company.BLL.SXXK.SanPhamCollection SPCollection = new Company.BLL.SXXK.SanPhamCollection();
        public Company.BLL.SXXK.SanPhamCollection SanPhamsCollectionSelected = new Company.BLL.SXXK.SanPhamCollection();

        public Company.BLL.KDT.SXXK.SanPham SPDetail = new Company.BLL.KDT.SXXK.SanPham();
        public Company.BLL.KDT.SXXK.SanPhamCollection SanPhamCollection = new Company.BLL.KDT.SXXK.SanPhamCollection();
        public SanPhamDangKy spDangKy = new SanPhamDangKy();

        public bool events = false;
        private DataSet dsRegistedList;
        public string whereCondition;
        public bool isBrowser = false;
        public SanPhamRegistedForm()
        {
            InitializeComponent();

        }

        public void BindData()
        {


        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            try
            {
                // Đơn vị tính.
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                // Đơn vị Hải quan.
                this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
                ctrCoQuanHQ.Ma = GlobalSettings.MA_HAI_QUAN;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý :\r\n" + ex.Message, false);
            }

        }

        //-----------------------------------------------------------------------------------------

        private void setDataToComboUserKB()
        {
            try
            {
                DataTable dt = Company.QuanTri.User.SelectAll().Tables[0];
                DataRow dr = dt.NewRow();
                dr["USER_NAME"] = -1;
                dr["HO_TEN"] = "--Tất cả--";
                dt.Rows.InsertAt(dr, 0);
                cbUserKB.DataSource = dt;
                cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
                cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------

        private void SanPhamRegistedForm_Load(object sender, EventArgs e)
        {
            try
            {
                lblTongSP.Text = "0";
                //btnExportExcel.Visible = true;

                this.khoitao_DuLieuChuan();
                setDataToComboUserKB();
                cbUserKB.SelectedIndex = 0;
                // Sản phẩm đã đăng ký.
                dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;
                this.search();
                // Doanh nghiệp / Đại lý TTHQ.
                if (MainForm.versionHD == 0)
                {
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.CapNhatDuLieu)))
                    {
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        //uiButton3.Visible = false;
                        //btnDelete.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------



        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string maSP = e.Row.Cells["Ma"].Text;
                    this.SanPhamSelected.Ma = maSP;
                    this.SanPhamSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SanPhamSelected.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.SanPhamSelected.Ten = e.Row.Cells["Ten"].Text;
                    this.SanPhamSelected.MaHS = e.Row.Cells["MaHS"].Text;
                    this.SanPhamSelected.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                    this.SanPhamSelected.Load();
                    Company.Interface.KDT.SXXK.SanPhamEditForm f = new Company.Interface.KDT.SXXK.SanPhamEditForm();
                    SPDetail.Ma = maSP;
                    SPDetail.Ten = e.Row.Cells["Ten"].Text;
                    SPDetail.MaHS = e.Row.Cells["MaHS"].Text;
                    SPDetail.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                    f.SPDetail = SPDetail;
                    f.SPCollection = SanPhamCollection;
                    f.spDangKy = spDangKy;
                    f.OpenType = OpenFormType.View;
                    if (this.CalledForm != "HangMauDichForm")
                    {
                        f.ShowDialog(this);
                    }
                    else
                    {
                        this.Close();
                    }
                }
                events = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private Company.BLL.SXXK.SanPhamCollection MuiltiSellect()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {

                    this.SanPhamsCollectionSelected.Add((Company.BLL.SXXK.SanPham)i.GetRow().DataRow);

                }
            }
            events = true;
            return SanPhamsCollectionSelected;
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                ImportSPForm f = new ImportSPForm();
                f.SPCollection = this.SPCollection;
                f.ShowDialog(this);
                BindData();
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            //this.MuiltiSellect();
            //this.Close();
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.BindData();
            dgList.Refetch();
            this.Cursor = Cursors.Default;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                // if (ShowMessage("Doanh nghiệp có muốn xóa các sản phẩm này không?", true) == "Yes")
                if (MLMessages("Doanh nghiệp có muốn xóa các sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            Company.BLL.SXXK.SanPham hmd = (Company.BLL.SXXK.SanPham)i.GetRow().DataRow;
                            hmd.Delete();
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                SanPhamEditForm f = new SanPhamEditForm();
                f.ShowDialog(this);
                search();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            try
            {
                SelectSanPhamForm f = new SelectSanPhamForm();
                f.ShowDialog(this);
                //ReportSanPham f2 = new ReportSanPham();
                ReportSanPham_TT128_2013 f2 = new ReportSanPham_TT128_2013();
                if (f.spCollectionSelect.Count > 0)
                {
                    f2.SPCollection = f.spCollectionSelect;
                    f2.BindReportDinhMucDaDangKy();
                    f2.ShowPreview();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void uiButton2_Click_1(object sender, EventArgs e)
        {
            try
            {
                ImportSPForm f = new ImportSPForm();
                f.SPCollection = this.SPCollection;
                f.ShowDialog(this);
                this.search();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void uiGroupBox4_Click(object sender, EventArgs e)
        {

        }
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = " MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + ctrCoQuanHQ.Ma + "'";
            if (txtMaSP.Text.Trim().Length > 0)
            {
                where += " AND Ma Like '%" + txtMaSP.Text.Trim() + "%'";
            }

            if (txtTenSP.Text.Trim().Length > 0)
            {
                where += " AND Ten Like '%" + txtTenSP.Text.Trim() + "%'";
            }
            if (isBrowser)
            {
                string whereConditionSearch = "";
                where = " MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + ctrCoQuanHQ.Ma + "'";
                whereConditionSearch = whereCondition;
                if (txtMaSP.Text.Trim().Length > 0)
                {
                    where += " AND Ma Like '%" + txtMaSP.Text.Trim() + "%'";
                }

                if (txtTenSP.Text.Trim().Length > 0)
                {
                    where += " AND Ten Like '%" + txtTenSP.Text.Trim() + "%'";
                }
                if (!String.IsNullOrEmpty(where))
                {
                    whereConditionSearch += where;
                }
                this.SPCollection = new Company.BLL.SXXK.SanPham().SelectCollectionDynamic(whereConditionSearch, " Ma");
                dgList.DataSource = this.SPCollection;

                lblTongSP.Text = SPCollection.Count.ToString();
            }
            else
            {
                this.SPCollection = new Company.BLL.SXXK.SanPham().SelectCollectionDynamic(where, " Ma");
                dgList.DataSource = this.SPCollection;

                lblTongSP.Text = SPCollection.Count.ToString();
            }
        }

        private DataTable dtSP;
        private void searchNguoiKhaiBao(string userName)
        {
            if (userName.Equals("-1"))
            {
                search();
            }
            else
            {
                dtSP = new Company.BLL.SXXK.SanPham().GetSPFromUserName(userName).Tables[0];
                dgList.DataSource = dtSP;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //search();
            try
            {
                if (cbUserKB.SelectedItem != null)
                    searchNguoiKhaiBao(cbUserKB.SelectedItem.Value.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count <= 0) return;
                //if (ShowMessage("Doanh nghiệp có muốn xóa các sản phẩm này không?", true) == "Yes")
                if (MLMessages("Doanh nghiệp có muốn xóa các sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            Company.BLL.SXXK.SanPham hmd = (Company.BLL.SXXK.SanPham)i.GetRow().DataRow;
                            hmd.Delete();
                        }
                    }
                    this.search();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "SanPhamDaDangKy_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.ShowDialog(this);

                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Doanh nghiệp có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdDelete":
                    btnDelete_Click(null, null);
                    break;
                case "cmdExportExcel":
                    btnExportExcel_Click(null, null);
                    break;
                case "cmdPrint":
                    uiButton4_Click(null, null);
                    break;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    Company.BLL.SXXK.SanPham sanPhamSelect = (Company.BLL.SXXK.SanPham)item.DataRow;
                    SanPhamsCollectionSelected.Add(sanPhamSelect);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
    }
}