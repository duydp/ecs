﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class VNACC_GiayPhep_SAA_HangForm : BaseFormHaveGuidPanel
    {
        private KDT_VNACC_HangGiayPhep HangGiayPhep = null;
        public KDT_VNACC_GiayPhep_SAA GiayPhep_SAA;
        private EGiayPhep GiayPhepType = EGiayPhep.SAA;
        private bool isAddNew = true;

        private DataTable dtHS = new DataTable();

        public VNACC_GiayPhep_SAA_HangForm()
        {
            InitializeComponent();

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_OGAProcedure.SAA.ToString());
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateForm(false))
                    return;

                if (isAddNew)
                {
                    HangGiayPhep = new KDT_VNACC_HangGiayPhep();

                    GetHangGiayPhep(HangGiayPhep);

                    GiayPhep_SAA.HangCollection.Add(HangGiayPhep);
                }
                else
                {
                    GetHangGiayPhep(HangGiayPhep);
                }

                grdHang.DataSource = GiayPhep_SAA.HangCollection;
                grdHang.Refetch();

                HangGiayPhep = new KDT_VNACC_HangGiayPhep();
                SetHangGiayPhep(HangGiayPhep);
                isAddNew = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetHangGiayPhep(KDT_VNACC_HangGiayPhep hangGiayPhep)
        {
            hangGiayPhep.TenHangHoa = txtTenHangHoa.Text;
            hangGiayPhep.MaSoHangHoa = txtMaSoHangHoa.Text;
            hangGiayPhep.TinhBiet = cboTinhBiet.SelectedValue.ToString();
            hangGiayPhep.Tuoi = Convert.ToInt32(txtTuoi.Value);
            hangGiayPhep.NoiSanXuat = txtNoiSanXuat.Text;
            hangGiayPhep.QuyCachDongGoi = txtQuyCachDongGoi.Text;
            hangGiayPhep.TongSoLuongNhapKhau = Convert.ToInt32(txtTongSoLuongNhapKhau.Value);
            hangGiayPhep.DonViTinhTongSoLuongNhapKhau = ucDonViTinhTongSoLuongNhapKhau.Code;
            hangGiayPhep.KichCoCaThe = txtKichCoCaThe.Text;
            hangGiayPhep.TrongLuongTinh = Convert.ToDecimal(txtTrongLuongTinh.Value);
            hangGiayPhep.DonViTinhTrongLuong = ucDonViTinhTrongLuong.Code;
            hangGiayPhep.TrongLuongCaBi = Convert.ToDecimal(txtTrongLuongCaBi.Value);
            hangGiayPhep.DonViTinhTrongLuongCaBi = ucDonViTinhTrongLuongCaBi.Code;
            hangGiayPhep.SoLuongKiemDich = Convert.ToInt32(txtSoLuongKiemDich.Value);
            hangGiayPhep.DonViTinhSoLuongKiemDich = ucDonViTinhSoLuongKiemDich.Code;
            hangGiayPhep.LoaiBaoBi = txtLoaiBaoBi.Text;
            hangGiayPhep.MucDichSuDung = txtMucDichSuDung.Text;

            errorProvider.Clear();
        }

        private void SetHangGiayPhep(KDT_VNACC_HangGiayPhep hangGiayPhep)
        {
            errorProvider.Clear();

            txtTenHangHoa.Text = hangGiayPhep.TenHangHoa;
            txtMaSoHangHoa.Text = hangGiayPhep.MaSoHangHoa;
            cboTinhBiet.SelectedValue = hangGiayPhep.TinhBiet;
            txtTuoi.Value = hangGiayPhep.Tuoi;
            txtNoiSanXuat.Text = hangGiayPhep.NoiSanXuat;
            txtQuyCachDongGoi.Text = hangGiayPhep.QuyCachDongGoi;
            txtTongSoLuongNhapKhau.Value = hangGiayPhep.TongSoLuongNhapKhau;
            ucDonViTinhTongSoLuongNhapKhau.Code = hangGiayPhep.DonViTinhTongSoLuongNhapKhau;
            txtKichCoCaThe.Text = hangGiayPhep.KichCoCaThe;
            txtTrongLuongTinh.Value = hangGiayPhep.TrongLuongTinh;
            ucDonViTinhTrongLuong.Code = hangGiayPhep.DonViTinhTrongLuong;
            txtTrongLuongCaBi.Value = hangGiayPhep.TrongLuongCaBi;
            ucDonViTinhTrongLuongCaBi.Code = hangGiayPhep.DonViTinhTrongLuongCaBi;
            txtSoLuongKiemDich.Value = hangGiayPhep.SoLuongKiemDich;
            ucDonViTinhSoLuongKiemDich.Code = hangGiayPhep.DonViTinhSoLuongKiemDich;
            txtLoaiBaoBi.Text = hangGiayPhep.LoaiBaoBi;
            txtMucDichSuDung.Text = hangGiayPhep.MucDichSuDung;
        }

        private void VNACC_GiayPhep_SAA_HangForm_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SetIDControl();

                SetMaxLengthControl();

                Khoitao_DuLieuChuan();

                txtTenHangHoa.AcceptsTab = false;

                grdHang.DataSource = GiayPhep_SAA.HangCollection;

                ValidateForm(true);

                SetAutoRemoveUnicodeAndUpperCaseControl();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void Khoitao_DuLieuChuan()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                dtHS = MaHS.SelectAll();
                foreach (DataRow dr in dtHS.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaSoHangHoa.AutoCompleteCustomSource = col;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                List<KDT_VNACC_HangGiayPhep> hangColl = new List<KDT_VNACC_HangGiayPhep>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hangColl.Add((KDT_VNACC_HangGiayPhep)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangGiayPhep hmd in hangColl)
                    {
                        if (hmd.ID > 0)
                            hmd.Delete();
                        GiayPhep_SAA.HangCollection.Remove(hmd);
                    }

                    grdHang.DataSource = GiayPhep_SAA.HangCollection;
                    try
                    {
                        grdHang.Refetch();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdHang_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                // List<KDT_VNACC_HangGiayPhep> hangColl = new List<KDT_VNACC_HangGiayPhep>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                HangGiayPhep = (KDT_VNACC_HangGiayPhep)items[0].GetRow().DataRow;
                SetHangGiayPhep(HangGiayPhep);
                isAddNew = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void txtMaSoHangHoa_Leave(object sender, EventArgs e)
        {
            try
            {
                //string MoTa = MaHS.CheckExist(txtMaSoHangHoa.Text);
                //if (MoTa == "")
                //{
                //    txtMaSoHangHoa.Focus();
                //    epError.SetIconPadding(txtMaSoHangHoa, -8);
                //    epError.SetError(txtMaSoHangHoa, setText("Mã HS không có trong danh mục mã HS.", "This HS is not exist"));
                //}
                //else
                //{
                //    epError.SetError(txtMaSoHangHoa, string.Empty);
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtTenHangHoa.Tag = "GDA";
                txtMaSoHangHoa.Tag = "HSC";
                cboTinhBiet.Tag = "SEX";
                txtTuoi.Tag = "AGE";
                txtNoiSanXuat.Tag = "PRC";
                txtQuyCachDongGoi.Tag = "PCM";
                txtTongSoLuongNhapKhau.Tag = "TQ";
                ucDonViTinhTongSoLuongNhapKhau.TagName = "TQU";
                txtKichCoCaThe.Tag = "SOG";
                txtTrongLuongTinh.Tag = "NW";
                ucDonViTinhTrongLuong.TagName = "NWU";
                txtTrongLuongCaBi.Tag = "GW";
                ucDonViTinhTrongLuongCaBi.TagName = "GWU";
                txtSoLuongKiemDich.Tag = "QQ";
                ucDonViTinhSoLuongKiemDich.TagName = "QQU";
                txtLoaiBaoBi.Tag = "KPG";
                txtMucDichSuDung.Tag = "UP";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                isValid &= ValidateControl.ValidateNull(txtTenHangHoa, errorProvider, "Mô tả hàng hóa (Loại động vật/sản phẩm động vật)", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtTenHangHoa.MaxLength = 768;
                txtMaSoHangHoa.MaxLength = 12;
                //txtTinhBiet.MaxLength = 1;
                txtTuoi.MaxLength = 3;
                txtNoiSanXuat.MaxLength = 150;
                txtQuyCachDongGoi.MaxLength = 50;
                txtTongSoLuongNhapKhau.MaxLength = 8;
                //txtDonViTinhTongSoLuongNhapKhau.MaxLength = 3;
                txtKichCoCaThe.MaxLength = 10;
                txtTrongLuongTinh.MaxLength = 10;
                txtTrongLuongTinh.DecimalDigits = 3;
                //txtDonViTinhTrongLuong.MaxLength = 3;
                txtTrongLuongCaBi.MaxLength = 10;
                txtTrongLuongCaBi.DecimalDigits = 3;
                //txtDonViTinhTrongLuongCaBi.MaxLength = 3;
                txtSoLuongKiemDich.MaxLength = 8;
                //txtDonViTinhSoLuongKiemDich.MaxLength = 3;
                txtLoaiBaoBi.MaxLength = 100;
                txtMucDichSuDung.MaxLength = 300;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtMaSoHangHoa.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtTinhBiet.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtNoiSanXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtQuyCachDongGoi.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTinhTongSoLuongNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtKichCoCaThe.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTinhTrongLuong.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTinhTrongLuongCaBi.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTinhSoLuongKiemDich.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtLoaiBaoBi.TextChanged += new EventHandler(SetTextChanged_Handler);

            txtMaSoHangHoa.CharacterCasing = CharacterCasing.Upper;
            //txtTinhBiet.CharacterCasing = CharacterCasing.Upper;
            txtNoiSanXuat.CharacterCasing = CharacterCasing.Upper;
            txtQuyCachDongGoi.CharacterCasing = CharacterCasing.Upper;
            //txtDonViTinhTongSoLuongNhapKhau.CharacterCasing = CharacterCasing.Upper;
            txtKichCoCaThe.CharacterCasing = CharacterCasing.Upper;
            //txtDonViTinhTrongLuong.CharacterCasing = CharacterCasing.Upper;
            //txtDonViTinhTrongLuongCaBi.CharacterCasing = CharacterCasing.Upper;
            //txtDonViTinhSoLuongKiemDich.CharacterCasing = CharacterCasing.Upper;

            txtLoaiBaoBi.CharacterCasing = CharacterCasing.Upper;
        }

    }
}
