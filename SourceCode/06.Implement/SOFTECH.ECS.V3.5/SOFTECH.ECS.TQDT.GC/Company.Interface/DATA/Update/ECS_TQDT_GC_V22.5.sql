﻿SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[t_GC_QuyetToan_Mau15]'
GO
CREATE TABLE [dbo].[t_GC_QuyetToan_Mau15]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoTaiKhoan] [int] NULL,
[HopDong_ID] [int] NULL,
[SoHopDong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoToKhai] [bigint] NULL,
[SoToKhaiVnacc] [numeric] (18, 0) NULL,
[NgayDangKy] [datetime] NULL,
[MaLoaiHinh] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Ten] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaHS] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuongDangKy] [numeric] (18, 5) NULL,
[SoLuongDaNhap] [numeric] (18, 5) NULL,
[SoLuongDaDung] [numeric] (18, 5) NULL,
[SoLuongCungUng] [numeric] (18, 5) NULL,
[LuongTon] [numeric] (18, 5) NULL,
[SoLuong] [numeric] (18, 5) NULL,
[LuongSD_TK] [numeric] (18, 5) NULL,
[LuongTon_TK] [numeric] (18, 5) NULL,
[DonGiaKB] [numeric] (24, 6) NULL,
[DonGiaTT] [numeric] (24, 6) NULL,
[TriGiaTT] [numeric] (24, 6) NULL,
[TriGiaSuDung] [numeric] (24, 6) NULL,
[TonTriGia] [numeric] (24, 6) NULL,
[TyGiaTT] [numeric] (18, 0) NULL,
[MaDVT] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_GC_QuyetToan_Mau15] on [dbo].[t_GC_QuyetToan_Mau15]'
GO
ALTER TABLE [dbo].[t_GC_QuyetToan_Mau15] ADD CONSTRAINT [PK_t_GC_QuyetToan_Mau15] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_t_GC_QuyetToan_Mau15_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_t_GC_QuyetToan_Mau15_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 22, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_t_GC_QuyetToan_Mau15_Insert]
	@SoTaiKhoan int,
	@HopDong_ID int,
	@SoHopDong varchar(50),
	@SoToKhai bigint,
	@SoToKhaiVnacc numeric(18, 0),
	@NgayDangKy datetime,
	@MaLoaiHinh varchar(5),
	@Ma varchar(50),
	@Ten nvarchar(200),
	@MaHS varchar(8),
	@DVT_ID varchar(10),
	@SoLuongDangKy numeric(18, 5),
	@SoLuongDaNhap numeric(18, 5),
	@SoLuongDaDung numeric(18, 5),
	@SoLuongCungUng numeric(18, 5),
	@LuongTon numeric(18, 5),
	@SoLuong numeric(18, 5),
	@LuongSD_TK numeric(18, 5),
	@LuongTon_TK numeric(18, 5),
	@DonGiaKB numeric(24, 6),
	@DonGiaTT numeric(24, 6),
	@TriGiaTT numeric(24, 6),
	@TriGiaSuDung numeric(24, 6),
	@TonTriGia numeric(24, 6),
	@TyGiaTT numeric(18, 0),
	@MaDVT varchar(10),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_GC_QuyetToan_Mau15]
(
	[SoTaiKhoan],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[SoToKhaiVnacc],
	[NgayDangKy],
	[MaLoaiHinh],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[LuongTon],
	[SoLuong],
	[LuongSD_TK],
	[LuongTon_TK],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaTT],
	[TriGiaSuDung],
	[TonTriGia],
	[TyGiaTT],
	[MaDVT]
)
VALUES 
(
	@SoTaiKhoan,
	@HopDong_ID,
	@SoHopDong,
	@SoToKhai,
	@SoToKhaiVnacc,
	@NgayDangKy,
	@MaLoaiHinh,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@SoLuongDaNhap,
	@SoLuongDaDung,
	@SoLuongCungUng,
	@LuongTon,
	@SoLuong,
	@LuongSD_TK,
	@LuongTon_TK,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaTT,
	@TriGiaSuDung,
	@TonTriGia,
	@TyGiaTT,
	@MaDVT
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_t_GC_QuyetToan_Mau15_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_t_GC_QuyetToan_Mau15_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 22, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_t_GC_QuyetToan_Mau15_Update]
	@ID bigint,
	@SoTaiKhoan int,
	@HopDong_ID int,
	@SoHopDong varchar(50),
	@SoToKhai bigint,
	@SoToKhaiVnacc numeric(18, 0),
	@NgayDangKy datetime,
	@MaLoaiHinh varchar(5),
	@Ma varchar(50),
	@Ten nvarchar(200),
	@MaHS varchar(8),
	@DVT_ID varchar(10),
	@SoLuongDangKy numeric(18, 5),
	@SoLuongDaNhap numeric(18, 5),
	@SoLuongDaDung numeric(18, 5),
	@SoLuongCungUng numeric(18, 5),
	@LuongTon numeric(18, 5),
	@SoLuong numeric(18, 5),
	@LuongSD_TK numeric(18, 5),
	@LuongTon_TK numeric(18, 5),
	@DonGiaKB numeric(24, 6),
	@DonGiaTT numeric(24, 6),
	@TriGiaTT numeric(24, 6),
	@TriGiaSuDung numeric(24, 6),
	@TonTriGia numeric(24, 6),
	@TyGiaTT numeric(18, 0),
	@MaDVT varchar(10)
AS

UPDATE
	[dbo].[t_GC_QuyetToan_Mau15]
SET
	[SoTaiKhoan] = @SoTaiKhoan,
	[HopDong_ID] = @HopDong_ID,
	[SoHopDong] = @SoHopDong,
	[SoToKhai] = @SoToKhai,
	[SoToKhaiVnacc] = @SoToKhaiVnacc,
	[NgayDangKy] = @NgayDangKy,
	[MaLoaiHinh] = @MaLoaiHinh,
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[SoLuongDaNhap] = @SoLuongDaNhap,
	[SoLuongDaDung] = @SoLuongDaDung,
	[SoLuongCungUng] = @SoLuongCungUng,
	[LuongTon] = @LuongTon,
	[SoLuong] = @SoLuong,
	[LuongSD_TK] = @LuongSD_TK,
	[LuongTon_TK] = @LuongTon_TK,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaSuDung] = @TriGiaSuDung,
	[TonTriGia] = @TonTriGia,
	[TyGiaTT] = @TyGiaTT,
	[MaDVT] = @MaDVT
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_t_GC_QuyetToan_Mau15_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_t_GC_QuyetToan_Mau15_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 22, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_t_GC_QuyetToan_Mau15_InsertUpdate]
	@ID bigint,
	@SoTaiKhoan int,
	@HopDong_ID int,
	@SoHopDong varchar(50),
	@SoToKhai bigint,
	@SoToKhaiVnacc numeric(18, 0),
	@NgayDangKy datetime,
	@MaLoaiHinh varchar(5),
	@Ma varchar(50),
	@Ten nvarchar(200),
	@MaHS varchar(8),
	@DVT_ID varchar(10),
	@SoLuongDangKy numeric(18, 5),
	@SoLuongDaNhap numeric(18, 5),
	@SoLuongDaDung numeric(18, 5),
	@SoLuongCungUng numeric(18, 5),
	@LuongTon numeric(18, 5),
	@SoLuong numeric(18, 5),
	@LuongSD_TK numeric(18, 5),
	@LuongTon_TK numeric(18, 5),
	@DonGiaKB numeric(24, 6),
	@DonGiaTT numeric(24, 6),
	@TriGiaTT numeric(24, 6),
	@TriGiaSuDung numeric(24, 6),
	@TonTriGia numeric(24, 6),
	@TyGiaTT numeric(18, 0),
	@MaDVT varchar(10)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_GC_QuyetToan_Mau15] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_GC_QuyetToan_Mau15] 
		SET
			[SoTaiKhoan] = @SoTaiKhoan,
			[HopDong_ID] = @HopDong_ID,
			[SoHopDong] = @SoHopDong,
			[SoToKhai] = @SoToKhai,
			[SoToKhaiVnacc] = @SoToKhaiVnacc,
			[NgayDangKy] = @NgayDangKy,
			[MaLoaiHinh] = @MaLoaiHinh,
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[SoLuongDaNhap] = @SoLuongDaNhap,
			[SoLuongDaDung] = @SoLuongDaDung,
			[SoLuongCungUng] = @SoLuongCungUng,
			[LuongTon] = @LuongTon,
			[SoLuong] = @SoLuong,
			[LuongSD_TK] = @LuongSD_TK,
			[LuongTon_TK] = @LuongTon_TK,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaSuDung] = @TriGiaSuDung,
			[TonTriGia] = @TonTriGia,
			[TyGiaTT] = @TyGiaTT,
			[MaDVT] = @MaDVT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_GC_QuyetToan_Mau15]
		(
			[SoTaiKhoan],
			[HopDong_ID],
			[SoHopDong],
			[SoToKhai],
			[SoToKhaiVnacc],
			[NgayDangKy],
			[MaLoaiHinh],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[SoLuongDaNhap],
			[SoLuongDaDung],
			[SoLuongCungUng],
			[LuongTon],
			[SoLuong],
			[LuongSD_TK],
			[LuongTon_TK],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaTT],
			[TriGiaSuDung],
			[TonTriGia],
			[TyGiaTT],
			[MaDVT]
		)
		VALUES 
		(
			@SoTaiKhoan,
			@HopDong_ID,
			@SoHopDong,
			@SoToKhai,
			@SoToKhaiVnacc,
			@NgayDangKy,
			@MaLoaiHinh,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@SoLuongDaNhap,
			@SoLuongDaDung,
			@SoLuongCungUng,
			@LuongTon,
			@SoLuong,
			@LuongSD_TK,
			@LuongTon_TK,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaTT,
			@TriGiaSuDung,
			@TonTriGia,
			@TyGiaTT,
			@MaDVT
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_t_GC_QuyetToan_Mau15_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_t_GC_QuyetToan_Mau15_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 22, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_t_GC_QuyetToan_Mau15_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_GC_QuyetToan_Mau15]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_t_GC_QuyetToan_Mau15_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_t_GC_QuyetToan_Mau15_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 22, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_t_GC_QuyetToan_Mau15_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTaiKhoan],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[SoToKhaiVnacc],
	[NgayDangKy],
	[MaLoaiHinh],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[LuongTon],
	[SoLuong],
	[LuongSD_TK],
	[LuongTon_TK],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaTT],
	[TriGiaSuDung],
	[TonTriGia],
	[TyGiaTT],
	[MaDVT]
FROM
	[dbo].[t_GC_QuyetToan_Mau15]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_t_GC_QuyetToan_Mau15_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_t_GC_QuyetToan_Mau15_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 22, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_t_GC_QuyetToan_Mau15_SelectAll]



























AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTaiKhoan],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[SoToKhaiVnacc],
	[NgayDangKy],
	[MaLoaiHinh],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[LuongTon],
	[SoLuong],
	[LuongSD_TK],
	[LuongTon_TK],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaTT],
	[TriGiaSuDung],
	[TonTriGia],
	[TyGiaTT],
	[MaDVT]
FROM
	[dbo].[t_GC_QuyetToan_Mau15]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GC_NPLQuyetToan]'
GO
CREATE TABLE [dbo].[t_KDT_GC_NPLQuyetToan]
(
[ID] [bigint] NOT NULL,
[HopDong_ID] [int] NULL,
[SoHopDong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoToKhai] [numeric] (18, 0) NULL,
[MaLoaiHinh] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayDangKy] [datetime] NULL,
[MaHang] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (24, 4) NULL,
[MaNguyenPhuLieu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DinhMucSuDung] [numeric] (24, 6) NULL,
[TyLeHaoHut] [numeric] (24, 6) NULL,
[LuongNPLChuaHH] [numeric] (24, 6) NULL,
[LuongNPLCoHH] [numeric] (24, 6) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_GC_NPLQuyetToan] on [dbo].[t_KDT_GC_NPLQuyetToan]'
GO
ALTER TABLE [dbo].[t_KDT_GC_NPLQuyetToan] ADD CONSTRAINT [PK_t_KDT_GC_NPLQuyetToan] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLQuyetToan_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_Insert]
	@ID bigint,
	@HopDong_ID int,
	@SoHopDong varchar(50),
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@MaHang varchar(50),
	@TenHang nvarchar(150),
	@SoLuong numeric(24, 4),
	@MaNguyenPhuLieu varchar(50),
	@DinhMucSuDung numeric(24, 6),
	@TyLeHaoHut numeric(24, 6),
	@LuongNPLChuaHH numeric(24, 6),
	@LuongNPLCoHH numeric(24, 6)
AS
INSERT INTO [dbo].[t_KDT_GC_NPLQuyetToan]
(
	[ID],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHang],
	[TenHang],
	[SoLuong],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[LuongNPLChuaHH],
	[LuongNPLCoHH]
)
VALUES
(
	@ID,
	@HopDong_ID,
	@SoHopDong,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaHang,
	@TenHang,
	@SoLuong,
	@MaNguyenPhuLieu,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@LuongNPLChuaHH,
	@LuongNPLCoHH
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLQuyetToan_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_Update]
	@ID bigint,
	@HopDong_ID int,
	@SoHopDong varchar(50),
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@MaHang varchar(50),
	@TenHang nvarchar(150),
	@SoLuong numeric(24, 4),
	@MaNguyenPhuLieu varchar(50),
	@DinhMucSuDung numeric(24, 6),
	@TyLeHaoHut numeric(24, 6),
	@LuongNPLChuaHH numeric(24, 6),
	@LuongNPLCoHH numeric(24, 6)
AS

UPDATE
	[dbo].[t_KDT_GC_NPLQuyetToan]
SET
	[HopDong_ID] = @HopDong_ID,
	[SoHopDong] = @SoHopDong,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaHang] = @MaHang,
	[TenHang] = @TenHang,
	[SoLuong] = @SoLuong,
	[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[LuongNPLChuaHH] = @LuongNPLChuaHH,
	[LuongNPLCoHH] = @LuongNPLCoHH
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLQuyetToan_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_InsertUpdate]
	@ID bigint,
	@HopDong_ID int,
	@SoHopDong varchar(50),
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@MaHang varchar(50),
	@TenHang nvarchar(150),
	@SoLuong numeric(24, 4),
	@MaNguyenPhuLieu varchar(50),
	@DinhMucSuDung numeric(24, 6),
	@TyLeHaoHut numeric(24, 6),
	@LuongNPLChuaHH numeric(24, 6),
	@LuongNPLCoHH numeric(24, 6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_NPLQuyetToan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_NPLQuyetToan] 
		SET
			[HopDong_ID] = @HopDong_ID,
			[SoHopDong] = @SoHopDong,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaHang] = @MaHang,
			[TenHang] = @TenHang,
			[SoLuong] = @SoLuong,
			[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[LuongNPLChuaHH] = @LuongNPLChuaHH,
			[LuongNPLCoHH] = @LuongNPLCoHH
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_NPLQuyetToan]
	(
			[ID],
			[HopDong_ID],
			[SoHopDong],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaHang],
			[TenHang],
			[SoLuong],
			[MaNguyenPhuLieu],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[LuongNPLChuaHH],
			[LuongNPLCoHH]
	)
	VALUES
	(
			@ID,
			@HopDong_ID,
			@SoHopDong,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaHang,
			@TenHang,
			@SoLuong,
			@MaNguyenPhuLieu,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@LuongNPLChuaHH,
			@LuongNPLCoHH
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLQuyetToan_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_NPLQuyetToan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLQuyetToan_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHang],
	[TenHang],
	[SoLuong],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[LuongNPLChuaHH],
	[LuongNPLCoHH]
FROM
	[dbo].[t_KDT_GC_NPLQuyetToan]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLQuyetToan_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHang],
	[TenHang],
	[SoLuong],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[LuongNPLChuaHH],
	[LuongNPLCoHH]
FROM
	[dbo].[t_KDT_GC_NPLQuyetToan]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GC_HoSoQuyetToan]'
GO
CREATE TABLE [dbo].[t_KDT_GC_HoSoQuyetToan]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[SoHoSo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayTao] [datetime] NULL,
[NamQT] [int] NULL,
[GhiChu] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_GC_HoSoQuyetToan] on [dbo].[t_KDT_GC_HoSoQuyetToan]'
GO
ALTER TABLE [dbo].[t_KDT_GC_HoSoQuyetToan] ADD CONSTRAINT [PK_t_KDT_GC_HoSoQuyetToan] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_Insert]
	@SoHoSo varchar(50),
	@NgayTao datetime,
	@NamQT int,
	@GhiChu nvarchar(500),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_HoSoQuyetToan]
(
	[SoHoSo],
	[NgayTao],
	[NamQT],
	[GhiChu]
)
VALUES 
(
	@SoHoSo,
	@NgayTao,
	@NamQT,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_Update]
	@ID int,
	@SoHoSo varchar(50),
	@NgayTao datetime,
	@NamQT int,
	@GhiChu nvarchar(500)
AS

UPDATE
	[dbo].[t_KDT_GC_HoSoQuyetToan]
SET
	[SoHoSo] = @SoHoSo,
	[NgayTao] = @NgayTao,
	[NamQT] = @NamQT,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_InsertUpdate]
	@ID int,
	@SoHoSo varchar(50),
	@NgayTao datetime,
	@NamQT int,
	@GhiChu nvarchar(500)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_HoSoQuyetToan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_HoSoQuyetToan] 
		SET
			[SoHoSo] = @SoHoSo,
			[NgayTao] = @NgayTao,
			[NamQT] = @NamQT,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_HoSoQuyetToan]
		(
			[SoHoSo],
			[NgayTao],
			[NamQT],
			[GhiChu]
		)
		VALUES 
		(
			@SoHoSo,
			@NgayTao,
			@NamQT,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_GC_HoSoQuyetToan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHoSo],
	[NgayTao],
	[NamQT],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_HoSoQuyetToan]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHoSo],
	[NgayTao],
	[NamQT],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_HoSoQuyetToan]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]'
GO
CREATE TABLE [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[Master_ID] [int] NULL,
[HopDong_ID] [bigint] NULL,
[SoHopDong] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayKy] [datetime] NULL,
[NgayHetHan] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_GC_HoSoQuyetToan_DSHopDong] on [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]'
GO
ALTER TABLE [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong] ADD CONSTRAINT [PK_t_KDT_GC_HoSoQuyetToan_DSHopDong] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_DSHopDong_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DSHopDong_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_Insert]
	@Master_ID int,
	@HopDong_ID bigint,
	@SoHopDong varchar(50),
	@NgayKy datetime,
	@NgayHetHan datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]
(
	[Master_ID],
	[HopDong_ID],
	[SoHopDong],
	[NgayKy],
	[NgayHetHan]
)
VALUES 
(
	@Master_ID,
	@HopDong_ID,
	@SoHopDong,
	@NgayKy,
	@NgayHetHan
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_DSHopDong_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DSHopDong_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_Update]
	@ID bigint,
	@Master_ID int,
	@HopDong_ID bigint,
	@SoHopDong varchar(50),
	@NgayKy datetime,
	@NgayHetHan datetime
AS

UPDATE
	[dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]
SET
	[Master_ID] = @Master_ID,
	[HopDong_ID] = @HopDong_ID,
	[SoHopDong] = @SoHopDong,
	[NgayKy] = @NgayKy,
	[NgayHetHan] = @NgayHetHan
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_DSHopDong_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DSHopDong_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_InsertUpdate]
	@ID bigint,
	@Master_ID int,
	@HopDong_ID bigint,
	@SoHopDong varchar(50),
	@NgayKy datetime,
	@NgayHetHan datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong] 
		SET
			[Master_ID] = @Master_ID,
			[HopDong_ID] = @HopDong_ID,
			[SoHopDong] = @SoHopDong,
			[NgayKy] = @NgayKy,
			[NgayHetHan] = @NgayHetHan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]
		(
			[Master_ID],
			[HopDong_ID],
			[SoHopDong],
			[NgayKy],
			[NgayHetHan]
		)
		VALUES 
		(
			@Master_ID,
			@HopDong_ID,
			@SoHopDong,
			@NgayKy,
			@NgayHetHan
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_DSHopDong_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DSHopDong_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_DSHopDong_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DSHopDong_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[HopDong_ID],
	[SoHopDong],
	[NgayKy],
	[NgayHetHan]
FROM
	[dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_DSHopDong_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DSHopDong_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[HopDong_ID],
	[SoHopDong],
	[NgayKy],
	[NgayHetHan]
FROM
	[dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_t_GC_QuyetToan_Mau15_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_t_GC_QuyetToan_Mau15_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 22, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_t_GC_QuyetToan_Mau15_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_QuyetToan_Mau15] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_t_GC_QuyetToan_Mau15_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_t_GC_QuyetToan_Mau15_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, February 22, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_t_GC_QuyetToan_Mau15_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTaiKhoan],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[SoToKhaiVnacc],
	[NgayDangKy],
	[MaLoaiHinh],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[LuongTon],
	[SoLuong],
	[LuongSD_TK],
	[LuongTon_TK],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaTT],
	[TriGiaSuDung],
	[TonTriGia],
	[TyGiaTT],
	[MaDVT]
FROM [dbo].[t_GC_QuyetToan_Mau15] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLQuyetToan_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_NPLQuyetToan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLQuyetToan_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, March 2, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHang],
	[TenHang],
	[SoLuong],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[LuongNPLChuaHH],
	[LuongNPLCoHH]
FROM [dbo].[t_KDT_GC_NPLQuyetToan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_HoSoQuyetToan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoHoSo],
	[NgayTao],
	[NamQT],
	[GhiChu]
FROM [dbo].[t_KDT_GC_HoSoQuyetToan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_DSHopDong_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DSHopDong_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HoSoQuyetToan_DSHopDong_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DSHopDong_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 26, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[HopDong_ID],
	[SoHopDong],
	[NgayKy],
	[NgayHetHan]
FROM [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_HopDong_SelectDynamic_QuyetToan]'
GO
   
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_HopDong_SelectDynamic]  
-- Database: ECS_TQDT_GC_V3  
-- Author: Ngo Thanh Tung  
-- Time created: Tuesday, March 06, 2012  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_GC_HopDong_SelectDynamic_QuyetToan]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT   
 [ID],  
 [SoTiepNhan],  
 [TrangThaiXuLy],  
 [SoHopDong],  
 [MaHaiQuan],  
 [MaDoanhNghiep],  
 [MaDaiLy],  
 [NgayKy],  
 [NgayDangKy],  
 [NgayHetHan],  
 [NgayGiaHan],  
 [NuocThue_ID],  
 [NguyenTe_ID],  
 [NgayTiepNhan],  
 [DonViDoiTac],  
 [DiaChiDoiTac],  
 [CanBoTheoDoi],  
 [CanBoDuyet],  
 [TrangThaiThanhKhoan],  
 [GUIDSTR],  
 [DeXuatKhac],  
 [LyDoSua],  
 [ActionStatus],  
 [GuidReference],  
 [NamTN],  
 [HUONGDAN],  
 [PhanLuong],  
 [HuongdanPL],  
 [DiaChiDoanhNghiep],  
 [TenDonViDoiTac],  
 [TenDoanhNghiep],  
 [GhiChu],  
 [PTTT_ID],  
 [TongTriGiaSP],  
 [TongTriGiaTienCong],
 Case when TrangThaiThanhKhoan = 2 then (select top 1 Master_ID from t_KDT_GC_HoSoQuyetToan_DSHopDong hd where hd.HopDong_ID = t_KDT_GC_HopDong.ID) else 0 end as ID_QuyetToan 
FROM [dbo].[t_KDT_GC_HopDong]   
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO



GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '22.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('22.5',GETDATE(), N'SQL for hồ sơ quyết toán mẫu 15')
END	