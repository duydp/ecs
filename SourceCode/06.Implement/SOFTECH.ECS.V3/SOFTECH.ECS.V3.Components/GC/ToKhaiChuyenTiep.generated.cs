﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class ToKhaiChuyenTiep
    {
        #region Properties.

        public long ID { set; get; }
        public long IDHopDong { set; get; }
        public long SoTiepNhan { set; get; }
        public DateTime NgayTiepNhan { set; get; }
        public int TrangThaiXuLy { set; get; }
        public string MaHaiQuanTiepNhan { set; get; }
        public long SoToKhai { set; get; }
        public string CanBoDangKy { set; get; }
        public DateTime NgayDangKy { set; get; }
        public string MaDoanhNghiep { set; get; }
        public string SoHopDongDV { set; get; }
        public DateTime NgayHDDV { set; get; }
        public DateTime NgayHetHanHDDV { set; get; }
        public string NguoiChiDinhDV { set; get; }
        public string MaKhachHang { set; get; }
        public string TenKH { set; get; }
        public string SoHDKH { set; get; }
        public DateTime NgayHDKH { set; get; }
        public DateTime NgayHetHanHDKH { set; get; }
        public string NguoiChiDinhKH { set; get; }
        public string MaDaiLy { set; get; }
        public string MaLoaiHinh { set; get; }
        public string DiaDiemXepHang { set; get; }
        public decimal TyGiaVND { set; get; }
        public decimal TyGiaUSD { set; get; }
        public decimal LePhiHQ { set; get; }
        public string SoBienLai { set; get; }
        public DateTime NgayBienLai { set; get; }
        public string ChungTu { set; get; }
        public string NguyenTe_ID { set; get; }
        public string MaHaiQuanKH { set; get; }
        public long ID_Relation { set; get; }
        public string GUIDSTR { set; get; }
        public string DeXuatKhac { set; get; }
        public string LyDoSua { set; get; }
        public int ActionStatus { set; get; }
        public string GuidReference { set; get; }
        public short NamDK { set; get; }
        public string HUONGDAN { set; get; }
        public string PhanLuong { set; get; }
        public string Huongdan_PL { set; get; }
        public string PTTT_ID { set; get; }
        public string DKGH_ID { set; get; }
        public DateTime ThoiGianGiaoHang { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<ToKhaiChuyenTiep> ConvertToCollection(IDataReader reader)
        {
            IList<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static ToKhaiChuyenTiep Load(long id)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<ToKhaiChuyenTiep> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<ToKhaiChuyenTiep> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<ToKhaiChuyenTiep> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<ToKhaiChuyenTiep> SelectCollectionBy_IDHopDong(long iDHopDong)
        {
            IDataReader reader = SelectReaderBy_IDHopDong(iDHopDong);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_IDHopDong(long iDHopDong)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_IDHopDong(long iDHopDong)
        {
            const string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertToKhaiChuyenTiep(long iDHopDong, long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string maHaiQuanTiepNhan, long soToKhai, string canBoDangKy, DateTime ngayDangKy, string maDoanhNghiep, string soHopDongDV, DateTime ngayHDDV, DateTime ngayHetHanHDDV, string nguoiChiDinhDV, string maKhachHang, string tenKH, string soHDKH, DateTime ngayHDKH, DateTime ngayHetHanHDKH, string nguoiChiDinhKH, string maDaiLy, string maLoaiHinh, string diaDiemXepHang, decimal tyGiaVND, decimal tyGiaUSD, decimal lePhiHQ, string soBienLai, DateTime ngayBienLai, string chungTu, string nguyenTe_ID, string maHaiQuanKH, long iD_Relation, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL, string pttt_ID, string dkgh, DateTime thoiGianGiaoHang)
        {
            ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
            entity.IDHopDong = iDHopDong;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThaiXuLy = trangThaiXuLy;
            entity.MaHaiQuanTiepNhan = maHaiQuanTiepNhan;
            entity.SoToKhai = soToKhai;
            entity.CanBoDangKy = canBoDangKy;
            entity.NgayDangKy = ngayDangKy;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.SoHopDongDV = soHopDongDV;
            entity.NgayHDDV = ngayHDDV;
            entity.NgayHetHanHDDV = ngayHetHanHDDV;
            entity.NguoiChiDinhDV = nguoiChiDinhDV;
            entity.MaKhachHang = maKhachHang;
            entity.TenKH = tenKH;
            entity.SoHDKH = soHDKH;
            entity.NgayHDKH = ngayHDKH;
            entity.NgayHetHanHDKH = ngayHetHanHDKH;
            entity.NguoiChiDinhKH = nguoiChiDinhKH;
            entity.MaDaiLy = maDaiLy;
            entity.MaLoaiHinh = maLoaiHinh;
            entity.DiaDiemXepHang = diaDiemXepHang;
            entity.TyGiaVND = tyGiaVND;
            entity.TyGiaUSD = tyGiaUSD;
            entity.LePhiHQ = lePhiHQ;
            entity.SoBienLai = soBienLai;
            entity.NgayBienLai = ngayBienLai;
            entity.ChungTu = chungTu;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.MaHaiQuanKH = maHaiQuanKH;
            entity.ID_Relation = iD_Relation;
            entity.GUIDSTR = gUIDSTR;
            entity.DeXuatKhac = deXuatKhac;
            entity.LyDoSua = lyDoSua;
            entity.ActionStatus = actionStatus;
            entity.GuidReference = guidReference;
            entity.NamDK = namDK;
            entity.HUONGDAN = hUONGDAN;
            entity.PhanLuong = phanLuong;
            entity.Huongdan_PL = huongdan_PL;
            entity.PTTT_ID = pttt_ID;
            entity.DKGH_ID = dkgh;
            entity.ThoiGianGiaoHang = thoiGianGiaoHang;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object)NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV.Year <= 1753 ? DBNull.Value : (object)NgayHDDV);
            db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV.Year <= 1753 ? DBNull.Value : (object)NgayHetHanHDDV);
            db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH.Year <= 1753 ? DBNull.Value : (object)NgayHDKH);
            db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH.Year <= 1753 ? DBNull.Value : (object)NgayHetHanHDKH);
            db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai.Year <= 1753 ? DBNull.Value : (object)NgayBienLai);
            db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<ToKhaiChuyenTiep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ToKhaiChuyenTiep item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateToKhaiChuyenTiep(long id, long iDHopDong, long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string maHaiQuanTiepNhan, long soToKhai, string canBoDangKy, DateTime ngayDangKy, string maDoanhNghiep, string soHopDongDV, DateTime ngayHDDV, DateTime ngayHetHanHDDV, string nguoiChiDinhDV, string maKhachHang, string tenKH, string soHDKH, DateTime ngayHDKH, DateTime ngayHetHanHDKH, string nguoiChiDinhKH, string maDaiLy, string maLoaiHinh, string diaDiemXepHang, decimal tyGiaVND, decimal tyGiaUSD, decimal lePhiHQ, string soBienLai, DateTime ngayBienLai, string chungTu, string nguyenTe_ID, string maHaiQuanKH, long iD_Relation, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL, string pttt_ID, string dkgh, DateTime thoiGianGiaoHang)
        {
            ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
            entity.ID = id;
            entity.IDHopDong = iDHopDong;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThaiXuLy = trangThaiXuLy;
            entity.MaHaiQuanTiepNhan = maHaiQuanTiepNhan;
            entity.SoToKhai = soToKhai;
            entity.CanBoDangKy = canBoDangKy;
            entity.NgayDangKy = ngayDangKy;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.SoHopDongDV = soHopDongDV;
            entity.NgayHDDV = ngayHDDV;
            entity.NgayHetHanHDDV = ngayHetHanHDDV;
            entity.NguoiChiDinhDV = nguoiChiDinhDV;
            entity.MaKhachHang = maKhachHang;
            entity.TenKH = tenKH;
            entity.SoHDKH = soHDKH;
            entity.NgayHDKH = ngayHDKH;
            entity.NgayHetHanHDKH = ngayHetHanHDKH;
            entity.NguoiChiDinhKH = nguoiChiDinhKH;
            entity.MaDaiLy = maDaiLy;
            entity.MaLoaiHinh = maLoaiHinh;
            entity.DiaDiemXepHang = diaDiemXepHang;
            entity.TyGiaVND = tyGiaVND;
            entity.TyGiaUSD = tyGiaUSD;
            entity.LePhiHQ = lePhiHQ;
            entity.SoBienLai = soBienLai;
            entity.NgayBienLai = ngayBienLai;
            entity.ChungTu = chungTu;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.MaHaiQuanKH = maHaiQuanKH;
            entity.ID_Relation = iD_Relation;
            entity.GUIDSTR = gUIDSTR;
            entity.DeXuatKhac = deXuatKhac;
            entity.LyDoSua = lyDoSua;
            entity.ActionStatus = actionStatus;
            entity.GuidReference = guidReference;
            entity.NamDK = namDK;
            entity.HUONGDAN = hUONGDAN;
            entity.PhanLuong = phanLuong;
            entity.Huongdan_PL = huongdan_PL;
            entity.PTTT_ID = pttt_ID;
            entity.DKGH_ID = dkgh;
            entity.ThoiGianGiaoHang = thoiGianGiaoHang;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_GC_ToKhaiChuyenTiep_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object)NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV.Year <= 1753 ? DBNull.Value : (object)NgayHDDV);
            db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV.Year <= 1753 ? DBNull.Value : (object)NgayHetHanHDDV);
            db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH.Year <= 1753 ? DBNull.Value : (object)NgayHDKH);
            db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH.Year <= 1753 ? DBNull.Value : (object)NgayHetHanHDKH);
            db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai.Year <= 1753 ? DBNull.Value : (object)NgayBienLai);
            db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<ToKhaiChuyenTiep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ToKhaiChuyenTiep item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateToKhaiChuyenTiep(long id, long iDHopDong, long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, string maHaiQuanTiepNhan, long soToKhai, string canBoDangKy, DateTime ngayDangKy, string maDoanhNghiep, string soHopDongDV, DateTime ngayHDDV, DateTime ngayHetHanHDDV, string nguoiChiDinhDV, string maKhachHang, string tenKH, string soHDKH, DateTime ngayHDKH, DateTime ngayHetHanHDKH, string nguoiChiDinhKH, string maDaiLy, string maLoaiHinh, string diaDiemXepHang, decimal tyGiaVND, decimal tyGiaUSD, decimal lePhiHQ, string soBienLai, DateTime ngayBienLai, string chungTu, string nguyenTe_ID, string maHaiQuanKH, long iD_Relation, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdan_PL, string pttt_ID, string dkgh, DateTime thoiGianGiaoHang)
        {
            ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
            entity.ID = id;
            entity.IDHopDong = iDHopDong;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThaiXuLy = trangThaiXuLy;
            entity.MaHaiQuanTiepNhan = maHaiQuanTiepNhan;
            entity.SoToKhai = soToKhai;
            entity.CanBoDangKy = canBoDangKy;
            entity.NgayDangKy = ngayDangKy;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.SoHopDongDV = soHopDongDV;
            entity.NgayHDDV = ngayHDDV;
            entity.NgayHetHanHDDV = ngayHetHanHDDV;
            entity.NguoiChiDinhDV = nguoiChiDinhDV;
            entity.MaKhachHang = maKhachHang;
            entity.TenKH = tenKH;
            entity.SoHDKH = soHDKH;
            entity.NgayHDKH = ngayHDKH;
            entity.NgayHetHanHDKH = ngayHetHanHDKH;
            entity.NguoiChiDinhKH = nguoiChiDinhKH;
            entity.MaDaiLy = maDaiLy;
            entity.MaLoaiHinh = maLoaiHinh;
            entity.DiaDiemXepHang = diaDiemXepHang;
            entity.TyGiaVND = tyGiaVND;
            entity.TyGiaUSD = tyGiaUSD;
            entity.LePhiHQ = lePhiHQ;
            entity.SoBienLai = soBienLai;
            entity.NgayBienLai = ngayBienLai;
            entity.ChungTu = chungTu;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.MaHaiQuanKH = maHaiQuanKH;
            entity.ID_Relation = iD_Relation;
            entity.GUIDSTR = gUIDSTR;
            entity.DeXuatKhac = deXuatKhac;
            entity.LyDoSua = lyDoSua;
            entity.ActionStatus = actionStatus;
            entity.GuidReference = guidReference;
            entity.NamDK = namDK;
            entity.HUONGDAN = hUONGDAN;
            entity.PhanLuong = phanLuong;
            entity.Huongdan_PL = huongdan_PL;
            entity.PTTT_ID = pttt_ID;
            entity.DKGH_ID = dkgh;
            entity.ThoiGianGiaoHang = thoiGianGiaoHang;

            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object)NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV.Year <= 1753 ? DBNull.Value : (object)NgayHDDV);
            db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV.Year <= 1753 ? DBNull.Value : (object)NgayHetHanHDDV);
            db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH.Year <= 1753 ? DBNull.Value : (object)NgayHDKH);
            db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH.Year <= 1753 ? DBNull.Value : (object)NgayHetHanHDKH);
            db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai.Year <= 1753 ? DBNull.Value : (object)NgayBienLai);
            db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<ToKhaiChuyenTiep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ToKhaiChuyenTiep item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteToKhaiChuyenTiep(long id)
        {
            ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_IDHopDong(long iDHopDong)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_DeleteBy_IDHopDong]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<ToKhaiChuyenTiep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ToKhaiChuyenTiep item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}