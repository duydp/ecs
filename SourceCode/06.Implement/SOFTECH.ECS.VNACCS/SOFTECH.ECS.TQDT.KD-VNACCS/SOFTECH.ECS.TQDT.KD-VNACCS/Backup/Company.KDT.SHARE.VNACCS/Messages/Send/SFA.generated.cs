using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class SFA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute SMC { get; set; }
public PropertiesAttribute APP { get; set; }
public PropertiesAttribute APN { get; set; }
public PropertiesAttribute FNC { get; set; }
public PropertiesAttribute APT { get; set; }
public PropertiesAttribute EXN { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EXA { get; set; }
public PropertiesAttribute EXS { get; set; }
public PropertiesAttribute EXD { get; set; }
public PropertiesAttribute EXC { get; set; }
public PropertiesAttribute ECC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute EXF { get; set; }
public PropertiesAttribute EXE { get; set; }
public PropertiesAttribute CNN { get; set; }
public PropertiesAttribute BLN { get; set; }
public PropertiesAttribute DPC { get; set; }
public PropertiesAttribute DPN { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute IPC { get; set; }
public PropertiesAttribute IMA { get; set; }
public PropertiesAttribute ICC { get; set; }
public PropertiesAttribute IPN { get; set; }
public PropertiesAttribute IMF { get; set; }
public PropertiesAttribute IME { get; set; }
public PropertiesAttribute APC { get; set; }
public PropertiesAttribute ARN { get; set; }
public PropertiesAttribute EDI { get; set; }
public PropertiesAttribute TLV { get; set; }
public PropertiesAttribute CU { get; set; }
public PropertiesAttribute PC { get; set; }
public PropertiesAttribute DI { get; set; }
public PropertiesAttribute PI { get; set; }
public PropertiesAttribute AD { get; set; }
public PropertiesAttribute RMK { get; set; }
public PropertiesAttribute PIC { get; set; }

public SFA()
        {
SMC = new PropertiesAttribute(13, typeof(string));
APP = new PropertiesAttribute(6, typeof(string));
APN = new PropertiesAttribute(12, typeof(int));
FNC = new PropertiesAttribute(1, typeof(int));
APT = new PropertiesAttribute(4, typeof(string));
EXN = new PropertiesAttribute(70, typeof(string));
EPC = new PropertiesAttribute(9, typeof(string));
EXA = new PropertiesAttribute(35, typeof(string));
EXS = new PropertiesAttribute(35, typeof(string));
EXD = new PropertiesAttribute(35, typeof(string));
EXC = new PropertiesAttribute(35, typeof(string));
ECC = new PropertiesAttribute(2, typeof(string));
EPN = new PropertiesAttribute(20, typeof(string));
EXF = new PropertiesAttribute(20, typeof(string));
EXE = new PropertiesAttribute(210, typeof(string));
CNN = new PropertiesAttribute(35, typeof(string));
BLN = new PropertiesAttribute(35, typeof(string));
DPC = new PropertiesAttribute(5, typeof(string));
DPN = new PropertiesAttribute(35, typeof(string));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
IPC = new PropertiesAttribute(7, typeof(string));
IMA = new PropertiesAttribute(300, typeof(string));
ICC = new PropertiesAttribute(2, typeof(string));
IPN = new PropertiesAttribute(20, typeof(string));
IMF = new PropertiesAttribute(20, typeof(string));
IME = new PropertiesAttribute(210, typeof(string));
APC = new PropertiesAttribute(6, typeof(string));
ARN = new PropertiesAttribute(35, typeof(string));
EDI = new PropertiesAttribute(8, typeof(DateTime));
TLV = new PropertiesAttribute(19, typeof(int));
CU = new PropertiesAttribute(3, typeof(string));
PC = new PropertiesAttribute(70, typeof(string));
DI = new PropertiesAttribute(8, typeof(DateTime));
PI = new PropertiesAttribute(70, typeof(string));
AD = new PropertiesAttribute(750, typeof(string));
RMK = new PropertiesAttribute(996, typeof(string));
PIC = new PropertiesAttribute(300, typeof(string));
TongSoByte = 3830;
}

}public partial class EnumGroupID
    {
}

}
