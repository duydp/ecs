﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class License : IssueBase
    {
        /// <summary>
        /// Mã kiểu
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
        /// <summary>
        /// Hạn
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        /// .....
        /// </summary>
        //[XmlElement("GoodsItem")]
        //public GoodsItem GoodsItem { set; get; }
        /// <summary>
        /// Hàng trong giấy phép
        /// </summary>
        [XmlElement("GoodsItem")]
        public List<GoodsItem> GoodItems { get; set; }
        

    }
}
