
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System;
using Company.KDT.SHARE.QuanLyChungTu;
namespace Company.BLL.KDT
{
    public partial class HangMauDich
    {
        public string Ma { get; set; }
        public string Ten { get; set; }
        public string MaHSMoi { get; set; }
        public ThuTucHQTruocDo ThuTucHQTruocDo { get; set; }
        List<MienGiamThue> _MienGiamThueCollection = new List<MienGiamThue>();
        public List<MienGiamThue> MienGiamThueCollection { get { return _MienGiamThueCollection; } set { _MienGiamThueCollection = value; } }
        public void LoadMienGiamThue()
        {
            if (this.ID > 0)
                _MienGiamThueCollection = (List<MienGiamThue>)MienGiamThue.SelectCollectionBy_HMD_ID(this.ID);


        }


        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        public string NhomHang { get; set; }
        public bool Load(long tkmdID, string maHS, string maPhu, string tenhang, string nuocXXID, string dvtID, decimal soLuong, decimal donGiaKB, decimal thueXNK)
        {
            string spName = "p_KDT_HangMauDich_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tkmdID);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, maHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, tenhang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, nuocXXID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, dvtID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, soLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, donGiaKB);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, thueXNK);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) this.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) this.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) this.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) this.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) this.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) this.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) this.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) this.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) this.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) this.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) this.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) this.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) this.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) this.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) this.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) this.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGiam"))) this.ThueSuatGiam = reader.GetString(reader.GetOrdinal("ThueSuatGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) this.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) this.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) this.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) this.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) this.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) this.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("Model"))) this.Model = reader.GetString(reader.GetOrdinal("Model"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) this.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) this.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) this.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) this.ThueSuatXNKGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) this.ThueSuatTTDBGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) this.ThueSuatVATGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatVATGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueBVMT"))) this.ThueBVMT = reader.GetDecimal(reader.GetOrdinal("ThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMT"))) this.ThueSuatBVMT = reader.GetDouble(reader.GetOrdinal("ThueSuatBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMTGiam"))) this.ThueSuatBVMTGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatBVMTGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueChongPhaGia"))) this.ThueChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGia"))) this.ThueSuatChongPhaGia = reader.GetDouble(reader.GetOrdinal("ThueSuatChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"))) this.ThueSuatChongPhaGiaGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("isHangCu"))) this.isHangCu = reader.GetBoolean(reader.GetOrdinal("isHangCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueXNK"))) this.BieuThueXNK = reader.GetString(reader.GetOrdinal("BieuThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueTTDB"))) this.BieuThueTTDB = reader.GetString(reader.GetOrdinal("BieuThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueBVMT"))) this.BieuThueBVMT = reader.GetString(reader.GetOrdinal("BieuThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueGTGT"))) this.BieuThueGTGT = reader.GetString(reader.GetOrdinal("BieuThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueCBPG"))) this.BieuThueCBPG = reader.GetString(reader.GetOrdinal("BieuThueCBPG"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) this.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsHangDongBo"))) this.IsHangDongBo = reader.GetBoolean(reader.GetOrdinal("IsHangDongBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("CheDoUuDai"))) this.CheDoUuDai = reader.GetString(reader.GetOrdinal("CheDoUuDai"));

                reader.Close();
                return true;
            }
            return false;
        }

        public HangMauDich Load(long tkmdID, string maHS, string maPhu, string tenhang, string nuocXXID, string dvtID, decimal soLuong, decimal donGiaKB, SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_HangMauDich_LoadBy2";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tkmdID);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, maHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, tenhang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, nuocXXID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, dvtID);

            HangMauDich entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGiam"))) entity.ThueSuatGiam = reader.GetString(reader.GetOrdinal("ThueSuatGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatVATGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueBVMT"))) entity.ThueBVMT = reader.GetDecimal(reader.GetOrdinal("ThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMT"))) entity.ThueSuatBVMT = reader.GetDouble(reader.GetOrdinal("ThueSuatBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMTGiam"))) entity.ThueSuatBVMTGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatBVMTGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueChongPhaGia"))) entity.ThueChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGia"))) entity.ThueSuatChongPhaGia = reader.GetDouble(reader.GetOrdinal("ThueSuatChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"))) entity.ThueSuatChongPhaGiaGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("isHangCu"))) entity.isHangCu = reader.GetBoolean(reader.GetOrdinal("isHangCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueXNK"))) entity.BieuThueXNK = reader.GetString(reader.GetOrdinal("BieuThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueTTDB"))) entity.BieuThueTTDB = reader.GetString(reader.GetOrdinal("BieuThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueBVMT"))) entity.BieuThueBVMT = reader.GetString(reader.GetOrdinal("BieuThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueGTGT"))) entity.BieuThueGTGT = reader.GetString(reader.GetOrdinal("BieuThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueCBPG"))) entity.BieuThueCBPG = reader.GetString(reader.GetOrdinal("BieuThueCBPG"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsHangDongBo"))) entity.IsHangDongBo = reader.GetBoolean(reader.GetOrdinal("IsHangDongBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("CheDoUuDai"))) this.CheDoUuDai = reader.GetString(reader.GetOrdinal("CheDoUuDai"));
                
            }
            reader.Close();

            return entity;
        }

        //---------------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdateBy()
        {
            return this.InsertUpdateTransactionBy(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransactionBy(SqlTransaction transaction)
        {
            string spName = "p_KDT_HangMauDich_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@ThueSuatGiam", SqlDbType.VarChar, ThueSuatGiam);
            db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
            db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
            db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
            db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
            db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
            db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
            db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
            db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
            db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
            db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.Float, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.Float, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.Float, ThueSuatVATGiam);
            db.AddInParameter(dbCommand, "@ThueBVMT", SqlDbType.Money, ThueBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMT", SqlDbType.Float, ThueSuatBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMTGiam", SqlDbType.Float, ThueSuatBVMTGiam);
            db.AddInParameter(dbCommand, "@ThueChongPhaGia", SqlDbType.Money, ThueChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGia", SqlDbType.Float, ThueSuatChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Float, ThueSuatChongPhaGiaGiam);
            db.AddInParameter(dbCommand, "@isHangCu", SqlDbType.Bit, isHangCu);
            db.AddInParameter(dbCommand, "@BieuThueXNK", SqlDbType.NVarChar, BieuThueXNK);
            db.AddInParameter(dbCommand, "@BieuThueTTDB", SqlDbType.NVarChar, BieuThueTTDB);
            db.AddInParameter(dbCommand, "@BieuThueBVMT", SqlDbType.NVarChar, BieuThueBVMT);
            db.AddInParameter(dbCommand, "@BieuThueGTGT", SqlDbType.NVarChar, BieuThueGTGT);
            db.AddInParameter(dbCommand, "@BieuThueCBPG", SqlDbType.NVarChar, BieuThueCBPG);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@IsHangDongBo", SqlDbType.Bit, IsHangDongBo);
            db.AddInParameter(dbCommand, "@CheDoUuDai", SqlDbType.NVarChar, CheDoUuDai);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransactionBy(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_HangMauDich_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@ThueSuatGiam", SqlDbType.VarChar, ThueSuatGiam);
            db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
            db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
            db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
            db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
            db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
            db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
            db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
            db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
            db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
            db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.Float, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.Float, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.Float, ThueSuatVATGiam);
            db.AddInParameter(dbCommand, "@ThueBVMT", SqlDbType.Money, ThueBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMT", SqlDbType.Float, ThueSuatBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMTGiam", SqlDbType.Float, ThueSuatBVMTGiam);
            db.AddInParameter(dbCommand, "@ThueChongPhaGia", SqlDbType.Money, ThueChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGia", SqlDbType.Float, ThueSuatChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Float, ThueSuatChongPhaGiaGiam);
            db.AddInParameter(dbCommand, "@isHangCu", SqlDbType.Bit, isHangCu);
            db.AddInParameter(dbCommand, "@BieuThueXNK", SqlDbType.NVarChar, BieuThueXNK);
            db.AddInParameter(dbCommand, "@BieuThueTTDB", SqlDbType.NVarChar, BieuThueTTDB);
            db.AddInParameter(dbCommand, "@BieuThueBVMT", SqlDbType.NVarChar, BieuThueBVMT);
            db.AddInParameter(dbCommand, "@BieuThueGTGT", SqlDbType.NVarChar, BieuThueGTGT);
            db.AddInParameter(dbCommand, "@BieuThueCBPG", SqlDbType.NVarChar, BieuThueCBPG);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@IsHangDongBo", SqlDbType.Bit, IsHangDongBo);
            db.AddInParameter(dbCommand, "@CheDoUuDai", SqlDbType.NVarChar, CheDoUuDai);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdateBy(List<HangMauDich> collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangMauDich item in collection)
                    {
                        if (item.InsertUpdateTransactionBy(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        public void TinhThue(decimal tygiaTT)
        {
            //decimal dongia_TT = DonGiaKB*tygiaTT;
            //decimal trigiaNT = DonGiaKB*SoLuong;
            ////decimal trigiaTT_XNK = (dongia_TT*SoLuong) + (ASEAN_KhoanPhaiCong*tygiaTT) - (ASEAN_KhoanPhaiTru*tygiaTT);
            //decimal trigiaTT_XNK = this.TriGiaTT;
            //decimal tienthue_XNK = trigiaTT_XNK*ThueSuatXNK/100;
            //decimal tienthue_TTDB = (trigiaTT_XNK + tienthue_XNK)*ThueSuatTTDB/100;
            //decimal tienthue_GTGT = (trigiaTT_XNK + tienthue_XNK + tienthue_TTDB)*ThueSuatGTGT/100;
            //decimal tienthukhac = TyLeThuKhac*trigiaTT_XNK/100;


            //DonGiaTT = dongia_TT;
            //TriGiaKB = trigiaNT;
            //TriGiaTT = trigiaTT_XNK;
            //TriGiaKB_VND = trigiaNT*tygiaTT;
            //ThueXNK = tienthue_XNK;
            //ThueTTDB = tienthue_TTDB;
            //ThueGTGT = tienthue_GTGT;
            //TriGiaThuKhac = tienthukhac;
        }

        //-----------------------------------------------------------------------------------------

        //public HangMauDichInfo ExportToInfo()
        //{
        //    HangMauDichInfo entityInfo = new HangMauDichInfo();                        
        //    entityInfo.SoThuTuHang = SoThuTuHang;
        //    entityInfo.MaHS = MaHS;
        //    entityInfo.MaPhu = MaPhu;
        //    entityInfo.TenHang = TenHang;
        //    entityInfo.NuocXX_ID = NuocXX_ID;
        //    entityInfo.DVT_ID = DVT_ID;
        //    entityInfo.SoLuong = SoLuong;            
        //    entityInfo.DonGiaKB = DonGiaKB;
        //    entityInfo.DonGiaTT = DonGiaTT;
        //    entityInfo.TriGiaKB = TriGiaKB;
        //    entityInfo.TriGiaTT = TriGiaTT;
        //    entityInfo.TriGiaKB_VND = TriGiaKB_VND;
        //    entityInfo.ThueSuatXNK = ThueSuatXNK;
        //    entityInfo.ThueSuatTTDB = ThueSuatTTDB;
        //    entityInfo.ThueSuatGTGT = ThueSuatGTGT;
        //    entityInfo.ThueXNK = ThueXNK;
        //    entityInfo.ThueTTDB = ThueTTDB;
        //    entityInfo.ThueGTGT = ThueGTGT;
        //    entityInfo.PhuThu = PhuThu;
        //    entityInfo.TyLeThuKhac = TyLeThuKhac;
        //    entityInfo.TriGiaThuKhac = TriGiaThuKhac;
        //    entityInfo.MienThue = MienThue;
        //    entityInfo.MA_NPL_SP = this.MaPhu;
        //    return entityInfo;
        //}

        public List<HangMauDich> SelectCollectionBy_TKMD_ID(SqlTransaction trans, SqlDatabase db)
        {
            string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);

            List<HangMauDich> collection = new List<HangMauDich>();
            IDataReader reader = null;
            if (trans != null)
                reader = this.db.ExecuteReader(dbCommand, trans);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGiam"))) entity.ThueSuatGiam = reader.GetString(reader.GetOrdinal("ThueSuatGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatVATGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueBVMT"))) entity.ThueBVMT = reader.GetDecimal(reader.GetOrdinal("ThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMT"))) entity.ThueSuatBVMT = reader.GetDouble(reader.GetOrdinal("ThueSuatBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMTGiam"))) entity.ThueSuatBVMTGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatBVMTGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueChongPhaGia"))) entity.ThueChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGia"))) entity.ThueSuatChongPhaGia = reader.GetDouble(reader.GetOrdinal("ThueSuatChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"))) entity.ThueSuatChongPhaGiaGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("isHangCu"))) entity.isHangCu = reader.GetBoolean(reader.GetOrdinal("isHangCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueXNK"))) entity.BieuThueXNK = reader.GetString(reader.GetOrdinal("BieuThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueTTDB"))) entity.BieuThueTTDB = reader.GetString(reader.GetOrdinal("BieuThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueBVMT"))) entity.BieuThueBVMT = reader.GetString(reader.GetOrdinal("BieuThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueGTGT"))) entity.BieuThueGTGT = reader.GetString(reader.GetOrdinal("BieuThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueCBPG"))) entity.BieuThueCBPG = reader.GetString(reader.GetOrdinal("BieuThueCBPG"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("IsHangDongBo"))) entity.IsHangDongBo = reader.GetBoolean(reader.GetOrdinal("IsHangDongBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("CheDoUuDai"))) entity.CheDoUuDai = reader.GetString(reader.GetOrdinal("CheDoUuDai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<HangMauDich> SelectHang(string MaHQ, string maDN, string maLoaiHinh)
        {
            List<HangMauDich> collection = new List<HangMauDich>();

            try
            {
                const string spName = "p_SXXK_SelectHang";

                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHQ);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
                db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, maLoaiHinh);

                SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
                while (reader.Read())
                {
                    HangMauDich entity = new HangMauDich();
                    if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                    if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                    if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                    entity.MaHSMoi = string.Empty;
                    collection.Add(entity);
                }
                reader.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return collection;
        }

    }
}