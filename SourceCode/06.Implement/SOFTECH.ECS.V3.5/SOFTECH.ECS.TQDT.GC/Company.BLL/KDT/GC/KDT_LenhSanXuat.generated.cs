﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class KDT_LenhSanXuat : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HopDong_ID { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string MaHaiQuan { set; get; }
		public string SoLenhSanXuat { set; get; }
		public DateTime TuNgay { set; get; }
		public DateTime DenNgay { set; get; }
		public string SoDonHang { set; get; }
		public int TinhTrang { set; get; }
		public string GhiChu { set; get; }
        public List<KDT_LenhSanXuat_SP> SPCollection = new List<KDT_LenhSanXuat_SP>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_LenhSanXuat> ConvertToCollection(IDataReader reader)
		{
			List<KDT_LenhSanXuat> collection = new List<KDT_LenhSanXuat>();
			while (reader.Read())
			{
				KDT_LenhSanXuat entity = new KDT_LenhSanXuat();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLenhSanXuat"))) entity.SoLenhSanXuat = reader.GetString(reader.GetOrdinal("SoLenhSanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TuNgay"))) entity.TuNgay = reader.GetDateTime(reader.GetOrdinal("TuNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("DenNgay"))) entity.DenNgay = reader.GetDateTime(reader.GetOrdinal("DenNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDonHang"))) entity.SoDonHang = reader.GetString(reader.GetOrdinal("SoDonHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetInt32(reader.GetOrdinal("TinhTrang"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_LenhSanXuat> collection, long id)
        {
            foreach (KDT_LenhSanXuat item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_LenhSanXuat VALUES(@HopDong_ID, @MaDoanhNghiep, @TenDoanhNghiep, @MaHaiQuan, @SoLenhSanXuat, @TuNgay, @DenNgay, @SoDonHang, @TinhTrang, @GhiChu)";
            string update = "UPDATE t_KDT_LenhSanXuat SET HopDong_ID = @HopDong_ID, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, MaHaiQuan = @MaHaiQuan, SoLenhSanXuat = @SoLenhSanXuat, TuNgay = @TuNgay, DenNgay = @DenNgay, SoDonHang = @SoDonHang, TinhTrang = @TinhTrang, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_LenhSanXuat WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLenhSanXuat", SqlDbType.NVarChar, "SoLenhSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonHang", SqlDbType.NVarChar, "SoDonHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrang", SqlDbType.Int, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLenhSanXuat", SqlDbType.NVarChar, "SoLenhSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonHang", SqlDbType.NVarChar, "SoDonHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrang", SqlDbType.Int, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_LenhSanXuat VALUES(@HopDong_ID, @MaDoanhNghiep, @TenDoanhNghiep, @MaHaiQuan, @SoLenhSanXuat, @TuNgay, @DenNgay, @SoDonHang, @TinhTrang, @GhiChu)";
            string update = "UPDATE t_KDT_LenhSanXuat SET HopDong_ID = @HopDong_ID, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, MaHaiQuan = @MaHaiQuan, SoLenhSanXuat = @SoLenhSanXuat, TuNgay = @TuNgay, DenNgay = @DenNgay, SoDonHang = @SoDonHang, TinhTrang = @TinhTrang, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_LenhSanXuat WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLenhSanXuat", SqlDbType.NVarChar, "SoLenhSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonHang", SqlDbType.NVarChar, "SoDonHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrang", SqlDbType.Int, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLenhSanXuat", SqlDbType.NVarChar, "SoLenhSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonHang", SqlDbType.NVarChar, "SoDonHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrang", SqlDbType.Int, "TinhTrang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_LenhSanXuat Load(long id)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_LenhSanXuat> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_LenhSanXuat> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_LenhSanXuat> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_LenhSanXuat_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_LenhSanXuat_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_LenhSanXuat_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_LenhSanXuat_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_LenhSanXuat(long hopDong_ID, string maDoanhNghiep, string tenDoanhNghiep, string maHaiQuan, string soLenhSanXuat, DateTime tuNgay, DateTime denNgay, string soDonHang, int tinhTrang, string ghiChu)
		{
			KDT_LenhSanXuat entity = new KDT_LenhSanXuat();	
			entity.HopDong_ID = hopDong_ID;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaHaiQuan = maHaiQuan;
			entity.SoLenhSanXuat = soLenhSanXuat;
			entity.TuNgay = tuNgay;
			entity.DenNgay = denNgay;
			entity.SoDonHang = soDonHang;
			entity.TinhTrang = tinhTrang;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_LenhSanXuat_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@SoLenhSanXuat", SqlDbType.NVarChar, SoLenhSanXuat);
			db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.Year <= 1753 ? DBNull.Value : (object) TuNgay);
			db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.Year <= 1753 ? DBNull.Value : (object) DenNgay);
			db.AddInParameter(dbCommand, "@SoDonHang", SqlDbType.NVarChar, SoDonHang);
			db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.Int, TinhTrang);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_LenhSanXuat> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_LenhSanXuat(long id, long hopDong_ID, string maDoanhNghiep, string tenDoanhNghiep, string maHaiQuan, string soLenhSanXuat, DateTime tuNgay, DateTime denNgay, string soDonHang, int tinhTrang, string ghiChu)
		{
			KDT_LenhSanXuat entity = new KDT_LenhSanXuat();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaHaiQuan = maHaiQuan;
			entity.SoLenhSanXuat = soLenhSanXuat;
			entity.TuNgay = tuNgay;
			entity.DenNgay = denNgay;
			entity.SoDonHang = soDonHang;
			entity.TinhTrang = tinhTrang;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_LenhSanXuat_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@SoLenhSanXuat", SqlDbType.NVarChar, SoLenhSanXuat);
			db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.Year <= 1753 ? DBNull.Value : (object) TuNgay);
			db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.Year <= 1753 ? DBNull.Value : (object) DenNgay);
			db.AddInParameter(dbCommand, "@SoDonHang", SqlDbType.NVarChar, SoDonHang);
			db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.Int, TinhTrang);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_LenhSanXuat> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_LenhSanXuat(long id, long hopDong_ID, string maDoanhNghiep, string tenDoanhNghiep, string maHaiQuan, string soLenhSanXuat, DateTime tuNgay, DateTime denNgay, string soDonHang, int tinhTrang, string ghiChu)
		{
			KDT_LenhSanXuat entity = new KDT_LenhSanXuat();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.MaHaiQuan = maHaiQuan;
			entity.SoLenhSanXuat = soLenhSanXuat;
			entity.TuNgay = tuNgay;
			entity.DenNgay = denNgay;
			entity.SoDonHang = soDonHang;
			entity.TinhTrang = tinhTrang;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@SoLenhSanXuat", SqlDbType.NVarChar, SoLenhSanXuat);
			db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.Year <= 1753 ? DBNull.Value : (object) TuNgay);
			db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.Year <= 1753 ? DBNull.Value : (object) DenNgay);
			db.AddInParameter(dbCommand, "@SoDonHang", SqlDbType.NVarChar, SoDonHang);
			db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.Int, TinhTrang);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_LenhSanXuat> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_LenhSanXuat(long id)
		{
			KDT_LenhSanXuat entity = new KDT_LenhSanXuat();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_LenhSanXuat_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_LenhSanXuat> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_LenhSanXuat item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
        public static long GetID(string SoLenhSanXuat ,long HopDong_ID)
        {
            string spName = "SELECT TOP 1 ID FROM dbo.t_KDT_LenhSanXuat WHERE SoLenhSanXuat = '" + SoLenhSanXuat + "' AND HopDong_ID = " + HopDong_ID + "";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return 0;
            else
                return (long)obj;
        }
        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (KDT_LenhSanXuat_SP item in SPCollection)
                    {
                        long id = 0;
                        item.LenhSanXuat_ID = this.ID;
                        if (item.ID == 0)
                        {
                            id = item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

	}	
}