//Lypt created
//Date 07/01/2010
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;

namespace Company.Interface.Report.KDDT
{
    public partial class ToKhaiHQDT : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public string PTVT_Name = "";
        public string PTTT_Name = "";
        public string DKGH = "";
        public string DongTienTT = "";
        public string NuocXK = "";
        public ToKhaiHQDT()
        {
            InitializeComponent();
        }
        public void BindData()
        {
            DateTime minDate = new DateTime(1900, 1, 1);            
            //BindData of Detail
            lblSTT.DataBindings.Add("Text", this.DataSource, "SoThuTuHang");
            lblTenHang.DataBindings.Add("Text", this.DataSource, "TenHang");
            lblMaSoHH.DataBindings.Add("Text", this.DataSource, "MaHS");
            lblXuatXu.DataBindings.Add("Text", this.DataSource, "XuatXu");
            lblSoLuong.DataBindings.Add("Text", this.DataSource, "SoLuong");
            lblDonViTinh.DataBindings.Add("Text", this.DataSource, "DonViTinh");
            lblDonGiaNT.DataBindings.Add("Text", this.DataSource, "DonGiaTT", "{0:N2}");
            lblTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGiaTT", "{0:N2}");

            //BindData header
            lblCangDoHang.Text = TKMD.TenDaiLyTTHQ.ToString();
            lblCangXepHang.Text = TKMD.DiaDiemXepHang.ToString();
            //lblChiCucHQ.Text
            //lblChiCucHQCK.Text
            lblDieuKienGiaoHang.Text = DKGH;
            lblDongTienThanhToan.Text = DongTienTT;
            lblGiayPhep.Text = TKMD.SoGiayPhep;
            lblHoaDonThuongMai.Text = TKMD.SoHoaDonThuongMai;
            lblHopDong.Tag = TKMD.SoHopDong;
            lblLoaiHinh.Text =  TKMD.LoaiVanDon;
            if (TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = TKMD.NgayDangKy.ToString();
            if (TKMD.NgayDangKy > minDate)
                lblNgayGiayPhep.Text = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            if (TKMD.NgayDangKy > minDate)
                lblNgayGui.Text = TKMD.NgayTiepNhan.ToString();
            if (TKMD.NgayDangKy > minDate)
                lblNgayHetHanGiayPhep.Text = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            if (TKMD.NgayDangKy > minDate)
                lblNgayHetHanHopDong.Text = TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            if (TKMD.NgayDangKy > minDate)
                lblNgayHoaDon.Text = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
            if (TKMD.NgayDangKy > minDate)
                lblNgayHopDong.Text = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            if (TKMD.NgayDangKy > minDate)
                lblNgayVanTaiDon.Text = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
            lblNguoiNK.Text = TKMD.TenChuHang;
            lblNguoiUyThac.Text = TKMD.TenDonViUT;
            lblNguoiXK.Text = TKMD.TenDonViDoiTac;
            lblNuocXK.Text = NuocXK;
            lblPhuongThucThanhToan.Text = PTTT_Name;
            lblPhuongTienVanTai.Text = PTVT_Name;
            //lblThamChieu.Text
            lblToKhai.Text = TKMD.SoToKhai.ToString();
            lblVanTaiDon.Text = TKMD.LoaiVanDon;
            lblTyGiaTinhThue.Text = TKMD.TyGiaTinhThue.ToString();
            lblHuongDan.Text = TKMD.HUONGDAN;
            string ctu = "";
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                ctu += ct.TenChungTu + ", ";
            }
            lblChungTu.Text = ctu;

        }
    }
}
