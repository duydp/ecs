﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class DongBoDuLieu
    {
        public static DongBoDuLieu Load(long tKMD_ID, int soToKhai, int namDK, string maHaiQuan, string maDoanhNghiep)
        {
            List<DongBoDuLieu> collection = new List<DongBoDuLieu>();

            string where = string.Format("TKMD_ID = {0} AND SoToKhai = {1} AND NamDK = {2} AND MaHaiQuan = '{3}' AND MaDoanhNghiep = '{4}'", tKMD_ID, soToKhai, namDK, maHaiQuan, maDoanhNghiep);

            collection = SelectCollectionDynamic(where, "");

            return collection.Count > 0 ? collection[0] : new DongBoDuLieu();
        }
    }
}
