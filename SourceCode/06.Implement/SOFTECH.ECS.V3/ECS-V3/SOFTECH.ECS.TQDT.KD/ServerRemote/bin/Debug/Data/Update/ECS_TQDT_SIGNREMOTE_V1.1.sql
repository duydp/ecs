GO
 IF OBJECT_ID(N'[dbo].[t_HaiQuan_Version]') IS NOT NULL
	DROP TABLE [dbo].[t_HaiQuan_Version]
GO
CREATE TABLE [dbo].[t_HaiQuan_Version]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Version] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date] [datetime] NOT NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[t_HaiQuan_Version] ADD CONSTRAINT [PK_t_HaiQuan_Version] PRIMARY KEY CLUSTERED ([ID]) ON [PRIMARY]
GO

INSERT dbo.t_HaiQuan_Version
        ( Version, Date, Notes )
VALUES  ( '1.0', -- Version - varchar(10)
          GETDATE(), -- Date - datetime
          N'CẬP NHẬT DỮ LIỆU MẶC ĐỊNH PHIÊN BẢN '  -- Notes - nvarchar(255)
          )

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '1.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('1.1',GETDATE(), N'CẬP NHẬT TABLE VERSION CHỮ KÝ SỐ')
END