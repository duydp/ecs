﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
namespace Company.Interface.KDT.GC
{
    public partial class NPLCungUngDetailForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public NPLCungUng nplCungUng = new NPLCungUng();
        private NPLCungUngDetail nplCungUngDetail = new NPLCungUngDetail();

        bool isEdit = false;
        public NPLCungUngDetailForm()
        {
            InitializeComponent();
        }
        private void KhoiTaoDuLieuChuan()
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            txtMaNPL.Text = nplCungUng.MaHang;
            txtTenNPL.Text = nplCungUng.TenHang;
            ctrMaLoaiHinh.Nhom = "XGC";
            ccNgayChungTu.Value = DateTime.Now;
            SetCommandStatus();

        }
        private void SetCommandStatus()
        {
            bool isEnable = TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET
                || TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            btnAddNew.Enabled = btnGhi.Enabled = btnXoa.Enabled = isEnable;
            dgList.AllowDelete = isEnable ? InheritableBoolean.True : InheritableBoolean.False;
        }
        private void Set()
        {
            try
            {
                ctrMaLoaiHinh.Ma = (string.IsNullOrEmpty(nplCungUngDetail.MaLoaiHinh) == false ? nplCungUngDetail.MaLoaiHinh.Trim() : TKMD.MaLoaiHinh);
                ctrMaHaiQuan.Ma = GlobalSettings.MA_HAI_QUAN;
                if (nplCungUngDetail.ID == 0)
                {
                    cbHinhThuc.SelectedIndex = 0;
                    cbDonViTinh.SelectedIndex = 0;
                }
                else
                {
                    cbHinhThuc.SelectedValue = nplCungUngDetail.HinhThuc;
                    cbDonViTinh.SelectedValue = nplCungUngDetail.DVT_ID;
                }
                txtSoHDon.Text = nplCungUngDetail.SoChungTu;
                ccNgayChungTu.Value = nplCungUngDetail.NgayChungTu;
                txtNguoiCap.Text = nplCungUngDetail.NoiPhatHanh;
                txtSoLuong.Value = nplCungUngDetail.SoLuong;
                txtTyLeQuyDoi.Value = nplCungUngDetail.TyLeQuyDoi;
                txtDongTrenChungTu.Value = nplCungUngDetail.DongHangTrenCT;
                txtGhiChu.Text = nplCungUngDetail.GhiChu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Get()
        {
            if (!isEdit)
                nplCungUngDetail = new NPLCungUngDetail();
            nplCungUngDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            nplCungUngDetail.MaLoaiHinh = ctrMaLoaiHinh.Ma;
            nplCungUngDetail.MaHaiQuan = ctrMaHaiQuan.Ma;
            nplCungUngDetail.HinhThuc = Convert.ToInt32(cbHinhThuc.SelectedValue.ToString().Trim());
            nplCungUngDetail.SoChungTu = txtSoHDon.Text.Trim();
            nplCungUngDetail.NgayChungTu = ccNgayChungTu.Value;
            nplCungUngDetail.NoiPhatHanh = txtNguoiCap.Text.Trim();
            nplCungUngDetail.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
            nplCungUngDetail.TyLeQuyDoi = Convert.ToDecimal(txtTyLeQuyDoi.Value);
            nplCungUngDetail.DongHangTrenCT = Convert.ToInt32(txtDongTrenChungTu.Value);
            nplCungUngDetail.GhiChu = txtGhiChu.Text.Trim();

            /*
             nplCungUng.DVT_ID = cbDonViTinh.SelectedValue.ToString();
             nplCungUng.MaHang = txtMaHang.Text.Trim();
             nplCungUng.TenHang = txtTenNPL.Text.Trim();
             nplCungUng.MaHS = txtMaHS.Text.Trim();
             nplCungUng.TongLuong = Convert.ToDouble(txtSoLuong.Value);
             * */


        }
        private void NPLCungUngForm_Load(object sender, EventArgs e)
        {
            KhoiTaoDuLieuChuan();
            dgList.DataSource = nplCungUng.NPLCungUngDetails;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["HinhThuc"].Value != null)
                e.Row.Cells["HinhThuc"].Text = Convert.ToInt32(e.Row.Cells["HinhThuc"].Value) == 1 ? "Nội địa" :
                    "Nhập khẩu";
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            error.Clear();
            bool isValid = ValidateControl.ValidateNull(cbHinhThuc, error, "Hình thức cung ứng");
            isValid &= ValidateControl.ValidateNull(txtSoHDon, error, "Số chứng từ");
            isValid &= ValidateControl.ValidateNull(ccNgayChungTu, error, "Ngày chứng từ");
            isValid &= ValidateControl.ValidateNull(txtTyLeQuyDoi, error, "Tỉ lệ quy đổi");

            if (Convert.ToDouble(txtSoLuong.Value) <= 0)
            {
                error.SetError(txtSoLuong, "Tổng số lượng không được để trống");
                isValid = false;
            }
            
            if (!isValid) return;
            Get();
            if (!isEdit)
            {
                nplCungUng.NPLCungUngDetails.Add(nplCungUngDetail);
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            isEdit = false;
            btnAddNew_Click(null, null);
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            nplCungUngDetail = new NPLCungUngDetail();
            Set();
            isEdit = false;
        }


        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                if (dgList.GetRow() == null) return;
                else nplCungUngDetail = (NPLCungUngDetail)dgList.GetRow().DataRow;
                if (nplCungUngDetail.ID > 0)
                {
                    nplCungUngDetail.Delete();
                }
                nplCungUng.NPLCungUngDetails.Remove(nplCungUngDetail);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.DataRow == null) return;
            nplCungUngDetail = (NPLCungUngDetail)e.Row.DataRow;
            isEdit = true;
            Set();
        }

        private void btnChungTuHQTruocDo_Click(object sender, EventArgs e)
        {
            if (nplCungUngDetail.ID == 0)
                ShowMessage("NPL tự cung ứng chưa được phân bổ", false);
            else
            {
                ListChungTuTruocDoForm f = new ListChungTuTruocDoForm();
                f.Master_ID = nplCungUngDetail.ID;
                f.Type = "NPLCU";
                f.ShowDialog(this);
            }
        }
    }
}
