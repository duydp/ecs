﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
#if KD_V3
using Company.KD.BLL;
#elif SXXK_V3
using Company.BLL;
#elif GC_V3
using Company.GC.BLL;
#endif
using System.Text;
using System.Security.Cryptography;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;

namespace Company.Interface.TTDaiLy
{
    public partial class FrmInfo : BaseForm
    {
        public ThongTinDaiLy DLInfo = new ThongTinDaiLy();
        public bool isSuccess = false;
        public int trangthai = 0;
        public FrmInfo()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            #region validate
            if (string.IsNullOrEmpty(txtMaDN.Text))
            {
                errorProvider1.SetError(txtMaDN, "Mã đại lý không được để trống");
                return;
            }
            if (string.IsNullOrEmpty(txtTenDN.Text))
            {
                errorProvider1.SetError(txtTenDN, "Tên đại lý không được để trống");
                return;
            }
            if (string.IsNullOrEmpty(txtDiaChi.Text))
            {
                errorProvider1.SetError(txtDiaChi, "Địa chỉ đại lý không được để trống");
                return;
            }
            if (string.IsNullOrEmpty(txtDienThoaiDN.Text))
            {
                errorProvider1.SetError(txtDienThoaiDN, "Điện thoại liên lạc đại lý không được để trống");
                return;
            }
            if (string.IsNullOrEmpty(txtMailDoanhNghiep.Text))
            {
                errorProvider1.SetError(txtMailDoanhNghiep, "Email liên lạc đại lý không được để trống");
                return;
            }
            if (string.IsNullOrEmpty(txtNguoiLienHeDN.Text))
            {
                errorProvider1.SetError(txtNguoiLienHeDN, "Người liên hệ không được để trống");
                return;
            }
            #endregion
            
            get();
            if (trangthai == 1)
            {
                string sql = string.Format(" [role]=1");
                List<HeThongPhongKhai> listhtpk = HeThongPhongKhai.SelectCollectionDynamic(sql, null);
                if (listhtpk != null && listhtpk.Count > 0)
                    HeThongPhongKhai.DeleteCollection(listhtpk);
            }
            if (HeThongPhongKhai.InsertThongTinDL(DLInfo))
            {
                MessageBox.Show("Cập nhật thành công");
                isSuccess = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Cập nhật thất bại, vui lòng kiểm tra lại dữ liệu");
            }
           
        }
        private void set()
        {
            if (DLInfo != null)
            {
                txtMaDN.Text = DLInfo.MaDL;
                txtTenDN.Text = DLInfo.TenDL;
                txtSoFaxDN.Text = DLInfo.FaxDL;
                txtDienThoaiDN.Text = DLInfo.SoDienThoaiDL;
                txtNguoiLienHeDN.Text = DLInfo.NguoiLienHeDL;
                txtDiaChi.Text = DLInfo.DiaChiDL;
                txtMailDoanhNghiep.Text = DLInfo.EmailDL;
            }
        }
        private void get()
        {
            
            DLInfo.MaDL = txtMaDN.Text.Trim();
            DLInfo.TenDL = txtTenDN.Text.Trim();
            DLInfo.DiaChiDL = txtDiaChi.Text.Trim();
            DLInfo.SoDienThoaiDL = txtDienThoaiDN.Text.Trim();
            DLInfo.FaxDL = txtSoFaxDN.Text.Trim();
            DLInfo.NguoiLienHeDL = txtNguoiLienHeDN.Text.Trim();
            DLInfo.EmailDL = txtMailDoanhNghiep.Text.Trim();
        }

        private void FrmInfo_Load(object sender, EventArgs e)
        {
            DLInfo = HeThongPhongKhai.GetThongTinDL();
            if (DLInfo == null)
            {
                DLInfo = new ThongTinDaiLy();
            }
            else
                set();
        }

        

    }
}