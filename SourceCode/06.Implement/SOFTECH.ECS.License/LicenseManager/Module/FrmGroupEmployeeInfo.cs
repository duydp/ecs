using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmGroupEmployeeInfo : DevExpress.XtraEditors.XtraForm
    {
        private Group group;
        public bool isEdit = false;

        public FrmGroupEmployeeInfo(string ID)
        {
            InitializeComponent();
            if (ID == null)
            {
                txtGroupID.Text = Math.Abs(DateTime.Now.GetHashCode()).ToString();
                this.Text = "Thêm nhóm nhân viên: " + txtGroupID.Text;
                lblCaption.Text = "Thêm nhóm nhân viên: " + txtGroupID.Text;
                group = new Group();
                this.Name = "Group_New";
                this.isEdit = true;
            }
            else
            {
                txtGroupID.Text = ID;
                this.Text = "Nhóm nhân viên: " + txtGroupID.Text;
                lblCaption.Text = "Nhóm nhân viên: " + txtGroupID.Text;
                group = Group.Load(Convert.ToInt32(ID));
                this.Name = "Group_" + ID;
                this.FillData();
            }
        }

        #region Add Methods.
        private void GetData()
        {
            group.ID = Convert.ToInt32(txtGroupID.Text);
            group.Name = txtGroupName.Text;
            group.Description = txtGroupDesc.Text;
        }

        private void FillData()
        {
            txtGroupID.Text = group.ID.ToString();
            txtGroupName.Text = group.Name;
            txtGroupDesc.Text = group.Description;
        }

        private void DisableButton(Boolean val)
        {
            btnCancel.Enabled = val;
            btnSave.Enabled = val;
            groupGroupInfo.Enabled = val;
            btnEdit.Enabled = !val;
        }
        #endregion

        private void FrmGroupuctInfo_Load(object sender, EventArgs e)
        {
            this.DisableButton(this.isEdit);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.Name == "Group_New")
            {
                this.Close();
            }
            else
            {
                this.FillData();
                this.DisableButton(false);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.isEdit = true;
            this.DisableButton(true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.GetData();
            if (this.Name == "Group_New")
            {
                group.Insert();
                this.Text = "Nhóm nhân viên: " + group.ID;
                lblCaption.Text = "Nhóm nhân viên: " + group.ID;
                this.Name = "Group_" + group.ID;
            }
            else
            {
                group.Update();
            }
            this.DisableButton(false);
            if (ClassLib.IsFormShowed("FrmGroupEmployeeManager"))
            {
                FrmGroupEmployeeManager obj = (FrmGroupEmployeeManager)Application.OpenForms["FrmGroupEmployeeManager"];
                obj.RefreshData();
            }
        }
    }
}