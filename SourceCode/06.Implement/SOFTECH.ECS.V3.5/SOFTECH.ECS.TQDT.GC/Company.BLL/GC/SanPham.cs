﻿using System;
using System.Data;
using Company.GC.BLL.Utils;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.GC
{
    public partial class SanPham
    {
        public SanPham Load(string MaNPL, long HopDong_ID)
        {
            SanPham entity = null;
            string where = "HopDong_ID = " + HopDong_ID + " AND Ma='" + MaNPL + "'";
            IDataReader reader = SelectReaderDynamic(where, "");
            if (reader.Read())
            {
                entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetInt64(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) entity.SoLuongDaXuat = reader.GetInt64(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) entity.NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetInt64(reader.GetOrdinal("DonGia"));
            }
            reader.Close();

            return entity;
        }
        public decimal GetTongSoSanPhamInHopDong(long IDHopDong)
        {
            string sql = " select Sum(SoLuongDaXuat )as SoLuongXuat  from t_GC_SanPham where HopDong_ID = @HopDong_ID  ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            decimal soLuong = 0;
            try
            {
                soLuong = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch { }
            return soLuong;
        }

        public DataSet BaoCaoBC02HSTK_GC_TT117(long idHD, string MaHaiQuan, string MaDoanhNghiep)
        {
            //int maxRow = 0;
            string spName = "p_GC_BC02HSTK_GC_TT117";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            //spName = "p_GC_BC02HSTK_GC_TT117_Tong";
            dbCommand.CommandTimeout = 60000;
            DataSet ds = this.db.ExecuteDataSet(dbCommand);
            
            //dbCommand.CommandText = spName;

            //DataSet ds1 = this.db.ExecuteDataSet(dbCommand);
            //ds.Tables[0].TableName = "Trang 1";
            //ds1.Tables[0].TableName = "Trang 2";
            //DataTable dt = ds1.Tables[0].Clone();
            //maxRow = ds.Tables[0].Rows.Count;
            //dt = ds1.Tables[0].Copy();
            //foreach (DataRow item in dt.Rows)
            //{
            //    item["Stt"] = int.Parse(item["Stt"].ToString()) + maxRow;
            //}
            //ds.Tables.Add(dt);
            return ds;
        }
        public DataSet BaoCaoBC02HSTK_GC_TT117_Export(long idHD, string MaHaiQuan, string MaDoanhNghiep)
        {
            //int maxRow = 0;
            string spName = "p_GC_BC02HSTK_GC_TT117_Export";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            //spName = "p_GC_BC02HSTK_GC_TT117_Tong";
            dbCommand.CommandTimeout = 60000;
            DataSet ds = this.db.ExecuteDataSet(dbCommand);

            //dbCommand.CommandText = spName;

            //DataSet ds1 = this.db.ExecuteDataSet(dbCommand);
            //ds.Tables[0].TableName = "Trang 1";
            //ds1.Tables[0].TableName = "Trang 2";
            //DataTable dt = ds1.Tables[0].Clone();
            //maxRow = ds.Tables[0].Rows.Count;
            //dt = ds1.Tables[0].Copy();
            //foreach (DataRow item in dt.Rows)
            //{
            //    item["Stt"] = int.Parse(item["Stt"].ToString()) + maxRow;
            //}
            //ds.Tables.Add(dt);
            return ds;
        }
        public DataSet BaoCaoBC02HSTK_GC_TT38(long idHD, string MaHaiQuan, string MaDoanhNghiep,DateTime tuNgay,DateTime denNgay)
        {
            //int maxRow = 0;
            string spName = "p_GC_BC02HSTK_GC_TT38";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, tuNgay);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, denNgay);
            //spName = "p_GC_BC02HSTK_GC_TT117_Tong";
            dbCommand.CommandTimeout = 60000;
            DataSet ds = new DataSet();
            try
            {
                ds = this.db.ExecuteDataSet(dbCommand);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
            //dbCommand.CommandText = spName;

            //DataSet ds1 = this.db.ExecuteDataSet(dbCommand);
            //ds.Tables[0].TableName = "Trang 1";
            //ds1.Tables[0].TableName = "Trang 2";
            //DataTable dt = ds1.Tables[0].Clone();
            //maxRow = ds.Tables[0].Rows.Count;
            //dt = ds1.Tables[0].Copy();
            //foreach (DataRow item in dt.Rows)
            //{
            //    item["Stt"] = int.Parse(item["Stt"].ToString()) + maxRow;
            //}
            //ds.Tables.Add(dt);
            return ds;
        }
        public DataSet BaoCaoBC02HSTK_GC_TT117_New(long idHD, string MaHaiQuan, string MaDoanhNghiep)
        {
            //int maxRow = 0;
            string spName = "p_GC_BC02HSTK_GC_TT117_New";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            //spName = "p_GC_BC02HSTK_GC_TT117_Tong";
            dbCommand.CommandTimeout = 60000;
            DataSet ds = this.db.ExecuteDataSet(dbCommand);
//             dbCommand.CommandText = spName;
// 
//             DataSet ds1 = this.db.ExecuteDataSet(dbCommand);
//             ds.Tables[0].TableName = "Trang 1";
//             ds1.Tables[0].TableName = "Trang 2";
//             DataTable dt = ds1.Tables[0].Clone();
//             maxRow = ds.Tables[0].Rows.Count;
//             dt = ds1.Tables[0].Copy();
//             foreach (DataRow item in dt.Rows)
//             {
//                 item["Stt"] = int.Parse(item["Stt"].ToString()) + maxRow;
//             }
//             ds.Tables.Add(dt);
            return ds;
        }




        public string GetDVTByMa(string Ma, long IDHopDong)
        {
            string sql = "Select DVT_ID from t_GC_SanPham where Ma = @Ma AND HopDong_ID = @HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            return db.ExecuteScalar(dbCommand).ToString();
        }

        public decimal GetTongSoSanPhamDangKyInHopDong(long IDHopDong)
        {
            string sql = " select Sum(SoLuongDangKy )as SoLuongDangKy  from t_GC_SanPham where HopDong_ID = @HopDong_ID  ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            decimal soLuong = 0;
            try
            {
                soLuong = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch { }
            return soLuong;
        }

        public DataSet GetSanPham(long IDHopDong)
        {
            string sql = " SELECT * "
                         + " FROM t_GC_SanPham  "
                         + " Where Ma in ( select  distinct MaSanPham from t_GC_DinhMuc Where  HopDong_ID = @HopDong_ID) AND (HopDong_ID = @HopDong_ID) AND SoLuongDaXuat >0";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetSP(long IDHopDong, string Ma)
        {
            string sql = " SELECT Ten, DVT_ID"
                         + " FROM t_GC_SanPham  "
                         + " Where Ma = @Ma AND HopDong_ID = @HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.NVarChar, Ma);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        // duydp 20/10/2015 Lấy tên Sản phẩm đúng với Sản phẩm đã đăng ký trong hợp đồng khi nhập hàng hóa bằng File Excel  
        public string GetTenSP(long IDHopDong, string MaHS,string MaHangHoa)
        {
            string sql = " SELECT Ten"
                         + " FROM t_GC_SanPham  "
                         + " Where Ma = @Ma AND HopDong_ID = @HopDong_ID AND MaHS=@MaHS";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.NVarChar, MaHangHoa);
            return db.ExecuteScalar(dbCommand).ToString();
            //object obj = dbCommand.ExecuteScalar();
            //return (String)obj;
        }

        public DataSet GetSoLuongSanPhamByHDAndMaSP(long IDHopDong, string Ma)
        {
            string sql = " SELECT SoLuongDaXuat,DVT_ID,Ma "
          + " FROM t_GC_SanPham  "
          + " Where Ma in ( select MaSanPham from t_GC_DinhMuc Where  HopDong_ID = @HopDong_ID AND MaSanPham =@Ma) AND (HopDong_ID = @HopDong_ID) ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public decimal GetSoLuongSanPhamByHDAndMaSPDC(long IDHopDong, string Ma)
        {
            decimal temp = 0;
            string sql = " SELECT SoLuongDaXuat,DVT_ID,Ma "
          + " FROM t_GC_SanPham  "
          + " Where HopDong_ID = @HopDong_ID AND Ma =@Ma ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return temp;

        }

        public SanPhamCollection SelectCollectionBy_HopDong_ID_ChuaCoDM()
        {
            string spName = "select * from t_GC_SanPham where Ma not in " +
                            "(select distinct MaSanPham from t_GC_DinhMuc where HopDong_ID=@HopDong_ID AND MaSanPham NOT IN " +
                            "(SELECT DISTINCT MaSanPham FROM t_KDT_GC_DinhMuc WHERE Master_ID IN (SELECT ID FROM t_KDT_GC_DinhMucDangKy WHERE TrangThaiXuLy <0 AND ID_HopDong = @HopDong_ID ))) and HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            SanPhamCollection collection = new SanPhamCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                SanPham entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) entity.SoLuongDaXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) entity.NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public int UpdateMaSPTransaction(SqlTransaction transaction, string MaCu)
        {
            string spName = "update t_GC_SanPham set ma=@MaMoi,TrangThai=0 where HopDong_ID=@HopDong_ID and ma=@MaCu";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaMoi", SqlDbType.VarChar, this.Ma);
            this.db.AddInParameter(dbCommand, "@MaCu", SqlDbType.VarChar, MaCu);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public bool Load(SqlTransaction tran)
        {
            string spName = "p_GC_SanPham_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand, tran);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) this._SoLuongDaXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) this._NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool Load(string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_SanPham_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) this._SoLuongDaXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) this._NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public SanPhamCollection SelectCollectionDaDuyetBy_HopDong_ID()
        {
            string sql = "select * from t_GC_SanPham where trangthai=0 and HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            SanPhamCollection collection = new SanPhamCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                SanPham entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) entity.SoLuongDaXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) entity.NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public SanPhamCollection SelectCollectionDaDuyetVaBoSungBy_HopDong_ID()
        {
            string sql = "select * from t_GC_SanPham where trangthai=0 and HopDong_ID=@HopDong_ID or trangthai=2 and HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            SanPhamCollection collection = new SanPhamCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                SanPham entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) entity.SoLuongDaXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) entity.NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public bool LoadKTX()
        {
            string spName = "p_GC_SanPham_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) this._SoLuongDaXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) this._NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //Update by HUNGTQ 06/05/2011.
        public bool LoadKTX(string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_SanPham_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) this._SoLuongDaXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) this._NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_GC_SanPham_Insert_ByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaXuat", SqlDbType.Decimal, this._SoLuongDaXuat);
            this.db.AddInParameter(dbCommand, "@NhomSanPham_ID", SqlDbType.VarChar, this._NhomSanPham_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        public long InsertTransactionTQDT(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_SanPham_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaXuat", SqlDbType.Decimal, this._SoLuongDaXuat);
            this.db.AddInParameter(dbCommand, "@NhomSanPham_ID", SqlDbType.VarChar, this._NhomSanPham_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        public int InsertUpdateTransactionKTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_SanPham_InsertUpdateByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaXuat", SqlDbType.Decimal, this._SoLuongDaXuat);
            this.db.AddInParameter(dbCommand, "@NhomSanPham_ID", SqlDbType.VarChar, this._NhomSanPham_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_SanPham_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaXuat", SqlDbType.Decimal, this._SoLuongDaXuat);
            this.db.AddInParameter(dbCommand, "@NhomSanPham_ID", SqlDbType.VarChar, this._NhomSanPham_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression, string databaseName)
        {
            string spName = "p_GC_SanPham_SelectDynamic";

            //Updated by HUNGTQ, 06/05/2011.
            SetDabaseMoi(databaseName);

            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        public int DeleteBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_GC_SanPham_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        public SanPhamCollection SelectCollectionBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_SanPham_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            SanPhamCollection collection = new SanPhamCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                SanPham entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) entity.SoLuongDaXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) entity.NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public SanPhamCollection SelectCollectionBy_HopDong_IDKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_SanPham_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            SanPhamCollection collection = new SanPhamCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                SanPham entity = new SanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaXuat"))) entity.SoLuongDaXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSanPham_ID"))) entity.NhomSanPham_ID = reader.GetString(reader.GetOrdinal("NhomSanPham_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public int UpdateTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_SanPham_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaXuat", SqlDbType.Decimal, this._SoLuongDaXuat);
            this.db.AddInParameter(dbCommand, "@NhomSanPham_ID", SqlDbType.VarChar, this._NhomSanPham_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_SanPham_UpdateByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaXuat", SqlDbType.Decimal, this._SoLuongDaXuat);
            this.db.AddInParameter(dbCommand, "@NhomSanPham_ID", SqlDbType.VarChar, this._NhomSanPham_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
    }
}