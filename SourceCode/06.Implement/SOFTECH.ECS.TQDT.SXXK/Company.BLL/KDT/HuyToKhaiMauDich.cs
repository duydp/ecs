﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;


namespace Company.BLL.KDT
{
    public partial class HuyToKhai
    {
        #region Huy va chinh sua to khai da duyet

        public string HuyToKhaiDaDuyet(string pass,ToKhaiMauDich tkmd)
        {
            XmlDocument doc = new XmlDocument();
            if (tkmd.MaLoaiHinh.StartsWith("N"))
                doc.LoadXml(tkmd.ConfigPhongBi(MessgaseType.ToKhaiNhap, MessgaseFunction.KhaiBao));
            else
                doc.LoadXml(tkmd.ConfigPhongBi(MessgaseType.ToKhaiXuat, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram ();
            docNPL.Load(path + @"\XMLChungTu\HuyTKDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            //root.SelectSingleNode("SoTKHQTC").InnerText = tkmd.SoTiepNhan.ToString();

            root.SelectSingleNode("ToKhai").Attributes["MaLH"].Value = tkmd.MaLoaiHinh;
            root.SelectSingleNode("ToKhai").Attributes["MaHQ"].Value = tkmd.MaHaiQuan;

            root.SelectSingleNode("ToKhai").Attributes["SoTK"].Value = tkmd.SoToKhai + "";
            root.SelectSingleNode("ToKhai").Attributes["NgayDK"].Value = tkmd.NgayDangKy.ToString("MM/dd/yyyy"); ;
            root.SelectSingleNode("ToKhai").Attributes["NamDK"].Value = tkmd.NgayDangKy.Year + "";

            root.SelectSingleNode("DoanhNghiep").Attributes["MaDN"].Value = tkmd.MaDoanhNghiep.ToString();
            root.SelectSingleNode("DoanhNghiep").Attributes["TenDN"].Value = tkmd.MaDoanhNghiep;

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            WS.KhaiDienTu.KDTService kdt = new WS.KhaiDienTu.KDTService();
            kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(Company.BLL.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";
        }

        public string ConfigPhongBi(int type, int function, ToKhaiMauDich tkmd)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProram();
            doc.Load(path + @"\TemplateXML\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(tkmd.MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = tkmd.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            string Guid = (System.Guid.NewGuid().ToString()); ;
            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            if(this.Guid=="")
                this.Guid = Guid;
            nodeReference.InnerText = this.Guid;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = Guid;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = tkmd.MaDoanhNghiep;
            this.Update();
            return doc.InnerXml;

        }

        public string LayPhanHoi(string pass, ToKhaiMauDich tkmd)
        {
            XmlDocument doc = new XmlDocument();
            if (tkmd.MaLoaiHinh.StartsWith("N"))
                doc.LoadXml(ConfigPhongBi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, tkmd));
            else
                doc.LoadXml(ConfigPhongBi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, tkmd));
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = tkmd.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = tkmd.MaDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = tkmd.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = tkmd.MaHaiQuan;

            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.Guid;

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            WS.KhaiDienTu.KDTService kdt = new Company.BLL.WS.KhaiDienTu.KDTService();
            kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "no")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(Company.BLL.Utils.FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        #endregion Huy va chinh sua to khai da duyet
    }
}