﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe06_HSTK_TT38_Tong : DevExpress.XtraReports.UI.XtraReport
    {
        //public Report.ReportViewBC06Form report = new ReportViewBC06Form();
        public HopDong HD = new HopDong();
        public DataTable dsBK = new DataTable();
        public DataTable dtDM_SP = new DataTable();
        decimal tongluong = 0;
        decimal tongLuongXuatTra = 0;
        decimal tongLuongNK = 0;
        decimal tongLuongXK = 0;
        decimal tongLuongChenhLech = 0;
        int index = 1;
        public BangKe06_HSTK_TT38_Tong()
        {
            InitializeComponent();
        }
        public static DataTable GetLuongCungUngOrHuy(DataTable dsBK,DataTable dtDM_SP,HopDong HD, bool isCU)
        {
            // DataTable dt = new Company.GC.BLL.GC.ThanhKhoanHDGC().getDuLieuThanhKhoan(this.HD.ID).Tables[0];
            DataTable dt = dsBK.Clone();
            if (!dt.Columns.Contains("LuongNhap_DauKy")) 
            {
                DataColumn dc1 = new DataColumn("LuongNhap_DauKy");
                dc1.DataType = typeof(System.Decimal);
                DataColumn dc2 = new DataColumn("LuongNhap_TrongKy");
                dc2.DataType = typeof(System.Decimal);
                DataColumn dc3 = new DataColumn("LuongXuat_DauKy");
                dc3.DataType = typeof(System.Decimal);
                DataColumn dc4 = new DataColumn("LuongXuat_TrongKy");
                dc4.DataType = typeof(System.Decimal);
                DataColumn dc5 = new DataColumn("LuongTonCuoi");
                dc4.DataType = typeof(System.Decimal);
                dt.Columns.Add(dc1);
                dt.Columns.Add(dc2);
                dt.Columns.Add(dc3);
                dt.Columns.Add(dc4);
                dt.Columns.Add(dc5);
            }
            

            DataTable tbNPL = dsBK.DefaultView.ToTable(true, "HopDong_ID", "Ma", "Ten", "DVT_ID", "SoLuongDaNhap", "SoLuongCungUng", "SoLuongDaDung", "LuongTon");
            int stt = 1;
            foreach (DataRow dr in tbNPL.Rows)
            {
                decimal luongNK_DauKy = 0;
                decimal luongNK_TrongKy = 0;
                decimal luongXK_DauKy = 0;
                decimal luongXK_TrongKy = 0;
                //decimal luongSD = 0;
                decimal luongTon = 0;
                decimal luongXT = 0;
                //decimal luongHuy = 0;
                try
                {
                    //tính lượng nhập NPL
                    foreach (DataRow drNPL in dsBK.Rows)
                    {
                        if (dr["Ma"].ToString() == drNPL["Ma"].ToString())
                        {
                            try
                            {

                                if (drNPL["Ma"].ToString() == "05/HTKR/2014-03")
                                {
                                    if (drNPL["MaLoaiHinh"].ToString().Contains("NGC"))
                                    {

                                    }
                                }
                                int nam = int.Parse(drNPL["NgayDangKy"].ToString().Substring(6, 4));
                                if (drNPL["MaLoaiHinh"].ToString().Contains("X"))
                                {

                                    if (nam <= HD.NgayKy.Year)
                                    {
                                        luongXK_DauKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                    }
                                    else
                                    {

                                        luongXK_TrongKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                    }

                                }
                                else
                                    if (nam <= HD.NgayKy.Year)
                                    {
                                        luongNK_DauKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                        //luongXK_DauKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                        //luongXK_DauKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        luongTon += Convert.ToDecimal(drNPL["LuongTon_TK"].ToString());
                                        //luongTon -= Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        //luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                    }
                                    else
                                    {
                                        luongNK_TrongKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                        //luongXK_TrongKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                        //luongXK_TrongKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        luongTon += Convert.ToDecimal(drNPL["LuongTon_TK"].ToString());
                                        //luongTon -= Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        //luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                    }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                                //throw;
                            }


                        }
                        else
                            continue;
                    }
                    //tính lượng xuất NPL
                    foreach (DataRow drNPL_DM in dtDM_SP.Rows)
                    {
                        if (dr["Ma"].ToString() == drNPL_DM["MaNguyenPhuLieu"].ToString())
                        {
                            try
                            {

                                //if (drNPL_DM["Ma"].ToString() == "05/HTKR/2014-03")
                                //{
                                //    if (drNPL_DM["MaLoaiHinh"].ToString().Contains("NGC"))
                                //    {

                                //    }
                                //}
                                int nam = int.Parse(drNPL_DM["NgayDangKy"].ToString().Substring(6, 4));
                                if (nam <= HD.NgayKy.Year)
                                {
                                    //luongNK_DauKy += Convert.ToDecimal(drNPL_DM["SoLuong"].ToString());
                                    luongXK_DauKy += Convert.ToDecimal(drNPL_DM["LuongNPLCoHH"].ToString());
                                    //luongXK_DauKy += Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                    //luongTon += Convert.ToDecimal(drNPL_DM["LuongTon_TK"].ToString());
                                    //luongTon -= Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                    //luongXT += Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                }
                                else
                                {
                                    //luongNK_TrongKy += Convert.ToDecimal(drNPL_DM["SoLuong"].ToString());
                                    luongXK_TrongKy += Convert.ToDecimal(drNPL_DM["LuongNPLCoHH"].ToString());
                                    //luongXK_TrongKy += Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                    //luongTon += Convert.ToDecimal(drNPL_DM["LuongTon_TK"].ToString());
                                    //luongTon -= Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                    //luongXT += Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                                //throw;
                            }


                        }
                        else
                            continue;
                    }
                    //kiểm tra dữ liệu nếu <0 thì đưa vào lượng cung ứng
                    if (isCU)
                    {
                        if ((luongNK_DauKy + luongNK_TrongKy) - (luongXK_DauKy + luongXK_TrongKy) < 0)
                        {
                            //add dữ liệu
                            DataRow drF = dt.NewRow();
                            drF["STT"] = stt;
                            drF["HopDong_ID"] = dr["HopDong_ID"].ToString();
                            drF["Ma"] = dr["Ma"].ToString();
                            drF["Ten"] = dr["Ten"].ToString();
                            drF["DVT"] = dr["DVT"].ToString();
                            drF["SoLuongDaNhap"] = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                            drF["SoLuongCungUng"] = Convert.ToDecimal(dr["SoLuongCungUng"].ToString());
                            drF["SoLuongDaDung"] = Convert.ToDecimal(dr["SoLuongDaDung"].ToString());
                            drF["LuongTon"] = Convert.ToDecimal(dr["LuongTon"].ToString());
                            drF["SoToKhai"] = 0;
                            drF["SoTKVnacc"] = 0;
                            drF["MaLoaiHinh"] = "";
                            drF["KetLuanXLCL"] = "";
                            drF["SoLuong"] = 0;
                            drF["LuongSD_TK"] = 0;
                            drF["LuongTon_TK"] = 0;
                            drF["LuongNhap_DauKy"] = luongNK_DauKy;
                            drF["LuongNhap_TrongKy"] = luongNK_TrongKy;
                            drF["LuongXuat_DauKy"] = luongXK_DauKy;
                            drF["LuongXuat_TrongKy"] = luongXK_TrongKy;
                            drF["LuongTonCuoi"] = ((luongNK_DauKy + luongNK_TrongKy) - (luongXK_DauKy + luongXK_TrongKy))*(-1);
                            drF["LuongXuatTra"] = luongXT;
                            stt++;
                            dt.Rows.Add(drF);
                        }
                    }
                    else 
                    {
                        if ((luongNK_DauKy + luongNK_TrongKy) - (luongXK_DauKy + luongXK_TrongKy) > 0)
                        {
                            //add dữ liệu
                            DataRow drF = dt.NewRow();
                            drF["STT"] = stt;
                            drF["HopDong_ID"] = dr["HopDong_ID"].ToString();
                            drF["Ma"] = dr["Ma"].ToString();
                            drF["Ten"] = dr["Ten"].ToString();
                            drF["DVT"] = dr["DVT"].ToString();
                            drF["SoLuongDaNhap"] = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                            drF["SoLuongCungUng"] = Convert.ToDecimal(dr["SoLuongCungUng"].ToString());
                            drF["SoLuongDaDung"] = Convert.ToDecimal(dr["SoLuongDaDung"].ToString());
                            drF["LuongTon"] = Convert.ToDecimal(dr["LuongTon"].ToString());
                            drF["SoToKhai"] = 0;
                            drF["SoTKVnacc"] = 0;
                            drF["MaLoaiHinh"] = "";
                            drF["KetLuanXLCL"] = "";
                            drF["SoLuong"] = 0;
                            drF["LuongSD_TK"] = 0;
                            drF["LuongTon_TK"] = 0;
                            drF["LuongNhap_DauKy"] = luongNK_DauKy;
                            drF["LuongNhap_TrongKy"] = luongNK_TrongKy;
                            drF["LuongXuat_DauKy"] = luongXK_DauKy;
                            drF["LuongXuat_TrongKy"] = luongXK_TrongKy;
                            drF["LuongTonCuoi"] = (luongNK_DauKy + luongNK_TrongKy) - (luongXK_DauKy + luongXK_TrongKy);
                            drF["LuongXuatTra"] = luongXT;
                            stt++;
                            dt.Rows.Add(drF);
                        }
                    }
                    
                    

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //throw;
                }

            }
            return dt;
        }
        public void BindReport()
        {
            // DataTable dt = new Company.GC.BLL.GC.ThanhKhoanHDGC().getDuLieuThanhKhoan(this.HD.ID).Tables[0];
            DataTable dt = dsBK.Clone();
            DataColumn dc1 = new DataColumn("LuongNhap_DauKy");
            dc1.DataType = typeof(System.Decimal);
            DataColumn dc2 = new DataColumn("LuongNhap_TrongKy");
            dc2.DataType = typeof(System.Decimal);
            DataColumn dc3 = new DataColumn("LuongXuat_DauKy");
            dc3.DataType = typeof(System.Decimal);
            DataColumn dc4 = new DataColumn("LuongXuat_TrongKy");
            dc4.DataType = typeof(System.Decimal);
            DataColumn dc5 = new DataColumn("LuongTonCuoi");
            dc4.DataType = typeof(System.Decimal);
            // duydp 03/07/2016
            if (!dt.Columns.Contains("LuongNhap_DauKy"))
                dt.Columns.Add(dc1);
            if (!dt.Columns.Contains("LuongNhap_TrongKy"))
                dt.Columns.Add(dc2);
            if (!dt.Columns.Contains("LuongXuat_TrongKy"))
                dt.Columns.Add(dc3);
            if (!dt.Columns.Contains("LuongTonCuoi"))
                dt.Columns.Add(dc4);
            //dt.Columns.Add(dc1);
            //dt.Columns.Add(dc2);
            //dt.Columns.Add(dc3);
            //dt.Columns.Add(dc4);
            //dt.Columns.Add(dc5);

            DataTable tbNPL = dsBK.DefaultView.ToTable(true, "HopDong_ID", "Ma", "Ten", "DVT_ID", "SoLuongDaNhap", "SoLuongCungUng", "SoLuongDaDung", "LuongTon");
            int stt = 1;
            foreach (DataRow dr in tbNPL.Rows)
            {
                decimal luongNK_DauKy = 0;
                decimal luongNK_TrongKy = 0;
                decimal luongXK_DauKy = 0;
                decimal luongXK_TrongKy = 0;
                //decimal luongSD = 0;
                decimal luongTon = 0;
                decimal luongXT = 0;
                decimal luongHuy = 0;
                try
                {
                    //tính lượng nhập NPL
                    foreach (DataRow drNPL in dsBK.Rows)
                    {
                        if (dr["Ma"].ToString() == drNPL["Ma"].ToString())
                        {
                            try
                            {

                                if (drNPL["Ma"].ToString() == "05/HTKR/2014-03")
                                {
                                    if (drNPL["MaLoaiHinh"].ToString().Contains("NGC"))
                                    {

                                    }
                                }
                                int nam = int.Parse(drNPL["NgayDangKy"].ToString().Substring(6, 4));
                                //tính lượng NPL xuất trả và chuyển hợp đồng
                                if (drNPL["MaLoaiHinh"].ToString().Contains("X"))
                                {

                                    if (nam <= HD.NgayKy.Year)
                                    {

                                        luongXK_DauKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                    }
                                    else
                                    {

                                        luongXK_TrongKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                    }

                                }
                                    //tính lượng NPL hủy
                                else if (drNPL["MaLoaiHinh"].ToString().Contains("NLHUY"))
                                {
                                    luongHuy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                }
                                    //Lượng NPL nhập và cung ứng
                                else
                                {
                                    if (nam <= HD.NgayKy.Year)
                                    {
                                        luongNK_DauKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                        //luongXK_DauKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                        //luongXK_DauKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        luongTon += Convert.ToDecimal(drNPL["LuongTon_TK"].ToString());
                                        //luongTon -= Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        //luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                    }
                                    else
                                    {
                                        luongNK_TrongKy += Convert.ToDecimal(drNPL["SoLuong"].ToString());
                                        //luongXK_TrongKy += Convert.ToDecimal(drNPL["LuongSD_TK"].ToString());
                                        //luongXK_TrongKy += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        luongTon += Convert.ToDecimal(drNPL["LuongTon_TK"].ToString());
                                        //luongTon -= Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                        //luongXT += Convert.ToDecimal(drNPL["LuongXuatTra"].ToString());
                                    }
                                }
                                    
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                                //throw;
                            }


                        }
                        else
                            continue;
                    }
                    //tính lượng xuất NPL
                    foreach (DataRow drNPL_DM in dtDM_SP.Rows)
                    {
                        if (dr["Ma"].ToString() == drNPL_DM["MaNguyenPhuLieu"].ToString())
                        {
                            try
                            {

                                //if (drNPL_DM["Ma"].ToString() == "05/HTKR/2014-03")
                                //{
                                //    if (drNPL_DM["MaLoaiHinh"].ToString().Contains("NGC"))
                                //    {

                                //    }
                                //}
                                int nam = int.Parse(drNPL_DM["NgayDangKy"].ToString().Substring(6, 4));
                                if (nam <= HD.NgayKy.Year)
                                {
                                    //luongNK_DauKy += Convert.ToDecimal(drNPL_DM["SoLuong"].ToString());
                                    luongXK_DauKy += Convert.ToDecimal(drNPL_DM["LuongNPLCoHH"].ToString());
                                    //luongXK_DauKy += Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                    //luongTon += Convert.ToDecimal(drNPL_DM["LuongTon_TK"].ToString());
                                    //luongTon -= Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                    //luongXT += Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                }
                                else
                                {
                                    //luongNK_TrongKy += Convert.ToDecimal(drNPL_DM["SoLuong"].ToString());
                                    luongXK_TrongKy += Convert.ToDecimal(drNPL_DM["LuongNPLCoHH"].ToString());
                                    //luongXK_TrongKy += Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                    //luongTon += Convert.ToDecimal(drNPL_DM["LuongTon_TK"].ToString());
                                    //luongTon -= Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                    //luongXT += Convert.ToDecimal(drNPL_DM["LuongXuatTra"].ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                                //throw;
                            }


                        }
                        else
                            continue;
                    }
                    //add dữ liệu
                    DataRow drF = dt.NewRow();
                    drF["STT"] = stt;
                    drF["HopDong_ID"] = dr["HopDong_ID"].ToString();
                    drF["Ma"] = dr["Ma"].ToString();
                    drF["Ten"] = dr["Ten"].ToString();
                    drF["DVT"] = dr["DVT"].ToString();
                    drF["SoLuongDaNhap"] = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                    drF["SoLuongCungUng"] = Convert.ToDecimal(dr["SoLuongCungUng"].ToString());
                    drF["SoLuongDaDung"] = Convert.ToDecimal(dr["SoLuongDaDung"].ToString());
                    drF["LuongTon"] = Convert.ToDecimal(dr["LuongTon"].ToString());
                    drF["SoToKhai"] = 0;
                    drF["SoTKVnacc"] = 0;
                    drF["MaLoaiHinh"] = "";
                    drF["KetLuanXLCL"] = "";
                    drF["SoLuong"] = 0;
                    drF["LuongSD_TK"] = 0;
                    drF["LuongTon_TK"] = 0;
                    drF["LuongNhap_DauKy"] = luongNK_DauKy;
                    drF["LuongNhap_TrongKy"] = luongNK_TrongKy;
                    drF["LuongXuat_DauKy"] = luongXK_DauKy;
                    drF["LuongXuat_TrongKy"] = luongXK_TrongKy;
                    drF["LuongTonCuoi"] = (luongNK_DauKy + luongNK_TrongKy)-(luongXK_DauKy+luongXK_TrongKy)-luongHuy;
                    drF["LuongXuatTra"] = luongXT;
                    stt++;
                    dt.Rows.Add(drF);

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //throw;
                }

            }
            dt.TableName = "t_GC_ThanhKhoanHDGC";

            this.DataSource = dt;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();
                }
                else
                {
                    loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                }
            }
            //this.GroupHeader1.Visible = false;
            //this.GroupFooter1.Visible = false;
            //this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            //    new DevExpress.XtraReports.UI.GroupField(".Ten"),
            //    new DevExpress.XtraReports.UI.GroupField(".Ma"),
            //    new DevExpress.XtraReports.UI.GroupField(".TongLuongNK"),
            //    new DevExpress.XtraReports.UI.GroupField(".TongLuongCU"),
            //    new DevExpress.XtraReports.UI.GroupField(".TongLuongXK"),
            //    new DevExpress.XtraReports.UI.GroupField(".ChenhLech"),
            //    });
            lblMathang.Text = loaiSP;
            DateTime dauKy = new DateTime(HD.NgayKy.Year, 12, 31);
            DateTime trongKy = new DateTime(HD.NgayKy.Year + 1, 01, 01);
            txtNhapDauKy.Text = "Lượng nhập  đến (" + dauKy.ToString("dd/MM/yyyy") + ")";
            txtNhapTrongKy.Text = "Lượng nhập từ (" + trongKy.ToString("dd/MM/yyyy") + ")";
            txtLuongXuatDauKy.Text = "Lượng xuất đến (" + dauKy.ToString("dd/MM/yyyy") + ")";
            txtLuongXuatTrongKy.Text = "Lượng xuất từ (" + trongKy.ToString("dd/MM/yyyy") + ")";
            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ma");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ten");
            //lblMaNPL_F.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ma");
            //lblTenNPL_F.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ten");
            //lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy");
            //lblDVT_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT", "");
            //lblTongLuongNK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongLuongNK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongLuongCU.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongLuongCU", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongLuongXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongLuongXK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblSoToKhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoTKVnacc");
            //lblMaLoaiHinh.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaLoaiHinh");
            //lblLuongSD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSD_TK", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongNK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoLuongDaNhap", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongXK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoLuongDaDung", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblChenhLech.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTon", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongCU.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoLuongCungUng", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblChenhLech_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChenhLech", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongXuatTra.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongXuatTra", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lbl_LuongNhap_DauKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap_DauKy", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNhap_TrongKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap_TrongKy", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongXuatDauKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongXuat_DauKy", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongXuat_TrongKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongXuat_TrongKy", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");



            //lblKLXLCL_H.DataBindings.Add("Text", this.DataSource, dt.TableName + ".KetLuanXLCL");
            lblNgayThangNam.Text = GlobalSettings.TieudeNgay;

        }

        private void xrLabel40_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string ten = GetCurrentColumnValue("Ten").ToString();
            //if (!ten.Contains("(&#!"))
            //    lblTenNPL.Text = ten.Substring(0, ten.LastIndexOf("(") - 1);
            //else
            //{
            //    lblTenNPL.Text = ten.Substring(0, ten.LastIndexOf("(&#!") - 1);
            //}
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string ten = GetCurrentColumnValue("Ten").ToString();
            //if(!ten.Contains("&#!"))
            //    {
            //string[] arr = ten.Split(new char[1] {'('});
            //lblMaNPL.Text = arr[arr.Length - 1].Replace(")","");
            //    }
            //else
            //{
            //    string[] arr = ten.Split(new string[] { "(&#!" },StringSplitOptions.None);
            //    lblMaNPL.Text = arr[arr.Length - 1].Replace("&#!)", "");
            //}
        }

        //private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        //{
        //    XRLabel label = (XRLabel)sender;
        //    report.Label = label;
        //    report.txtName.Text = label.Text;
        //    report.lblName.Text = label.Tag.ToString();
        //}
        public void setText(XRLabel label, string text)
        {
            //label.Text = text;
        }

        private void lblMaLoaiHinh_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblMaLoaiHinh.Text = lblMaLoaiHinh.Text.Substring(2, 3);
        }

        private void lblLuongSD_AfterPrint(object sender, EventArgs e)
        {
            //tongluong += Convert.ToDecimal(lblLuongSD.Text);
        }

        private void xrTableCell27_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblTongLuongXuat_F.Text = tongluong.ToString("N4");
            //tongluong = 0;
        }

        private void xrTableCell4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblLuongTonCuoi.Text = index.ToString();
            //index++;
        }

        private void lblTongXuatTra_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    tongLuongXuatTra += Convert.ToDecimal(lblTongXuatTra.Text);
            //}
            //catch (Exception ex) 
            //{

            //    //throw;
            //}

        }

        private void lblTongLuongXuatTra_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblTongLuongXuatTra_F.Text = tongLuongXuatTra.ToString("N4");
            //tongLuongXuatTra = 0;
        }

        private void lblTongNK_AfterPrint(object sender, EventArgs e)
        {
            //tongLuongNK += Convert.ToDecimal(lblTongNK.Text);
        }

        private void lblTongXK_AfterPrint(object sender, EventArgs e)
        {
            //tongLuongXK += Convert.ToDecimal(lblTongXK.Text);
        }

        private void lblTongLuongNK_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblTongLuongNK_F.Text = tongLuongNK.ToString("N4");
            //tongLuongNK = 0;
        }

        private void lblTongLuongXuat_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblTongLuongXuat_F.Text = tongLuongXK.ToString("N4");
            //tongLuongXK = 0;
        }

        private void lblTongChenhLech_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblChenhLech_AfterPrint(object sender, EventArgs e)
        {

        }

        private void lblNgayDangKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblNgayDangKy.Text = lblNgayDangKy.Text.Substring(0,10);
        }

        private void lblSTT_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblSTT_F.Text = lblLuongTonCuoi.Text;
        }

        private void BangKe06_HSTK_TT38_Tong_AfterPrint(object sender, EventArgs e)
        {
            //this.Detail.Visible = false;
        }
    }
}
