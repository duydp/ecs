 
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_TotalInventoryReport_Details'))
	DROP TABLE t_KDT_VNACCS_TotalInventoryReport_Details
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_ScrapInformation_Details'))
	DROP TABLE t_KDT_VNACCS_ScrapInformation_Details
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_ScrapInformation'))
	DROP TABLE t_KDT_VNACCS_ScrapInformation
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_TotalInventoryReport'))
	DROP TABLE t_KDT_VNACCS_TotalInventoryReport
GO

IF NOT EXISTS (SELECT * FROM sys.tables WHERE name='t_KDT_VNACCS_ScrapInformation')
BEGIN
 CREATE TABLE t_KDT_VNACCS_ScrapInformation
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[SoTiepNhan] [bigint] NOT NULL,
[NgayTiepNhan] [datetime] NOT NULL,
[MaHaiQuan] [char] (6)  NOT NULL,
[MaDoanhNghiep] [varchar] (17)  NOT NULL,
[TrangThaiXuLy] [int] NOT NULL,
[GuidString] [nvarchar] (500)  NULL
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name='t_KDT_VNACCS_ScrapInformation_Details')
BEGIN
CREATE TABLE t_KDT_VNACCS_ScrapInformation_Details
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
Scrap_ID BIGINT NOT NULL FOREIGN KEY REFERENCES t_KDT_VNACCS_ScrapInformation (ID),
MaHangHoa NVARCHAR(50) NOT NULL,
TenHangHoa NVARCHAR(255) NOT NULL,
LoaiHangHoa NUMERIC(2) NOT NULL,
SoLuong NUMERIC(18,4) NOT NULL,
DVT NVARCHAR(4) NOT NULL
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name='t_KDT_VNACCS_TotalInventoryReport')
BEGIN
CREATE TABLE t_KDT_VNACCS_TotalInventoryReport
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[SoTiepNhan] [bigint] NOT NULL,
[NgayTiepNhan] [datetime] NOT NULL,
[MaHaiQuan] [char] (6)  NOT NULL,
[MaDoanhNghiep] [varchar] (17)  NOT NULL,
[TrangThaiXuLy] [int] NOT NULL,
[GuidString] [nvarchar] (500)  NULL,
NgayChotTon DATETIME NOT NULL,
LoaiBaoCao NUMERIC(5) NOT NULL,
LoaiSua NUMERIC(1,0) NULL ,
GhiChuKhac NVARCHAR(2000)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name='t_KDT_VNACCS_TotalInventoryReport_Details')
BEGIN
CREATE TABLE t_KDT_VNACCS_TotalInventoryReport_Details
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
TotalInventory_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_TotalInventoryReport (ID),
TenHangHoa NVARCHAR(255) NOT NULL,
MaHangHoa NVARCHAR(50) NOT NULL,
SoLuongTonKhoSoSach NUMERIC(18,8) NOT NULL,
SoLuongTonKhoThucTe NUMERIC(18,4) NOT NULL,
DVT NVARCHAR(4) NOT NULL
)
END
GO
ALTER TABLE dbo.t_KDT_VNACCS_Careeries
ALTER COLUMN [ChuKySanXuat] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS
GO
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(17),
	@TrangThaiXuLy int,
	@GuidString nvarchar(500),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_ScrapInformation]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@TrangThaiXuLy,
	@GuidString
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(17),
	@TrangThaiXuLy int,
	@GuidString nvarchar(500)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_ScrapInformation]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(17),
	@TrangThaiXuLy int,
	@GuidString nvarchar(500)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_ScrapInformation] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_ScrapInformation] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_ScrapInformation]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[TrangThaiXuLy],
			[GuidString]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@TrangThaiXuLy,
			@GuidString
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_ScrapInformation]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_ScrapInformation] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString]
FROM
	[dbo].[t_KDT_VNACCS_ScrapInformation]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString]
FROM [dbo].[t_KDT_VNACCS_ScrapInformation] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString]
FROM
	[dbo].[t_KDT_VNACCS_ScrapInformation]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectBy_Scrap_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectBy_Scrap_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ScrapInformation_Detail_DeleteBy_Scrap_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_DeleteBy_Scrap_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Insert]
	@Scrap_ID bigint,
	@MaHangHoa nvarchar(50),
	@TenHangHoa nvarchar(255),
	@LoaiHangHoa numeric(2, 0),
	@SoLuong numeric(18, 4),
	@DVT nvarchar(4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_ScrapInformation_Details]
(
	[Scrap_ID],
	[MaHangHoa],
	[TenHangHoa],
	[LoaiHangHoa],
	[SoLuong],
	[DVT]
)
VALUES 
(
	@Scrap_ID,
	@MaHangHoa,
	@TenHangHoa,
	@LoaiHangHoa,
	@SoLuong,
	@DVT
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Update]
	@ID bigint,
	@Scrap_ID bigint,
	@MaHangHoa nvarchar(50),
	@TenHangHoa nvarchar(255),
	@LoaiHangHoa numeric(2, 0),
	@SoLuong numeric(18, 4),
	@DVT nvarchar(4)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_ScrapInformation_Details]
SET
	[Scrap_ID] = @Scrap_ID,
	[MaHangHoa] = @MaHangHoa,
	[TenHangHoa] = @TenHangHoa,
	[LoaiHangHoa] = @LoaiHangHoa,
	[SoLuong] = @SoLuong,
	[DVT] = @DVT
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_InsertUpdate]
	@ID bigint,
	@Scrap_ID bigint,
	@MaHangHoa nvarchar(50),
	@TenHangHoa nvarchar(255),
	@LoaiHangHoa numeric(2, 0),
	@SoLuong numeric(18, 4),
	@DVT nvarchar(4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_ScrapInformation_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_ScrapInformation_Details] 
		SET
			[Scrap_ID] = @Scrap_ID,
			[MaHangHoa] = @MaHangHoa,
			[TenHangHoa] = @TenHangHoa,
			[LoaiHangHoa] = @LoaiHangHoa,
			[SoLuong] = @SoLuong,
			[DVT] = @DVT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_ScrapInformation_Details]
		(
			[Scrap_ID],
			[MaHangHoa],
			[TenHangHoa],
			[LoaiHangHoa],
			[SoLuong],
			[DVT]
		)
		VALUES 
		(
			@Scrap_ID,
			@MaHangHoa,
			@TenHangHoa,
			@LoaiHangHoa,
			@SoLuong,
			@DVT
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_ScrapInformation_Details]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_DeleteBy_Scrap_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_DeleteBy_Scrap_ID]
	@Scrap_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_ScrapInformation_Details]
WHERE
	[Scrap_ID] = @Scrap_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_ScrapInformation_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Scrap_ID],
	[MaHangHoa],
	[TenHangHoa],
	[LoaiHangHoa],
	[SoLuong],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_ScrapInformation_Details]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectBy_Scrap_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectBy_Scrap_ID]
	@Scrap_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Scrap_ID],
	[MaHangHoa],
	[TenHangHoa],
	[LoaiHangHoa],
	[SoLuong],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_ScrapInformation_Details]
WHERE
	[Scrap_ID] = @Scrap_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Scrap_ID],
	[MaHangHoa],
	[TenHangHoa],
	[LoaiHangHoa],
	[SoLuong],
	[DVT]
FROM [dbo].[t_KDT_VNACCS_ScrapInformation_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ScrapInformation_Detail_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Scrap_ID],
	[MaHangHoa],
	[TenHangHoa],
	[LoaiHangHoa],
	[SoLuong],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_ScrapInformation_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(17),
	@TrangThaiXuLy int,
	@GuidString nvarchar(500),
	@NgayChotTon datetime,
	@LoaiBaoCao numeric(5, 0),
	@LoaiSua numeric(1, 0),
	@GhiChuKhac nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString],
	[NgayChotTon],
	[LoaiBaoCao],
	[LoaiSua],
	[GhiChuKhac]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@TrangThaiXuLy,
	@GuidString,
	@NgayChotTon,
	@LoaiBaoCao,
	@LoaiSua,
	@GhiChuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(17),
	@TrangThaiXuLy int,
	@GuidString nvarchar(500),
	@NgayChotTon datetime,
	@LoaiBaoCao numeric(5, 0),
	@LoaiSua numeric(1, 0),
	@GhiChuKhac nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TotalInventoryReport]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString,
	[NgayChotTon] = @NgayChotTon,
	[LoaiBaoCao] = @LoaiBaoCao,
	[LoaiSua] = @LoaiSua,
	[GhiChuKhac] = @GhiChuKhac
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(17),
	@TrangThaiXuLy int,
	@GuidString nvarchar(500),
	@NgayChotTon datetime,
	@LoaiBaoCao numeric(5, 0),
	@LoaiSua numeric(1, 0),
	@GhiChuKhac nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TotalInventoryReport] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString,
			[NgayChotTon] = @NgayChotTon,
			[LoaiBaoCao] = @LoaiBaoCao,
			[LoaiSua] = @LoaiSua,
			[GhiChuKhac] = @GhiChuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[TrangThaiXuLy],
			[GuidString],
			[NgayChotTon],
			[LoaiBaoCao],
			[LoaiSua],
			[GhiChuKhac]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@TrangThaiXuLy,
			@GuidString,
			@NgayChotTon,
			@LoaiBaoCao,
			@LoaiSua,
			@GhiChuKhac
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TotalInventoryReport]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString],
	[NgayChotTon],
	[LoaiBaoCao],
	[LoaiSua],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString],
	[NgayChotTon],
	[LoaiBaoCao],
	[LoaiSua],
	[GhiChuKhac]
FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[GuidString],
	[NgayChotTon],
	[LoaiBaoCao],
	[LoaiSua],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_TotalInventory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_TotalInventory_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteBy_TotalInventory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteBy_TotalInventory_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Insert]
	@TotalInventory_ID bigint,
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@SoLuongTonKhoSoSach numeric(18, 8),
	@SoLuongTonKhoThucTe numeric(18, 4),
	@DVT nvarchar(4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
(
	[TotalInventory_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
)
VALUES 
(
	@TotalInventory_ID,
	@TenHangHoa,
	@MaHangHoa,
	@SoLuongTonKhoSoSach,
	@SoLuongTonKhoThucTe,
	@DVT
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Update]
	@ID bigint,
	@TotalInventory_ID bigint,
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@SoLuongTonKhoSoSach numeric(18, 8),
	@SoLuongTonKhoThucTe numeric(18, 4),
	@DVT nvarchar(4)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
SET
	[TotalInventory_ID] = @TotalInventory_ID,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[SoLuongTonKhoSoSach] = @SoLuongTonKhoSoSach,
	[SoLuongTonKhoThucTe] = @SoLuongTonKhoThucTe,
	[DVT] = @DVT
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_InsertUpdate]
	@ID bigint,
	@TotalInventory_ID bigint,
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@SoLuongTonKhoSoSach numeric(18, 8),
	@SoLuongTonKhoThucTe numeric(18, 4),
	@DVT nvarchar(4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details] 
		SET
			[TotalInventory_ID] = @TotalInventory_ID,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[SoLuongTonKhoSoSach] = @SoLuongTonKhoSoSach,
			[SoLuongTonKhoThucTe] = @SoLuongTonKhoThucTe,
			[DVT] = @DVT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
		(
			[TotalInventory_ID],
			[TenHangHoa],
			[MaHangHoa],
			[SoLuongTonKhoSoSach],
			[SoLuongTonKhoThucTe],
			[DVT]
		)
		VALUES 
		(
			@TotalInventory_ID,
			@TenHangHoa,
			@MaHangHoa,
			@SoLuongTonKhoSoSach,
			@SoLuongTonKhoThucTe,
			@DVT
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteBy_TotalInventory_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteBy_TotalInventory_ID]
	@TotalInventory_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
WHERE
	[TotalInventory_ID] = @TotalInventory_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TotalInventory_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_TotalInventory_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_TotalInventory_ID]
	@TotalInventory_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TotalInventory_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]
WHERE
	[TotalInventory_ID] = @TotalInventory_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TotalInventory_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
FROM [dbo].[t_KDT_VNACCS_TotalInventoryReport_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TotalInventory_ID],
	[TenHangHoa],
	[MaHangHoa],
	[SoLuongTonKhoSoSach],
	[SoLuongTonKhoThucTe],
	[DVT]
FROM
	[dbo].[t_KDT_VNACCS_TotalInventoryReport_Details]	

GO

 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.2',GETDATE(), N'CẬP NHẬT TABLE THÔNG TIN TIÊU HỦY VÀ BÁO CÁO CHỐT TỒN VÀ CƠ SỞ SẢN XUẤT')
END