﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.Interface.Report.GC.ThongTu38;
using Company.KDT.SHARE.Components;
using System.IO;

namespace Company.Interface.GC
{
    public partial class XuLyQuyetToanHopDong : BaseForm
    {
        public HopDong HD = new HopDong();
        DataTable SelectToKhaiNhapPB = new DataTable();
        public IList<NPLNhapTonThucTe> Collection = new List<NPLNhapTonThucTe>();
        public T_GC_NPL_QUYETOAN NPLQuyetToan = new T_GC_NPL_QUYETOAN();
        public List<T_GC_NPL_QUYETOAN> NPLQuyetToanCollection = new List<T_GC_NPL_QUYETOAN>();
        List<T_GC_SANPHAM_QUYETOAN> SPQuyetToanCollection = new List<T_GC_SANPHAM_QUYETOAN>();
        public int NamQuyetToan;
        public int NamQuyetToanOld;
        public string MaNPL;
        public string TenNPL;
        public string DVT;
        public string MaNPLTemp;
        public string TenNPLTemp;
        public string MaSP;
        public string TenSP;
        public string MaSPTemp;
        public string TenSPTemp;
        public decimal SoTKN;
        public decimal SoTKX;
        public decimal SoTKNTemp;
        public decimal SoTKXTemp;
        public DateTime dateTo;
        public DateTime dateFrom;
        public string NPL;
        public string NPLTemp;
        public decimal LuongNhap = 0;
        public decimal LuongXuat = 0;
        public decimal LuongTon = 0;
        public decimal TriGiaNhap = 0;
        public decimal TriGiaXuat = 0;
        public decimal TriGiaTon = 0;
        public XuLyQuyetToanHopDong()
        {
            InitializeComponent();
        }

        private void XuLyQuyetToanHopDong_Load(object sender, EventArgs e)
        {

            progressBar1.Value = 100;
            if (HD.TrangThaiThanhKhoan==2)
            {
                btnProcessDataNPL.Enabled = false;
                btnProcessDataSP.Enabled = false;
                btnUpdate.Enabled = false;
                btnProcess.Enabled = false;
            }
            if (GlobalSettings.MA_DON_VI != "4000395355")
            {
                tabControl1.TabPages.Remove(tpTKN);
                tabControl1.TabPages.Remove(tpTKX);
                tabControl1.TabPages.Remove(tpNPLNhap);
                tabControl1.TabPages.Remove(tpNPLXuat);
            }
            else
            {
                btnPrintReportValue.Visible = false;
                tabControl1.TabPages.Remove(tpTotalNPLNhap);
                tabControl1.TabPages.Remove(tpTotalNPLXuat);
            }
        }
        private void BindDataTKN(long HopDong_ID)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    this.Cursor = Cursors.WaitCursor;
                    dgListTKN.DataSource = ToKhaiMauDich.SelectToKhaiNhapDaPhanBo(HD.ID, NamQuyetToan);
                    dgListTKN.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void BindDataTKX(long HopDong_ID)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    this.Cursor = Cursors.WaitCursor;
                    dgListTKX.DataSource = ToKhaiMauDich.SelectToKhaiXuatDaPhanBo(HD.ID, NamQuyetToan);
                    dgListTKX.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void BindDataToTalNPLNhap(long HopDong_ID)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    this.Cursor = Cursors.WaitCursor;
                    dgListTotalNPLNhap.DataSource = NPLQuyetToan.GetToTalNPLNhapBy_HD_ID(HD.ID, NamQuyetToan).Tables[0];
                    dgListTotalNPLNhap.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void BindDataToTalNPLXuat(long HopDong_ID)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    this.Cursor = Cursors.WaitCursor;
                    dgListTotalNPLXuat.DataSource = NPLQuyetToan.GetToTalNPLXuatBy_HD_ID(HD.ID, NamQuyetToan).Tables[0];
                    dgListTotalNPLXuat.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void BindDataNPLNhap(long HopDong_ID)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    this.Cursor = Cursors.WaitCursor;
                    dgListNPL.DataSource = NPLNhapTonThucTe.SelectBy_ID_HopDong_NamQuyetToan(HD.ID, NamQuyetToan).Tables[0];
                    dgListNPL.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void BindDataProcessNPL(long HopDong_ID)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    this.Cursor = Cursors.WaitCursor;
                    dgListDataNPL.DataSource = T_GC_NPL_QUYETOAN_CHITIET.SelectCollectionDynamic("HOPDONG_ID =" + HopDong_ID + " AND NAMQUYETTOAN =" + NamQuyetToan + "", "MANPL");
                    dgListDataNPL.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void BindDataNPLQuyetToan(long HopDong_ID)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    this.Cursor = Cursors.WaitCursor;
                    dgListDataNPLQT.DataSource = T_GC_NPL_QUYETOAN.SelectDynamic("HOPDONG_ID =" + HopDong_ID + " AND NAMQUYETTOAN =" + NamQuyetToan + "", "MANPL").Tables[0];
                    dgListDataNPLQT.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void BindDataSPQuyetToan(long HopDong_ID)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    this.Cursor = Cursors.WaitCursor;
                    dgListDataSPQT.DataSource = T_GC_SANPHAM_QUYETOAN.SelectDynamic("HOPDONG_ID =" + HopDong_ID + " AND NAMQUYETTOAN =" + NamQuyetToan + "", "MASP").Tables[0];
                    dgListDataSPQT.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void BindDataSPXuat(long HopDong_ID)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    this.Cursor = Cursors.WaitCursor;
                    dgListDataSPXuat.DataSource = T_GC_SANPHAM_QUYETOAN_CHITIET.SelectCollectionDynamic("HOPDONG_ID =" + HopDong_ID + " AND NAMQUYETTOAN =" + NamQuyetToan + "", "MASP");
                    dgListDataSPXuat.Refetch();
                    this.Cursor = Cursors.Default;
                }

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                progressBar1.Minimum = 0;
                progressBar1.Maximum = 100;
                backgroundWorker1.WorkerReportsProgress = true;
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void dgListNPL_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void mnuImportDataNumber_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgListNPL.SelectedItems.Count < 0)
                {
                    return;
                }
                else
                {
                    foreach (GridEXSelectedItem grItem in dgListNPL.SelectedItems)
                    {
                        if (grItem.RowType == RowType.Record)
                        {
                            T_GC_NPL_QUYETOAN NPLQuyetToan = new T_GC_NPL_QUYETOAN();
                            NPLQuyetToan.MANPL = dgListNPL.GetRow(grItem.Position).Cells["Ma"].Value.ToString();
                            NPLQuyetToan.TENNPL = dgListNPL.GetRow(grItem.Position).Cells["Ten"].Value.ToString();
                            NPLQuyetToan.DVT = dgListNPL.GetRow(grItem.Position).Cells["DVT_ID"].Value.ToString();
                            NPLQuyetToan.HOPDONG_ID = HD.ID;
                            UpdateDataNumber f = new UpdateDataNumber();
                            f.NPLQuyetToan = NPLQuyetToan;
                            f.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void ProcessDataNPL()
        {
            //int Percen;
            //int TotalPercen;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    T_GC_NPL_QUYETOAN NPLQuyetToan = new T_GC_NPL_QUYETOAN();
                    List<T_GC_NPL_QUYETOAN> NPLQTCollection = new List<T_GC_NPL_QUYETOAN>();
                    T_GC_NPL_QUYETOAN_CHITIET NPLQTChiTiet = new T_GC_NPL_QUYETOAN_CHITIET();
                    List<T_GC_NPL_QUYETOAN_CHITIET> NPLQTChiTietCollection = new List<T_GC_NPL_QUYETOAN_CHITIET>();
                    DataSet ds = new DataSet();
                    DataTable dtTotal = new DataTable();
                    //Xử lý lượng tồn đầu kỳ
                    #region Xử lý lượng tồn đầu kỳ
                    NamQuyetToanOld = NamQuyetToan - 1;
                    DataTable dtbNPLQTOld = NPLQuyetToan.SelectDynamicBy("HOPDONG_ID =" + HD.ID + " AND NAMQUYETTOAN =" + NamQuyetToanOld, "MANPL").Tables[0];
                    // Kiểm tra quyết toán là lần đầu tiên hay lần tiếp theo
                    if (dtbNPLQTOld.Rows.Count == 0)
                    {
                        // Lần đầu tiên thì tính toán số liệu tồn đầu kỳ
                        DataTable dtNPLNhap = NPLQuyetToan.SelectToTalNPLNhapBy_HD_ID(HD.ID,NamQuyetToanOld).Tables[0];
                        DataTable dtNPLXuat = NPLQuyetToan.SelectToTalNPLXuatBy_HD_ID(HD.ID,NamQuyetToanOld).Tables[0];
                        DataTable dtNPL = Company.GC.BLL.GC.NguyenPhuLieu.SelectCollectionBy_HD_ID(HD.ID.ToString()).Tables[0];

                        DataColumn dtColMANPL = new DataColumn("MANPL");
                        dtColMANPL.DataType = typeof(System.String);
                        DataColumn dtColTENNPL = new DataColumn("TENNPL");
                        dtColTENNPL.DataType = typeof(System.String);
                        DataColumn dtColDVT = new DataColumn("DVT");
                        dtColDVT.DataType = typeof(System.String);
                        DataColumn dtColLUONGNHAP = new DataColumn("LUONGNHAP");
                        dtColLUONGNHAP.DataType = typeof(System.Decimal);
                        DataColumn dtColTRIGIANHAP = new DataColumn("TRIGIANHAP");
                        dtColTRIGIANHAP.DataType = typeof(System.Decimal);
                        DataColumn dtColLUONGXUAT = new DataColumn("LUONGXUAT");
                        dtColLUONGXUAT.DataType = typeof(System.Decimal);
                        DataColumn dtColTRIGIAXUAT = new DataColumn("TRIGIAXUAT");
                        dtColTRIGIAXUAT.DataType = typeof(System.Decimal);
                        DataColumn dtColLUONGTON = new DataColumn("LUONGTON");
                        dtColLUONGTON.DataType = typeof(System.Decimal);
                        DataColumn dtColTRIGIATON = new DataColumn("TRIGIATON");
                        dtColTRIGIATON.DataType = typeof(System.Decimal);

                        dtTotal.Columns.Add(dtColMANPL);
                        dtTotal.Columns.Add(dtColTENNPL);
                        dtTotal.Columns.Add(dtColDVT);
                        dtTotal.Columns.Add(dtColLUONGNHAP);
                        dtTotal.Columns.Add(dtColTRIGIANHAP);
                        dtTotal.Columns.Add(dtColLUONGXUAT);
                        dtTotal.Columns.Add(dtColTRIGIAXUAT);
                        dtTotal.Columns.Add(dtColLUONGTON);
                        dtTotal.Columns.Add(dtColTRIGIATON);
                        foreach (DataRow dr in dtNPLNhap.Rows)
                        {
                            try
                            {
                                NPL = dr["MaNPL"].ToString();
                                foreach (DataRow drNPL in dtNPL.Rows)
                                {
                                    NPLTemp = drNPL["Ma"].ToString();
                                    if (NPL == NPLTemp)
                                    {
                                        DataRow drTotal = dtTotal.NewRow();
                                        drTotal["MANPL"] = NPL;
                                        drTotal["TENNPL"] = drNPL["Ten"].ToString();
                                        drTotal["DVT"] = drNPL["TenDVT"].ToString();
                                        drTotal["LUONGNHAP"] = dr["LUONGNHAP"].ToString();
                                        drTotal["TRIGIANHAP"] = dr["TRIGIANHAP"].ToString();
                                        drTotal["LUONGXUAT"] = 0;
                                        drTotal["TRIGIAXUAT"] = 0;
                                        drTotal["LUONGTON"] = 0;
                                        drTotal["TRIGIATON"] = 0;
                                        dtTotal.Rows.Add(drTotal);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        // Xử lý trị giá xuất trong kỳ
                        DataTable dtNPLNhapTKOld = NPLQuyetToan.SelectNPLNhapQuyetToanBy_HD_ID(HD.ID, NamQuyetToanOld).Tables[0];
                        DataTable dtNPLXuatTheoDMOld = NPLQuyetToan.SelectNPLXuatTheoDinhMucBy_HD_ID(HD.ID, NamQuyetToanOld).Tables[0];
                        string MaNPLXuatTempOld;
                        decimal LuongXuatTempOld;
                        decimal TriGiaXuatTempOld = 0;
                        decimal LuongNhapTempOld;
                        decimal DonGiaNhapTempOld;
                        foreach (DataRow drNPL in dtNPLXuatTheoDMOld.Rows)
                        {
                            MaNPLXuatTempOld = drNPL["MaNguyenPhuLieu"].ToString();
                            LuongXuatTempOld = Convert.ToDecimal(drNPL["LuongNPL"].ToString());
                            TriGiaXuatTempOld = 0;

                            foreach (DataRow dr in dtNPLNhapTKOld.Select(" MaPhu ='" + MaNPLXuatTempOld + "'", "NgayDangKy"))
                            {
                                LuongNhapTempOld = Convert.ToDecimal(dr["SoLuong"].ToString());
                                DonGiaNhapTempOld = Convert.ToDecimal(dr["DonGiaTT"].ToString());
                                if (LuongNhapTempOld > 0)
                                {
                                    if (LuongXuatTempOld >= LuongNhapTempOld)
                                    {
                                        LuongXuatTempOld = LuongXuatTempOld - LuongNhapTempOld;
                                        TriGiaXuatTempOld += LuongNhapTempOld * DonGiaNhapTempOld;
                                        dr["SoLuong"] = 0;
                                        drNPL["LuongNPL"] = LuongXuatTempOld;
                                        drNPL["TriGiaXuat"] = TriGiaXuatTempOld;
                                    }
                                    else
                                    {
                                        TriGiaXuatTempOld += LuongXuatTempOld * DonGiaNhapTempOld;
                                        dr["SoLuong"] = LuongNhapTempOld - LuongXuatTempOld;
                                        drNPL["LuongNPL"] = 0;
                                        drNPL["TriGiaXuat"] = TriGiaXuatTempOld;
                                        LuongXuatTempOld = 0;
                                        break;
                                    }
                                }

                            }
                        }

                        DataTable dtGroupTotalXuatOld = GetGroupedBy(dtNPLXuatTheoDMOld, "MaNguyenPhuLieu,TriGiaXuat", "MaNguyenPhuLieu", "Sum");
                        #region Old
                        //foreach (DataRow dr in dtNPLXuat.Rows)
                        //{
                        //    try
                        //    {
                        //        NPL = dr["MANPL"].ToString();
                        //        foreach (DataRow drTotal in dtTotal.Rows)
                        //        {
                        //            NPLTemp = drTotal["MANPL"].ToString();
                        //            if (NPL == NPLTemp)
                        //            {
                        //                LuongNhap = Convert.ToDecimal(drTotal["LUONGNHAP"].ToString());
                        //                LuongXuat = Convert.ToDecimal(dr["LUONGXUAT"].ToString());
                        //                LuongTon = LuongNhap - LuongXuat;
                        //                TriGiaNhap = Convert.ToDecimal(drTotal["TRIGIANHAP"].ToString());
                        //                TriGiaXuat = Convert.ToDecimal(dr["TRIGIAXUAT"].ToString());
                        //                TriGiaTon = TriGiaNhap - TriGiaXuat;
                        //                //Gán giá trị vào  lại Table
                        //                drTotal["LUONGXUAT"] = LuongXuat.ToString();
                        //                drTotal["TRIGIAXUAT"] = TriGiaXuat.ToString();
                        //                drTotal["LUONGTON"] = LuongTon.ToString();
                        //                drTotal["TRIGIATON"] = TriGiaTon.ToString();
                        //            }
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        StreamWriter write = File.AppendText("Error.txt");
                        //        write.WriteLine("--------------------------------");
                        //        write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                        //        write.WriteLine(ex.StackTrace);
                        //        write.WriteLine("Lỗi là : ");
                        //        write.WriteLine(ex.Message);
                        //        write.WriteLine("Lỗi xử lý lượng tồn tại MÃ NPL : " + NPL.ToString());
                        //        write.WriteLine("--------------------------------");
                        //        write.Flush();
                        //        write.Close();
                        //        Logger.LocalLogger.Instance().WriteMessage(ex);
                        //    }
                        //}
                        #endregion 
                        #region New
                        foreach (DataRow dr in dtNPLXuat.Rows)
                        {
                            try
                            {
                                NPL = dr["MANPL"].ToString();
                                foreach (DataRow drTotal in dtTotal.Rows)
                                {
                                    NPLTemp = drTotal["MANPL"].ToString();
                                    if (NPL == NPLTemp)
                                    {
                                        LuongNhap = Convert.ToDecimal(drTotal["LUONGNHAP"].ToString());
                                        LuongXuat = Convert.ToDecimal(dr["LUONGXUAT"].ToString());
                                        LuongTon = LuongNhap - LuongXuat;
                                        TriGiaNhap = Convert.ToDecimal(drTotal["TRIGIANHAP"].ToString());
                                        TriGiaXuat = 0;
                                        TriGiaTon = TriGiaNhap - TriGiaXuat;
                                        //Gán giá trị vào  lại Table
                                        drTotal["LUONGXUAT"] = LuongXuat.ToString();
                                        drTotal["TRIGIAXUAT"] = TriGiaXuat.ToString();
                                        drTotal["LUONGTON"] = LuongTon.ToString();
                                        drTotal["TRIGIATON"] = TriGiaTon.ToString();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng tồn tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        foreach (DataRow dr in dtGroupTotalXuatOld.Rows)
                        {
                            try
                            {
                                NPL = dr["MaNguyenPhuLieu"].ToString();
                                foreach (DataRow drTotal in dtTotal.Rows)
                                {
                                    NPLTemp = drTotal["MANPL"].ToString();
                                    if (NPL == NPLTemp)
                                    {
                                        LuongNhap = Convert.ToDecimal(drTotal["LUONGNHAP"].ToString());
                                        LuongXuat = Convert.ToDecimal(drTotal["LUONGXUAT"].ToString());
                                        LuongTon = LuongNhap - LuongXuat;
                                        TriGiaNhap = Convert.ToDecimal(drTotal["TRIGIANHAP"].ToString());
                                        TriGiaXuat = Convert.ToDecimal(dr["TriGiaXuat"].ToString());
                                        TriGiaTon = TriGiaNhap - TriGiaXuat;
                                        //Gán giá trị vào  lại Table
                                        drTotal["LUONGXUAT"] = LuongXuat.ToString();
                                        drTotal["TRIGIAXUAT"] = TriGiaXuat.ToString();
                                        drTotal["LUONGTON"] = LuongTon.ToString();
                                        drTotal["TRIGIATON"] = TriGiaTon.ToString();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng tồn tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        #endregion
                        foreach (DataRow drTotal in dtTotal.Rows)
                        {
                            LuongNhap = Convert.ToDecimal(drTotal["LUONGNHAP"].ToString());
                            LuongXuat = Convert.ToDecimal(drTotal["LUONGXUAT"].ToString());
                            LuongTon = LuongNhap - LuongXuat;
                            TriGiaNhap = Convert.ToDecimal(drTotal["TRIGIANHAP"].ToString());
                            TriGiaXuat = Convert.ToDecimal(drTotal["TRIGIAXUAT"].ToString());
                            TriGiaTon = TriGiaNhap - TriGiaXuat;
                            //Gán giá trị vào  lại Table
                            drTotal["LUONGXUAT"] = LuongXuat.ToString();
                            drTotal["TRIGIAXUAT"] = TriGiaXuat.ToString();
                            drTotal["LUONGTON"] = LuongTon.ToString();
                            drTotal["TRIGIATON"] = TriGiaTon.ToString();
                        }
                    }
                    #endregion
                    #region Xử lý lượng nhập trong kỳ 
                    // Xóa dữ liệu ban đầu
                    NPLQuyetToan.DeleteBy_HD_ID(HD.ID, NamQuyetToan);
                    // Lấy danh sách tất cả các NPL trong hợp đồng 
                    DataTable dtbNPL = Company.GC.BLL.GC.NguyenPhuLieu.SelectCollectionBy_HD_ID(HD.ID.ToString()).Tables[0];
                    // Thêm danh sách NPL vào bảng quyết toán
                    foreach (DataRow drNPL in dtbNPL.Rows)
                    {
                        NPLQuyetToan.MANPL = drNPL["Ma"].ToString();
                        NPLQuyetToan.TENNPL = drNPL["Ten"].ToString();
                        NPLQuyetToan.DVT = drNPL["TenDVT"].ToString();
                        NPLQuyetToan.LUONGTONDK = 0;
                        NPLQuyetToan.TRIGIATONDK = 0;
                        NPLQuyetToan.LUONGNHAPTK = 0;
                        NPLQuyetToan.TRIGIANHAPTK = 0;
                        NPLQuyetToan.LUONGXUATTK = 0;
                        NPLQuyetToan.TRIGIAXUATTK = 0;
                        NPLQuyetToan.LUONGTONCK = 0;
                        NPLQuyetToan.TRIGIATONDK = 0;
                        NPLQuyetToan.HOPDONG_ID = HD.ID;
                        NPLQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                        NPLQuyetToan.Insert(transaction);
                    }
                    transaction.Commit();
                    transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                    //Cập nhật lượng và trị giá tồn đầu kỳ
                    DataTable dtbNPLQuyeToan = NPLQuyetToan.SelectDynamicBy("HOPDONG_ID =" + HD.ID + " AND NAMQUYETTOAN =" + NamQuyetToan, "MANPL").Tables[0];
                    // Kiểm tra xem lần quyết toán là lần đầu tiên hay tiếp theo 
                    if (dtbNPLQTOld.Rows.Count == 0)
                    {
                        //Lần đầu tiên tính toán số liệu tồn đầu kỳ
                        foreach (DataRow drNPLQT in dtbNPLQuyeToan.Rows)
                        {
                            NPL = drNPLQT["MANPL"].ToString();
                            foreach (DataRow drNPLTon in dtTotal.Rows)
                            {
                                NPLTemp = drNPLTon["MANPL"].ToString();
                                if (NPL == NPLTemp)
                                {
                                    NPLQuyetToan.MANPL = drNPLTon["MANPL"].ToString();
                                    NPLQuyetToan.TENNPL = drNPLTon["TENNPL"].ToString();
                                    NPLQuyetToan.DVT = drNPLTon["DVT"].ToString();
                                    NPLQuyetToan.LUONGTONDK = Convert.ToDecimal(drNPLTon["LUONGTON"].ToString());
                                    NPLQuyetToan.TRIGIATONDK = Convert.ToDecimal(drNPLTon["TRIGIATON"].ToString());
                                    NPLQuyetToan.HOPDONG_ID = HD.ID;
                                    NPLQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                                    NPLQuyetToan.UPDATE_TONDK(transaction);
                                }
                            }
                        }
                        transaction.Commit();
                        transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                    }
                    else
                    {
                        // Lần tiếp theo lấy số liệu tồn cuối kỳ của lần trước gán cho tồn đầu kỳ cho lần này
                        foreach (DataRow drNPLQT in dtbNPLQuyeToan.Rows)
                        {
                            NPL = drNPLQT["MANPL"].ToString();
                            foreach (DataRow drNPLTon in dtbNPLQTOld.Rows)
                            {
                                NPLTemp = drNPLTon["MANPL"].ToString();
                                if (NPL == NPLTemp)
                                {
                                    NPLQuyetToan.MANPL = drNPLTon["MANPL"].ToString();
                                    NPLQuyetToan.TENNPL = drNPLTon["TENNPL"].ToString();
                                    NPLQuyetToan.DVT = drNPLTon["DVT"].ToString();
                                    NPLQuyetToan.LUONGTONDK = Convert.ToDecimal(drNPLTon["LUONGTONCK"].ToString());
                                    NPLQuyetToan.TRIGIATONDK = Convert.ToDecimal(drNPLTon["TRIGIATONCK"].ToString());
                                    NPLQuyetToan.HOPDONG_ID = HD.ID;
                                    NPLQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                                    NPLQuyetToan.UPDATE_TONDK(transaction);
                                }
                            }
                        }
                        transaction.Commit();
                        transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                    }
                    dtbNPLQuyeToan = NPLQuyetToan.SelectToTalNPLNhapBy_HD_ID(HD.ID, NamQuyetToan).Tables[0];
                    // Cập nhật lượng và trị giá nhập trong kỳ
                    foreach (DataRow drNPL in dtbNPL.Rows)
                    {
                        MaNPL = drNPL["Ma"].ToString();
                        foreach (DataRow drNPLQuyeToan in dtbNPLQuyeToan.Rows)
                        {
                            MaNPLTemp = drNPLQuyeToan["MaNPL"].ToString();
                            if (MaNPLTemp == MaNPL)
                            {
                                NPLQuyetToan.MANPL = drNPLQuyeToan["MaNPL"].ToString();
                                //NPLQuyetToan.TENNPL = drNPLQuyeToan["TenNPL"].ToString();
                                //NPLQuyetToan.DVT = drNPLQuyeToan["DVT"].ToString();
                                NPLQuyetToan.LUONGNHAPTK = Convert.ToDecimal(drNPLQuyeToan["LUONGNHAP"].ToString());
                                NPLQuyetToan.TRIGIANHAPTK = Convert.ToDecimal(drNPLQuyeToan["TRIGIANHAP"].ToString());
                                NPLQuyetToan.HOPDONG_ID = HD.ID;
                                NPLQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                                NPLQuyetToan.UPDATE_NHAPTK(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                    transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                    #endregion
                    #region Xử lý lượng xuất trong kỳ
                    dtbNPLQuyeToan = NPLQuyetToan.SelectToTalNPLXuatBy_HD_ID(HD.ID, NamQuyetToan).Tables[0];
                    // Cập nhật lượng và trị giá xuất trong kỳ
                    foreach (DataRow drNPL in dtbNPL.Rows)
                    {
                        MaNPL = drNPL["Ma"].ToString();
                        foreach (DataRow drNPLQuyeToan in dtbNPLQuyeToan.Rows)
                        {
                            MaNPLTemp = drNPLQuyeToan["MaNPL"].ToString();
                            if (MaNPLTemp == MaNPL)
                            {
                                NPLQuyetToan.MANPL = drNPLQuyeToan["MaNPL"].ToString();
                                //NPLQuyetToan.TENNPL = drNPLQuyeToan["TenNPL"].ToString();
                                //NPLQuyetToan.DVT = drNPLQuyeToan["DVT"].ToString();
                                NPLQuyetToan.LUONGXUATTK = Convert.ToDecimal(drNPLQuyeToan["LUONGXUAT"].ToString());
                                NPLQuyetToan.TRIGIAXUATTK = Convert.ToDecimal(drNPLQuyeToan["TRIGIAXUAT"].ToString());
                                NPLQuyetToan.HOPDONG_ID = HD.ID;
                                NPLQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                                NPLQuyetToan.UPDATE_XUATTK(transaction);
                            }
                        }
                    }
                    // Cập nhật Lượng xuất và Trị giá xuất trong kỳ cho những NPL có giá trị NULL
                    //NPLQTCollection = T_GC_NPL_QUYETOAN.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID + " AND NAMQUYETTOAN =" + NamQuyetToan, "MANPL");
                    //foreach (T_GC_NPL_QUYETOAN NPLDataItem in NPLQTCollection)
                    //{
                    //    if (String.IsNullOrEmpty(NPLDataItem.LUONGXUATTK.ToString()))
                    //    {
                    //        NPLDataItem.LUONGXUATTK = 0;
                    //        NPLDataItem.TRIGIAXUATTK = 0;
                    //        NPLDataItem.HOPDONG_ID = HD.ID;
                    //        NPLDataItem.NAMQUYETTOAN = NamQuyetToan;
                    //        NPLDataItem.UPDATE_XUATTK(transaction);
                    //    }
                    //}
                    #endregion
                    
                    transaction.Commit();
                    transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                    #region
                    // Xử lý trị giá xuất trong kỳ
                    DataTable dtNPLNhapTK = NPLQuyetToan.SelectNPLNhapQuyetToanBy_HD_ID(HD.ID, NamQuyetToan).Tables[0];
                    DataTable dtNPLXuatTheoDM = NPLQuyetToan.SelectNPLXuatTheoDinhMucBy_HD_ID(HD.ID, NamQuyetToan).Tables[0];
                    string MaNPLXuatTemp;
                    decimal LuongXuatTemp;
                    decimal TriGiaXuatTemp = 0;
                    decimal LuongNhapTemp;
                    decimal DonGiaNhapTemp;
                    foreach (DataRow drNPL in dtNPLXuatTheoDM.Rows)
                    {
                        MaNPLXuatTemp = drNPL["MaNguyenPhuLieu"].ToString();
                        LuongXuatTemp = Convert.ToDecimal(drNPL["LuongNPL"].ToString());
                        TriGiaXuatTemp = 0;

                        foreach (DataRow dr in dtNPLNhapTK.Select(" MaPhu ='" + MaNPLXuatTemp + "'", "NgayDangKy"))
                        {
                            LuongNhapTemp = Convert.ToDecimal(dr["SoLuong"].ToString());
                            DonGiaNhapTemp = Convert.ToDecimal(dr["DonGiaTT"].ToString());
                            if (LuongNhapTemp > 0)
                            {
                                if (LuongXuatTemp >= LuongNhapTemp)
                                {
                                    LuongXuatTemp = LuongXuatTemp - LuongNhapTemp;
                                    TriGiaXuatTemp += LuongNhapTemp * DonGiaNhapTemp;
                                    dr["SoLuong"] = 0;
                                    drNPL["LuongNPL"] = LuongXuatTemp;
                                    drNPL["TriGiaXuat"] = TriGiaXuatTemp;
                                }
                                else
                                {
                                    TriGiaXuatTemp += LuongXuatTemp * DonGiaNhapTemp;                 
                                    dr["SoLuong"] = LuongNhapTemp - LuongXuatTemp;
                                    drNPL["LuongNPL"] = 0;
                                    drNPL["TriGiaXuat"] = TriGiaXuatTemp;
                                    LuongXuatTemp = 0;
                                    break;
                                }
                            }

                        }
                    }

                    DataTable dtGroupTotalXuat = GetGroupedBy(dtNPLXuatTheoDM, "MaNguyenPhuLieu,TriGiaXuat", "MaNguyenPhuLieu", "Sum");
                    NPLQTCollection = T_GC_NPL_QUYETOAN.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID + " AND NAMQUYETTOAN =" + NamQuyetToan, "MANPL");
                    string MaTemp;
                    decimal TotalTriGiaXuat = 0;
                    foreach (DataRow dr in dtGroupTotalXuat.Rows)
                    {
                        MaTemp = dr["MaNguyenPhuLieu"].ToString();
                        TotalTriGiaXuat = Convert.ToDecimal(dr["TriGiaXuat"].ToString());
                        foreach (T_GC_NPL_QUYETOAN item in NPLQTCollection)
                        {
                            if (item.MANPL == MaTemp)
                            {
                                item.TRIGIAXUATTK = item.TRIGIAXUATTK + TotalTriGiaXuat;
                                item.UPDATE_XUATTK(transaction);
                            }
                        }
                    }
                    #endregion
                    this.Cursor = Cursors.Default;
                    transaction.Commit();
                    //Xử lý lượng tồn cuối kỳ
                    //DataTable dtNPLNhapTK = NPLNhapTonThucTe.SelectBy_Date(HD.ID, HD.NgayKy, dateFrom).Tables[0];
                    //DataTable dtNPLXuatTK = PhanBoToKhaiNhap.SelectBy_Date(HD.ID, HD.NgayKy, dateFrom).Tables[0];  
                    MLMessages("Xử lý dữ liệu thành công ", "MSG_WRN34", "", false);
                    btnProcessDataNPL.Enabled = false;
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    transaction.Rollback();
                    MLMessages("Xử lý dữ liệu không thành công ", "MSG_WRN34", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }        
        }
        private DataTable GetGroupedBy(DataTable dt, string columnNamesInDt, string groupByColumnNames, string typeOfCalculation)
        {
            //Return its own if the column names are empty
            if (columnNamesInDt == string.Empty || groupByColumnNames == string.Empty)
            {
                return dt;
            }

            //Once the columns are added find the distinct rows and group it bu the numbet
            DataTable _dt = dt.DefaultView.ToTable(true, groupByColumnNames);

            //The column names in data table
            string[] _columnNamesInDt = columnNamesInDt.Split(',');

            for (int i = 0; i < _columnNamesInDt.Length; i = i + 1)
            {
                if (_columnNamesInDt[i] != groupByColumnNames)
                {
                    _dt.Columns.Add(_columnNamesInDt[i]);
                }
            }

            //Gets the collection and send it back
            for (int i = 0; i < _dt.Rows.Count; i = i + 1)
            {
                for (int j = 0; j < _columnNamesInDt.Length; j = j + 1)
                {
                    if (_columnNamesInDt[j] != groupByColumnNames)
                    {
                        _dt.Rows[i][j] = dt.Compute(typeOfCalculation + "(" + _columnNamesInDt[j] + ")", groupByColumnNames + " = '" + _dt.Rows[i][groupByColumnNames].ToString() + "'");
                    }
                }
            }

            return _dt;
        }
        private void btnProcessDataNPL_Click(object sender, EventArgs e)
        {
            if (GlobalSettings.MA_DON_VI != "4000395355")
            {
                ProcessDataNPL();
            }
            else
            {
                //if (backgroundWorker1.IsBusy)
                //    backgroundWorker1.CancelAsync();
                //backgroundWorker1.RunWorkerAsync();
                //int Percen;
                //int TotalPercen;
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                    try
                    {
                        this.Cursor = Cursors.WaitCursor;
                        T_GC_NPL_QUYETOAN NPLQuyetToan = new T_GC_NPL_QUYETOAN();
                        List<T_GC_NPL_QUYETOAN> NPLQTCollection = new List<T_GC_NPL_QUYETOAN>();
                        T_GC_NPL_QUYETOAN_CHITIET NPLQTChiTiet = new T_GC_NPL_QUYETOAN_CHITIET();
                        List<T_GC_NPL_QUYETOAN_CHITIET> NPLQTChiTietCollection = new List<T_GC_NPL_QUYETOAN_CHITIET>();
                        DataSet ds = new DataSet();
                        DataTable dtTotal = new DataTable();
                        //Xử lý lượng tồn đầu kỳ
                        #region Xử lý lượng tồn đầu kỳ
                        NamQuyetToanOld = NamQuyetToan - 1;
                        DataTable dtbNPLQTOld = NPLQuyetToan.SelectDynamicBy("HOPDONG_ID =" + HD.ID + " AND NAMQUYETTOAN =" + NamQuyetToanOld, "MANPL").Tables[0];
                        // Kiểm tra quyết toán là lần đầu tiên hay lần tiếp theo
                        if (dtbNPLQTOld.Rows.Count == 0)
                        {
                            // Lần đầu tiên thì tính toán số liệu tồn đầu kỳ
                            DataTable dtNPLNhap = NPLNhapTonThucTe.SelectBy_Date(HD.ID, HD.NgayKy, dateFrom).Tables[0];
                            DataTable dtNPLXuat = PhanBoToKhaiNhap.SelectBy_Date(HD.ID, HD.NgayKy, dateFrom).Tables[0];
                            DataTable dtNPL = Company.GC.BLL.GC.NguyenPhuLieu.SelectCollectionBy_HD_ID(HD.ID.ToString()).Tables[0];

                            DataColumn dtColMANPL = new DataColumn("MANPL");
                            dtColMANPL.DataType = typeof(System.String);
                            DataColumn dtColTENNPL = new DataColumn("TENNPL");
                            dtColTENNPL.DataType = typeof(System.String);
                            DataColumn dtColDVT = new DataColumn("DVT");
                            dtColDVT.DataType = typeof(System.String);
                            DataColumn dtColLUONGNHAP = new DataColumn("LUONGNHAP");
                            dtColLUONGNHAP.DataType = typeof(System.Decimal);
                            DataColumn dtColTRIGIANHAP = new DataColumn("TRIGIANHAP");
                            dtColTRIGIANHAP.DataType = typeof(System.Decimal);
                            DataColumn dtColLUONGXUAT = new DataColumn("LUONGXUAT");
                            dtColLUONGXUAT.DataType = typeof(System.Decimal);
                            DataColumn dtColTRIGIAXUAT = new DataColumn("TRIGIAXUAT");
                            dtColTRIGIAXUAT.DataType = typeof(System.Decimal);
                            DataColumn dtColLUONGTON = new DataColumn("LUONGTON");
                            dtColLUONGTON.DataType = typeof(System.Decimal);
                            DataColumn dtColTRIGIATON = new DataColumn("TRIGIATON");
                            dtColTRIGIATON.DataType = typeof(System.Decimal);

                            dtTotal.Columns.Add(dtColMANPL);
                            dtTotal.Columns.Add(dtColTENNPL);
                            dtTotal.Columns.Add(dtColDVT);
                            dtTotal.Columns.Add(dtColLUONGNHAP);
                            dtTotal.Columns.Add(dtColTRIGIANHAP);
                            dtTotal.Columns.Add(dtColLUONGXUAT);
                            dtTotal.Columns.Add(dtColTRIGIAXUAT);
                            dtTotal.Columns.Add(dtColLUONGTON);
                            dtTotal.Columns.Add(dtColTRIGIATON);
                            foreach (DataRow dr in dtNPLNhap.Rows)
                            {
                                try
                                {
                                    NPL = dr["MaNPL"].ToString();
                                    foreach (DataRow drNPL in dtNPL.Rows)
                                    {
                                        NPLTemp = drNPL["Ma"].ToString();
                                        if (NPL == NPLTemp)
                                        {
                                            DataRow drTotal = dtTotal.NewRow();
                                            drTotal["MANPL"] = NPL;
                                            drTotal["TENNPL"] = drNPL["Ten"].ToString();
                                            drTotal["DVT"] = drNPL["TenDVT"].ToString();
                                            drTotal["LUONGNHAP"] = dr["LUONGNHAP"].ToString();
                                            drTotal["TRIGIANHAP"] = dr["TRIGIANHAP"].ToString();
                                            drTotal["LUONGXUAT"] = 0;
                                            drTotal["TRIGIAXUAT"] = 0;
                                            drTotal["LUONGTON"] = 0;
                                            drTotal["TRIGIATON"] = 0;
                                            dtTotal.Rows.Add(drTotal);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    StreamWriter write = File.AppendText("Error.txt");
                                    write.WriteLine("--------------------------------");
                                    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                    write.WriteLine(ex.StackTrace);
                                    write.WriteLine("Lỗi là : ");
                                    write.WriteLine(ex.Message);
                                    write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                    write.WriteLine("--------------------------------");
                                    write.Flush();
                                    write.Close();
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                            }
                            foreach (DataRow dr in dtNPLXuat.Rows)
                            {
                                try
                                {
                                    NPL = dr["MANPL"].ToString();
                                    foreach (DataRow drTotal in dtTotal.Rows)
                                    {
                                        NPLTemp = drTotal["MANPL"].ToString();
                                        if (NPL == NPLTemp)
                                        {
                                            LuongNhap = Convert.ToDecimal(drTotal["LUONGNHAP"].ToString());
                                            LuongXuat = Convert.ToDecimal(dr["LUONGXUAT"].ToString());
                                            LuongTon = LuongNhap - LuongXuat;
                                            TriGiaNhap = Convert.ToDecimal(drTotal["TRIGIANHAP"].ToString());
                                            TriGiaXuat = Convert.ToDecimal(dr["TRIGIAXUAT"].ToString());
                                            TriGiaTon = TriGiaNhap - TriGiaXuat;
                                            //Gán giá trị vào  lại Table
                                            drTotal["LUONGXUAT"] = LuongXuat.ToString();
                                            drTotal["TRIGIAXUAT"] = TriGiaXuat.ToString();
                                            drTotal["LUONGTON"] = LuongTon.ToString();
                                            drTotal["TRIGIATON"] = TriGiaTon.ToString();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    StreamWriter write = File.AppendText("Error.txt");
                                    write.WriteLine("--------------------------------");
                                    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                    write.WriteLine(ex.StackTrace);
                                    write.WriteLine("Lỗi là : ");
                                    write.WriteLine(ex.Message);
                                    write.WriteLine("Lỗi xử lý lượng tồn tại MÃ NPL : " + NPL.ToString());
                                    write.WriteLine("--------------------------------");
                                    write.Flush();
                                    write.Close();
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                            }
                            foreach (DataRow drTotal in dtTotal.Rows)
                            {
                                LuongNhap = Convert.ToDecimal(drTotal["LUONGNHAP"].ToString());
                                LuongXuat = Convert.ToDecimal(drTotal["LUONGXUAT"].ToString());
                                LuongTon = LuongNhap - LuongXuat;
                                TriGiaNhap = Convert.ToDecimal(drTotal["TRIGIANHAP"].ToString());
                                TriGiaXuat = Convert.ToDecimal(drTotal["TRIGIAXUAT"].ToString());
                                TriGiaTon = TriGiaNhap - TriGiaXuat;
                                //Gán giá trị vào  lại Table
                                drTotal["LUONGXUAT"] = LuongXuat.ToString();
                                drTotal["TRIGIAXUAT"] = TriGiaXuat.ToString();
                                drTotal["LUONGTON"] = LuongTon.ToString();
                                drTotal["TRIGIATON"] = TriGiaTon.ToString();
                            }
                        }
                        #endregion
                        #region Xử lý lượng nhập trong kỳ theo ngày hàng về kho trong phân bổ
                        // Xóa dữ liệu ban đầu
                        NPLQuyetToan.DeleteBy_HD_ID(HD.ID, NamQuyetToan);
                        // Lấy danh sách tất cả các NPL trong hợp đồng 
                        DataTable dtbNPL = Company.GC.BLL.GC.NguyenPhuLieu.SelectCollectionBy_HD_ID(HD.ID.ToString()).Tables[0];
                        // Thêm danh sách NPL vào bảng quyết toán
                        foreach (DataRow drNPL in dtbNPL.Rows)
                        {
                            NPLQuyetToan.MANPL = drNPL["Ma"].ToString();
                            NPLQuyetToan.TENNPL = drNPL["Ten"].ToString();
                            NPLQuyetToan.DVT = drNPL["TenDVT"].ToString();
                            NPLQuyetToan.LUONGTONDK = 0;
                            NPLQuyetToan.TRIGIATONDK = 0;
                            NPLQuyetToan.LUONGNHAPTK = 0;
                            NPLQuyetToan.TRIGIANHAPTK = 0;
                            NPLQuyetToan.LUONGXUATTK = 0;
                            NPLQuyetToan.TRIGIAXUATTK = 0;
                            NPLQuyetToan.LUONGTONCK = 0;
                            NPLQuyetToan.TRIGIATONDK = 0;
                            NPLQuyetToan.HOPDONG_ID = HD.ID;
                            NPLQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                            NPLQuyetToan.Insert(transaction);
                        }
                        transaction.Commit();
                        transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                        //Cập nhật lượng và trị giá tồn đầu kỳ
                        DataTable dtbNPLQuyeToan = NPLQuyetToan.SelectDynamicBy("HOPDONG_ID =" + HD.ID + " AND NAMQUYETTOAN =" + NamQuyetToan, "MANPL").Tables[0];
                        // Kiểm tra xem lần quyết toán là lần đầu tiên hay tiếp theo 
                        if (dtbNPLQTOld.Rows.Count == 0)
                        {
                            //Lần đầu tiên tính toán số liệu tồn đầu kỳ
                            foreach (DataRow drNPLQT in dtbNPLQuyeToan.Rows)
                            {
                                NPL = drNPLQT["MANPL"].ToString();
                                foreach (DataRow drNPLTon in dtTotal.Rows)
                                {
                                    NPLTemp = drNPLTon["MANPL"].ToString();
                                    if (NPL == NPLTemp)
                                    {
                                        NPLQuyetToan.MANPL = drNPLTon["MANPL"].ToString();
                                        NPLQuyetToan.TENNPL = drNPLTon["TENNPL"].ToString();
                                        NPLQuyetToan.DVT = drNPLTon["DVT"].ToString();
                                        NPLQuyetToan.LUONGTONDK = Convert.ToDecimal(drNPLTon["LUONGTON"].ToString());
                                        NPLQuyetToan.TRIGIATONDK = Convert.ToDecimal(drNPLTon["TRIGIATON"].ToString());
                                        NPLQuyetToan.HOPDONG_ID = HD.ID;
                                        NPLQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                                        NPLQuyetToan.UPDATE_TONDK(transaction);
                                    }
                                }
                            }
                            transaction.Commit();
                            transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                        }
                        else
                        {
                            // Lần tiếp theo lấy số liệu tồn cuối kỳ của lần trước gán cho tồn đầu kỳ cho lần này
                            foreach (DataRow drNPLQT in dtbNPLQuyeToan.Rows)
                            {
                                NPL = drNPLQT["MANPL"].ToString();
                                foreach (DataRow drNPLTon in dtbNPLQTOld.Rows)
                                {
                                    NPLTemp = drNPLTon["MANPL"].ToString();
                                    if (NPL == NPLTemp)
                                    {
                                        NPLQuyetToan.MANPL = drNPLTon["MANPL"].ToString();
                                        NPLQuyetToan.TENNPL = drNPLTon["TENNPL"].ToString();
                                        NPLQuyetToan.DVT = drNPLTon["DVT"].ToString();
                                        NPLQuyetToan.LUONGTONDK = Convert.ToDecimal(drNPLTon["LUONGTONCK"].ToString());
                                        NPLQuyetToan.TRIGIATONDK = Convert.ToDecimal(drNPLTon["TRIGIATONCK"].ToString());
                                        NPLQuyetToan.HOPDONG_ID = HD.ID;
                                        NPLQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                                        NPLQuyetToan.UPDATE_TONDK(transaction);
                                    }
                                }
                            }
                            transaction.Commit();
                            transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                        }
                        dtbNPLQuyeToan = NPLQuyetToan.SelectNPLNhapBy_HD_ID(HD.ID, NamQuyetToan).Tables[0];
                        // Cập nhật lượng và trị giá nhập trong kỳ
                        foreach (DataRow drNPL in dtbNPL.Rows)
                        {
                            MaNPL = drNPL["Ma"].ToString();
                            foreach (DataRow drNPLQuyeToan in dtbNPLQuyeToan.Rows)
                            {
                                MaNPLTemp = drNPLQuyeToan["MaNPL"].ToString();
                                if (MaNPLTemp == MaNPL)
                                {
                                    NPLQuyetToan.MANPL = drNPLQuyeToan["MaNPL"].ToString();
                                    NPLQuyetToan.TENNPL = drNPLQuyeToan["TenNPL"].ToString();
                                    NPLQuyetToan.DVT = drNPLQuyeToan["DVT"].ToString();
                                    NPLQuyetToan.LUONGNHAPTK = Convert.ToDecimal(drNPLQuyeToan["LUONGNHAPTK"].ToString());
                                    NPLQuyetToan.TRIGIANHAPTK = Convert.ToDecimal(drNPLQuyeToan["TRIGIANHAPTK"].ToString());
                                    NPLQuyetToan.HOPDONG_ID = HD.ID;
                                    NPLQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                                    NPLQuyetToan.UPDATE_NHAPTK(transaction);
                 
                                }
                            }
                        }
                        transaction.Commit();
                        transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                        #endregion
                        #region Xử lý lượng xuất trong kỳ
                        // Xử lý số liệu ban đầu
                        NPLQTChiTiet.DeleteBy_HD_ID(HD.ID, NamQuyetToan);
                        // Tạo số liệu mới
                        try
                        {
                            DbCommand cmd = db.GetStoredProcCommand("P_GC_NPL_QUYETOAN_CHITIET_INSERT");
                            db.AddInParameter(cmd, "@IDHopDong", DbType.Int64, HD.ID);
                            db.AddInParameter(cmd, "@NamQuyetToan", DbType.Int32, NamQuyetToan);
                            cmd.CommandTimeout = 6000;
                            ds = db.ExecuteDataSet(cmd);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        // Cập nhật Lượng xuất và trị giá Xuất trong kỳ 
                        try
                        {
                            DbCommand cmd = db.GetStoredProcCommand("P_GC_NPL_QUYETOAN_UPDATE");
                            db.AddInParameter(cmd, "@HopDong_ID", DbType.Int64, HD.ID);
                            db.AddInParameter(cmd, "@NamQuyetToan", DbType.Int32, NamQuyetToan);
                            cmd.CommandTimeout = 6000;
                            ds = db.ExecuteDataSet(cmd);
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        // Cập nhật Lượng xuất và Trị giá xuất trong kỳ cho những NPL có giá trị NULL
                        NPLQTCollection = T_GC_NPL_QUYETOAN.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID + " AND NAMQUYETTOAN =" + NamQuyetToan, "MANPL");
                        foreach (T_GC_NPL_QUYETOAN NPLDataItem in NPLQTCollection)
                        {
                            if (String.IsNullOrEmpty(NPLDataItem.LUONGXUATTK.ToString()))
                            {
                                NPLDataItem.LUONGXUATTK = 0;
                                NPLDataItem.TRIGIAXUATTK = 0;
                                NPLDataItem.HOPDONG_ID = HD.ID;
                                NPLDataItem.NAMQUYETTOAN = NamQuyetToan;
                                NPLDataItem.UPDATE_XUATTK(transaction);
                            }
                        }
                        #endregion
                        this.Cursor = Cursors.Default;
                        transaction.Commit();
                        //Xử lý lượng tồn cuối kỳ
                        //DataTable dtNPLNhapTK = NPLNhapTonThucTe.SelectBy_Date(HD.ID, HD.NgayKy, dateFrom).Tables[0];
                        //DataTable dtNPLXuatTK = PhanBoToKhaiNhap.SelectBy_Date(HD.ID, HD.NgayKy, dateFrom).Tables[0];  
                        MLMessages("Xử lý dữ liệu thành công ", "MSG_WRN34", "", false);
                        btnProcessDataNPL.Enabled = false;
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        transaction.Rollback();
                        MLMessages("Xử lý dữ liệu không thành công ", "MSG_WRN34", "", false);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateNPLQuyetToan f = new UpdateNPLQuyetToan();
                f.HD = HD;
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnReadExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ReadExcelFormNPL f = new ReadExcelFormNPL();
                f.ShowDialog();
                if (f.ListNPLQuyetToan.Count > 0)
                {
                    UpdateNPLQuyetToan form = new UpdateNPLQuyetToan();
                    form.ListNPLQuyetToan = f.ListNPLQuyetToan;
                    form.OpenType = OpenFormType.EditAll;
                    form.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            switch (e.TabPage.Name)
            {
                case "tpTKN" :
                    BindDataTKN(HD.ID);
                    break;
                case "tpTKX":
                    BindDataTKX(HD.ID);
                    break;
                case "tpNPLNhap":
                    BindDataNPLNhap(HD.ID);
                    break;
                case "tpNPLXuat":
                    BindDataProcessNPL(HD.ID);
                    break;
                case "tpNPLQT":
                    BindDataNPLQuyetToan(HD.ID);
                    break;
                case "tpSPQT":
                    BindDataSPQuyetToan(HD.ID);
                    break;
                case "tpSPXuat":
                    BindDataSPXuat(HD.ID);
                    break;
                case "tpTotalNPLNhap":
                    BindDataToTalNPLNhap(HD.ID);
                    break;
                case "tpTotalNPLXuat":
                    BindDataToTalNPLXuat(HD.ID);
                    break;
                default:
                    break;
            }
        }

        private void btnPrintReportNumber_Click(object sender, EventArgs e)
        {
            try
            {
                BaoCaoQuyetToan_Mau15_TT38_Total report = new BaoCaoQuyetToan_Mau15_TT38_Total();
                report.LIST_HOPDONG_ID = HD.ID.ToString();
                report.HD = HD;
                report.NAMQUYETTOAN = NamQuyetToan;
                report.isNumber = true;
                report.BindReport(true);
                report.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnPrintReportValue_Click(object sender, EventArgs e)
        {
            try
            {
                BaoCaoQuyetToan_Mau15_TT38_Total report = new BaoCaoQuyetToan_Mau15_TT38_Total();
                report.LIST_HOPDONG_ID = HD.ID.ToString();
                report.HD = HD;
                report.NAMQUYETTOAN = NamQuyetToan;
                report.isNumber = false;
                report.BindReport(false);
                report.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ProcessDataSPNew()
        {
            DataSet ds = new DataSet();
            T_GC_SANPHAM_QUYETOAN SPQuyetToan = new T_GC_SANPHAM_QUYETOAN();
            T_GC_SANPHAM_QUYETOAN_CHITIET SPQuyetToanChiTiet = new T_GC_SANPHAM_QUYETOAN_CHITIET();
            try
            {
                SPQuyetToanChiTiet.DeleteBy_HD_ID(HD.ID, NamQuyetToan);
                DataTable dtbSP = new DataTable();
                dtbSP = SPQuyetToanChiTiet.SelectSPXuatBy_HD_ID(HD.ID, NamQuyetToan).Tables[0];
                try
                {
                    foreach (DataRow dr in dtbSP.Rows)
                    {
                        SPQuyetToanChiTiet.TOKHAIXUAT = Convert.ToDecimal(dr["SoTKVNACCS"].ToString());
                        MaSP = dr["MaPhu"].ToString();
                        SPQuyetToanChiTiet.MASP = dr["MaPhu"].ToString();
                        SPQuyetToanChiTiet.TENSP = dr["TenHang"].ToString();
                        SPQuyetToanChiTiet.DVT = dr["DVT_ID"].ToString();
                        SPQuyetToanChiTiet.LUONGXUAT = Convert.ToDecimal(dr["LUONGXUAT"].ToString());
                        SPQuyetToanChiTiet.DONGIA = Convert.ToDecimal(dr["DONGIA"].ToString());
                        SPQuyetToanChiTiet.TRIGIAXUAT = Convert.ToDecimal(dr["TRIGIAXUAT"].ToString());
                        if (String.IsNullOrEmpty(dr["TYGIATINHTHUE"].ToString()))
                        {
                            SPQuyetToanChiTiet.TYGIATINHTHUE = 0;
                        }
                        else
                        {
                            SPQuyetToanChiTiet.TYGIATINHTHUE = Convert.ToInt32(dr["TYGIATINHTHUE"].ToString());
                        }
                        SPQuyetToanChiTiet.HOPDONG_ID = Convert.ToInt32(dr["HOPDONG_ID"].ToString());
                        SPQuyetToanChiTiet.NAMQUYETTOAN = Convert.ToInt32(dr["NAMQUYETTOAN"].ToString());
                        SPQuyetToanChiTiet.Insert();
                    }
                }
                catch (Exception ex)
                {
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("Lỗi thêm dữ liệu vào Table chi tiết tại MÃ SP : " + MaSP.ToString());
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                MLMessages("Xử lý dữ liệu thành công ", "MSG_WRN34", "", false);
                btnProcessDataSP.Enabled = false;
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MLMessages("Xử lý dữ liệu không thành công ", "MSG_WRN34", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnProcessDataSP_Click(object sender, EventArgs e)
        {
             DataSet ds = new DataSet();
             T_GC_SANPHAM_QUYETOAN SPQuyetToan = new T_GC_SANPHAM_QUYETOAN();
             T_GC_SANPHAM_QUYETOAN_CHITIET SPQuyetToanChiTiet = new T_GC_SANPHAM_QUYETOAN_CHITIET();
             SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
             using (SqlConnection connection = (SqlConnection)db.CreateConnection())
             {
                 connection.Open();
                 SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                 try
                 {
                     this.Cursor = Cursors.WaitCursor;
                     // Xóa dữ liệu ban đầu
                     SPQuyetToan.DeleteBy_HD_ID(HD.ID,NamQuyetToan);
                     DataTable dtbSPQuyetToan = SPQuyetToan.SelectSumSPXuat(HD.ID,NamQuyetToan).Tables[0];
                     // Tạo số liệu mới
                     foreach (DataRow drSP in dtbSPQuyetToan.Rows)
                     {
                         try
                         {
                             MaSP = drSP["MASP"].ToString(); ;
                             SPQuyetToan.MASP = drSP["MASP"].ToString();
                             SPQuyetToan.TENSP = drSP["TENSP"].ToString();
                             SPQuyetToan.DVT = drSP["DVT"].ToString();
                             SPQuyetToan.LUONGTONDK = 0;
                             SPQuyetToan.TRIGIATONDK = 0;
                             SPQuyetToan.LUONGNHAPTK = Convert.ToDecimal(drSP["LUONGNHAPTK"].ToString());
                             SPQuyetToan.TRIGIANHAPTK = Convert.ToDecimal(drSP["TRIGIANHAPTK"].ToString());
                             SPQuyetToan.LUONGXUATTK = Convert.ToDecimal(drSP["LUONGXUATTK"].ToString());
                             SPQuyetToan.TRIGIAXUATTK = Convert.ToDecimal(drSP["TRIGIAXUATTK"].ToString());
                             SPQuyetToan.LUONGTONCK = 0;
                             SPQuyetToan.TRIGIATONCK = 0;
                             SPQuyetToan.HOPDONG_ID = HD.ID;
                             SPQuyetToan.NAMQUYETTOAN = NamQuyetToan;
                             SPQuyetToan.Insert();
                         }
                         catch (Exception ex)
                         {
                             StreamWriter write = File.AppendText("Error.txt");
                             write.WriteLine("--------------------------------");
                             write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                             write.WriteLine(ex.StackTrace);
                             write.WriteLine("Lỗi là : ");
                             write.WriteLine(ex.Message);
                             write.WriteLine("Lỗi tạo dữ liệu mới tại MÃ SP : " + MaSP.ToString());
                             write.WriteLine("--------------------------------");
                             write.Flush();
                             write.Close();
                             transaction.Rollback();
                             Logger.LocalLogger.Instance().WriteMessage(ex);
                         }

                     }
                     transaction.Commit();
                 }
                 catch (Exception ex)
                 {
                     this.Cursor = Cursors.Default;
                     transaction.Rollback();
                     MLMessages("Xử lý dữ liệu không thành công ", "MSG_WRN34", "", false);
                     Logger.LocalLogger.Instance().WriteMessage(ex);
                 }
                 finally
                 {
                     connection.Close();
                 }
             }
             ProcessDataSPNew();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
           SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
           using (SqlConnection connection = (SqlConnection)db.CreateConnection())
           {
               connection.Open();
               SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
               try
               {
                   this.Cursor = Cursors.WaitCursor;
                   List<T_GC_NPL_QUYETOAN> NPLQTCollection = new List<T_GC_NPL_QUYETOAN>();
                   #region Xử lý lượng tồn cuối kỳ
                   NPLQTCollection = T_GC_NPL_QUYETOAN.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID, "MANPL");
                   foreach (T_GC_NPL_QUYETOAN NPLDataItem in NPLQTCollection)
                   {
                       NPLDataItem.LUONGTONCK = NPLDataItem.LUONGTONDK + NPLDataItem.LUONGNHAPTK - NPLDataItem.LUONGXUATTK;
                       NPLDataItem.TRIGIATONCK = NPLDataItem.TRIGIATONDK + NPLDataItem.TRIGIANHAPTK - NPLDataItem.TRIGIAXUATTK;
                       NPLDataItem.UPDATE_TONCK(transaction);
                   }
                   transaction.Commit();
                   this.Cursor = Cursors.Default;
                   Company.GC.BLL.KDT.GC.HopDong.UpdateQuyetToan(HD.ID.ToString());
                   this.HD = HopDong.Load(HD.ID);
                   MLMessages("Xử lý dữ liệu thành công ", "MSG_WRN34", "", false);
                   #endregion
               }
               catch (Exception ex)
               {
                   this.Cursor = Cursors.Default;
                   transaction.Rollback();
                   MLMessages("Xử lý dữ liệu không thành công ", "MSG_WRN34", "", false);
                   Logger.LocalLogger.Instance().WriteMessage(ex);
               }
           }

        }

        private void mnExportExcel_Click(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedTab.Name)
            {
                case "tpTKN":
                    ExportExcel(dgListTKN);
                    break;
                case "tpTKX":
                    ExportExcel(dgListTKX);
                    break;
                case "tpNPLNhap":
                    ExportExcel(dgListNPL);
                    break;
                case "tpNPLXuat":
                    ExportExcel(dgListDataNPL);
                    break;
                case "tpNPLQT":
                    ExportExcel(dgListDataNPLQT);
                    break;
                case "tpSPQT":
                    ExportExcel(dgListDataSPQT);
                    break;
                case "tpSPXuat":
                    ExportExcel(dgListDataSPXuat);
                    break;
                default:
                    break;
            }
        }

        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = tabControl1.SelectedTab.Text + "_" +HD.SoHopDong +"_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close(); 
                   
                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
