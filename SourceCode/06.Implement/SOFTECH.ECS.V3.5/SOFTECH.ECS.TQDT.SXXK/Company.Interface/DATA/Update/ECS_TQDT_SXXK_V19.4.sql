
  
  ---------------------Cập nhật danh mục DN khu chế xuất vào A016A (ma địa điểm đường bộ)-----------------------
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAAL','CT BROTHER VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH công nghiệp BROTHER Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXABL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXABL','CT LEO VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Điện tử LEO Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXACL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXACL','VINH HAN PRECISION','VN',N'ĐƯỜNG BỘ - Công ty TNHH công nghệ Vĩnh Hàn Precision')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXADL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXADL','HITACHI CABLE VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH HITACHI CABLE Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAEL','ADVANEX VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Advanex Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAFL','CT MATEX VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Matex Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAGL','PRINCETON BIOMEDITEC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Princeton BioMeditech Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAHL','CT JAGUAR HA NOI','VN',N'ĐƯỜNG BỘ - Công ty TNHH Quốc tế Jaguar Hà Nội')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAIL','PEGASUS VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH sản xuất Máy may PEGASUS Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAJL','CT VSM NHAT BAN','VN',N'ĐƯỜNG BỘ - Công ty TNHH sản xuất VSM Nhật Bản')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAKL','CT SEIKO VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH SEIKO Việt Nam ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXALL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXALL','SUMIDENSO VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Sumidenso Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAML','TOWADA VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH điện tử TOWADA Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXANL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXANL','CT AUREOLE BCD','VN',N'ĐƯỜNG BỘ - Chi nhánh công ty TNHH Aureole BCD')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAOL','VINA OKAMOTO','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vina OKAMOTO')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAPL','LFV METAL VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty LFV Metal Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAQL','VALQUA VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH VALQUA Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXARL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXARL','DIEN TU IRISO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Điện tử IRISO Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXASL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXASL','CT UMC VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH UMC Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXATL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXATL','DIEN TU TAISEI HANOI','VN',N'ĐƯỜNG BỘ - Công ty TNHH điện tử TAISEI Hà nội')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAUL','AIDEN VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH AIDEN Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAVL','ATARIH PRECISION VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Atarih Precision Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAWL','IKKA VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Công nghệ IKKA (VN)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAXL','NISSEI VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH công nghệ NISSEI Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAYL','SUMIDEN VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Dây cáp điện ôtô SUMIDEN Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXAZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXAZL','FUJI SEIKO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH FUJI SEIKO VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBAL','CT GESHEN VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH GE-SHEN VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBBL','HINSITSU SCREEN VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH HINSITSU SCREEN VN ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBCL','KEFICO VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH KEFICO VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBDL','CT KIM THUY PHUC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kim Thụy Phúc')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBEL','KURODA KAGAKU VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH KURODA KAGAKU Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBFL','MEIJTSU TONGDA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH MEIJTSU TONGDA Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBGL','MIZUHO PRECISION VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH MIZUHO Precision Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBHL','NISHOKU TECHNOLOGY','VN',N'ĐƯỜNG BỘ - Công ty TNHH Nishoku Technology VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBIL','CT SANSEI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH SANSEI VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBJL','CT SSK VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH SSK Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBKL','TAISHODO VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH TAISHODO Việt NAM')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBLL','CT LIEN DAI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Thiết bị điện Liên Đại Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBML','CT UNIDEN VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH UNIDEN VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBNL','SANYU SEIMITSU','VN',N'ĐƯỜNG BỘ - Công ty TNHH SANYU SEIMITSU')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBOL','CT FULUHASHI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH FULUHASHI VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBPL','YAZAKI HAI PHONG(TB)','VN',N'ĐƯỜNG BỘ - Chi nhánh công ty TNHH YAZAKI Hải Phòng tại Thái Bình')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBQL','CT SHENGFANG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Công nghiệp ShengFang')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBRL','CT TACTICIAN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Công nghiệp Tactician')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBSL','CT QTE DINH LUC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Quốc tế Công cụ Đỉnh Lực')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBTL','MIZUNOPRECISION','VN',N'ĐƯỜNG BỘ - Công ty TNHH MIZUNOPRECISION PARTS VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBUL','AKIYAMA SC VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH AKIYAMA-SC VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBVL','CT SANWA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH CN SANWA VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBWL','CT CORONA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH CORONA VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBXL','CT DENYO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH DENYO Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBYL','CT CANON VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH điện tử  CANON Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXBZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXBZL','DROSSAPHARM VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Drossapharm(Việt Nam)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCAL','FANCY CREATION VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Fancy Creation Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCBL','CT HAMADEN VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH HAMADEN Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCCL','HOYA GLASSDISK VN II','VN',N'ĐƯỜNG BỘ - Công ty TNHH Hoya Glass Disk Việt Nam II')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCDL','CT ICAM VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH ICAM Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCEL','CT KOSAKA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH KOSAKAViệt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCFL','CT KYOCERA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kyocera Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCGL','KYOTO BIKEN HN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kyoto Biken Hà Nội Laboratories')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCHL','MG PLASTIC VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH MG Plastic Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCIL','MIKASA VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH MIKASA Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCJL','CT VIETNAM MIE','VN',N'ĐƯỜNG BỘ - Công ty TNHH Một thành viên Việt Nam MIE')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCKL','CT NIKKISO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH NIKKISO Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCLL','CT OCHIAI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH OCHIAI Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCML','SEW COMPONENTS VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH SEWS - Components Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCNL','SHINEI SEIKO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Shinei Seiko Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCOL','SHINJO VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH SHINJO Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCPL','SHOEI VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH SHOEI Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCQL','CT SOC VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH SOC Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCRL','CT TBI CN TOYOTA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Thiết bị công nghiệp TOYOTA Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCSL','CT VLDT SHIN ETSU VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vật Liệu Điện Tử SHIN-ETSU Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCTL','CT VIETINAK','VN',N'ĐƯỜNG BỘ - Công ty TNHH VIETINAK')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCUL','CT CN SEICO','VN',N'ĐƯỜNG BỘ - Công ty TNHH công nghiệp Seico')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCVL','CT FUJI BAKELITE VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH FUJI bakelite Viêt nam ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCWL','CTCP NIHON LAMY VN','VN',N'ĐƯỜNG BỘ - Công ty Cổ phần Nihon Lamy VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCXL','CT BRIDGESTONE VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH sản xuất lốp xe Bridgestone Việt Nam(BTMV)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCYL','CT BUCHEON VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Bucheon Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXCZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXCZL','CTM CITIZEN VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Chế tạo máy CITIZEN VIỆT NAM')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEAL','NICHIAS HAI PHONG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Nichias Hải Phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEBL','CT GE VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty  TNHH GE VIET NAM tại Hải Phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXECL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXECL','IIYAMA SEIKI VIETNAM','VN',N'ĐƯỜNG BỘ - CÔNG TY TNHH IIYAMA SEIKI VIỆT NAM')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEDL','CT GIAY KONYA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH giấy Konya Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEEL','CT SIK VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH SIK Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEFL','CT SOUGOU VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH SOUGOU Việt nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEGL','CT SUMIDA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH điện tử Sumida Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEHL','CT SYNZTEC VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Synztec Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEIL','TOYODA GOSEI HP','VN',N'ĐƯỜNG BỘ - Công ty TNHH Toyoda Gosei Hải Phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEJL','ZHONGXIN YATAI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH ZhongXin YaTai Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEKL','HUGE GAIN HOLDINGS','VN',N'ĐƯỜNG BỘ - Công ty TNHH Huge Gain Holdings Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXELL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXELL','CT TIAN JIAO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Văn phòng phẩm Tian Jiao Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEML','CT WANLI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH văn phòng phẩm WANLI VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXENL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXENL','FUJI XEROX HAI PHONG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Fuji Xerox Hải Phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEOL','GERBERA PRECISION VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Gerbera Precision Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEPL','HUAFENG PLASTIC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Huafeng Plastic Hải phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEQL','IKO THOMPSON VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH IKO THOMPSON Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXERL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXERL','CT INSTANA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Instanta Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXESL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXESL','CT KOKUYO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kokuyo Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXETL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXETL','NAKASHIMA VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Nakashima Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEUL','CT NISSEI ECO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH NISSEI ECO VIET NAM')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEVL','TOHOKU PIONEER VN','VN',N'ĐƯỜNG BỘ - CTY TNHH TOHOKU PIONEER VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEWL','YANAGAWA SEIKO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH YANAGAWA SEIKO VIỆT NAM')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEXL','CT CHENG V','VN',N'ĐƯỜNG BỘ - Công ty TNHH Cheng-V')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEYL','FUJI MOLD VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Fuji Mold Việt Nam (FMV)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXEZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXEZL','TAKAHATA PRECISION','VN',N'ĐƯỜNG BỘ - Công ty TNHH Takahata Precision Vietnam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFAL','CT PV HAI PHONG','VN',N'ĐƯỜNG BỘ - Công ty TNHH PV. Hải Phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFBL','YAZAKI HAI PHONG VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Yazaki Hải Phòng Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFCL','CT CN NISHIMA VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Công Nghiệp Nishina Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFDL','CT RAYHO VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Rayho Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFEL','CT FONGHO','VN',N'ĐƯỜNG BỘ - Công ty  TNHH FONG HO')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFFL','CHUNG YANG FOODS VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Chung Yang Foods Việt nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFGL','CT PTCN BECKEN VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Phát triển công nghệ Becken VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFHL','CT KYOCERA VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Công nghệ máy Văn phòng Kyocera Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFIL','CT KYORITSU VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kyoritsu Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFJL','CT DTU SONG HAO','VN',N'ĐƯỜNG BỘ - Công ty TNHH Đầu Tư SONG HAO')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFKL','FUJIKURA COMPOSITES','VN',N'ĐƯỜNG BỘ - Công ty TNHH FUJIKURA COMPOSITES HẢI PHÒNG')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFLL','CT JKC VINA','VN',N'ĐƯỜNG BỘ - Công ty TNHH J.K.C VINA')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFML','CT KORG VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH KORG Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFNL','CT LIVAX VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Livax Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFOL','CT MAIKO HP','VN',N'ĐƯỜNG BỘ - Công ty TNHH Maiko Hải Phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFPL','NIPPON KODO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Nippon Kodo Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFQL','CT VINA BINGO','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vina-Bingo')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFRL','CT YONEDA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Yoneda Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFSL','CT CO KHI RK','VN',N'ĐƯỜNG BỘ - Công ty TNHH cơ khí RK')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFTL','CT CHE TAO ZEON VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Chế tạo ZEON Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFUL','MEIHOTECH VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Meihotech Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFVL','CT CHE TAO MAY EBA','VN',N'ĐƯỜNG BỘ - Công ty TNHH chế tạo máy EBA')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFWL','CT SHINYONG VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH SHINYONG Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFXL','ADVANCED TECHNOLOGY','VN',N'ĐƯỜNG BỘ - Công ty TNHH Advanced Technology Hải Phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFYL','CT HOP THINH','VN',N'ĐƯỜNG BỘ - Công ty TNHH Hợp Thịnh')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXFZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXFZL','HUADE HOLDINGS VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Huade Holdings VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGAL','CT RORZE ROBOTECH','VN',N'ĐƯỜNG BỘ - Công ty TNHH RORZE ROBOTECH')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGBL','CT QTE THOI TRANG VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Quốc Tế Thời Trang Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGCL','HONG YUAN HPHONG VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH chế tạo máy HONG YUAN Hải Phòng Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGDL','CT HANMIFLEX','VN',N'ĐƯỜNG BỘ - Công ty TNHH HANMIFLEX ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGEL','TH SOI HAILONG VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Sợi tổng hợp HaiLong Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGFL','CT ASTY VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH ASTY Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGGL','CT LIHITLAB VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Lihitlab. Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGIL','AKITA OIL SEAL VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Akita Oil Seal Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGJL','CT GIAY NAN I VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH giày Nan I VIỆT NAM')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGLL','SHIN ETSU VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vật Liệu Nam Châm Shin-Etsu Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGML','CT VINOMARINE','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vinomarine')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGNL','CT MOCHIZUKI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Công Nghệ Đột Dập Mochizuki Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGOL','CT JOHOKU HPHONG','VN',N'ĐƯỜNG BỘ - Công ty TNHH JOHOKU Hải Phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGPL','MEDIKIT VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Medikit Việt nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGQL','CT AURORA ART','VN',N'ĐƯỜNG BỘ - Công ty TNHH Aurora Art')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGRL','TOYOTA BOSHOKU HP','VN',N'ĐƯỜNG BỘ - Công ty TNHH TOYOTA BOSHOKU Hải Phòng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGSL','CT BORUN VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Borun Việt nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGTL','CT TRI WALL VINA','VN',N'ĐƯỜNG BỘ - Công ty TNHH Bao bì Tri - Wall Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGUL','CT CAO SU YAMATO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Cao Su Yamato VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGVL','CT CN SEIKO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Công Nghiệp Seiko VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGWL','CT DTU KYOCERA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Đầu Nối Điện Tử Kyocera VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGXL','CT HIRAKAWA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Hirakawa VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGYL','CT SHINEI CORONA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Shinei Corona VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXGZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXGZL','CT SOLDER COAT VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Solder Coat VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHAL','CT JOHOKU HP(TB)','VN',N'ĐƯỜNG BỘ - Chi nhánh công ty TNHH Johoku Hải Phòng tại Thái Bình')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHBL','CHE BIEN TP THUY SAN','VN',N'ĐƯỜNG BỘ - Cty TNHH một thành viên chế biến thực phẩm thủy sản Kaiyo')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHCL','CT DO GO FUKUI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH MTV Đồ gỗ FuKui Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHDL','NISSEI ELECTRIC VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH MTV Nissei Electric Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHEL','CT KOGANEI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH MTV Koganei Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHFL','CT TAZMO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Tazmo Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHGL','CT RIKA LONG HAU','VN',N'ĐƯỜNG BỘ - Công ty TNHH MTV Rika Long Hậu')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHHL','CT VIET FIBER','VN',N'ĐƯỜNG BỘ - Công ty TNHH Viet Fiber')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSGL','FURUKAWA AUTOMOTIVE','VN',N'ĐƯỜNG BỘ - DNCX (Furukawa Automotive Sys)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHJL','ALLIANCE ONE','VN',N'ĐƯỜNG BỘ - DNCX (Alliance One)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHKL','NICDEC TOSOK PREC','VN',N'ĐƯỜNG BỘ - DNCX (Nicdec Tosok Precision)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHLL','HAILLIANG','VN',N'ĐƯỜNG BỘ - DNCX (Hailiang)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHML','FANG ZHENG','VN',N'ĐƯỜNG BỘ - DNCX (Fang Zheng)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHNL','NIESSEI ELECTRIC','VN',N'ĐƯỜNG BỘ - DNCX (Nissei Electric My Tho)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHOL','EBISUYA','VN',N'ĐƯỜNG BỘ - DNCX (Ebisuya)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHPL','VINMAY S.STELL','VN',N'ĐƯỜNG BỘ - DNCX (Vinmay Stainless Steel Viet Nam)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHQL','KING HAM','VN',N'ĐƯỜNG BỘ - DNCX (King Ham)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHRL','ON ACCESSORIES','VN',N'ĐƯỜNG BỘ - DNCX (On Accessories)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHSL','TOYANAGE','VN',N'ĐƯỜNG BỘ - DNCX (Toyonage)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHTL','BELLA','VN',N'ĐƯỜNG BỘ - DNCX (Bella)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHWL','MIDORI  FOOTWEAR','VN',N'ĐƯỜNG BỘ - Công ty TNHH Midori safety Footwear Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHXL','INITATION VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Initation Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHYL','CTCP VIETNAM MOC BAI','VN',N'ĐƯỜNG BỘ - Kho Cty CP Việt Nam Mộc Bài (3900831450)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXHZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXHZL','CT BAT LUA HUAXING','VN',N'ĐƯỜNG BỘ - Công ty TNHH SX bật lửa HUAXING VN (3900358160)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIAL','CT VLMT PHUONG HOANG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vật liệu Mỹ thuật Phượng Hoàng (3900386760)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIBL','CT SX BAO BI AMPAC','VN',N'ĐƯỜNG BỘ - Công ty TNHH SX Bao bì AMPAC VN (3900388775)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXICL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXICL','CT MAY MAC JIAFA SOK','VN',N'ĐƯỜNG BỘ - Công ty TNHH May mặc JIAFA S.O.K (3900385319)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIDL','CT BAO BI NHUA HUADA','VN',N'ĐƯỜNG BỘ - Công ty TNHH Bao bì nhựa HUADA (3900421817)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIEL','CT NIFCO VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH NIFCO Việt Nam (3900366820)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIFL','CT KY THUAT DD','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kỹ thuật D&D (3900473452)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIGL','CT ONG THEP SUJIA','VN',N'ĐƯỜNG BỘ - Công ty TNHH Ống Thép SUJIA (VN) (3900797947)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIHL','CT PAYLOUD VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH PAYLOUD VN (3900370369)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIIL','CT VLM A THAI LOI','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vật liệu mới Á Thái Lợi (3900941291)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIJL','CT SUNRISE','VN',N'ĐƯỜNG BỘ - Công ty TNHH Văn Phòng Phẩm SUNRISE (3900395099)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIKL','CT NGU KIM HAILANG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Sản Phẩm Ngũ Kim HAILANG (3900405685)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXILL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXILL','CT NHUA XINSHENG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Nhựa XinSheng (3900444557)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIML','CT QT CHAN GIA THANH','VN',N'ĐƯỜNG BỘ - Công ty TNHH Quốc Tế Chấn Gia Thanh (3900370270)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXINL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXINL','CT CHERRY YEAR VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH SX Bật lửa CHERRY YEAR Việt Nam (3900445832)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIOL','CT DA DUC TIN VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Da Đức Tín Việt Nam (3900408502)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIPL','CT ICHIHIRO VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH ICHIHIRO Việt Nam (3900377597) ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIQL','CT KATAGIRY INDUSTRY','VN',N'ĐƯỜNG BỘ - Công ty TNHH KATAGIRY INDUSTRY (Việt Nam) (3900367581)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIRL','CT GIAY MAX FORTUNE','VN',N'ĐƯỜNG BỘ - Công ty TNHH Giấy MAX FORTUNE Việt Nam (3900358153)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXISL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXISL','CT SAIGLASS HCM VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH  SAIGLASS - HCM Việt Nam  (3900365908)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXITL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXITL','CT HAOJIAO VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH HAOJIAO Việt Nam (3901163615)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIUL','CT KODIM VIET','VN',N'ĐƯỜNG BỘ - Công ty TNHH Một thành viên KODIM VIET (3901169222)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIVL','CT RANGER VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH RANGER Việt Nam (3901172225)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIWL','CT QTE VIEN DONG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Quốc tế Viễn Đông (3900438225)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIXL','CT TOPTIPE HUABANG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Dệt may TOPTIDE HUABANG Việt Nam (3900441764)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIYL','CT CAO SU YONGJIN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Cao su và nhựa YongJin Việt Nam (3900917411)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXIZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXIZL','CT BROTEX VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Brotex VN (391157636)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJAL','CT GAIN LUCKY','VN',N'ĐƯỜNG BỘ - Công ty TNHH Gain Lucky (3901166775)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJBL','CT VINH DU','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vĩnh Dụ (3900975300)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJCL','CT DAY DAN SUMI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH dây dẫn SUMI Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJDL','SHIKOKU CABLE VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH SHIKOKU CABLE Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJEL','CT DTU TACHIBANA','VN',N'ĐƯỜNG BỘ - Công ty TNHH điện tử Việt Nam TACHIBANA')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJFL','POME INTERNATIONAL','VN',N'ĐƯỜNG BỘ - Công ty TNHH POME International')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJGL','CT YOKOWO VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH YOKOWO Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJHL','CT KALBAS VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH KALBAS Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJIL','CT FINETEK VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH FINETEK Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJJL','CT EIDAI VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH EIDAI Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJKL','CT SHOWA DENKO ','VN',N'ĐƯỜNG BỘ - Công ty TNHH SHOWA DENKO RARE - EARTH Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJLL','CT FUJIGEN VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH FUJIGEN Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJML','CT UEDA VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH UEDA Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJNL','CT MARUKOH PAPER VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH MARUKOH PAPER Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJOL','CT OSAWA VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH OSAWA Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJPL','CT ANAM ELECTRONICS','VN',N'ĐƯỜNG BỘ - Công ty TNHH Anam Electronics')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJQL','CT KEYRIN TELECOM VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH KEYRIN TELECOM Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJVL','CT MCNEX VINA','VN',N'ĐƯỜNG BỘ - Công ty TNHH MCNex Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJWL','CT SAKURAI VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Sakurai Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJXL','CT YOTSUBA DRESS VN','VN',N'ĐƯỜNG BỘ - Công ty Yotsuba Dress Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXJYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXJYL','CHUANG SHENG OPTCIAL','VN',N'ĐƯỜNG BỘ - Công ty TNHH Chuang Sheng Optcial Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKAL','CT SETO VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH  SETO Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKBL','CT DAIWA VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Daiwa Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKCL','CT MABUCHI DA NANG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Mabuchi Đà Nẵng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKDL','CT HOSO VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Hoso Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKEL','CT KANE M DA NANG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kane - M Đà Nẵng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKFL','CT YURI ABC DA NANG','VN',N'ĐƯỜNG BỘ - Công ty TNHH YURI ABC Đà Nẵng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKGL','CT VIET NAM KANZAKI','VN',N'ĐƯỜNG BỘ - Công ty TNHH Việt Nam Kanzaki')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKHL','CT FUKUI VIET NAM','VN',N'ĐƯỜNG BỘ - Cty TNHH FUKUI Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKIL','CT OISHI INDUSTRIES','VN',N'ĐƯỜNG BỘ - Công ty TNHH OISHI Industries Việt Nam ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKJL','CT DA NANG TELALA','VN',N'ĐƯỜNG BỘ - Cty TNHH Đà Nẵng TELALA')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKKL','CT MAEDA NAME DANANG','VN',N'ĐƯỜNG BỘ - Cty TNHH MAEDA NAME Đà Nẵng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKLL','CT NITTO JOKASO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Nitto Jokaso Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKML','CT NUI CAO','VN',N'ĐƯỜNG BỘ - Công ty TNHH    NÚI CAO')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKNL','TOKYO KEIKI PREC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Tokyo Keiki Precision Technology')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKOL','CT SETO SEISAKUSHO','VN',N'ĐƯỜNG BỘ - Công ty TNHH    SETO SEISAKUSHO')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKPL','CT SINARAN VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Sinaran VN ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKQL','CT TTTI DA NANG','VN',N'ĐƯỜNG BỘ - Công ty TNHH TTTI Đà Nẵng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKRL','SX GANG TAY SP BHLD','VN',N'ĐƯỜNG BỘ - Công ty sx găng tay và sản phẩm BHLĐ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKSL','FUJIKURA AUTOMOTIVE','VN',N'ĐƯỜNG BỘ - Công ty TNHH  FUJIKURA AUTOMOTIVE Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKTL','CT VN TOKAI','VN',N'ĐƯỜNG BỘ - Công ty TNHH Việt Nam TOKAI')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKUL','CT DET MAY ECLAT VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Dệt May Eclat Việt Nam ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKVL','ADVANCED MULTITECH','VN',N'ĐƯỜNG BỘ - Công ty TNHH Advanced Multitech VN ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKWL','CTSX ONG THEP K.HAN','VN',N'ĐƯỜNG BỘ - Công ty TNHH SX Ống Thép Không Hàn Cán Nóng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKXL','CT HARADA INDUSTRIES','VN',N'ĐƯỜNG BỘ - Công ty TNHH Harada Industries Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKYL','CT YUPOONG VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Yupoong VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXKZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXKZL','NEC TOKIN ELECTRONIC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Nec Tokin Electronics VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLAL','CT VEGA FASHIONE','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vega Fashion')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLBL','CT SEORIM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Seorim')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLCL','CT FC VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH FC Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLDL','CT TOMIYA SUMMIT','VN',N'ĐƯỜNG BỘ - Công ty TNHH Tomiya Summit Garment Export')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLFL','CT AUREOLE BCD','VN',N'ĐƯỜNG BỘ - Công ty TNHH Aureole BCD')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLGL','CT FUJIKURA VIETNAM','VN',N'ĐƯỜNG BỘ - Công ty TNHH Fujikura Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLHL','CT TAEKWANG MTC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Taekwang MTC')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLIL','CT PEAKTOP VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Peaktop Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLJL','CT OKEN SEIKO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH OKEN SEIKO VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLKL','AUREOLE CHEMICAL PRD','VN',N'ĐƯỜNG BỘ - Công ty TNHH Aureole Fine Chemical Products')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLLL','CT AIDO INDUSTRY VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Aido Industry VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLML','CT KY THUAT MURO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kỷ thuật Muro VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLNL','CT SHIRAI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Shirai Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLOL','CT UNIPAX','VN',N'ĐƯỜNG BỘ - Công ty TNHH Unipax')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLPL','CT MAY TINH FUJITSU','VN',N'ĐƯỜNG BỘ - Công ty TNHH Sản phẩm Máy Tính Fujitsu VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLRL','CT VN MEIWA','VN',N'ĐƯỜNG BỘ - Công ty TNHH Việt Nam MEIWA')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLSL','CT CHANG SHIN VN','VN',N'ĐƯỜNG BỘ - Công ty Chang Shin Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLTL','HUNG NHIEP POU CHEN','VN',N'ĐƯỜNG BỘ - Công ty Hưng nhiệp cổ phần Pou Chen VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLUL','CT TOHOKU CHEMICAL','VN',N'ĐƯỜNG BỘ - Công ty TNHH Tohoku Chemical Industries VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLWL','WHITTIER WOOD PRODUC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Whittier Wood Products VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLXL','ASIA GARMENT MANUFAC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Asia Garment Manufactrer VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLYL','VINA MELT TECHNOS','VN',N'ĐƯỜNG BỘ - Công ty TNHH Vina melt Technos')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXLZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXLZL','MABUCHI MOTOR VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Mabuchi Motor Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMAL','CT RITEK VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Ritek VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMBL','KIM CUONG SAO SANG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kim cương sao sáng')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMCL','CT FIGLA VIET NAM','VN',N'ĐƯỜNG BỘ - Công ty Figla Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMDL','GIAY TAEKWANG VINA','VN',N'ĐƯỜNG BỘ - Công ty TNHH Giày TaeKwang Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMEL','ARTUS SCIENTIFIC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Artus VN Pacific Scientific ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMFL','CT ON SEMICONDUCTOR','VN',N'ĐƯỜNG BỘ - Công ty TNHH ON SEMICONDUCTOR Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMGL','CT SPITFIRE CONTROLS','VN',N'ĐƯỜNG BỘ - Công ty TNHH Spitfire controls Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMHL','CT JIANGSU JING MENG','VN',N'ĐƯỜNG BỘ - Công ty TNHH Jiangsu Jing Meng VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMIL','CT NEW VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH New VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMJL','CT EMARALD BLUE VN','VN',N'ĐƯỜNG BỘ - Công tyTNHH Emerald Blue Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMKL','CT QTE FLEMING','VN',N'ĐƯỜNG BỘ - Công ty TNHH Quốc tế Fleming VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMLL','CT WATABE WEDDING VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Watabe Wedding Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMML','CT TNHH MATSUYA RD','VN',N'ĐƯỜNG BỘ - Công ty TNHH Matsuya R&D (Việt Nam)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMNL','CT TNHH SHISEIDO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Shiseido Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMOL','CT TNHH KUREHA VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Kureha Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMPL','CT SPCN TOSHIBA ASIA','VN',N'ĐƯỜNG BỘ - Công ty TNHH Sản phẩm công nghiệp Toshiba Asia')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMQL','CT MORITO VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Morito VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMRL','CT DAIKAN VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Daikan Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMSL','CT VN CREATE MEDIC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Việt Nam Create Medic')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMTL','CT CN BROTHER SAIGON','VN',N'ĐƯỜNG BỘ - Công ty TNHH Công nghiệp Brother Sài Gòn')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMUL','CT GOJO PAPER VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH GOJO Paper VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMVL','TBI TAN TIEN SUMIDEN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Thiết Bị Tân Tiến Sumiden VN ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMWL','CT MAJOR CRAFT VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Major Craft Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMXL','CT MARIGOT VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Marigot Việt Nam ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMYL','CROWN ASIA LEATHER','VN',N'ĐƯỜNG BỘ - Công ty TNHH Crown Asia Leather Goods Industrial')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXMZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXMZL','CT FUKUVI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Fukuvi VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNAL','CT NISSHIN FEIFUN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Nisshin Feifun')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNBL','CT SANOR ELECTRONICS','VN',N'ĐƯỜNG BỘ - Công ty TNHH Sanor Electronics Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNCL','CT LADE SUCCESS','VN',N'ĐƯỜNG BỘ - Công ty TNHH Lade Success')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNDL','CT BELMONT','VN',N'ĐƯỜNG BỘ - Công ty TNHH Belmont Manufacturing')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNEL','CT LIXIL GLOBAL','VN',N'ĐƯỜNG BỘ - Công ty TNHH Lixil Global Manufacturing VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNGL','CT TERUMO BCT VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Terumo BCT Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNHL','CT NEOSYS VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Neosys Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNIL','CT PEGASU SHINAMOTO','VN',N'ĐƯỜNG BỘ - Công ty TNHH Pegasus-Shimamoto auto parts VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNJL','FUJI IMPULSE LONGDUC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Fuji Impulse Long Đức')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNKL','CT NAGAE VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Nagae Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNLL','CT FUKUOKARASHI VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Fukuokarashi Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNML','CT HORY VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Hory Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNNL','CT PARAMOUNT BED VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Paramount Bed Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNOL','CT OLYMPUS VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Olympus Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNPL','CN AIDO INDUSTRY VN','VN',N'ĐƯỜNG BỘ - Chi nhánh Công ty TNHH Aido Industry Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNRL','CT PERIN ROSTAING VN','VN',N'ĐƯỜNG BỘ - Công ty Thuộc da Perin-rostaing (VN)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNSL','CT AMAPEX','VN',N'ĐƯỜNG BỘ - Công ty TNHH Amapex')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNTL','CT ACROWEL VN','VN',N'ĐƯỜNG BỘ - Công ty TNHH Acrowel Vietnam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNUL','CT SX TOAN CAU LIXIL','VN',N'ĐƯỜNG BỘ - Công ty TNHH SX Toàn Cầu LIXIL VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNVL','CT GC KIM LOAI NIC','VN',N'ĐƯỜNG BỘ - Công ty TNHH Gia công Kim Loại NIC (DNCX)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNWL','CT RICOH IMAGING VN','VN',N'ĐƯỜNG BỘ - CtyTNHH Sản phẩm RICOH IMAGING (Việt Nam) ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNXL','CT MALUGO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Malugo Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNYL','CT NAKAMURA VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Nakamura Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXNZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXNZL','CT HEIWA HYGIENE HN','VN',N'ĐƯỜNG BỘ - Cty TNHH Heiwa Hygiene Hà Nôi')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOAL','CT AKEBONO KASEI','VN',N'ĐƯỜNG BỘ - Cty TNHH Akebono Kasei Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOBL','CT OSK METAL VN','VN',N'ĐƯỜNG BỘ - Cty TNHH OSK Metal Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOHL','CT DO CHOI CHEEWAH','VN',N'ĐƯỜNG BỘ - Cty TNHH đồ chơi Cheewah VN ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOIL','TOYO ELECTRICCONTROL','VN',N'ĐƯỜNG BỘ - Cty TNHH Toyo Electric Control ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOJL','CT DOO JUNG VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Doo Jung Việt Nam   ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOKL','CT ESQUEL VN HBINH','VN',N'ĐƯỜNG BỘ - CTy TNHH SX hang may mặc Esquel Việt Nam – Hòa Bình')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOLL','CT YOUNGFAST OPTO','VN',N'ĐƯỜNG BỘ - Cty TNHH Youngfast Optoelectronics Việt Nam Mã: 01M2C61 ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOML','CT MEIKO ELECTRONICS','VN',N'ĐƯỜNG BỘ - Cty TNHH Điện Tử Meiko Electronics Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXONL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXONL','CT KT LUMINOUS VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Kính Kỹ Thuật Luminous Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOOL','CT CNDT YAN TIN VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Công nghệ điện tử Yan tin (Việt Nam)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOPL','CT NOBLE VIET NAM','VN',N'ĐƯỜNG BỘ - Cty TNHH Điện Tử Noble Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOQL','CT HICEL VINA','VN',N'ĐƯỜNG BỘ - Cty TNHH Hicel Vina ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXORL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXORL','CT HNT VINA','VN',N'ĐƯỜNG BỘ - Cty TNHH HNT Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOSL','CT TAIPAI PACKING','VN',N'ĐƯỜNG BỘ - Cty TNHH TaiPai Packing ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOTL','CT MIDORI APPAREL VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Midori Apparel Việt Nam Hòa Bình   ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOXL','CT GOKO SPRING VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Goko Spring VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOYL','CT KISHIRO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Kishiro VN ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXOZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXOZL','CT RHYTHM PRECISION','VN',N'ĐƯỜNG BỘ - Cty TNHH Rhythm Precision VN  ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPAL','CT TOYADA GIKEN VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Toyoda Giken Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPBL','CT HONEST VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Honest VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPCL','CT IKI CAST VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Iki cast VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPDL','CT FUKOKU VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Fukoku VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPEL','CT TAMRON OPTICAL','VN',N'ĐƯỜNG BỘ - Cty TNHH Tamron  Optical')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPFL','CT KOSAI VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Kosai VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPGL','CT SPINDEX HN','VN',N'ĐƯỜNG BỘ - Cty công nghiệp Spindex Hà Nội')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPHL','CT HOKUYO PRECISION','VN',N'ĐƯỜNG BỘ - Cty TNHH Hokuyo Precision VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPIL','CT INOAC VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Inoac VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPJL','CT VINA OSHOE','VN',N'ĐƯỜNG BỘ - Cty TNHH Vina Oshoe')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPKL','CT ASAHI INTECC HN','VN',N'ĐƯỜNG BỘ - Cty TNHH Asahi Intecc Hà Nội')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPLL','CT BEMAC PANELS','VN',N'ĐƯỜNG BỘ - Cty TNHH Bemac Panels Manufacturing VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPML','CT DENSO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Denso VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPNL','CT EIWO RUBBER MFG','VN',N'ĐƯỜNG BỘ - Cty TNHH Eiwo Rubber MFG')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPOL','CT ENPLAS VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Enplas VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPPL','FUJIPLA ENGINEERING','VN',N'ĐƯỜNG BỘ - Cty TNHH Fujipla Engineering VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPQL','CT HOEV','VN',N'ĐƯỜNG BỘ - Cty TNHH Hoev')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPRL','CT HOYA GLASS DISK','VN',N'ĐƯỜNG BỘ - Cty TNHH Hoya Glass Disk VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPSL','CT IKEUCHI VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Ikeuchi VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPTL','CT JTEC HN','VN',N'ĐƯỜNG BỘ - Cty TNHH Jtec Hà Nội')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPUL','CT KAI VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Kai VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPVL','CT KYOEI DIETECH VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Kyoei Dietech VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPWL','CT MATSUO INDUSTRIES','VN',N'ĐƯỜNG BỘ - Cty TNHH Matsuo Industries VN  ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPXL','CT BUTCHI MITSUBISHI','VN',N'ĐƯỜNG BỘ - Cty TNHH bút chì Mitsubishi Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPYL','CT MOLEX VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Molex Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXPZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXPZL','CT NAGATSU','VN',N'ĐƯỜNG BỘ - Cty TNHH Nagatsu')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQAL','NISSEI ELECTRONIC HN','VN',N'ĐƯỜNG BỘ - Cty TNHH Nissei Electronic Hà Nội')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQBL','CT OHARA PLASTICS VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Ohara Plastics Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQCL','PANASONIC SYS NETW','VN',N'ĐƯỜNG BỘ - Cty TNHH Panasonic System Networks ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQDL','CT RYONAN ELECTRIC','VN',N'ĐƯỜNG BỘ - Cty TNHH Ryonan Electric VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQEL','CT SANKO SOKEN VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Sanko Soken VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQFL','CT SATO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Sato VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQGL','CT SD VN','VN',N'ĐƯỜNG BỘ - Cty TNHH SD Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQHL','CT SEED VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Seed VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQIL','CT LKIEN DTU SEI VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Linh kiện điện tử Sei (Việt Nam)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQJL','SUMITOMO HEAVY INDUS','VN',N'ĐƯỜNG BỘ - Cty TNHH Sumitomo Heavy industries VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQKL','CT SUMITOMO NACCO','VN',N'ĐƯỜNG BỘ - Cty TNHH Sumitomo NACCO Materials Handling (VN)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQLL','SUNCALL TECHNOLOGY','VN',N'ĐƯỜNG BỘ - Cty TNHH Suncall Technology VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQML','CT SWCC SHOWA VN','VN',N'ĐƯỜNG BỘ - Cty TNHH SWCC Showa VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQNL','TAKARA TOOL DIE HN','VN',N'ĐƯỜNG BỘ - Cty TNHH Takara Tool&Die Hà Nội')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQOL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQOL','CT TOA VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Toa Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQPL','CT TOKYO BYOKANE VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Tokyo Byokane Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQQL','CT VN IRITANI','VN',N'ĐƯỜNG BỘ - Cty TNHH Việt Nam Iritani')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQRL','CT MHI AEROSPACE VN','VN',N'ĐƯỜNG BỘ - Cty TNHH MHI Aerospace VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQSL','CT CHIYODA INTEGRE','VN',N'ĐƯỜNG BỘ - Cty TNHH Chiyoda Integre VN ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQTL','CT CANON','VN',N'ĐƯỜNG BỘ - Cty TNHH Canon')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQUL','CT SANTOMAS VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Santomas VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQVL','CT KANEPACKAGE','VN',N'ĐƯỜNG BỘ - Cty TNHH Kanepackage')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQWL','CT IKAWA','VN',N'ĐƯỜNG BỘ - Cty TNHH Ikawa')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQXL','PANASONIC ELECT DEV','VN',N'ĐƯỜNG BỘ - Cty TNHH Panasonic Electronic Devices VN  ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQYL','CT TOKYO MICRO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Tokyo Micro VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXQZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXQZL','CT NODA VN','VN',N'ĐƯỜNG BỘ - Cty TNHH NODA Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRAL','KEIN HING MURAMOTO','VN',N'ĐƯỜNG BỘ - Cty TNHH Kein Hing Muramoto VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRBL','CT FUJIKIN VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Fujikin Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRCL','STANDAR UNIT SUPPLY','VN',N'ĐƯỜNG BỘ - Cty TNHH  Standar unit Supply VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRDL','CT ENDO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Endo  VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXREL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXREL','CT TOKUSHU','VN',N'ĐƯỜNG BỘ - Cty TNHH Tokushu')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRFL','CT TOYODA VAN MOPPES','VN',N'ĐƯỜNG BỘ - Cty TNHH Toyoda Van Moppes Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRGL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRGL','CT PARTRON VINA','VN',N'ĐƯỜNG BỘ - Cty TNHH Partron Vina  ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRHL','CT CN MUTO HN','VN',N'ĐƯỜNG BỘ - Cty TNHH Công nghệ MUTO Hà nội')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRIL','CT TERUMO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH TERUMO Việt nam ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRJL','CT NIDEC SANKYO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH NIDEC SANKYO VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRKL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRKL','CT CN ADCEL VN','VN',N'ĐƯỜNG BỘ - Cty TNHH công nghiệp ADCEL VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRLL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRLL','DN NITORI','VN',N'ĐƯỜNG BỘ - Doanh nghiệp chế xuất Nitori  ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRML')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRML','CT KATOLEC VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Katolec Việt nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRNL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRNL','CT ABEISM VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Abeism Việt nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXROL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXROL','CT JPK HN','VN',N'ĐƯỜNG BỘ - Cty TNHH JPK Hà nội')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRPL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRPL','CT HAESUNG VINA','VN',N'ĐƯỜNG BỘ - Cty TNHH Haesung Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRQL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRQL','CT PREC VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Prec Việt nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRRL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRRL','CT MD FLEX VINA','VN',N'ĐƯỜNG BỘ - Cty TNHH MD Flex Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRSL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRSL','CT LKIEN FLEXI VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Linh Kiện Flexi Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRTL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRTL','CT TAKASAGO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Takasago Việt nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRUL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRUL','CT INKEL VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Inkel Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRVL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRVL','CT CSM VN','VN',N'ĐƯỜNG BỘ - Cty TNHH CSM Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRWL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRWL','CT UJU VINA','VN',N'ĐƯỜNG BỘ - Cty TNHH UJU Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRXL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRXL','CT KL PRECISION HN','VN',N'ĐƯỜNG BỘ - Cty TNHH KL Precision (Hà nội)')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRYL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRYL','CT HOEI TEKKO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH Hoei Tekko Việt Nam')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXRZL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXRZL','CT OPM VINA','VN',N'ĐƯỜNG BỘ - Cty TNHH OPM Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSAL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSAL','CT LINDAN VN','VN',N'ĐƯỜNG BỘ - Cty TNHH MTV Lindan VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSBL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSBL','CT OHASHI TEKKO VN','VN',N'ĐƯỜNG BỘ - Cty TNHH OHASHI TEKKO VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSCL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSCL','CT BH VINA','VN',N'ĐƯỜNG BỘ - Cty TNHH BH Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSDL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSDL','CT KY THUAT OKAMOTO','VN',N'ĐƯỜNG BỘ - Cty TNHH Kỹ Thuật OKAMOTO VN')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSEL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSEL','CT POWER LOGICS VINA','VN',N'ĐƯỜNG BỘ - Cty TNHH Power Logics Vina')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSFL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSFL','CT CN NAMUGA PHU THO','VN',N'ĐƯỜNG BỘ - Cty TNHH Công nghệ Namuga Phú thọ')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSHL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSHL','KCX TAN THUAN','VN',N'ĐƯỜNG BỘ - Khu chế xuất Tân Thuận')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSIL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSIL','KCX LINH TRUNG','VN',N'ĐƯỜNG BỘ - Khu chế xuất Linh Trung')
  IF NOT EXISTS(SELECT * FROM t_VNACC_Category_CityUNLOCODE WHERE LOCODE='VNXSJL')
  INSERT INTO t_VNACC_Category_CityUNLOCODE(TableID,ProcessClassification,CreatorClassification,NumberOfKeyItems,LOCODE,CityNameOrStateName,CountryCode,Notes) VALUES('A016A','A','1','04','VNXSJL','XINDADONG TEXTILES','VN',N'ĐƯỜNG BỘ - Công ty TNHH Xindadong textiles (Việt Nam)')


/****** Object:  StoredProcedure [dbo].[p_SXXK_BCXuatNhapTon_TT22]    Script Date: 09/05/2014 09:45:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_SXXK_BCXuatNhapTon_TT196]  
-- Database: ECS_TQ_SXXK  
-- Author: Ngo Thanh Tung  
-- Time created: Wednesday, April 21, 2010  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_SXXK_BCXuatNhapTon_TT22]  
  @LanThanhLy bigint  
  ,@NamThanhLy int  
  ,@MaDoanhNghiep varchar(50)  
  ,@SapXepNgayDangKyTKN bit  
AS  
BEGIN  
   
SET NOCOUNT ON  
--SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
if OBJECT_ID('tempdb..#BCNPLXuatNhapTon_TT196') is not null drop table #BCNPLXuatNhapTon_TT196  
  
CREATE TABLE #BCNPLXuatNhapTon_TT196  
(  
 STT BIGINT  
 ,TenNPL NVARCHAR(1000)  
 ,DVT NVARCHAR(30)  
 ,SoToKhai NVARCHAR(50)  
 ,LuongNhap DECIMAL(18,8)  
 ,LuongTonDau DECIMAL(18,8)  
 ,LuongSuDung DECIMAL(18,8)  
 ,LuongTaiXuat DECIMAL(18,8)  
 ,LuongTonCuoi DECIMAL(18,8)  
 ,XuLy NVARCHAR(255)  
 ,MaNPL NVARCHAR(50)  
 ,STK BIGINT  
 ,MaLoaiHinh NVARCHAR(20)  
 ,NamDangKy bigint  
)  
IF(@SapXepNgayDangKyTKN = 0)  
BEGIN -- Sắp xếp theo mã NPL  
   
INSERT INTO #BCNPLXuatNhapTon_TT196  
(  
 STT,  
 TenNPL,  
 DVT,  
 SoToKhai,  
 LuongNhap,  
 LuongSuDung,  
 LuongTonDau,  
 LuongTaiXuat,  
 LuongTonCuoi,  
 XuLy,  
 MaNPL ,  
 STK,  
 MaLoaiHinh,   
 NamDangKy  
)  
  
  
SELECT   
   (ROW_NUMBER () OVER( ORDER BY MaNPL,NgayDangKyNhap,SoToKhaiNhap)) AS STT  
      ,(MAX([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL  
      ,MAX(TenDVT_NPL) AS DVT  
      ,(Convert(Nvarchar(12),Case when MaLoaiHinhNhap like '%V%' THEN (SELECT SoTKVNACCS FROM t_vnaccs_capsotokhai WHERE SoTK =  SoToKhaiNhap ) ELSE SoToKhaiNhap END ) + ';'   
       + (Case when MaLoaiHinhNhap like '%V%' THEN (SELECT Ten_VT FROM  t_HaiQuan_LoaiHinhMauDich WHERE ID =  MaLoaiHinhNhap )ELSE MaLoaiHinhNhap END ) + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai  
      ,MAX(CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end )as LuongNhap   
      ,Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung  
      ,MAX([LuongTonDau]) AS LuongTonDau  
      --,SUM([LuongNPLSuDung]) AS LuongSuDung   
      ,SUM([LuongNPLTaiXuat]) AS LuongTaiXuat  
      ,MIN([LuongTonCuoi]) AS LuongTonCuoi  
   ,'' AS  XuLy  
   ,MaNPL AS MaNPL  
   ,SoToKhaiNhap AS STK  
   ,MaLoaiHinhNhap AS MaLoaiHinh  
   ,YEAR(NgayDangKyNhap) AS NamDangKy  
     
  FROM [t_KDT_SXXK_BCXuatNhapTon]  
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep  
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap  ,DonGiaTT 
ORDER BY MaNPL,NgayDangKyNhap,SoToKhaiNhap  
  
END  
ELSE  
 BEGIN -- sắp xếp theo ngày đăng ký  
     
INSERT INTO #BCNPLXuatNhapTon_TT196  
(  
 STT,  
 TenNPL,  
 DVT,  
 SoToKhai,  
 LuongNhap,  
 LuongSuDung,  
 LuongTonDau,  
 LuongTaiXuat,  
 LuongTonCuoi,  
 XuLy,  
 MaNPL ,  
 STK,  
 MaLoaiHinh,   
 NamDangKy  
)  
  
  
SELECT   
   (ROW_NUMBER () OVER( ORDER BY MaNPL,NgayDangKyNhap,SoToKhaiNhap)) AS STT  
      ,(MAX([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL  
      ,MAX(TenDVT_NPL) AS DVT  
      ,(Convert(Nvarchar(12),  
       Case when MaLoaiHinhNhap like '%V%' THEN (SELECT SoTKVNACCS FROM t_vnaccs_capsotokhai WHERE SoTK =  SoToKhaiNhap ) ELSE SoToKhaiNhap END ) + ';'   
       + (Case when MaLoaiHinhNhap like '%V%' THEN (SELECT Ten_VT FROM  t_HaiQuan_LoaiHinhMauDich WHERE ID =  MaLoaiHinhNhap )ELSE MaLoaiHinhNhap END ) + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai  
      ,MAX(CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end )as LuongNhap   
      ,Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung  
      ,MAX([LuongTonDau]) AS LuongTonDau  
      --,SUM([LuongNPLSuDung]) AS LuongSuDung   
      ,SUM([LuongNPLTaiXuat]) AS LuongTaiXuat  
      ,MIN([LuongTonCuoi]) AS LuongTonCuoi  
   ,'' AS  XuLy  
   ,MaNPL AS MaNPL  
   ,SoToKhaiNhap AS STK  
   ,MaLoaiHinhNhap AS MaLoaiHinh  
   ,YEAR(NgayDangKyNhap) AS NamDangKy  
     
  FROM [t_KDT_SXXK_BCXuatNhapTon]  
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep  
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap    ,DonGiaTT
ORDER BY NgayDangKyNhap,SoToKhaiNhap,MaNPL  
  
 END  
  
  
UPDATE #BCNPLXuatNhapTon_TT196   
SET XuLy = dbo.f_Convert_GhiChuTK((SELECT TOP 1 ThanhKhoanTiep FROM [t_KDT_SXXK_BCXuatNhapTon] bc  
            WHERE bc.MaNPL = #BCNPLXuatNhapTon_TT196.MaNPL AND bc.MaLoaiHinhNhap = #BCNPLXuatNhapTon_TT196.MaLoaiHinh AND bc.SoToKhaiNhap = #BCNPLXuatNhapTon_TT196.STK AND  
      Year(bc.NgayDangKyNhap) = #BCNPLXuatNhapTon_TT196.NamDangKy AND bc.LuongTonCuoi = #BCNPLXuatNhapTon_TT196.LuongTonCuoi),@LanThanhLy)  
  
SELECT   
STT,  
TenNPL,  
DVT,  
SoToKhai,  
LuongNhap,  
LuongTonDau,  
LuongSuDung,  
LuongSuDung,  
LuongTaiXuat,  
LuongTonCuoi,  
XuLy  
  FROM #BCNPLXuatNhapTon_TT196         
   
DROP TABLE #BCNPLXuatNhapTon_TT196  
  
END  

GO
----------------------------------------------------------  
  
/****** Object:  StoredProcedure [dbo].[p_SXXK_BCThueXNK_TT196]    Script Date: 09/05/2014 09:54:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]  
-- Database: ECS_TQDT_SXXK  
-- Author: Ngo Thanh Tung  
-- Time created: Friday, October 07, 2011  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_SXXK_BCThueXNK_TT196]  
   @LanThanhLy bigint  
  ,@NamThanhLy int  
  ,@MaDoanhNghiep varchar(50)  
  ,@SapXepNgayDangKyTKN bit  
AS  
  
BEGIN  
   
   
 if OBJECT_ID('tempdb..#BCThueXNK_TT196') is not null drop table #BCThueXNK_TT196  
  
CREATE TABLE #BCThueXNK_TT196  
(  
 STT BIGINT  
 ,SoToKhai NVARCHAR(255)  
 ,TenNPL NVARCHAR(1000)  
 ,DVT NVARCHAR(30)  
 ,LuongNhap DECIMAL(18,5)  
 ,TienThueHoan DECIMAL(18,8)  
 ,ThuePhaiThu DECIMAL(18,8)  
 ,GhiChu NVARCHAR(255)  
 ,ThueNKNop DECIMAL(18,0)  
 ,LuongNPLSuDung DECIMAL(18,5)  
)  
IF(@SapXepNgayDangKyTKN = 1)  
   
 BEGIN -- Sắp xếp theo ngày đăng ký  
    
INSERT INTO #BCThueXNK_TT196  
(  
 STT,  
 SoToKhai,  
 TenNPL,  
 DVT,  
 LuongNhap,  
 TienThueHoan,  
 ThuePhaiThu,  
 GhiChu,  
 ThueNKNop,  
 LuongNPLSuDung  
   
)  
  
SELECT   
   (ROW_NUMBER () OVER( ORDER BY NgayDangKyNhap,SoToKhaiNhap, MaNPL)) AS STT  
      ,(Convert(Nvarchar(12),Case when MaLoaiHinhNhap like '%V%' THEN (SELECT SoTKVNACCS FROM t_vnaccs_capsotokhai WHERE SoTK =  SoToKhaiNhap ) ELSE SoToKhaiNhap END ) + ';'        + (Case when MaLoaiHinhNhap like '%V%' THEN (SELECT Ten_VT FROM  t_HaiQuan_LoaiHinhMauDich WHERE ID =  MaLoaiHinhNhap )ELSE MaLoaiHinhNhap END ) + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai  
      ,(MIN([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL  
      ,MAX(TenDVT_NPL) AS DVT  
      ,Max([LuongNhap]) AS LuongNhap  
      ,0 AS TienThueHoan  
      ,0 AS ThuePhaiThu  
      ,'' as GhiChu  
      ,MAX(ThueNKNop) AS ThueNKNop  
   ,SUM(LuongNPLSuDung) AS LuongSuDung  
     
  FROM t_KDT_SXXK_BCThueXNK  
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep  
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap , DonGiaTT   
ORDER BY NgayDangKyNhap,SoToKhaiNhap, MaNPL  
  
  END  
ELSE -- Sắp xếp theo mã NPL  
 BEGIN  
    
  INSERT INTO #BCThueXNK_TT196  
(  
 STT,  
 SoToKhai,  
 TenNPL,  
 DVT,  
 LuongNhap,  
 TienThueHoan,  
 ThuePhaiThu,  
 GhiChu,  
 ThueNKNop,  
 LuongNPLSuDung  
   
)  
  
SELECT   
   (ROW_NUMBER () OVER( ORDER BY  MaNPL ,NgayDangKyNhap,SoToKhaiNhap)) AS STT  
      ,(Convert(Nvarchar(12),Case when MaLoaiHinhNhap like '%V%' THEN (SELECT SoTKVNACCS FROM t_vnaccs_capsotokhai WHERE SoTK =  SoToKhaiNhap ) ELSE SoToKhaiNhap END ) + ';'        + (Case when MaLoaiHinhNhap like '%V%' THEN (SELECT Ten_VT FROM  t_HaiQuan_LoaiHinhMauDich WHERE ID =  MaLoaiHinhNhap )ELSE MaLoaiHinhNhap END ) + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai  
      ,(MIN([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL  
      ,MAX(TenDVT_NPL) AS DVT  
      ,Max([LuongNhap]) AS LuongNhap  
      ,0 AS TienThueHoan  
      ,0 AS ThuePhaiThu  
      ,'' as GhiChu  
      ,MAX(ThueNKNop) AS ThueNKNop  
   ,SUM(LuongNPLSuDung) AS LuongSuDung  
     
  FROM t_KDT_SXXK_BCThueXNK  
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep  
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap , DonGiaTT   
ORDER BY  MaNPL,NgayDangKyNhap,SoToKhaiNhap  
    
 END  
UPDATE #BCThueXNK_TT196  
SET  
 TienThueHoan = ROUND((LuongNPLSuDung/LuongNhap) * ThueNKNop,0)  
UPDATE #BCThueXNK_TT196  
SET   
 ThuePhaiThu =  ThueNKNop - TienThueHoan  
  
SELECT * FROM #BCThueXNK_TT196  
  
DROP TABLE #BCThueXNK_TT196  
   
END  
  
  GO
  IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.3',GETDATE(), N' Cap nhat store ma luu kho cho thong quan,DVT,Cang ICD.... ')
END