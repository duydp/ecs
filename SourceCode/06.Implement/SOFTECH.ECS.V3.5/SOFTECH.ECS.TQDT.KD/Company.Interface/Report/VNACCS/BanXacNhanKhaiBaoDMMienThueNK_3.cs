using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
using Company.KDT.SHARE.VNACCS.Messages.Recived;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanXacNhanKhaiBaoDMMienThueNK_3 : DevExpress.XtraReports.UI.XtraReport
    {
        public BanXacNhanKhaiBaoDMMienThueNK_3()
        {
            InitializeComponent();
        }
        public void BindingReport(VAD8010 vad8010,string maNV)
        {
            lblTenThongTinXuat.Text = EnumThongBao.GetTenNV(maNV);
            int HangHoa = vad8010.HangHoa.Count;
            if (HangHoa <= 8)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 8, 0, MidpointRounding.AwayFromZero)).ToString();
            }

            DataTable dt = new DataTable();
            if (vad8010.HangHoa != null && vad8010.HangHoa.Count > 0)
            {
                
                dt.Columns.Add("SoDong", typeof(string));
                dt.Columns.Add("MoTaHH", typeof(string));
                dt.Columns.Add("SoLuongDKMT", typeof(string));
                dt.Columns.Add("DonViSoLuongDKMT", typeof(string));
                dt.Columns.Add("SoLuongDaSD", typeof(string));
                dt.Columns.Add("DonViSoLuongDaSD", typeof(string));
                dt.Columns.Add("SoLuongConLai", typeof(string));
                dt.Columns.Add("DonViSoLuongConLai", typeof(string));
                dt.Columns.Add("TriGia", typeof(string));
                dt.Columns.Add("TriGiaDuKien", typeof(string));
                for (int i = 0; i < vad8010.HangHoa.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["SoDong"] = vad8010.HangHoa[i].D01.GetValue().ToString().Trim();
                    dr["MoTaHH"] = vad8010.HangHoa[i].D02.GetValue().ToString().Trim();
                    dr["SoLuongDKMT"] = vad8010.HangHoa[i].D03.GetValue().ToString().Trim();
                    dr["DonViSoLuongDKMT"] = vad8010.HangHoa[i].D04.GetValue().ToString().Trim();
                    dr["SoLuongDaSD"] = vad8010.HangHoa[i].D05.GetValue().ToString().Trim();
                    dr["DonViSoLuongDaSD"] = vad8010.HangHoa[i].D06.GetValue().ToString().Trim();
                    dr["SoLuongConLai"] = vad8010.HangHoa[i].D07.GetValue().ToString().Trim();
                    dr["DonViSoLuongConLai"] = vad8010.HangHoa[i].D08.GetValue().ToString().Trim();
                    dr["TriGia"] = vad8010.HangHoa[i].D09.GetValue().ToString().Trim();
                    dr["TriGiaDuKien"] = vad8010.HangHoa[i].D10.GetValue().ToString().Trim();

                    dt.Rows.Add(dr);
                }
                BindingReportHang(dt);
            }
        }
        public void BindingReport(VAD8020 vad8010, string maNV)
        {
            lblTenThongTinXuat.Text = EnumThongBao.GetTenNV(maNV);
            int HangHoa = vad8010.HangHoa.Count;
            if (HangHoa <= 8)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 8, 0, MidpointRounding.AwayFromZero)).ToString();
            }

            DataTable dt = new DataTable();
            if (vad8010.HangHoa != null && vad8010.HangHoa.Count > 0)
            {

                dt.Columns.Add("SoDong", typeof(string));
                dt.Columns.Add("MoTaHH", typeof(string));
                dt.Columns.Add("SoLuongDKMT", typeof(string));
                dt.Columns.Add("DonViSoLuongDKMT", typeof(string));
                dt.Columns.Add("SoLuongDaSD", typeof(string));
                dt.Columns.Add("DonViSoLuongDaSD", typeof(string));
                dt.Columns.Add("SoLuongConLai", typeof(string));
                dt.Columns.Add("DonViSoLuongConLai", typeof(string));
                dt.Columns.Add("TriGia", typeof(string));
                dt.Columns.Add("TriGiaDuKien", typeof(string));
                for (int i = 0; i < vad8010.HangHoa.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["SoDong"] = vad8010.HangHoa[i].D01.GetValue().ToString().Trim();
                    dr["MoTaHH"] = vad8010.HangHoa[i].D02.GetValue().ToString().Trim();
                    dr["SoLuongDKMT"] = vad8010.HangHoa[i].D03.GetValue().ToString().Trim();
                    dr["DonViSoLuongDKMT"] = vad8010.HangHoa[i].D04.GetValue().ToString().Trim();
                    dr["SoLuongDaSD"] = vad8010.HangHoa[i].D05.GetValue().ToString().Trim();
                    dr["DonViSoLuongDaSD"] = vad8010.HangHoa[i].D06.GetValue().ToString().Trim();
                    dr["SoLuongConLai"] = vad8010.HangHoa[i].D07.GetValue().ToString().Trim();
                    dr["DonViSoLuongConLai"] = vad8010.HangHoa[i].D08.GetValue().ToString().Trim();
                    dr["TriGia"] = vad8010.HangHoa[i].D09.GetValue().ToString().Trim();
                    dr["TriGiaDuKien"] = vad8010.HangHoa[i].D10.GetValue().ToString().Trim();

                    dt.Rows.Add(dr);
                }
                BindingReportHang(dt);
            }
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSoDong.DataBindings.Add("Text", DetailReport.DataSource, "SoDong");
            lblMoTaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MoTaHH");
            lblSoLuongDKMT.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongDKMT");
            lblDonViSoLuongDKMT.DataBindings.Add("Text", DetailReport.DataSource, "DonViSoLuongDKMT");
            lblSoLuongDaSD.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongDaSD");
            lblDonViSoLuongDaSD.DataBindings.Add("Text", DetailReport.DataSource, "DonViSoLuongDaSD");
            lblSoLuongConLai.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongConLai");
            lblDonViSoLuongConLai.DataBindings.Add("Text", DetailReport.DataSource, "DonViSoLuongConLai");
            lblTriGia.DataBindings.Add("Text", DetailReport.DataSource, "TriGia");
            lblTriGiaDuKien.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaDuKien");
        }

        private void lblSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 3).ToString();
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
            if(lbl.Name.Contains("lblSoLuongDaSD"))
                if (lbl.Text.Trim() == "")
                    lbl.Text = "0";
        }

    }
}
