﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerRemote
{
    public enum TypeOfMessage
    {
        Success,
        Warning,
        Error,
    }
    public static class SplashScreen
    {
        static frmFlash sf = null;

        /// <summary>
        /// Displays the splashscreen
        /// </summary>
        public static void ShowSplashScreen()
        {
            try
            {
                if (sf == null)
                {
                    sf = new frmFlash();
                    sf.ShowSplashScreen();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("ShowSplashScreen", ex);
            }

        }
        /// <summary>
        /// Closes the SplashScreen
        /// </summary>
        public static void CloseSplashScreen()
        {
            try
            {
                if (sf != null)
                {
                    sf.CloseSplashScreen();
                    sf = null;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("CloseSplashScreen", ex);
            }

        }

        /// <summary>
        /// Update text in default green color of success message
        /// </summary>
        /// <param name="Text">Message</param>
        public static void UpdateStatusText(string Text)
        {
            if (sf != null)
                sf.UpdateStatusText(Text);

        }

        /// <summary>
        /// Update text with message color defined as green/yellow/red/ for success/warning/failure
        /// </summary>
        /// <param name="Text">Message</param>
        /// <param name="tom">Type of Message</param>
        public static void UpdateStatusTextWithStatus(string Text, TypeOfMessage tom)
        {

            if (sf != null)
                sf.UpdateStatusTextWithStatus(Text, tom);
        }
    }
}
