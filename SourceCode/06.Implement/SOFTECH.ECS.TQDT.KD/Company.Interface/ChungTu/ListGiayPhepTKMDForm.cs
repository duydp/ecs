﻿using System;
using System.Drawing;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListGiayPhepTKMDForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListGiayPhepTKMDForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKMD.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            //if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
                //btnTaoMoi.Enabled = false;
                //btnXoa.Enabled = false;
            //}
                btnTaoMoi.Enabled = true;
                btnXoa.Enabled = true ;
        }

  
      

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<GiayPhep> listGiayPhep = new List<GiayPhep>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiayPhep gp = (GiayPhep)i.GetRow().DataRow;
                        listGiayPhep.Add(gp);
                    }
                }
            }

            foreach (GiayPhep gp in listGiayPhep)
            {
                if (gp.ID > 0)
                {
                    gp.Delete();
                }
                TKMD.GiayPhepCollection.Remove(gp);
            }

            dgList.DataSource = TKMD.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

      
        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            GiayPhepForm f = new GiayPhepForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKMD.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GiayPhepForm f = new GiayPhepForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.giayPhep = (GiayPhep)e.Row.DataRow;
            if (f.giayPhep.ID > 0 && f.giayPhep.ListHMDofGiayPhep.Count == 0)
                f.giayPhep.LoadListHMDofGiayPhep();

            f.ShowDialog();
            dgList.DataSource = TKMD.GiayPhepCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

     


    }
}
