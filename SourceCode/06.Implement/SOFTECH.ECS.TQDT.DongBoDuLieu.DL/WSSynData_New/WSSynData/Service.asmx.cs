﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using Company.KDT.SHARE.Components;
using System.Data;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.Controls;
using Company.BLL.VNACCS;

namespace WSSynData
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        #region USER AGENT - ĐẠI LÝ

        /// <summary>
        /// Kiểm tra thông tin người dùng Đại lý.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <returns></returns>
        [WebMethod]
        public Company.KDT.SHARE.Components.DaiLy LoginDaiLy(string userNameLogin, string passWordLogin)
        {
            Company.KDT.SHARE.Components.DaiLy user = Company.KDT.SHARE.Components.DaiLy.Load(userNameLogin, passWordLogin);
            return user;
        }

        /// <summary>
        /// Danh sách người dùng Đại lý
        /// </summary>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public List<Company.KDT.SHARE.Components.DaiLy> SelectUserDaiLy(string maDoanhNghiep)
        {
            return Company.KDT.SHARE.Components.DaiLy.SelectUserDaiLy(maDoanhNghiep);
        }

        [WebMethod]
        public List<Company.KDT.SHARE.Components.DaiLy> SelectUserDaiLyAll()
        {
            return (List<Company.KDT.SHARE.Components.DaiLy>)Company.KDT.SHARE.Components.DaiLy.SelectCollectionAll();

        }

        /// <summary>
        /// Tạo mới người dùng Đại lý
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="tenNguoiDung"></param>
        /// <param name="matKhau"></param>
        /// <param name="hoVaTen"></param>
        /// <param name="moTa"></param>
        /// <param name="laQuanTri"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public int InsertUpdateUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung, string matKhau, string hoVaTen, string moTa, bool laQuanTri, string maDoanhNghiep)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
                return Company.KDT.SHARE.Components.DaiLy.InsertUpdateUserDaiLy(tenNguoiDung, matKhau, hoVaTen, moTa, laQuanTri, maDoanhNghiep);
            else
                return 0;
        }

        /// <summary>
        /// Xóa người dùng Đại lý
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="tenNguoiDung"></param>
        /// <param name="matKhau"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DeleteUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung, string matKhau)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
                return Company.KDT.SHARE.Components.DaiLy.DeleteUserDaiLy(tenNguoiDung, matKhau);
            else
                return false;
        }

        /// <summary>
        /// Kiểm tra tên người dùng đã tồn tại?.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public bool CheckUserName(string userName)
        {
            Company.QuanTri.User user = new Company.QuanTri.User();

            return user.CheckUserName(userName);
        }

        #endregion

        #region TỜ KHAI

        /// <summary>
        /// Gửi dữ liệu một tờ khai và hàng mậu dịch, các thông tin liên quan đến tờ khai.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="TKMD"></param>
        /// <param name="overwrite"></param>
        /// <returns></returns>
        [WebMethod]
        public string Send(string userNameLogin, string passWordLogin, ref Company.BLL.KDT.ToKhaiMauDich TKMD)
        {
            string error = "";
            //Neu login thanh cong
            Company.KDT.SHARE.Components.DaiLy userDaiLy = LoginDaiLy(userNameLogin, passWordLogin);

            if (userDaiLy != null)
            {
                try
                {
                    //Insert to khai va hang mau dich
                    TKMD.InsertUpdateFullDaiLy();
                    error = "";
                }
                catch (Exception ex) { error = ex.Message; }

                //Them vao bang t_DongBoDuLieu_Track
                Company.KDT.SHARE.Components.Track newTrack = new Company.KDT.SHARE.Components.Track();
                newTrack.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                newTrack.MaHaiQuan = TKMD.MaHaiQuan;
                newTrack.SoToKhai = TKMD.SoToKhai;
                newTrack.NamDangKy = TKMD.NamDK;
                newTrack.MaLoaiHinh = TKMD.MaLoaiHinh;
                newTrack.TKMD_GUIDSTR = TKMD.GUIDSTR;
                //Ghi log Error neu co.
                newTrack.GhiChu = error;
                //1: Thanh cong; 0: Khong thanh cong.
                newTrack.TrangThaiDongBoTaiLen = error.Length == 0 ? 1 : 0;
                newTrack.TrangThaiDongBoTaiXuong = 0;
                //ID user dai ly gui du lieu den Server.
                newTrack.IDUserDaiLy = userDaiLy != null ? userDaiLy.ID : 0;
                newTrack.UserName = userDaiLy != null ? userDaiLy.USER_NAME : "";
                newTrack.NgayDongBo = DateTime.Now;

                newTrack.InsertUpdateBy();
            }
            else
                error = "MK"; /*Sai mật khẩu*/

            return error;
        }

        /// <summary>
        /// Gửi dữ liệu danh sách tờ khai và hàng mậu dịch, các thông tin liên quan đến tờ khai.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="TKMD"></param>
        /// <param name="overwrite"></param>
        /// <returns></returns>
        [WebMethod]
        public string Sends(string userNameLogin, string passWordLogin, ref Company.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string error = "";
            //Neu login thanh cong
            Company.KDT.SHARE.Components.DaiLy userDaiLy = LoginDaiLy(userNameLogin, passWordLogin);

            if (userDaiLy != null)
            {
                foreach (Company.BLL.KDT.ToKhaiMauDich item in TKMDCollection)
                {
                    try
                    {
                        //Insert to khai va hang mau dich
                        item.InsertUpdateFull();
                        error = "";
                    }
                    catch (Exception ex) { error = ex.Message; }

                    //Them vao bang t_DongBoDuLieu_Track
                    Company.KDT.SHARE.Components.Track newTrack = new Company.KDT.SHARE.Components.Track();
                    newTrack.MaDoanhNghiep = item.MaDoanhNghiep;
                    newTrack.MaHaiQuan = item.MaHaiQuan;
                    newTrack.SoToKhai = item.SoToKhai;
                    newTrack.NamDangKy = item.NamDK;
                    newTrack.MaLoaiHinh = item.MaLoaiHinh;
                    newTrack.TKMD_GUIDSTR = item.GUIDSTR;
                    //Ghi log Error neu co.
                    newTrack.GhiChu = error;
                    //1: Thanh cong; 0: Khong thanh cong.
                    newTrack.TrangThaiDongBoTaiLen = error.Length == 0 ? 1 : 0;
                    newTrack.TrangThaiDongBoTaiXuong = 0;
                    //ID user dai ly gui du lieu den Server.
                    newTrack.IDUserDaiLy = userDaiLy != null ? userDaiLy.ID : 0;
                    newTrack.UserName = userDaiLy != null ? userDaiLy.USER_NAME : "";
                    newTrack.NgayDongBo = DateTime.Now;

                    newTrack.Insert();

                    //Neu co loi xay ra -> Thoat khoi vong lap
                    if (error.Length > 0) break;
                }
            }

            return error;
        }

        /// <summary>
        /// Lấy thông tin danh sách tờ khai theo mã doanh nghiệp.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public Company.BLL.KDT.ToKhaiMauDichCollection Receives(string userNameLogin, string passWordLogin, string maDoanhNghiep, DateTime tuNgay, DateTime denNgay)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
                foreach (Company.BLL.KDT.ToKhaiMauDich item in tkmd.SelectCollectionByDaiLy(maDoanhNghiep, tuNgay, denNgay))
                {
                        
                    tkmd.LoadHMDCollection();
                    //Load Chung tu kem lien quan cua To khai.
                    //tkmd.LoadChungTuHaiQuan();
                }
            }

            return null;
        }

        /// <summary>
        /// Lấy thông tin danh sách tờ khai theo mã doanh nghiệp.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public Company.BLL.KDT.ToKhaiMauDichCollection ReceivesByDaiLy(string userNameLogin, string passWordLogin, string maDoanhNghiep, DateTime tuNgay, DateTime denNgay, int idUserDaiLy)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();

                return tkmd.SelectCollectionByDaiLy(maDoanhNghiep, tuNgay, denNgay, idUserDaiLy);
            }

            return null;
        }

        /// <summary>
        /// Lấy thông tin tờ khai theo mã doanh nghiệp.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="soToKhai"></param>
        /// <param name="namDangKy"></param>
        /// <param name="maLoaiHinh"></param>
        /// <param name="maHaiQuan"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public Company.BLL.KDT.ToKhaiMauDich Receive(string userNameLogin, string passWordLogin, int soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
                tkmd.Load(maHaiQuan, maDoanhNghiep, soToKhai, namDangKy, maLoaiHinh);

                //Load Hang hoa mau dich
                tkmd.LoadHMDCollection();

                //Load Chung tu kem lien quan cua To khai.
                tkmd.LoadChungTuHaiQuan();

                return tkmd;
            }

            return null;
        }

        #endregion

        #region TRACK

        [WebMethod]
        public bool InsertUpdateTrack(string userNameLogin, string passWordLogin, Company.KDT.SHARE.Components.Track log)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                log.InsertUpdateBy();

                return true;
            }

            return false;
        }

        [WebMethod]
        public Company.KDT.SHARE.Components.Track SelectTrack(string userNameLogin, string passWordLogin, long soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                return Company.KDT.SHARE.Components.Track.Load(soToKhai, namDangKy, maLoaiHinh, maHaiQuan, maDoanhNghiep);
            }

            return null;
        }
       


        [WebMethod]
        public List<Company.KDT.SHARE.Components.Track> SelectTrackDynamic(string userNameLogin, string passWordLogin, string whereCondition, string orderBy)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                return (List<Company.KDT.SHARE.Components.Track>)Company.KDT.SHARE.Components.Track.SelectCollectionDynamic(whereCondition, orderBy);
            }

            return null;
        }

        #endregion
        #region TracK_VANCC
        [WebMethod]
        public List<Company.BLL.VNACCS.DongBoDuLieu_TrackVNACCS> SelectTrack_VNACCS(string userNameLogin, string passWordLogin, string tuNgay,string denNgay, string madoanhnghiep, string userDaiLy)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                string where = string.Format("madoanhnghiep='{0}' And (NgayDangKy BETWEEN '{1}' and '{2}') AND IDUserDaiLy in ({3})", madoanhnghiep, tuNgay, denNgay, userDaiLy);
                return Company.BLL.VNACCS.DongBoDuLieu_TrackVNACCS.SelectCollectionDynamic(where,"");
            }

            return null;
        }
        [WebMethod]
        public List<string> SelectSoToKhai(string userNameLogin, string passWordLogin, string tuNgay, string denNgay, string madoanhnghiep, string userDaiLy)
        {
            
            //Neu login thanh cong
            
            if (LoginDaiLy(userNameLogin, passWordLogin)!=null)
            {
                string where = string.Format("IDUserDaiLy in ({0}) and  MaDoanhNghiep='{1}' and (NgayDangKy BETWEEN '{2}' and '{3}')", userDaiLy, madoanhnghiep, tuNgay, denNgay);
                return Company.BLL.VNACCS.DongBoDuLieu_TrackVNACCS.SelectListSoToKhai(where);
            }
            return null;
        }
        #endregion
        #region TỜ KHAI VNACCS
        [WebMethod]
        public string SendVNACCS(string userNameLogin, string passWordLogin, string datastr )
        {
            string error = "";
            //Neu login thanh cong
            Company.KDT.SHARE.Components.DaiLy userDaiLy = LoginDaiLy(userNameLogin, passWordLogin);

            if (userDaiLy != null)
            {
                try
                {
                    Company.BLL.VNACCS.DataVNACCSSync data = Company.KDT.SHARE.Components.Helpers.Deserialize<Company.BLL.VNACCS.DataVNACCSSync>(datastr);
                    //Insert data
                    if (data == null)
                        return "Lỗi data null";
                    data.insertDataFullToServer(userDaiLy.MaDoanhNghiep, userDaiLy.USER_NAME, userDaiLy.ID);
                    error = "";
                }
                catch (Exception ex) { error = ex.Message; }
                
            }
            else
                error = "MK"; /*Sai mật khẩu*/

            return error;
        }
        [WebMethod]
        public string ReceiveVNACCS(string sotokhai,DateTime ngayDangKy, string userNameLogin, string passWordLogin)
        {
            //Neu login thanh cong
            
            string error="";
            Company.KDT.SHARE.Components.DaiLy userDaiLy = LoginDaiLy(userNameLogin, passWordLogin);
            if (userDaiLy != null)
            {
                Company.BLL.VNACCS.DataVNACCSSync data = new Company.BLL.VNACCS.DataVNACCSSync(sotokhai,false,ngayDangKy);
                string datastr = Helpers.Serializer(data);
                return datastr;
            }
            return error= "Sai mật khẩu kết nối (PasswordERROR)" ;
        }
        [WebMethod]
        public string ReceiveVNACCSCTQ(string sotokhai, DateTime ngayDangKy, string userNameLogin, string passWordLogin, string trangThaiXuLy)
        {
            //Neu login thanh cong

            string error = "";
            Company.KDT.SHARE.Components.DaiLy userDaiLy = LoginDaiLy(userNameLogin, passWordLogin);
            if (userDaiLy != null)
            {
                DataVNACCSSync data = new DataVNACCSSync(sotokhai, false, ngayDangKy, trangThaiXuLy);
                string datastr = Helpers.Serializer(data);
                return datastr;
            }
            return error = "Sai mật khẩu kết nối (PasswordERROR)";
        }
        #endregion
        #region

        [WebMethod]
        public string ReceiveVNACCSCTQ_TEXT()
        {
            return "";
        }
        [WebMethod]
        public DataSet UpdateCategoryOnline(string password, DateTime dateLastUpdated, ECategory CategoryType)
        {
            //Neu login thanh cong
            DataSet dsCategory = new DataSet();
            if (password == "220c888ca1a69018c4a78bbff19e6273")
            {
                switch (CategoryType)
                {
                    case ECategory.A014:
                        dsCategory = VNACC_Category_CustomsSubSection.SelectAll();
                        break;
                    case ECategory.A015:
                        dsCategory = VNACC_Category_Nation.SelectAll();
                        break;
                    case ECategory.A016:
                        dsCategory = VNACC_Category_CityUNLOCODE.SelectAll();
                        break;
                    case ECategory.A016T:
                        dsCategory = VNACC_Category_CityUNLOCODE.SelectAll();
                        break;
                    case ECategory.A038:
                        dsCategory = VNACC_Category_CustomsOffice.SelectAll();
                        break;
                    case ECategory.A202:
                        dsCategory = VNACC_Category_Cargo.SelectAll();
                        break;
                    case ECategory.A204:
                        dsCategory = VNACC_Category_Cargo.SelectAll();
                        break;
                    case ECategory.A301:
                        dsCategory = VNACC_Category_ContainerSize.SelectAll();
                        break;
                    case ECategory.A316:
                        dsCategory = VNACC_Category_PackagesUnit.SelectAll();
                        break;
                    case ECategory.A404:
                        dsCategory = VNACC_Category_TaxClassificationCode.SelectAll();
                        break;
                    case ECategory.A501:
                        dsCategory = VNACC_Category_QuantityUnit.SelectAll();
                        break;
                    case ECategory.A506:
                        dsCategory = VNACC_Category_HSCode.SelectAll();
                        break;
                    case ECategory.A527:
                        dsCategory = VNACC_Category_CurrencyExchange.SelectAll();
                        break;
                    case ECategory.A546:
                        dsCategory = VNACC_Category_ApplicationProcedureType.SelectAll();
                        break;
                    case ECategory.A601:
                        dsCategory = VNACC_Category_Station.SelectAll();
                        break;
                    case ECategory.A620:
                        dsCategory = VNACC_Category_BorderGate.SelectAll();
                        break;
                    case ECategory.A621:
                        dsCategory = VNACC_Category_TransportMean.SelectAll();
                        break;
                    case ECategory.A700:
                        dsCategory = VNACC_Category_OGAUser.SelectAll();
                        break;
                    case ECategory.E001:
                        dsCategory = VNACC_Category_Common.SelectAll();
                        break;
                    default:
                        break;
                }
                return dsCategory;   
            }
            return dsCategory;
        }
        #endregion
    }

}
