using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;


namespace Company.BLL.SXXK.ThanhKhoan
{
    public partial class NPLNhapTon 
    {

        #region SelectMethod_NhieuToKhai
        public NPLNhapTonCollection KhaiBaoNhapTon(string where)
        {
            StringBuilder strBD = new StringBuilder(string.Empty);
            
            strBD.Append(" SELECT  ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.SoToKhai , ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.MaLoaiHinh , ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.NamDangKy , ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.MaHaiQuan , ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.MaDoanhNghiep , ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.MaNPL,  ");          
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.Luong,  ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.Ton,  ");
            strBD.Append(" (t_SXXK_ThanhLy_NPLNhapTon.Ton * t_SXXK_ThanhLy_NPLNhapTon.ThueXNK)/t_SXXK_ThanhLy_NPLNhapTon.Luong  as ThueTon,  ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.ThueXNK,  ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.ThueTTDB,  ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.ThueVAT,  ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.PhuThu, ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon.ThueCLGia  ");
            strBD.Append(" FROM  ");
            strBD.Append(" t_SXXK_ThanhLy_NPLNhapTon  ");           
            if (where != "")
                strBD.Append("WHERE "+where);
        
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(strBD.ToString());
            NPLNhapTonCollection collection = new NPLNhapTonCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            
            while (reader.Read())
            {
                NPLNhapTon entity = new NPLNhapTon();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai  = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTon"))) entity.ThueTon = reader.GetDouble(reader.GetOrdinal("ThueTon")); // add as
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB= reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT= reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueCLGia"))) entity.ThueCLGia = reader.GetDouble(reader.GetOrdinal("ThueCLGia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;

        }
        #endregion

        #region SelectMethod_MotToKhai
        public DataSet  KhaiBaoChiTietNhapTon()
        {
            StringBuilder strBD = new StringBuilder(string.Empty);

            strBD.Append(" SELECT  ");
            strBD.Append(" v_ThongKeNPLTon.SoToKhai , ");
            strBD.Append(" v_ThongKeNPLTon.MaLoaiHinh , ");
            strBD.Append(" v_ThongKeNPLTon.NamDangKy , ");
            strBD.Append(" v_ThongKeNPLTon.MaHaiQuan , ");
            strBD.Append(" v_ThongKeNPLTon.MaNPL,  ");            
            strBD.Append(" v_ThongKeNPLTon.Luong,  ");
            strBD.Append(" v_ThongKeNPLTon.Ton,  ");
            strBD.Append(" (v_ThongKeNPLTon.Ton * v_ThongKeNPLTon.ThueXNK)/v_ThongKeNPLTon.Luong  as ThueTon,  ");
            strBD.Append(" v_ThongKeNPLTon.ThueXNK,  ");
            strBD.Append(" v_ThongKeNPLTon.ThueTTDB,  ");
            strBD.Append(" v_ThongKeNPLTon.ThueVAT,  ");
            strBD.Append(" v_ThongKeNPLTon.PhuThu, ");
            strBD.Append(" v_ThongKeNPLTon.ThueCLGia  ");
            strBD.Append(" FROM  ");
            strBD.Append(" v_ThongKeNPLTon  ");           
            strBD.Append(" WHERE  v_ThongKeNPLTon.Ton >= 0 ");
            strBD.Append(" AND ");
            strBD.Append("  v_ThongKeNPLTon.SoToKhai =@SoToKhai ");
            strBD.Append(" AND ");
            strBD.Append(" v_ThongKeNPLTon.MaLoaiHinh = @MaLoaiHinh ");
            strBD.Append(" AND ");
            strBD.Append("   v_ThongKeNPLTon.NamDangKy = @NamDangKy ");
            strBD.Append(" AND ");
            strBD.Append(" v_ThongKeNPLTon.MaHaiQuan = @MaHaiQuan ");
            strBD.Append(" AND ");
            strBD.Append(" v_ThongKeNPLTon.MaNPL =@MaNPL ");

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(strBD.ToString());

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);            
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);

            return this.db.ExecuteDataSet(dbCommand);

        }
        #endregion

        
        #region Thong ke 
        public NPLNhapTonCollection ThongKeHangTon()
        {
            StringBuilder strBD = new StringBuilder(string.Empty);

            strBD.Append(" SELECT  ");            
            strBD.Append(" v_ThongKeNPLTon.MaNPL,  ");
            strBD.Append(" v_ThongKeNPLTon.TenHang,  ");
            strBD.Append("  SUM (v_ThongKeNPLTon.Ton) as TotalNPL  ");
            strBD.Append(" FROM  ");
            strBD.Append(" v_ThongKeNPLTon  ");
            
            strBD.Append("  GROUP BY ");
            strBD.Append(" v_ThongKeNPLTon.MaNPL, ");
            strBD.Append(" v_ThongKeNPLTon.TenHang ");
            strBD.Append(" HAVING SUM (v_ThongKeNPLTon.Ton) >=0 ");
            strBD.Append(" ORDER BY v_ThongKeNPLTon.MaNPL ");

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(strBD.ToString());
            NPLNhapTonCollection collection = new NPLNhapTonCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLNhapTon entity = new NPLNhapTon();
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TotalNPL"))) entity.TotalNPL = reader.GetDecimal (reader.GetOrdinal("TotalNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenNPL = reader.GetString (reader.GetOrdinal("TenHang"));
                collection.Add(entity);
            }
            return collection;

        }

        #endregion 

        
        #region Dinhmuc

        #endregion
        
        #region Tongton

        #endregion

        public NPLNhapTon Copy()
        {
            NPLNhapTon temp = new NPLNhapTon();
            temp.SoToKhai = this.SoToKhai;
            temp.MaHaiQuan = this.MaHaiQuan;
            temp.MaLoaiHinh = this.MaLoaiHinh;
            temp.NamDangKy = this.NamDangKy;
            temp.MaNPL = this.MaNPL;
            temp.Load();
            return temp;
        }

    }
}
