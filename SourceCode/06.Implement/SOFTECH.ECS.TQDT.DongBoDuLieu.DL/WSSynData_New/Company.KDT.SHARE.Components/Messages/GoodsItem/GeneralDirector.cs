﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
    [XmlRoot("Importer")]
   public class GeneralDirector
    {
        /// <summary>
        /// Số CMND/hộ chiếu
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; }

        /// <summary>
        /// Ngày cấp giấy phép
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Nơi cấp giấy phép
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Nơi đăng ký hộ khẩu thường trú
        /// </summary>
        [XmlElement("permanentResidence")]
        public string PermanentResidence { get; set; }

        /// <summary>
        /// Số điện thoại
        /// </summary>
        [XmlElement("phoneNumbers")]
        public string PhoneNumbers { get; set; }
    }
}
