using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;

namespace Company.Interface.Controls
{
    public partial class VComboBox : UserControl
    {
        public VComboBox()
        {
            this.InitializeComponent();
        }

        private string _DuLieu;
        public string DuLieu
        {
            set { this._DuLieu = value; }
        }

        public string Ma
        {
            set
            {
                this.txtMa.Text = value;
                this.cbTen.Value = this.txtMa.Text.PadRight(4);
            }
            get { return this.txtMa.Text; }
        }

        public string Ten
        {
            get { return this.cbTen.Text; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.txtMa.VisualStyleManager = value;
                this.cbTen.VisualStyleManager = value;
            }
            get
            {
                return this.txtMa.VisualStyleManager;
            }
        }
        public string ErrorMessage
        {
            get { return this.rfvTen.ErrorMessage; }
            set { this.rfvTen.ErrorMessage = value; }
        }

        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);

        private void loadData()
        {
            switch (this._DuLieu)
            {
                case "HaiQuan":
                    break;
            }


            if (this.txtMa.Text.Trim().Length == 0) this.txtMa.Text = GlobalSettings.NGUYEN_TE_MAC_DINH;
            DataTable dt = NguyenTe.SelectAll();
            foreach (DataRow row in dt.Rows)
            {
                this.dtNguyenTe.ImportRow(row);
            }
        }

        private void NguyenTeControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.loadData();
            }
        }

        private void cbList_ValueChanged(object sender, EventArgs e)
        {
            this.txtMa.Text = this.cbTen.Value.ToString().Trim();
            if (this.ValueChanged != null) this.ValueChanged(sender, e);
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {
            this.txtMa.Text = this.txtMa.Text.Trim();
            this.cbTen.Value = this.txtMa.Text.PadRight(4);
        }
    }
}