﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class HangCODetail
    {
        #region Properties.

        public long ID { set; get; }
        public long CO_ID { set; get; }
        public int SoThuTuHang { set; get; }
        public double TriGiaTinhThue { set; get; }
        public string MaNguyenTe { set; get; }
        public int SoKien { set; get; }
        public string LoaiKien { set; get; }
        public string NhanMac { set; get; }
        public string MoTaHangHoa { set; get; }
        public string MaHangDoanhNghiepKhai { set; get; }
        public string MaHS { set; get; }
        public double TrongLuong { set; get; }
        public string MaDonViTinh { set; get; }
        public string NuocXX_ID { set; get; }
        public string SoHoaDon { set; get; }
        public DateTime NgayHoaDon { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<HangCODetail> ConvertToCollection(IDataReader reader)
        {
            IList<HangCODetail> collection = new List<HangCODetail>();
            while (reader.Read())
            {
                HangCODetail entity = new HangCODetail();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CO_ID"))) entity.CO_ID = reader.GetInt64(reader.GetOrdinal("CO_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThue"))) entity.TriGiaTinhThue = reader.GetDouble(reader.GetOrdinal("TriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenTe"))) entity.MaNguyenTe = reader.GetString(reader.GetOrdinal("MaNguyenTe"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetInt32(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKien"))) entity.LoaiKien = reader.GetString(reader.GetOrdinal("LoaiKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhanMac"))) entity.NhanMac = reader.GetString(reader.GetOrdinal("NhanMac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MoTaHangHoa"))) entity.MoTaHangHoa = reader.GetString(reader.GetOrdinal("MoTaHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangDoanhNghiepKhai"))) entity.MaHangDoanhNghiepKhai = reader.GetString(reader.GetOrdinal("MaHangDoanhNghiepKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDouble(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViTinh"))) entity.MaDonViTinh = reader.GetString(reader.GetOrdinal("MaDonViTinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDon"))) entity.NgayHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayHoaDon"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static HangCODetail Load(long id)
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<HangCODetail> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<HangCODetail> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<HangCODetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<HangCODetail> SelectCollectionBy_CO_ID(long cO_ID)
        {
            IDataReader reader = SelectReaderBy_CO_ID(cO_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_CO_ID(long cO_ID)
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_SelectBy_CO_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@CO_ID", SqlDbType.BigInt, cO_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_CO_ID(long cO_ID)
        {
            const string spName = "p_KDT_HangCODetail_SelectBy_CO_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@CO_ID", SqlDbType.BigInt, cO_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertHangCODetail(long cO_ID, int soThuTuHang, double triGiaTinhThue, string maNguyenTe, int soKien, string loaiKien, string nhanMac, string moTaHangHoa, string maHangDoanhNghiepKhai, string maHS, double trongLuong, string maDonViTinh, string nuocXX_ID, string soHoaDon, DateTime ngayHoaDon)
        {
            HangCODetail entity = new HangCODetail();
            entity.CO_ID = cO_ID;
            entity.SoThuTuHang = soThuTuHang;
            entity.TriGiaTinhThue = triGiaTinhThue;
            entity.MaNguyenTe = maNguyenTe;
            entity.SoKien = soKien;
            entity.LoaiKien = loaiKien;
            entity.NhanMac = nhanMac;
            entity.MoTaHangHoa = moTaHangHoa;
            entity.MaHangDoanhNghiepKhai = maHangDoanhNghiepKhai;
            entity.MaHS = maHS;
            entity.TrongLuong = trongLuong;
            entity.MaDonViTinh = maDonViTinh;
            entity.NuocXX_ID = nuocXX_ID;
            entity.SoHoaDon = soHoaDon;
            entity.NgayHoaDon = ngayHoaDon;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@CO_ID", SqlDbType.BigInt, CO_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Float, TriGiaTinhThue);
            db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Int, SoKien);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.VarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@NhanMac", SqlDbType.VarChar, NhanMac);
            db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.VarChar, MoTaHangHoa);
            db.AddInParameter(dbCommand, "@MaHangDoanhNghiepKhai", SqlDbType.VarChar, MaHangDoanhNghiepKhai);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
            db.AddInParameter(dbCommand, "@MaDonViTinh", SqlDbType.VarChar, MaDonViTinh);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.VarChar, NuocXX_ID);
            db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.NVarChar, SoHoaDon);
            db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year <= 1753 ? DBNull.Value : (object)NgayHoaDon);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<HangCODetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangCODetail item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateHangCODetail(long id, long cO_ID, int soThuTuHang, double triGiaTinhThue, string maNguyenTe, int soKien, string loaiKien, string nhanMac, string moTaHangHoa, string maHangDoanhNghiepKhai, string maHS, double trongLuong, string maDonViTinh, string nuocXX_ID, string soHoaDon, DateTime ngayHoaDon)
        {
            HangCODetail entity = new HangCODetail();
            entity.ID = id;
            entity.CO_ID = cO_ID;
            entity.SoThuTuHang = soThuTuHang;
            entity.TriGiaTinhThue = triGiaTinhThue;
            entity.MaNguyenTe = maNguyenTe;
            entity.SoKien = soKien;
            entity.LoaiKien = loaiKien;
            entity.NhanMac = nhanMac;
            entity.MoTaHangHoa = moTaHangHoa;
            entity.MaHangDoanhNghiepKhai = maHangDoanhNghiepKhai;
            entity.MaHS = maHS;
            entity.TrongLuong = trongLuong;
            entity.MaDonViTinh = maDonViTinh;
            entity.NuocXX_ID = nuocXX_ID;
            entity.SoHoaDon = soHoaDon;
            entity.NgayHoaDon = ngayHoaDon;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_HangCODetail_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@CO_ID", SqlDbType.BigInt, CO_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Float, TriGiaTinhThue);
            db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Int, SoKien);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.VarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@NhanMac", SqlDbType.VarChar, NhanMac);
            db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.VarChar, MoTaHangHoa);
            db.AddInParameter(dbCommand, "@MaHangDoanhNghiepKhai", SqlDbType.VarChar, MaHangDoanhNghiepKhai);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
            db.AddInParameter(dbCommand, "@MaDonViTinh", SqlDbType.VarChar, MaDonViTinh);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.VarChar, NuocXX_ID);
            db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.NVarChar, SoHoaDon);
            db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year <= 1753 ? DBNull.Value : (object)NgayHoaDon);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<HangCODetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangCODetail item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateHangCODetail(long id, long cO_ID, int soThuTuHang, double triGiaTinhThue, string maNguyenTe, int soKien, string loaiKien, string nhanMac, string moTaHangHoa, string maHangDoanhNghiepKhai, string maHS, double trongLuong, string maDonViTinh, string nuocXX_ID, string soHoaDon, DateTime ngayHoaDon)
        {
            HangCODetail entity = new HangCODetail();
            entity.ID = id;
            entity.CO_ID = cO_ID;
            entity.SoThuTuHang = soThuTuHang;
            entity.TriGiaTinhThue = triGiaTinhThue;
            entity.MaNguyenTe = maNguyenTe;
            entity.SoKien = soKien;
            entity.LoaiKien = loaiKien;
            entity.NhanMac = nhanMac;
            entity.MoTaHangHoa = moTaHangHoa;
            entity.MaHangDoanhNghiepKhai = maHangDoanhNghiepKhai;
            entity.MaHS = maHS;
            entity.TrongLuong = trongLuong;
            entity.MaDonViTinh = maDonViTinh;
            entity.NuocXX_ID = nuocXX_ID;
            entity.SoHoaDon = soHoaDon;
            entity.NgayHoaDon = ngayHoaDon;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@CO_ID", SqlDbType.BigInt, CO_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Float, TriGiaTinhThue);
            db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Int, SoKien);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.VarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@NhanMac", SqlDbType.VarChar, NhanMac);
            db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.VarChar, MoTaHangHoa);
            db.AddInParameter(dbCommand, "@MaHangDoanhNghiepKhai", SqlDbType.VarChar, MaHangDoanhNghiepKhai);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
            db.AddInParameter(dbCommand, "@MaDonViTinh", SqlDbType.VarChar, MaDonViTinh);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.VarChar, NuocXX_ID);
            db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.NVarChar, SoHoaDon);
            db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year <= 1753 ? DBNull.Value : (object)NgayHoaDon);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<HangCODetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangCODetail item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteHangCODetail(long id)
        {
            HangCODetail entity = new HangCODetail();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_CO_ID(long cO_ID)
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_DeleteBy_CO_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@CO_ID", SqlDbType.BigInt, cO_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_HangCODetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<HangCODetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangCODetail item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}