﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAGP110 : BasicVNACC
    {

        public List<VAGP110_HANG> HangHoa { get; set; }
        public void LoadVAGP110(string strResult)
        {
            try
            {
                this.GetObject<VAGP110>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAGP110, false, VAGP110.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAGP110.TongSoByte);
                while (true)
                {
                    VAGP110_HANG ivaHang = new VAGP110_HANG();
                    ivaHang.GetObject<VAGP110_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAGP110_HANG", false, VAGP110_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAGP110_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAGP110_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAGP110_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAGP110_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
