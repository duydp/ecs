﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class CoppyDinhMucForm : Company.Interface.BaseForm
    {
        public string MaSanPham = "";
        public List<DinhMuc> DMCollectionCopy = new List<DinhMuc>();
        public List<DinhMuc> DMCollectionRemove = new List<DinhMuc>();
        public List<DinhMuc> DMCollection = new List<DinhMuc>();
        public DinhMucDangKy dmDangKy;
        public CoppyDinhMucForm()
        {
            InitializeComponent();
        }

        private void CoppyDinhMucForm_Load(object sender, EventArgs e)
        {
            this.Text = "SAO CHÉP ĐỊNH MỨC CHO MÃ SẢN PHẨM : " + MaSanPham.ToString();
            BindData();
        }
        private void BindData()
        {
            try
            {
                foreach (DinhMuc item in dmDangKy.DMCollection)
                {
                    if (item.MaSanPham != MaSanPham)
                    {
                        DMCollection.Add(item);
                    }
                }
                dgList.Refresh();
                dgList.DataSource = DMCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAdd = true;
                DMCollectionCopy = new List<DinhMuc>();
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    isAdd = true;
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    DinhMuc dinhmuc = (DinhMuc)item.DataRow;

                    DinhMuc dmCoppy = new DinhMuc();
                    dmCoppy.MaSanPham = MaSanPham;
                    dmCoppy.MaNguyenPhuLieu = dinhmuc.MaNguyenPhuLieu;
                    dmCoppy.TenNPL = dinhmuc.TenNPL;
                    dmCoppy.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                    dmCoppy.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                    dmCoppy.GhiChu = dmCoppy.GhiChu;

                    //KIỂM TRA CHỌN TRÙNG NHIỀU MÃ NPL KHI CHỌN
                    foreach (DinhMuc it in DMCollectionCopy)
                    {
                        if (it.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                        {
                            if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                            {
                                it.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ GHỘP ĐỊNH MỨC CỦA MÃ NPL NÀY KHÔNG ?", true) == "Yes")
                            {
                                it.DinhMucSuDung = it.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = it.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else
                            {
                                isAdd = false;
                            }
                        }
                    }
                    if (isAdd)
                        DMCollectionCopy.Add(dmCoppy);
                }
                if (DMCollectionCopy.Count > 0)
                {
                    //KIỂM TRA TRÙNG MÃ NPL ĐÃ NHẬP ĐỊNH MỨC TRƯỚC ĐÓ
                    foreach (DinhMuc ite in DMCollectionCopy)
                    {
                        foreach (DinhMuc dinhmuc in dmDangKy.DMCollection)
                        {
                            if (ite.MaSanPham == dinhmuc.MaSanPham && ite.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                            {
                                if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                                {
                                    dinhmuc.DinhMucSuDung = ite.DinhMucSuDung;
                                    dinhmuc.TyLeHaoHut = ite.TyLeHaoHut;
                                    DMCollectionRemove.Add(ite);
                                    break;
                                }
                                else if ((ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN GHỘP ĐỊNH MỨC CỦA MÃ NPL KHÔNG ?", true) == "Yes"))
                                {
                                    dinhmuc.DinhMucSuDung = ite.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                    dinhmuc.TyLeHaoHut = ite.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                    DMCollectionRemove.Add(ite);
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DinhMuc item in DMCollectionRemove)
                    {
                        DMCollectionCopy.Remove(item);
                    }
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
