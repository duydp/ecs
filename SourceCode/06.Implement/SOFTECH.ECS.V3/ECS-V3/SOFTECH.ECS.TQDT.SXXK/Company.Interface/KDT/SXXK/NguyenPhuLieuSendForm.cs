﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuSendForm : BaseForm
    {
        private readonly NguyenPhuLieuDangKy nplDangKy = new NguyenPhuLieuDangKy();
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        private string xmlCurrent = "";



        //-----------------------------------------------------------------------------------------
        public NguyenPhuLieuSendForm()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }

        //-----------------------------------------------------------------------------------------
        private void NguyenPhuLieuSendForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            dgList.DataSource = this.nplDangKy.NPLCollection;


            //InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        }
        private void SetCommand()
        {
            bool allowSend = (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO);
            XacNhan.Enabled = allowSend ? Janus.Windows.UI.InheritableBoolean.False : Janus.Windows.UI.InheritableBoolean.True;
            cmdSend.Enabled = cmdAdd.Enabled = cmdSave.Enabled = cmdSend.Enabled = cmdSend1.Enabled = allowSend ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
            btnDelete.Enabled = allowSend;
            if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Chờ duyệt chính thức";
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Đã duyệt";
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Không phê duyệt";
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Hủy khai báo";
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Chờ hủy";
            }
            else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                txtSoTiepNhan.Text = nplDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Đã hủy thành công";
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void LaySoTiepNhanDT()
        {
            string password = "";
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "NPL";
            sendXML.master_id = nplDangKy.ID;
            if (!sendXML.Load())
            {
                // ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                return;
            }
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                {
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    xmlCurrent = nplDangKy.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    //string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    //  ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + nplDangKy.SoTiepNhan, false);
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + nplDangKy.SoTiepNhan, "MSG_SEN05", "" + nplDangKy.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.nplDangKy.SoTiepNhan.ToString();

                    //lblTrangThai.Text = "Chờ duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    else
                        lblTrangThai.Text = "Wait to approve";
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = nplDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                                hd.TrangThai = nplDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                            {
                                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                sendXML.Delete();
                            }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);

                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = nplDangKy.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                //nplDangKy.Wait -= new EventWait(nplDangKy_Wait);
                //nplDangKy.Wait += new EventWait(nplDangKy_Wait);
                xmlCurrent = nplDangKy.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    // string kq=ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + nplDangKy.SoTiepNhan, false);
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + nplDangKy.SoTiepNhan, "MSG_SEN05", "" + nplDangKy.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.nplDangKy.SoTiepNhan.ToString();
                    //lblTrangThai.Text = "Chờ duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    else
                        lblTrangThai.Text = "Wait to approve";
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = nplDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                                hd.TrangThai = nplDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void send()
        {

            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                if (this.nplDangKy.NPLCollection.Count == 0)
                {
                    // ShowMessage("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    return;
                }
                // Master.
                this.nplDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN.Trim();
                this.nplDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI.Trim();
                this.nplDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY.Trim();

                // Thực hiện gửi thông tin.
                string[] danhsachDaDangKy = new string[0];

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                xmlCurrent = this.nplDangKy.WSSendXML(password);
                this.Cursor = Cursors.Default;

                //lblTrangThai.Text = "Chờ xác nhận phía hải quan";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Chờ xác nhận phía hải quan";
                }
                else
                {
                    lblTrangThai.Text = "Wait to confirm from Customs";
                }
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                // Thực hiện kiểm tra.        
                sendXML = new MsgSend();
                sendXML.LoaiHS = "NPL";
                sendXML.master_id = nplDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
                if (this.nplDangKy.SoTiepNhan > 0)
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //  if (ShowMessage("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = nplDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                                hd.TrangThai = nplDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.KHAI_BAO;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                                GlobalSettings.PassWordDT = "";
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới nguyên phụ liệu.
        /// </summary>
        private void add()
        {
            NguyenPhuLieuEditForm f = new NguyenPhuLieuEditForm();
            f.OpenType = OpenFormType.Insert;
            f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            f.NPLCollection = nplDangKy.NPLCollection;
            f.nplDangKy = nplDangKy;
            f.ShowDialog(this);

            if (f.NPLDetail != null)
            {
                this.nplDangKy.NPLCollection.Add(f.NPLDetail);
                dgList.Refetch();
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void save()
        {
            try
            {
                if (this.nplDangKy.NPLCollection.Count == 0)
                {
                    //ShowMessage("Danh sách nguyên phụ liệu rỗng.\nKhông thể cập nhật dữ liệu.", false);
                    MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể cập nhật dữ liệu.", "MSG_SAV11", "", false);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                // Master.
                this.nplDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (this.nplDangKy.ID == 0)
                {
                    this.nplDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    this.nplDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.nplDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.nplDangKy.NgayTiepNhan = new DateTime(1900, 1, 1);
                    //add lannt
                    this.nplDangKy.GUIDSTR = Guid.NewGuid().ToString();
                }
                // Detail.
                int sttHang = 1;
                foreach (NguyenPhuLieu nplD in this.nplDangKy.NPLCollection)
                {
                    nplD.STTHang = sttHang++;
                }

                if (this.nplDangKy.InsertUpdateFull())
                {
                    this.Cursor = Cursors.Default;


                    //if (GlobalSettings.NGON_NGU == "0")
                    //{
                    //    ShowMessage("Cập nhật thành công!", false);
                    //}
                    //else
                    //{
                    //    Message("MSG_SAV02", "", false);
                    //}

                    #region Lưu log thao tác
                    string where = "1 = 1";
                    where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", nplDangKy.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.NguyenPhuLieu);
                    List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                    if (listLog.Count > 0)
                    {
                        long idLog = listLog[0].IDLog;
                        string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                        long idDK = listLog[0].ID_DK;
                        string guidstr = listLog[0].GUIDSTR_DK;
                        string userKhaiBao = listLog[0].UserNameKhaiBao;
                        DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                        string userSuaDoi = GlobalSettings.UserLog;
                        DateTime ngaySuaDoi = DateTime.Now;
                        string ghiChu = listLog[0].GhiChu;
                        bool isDelete = listLog[0].IsDelete;
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                    userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                    }
                    else
                    {
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                        log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.NguyenPhuLieu;
                        log.ID_DK = nplDangKy.ID;
                        log.GUIDSTR_DK = "";
                        log.UserNameKhaiBao = GlobalSettings.UserLog;
                        log.NgayKhaiBao = DateTime.Now;
                        log.UserNameSuaDoi = GlobalSettings.UserLog;
                        log.NgaySuaDoi = DateTime.Now;
                        log.GhiChu = "";
                        log.IsDelete = false;
                        log.Insert();
                    }
                    #endregion

                    MLMessages("Lưu thành công!", "MSG_SAV02", "", false);

                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else
                {
                    this.Cursor = Cursors.Default;
                    MLMessages("Lưu không thành công!", "MSG_SAV01", "", false);

                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("" + ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        //-----------------------------------------------------------------------------------------
        private void updateRowOnGrid(string maNPL)
        {
            GridEXRow[] jrows = dgList.GetRows();
            foreach (GridEXRow row in jrows)
            {
                if (row.Cells["Ma"].Value.ToString().Equals(maNPL))
                {
                    row.BeginEdit();
                    row.Cells["IsExistOnServer"].Value = 1;
                    row.EndEdit();
                    break;
                }
            }

        }

        //-----------------------------------------------------------------------------------------
        private void updateRowsOnGrid(IEnumerable<string> collection)
        {
            GridEXRow[] jrows = dgList.GetRows();
            foreach (GridEXRow row in jrows)
            {
                row.BeginEdit();
                row.Cells["IsExistOnServer"].Value = 0;
                row.EndEdit();
            }

            foreach (string s in collection)
            {
                this.updateRowOnGrid(s);
            }
        }

        #endregion


        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (txtSoTiepNhan.Text.Trim().Length == 0)
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieuEditForm f = new NguyenPhuLieuEditForm();
                        f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        f.NPLDetail = this.nplDangKy.NPLCollection[i.Position];
                        f.OpenType = OpenFormType.Edit;
                        f.NPLCollection = this.nplDangKy.NPLCollection;
                        f.NPLCollection.RemoveAt(i.Position);
                        f.nplDangKy = nplDangKy;
                        f.ShowDialog(this);

                        // Chưa lưu.
                        if (this.nplDangKy.ID == 0)
                        {
                            if (f.NPLDetail != null)
                            {
                                this.nplDangKy.NPLCollection.Insert(i.Position, f.NPLDetail);
                            }
                        }
                        else
                        {
                            this.nplDangKy.LoadNPLCollection();
                            dgList.DataSource = this.nplDangKy.NPLCollection;
                        }
                        try
                        {
                            dgList.Refetch();
                        }
                        catch
                        {
                            dgList.Refresh();
                        }
                    }
                    break;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSend":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        this.SendV3();
                    else
                        this.send();
                    break;
                case "cmdAddExcel":
                    this.AddNguyenPhuLieuFromExcel();
                    break;
                case "XacNhan":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        this.FeedBackV3();
                    else
                        this.LaySoTiepNhanDT();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;

            }
        }
        private void inPhieuTN()
        {
            if (this.nplDangKy.SoTiepNhan == 0) return;
            Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
            phieuTN.phieu = "NGUYÊN PHỤ LIỆU";
            phieuTN.soTN = this.nplDangKy.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.nplDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = nplDangKy.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void AddNguyenPhuLieuFromExcel()
        {
            NguyenPhuLieuReadExcelForm f = new NguyenPhuLieuReadExcelForm();
            f.NPLCollection = this.nplDangKy.NPLCollection;
            f.ShowDialog(this);
            dgList.DataSource = this.nplDangKy.NPLCollection;
            dgList.Refetch();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
            #region Code cũ
            //{
            //    //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            //    if (MLMessages("Bạn có muốn xóa nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            //    {
            //        GridEXSelectedItemCollection items = dgList.SelectedItems;
            //        foreach (GridEXSelectedItem i in items)
            //        {
            //            if (i.RowType == RowType.Record)
            //            {
            //                NguyenPhuLieu nplSelected = (NguyenPhuLieu)i.GetRow().DataRow;                                                        
            //                // Thực hiện xóa trong CSDL.
            //                if (nplSelected.ID > 0)
            //                {
            //                    nplSelected.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
            //                }
            //            }
            //        }                    
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //    }
            //}
            #endregion Code cũ

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void XoaNguyenPhuLieu(GridEXSelectedItemCollection items, NguyenPhuLieuCollection nguyenphulieus)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            List<NguyenPhuLieu> npls = new List<NguyenPhuLieu>();
            List<GridEXSelectedItem> itemRemove = new List<GridEXSelectedItem>();
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    NguyenPhuLieu npl = (NguyenPhuLieu)i.GetRow().DataRow;
                    npls.Add(npl);
                    msgWarning += string.Format("Mã ={0} [Xóa]\r\n", npl.Ma);
                }
            }
            try
            {
                msgWarning += "Bạn có đồng ý xóa không?";
                if (Globals.ShowMessage(msgWarning, true) != "Yes") return;

                foreach (NguyenPhuLieu item in npls)
                {
                    if (item.ID > 0 && item.CloneToDB(null))
                        item.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                }
                foreach (NguyenPhuLieu item in npls)
                {
                    nguyenphulieus.Remove(item);
                }
            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void Delete()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaNguyenPhuLieu(items, nplDangKy.NPLCollection);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            Delete();
            #region code cũ
            //NguyenPhuLieuCollection NPLCollectionTMP = new NguyenPhuLieuCollection();
            //if (MLMessages("Bạn có muốn xóa nguyên phụ liệu này không?", "", "MSG_DEL01", true) == "Yes")
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            NguyenPhuLieu nplSelected = (NguyenPhuLieu)i.GetRow().DataRow;
            //            // Thực hiện xóa trong CSDL.
            //            if (nplSelected.ID > 0)
            //            {
            //                nplSelected.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
            //                NPLCollectionTMP.Add(nplSelected);
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    return;
            //}
            //foreach (NguyenPhuLieu npl in NPLCollectionTMP)
            //{
            //    nplDangKy.NPLCollection.Remove(npl);
            //}
            //try { dgList.Refetch(); }
            //catch { dgList.Refresh(); }
            #endregion
        }
        #region Create by LANNT
        private void SendV3()
        {

            if (nplDangKy.ID == 0)
            {
                this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                return;
            }
            try
            {
                if (nplDangKy.NPLCollection.Count == 0)
                {
                    // ShowMessage("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }
                nplDangKy.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu npl = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_NPL(nplDangKy, false);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = nplDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(nplDangKy.MaHaiQuan),
                                     Identity = nplDangKy.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_NPL,
                                    Function = Company.KDT.SHARE.Components.DeclarationFunction.KHAI_BAO,
                                    Reference = nplDangKy.GUIDSTR,
                                }
                                ,
                                npl);
                nplDangKy.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoNguyenPhuLieu);
                    XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdAdd.Enabled = cmdSave.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    btnDelete.Enabled = false;
                    MsgSend sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
                    sendXML.master_id = nplDangKy.ID;
                    sendXML.func = 1;
                   // sendXML.msg = msgSend;
                    sendXML.InsertUpdate();
                    nplDangKy.TransgferDataToSXXK();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);

            }


            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.NguyenPhuLieuSendHandler(nplDangKy, ref msgInfor, e);
        }
        private void FeedBackV3()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
            sendXML.master_id = nplDangKy.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = nplDangKy.GUIDSTR;

                Company.KDT.SHARE.Components.SubjectBase subjectBase = new Company.KDT.SHARE.Components.SubjectBase()
                {
                    Issuer = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_NPL,
                    Reference = reference,
                    Function = Company.KDT.SHARE.Components.DeclarationFunction.HOI_TRANG_THAI,
                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_NPL,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new Company.KDT.SHARE.Components.NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = nplDangKy.MaDoanhNghiep
                                            },
                                              new Company.KDT.SHARE.Components.NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(nplDangKy.MaHaiQuan.Trim()),
                                                  Identity = nplDangKy.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    //if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    //{
                    //    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    //    {
                    //        isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                    //        ShowMessageTQDT(msgInfor, false);
                    //    }
                    //    count--;
                    //}
                    //else if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || nplDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    //{
                    //    ShowMessageTQDT(msgInfor, false);
                    //    isFeedBack = false;
                    //}
                    //else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    //else isFeedBack = false;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) SetCommand();
                }

            }
        }

        #endregion
    }
}
