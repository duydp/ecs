﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class SXXK_SanPham:DeclarationBase
    {
        /// <summary>
        /// Mặt hàng
        /// </summary>
        [XmlElement("Product")]
        public List<Product> Product { get; set; }
        /// <summary>
        /// Mặt hàng
        /// </summary>
        [XmlElement("IncomingGoodsItem")]
        public List<Product> IncomingGoodsItem { get; set; }

        [XmlElement("OutgoingGoodsItem")]
        public List<Product> OutgoingGoodsItem { get; set; }

        [XmlElement("CustomsGoodsItem")]
        public List<Product> CustomsGoodsItem { get; set; }
        [XmlElement("time")]
        public string time { get; set; }
        
    }
}
