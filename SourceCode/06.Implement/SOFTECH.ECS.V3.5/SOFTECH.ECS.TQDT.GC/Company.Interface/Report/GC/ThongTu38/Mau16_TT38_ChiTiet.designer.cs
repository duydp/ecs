﻿namespace Company.Interface.Report.SXXK
{
    partial class Mau16_TT38_ChiTiet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSoHopDong_D = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtTenMM_TB = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtMaMM_TB = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtDVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSoLuongTamNhap = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSoLuongTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSoLuong_ChuyenHD = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSo_NgayHD = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSoLuongConLai = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtGhiChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbNam = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbDiaChi = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbMaSo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbTenToChuc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbl_STT_G_H = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt_SoTaiKhoan_G_H = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl_TenHang_G_H = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbl_MaHang_G_H = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtGhiChu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtSoTaiKhoan_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt_TenMMTB_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtMaMM_TB_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt_DVT_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt_SoLuongTamNhap_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt_SoLuongTaiXuat_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSoLuongChuyenHD_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtSo_NgayHD_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt_SoLuongConLai_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtGhiChu_G_F = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txtHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtSTT,
            this.txtSoHopDong_D,
            this.txtTenMM_TB,
            this.txtMaMM_TB,
            this.txtDVT,
            this.txtSoLuongTamNhap,
            this.txtSoLuongTaiXuat,
            this.txtSoLuong_ChuyenHD,
            this.txtSo_NgayHD,
            this.txtSoLuongConLai,
            this.txtGhiChu});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // txtSTT
            // 
            this.txtSTT.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtSTT.Name = "txtSTT";
            this.txtSTT.StylePriority.UseFont = false;
            this.txtSTT.Weight = 0.13007106528839757;
            this.txtSTT.AfterPrint += new System.EventHandler(this.txtSTT_AfterPrint);
            this.txtSTT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtSTT_BeforePrint);
            // 
            // txtSoHopDong_D
            // 
            this.txtSoHopDong_D.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtSoHopDong_D.Name = "txtSoHopDong_D";
            this.txtSoHopDong_D.StylePriority.UseFont = false;
            this.txtSoHopDong_D.Weight = 0.16214374570053572;
            // 
            // txtTenMM_TB
            // 
            this.txtTenMM_TB.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtTenMM_TB.Name = "txtTenMM_TB";
            this.txtTenMM_TB.StylePriority.UseFont = false;
            this.txtTenMM_TB.StylePriority.UseTextAlignment = false;
            this.txtTenMM_TB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtTenMM_TB.Weight = 0.61494338860182729;
            // 
            // txtMaMM_TB
            // 
            this.txtMaMM_TB.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtMaMM_TB.Name = "txtMaMM_TB";
            this.txtMaMM_TB.StylePriority.UseFont = false;
            this.txtMaMM_TB.StylePriority.UseTextAlignment = false;
            this.txtMaMM_TB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtMaMM_TB.Weight = 0.30340841080618663;
            // 
            // txtDVT
            // 
            this.txtDVT.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtDVT.Name = "txtDVT";
            this.txtDVT.StylePriority.UseFont = false;
            this.txtDVT.Text = "CHIEC";
            this.txtDVT.Weight = 0.1980176443257185;
            this.txtDVT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtDVT_BeforePrint);
            // 
            // txtSoLuongTamNhap
            // 
            this.txtSoLuongTamNhap.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtSoLuongTamNhap.Name = "txtSoLuongTamNhap";
            this.txtSoLuongTamNhap.StylePriority.UseFont = false;
            this.txtSoLuongTamNhap.Text = "123456789123";
            this.txtSoLuongTamNhap.Weight = 0.27342826058766684;
            this.txtSoLuongTamNhap.AfterPrint += new System.EventHandler(this.txtSoLuongTamNhap_AfterPrint);
            this.txtSoLuongTamNhap.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtSoLuongTamNhap_BeforePrint);
            // 
            // txtSoLuongTaiXuat
            // 
            this.txtSoLuongTaiXuat.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtSoLuongTaiXuat.Name = "txtSoLuongTaiXuat";
            this.txtSoLuongTaiXuat.StylePriority.UseFont = false;
            this.txtSoLuongTaiXuat.Text = "123456789123";
            this.txtSoLuongTaiXuat.Weight = 0.23860675484731397;
            this.txtSoLuongTaiXuat.AfterPrint += new System.EventHandler(this.txtSoLuongTaiXuat_AfterPrint);
            this.txtSoLuongTaiXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtSoLuongTaiXuat_BeforePrint);
            // 
            // txtSoLuong_ChuyenHD
            // 
            this.txtSoLuong_ChuyenHD.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtSoLuong_ChuyenHD.Name = "txtSoLuong_ChuyenHD";
            this.txtSoLuong_ChuyenHD.StylePriority.UseFont = false;
            this.txtSoLuong_ChuyenHD.Weight = 0.35096030596561822;
            this.txtSoLuong_ChuyenHD.AfterPrint += new System.EventHandler(this.txtSoLuong_ChuyenHD_AfterPrint);
            this.txtSoLuong_ChuyenHD.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtSoLuong_ChuyenHD_BeforePrint);
            // 
            // txtSo_NgayHD
            // 
            this.txtSo_NgayHD.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtSo_NgayHD.Name = "txtSo_NgayHD";
            this.txtSo_NgayHD.StylePriority.UseFont = false;
            this.txtSo_NgayHD.Weight = 0.47258696501160835;
            // 
            // txtSoLuongConLai
            // 
            this.txtSoLuongConLai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtSoLuongConLai.Name = "txtSoLuongConLai";
            this.txtSoLuongConLai.StylePriority.UseFont = false;
            this.txtSoLuongConLai.Weight = 0.25506016733963288;
            this.txtSoLuongConLai.AfterPrint += new System.EventHandler(this.txtSoLuongConLai_AfterPrint);
            this.txtSoLuongConLai.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtSoLuongConLai_BeforePrint);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.StylePriority.UseFont = false;
            this.txtGhiChu.Weight = 0.31511309926913939;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 10F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel29,
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel23,
            this.xrLabel25,
            this.xrLabel24,
            this.xrLabel21,
            this.xrLabel20,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel18,
            this.xrLabel17,
            this.lbNam,
            this.xrLabel14,
            this.xrLabel9,
            this.lbDiaChi,
            this.xrLabel13,
            this.xrLabel12,
            this.lbMaSo,
            this.xrLabel10,
            this.lbTenToChuc,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5});
            this.ReportHeader.HeightF = 294.2612F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseBorders = false;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(98.65869F, 250.4277F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(207.6195F, 43.83336F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "Tên máy móc, thiết bị";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(408.7162F, 199.4697F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(68.30237F, 94.79141F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Đơn vị tính";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(477.0186F, 199.4697F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(90.86893F, 94.79117F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Số lượng tạm nhập";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(567.8876F, 199.4697F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(80.5592F, 94.79141F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Số lượng tái xuất";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(648.4468F, 250.4278F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(118.4926F, 43.83334F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Số lượng";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(766.9396F, 250.4277F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(159.5563F, 43.83336F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Số, ngày hợp đồng GC";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(306.2782F, 250.4277F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(102.4378F, 43.83311F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Mã thiết bị";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(926.496F, 199.4697F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(86.11438F, 94.79141F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Số lượng máy móc, thiết bị còn lại chưa tái xuất";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(1012.61F, 199.4692F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(106.3888F, 94.79169F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Ghi chú";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(43.91509F, 199.4695F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(54.7436F, 94.79164F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Số hợp đồng";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 199.4695F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(43.91508F, 94.79164F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "STT";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(648.4468F, 199.4692F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(278.0491F, 50.95833F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Số lượng máy móc, thiết bị chuyển sang HĐGC khác trong khi thực hiện HĐGC";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(98.65871F, 199.4697F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(310.0573F, 50.95828F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Tên máy móc, thiết bị tạm nhập";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbNam
            // 
            this.lbNam.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot;
            this.lbNam.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lbNam.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lbNam.LocationFloat = new DevExpress.Utils.PointFloat(555.3875F, 164.1575F);
            this.lbNam.Name = "lbNam";
            this.lbNam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbNam.SizeF = new System.Drawing.SizeF(98.48871F, 23.00002F);
            this.lbNam.StylePriority.UseBorderDashStyle = false;
            this.lbNam.StylePriority.UseBorders = false;
            this.lbNam.StylePriority.UseFont = false;
            this.lbNam.StylePriority.UseTextAlignment = false;
            this.lbNam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(501.4785F, 164.1575F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(53.909F, 23.00002F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Năm:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 137.1575F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(1109F, 23F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "BẢNG BÁO CÁO QUYẾT TOÁN SỬ DỤNG MÁY MÓC, THIẾT BỊ THEO TỪNG HỢP ĐỒNG GIA CÔNG";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbDiaChi
            // 
            this.lbDiaChi.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot;
            this.lbDiaChi.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbDiaChi.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lbDiaChi.LocationFloat = new DevExpress.Utils.PointFloat(98.65869F, 84.27054F);
            this.lbDiaChi.Name = "lbDiaChi";
            this.lbDiaChi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbDiaChi.SizeF = new System.Drawing.SizeF(523.0219F, 23.00002F);
            this.lbDiaChi.StylePriority.UseBorderDashStyle = false;
            this.lbDiaChi.StylePriority.UseBorders = false;
            this.lbDiaChi.StylePriority.UseFont = false;
            this.lbDiaChi.StylePriority.UseTextAlignment = false;
            this.lbDiaChi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(788.5498F, 77.27052F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(152.9281F, 10.15753F);
            this.xrLabel13.StylePriority.UseBorderDashStyle = false;
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Italic);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(680.4841F, 61.27052F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(357.1233F, 23F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Độc lập - Tự do - Hạnh phúc";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbMaSo
            // 
            this.lbMaSo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbMaSo.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lbMaSo.LocationFloat = new DevExpress.Utils.PointFloat(98.65866F, 61.27055F);
            this.lbMaSo.Name = "lbMaSo";
            this.lbMaSo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbMaSo.SizeF = new System.Drawing.SizeF(523.0217F, 23F);
            this.lbMaSo.StylePriority.UseBorders = false;
            this.lbMaSo.StylePriority.UseFont = false;
            this.lbMaSo.StylePriority.UseTextAlignment = false;
            this.lbMaSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(680.4841F, 38.27054F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(357.1233F, 23F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbTenToChuc
            // 
            this.lbTenToChuc.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot;
            this.lbTenToChuc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbTenToChuc.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lbTenToChuc.LocationFloat = new DevExpress.Utils.PointFloat(188.8075F, 38.27052F);
            this.lbTenToChuc.Name = "lbTenToChuc";
            this.lbTenToChuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbTenToChuc.SizeF = new System.Drawing.SizeF(432.873F, 23F);
            this.lbTenToChuc.StylePriority.UseBorderDashStyle = false;
            this.lbTenToChuc.StylePriority.UseBorders = false;
            this.lbTenToChuc.StylePriority.UseFont = false;
            this.lbTenToChuc.StylePriority.UseTextAlignment = false;
            this.lbTenToChuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(24.06959F, 84.27053F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(74.58908F, 23.00001F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Địa chỉ:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(24.06959F, 61.27052F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(74.58907F, 23F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Mã số:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(24.0696F, 38.27052F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(164.7379F, 23F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tên tổ chức/cá nhân:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(788.5498F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(307.1804F, 23F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Mẫu số 16/BCQT-MMTB/GSQL";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.ReportFooter.HeightF = 169.8529F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(609.3766F, 63.70837F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(499.6234F, 23F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "(ký, ghi rõ họ tên, đóng dấu)";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(609.3766F, 40.70842F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(499.6234F, 23.00001F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "ĐẠI DIỆN THEO PHÁP LUẬT CỦA TỔ CHỨC, CÁ NHÂN";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 40.70842F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(218.7566F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "(Ký, ghi rõ họ tên)";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(609.3766F, 17.7084F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(499.6234F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Ngày…..tháng….năm….";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 17.7084F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(218.7566F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "NGƯỜI LẬP";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Format = "Trang {0} / {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(926.496F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(84.37494F, 23F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.PageHeader.HeightF = 25F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell7,
            this.xrTableCell6,
            this.xrTableCell5,
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell4,
            this.xrTableCell3,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "(1)";
            this.xrTableCell8.Weight = 0.13007104265082395;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "(2)";
            this.xrTableCell7.Weight = 0.16214372222642023;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "(3)";
            this.xrTableCell6.Weight = 0.61494346119491394;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "(4)";
            this.xrTableCell5.Weight = 0.30340826080055505;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "(5)";
            this.xrTableCell1.Weight = 0.198017800233718;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "(6)";
            this.xrTableCell2.Weight = 0.27342860283111409;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "(7)";
            this.xrTableCell4.Weight = 0.2386063863671174;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "(8)";
            this.xrTableCell3.Weight = 0.350960873822754;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "(9)";
            this.xrTableCell9.Weight = 0.47258610734496509;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "(10)";
            this.xrTableCell10.Weight = 0.25506014246003311;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "(11)";
            this.xrTableCell11.Weight = 0.31511307122077092;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1,
            this.GroupFooter1,
            this.GroupHeader3});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail1.HeightF = 25F;
            this.Detail1.Name = "Detail1";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.GroupHeader1.HeightF = 25F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(3.973643E-05F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbl_STT_G_H,
            this.txt_SoTaiKhoan_G_H,
            this.lbl_TenHang_G_H,
            this.lbl_MaHang_G_H,
            this.xrTableCell18,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell29,
            this.txtGhiChu1});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1;
            // 
            // lbl_STT_G_H
            // 
            this.lbl_STT_G_H.Name = "lbl_STT_G_H";
            this.lbl_STT_G_H.Weight = 0.13007106528839757;
            // 
            // txt_SoTaiKhoan_G_H
            // 
            this.txt_SoTaiKhoan_G_H.Name = "txt_SoTaiKhoan_G_H";
            this.txt_SoTaiKhoan_G_H.Weight = 0.16214374570053572;
            // 
            // lbl_TenHang_G_H
            // 
            this.lbl_TenHang_G_H.Name = "lbl_TenHang_G_H";
            this.lbl_TenHang_G_H.Weight = 0.61494334340717849;
            // 
            // lbl_MaHang_G_H
            // 
            this.lbl_MaHang_G_H.Name = "lbl_MaHang_G_H";
            this.lbl_MaHang_G_H.Weight = 0.30340850119548424;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 0.19801755393642087;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.273428534374861;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Weight = 0.23860648106011984;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.35096079836159383;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Weight = 0.47258651781028149;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.25505998656103768;
            // 
            // txtGhiChu1
            // 
            this.txtGhiChu1.Name = "txtGhiChu1";
            this.txtGhiChu1.Weight = 0.31511328004773459;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupFooter1.HeightF = 25F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtSoTaiKhoan_G_F,
            this.txt_TenMMTB_G_F,
            this.txtMaMM_TB_G_F,
            this.txt_DVT_G_F,
            this.txt_SoLuongTamNhap_G_F,
            this.txt_SoLuongTaiXuat_G_F,
            this.txtSoLuongChuyenHD_G_F,
            this.txtSo_NgayHD_G_F,
            this.txt_SoLuongConLai_G_F,
            this.txtGhiChu_G_F});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // txtSoTaiKhoan_G_F
            // 
            this.txtSoTaiKhoan_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoTaiKhoan_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtSoTaiKhoan_G_F.Name = "txtSoTaiKhoan_G_F";
            this.txtSoTaiKhoan_G_F.StylePriority.UseBackColor = false;
            this.txtSoTaiKhoan_G_F.StylePriority.UseFont = false;
            this.txtSoTaiKhoan_G_F.StylePriority.UseTextAlignment = false;
            this.txtSoTaiKhoan_G_F.Text = "Tổng cộng:";
            this.txtSoTaiKhoan_G_F.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.txtSoTaiKhoan_G_F.Weight = 0.29221481098893332;
            // 
            // txt_TenMMTB_G_F
            // 
            this.txt_TenMMTB_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txt_TenMMTB_G_F.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.txt_TenMMTB_G_F.Name = "txt_TenMMTB_G_F";
            this.txt_TenMMTB_G_F.StylePriority.UseBackColor = false;
            this.txt_TenMMTB_G_F.StylePriority.UseFont = false;
            this.txt_TenMMTB_G_F.StylePriority.UseTextAlignment = false;
            this.txt_TenMMTB_G_F.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txt_TenMMTB_G_F.Weight = 0.61494338860182729;
            this.txt_TenMMTB_G_F.AfterPrint += new System.EventHandler(this.txt_TenMMTB_G_F_AfterPrint);
            this.txt_TenMMTB_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txt_TenMMTB_G_F_BeforePrint);
            // 
            // txtMaMM_TB_G_F
            // 
            this.txtMaMM_TB_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtMaMM_TB_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtMaMM_TB_G_F.Name = "txtMaMM_TB_G_F";
            this.txtMaMM_TB_G_F.StylePriority.UseBackColor = false;
            this.txtMaMM_TB_G_F.StylePriority.UseFont = false;
            this.txtMaMM_TB_G_F.StylePriority.UseTextAlignment = false;
            this.txtMaMM_TB_G_F.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.txtMaMM_TB_G_F.Weight = 0.30340841080618663;
            this.txtMaMM_TB_G_F.AfterPrint += new System.EventHandler(this.txtMaMM_TB_G_F_AfterPrint);
            this.txtMaMM_TB_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtMaMM_TB_G_F_BeforePrint);
            // 
            // txt_DVT_G_F
            // 
            this.txt_DVT_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txt_DVT_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txt_DVT_G_F.Name = "txt_DVT_G_F";
            this.txt_DVT_G_F.StylePriority.UseBackColor = false;
            this.txt_DVT_G_F.StylePriority.UseFont = false;
            this.txt_DVT_G_F.Weight = 0.1980176443257185;
            this.txt_DVT_G_F.AfterPrint += new System.EventHandler(this.txt_DVT_G_F_AfterPrint);
            this.txt_DVT_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txt_DVT_G_F_BeforePrint);
            // 
            // txt_SoLuongTamNhap_G_F
            // 
            this.txt_SoLuongTamNhap_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txt_SoLuongTamNhap_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txt_SoLuongTamNhap_G_F.Name = "txt_SoLuongTamNhap_G_F";
            this.txt_SoLuongTamNhap_G_F.StylePriority.UseBackColor = false;
            this.txt_SoLuongTamNhap_G_F.StylePriority.UseFont = false;
            this.txt_SoLuongTamNhap_G_F.Weight = 0.27342880554275378;
            this.txt_SoLuongTamNhap_G_F.AfterPrint += new System.EventHandler(this.txt_SoLuongTamNhap_G_F_AfterPrint);
            this.txt_SoLuongTamNhap_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txt_SoLuongTamNhap_G_F_BeforePrint);
            // 
            // txt_SoLuongTaiXuat_G_F
            // 
            this.txt_SoLuongTaiXuat_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txt_SoLuongTaiXuat_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txt_SoLuongTaiXuat_G_F.Name = "txt_SoLuongTaiXuat_G_F";
            this.txt_SoLuongTaiXuat_G_F.StylePriority.UseBackColor = false;
            this.txt_SoLuongTaiXuat_G_F.StylePriority.UseFont = false;
            this.txt_SoLuongTaiXuat_G_F.Weight = 0.23860620989222703;
            this.txt_SoLuongTaiXuat_G_F.AfterPrint += new System.EventHandler(this.txt_SoLuongTaiXuat_G_F_AfterPrint);
            this.txt_SoLuongTaiXuat_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txt_SoLuongTaiXuat_G_F_BeforePrint);
            // 
            // txtSoLuongChuyenHD_G_F
            // 
            this.txtSoLuongChuyenHD_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoLuongChuyenHD_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtSoLuongChuyenHD_G_F.Name = "txtSoLuongChuyenHD_G_F";
            this.txtSoLuongChuyenHD_G_F.StylePriority.UseBackColor = false;
            this.txtSoLuongChuyenHD_G_F.StylePriority.UseFont = false;
            this.txtSoLuongChuyenHD_G_F.Weight = 0.35096057238834982;
            this.txtSoLuongChuyenHD_G_F.AfterPrint += new System.EventHandler(this.txtSoLuong_G_F_AfterPrint);
            this.txtSoLuongChuyenHD_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtSoLuong_G_F_BeforePrint);
            // 
            // txtSo_NgayHD_G_F
            // 
            this.txtSo_NgayHD_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtSo_NgayHD_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtSo_NgayHD_G_F.Name = "txtSo_NgayHD_G_F";
            this.txtSo_NgayHD_G_F.StylePriority.UseBackColor = false;
            this.txtSo_NgayHD_G_F.StylePriority.UseFont = false;
            this.txtSo_NgayHD_G_F.Weight = 0.4725868793674719;
            // 
            // txt_SoLuongConLai_G_F
            // 
            this.txt_SoLuongConLai_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txt_SoLuongConLai_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txt_SoLuongConLai_G_F.Name = "txt_SoLuongConLai_G_F";
            this.txt_SoLuongConLai_G_F.StylePriority.UseBackColor = false;
            this.txt_SoLuongConLai_G_F.StylePriority.UseFont = false;
            this.txt_SoLuongConLai_G_F.Weight = 0.24990797737637108;
            this.txt_SoLuongConLai_G_F.AfterPrint += new System.EventHandler(this.txt_SoLuongConLai_G_F_AfterPrint);
            this.txt_SoLuongConLai_G_F.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txt_SoLuongConLai_G_F_BeforePrint);
            // 
            // txtGhiChu_G_F
            // 
            this.txtGhiChu_G_F.BackColor = System.Drawing.SystemColors.Info;
            this.txtGhiChu_G_F.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.txtGhiChu_G_F.Name = "txtGhiChu_G_F";
            this.txtGhiChu_G_F.StylePriority.UseBackColor = false;
            this.txtGhiChu_G_F.StylePriority.UseFont = false;
            this.txtGhiChu_G_F.Weight = 0.32026510845380607;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupHeader3.HeightF = 25F;
            this.GroupHeader3.Level = 1;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.LightCyan;
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1119F, 25F);
            this.xrTable3.StylePriority.UseBackColor = false;
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txtHopDong});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // txtHopDong
            // 
            this.txtHopDong.Font = new System.Drawing.Font("Times New Roman", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.txtHopDong.Name = "txtHopDong";
            this.txtHopDong.StylePriority.UseFont = false;
            this.txtHopDong.StylePriority.UseTextAlignment = false;
            this.txtHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txtHopDong.Weight = 3.3143398077436457;
            this.txtHopDong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtHopDong_BeforePrint);
            // 
            // Mau16_TT38_ChiTiet
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter,
            this.PageFooter,
            this.PageHeader,
            this.DetailReport});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(25, 25, 10, 0);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "14.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel lbTenToChuc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel lbDiaChi;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lbMaSo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lbNam;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell txtSTT;
        private DevExpress.XtraReports.UI.XRTableCell txtSoHopDong_D;
        private DevExpress.XtraReports.UI.XRTableCell txtTenMM_TB;
        private DevExpress.XtraReports.UI.XRTableCell txtMaMM_TB;
        private DevExpress.XtraReports.UI.XRTableCell txtDVT;
        private DevExpress.XtraReports.UI.XRTableCell txtSoLuongTamNhap;
        private DevExpress.XtraReports.UI.XRTableCell txtSoLuongTaiXuat;
        private DevExpress.XtraReports.UI.XRTableCell txtSoLuong_ChuyenHD;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell txtSo_NgayHD;
        private DevExpress.XtraReports.UI.XRTableCell txtSoLuongConLai;
        private DevExpress.XtraReports.UI.XRTableCell txtGhiChu;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lbl_STT_G_H;
        private DevExpress.XtraReports.UI.XRTableCell txt_SoTaiKhoan_G_H;
        private DevExpress.XtraReports.UI.XRTableCell lbl_TenHang_G_H;
        private DevExpress.XtraReports.UI.XRTableCell lbl_MaHang_G_H;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell txtGhiChu1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell txtSoTaiKhoan_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txt_TenMMTB_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txtMaMM_TB_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txt_DVT_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txt_SoLuongTamNhap_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txt_SoLuongTaiXuat_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txtSoLuongChuyenHD_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txtSo_NgayHD_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txt_SoLuongConLai_G_F;
        private DevExpress.XtraReports.UI.XRTableCell txtGhiChu_G_F;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell txtHopDong;
    }
}
