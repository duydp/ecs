﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class KD_BoSungCtuDangAnh:DeclarationBase
    {
       [XmlElement("DeclarationDocument")]
       public DeclarationBase DeclarationDocument { get; set; }
       [XmlArray("AttachDocuments")]
       [XmlArrayItem("AttachDocumentItem")]
       public List<AttachDocumentItem> AttachDocuments { get; set; }

    }
}
