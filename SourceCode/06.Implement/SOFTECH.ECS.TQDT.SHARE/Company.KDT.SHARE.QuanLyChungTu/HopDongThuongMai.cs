﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class HopDongThuongMai
    {
        public List<HopDongThuongMaiDetail> ListHangMDOfHopDong = new List<HopDongThuongMaiDetail>();

        public void LoadListHangDMOfHopDong()
        {
            ListHangMDOfHopDong = HopDongThuongMaiDetail.SelectCollectionBy_HopDongTM_ID(this.ID);
        }
        public DataTable ConvertListToDataSet(DataTable dsHMD)
        {
            DataTable tmp = dsHMD.Copy();
            tmp.Rows.Clear();
            tmp.Columns.Add("GhiChu");
            dsHMD.Columns.Add("GhiChu");
            dsHMD.Columns.Add("HMD_ID");
            tmp.Columns["ID"].ColumnName = "HMD_ID";
            tmp.Columns["HMD_ID"].Caption = "HMD_ID";
            tmp.Columns.Add("ID");
            foreach (HopDongThuongMaiDetail item in ListHangMDOfHopDong)
            {
                DataRow[] row = dsHMD.Select("ID=" + item.HMD_ID);
                if (row != null && row.Length != 0)
                {
                    row[0]["GhiChu"] = item.GhiChu;
                    row[0]["HMD_ID"] = row[0]["ID"];
                    row[0]["ID"] = item.ID.ToString();

                    row[0]["SoThuTuHang"] = item.SoThuTuHang;
                    row[0]["MaHS"] = item.MaHS;
                    row[0]["MaPhu"] = item.MaPhu;
                    row[0]["TenHang"] = item.TenHang;
                    row[0]["NuocXX_ID"] = item.NuocXX_ID;
                    row[0]["DVT_ID"] = item.DVT_ID;
                    row[0]["SoLuong"] = item.SoLuong;
                    row[0]["DonGiaKB"] = item.DonGiaKB;
                    row[0]["TriGiaKB"] = item.TriGiaKB;

                    tmp.ImportRow(row[0]);
                }
            }
            return tmp;

        }

        public static HopDongThuongMai Load(string maDoanhNghiep, string soHopDongTM, DateTime ngayHopDongTM, DateTime thoiHanThanhToan, long idTKMD, SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_LoadBy]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoHopDongTM", SqlDbType.VarChar, soHopDongTM);
            db.AddInParameter(dbCommand, "@NgayHopDongTM", SqlDbType.DateTime, ngayHopDongTM);
            db.AddInParameter(dbCommand, "@ThoiHanThanhToan", SqlDbType.DateTime, thoiHanThanhToan);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, idTKMD);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);

            HopDongThuongMai entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new HopDongThuongMai();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongTM"))) entity.SoHopDongTM = reader.GetString(reader.GetOrdinal("SoHopDongTM"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDongTM"))) entity.NgayHopDongTM = reader.GetDateTime(reader.GetOrdinal("NgayHopDongTM"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanThanhToan"))) entity.ThoiHanThanhToan = reader.GetDateTime(reader.GetOrdinal("ThoiHanThanhToan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViMua"))) entity.MaDonViMua = reader.GetString(reader.GetOrdinal("MaDonViMua"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViMua"))) entity.TenDonViMua = reader.GetString(reader.GetOrdinal("TenDonViMua"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViBan"))) entity.MaDonViBan = reader.GetString(reader.GetOrdinal("MaDonViBan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViBan"))) entity.TenDonViBan = reader.GetString(reader.GetOrdinal("TenDonViBan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGia"))) entity.TongTriGia = reader.GetDecimal(reader.GetOrdinal("TongTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoHopDongTM", SqlDbType.VarChar, SoHopDongTM);
            db.AddInParameter(dbCommand, "@NgayHopDongTM", SqlDbType.DateTime, NgayHopDongTM.Year <= 1753 ? DBNull.Value : (object)NgayHopDongTM);
            db.AddInParameter(dbCommand, "@ThoiHanThanhToan", SqlDbType.DateTime, ThoiHanThanhToan.Year <= 1753 ? DBNull.Value : (object)ThoiHanThanhToan);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.VarChar, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
            db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
            db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
            db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
            db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Money, TongTriGia);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMai_Update]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoHopDongTM", SqlDbType.VarChar, SoHopDongTM);
            db.AddInParameter(dbCommand, "@NgayHopDongTM", SqlDbType.DateTime, NgayHopDongTM.Year == 1753 ? DBNull.Value : (object)NgayHopDongTM);
            db.AddInParameter(dbCommand, "@ThoiHanThanhToan", SqlDbType.DateTime, ThoiHanThanhToan.Year == 1753 ? DBNull.Value : (object)ThoiHanThanhToan);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.VarChar, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
            db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
            db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
            db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
            db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Money, TongTriGia);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);
                        foreach (HopDongThuongMaiDetail item in ListHangMDOfHopDong)
                        {
                            item.HopDongTM_ID = this.ID;
                            if (id == 0)
                                item.ID = 0;

                            if (item.ID == 0)
                                item.Insert(transaction);
                            else
                                item.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this.ID = id;
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public int Delete()
        {
            bool ret = true;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        HopDongThuongMaiDetail.DeleteBy_HopDongTM_ID(this.ID);
                        this.Delete(null);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return 1;
        }

        public static XmlNode ConvertCollectionHopDongToXML_TKX(XmlDocument doc, List<HopDongThuongMai> HopDongThuongMaiCollection, long TKMD_ID, List<HangMauDich> listHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            if (HopDongThuongMaiCollection == null || HopDongThuongMaiCollection.Count == 0)
            {
                HopDongThuongMaiCollection = HopDongThuongMai.SelectCollectionBy_TKMD_ID(TKMD_ID);
            }
            XmlElement CHUNG_TU_HOPDONG = doc.CreateElement("CHUNG_TU_HOPDONG");
            foreach (HopDongThuongMai hopdongTM in HopDongThuongMaiCollection)
            {
                XmlElement hopdongTMItem = doc.CreateElement("CHUNG_TU_HOPDONG.ITEM");
                CHUNG_TU_HOPDONG.AppendChild(hopdongTMItem);

                XmlAttribute SO_CT = doc.CreateAttribute("SO_CT");
                SO_CT.Value = FontConverter.Unicode2TCVN(hopdongTM.SoHopDongTM.Trim());
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = doc.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = hopdongTM.NgayHopDongTM.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute THOI_HAN_TT = doc.CreateAttribute("THOI_HAN_TT");
                THOI_HAN_TT.Value = hopdongTM.ThoiHanThanhToan.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(THOI_HAN_TT);

                XmlAttribute MA_GH = doc.CreateAttribute("MA_GH");
                MA_GH.Value = hopdongTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute DIADIEM_GH = doc.CreateAttribute("DIADIEM_GH");
                DIADIEM_GH.Value = FontConverter.Unicode2TCVN(hopdongTM.DiaDiemGiaoHang);
                hopdongTMItem.Attributes.Append(DIADIEM_GH);

                XmlAttribute MA_PTTT = doc.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = hopdongTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = doc.CreateAttribute("MA_NT");
                MA_NT.Value = hopdongTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute TONGTRIGIA = doc.CreateAttribute("TONGTRIGIA");
                TONGTRIGIA.Value = BaseClass.Round(hopdongTM.TongTriGia, 9);
                hopdongTMItem.Attributes.Append(TONGTRIGIA);

                XmlAttribute MA_DV_DT = doc.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = hopdongTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = doc.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = FontConverter.Unicode2TCVN(hopdongTM.TenDonViMua.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute MA_DV = doc.CreateAttribute("MA_DV");
                MA_DV.Value = hopdongTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = doc.CreateAttribute("TEN_DV");
                TEN_DV.Value = FontConverter.Unicode2TCVN(hopdongTM.TenDonViBan.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = FontConverter.Unicode2TCVN(hopdongTM.ThongTinKhac.Trim());
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (hopdongTM.ListHangMDOfHopDong == null || hopdongTM.ListHangMDOfHopDong.Count == 0)
                {
                    hopdongTM.LoadListHangDMOfHopDong();
                }

                foreach (HopDongThuongMaiDetail hangHopDong in hopdongTM.ListHangMDOfHopDong)
                {
                    HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hangHopDong.HMD_ID, listHang);

                    if (hmd == null) continue;

                    //Cap nhat thong tin hang thay doi cua rieng giay phep.
                    if (hmd != null)
                    {
                        hmd.MaHS = hangHopDong.MaHS;
                        hmd.MaPhu = hangHopDong.MaPhu;
                        hmd.TenHang = hangHopDong.TenHang;
                        hmd.NuocXX_ID = hangHopDong.NuocXX_ID;
                        hmd.DVT_ID = hangHopDong.DVT_ID;
                        hmd.SoLuong = hangHopDong.SoLuong;
                        hmd.DonGiaKB = hangHopDong.DonGiaKB;
                        hmd.TriGiaKB = hangHopDong.TriGiaKB;
                    }

                    XmlElement hangItem = doc.CreateElement("CHUNG_TU_HOPDONG.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = doc.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = doc.CreateAttribute("TEN_HANG");
                    // KhanhHN upadte 01/06/2012. Config mã font theo hải quan
                    if (Globals.FontName == "TCVN3")
                        TEN_HANG.Value = FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    else
                        TEN_HANG.Value = hmd.TenHang.Trim();
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = doc.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute MA_DVT = doc.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = doc.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = doc.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = doc.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GHI_CHUhang = doc.CreateAttribute("GHI_CHU");
                    GHI_CHUhang.Value = FontConverter.Unicode2TCVN(hangHopDong.GhiChu != null ? hangHopDong.GhiChu.Trim() : "");
                    hangItem.Attributes.Append(GHI_CHUhang);

                    XmlAttribute NUOC_XXHang = doc.CreateAttribute("NUOC_XX");
                    NUOC_XXHang.Value = hmd.NuocXX_ID;
                    hangItem.Attributes.Append(NUOC_XXHang);
                }
            }
            if (CHUNG_TU_HOPDONG.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_HOPDONG;
        }

        public static XmlNode ConvertCollectionHopDongToXML_TKN(XmlDocument doc, List<HopDongThuongMai> HopDongThuongMaiCollection, long TKMD_ID, List<HangMauDich> listHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            if (HopDongThuongMaiCollection == null || HopDongThuongMaiCollection.Count == 0)
            {
                HopDongThuongMaiCollection = HopDongThuongMai.SelectCollectionBy_TKMD_ID(TKMD_ID);
            }
            XmlElement CHUNG_TU_HOPDONG = doc.CreateElement("CHUNG_TU_HOPDONG");
            foreach (HopDongThuongMai hopdongTM in HopDongThuongMaiCollection)
            {
                XmlElement hopdongTMItem = doc.CreateElement("CHUNG_TU_HOPDONG.ITEM");
                CHUNG_TU_HOPDONG.AppendChild(hopdongTMItem);

                XmlAttribute SO_CT = doc.CreateAttribute("SO_CT");
                SO_CT.Value = hopdongTM.SoHopDongTM.Trim();
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = doc.CreateAttribute("NGAY_CT");
                //NGAY_CT.Value = hopdongTM.NgayHopDongTM.ToString("yyyy-MM-dd");
                NGAY_CT.Value = hopdongTM.NgayHopDongTM.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute THOI_HAN_TT = doc.CreateAttribute("THOI_HAN_TT");
                //THOI_HAN_TT.Value = hopdongTM.ThoiHanThanhToan.ToString("yyyy-MM-dd");
                THOI_HAN_TT.Value = hopdongTM.ThoiHanThanhToan.ToString("yyyy-MM-dd");
                hopdongTMItem.Attributes.Append(THOI_HAN_TT);

                XmlAttribute MA_GH = doc.CreateAttribute("MA_GH");
                MA_GH.Value = hopdongTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute DIADIEM_GH = doc.CreateAttribute("DIADIEM_GH");
                DIADIEM_GH.Value = hopdongTM.DiaDiemGiaoHang;
                hopdongTMItem.Attributes.Append(DIADIEM_GH);

                XmlAttribute MA_PTTT = doc.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = hopdongTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = doc.CreateAttribute("MA_NT");
                MA_NT.Value = hopdongTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute TONGTRIGIA = doc.CreateAttribute("TONGTRIGIA");
                TONGTRIGIA.Value = BaseClass.Round(hopdongTM.TongTriGia, 9);
                hopdongTMItem.Attributes.Append(TONGTRIGIA);

                XmlAttribute MA_DV = doc.CreateAttribute("MA_DV");
                MA_DV.Value = hopdongTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = doc.CreateAttribute("TEN_DV");
                TEN_DV.Value = hopdongTM.TenDonViMua.Trim();
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute MA_DV_DT = doc.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = hopdongTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = doc.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = hopdongTM.TenDonViBan.Trim();
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = hopdongTM.ThongTinKhac.Trim();
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (hopdongTM.ListHangMDOfHopDong == null || hopdongTM.ListHangMDOfHopDong.Count == 0)
                {
                    hopdongTM.LoadListHangDMOfHopDong();
                }

                foreach (HopDongThuongMaiDetail hangHopDong in hopdongTM.ListHangMDOfHopDong)
                {
                    HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hangHopDong.HMD_ID, listHang);

                    if (hmd == null) continue;

                    //Cap nhat thong tin hang thay doi cua rieng giay phep.
                    if (hmd != null)
                    {
                        hmd.MaHS = hangHopDong.MaHS;
                        hmd.MaPhu = hangHopDong.MaPhu;
                        hmd.TenHang = hangHopDong.TenHang;
                        hmd.NuocXX_ID = hangHopDong.NuocXX_ID;
                        hmd.DVT_ID = hangHopDong.DVT_ID;
                        hmd.SoLuong = hangHopDong.SoLuong;
                        hmd.DonGiaKB = hangHopDong.DonGiaKB;
                        hmd.TriGiaKB = hangHopDong.TriGiaKB;
                    }

                    XmlElement hangItem = doc.CreateElement("CHUNG_TU_HOPDONG.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = doc.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = doc.CreateAttribute("TEN_HANG");
                    //Hungtq updated 25/05/2012. Chuyển mã font từ Unicde -> TCVN3.
                    //TEN_HANG.Value = hmd.TenHang.Trim();
                    // KhanhHN upadte 01/06/2012. Config mã font theo hải quan
                    if (Globals.FontName == "TCVN3")
                        TEN_HANG.Value = FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    else
                        TEN_HANG.Value = hmd.TenHang.Trim();
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = doc.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute MA_DVT = doc.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = doc.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = doc.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = doc.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GHI_CHUhang = doc.CreateAttribute("GHI_CHU");
                    GHI_CHUhang.Value = hangHopDong.GhiChu != null ? hangHopDong.GhiChu.Trim() : "";
                    hangItem.Attributes.Append(GHI_CHUhang);

                    XmlAttribute NUOC_XXHang = doc.CreateAttribute("NUOC_XX");
                    NUOC_XXHang.Value = hmd.NuocXX_ID;
                    hangItem.Attributes.Append(NUOC_XXHang);
                }
            }
            if (CHUNG_TU_HOPDONG.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_HOPDONG;
        }

        public string send(string password, string maHQ, long soTK, string maLH, int namDK, long TKMD_ID, List<HangMauDich> listHang)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.ChildNodes[0].Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.ChildNodes[0].Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<HopDongThuongMai> listHoaDonThuongMai = new List<HopDongThuongMai>();
            listHoaDonThuongMai.Add(this);
            nodeDulieu.AppendChild(HopDongThuongMai.ConvertCollectionHopDongToXML_TKN(doc, listHoaDonThuongMai, TKMD_ID, listHang));


            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public string LayPhanHoi(string password, string maHQ)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\LayPhanHoiDaDuyet.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;




            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            nodeDulieu.Attributes[0].Value = this.GuidStr;

            KDTService kdt = WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {

                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, password);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }

                docNPL.LoadXml(kq);
                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                    }
                    else
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            return "";
        }

        public bool WSKhaiBaoBoSungHopDong(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, List<HopDongThuongMai> list, MessageTypes messageType, MessageFunctions messageFunction, List<HangMauDich> listHang)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];

            nodeReference.InnerText = this.GuidStr;
            // GuidStr = nodeReference.InnerText = System.Guid.NewGuid().ToString();
            this.Update();

            //LaNT đã thêm GuidStr từ method Save
            //if (GuidStr == "")
            //{
            //    GuidStr = (System.Guid.NewGuid().ToString()); ;
            //    nodeReference.InnerText = this.GuidStr;
            //    this.Update();
            //}

            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<HopDongThuongMai> listHopDongThuongMai = new List<HopDongThuongMai>();
            listHopDongThuongMai.Add(this);
            xmlNodeDulieu.AppendChild(HopDongThuongMai.ConvertCollectionHopDongToXML_TKN(xmlDocument, listHopDongThuongMai, TKMD_ID, listHang));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(xmlDocument.InnerXml, password);
                Globals.SaveMessage(xmlDocument.InnerXml, this.ID, MessageTitle.KhaiBaoBoSungHopDong);

            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {

                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        //string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                        //XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                        //XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                        return true;
                    }
                }
                else
                {
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL";
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage("Kết quả từ hải quan:", new Exception(kq));
                }
                throw;
            }
            return false;

        }

        public string GenerateToXmlEnvelope(MessageTypes messageType, MessageFunctions messageFunction, Guid referenceId, string path, string maHQ, string maDN)
        {
            XmlDocument doc = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");
                else
                    doc.Load(path + @"\TemplateXML\PhongBi.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = Globals.trimMaHaiQuan(maHQ);

            // Subject.
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)messageType).ToString();

            // Function.
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference ID.
            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = referenceId.ToString();

            // Message ID.
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = System.Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = maDN;
            nodeFrom.ChildNodes[0].InnerText = maDN;

            this.GuidStr = referenceId.ToString();
            this.Update();
            return doc.InnerXml;
        }

        /// <summary>
        /// Lấy số tiếp nhận khai báo.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLaySoTiepNhan(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi: " + ex.Message);
            }

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            string msgError = string.Empty;
            XmlNode xmlNodeResult = null;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);


                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    Globals.SaveMessage(xmlNodeContent.InnerXml, this.ID, MessageTitle.KhaiBaoBoSungLoiNhanDuLieuHoiHopDong, xmlNodeResult.InnerText);
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }
                }

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID, MessageTitle.TuChoiTiepNhan, ex.Message);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                }
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            try
            {
                if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();

                    Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoBoSungLayPhanHoiHopDong,
                        string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(new Exception(xmlDocument.InnerXml));
                throw ex;
            }
            return false;
        }

        /// <summary>
        /// Nhận phản hồi thông quan điện tử sau khi khai báo đã có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLayPhanHoi(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi: " + ex.Message);
            }
            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                kq.XmlSaveMessage(this.ID, MessageTitle.Error);
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                return false;
            }
            if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
            {



                Globals.SaveMessage(kq, this.ID, MessageTitle.TuChoiTiepNhan,
                                        string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), xmlNodeResult.InnerText));
                // Cập lại số tiếp nhận
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.Update();

                return true;
            }
            else if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungHopDongThanhCong, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                return true;
            }
            else if (xmlNodeResult.Attributes["SOTN"] != null)
            {

                kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungHopDongDuocChapNhan,
                    string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã được Hải quan chấp nhận.", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                this.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                this.Update();
                return true;
            }
            return false;
        }
        public static List<HopDongThuongMai> SelectCollectionDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction, SqlDatabase db)
        {
            List<HopDongThuongMai> collection = new List<HopDongThuongMai>();

            const string spName = "[dbo].[p_KDT_HopDongThuongMai_SelectDynamic]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand, transaction);
            while (reader.Read())
            {
                HopDongThuongMai entity = new HopDongThuongMai();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongTM"))) entity.SoHopDongTM = reader.GetString(reader.GetOrdinal("SoHopDongTM"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDongTM"))) entity.NgayHopDongTM = reader.GetDateTime(reader.GetOrdinal("NgayHopDongTM"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanThanhToan"))) entity.ThoiHanThanhToan = reader.GetDateTime(reader.GetOrdinal("ThoiHanThanhToan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViMua"))) entity.MaDonViMua = reader.GetString(reader.GetOrdinal("MaDonViMua"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViMua"))) entity.TenDonViMua = reader.GetString(reader.GetOrdinal("TenDonViMua"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViBan"))) entity.MaDonViBan = reader.GetString(reader.GetOrdinal("MaDonViBan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViBan"))) entity.TenDonViBan = reader.GetString(reader.GetOrdinal("TenDonViBan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGia"))) entity.TongTriGia = reader.GetDecimal(reader.GetOrdinal("TongTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

    }
}
