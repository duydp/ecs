﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class HuyToKhai
    {
        #region Huy va chinh sua to khai da duyet

        public bool WSLaySoTiepNhan(string password, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            XmlDocument xmlDocumentRequest = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocumentRequest.Load(path + @"\XMLChungTu\LayPhanHoiDaDuyet.xml");

            //set thong tin hai quan den
            XmlNode nodeTo = xmlDocumentRequest.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ.Trim();
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ.Trim();

            //set thong so gui di subject
            XmlNode nodeSubject = xmlDocumentRequest.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.Guid;

            XmlNode nodeMessageID = xmlDocumentRequest.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString()); ;

            XmlNode nodeFrom = xmlDocumentRequest.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = maDN.Trim();

            XmlNode nodeThongTin = xmlDocumentRequest.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDN.Trim();
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDN.Trim();

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ.Trim();
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ.Trim();

            XmlNode nodeDulieu = xmlDocumentRequest.GetElementsByTagName("DU_LIEU")[0];
            nodeDulieu.Attributes[0].Value = this.Guid;

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                //throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                kq.XmlSaveMessage(TKMD_ID, MessageTitle.KhaiBaoHuyTK, xmlNodeResult.InnerText);

                return false;
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                this.Update();

                //Message message = new Message();
                //message.ItemID = this.ID;
                //message.ReferenceID = new Guid(this.Guid);
                //message.MessageType = MessageTypes.ThongTin;
                //message.MessageFunction = MessageFunctions.LayPhanHoi;
                //message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                //message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                //message.MessageContent = kq;
                //message.TieuDeThongBao = MessageTitle.KhaiBaoHuyTKThanhCong;
                //message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                //message.CreatedTime = DateTime.Now;
                //message.Insert();

                kq.XmlSaveMessage(TKMD_ID, MessageTitle.KhaiBaoHuyTKThanhCong, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                return true;
            }

            return false;
        }

        public bool WSLayPhanHoi(string password, int soToKhai, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            XmlDocument xmlDocumentRequest = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocumentRequest.Load(path + @"\XMLChungTu\LayPhanHoiDaDuyet.xml");

            //set thong tin hai quan den
            XmlNode nodeTo = xmlDocumentRequest.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ.Trim();
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ.Trim();

            //set thong so gui di subject
            XmlNode nodeSubject = xmlDocumentRequest.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.Guid;

            XmlNode nodeMessageID = xmlDocumentRequest.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString()); ;

            XmlNode nodeFrom = xmlDocumentRequest.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = maDN.Trim();

            XmlNode nodeThongTin = xmlDocumentRequest.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDN.Trim();
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDN.Trim();

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ.Trim();
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ.Trim();

            XmlNode nodeDulieu = xmlDocumentRequest.GetElementsByTagName("DU_LIEU")[0];
            nodeDulieu.Attributes[0].Value = this.Guid;

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["TrangThai"].Value == "no")
                return false;
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }


            if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
            {
                //Message message = new Message();
                //message.ItemID = this.ID;
                //message.ReferenceID = new Guid(this.Guid);
                //message.MessageType = MessageTypes.ThongTin;
                //message.MessageFunction = MessageFunctions.LayPhanHoi;
                //message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                //message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                //message.MessageContent = kq;
                //message.TieuDeThongBao = MessageTitle.TuChoiTiepNhan;
                //string lydo = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                //message.NoiDungThongBao = string.Format("Tờ khai đã bị Hải quan từ chối hủy với lý do:\r\n{0}", lydo);
                //message.CreatedTime = DateTime.Now;
                //message.Insert();

                string lydo = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                kq.XmlSaveMessage(TKMD_ID, MessageTitle.TuChoiTiepNhan, string.Format("Tờ khai đã bị Hải quan từ chối hủy với lý do:\r\n{0}", lydo));

                // Cập lại số tiếp nhận
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                this.Update();

                return true;
            }
            else if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                this.Update();

                //Message message = new Message();
                //message.ItemID = this.ID;
                //message.ReferenceID = new Guid(this.Guid);
                //message.MessageType = MessageTypes.ThongTin;
                //message.MessageFunction = MessageFunctions.LayPhanHoi;
                //message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                //message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                //message.MessageContent = kq;
                //message.TieuDeThongBao = MessageTitle.KhaiBaoHuyTKThanhCong;
                //message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                //message.CreatedTime = DateTime.Now;
                //message.Insert();

                kq.XmlSaveMessage(TKMD_ID, MessageTitle.KhaiBaoHuyTKDuocChapNhan, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                return true;
            }
            else if (xmlNodeResult.Attributes["SOTK"].Value != null && xmlNodeResult.Attributes["SOTK"].Value == soToKhai.ToString())
            {
                //Message message = new Message();
                //message.ItemID = this.ID;
                //message.ReferenceID = new Guid(this.Guid);
                //message.MessageType = MessageTypes.ThongTin;
                //message.MessageFunction = MessageFunctions.LayPhanHoi;
                //message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                //message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                //message.MessageContent = kq;
                //message.TieuDeThongBao = MessageTitle.KhaiBaoHuyTKDuocChapNhan;
                //message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nHủy tờ khai đã được Hải quan chấp nhận.", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                //message.CreatedTime = DateTime.Now;
                //message.Insert();

                kq.XmlSaveMessage(TKMD_ID, MessageTitle.KhaiBaoHuyTKDuocChapNhan, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nHủy tờ khai đã được Hải quan chấp nhận.", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                this.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY;
                this.Update();
                return true;
            }

            return false;
        }

        #endregion Huy va chinh sua to khai da duyet

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HuyToKhai_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@Guid", SqlDbType.VarChar, Guid);
            db.AddInParameter(dbCommand, "@LyDoHuy", SqlDbType.NVarChar, LyDoHuy);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(SqlTransaction transaction, long tKMD_ID, SqlDatabase db)
        {
            string spName = "[dbo].[p_KDT_HuyToKhai_DeleteBy_TKMD_ID]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteNonQuery(dbCommand, transaction);
        }

    }
}
