﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class VNACC_CertificateOfOrigin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_CertificateOfOrigin));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbLoaiCO = new Janus.Windows.EditControls.UIComboBox();
            this.txtNuocCap = new Company.Interface.Controls.NuocHControl();
            this.dateNgayCap = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtToChucCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.uiCommandManager1 = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.btnView = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.gridList = new Janus.Windows.GridEX.GridEX();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(829, 494);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.BorderColor = System.Drawing.SystemColors.Highlight;
            this.uiGroupBox1.Controls.Add(this.cbbLoaiCO);
            this.uiGroupBox1.Controls.Add(this.txtNuocCap);
            this.uiGroupBox1.Controls.Add(this.dateNgayCap);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.txtNguoiCap);
            this.uiGroupBox1.Controls.Add(this.txtToChucCap);
            this.uiGroupBox1.Controls.Add(this.txtSoCO);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(829, 175);
            this.uiGroupBox1.TabIndex = 25;
            this.uiGroupBox1.Text = "Chi tiết C/0";
            // 
            // cbbLoaiCO
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = true;
            uiComboBoxItem1.Text = "A";
            uiComboBoxItem1.Value = "A";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = true;
            uiComboBoxItem2.Text = "D";
            uiComboBoxItem2.Value = "D";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = true;
            uiComboBoxItem3.Text = "E";
            uiComboBoxItem3.Value = "E";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "AK";
            uiComboBoxItem4.Value = "AK";
            this.cbbLoaiCO.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbbLoaiCO.Location = new System.Drawing.Point(551, 25);
            this.cbbLoaiCO.Name = "cbbLoaiCO";
            this.cbbLoaiCO.Size = new System.Drawing.Size(254, 21);
            this.cbbLoaiCO.TabIndex = 40;
            this.cbbLoaiCO.VisualStyleManager = this.vsmMain;
            // 
            // txtNuocCap
            // 
            this.txtNuocCap.BackColor = System.Drawing.Color.Transparent;
            this.txtNuocCap.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.txtNuocCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNuocCap.Location = new System.Drawing.Point(551, 59);
            this.txtNuocCap.Ma = "";
            this.txtNuocCap.Name = "txtNuocCap";
            this.txtNuocCap.ReadOnly = false;
            this.txtNuocCap.Size = new System.Drawing.Size(267, 22);
            this.txtNuocCap.TabIndex = 39;
            this.txtNuocCap.VisualStyleManager = null;
            // 
            // dateNgayCap
            // 
            this.dateNgayCap.Location = new System.Drawing.Point(109, 59);
            this.dateNgayCap.Name = "dateNgayCap";
            this.dateNgayCap.ReadOnly = false;
            this.dateNgayCap.Size = new System.Drawing.Size(106, 21);
            this.dateNgayCap.TabIndex = 37;
            this.dateNgayCap.TagName = "";
            this.dateNgayCap.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayCap.WhereCondition = "";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "message_32px.png");
            this.imageList1.Images.SetKeyName(1, "save_32px.png");
            this.imageList1.Images.SetKeyName(2, "sent_32px.png");
            this.imageList1.Images.SetKeyName(3, "engineering_32px.png");
            this.imageList1.Images.SetKeyName(4, "download_32px.png");
            this.imageList1.Images.SetKeyName(5, "cancel_32px.png");
            this.imageList1.Images.SetKeyName(6, "search_32px.png");
            this.imageList1.Images.SetKeyName(7, "plus_32px.png");
            this.imageList1.Images.SetKeyName(8, "ok_32px.png");
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(551, 101);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(254, 61);
            this.txtGhiChu.TabIndex = 35;
            this.txtGhiChu.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiCap
            // 
            this.txtNguoiCap.Location = new System.Drawing.Point(109, 141);
            this.txtNguoiCap.Name = "txtNguoiCap";
            this.txtNguoiCap.Size = new System.Drawing.Size(343, 21);
            this.txtNguoiCap.TabIndex = 34;
            this.txtNguoiCap.VisualStyleManager = this.vsmMain;
            // 
            // txtToChucCap
            // 
            this.txtToChucCap.Location = new System.Drawing.Point(109, 101);
            this.txtToChucCap.Name = "txtToChucCap";
            this.txtToChucCap.Size = new System.Drawing.Size(343, 21);
            this.txtToChucCap.TabIndex = 33;
            this.txtToChucCap.VisualStyleManager = this.vsmMain;
            // 
            // txtSoCO
            // 
            this.txtSoCO.Location = new System.Drawing.Point(109, 20);
            this.txtSoCO.Name = "txtSoCO";
            this.txtSoCO.Size = new System.Drawing.Size(343, 21);
            this.txtSoCO.TabIndex = 32;
            this.txtSoCO.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(15, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Người cấp C/O :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(14, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Tổ chức cấp C/O :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(13, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Ngày cấp C/O :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(467, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Ghi chú khác :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(467, 67);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Nước cấp C/O :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(467, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Loại C/O :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(13, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Số C/O :";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ImageIndex = 7;
            this.btnAdd.ImageList = this.imageList1;
            this.btnAdd.ImageSize = new System.Drawing.Size(22, 22);
            this.btnAdd.Location = new System.Drawing.Point(545, 114);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(90, 26);
            this.btnAdd.TabIndex = 36;
            this.btnAdd.Text = "Thêm File";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // uiCommandManager1
            // 
            this.uiCommandManager1.BottomRebar = this.BottomRebar1;
            this.uiCommandManager1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.uiCommandManager1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSave,
            this.cmdResult,
            this.cmdFeedback});
            this.uiCommandManager1.ContainerControl = this;
            this.uiCommandManager1.Id = new System.Guid("0b2c96e0-86c7-432f-be59-c0f442e4efca");
            this.uiCommandManager1.ImageList = this.imageList1;
            this.uiCommandManager1.LeftRebar = this.LeftRebar1;
            this.uiCommandManager1.MenuAnimation = Janus.Windows.UI.DropDownAnimation.Fade;
            this.uiCommandManager1.RightRebar = this.RightRebar1;
            this.uiCommandManager1.TopRebar = this.TopRebar1;
            this.uiCommandManager1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiCommandManager1.VisualStyleManager = this.vsmMain;
            this.uiCommandManager1.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandManager1_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.uiCommandManager1;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 391);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(837, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.uiCommandManager1;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend1,
            this.cmdSave1,
            this.cmdResult1,
            this.cmdFeedback1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(405, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.ImageIndex = 0;
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.ImageIndex = 3;
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.ImageIndex = 4;
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdSend
            // 
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.LargeImageIndex = 7;
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSave
            // 
            this.cmdSave.ImageIndex = 1;
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu thông tin";
            // 
            // cmdResult
            // 
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "Nhận phản hồi";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.uiCommandManager1;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 363);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.uiCommandManager1;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(837, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 363);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.uiCommandManager1;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(829, 28);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.BorderColor = System.Drawing.SystemColors.Highlight;
            this.uiGroupBox2.Controls.Add(this.dgList);
            this.uiGroupBox2.Controls.Add(this.btnView);
            this.uiGroupBox2.Controls.Add(this.btnDelete);
            this.uiGroupBox2.Controls.Add(this.btnAdd);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 175);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(829, 144);
            this.uiGroupBox2.TabIndex = 27;
            this.uiGroupBox2.Text = "File đính kèm";
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.CellToolTip = Janus.Windows.GridEX.CellToolTip.UseHeaderToolTip;
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.Location = new System.Drawing.Point(3, 17);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.Size = new System.Drawing.Size(823, 95);
            this.dgList.TabIndex = 37;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // btnView
            // 
            this.btnView.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ImageIndex = 6;
            this.btnView.ImageList = this.imageList1;
            this.btnView.ImageSize = new System.Drawing.Size(22, 22);
            this.btnView.Location = new System.Drawing.Point(737, 114);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(90, 26);
            this.btnView.TabIndex = 36;
            this.btnView.Text = "Xem File";
            this.btnView.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ImageIndex = 5;
            this.btnDelete.ImageList = this.imageList1;
            this.btnDelete.ImageSize = new System.Drawing.Size(22, 22);
            this.btnDelete.Location = new System.Drawing.Point(641, 114);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 26);
            this.btnDelete.TabIndex = 36;
            this.btnDelete.Text = "Xóa File";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.BorderColor = System.Drawing.SystemColors.Highlight;
            this.uiGroupBox3.Controls.Add(this.gridList);
            this.uiGroupBox3.Controls.Add(this.uiButton1);
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 325);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(829, 166);
            this.uiGroupBox3.TabIndex = 40;
            this.uiGroupBox3.Text = "Danh sách";
            // 
            // gridList
            // 
            this.gridList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridList.AlternatingColors = true;
            this.gridList.CellToolTip = Janus.Windows.GridEX.CellToolTip.UseHeaderToolTip;
            this.gridList.ColumnAutoResize = true;
            gridList_DesignTimeLayout.LayoutString = resources.GetString("gridList_DesignTimeLayout.LayoutString");
            this.gridList.DesignTimeLayout = gridList_DesignTimeLayout;
            this.gridList.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridList.GroupByBoxVisible = false;
            this.gridList.Location = new System.Drawing.Point(3, 17);
            this.gridList.Name = "gridList";
            this.gridList.RecordNavigator = true;
            this.gridList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridList.Size = new System.Drawing.Size(823, 111);
            this.gridList.TabIndex = 42;
            this.gridList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridList.VisualStyleManager = this.vsmMain;
            this.gridList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridList_RowDoubleClick);
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.ImageIndex = 5;
            this.uiButton1.ImageList = this.imageList1;
            this.uiButton1.ImageSize = new System.Drawing.Size(22, 22);
            this.uiButton1.Location = new System.Drawing.Point(733, 134);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(90, 26);
            this.uiButton1.TabIndex = 41;
            this.uiButton1.Text = "Xóa";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // VNACC_CertificateOfOrigin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 522);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_CertificateOfOrigin";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "VNACC_CertificateOfOrigin";
            this.Load += new System.EventHandler(this.VNACC_CertificateOfOrigin_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayCap;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiCap;
        private Janus.Windows.GridEX.EditControls.EditBox txtToChucCap;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.UI.CommandBars.UICommandManager uiCommandManager1;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnView;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private System.Windows.Forms.ImageList imageList1;
        private Company.Interface.Controls.NuocHControl txtNuocCap;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiCO;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX gridList;
        private Janus.Windows.EditControls.UIButton uiButton1;
    }
}