IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New]
GO

CREATE TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[TrangThaiXuLy] [int] NOT NULL,
[SoTN] [bigint] NULL,
[NgayTN] [datetime] NULL,
[MaHQ] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoaiBaoCao] [numeric] (1, 0) NOT NULL,
[NgayBatDauBC] [datetime] NOT NULL,
[NgayKetThucBC] [datetime] NOT NULL,
[LoaiSua] [int] NULL,
[GhiChuKhac] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GuidStr] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[GoodItem_ID] [bigint] NOT NULL FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New(ID),
[STT] [numeric] (5, 0) NOT NULL,
[TenHangHoa] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHangHoa] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TonDauKy] [numeric] (18, 4) NOT NULL,
[NhapTrongKy] [numeric] (18, 4) NOT NULL,
[TaiXuat] [numeric] (18, 4) NOT NULL,
[ChuyenMDSD] [numeric] (18, 4) NOT NULL,
[XuatKhac] [numeric] (18, 4) NOT NULL,
[XuatTrongKy] [numeric] (18, 4) NOT NULL,
[TonCuoiKy] [numeric] (18, 4) NOT NULL,
[GhiChu] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@LoaiBaoCao numeric(1, 0),
	@NgayBatDauBC datetime,
	@NgayKetThucBC datetime,
	@LoaiSua int,
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[LoaiBaoCao],
	[NgayBatDauBC],
	[NgayKetThucBC],
	[LoaiSua],
	[GhiChuKhac],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@LoaiBaoCao,
	@NgayBatDauBC,
	@NgayKetThucBC,
	@LoaiSua,
	@GhiChuKhac,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@LoaiBaoCao numeric(1, 0),
	@NgayBatDauBC datetime,
	@NgayKetThucBC datetime,
	@LoaiSua int,
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[LoaiBaoCao] = @LoaiBaoCao,
	[NgayBatDauBC] = @NgayBatDauBC,
	[NgayKetThucBC] = @NgayKetThucBC,
	[LoaiSua] = @LoaiSua,
	[GhiChuKhac] = @GhiChuKhac,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@LoaiBaoCao numeric(1, 0),
	@NgayBatDauBC datetime,
	@NgayKetThucBC datetime,
	@LoaiSua int,
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[LoaiBaoCao] = @LoaiBaoCao,
			[NgayBatDauBC] = @NgayBatDauBC,
			[NgayKetThucBC] = @NgayKetThucBC,
			[LoaiSua] = @LoaiSua,
			[GhiChuKhac] = @GhiChuKhac,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[LoaiBaoCao],
			[NgayBatDauBC],
			[NgayKetThucBC],
			[LoaiSua],
			[GhiChuKhac],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@LoaiBaoCao,
			@NgayBatDauBC,
			@NgayKetThucBC,
			@LoaiSua,
			@GhiChuKhac,
			@GuidStr
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[LoaiBaoCao],
	[NgayBatDauBC],
	[NgayKetThucBC],
	[LoaiSua],
	[GhiChuKhac],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[LoaiBaoCao],
	[NgayBatDauBC],
	[NgayKetThucBC],
	[LoaiSua],
	[GhiChuKhac],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[LoaiBaoCao],
	[NgayBatDauBC],
	[NgayKetThucBC],
	[LoaiSua],
	[GhiChuKhac],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectBy_GoodItem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectBy_GoodItem_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteBy_GoodItem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteBy_GoodItem_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Insert]
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@TonDauKy numeric(18, 4),
	@NhapTrongKy numeric(18, 4),
	@TaiXuat numeric(18, 4),
	@ChuyenMDSD numeric(18, 4),
	@XuatKhac numeric(18, 4),
	@XuatTrongKy numeric(18, 4),
	@TonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]
(
	[GoodItem_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[TaiXuat],
	[ChuyenMDSD],
	[XuatKhac],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
)
VALUES 
(
	@GoodItem_ID,
	@STT,
	@TenHangHoa,
	@MaHangHoa,
	@DVT,
	@TonDauKy,
	@NhapTrongKy,
	@TaiXuat,
	@ChuyenMDSD,
	@XuatKhac,
	@XuatTrongKy,
	@TonCuoiKy,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Update]
	@ID bigint,
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@TonDauKy numeric(18, 4),
	@NhapTrongKy numeric(18, 4),
	@TaiXuat numeric(18, 4),
	@ChuyenMDSD numeric(18, 4),
	@XuatKhac numeric(18, 4),
	@XuatTrongKy numeric(18, 4),
	@TonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]
SET
	[GoodItem_ID] = @GoodItem_ID,
	[STT] = @STT,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[DVT] = @DVT,
	[TonDauKy] = @TonDauKy,
	[NhapTrongKy] = @NhapTrongKy,
	[TaiXuat] = @TaiXuat,
	[ChuyenMDSD] = @ChuyenMDSD,
	[XuatKhac] = @XuatKhac,
	[XuatTrongKy] = @XuatTrongKy,
	[TonCuoiKy] = @TonCuoiKy,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_InsertUpdate]
	@ID bigint,
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@TonDauKy numeric(18, 4),
	@NhapTrongKy numeric(18, 4),
	@TaiXuat numeric(18, 4),
	@ChuyenMDSD numeric(18, 4),
	@XuatKhac numeric(18, 4),
	@XuatTrongKy numeric(18, 4),
	@TonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New] 
		SET
			[GoodItem_ID] = @GoodItem_ID,
			[STT] = @STT,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[DVT] = @DVT,
			[TonDauKy] = @TonDauKy,
			[NhapTrongKy] = @NhapTrongKy,
			[TaiXuat] = @TaiXuat,
			[ChuyenMDSD] = @ChuyenMDSD,
			[XuatKhac] = @XuatKhac,
			[XuatTrongKy] = @XuatTrongKy,
			[TonCuoiKy] = @TonCuoiKy,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]
		(
			[GoodItem_ID],
			[STT],
			[TenHangHoa],
			[MaHangHoa],
			[DVT],
			[TonDauKy],
			[NhapTrongKy],
			[TaiXuat],
			[ChuyenMDSD],
			[XuatKhac],
			[XuatTrongKy],
			[TonCuoiKy],
			[GhiChu]
		)
		VALUES 
		(
			@GoodItem_ID,
			@STT,
			@TenHangHoa,
			@MaHangHoa,
			@DVT,
			@TonDauKy,
			@NhapTrongKy,
			@TaiXuat,
			@ChuyenMDSD,
			@XuatKhac,
			@XuatTrongKy,
			@TonCuoiKy,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteBy_GoodItem_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteBy_GoodItem_ID]
	@GoodItem_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]
WHERE
	[GoodItem_ID] = @GoodItem_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[TaiXuat],
	[ChuyenMDSD],
	[XuatKhac],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectBy_GoodItem_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectBy_GoodItem_ID]
	@GoodItem_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[TaiXuat],
	[ChuyenMDSD],
	[XuatKhac],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]
WHERE
	[GoodItem_ID] = @GoodItem_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GoodItem_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[TaiXuat],
	[ChuyenMDSD],
	[XuatKhac],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[TonDauKy],
	[NhapTrongKy],
	[TaiXuat],
	[ChuyenMDSD],
	[XuatKhac],
	[XuatTrongKy],
	[TonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]	

GO

 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.1',GETDATE(), N'CẬP NHẬT TABE BÁO CÁO QUYẾT TOÁN MỚI')
END