﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages;
namespace Company.KDT.SHARE.Components
{
    public class ExportNonDeclarationDocument : DeclarationBase
    {
        [XmlElement("Consignee")]
        public List<Consignee> Consignees { get; set; }

        

    }
    public class Consignee 
    {
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("identity")]
        public string Identity { get; set; }
        [XmlElement("function")]
        public string Function { get; set; }
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }
        [XmlElement("CustomsGoodsItem")]
        public List<CustomsGoodsItem> CustomsGoodsItems { get; set; }
    }
}
