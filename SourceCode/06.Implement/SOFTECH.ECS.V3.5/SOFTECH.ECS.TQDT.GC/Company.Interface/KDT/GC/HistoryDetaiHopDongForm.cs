﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.GC
{
    public partial class HistoryDetaiHopDongForm : Company.Interface.BaseForm
    {
        public HopDong HD = new HopDong();
        public List<KDT_GC_NhomSanPham_Log> NhomSPCollection = new List<KDT_GC_NhomSanPham_Log>();
        public List<KDT_GC_SanPham_Log> SPCollection = new List<KDT_GC_SanPham_Log>();
        public List<KDT_GC_NguyenPhuLieu_Log> NPLCollection = new List<KDT_GC_NguyenPhuLieu_Log>();
        public List<KDT_GC_ThietBi_Log> TBCollection = new List<KDT_GC_ThietBi_Log>();
        public List<KDT_GC_HangMau_Log> HMCollection = new List<KDT_GC_HangMau_Log>();
        public HistoryDetaiHopDongForm()
        {
            InitializeComponent();
        }
        private void BinDataNhomSP()
        {
            try
            {
                dgNhomSanPham.Refetch();
                dgNhomSanPham.DataSource = NhomSPCollection;
                dgNhomSanPham.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BinDataSP()
        {
            try
            {
                dgSanPham.Refetch();
                dgSanPham.DataSource = SPCollection;
                dgSanPham.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BinDataNPL()
        {
            try
            {
                dgNguyenPhuLieu.Refetch();
                dgNguyenPhuLieu.DataSource = NPLCollection;
                dgNguyenPhuLieu.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BinDataTB()
        {
            try
            {
                dgThietBi.Refetch();
                dgThietBi.DataSource = TBCollection;
                dgThietBi.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BinDataHM()
        {
            try
            {
                dgHangMau.Refetch();
                dgHangMau.DataSource = HMCollection;
                dgHangMau.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                switch (tabHopDong.SelectedTab.Name)
                {
                    case "tpNhomSanPham":
                        GridEXFilterCondition filterNSP = new GridEXFilterCondition(dgNhomSanPham.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaHangHoa.Text);
                        dgNhomSanPham.RootTable.FilterCondition = filterNSP;
                        break;
                    case "tpNguyenPhuLieu":
                        GridEXFilterCondition filterNPL = new GridEXFilterCondition(dgNguyenPhuLieu.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaHangHoa.Text);
                        dgNguyenPhuLieu.RootTable.FilterCondition = filterNPL;
                        break;
                    case "tpSanPham":
                        GridEXFilterCondition filterSP = new GridEXFilterCondition(dgSanPham.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaHangHoa.Text);
                        dgSanPham.RootTable.FilterCondition = filterSP;
                        break;
                    case "tpThietBi":
                        GridEXFilterCondition filterTB = new GridEXFilterCondition(dgThietBi.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaHangHoa.Text);
                        dgThietBi.RootTable.FilterCondition = filterTB;
                        break;
                    case "tpHangMau":
                        GridEXFilterCondition filter = new GridEXFilterCondition(dgHangMau.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaHangHoa.Text);
                        dgHangMau.RootTable.FilterCondition = filter;
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void HistoryDetaiHopDongForm_Load(object sender, EventArgs e)
        {
            if (NhomSPCollection.Count > 0)
            {
                BinDataNhomSP();
            }
            else
            {
                tabHopDong.TabPages[0].TabVisible = false;
            }
            if (NPLCollection.Count > 0)
            {
                BinDataNPL();
            }
            else
            {
                tabHopDong.TabPages[1].TabVisible = false;
            }
            if (SPCollection.Count > 0)
            {
                BinDataSP();
            }
            else
            {
                tabHopDong.TabPages[2].TabVisible = false;
            }
            if (TBCollection.Count > 0)
            {
                BinDataTB();
            }
            else
            {
                tabHopDong.TabPages[3].TabVisible = false;
            }
            if (HMCollection.Count > 0)
            {
                BinDataHM();
            }
            else
            {
                tabHopDong.TabPages[4].TabVisible = false;
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH LỊCH SỬ HÀNG HOÁ.xls";
                sfNPL.Filter = "Excel files| *.xls";
                Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter = new Janus.Windows.GridEX.Export.GridEXExporter();
                switch (tabHopDong.SelectedTab.Name)
                {
                    case "tpNhomSanPham":
                        if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                        {
                            gridEXExporter.GridEX = dgNhomSanPham;
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter.Export(str);
                            str.Close();

                            if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        break;
                    case "tpNguyenPhuLieu":
                        if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                        {
                            gridEXExporter.GridEX = dgNguyenPhuLieu;
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter.Export(str);
                            str.Close();

                            if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        break;
                    case "tpSanPham":
                        if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                        {
                            gridEXExporter.GridEX = dgSanPham;
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter.Export(str);
                            str.Close();

                            if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        break;
                    case "tpThietBi":
                        if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                        {
                            gridEXExporter.GridEX = dgThietBi;
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter.Export(str);
                            str.Close();

                            if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        break;
                    case "tpHangMau":
                        if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                        {
                            gridEXExporter.GridEX = dgHangMau;
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter.Export(str);
                            str.Close();

                            if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgNhomSanPham_LoadingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgNguyenPhuLieu_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgSanPham_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["TenNhomSanPham"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value);
                    e.Row.Cells["NhomSanPham_ID"].Text = e.Row.Cells["TenNhomSanPham"].Text;
                    e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgThietBi_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                e.Row.Cells["TenNguyenTe"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value).Trim();
                e.Row.Cells["XuatXu"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value).Trim();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgHangMau_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
