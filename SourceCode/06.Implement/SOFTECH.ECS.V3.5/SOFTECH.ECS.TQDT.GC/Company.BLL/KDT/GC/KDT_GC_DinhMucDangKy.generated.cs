﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.GC.BLL.GC;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_DinhMucDangKy : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long SoTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public long ID_HopDong { set; get; }
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string GUIDSTR { set; get; }
		public string DeXuatKhac { set; get; }
		public string LyDoSua { set; get; }
		public int ActionStatus { set; get; }
		public string GuidReference { set; get; }
		public string NamTN { set; get; }
		public string HUONGDAN { set; get; }
		public string PhanLuong { set; get; }
		public string Huongdan_PL { set; get; }
		public int LoaiDinhMuc { set; get; }
        public string MaSanPham { set; get; }
        public List<KDT_GC_DinhMuc> DinhMucCollection = new List<KDT_GC_DinhMuc>();
		#endregion
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_DinhMucDangKy> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_DinhMucDangKy> collection = new List<KDT_GC_DinhMucDangKy>();
			while (reader.Read())
			{
				KDT_GC_DinhMucDangKy entity = new KDT_GC_DinhMucDangKy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiDinhMuc"))) entity.LoaiDinhMuc = reader.GetInt32(reader.GetOrdinal("LoaiDinhMuc"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        protected static List<KDT_GC_DinhMucDangKy> ConvertToCollectionAndMaSP(IDataReader reader)
        {
            List<KDT_GC_DinhMucDangKy> collection = new List<KDT_GC_DinhMucDangKy>();
            while (reader.Read())
            {
                KDT_GC_DinhMucDangKy entity = new KDT_GC_DinhMucDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiDinhMuc"))) entity.LoaiDinhMuc = reader.GetInt32(reader.GetOrdinal("LoaiDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
		public static bool Find(List<KDT_GC_DinhMucDangKy> collection, long id)
        {
            foreach (KDT_GC_DinhMucDangKy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_DinhMucDangKy VALUES(@SoTiepNhan, @TrangThaiXuLy, @NgayTiepNhan, @ID_HopDong, @MaHaiQuan, @MaDoanhNghiep, @GUIDSTR, @DeXuatKhac, @LyDoSua, @ActionStatus, @GuidReference, @NamTN, @HUONGDAN, @PhanLuong, @Huongdan_PL, @LoaiDinhMuc)";
            string update = "UPDATE t_KDT_GC_DinhMucDangKy SET SoTiepNhan = @SoTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, NgayTiepNhan = @NgayTiepNhan, ID_HopDong = @ID_HopDong, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, GUIDSTR = @GUIDSTR, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ActionStatus = @ActionStatus, GuidReference = @GuidReference, NamTN = @NamTN, HUONGDAN = @HUONGDAN, PhanLuong = @PhanLuong, Huongdan_PL = @Huongdan_PL, LoaiDinhMuc = @LoaiDinhMuc WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_DinhMucDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_HopDong", SqlDbType.BigInt, "ID_HopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamTN", SqlDbType.NVarChar, "NamTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Huongdan_PL", SqlDbType.NVarChar, "Huongdan_PL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDinhMuc", SqlDbType.Int, "LoaiDinhMuc", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_HopDong", SqlDbType.BigInt, "ID_HopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamTN", SqlDbType.NVarChar, "NamTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Huongdan_PL", SqlDbType.NVarChar, "Huongdan_PL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDinhMuc", SqlDbType.Int, "LoaiDinhMuc", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_DinhMucDangKy VALUES(@SoTiepNhan, @TrangThaiXuLy, @NgayTiepNhan, @ID_HopDong, @MaHaiQuan, @MaDoanhNghiep, @GUIDSTR, @DeXuatKhac, @LyDoSua, @ActionStatus, @GuidReference, @NamTN, @HUONGDAN, @PhanLuong, @Huongdan_PL, @LoaiDinhMuc)";
            string update = "UPDATE t_KDT_GC_DinhMucDangKy SET SoTiepNhan = @SoTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, NgayTiepNhan = @NgayTiepNhan, ID_HopDong = @ID_HopDong, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, GUIDSTR = @GUIDSTR, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ActionStatus = @ActionStatus, GuidReference = @GuidReference, NamTN = @NamTN, HUONGDAN = @HUONGDAN, PhanLuong = @PhanLuong, Huongdan_PL = @Huongdan_PL, LoaiDinhMuc = @LoaiDinhMuc WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_DinhMucDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_HopDong", SqlDbType.BigInt, "ID_HopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamTN", SqlDbType.NVarChar, "NamTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Huongdan_PL", SqlDbType.NVarChar, "Huongdan_PL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDinhMuc", SqlDbType.Int, "LoaiDinhMuc", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_HopDong", SqlDbType.BigInt, "ID_HopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamTN", SqlDbType.NVarChar, "NamTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Huongdan_PL", SqlDbType.NVarChar, "Huongdan_PL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDinhMuc", SqlDbType.Int, "LoaiDinhMuc", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_DinhMucDangKy Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_DinhMucDangKy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_DinhMucDangKy> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_DinhMucDangKy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_GC_DinhMucDangKy> SelectCollectionBy_ID_HopDong(long iD_HopDong)
		{
            IDataReader reader = SelectReaderBy_ID_HopDong(iD_HopDong);
			return ConvertToCollection(reader);	
		}
        public static List<KDT_GC_DinhMucDangKy> SelectCollectionBy_ID_HopDongAndMaSP(long iD_HopDong)
        {
            IDataReader reader = SelectReaderBy_ID_HopDongAndMaSP(iD_HopDong);
            return ConvertToCollectionAndMaSP(reader);
        }	
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ID_HopDong(long iD_HopDong)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, iD_HopDong);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_ID_HopDong(long iD_HopDong)
		{
			const string spName = "p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, iD_HopDong);
			
            return db.ExecuteReader(dbCommand);
		}
        public static IDataReader SelectReaderBy_ID_HopDongAndMaSP(long iD_HopDong)
        {
            const string spName = "p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDongAnMaSP";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, iD_HopDong);

            return db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_DinhMucDangKy(long soTiepNhan, int trangThaiXuLy, DateTime ngayTiepNhan, long iD_HopDong, string maHaiQuan, string maDoanhNghiep, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, string namTN, string hUONGDAN, string phanLuong, string huongdan_PL, int loaiDinhMuc)
		{
			KDT_GC_DinhMucDangKy entity = new KDT_GC_DinhMucDangKy();	
			entity.SoTiepNhan = soTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.ID_HopDong = iD_HopDong;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamTN = namTN;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			entity.LoaiDinhMuc = loaiDinhMuc;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, ID_HopDong);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamTN", SqlDbType.NVarChar, NamTN);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			db.AddInParameter(dbCommand, "@LoaiDinhMuc", SqlDbType.Int, LoaiDinhMuc);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_DinhMucDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_DinhMucDangKy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_DinhMucDangKy(long id, long soTiepNhan, int trangThaiXuLy, DateTime ngayTiepNhan, long iD_HopDong, string maHaiQuan, string maDoanhNghiep, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, string namTN, string hUONGDAN, string phanLuong, string huongdan_PL, int loaiDinhMuc)
		{
			KDT_GC_DinhMucDangKy entity = new KDT_GC_DinhMucDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.ID_HopDong = iD_HopDong;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamTN = namTN;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			entity.LoaiDinhMuc = loaiDinhMuc;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_DinhMucDangKy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, ID_HopDong);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamTN", SqlDbType.NVarChar, NamTN);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			db.AddInParameter(dbCommand, "@LoaiDinhMuc", SqlDbType.Int, LoaiDinhMuc);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_DinhMucDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_DinhMucDangKy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_DinhMucDangKy(long id, long soTiepNhan, int trangThaiXuLy, DateTime ngayTiepNhan, long iD_HopDong, string maHaiQuan, string maDoanhNghiep, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, string namTN, string hUONGDAN, string phanLuong, string huongdan_PL, int loaiDinhMuc)
		{
			KDT_GC_DinhMucDangKy entity = new KDT_GC_DinhMucDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.ID_HopDong = iD_HopDong;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamTN = namTN;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.Huongdan_PL = huongdan_PL;
			entity.LoaiDinhMuc = loaiDinhMuc;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, ID_HopDong);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamTN", SqlDbType.NVarChar, NamTN);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
			db.AddInParameter(dbCommand, "@LoaiDinhMuc", SqlDbType.Int, LoaiDinhMuc);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_DinhMucDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_DinhMucDangKy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_DinhMucDangKy(long id)
		{
			KDT_GC_DinhMucDangKy entity = new KDT_GC_DinhMucDangKy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_ID_HopDong(long iD_HopDong)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_DeleteBy_ID_HopDong]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, iD_HopDong);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_DinhMucDangKy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_DinhMucDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_DinhMucDangKy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}