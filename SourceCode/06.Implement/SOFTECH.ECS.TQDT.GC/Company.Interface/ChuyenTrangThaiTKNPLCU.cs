using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL;

namespace Company.Interface
{
    public partial class ChuyenTrangThaiTKNPLCU : BaseForm
    {
        BKCungUngDangKy bkcu;
        public ChuyenTrangThaiTKNPLCU(BKCungUngDangKy bk)
        {
            InitializeComponent();
            this.bkcu = bk;
            this.Text = this.Text + " " + this.bkcu.ID.ToString();
            this.lblCaption.Text = this.lblCaption.Text + " " + this.bkcu.ID.ToString();
            this.lblMaToKhai.Text = this.bkcu.ID.ToString();
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            if (this.validData())
            {
                this.getData();
                this.bkcu.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                this.bkcu.Update();
                Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty,bkcu.ID,bkcu.GUIDSTR,
                    Company.KDT.SHARE.Components.MessageTypes.DanhMucNguyenPhuLieu,
                     Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                    Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay,string.Format("Trước khi chuyển số tiếp nhận ={0}", bkcu.SoTiepNhan));
                if (showMsg("MSG_240241") == "Cancel")
                //if (ShowMessage("Chuyển trạng thái thành công", false) == "Cancel")
                {
                    this.Close();
                }
            }
            else
            {
                showMsg("MSG_240243");
                //ShowMessage("Dữ liệu nhập vào chưa hợp lệ! \nVui lòng nhập lại.", false);
            }
        }

        private bool validData()
        {
            bool value = true;
            if (txtSoToKhai.Text == "" || int.Parse(txtSoToKhai.Text) < 0) value = false;

            return value;
        }

        private void getData()
        {
            this.bkcu.SoBangKe = int.Parse(txtSoToKhai.Text);
            this.bkcu.NgayTiepNhan = ccNgayDayKy.Value;
        }

        private void btnHuybo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}