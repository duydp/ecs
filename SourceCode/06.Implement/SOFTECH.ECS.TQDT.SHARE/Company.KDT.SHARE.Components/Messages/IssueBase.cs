﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class IssueBase
    {
        /// <summary>
        /// Loại chứng từ  (Nhập khẩu=929, Xuất khẩu=930)
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số tham chiếu, định danh gửi và lấy phản hồi.
        /// Sử dụng mã GUID
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày khai báo YYYY-MM-DD HH:mm:ss
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Loại chức năng
        /// 1. Hủy
        /// 5. Sửa
        /// 8. Khai báo
        /// 12. Chưa xử lý
        /// 13. Hỏi trạng thái
        /// 16. Đề nghị 
        /// 27. Không chấp nhận
        /// 29. Cấp số tiếp nhận
        /// 30. Cấp số tờ khai
        /// 31. Duyệt luồng chứng từ
        /// 32. Thông báo thông quan
        /// 33. Thông báo thực xuất
        /// 313. Thông báo bổ sung chứng từ
        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        /// Nơi khai báo
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Đơn vị HQ khai báo
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }
    
    }
}
