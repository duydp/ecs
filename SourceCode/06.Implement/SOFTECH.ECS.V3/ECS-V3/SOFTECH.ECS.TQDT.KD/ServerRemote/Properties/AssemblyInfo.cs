﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("HỖ TRỢ KÝ CHỮ KÝ SỐ KHAI HQ")]
[assembly: AssemblyDescription("PHẦN MỀM ECS-SIGNREMOTE")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Công ty Cổ phần Softech")]
[assembly: AssemblyProduct("SOFTECH.ECS.TQDT.SIGNREMOTE")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("ecs@softech.vn")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("34779e10-2ecb-4f5e-ac4d-97f3a8841cbf")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
