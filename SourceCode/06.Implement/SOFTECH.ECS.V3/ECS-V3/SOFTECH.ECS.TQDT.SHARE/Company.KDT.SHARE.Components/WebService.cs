﻿//#define DEBUG
using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.ServiceModel.Description;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Data;


namespace Company.KDT.SHARE.Components
{

    [ServiceContractAttribute(Namespace = "http://softech.vn/", ConfigurationName = "ISignMessage")]
    public interface ISignMessage
    {
        [OperationContractAttribute(Action = "http://softech.vn/ClientSend", ReplyAction = "*")]
        string ClientSend(string message);

        [OperationContractAttribute(Action = "http://softech.vn/ServerSend", ReplyAction = "*")]
        string ServerSend(string message);

        [OperationContractAttribute(Action = "http://softech.vn/IsExist", ReplyAction = "*")]
        bool IsExist(string username, string password);

        [OperationContractAttribute(Action = "http://softech.vn/GetDaiLy", ReplyAction = "*")]
        DataTable GetDaiLy(string username, string password);

        [OperationContractAttribute(Action = "http://softech.vn/ChangePass", ReplyAction = "*")]
        string ChangePass(string username, string password, string accountDL, string NewPass, bool ChangePassAdmin);
    }
    [ServiceContractAttribute(Namespace = "http://cis.customs.gov.vn/", ConfigurationName = "IHaiQuanService")]
    public interface IHaiQuanContract
    {
        [OperationContractAttribute(Action = "http://cis.customs.gov.vn/Send", ReplyAction = "*")]
        string Send(string MessageXML, string UserId, string Password);
        [OperationContractAttribute(Action = "http://cis.customs.gov.vn/TestWebservie", ReplyAction = "*")]
        string TestWebservie();
    }
    /// <summary>
    /// KhanhHN bo sung webservice dong bo du lieu
    /// </summary>
    [ServiceContractAttribute(Namespace = "http://tempuri.org/", ConfigurationName = "ISyncData")]
    public interface ISyncData
    {
        #region Đồng bộ dữ liệu giữa đại lý và doanh nghiệp
        [OperationContractAttribute(Action = "http://tempuri.org/UpdateTrack", ReplyAction = "*")]
        string UpdateTrack(string userNameLogin, string passWordLogin, long soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep, string msg);


        [OperationContractAttribute(Action = "http://tempuri.org/InsertUpdateUserDaiLy", ReplyAction = "*")]
        int InsertUpdateUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung, string matKhau, string hoVaTen, string moTa, string maDoanhNghiep);

        [OperationContractAttribute(Action = "http://tempuri.org/CheckUserName", ReplyAction = "*")]
        bool CheckUserName(string userName);

        [OperationContractAttribute(Action = "http://tempuri.org/DeleteUserDaiLy", ReplyAction = "*")]
        bool DeleteUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung, string matKhau);

        [OperationContractAttribute(Action = "http://tempuri.org/SelectTrackDynamic", ReplyAction = "*")]
        string SelectTrackDynamic(string userNameLogin, string passWordLogin, string whereCondition, string orderBy);

        [OperationContractAttribute(Action = "http://tempuri.org/SelectUserDaiLyAll", ReplyAction = "*")]
        string SelectUserDaiLyAll(string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/SelectUserDaiLy", ReplyAction = "*")]
        string SelectUserDaiLy(string maDoanhNghiep, string userNameLogin, string passWordLogin);


        [OperationContractAttribute(Action = "http://tempuri.org/SelectTrack", ReplyAction = "*")]
        string SelectTrack(string userNameLogin, string passWordLogin, long soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep);

        [OperationContractAttribute(Action = "http://tempuri.org/LoginDaiLy", ReplyAction = "*")]
        string LoginDaiLy(string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/SendDaiLy", ReplyAction = "*")]
        string SendDaiLy(string message, string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/CheckDaiLy", ReplyAction = "*")]
        string CheckDaiLy(string maDaiLy, string maDoanhNghiep);

        [OperationContractAttribute(Action = "http://tempuri.org/SendDoanhNghiep", ReplyAction = "*")]
        string SendDoanhNghiep(string guiStr, string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/ReceivesViews", ReplyAction = "*")]
        string ReceivesViews(string userNameLogin, string passWordLogin, string messages);

        [OperationContractAttribute(Action = "http://tempuri.org/CheckCauHinhDL", ReplyAction = "*")]
        string CheckCauHinhDL(string MaDoanhNghiep);

        [OperationContractAttribute(Action = "http://tempuri.org/InsertUpdateCauHinhDaiLy", ReplyAction = "*")]
        bool InsertUpdateCauHinhDaiLy(string userNameLogin, string passWordLogin, string maDoanhNghiep, string tenDoanhNghiep, int value, string ghiChu, int isUse);

        [OperationContractAttribute(Action = "http://tempuri.org/SendGC", ReplyAction = "*")]
        string SendGC(string message, string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/ReceivesGC", ReplyAction = "*")]
        string ReceivesGC(string Type, string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/LoginDaiLy_V3", ReplyAction = "*")]
        string LoginDaiLy_V3(string userNameLogin, string passWordLogin);
        #endregion
        [OperationContractAttribute(Action = "http://tempuri.org/GetDanhMucOnline", ReplyAction = "*")]
        System.Data.DataSet GetDanhMucOnline(string passWordLogin, DateTime dateLastUpdated, Company.KDT.SHARE.Components.EDanhMucHaiQuan type);

        [OperationContractAttribute(Action = "http://tempuri.org/LayDanhMucLinhVuc", ReplyAction = "*")]
        System.Data.DataSet LayDanhMucLinhVuc(string passWordLogin, DateTime dateLastUpdated);

        [OperationContractAttribute(Action = "http://tempuri.org/XoaDanhMucLinhVuc", ReplyAction = "*")]
        bool XoaDanhMucLinhVuc(string passWordLogin, string maLinhVuc);

        [OperationContractAttribute(Action = "http://tempuri.org/LuuDanhMucLinhVuc", ReplyAction = "*")]
        bool LuuDanhMucLinhVuc(string passWordLogin, System.Data.DataTable data);

        [OperationContractAttribute(Action = "http://tempuri.org/LayDanhMucThuVien", ReplyAction = "*")]
        System.Data.DataSet LayDanhMucThuVien(string passWordLogin, DateTime dateLastUpdated);

        [OperationContractAttribute(Action = "http://tempuri.org/LayDanhMucCauHoiDoanhNghiep", ReplyAction = "*")]
        System.Data.DataSet LayDanhMucCauHoiDoanhNghiep(string passWordLogin, DateTime dateLastUpdated);

        [OperationContractAttribute(Action = "http://tempuri.org/TaoMoiCauHoi", ReplyAction = "*")]
        string TaoMoiCauHoi(string passWordLogin, int LinhVuc_ID, string Phanloai, string TieuDe, string NoiDungCauHoi, string TraLoi, bool HienThi, string GhiChu);


        [OperationContractAttribute(Action = "http://tempuri.org/CapNhatCauHoi", ReplyAction = "*")]
        string CapNhatCauHoi(string passWordLogin, int MaCauHoi, int LinhVuc, string Phanloai, string TieuDe, string NoiDungCauHoi, string TraLoi, bool HienThi, string GhiChu);

        [OperationContractAttribute(Action = "http://tempuri.org/XoaCauHoi", ReplyAction = "*")]
        string XoaCauHoi(string passWordLogin, string maCauHoi);

      
    }

    public class WebService
    {

        #region Load Config Webservice

        public static string LoadConfigure(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);

            return config.AppSettings.Settings[key].Value.ToString();
        }
        static WebService()
        {
            //Hook a callback to verify the remote certificate
            ServicePointManager.ServerCertificateValidationCallback =
                delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

        }
        private static T CreateService<T>(string url)
        {
            T service;
            try
            {

                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                    url = string.Format("http://{0}", url);
                BasicHttpBinding binding = new BasicHttpBinding();
                EndpointAddress address = new EndpointAddress(url);
                string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
                string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
                if (!string.IsNullOrEmpty(host + port))
                {
                    WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                    proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    binding.ProxyAddress = proxy.Address;
                }
                binding.UseDefaultWebProxy = true;
                binding.SendTimeout = new TimeSpan(0, Globals.TimeConnect, 0);
                binding.MaxReceivedMessageSize = int.MaxValue;
                binding.MaxBufferSize = int.MaxValue;
                binding.TextEncoding = System.Text.Encoding.UTF8;
                binding.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                binding.OpenTimeout = new TimeSpan(0, Globals.TimeConnect, 0);
                //binding.Security.Message.ClientCredentialType.
                if (address.Uri.Scheme == "https")
                {
                    //Dùng https
                    binding.Security.Mode = BasicHttpSecurityMode.Transport;
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
                }

                /*
                 * Mở theo cách add service(Source gen). LanNT
                 * cis = new Company.KDT.SHARE.Components.CisWS.CISServiceSoapClient(binding, address);
                 */
                /*
                 * Mở theo Channel
                 */
                ChannelFactory<T> factory = new ChannelFactory<T>(binding, address);

                //client.ClientCredentials.UserName.UserName = "shoaib";
                //client.ClientCredentials.UserName.Password = "shaikh";
                service = factory.CreateChannel();


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Khởi tạo service kết nối", ex);
                throw new Exception("Mở kênh kết nối với Hải quan gặp lỗi.\r\n" + ex.Message + "\r\nURL = " + url);
            }
            return service;
        }
        public static ISignMessage SignMessage()
        {
            ISignMessage service = null;
            try
            {
                string url;
                url = LoadConfigure("WS_SIGNMESSAGE");
                service = CreateService<ISignMessage>(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        
        public static IHaiQuanContract HQService(bool isDaiLy,string MaDoanhNghiep)
        {
            IHaiQuanContract service = null;
            string url;
            try
            {
                if (isDaiLy)
                {
                    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                    if (configURL.Contains("http"))
                        url = configURL;
                    else
                        url = string.Format("http://{0}",configURL);
                }
                else
                {
                    url = LoadConfigure("WS_URL");
                }
                
                service = CreateService<IHaiQuanContract>(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }

        public static ISyncData SyncService()
        {
            ISyncData service = null;
            string url;
            try
            {
                url = LoadConfigure("WS_SyncData");
                service = CreateService<ISyncData>(url);

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw (ex);
            }
            return service;
        }
        public static Company.KDT.SHARE.Components.WS.KDTService GetWS()
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = new Company.KDT.SHARE.Components.WS.KDTService();
            try
            {
                string url = LoadConfigure("WS_URL");
                kdt.Url = url;

                string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");

                string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");

                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsUseProxy").Equals("True") || !string.IsNullOrEmpty(host + port))
                {
                    WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                    kdt.Proxy = proxy;

                }
                else
                {
                    kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                    kdt.Proxy.Credentials = CredentialCache.DefaultCredentials;
                    kdt.Credentials = CredentialCache.DefaultCredentials;
                }

                kdt.Timeout = 300000;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Khởi tạo service kết nối", ex);
            }

            return kdt;
        }
        #endregion
    }
}
