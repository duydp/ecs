﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System.Diagnostics;

namespace Company.Interface.Report
{
    
    public partial class ReportViewTKNTQDTForm : BaseForm
    {
        public Company.Interface.Report.KDDT.TQDTToKhaiNK ToKhaiChinhReport = new Company.Interface.Report.KDDT.TQDTToKhaiNK();
        public Company.Interface.Report.KDTD.TQDTPhuLucToKhaiNhap PhuLucReport = new Company.Interface.Report.KDTD.TQDTPhuLucToKhaiNhap();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public XRControl Cell = new XRControl();
        int index = 0;
        private int soDongHang = 9; //Mac dinh = 9 dong hang tren phu luc to khai.
        public ReportViewTKNTQDTForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            //DATLMQ bổ sung thiết lập margin: 25/02/2011
            this.ToKhaiChinhReport.Margins.Top = 0;
            this.ToKhaiChinhReport.Margins.Bottom = 0;
            this.PhuLucReport.Margins.Top = 0;
            this.PhuLucReport.Margins.Bottom = 0;

            if (this.TKMD.HMDCollection.Count > 3)
            {
                int count = (this.TKMD.HMDCollection.Count - 1) / 9 + 1;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            cboToKhai.SelectedIndex = 0;
        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (char.IsDigit(txtSoDongHang.Text, 0))
                {
                    soDongHang = Convert.ToInt32(txtSoDongHang.Text);
                }
                else
                {
                    Globals.ShowMessage("Số dòng hàng không phải là kiểu số.", false);
                    txtSoDongHang.Focus();
                    return;
                }

                if (cboToKhai.SelectedIndex == 0)
                {
                    this.ToKhaiChinhReport.TKMD = this.TKMD;
                    this.ToKhaiChinhReport.report = this;
                    this.ToKhaiChinhReport.isCuaKhau = chkCuaKhau.Checked;
                    this.ToKhaiChinhReport.BindReport();
                    printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                    this.ToKhaiChinhReport.CreateDocument();

                }
                else
                {

                    try
                    {
                        index = cboToKhai.SelectedIndex;
                    }
                    catch { index = 1; }
                    txtDeXuatKhac.Text = "";
                    txtTenNhomHang.Text = "";
                    HangMauDichCollection HMDReportCollection = new HangMauDichCollection();
                    //int begin = (cboToKhai.SelectedIndex - 1) * 9; //Comment by Hungtq
                    int begin = (cboToKhai.SelectedIndex - 1) * soDongHang;
                    //int end = cboToKhai.SelectedIndex * 9; //Comment by Hungtq
                    int end = cboToKhai.SelectedIndex * soDongHang;
                    if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                    for (int i = begin; i < end; i++)
                        HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                    this.PhuLucReport = new Company.Interface.Report.KDTD.TQDTPhuLucToKhaiNhap();
                    this.PhuLucReport.report = this;
                    this.PhuLucReport.TKMD = this.TKMD;
                    if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                        this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                    this.PhuLucReport.HMDCollection = HMDReportCollection;
                    this.PhuLucReport.soDongHang = soDongHang;
                    this.PhuLucReport.isCuaKhau = chkCuaKhau.Checked;
                    this.PhuLucReport.BindReport(index.ToString());
                    printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                    this.PhuLucReport.CreateDocument();

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {

                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.PhuLucReport.CreateDocument();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
            //        break;
            //    case "excel":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            //        break;

            //}
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                //datlmq update 29072010
                if (txtTenNhomHang.Text != "")
                    this.ToKhaiChinhReport.setThongTin(this.Cell, txtTenNhomHang.Text);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.PhuLucReport.CreateDocument();
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                //this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.PhuLucReport.setVisibleImage(true);
                this.PhuLucReport.CreateDocument();
            }
        }

        private void chkInMaHang_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                if (chkInMaHang.Checked)
                    this.ToKhaiChinhReport.inMaHang = true;
                else
                    this.ToKhaiChinhReport.inMaHang = false;
                this.ToKhaiChinhReport.isCuaKhau = chkCuaKhau.Checked;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                if (chkInMaHang.Checked)
                    this.PhuLucReport.inMaHang = true;
                else
                    this.PhuLucReport.inMaHang = false;
                this.PhuLucReport.isCuaKhau = chkCuaKhau.Checked;
                this.PhuLucReport.BindReport(index.ToString());
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void chkHinhNen_CheckedChanged(object sender, EventArgs e)
        {

        } 

        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReport.isCuaKhau = chkCuaKhau.Checked;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                //this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.PhuLucReport.isCuaKhau = chkCuaKhau.Checked;
                this.PhuLucReport.BindReport(index.ToString());
                this.PhuLucReport.CreateDocument();

            }
        }

        private void chkMienThue1_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReport.MienThue1 = chkMienThue1.Checked;
                this.ToKhaiChinhReport.MienThue2 = chkMienThue2.Checked;
                this.ToKhaiChinhReport.isCuaKhau = chkCuaKhau.Checked;
                this.ToKhaiChinhReport.BindReport();
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.MienThue1 = chkMienThue1.Checked;
                this.PhuLucReport.MienThue2 = chkMienThue2.Checked;
                this.PhuLucReport.isCuaKhau = chkCuaKhau.Checked;
                this.PhuLucReport.BindReport(cboToKhai.Items.Count.ToString());
                this.PhuLucReport.CreateDocument();

            }
        }
        /// <summary>
        /// In pdf Button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {

                this.ToKhaiChinhReport.CreateDocument();
                this.ToKhaiChinhReport.Margins.Top = 0;
                this.ToKhaiChinhReport.Margins.Bottom = 0;

                // BEGIN: SAVE FILE TO PDF.
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.InitialDirectory = "C:\\";
                dlg.RestoreDirectory = true;
                dlg.Filter = "pdf files (*.pdf)|*.pdf";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    if (string.IsNullOrEmpty(dlg.FileName) == false)
                    {
                        string filePath = dlg.FileName;
                        this.ToKhaiChinhReport.PrintingSystem.ExportToPdf(filePath);
                        try
                        {
                            // Open file pdf.
                            Process.Start(filePath);
                        }
                        catch { }
                    }
                }
                // END: SAVE FILE TO PDF.
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.CreateDocument();
                this.PhuLucReport.Margins.Top = 0;
                this.PhuLucReport.Margins.Bottom = 0;

                // BEGIN: SAVE FILE TO PDF.
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.InitialDirectory = "C:\\";
                dlg.RestoreDirectory = true;
                dlg.Filter = "pdf files (*.pdf)|*.pdf";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    if (string.IsNullOrEmpty(dlg.FileName) == false)
                    {
                        string filePath = dlg.FileName;
                        this.PhuLucReport.PrintingSystem.ExportToPdf(filePath);
                        try
                        {
                            // Open file pdf.
                            Process.Start(filePath);
                        }
                        catch { }
                    }
                }
                // END: SAVE FILE TO PDF.
                this.PhuLucReport.CreateDocument();
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            //if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
            //{
            //    Globals.ExportExcel(Globals.ToKhaiType.Nhap, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThue1.Checked, chkMienThue2.Checked);
            //}
            if (this.cboToKhai.SelectedIndex == 0)
            {
                Globals.ExportExcel(Globals.ToKhaiType.Nhap, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThue1.Checked, chkMienThue2.Checked, "");
            }
            else
            {
                Globals.ExportExcel(Globals.ToKhaiType.PhucLucTKN, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThue1.Checked, chkMienThue2.Checked, cboToKhai.SelectedIndex.ToString());
            }
        }

        private void chkCuaKhau_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReport.MienThue1 = chkMienThue1.Checked;
                this.ToKhaiChinhReport.MienThue2 = chkMienThue2.Checked;
                this.ToKhaiChinhReport.isCuaKhau = chkCuaKhau.Checked;
                this.ToKhaiChinhReport.BindReport();
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.MienThue1 = chkMienThue1.Checked;
                this.PhuLucReport.MienThue2 = chkMienThue2.Checked;
                this.PhuLucReport.isCuaKhau = chkCuaKhau.Checked;
                this.PhuLucReport.BindReport(cboToKhai.Items.Count.ToString());
                this.PhuLucReport.CreateDocument();

            }
        }

        private void chkChuyenDoiDinhDang_CheckedChanged(object sender, EventArgs e)
        {
            
        }
    }
}