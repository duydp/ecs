﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.SXXK
{
    public partial class DMHHSendForm : BaseForm
    {
        private readonly SanPhamDangKy spDangKy = new SanPhamDangKy();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        //-----------------------------------------------------------------------------------------
        public DMHHSendForm()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }

        //-----------------------------------------------------------------------------------------
        private void SanPhamSendForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            dgList.DataSource = this.spDangKy.SPCollection;
            cbLoaiChungTu.SelectedIndex = 0;
        }
        private void SetCommand()
        {
            bool allowSend = (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || spDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO);
            XacNhan.Enabled = allowSend ? Janus.Windows.UI.InheritableBoolean.False : Janus.Windows.UI.InheritableBoolean.True;
            cmdSend.Enabled = cmdAdd.Enabled = cmdSave.Enabled = cmdSend.Enabled = cmdSend1.Enabled = allowSend ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
            btnDelete.Enabled = allowSend;
            if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Chờ duyệt chính thức";
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Đã duyệt";
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Không phê duyệt";
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Hủy khai báo";
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Chờ hủy";
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Đã hủy thành công";
            }
        }


        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            WSForm wsForm = new WSForm();
            string password = "";
            try
            {

                if (this.spDangKy.SPCollection.Count == 0)
                {
                    // ShowMessage("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    return;
                }
                // Master.
                this.spDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN.Trim();
                this.spDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI.Trim();
                this.spDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY.Trim();

                // Thực hiện gửi thông tin.
                string[] danhsachDaDangKy = new string[0];

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                //progessSendXML.RunWorkerAsync(prbar);

                xmlCurrent = this.spDangKy.WSSendXML(password);

                this.Cursor = Cursors.Default;
                //lblTrangThai.Text = "Chờ xác nhận phía hải quan";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Chờ xác nhận phía hải quan";
                }
                else
                {
                    lblTrangThai.Text = "Wait to confirm from Customs";
                }
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spDangKy.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                LayPhanHoi(password);

                if (this.spDangKy.SoTiepNhan > 0)
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = spDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                                hd.TrangThai = spDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.KHAI_BAO;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới nguyên phụ liệu.
        /// </summary>
        private void add()
        {
            DMHHEditForm f = new DMHHEditForm();
            f.OpenType = OpenFormType.Insert;
            f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            f.SPCollection = spDangKy.SPCollection;
            f.spcollection = spDangKy.SPCollection;
            f.spDangKy = spDangKy;
            f.LoaiChungTu = Convert.ToInt16(cbLoaiChungTu.SelectedValue);
            f.ShowDialog(this);

            if (f.SPDetail != null)
            {
                this.spDangKy.SPCollection = f.spcollection;
                dgList.DataSource = spDangKy.SPCollection;
                
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void save()
        {
            try
            {

                if (this.spDangKy.SPCollection.Count == 0)
                {
                    // ShowMessage("Danh sách sản phẩm rỗng.\nKhông thể cập nhật dữ liệu.", false);
                    MLMessages("Danh sách sản phẩm rỗng.\nKhông thể cập nhật dữ liệu.", "MSG_SAV11", "", false);

                    return;
                }

                this.Cursor = Cursors.WaitCursor;
                // Master.
                this.spDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (this.spDangKy.ID == 0)
                {
                    this.spDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    this.spDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.spDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.spDangKy.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.spDangKy.GUIDSTR = Guid.NewGuid().ToString();
                }
                // Detail.
                int sttHang = 1;
                foreach (SanPham spD in this.spDangKy.SPCollection)
                {
                    spD.STTHang = sttHang++;
                }

                if (this.spDangKy.InsertUpdateFull())
                {
                    this.Cursor = Cursors.Default;

                    #region Lưu log thao tác
                    string where = "1 = 1";
                    where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", spDangKy.ID,LoaiKhaiBao.SanPham);
                    List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                    if (listLog.Count > 0)
                    {
                        long idLog = listLog[0].IDLog;
                        string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                        long idDK = listLog[0].ID_DK;
                        string guidstr = listLog[0].GUIDSTR_DK;
                        string userKhaiBao = listLog[0].UserNameKhaiBao;
                        DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                        string userSuaDoi = GlobalSettings.UserLog;
                        DateTime ngaySuaDoi = DateTime.Now;
                        string ghiChu = listLog[0].GhiChu;
                        bool isDelete = listLog[0].IsDelete;
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                    userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                    }
                    else
                    {
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                        log.LoaiKhaiBao =LoaiKhaiBao.SanPham;
                        log.ID_DK = spDangKy.ID;
                        log.GUIDSTR_DK = this.spDangKy.GUIDSTR;
                        log.UserNameKhaiBao = GlobalSettings.UserLog;
                        log.NgayKhaiBao = DateTime.Now;
                        log.UserNameSuaDoi = GlobalSettings.UserLog;
                        log.NgaySuaDoi = DateTime.Now;
                        log.GhiChu = "";
                        log.IsDelete = false;
                        log.Insert();
                    }
                    #endregion

                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    //ShowMessage("Cập nhật thành công!", false);
                    MLMessages("Lưu thành công!", "MSG_SAV02", "", false);
                }
                else
                {
                    this.Cursor = Cursors.Default;
                    //ShowMessage("Cập nhật không thành công!", false);
                    MLMessages("Lưu không thành công!", "MSG_SAV01", "", false);
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("" + ex.Source);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        //-----------------------------------------------------------------------------------------
        private void updateRowOnGrid(string maNPL)
        {
            GridEXRow[] jrows = dgList.GetRows();
            foreach (GridEXRow row in jrows)
            {
                if (row.Cells["Ma"].Value.ToString().Equals(maNPL))
                {
                    row.BeginEdit();
                    row.Cells["IsExistOnServer"].Value = 1;
                    row.EndEdit();
                    break;
                }
            }

        }

        //-----------------------------------------------------------------------------------------
        private void updateRowsOnGrid(IEnumerable<string> collection)
        {
            GridEXRow[] jrows = dgList.GetRows();
            foreach (GridEXRow row in jrows)
            {
                row.BeginEdit();
                row.Cells["IsExistOnServer"].Value = 0;
                row.EndEdit();
            }

            foreach (string s in collection)
            {
                this.updateRowOnGrid(s);
            }
        }
        #endregion


        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                switch (e.Row.Cells["LoaiSP"].Value.ToString())
                {
                    case "1":
                        e.Row.Cells["LoaiSP"].Text = "Nguyên phụ liệu";
                        break;
                        case "2":
                        e.Row.Cells["LoaiSP"].Text = "Sản phẩm";
                        break;
                        case "3":
                        e.Row.Cells["LoaiSP"].Text = "Thiết bị";
                        break;
                        case "4":
                        e.Row.Cells["LoaiSP"].Text = "Hàng mẫu";
                        break;
                }
 
                
            }
            
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    DMHHEditForm f = new DMHHEditForm();
                    f.SPDetail = this.spDangKy.SPCollection[i.Position];
                    f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    f.OpenType = OpenFormType.Edit;
                    f.SPCollection = spDangKy.SPCollection;
                    f.SPCollection.RemoveAt(i.Position);
                    f.spDangKy = spDangKy;
                    f.ShowDialog(this);

                    // Chưa lưu.
                    if (this.spDangKy.ID == 0)
                    {
                        if (f.SPDetail != null)
                        {
                            this.spDangKy.SPCollection.Insert(i.Position, f.SPDetail);
                        }
                    }
                    else
                    {
                        this.spDangKy.LoadSPCollection();
                        dgList.DataSource = this.spDangKy.SPCollection;
                    }
                    dgList.Refetch();
                }
                break;
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSend":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        this.SendV3();
                    else
                        this.send();
                    break;
                case "cmdAddExcel":
                    this.AddSanPhamFromExcel();
                    break;
                case "XacNhan":
                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        this.FeedBackV3();
                    else
                        this.LaySoTiepNhanDT();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
            }
        }
        private void inPhieuTN()
        {
            if (this.spDangKy.SoTiepNhan == 0) return;
            Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
            phieuTN.phieu = "SẢN PHẨM";
            phieuTN.soTN = this.spDangKy.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.spDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void LaySoTiepNhanDT()
        {
            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spDangKy.ID;
                if (!sendXML.Load())
                {
                    // ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_SEN03", "", false);
                    return;
                }
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                xmlCurrent = spDangKy.LayPhanHoi(password, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    // string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }

                if (sendXML.func == 1)
                {
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + spDangKy.SoTiepNhan, false);
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + spDangKy.SoTiepNhan, "MSG_SEN05", "" + spDangKy.SoTiepNhan, false);
                    txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    else
                        lblTrangThai.Text = "Wait to approve";
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //  if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = spDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                                hd.TrangThai = spDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                            {
                                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                dgList.AllowDelete = InheritableBoolean.True; ;
                                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                sendXML.Delete();
                            }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void SetCommandStatus()
        {
            if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = InheritableBoolean.False;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (GlobalSettings.NGON_NGU == "0")
                    lblTrangThai.Text = "Chờ duyệt";
                else
                    lblTrangThai.Text = "Wait to approve";

            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = InheritableBoolean.True;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }

        }
        private void LayPhanHoi(string pass)
        {
            // Wait.StartWait();
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "SP";
                sendXML.master_id = spDangKy.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = spDangKy.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                //Wait.Close();
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    // string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    //  ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + spDangKy.SoTiepNhan, false);
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + spDangKy.SoTiepNhan, "MSG_SEN05", "" + spDangKy.SoTiepNhan, false);
                    txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    else
                        lblTrangThai.Text = "Wait to approve";

                }

                //xoa thông tin msg nay trong database
                //sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = spDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                                hd.TrangThai = spDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            dgList.AllowDelete = InheritableBoolean.True;
                            cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách NPL. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void AddSanPhamFromExcel()
        {
            SanPhamReadExcelForm f = new SanPhamReadExcelForm();
            f.SPCollection = this.spDangKy.SPCollection;
            f.ShowDialog(this);
            dgList.DataSource = this.spDangKy.SPCollection;
            dgList.Refetch();
        }



        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
            #region Old
            //{
            //    // if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            //    if (MLMessages("Bạn có muốn xóa sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
            //    {
            //        GridEXSelectedItemCollection items = dgList.SelectedItems;
            //        foreach (GridEXSelectedItem i in items)
            //        {
            //            if (i.RowType == RowType.Record)
            //            {
            //                SanPham spSelected = (SanPham)i.GetRow().DataRow;
            //                // Thực hiện xóa trong CSDL.
            //                if (spSelected.ID > 0)
            //                {
            //                    spSelected.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //    }
            //}
            #endregion Old

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void XoaSanPham(GridEXSelectedItemCollection items)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            List<SanPham> sanphams = new List<SanPham>();
            List<GridEXSelectedItem> itemRemove = new List<GridEXSelectedItem>();
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    SanPham sp = (SanPham)i.GetRow().DataRow;
                    sanphams.Add(sp);
                    msgWarning += string.Format("Mã ={0} [Xóa]\r\n", sp.Ma);
                }
            }
            try
            {
                msgWarning += "Bạn có đồng ý xóa không?";
                if (Globals.ShowMessage(msgWarning, true) != "Yes") return;

                foreach (SanPham item in sanphams)
                {
                    if (item.ID > 0 && item.CloneToDB(null))
                        item.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                }
                foreach (SanPham item in sanphams)
                {
                    spDangKy.SPCollection.Remove(item);
                }

            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void Delete()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaSanPham(items);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
            //SanPhamCollection SPCollectionTMP = new SanPhamCollection();
            //if (MLMessages("Bạn có muốn xóa sản phẩm này không?", "", "MSG_DEL01", true) == "Yes")
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            SanPham spSelected = (SanPham)i.GetRow().DataRow;
            //            // Thực hiện xóa trong CSDL.
            //            if (spSelected.ID > 0)
            //            {
            //                spSelected.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
            //                SPCollectionTMP.Add(spSelected);
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    return;
            //}
            //foreach (SanPham sp in SPCollectionTMP)
            //{
            //    spDangKy.SPCollection.Remove(sp);
            //}
            //try { dgList.Refetch(); }
            //catch { dgList.Refresh(); }
        }
        //--------------------------------------------------------------------------------
        #region Send V3 Create by LANNT
        private void SendV3()
        {
            if (spDangKy.ID == 0)
            {
                this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                return;
            }
            if (this.spDangKy.SPCollection.Count == 0)
            {
                // ShowMessage("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                return;
            }
            this.spDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN.Trim();
            this.spDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI.Trim();
            this.spDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY.Trim();
            try
            {
                spDangKy.GUIDSTR = Guid.NewGuid().ToString();
               SXXK_SanPham sanpham = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_DNCX_SP(spDangKy, txtGhiChu.Text,Convert.ToInt16(cbLoaiChungTu.SelectedValue));
                //string msgSend =Helpers.BuildSend(
                //               newNameBase()
                //               {
                //                   Name = GlobalSettings.TEN_DON_VI,
                //                   Identity = spDangKy.MaDoanhNghiep,
                //               }
                //                 , newNameBase()
                //                 {
                //                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spDangKy.MaHaiQuan),
                //                     Identity = spDangKy.MaHaiQuan,
                //                 }
                //              ,
                //                newSubjectBase()
                //                {
                //                    Type =DeclarationIssuer.SXXK_DANHMUC_SP,
                //                    Function =DeclarationFunction.KHAI_BAO,
                //                    Reference = spDangKy.GUIDSTR,
                //                }
                //                ,
                //                sanpham, null, true);
                ObjectSend msgSend = new ObjectSend(new NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = spDangKy.MaDoanhNghiep,
                               },
                                new NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spDangKy.MaHaiQuan),
                                     Identity = spDangKy.MaHaiQuan,
                                 },
                                  new SubjectBase()
                                {
                                    Type =cbLoaiChungTu.SelectedValue.ToString(),
                                    Function =DeclarationFunction.KHAI_BAO,
                                    Reference = spDangKy.GUIDSTR,
                                },
                                sanpham);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(spDangKy.ID,MessageTitle.KhaiBaoSanPham);
                    cmdSend.Enabled = cmdAdd.Enabled = cmdAddExcel.Enabled = cmdAddExcel1.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    MsgSend sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.SanPham;
                    sendXML.func = 1;
                    sendXML.master_id = spDangKy.ID;
                    //sendXML.msg = msgSend;
                    sendXML.InsertUpdate();
                    spDangKy.TransgferDataToSXXK();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }

        private void FeedBackV3()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.SanPham;
            sendXML.master_id = spDangKy.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = spDangKy.GUIDSTR;

               SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_SP,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_SP,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = spDangKy.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spDangKy.MaHaiQuan.Trim()),
                                                  Identity = spDangKy.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    //if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    //{
                    //    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    //    {
                    //        isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                    //        ShowMessageTQDT(msgInfor, false);
                    //    }
                    //    count--;
                    //}
                    //else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || spDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    //{
                    //    ShowMessageTQDT(msgInfor, false);
                    //    isFeedBack = false;
                    //}
                    //else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    //else isFeedBack = false;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) SetCommand();
                }

            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            {
                feedbackContent = SingleMessage.SanPhamSendHandler(spDangKy, ref msgInfor, e);
            }
        }

        #endregion
    }
}