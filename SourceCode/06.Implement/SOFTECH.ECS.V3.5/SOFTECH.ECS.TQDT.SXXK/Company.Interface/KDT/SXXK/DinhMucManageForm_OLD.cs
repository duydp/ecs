﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using System.Drawing;
using System.Data;
using Company.Interface.Report.GC.TT39;
using Company.Interface.KDT.GC;


namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucManageForm_OLD : BaseForm
    {
        /// <summary>
        /// Dùng cho kết quả tìm kiếm.
        /// </summary>
        private DinhMucDangKyCollection dmDangKyCollection = new DinhMucDangKyCollection();
        private DinhMucDangKyCollection tmpCollection = new DinhMucDangKyCollection();
        DataTable dtDinhMucAll = new DataTable();
        DinhMuc dm = new DinhMuc();

        /// <summary>
        /// Thông tin sản phẩm đang dược chọn.
        /// </summary>
        /// 
        private readonly DinhMucDangKy currentDMDangKy = new DinhMucDangKy();
        private string xmlCurrent = "";
        private DinhMucDangKy dmdk = new DinhMucDangKy();
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public DataTable dtDinhMuc;
        public DataTable dt;
        public DinhMucManageForm_OLD()
        {
            this.InitializeComponent();
        }
        private int CheckNPLVaSPDuyet(DinhMucCollection collection, string mahaiquan)
        {
            foreach (DinhMuc dm in collection)
            {
                BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.Ma = dm.MaNguyenPhuLieu;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = mahaiquan;
                if (!npl.Load())
                    return 0;
                BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = dm.MaSanPham;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = mahaiquan;
                if (!sp.Load())
                    return 1;
            }
            return 2;
        }

        //-----------------------------------------------------------------------------------------
        private void DinhMucManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //Deactive event search
                ctrCoQuanHQ.ValueChanged -= new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(btnSearch_Click);
                txtSoTiepNhan.TextChanged -= new EventHandler(btnSearch_Click);
                txtNamTiepNhan.TextChanged -= new EventHandler(btnSearch_Click);
                txtMaSP.TextChanged -= new EventHandler(btnSearch_Click);
                txtMaNPL.TextChanged -= new EventHandler(btnSearch_Click);
                cbStatus.SelectedValueChanged -= new EventHandler(btnSearch_Click);

                this.cbStatus.SelectedIndex = 0;
                this.khoitao_DuLieuChuan();
                txtNamTiepNhan.Text = DateTime.Now.Year.ToString();
                if (ckbTimKiem.Checked)
                    grbtimkiem.Enabled = true;
                else
                    grbtimkiem.Enabled = false;

                //Active event search
                ctrCoQuanHQ.ValueChanged += new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(btnSearch_Click);
                //txtSoTiepNhan.TextChanged += new EventHandler(btnSearch_Click);
                //txtNamTiepNhan.TextChanged += new EventHandler(btnSearch_Click);
                //txtMaSP.TextChanged += new EventHandler(btnSearch_Click);
                //txtMaNPL.TextChanged += new EventHandler(btnSearch_Click);
                cbStatus.SelectedValueChanged += new EventHandler(btnSearch_Click);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            setCommandStatusNew();
            saveFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            try
            {
                // Đơn vị tính.
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                // Đơn vị Hải quan.
                this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
                txtNamTiepNhan.Value = DateTime.Today.Year;
                ctrCoQuanHQ.Ma = GlobalSettings.MA_HAI_QUAN;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        //-----------------------------------------------------------------------------------------      
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string masanpham = txtMaSP.Text;
                string manpl = txtMaNPL.Text;
                if (masanpham == "" && manpl == "")
                {
                    this.search();
                }
                else
                    this.search_Ma(masanpham, manpl);
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                this.Cursor = Cursors.WaitCursor;

                if (this.cbStatus.SelectedValue == null)
                    return;

                string where = " MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", ctrCoQuanHQ.Ma);

                if (this.txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + this.txtSoTiepNhan.Value + "%'";
                }

                if (Convert.ToInt32(this.cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (this.txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + this.txtNamTiepNhan.Value;
                    }
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }

                where += " AND TrangThaiXuLy = " + this.cbStatus.SelectedValue;

                // Thực hiện tìm kiếm.
                this.dmDangKyCollection = DinhMucDangKy.SelectCollectionDynamic(where, "ID Desc");
                this.dgList.DataSource = this.dmDangKyCollection;
                try
                {
                    dtDinhMucAll = new DataTable();
                    dtDinhMucAll = DinhMucDangKy.SelectDynamicAll(where, "ID DESC").Tables[0];
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                setCommandStatusNew();
                this.currentDMDangKy.TrangThaiXuLy = Convert.ToInt32(this.cbStatus.SelectedValue);
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        //Phiph----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu. theo ma SP vaNPL
        /// </summary>
        private void search_Ma(string masanpham, string manpl)
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                this.Cursor = Cursors.WaitCursor;
                string where = " MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                if (!String.IsNullOrEmpty(masanpham))
                    where = " Where DM.MaSanPham like '%" + masanpham + "%'";
                if (!String.IsNullOrEmpty(manpl))
                    where += " and DM.MaNguyenPhuLieu like '%" + manpl + "%'";

                where += string.Format(" AND MaHaiQuan = '{0}'", ctrCoQuanHQ.Ma);


                if (this.txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + this.txtSoTiepNhan.Value + "%'";
                }

                if (Convert.ToInt32(this.cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (this.txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + this.txtNamTiepNhan.Value;
                    }
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                where += " AND  TrangThaiXuLy = " + this.cbStatus.SelectedValue;
                // thuc hien tim kiem
                this.dmDangKyCollection = DinhMucDangKy.SelectCollectionMaSanPham(where, "ID Desc");
                this.dgList.DataSource = this.dmDangKyCollection;
                try
                {
                    dtDinhMucAll = new DataTable();
                    dtDinhMucAll = DinhMucDangKy.SelectDynamicAll(where, "ID DESC").Tables[0];
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                setCommandStatusNew();

                this.currentDMDangKy.TrangThaiXuLy = Convert.ToInt32(this.cbStatus.SelectedValue);
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void setCommandStatusNew()
        {
            if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                //cmdSend1.Enabled = cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdSingleDownload1.Enabled = cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCSDaDuyet1.Enabled = cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                mnuCSDaDuyet.Enabled = false;
                //cmdCancel.Enabled = cmdCancel1.Enabled = InheritableBoolean.False;
                //cmdCancel.Visible = cmdCancel1.Visible = InheritableBoolean.False;
                //XacNhan.Enabled = XacNhan1.Enabled = InheritableBoolean.False;
                //XacNhan.Visible = XacNhan1.Visible = InheritableBoolean.False;
                //HuyCTMenu.Enabled = false;
                //HuyCTMenu.Visible = false;
                ////XacNhan.Visible = XacNhan1.Visible = InheritableBoolean.False;
                //khaibaoCTMenu.Enabled = false;
                //khaibaoCTMenu.Visible = false;
                //NhanDuLieuCTMenu.Enabled = false;
                //NhanDuLieuCTMenu.Visible = false;
                //xacnhanToolStripMenuItem.Enabled = false;
                //xacnhanToolStripMenuItem.Visible = false;
                PhieuTNMenuItem.Enabled = false;
                PhieuTNMenuItem.Visible = false;
            }
            else
            {
                //    cmdSend1.Enabled = cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //    cmdSingleDownload1.Enabled = cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCSDaDuyet1.Enabled = cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = true;
                //cmdCancel.Visible = cmdCancel1.Visible = InheritableBoolean.False;
                //XacNhan.Enabled = XacNhan1.Enabled = InheritableBoolean.False;
                ////XacNhan.Visible = XacNhan1.Visible = InheritableBoolean.False;
                //HuyCTMenu.Enabled = false;
                //HuyCTMenu.Visible = false;
                ////XacNhan.Visible = XacNhan1.Visible = InheritableBoolean.False;
                //khaibaoCTMenu.Enabled = false;
                //khaibaoCTMenu.Visible = false;
                //NhanDuLieuCTMenu.Enabled = false;
                //NhanDuLieuCTMenu.Visible = false;
                //xacnhanToolStripMenuItem.Enabled = false;
                //xacnhanToolStripMenuItem.Visible = false;
                PhieuTNMenuItem.Enabled = false;
                PhieuTNMenuItem.Visible = false;
                cmdXoa.Enabled = InheritableBoolean.True;
                XoaToolStripMenuItem.Enabled = true;
            }

        }
        private void setCommandStatus()
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            DongBoDuLieu.Enabled = InheritableBoolean.True;
            if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET || Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.CHO_HUY)
            {
                InheritableBoolean cho_duyet = Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET ? InheritableBoolean.True : InheritableBoolean.False;
                this.dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                this.dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                //this.cmdSend.Enabled = cmdSend1.Enabled = InheritableBoolean.False;
                //this.cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = InheritableBoolean.True;
                //this.cmdCancel.Enabled = cmdCancel1.Enabled = cho_duyet;
                this.cmdXoa.Enabled = cmdXoa.Enabled = InheritableBoolean.False;
                XoaToolStripMenuItem.Enabled = false;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = true;
                cmdXuatDinhMucChoPhongKhai.Enabled = InheritableBoolean.False;
                XacNhan.Enabled = XacNhan.Enabled = InheritableBoolean.True;
                //khaibaoCTMenu.Enabled = false;
                //NhanDuLieuCTMenu.Enabled = true;
                //HuyCTMenu.Enabled = true;
                PhieuTNMenuItem.Enabled = true;
                btnDelete.Enabled = false;

            }
            else if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                this.dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                this.dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                //this.cmdSend.Enabled = cmdSend1.Enabled = InheritableBoolean.False;
                //this.cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = InheritableBoolean.False;
                //this.cmdCancel.Enabled = cmdCancel1.Enabled = InheritableBoolean.False;
                this.cmdXoa.Enabled = InheritableBoolean.False;
                XoaToolStripMenuItem.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan.Enabled = InheritableBoolean.True;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = false;
                cmdXuatDinhMucChoPhongKhai.Enabled = InheritableBoolean.False;
                //khaibaoCTMenu.Enabled = false;
                //NhanDuLieuCTMenu.Enabled = false;
                //HuyCTMenu.Enabled = false;
                InPhieuTN.Enabled = InheritableBoolean.True;
                PhieuTNMenuItem.Enabled = true;
                //btnSuaDM.Enabled = true;
                btnDelete.Enabled = false;
            }
            else if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO ||
                Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.DA_HUY ||
                 Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                this.dgList.RootTable.Columns["SoTiepNhan"].Visible = false;
                this.dgList.RootTable.Columns["NgayTiepNhan"].Visible = false;
                //this.cmdSend.Enabled = cmdSend1.Enabled = InheritableBoolean.True;
                //this.cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = InheritableBoolean.True;
                //this.cmdCancel.Enabled = cmdCancel1.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                this.cmdXoa.Enabled = InheritableBoolean.True;
                XoaToolStripMenuItem.Enabled = true;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = true;
                cmdXuatDinhMucChoPhongKhai.Enabled = InheritableBoolean.True;
                XacNhan.Enabled = XacNhan.Enabled = InheritableBoolean.True;
                //khaibaoCTMenu.Enabled = true;
                //NhanDuLieuCTMenu.Enabled = true;
                //HuyCTMenu.Enabled = false;
                InPhieuTN.Enabled = InheritableBoolean.False;
                PhieuTNMenuItem.Enabled = false;
                btnDelete.Enabled = true;
                //btnSuaDM.Enabled = false;
            }
            //else if (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET)
            //{
            //    this.dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
            //    this.dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
            //    this.cmdSend.Enabled = cmdSend1.Enabled = InheritableBoolean.True;
            //    this.cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = InheritableBoolean.False;
            //    this.cmdCancel.Enabled = cmdCancel1.Enabled = InheritableBoolean.False;
            //    this.cmdXoa.Enabled = InheritableBoolean.False;
            //    XoaToolStripMenuItem.Enabled = false;
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            //    cmdCSDaDuyet.Enabled = InheritableBoolean.False;
            //    mnuCSDaDuyet.Enabled = false;
            //    cmdXuatDinhMucChoPhongKhai.Enabled = InheritableBoolean.False;
            //    khaibaoCTMenu.Enabled = true;
            //    NhanDuLieuCTMenu.Enabled = true;
            //    HuyCTMenu.Enabled = true;
            //    InPhieuTN.Enabled = InheritableBoolean.True;
            //    PhieuTNMenuItem.Enabled = true;
            //    btnDelete.Enabled = true;
            //    btnSuaDM.Enabled = false;
            //}

            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)))
                {
                    uiCommandBar1.Visible = false;
                    this.cmMain.SetContextMenu(dgList, null);
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    DongBoDuLieu.Enabled = InheritableBoolean.False;
                }
            }
            //NhanDuLieuCTMenu.Enabled = true;
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["TenHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString());
                    switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                    {

                        case -2:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt có sửa chữa";
                            else
                                e.Row.Cells["TrangThaiXuLy"].Text = "Wait to approve(can update)";
                            break;
                        case -1:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            else
                                e.Row.Cells["TrangThaiXuLy"].Text = "Not declarate yet";
                            break;
                        case 0:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            else
                                e.Row.Cells["TrangThaiXuLy"].Text = "Wait to approve ";
                            break;
                        case 1:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            else
                                e.Row.Cells["TrangThaiXuLy"].Text = "Approved";
                            break;
                        case 2:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            else
                                e.Row.Cells["TrangThaiXuLy"].Text = " Not Approved";
                            break;
                        case 11:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ Hủy";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Wait for canceled";

                            }
                            break;
                        case 10:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã Hủy";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Canceled";

                            }
                            break;
                        case -3:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                            }
                            break;
                        case -4:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo hủy";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                            }
                            break;
                        case 4:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo sửa";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                            }
                            break;
                        case 5:
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đang sửa";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Sended";

                            }
                            break;

                    }

                    //TODO: Cao Hữu Tú updated:15-09-2011
                    //Contents: bổ sung cột mã sản phẩm vào Grid Định mức đăng ký
                    //Methods: dựa vào ID trong dmDangKyCollection so sánh với master_id trong  DinhMucCollection
                    //         lấy ra mã sản phẩm và gán vào cột Mã Sản Phẩm

                    Company.BLL.KDT.SXXK.DinhMucCollection DinhMucCollection = new DinhMucCollection();
                    int ValueIdCell = Int32.Parse(e.Row.Cells["ID"].Value.ToString());
                    string TatCaMaSanPham = "";

                    DinhMucCollection = Company.BLL.KDT.SXXK.DinhMuc.SelectCollectionBy_Master_ID(ValueIdCell);
                    Company.BLL.KDT.SXXK.DinhMuc EntityDinhMuc = new DinhMuc();
                    if (DinhMucCollection.Count > 0)
                    {


                        //lấy mã đầu tiên trong DinhMucCollection đưa vào Cột MaSanPham
                        EntityDinhMuc = DinhMucCollection[0];
                        e.Row.Cells["MaSanPham"].Text = EntityDinhMuc.MaSanPham;

                        //lấy mã sản phẩm bắt đầu từ phần tử thứ 2 đưa vào 
                        foreach (Company.BLL.KDT.SXXK.DinhMuc EntityDM in DinhMucCollection)
                        {

                            TatCaMaSanPham = TatCaMaSanPham + EntityDM.MaSanPham + "\n";

                        }
                        e.Row.Cells["MaSanPham"].ToolTipText = TatCaMaSanPham;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "DinhMuc":
                    this.InDinhMuc();
                    break;
                case "InDM":
                    this.InDinhMuc();
                    break;
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "Delete":
                    this.Delete();
                    break;
                case "cmdCSDaDuyet":
                    this.ChuyenTrangThai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdExportExcel":
                    btnExportExcel_Click(null, null);
                    break;
            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (dgList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
                phieuTNForm.TenPhieu = "SẢN PHẨM";
                Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
                string[,] arrPhieuTN = new string[items.Count, 2];
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                        arrPhieuTN[j, 0] = dmDangKySelected.SoTiepNhan.ToString();
                        arrPhieuTN[j, 1] = dmDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                        j++;
                    }
                }
                phieuTNForm.phieuTN = arrPhieuTN;
                phieuTNForm.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void ChuyenTrangThai()
        {
            try
            {
                if (dgList.SelectedItems.Count > 0)
                {
                    DinhMucDangKyCollection dmDKColl = new DinhMucDangKyCollection();
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        dmDKColl.Add((DinhMucDangKy)grItem.GetRow().DataRow);
                    }
                    string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN CHUYỂN DANH SÁCH KHAI BÁO ĐỊNH MỨC NÀY SANG ĐÃ DUYỆT KHÔNG?\n";
                    string msgAccept = "";
                    for (int i = 0; i < dmDKColl.Count; i++)
                    {
                        if (dmDKColl[i].TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            dmDKColl[i].LoadDMCollection();
                            msgAccept += "[" + (i + 1) + "]-[" + dmDKColl[i].ID + "]-[" + dmDKColl[i].SoTiepNhan + "]-[" + dmDKColl[i].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(dmDKColl[i].TrangThaiXuLy) + "]\n";
                        }
                    }
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgAccept + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", msgWarning, true) == "Yes")
                    {
                        foreach (DinhMucDangKy item in dmDKColl)
                        {
                            item.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            item.DeleteDinhMucSXXK();
                            item.TransferDataToSXXK();
                        }
                    }
                    ShowMessage("CHUYỂN TRẠNG THÁI THÀNH CÔNG ", false);
                    btnSearch_Click(null, null);
                }
                else
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "CHƯA CÓ DỮ LIỆU ĐƯỢC CHỌN", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void XoaDinhMucDangKy(GridEXSelectedItemCollection items)
        {

            try
            {
                if (items.Count == 0) return;
                List<DinhMucDangKy> itemsDelete = new List<DinhMucDangKy>();
                int k = 1;
                string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN XÓA DANH SÁCH KHAI BÁO ĐỊNH MỨC NÀY KHÔNG ?\n";
                string msgDelete = "";
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMucDangKy dmDangKy = (DinhMucDangKy)i.GetRow().DataRow;
                        dmDangKy.LoadDMCollection();
                        msgDelete += "[" + (k) + "]-[" + dmDangKy.ID + "]-[" + dmDangKy.SoTiepNhan + "]-[" + dmDangKy.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(dmDangKy.TrangThaiXuLy).ToUpper() + "]\n";
                        k++;
                        itemsDelete.Add(dmDangKy);
                    }
                }
                if (itemsDelete.Count >= 1)
                {
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgDelete + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", msgWarning, true) == "Yes")
                    {
                        foreach (DinhMucDangKy item in itemsDelete)
                        {
                            item.DeleteDinhMucSXXK();
                            foreach (DinhMuc itemss in item.DMCollection)
                            {
                                itemss.Delete();
                            }
                            item.Delete();
                        }
                    }
                    btnSearch_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Delete()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaDinhMucDangKy(items);
            btnSearch_Click(null, null);
        }

        private string checkDataHangImport(DinhMucDangKy dmDangky)
        {
            string st = "";
            foreach (DinhMuc dm in dmDangky.DMCollection)
            {
                BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                ttdm.MaSanPham = dm.MaSanPham;
                ttdm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (ttdm.Load())
                {
                    if (dm.STTHang == 1)
                        st = "Danh sách có ID='" + dmDangky.ID + "'\n";
                    st += "Sản phẩm có mã '" + dm.MaSanPham + "' đã có định mức trong hệ thống.\n";
                }
            }
            return st;
        }
        private string checkDataImport(DinhMucDangKyCollection collection)
        {
            string st = "";
            foreach (DinhMucDangKy dmDangky in collection)
            {
                DinhMucDangKy dmInDatabase = DinhMucDangKy.Load(dmDangky.ID);
                if (dmInDatabase != null)
                {
                    if (dmInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        st += "Danh sách có ID=" + dmDangky.ID + " đã được duyệt.\n";
                    }
                    else
                    {
                        string tmp = checkDataHangImport(dmDangky);
                        st += tmp;
                        if (tmp == "")
                            tmpCollection.Add(dmDangky);
                    }
                }
                else
                {
                    if (dmDangky.ID > 0)
                        dmDangky.ID = 0;
                    tmpCollection.Add(dmDangky);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                tmpCollection.Clear();
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    DinhMucDangKyCollection dmDKCollection = (DinhMucDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(dmDKCollection);
                    if (st != "")
                    {
                        if (MLMessages("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "MSG_PUB04", "", true) == "Yes")
                        {
                            DinhMucDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                            MLMessages("Import thành công", "MSG_PUB02", "", false);
                        }
                    }
                    else
                    {
                        DinhMucDangKy.DongBoDuLieuPhongKhai(tmpCollection);

                        MLMessages("Import thành công", "MSG_PUB02", "", false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                MLMessages("Chưa chọn danh sách định mức", "MSG_DMC01", "", false);
                return;
            }
            try
            {
                DinhMucDangKyCollection col = new DinhMucDangKyCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                            dmDangKySelected.LoadDMCollection();
                            col.Add(dmDangKySelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void InDinhMuc()
        {
            DinhMucDangKy dmDangKy = new DinhMucDangKy();
            if (dgList.GetRow() != null)
            {
                dmDangKy = (DinhMucDangKy)dgList.GetRow().DataRow;
            }
            else
                return;
            Report.ReportViewDinhMucForm f = new Company.Interface.Report.ReportViewDinhMucForm();
            f.DMDangKy = dmDangKy;
            f.ShowDialog(this);
        }
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                //minhnd 06/11/2014
                long ID = (long)Convert.ToInt64(e.Row.Cells["ID"].Text);
                long SoTiepNhan = (long)Convert.ToInt64(e.Row.Cells["SoTiepNhan"].Text);
                Form[] forms = this.ParentForm.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("DM" + ID.ToString()))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                DinhMucDangKy dmdk = new DinhMucDangKy();
                dmdk.ID = ID;
                dmdk.SoTiepNhan = SoTiepNhan;
                dmdk.Load();
                dmdk.LoadDMCollection();
                DinhMucSendForm_OLD dmform = new DinhMucSendForm_OLD();
                dmform.dmDangKy = dmdk;
                dmform.Name = "DM" + ID.ToString();
                dmform.ShowDialog();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void DinhMucManageForm_Shown(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (e.Row.RowType == RowType.Record)
                {
                    DinhMucDangKy dmDangKySelected = (DinhMucDangKy)e.Row.DataRow;
                    dmDangKySelected.LoadDMCollection();
                    if (dmDangKySelected.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                    {
                        if (MLMessages("DOANH NGHIỆP CÓ MUỐN XÓA THÔNG TIN ĐĂNG KÝ NÀY VÀ CÁC ĐỊNH MỨC LIÊN QUAN KHÔNG?", "MSG_DEL01", "", true) == "Yes")
                        {
                            dmDangKySelected.DeleteDinhMucSXXK();
                            foreach (DinhMuc item in dmDangKySelected.DMCollection)
                            {
                                item.Delete();
                            }
                            dmDangKySelected.Delete();
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        MLMessages("DANH SÁCH ĐÃ ĐƯỢC DUYỆT. KHÔNG ĐƯỢC XÓA", "MSG_SEN12", "", false);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                {
                    if (MLMessages("DOANH NGHIỆP CÓ MUỐN XÓA THÔNG TIN ĐĂNG KÝ NÀY VÀ CÁC ĐỊNH MỨC LIÊN QUAN KHÔNG?", "MSG_DEL01", "", true) == "Yes")
                    {
                        GridEXSelectedItemCollection items = dgList.SelectedItems;
                        foreach (GridEXSelectedItem i in items)
                        {
                            if (i.RowType == RowType.Record)
                            {
                                DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                                dmDangKySelected.LoadDMCollection();
                                if (dmDangKySelected.ID > 0)
                                {
                                    dmDangKySelected.DeleteDinhMucSXXK();
                                    foreach (DinhMuc item in dmDangKySelected.DMCollection)
                                    {
                                        item.Delete();
                                    }
                                    dmDangKySelected.Delete();
                                }
                            }
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                    btnSearch_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void DinhMucMenuItem_Click(object sender, EventArgs e)
        {
            InDinhMuc();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void xóaĐịnhMứcToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgList_DeletingRecords(null, null);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            dgList_DeletingRecords(null, null);
        }

        private void mnuCSDaDuyet_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangThai();
        }

        private void uiCommandBar1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void PhieuTNMenuItem_Click(object sender, EventArgs e)
        {
            this.inPhieuTN();
        }

        private void DinhMucMenuItem_Click_1(object sender, EventArgs e)
        {
            InDinhMuc();
        }

        private void btnSuaDM_Click(object sender, EventArgs e)
        {
            try
            {
                DinhMucSuaListForm f = new DinhMucSuaListForm();
                DinhMucDangKy dinhmucdk = (DinhMucDangKy)dgList.GetRow().DataRow;
                f.IDDM = dinhmucdk.ID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void dgList_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void mnuSuaDinhMuc_Click(object sender, EventArgs e)
        {
            btnSuaDM_Click(null, null);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = "DANH SÁCH ĐỊNH MỨC_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sfNPL.Filter = "Excel files| *.xls";
            if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XUẤT KÈM THEO THÔNG TIN ĐỊNH MỨC KHÔNG ? ", true) == "No")
            {
                if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                {

                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    try
                    {
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            else
            {
                if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
                {
                    try
                    {
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TenSanPham", ColumnType.Text, EditType.NoEdit) { Caption = "TÊN SẢN PHẨM" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DVT_SP", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MaNPL", ColumnType.Text, EditType.NoEdit) { Caption = "MÃ NPL" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TenNPL", ColumnType.Text, EditType.NoEdit) { Caption = "TÊN NPL" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DVT", ColumnType.Text, EditType.NoEdit) { Caption = "ĐƠN VỊ TÍNH" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DinhMucSuDung", ColumnType.Text, EditType.NoEdit) { Caption = "ĐỊNH MỨC SỬ DỤNG" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TyLeHaoHut", ColumnType.Text, EditType.NoEdit) { Caption = "TỶ LỆ HAO HỤT" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DinhMucChung", ColumnType.Text, EditType.NoEdit) { Caption = "ĐỊNH MỨC KỂ CẢ HAO HỤT" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MuaVN", ColumnType.Text, EditType.NoEdit) { Caption = "MUA VN" });
                        dgList.DataSource = dtDinhMucAll;
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        try
                        {
                            System.IO.Stream str = sfNPL.OpenFile();
                            gridEXExporter1.Export(str);
                            str.Close();
                            if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                            {
                                System.Diagnostics.Process.Start(sfNPL.FileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                        dgList.Tables[0].Columns.Remove("TenSanPham");
                        dgList.Tables[0].Columns.Remove("DVT_SP");
                        dgList.Tables[0].Columns.Remove("MaNPL");
                        dgList.Tables[0].Columns.Remove("TenNPL");
                        dgList.Tables[0].Columns.Remove("DVT");
                        dgList.Tables[0].Columns.Remove("TyLeHaoHut");
                        dgList.Tables[0].Columns.Remove("DinhMucChung");
                        dgList.Tables[0].Columns.Remove("DinhMucSuDung");
                        dgList.Tables[0].Columns.Remove("MuaVN");
                        btnSearch_Click(null, null);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);

                    }

                }
            }

        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void btnReportĐMTT_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<DinhMucDangKy> DinhMucDangKyCollection = new List<DinhMucDangKy>();
                if (items.Count < 0) return;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        DinhMucDangKy dm = (DinhMucDangKy)row.GetRow().DataRow;
                        DinhMucDangKyCollection.Add(dm);
                    }
                }
                List<DinhMuc> DinhMucCollection = new List<DinhMuc>();
                Company.BLL.SXXK.NguyenPhuLieuCollection NPLCollection = Company.BLL.SXXK.NguyenPhuLieu.SelectCollectionDynamicBy("MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'", "");
                Company.BLL.SXXK.SanPhamCollection SPCollecrion = Company.BLL.SXXK.SanPham.SelectCollectionDynamicBy("MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'", "");
                foreach (DinhMucDangKy item in DinhMucDangKyCollection)
                {
                    item.LoadDMCollection();
                    foreach (DinhMuc dm in item.DMCollection)
                    {
                        foreach (Company.BLL.SXXK.SanPham SP in SPCollecrion)
                        {
                            if (SP.Ma == dm.MaSanPham)
                            {
                                dm.TenSP = SP.Ten;
                                dm.DVT = SP.DVT_ID;
                            }
                        }
                        foreach (Company.BLL.SXXK.NguyenPhuLieu NPL in NPLCollection)
                        {
                            if (NPL.Ma == dm.MaNguyenPhuLieu)
                            {
                                dm.TenNPL = NPL.Ten;
                                dm.DVT_ID = NPL.DVT_ID;
                            }
                        }
                        dm.GhiChu = "Cấp số TN : " + item.SoTiepNhan + " \nNgày : " + item.NgayTiepNhan.ToString("dd/MM/yyyy");
                        DinhMucCollection.Add(dm);
                    }
                }
                Report_16_ĐMTT_GSQL f = new Report_16_ĐMTT_GSQL();
                f.BindReport(DinhMucCollection, clcTuNgay.Value, ucCalendar1.Value);
                f.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<DinhMucDangKy> DinhMucDangKyCollection = new List<DinhMucDangKy>();
                if (items.Count < 0) return;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        DinhMucDangKy dm = (DinhMucDangKy)row.GetRow().DataRow;
                        DinhMucDangKyCollection.Add(dm);
                    }
                }
                List<DinhMuc> DinhMucCollection = new List<DinhMuc>();
                Company.BLL.SXXK.NguyenPhuLieuCollection NPLCollection = Company.BLL.SXXK.NguyenPhuLieu.SelectCollectionDynamicBy("MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'", "");
                Company.BLL.SXXK.SanPhamCollection SPCollecrion = Company.BLL.SXXK.SanPham.SelectCollectionDynamicBy("MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'", "");
                foreach (DinhMucDangKy item in DinhMucDangKyCollection)
                {
                    item.LoadDMCollection();
                    foreach (DinhMuc dm in item.DMCollection)
                    {
                        foreach (Company.BLL.SXXK.SanPham SP in SPCollecrion)
                        {
                            if (SP.Ma == dm.MaSanPham)
                            {
                                dm.TenSP = SP.Ten;
                                dm.DVT = SP.DVT_ID;
                            }
                        }
                        foreach (Company.BLL.SXXK.NguyenPhuLieu NPL in NPLCollection)
                        {
                            if (NPL.Ma == dm.MaNguyenPhuLieu)
                            {
                                dm.TenNPL = NPL.Ten;
                                dm.DVT_ID = NPL.DVT_ID;
                            }
                        }
                        dm.GhiChu = "Cấp số TN : " + item.SoTiepNhan + " \nNgày : " + item.NgayTiepNhan.ToString("dd/MM/yyyy");
                        DinhMucCollection.Add(dm);
                    }
                }
                Company.Interface.Report.SXXK.TT38.ReportExport_ĐM f = new Company.Interface.Report.SXXK.TT38.ReportExport_ĐM();
                f.BindReport(DinhMucCollection, clcTuNgay.Value, ucCalendar1.Value);
                f.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void mnuUpdateStatus_Click(object sender, EventArgs e)
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        DinhMucDangKyCollection dmdkColl = new DinhMucDangKyCollection();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            dmdkColl.Add((DinhMucDangKy)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < dmdkColl.Count; i++)
                        {
                            dmdkColl[i].LoadDMCollection();
                            UpdateStatusForm f = new UpdateStatusForm();
                            f.DMDK = dmdkColl[i];
                            f.formType = "DM";
                            f.ShowDialog(this);
                        }
                        this.btnSearch_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAutoSend_Click(object sender, EventArgs e)
        {
            AutoSendAndFeedbackXMLForm f = new AutoSendAndFeedbackXMLForm();
            f.Type = "DM";
            f.ShowDialog(this);
        }
    }

}
