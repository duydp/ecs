using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.IO;
namespace KDTService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class KDTService : System.Web.Services.WebService
    {


        [WebMethod]
        public string Send(string MsgXML, string Authentication)
        {

            Helpers.SaveMessage(MsgXML, Authentication);
            WS.KDTService service = new WS.KDTService();
            service.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            string ret = service.Send(MsgXML, Authentication);
            Helpers.SaveMessage(ret, Authentication);
            return ret;
        }
        [WebMethod]
        public string Request(string MsgXML, string Authentication)
        {
            Helpers.SaveMessage(MsgXML, Authentication);
            WS.KDTService service = new WS.KDTService();
            service.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            string ret = service.Request(MsgXML, Authentication);
            Helpers.SaveMessage(ret, Authentication);
            return ret;
        }
    }
}
