﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.BLL.Utils;
using System.Threading;
using Company.BLL.KDT.SXXK;
using System.Collections;
using System.Text;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using DuLieuChuan = Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.BLL.KDT
{
    public partial class ToKhaiMauDich
    {
        //HungTQ Update 30/12/2010. Su dung cho dong bo du lieu Mabuchi
        protected string _TenDaiLyKhaiBao = String.Empty;
        protected int _TrangThaiDongBoTaiLen = 0;
        protected int _TrangThaiDongBoTaiXuong = 0;

        //Thong Bao Thue By DATLMQ 24/01/2011
        public bool IsThongBaoThue = false;
        public string SoQD = "";
        public DateTime NgayQD = new DateTime();
        public DateTime NgayHetHan = new DateTime();
        public string TaiKhoanKhoBac = "";
        public string TenKhoBac = "";
        //Thue XNK
        public string ChuongThueXNK = "";
        public string LoaiThueXNK = "";
        public string KhoanThueXNK = "";
        public string MucThueXNK = "";
        public string TieuMucThueXNK = "";
        public double SoTienThueXNK = 0;
        //Thue VAT
        public string ChuongThueVAT = "";
        public string LoaiThueVAT = "";
        public string KhoanThueVAT = "";
        public string MucThueVAT = "";
        public string TieuMucThueVAT = "";
        public double SoTienThueVAT = 0;
        //Thue TTDB
        public string ChuongThueTTDB = "";
        public string LoaiThueTTDB = "";
        public string KhoanThueTTDB = "";
        public string MucThueTTDB = "";
        public string TieuMucThueTTDB = "";
        public double SoTienThueTTDB = 0;
        //Thue TVCBPG
        public string ChuongThueTVCBPG = "";
        public string LoaiThueTVCBPG = "";
        public string KhoanThueTVCBPG = "";
        public string MucThueTVCBPG = "";
        public string TieuMucThueTVCBPG = "";
        public double SoTienThueTVCBPG = 0;

        /// <summary>
        /// HungTQ Update 30/12/2010. Su dung cho dong bo du lieu Mabuchi
        /// </summary>
        public string TenDaiLyKhaiBao
        {
            set { _TenDaiLyKhaiBao = value; }
            get { return _TenDaiLyKhaiBao; }
        }

        /// <summary>
        /// 1: Thanh cong; 0: Khong thanh cong.
        /// </summary>
        public int TrangThaiDongBoTaiLen
        {
            set { _TrangThaiDongBoTaiLen = value; }
            get { return _TrangThaiDongBoTaiLen; }
        }

        /// <summary>
        /// 1: Thanh cong; 0: Khong thanh cong.
        /// </summary>
        public int TrangThaiDongBoTaiXuong
        {
            set { _TrangThaiDongBoTaiXuong = value; }
            get { return _TrangThaiDongBoTaiXuong; }
        }
        private List<HangMauDich> _HMDCollection = new List<HangMauDich>();
        private ChungTuCollection _chungtuCollection = new ChungTuCollection();

        private List<CO> _ListCO = new List<CO>();
        public List<ChungTuKem> listCTDK = new List<ChungTuKem>();
        //Hungtq updated 24/11/2011.
        private Company.BLL.SXXK.DinhMucCollection _DinhMucColecction = new Company.BLL.SXXK.DinhMucCollection();
        private Company.BLL.SXXK.NguyenPhuLieuCollection _NguyenPhuLieuColecction = new Company.BLL.SXXK.NguyenPhuLieuCollection();
        private Company.BLL.SXXK.SanPhamCollection _SanPhamColecction = new Company.BLL.SXXK.SanPhamCollection();
        private ThongTinDinhMucCollection _ThongTinDinhMucCollections = new ThongTinDinhMucCollection();
        public Company.BLL.SXXK.NguyenPhuLieuCollection NguyenPhuLieuColecction
        {
            get { return _NguyenPhuLieuColecction; }
            set { _NguyenPhuLieuColecction = value; }
        }
        List<GiayNopTien> _giayNopTiens = new List<GiayNopTien>();
        public List<GiayNopTien> GiayNopTiens { get { return _giayNopTiens; } set { _giayNopTiens = value; } }
        public void LoadListGiayNopTiens()
        {
            _giayNopTiens = GiayNopTien.SelectCollectionDynamic("TKMD_ID=" + ID, "");
        }
        public Company.BLL.SXXK.SanPhamCollection SanPhamColecction
        {
            get { return _SanPhamColecction; }
            set { _SanPhamColecction = value; }
        }

        public Company.BLL.SXXK.DinhMucCollection DinhMucColecction
        {
            get { return _DinhMucColecction; }
            set { _DinhMucColecction = value; }
        }
        public ThongTinDinhMucCollection ThongTinDinhMucCollection
        {
            get
            {
                return _ThongTinDinhMucCollections;
            }
            set
            {
                _ThongTinDinhMucCollections = value;
            }
        }

        public bool IsToKhaiDaThanhKhoanVaDongHoSo()
        {
            return TheoDoiTrangThaiThanhKhoanTKN(this.MaDoanhNghiep, this.SoToKhai, this.MaLoaiHinh, this.NgayDangKy.Year, Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo);
        }

        public bool IsToKhaiDaThanhKhoanVaDongHoSo(SqlTransaction transaction, SqlDatabase db)
        {
            return TheoDoiTrangThaiThanhKhoanTKN(transaction, db, this.MaDoanhNghiep, this.SoToKhai, this.MaLoaiHinh, this.NgayDangKy.Year, Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaDongHoSo);
        }

        public bool IsToKhaiDangThanhKhoan()
        {
            return TheoDoiTrangThaiThanhKhoanTKN(this.MaDoanhNghiep, this.SoToKhai, this.MaLoaiHinh, this.NgayDangKy.Year, Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaChayThanhKhoan);
        }

        public bool IsToKhaiDangThanhKhoan(SqlTransaction transaction, SqlDatabase db)
        {
            return TheoDoiTrangThaiThanhKhoanTKN(transaction, db, this.MaDoanhNghiep, this.SoToKhai, this.MaLoaiHinh, this.NgayDangKy.Year, Company.KDT.SHARE.Components.TrangThaiThanhKhoan.DaChayThanhKhoan);
        }

        private bool TheoDoiTrangThaiThanhKhoanTKN(string maDoanhNghiep, long soToKhai, string maLoaiHinh, int namDangKy, Company.KDT.SHARE.Components.TrangThaiThanhKhoan trangThaiThanhKhoan)
        {
            string spName = "p_KDT_ToKhaimauDich_TheoDoiThanhKhoanTKN";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, soToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, maLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namDangKy);
            db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, (int)trangThaiThanhKhoan);

            object result = db.ExecuteScalar(dbCommand);

            return result != null ? (System.Convert.ToInt32(result) > 0 ? true : false) : false;
        }

        private bool TheoDoiTrangThaiThanhKhoanTKN(SqlTransaction transaction, SqlDatabase db, string maDoanhNghiep, long soToKhai, string maLoaiHinh, int namDangKy, Company.KDT.SHARE.Components.TrangThaiThanhKhoan trangThaiThanhKhoan)
        {
            string spName = "p_KDT_ToKhaimauDich_TheoDoiThanhKhoanTKN";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, soToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, maLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namDangKy);
            db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, (int)trangThaiThanhKhoan);

            object result = db.ExecuteScalar(dbCommand, transaction);

            return result != null ? (System.Convert.ToInt32(result) > 0 ? true : false) : false;
        }

        public static bool IsExists(string mahaiQuan, string maDoanhNghiep, int soToKhai, int namDK, string maLoaiHinh)
        {
            string sql = string.Format("Select count(*) from t_kdt_ToKhaiMauDich Where MaHaiQuan = '{0}' AND MaDoanhNghiep = '{1}' AND SoToKhai = {2} AND MaLoaiHinh = '{3}' AND NamDK = {4}", mahaiQuan, maDoanhNghiep, soToKhai, maLoaiHinh, namDK);

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            else
                return Convert.ToInt32(o) > 0;
        }

        public bool Load(string mahaiQuan, string maDoanhNghiep, int soToKhai, int namDK, string maLoaiHinh)
        {
            string spName = "p_KDT_ToKhaiMauDich_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, mahaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, namDK);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, maLoaiHinh);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) this._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) this._HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) this._NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool Load(string GUIDSTR)
        {
            string spName = "p_KDT_ToKhaiMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            string whereCondition = string.Format("[t_KDT_ToKhaiMauDich].GUIDSTR = '{0}'", GUIDSTR);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) this._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) this._HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) this._NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public static ToKhaiMauDich Load(string mahaiQuan, string maDoanhNghiep, int soToKhai, int namDK, string maLoaiHinh, int trangThaiXuLy, string connectionString)
        {
            SqlDatabase db = new SqlDatabase(connectionString);
            string spName = "p_KDT_ToKhaiMauDich_LoadBy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, mahaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, namDK);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, maLoaiHinh);

            ToKhaiMauDich entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity._HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity._ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity._NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }

        public static ToKhaiMauDich Load(string GUIDSTR, string connectionString)
        {
            SqlDatabase db = new SqlDatabase(connectionString);

            string spName = "p_KDT_ToKhaiMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            string whereCondition = string.Format("[t_KDT_ToKhaiMauDich].GUIDSTR = '{0}'", GUIDSTR);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            ToKhaiMauDich entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity._HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity._ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity._NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }

        public bool LoadByDaiLy(string mahaiQuan, string maDoanhNghiep, int soToKhai, int namDK, string maLoaiHinh)
        {
            string spName = "p_KDT_ToKhaiMauDich_Load_ByDaiLy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, mahaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, namDK);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, maLoaiHinh);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) this._ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) this._HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) this._NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                //HungTQ Update 30/12/2010. Su dung cho dong bo du lieu Mabuchi
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyKhaiBao"))) this.TenDaiLyKhaiBao = reader.GetString(reader.GetOrdinal("TenDaiLyKhaiBao"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public ToKhaiMauDichCollection SelectCollectionByDaiLy(string maDoanhNghiep, DateTime tuNgay, DateTime denNgay)
        {
            string whereCondition = string.Format("[t_KDT_ToKhaiMauDich].MaDoanhNghiep = '{0}' AND NgayDangky BETWEEN '{1}' AND '{2}'", maDoanhNghiep, tuNgay.ToString("MM/dd/yyyy 00:00:00"), denNgay.ToString("MM/dd/yyyy 23:00:00"));

            return SelectCollectionDynamicByDaiLy(whereCondition, "");
        }
        //---------------------------------------------------------------------------------------------

        public ToKhaiMauDichCollection SelectCollectionByDaiLy(string maDoanhNghiep, DateTime tuNgay, DateTime denNgay, int idUserDaiLy)
        {
            string whereCondition = string.Format("[t_KDT_ToKhaiMauDich].MaDoanhNghiep = '{0}' AND NgayDangky BETWEEN '{1}' AND '{2}' AND IDUserDaiLy = {3}", maDoanhNghiep, tuNgay.ToString("MM/dd/yyyy 00:00:00"), denNgay.ToString("MM/dd/yyyy 23:59:59"), idUserDaiLy);

            return SelectCollectionDynamicByDaiLy(whereCondition, "");
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiMauDichCollection SelectCollectionDynamicByDaiLy(string whereCondition, string orderByExpression)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDynamicByDaiLy(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                //HungTQ Update 30/12/2010. Su dung cho dong bo du lieu Mabuchi
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyKhaiBao"))) entity.TenDaiLyKhaiBao = reader.GetString(reader.GetOrdinal("TenDaiLyKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiDongBoTaiLen"))) entity.TrangThaiDongBoTaiLen = reader.GetInt32(reader.GetOrdinal("TrangThaiDongBoTaiLen"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiDongBoTaiXuong"))) entity.TrangThaiDongBoTaiXuong = reader.GetInt32(reader.GetOrdinal("TrangThaiDongBoTaiXuong"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        private IDataReader SelectReaderDynamicByDaiLy(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_ToKhaiMauDich_SelectDynamic_ByDaiLy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataTable GetContainerInfo(long tkmd)
        {
            string sql = " SELECT  distinct   dbo.t_KDT_Container.SoHieu, dbo.t_KDT_Container.LoaiContainer, dbo.t_KDT_Container.Seal_No, dbo.t_KDT_ToKhaiMauDich.NgayDangKy, "
                     + " dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep, dbo.t_KDT_ToKhaiMauDich.SoToKhai, "
                     + " dbo.t_KDT_VanDon.NgayVanDon, dbo.t_KDT_VanDon.SoHieuChuyenDi "
                     + " FROM         dbo.t_KDT_Container INNER JOIN "
                     + " dbo.t_KDT_VanDon ON dbo.t_KDT_Container.VanDon_ID = dbo.t_KDT_VanDon.ID inner join dbo.t_KDT_ToKhaiMauDich ON "
                     + " dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_VanDon.TKMD_ID "
                     + " where dbo.t_KDT_VanDon.TKMD_ID = " + tkmd;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public ToKhaiMauDichCollection searchTKMDByUserName(string userName, string where)
        {
            string query = "SELECT * FROM dbo.t_KDT_ToKhaiMauDich tkmd INNER JOIN dbo.t_KDT_SXXK_LogKhaiBao lg ON lg.ID_DK = tkmd.ID " +
                           "WHERE lg.UserNameKhaiBao = '" + userName + "' AND LoaiKhaiBao = 'TK' AND ";
            if (!where.Equals(""))
                query += where;
            System.Data.Common.DbCommand dbCommand = (System.Data.Common.DbCommand)this.db.GetSqlStringCommand(query);
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public List<HangMauDich> HMDCollection
        {
            set { this._HMDCollection = value; }
            get { return this._HMDCollection; }
        }

        public ChungTuCollection ChungTuTKCollection
        {
            set { this._chungtuCollection = value; }
            get { return this._chungtuCollection; }
        }

        public void LoadChungTuKem()
        {
            this.listCTDK = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
        }

        public IDataReader SelectReaderDynamic(string sql, DateTime from, DateTime to)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@From", SqlDbType.DateTime, from);
            db.AddInParameter(dbCommand, "@To", SqlDbType.DateTime, to);

            return db.ExecuteReader(dbCommand);
        }
        public static long SelectCountSoTK(string madoanhnghiep)
        {
            string sql = "select count(*) from t_KDT_ToKhaiMauDich where MaDoanhNghiep='" + madoanhnghiep + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return 0;
            else
                return Convert.ToInt64(o);
        }
        //-----------------------------------------------------------------------------------------
        public ToKhaiMauDichCollection SelectCollectionDynamic(string sql, DateTime from, DateTime to)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDynamic(sql, from, to);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public void LoadHMDCollection()
        {
            try
            {
                //HangMauDich hmd = new HangMauDich();
                //hmd.TKMD_ID = this.ID;
                //this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);

                LoadNguyenPhuLieuCollection();

                LoadSanPhamCollection();

                LoadDinhMucCollection();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public void LoadNguyenPhuLieuCollection()
        {
            try
            {
                //Neu la TK Nhap & co thong tin hang thi thuc hien
                if (!this.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    return;

                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();

                //Duyet qua cac Hang cua TK 
                foreach (HangMauDich hmd in this.HMDCollection)
                {
                    //Lay NPL
                    npl = Company.BLL.SXXK.NguyenPhuLieu.getNguyenPhuLieu(this.MaHaiQuan, this.MaDoanhNghiep, hmd.MaPhu);
                    if (npl == null) continue;
                    if (NguyenPhuLieuColecction.Count != 0)
                        for (int i = 0; i < NguyenPhuLieuColecction.Count; i++)
                        {
                            //Kiem tra da ton tai MaNPL trong danh sach.
                            if (!IsExistsNPL(NguyenPhuLieuColecction, npl.Ma))
                            {
                                //Them NPL vao danh sach
                                NguyenPhuLieuColecction.Add(npl);
                            }
                        }
                    else NguyenPhuLieuColecction.Add(npl);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public void LoadSanPhamCollection()
        {
            try
            {
                //Neu la TK Xuat & co thong tin hang thi thuc hien
                if (!this.MaLoaiHinh.Substring(0, 1).Equals("X"))
                    return;

                Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();

                //Duyet qua cac Hang cua TK 
                foreach (HangMauDich hmd in this.HMDCollection)
                {
                    //Lay SP
                    sp = Company.BLL.SXXK.SanPham.getSanPham(this.MaHaiQuan, this.MaDoanhNghiep, hmd.MaPhu);
                    if (sp == null) continue;
                    if (SanPhamColecction.Count != 0)
                        for (int i = 0; i < SanPhamColecction.Count; i++)
                        {
                            //Kiem tra da ton tai MaSP trong danh sach.
                            if (!IsExistsSP(SanPhamColecction, sp.Ma))
                            {
                                //Them SP vao danh sach
                                SanPhamColecction.Add(sp);
                            }
                        }
                    else SanPhamColecction.Add(sp);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public void LoadDinhMucCollection()
        {
            try
            {
                if (this.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    return;

                Company.BLL.SXXK.DinhMuc dm = new Company.BLL.SXXK.DinhMuc();
                Company.BLL.SXXK.DinhMucCollection dmCollections = new Company.BLL.SXXK.DinhMucCollection();

                //Duyet qua cac San pham cua TK Xuat
                foreach (HangMauDich hmd in this.HMDCollection)
                {
                    //Lay DM theo ma SP cua TK xuat
                    dmCollections = new Company.BLL.SXXK.DinhMucCollection();
                    dmCollections = dm.getSanPhamDinhMucCollection(this.MaHaiQuan, this.MaDoanhNghiep, hmd.MaPhu);

                    for (int i = 0; i < dmCollections.Count; i++)
                    {
                        //Kiem tra da ton tai MaSP, MaNPL trong danh sach.
                        if (!IsExistsDinhMuc(_DinhMucColecction, dmCollections[i].MaSanPHam, dmCollections[i].MaNguyenPhuLieu))
                        {
                            //Them dinh muc vao danh sach

                            ThongTinDinhMuc thongtindinhmuc = new ThongTinDinhMuc();

                            thongtindinhmuc.MaDoanhNghiep = this.MaDoanhNghiep;
                            thongtindinhmuc.MaHaiQuan = this.MaHaiQuan;
                            thongtindinhmuc.MaSanPham = dmCollections[i].MaSanPHam;

                            if (thongtindinhmuc.Load())
                            {
                                _ThongTinDinhMucCollections.Add(thongtindinhmuc);
                                _DinhMucColecction.Add(dmCollections[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public bool IsExistsNPL(Company.BLL.SXXK.NguyenPhuLieuCollection collections, string maNPL)
        {
            foreach (Company.BLL.SXXK.NguyenPhuLieu item in collections)
            {
                if (item.Ma.Trim() == maNPL.Trim())
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsExistsSP(Company.BLL.SXXK.SanPhamCollection collections, string maSP)
        {
            foreach (Company.BLL.SXXK.SanPham item in collections)
            {
                if (item.Ma.Trim() == maSP.Trim())
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsExistsDinhMuc(Company.BLL.SXXK.DinhMucCollection collections, string maSP, string maNPL)
        {
            foreach (Company.BLL.SXXK.DinhMuc dm in collections)
            {
                if (dm.MaSanPHam.Trim() == maSP.Trim() && dm.MaNguyenPhuLieu.Trim() == maNPL.Trim())
                {
                    return true;
                }
            }

            return false;
        }

        public void LoadHMDCollection(SqlTransaction transaction, SqlDatabase db)
        {
            HangMauDich hmd = new HangMauDich();
            hmd.TKMD_ID = this.ID;
            this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID(transaction, db);
        }

        public void LoadChungTuTKCollection()
        {
            ChungTu ct = new ChungTu();
            ct.Master_ID = this.ID;
            this.ChungTuTKCollection = ct.SelectCollectionBy_Master_ID();
        }

        private void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this.ID == 0)
                this.ID = this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);

            foreach (HangMauDich hmdDetail in this.HMDCollection)
            {
                if (hmdDetail.ID == 0)
                {
                    hmdDetail.TKMD_ID = this.ID;
                    hmdDetail.ID = hmdDetail.Insert(transaction);
                }
                else
                {
                    hmdDetail.Update(transaction);
                }
            }
            foreach (ChungTu ct in this.ChungTuTKCollection)
            {
                ct.InsertUpdateTransaction(transaction);
            }
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    int i = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;
                        if (hmd.ID == 0)
                        {
                            hmd.TKMD_ID = this.ID;
                            hmd.ID = hmd.Insert(transaction);
                        }
                        else
                        {
                            hmd.Update(transaction);
                        }
                    }
                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;
                        if (ct.ID == 0)
                        {
                            ct.Master_ID = this.ID;
                            ct.ID = ct.InsertTransaction(transaction);
                        }
                        else
                        {
                            ct.UpdateTransaction(transaction);
                        }
                    }
                    #region Van Tai Don
                    if (VanTaiDon != null)
                    {
                        if (VanTaiDon.ID == 0)
                        {
                            VanTaiDon.TKMD_ID = this.ID;
                            VanTaiDon.ID = VanTaiDon.Insert(transaction);
                        }
                        else
                        {
                            VanTaiDon.Update(transaction);
                        }
                        foreach (Container container in VanTaiDon.ContainerCollection)
                        {
                            container.VanDon_ID = VanTaiDon.ID;
                            if (container.ID == 0)
                                container.Insert(transaction);
                            else
                                container.Update(transaction);
                        }
                        foreach (HangVanDonDetail item in VanTaiDon.ListHangOfVanDon)
                        {
                            item.VanDon_ID = VanTaiDon.ID;
                            if (item.ID == 0)
                                item.Insert(transaction);
                            else item.Update(transaction);
                        }
                    }
                    #endregion Van Tai Don
                    #region Chứng từ nợ
                    if (ChungTuNoCollection != null)
                    {
                        foreach (ChungTuNo item in ChungTuNoCollection)
                        {
                            item.TKMDID = this.ID;
                            if (item.ID == 0)
                                item.Insert(transaction);
                            else item.Update(transaction);
                        }
                    }
                    #endregion
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFull(string connectionString)
        {
            bool ret;
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    #region To khai mau dich

                    ToKhaiMauDich tkmdTemp = null;
                    //Kiem tra neu TKMD co GUID ID thi kiem tra ton tai To khai tren Database Target dua tren GUID.
                    if (this.GUIDSTR.Length != 0)
                        tkmdTemp = ToKhaiMauDich.Load(this.GUIDSTR, connectionString);
                    //Kiem tra ton tai To khai tren Database Source dua tren cap khoa: this._MaHaiQuan, this._MaDoanhNghiep, this._SoToKhai, this._NamDK, this._MaLoaiHinh, this._TrangThaiXuLy 
                    else
                        tkmdTemp = ToKhaiMauDich.Load(this._MaHaiQuan, this._MaDoanhNghiep, this._SoToKhai, this._NgayDangKy.Year, this._MaLoaiHinh, this._TrangThaiXuLy, connectionString);

                    if (tkmdTemp == null || tkmdTemp.ID == 0)
                    {
                        //Comment do xu ly luon chuyen trang thai cho TK trong code
                        //Chuyen trang thai thanh cho duyet cho TK co trang thai = DA DUYET
                        //if (this.TrangThaiXuLy == 1)
                        //    this.TrangThaiXuLy = 0; //Chuyen thanh cho duyet

                        this.ID = this.InsertTransaction(transaction, db);
                    }
                    else
                    {
                        //Kiem tra tk nay da tham gia thanh khoan va da dong ho so chua?. Neu da dong ho so -> khong cap nhat.
                        //Hungtq, updated 06/04/2011.
                        if (!tkmdTemp.IsToKhaiDaThanhKhoanVaDongHoSo(transaction, db))
                        {
                            this._ID = tkmdTemp.ID;
                            this.UpdateTransaction(transaction, db);
                        }
                    }

                    #endregion

                    #region Hang Mau Dich
                    int i = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;

                        hmd.TKMD_ID = this.ID;

                        hmd.InsertUpdateTransactionBy(transaction, db);

                        /* Comment do da co script xu ly thong tin cho van nan nay o duoi CSDL.
                        //Kiem tra tk nay da tham gia thanh khoan va da dong ho so chua?. Neu da dong ho so -> khong cap nhat.
                        //Hungtq, updated 06/04/2011.
                        if ((tkmdTemp != null && tkmdTemp.ID != 0) && !tkmdTemp.IsToKhaiDaThanhKhoanVaDongHoSo(transaction, db))
                        {
                            //Kiem tra da co to khai nay trong bang Dang ky t_SXXK_ToKhaiMauDich chua?. Neu co -> cap nhat thong tin npl ton
                            if (Company.BLL.SXXK.ToKhai.ToKhaiMauDich.LoadBy(transaction, db, this.MaHaiQuan, this.SoToKhai, this.MaLoaiHinh, this.NgayDangKy.Year) != null)
                            {
                                //Cap nhat thong tin NPL thanh ly ton
                                BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                                nplTon.MaHaiQuan = this.MaHaiQuan;
                                nplTon.MaNPL = hmd.MaPhu; ;
                                nplTon.SoToKhai = this.SoToKhai;
                                nplTon.MaLoaiHinh = this.MaLoaiHinh;
                                nplTon.NamDangKy = (short)this.NgayDangKy.Year;
                                nplTon.MaDoanhNghiep = this.MaDoanhNghiep;
                                nplTon.Ton = hmd.SoLuong;
                                nplTon.Luong = hmd.SoLuong;
                                nplTon.ThueXNK = (double)hmd.ThueXNK;
                                nplTon.ThueVAT = (double)hmd.ThueGTGT;
                                nplTon.ThueTTDB = (double)hmd.ThueTTDB;
                                nplTon.TenNPL = hmd.TenHang;
                                nplTon.PhuThu = (double)hmd.PhuThu;
                                nplTon.ThueCLGia = (double)hmd.TriGiaThuKhac;
                                nplTon.ThueXNKTon = nplTon.ThueXNK;
                                nplTon.InsertUpdateTransaction(transaction, db);

                                //Cap nhat thong tin NPL ton thuc te
                                BLL.SXXK.NPLNhapTonThucTe nplTonTT = new Company.BLL.SXXK.NPLNhapTonThucTe();
                                nplTonTT.MaHaiQuan = this.MaHaiQuan;
                                nplTonTT.MaNPL = hmd.MaPhu; ;
                                nplTonTT.SoToKhai = this.SoToKhai;
                                nplTonTT.MaLoaiHinh = this.MaLoaiHinh;
                                nplTonTT.NamDangKy = (short)this.NgayDangKy.Year;
                                nplTonTT.MaDoanhNghiep = this.MaDoanhNghiep;
                                nplTonTT.Luong = hmd.SoLuong;
                                nplTonTT.Ton = nplTon.Ton;
                                nplTonTT.ThueXNK = (double)hmd.ThueXNK;
                                nplTonTT.ThueVAT = (double)hmd.ThueGTGT;
                                nplTonTT.ThueTTDB = (double)hmd.ThueTTDB;
                                nplTonTT.PhuThu = (double)hmd.PhuThu;
                                nplTonTT.ThueCLGia = (double)hmd.TriGiaThuKhac;
                                nplTonTT.ThueXNKTon = nplTon.ThueXNK;
                                nplTonTT.InsertUpdate(transaction, db);
                            }
                        }
                        */
                    }
                    #endregion

                    #region Chung tu
                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;

                        ct.Master_ID = this.ID;

                        ct.InsertUpdateTransactionBy(transaction, db);
                    }
                    #endregion

                    #region Van Tai Don
                    if (this.VanTaiDon != null)
                    {
                        //this.VanTaiDon.TKMD_ID = this.ID;
                        //this.VanTaiDon.InsertUpdateFullTrasaction(transaction, db);
                        this.VanTaiDon.TKMD_ID = this.ID;

                        //Kiem tra ton tai VanTaiDon tren Database Target
                        VanDon vtdTemp = VanDon.Load(this.VanTaiDon.SoVanDon, this.VanTaiDon.NgayVanDon, this.VanTaiDon.TKMD_ID, db);
                        VanDon vdObj = this.VanTaiDon;
                        vdObj.TKMD_ID = this.ID;
                        if (vtdTemp == null || vtdTemp.ID == 0)
                            vdObj.ID = vdObj.Insert(transaction, db);
                        else
                        {
                            vdObj.ID = vtdTemp.ID;
                            vdObj.Update(transaction, db);
                        }

                        foreach (Container container in this.VanTaiDon.ContainerCollection)
                        {
                            container.VanDon_ID = this.VanTaiDon.ID;
                            container.InsertUpdateBy(transaction, db);
                        }
                    }
                    #endregion Van Tai Don

                    /*
                     * Chung tu kem khac: 
                     * 1. Giay phep.
                     * 2. Hoa don thuong mai.
                     * 3. Hop dong thuong mai.
                     * 4. CO.
                     * 5. De nghi chuyen cua khau.
                     * 6. Chung tu dang anh.
                     */
                    #region Giay phep
                    foreach (GiayPhep giayPhep in this.GiayPhepCollection)
                    {
                        giayPhep.TKMD_ID = this.ID;

                        //Kiem tra ton tai GiayPhep tren Database Target
                        GiayPhep gpTemp = GiayPhep.Load(giayPhep.MaDoanhNghiep, giayPhep.SoGiayPhep, giayPhep.NgayGiayPhep, giayPhep.NgayHetHan, giayPhep.TKMD_ID, db);

                        if (gpTemp == null || gpTemp.ID == 0)
                            giayPhep.ID = giayPhep.InsertTransaction(transaction, db);
                        else
                        {
                            giayPhep.ID = gpTemp.ID;
                            giayPhep.UpdateTransaction(transaction, db);
                        }

                        foreach (HangGiayPhepDetail hangGP in giayPhep.ListHMDofGiayPhep)
                        {
                            hangGP.GiayPhep_ID = giayPhep.ID;

                            HangGiayPhepDetail.DeleteBy_GiayPhep_ID(hangGP.GiayPhep_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(giayPhep.TKMD_ID, hangGP.MaHS, hangGP.MaPhu, hangGP.TenHang, hangGP.NuocXX_ID, hangGP.DVT_ID, hangGP.SoLuong, (decimal)hangGP.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                hangGP.HMD_ID = hmd.ID;

                                hangGP.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region Hoa don thuong mai
                    foreach (HoaDonThuongMai hoaDon in this.HoaDonThuongMaiCollection)
                    {
                        hoaDon.TKMD_ID = this.ID;

                        //Kiem tra ton tai HoaDonThuongMai tren Database Target
                        HoaDonThuongMai hoaDonTemp = HoaDonThuongMai.Load(hoaDon.MaDoanhNghiep, hoaDon.SoHoaDon, hoaDon.NgayHoaDon, hoaDon.TKMD_ID, transaction, db);

                        if (hoaDonTemp == null || hoaDonTemp.ID == 0)
                            hoaDon.ID = hoaDon.InsertTransaction(transaction, db);
                        else
                        {
                            hoaDon.ID = hoaDonTemp.ID;
                            hoaDon.UpdateTransaction(transaction, db);
                        }

                        foreach (HoaDonThuongMaiDetail hoaDonDetail in hoaDon.ListHangMDOfHoaDon)
                        {
                            hoaDonDetail.HoaDonTM_ID = hoaDon.ID;

                            HoaDonThuongMaiDetail.DeleteBy_HoaDonTM_ID(hoaDonDetail.HoaDonTM_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(hoaDon.TKMD_ID, hoaDonDetail.MaHS, hoaDonDetail.MaPhu, hoaDonDetail.TenHang, hoaDonDetail.NuocXX_ID, hoaDonDetail.DVT_ID, hoaDonDetail.SoLuong, (decimal)hoaDonDetail.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                hoaDonDetail.HMD_ID = hmd.ID;
                                hoaDonDetail.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region Hop dong thuong mai
                    foreach (HopDongThuongMai hopDong in this.HopDongThuongMaiCollection)
                    {
                        hopDong.TKMD_ID = this.ID;

                        //Kiem tra ton tai HopDongThuongMai tren Database Target
                        HopDongThuongMai hopDongTemp = HopDongThuongMai.Load(hopDong.MaDoanhNghiep, hopDong.SoHopDongTM, hopDong.NgayHopDongTM, hopDong.ThoiHanThanhToan, hopDong.TKMD_ID, transaction, db);

                        if (hopDongTemp == null || hopDongTemp.ID == 0)
                            hopDong.ID = hopDong.InsertTransaction(transaction, db);
                        else
                        {
                            hopDong.ID = hopDongTemp.ID;
                            hopDong.UpdateTransaction(transaction, db);
                        }

                        foreach (HopDongThuongMaiDetail hopDongDetail in hopDong.ListHangMDOfHopDong)
                        {
                            hopDongDetail.HopDongTM_ID = hopDong.ID;

                            HopDongThuongMaiDetail.DeleteBy_HopDongTM_ID(hopDongDetail.HopDongTM_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(hopDong.TKMD_ID, hopDongDetail.MaHS, hopDongDetail.MaPhu, hopDongDetail.TenHang, hopDongDetail.NuocXX_ID, hopDongDetail.DVT_ID, hopDongDetail.SoLuong, (decimal)hopDongDetail.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                hopDongDetail.HMD_ID = hmd.ID;
                                hopDongDetail.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region CO
                    foreach (CO co in this.COCollection)
                    {
                        co.TKMD_ID = this.ID;

                        //Kiem tra ton tai CO tren Database Target
                        CO coTemp = CO.Load(co.MaDoanhNghiep, co.SoCO, co.NgayCO, co.LoaiCO, co.TKMD_ID, transaction, db);

                        if (coTemp == null || coTemp.ID == 0)
                            co.ID = co.InsertTransaction(transaction, db);
                        else
                        {
                            co.ID = coTemp.ID;
                            co.UpdateTransaction(transaction, db);
                        }
                    }
                    #endregion

                    #region De nghi chuyen cua khau
                    foreach (DeNghiChuyenCuaKhau chuyenCuaKhau in this.listChuyenCuaKhau)
                    {
                        chuyenCuaKhau.TKMD_ID = this.ID;

                        DeNghiChuyenCuaKhau.DeleteBy_TKMD_ID(chuyenCuaKhau.TKMD_ID, db);

                        chuyenCuaKhau.ID = chuyenCuaKhau.InsertTransaction(transaction, db);
                    }
                    #endregion

                    #region Chung tu dang anh
                    foreach (ChungTuKem ctk in this.ChungTuKemCollection)
                    {
                        ctk.TKMDID = this.ID;

                        //Kiem tra ton tai HopDongThuongMai tren Database Target
                        ChungTuKem ctkTemp = ChungTuKem.Load(ctk.SO_CT, ctk.NGAY_CT, ctk.MA_LOAI_CT, ctk.TKMDID, transaction, db);

                        if (ctkTemp == null || ctkTemp.ID == 0)
                            ctk.ID = ctk.InsertTransaction(transaction, db);
                        else
                        {
                            ctk.ID = ctkTemp.ID;
                            ctk.UpdateTransaction(transaction, db);
                        }

                        foreach (ChungTuKemChiTiet ctkDetail in ctk.listCTChiTiet)
                        {
                            ctkDetail.ChungTuKemID = ctk.ID;

                            ChungTuKemChiTiet.DeleteBy_ChungTuKemID(ctkDetail.ChungTuKemID, transaction, db);

                            ctkDetail.InsertTransaction(transaction, db);
                        }
                    }
                    #endregion

                    // Va Noi dung chinh sua to khai.
                    #region Noi dung dieu chinh to khai
                    //Xoa chi tiet con
                    NoiDungDieuChinhTKDetail.DeleteBy_Id_DieuChinh(transaction, this.ID, db);
                    //Xoa cha
                    NoiDungDieuChinhTK.DeleteByTKMD(transaction, this.ID, db);

                    foreach (NoiDungDieuChinhTK dieuChinh in this.NoiDungDieuChinhTKCollection)
                    {
                        dieuChinh.TKMD_ID = this.ID;

                        dieuChinh.ID = dieuChinh.Insert(transaction, db);

                        foreach (NoiDungDieuChinhTKDetail detail in dieuChinh.listNDDCTKChiTiet)
                        {
                            detail.Id_DieuChinh = dieuChinh.ID;

                            detail.Insert(transaction, db);
                        }
                    }
                    #endregion

                    //Ket qua xu ly to khai
                    #region Ket qua xy ly
                    KetQuaXuLy.DeleteBy_TKMD_ID(transaction, this.ID, db);

                    foreach (KetQuaXuLy log in this.KetQuaXuLyCollection)
                    {
                        log.ItemID = this.ID;

                        log.ID = log.InsertUpdate(transaction, db);
                    }
                    #endregion

                    //Thong tin Huy to khai
                    #region Huy to khai
                    HuyToKhai.DeleteBy_TKMD_ID(transaction, this.ID, db);

                    foreach (HuyToKhai obj in this.HuyToKhaiCollection)
                    {
                        obj.TKMD_ID = this.ID;

                        obj.ID = obj.Insert(transaction, db);
                    }
                    #endregion

                    //Thong tin Message send
                    #region Message send
                    if (MessageSend != null && MessageSend.msg.Trim().Length != 0)
                    {
                        MsgSend.DeleteTransaction(transaction, db, this.ID, "TK");

                        MsgSend.InsertTransaction(transaction, db, this.ID, MessageSend.func, MessageSend.LoaiHS, MessageSend.msg);
                    }
                    #endregion

                    //Chuyen trang thai to khai vao Dang ky
                    TransgferDataToSXXK(connectionString);

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFull(string connectionString, int cnt)
        {
            bool ret;
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    #region To khai mau dich

                    ToKhaiMauDich tkmdTemp = null;
                    //Kiem tra neu TKMD co GUID ID thi kiem tra ton tai To khai tren Database Target dua tren GUID.
                    if (this.GUIDSTR.Length != 0)
                        tkmdTemp = ToKhaiMauDich.Load(this.GUIDSTR, connectionString);
                    //Kiem tra ton tai To khai tren Database Source dua tren cap khoa: this._MaHaiQuan, this._MaDoanhNghiep, this._SoToKhai, this._NamDK, this._MaLoaiHinh, this._TrangThaiXuLy 
                    else
                        tkmdTemp = ToKhaiMauDich.Load(this._MaHaiQuan, this._MaDoanhNghiep, this._SoToKhai, this._NgayDangKy.Year, this._MaLoaiHinh, this._TrangThaiXuLy, connectionString);

                    if (tkmdTemp == null || tkmdTemp.ID == 0)
                    {
                        //Comment do xu ly luon chuyen trang thai cho TK trong code
                        //Chuyen trang thai thanh cho duyet cho TK co trang thai = DA DUYET
                        //if (this.TrangThaiXuLy == 1)
                        //    this.TrangThaiXuLy = 0; //Chuyen thanh cho duyet

                        this.ID = this.InsertTransaction(transaction, db);
                    }
                    else
                    {
                        //Kiem tra tk nay da tham gia thanh khoan va da dong ho so chua?. Neu da dong ho so -> khong cap nhat.
                        //Hungtq, updated 06/04/2011.
                        if (!tkmdTemp.IsToKhaiDaThanhKhoanVaDongHoSo(transaction, db))
                        {
                            this._ID = tkmdTemp.ID;
                            this.UpdateTransaction(transaction, db);
                        }
                    }

                    #endregion

                    #region Hang Mau Dich
                    int i = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;

                        hmd.TKMD_ID = this.ID;

                        hmd.InsertUpdateTransactionBy(transaction, db);

                        /* Comment do da co script xu ly thong tin cho van nan nay o duoi CSDL.
                        //Kiem tra tk nay da tham gia thanh khoan va da dong ho so chua?. Neu da dong ho so -> khong cap nhat.
                        //Hungtq, updated 06/04/2011.
                        if ((tkmdTemp != null && tkmdTemp.ID != 0) && !tkmdTemp.IsToKhaiDaThanhKhoanVaDongHoSo(transaction, db))
                        {
                            //Kiem tra da co to khai nay trong bang Dang ky t_SXXK_ToKhaiMauDich chua?. Neu co -> cap nhat thong tin npl ton
                            if (Company.BLL.SXXK.ToKhai.ToKhaiMauDich.LoadBy(transaction, db, this.MaHaiQuan, this.SoToKhai, this.MaLoaiHinh, this.NgayDangKy.Year) != null)
                            {
                                //Cap nhat thong tin NPL thanh ly ton
                                BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                                nplTon.MaHaiQuan = this.MaHaiQuan;
                                nplTon.MaNPL = hmd.MaPhu; ;
                                nplTon.SoToKhai = this.SoToKhai;
                                nplTon.MaLoaiHinh = this.MaLoaiHinh;
                                nplTon.NamDangKy = (short)this.NgayDangKy.Year;
                                nplTon.MaDoanhNghiep = this.MaDoanhNghiep;
                                nplTon.Ton = hmd.SoLuong;
                                nplTon.Luong = hmd.SoLuong;
                                nplTon.ThueXNK = (double)hmd.ThueXNK;
                                nplTon.ThueVAT = (double)hmd.ThueGTGT;
                                nplTon.ThueTTDB = (double)hmd.ThueTTDB;
                                nplTon.TenNPL = hmd.TenHang;
                                nplTon.PhuThu = (double)hmd.PhuThu;
                                nplTon.ThueCLGia = (double)hmd.TriGiaThuKhac;
                                nplTon.ThueXNKTon = nplTon.ThueXNK;
                                nplTon.InsertUpdateTransaction(transaction, db);

                                //Cap nhat thong tin NPL ton thuc te
                                BLL.SXXK.NPLNhapTonThucTe nplTonTT = new Company.BLL.SXXK.NPLNhapTonThucTe();
                                nplTonTT.MaHaiQuan = this.MaHaiQuan;
                                nplTonTT.MaNPL = hmd.MaPhu; ;
                                nplTonTT.SoToKhai = this.SoToKhai;
                                nplTonTT.MaLoaiHinh = this.MaLoaiHinh;
                                nplTonTT.NamDangKy = (short)this.NgayDangKy.Year;
                                nplTonTT.MaDoanhNghiep = this.MaDoanhNghiep;
                                nplTonTT.Luong = hmd.SoLuong;
                                nplTonTT.Ton = nplTon.Ton;
                                nplTonTT.ThueXNK = (double)hmd.ThueXNK;
                                nplTonTT.ThueVAT = (double)hmd.ThueGTGT;
                                nplTonTT.ThueTTDB = (double)hmd.ThueTTDB;
                                nplTonTT.PhuThu = (double)hmd.PhuThu;
                                nplTonTT.ThueCLGia = (double)hmd.TriGiaThuKhac;
                                nplTonTT.ThueXNKTon = nplTon.ThueXNK;
                                nplTonTT.InsertUpdate(transaction, db);
                            }
                        }
                        */
                    }
                    #endregion

                    #region Chung tu
                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;

                        ct.Master_ID = this.ID;

                        ct.InsertUpdateTransactionBy(transaction, db);
                    }
                    #endregion

                    #region Van Tai Don
                    if (this.VanTaiDon != null)
                    {
                        //this.VanTaiDon.TKMD_ID = this.ID;
                        //this.VanTaiDon.InsertUpdateFullTrasaction(transaction, db);
                        this.VanTaiDon.TKMD_ID = this.ID;

                        //Kiem tra ton tai VanTaiDon tren Database Target
                        VanDon vtdTemp = VanDon.Load(this.VanTaiDon.SoVanDon, this.VanTaiDon.NgayVanDon, this.VanTaiDon.TKMD_ID, db);
                        VanDon vdObj = this.VanTaiDon;
                        vdObj.TKMD_ID = this.ID;
                        if (vtdTemp == null || vtdTemp.ID == 0)
                            vdObj.ID = vdObj.Insert(transaction, db);
                        else
                        {
                            vdObj.ID = vtdTemp.ID;
                            vdObj.Update(transaction, db);
                        }

                        foreach (Container container in this.VanTaiDon.ContainerCollection)
                        {
                            container.VanDon_ID = this.VanTaiDon.ID;
                            container.InsertUpdateBy(transaction, db);
                        }
                    }
                    #endregion Van Tai Don

                    /*
                     * Chung tu kem khac: 
                     * 1. Giay phep.
                     * 2. Hoa don thuong mai.
                     * 3. Hop dong thuong mai.
                     * 4. CO.
                     * 5. De nghi chuyen cua khau.
                     * 6. Chung tu dang anh.
                     */
                    #region Giay phep
                    foreach (GiayPhep giayPhep in this.GiayPhepCollection)
                    {
                        giayPhep.TKMD_ID = this.ID;

                        //Kiem tra ton tai GiayPhep tren Database Target
                        GiayPhep gpTemp = GiayPhep.Load(giayPhep.MaDoanhNghiep, giayPhep.SoGiayPhep, giayPhep.NgayGiayPhep, giayPhep.NgayHetHan, giayPhep.TKMD_ID, db);

                        if (gpTemp == null || gpTemp.ID == 0)
                            giayPhep.ID = giayPhep.InsertTransaction(transaction, db);
                        else
                        {
                            giayPhep.ID = gpTemp.ID;
                            giayPhep.UpdateTransaction(transaction, db);
                        }

                        foreach (HangGiayPhepDetail hangGP in giayPhep.ListHMDofGiayPhep)
                        {
                            hangGP.GiayPhep_ID = giayPhep.ID;

                            HangGiayPhepDetail.DeleteBy_GiayPhep_ID(hangGP.GiayPhep_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(giayPhep.TKMD_ID, hangGP.MaHS, hangGP.MaPhu, hangGP.TenHang, hangGP.NuocXX_ID, hangGP.DVT_ID, hangGP.SoLuong, (decimal)hangGP.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                hangGP.HMD_ID = hmd.ID;

                                hangGP.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region Hoa don thuong mai
                    foreach (HoaDonThuongMai hoaDon in this.HoaDonThuongMaiCollection)
                    {
                        hoaDon.TKMD_ID = this.ID;

                        //Kiem tra ton tai HoaDonThuongMai tren Database Target
                        HoaDonThuongMai hoaDonTemp = HoaDonThuongMai.Load(hoaDon.MaDoanhNghiep, hoaDon.SoHoaDon, hoaDon.NgayHoaDon, hoaDon.TKMD_ID, transaction, db);

                        if (hoaDonTemp == null || hoaDonTemp.ID == 0)
                            hoaDon.ID = hoaDon.InsertTransaction(transaction, db);
                        else
                        {
                            hoaDon.ID = hoaDonTemp.ID;
                            hoaDon.UpdateTransaction(transaction, db);
                        }

                        foreach (HoaDonThuongMaiDetail hoaDonDetail in hoaDon.ListHangMDOfHoaDon)
                        {
                            hoaDonDetail.HoaDonTM_ID = hoaDon.ID;

                            HoaDonThuongMaiDetail.DeleteBy_HoaDonTM_ID(hoaDonDetail.HoaDonTM_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(hoaDon.TKMD_ID, hoaDonDetail.MaHS, hoaDonDetail.MaPhu, hoaDonDetail.TenHang, hoaDonDetail.NuocXX_ID, hoaDonDetail.DVT_ID, hoaDonDetail.SoLuong, (decimal)hoaDonDetail.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                hoaDonDetail.HMD_ID = hmd.ID;
                                hoaDonDetail.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region Hop dong thuong mai
                    foreach (HopDongThuongMai hopDong in this.HopDongThuongMaiCollection)
                    {
                        hopDong.TKMD_ID = this.ID;

                        //Kiem tra ton tai HopDongThuongMai tren Database Target
                        HopDongThuongMai hopDongTemp = HopDongThuongMai.Load(hopDong.MaDoanhNghiep, hopDong.SoHopDongTM, hopDong.NgayHopDongTM, hopDong.ThoiHanThanhToan, hopDong.TKMD_ID, transaction, db);

                        if (hopDongTemp == null || hopDongTemp.ID == 0)
                            hopDong.ID = hopDong.InsertTransaction(transaction, db);
                        else
                        {
                            hopDong.ID = hopDongTemp.ID;
                            hopDong.UpdateTransaction(transaction, db);
                        }

                        foreach (HopDongThuongMaiDetail hopDongDetail in hopDong.ListHangMDOfHopDong)
                        {
                            hopDongDetail.HopDongTM_ID = hopDong.ID;

                            HopDongThuongMaiDetail.DeleteBy_HopDongTM_ID(hopDongDetail.HopDongTM_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(hopDong.TKMD_ID, hopDongDetail.MaHS, hopDongDetail.MaPhu, hopDongDetail.TenHang, hopDongDetail.NuocXX_ID, hopDongDetail.DVT_ID, hopDongDetail.SoLuong, (decimal)hopDongDetail.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                hopDongDetail.HMD_ID = hmd.ID;
                                hopDongDetail.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region CO
                    foreach (CO co in this.COCollection)
                    {
                        co.TKMD_ID = this.ID;

                        //Kiem tra ton tai CO tren Database Target
                        CO coTemp = CO.Load(co.MaDoanhNghiep, co.SoCO, co.NgayCO, co.LoaiCO, co.TKMD_ID, transaction, db);

                        if (coTemp == null || coTemp.ID == 0)
                            co.ID = co.InsertTransaction(transaction, db);
                        else
                        {
                            co.ID = coTemp.ID;
                            co.UpdateTransaction(transaction, db);
                        }
                    }
                    #endregion

                    #region De nghi chuyen cua khau
                    foreach (DeNghiChuyenCuaKhau chuyenCuaKhau in this.listChuyenCuaKhau)
                    {
                        chuyenCuaKhau.TKMD_ID = this.ID;

                        DeNghiChuyenCuaKhau.DeleteBy_TKMD_ID(chuyenCuaKhau.TKMD_ID, db);

                        chuyenCuaKhau.ID = chuyenCuaKhau.InsertTransaction(transaction, db);
                    }
                    #endregion

                    #region Chung tu dang anh
                    foreach (ChungTuKem ctk in this.ChungTuKemCollection)
                    {
                        ctk.TKMDID = this.ID;

                        //Kiem tra ton tai HopDongThuongMai tren Database Target
                        ChungTuKem ctkTemp = ChungTuKem.Load(ctk.SO_CT, ctk.NGAY_CT, ctk.MA_LOAI_CT, ctk.TKMDID, transaction, db);

                        if (ctkTemp == null || ctkTemp.ID == 0)
                            ctk.ID = ctk.InsertTransaction(transaction, db);
                        else
                        {
                            ctk.ID = ctkTemp.ID;
                            ctk.UpdateTransaction(transaction, db);
                        }

                        foreach (ChungTuKemChiTiet ctkDetail in ctk.listCTChiTiet)
                        {
                            ctkDetail.ChungTuKemID = ctk.ID;

                            ChungTuKemChiTiet.DeleteBy_ChungTuKemID(ctkDetail.ChungTuKemID, transaction, db);

                            ctkDetail.InsertTransaction(transaction, db);
                        }
                    }
                    #endregion

                    // Va Noi dung chinh sua to khai.
                    #region Noi dung dieu chinh to khai
                    //Xoa chi tiet con
                    NoiDungDieuChinhTKDetail.DeleteBy_Id_DieuChinh(transaction, this.ID, db);
                    //Xoa cha
                    NoiDungDieuChinhTK.DeleteByTKMD(transaction, this.ID, db);

                    foreach (NoiDungDieuChinhTK dieuChinh in this.NoiDungDieuChinhTKCollection)
                    {
                        dieuChinh.TKMD_ID = this.ID;

                        dieuChinh.ID = dieuChinh.Insert(transaction, db);

                        foreach (NoiDungDieuChinhTKDetail detail in dieuChinh.listNDDCTKChiTiet)
                        {
                            detail.Id_DieuChinh = dieuChinh.ID;

                            detail.Insert(transaction, db);
                        }
                    }
                    #endregion

                    //Ket qua xu ly to khai
                    #region Ket qua xy ly
                    KetQuaXuLy.DeleteBy_TKMD_ID(transaction, this.ID, db);

                    foreach (KetQuaXuLy log in this.KetQuaXuLyCollection)
                    {
                        log.ItemID = this.ID;

                        log.ID = log.InsertUpdate(transaction, db);
                    }
                    #endregion

                    //Thong tin Huy to khai
                    #region Huy to khai
                    HuyToKhai.DeleteBy_TKMD_ID(transaction, this.ID, db);

                    foreach (HuyToKhai obj in this.HuyToKhaiCollection)
                    {
                        obj.TKMD_ID = this.ID;

                        obj.ID = obj.Insert(transaction, db);
                    }
                    #endregion

                    //Thong tin Message send
                    #region Message send
                    if (MessageSend != null && MessageSend.msg.Trim().Length != 0)
                    {
                        MsgSend.DeleteTransaction(transaction, db, this.ID, "TK");

                        MsgSend.InsertTransaction(transaction, db, this.ID, MessageSend.func, MessageSend.LoaiHS, MessageSend.msg);
                    }
                    #endregion

                    transaction.Commit();
                    ret = true;

                    //TODO: HungTQ, Update 27/04/2010.
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("{0}.KDT To khai: MaHaiQuan={1}, MaDoanhNghiep={2}, SoToKhai={3}, NgayDangKy={4}, MaLoaiHinh={5}, NamDangKy={6}", cnt, this.MaHaiQuan, this.MaDoanhNghiep, this.SoToKhai, this.NgayDangKy.ToString("dd/MM/yyyy hh:mm:ss tt"), this.MaLoaiHinh, this.NgayDangKy.Year)));

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);

                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFullDaiLy()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    //Kiem tra ID cua TK da co tren he thong. Tao moi 1 object TK de kiem tra vi neu Load thong tin tren doi tuong TK 
                    //dang kiem tra se lam mat thong tin cua TK do neu ko co du lieu.
                    ToKhaiMauDich objTemp = new ToKhaiMauDich();
                    objTemp.Load(MaHaiQuan, MaDoanhNghiep, SoToKhai, NamDK, MaLoaiHinh);

                    if (objTemp.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.ID = objTemp.ID;
                        this.UpdateTransaction(transaction);
                    }

                    #region NguyenPhuLieu, SanPham, Dinh muc

                    new Company.BLL.SXXK.NguyenPhuLieu().InsertUpdate(NguyenPhuLieuColecction, transaction);

                    new Company.BLL.SXXK.SanPham().InsertUpdate(SanPhamColecction, transaction);
                    new Company.BLL.SXXK.ThongTinDinhMuc().InsertUpdateCollections(ThongTinDinhMucCollection, transaction);
                    new Company.BLL.SXXK.DinhMuc().InsertUpdate(this.DinhMucColecction, transaction);

                    #endregion

                    #region Hang Mau Dich
                    int i = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;

                        hmd.TKMD_ID = this.ID;

                        hmd.InsertUpdateTransactionBy(transaction);
                    }
                    #endregion

                    #region Chung tu
                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;
                        if (ct.ID == 0)
                        {
                            ct.Master_ID = this.ID;
                            ct.ID = ct.InsertTransaction(transaction);
                        }
                        else
                        {
                            ct.UpdateTransaction(transaction);
                        }
                    }
                    #endregion

                    #region Van Tai Don
                    if (this.VanTaiDon != null)
                    {
                        //if (this.VanTaiDon.ID == 0)
                        //{
                        //    this.VanTaiDon.TKMD_ID = this.ID;
                        //    this.VanTaiDon.ID = this.VanTaiDon.Insert(transaction);
                        //}
                        //else
                        //{
                        //    this.VanTaiDon.Update(transaction);
                        //}
                        //Container
                        //foreach (Container container in this.VanTaiDon.ContainerCollection)
                        //{
                        //    container.VanDon_ID = this.VanTaiDon.ID;
                        //    if (container.ID == 0)
                        //        container.Insert(transaction);
                        //    else
                        //        container.Update(transaction);
                        //}
                        this.VanTaiDon.TKMD_ID = this.ID;

                        //Kiem tra ton tai VanTaiDon tren Database Target
                        VanDon vtdTemp = VanDon.Load(this.VanTaiDon.SoVanDon, this.VanTaiDon.NgayVanDon, this.VanTaiDon.TKMD_ID, db);
                        VanDon vdObj = this.VanTaiDon;
                        vdObj.TKMD_ID = this.ID;
                        if (vtdTemp == null || vtdTemp.ID == 0)
                            vdObj.ID = vdObj.Insert(transaction, db);
                        else
                        {
                            vdObj.ID = vtdTemp.ID;
                            vdObj.Update(transaction, db);
                        }

                        foreach (Container container in this.VanTaiDon.ContainerCollection)
                        {
                            container.VanDon_ID = this.VanTaiDon.ID;
                            container.InsertUpdateBy(transaction, db);
                        }
                    }
                    #endregion Van Tai Don

                    /*
                     * Chung tu kem khac: 
                     * 1. Giay phep.
                     * 2. Hoa don thuong mai.
                     * 3. Hop dong thuong mai.
                     * 4. CO.
                     * 5. De nghi chuyen cua khau.
                     * 6. Chung tu dang anh.
                     */
                    #region Giay phep
                    foreach (GiayPhep giayPhep in this.GiayPhepCollection)
                    {
                        giayPhep.TKMD_ID = this.ID;

                        if (giayPhep.NgayTiepNhan.Year <= 1900)
                            giayPhep.NgayTiepNhan = new DateTime(1900, 1, 1);

                        //Kiem tra ton tai GiayPhep tren Database Target
                        GiayPhep gpTemp = GiayPhep.Load(giayPhep.MaDoanhNghiep, giayPhep.SoGiayPhep, giayPhep.NgayGiayPhep, giayPhep.NgayHetHan, giayPhep.TKMD_ID, db);

                        if (gpTemp == null || gpTemp.ID == 0)
                            giayPhep.ID = giayPhep.InsertTransaction(transaction, db);
                        else
                        {
                            giayPhep.ID = gpTemp.ID;
                            giayPhep.UpdateTransaction(transaction, db);
                        }

                        foreach (HangGiayPhepDetail hangGP in giayPhep.ListHMDofGiayPhep)
                        {
                            hangGP.GiayPhep_ID = giayPhep.ID;

                            HangGiayPhepDetail.DeleteBy_GiayPhep_ID(hangGP.GiayPhep_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(giayPhep.TKMD_ID, hangGP.MaHS, hangGP.MaPhu, hangGP.TenHang, hangGP.NuocXX_ID, hangGP.DVT_ID, hangGP.SoLuong, (decimal)hangGP.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                hangGP.HMD_ID = hmd.ID;

                                hangGP.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region Hoa don thuong mai
                    foreach (HoaDonThuongMai hoaDon in this.HoaDonThuongMaiCollection)
                    {
                        hoaDon.TKMD_ID = this.ID;

                        if (hoaDon.NgayTiepNhan.Year <= 1900)
                            hoaDon.NgayTiepNhan = new DateTime(1900, 1, 1);

                        //Kiem tra ton tai HoaDonThuongMai tren Database Target
                        HoaDonThuongMai hoaDonTemp = HoaDonThuongMai.Load(hoaDon.MaDoanhNghiep, hoaDon.SoHoaDon, hoaDon.NgayHoaDon, hoaDon.TKMD_ID, transaction, db);

                        if (hoaDonTemp == null || hoaDonTemp.ID == 0)
                            hoaDon.ID = hoaDon.InsertTransaction(transaction, db);
                        else
                        {
                            hoaDon.ID = hoaDonTemp.ID;
                            hoaDon.UpdateTransaction(transaction, db);
                        }

                        foreach (HoaDonThuongMaiDetail hoaDonDetail in hoaDon.ListHangMDOfHoaDon)
                        {
                            hoaDonDetail.HoaDonTM_ID = hoaDon.ID;

                            HoaDonThuongMaiDetail.DeleteBy_HoaDonTM_ID(hoaDonDetail.HoaDonTM_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(hoaDon.TKMD_ID, hoaDonDetail.MaHS, hoaDonDetail.MaPhu, hoaDonDetail.TenHang, hoaDonDetail.NuocXX_ID, hoaDonDetail.DVT_ID, hoaDonDetail.SoLuong, (decimal)hoaDonDetail.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                hoaDonDetail.HMD_ID = hmd.ID;
                                hoaDonDetail.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region Hop dong thuong mai
                    foreach (HopDongThuongMai hopDong in this.HopDongThuongMaiCollection)
                    {
                        hopDong.TKMD_ID = this.ID;

                        if (hopDong.NgayTiepNhan.Year <= 1900)
                            hopDong.NgayTiepNhan = new DateTime(1900, 1, 1);

                        //Kiem tra ton tai HopDongThuongMai tren Database Target
                        HopDongThuongMai hopDongTemp = HopDongThuongMai.Load(hopDong.MaDoanhNghiep, hopDong.SoHopDongTM, hopDong.NgayHopDongTM, hopDong.ThoiHanThanhToan, hopDong.TKMD_ID, transaction, db);

                        if (hopDongTemp == null || hopDongTemp.ID == 0)
                            hopDong.ID = hopDong.InsertTransaction(transaction, db);
                        else
                        {
                            hopDong.ID = hopDongTemp.ID;
                            hopDong.UpdateTransaction(transaction, db);
                        }

                        foreach (HopDongThuongMaiDetail hopDongDetail in hopDong.ListHangMDOfHopDong)
                        {
                            hopDongDetail.HopDongTM_ID = hopDong.ID;

                            HopDongThuongMaiDetail.DeleteBy_HopDongTM_ID(hopDongDetail.HopDongTM_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(hopDong.TKMD_ID, hopDongDetail.MaHS, hopDongDetail.MaPhu, hopDongDetail.TenHang, hopDongDetail.NuocXX_ID, hopDongDetail.DVT_ID, hopDongDetail.SoLuong, (decimal)hopDongDetail.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                hopDongDetail.HMD_ID = hmd.ID;
                                hopDongDetail.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region CO
                    foreach (CO co in this.COCollection)
                    {
                        co.TKMD_ID = this.ID;

                        if (co.NgayTiepNhan.Year <= 1900)
                            co.NgayTiepNhan = new DateTime(1900, 1, 1);

                        //Kiem tra ton tai CO tren Database Target
                        CO coTemp = CO.Load(co.MaDoanhNghiep, co.SoCO, co.NgayCO, co.LoaiCO, co.TKMD_ID, transaction, db);

                        if (coTemp == null || coTemp.ID == 0)
                            co.ID = co.InsertTransaction(transaction, db);
                        else
                        {
                            co.ID = coTemp.ID;
                            co.UpdateTransaction(transaction, db);
                        }

                        foreach (HangCoDetail coDetail in co.ListHMDofCo)
                        {
                            coDetail.Co_ID = co.ID;

                            HangCoDetail.DeleteBy_CO_ID(coDetail.Co_ID, transaction, db);

                            HangMauDich hmd = new HangMauDich().Load(co.TKMD_ID, coDetail.MaHS, coDetail.MaPhu, coDetail.TenHang, coDetail.NuocXX_ID, coDetail.DVT_ID, coDetail.SoLuong, (decimal)coDetail.DonGiaKB, transaction, db);
                            if (hmd != null)
                            {
                                coDetail.HMD_ID = hmd.ID;
                                coDetail.InsertUpdate(transaction, db);
                            }
                        }
                    }
                    #endregion

                    #region De nghi chuyen cua khau
                    foreach (DeNghiChuyenCuaKhau chuyenCuaKhau in this.listChuyenCuaKhau)
                    {
                        chuyenCuaKhau.TKMD_ID = this.ID;

                        DeNghiChuyenCuaKhau.DeleteBy_TKMD_ID(chuyenCuaKhau.TKMD_ID);

                        chuyenCuaKhau.ID = chuyenCuaKhau.InsertTransaction(transaction, db);
                    }
                    #endregion

                    #region Chung tu dang anh
                    foreach (ChungTuKem ctk in this.ChungTuKemCollection)
                    {
                        ctk.TKMDID = this.ID;

                        //Kiem tra ton tai HopDongThuongMai tren Database Target
                        ChungTuKem ctkTemp = ChungTuKem.Load(ctk.SO_CT, ctk.NGAY_CT, ctk.MA_LOAI_CT, ctk.TKMDID, transaction, db);

                        if (ctkTemp == null || ctkTemp.ID == 0)
                            ctk.ID = ctk.InsertTransaction(transaction, db);
                        else
                        {
                            ctk.ID = ctkTemp.ID;
                            ctk.UpdateTransaction(transaction, db);
                        }

                        foreach (ChungTuKemChiTiet ctkDetail in ctk.listCTChiTiet)
                        {
                            ctkDetail.ChungTuKemID = ctk.ID;

                            ChungTuKemChiTiet.DeleteBy_ChungTuKemID(ctkDetail.ChungTuKemID, transaction, db);

                            ctkDetail.InsertTransaction(transaction, db);
                        }
                    }
                    #endregion

                    #region Chung tu No
                    foreach (ChungTuNo chungTuNo in this.ChungTuNoCollection)
                    {
                        chungTuNo.TKMDID = this.ID;

                        ChungTuNo.DeleteDynamic("1=1");

                        chungTuNo.ID = chungTuNo.InsertTransaction(transaction, db);
                    }
                    #endregion

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);

                    transaction.Rollback();
                    this.ID = 0;
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public long InsertFull()
        {
            long ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    long ret01 = this.InsertTransaction(transaction);
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.TKMD_ID = ret01;
                        hmd.Insert(transaction);
                    }
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.Master_ID = ret01;
                        ct.InsertTransaction(transaction);
                    }
                    if (ret01 > 0)
                    {
                        transaction.Commit();
                        ret = ret01;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = 0;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public long UpdateFull()
        {
            long ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    long ret01 = this.UpdateTransaction(transaction);
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        //if (hmd.Load()) hmd.Update(transaction);
                        //else
                        //{
                        //    hmd.TKMD_ID = this.ID;
                        //    hmd.Insert(transaction);
                        //}
                        hmd.InsertUpdate(transaction);

                    }
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.Master_ID = ret01;
                        ct.InsertTransaction(transaction);
                    }
                    if (ret01 > 0)
                    {
                        transaction.Commit();
                        ret = ret01;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = 0;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public void DeleteHangCollection(SqlTransaction transaction)
        {
            string sql = "delete from t_KDT_HangMauDich where TKMD_ID=@ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                db.ExecuteNonQuery(dbCommand, transaction);
            else
                db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteDynamicTransaction(string where, SqlTransaction transaction)
        {
            string spName = "p_KDT_ToKhaiMauDich_DeleteDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, where);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public static void DongBoDuLieuPhongKhai(ToKhaiMauDichCollection tkmdCollection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (ToKhaiMauDich tkmd in tkmdCollection)
                    {
                        foreach (HangMauDich hmd in tkmd.HMDCollection)
                        {
                            hmd.ID = 0;
                        }
                        MsgSend msg = new MsgSend();
                        msg.master_id = tkmd.ID;
                        msg.LoaiHS = "TK";
                        msg.DeleteTransaction(transaction);
                        tkmd.DeleteHangCollection(transaction);
                        tkmd.InsertUpdateFull(transaction);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }


        }

        public List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> ConvertHMDKDToHangMauDich()
        {
            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            foreach (Company.BLL.KDT.HangMauDich hmdKD in this.HMDCollection)
            {
                Company.KDT.SHARE.QuanLyChungTu.HangMauDich hang = new Company.KDT.SHARE.QuanLyChungTu.HangMauDich();
                hang.ID = hmdKD.ID;
                hang.DonGiaKB = (double)hmdKD.DonGiaKB;
                hang.DVT_ID = hmdKD.DVT_ID;
                hang.MaHS = hmdKD.MaHS;
                hang.MaPhu = hmdKD.MaPhu;
                hang.NuocXX_ID = hmdKD.NuocXX_ID;
                hang.SoLuong = hmdKD.SoLuong;
                hang.SoThuTuHang = hmdKD.SoThuTuHang;
                hang.TenHang = hmdKD.TenHang;
                hang.TriGiaKB = (double)hmdKD.TriGiaKB;
                hang.TriGiaKB_VND = (double)hmdKD.TriGiaKB_VND;
                listHang.Add(hang);
            }
            return listHang;
        }
        public List<CO> ListCO
        {
            set { this._ListCO = value; }
            get { return this._ListCO; }
        }
        public void LoadCO()
        {
            ListCO = (List<CO>)CO.SelectCollectionBy_TKMD_ID(this.ID);
        }

        public void LoadGiayPhep()
        {
            GiayPhep.SelectCollectionBy_TKMD_ID(this.ID);
        }
        public void LoadHoaDon()
        {
            HoaDonThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
        }
        public void LoadHopDong()
        {
            HopDongThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
        }
        public void LoadVanDon()
        {
            VanDon.SelectCollectionBy_TKMD_ID(this.ID);
        }
        public void LoadDeNghiChuyenCuaKhau()
        {
            DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(this.ID);
        }
        public void LoadKetQuaXuLy()
        {
            KetQuaXuLyCollection = KetQuaXuLy.SelectCollectionBy_ItemID(this.ID);
        }
        public void LoadHuyToKhai()
        {
            HuyToKhaiCollection = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_TKMD_ID(this.ID);
        }
        public void LoadMsgSend()
        {
            MessageSend = new MsgSend();
            MessageSend.master_id = this.ID;
            MessageSend.LoaiHS = "TK";
            MessageSend.Load();
        }

        #region ChungTuHaiQuan

        public List<GiayPhep> GiayPhepCollection = new List<GiayPhep>();
        public List<HoaDonThuongMai> HoaDonThuongMaiCollection = new List<HoaDonThuongMai>();
        public List<HopDongThuongMai> HopDongThuongMaiCollection = new List<HopDongThuongMai>();
        public VanDon VanTaiDon;
        public List<CO> COCollection = new List<CO>();
        public List<DeNghiChuyenCuaKhau> listChuyenCuaKhau = new List<DeNghiChuyenCuaKhau>();
        public List<ChungTuKem> ChungTuKemCollection = new List<ChungTuKem>();
        public List<NoiDungDieuChinhTK> NoiDungDieuChinhTKCollection = new List<NoiDungDieuChinhTK>();
        public List<ChungTuNo> ChungTuNoCollection = new List<ChungTuNo>();
        public List<KetQuaXuLy> KetQuaXuLyCollection = new List<KetQuaXuLy>();
        public List<HuyToKhai> HuyToKhaiCollection = new List<HuyToKhai>();
        public MsgSend MessageSend;

        public void LoadChungTuHaiQuan()
        {
            try
            {
                //Xoa du lieu truoc khi load lai tranh truong hop trung du lieu.
                COCollection.Clear();
                HoaDonThuongMaiCollection.Clear();
                HopDongThuongMaiCollection.Clear();
                GiayPhepCollection.Clear();
                listChuyenCuaKhau.Clear();
                ChungTuKemCollection.Clear();
                NoiDungDieuChinhTKCollection.Clear();
                ChungTuNoCollection.Clear();

                COCollection = (List<CO>)CO.SelectCollectionBy_TKMD_ID(this.ID);
                for (int l = 0; l < COCollection.Count; l++)
                {
                    COCollection[l].LoadListHMDofCo();
                }

                HoaDonThuongMaiCollection = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
                for (int j = 0; j < HoaDonThuongMaiCollection.Count; j++)
                {
                    HoaDonThuongMaiCollection[j].LoadListHangDMOfHoaDon();
                }

                HopDongThuongMaiCollection = (List<HopDongThuongMai>)HopDongThuongMai.SelectCollectionBy_TKMD_ID(this.ID);
                for (int i = 0; i < HopDongThuongMaiCollection.Count; i++)
                {
                    HopDongThuongMaiCollection[i].LoadListHangDMOfHopDong();
                }

                List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }

                GiayPhepCollection = GiayPhep.SelectCollectionBy_TKMD_ID(this.ID);
                for (int m = 0; m < GiayPhepCollection.Count; m++)
                {
                    GiayPhepCollection[m].LoadListHMDofGiayPhep();
                }

                listChuyenCuaKhau = DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(this.ID);

                ChungTuKemCollection = ChungTuKem.SelectCollectionBy_TKMDID(this.ID);
                for (int k = 0; k < ChungTuKemCollection.Count; k++)
                {
                    ChungTuKemCollection[k].LoadListCTChiTiet();
                }

                //datlmq bo sung them noi dung dieu chinh to khai
                NoiDungDieuChinhTKCollection = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(this.ID);
                for (int k = 0; k < NoiDungDieuChinhTKCollection.Count; k++)
                {
                    NoiDungDieuChinhTKCollection[k].LoadListNDDCTK();
                }

                ChungTuNoCollection = ChungTuNo.SelectCollectionDynamic("TKMDID=" + this.ID, "");

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #endregion ChungTuHaiQuan

        //-----------------------------------------------------------------------------------------

        public string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = FontConverter.Unicode2TCVN(DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            //[ERROR]- LanNT Không cho phép thêm mới id ở đây -- 
            //this.GUIDSTR = (System.Guid.NewGuid().ToString().ToUpper());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeDeclationType = nodeSubject.ChildNodes[5];
            nodeDeclationType.InnerText = "1";

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep.Trim();
            this.Update();
            return doc.InnerXml;
        }

        //public string WSRequestDaDuyet(string pass)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.LoadXml(ConfigPhongBi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi));

        //    XmlDocument docNPL = new XmlDocument();
        //    string path = EntityBase.GetPathProram();
        //    docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

        //    XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
        //    root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

        //    root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = this.MaHaiQuan;

        //    root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

        //    XmlNode Content = doc.GetElementsByTagName("Content")[0];
        //    Content.AppendChild(root);

        //    Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


        //    string kq = "";
        //    try
        //    {
        //        kq = kdt.Send(doc.InnerXml, pass);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
        //    }
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);
        //    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
        //    {
        //        if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "no")
        //        {
        //            return doc.InnerXml;
        //        }
        //    }
        //    else
        //    {
        //        throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
        //    }
        //    return "";

        //}

        #region Khai báo tờ khai nhập

        private string ConvertCollectionToXMLNHAP()
        {
            //load du lieu
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\KhaiBaoToKhaiNhap.xml");

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = this.TenDoanhNghiep;
            //thong tin to khai

            XmlNode nodeToKhai = docNPL.GetElementsByTagName("TO_KHAI")[0];
            if (nodeToKhai.Attributes["DE_XUAT_KHAC"] != null)
                nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = this._DeXuatKhac;

            nodeToKhai.Attributes["MA_LH"].Value = this.MaLoaiHinh;
            nodeToKhai.Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            nodeToKhai.Attributes["MA_DV_NK"].Value = this.MaDoanhNghiep;
            nodeToKhai.Attributes["MA_DV_UT"].Value = this.MaDonViUT;
            nodeToKhai.Attributes["MA_DV_XK"].Value = "";
            if (nodeToKhai.Attributes["TEN_DV_UT"] != null) nodeToKhai.Attributes["TEN_DV_UT"].Value = this.TenDonViUT;
            //nodeToKhai.Attributes["MA_DV_KT"].Value = this.MaDaiLyTTHQ; // Commnent 29/04/2010 Hungtq

            //if (this.TenDonViDoiTac.Length <= 30)
            nodeToKhai.Attributes["TEN_DV_XK"].Value = this.TenDonViDoiTac;
            //else
            //nodeToKhai.Attributes["TEN_DV_XK"].Value = this.TenDonViDoiTac.Substring(0, 30);

            if (this.SoGiayPhep.Length <= 35)
                nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep;
            else
                nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep.Substring(0, 35);
            if (this.NgayGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_GP"].Value = this.NgayGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_GP"].Value = null;
            if (this.NgayHetHanGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHGP"].Value = this.NgayHetHanGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHGP"].Value = null;
            if (this.SoHopDong.Length <= 50)
                nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong;
            else
                nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong.Substring(0, 50);
            if (this.NgayHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HD"].Value = this.NgayHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HD"].Value = null;
            if (this.NgayHetHanHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHHD"].Value = this.NgayHetHanHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHHD"].Value = null;
            if (this.SoHoaDonThuongMai.Length <= 50)
                nodeToKhai.Attributes["SO_HDTM"].Value = this.SoHoaDonThuongMai;
            else
                nodeToKhai.Attributes["SO_HDTM"].Value = this.SoHoaDonThuongMai.Substring(0, 50);
            if (this.NgayHoaDonThuongMai.Year > 1900)
                nodeToKhai.Attributes["NGAY_HDTM"].Value = this.NgayHoaDonThuongMai.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HDTM"].Value = null;
            nodeToKhai.Attributes["MA_PTVT"].Value = this.PTVT_ID;
            if (this.SoHieuPTVT.Length <= 20)
                nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT;
            else
                nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT.Substring(0, 20);
            if (this.NgayDenPTVT.Year > 1900)
                nodeToKhai.Attributes["NGAY_DEN"].Value = this.NgayDenPTVT.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_DEN"].Value = null;
            if (SoVanDon.Length <= 20)
                nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon;
            else
                nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon.Substring(0, 20);
            if (this.NgayVanDon.Year > 1900)
                nodeToKhai.Attributes["NGAY_VANDON"].Value = this.NgayVanDon.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_VANDON"].Value = null;
            nodeToKhai.Attributes["NUOC_XK"].Value = this.NuocXK_ID;
            if (this.DiaDiemXepHang.Length <= 40)
                nodeToKhai.Attributes["CANG_XUAT"].Value = this.DiaDiemXepHang;
            else
                nodeToKhai.Attributes["CANG_XUAT"].Value = this.DiaDiemXepHang.Substring(0, 40);
            nodeToKhai.Attributes["MA_CK_NHAP"].Value = this.CuaKhau_ID;
            nodeToKhai.Attributes["MA_DKGH"].Value = this.DKGH_ID;
            nodeToKhai.Attributes["MA_NGTE"].Value = this.NguyenTe_ID;
            nodeToKhai.Attributes["TY_GIA_VND"].Value = BaseClass.Round(this.TyGiaTinhThue, 9);
            nodeToKhai.Attributes["MA_PTTT"].Value = this.PTTT_ID;
            if (this.GiayTo.Length > 40)
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo.Substring(0, 40);
            else
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo;
            nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(this.SoContainer20).ToString();
            nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(this.SoContainer40).ToString();
            nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(this.SoLuongPLTK).ToString();
            nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(this.SoKien).ToString();
            nodeToKhai.Attributes["TRONG_LUONG"].Value = BaseClass.Round(this.TrongLuong, 9);
            nodeToKhai.Attributes["PHI_BH"].Value = BaseClass.Round(this.PhiBaoHiem, 9);
            decimal PhiKhacNH = this.PhiKhac + this.PhiVanChuyen;
            nodeToKhai.Attributes["PHI_VC"].Value = BaseClass.Round(PhiKhacNH, 9);
            nodeToKhai.Attributes["LE_PHI_HQ"].Value = BaseClass.Round(this.LePhiHaiQuan, 9);
            if (this.TenChuHang.Length > 30)
                nodeToKhai.Attributes["CHU_HANG"].Value = this.TenChuHang.Substring(0, 30);
            else
                nodeToKhai.Attributes["CHU_HANG"].Value = this.TenChuHang;


            //hang hoa
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                //HangMauDich hmd = new HangMauDich();
                //hmd.TKMD_ID = this.ID;
                //this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlNode nodeHang = docNPL.GetElementsByTagName("HANG")[0];
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("HANG.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT_HANG");
                sttAtt.Value = hmd.SoThuTuHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_HANG");

                maAtt.Value = hmd.MaPhu;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = hmd.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_HANG");
                TenAtt.Value = hmd.TenHang;
                node.Attributes.Append(TenAtt);

                XmlAttribute NuocAtt = docNPL.CreateAttribute("NUOC_XX");
                NuocAtt.Value = hmd.NuocXX_ID;
                node.Attributes.Append(NuocAtt);

                XmlAttribute SoLuongAtt = docNPL.CreateAttribute("LUONG");
                SoLuongAtt.Value = BaseClass.Round(hmd.SoLuong, 9);
                node.Attributes.Append(SoLuongAtt);

                XmlAttribute Ma_DVTAtt = docNPL.CreateAttribute("MA_DVT");
                Ma_DVTAtt.Value = hmd.DVT_ID;
                node.Attributes.Append(Ma_DVTAtt);

                XmlAttribute DonGiaAtt = docNPL.CreateAttribute("DGIA_NT");
                DonGiaAtt.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                node.Attributes.Append(DonGiaAtt);

                XmlAttribute TriGiaAtt = docNPL.CreateAttribute("TGIA_NT");
                TriGiaAtt.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                node.Attributes.Append(TriGiaAtt);

                XmlAttribute DonGiaVNDAtt = docNPL.CreateAttribute("DGIA_TT_VND");
                decimal dongiatt = hmd.TriGiaTT / hmd.SoLuong;
                DonGiaVNDAtt.Value = BaseClass.Round(dongiatt, 9);
                node.Attributes.Append(DonGiaVNDAtt);

                XmlAttribute TriGiaVNDAtt = docNPL.CreateAttribute("TGIA_TT_VND");
                TriGiaVNDAtt.Value = BaseClass.Round(hmd.TriGiaTT, 9);
                node.Attributes.Append(TriGiaVNDAtt);

                XmlAttribute TS_XNKAtt = docNPL.CreateAttribute("TS_XNK");
                TS_XNKAtt.Value = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.Attributes.Append(TS_XNKAtt);

                XmlAttribute TS_TTDBAtt = docNPL.CreateAttribute("TS_TTDB");
                TS_TTDBAtt.Value = BaseClass.Round(hmd.ThueSuatTTDB, 9);
                node.Attributes.Append(TS_TTDBAtt);

                XmlAttribute TS_VSTAtt = docNPL.CreateAttribute("TS_VAT");
                TS_VSTAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(TS_VSTAtt);

                XmlAttribute TL_PhuThuAtt = docNPL.CreateAttribute("TL_PHU_THU");
                TL_PhuThuAtt.Value = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.Attributes.Append(TL_PhuThuAtt);

                XmlAttribute ThueXNKAtt = docNPL.CreateAttribute("THUE_XNK");
                ThueXNKAtt.Value = BaseClass.Round(hmd.ThueXNK, 9);
                node.Attributes.Append(ThueXNKAtt);

                XmlAttribute ThueTTDBAtt = docNPL.CreateAttribute("THUE_TTDB");
                ThueTTDBAtt.Value = BaseClass.Round(hmd.ThueTTDB, 9);
                node.Attributes.Append(ThueTTDBAtt);

                XmlAttribute ThueVATAtt = docNPL.CreateAttribute("THUE_VAT");
                ThueVATAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(ThueVATAtt);

                XmlAttribute TPhuThuAtt = docNPL.CreateAttribute("PHU_THU");
                TPhuThuAtt.Value = BaseClass.Round(hmd.PhuThu, 9);
                node.Attributes.Append(TPhuThuAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        public string WSSendXMLNHAP(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiNhap, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXMLNHAP());

            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            //--------Khai dinh kem --------------
            XmlNode DU_LEU = doc.GetElementsByTagName("DU_LIEU")[0];

            XmlNode node = CO.ConvertCollectionCOToXML(doc, this.COCollection, this.ID);

            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            listHang = ConvertHMDKDToHangMauDich();
            if (node != null)
                DU_LEU.AppendChild(node);

            node = GiayPhep.ConvertCollectionGiayPhepToXML(doc, this.GiayPhepCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKN(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HopDongThuongMai.ConvertCollectionHopDongToXML_TKN(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);


            if (VanTaiDon == null)
            {
                List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }
            }
            if (VanTaiDon != null)
            {
                //node = this.VanTaiDon.ConvertVanDonToXML(doc);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            node = DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, this.listChuyenCuaKhau, this.ID);
            if (node != null)
                DU_LEU.AppendChild(node);

            //tkem
            ChungTuKem ctct = new ChungTuKem();

            node = ChungTuKem.ConvertCollectionCTDinhKemToXML(doc, ChungTuKemCollection, this.ID, ctct.listCTChiTiet);
            if (node != null)
                DU_LEU.AppendChild(node);


            //----------------End Khai dinh kem --------------------

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            string kq = "";

            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, this.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoToKhai);

                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        return doc.InnerXml;
                    }
                }
                else
                {
                    string msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WebService_LEVEL";
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID, msgError);
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(kq))
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml + "\nKết quả:" + kq));
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }
            return "";
        }

        public XmlDocument GenerateTKNToXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiNhap, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXMLNHAP());

            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            //--------Khai dinh kem --------------
            XmlNode DU_LEU = doc.GetElementsByTagName("DU_LIEU")[0];

            XmlNode node = CO.ConvertCollectionCOToXML(doc, this.COCollection, this.ID);

            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            listHang = ConvertHMDKDToHangMauDich();
            if (node != null)
                DU_LEU.AppendChild(node);

            node = GiayPhep.ConvertCollectionGiayPhepToXML(doc, this.GiayPhepCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKN(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HopDongThuongMai.ConvertCollectionHopDongToXML_TKN(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);


            if (VanTaiDon == null)
            {
                List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }
            }
            if (VanTaiDon != null)
            {
                // node = this.VanTaiDon.ConvertVanDonToXML(doc);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            node = DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, this.listChuyenCuaKhau, this.ID);
            if (node != null)
                DU_LEU.AppendChild(node);

            //tkem
            ChungTuKem ctct = new ChungTuKem();

            node = ChungTuKem.ConvertCollectionCTDinhKemToXML(doc, ChungTuKemCollection, this.ID, ctct.listCTChiTiet);
            if (node != null)
                DU_LEU.AppendChild(node);

            return doc;
        }

        #endregion Khai báo tờ khai nhập

        #region Khai báo tờ khai xuất

        private string ConvertCollectionToXMLXUAT(string maMid)
        {
            //Update 15062010. HUNGTQ
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\KhaiBaoToKhaiXuat.xml");

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            //thong tin to khai
            XmlNode nodeToKhai = docNPL.GetElementsByTagName("TO_KHAI")[0];
            nodeToKhai.Attributes["XUAT_NPL_SP"].Value = this.LoaiHangHoa;
            nodeToKhai.Attributes["MA_LH"].Value = this.MaLoaiHinh;
            nodeToKhai.Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            nodeToKhai.Attributes["MA_DV_XK"].Value = this.MaDoanhNghiep;
            nodeToKhai.Attributes["MA_DV_UT"].Value = this.MaDonViUT;
            nodeToKhai.Attributes["MA_DV_NK"].Value = "";
            nodeToKhai.Attributes["MA_DV_KT"].Value = this.MaDaiLyTTHQ;
            if (this.TenDonViDoiTac.Length > 30)
                nodeToKhai.Attributes["TEN_DV_NK"].Value = this.TenDonViDoiTac.Substring(0, 30);
            else
                nodeToKhai.Attributes["TEN_DV_NK"].Value = this.TenDonViDoiTac;
            if (this.SoGiayPhep.Length <= 35)
                nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep;
            else
                nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep.Substring(0, 35);
            if (this.NgayGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_GP"].Value = this.NgayGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_GP"].Value = null;
            if (this.NgayHetHanGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHGP"].Value = this.NgayHetHanGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHGP"].Value = null;
            if (this.SoHopDong.Length <= 50)
                nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong;
            else
                nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong.Substring(0, 50);
            if (this.NgayHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HD"].Value = this.NgayHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HD"].Value = null;
            if (this.NgayHetHanHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHHD"].Value = this.NgayHetHanHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHHD"].Value = null;
            nodeToKhai.Attributes["SO_HDTM"].Value = this.SoHoaDonThuongMai;
            if (this.NgayHoaDonThuongMai.Year > 1900)
                nodeToKhai.Attributes["NGAY_HDTM"].Value = this.NgayHoaDonThuongMai.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HDTM"].Value = null;
            nodeToKhai.Attributes["MA_PTVT"].Value = this.PTVT_ID;
            nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT;
            if (this.NgayDenPTVT.Year > 1900)
                nodeToKhai.Attributes["NGAY_DEN"].Value = this.NgayDenPTVT.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_DEN"].Value = null;
            if (SoVanDon.Length <= 25)
                nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon;
            else
                nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon.Substring(0, 25);
            if (this.NgayVanDon.Year > 1900)
                nodeToKhai.Attributes["NGAY_VANDON"].Value = this.NgayVanDon.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_VANDON"].Value = DateTime.Today.ToString("yyyy-MM-dd");
            nodeToKhai.Attributes["NUOC_NK"].Value = this.NuocNK_ID;
            nodeToKhai.Attributes["MA_CK_XUAT"].Value = this.CuaKhau_ID;
            nodeToKhai.Attributes["MA_DKGH"].Value = this.DKGH_ID;
            nodeToKhai.Attributes["MA_NGTE"].Value = this.NguyenTe_ID;
            nodeToKhai.Attributes["TY_GIA_VND"].Value = BaseClass.Round(this.TyGiaTinhThue, 9);
            nodeToKhai.Attributes["MA_PTTT"].Value = this.PTTT_ID;
            if (this.GiayTo.Length > 40)
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo.Substring(0, 40);
            else
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo;
            nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(this.SoContainer20).ToString();
            nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(this.SoContainer40).ToString();
            nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(this.SoLuongPLTK).ToString();
            nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(this.SoKien).ToString();
            nodeToKhai.Attributes["TRONG_LUONG"].Value = BaseClass.Round(this.TrongLuong, 9);
            nodeToKhai.Attributes["PHI_BH"].Value = BaseClass.Round(this.PhiBaoHiem, 9);
            decimal PhiKhacNH = this.PhiVanChuyen + this.PhiKhac;
            nodeToKhai.Attributes["PHI_VC"].Value = BaseClass.Round(PhiKhacNH, 9);
            nodeToKhai.Attributes["LE_PHI_HQ"].Value = BaseClass.Round(this.LePhiHaiQuan, 9);

            nodeToKhai.Attributes["MA_MID"].Value = maMid;

            //DATLMQ update De xuat khac 29102010
            nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = this.DeXuatKhac;

            //hang hoa
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                //HangMauDich hmd = new HangMauDich();
                //hmd.TKMD_ID = this.ID;
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);// hmd.SelectCollectionBy_TKMD_ID();

            }
            XmlNode nodeHang = docNPL.GetElementsByTagName("HANG")[0];
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("HANG.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT_HANG");
                sttAtt.Value = hmd.SoThuTuHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_HANG");
                maAtt.Value = hmd.MaPhu;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = hmd.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_HANG");
                TenAtt.Value = hmd.TenHang;
                node.Attributes.Append(TenAtt);

                XmlAttribute NuocAtt = docNPL.CreateAttribute("NUOC_XX");
                NuocAtt.Value = hmd.NuocXX_ID;
                node.Attributes.Append(NuocAtt);

                XmlAttribute SoLuongAtt = docNPL.CreateAttribute("LUONG");
                SoLuongAtt.Value = BaseClass.Round(hmd.SoLuong, 9);
                node.Attributes.Append(SoLuongAtt);

                XmlAttribute Ma_DVTAtt = docNPL.CreateAttribute("MA_DVT");
                Ma_DVTAtt.Value = hmd.DVT_ID;
                node.Attributes.Append(Ma_DVTAtt);

                XmlAttribute DonGiaAtt = docNPL.CreateAttribute("DGIA_NT");
                DonGiaAtt.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                node.Attributes.Append(DonGiaAtt);

                XmlAttribute TriGiaAtt = docNPL.CreateAttribute("TGIA_NT");
                TriGiaAtt.Value = BaseClass.Round(hmd.TriGiaKB, 9); ;
                node.Attributes.Append(TriGiaAtt);

                XmlAttribute DonGiaVNDAtt = docNPL.CreateAttribute("DGIA_TT_VND");
                decimal dongiatt = hmd.TriGiaTT / hmd.SoLuong;
                DonGiaVNDAtt.Value = BaseClass.Round(dongiatt, 9); ;
                node.Attributes.Append(DonGiaVNDAtt);

                XmlAttribute TriGiaVNDAtt = docNPL.CreateAttribute("TGIA_TT_VND");
                TriGiaVNDAtt.Value = BaseClass.Round(hmd.TriGiaTT, 9); ;
                node.Attributes.Append(TriGiaVNDAtt);

                XmlAttribute TS_XNKAtt = docNPL.CreateAttribute("TS_XNK");
                TS_XNKAtt.Value = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.Attributes.Append(TS_XNKAtt);

                XmlAttribute TS_TTDBAtt = docNPL.CreateAttribute("TS_TTDB");
                TS_TTDBAtt.Value = BaseClass.Round(hmd.ThueSuatTTDB, 9);
                node.Attributes.Append(TS_TTDBAtt);

                XmlAttribute TS_VSTAtt = docNPL.CreateAttribute("TS_VAT");
                TS_VSTAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(TS_VSTAtt);

                XmlAttribute TL_PhuThuAtt = docNPL.CreateAttribute("TL_PHU_THU");
                TL_PhuThuAtt.Value = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.Attributes.Append(TL_PhuThuAtt);

                XmlAttribute ThueXNKAtt = docNPL.CreateAttribute("THUE_XNK");
                ThueXNKAtt.Value = BaseClass.Round(hmd.ThueXNK, 9);
                node.Attributes.Append(ThueXNKAtt);

                XmlAttribute ThueTTDBAtt = docNPL.CreateAttribute("THUE_TTDB");
                ThueTTDBAtt.Value = BaseClass.Round(hmd.ThueTTDB, 9);
                node.Attributes.Append(ThueTTDBAtt);

                XmlAttribute ThueVATAtt = docNPL.CreateAttribute("THUE_VAT");
                ThueVATAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(ThueVATAtt);

                XmlAttribute TPhuThuAtt = docNPL.CreateAttribute("PHU_THU");
                TPhuThuAtt.Value = BaseClass.Round(hmd.PhuThu, 9);
                node.Attributes.Append(TPhuThuAtt);

                XmlAttribute MA_HTSAtt = docNPL.CreateAttribute("MA_HTS");
                MA_HTSAtt.Value = hmd.Ma_HTS;
                node.Attributes.Append(MA_HTSAtt);

                XmlAttribute DVT_HTSAtt = docNPL.CreateAttribute("MA_DVT_HTS");
                DVT_HTSAtt.Value = hmd.DVT_HTS;
                node.Attributes.Append(DVT_HTSAtt);

                XmlAttribute Luong_HTSAtt = docNPL.CreateAttribute("LUONG_HTS");
                Luong_HTSAtt.Value = BaseClass.Round(hmd.SoLuong_HTS, 9);
                node.Attributes.Append(Luong_HTSAtt);

                XmlAttribute donGia_HTSAtt = docNPL.CreateAttribute("DGIA_KB_HTS");
                decimal dongiaHTS = 0;
                try
                {
                    dongiaHTS = Convert.ToDecimal(hmd.TriGiaKB / hmd.SoLuong_HTS);
                }
                catch
                {

                }
                donGia_HTSAtt.Value = BaseClass.Round(dongiaHTS, 9);
                node.Attributes.Append(donGia_HTSAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        public string WSSendXMLXuat(string pass, string mamid)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiXuat, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXMLXUAT(mamid));

            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            // -------------Begin Khai dinh kem --------------------
            XmlNode DU_LEU = doc.GetElementsByTagName("DU_LIEU")[0];

            XmlNode node = CO.ConvertCollectionCOToXML(doc, this.COCollection, this.ID);

            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            listHang = ConvertHMDKDToHangMauDich();
            if (node != null)
                DU_LEU.AppendChild(node);

            node = GiayPhep.ConvertCollectionGiayPhepToXML(doc, this.GiayPhepCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKX(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);


            node = HopDongThuongMai.ConvertCollectionHopDongToXML_TKX(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);


            if (VanTaiDon == null)
            {
                List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }
            }
            if (VanTaiDon != null)
            {
                //node = this.VanTaiDon.ConvertVanDonToXML(doc);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            node = DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, this.listChuyenCuaKhau, this.ID);
            if (node != null)
                DU_LEU.AppendChild(node);

            //tkem
            ChungTuKem ctct = new ChungTuKem();

            List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet> listCTCT = new List<ChungTuKemChiTiet>();
            listCTCT = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctct.LoadCT(this.ID));
            if (listCTCT != null && listCTCT.Count != 0)
            {
                node = ChungTuKem.ConvertCollectionCTDinhKemToXML(doc, ChungTuKemCollection, this.ID, listCTCT);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            // -------------End Khai dinh kem --------------------

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {

                kq = kdt.Send(doc.InnerXml, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, ID,
                    Company.KDT.SHARE.Components.MessageTitle.KhaiBaoToKhai);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node11 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node11);
                    return doc.InnerXml;
                }
            }
            else
            {
                string msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WebService_LEVEL";
                Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID, msgError);
                throw new Exception(msgError);
            }
            return "";
        }

        public string WSSendXMLXuat(string pass, string mamid, ToKhaiMauDich tkmd)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiXuat, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXMLXUAT(mamid));

            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            // -------------Begin Khai dinh kem --------------------
            XmlNode DU_LEU = doc.GetElementsByTagName("DU_LIEU")[0];
            XmlNode node = CO.ConvertCollectionCOToXML(doc, this.COCollection, this.ID);

            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            listHang = ConvertHMDKDToHangMauDich();
            if (node != null)
                DU_LEU.AppendChild(node);

            node = GiayPhep.ConvertCollectionGiayPhepToXML(doc, this.GiayPhepCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKX(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HopDongThuongMai.ConvertCollectionHopDongToXML_TKX(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            if (VanTaiDon == null)
            {
                List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }
            }
            if (tkmd.MaLoaiHinh.StartsWith("XSX"))
            {
                if (VanTaiDon != null)
                {
                    //node = this.VanTaiDon.ConvertVanDonToXML(doc);
                    if (node != null)
                        DU_LEU.AppendChild(node);
                }
            }
            // -------------End Khai dinh kem --------------------

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, ID,
                     Company.KDT.SHARE.Components.MessageTitle.KhaiBaoToKhai);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node11 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node11);
                    return doc.InnerXml;
                }
            }
            else
            {
                string msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WebService_LEVEL";
                Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID, msgError);
                throw new Exception(msgError);
            }
            return "";
        }

        public XmlDocument GenerateTKXToXML(string pass, string mamid)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiXuat, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXMLXUAT(mamid));

            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            // -------------Begin Khai dinh kem --------------------
            XmlNode DU_LEU = doc.GetElementsByTagName("DU_LIEU")[0];

            XmlNode node = CO.ConvertCollectionCOToXML(doc, this.COCollection, this.ID);

            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            listHang = ConvertHMDKDToHangMauDich();
            if (node != null)
                DU_LEU.AppendChild(node);

            node = GiayPhep.ConvertCollectionGiayPhepToXML(doc, this.GiayPhepCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKX(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);


            node = HopDongThuongMai.ConvertCollectionHopDongToXML_TKX(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);


            if (VanTaiDon == null)
            {
                List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }
            }
            if (VanTaiDon != null)
            {
                // node = this.VanTaiDon.ConvertVanDonToXML(doc);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            node = DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, this.listChuyenCuaKhau, this.ID);
            if (node != null)
                DU_LEU.AppendChild(node);

            //tkem
            ChungTuKem ctct = new ChungTuKem();

            List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet> listCTCT = new List<ChungTuKemChiTiet>();
            listCTCT = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctct.LoadCT(this.ID));
            if (listCTCT != null && listCTCT.Count != 0)
            {
                node = ChungTuKem.ConvertCollectionCTDinhKemToXML(doc, ChungTuKemCollection, this.ID, listCTCT);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            // -------------End Khai dinh kem --------------------

            return doc;
        }

        #endregion Khai báo tờ khai xuất

        #region Huy khai báo tờ khai

        public string WSCancelXMLNhap(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiNhap, MessgaseFunction.HuyKhaiBao));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\HuyKhaiBaoToKhaiNhap.xml");

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            nodeHQNhan.Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = this.TenDoanhNghiep;

            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, ID,
                  Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoToKhai);

            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                string msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WebService_LEVEL";
                Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID, msgError);
                throw new Exception(msgError);
            }
            return "";

        }

        public string WSCancelXMLXuat(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiXuat, MessgaseFunction.HuyKhaiBao));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\HuyKhaiBaoToKhaiXuat.xml");

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            nodeHQNhan.Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, ID,
                Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoToKhai);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                string msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WebService_LEVEL";
                Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID, msgError);
                throw new Exception(msgError);
            }
            return "";

        }

        #endregion Huy khai báo tờ khai

        #region Lấy phản hồi

        public string ConfigPhongBiLayPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + @"\TemplateXML\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            nodeFrom.ChildNodes[0].InnerText = this.TenDoanhNghiep;

            return doc.InnerXml;

        }

        public string WSRequestXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.StartsWith("NSX"))
                doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiNhap, MessgaseFunction.HoiTrangThai));
            else
                doc.LoadXml(ConfigPhongBi(MessgaseType.ToKhaiXuat, MessgaseFunction.HoiTrangThai));
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\LayPhanHoiToKhai.xml");

            XmlNode nodeHQNhan = docNPL.GetElementsByTagName("HQ_NHAN")[0];
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            XmlNode nodeDuLieu = docNPL.GetElementsByTagName("DU_LIEU")[0];
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WebService_LEVEL");
            }
            return "";

        }

        public static bool tuChoi = false;
        public string WSRequestPhanLuong(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiLayPhanHoi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.TenDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            int i = 0;
            XmlNode Result = null;
            string msgError = string.Empty;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    //Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, ID,
                    //   Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayThongBaoHQ);
                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);

                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                    if (Result.Attributes["Err"].Value == "no")
                    {
                        if (Result.Attributes["TrangThai"].Value == "yes")
                            break;
                    }
                    else
                    {

                        if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                        {
                            msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                            throw new Exception(msgError);
                        }
                        else
                        {
                            msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                            throw new Exception(msgError);
                        }
                    }
                }
                catch (Exception ex)
                {

                    if (!string.IsNullOrEmpty(msgError))
                    {
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID,
                            Company.KDT.SHARE.Components.MessageTitle.Error);
                        throw ex;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(kq))
                            Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));

                    }
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không thể kết nối tới hệ thống hải quan!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
            {
                if (!string.IsNullOrEmpty(kq))
                    return kq;
                else
                    return doc.InnerXml;
            }


            #region XỬ LÝ MSG TRẢ VỀ

            string errorSt = ""; // Luu thong tin loi
            try
            {

                XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                XmlNode nodeRootSXXK = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");

                if (nodeRoot.Attributes.GetNamedItem("TuChoi") != null && nodeRoot.Attributes["TuChoi"].Value == "yes")
                {
                    #region Từ chối

                    this.HUONGDAN = FontConverter.TCVN2Unicode(nodeRoot.InnerText);

                    //KetQuaXuLy kqxl = new KetQuaXuLy();
                    //kqxl.ItemID = this.ID;
                    //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    //kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    //kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                    //kqxl.NoiDung = FontConverter.TCVN2Unicode(nodeRoot.InnerText);
                    //kqxl.Ngay = DateTime.Now;
                    //kqxl.Insert();
                    tuChoi = true;

                    if (this.ActionStatus != (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                    {
                        this.SoTiepNhan = this.SoToKhai = 0;
                        this.NgayTiepNhan = new DateTime(1900, 1, 1);
                        this.ActionStatus = -1;
                    }
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;// Vẫn để trạng thái  không phê duyệt, chỉ update ở ToKhaiMauDichForm
                    this.Update();

                    #endregion

                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan, this.HUONGDAN);
                }
                else if (nodeRootSXXK != null && nodeRootSXXK.Attributes["TRANG_THAI"].Value == "THANH CONG")
                {
                    string strTraLoi = nodeRootSXXK.Attributes["TRA_LOI"].Value;
                    if (strTraLoi == THONG_TIN_DANG_KY.TOKHAINHAP || strTraLoi == THONG_TIN_DANG_KY.TOKHAIXUAT)
                    {
                        #region Lấy số tiếp nhận

                        if (nodeRootSXXK.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NamDK = Convert.ToInt32(nodeDuLieu.Attributes["NAM_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                            this.Update();


                            //KetQuaXuLy kqxl = new KetQuaXuLy();
                            //kqxl.ItemID = this.ID;
                            //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            //kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            //kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                            //kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                            //kqxl.Ngay = DateTime.Now;
                            //kqxl.Insert();
                        }

                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayThongBaoHQ,
                          string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}, năm tiếp nhận {2}", this.SoTiepNhan, this.NgayTiepNhan, NamDK));
                        #endregion
                    }
                    else if (strTraLoi == THONG_TIN_HUY.TOKHAINHAP || strTraLoi == THONG_TIN_HUY.TOKHAIXUAT)
                    {
                        #region Hủy khai báo

                        if (nodeRootSXXK.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            KetQuaXuLy kqxl = new KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = "Hủy khai báo";
                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nNgày hủy: {2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), DateTime.Now.ToString())
                                 + "\r\n" + FontConverter.TCVN2Unicode(nodeRoot.InnerText);
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();

                            this.HUONGDAN = FontConverter.TCVN2Unicode(nodeRoot.InnerText);
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.ActionStatus = -1;
                            this.SoTiepNhan = 0;
                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                            this.Update();
                            Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.HQHuyKhaiBaoToKhai, "Hủy thành công");

                        }
                        #endregion

                    }
                    else if (strTraLoi == "SUA_TKNSX" || strTraLoi == "SUA_TKXSX")
                    {
                        #region Lấy số tiếp nhận

                        if (nodeRootSXXK.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NamDK = Convert.ToInt32(nodeDuLieu.Attributes["NAM_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                            this.Update();


                            KetQuaXuLy kqxl = new KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();

                            Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.SuaToKhai,
                             kqxl.NoiDung);

                        }
                        #endregion
                    }
                }
                else if (nodeRootSXXK != null && nodeRootSXXK.Attributes["TRANG_THAI"].Value == "LOI")
                {
                    #region Xảy ra lỗi

                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;

                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, "Lỗi dữ liệu trả về",
                              nodeRoot.InnerText);

                    #endregion

                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai + " " + this.MaLoaiHinh;
                    kqxl.LoaiThongDiep = errorSt;
                    kqxl.NoiDung = nodeRoot.InnerText; // FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    errorSt = errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi;

                    throw new Exception(errorSt);
                }
                /*Tờ khai sửa được duyệt*/
                //else if (nodeRoot.Attributes.GetNamedItem("SOTK").Value == this.SoToKhai.ToString()
                //    && this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET
                //    && this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                //{
                //    this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;
                //    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                //    this.Update();

                //    KetQuaXuLy kqxl = new KetQuaXuLy();
                //    kqxl.ItemID = this.ID;
                //    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                //    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                //    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiSuaDuocDuyet;
                //    kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                //    kqxl.Ngay = DateTime.Now;
                //    kqxl.Insert();
                //}
                else if (nodeRoot != null && nodeRoot.Attributes["TrangThai"].Value == "yes")
                {
                    #region Thành công
                    /*Tờ khai sửa được duyệt*/
                    if (nodeRoot.Attributes.GetNamedItem("SOTK") != null
                        && nodeRoot.Attributes.GetNamedItem("SOTK").Value == this.SoToKhai.ToString()
                        && this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET
                        && this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua
                        && nodeRoot.SelectSingleNode("PHAN_LUONG") == null)
                    {
                        #region Duyệt TK sửa

                        this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        this.Update();
                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiSuaDuocDuyet;
                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();

                        //TODO: Hungtq Updated 31/03/2011. Bổ sung cập nhật lại thông tin Hàng của tờ khai sau khi Tờ khai sửa được duyệt.
                        this.CapNhatThongTinHangToKhaiSua();

                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiSuaDuocDuyet,
                                string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim()));

                        #endregion
                    }
                    /*Kiem tra lấy số tờ khai*/
                    else if (nodeRoot.Attributes.GetNamedItem("SOTK") != null && this.SoToKhai == 0)
                    {
                        #region Lấy số tờ khai
                        string strMsg = string.Empty;
                        this.SoToKhai = Convert.ToInt32(nodeRoot.Attributes["SOTK"].Value);
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("NGAYDK") != null)
                            this.NgayDangKy = Convert.ToDateTime(nodeRoot.Attributes["NGAYDK"].Value);
                        this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMDK"].Value);

                        this.ActionStatus = 1;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        this.Update();

                        //TODO: Chuyen trang thai to khai. Hungtq bo sung 12/02/2011 7:15 PM.
                        TransgferDataToSXXK();



                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                        strMsg = kqxl.NoiDung;

                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo, kqxl.NoiDung);


                        #endregion
                        // Trong trường hợp nội dung trả về là phân luồng TK mà TK chưa nhận được Duyệt thì sau khi thực hiện đoạn code fía trên thì fải thêm đoạn này nữa.
                        if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                        {
                            #region Phân luồng Tờ khai

                            XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                            this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                            this.HUONGDAN = FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                            this.ActionStatus = 1;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                            this.Update();



                            kqxl = new KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;

                            string tenluong = "Xanh";

                            if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                                tenluong = "Vàng";
                            else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                                tenluong = "Đỏ";

                            kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();

                            //AN DINH THUE
                            AnDinhThue(docNPL, kq);

                            #endregion
                            strMsg += "Tờ khai phân luồng: " + tenluong;

                            Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocPhanLuong, strMsg);
                        }


                    }/*Kiem tra phân luồng tờ khai*/
                    else if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                    {
                        #region Phân luồng Tờ khai

                        XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                        this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                        this.HUONGDAN = FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                        this.ActionStatus = 1;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        this.Update();


                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;

                        string tenluong = "Xanh";

                        if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                            tenluong = "Vàng";
                        else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                            tenluong = "Đỏ";

                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();

                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocPhanLuong, kqxl.NoiDung);

                        //AN DINH THUE
                        AnDinhThue(docNPL, kq);

                        #endregion
                    }

                    #endregion
                }

            #endregion


                #region Comment
                //else if (nodeRoot.Attributes.GetNamedItem("SOTN") != null)
                //{
                //    #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                //if (nodeRoot.SelectSingleNode("PHAN_LUONG") == null) //Neu message chua phan luong -> cap nhat SO TN.
                //{
                //    this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                //    this.NgayTiepNhan = DateTime.Today;
                //    this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);

                //    this.ActionStatus = 0;
                //    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;

                //    KetQuaXuLy kqxl = new KetQuaXuLy();
                //    kqxl.ItemID = this.ID;
                //    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                //    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                //    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                //    kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                //    kqxl.Ngay = DateTime.Now;
                //    kqxl.Insert();

                //}

                /*Kiem tra co So to khai*/
                //if (nodeRoot.Attributes.GetNamedItem("SOTK") != null)
                //{
                //    this.SoToKhai = Convert.ToInt32(nodeRoot.Attributes["SOTK"].Value);
                //    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("NGAYDK") != null)
                //        this.NgayDangKy = Convert.ToDateTime(nodeRoot.Attributes["NGAYDK"].Value);
                //    this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMDK"].Value);

                //    this.ActionStatus = 1;
                //    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                //    KetQuaXuLy kqxl = new KetQuaXuLy();
                //    kqxl.ItemID = this.ID;
                //    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                //    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                //    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                //    kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                //    kqxl.Ngay = DateTime.Now;
                //    kqxl.Insert();
                //}

                /*Lay thong tin phan luong*/
                //if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                //{
                //XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                //this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                //this.HUONGDAN = FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                //this.ActionStatus = 1;
                //this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;


                //KetQuaXuLy kqxl = new KetQuaXuLy();
                //kqxl.ItemID = this.ID;
                //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                //kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                //kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;

                //string tenluong = "Xanh";

                //if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                //    tenluong = "Vàng";
                //else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                //    tenluong = "Đỏ";

                //kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                //kqxl.Ngay = DateTime.Now;
                //kqxl.Insert();

                /*
                 * HUNGTQ, Update 03/06/2010
                 * Bổ sung nội dung phân luồng trả về: Ấn định thuế
                 */
                /*
                if (nodeRoot.SelectSingleNode("AN_DINH_THUE") != null)
                {
                    XmlNode anDinhThue = nodeRoot.SelectSingleNode("AN_DINH_THUE");

                    string strBoSung = "";

                    strBoSung += "Ấn đinh thuế:";
                    strBoSung += string.Format("+ Số quyết định: {0}\r\n+ Ngày quyết định: {1}\r\n+ Ngày hết hạn: {2}";


                    if (strBoSung.Length != 0)
                    {
                        kqxl.NoiDung += "\r\n" + strBoSung;
                        kqxl.Update();
                    }
                }
                */
                //}

                //    #endregion Lấy số tiếp nhận của danh sách NPL
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception(errorSt);
            }

            #region code cũ

            /*
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            bool ok = false;

            if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
            {
                // XmlNode nodeCheck = docNPL.SelectSingleNode("SOTK");
                // XmlAttribute xmlattSoTK = docNPL.Attributes["SOTK"];
                //if (xmlattSoTK != null)
                //{
                //kiem tra neu ton tai so khai :
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SOTK"].Value != "")
                {

                    this.SoToKhai = Convert.ToInt32(docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SOTK"].Value);
                    this.MaLoaiHinh = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["MALH"].Value;
                    this.NamDK = Convert.ToInt32(docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["NAMDK"].Value);
                    this.TrangThaiXuLy = 1;
                    this.ActionStatus = 1;
                }
                //}
                XmlNode nodeCheckPhanLuong = docNPL.SelectSingleNode("PHAN_LUONG");
                XmlAttribute xmlattPL = docNPL.Attributes["MALUONG"];
                if (nodeCheckPhanLuong != null)
                {
                    //Lây thông tin mã luồng
                    this.PhanLuong = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["MALUONG"].Value.Trim();
                    //Lay thông tin huong dan luong :
                    this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["HUONGDAN"].Value);
                    this.ActionStatus = 1;
                }
                //TransgferDataToSXXK();
                this.Update();
                ok = false;
                try
                {
                    XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    if (nodeMess.InnerText != "")
                    {
                        XmlElement mess = docNPL.CreateElement("Megs");
                        XmlElement messCon = docNPL.CreateElement("Meg");
                        messCon.InnerText = nodeMess.InnerText;
                        mess.AppendChild(messCon);
                        return mess.OuterXml;
                    }
                }
                catch { }
            }
            */
            #endregion
            return "";

        }

        private void AnDinhThue(XmlDocument docNPL, string kq)
        {
            try
            {
                #region BEGIN AN DINH THUE
                //DATLMQ bổ sung In ra thông báo thuế 06/08/2011
                XmlNode xmlNodeAnDinhThue = docNPL.SelectSingleNode("Envelope/Body/Content/Root/AN_DINH_THUE");
                if (xmlNodeAnDinhThue != null)
                {
                    IsThongBaoThue = true;
                    SoQD = xmlNodeAnDinhThue.Attributes["SOQD"].Value;
                    NgayQD = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYQD"].Value);
                    NgayHetHan = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYHH"].Value);
                    TaiKhoanKhoBac = xmlNodeAnDinhThue.Attributes["TKKB"].Value;
                    TenKhoBac = xmlNodeAnDinhThue.Attributes["KHOBAC"].Value;

                    //Kiem tra thong tin an dinh thue cau to khai
                    AnDinhThue anDinhThueObj = new AnDinhThue();
                    List<AnDinhThue> dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(this.ID);
                    if (dsADT.Count > 0)
                    {
                        anDinhThueObj.ID = dsADT[0].ID;
                    }

                    //Luu an dinh thue
                    anDinhThueObj.SoQuyetDinh = SoQD;
                    anDinhThueObj.NgayQuyetDinh = NgayQD;
                    anDinhThueObj.NgayHetHan = NgayHetHan;
                    anDinhThueObj.TaiKhoanKhoBac = TaiKhoanKhoBac;
                    anDinhThueObj.TenKhoBac = TenKhoBac;
                    anDinhThueObj.GhiChu = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), Company.KDT.SHARE.Components.Globals.GetPhanLuong(this.PhanLuong), this.HUONGDAN);
                    anDinhThueObj.TKMD_ID = ID;
                    anDinhThueObj.TKMD_Ref = GUIDSTR;
                    if (anDinhThueObj.ID == 0)
                        anDinhThueObj.Insert();
                    else
                        anDinhThueObj.InsertUpdate();

                    #region Begin Danh sach thue chi tiet
                    ChiTiet thueObj = new ChiTiet();

                    XmlNodeList xmlNodeThue = docNPL.SelectNodes("Envelope/Body/Content/Root/AN_DINH_THUE/THUE");
                    foreach (XmlNode node in xmlNodeThue)
                    {
                        if (node.Attributes["SACTHUE"].Value.Equals("XNK"))
                        {
                            ChuongThueXNK = node.Attributes["CHUONG"].Value;
                            SoTienThueXNK = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueXNK = node.Attributes["LOAI"].Value;
                            KhoanThueXNK = node.Attributes["KHOAN"].Value;
                            MucThueXNK = node.Attributes["MUC"].Value;
                            TieuMucThueXNK = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue XNK
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "XNK";
                            thueObj.Chuong = ChuongThueXNK;
                            thueObj.TienThue = (decimal)SoTienThueXNK;
                            thueObj.Loai = int.Parse(LoaiThueXNK);
                            thueObj.Khoan = int.Parse(KhoanThueXNK);
                            thueObj.Muc = int.Parse(MucThueXNK);
                            thueObj.TieuMuc = int.Parse(TieuMucThueXNK);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                        if (node.Attributes["SACTHUE"].Value.Equals("VAT"))
                        {
                            ChuongThueVAT = node.Attributes["CHUONG"].Value;
                            SoTienThueVAT = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueVAT = node.Attributes["LOAI"].Value;
                            KhoanThueVAT = node.Attributes["KHOAN"].Value;
                            MucThueVAT = node.Attributes["MUC"].Value;
                            TieuMucThueVAT = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue VAT
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "VAT";
                            thueObj.Chuong = ChuongThueVAT;
                            thueObj.TienThue = (decimal)SoTienThueVAT;
                            thueObj.Loai = int.Parse(LoaiThueVAT);
                            thueObj.Khoan = int.Parse(KhoanThueVAT);
                            thueObj.Muc = int.Parse(MucThueVAT);
                            thueObj.TieuMuc = int.Parse(TieuMucThueVAT);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                        if (node.Attributes["SACTHUE"].Value.Equals("TTDB"))
                        {
                            ChuongThueTTDB = node.Attributes["CHUONG"].Value;
                            SoTienThueTTDB = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueTTDB = node.Attributes["LOAI"].Value;
                            KhoanThueTTDB = node.Attributes["KHOAN"].Value;
                            MucThueTTDB = node.Attributes["MUC"].Value;
                            TieuMucThueTTDB = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue Tieu thu dac biet
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "TTDB";
                            thueObj.Chuong = ChuongThueTTDB;
                            thueObj.TienThue = (decimal)SoTienThueTTDB;
                            thueObj.Loai = int.Parse(LoaiThueTTDB);
                            thueObj.Khoan = int.Parse(KhoanThueTTDB);
                            thueObj.Muc = int.Parse(MucThueTTDB);
                            thueObj.TieuMuc = int.Parse(TieuMucThueTTDB);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                        if (node.Attributes["SACTHUE"].Value.Equals("TVCBPG"))
                        {
                            ChuongThueTVCBPG = node.Attributes["CHUONG"].Value;
                            SoTienThueTVCBPG = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                            LoaiThueTVCBPG = node.Attributes["LOAI"].Value;
                            KhoanThueTVCBPG = node.Attributes["KHOAN"].Value;
                            MucThueTVCBPG = node.Attributes["MUC"].Value;
                            TieuMucThueTVCBPG = node.Attributes["TIEUMUC"].Value;

                            //Tao doi tuong thue Thu chenh lech gia
                            thueObj = new ChiTiet();
                            thueObj.SacThue = "TVCBPG";
                            thueObj.Chuong = ChuongThueTVCBPG;
                            thueObj.TienThue = (decimal)SoTienThueTVCBPG;
                            thueObj.Loai = int.Parse(LoaiThueTVCBPG);
                            thueObj.Khoan = int.Parse(KhoanThueTVCBPG);
                            thueObj.Muc = int.Parse(MucThueTVCBPG);
                            thueObj.TieuMuc = int.Parse(TieuMucThueTVCBPG);
                            thueObj.IDAnDinhThue = anDinhThueObj.ID;
                            thueObj.InsertUpdateBy(null);
                        }
                    }


                    #endregion End Danh sach thue chi tiet

                    KetQuaXuLy kqxlAnDinhThue = new KetQuaXuLy();
                    kqxlAnDinhThue.ItemID = this.ID;
                    kqxlAnDinhThue.ReferenceID = new Guid(this.GUIDSTR);
                    kqxlAnDinhThue.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxlAnDinhThue.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_AnDinhThueToKhai;
                    kqxlAnDinhThue.NoiDung = string.Format("Ấn định thuế:\r\nSố quyết định: {0}\r\nNgày quyết định: {1}\r\nNgày hết hạn: {2}\r\nTài khoản kho bạc: {3}\r\nKho bạc: {4}", SoQD, NgayQD.ToString(), NgayHetHan.ToString(), TaiKhoanKhoBac, TenKhoBac);
                    kqxlAnDinhThue.Ngay = DateTime.Now;
                    kqxlAnDinhThue.Insert();

                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiAnDinhThue, kqxlAnDinhThue.NoiDung);
                }
                #endregion END AN DINH THUE
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(string.Format("Ấn định thuế tờ khai số: {0}, ngày đăng ký: {1}, mã loại hình: {2}, ID: {3}", this.SoToKhai, NgayDangKy.ToString(), MaLoaiHinh, GUIDSTR), ex); }
        }

        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.GetElementsByTagName("function")[0].InnerText = "5";

            //HungTQ, Update 21/04/2010.
            doc.GetElementsByTagName("declarationType")[0].InnerText = "1";

            string kq = "";
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        string errorSt = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                        //KetQuaXuLy kqxl = new KetQuaXuLy();
                        //kqxl.ItemID = this.ID;
                        //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        //kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai + " " + this.MaLoaiHinh;
                        //kqxl.LoaiThongDiep = "TK_BS"; // KetQuaXuLy.LoaiChungTu_ToKhai;
                        //kqxl.NoiDung = errorSt; // FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        //kqxl.Ngay = DateTime.Now;
                        //kqxl.Insert();

                        throw new Exception(errorSt + "|" + "WebService_LEVEL");
                    }
                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(kq))
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.Error, ex.Message);
                    else
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml + "\nKết quả:" + kq));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;

            if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") == null)
            {
                XmlNode node = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (node != null
                    && node.Attributes["Err"].Value == "no" && node.Attributes["TrangThai"].Value == "yes")
                {
                    List<HuyToKhai> huyTk = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_TKMD_ID(this.ID);
                    if (huyTk[0].SoTiepNhan != 0)
                    {
                        huyTk[0].TrangThai = 10;
                        huyTk[0].Update();
                        TrangThaiXuLy = 10;
                        this.Update();
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.HuyToKhaiThanhCong);

                    }
                    else
                    {
                        huyTk[0].TKMD_ID = this.ID;
                        huyTk[0].SoTiepNhan = long.Parse(node.Attributes["SO_TN"].Value);
                        huyTk[0].NgayTiepNhan = DateTime.Parse(node.Attributes["Ngay_TN"].Value);
                        huyTk[0].NamTiepNhan = int.Parse(node.Attributes["NAM_TN"].Value);
                        huyTk[0].TrangThai = 11;
                        huyTk[0].Update();
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.HuyTokhaiDuocCapSo);
                    }

                }
            }
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
            {
                XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.TOKHAINHAP || nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.TOKHAIXUAT
                    || nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.CAPNHATTOKHAINHAP || nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.CAPNHATTOKHAIXUAT)
                {
                    #region Lấy số tiếp nhận của danh sách

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                        this.NgayTiepNhan = DateTime.Today;
                        this.TrangThaiXuLy = 0;
                        this.Update();


                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                        kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                        string noidung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiToKhai, noidung);

                    }
                    #endregion Lấy số tiếp nhận của danh sách

                }
                else if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.TOKHAINHAP || nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.TOKHAIXUAT)
                {
                    #region Hủy khai báo

                    if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                    {
                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = "Hủy khai báo";
                        kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nNgày hủy: {2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), DateTime.Now.ToString())
                            + "\r\n" + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();

                        this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.ActionStatus = -1;
                        this.SoTiepNhan = 0;
                        this.NgayTiepNhan = new DateTime(1900, 1, 1);
                        this.Update();
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoToKhai, "Hủy thành công");

                    }

                    #endregion Hủy khai báo
                }
                else if (nodeTrangthai.Attributes["TRA_LOI"].Value == LAY_THONG_TIN.TOKHAI)
                {
                    #region Nhận trạng thái hồ sơ
                    XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                    if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "DA_XU_LY")
                    {
                        string phanHoi = LayPhanHoiTQDTKhaiBao(pass, doc.InnerXml);

                        if (phanHoi.Length != 0)
                        {
                            XmlDocument docPH = new XmlDocument();
                            docPH.LoadXml(phanHoi);

                            XmlNode nodeTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                            if (nodeTuChoi.Attributes.GetNamedItem("TuChoi") != null
                                && nodeTuChoi.Attributes["TuChoi"].Value == "yes")
                            {
                                //nodeTuChoi.InnerText;
                                this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.SoTiepNhan = this.SoToKhai = 0;
                                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                                this.ActionStatus = 2;
                                this.Update();

                                KetQuaXuLy kqxl = new KetQuaXuLy();
                                kqxl.ItemID = this.ID;
                                kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                                kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                                kqxl.NoiDung = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                                kqxl.Ngay = DateTime.Now;
                                kqxl.Insert();
                            }
                        }
                        else
                        {
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                            this.SoToKhai = Convert.ToInt32(nodeDuLieu.Attributes["SO_TK"].Value);
                            this.NgayDangKy = Convert.ToDateTime(nodeDuLieu.Attributes["NGAY_DK"].Value);
                            TransgferDataToSXXK();


                            KetQuaXuLy kqxl = new KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                            kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();
                            string noidung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                            Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);

                        }
                    }
                    else if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "TU_CHOI")
                    {
                        this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.SoTiepNhan = this.SoToKhai = 0;
                        this.NgayTiepNhan = new DateTime(1900, 1, 1);
                        this.ActionStatus = 2;
                        this.Update();

                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                        kqxl.NoiDung = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                        string noidung = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan, noidung);

                    }


                    #endregion
                }
            }
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
            {
                XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                string errorSt = "";
                if (stMucLoi == "XML_LEVEL")
                    errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                else if (stMucLoi == "DATA_LEVEL")
                    errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                else if (stMucLoi == "SERVICE_LEVEL")
                    errorSt = "Lỗi do Web service trả về ";
                else if (stMucLoi == "DOTNET_LEVEL")
                    errorSt = "Lỗi do hệ thống của hải quan ";
                throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
            }
            /*Kiem tra PHAN LUONG*/
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTN") != null)
            {
                #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                if (nodeRoot.SelectSingleNode("PHAN_LUONG") == null) //Neu message chua phan luong -> cap nhat SO TN.
                {
                    this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                    this.NgayTiepNhan = DateTime.Today;
                    this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);

                    this.ActionStatus = 0;
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;


                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                    kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                    string noidung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo, noidung);

                }

                /*Kiem tra co So to khai*/
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTK") != null)
                {
                    this.SoToKhai = Convert.ToInt32(nodeRoot.Attributes["SOTK"].Value);
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("NGAYDK") != null)
                        this.NgayDangKy = Convert.ToDateTime(nodeRoot.Attributes["NGAYDK"].Value);
                    this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMDK"].Value);

                    this.ActionStatus = 1;
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                    kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    string noidung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo, noidung);

                }

                /*Lay thong tin phan luong*/
                if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                {
                    XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                    this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                    this.HUONGDAN = FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                    this.ActionStatus = 1;
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                    //KetQuaXuLy kqxl = new KetQuaXuLy();
                    //kqxl.ItemID = this.ID;
                    //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    //kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    //kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;

                    string tenluong = "Xanh";

                    if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        tenluong = "Vàng";
                    else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        tenluong = "Đỏ";

                    //kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                    //kqxl.Ngay = DateTime.Now;
                    //kqxl.Insert();
                    string noidung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocPhanLuong, noidung);
                }

                this.Update();

                #endregion Lấy số tiếp nhận của danh sách NPL
            }

            return "";

        }

        public string LayPhanHoiTQDTKhaiBao(string pass, string xml)
        {
            #region Load header xml
            //-------------begin----------------
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiLayPhanHoi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.TenDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            //-----------end new --------------
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);
            #endregion
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            int function = MessgaseFunction.KhaiBao;
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);

                //Lay funtion tu ket qua tra ve
                function = Convert.ToInt32(docNPL.GetElementsByTagName("function")[0].InnerText);

                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                if (Result == null)
                    continue;

                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {

                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = "Lỗi khai báo";
                    kqxl.NoiDung = string.Format("Lỗi khai báo tờ khai: \r\nID: {0} \r\nLoại hình: {1}", this.ID, this.MaLoaiHinh)
                        + "\r\nNội dung: " + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                }
            }

            if (i > 1)
                return doc.InnerXml;


            string errorSt = ""; // Luu thong tin loi
            try
            {
                #region XU LY MESSAGE
                /*HUNGTQ, Update 19/05/2010, XU LY THONG BAO TRA VE*/
                //Kiem tra thong tin tu choi TK?

                XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                XmlNode nodeRootSXXK = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");

                if (nodeRoot.Attributes.GetNamedItem("TuChoi") != null && nodeRoot.Attributes["TuChoi"].Value == "yes")
                {
                    #region Từ chối tờ khai

                    this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                    kqxl.NoiDung = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.SoTiepNhan = this.SoToKhai = 0;
                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.ActionStatus = 2;
                    this.Update();
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan, this.HUONGDAN);

                    #endregion
                }
                else if (nodeRootSXXK != null && nodeRootSXXK.Attributes["TRANG_THAI"].Value == "THANH CONG")
                {
                    string strTraLoi = nodeRootSXXK.Attributes["TRA_LOI"].Value;

                    if (strTraLoi == THONG_TIN_DANG_KY.TOKHAINHAP || strTraLoi == THONG_TIN_DANG_KY.TOKHAIXUAT)
                    {
                        #region Lấy số tiếp nhận TK MỚI

                        if (nodeRootSXXK.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NamDK = Convert.ToInt32(nodeDuLieu.Attributes["NAM_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.ActionStatus = 0;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                            this.Update();

                            KetQuaXuLy kqxl = new KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();
                        }
                        #endregion
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoLayPhanHoiToKhai);

                    }
                    else if (strTraLoi == "SUA_TKNSX" || strTraLoi == "SUA_TKXSX")
                    {
                        #region Lấy số tiếp nhận TK SỬA

                        if (nodeRootSXXK.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NamDK = Convert.ToInt32(nodeDuLieu.Attributes["NAM_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                            this.Update();

                            KetQuaXuLy kqxl = new KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();
                            Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.SuaToKhai, kqxl.NoiDung);

                        }
                        #endregion
                    }
                    else if (strTraLoi == THONG_TIN_HUY.TOKHAINHAP || strTraLoi == THONG_TIN_HUY.TOKHAIXUAT)
                    {
                        #region Hủy khai báo

                        if (nodeRootSXXK.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            KetQuaXuLy kqxl = new KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = "Hủy khai báo";
                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nNgày hủy: {2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), DateTime.Now.ToString())
                                + "\r\n" + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText); kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();

                            this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.ActionStatus = -1;
                            this.SoTiepNhan = 0;
                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                            this.Update();
                            Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.HuyToKhai, kqxl.NoiDung);

                        }

                        #endregion
                    }
                    else if (strTraLoi == LAY_THONG_TIN.TOKHAI)
                    {
                        #region Nhận trạng thái hồ sơ
                        //XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        //if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "DA_XU_LY")
                        //{
                        //    string phanHoi = LayPhanHoiTQDTKhaiBao(pass, doc.InnerXml);

                        //    if (phanHoi.Length != 0)
                        //    {
                        //        XmlDocument docPH = new XmlDocument();
                        //        docPH.LoadXml(phanHoi);

                        //        XmlNode nodeTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                        //        if (nodeTuChoi.Attributes.GetNamedItem("TuChoi") != null
                        //            && nodeTuChoi.Attributes["TuChoi"].Value == "yes")
                        //        {
                        //            //nodeTuChoi.InnerText;
                        //            KetQuaXuLy kqxl = new KetQuaXuLy();
                        //            kqxl.ItemID = this.ID;
                        //            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        //            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        //            kqxl.LoaiThongDiep = "Hủy khai báo";
                        //            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nNgày hủy: {2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), DateTime.Now.ToString())
                        //                + "\r\n" + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText); kqxl.Ngay = DateTime.Now;
                        //            kqxl.Insert();

                        //            this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        //            this.SoTiepNhan = 0;
                        //            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        //            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                        //            this.Update();
                        //        }
                        //    }
                        //    else
                        //    {
                        //        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        //        this.SoToKhai = Convert.ToInt32(nodeDuLieu.Attributes["SO_TK"].Value);
                        //        this.NgayDangKy = Convert.ToDateTime(nodeDuLieu.Attributes["NGAY_DK"].Value);
                        //        //TransgferDataToSXXK();
                        //        this.Update();


                        //        KetQuaXuLy kqxl = new KetQuaXuLy();
                        //        kqxl.ItemID = this.ID;
                        //        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        //        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        //        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                        //        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                        //        kqxl.Ngay = DateTime.Now;
                        //        kqxl.Insert();
                        //    }
                        //}
                        //else if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "TU_CHOI")
                        //{
                        //    this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        //    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        //    this.SoTiepNhan = this.SoToKhai = 0;
                        //    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                        //    this.ActionStatus = 2;
                        //    this.Update();

                        //    KetQuaXuLy kqxl = new KetQuaXuLy();
                        //    kqxl.ItemID = this.ID;
                        //    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        //    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        //    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                        //    kqxl.NoiDung = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        //    kqxl.Ngay = DateTime.Now;
                        //    kqxl.Insert();
                        //}


                        #endregion
                    }
                }
                else if (nodeRootSXXK != null && nodeRootSXXK.Attributes["TRANG_THAI"].Value == "LOI")
                {
                    #region lỗi

                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;

                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";

                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = errorSt;
                    kqxl.NoiDung = docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText; // FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    errorSt = errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi;

                    throw new Exception(errorSt);
                    #endregion
                }
                /*Kiểm tra phân luồng và duyệt TK*/
                else if (nodeRoot.Attributes.GetNamedItem("SOTN") != null)
                {
                    #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                    if (nodeRoot.SelectSingleNode("PHAN_LUONG") == null) //Neu message chua phan luong -> cap nhat SO TN.
                    {
                        this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                        this.NgayTiepNhan = DateTime.Today;
                        this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);

                        this.ActionStatus = 0;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;


                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                        kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo, kqxl.NoiDung);

                    }

                    /*Kiem tra co So to khai*/
                    else if (nodeRoot.Attributes.GetNamedItem("SOTK") != null && this.SoToKhai == 0)
                    {
                        this.SoToKhai = Convert.ToInt32(nodeRoot.Attributes["SOTK"].Value);

                        if (nodeRoot.Attributes.GetNamedItem("NGAYDK") != null)
                            this.NgayDangKy = Convert.ToDateTime(nodeRoot.Attributes["NGAYDK"].Value);

                        this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMDK"].Value);
                        this.ActionStatus = 1;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim());
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo, kqxl.NoiDung);

                    }

                    /*Lay thong tin phan luong*/
                    else if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                    {
                        #region Lấy thông tin phân luồng

                        XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                        this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                        this.HUONGDAN = FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                        this.ActionStatus = 1;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;

                        string tenluong = "Xanh";

                        if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                            tenluong = "Vàng";
                        else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                            tenluong = "Đỏ";

                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();

                        #endregion
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocPhanLuong, kqxl.NoiDung);

                    }

                    this.Update();

                    #endregion Lấy số tiếp nhận của danh sách NPL
                }

                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception(errorSt);
            }

            #region hungtq

            /*
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                //throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                throw new Exception(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");

                //return FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "").ToString();
            }
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
            {
                /*
                 * HungTQ, 14/04/2010.
                 * Kiểm tra trạng thái lỗi trong Nod SXXK.
                 */
            /*
                XmlNode infoLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI");

                XmlNode maLoi = infoLoi.ChildNodes[0];
                string msgMaLoi = maLoi.Attributes["MA_LOI"].Value;
                string msgMucLoi = maLoi.Attributes["MUC_LOI"].Value;
                string msgNguonLoi = maLoi.Attributes["NGUON_LOI"].Value;

                XmlNode moTa = infoLoi.ChildNodes[1];
                string msgMoTa = moTa.InnerText;

                throw new Exception("Mã lỗi: " + msgMaLoi + "\nMô tả: " + msgMoTa + "|" + msgMucLoi);
            }

            bool ok = false;
            XmlNode Resultstn = null;

            if (function == MessgaseFunction.KhaiBao)
            {
                Resultstn = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                this.SoTiepNhan = Resultstn.Attributes["SO_TN"].Value != "" ? Convert.ToInt64(Resultstn.Attributes["SO_TN"].Value) : 0;
                //this.MaLoaiHinh = Resultstn.Attributes["MALH"].Value;
                this.NgayTiepNhan = DateTime.Now.Date; //Convert.ToDateTime(Resultstn.Attributes["Ngay_TN"].Value);
                this.NamDK = Convert.ToInt32(Resultstn.Attributes["NAM_TN"].Value);
                this.TrangThaiXuLy = 0;
                ok = true;
            }
            else if (function == MessgaseFunction.HuyKhaiBao)
            {
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                ok = true;

                //Xoa SendMessage da tao de lay phan hoi
            }

            if (ok)
                this.Update();
            */
            #endregion

            return "";

        }

        #endregion

        //private XmlNode ConvertToKhaiToXMLNHAP(List<HangMauDich> collection, XmlDocument docNPL)
        //{
        //    //load du lieu
        //    //XmlDocument docNPL = new XmlDocument();
        //    //docNPL.Load(@"FPTService\SXXK_TKN_CAPNHAT.xml");
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    XmlNode nodeToKhai = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU/TO_KHAI_NK/TO_KHAI");
        //    XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    nodeHQNhan.Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);


        //    XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    nodeDN.Attributes["TEN_DV"].Value = "";
        //    //thong tin to khai            
        //    nodeToKhai.Attributes["MA_LH"].Value = this.MaLoaiHinh;
        //    nodeToKhai.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    nodeToKhai.Attributes["MA_DV_NK"].Value = this.MaDoanhNghiep;
        //    nodeToKhai.Attributes["MA_DV_UT"].Value = this.MaDonViUT;
        //    nodeToKhai.Attributes["MA_DV_KT"].Value = this.MaDaiLyTTHQ;
        //    nodeToKhai.Attributes["MA_DV_XK"].Value = "";
        //    nodeToKhai.Attributes["TEN_DV_XK"].Value = this.TenDonViDoiTac;
        //    nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep;
        //    if (this.NgayGiayPhep.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_GP"].Value = this.NgayGiayPhep.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_GP"].Value = null;
        //    if (this.NgayHetHanGiayPhep.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_HHGP"].Value = this.NgayHetHanGiayPhep.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_HHGP"].Value = null;
        //    nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong;
        //    if (this.NgayHopDong.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_HD"].Value = this.NgayHopDong.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_HD"].Value = null;
        //    if (this.NgayHetHanHopDong.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_HHHD"].Value = this.NgayHetHanHopDong.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_HHHD"].Value = null;
        //    nodeToKhai.Attributes["SO_HDTM"].Value = this.SoHoaDonThuongMai;
        //    if (this.NgayHoaDonThuongMai.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_HDTM"].Value = this.NgayHoaDonThuongMai.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_HDTM"].Value = null;
        //    nodeToKhai.Attributes["MA_PTVT"].Value = this.PTVT_ID;
        //    nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT;
        //    if (this.NgayDenPTVT.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_DEN"].Value = this.NgayDenPTVT.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_DEN"].Value = null;
        //    nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon;
        //    if (this.NgayVanDon.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_VANDON"].Value = this.NgayVanDon.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_VANDON"].Value = null;
        //    nodeToKhai.Attributes["NUOC_XK"].Value = this.NuocXK_ID;
        //    nodeToKhai.Attributes["CANG_XUAT"].Value = this.DiaDiemXepHang;
        //    nodeToKhai.Attributes["MA_CK_NHAP"].Value = this.CuaKhau_ID;
        //    nodeToKhai.Attributes["MA_DKGH"].Value = this.DKGH_ID;
        //    nodeToKhai.Attributes["MA_NGTE"].Value = this.NguyenTe_ID;
        //    nodeToKhai.Attributes["TY_GIA_VND"].Value = this.TyGiaTinhThue.ToString(f);
        //    nodeToKhai.Attributes["MA_PTTT"].Value = this.PTTT_ID;
        //    nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo;
        //    nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(this.SoContainer20).ToString();
        //    nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(this.SoContainer40).ToString();
        //    nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(this.SoLuongPLTK).ToString();
        //    nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(this.SoKien).ToString();
        //    nodeToKhai.Attributes["TRONG_LUONG"].Value = this.TrongLuong.ToString(f);
        //    nodeToKhai.Attributes["PHI_BH"].Value = this.PhiBaoHiem.ToString(f);
        //    nodeToKhai.Attributes["PHI_VC"].Value = this.PhiVanChuyen.ToString(f);
        //    nodeToKhai.Attributes["LE_PHI_HQ"].Value = this.LePhiHaiQuan.ToString(f);
        //    nodeToKhai.Attributes["CHU_HANG"].Value = this.TenChuHang;


        //    //hang hoa
        //    if (this.HMDCollection == null || this.HMDCollection.Count == 0)
        //    {
        //        HangMauDich hmd = new HangMauDich();
        //        hmd.TKMD_ID = this.ID;
        //        this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
        //    }
        //    XmlNode nodeHang = docNPL.GetElementsByTagName("HANG")[0];
        //    foreach (HangMauDich hmd in this.HMDCollection)
        //    {
        //        XmlNode node = docNPL.CreateElement("HANG.ITEM");
        //        XmlAttribute sttAtt = docNPL.CreateAttribute("STT_HANG");
        //        sttAtt.Value = hmd.SoThuTuHang.ToString();
        //        node.Attributes.Append(sttAtt);

        //        XmlAttribute maAtt = docNPL.CreateAttribute("MA_HANG");
        //        maAtt.Value = hmd.MaPhu;
        //        node.Attributes.Append(maAtt);

        //        XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
        //        MaHSAtt.Value = hmd.MaHS;
        //        node.Attributes.Append(MaHSAtt);

        //        XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_HANG");
        //        TenAtt.Value = hmd.TenHang;
        //        node.Attributes.Append(TenAtt);

        //        XmlAttribute NuocAtt = docNPL.CreateAttribute("NUOC_XX");
        //        NuocAtt.Value = hmd.NuocXX_ID;
        //        node.Attributes.Append(NuocAtt);

        //        XmlAttribute SoLuongAtt = docNPL.CreateAttribute("LUONG");
        //        SoLuongAtt.Value = hmd.SoLuong.ToString(f);
        //        node.Attributes.Append(SoLuongAtt);

        //        XmlAttribute Ma_DVTAtt = docNPL.CreateAttribute("MA_DVT");
        //        Ma_DVTAtt.Value = hmd.DVT_ID;
        //        node.Attributes.Append(Ma_DVTAtt);

        //        XmlAttribute DonGiaAtt = docNPL.CreateAttribute("DGIA_NT");
        //        DonGiaAtt.Value = hmd.DonGiaKB.ToString(f);
        //        node.Attributes.Append(DonGiaAtt);

        //        XmlAttribute TriGiaAtt = docNPL.CreateAttribute("TGIA_NT");
        //        TriGiaAtt.Value = hmd.TriGiaKB.ToString(f); ;
        //        node.Attributes.Append(TriGiaAtt);

        //        XmlAttribute DonGiaVNDAtt = docNPL.CreateAttribute("DGIA_TT_VND");
        //        DonGiaVNDAtt.Value = hmd.DonGiaTT.ToString(f); ;
        //        node.Attributes.Append(DonGiaVNDAtt);

        //        XmlAttribute TriGiaVNDAtt = docNPL.CreateAttribute("TGIA_TT_VND");
        //        TriGiaVNDAtt.Value = hmd.TriGiaTT.ToString(f); ;
        //        node.Attributes.Append(TriGiaVNDAtt);

        //        XmlAttribute TS_XNKAtt = docNPL.CreateAttribute("TS_XNK");
        //        TS_XNKAtt.Value = hmd.ThueSuatXNK.ToString(f);
        //        node.Attributes.Append(TS_XNKAtt);

        //        XmlAttribute TS_TTDBAtt = docNPL.CreateAttribute("TS_TTDB");
        //        TS_TTDBAtt.Value = hmd.ThueSuatTTDB.ToString(f);
        //        node.Attributes.Append(TS_TTDBAtt);

        //        XmlAttribute TS_VSTAtt = docNPL.CreateAttribute("TS_VAT");
        //        TS_VSTAtt.Value = hmd.ThueGTGT.ToString(f);
        //        node.Attributes.Append(TS_VSTAtt);

        //        XmlAttribute TL_PhuThuAtt = docNPL.CreateAttribute("TL_PHU_THU");
        //        TL_PhuThuAtt.Value = hmd.TyLeThuKhac.ToString(f);
        //        node.Attributes.Append(TL_PhuThuAtt);

        //        XmlAttribute ThueXNKAtt = docNPL.CreateAttribute("THUE_XNK");
        //        ThueXNKAtt.Value = hmd.ThueXNK.ToString(f);
        //        node.Attributes.Append(ThueXNKAtt);

        //        XmlAttribute ThueTTDBAtt = docNPL.CreateAttribute("THUE_TTDB");
        //        ThueTTDBAtt.Value = hmd.ThueTTDB.ToString(f);
        //        node.Attributes.Append(ThueTTDBAtt);

        //        XmlAttribute ThueVATAtt = docNPL.CreateAttribute("THUE_VAT");
        //        ThueVATAtt.Value = hmd.ThueGTGT.ToString(f);
        //        node.Attributes.Append(ThueVATAtt);

        //        XmlAttribute TPhuThuAtt = docNPL.CreateAttribute("PHU_THU");
        //        TPhuThuAtt.Value = hmd.PhuThu.ToString(f);
        //        node.Attributes.Append(TPhuThuAtt);

        //        nodeHang.AppendChild(node);
        //    }
        //    return nodeToKhai;
        //}

        //private XmlNode ConvertToKhaiToXMLXUAT(List<HangMauDich> collection, XmlDocument docNPL)
        //{
        //    //load du lieu
        //    //XmlDocument docNPL = new XmlDocument();
        //    //docNPL.Load(@"FPTService\SXXK_TKX_CAPNHAT.xml");
        //    NumberFormatInfo f = new NumberFormatInfo();
        //    f.NumberDecimalSeparator = ".";
        //    f.NumberGroupSeparator = ",";
        //    XmlNode nodeToKhai = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU/TO_KHAI_XK/TO_KHAI");
        //    //thong tin to khai

        //    XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    nodeHQNhan.Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

        //    XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    nodeDN.Attributes["TEN_DV"].Value = "";

        //    //thong tin to khai            
        //    nodeToKhai.Attributes["XUAT_NPL_SP"].Value = this.LoaiHangHoa;
        //    nodeToKhai.Attributes["MA_LH"].Value = this.MaLoaiHinh;
        //    nodeToKhai.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    nodeToKhai.Attributes["MA_DV_XK"].Value = this.MaDoanhNghiep;
        //    nodeToKhai.Attributes["MA_DV_UT"].Value = this.MaDonViUT;
        //    nodeToKhai.Attributes["MA_DV_NK"].Value = "";
        //    nodeToKhai.Attributes["MA_DV_KT"].Value = this.MaDaiLyTTHQ;
        //    nodeToKhai.Attributes["TEN_DV_NK"].Value = this.TenDonViDoiTac;
        //    nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep;
        //    if (this.NgayGiayPhep.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_GP"].Value = this.NgayGiayPhep.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_GP"].Value = null;
        //    if (this.NgayHetHanGiayPhep.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_HHGP"].Value = this.NgayHetHanGiayPhep.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_HHGP"].Value = null;
        //    nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong;
        //    if (this.NgayHopDong.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_HD"].Value = this.NgayHopDong.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_HD"].Value = null;
        //    if (this.NgayHetHanHopDong.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_HHHD"].Value = this.NgayHetHanHopDong.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_HHHD"].Value = null;
        //    nodeToKhai.Attributes["SO_HDTM"].Value = this.SoHoaDonThuongMai;
        //    if (this.NgayHoaDonThuongMai.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_HDTM"].Value = this.NgayHoaDonThuongMai.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_HDTM"].Value = null;
        //    nodeToKhai.Attributes["MA_PTVT"].Value = this.PTVT_ID;
        //    nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT;
        //    if (this.NgayDenPTVT.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_DEN"].Value = this.NgayDenPTVT.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_DEN"].Value = null;
        //    nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon;
        //    if (this.NgayVanDon.Year > 1900)
        //        nodeToKhai.Attributes["NGAY_VANDON"].Value = this.NgayVanDon.ToString("yyyy-MM-dd");
        //    else
        //        nodeToKhai.Attributes["NGAY_VANDON"].Value = null;
        //    nodeToKhai.Attributes["NUOC_NK"].Value = this.NuocNK_ID;
        //    nodeToKhai.Attributes["MA_CK_XUAT"].Value = this.CuaKhau_ID;
        //    nodeToKhai.Attributes["MA_DKGH"].Value = this.DKGH_ID;
        //    nodeToKhai.Attributes["MA_NGTE"].Value = this.NguyenTe_ID;
        //    nodeToKhai.Attributes["TY_GIA_VND"].Value = this.TyGiaTinhThue.ToString(f);
        //    nodeToKhai.Attributes["MA_PTTT"].Value = this.PTTT_ID;
        //    nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo;
        //    nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(this.SoContainer20).ToString();
        //    nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(this.SoContainer40).ToString();
        //    nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(this.SoLuongPLTK).ToString();
        //    nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(this.SoKien).ToString();
        //    nodeToKhai.Attributes["TRONG_LUONG"].Value = this.TrongLuong.ToString(f);
        //    nodeToKhai.Attributes["PHI_BH"].Value = this.PhiBaoHiem.ToString(f);
        //    nodeToKhai.Attributes["PHI_VC"].Value = this.PhiVanChuyen.ToString(f);
        //    nodeToKhai.Attributes["LE_PHI_HQ"].Value = this.LePhiHaiQuan.ToString(f);
        //    nodeToKhai.Attributes["CHU_HANG"].Value = this.TenChuHang;

        //    //hang hoa
        //    XmlNode nodeHANG = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU/TO_KHAI_XK/TO_KHAI/HANG");
        //    foreach (HangMauDich hmd in this.HMDCollection)
        //    {
        //        XmlNode node = docNPL.CreateElement("HANG.ITEM");
        //        XmlAttribute sttAtt = docNPL.CreateAttribute("STT_HANG");
        //        sttAtt.Value = hmd.SoThuTuHang.ToString();
        //        node.Attributes.Append(sttAtt);

        //        XmlAttribute maAtt = docNPL.CreateAttribute("MA_HANG");
        //        maAtt.Value = hmd.MaPhu;
        //        node.Attributes.Append(maAtt);

        //        XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
        //        MaHSAtt.Value = hmd.MaHS;
        //        node.Attributes.Append(MaHSAtt);

        //        XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_HANG");
        //        TenAtt.Value = hmd.TenHang;
        //        node.Attributes.Append(TenAtt);

        //        XmlAttribute NuocAtt = docNPL.CreateAttribute("NUOC_XX");
        //        NuocAtt.Value = hmd.NuocXX_ID;
        //        node.Attributes.Append(NuocAtt);

        //        XmlAttribute SoLuongAtt = docNPL.CreateAttribute("LUONG");
        //        SoLuongAtt.Value = hmd.SoLuong.ToString(f);
        //        node.Attributes.Append(SoLuongAtt);

        //        XmlAttribute Ma_DVTAtt = docNPL.CreateAttribute("MA_DVT");
        //        Ma_DVTAtt.Value = hmd.DVT_ID;
        //        node.Attributes.Append(Ma_DVTAtt);

        //        XmlAttribute DonGiaAtt = docNPL.CreateAttribute("DGIA_NT");
        //        DonGiaAtt.Value = hmd.DonGiaKB.ToString(f);
        //        node.Attributes.Append(DonGiaAtt);

        //        XmlAttribute TriGiaAtt = docNPL.CreateAttribute("TGIA_NT");
        //        TriGiaAtt.Value = hmd.TriGiaKB.ToString(f); ;
        //        node.Attributes.Append(TriGiaAtt);

        //        XmlAttribute DonGiaVNDAtt = docNPL.CreateAttribute("DGIA_TT_VND");
        //        DonGiaVNDAtt.Value = hmd.DonGiaTT.ToString(f); ;
        //        node.Attributes.Append(DonGiaVNDAtt);

        //        XmlAttribute TriGiaVNDAtt = docNPL.CreateAttribute("TGIA_TT_VND");
        //        TriGiaVNDAtt.Value = hmd.TriGiaTT.ToString(f); ;
        //        node.Attributes.Append(TriGiaVNDAtt);

        //        XmlAttribute TS_XNKAtt = docNPL.CreateAttribute("TS_XNK");
        //        TS_XNKAtt.Value = hmd.ThueSuatXNK.ToString(f);
        //        node.Attributes.Append(TS_XNKAtt);

        //        XmlAttribute TS_TTDBAtt = docNPL.CreateAttribute("TS_TTDB");
        //        TS_TTDBAtt.Value = hmd.ThueSuatTTDB.ToString(f);
        //        node.Attributes.Append(TS_TTDBAtt);

        //        XmlAttribute TS_VSTAtt = docNPL.CreateAttribute("TS_VAT");
        //        TS_VSTAtt.Value = hmd.ThueGTGT.ToString(f);
        //        node.Attributes.Append(TS_VSTAtt);

        //        XmlAttribute TL_PhuThuAtt = docNPL.CreateAttribute("TL_PHU_THU");
        //        TL_PhuThuAtt.Value = hmd.TyLeThuKhac.ToString(f);
        //        node.Attributes.Append(TL_PhuThuAtt);

        //        XmlAttribute ThueXNKAtt = docNPL.CreateAttribute("THUE_XNK");
        //        ThueXNKAtt.Value = hmd.ThueXNK.ToString(f);
        //        node.Attributes.Append(ThueXNKAtt);

        //        XmlAttribute ThueTTDBAtt = docNPL.CreateAttribute("THUE_TTDB");
        //        ThueTTDBAtt.Value = hmd.ThueTTDB.ToString(f);
        //        node.Attributes.Append(ThueTTDBAtt);

        //        XmlAttribute ThueVATAtt = docNPL.CreateAttribute("THUE_VAT");
        //        ThueVATAtt.Value = hmd.ThueGTGT.ToString(f);
        //        node.Attributes.Append(ThueVATAtt);

        //        XmlAttribute TPhuThuAtt = docNPL.CreateAttribute("PHU_THU");
        //        TPhuThuAtt.Value = hmd.PhuThu.ToString(f);
        //        node.Attributes.Append(TPhuThuAtt);

        //        nodeHANG.AppendChild(node);
        //    }
        //    return nodeToKhai;
        //}
        public void ReSetSoLuong()
        {
            foreach (HangMauDich item in HMDCollection)
            {
                string spUpdate = "update t_SXXK_HangMauDich set soluong=0 where SoToKhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and NamDangKy=@NamDangKy and MaPhu=@MaPhu";
                SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spUpdate);
                this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
                this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
                this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuan);
                this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this.NamDK);
                this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, item.MaPhu);
                try
                {
                    this.db.ExecuteNonQuery(dbCommand);
                }
                catch
                {

                }
            }
        }
        /// <summary>
        /// Đồng bộ từ khai báo xuống đã đăng ký
        /// </summary>
        public void TransgferDataToSXXK()
        {
            #region GET DATA

            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted);

                try
                {
                    //Lay thong tin to khai da dang ky cu (Neu co)
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkDuyetTemp = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDuyetTemp.LoadBy(this.MaHaiQuan.Trim(), this.SoToKhai, this.MaLoaiHinh, this.NgayDangKy.Year);

                    if (tkDuyetTemp != null && tkDuyetTemp.SoToKhai != 0 && tkDuyetTemp.MaLoaiHinh != "" && tkDuyetTemp.NamDangKy != 0 && tkDuyetTemp.MaHaiQuan != "")
                    {
                       // transaction.Rollback();
                        throw new Exception(string.Format("Đã tồn tại:\r\nTờ khai số: {0} \r\nMã Loại hình: {1}\r\n Năm đăng ký: {2}\r\nHải quan: {3}", this.SoToKhai, this.MaLoaiHinh, this.NgayDangKy.Year, this.MaHaiQuan.Trim()));
                    }

                    this.UpdateTransaction(transaction);
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkDuyet = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
                    tkDuyet.PhanLuong = this.PhanLuong;
                    tkDuyet.MaHaiQuan = this.MaHaiQuan.Trim();
                    tkDuyet.MaLoaiHinh = this.MaLoaiHinh;
                    tkDuyet.NamDangKy = (short)this.NgayDangKy.Year;
                    tkDuyet.NgayDangKy = this.NgayDangKy;
                    tkDuyet.SoToKhai = this.SoToKhai;
                    tkDuyet.TenDonViDoiTac = this.TenDonViDoiTac;
                    tkDuyet.NgayGiayPhep = this.NgayGiayPhep;
                    tkDuyet.NgayHetHanGiayPhep = this.NgayHetHanGiayPhep;
                    tkDuyet.SoHopDong = this.SoHopDong;
                    tkDuyet.NgayHopDong = this.NgayHopDong;
                    tkDuyet.NgayHetHanHopDong = this.NgayHetHanHopDong;
                    tkDuyet.SoHoaDonThuongMai = this.SoHoaDonThuongMai;
                    tkDuyet.NgayHoaDonThuongMai = this.NgayHoaDonThuongMai;
                    tkDuyet.PTVT_ID = this.PTVT_ID;
                    tkDuyet.SoHieuPTVT = this.SoHieuPTVT;
                    tkDuyet.NgayDenPTVT = this.NgayDenPTVT;
                    tkDuyet.LoaiVanDon = this.LoaiVanDon;
                    tkDuyet.NgayVanDon = this.NgayVanDon;
                    tkDuyet.NuocXK_ID = this.NuocXK_ID;
                    tkDuyet.NuocNK_ID = this.NuocNK_ID;
                    tkDuyet.DiaDiemXepHang = this.DiaDiemXepHang;
                    tkDuyet.DKGH_ID = this.DKGH_ID;
                    tkDuyet.CuaKhau_ID = this.CuaKhau_ID;
                    tkDuyet.NguyenTe_ID = this.NguyenTe_ID;
                    tkDuyet.TyGiaTinhThue = this.TyGiaTinhThue;
                    tkDuyet.TyGiaUSD = this.TyGiaUSD;
                    tkDuyet.PTTT_ID = this.PTTT_ID;
                    tkDuyet.SoHang = this.SoHang;
                    tkDuyet.SoLuongPLTK = this.SoLuongPLTK;
                    tkDuyet.TenChuHang = this.TenChuHang;
                    tkDuyet.SoContainer20 = this.SoContainer20;
                    tkDuyet.SoContainer40 = this.SoContainer40;
                    tkDuyet.SoKien = this.SoKien;
                    tkDuyet.TrongLuong = this.TrongLuong;
                    tkDuyet.TongTriGiaKhaiBao = this.TongTriGiaKhaiBao;
                    tkDuyet.TongTriGiaTinhThue = this.TongTriGiaTinhThue;
                    tkDuyet.LoaiToKhaiGiaCong = this.LoaiToKhaiGiaCong;
                    tkDuyet.LePhiHaiQuan = this.LePhiHaiQuan;
                    tkDuyet.PhiBaoHiem = this.PhiBaoHiem;
                    tkDuyet.PhiVanChuyen = this.PhiVanChuyen;
                    tkDuyet.ThanhLy = "";
                    tkDuyet.Xuat_NPL_SP = this.LoaiHangHoa;
                    tkDuyet.ChungTu = this.GiayTo;
                    tkDuyet.TrangThaiThanhKhoan = "D";
                    tkDuyet.PhiKhac = this.PhiKhac;

                    tkDuyet.NgayHoanThanh = tkDuyet.NgayDangKy;

                    tkDuyet.HeSoNhan = this.HeSoNhan;
                    if (tkDuyet.MaLoaiHinh.StartsWith("XSX"))
                        tkDuyet.TrangThai = 1;//chua dc phan bo

                    //Bo sung thong tin bi thieu. Hungtq 25/05/2012.
                    tkDuyet.TrongLuongNet = this.TrongLuongNet;

                    //Luu to khai tren bang du lieu cung DATABSE TQDT.
                    tkDuyet.InsertUpdateTransaction(transaction);

                    #region Hang

                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        BLL.SXXK.ToKhai.HangMauDich hmdDuyet = new Company.BLL.SXXK.ToKhai.HangMauDich();
                        hmdDuyet.DonGiaKB = hmd.DonGiaKB;
                        hmdDuyet.DonGiaTT = hmd.DonGiaTT;
                        hmdDuyet.DVT_ID = hmd.DVT_ID;
                        hmdDuyet.MaHaiQuan = this.MaHaiQuan.Trim();
                        hmdDuyet.MaHS = hmd.MaHS;
                        hmdDuyet.MaLoaiHinh = this.MaLoaiHinh;
                        hmdDuyet.MaPhu = hmd.MaPhu;
                        hmdDuyet.MienThue = hmd.MienThue;
                        hmdDuyet.NamDangKy = (short)this.NgayDangKy.Year;
                        hmdDuyet.NuocXX_ID = hmd.NuocXX_ID;
                        hmdDuyet.PhuThu = hmd.PhuThu;
                        hmdDuyet.SoLuong = hmd.SoLuong;
                        hmdDuyet.SoThuTuHang = (short)hmd.SoThuTuHang;
                        hmdDuyet.SoToKhai = this.SoToKhai;
                        hmdDuyet.TenHang = hmd.TenHang;
                        hmdDuyet.ThueGTGT = hmd.ThueGTGT;
                        hmdDuyet.ThueSuatGTGT = hmd.ThueSuatGTGT;
                        hmdDuyet.ThueSuatTTDB = hmd.ThueSuatTTDB;
                        hmdDuyet.ThueSuatXNK = hmd.ThueSuatXNK;
                        hmdDuyet.ThueTTDB = hmd.ThueTTDB;
                        hmdDuyet.ThueXNK = hmd.ThueXNK;
                        hmdDuyet.TriGiaKB = hmd.TriGiaKB;
                        hmdDuyet.TriGiaKB_VND = hmd.TriGiaKB_VND;
                        hmdDuyet.TriGiaThuKhac = hmd.TriGiaThuKhac;
                        hmdDuyet.TriGiaTT = hmd.TriGiaTT;
                        hmdDuyet.TyLeThuKhac = hmd.TyLeThuKhac;

                        if (tkDuyet.MaLoaiHinh.StartsWith("NSX"))
                        {
                            if (hmdDuyet.LoadByMaHang(transaction))
                            {
                                hmdDuyet.SoLuong += hmd.SoLuong;
                                hmdDuyet.ThueXNK += hmd.ThueXNK;
                                hmdDuyet.TriGiaKB += hmd.TriGiaKB;
                                hmdDuyet.DonGiaKB = hmdDuyet.TriGiaKB / hmdDuyet.SoLuong;
                                hmdDuyet.DonGiaTT = hmdDuyet.DonGiaKB * tkDuyet.TyGiaTinhThue;
                                if (hmdDuyet.ThueXNK > 0)
                                    hmdDuyet.TriGiaTT = hmdDuyet.ThueXNK * 100 / hmdDuyet.ThueSuatXNK;
                                else
                                    hmdDuyet.TriGiaTT = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;
                                hmdDuyet.TriGiaKB_VND = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;
                            }
                        }

                        //Luu hang mau dich cua to khai tren bang du lieu cung DATABSE TQDT.
                        hmdDuyet.InsertUpdateTransaction(transaction);

                        if (tkDuyet.MaLoaiHinh.StartsWith("NSX"))
                        {
                            //cap nhat vao hang dieu chinh
                            BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                            nplNhapTon.SoToKhai = tkDuyet.SoToKhai;
                            nplNhapTon.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                            nplNhapTon.NamDangKy = tkDuyet.NamDangKy;
                            nplNhapTon.MaHaiQuan = tkDuyet.MaHaiQuan;
                            nplNhapTon.MaNPL = hmdDuyet.MaPhu;
                            nplNhapTon.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                            nplNhapTon.Luong = hmdDuyet.SoLuong;
                            nplNhapTon.Ton = hmdDuyet.SoLuong;
                            nplNhapTon.ThueXNK = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTon.ThueXNKTon = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTon.ThueTTDB = Convert.ToDouble(hmdDuyet.ThueTTDB);
                            nplNhapTon.ThueVAT = Convert.ToDouble(hmdDuyet.ThueGTGT);
                            nplNhapTon.PhuThu = Convert.ToDouble(hmdDuyet.PhuThu);
                            nplNhapTon.ThueCLGia = Convert.ToDouble(hmdDuyet.TriGiaThuKhac);

                            //Luu NPL ton cua to khai tren bang du lieu cung DATABSE TQDT.
                            nplNhapTon.InsertUpdateTransaction(transaction);

                            //CAP NHAT VAO LUONG TON THUC TE
                            BLL.SXXK.NPLNhapTonThucTe nplNhapTonThucTe = new Company.BLL.SXXK.NPLNhapTonThucTe();
                            nplNhapTonThucTe.SoToKhai = tkDuyet.SoToKhai;
                            nplNhapTonThucTe.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                            nplNhapTonThucTe.NamDangKy = tkDuyet.NamDangKy;
                            nplNhapTonThucTe.MaHaiQuan = tkDuyet.MaHaiQuan;
                            nplNhapTonThucTe.MaNPL = hmdDuyet.MaPhu;
                            nplNhapTonThucTe.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                            nplNhapTonThucTe.Luong = hmdDuyet.SoLuong;
                            nplNhapTonThucTe.Ton = hmdDuyet.SoLuong;
                            nplNhapTonThucTe.ThueXNK = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTonThucTe.ThueXNKTon = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTonThucTe.ThueTTDB = Convert.ToDouble(hmdDuyet.ThueTTDB);
                            nplNhapTonThucTe.ThueVAT = Convert.ToDouble(hmdDuyet.ThueGTGT);
                            nplNhapTonThucTe.PhuThu = Convert.ToDouble(hmdDuyet.PhuThu);
                            nplNhapTonThucTe.ThueCLGia = Convert.ToDouble(hmdDuyet.TriGiaThuKhac); ;
                            nplNhapTonThucTe.NgayDangKy = this.NgayDangKy;

                            //Luu NPL ton thuc te cua to khai tren bang du lieu cung DATABSE TQDT.
                            nplNhapTonThucTe.InsertUpdate(transaction);
                        }
                    }

                    #endregion

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    //Hungtq update 16/07/2012.
                    string loaiHinh = "SXXK";
                    string appVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Windows.Forms.Application.ExecutablePath).ProductVersion.ToString();
                    string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                    dataVersion = (dataVersion != "" ? dataVersion : "?");

                    Company.KDT.SHARE.Components.Globals.sendEmail("", "", "", this.TenDoanhNghiep, this.MaDoanhNghiep, this.MaHaiQuan, "", "", "", string.Empty, string.Empty, loaiHinh, appVersion, dataVersion, string.Empty, new System.Collections.Generic.List<string>());

                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            #endregion
        }
        public void TransgferDataToSXXK(string connectionString)
        {
            #region GET DATA

            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted);

                try
                {
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkDuyet = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
                    tkDuyet.PhanLuong = this.PhanLuong;
                    tkDuyet.MaHaiQuan = this.MaHaiQuan.Trim();
                    tkDuyet.MaLoaiHinh = this.MaLoaiHinh;
                    tkDuyet.NamDangKy = (short)this.NgayDangKy.Year;
                    tkDuyet.NgayDangKy = this.NgayDangKy;
                    tkDuyet.SoToKhai = this.SoToKhai;

                    /*kiem tra da ton tai to khai nay trong Dang ky chua?. Neu da co roi -> khong thuc hien chuyen trang thai to khai nua, tranh trung lap du lieu trong THANH KHOAN*/
                    if (tkDuyet.Load(transaction, db))
                        return;

                    tkDuyet.TenDonViDoiTac = this.TenDonViDoiTac;
                    tkDuyet.NgayGiayPhep = this.NgayGiayPhep;
                    tkDuyet.NgayHetHanGiayPhep = this.NgayHetHanGiayPhep;
                    tkDuyet.SoHopDong = this.SoHopDong;
                    tkDuyet.NgayHopDong = this.NgayHopDong;
                    tkDuyet.NgayHetHanHopDong = this.NgayHetHanHopDong;
                    tkDuyet.SoHoaDonThuongMai = this.SoHoaDonThuongMai;
                    tkDuyet.NgayHoaDonThuongMai = this.NgayHoaDonThuongMai;
                    tkDuyet.PTVT_ID = this.PTVT_ID;
                    tkDuyet.SoHieuPTVT = this.SoHieuPTVT;
                    tkDuyet.NgayDenPTVT = this.NgayDenPTVT;
                    tkDuyet.LoaiVanDon = this.LoaiVanDon;
                    tkDuyet.NgayVanDon = this.NgayVanDon;
                    tkDuyet.NuocXK_ID = this.NuocXK_ID;
                    tkDuyet.NuocNK_ID = this.NuocNK_ID;
                    tkDuyet.DiaDiemXepHang = this.DiaDiemXepHang;
                    tkDuyet.DKGH_ID = this.DKGH_ID;
                    tkDuyet.CuaKhau_ID = this.CuaKhau_ID;
                    tkDuyet.NguyenTe_ID = this.NguyenTe_ID;
                    tkDuyet.TyGiaTinhThue = this.TyGiaTinhThue;
                    tkDuyet.TyGiaUSD = this.TyGiaUSD;
                    tkDuyet.PTTT_ID = this.PTTT_ID;
                    tkDuyet.SoHang = this.SoHang;
                    tkDuyet.SoLuongPLTK = this.SoLuongPLTK;
                    tkDuyet.TenChuHang = this.TenChuHang;
                    tkDuyet.SoContainer20 = this.SoContainer20;
                    tkDuyet.SoContainer40 = this.SoContainer40;
                    tkDuyet.SoKien = this.SoKien;
                    tkDuyet.TrongLuong = this.TrongLuong;
                    tkDuyet.TongTriGiaKhaiBao = this.TongTriGiaKhaiBao;
                    tkDuyet.TongTriGiaTinhThue = this.TongTriGiaTinhThue;
                    tkDuyet.LoaiToKhaiGiaCong = this.LoaiToKhaiGiaCong;
                    tkDuyet.LePhiHaiQuan = this.LePhiHaiQuan;
                    tkDuyet.PhiBaoHiem = this.PhiBaoHiem;
                    tkDuyet.PhiVanChuyen = this.PhiVanChuyen;
                    tkDuyet.ThanhLy = "";
                    tkDuyet.Xuat_NPL_SP = this.LoaiHangHoa;
                    tkDuyet.ChungTu = this.GiayTo;
                    tkDuyet.TrangThaiThanhKhoan = "D";
                    tkDuyet.PhiKhac = this.PhiKhac;
                    tkDuyet.NgayHoanThanh = tkDuyet.NgayDangKy;
                    tkDuyet.HeSoNhan = this.HeSoNhan;
                    if (tkDuyet.MaLoaiHinh.StartsWith("XSX"))
                        tkDuyet.TrangThai = 1;//chua dc phan bo

                    //Luu to khai tren bang du lieu cung DATABSE TQDT.
                    tkDuyet.InsertUpdateTransaction(transaction, db);

                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        BLL.SXXK.ToKhai.HangMauDich hmdDuyet = new Company.BLL.SXXK.ToKhai.HangMauDich();
                        hmdDuyet.DonGiaKB = hmd.DonGiaKB;
                        hmdDuyet.DonGiaTT = hmd.DonGiaTT;
                        hmdDuyet.DVT_ID = hmd.DVT_ID;
                        hmdDuyet.MaHaiQuan = this.MaHaiQuan.Trim();
                        hmdDuyet.MaHS = hmd.MaHS;
                        hmdDuyet.MaLoaiHinh = this.MaLoaiHinh;
                        hmdDuyet.MaPhu = hmd.MaPhu;
                        hmdDuyet.MienThue = hmd.MienThue;
                        hmdDuyet.NamDangKy = (short)this.NgayDangKy.Year;
                        hmdDuyet.NuocXX_ID = hmd.NuocXX_ID;
                        hmdDuyet.PhuThu = hmd.PhuThu;
                        hmdDuyet.SoLuong = hmd.SoLuong;
                        hmdDuyet.SoThuTuHang = (short)hmd.SoThuTuHang;
                        hmdDuyet.SoToKhai = this.SoToKhai;
                        hmdDuyet.TenHang = hmd.TenHang;
                        hmdDuyet.ThueGTGT = hmd.ThueGTGT;
                        hmdDuyet.ThueSuatGTGT = hmd.ThueSuatGTGT;
                        hmdDuyet.ThueSuatTTDB = hmd.ThueSuatTTDB;
                        hmdDuyet.ThueSuatXNK = hmd.ThueSuatXNK;
                        hmdDuyet.ThueTTDB = hmd.ThueTTDB;
                        hmdDuyet.ThueXNK = hmd.ThueXNK;
                        hmdDuyet.TriGiaKB = hmd.TriGiaKB;
                        hmdDuyet.TriGiaKB_VND = hmd.TriGiaKB_VND;
                        hmdDuyet.TriGiaThuKhac = hmd.TriGiaThuKhac;
                        hmdDuyet.TriGiaTT = hmd.TriGiaTT;
                        hmdDuyet.TyLeThuKhac = hmd.TyLeThuKhac;
                        if (tkDuyet.MaLoaiHinh.StartsWith("NSX"))
                        {
                            if (hmdDuyet.LoadByMaHang(transaction, db) != null)
                            {
                                hmdDuyet.SoLuong += hmd.SoLuong;
                                hmdDuyet.ThueXNK += hmd.ThueXNK;
                                hmdDuyet.TriGiaKB += hmd.TriGiaKB;
                                hmdDuyet.DonGiaKB = hmdDuyet.TriGiaKB / hmdDuyet.SoLuong;
                                hmdDuyet.DonGiaTT = hmdDuyet.DonGiaKB * tkDuyet.TyGiaTinhThue;
                                if (hmdDuyet.ThueXNK > 0)
                                    hmdDuyet.TriGiaTT = hmdDuyet.ThueXNK * 100 / hmdDuyet.ThueSuatXNK;
                                else
                                    hmdDuyet.TriGiaTT = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;
                                hmdDuyet.TriGiaKB_VND = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;
                            }
                        }

                        //Luu hang mau dich cua to khai tren bang du lieu cung DATABSE TQDT.
                        hmdDuyet.InsertUpdateTransaction(transaction, db);

                        if (tkDuyet.MaLoaiHinh.StartsWith("NSX"))
                        {
                            //cap nhat vao hang dieu chinh
                            BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                            nplNhapTon.SoToKhai = tkDuyet.SoToKhai;
                            nplNhapTon.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                            nplNhapTon.NamDangKy = tkDuyet.NamDangKy;
                            nplNhapTon.MaHaiQuan = tkDuyet.MaHaiQuan;
                            nplNhapTon.MaNPL = hmdDuyet.MaPhu;
                            nplNhapTon.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                            nplNhapTon.Luong = hmdDuyet.SoLuong;
                            nplNhapTon.Ton = hmdDuyet.SoLuong;
                            nplNhapTon.ThueXNK = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTon.ThueXNKTon = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTon.ThueTTDB = Convert.ToDouble(hmdDuyet.ThueTTDB);
                            nplNhapTon.ThueVAT = Convert.ToDouble(hmdDuyet.ThueGTGT);
                            nplNhapTon.PhuThu = Convert.ToDouble(hmdDuyet.PhuThu);
                            nplNhapTon.ThueCLGia = Convert.ToDouble(hmdDuyet.TriGiaThuKhac);

                            //Luu NPL ton cua to khai tren bang du lieu cung DATABSE TQDT.
                            nplNhapTon.InsertUpdateTransaction(transaction, db);

                            //CAP NHAT VAO LUONG TON THUC TE
                            BLL.SXXK.NPLNhapTonThucTe nplNhapTonThucTe = new Company.BLL.SXXK.NPLNhapTonThucTe();
                            nplNhapTonThucTe.SoToKhai = tkDuyet.SoToKhai;
                            nplNhapTonThucTe.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                            nplNhapTonThucTe.NamDangKy = tkDuyet.NamDangKy;
                            nplNhapTonThucTe.MaHaiQuan = tkDuyet.MaHaiQuan;
                            nplNhapTonThucTe.MaNPL = hmdDuyet.MaPhu;
                            nplNhapTonThucTe.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                            nplNhapTonThucTe.Luong = hmdDuyet.SoLuong;
                            nplNhapTonThucTe.Ton = hmdDuyet.SoLuong;
                            nplNhapTonThucTe.ThueXNK = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTonThucTe.ThueXNKTon = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTonThucTe.ThueTTDB = Convert.ToDouble(hmdDuyet.ThueTTDB);
                            nplNhapTonThucTe.ThueVAT = Convert.ToDouble(hmdDuyet.ThueGTGT);
                            nplNhapTonThucTe.PhuThu = Convert.ToDouble(hmdDuyet.PhuThu);
                            nplNhapTonThucTe.ThueCLGia = Convert.ToDouble(hmdDuyet.TriGiaThuKhac); ;
                            nplNhapTonThucTe.NgayDangKy = this.NgayDangKy;

                            //Luu NPL ton thuc te cua to khai tren bang du lieu cung DATABSE TQDT.
                            nplNhapTonThucTe.InsertUpdate(transaction, db);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            #endregion
        }
        public void TransgferDataToSXXK(SqlTransaction transaction, SqlDatabase db)
        {
            #region GET DATA

            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection(transaction, db);

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();

                try
                {
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkDuyet = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
                    tkDuyet.PhanLuong = this.PhanLuong;
                    tkDuyet.MaHaiQuan = this.MaHaiQuan.Trim();
                    tkDuyet.MaLoaiHinh = this.MaLoaiHinh;
                    tkDuyet.NamDangKy = (short)this.NgayDangKy.Year;
                    tkDuyet.NgayDangKy = this.NgayDangKy;
                    tkDuyet.SoToKhai = this.SoToKhai;

                    /*kiem tra da ton tai to khai nay trong Dang ky chua?. Neu da co roi -> khong thuc hien chuyen trang thai to khai nua, tranh trung lap du lieu trong THANH KHOAN*/
                    if (tkDuyet.Load(transaction, db))
                        return;

                    tkDuyet.TenDonViDoiTac = this.TenDonViDoiTac;
                    tkDuyet.NgayGiayPhep = this.NgayGiayPhep;
                    tkDuyet.NgayHetHanGiayPhep = this.NgayHetHanGiayPhep;
                    tkDuyet.SoHopDong = this.SoHopDong;
                    tkDuyet.NgayHopDong = this.NgayHopDong;
                    tkDuyet.NgayHetHanHopDong = this.NgayHetHanHopDong;
                    tkDuyet.SoHoaDonThuongMai = this.SoHoaDonThuongMai;
                    tkDuyet.NgayHoaDonThuongMai = this.NgayHoaDonThuongMai;
                    tkDuyet.PTVT_ID = this.PTVT_ID;
                    tkDuyet.SoHieuPTVT = this.SoHieuPTVT;
                    tkDuyet.NgayDenPTVT = this.NgayDenPTVT;
                    tkDuyet.LoaiVanDon = this.LoaiVanDon;
                    tkDuyet.NgayVanDon = this.NgayVanDon;
                    tkDuyet.NuocXK_ID = this.NuocXK_ID;
                    tkDuyet.NuocNK_ID = this.NuocNK_ID;
                    tkDuyet.DiaDiemXepHang = this.DiaDiemXepHang;
                    tkDuyet.DKGH_ID = this.DKGH_ID;
                    tkDuyet.CuaKhau_ID = this.CuaKhau_ID;
                    tkDuyet.NguyenTe_ID = this.NguyenTe_ID;
                    tkDuyet.TyGiaTinhThue = this.TyGiaTinhThue;
                    tkDuyet.TyGiaUSD = this.TyGiaUSD;
                    tkDuyet.PTTT_ID = this.PTTT_ID;
                    tkDuyet.SoHang = this.SoHang;
                    tkDuyet.SoLuongPLTK = this.SoLuongPLTK;
                    tkDuyet.TenChuHang = this.TenChuHang;
                    tkDuyet.SoContainer20 = this.SoContainer20;
                    tkDuyet.SoContainer40 = this.SoContainer40;
                    tkDuyet.SoKien = this.SoKien;
                    tkDuyet.TrongLuong = this.TrongLuong;
                    tkDuyet.TongTriGiaKhaiBao = this.TongTriGiaKhaiBao;
                    tkDuyet.TongTriGiaTinhThue = this.TongTriGiaTinhThue;
                    tkDuyet.LoaiToKhaiGiaCong = this.LoaiToKhaiGiaCong;
                    tkDuyet.LePhiHaiQuan = this.LePhiHaiQuan;
                    tkDuyet.PhiBaoHiem = this.PhiBaoHiem;
                    tkDuyet.PhiVanChuyen = this.PhiVanChuyen;
                    tkDuyet.ThanhLy = "";
                    tkDuyet.Xuat_NPL_SP = this.LoaiHangHoa;
                    tkDuyet.ChungTu = this.GiayTo;
                    tkDuyet.TrangThaiThanhKhoan = "D";
                    tkDuyet.PhiKhac = this.PhiKhac;
                    tkDuyet.NgayHoanThanh = tkDuyet.NgayDangKy;
                    tkDuyet.HeSoNhan = this.HeSoNhan;
                    if (tkDuyet.MaLoaiHinh.StartsWith("XSX"))
                        tkDuyet.TrangThai = 1;//chua dc phan bo

                    //Luu to khai tren bang du lieu cung DATABSE TQDT.
                    tkDuyet.InsertUpdateTransaction(transaction, db);

                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        BLL.SXXK.ToKhai.HangMauDich hmdDuyet = new Company.BLL.SXXK.ToKhai.HangMauDich();
                        hmdDuyet.DonGiaKB = hmd.DonGiaKB;
                        hmdDuyet.DonGiaTT = hmd.DonGiaTT;
                        hmdDuyet.DVT_ID = hmd.DVT_ID;
                        hmdDuyet.MaHaiQuan = this.MaHaiQuan.Trim();
                        hmdDuyet.MaHS = hmd.MaHS;
                        hmdDuyet.MaLoaiHinh = this.MaLoaiHinh;
                        hmdDuyet.MaPhu = hmd.MaPhu;
                        hmdDuyet.MienThue = hmd.MienThue;
                        hmdDuyet.NamDangKy = (short)this.NgayDangKy.Year;
                        hmdDuyet.NuocXX_ID = hmd.NuocXX_ID;
                        hmdDuyet.PhuThu = hmd.PhuThu;
                        hmdDuyet.SoLuong = hmd.SoLuong;
                        hmdDuyet.SoThuTuHang = (short)hmd.SoThuTuHang;
                        hmdDuyet.SoToKhai = this.SoToKhai;
                        hmdDuyet.TenHang = hmd.TenHang;
                        hmdDuyet.ThueGTGT = hmd.ThueGTGT;
                        hmdDuyet.ThueSuatGTGT = hmd.ThueSuatGTGT;
                        hmdDuyet.ThueSuatTTDB = hmd.ThueSuatTTDB;
                        hmdDuyet.ThueSuatXNK = hmd.ThueSuatXNK;
                        hmdDuyet.ThueTTDB = hmd.ThueTTDB;
                        hmdDuyet.ThueXNK = hmd.ThueXNK;
                        hmdDuyet.TriGiaKB = hmd.TriGiaKB;
                        hmdDuyet.TriGiaKB_VND = hmd.TriGiaKB_VND;
                        hmdDuyet.TriGiaThuKhac = hmd.TriGiaThuKhac;
                        hmdDuyet.TriGiaTT = hmd.TriGiaTT;
                        hmdDuyet.TyLeThuKhac = hmd.TyLeThuKhac;
                        if (tkDuyet.MaLoaiHinh.StartsWith("NSX"))
                        {
                            if (hmdDuyet.LoadByMaHang(transaction, db) != null)
                            {
                                hmdDuyet.SoLuong += hmd.SoLuong;
                                hmdDuyet.ThueXNK += hmd.ThueXNK;
                                hmdDuyet.TriGiaKB += hmd.TriGiaKB;
                                hmdDuyet.DonGiaKB = hmdDuyet.TriGiaKB / hmdDuyet.SoLuong;
                                hmdDuyet.DonGiaTT = hmdDuyet.DonGiaKB * tkDuyet.TyGiaTinhThue;
                                if (hmdDuyet.ThueXNK > 0)
                                    hmdDuyet.TriGiaTT = hmdDuyet.ThueXNK * 100 / hmdDuyet.ThueSuatXNK;
                                else
                                    hmdDuyet.TriGiaTT = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;
                                hmdDuyet.TriGiaKB_VND = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;
                            }
                        }

                        //Luu hang mau dich cua to khai tren bang du lieu cung DATABSE TQDT.
                        hmdDuyet.InsertUpdateTransaction(transaction, db);

                        if (tkDuyet.MaLoaiHinh.StartsWith("NSX"))
                        {
                            //cap nhat vao hang dieu chinh
                            BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                            nplNhapTon.SoToKhai = tkDuyet.SoToKhai;
                            nplNhapTon.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                            nplNhapTon.NamDangKy = tkDuyet.NamDangKy;
                            nplNhapTon.MaHaiQuan = tkDuyet.MaHaiQuan;
                            nplNhapTon.MaNPL = hmdDuyet.MaPhu;
                            nplNhapTon.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                            nplNhapTon.Luong = hmdDuyet.SoLuong;
                            nplNhapTon.Ton = hmdDuyet.SoLuong;
                            nplNhapTon.ThueXNK = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTon.ThueXNKTon = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTon.ThueTTDB = Convert.ToDouble(hmdDuyet.ThueTTDB);
                            nplNhapTon.ThueVAT = Convert.ToDouble(hmdDuyet.ThueGTGT);
                            nplNhapTon.PhuThu = Convert.ToDouble(hmdDuyet.PhuThu);
                            nplNhapTon.ThueCLGia = Convert.ToDouble(hmdDuyet.TriGiaThuKhac);

                            //Luu NPL ton cua to khai tren bang du lieu cung DATABSE TQDT.
                            nplNhapTon.InsertUpdateTransaction(transaction, db);

                            //CAP NHAT VAO LUONG TON THUC TE
                            BLL.SXXK.NPLNhapTonThucTe nplNhapTonThucTe = new Company.BLL.SXXK.NPLNhapTonThucTe();
                            nplNhapTonThucTe.SoToKhai = tkDuyet.SoToKhai;
                            nplNhapTonThucTe.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                            nplNhapTonThucTe.NamDangKy = tkDuyet.NamDangKy;
                            nplNhapTonThucTe.MaHaiQuan = tkDuyet.MaHaiQuan;
                            nplNhapTonThucTe.MaNPL = hmdDuyet.MaPhu;
                            nplNhapTonThucTe.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                            nplNhapTonThucTe.Luong = hmdDuyet.SoLuong;
                            nplNhapTonThucTe.Ton = hmdDuyet.SoLuong;
                            nplNhapTonThucTe.ThueXNK = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTonThucTe.ThueXNKTon = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTonThucTe.ThueTTDB = Convert.ToDouble(hmdDuyet.ThueTTDB);
                            nplNhapTonThucTe.ThueVAT = Convert.ToDouble(hmdDuyet.ThueGTGT);
                            nplNhapTonThucTe.PhuThu = Convert.ToDouble(hmdDuyet.PhuThu);
                            nplNhapTonThucTe.ThueCLGia = Convert.ToDouble(hmdDuyet.TriGiaThuKhac); ;
                            nplNhapTonThucTe.NgayDangKy = this.NgayDangKy;

                            //Luu NPL ton thuc te cua to khai tren bang du lieu cung DATABSE TQDT.
                            nplNhapTonThucTe.InsertUpdate(transaction, db);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            #endregion
        }

        #region TransgferData
        /// <summary>
        /// Đồng bộ dữ liệu tờ khai từ thông quan sang khai từ xa
        /// </summary>
        public void TransgferDataToSXXK(string connectionString, BLL.KDT.ToKhaiMauDich tkmdTQ)
        {
            //Tao Collection luu tam thong tin cua to khai mau dich se copy sang Khai tu xa
            List<BLL.SXXK.ToKhai.ToKhaiMauDich> collectionToKhaiMauDichKDT = new List<BLL.SXXK.ToKhai.ToKhaiMauDich>();
            List<BLL.SXXK.ThanhKhoan.NPLNhapTon> collectionNPLNhapTonKDT = new List<BLL.SXXK.ThanhKhoan.NPLNhapTon>();
            List<BLL.SXXK.NPLNhapTonThucTe> collectionNPLNhapTonThucTeKDT = new List<BLL.SXXK.NPLNhapTonThucTe>();
            List<BLL.SXXK.DinhMuc> collectionDinhMucKDT = new List<BLL.SXXK.DinhMuc>();
            List<BLL.SXXK.ThongTinDinhMuc> collectionThongTinDinhMucKDT = new List<BLL.SXXK.ThongTinDinhMuc>();

            #region GET DATA

            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkDuyet = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDuyet.MaDoanhNghiep = tkmdTQ.MaDoanhNghiep;
                    tkDuyet.PhanLuong = tkmdTQ.PhanLuong;
                    tkDuyet.MaHaiQuan = tkmdTQ.MaHaiQuan;
                    tkDuyet.MaLoaiHinh = tkmdTQ.MaLoaiHinh;
                    tkDuyet.NamDangKy = (short)tkmdTQ.NamDK;
                    tkDuyet.NgayDangKy = tkmdTQ.NgayDangKy;
                    tkDuyet.SoToKhai = tkmdTQ.SoToKhai;
                    tkDuyet.TenDonViDoiTac = tkmdTQ.TenDonViDoiTac;
                    tkDuyet.NgayGiayPhep = tkmdTQ.NgayGiayPhep;
                    tkDuyet.NgayHetHanGiayPhep = tkmdTQ.NgayHetHanGiayPhep;
                    tkDuyet.SoHopDong = tkmdTQ.SoHopDong;
                    tkDuyet.NgayHopDong = tkmdTQ.NgayHopDong;
                    tkDuyet.NgayHetHanHopDong = tkmdTQ.NgayHetHanHopDong;
                    tkDuyet.SoHoaDonThuongMai = tkmdTQ.SoHoaDonThuongMai;
                    tkDuyet.NgayHoaDonThuongMai = tkmdTQ.NgayHoaDonThuongMai;
                    tkDuyet.PTVT_ID = tkmdTQ.PTVT_ID;
                    tkDuyet.SoHieuPTVT = tkmdTQ.SoHieuPTVT;
                    tkDuyet.NgayDenPTVT = tkmdTQ.NgayDenPTVT;
                    tkDuyet.LoaiVanDon = tkmdTQ.LoaiVanDon;
                    tkDuyet.NgayVanDon = tkmdTQ.NgayVanDon;
                    tkDuyet.NuocXK_ID = tkmdTQ.NuocXK_ID;
                    tkDuyet.NuocNK_ID = tkmdTQ.NuocNK_ID;
                    tkDuyet.DiaDiemXepHang = tkmdTQ.DiaDiemXepHang;
                    tkDuyet.DKGH_ID = tkmdTQ.DKGH_ID;
                    tkDuyet.CuaKhau_ID = tkmdTQ.CuaKhau_ID;
                    tkDuyet.NguyenTe_ID = tkmdTQ.NguyenTe_ID;
                    tkDuyet.TyGiaTinhThue = tkmdTQ.TyGiaTinhThue;
                    tkDuyet.TyGiaUSD = tkmdTQ.TyGiaUSD;
                    tkDuyet.PTTT_ID = tkmdTQ.PTTT_ID;
                    tkDuyet.SoHang = tkmdTQ.SoHang;
                    tkDuyet.SoLuongPLTK = tkmdTQ.SoLuongPLTK;
                    tkDuyet.TenChuHang = tkmdTQ.TenChuHang;
                    tkDuyet.SoContainer20 = tkmdTQ.SoContainer20;
                    tkDuyet.SoContainer40 = tkmdTQ.SoContainer40;
                    tkDuyet.SoKien = tkmdTQ.SoKien;
                    tkDuyet.TrongLuong = tkmdTQ.TrongLuong;
                    tkDuyet.TongTriGiaKhaiBao = tkmdTQ.TongTriGiaKhaiBao;
                    tkDuyet.TongTriGiaTinhThue = tkmdTQ.TongTriGiaTinhThue;
                    tkDuyet.LoaiToKhaiGiaCong = tkmdTQ.LoaiToKhaiGiaCong;
                    tkDuyet.LePhiHaiQuan = tkmdTQ.LePhiHaiQuan;
                    tkDuyet.PhiBaoHiem = tkmdTQ.PhiBaoHiem;
                    tkDuyet.PhiVanChuyen = tkmdTQ.PhiVanChuyen;
                    tkDuyet.ThanhLy = "";
                    tkDuyet.Xuat_NPL_SP = tkmdTQ.LoaiHangHoa;
                    tkDuyet.ChungTu = tkmdTQ.GiayTo;
                    tkDuyet.TrangThaiThanhKhoan = "D";
                    tkDuyet.PhiKhac = tkmdTQ.PhiKhac;
                    tkDuyet.NgayHoanThanh = tkDuyet.NgayDangKy;
                    tkDuyet.HeSoNhan = tkmdTQ.HeSoNhan;
                    if (tkDuyet.MaLoaiHinh.StartsWith("XSX"))
                        tkDuyet.TrangThai = 1;//chua dc phan bo

                    //Luu to khai tren bang du lieu cung DATABSE TQDT.
                    tkDuyet.InsertUpdateTransaction(transaction);

                    //Luu to khai vao Collection tam de luu du lieu sang DATABSE khac (Khai tu xa).
                    collectionToKhaiMauDichKDT.Add(tkDuyet);

                    //Hang mau dich cua to khai
                    foreach (HangMauDich hmd in tkmdTQ.HMDCollection)
                    {
                        BLL.SXXK.ToKhai.HangMauDich hmdDuyet = new Company.BLL.SXXK.ToKhai.HangMauDich();
                        hmdDuyet.DonGiaKB = hmd.DonGiaKB;
                        hmdDuyet.DonGiaTT = hmd.DonGiaTT;
                        hmdDuyet.DVT_ID = hmd.DVT_ID;
                        hmdDuyet.MaHaiQuan = tkmdTQ.MaHaiQuan;
                        hmdDuyet.MaHS = hmd.MaHS;
                        hmdDuyet.MaLoaiHinh = tkmdTQ.MaLoaiHinh;
                        hmdDuyet.MaPhu = hmd.MaPhu;
                        hmdDuyet.MienThue = hmd.MienThue;
                        hmdDuyet.NamDangKy = (short)tkmdTQ.NgayDangKy.Year;
                        hmdDuyet.NuocXX_ID = hmd.NuocXX_ID;
                        hmdDuyet.PhuThu = hmd.PhuThu;
                        hmdDuyet.SoLuong = hmd.SoLuong;
                        hmdDuyet.SoThuTuHang = (short)hmd.SoThuTuHang;
                        hmdDuyet.SoToKhai = tkmdTQ.SoToKhai;
                        hmdDuyet.TenHang = hmd.TenHang;
                        hmdDuyet.ThueGTGT = hmd.ThueGTGT;
                        hmdDuyet.ThueSuatGTGT = hmd.ThueSuatGTGT;
                        hmdDuyet.ThueSuatTTDB = hmd.ThueSuatTTDB;
                        hmdDuyet.ThueSuatXNK = hmd.ThueSuatXNK;
                        hmdDuyet.ThueTTDB = hmd.ThueTTDB;
                        hmdDuyet.ThueXNK = hmd.ThueXNK;
                        hmdDuyet.TriGiaKB = hmd.TriGiaKB;
                        hmdDuyet.TriGiaKB_VND = hmd.TriGiaKB_VND;
                        hmdDuyet.TriGiaThuKhac = hmd.TriGiaThuKhac;
                        hmdDuyet.TriGiaTT = hmd.TriGiaTT;
                        hmdDuyet.TyLeThuKhac = hmd.TyLeThuKhac;
                        if (tkDuyet.MaLoaiHinh.StartsWith("NSX"))
                        {
                            if (hmdDuyet.LoadByMaHang(transaction))
                            {
                                hmdDuyet.SoLuong += hmd.SoLuong;
                                hmdDuyet.ThueXNK += hmd.ThueXNK;
                                hmdDuyet.TriGiaKB += hmd.TriGiaKB;
                                hmdDuyet.DonGiaKB = hmdDuyet.TriGiaKB / hmdDuyet.SoLuong;
                                hmdDuyet.DonGiaTT = hmdDuyet.DonGiaKB * tkDuyet.TyGiaTinhThue;
                                if (hmdDuyet.ThueXNK > 0)
                                    hmdDuyet.TriGiaTT = hmdDuyet.ThueXNK * 100 / hmdDuyet.ThueSuatXNK;
                                else
                                    hmdDuyet.TriGiaTT = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;
                                hmdDuyet.TriGiaKB_VND = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;
                            }
                        }

                        //Luu hang mau dich cua to khai tren bang du lieu cung DATABSE TQDT.
                        hmdDuyet.InsertUpdateTransaction(transaction);

                        //Luu hang mau dich cua to khai vao Collection tam de luu du lieu sang DATABSE khac (Khai tu xa).
                        tkDuyet.HMDCollection.Add(hmdDuyet);

                        if (tkDuyet.MaLoaiHinh.StartsWith("NSX"))
                        {
                            //cap nhat vao hang dieu chinh
                            BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                            nplNhapTon.SoToKhai = tkDuyet.SoToKhai;
                            nplNhapTon.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                            nplNhapTon.NamDangKy = tkDuyet.NamDangKy;
                            nplNhapTon.MaHaiQuan = tkDuyet.MaHaiQuan;
                            nplNhapTon.MaNPL = hmdDuyet.MaPhu;
                            nplNhapTon.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                            nplNhapTon.Luong = hmdDuyet.SoLuong;
                            nplNhapTon.Ton = hmdDuyet.SoLuong;
                            nplNhapTon.ThueXNK = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTon.ThueXNKTon = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTon.ThueTTDB = Convert.ToDouble(hmdDuyet.ThueTTDB);
                            nplNhapTon.ThueVAT = Convert.ToDouble(hmdDuyet.ThueGTGT);
                            nplNhapTon.PhuThu = Convert.ToDouble(hmdDuyet.PhuThu);
                            nplNhapTon.ThueCLGia = Convert.ToDouble(hmdDuyet.TriGiaThuKhac);

                            //Luu NPL ton cua to khai tren bang du lieu cung DATABSE TQDT.
                            nplNhapTon.InsertUpdateTransaction(transaction);

                            //Luu NPL tonm cua to khai vao Collection tam de luu du lieu sang DATABSE khac (Khai tu xa).
                            collectionNPLNhapTonKDT.Add(nplNhapTon);

                            //CAP NHAT VAO LUONG TON THUC TE
                            BLL.SXXK.NPLNhapTonThucTe nplNhapTonThucTe = new Company.BLL.SXXK.NPLNhapTonThucTe();
                            nplNhapTonThucTe.SoToKhai = tkDuyet.SoToKhai;
                            nplNhapTonThucTe.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                            nplNhapTonThucTe.NamDangKy = tkDuyet.NamDangKy;
                            nplNhapTonThucTe.MaHaiQuan = tkDuyet.MaHaiQuan;
                            nplNhapTonThucTe.MaNPL = hmdDuyet.MaPhu;
                            nplNhapTonThucTe.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                            nplNhapTonThucTe.Luong = hmdDuyet.SoLuong;
                            nplNhapTonThucTe.Ton = hmdDuyet.SoLuong;
                            nplNhapTonThucTe.ThueXNK = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTonThucTe.ThueXNKTon = Convert.ToDouble(hmdDuyet.ThueXNK);
                            nplNhapTonThucTe.ThueTTDB = Convert.ToDouble(hmdDuyet.ThueTTDB);
                            nplNhapTonThucTe.ThueVAT = Convert.ToDouble(hmdDuyet.ThueGTGT);
                            nplNhapTonThucTe.PhuThu = Convert.ToDouble(hmdDuyet.PhuThu);
                            nplNhapTonThucTe.ThueCLGia = Convert.ToDouble(hmdDuyet.TriGiaThuKhac); ;
                            nplNhapTonThucTe.NgayDangKy = tkmdTQ.NgayDangKy;

                            //Luu NPL ton thuc te cua to khai tren bang du lieu cung DATABSE TQDT.
                            nplNhapTonThucTe.InsertUpdate(transaction);

                            //Luu NPL tonm cua to khai vao Collection tam de luu du lieu sang DATABSE khac (Khai tu xa).
                            collectionNPLNhapTonThucTeKDT.Add(nplNhapTonThucTe);
                        }

                        /*Kiem tra thong tin DINH MUC*/
                        string maSanPham = hmdDuyet.MaPhu;
                        BLL.KDT.SXXK.DinhMucCollection collectionDM = BLL.KDT.SXXK.DinhMuc.SelectCollectionDynamic("MaSanPham = '" + maSanPham + "'", "");
                        long masterID = collectionDM.Count > 0 ? collectionDM[0].Master_ID : 0;

                        BLL.KDT.SXXK.DinhMucDangKyCollection collectionDMDK = BLL.KDT.SXXK.DinhMucDangKy.SelectCollectionDynamic("ID = " + masterID, "");
                        foreach (BLL.KDT.SXXK.DinhMucDangKy dmdk in collectionDMDK)
                        {
                            BLL.SXXK.ThongTinDinhMuc thongTinDM = new BLL.SXXK.ThongTinDinhMuc();
                            thongTinDM.MaDoanhNghiep = dmdk.MaDoanhNghiep;
                            thongTinDM.MaHaiQuan = dmdk.MaHaiQuan;
                            thongTinDM.MaSanPham = maSanPham;
                            thongTinDM.NgayDangKy = dmdk.NgayDangKy;
                            thongTinDM.NgayApDung = dmdk.NgayApDung;
                            thongTinDM.NgayHetHan = dmdk.NgayApDung;
                            thongTinDM.SoDinhMuc = dmdk.SoDinhMuc;
                            thongTinDM.ThanhLy = 0;

                            //Luu thong tin dinh muc tren bang du lieu cung DATABSE TQDT.
                            thongTinDM.InsertUpdateTransaction(transaction);

                            //Luu thong tin dinh muc vao Collection tam de luu du lieu sang DATABSE khac (Khai tu xa).
                            collectionThongTinDinhMucKDT.Add(thongTinDM);
                        }

                        foreach (BLL.KDT.SXXK.DinhMuc dmTQ in collectionDM)
                        {
                            BLL.SXXK.DinhMuc dmKDT = new BLL.SXXK.DinhMuc();
                            dmKDT.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                            dmKDT.MaHaiQuan = tkDuyet.MaHaiQuan;
                            dmKDT.MaNguyenPhuLieu = dmTQ.MaNguyenPhuLieu;
                            dmKDT.MaSanPHam = dmTQ.MaSanPham;
                            dmKDT.TenNPL = dmTQ.TenNPL;
                            dmKDT.TyLeHaoHut = dmTQ.TyLeHaoHut;
                            dmKDT.GhiChu = dmTQ.GhiChu;
                            dmKDT.DinhMucSuDung = dmTQ.DinhMucSuDung;
                            dmKDT.DinhMucChung = dmTQ.DinhMucSuDung;
                            //Luu dinh muc tren bang du lieu cung DATABSE TQDT.
                            dmKDT.InsertUpdateTransaction(transaction);

                            //Luu dinh muc vao Collection tam de luu du lieu sang DATABSE khac (Khai tu xa).
                            collectionDinhMucKDT.Add(dmKDT);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            #endregion

            #region Mo ket noi den CSDL Khai tu xa
            SqlDatabase dbKDT = new SqlDatabase(connectionString);
            SqlConnection connectionKDT = (SqlConnection)dbKDT.CreateConnection();
            connectionKDT.Open();
            SqlTransaction transactionKDT = connectionKDT.BeginTransaction();
            try
            {
                foreach (BLL.SXXK.ToKhai.ToKhaiMauDich tk in collectionToKhaiMauDichKDT)
                {
                    //Truoc het cap nhat nam dang ky cua TO KHAI vi da duoc duyet (Neu co);
                    InsertUpdateTransaction(transactionKDT, tk);

                    #region DELETE DU LIEU
                    BLL.SXXK.ToKhai.HangMauDich HMDDelete = new BLL.SXXK.ToKhai.HangMauDich();
                    HMDDelete.MaHaiQuan = tk.MaHaiQuan;
                    HMDDelete.MaLoaiHinh = tk.MaLoaiHinh;
                    HMDDelete.SoToKhai = tk.SoToKhai;
                    HMDDelete.NamDangKy = tk.NamDangKy;

                    DeleteByForeigKeyTransaction(transactionKDT, HMDDelete);

                    //short STTHang = 1;
                    //foreach (BLL.SXXK.ToKhai.HangMauDich hmd in tk.HMDCollection)
                    //{

                    //    hmd.MaHaiQuan = tk.MaHaiQuan;
                    //    hmd.MaLoaiHinh = tk.MaLoaiHinh;

                    //    hmd.NamDangKy = tk.NamDangKy;
                    //    hmd.SoThuTuHang = STTHang;
                    //    hmd.SoToKhai = tk.SoToKhai;
                    //    STTHang++;
                    //}
                    if (MaLoaiHinh.StartsWith("N"))
                    {
                        BLL.SXXK.ThanhKhoan.NPLNhapTon nplDelete = new BLL.SXXK.ThanhKhoan.NPLNhapTon();
                        nplDelete.SoToKhai = tk.SoToKhai;
                        nplDelete.MaLoaiHinh = tk.MaLoaiHinh;
                        nplDelete.NamDangKy = tk.NamDangKy;
                        nplDelete.MaHaiQuan = tk.MaHaiQuan;
                        DeleteTransactionByToKhai(transactionKDT, nplDelete);

                        BLL.SXXK.NPLNhapTonThucTe nplThucTeDelete = new BLL.SXXK.NPLNhapTonThucTe();
                        nplThucTeDelete.SoToKhai = tk.SoToKhai;
                        nplThucTeDelete.MaLoaiHinh = tk.MaLoaiHinh;
                        nplThucTeDelete.NamDangKy = tk.NamDangKy;
                        nplThucTeDelete.MaHaiQuan = tk.MaHaiQuan;
                        DeleteTransactionByToKhai(transactionKDT, nplThucTeDelete);
                    }

                    //Thong tin Dinh muc
                    foreach (BLL.SXXK.ThongTinDinhMuc thongTinDM in collectionThongTinDinhMucKDT)
                    {
                        DeleteTransaction(transactionKDT, thongTinDM);
                    }
                    //Dinh muc
                    foreach (BLL.SXXK.DinhMuc dinhMuc in collectionDinhMucKDT)
                    {
                        DeleteByForeignKeyTransaction(transactionKDT, dinhMuc);
                    }
                    #endregion

                    #region INSET DU LIEU
                    //Hang mau dich
                    foreach (BLL.SXXK.ToKhai.HangMauDich hmdKDT in tk.HMDCollection)
                    {
                        InsertUpdateTransaction(transactionKDT, hmdKDT);

                        if (tk.MaLoaiHinh.StartsWith("NSX"))
                        {
                            //Nguyen phu lieu ton
                            foreach (BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon in collectionNPLNhapTonKDT)
                            {
                                InsertUpdateTransaction(transactionKDT, nplTon);
                            }
                            //Nguyen phu lieu ton thuc te
                            foreach (BLL.SXXK.NPLNhapTonThucTe nplTonTT in collectionNPLNhapTonThucTeKDT)
                            {
                                InsertUpdateTransaction(transactionKDT, nplTonTT);
                            }
                        }
                    }

                    //Thong tin Dinh muc
                    foreach (BLL.SXXK.ThongTinDinhMuc thongTinDM in collectionThongTinDinhMucKDT)
                    {
                        InsertUpdateTransaction(transactionKDT, thongTinDM);
                    }
                    //Dinh muc
                    foreach (BLL.SXXK.DinhMuc dinhMuc in collectionDinhMucKDT)
                    {
                        InsertUpdateTransaction(transactionKDT, dinhMuc);
                    }
                    #endregion
                }

                transactionKDT.Commit();
            }
            catch (Exception ex)
            {
                transactionKDT.Rollback();
                throw ex; ;
            }
            finally { connectionKDT.Close(); }

            #endregion
        }

        //-------TO KHAI-------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, BLL.SXXK.ToKhai.ToKhaiMauDich tkmd)
        {
            string spName = "p_SXXK_ToKhaiMauDich_InsertUpdate";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.Connection = transaction.Connection;
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, tkmd.MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, tkmd.SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, tkmd.MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, tkmd.NamDangKy);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, tkmd.NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, tkmd.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, tkmd.TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, tkmd.MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, tkmd.TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, tkmd.TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, tkmd.ChiTietDonViDoiTac);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, tkmd.SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, tkmd.NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, tkmd.NgayHetHanGiayPhep);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, tkmd.SoHopDong);
            db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, tkmd.NgayHopDong);
            db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, tkmd.NgayHetHanHopDong);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, tkmd.SoHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, tkmd.NgayHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, tkmd.PTVT_ID);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, tkmd.SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, tkmd.NgayDenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, tkmd.QuocTichPTVT_ID);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, tkmd.LoaiVanDon);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, tkmd.SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, tkmd.NgayVanDon);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, tkmd.NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, tkmd.NuocNK_ID);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, tkmd.DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, tkmd.CuaKhau_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, tkmd.DKGH_ID);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, tkmd.NguyenTe_ID);
            db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, tkmd.TyGiaTinhThue);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, tkmd.TyGiaUSD);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, tkmd.PTTT_ID);
            db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, tkmd.SoHang);
            db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, tkmd.SoLuongPLTK);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, tkmd.TenChuHang);
            db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, tkmd.SoContainer20);
            db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, tkmd.SoContainer40);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, tkmd.SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, tkmd.TrongLuong);
            db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, tkmd.TongTriGiaKhaiBao);
            db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, tkmd.TongTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, tkmd.LoaiToKhaiGiaCong);
            db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, tkmd.LePhiHaiQuan);
            db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, tkmd.PhiBaoHiem);
            db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, tkmd.PhiVanChuyen);
            db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, tkmd.PhiXepDoHang);
            db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, tkmd.PhiKhac);
            db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, tkmd.Xuat_NPL_SP);
            db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, tkmd.ThanhLy);
            db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, tkmd.NGAY_THN_THX);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, tkmd.MaDonViUT);
            db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, tkmd.TrangThaiThanhKhoan);
            db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, tkmd.ChungTu);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, tkmd.PhanLuong);
            db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, tkmd.NgayHoanThanh);
            //Bổ sung thêm
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, tkmd.TrangThai);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, tkmd.TrongLuongNet);
            db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, tkmd.SoTienKhoan);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, tkmd.GhiChu);
            db.AddInParameter(dbCommand, "@HeSoNhan", SqlDbType.Float, tkmd.HeSoNhan);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //-------HANG MAU DICH-------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, BLL.SXXK.ToKhai.HangMauDich hmd)
        {
            string spName = "p_SXXK_HangMauDich_InsertUpdate";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.Connection = transaction.Connection;
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, hmd.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, hmd.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, hmd.MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, hmd.NamDangKy);
            this.db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, hmd.SoThuTuHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, hmd.MaHS);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, hmd.MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, hmd.TenHang);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, hmd.NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, hmd.DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, hmd.SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, hmd.DonGiaKB);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, hmd.DonGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, hmd.TriGiaKB);
            this.db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, hmd.TriGiaTT);
            this.db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, hmd.TriGiaKB_VND);
            this.db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, hmd.ThueSuatXNK);
            this.db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, hmd.ThueSuatTTDB);
            this.db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, hmd.ThueSuatGTGT);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, hmd.ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, hmd.ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, hmd.ThueGTGT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, hmd.PhuThu);
            this.db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, hmd.TyLeThuKhac);
            this.db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, hmd.TriGiaThuKhac);
            this.db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, hmd.MienThue);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteByForeigKeyTransaction(SqlTransaction transaction, BLL.SXXK.ToKhai.HangMauDich hmd)
        {
            string spName = "p_SXXK_HangMauDich_DeleteBy_MaHaiQuan_And_SoToKhai_And_MaLoaiHinh_And_NamDangKy";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.Connection = transaction.Connection;

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, hmd.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, hmd.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, hmd.MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, hmd.NamDangKy);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //-------NGUYEN PHU LIEU-----------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_InsertUpdate";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.Connection = transaction.Connection;
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, nplTon.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, nplTon.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, nplTon.NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, nplTon.MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, nplTon.MaNPL);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, nplTon.MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, nplTon.Luong);
            this.db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, nplTon.Ton);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, nplTon.ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, nplTon.ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, nplTon.ThueVAT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, nplTon.PhuThu);
            this.db.AddInParameter(dbCommand, "@ThueCLGia", SqlDbType.Float, nplTon.ThueCLGia);
            this.db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, Math.Round((double)nplTon.Ton * nplTon.ThueXNK / (double)nplTon.Luong, 0));
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteTransactionByToKhai(SqlTransaction transaction, BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon)
        {
            string spName = "DELETE FROM t_SXXK_ThanhLy_NPLNhapTon WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND NamDangKy = @NamDangKy AND MaHaiQuan = @MaHaiQuan";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.Connection = transaction.Connection;

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, nplTon.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, nplTon.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, nplTon.NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, nplTon.MaHaiQuan);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //----------NGUYEN PHU LIEU THUC TE----------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, BLL.SXXK.NPLNhapTonThucTe nplTonThucTe)
        {
            const string spName = "p_SXXK_NPLNhapTonThucTe_InsertUpdate";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.Connection = transaction.Connection;

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, nplTonThucTe.SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, nplTonThucTe.MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, nplTonThucTe.NamDangKy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, nplTonThucTe.MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, nplTonThucTe.MaNPL);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, nplTonThucTe.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, nplTonThucTe.Luong);
            db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, nplTonThucTe.Ton);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, nplTonThucTe.ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, nplTonThucTe.ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, nplTonThucTe.ThueVAT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, nplTonThucTe.PhuThu);
            db.AddInParameter(dbCommand, "@ThueCLGia", SqlDbType.Float, nplTonThucTe.ThueCLGia);
            db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, nplTonThucTe.ThueXNKTon);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, nplTonThucTe.NgayDangKy.Year == 1753 ? DBNull.Value : (object)nplTonThucTe.NgayDangKy);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteTransactionByToKhai(SqlTransaction transaction, BLL.SXXK.NPLNhapTonThucTe nplTonThucTe)
        {
            string spName = "DELETE FROM t_SXXK_NPLNhapTonThucTe WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND NamDangKy = @NamDangKy AND MaHaiQuan = @MaHaiQuan";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.Connection = transaction.Connection;

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, nplTonThucTe.SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, nplTonThucTe.MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, nplTonThucTe.NamDangKy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, nplTonThucTe.MaHaiQuan);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //----------DINH MUC---------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, BLL.SXXK.DinhMuc dinhMuc)
        {
            string spName = "p_SXXK_DinhMuc_InsertUpdate";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.Connection = transaction.Connection;
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, dinhMuc.MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, dinhMuc.MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, dinhMuc.MaSanPHam);
            this.db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, dinhMuc.MaNguyenPhuLieu);
            this.db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, dinhMuc.DinhMucSuDung);
            this.db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, dinhMuc.TyLeHaoHut);
            this.db.AddInParameter(dbCommand, "@DinhMucChung", SqlDbType.Decimal, dinhMuc.DinhMucChung);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, dinhMuc.GhiChu);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteByForeignKeyTransaction(SqlTransaction transaction, BLL.SXXK.DinhMuc dinhMuc)
        {
            string spName = "p_SXXK_DinhMuc_DeleteBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.Connection = transaction.Connection;

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, dinhMuc.MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, dinhMuc.MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, dinhMuc.MaSanPHam);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //----------THONG TIN DINH MUC---------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, BLL.SXXK.ThongTinDinhMuc thongTinDM)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_InsertUpdate";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.Connection = transaction.Connection;

            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, thongTinDM.MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, thongTinDM.MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, thongTinDM.MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoDinhMuc", SqlDbType.Int, thongTinDM.SoDinhMuc);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, thongTinDM.NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayApDung", SqlDbType.DateTime, thongTinDM.NgayApDung);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, thongTinDM.NgayHetHan);
            this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Int, thongTinDM.ThanhLy);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteTransaction(SqlTransaction transaction, BLL.SXXK.ThongTinDinhMuc thongTinDM)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_Delete";
            SqlDatabase db = new SqlDatabase(transaction.Connection.ConnectionString);
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.Connection = transaction.Connection;

            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, thongTinDM.MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, thongTinDM.MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, thongTinDM.MaHaiQuan);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        #endregion

        //#endregion Lấy thông tin trả lời từ hải quan

        ////public void TinhToanCanDoiNhapXuat()
        ////{
        ////    if (this.HMDCollection.Count == 0)
        ////        this.LoadHMDCollection();
        ////    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        ////    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        ////    {
        ////        connection.Open();
        ////        SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
        ////        try
        ////        {
        ////            foreach (HangMauDich HMDTon in this.HMDCollection)
        ////            {
        ////                Company.BLL.SXXK.NguyenPhuLieuTon NPLTonDuyet = new Company.BLL.SXXK.NguyenPhuLieuTon();
        ////                NPLTonDuyet.Ma = HMDTon.MaPhu;
        ////                NPLTonDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
        ////                NPLTonDuyet.MaHaiQuan = this.MaHaiQuan;
        ////                if (!NPLTonDuyet.Load(transaction))
        ////                    NPLTonDuyet.Ten = HMDTon.TenHang;
        ////                if (MaLoaiHinh.StartsWith("N"))
        ////                {
        ////                    NPLTonDuyet.LuongTon += Convert.ToDouble(HMDTon.SoLuong);
        ////                    NPLTonDuyet.InsertUpdateTransaction(transaction);
        ////                }
        ////                else
        ////                {
        ////                    if (LoaiHangHoa == "N")
        ////                    {
        ////                        NPLTonDuyet.LuongTon -= Convert.ToDouble(HMDTon.SoLuong);
        ////                        NPLTonDuyet.InsertUpdateTransaction(transaction);
        ////                    }
        ////                    else
        ////                    {
        ////                        DataSet dsDM = Company.BLL.SXXK.DinhMuc.getDinhMucOfSanPham(HMDTon.MaPhu, this.MaHaiQuan, this.MaDoanhNghiep, HMDTon.SoLuong, transaction);
        ////                        if (dsDM.Tables[0].Rows.Count == 0)
        ////                            throw new Exception("Sản phẩm : " + HMDTon.MaPhu +" chưa có định mức nên không thể cập nhật vào lượng tồn được.");
        ////                        foreach (DataRow row in dsDM.Tables[0].Rows)
        ////                        {
        ////                            Company.BLL.SXXK.NguyenPhuLieuTon NPLTonDuyetDM = new Company.BLL.SXXK.NguyenPhuLieuTon();
        ////                            NPLTonDuyetDM.Ma = row["MaNguyenPhuLieu"].ToString().Trim();
        ////                            NPLTonDuyetDM.MaDoanhNghiep = this.MaDoanhNghiep;
        ////                            NPLTonDuyetDM.MaHaiQuan = this.MaHaiQuan;
        ////                            if (!NPLTonDuyetDM.Load(transaction))
        ////                                NPLTonDuyetDM.Ten = row["Ten"].ToString();
        ////                            NPLTonDuyetDM.LuongTon -= Convert.ToDouble(row["SoLuong"].ToString());
        ////                            NPLTonDuyetDM.InsertUpdateTransaction(transaction);
        ////                        }
        ////                    }
        ////                }
        ////            }
        ////            transaction.Commit();


        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            transaction.Rollback();
        ////            throw new Exception(ex.Message);
        ////        }
        ////        finally
        ////        {
        ////            connection.Close();
        ////        }
        ////    }

        ////}



        //#endregion Webservice của FPT

        #region SỬA TỜ KHAI ĐÃ DUYỆT

        public string ConfigPhongBiSua(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            //if (this.GUIDSTR == "")
            //{
            //    this.GUIDSTR = (System.Guid.NewGuid().ToString());
            //    this.Update();
            //}

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = this._TenDoanhNghiep;
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            this.Update();
            return doc.InnerXml;
        }

        public string WSKhaiBaoToKhaiSua(string password, string maMid)
        {

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiSua(this.MaLoaiHinh.StartsWith("N") ? MessgaseType.ToKhaiNhap : MessgaseType.ToKhaiXuat, MessgaseFunction.KhaiBao));

            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(this.MaLoaiHinh.StartsWith("N") ? ConvertCollectionToXMLSuaToKhaiNhap() : ConvertCollectionToXMLSuaToKhaiXuat(maMid));

            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            //--------Khai dinh kem --------------
            XmlNode DU_LEU = doc.GetElementsByTagName("DU_LIEU")[0];

            XmlNode node = CO.ConvertCollectionCOToXML(doc, this.COCollection, this.ID);

            List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.HangMauDich>();
            listHang = ConvertHMDKDToHangMauDich();
            if (node != null)
                DU_LEU.AppendChild(node);

            node = GiayPhep.ConvertCollectionGiayPhepToXML(doc, this.GiayPhepCollection, this.ID, listHang);
            if (node != null)
                DU_LEU.AppendChild(node);

            //DATLMQ update kiểm tra Mã loại hình tờ khai 11/02/2011
            if (this.MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKN(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
                if (node != null)
                    DU_LEU.AppendChild(node);

                node = HopDongThuongMai.ConvertCollectionHopDongToXML_TKN(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }
            else
            {
                node = HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKX(doc, this.HoaDonThuongMaiCollection, this.ID, listHang);
                if (node != null)
                    DU_LEU.AppendChild(node);

                node = HopDongThuongMai.ConvertCollectionHopDongToXML_TKX(doc, this.HopDongThuongMaiCollection, this.ID, listHang);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            if (VanTaiDon == null)
            {
                List<VanDon> VanDonCollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(this.ID);
                if (VanDonCollection != null && VanDonCollection.Count > 0)
                {
                    VanTaiDon = VanDonCollection[0];
                    VanTaiDon.LoadContainerCollection();
                }
            }
            if (VanTaiDon != null)
            {
                //node = this.VanTaiDon.ConvertVanDonToXML(doc);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            node = DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, this.listChuyenCuaKhau, this.ID);
            if (node != null)
                DU_LEU.AppendChild(node);


            //tkem
            ChungTuKem ctct = new ChungTuKem();

            List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet> listCTCT = new List<ChungTuKemChiTiet>();
            listCTCT = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctct.LoadCT(this.ID));
            if (listCTCT != null && listCTCT.Count != 0)
            {
                node = ChungTuKem.ConvertCollectionCTDinhKemToXML(doc, ChungTuKemCollection, this.ID, listCTCT);
                if (node != null)
                    DU_LEU.AppendChild(node);
            }

            //----------------End Khai dinh kem --------------------

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, this.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoToKhaiSua);
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node11 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node11);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WebService_LEVEL");
            }


            return "";
        }

        private string ConvertCollectionToXMLSuaToKhaiNhap()
        {
            //load du lieu
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\SUA_ToKhaiNhap.xml");

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            nodeHQNhan.Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = this.TenDoanhNghiep;

            //thong tin to khai
            XmlNode nodeToKhai = docNPL.GetElementsByTagName("TO_KHAI")[0];

            nodeToKhai.Attributes["MA_LH"].Value = this.MaLoaiHinh;
            nodeToKhai.Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            nodeToKhai.Attributes["MA_DV_NK"].Value = this.MaDoanhNghiep;
            nodeToKhai.Attributes["MA_DV_UT"].Value = this.MaDonViUT;
            nodeToKhai.Attributes["MA_DV_XK"].Value = "";

            nodeToKhai.Attributes["TEN_DV_XK"].Value = this.TenDonViDoiTac;

            if (this.SoGiayPhep.Length <= 35)
                nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep;
            else
                nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep.Substring(0, 35);
            if (this.NgayGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_GP"].Value = this.NgayGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_GP"].Value = null;
            if (this.NgayHetHanGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHGP"].Value = this.NgayHetHanGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHGP"].Value = null;
            if (this.SoHopDong.Length <= 50)
                nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong;
            else
                nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong.Substring(0, 50);
            if (this.NgayHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HD"].Value = this.NgayHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HD"].Value = null;
            if (this.NgayHetHanHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHHD"].Value = this.NgayHetHanHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHHD"].Value = null;
            if (this.SoHoaDonThuongMai.Length <= 50)
                nodeToKhai.Attributes["SO_HDTM"].Value = this.SoHoaDonThuongMai;
            else
                nodeToKhai.Attributes["SO_HDTM"].Value = this.SoHoaDonThuongMai.Substring(0, 50);
            if (this.NgayHoaDonThuongMai.Year > 1900)
                nodeToKhai.Attributes["NGAY_HDTM"].Value = this.NgayHoaDonThuongMai.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HDTM"].Value = null;
            nodeToKhai.Attributes["MA_PTVT"].Value = this.PTVT_ID;
            if (this.SoHieuPTVT.Length <= 20)
                nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT;
            else
                nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT.Substring(0, 20);
            if (this.NgayDenPTVT.Year > 1900)
                nodeToKhai.Attributes["NGAY_DEN"].Value = this.NgayDenPTVT.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_DEN"].Value = null;
            if (SoVanDon.Length <= 20)
                nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon;
            else
                nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon.Substring(0, 20);
            if (this.NgayVanDon.Year > 1900)
                nodeToKhai.Attributes["NGAY_VANDON"].Value = this.NgayVanDon.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_VANDON"].Value = null;
            nodeToKhai.Attributes["NUOC_XK"].Value = this.NuocXK_ID;
            if (this.DiaDiemXepHang.Length <= 40)
                nodeToKhai.Attributes["CANG_XUAT"].Value = this.DiaDiemXepHang;
            else
                nodeToKhai.Attributes["CANG_XUAT"].Value = this.DiaDiemXepHang.Substring(0, 40);
            nodeToKhai.Attributes["MA_CK_NHAP"].Value = this.CuaKhau_ID;
            nodeToKhai.Attributes["MA_DKGH"].Value = this.DKGH_ID;
            nodeToKhai.Attributes["MA_NGTE"].Value = this.NguyenTe_ID;
            nodeToKhai.Attributes["TY_GIA_VND"].Value = BaseClass.Round(this.TyGiaTinhThue, 9);
            nodeToKhai.Attributes["MA_PTTT"].Value = this.PTTT_ID;
            if (this.GiayTo.Length > 40)
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo.Substring(0, 40);
            else
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo;
            nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(this.SoContainer20).ToString();
            nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(this.SoContainer40).ToString();
            nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(this.SoLuongPLTK).ToString();
            nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(this.SoKien).ToString();
            nodeToKhai.Attributes["TRONG_LUONG"].Value = BaseClass.Round(this.TrongLuong, 9);
            nodeToKhai.Attributes["PHI_BH"].Value = BaseClass.Round(this.PhiBaoHiem, 9);
            decimal PhiKhacNH = this.PhiKhac + this.PhiVanChuyen;
            nodeToKhai.Attributes["PHI_VC"].Value = BaseClass.Round(PhiKhacNH, 9);
            nodeToKhai.Attributes["LE_PHI_HQ"].Value = BaseClass.Round(this.LePhiHaiQuan, 9);
            if (this.TenChuHang.Length > 30)
                nodeToKhai.Attributes["CHU_HANG"].Value = this.TenChuHang.Substring(0, 30);
            else
                nodeToKhai.Attributes["CHU_HANG"].Value = this.TenChuHang;
            //linhhtn
            nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = this.DeXuatKhac;
            nodeToKhai.Attributes["LY_DO_SUA"].Value = FontConverter.Unicode2TCVN(this.LyDoSua);
            nodeToKhai.Attributes["SOTK"].Value = this.SoToKhai.ToString();
            nodeToKhai.Attributes["NGAY_DK"].Value = this.NgayDangKy.ToString("yyyy-MM-dd");

            //hang hoa
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                //HangMauDich hmd = new HangMauDich();
                //hmd.TKMD_ID = this.ID;
                //this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlNode nodeHang = docNPL.GetElementsByTagName("HANG")[0];
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("HANG.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT_HANG");
                sttAtt.Value = hmd.SoThuTuHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_HANG");

                maAtt.Value = hmd.MaPhu;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = hmd.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_HANG");
                TenAtt.Value = hmd.TenHang;
                node.Attributes.Append(TenAtt);

                XmlAttribute NuocAtt = docNPL.CreateAttribute("NUOC_XX");
                NuocAtt.Value = hmd.NuocXX_ID;
                node.Attributes.Append(NuocAtt);

                XmlAttribute SoLuongAtt = docNPL.CreateAttribute("LUONG");
                SoLuongAtt.Value = BaseClass.Round(hmd.SoLuong, 9);
                node.Attributes.Append(SoLuongAtt);

                XmlAttribute Ma_DVTAtt = docNPL.CreateAttribute("MA_DVT");
                Ma_DVTAtt.Value = hmd.DVT_ID;
                node.Attributes.Append(Ma_DVTAtt);

                XmlAttribute DonGiaAtt = docNPL.CreateAttribute("DGIA_NT");
                DonGiaAtt.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                node.Attributes.Append(DonGiaAtt);

                XmlAttribute TriGiaAtt = docNPL.CreateAttribute("TGIA_NT");
                TriGiaAtt.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                node.Attributes.Append(TriGiaAtt);

                XmlAttribute DonGiaVNDAtt = docNPL.CreateAttribute("DGIA_TT_VND");
                decimal dongiatt = hmd.TriGiaTT / hmd.SoLuong;
                DonGiaVNDAtt.Value = BaseClass.Round(dongiatt, 9);
                node.Attributes.Append(DonGiaVNDAtt);

                XmlAttribute TriGiaVNDAtt = docNPL.CreateAttribute("TGIA_TT_VND");
                TriGiaVNDAtt.Value = BaseClass.Round(hmd.TriGiaTT, 9);
                node.Attributes.Append(TriGiaVNDAtt);

                XmlAttribute TS_XNKAtt = docNPL.CreateAttribute("TS_XNK");
                TS_XNKAtt.Value = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.Attributes.Append(TS_XNKAtt);

                XmlAttribute TS_TTDBAtt = docNPL.CreateAttribute("TS_TTDB");
                TS_TTDBAtt.Value = BaseClass.Round(hmd.ThueSuatTTDB, 9);
                node.Attributes.Append(TS_TTDBAtt);

                XmlAttribute TS_VSTAtt = docNPL.CreateAttribute("TS_VAT");
                TS_VSTAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(TS_VSTAtt);

                XmlAttribute TL_PhuThuAtt = docNPL.CreateAttribute("TL_PHU_THU");
                TL_PhuThuAtt.Value = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.Attributes.Append(TL_PhuThuAtt);

                XmlAttribute ThueXNKAtt = docNPL.CreateAttribute("THUE_XNK");
                ThueXNKAtt.Value = BaseClass.Round(hmd.ThueXNK, 9);
                node.Attributes.Append(ThueXNKAtt);

                XmlAttribute ThueTTDBAtt = docNPL.CreateAttribute("THUE_TTDB");
                ThueTTDBAtt.Value = BaseClass.Round(hmd.ThueTTDB, 9);
                node.Attributes.Append(ThueTTDBAtt);

                XmlAttribute ThueVATAtt = docNPL.CreateAttribute("THUE_VAT");
                ThueVATAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(ThueVATAtt);

                XmlAttribute TPhuThuAtt = docNPL.CreateAttribute("PHU_THU");
                TPhuThuAtt.Value = BaseClass.Round(hmd.PhuThu, 9);
                node.Attributes.Append(TPhuThuAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        private string ConvertCollectionToXMLSuaToKhaiXuat(string maMid)
        {
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\SUA_ToKhaiXuat.xml");

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            nodeHQNhan.Attributes["TEN_HQ"].Value = DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            //thong tin to khai
            XmlNode nodeToKhai = docNPL.GetElementsByTagName("TO_KHAI")[0];
            nodeToKhai.Attributes["XUAT_NPL_SP"].Value = this.LoaiHangHoa;
            nodeToKhai.Attributes["MA_LH"].Value = this.MaLoaiHinh;
            nodeToKhai.Attributes["MA_HQ"].Value = this.MaHaiQuan.Trim();
            nodeToKhai.Attributes["MA_DV_XK"].Value = this.MaDoanhNghiep;
            nodeToKhai.Attributes["MA_DV_UT"].Value = this.MaDonViUT;
            nodeToKhai.Attributes["MA_DV_NK"].Value = "";
            //nodeToKhai.Attributes["MA_DV_KT"].Value = this.MaDaiLyTTHQ;
            if (this.TenDonViDoiTac.Length > 30)
                nodeToKhai.Attributes["TEN_DV_NK"].Value = this.TenDonViDoiTac.Substring(0, 30);
            else
                nodeToKhai.Attributes["TEN_DV_NK"].Value = this.TenDonViDoiTac;
            if (this.SoGiayPhep.Length <= 35)
                nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep;
            else
                nodeToKhai.Attributes["SO_GP"].Value = this.SoGiayPhep.Substring(0, 35);
            if (this.NgayGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_GP"].Value = this.NgayGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_GP"].Value = null;
            if (this.NgayHetHanGiayPhep.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHGP"].Value = this.NgayHetHanGiayPhep.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHGP"].Value = null;
            if (this.SoHopDong.Length <= 50)
                nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong;
            else
                nodeToKhai.Attributes["SO_HD"].Value = this.SoHopDong.Substring(0, 50);
            if (this.NgayHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HD"].Value = this.NgayHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HD"].Value = null;
            if (this.NgayHetHanHopDong.Year > 1900)
                nodeToKhai.Attributes["NGAY_HHHD"].Value = this.NgayHetHanHopDong.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HHHD"].Value = null;
            nodeToKhai.Attributes["SO_HDTM"].Value = this.SoHoaDonThuongMai;
            if (this.NgayHoaDonThuongMai.Year > 1900)
                nodeToKhai.Attributes["NGAY_HDTM"].Value = this.NgayHoaDonThuongMai.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_HDTM"].Value = null;
            nodeToKhai.Attributes["MA_PTVT"].Value = this.PTVT_ID;
            nodeToKhai.Attributes["TEN_PTVT"].Value = this.SoHieuPTVT;
            if (this.NgayDenPTVT.Year > 1900)
                nodeToKhai.Attributes["NGAY_DEN"].Value = this.NgayDenPTVT.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_DEN"].Value = null;
            if (SoVanDon.Length <= 25)
                nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon;
            else
                nodeToKhai.Attributes["SO_VANDON"].Value = this.SoVanDon.Substring(0, 25);
            if (this.NgayVanDon.Year > 1900)
                nodeToKhai.Attributes["NGAY_VANDON"].Value = this.NgayVanDon.ToString("yyyy-MM-dd");
            else
                nodeToKhai.Attributes["NGAY_VANDON"].Value = DateTime.Today.ToString("yyyy-MM-dd");
            nodeToKhai.Attributes["NUOC_NK"].Value = this.NuocNK_ID;
            nodeToKhai.Attributes["MA_CK_XUAT"].Value = this.CuaKhau_ID;
            nodeToKhai.Attributes["MA_DKGH"].Value = this.DKGH_ID;
            nodeToKhai.Attributes["MA_NGTE"].Value = this.NguyenTe_ID;
            nodeToKhai.Attributes["TY_GIA_VND"].Value = BaseClass.Round(this.TyGiaTinhThue, 9);
            nodeToKhai.Attributes["MA_PTTT"].Value = this.PTTT_ID;
            if (this.GiayTo.Length > 40)
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo.Substring(0, 40);
            else
                nodeToKhai.Attributes["CHUNGTU_KEM"].Value = this.GiayTo;
            nodeToKhai.Attributes["SO_CONT20"].Value = Convert.ToUInt32(this.SoContainer20).ToString();
            nodeToKhai.Attributes["SO_CONT40"].Value = Convert.ToUInt32(this.SoContainer40).ToString();
            nodeToKhai.Attributes["SO_PLTK"].Value = Convert.ToUInt32(this.SoLuongPLTK).ToString();
            nodeToKhai.Attributes["SO_KIEN"].Value = Convert.ToUInt32(this.SoKien).ToString();
            nodeToKhai.Attributes["TRONG_LUONG"].Value = BaseClass.Round(this.TrongLuong, 9);
            nodeToKhai.Attributes["PHI_BH"].Value = BaseClass.Round(this.PhiBaoHiem, 9);
            decimal PhiKhacNH = this.PhiVanChuyen + this.PhiKhac;
            nodeToKhai.Attributes["PHI_VC"].Value = BaseClass.Round(PhiKhacNH, 9);
            nodeToKhai.Attributes["LE_PHI_HQ"].Value = BaseClass.Round(this.LePhiHaiQuan, 9);

            nodeToKhai.Attributes["MA_MID"].Value = maMid;

            nodeToKhai.Attributes["DE_XUAT_KHAC"].Value = this.DeXuatKhac;
            nodeToKhai.Attributes["LY_DO_SUA"].Value = FontConverter.Unicode2TCVN(this.LyDoSua);
            nodeToKhai.Attributes["SOTK"].Value = this.SoToKhai.ToString();
            nodeToKhai.Attributes["NGAY_DK"].Value = this.NgayDangKy.ToString("yyyy-MM-dd");
            //hang hoa
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                //HangMauDich hmd = new HangMauDich();
                //hmd.TKMD_ID = this.ID;
                //this.HMDCollection =  hmd.SelectCollectionBy_TKMD_ID();
                this.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(this.ID);
            }
            XmlNode nodeHang = docNPL.GetElementsByTagName("HANG")[0];
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("HANG.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT_HANG");
                sttAtt.Value = hmd.SoThuTuHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_HANG");
                maAtt.Value = hmd.MaPhu;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = hmd.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_HANG");
                TenAtt.Value = hmd.TenHang;
                node.Attributes.Append(TenAtt);

                XmlAttribute NuocAtt = docNPL.CreateAttribute("NUOC_XX");
                NuocAtt.Value = hmd.NuocXX_ID;
                node.Attributes.Append(NuocAtt);

                XmlAttribute SoLuongAtt = docNPL.CreateAttribute("LUONG");
                SoLuongAtt.Value = BaseClass.Round(hmd.SoLuong, 9);
                node.Attributes.Append(SoLuongAtt);

                XmlAttribute Ma_DVTAtt = docNPL.CreateAttribute("MA_DVT");
                Ma_DVTAtt.Value = hmd.DVT_ID;
                node.Attributes.Append(Ma_DVTAtt);

                XmlAttribute DonGiaAtt = docNPL.CreateAttribute("DGIA_NT");
                DonGiaAtt.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                node.Attributes.Append(DonGiaAtt);

                XmlAttribute TriGiaAtt = docNPL.CreateAttribute("TGIA_NT");
                TriGiaAtt.Value = BaseClass.Round(hmd.TriGiaKB, 9); ;
                node.Attributes.Append(TriGiaAtt);

                XmlAttribute DonGiaVNDAtt = docNPL.CreateAttribute("DGIA_TT_VND");
                decimal dongiatt = hmd.TriGiaTT / hmd.SoLuong;
                DonGiaVNDAtt.Value = BaseClass.Round(dongiatt, 9); ;
                node.Attributes.Append(DonGiaVNDAtt);

                XmlAttribute TriGiaVNDAtt = docNPL.CreateAttribute("TGIA_TT_VND");
                TriGiaVNDAtt.Value = BaseClass.Round(hmd.TriGiaTT, 9); ;
                node.Attributes.Append(TriGiaVNDAtt);

                XmlAttribute TS_XNKAtt = docNPL.CreateAttribute("TS_XNK");
                TS_XNKAtt.Value = BaseClass.Round(hmd.ThueSuatXNK, 9);
                node.Attributes.Append(TS_XNKAtt);

                XmlAttribute TS_TTDBAtt = docNPL.CreateAttribute("TS_TTDB");
                TS_TTDBAtt.Value = BaseClass.Round(hmd.ThueSuatTTDB, 9);
                node.Attributes.Append(TS_TTDBAtt);

                XmlAttribute TS_VSTAtt = docNPL.CreateAttribute("TS_VAT");
                TS_VSTAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(TS_VSTAtt);

                XmlAttribute TL_PhuThuAtt = docNPL.CreateAttribute("TL_PHU_THU");
                TL_PhuThuAtt.Value = BaseClass.Round(hmd.TyLeThuKhac, 9);
                node.Attributes.Append(TL_PhuThuAtt);

                XmlAttribute ThueXNKAtt = docNPL.CreateAttribute("THUE_XNK");
                ThueXNKAtt.Value = BaseClass.Round(hmd.ThueXNK, 9);
                node.Attributes.Append(ThueXNKAtt);

                XmlAttribute ThueTTDBAtt = docNPL.CreateAttribute("THUE_TTDB");
                ThueTTDBAtt.Value = BaseClass.Round(hmd.ThueTTDB, 9);
                node.Attributes.Append(ThueTTDBAtt);

                XmlAttribute ThueVATAtt = docNPL.CreateAttribute("THUE_VAT");
                ThueVATAtt.Value = BaseClass.Round(hmd.ThueGTGT, 9);
                node.Attributes.Append(ThueVATAtt);

                XmlAttribute TPhuThuAtt = docNPL.CreateAttribute("PHU_THU");
                TPhuThuAtt.Value = BaseClass.Round(hmd.PhuThu, 9);
                node.Attributes.Append(TPhuThuAtt);

                XmlAttribute MA_HTSAtt = docNPL.CreateAttribute("MA_HTS");
                MA_HTSAtt.Value = hmd.Ma_HTS;
                node.Attributes.Append(MA_HTSAtt);

                XmlAttribute DVT_HTSAtt = docNPL.CreateAttribute("MA_DVT_HTS");
                DVT_HTSAtt.Value = hmd.DVT_HTS;
                node.Attributes.Append(DVT_HTSAtt);

                XmlAttribute Luong_HTSAtt = docNPL.CreateAttribute("LUONG_HTS");
                Luong_HTSAtt.Value = BaseClass.Round(hmd.SoLuong_HTS, 9);
                node.Attributes.Append(Luong_HTSAtt);

                XmlAttribute donGia_HTSAtt = docNPL.CreateAttribute("DGIA_KB_HTS");
                decimal dongiaHTS = 0;
                try
                {
                    dongiaHTS = Convert.ToDecimal(hmd.TriGiaKB / hmd.SoLuong_HTS);
                }
                catch
                {

                }
                donGia_HTSAtt.Value = BaseClass.Round(dongiaHTS, 9);
                node.Attributes.Append(donGia_HTSAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        #endregion

        #region HỦY TỜ KHAI ĐÃ DUYỆT

        public string WSHuyToKhai(string password)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiSua(this.MaLoaiHinh.StartsWith("N") ? MessgaseType.ToKhaiNhap : MessgaseType.ToKhaiXuat, MessgaseFunction.KhaiBao));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\XMLChungTu\\HuyTKDaDuyet.XML");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.Attributes["SOTN"].Value = this.SoTiepNhan.ToString();
            root.Attributes["NAMTN"].Value = this.NamDK.ToString();

            root.SelectSingleNode("ToKhai").Attributes["MaLH"].Value = this.MaLoaiHinh;
            root.SelectSingleNode("ToKhai").Attributes["MaHQ"].Value = this.MaHaiQuan.Trim();
            root.SelectSingleNode("ToKhai").Attributes["SoTK"].Value = this.SoToKhai.ToString();
            root.SelectSingleNode("ToKhai").Attributes["NgayDK"].Value = this.NgayDangKy.ToString("yyyy-MM-dd");
            root.SelectSingleNode("ToKhai").Attributes["NamDK"].Value = this.NamDK.ToString();

            root.SelectSingleNode("DoanhNghiep").Attributes["MaDN"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("DoanhNghiep").Attributes["TenDN"].Value = this.TenDoanhNghiep;

            List<HuyToKhai> huyTk = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_TKMD_ID(this.ID);

            root.SelectSingleNode("LyDo").InnerText = FontConverter.Unicode2TCVN(huyTk[0].LyDoHuy);

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());

                kq = kdt.Send(doc.InnerXml, password);

                Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, this.ID, Company.KDT.SHARE.Components.MessageTitle.HuyToKhai);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);

                if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {

                        Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, this.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHuyTKThanhCong);
                        this.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                        this.Update();
                        huyTk[0].TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                        huyTk[0].Guid = this.GUIDSTR;
                        huyTk[0].Update();
                        return doc.InnerXml;
                    }
                }
                else
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
                }
                return doc.InnerXml;
            }
            catch
            {
                if (!string.IsNullOrEmpty(kq))
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, this.ID, Company.KDT.SHARE.Components.MessageTitle.Error);
                throw;
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_ToKhaiMauDich_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, _SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, _NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, _MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, _SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, _MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, _NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, _MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, _TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, _MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, _TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, _TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, _ChiTietDonViDoiTac);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, _SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, _NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, _NgayHetHanGiayPhep);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, _SoHopDong);
            db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, _NgayHopDong);
            db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, _NgayHetHanHopDong);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, _SoHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, _NgayHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, _PTVT_ID);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, _SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, _NgayDenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, _QuocTichPTVT_ID);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, _LoaiVanDon);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, _SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, _NgayVanDon);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, _NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, _NuocNK_ID);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, _DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, _CuaKhau_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, _DKGH_ID);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, _NguyenTe_ID);
            db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, _TyGiaTinhThue);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, _TyGiaUSD);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, _PTTT_ID);
            db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, _SoHang);
            db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, _SoLuongPLTK);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, _TenChuHang);
            db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, _ChucVu);
            db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, _SoContainer20);
            db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, _SoContainer40);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, _SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, _TrongLuong);
            db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, _TongTriGiaKhaiBao);
            db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, _TongTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, _LoaiToKhaiGiaCong);
            db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, _LePhiHaiQuan);
            db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, _PhiBaoHiem);
            db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, _PhiVanChuyen);
            db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, _PhiXepDoHang);
            db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, _PhiKhac);
            db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, _CanBoDangKy);
            db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, _QuanLyMay);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, _TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, _LoaiHangHoa);
            db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, _GiayTo);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, _PhanLuong);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, _MaDonViUT);
            db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, _TenDonViUT);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, _TrongLuongNet);
            db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, _SoTienKhoan);
            db.AddInParameter(dbCommand, "@HeSoNhan", SqlDbType.Float, _HeSoNhan);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, _GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, _DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, _LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, _ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, _GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, _NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, _HUONGDAN);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                _ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return _ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                _ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return _ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_ToKhaiMauDich_Update";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, _ID);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, _SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, _NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, _MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, _SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, _MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, _NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, _MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, _TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, _MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, _TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, _TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, _ChiTietDonViDoiTac);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, _SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, _NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, _NgayHetHanGiayPhep);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, _SoHopDong);
            db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, _NgayHopDong);
            db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, _NgayHetHanHopDong);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, _SoHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, _NgayHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, _PTVT_ID);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, _SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, _NgayDenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, _QuocTichPTVT_ID);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, _LoaiVanDon);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, _SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, _NgayVanDon);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, _NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, _NuocNK_ID);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, _DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, _CuaKhau_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, _DKGH_ID);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, _NguyenTe_ID);
            db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, _TyGiaTinhThue);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, _TyGiaUSD);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, _PTTT_ID);
            db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, _SoHang);
            db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, _SoLuongPLTK);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, _TenChuHang);
            db.AddInParameter(dbCommand, "@ChucVu", SqlDbType.NVarChar, _ChucVu);
            db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, _SoContainer20);
            db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, _SoContainer40);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, _SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, _TrongLuong);
            db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, _TongTriGiaKhaiBao);
            db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, _TongTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, _LoaiToKhaiGiaCong);
            db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, _LePhiHaiQuan);
            db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, _PhiBaoHiem);
            db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, _PhiVanChuyen);
            db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, _PhiXepDoHang);
            db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, _PhiKhac);
            db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, _CanBoDangKy);
            db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, _QuanLyMay);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, _TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, _LoaiHangHoa);
            db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, _GiayTo);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, _PhanLuong);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, _MaDonViUT);
            db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, _TenDonViUT);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, _TrongLuongNet);
            db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, _SoTienKhoan);
            db.AddInParameter(dbCommand, "@HeSoNhan", SqlDbType.Float, _HeSoNhan);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, _GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, _DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, _LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, _ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, _GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, _NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, _HUONGDAN);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public bool CloneToDB(SqlTransaction transaction)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand("ToKhaiMauDichCopy");
                db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
                if (transaction != null)
                    db.ExecuteNonQuery(dbCommand, transaction);
                else
                    db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private DataSet ToKhaiSua_KiemTra_ThongTinHang(long soToKhai, string maLoaiHinh, int namDangKy)
        {
            string spName = "p_ToKhaiSua_KiemTra_ThongTinHang";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            //db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, namDangKy);
            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soToKhai);
            db.AddInParameter(dbCommand, "@MaLH", SqlDbType.NVarChar, maLoaiHinh);

            DataSet ds = this.db.ExecuteDataSet(dbCommand);

            return ds;
        }

        private DataSet ToKhaiSua_KiemTra_ThongTinHang(SqlTransaction transaction, long soToKhai, string maLoaiHinh, int namDangKy)
        {
            string spName = "p_ToKhaiSua_KiemTra_ThongTinHang";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            //db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, namDangKy);
            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soToKhai);
            db.AddInParameter(dbCommand, "@MaLH", SqlDbType.NVarChar, maLoaiHinh);

            DataSet ds = this.db.ExecuteDataSet(dbCommand, transaction);

            return ds;
        }

        
         private Decimal TinhTongTriGiaKhaiBao(ToKhaiMauDich TKMD)
        {
            TKMD.TongTriGiaKhaiBao = 0;
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
            }
            return TKMD.TongTriGiaKhaiBao;
        }
        /// <summary>
        /// Cập nhật lại thông tin hàng của tờ khai Sửa được duyệt.
        /// </summary>
        /// <returns></returns>
        /// HungTQ, Updated 31/03/2011.
        public bool CapNhatThongTinHangToKhaiSua()
        {
            bool success = true;

            //Lay thong tin hang to khai trong Ton.
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplThanhLyNhapTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
            nplThanhLyNhapTon.SoToKhai = this.SoToKhai;
            nplThanhLyNhapTon.MaLoaiHinh = this.MaLoaiHinh;
            nplThanhLyNhapTon.NamDangKy = (short)this.NgayDangKy.Year;
            nplThanhLyNhapTon.MaHaiQuan = this.MaHaiQuan;
            //Lay du lieu NPL ton thanh ly cua to khai
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection nplNhapTonCollections = nplThanhLyNhapTon.SelectCollectionBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy();

            decimal luongChenhLech = 0;
            double thueChenhLech = 0;
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = null;
            Company.BLL.SXXK.NPLNhapTonThucTe nptTonThucTe = null;

            BLL.SXXK.ToKhai.HangMauDichCollection hmdDuyetCollections = new Company.BLL.SXXK.ToKhai.HangMauDichCollection();

            //CAP NHAT THONG TIN TO KHAI DA DANG KY
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.HMDCollection.Count == 0)
                        this.LoadHMDCollection();

                    //Lay thong tin to khai da dang ky cu (Neu co)
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkDuyetTemp = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDuyetTemp.LoadBy(this.MaHaiQuan.Trim(), this.SoToKhai, this.MaLoaiHinh, this.NgayDangKy.Year);

                    //Cap nhat thong tin to khai DA DANG KY 
                    #region TO KHAI

                    BLL.SXXK.ToKhai.ToKhaiMauDich tkDuyet = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
                    tkDuyet.PhanLuong = this.PhanLuong;
                    tkDuyet.MaHaiQuan = this.MaHaiQuan.Trim();
                    tkDuyet.MaLoaiHinh = this.MaLoaiHinh;
                    tkDuyet.NamDangKy = (tkDuyetTemp != null ? tkDuyetTemp.NamDangKy : (short)this.NgayDangKy.Year);
                    tkDuyet.NgayDangKy = (tkDuyetTemp != null ? tkDuyetTemp.NgayDangKy : this.NgayDangKy);
                    tkDuyet.SoToKhai = (tkDuyetTemp != null ? tkDuyetTemp.SoToKhai : this.SoToKhai);
                    tkDuyet.TenDonViDoiTac = this.TenDonViDoiTac;
                    tkDuyet.NgayGiayPhep = this.NgayGiayPhep;
                    tkDuyet.NgayHetHanGiayPhep = this.NgayHetHanGiayPhep;
                    tkDuyet.SoHopDong = this.SoHopDong;
                    tkDuyet.NgayHopDong = this.NgayHopDong;
                    tkDuyet.NgayHetHanHopDong = this.NgayHetHanHopDong;
                    tkDuyet.SoHoaDonThuongMai = this.SoHoaDonThuongMai;
                    tkDuyet.NgayHoaDonThuongMai = this.NgayHoaDonThuongMai;
                    tkDuyet.PTVT_ID = this.PTVT_ID;
                    tkDuyet.SoHieuPTVT = this.SoHieuPTVT;
                    tkDuyet.NgayDenPTVT = this.NgayDenPTVT;
                    tkDuyet.SoVanDon = this.SoVanDon;
                    tkDuyet.LoaiVanDon = this.LoaiVanDon;
                    tkDuyet.NgayVanDon = this.NgayVanDon;
                    tkDuyet.NuocXK_ID = this.NuocXK_ID;
                    tkDuyet.NuocNK_ID = this.NuocNK_ID;
                    tkDuyet.DiaDiemXepHang = this.DiaDiemXepHang;
                    tkDuyet.DKGH_ID = this.DKGH_ID;
                    tkDuyet.CuaKhau_ID = this.CuaKhau_ID;
                    tkDuyet.NguyenTe_ID = this.NguyenTe_ID;
                    tkDuyet.TyGiaTinhThue = this.TyGiaTinhThue;
                    tkDuyet.TyGiaUSD = this.TyGiaUSD;
                    tkDuyet.PTTT_ID = this.PTTT_ID;
                    tkDuyet.SoHang = this.SoHang;
                    tkDuyet.TenChuHang = this.TenChuHang;
                    tkDuyet.SoContainer20 = this.SoContainer20;
                    tkDuyet.SoContainer40 = this.SoContainer40;
                    tkDuyet.SoKien = this.SoKien;
                    tkDuyet.SoLuongPLTK = this.SoLuongPLTK;
                    tkDuyet.TrongLuong = this.TrongLuong;
                    tkDuyet.TrongLuongNet = this.TrongLuongNet;
                    tkDuyet.TongTriGiaKhaiBao = TinhTongTriGiaKhaiBao(this); // Cập nhật lại tổng trị giá tính thuế 
                    tkDuyet.TongTriGiaTinhThue = this.TongTriGiaTinhThue;
                    tkDuyet.LoaiToKhaiGiaCong = this.LoaiToKhaiGiaCong;
                    tkDuyet.LePhiHaiQuan = this.LePhiHaiQuan;

                    tkDuyet.PhiBaoHiem = this.PhiBaoHiem;
                    tkDuyet.PhiVanChuyen = this.PhiVanChuyen;
                    tkDuyet.PhiKhac = this.PhiKhac;
                    tkDuyet.PhiXepDoHang = this.PhiXepDoHang;

                    tkDuyet.ThanhLy = (tkDuyetTemp != null ? tkDuyetTemp.ThanhLy : "");
                    tkDuyet.Xuat_NPL_SP = this.LoaiHangHoa;
                    tkDuyet.ChungTu = this.GiayTo;
                    tkDuyet.TrangThaiThanhKhoan = (tkDuyetTemp != null ? tkDuyetTemp.TrangThaiThanhKhoan : "D");

                    tkDuyet.NgayHoanThanh = this.NgayDangKy;
                    tkDuyet.NGAY_THN_THX = (tkDuyetTemp != null ? tkDuyetTemp.NGAY_THN_THX : new DateTime(1900, 01, 01));

                    tkDuyet.HeSoNhan = this.HeSoNhan;
                    tkDuyet.MaDonViUT = this.MaDonViUT;
                    tkDuyet.MaDaiLyTTHQ = this.MaDaiLyTTHQ;
                    tkDuyet.ChiTietDonViDoiTac = this.ChiTietDonViDoiTac;
                    tkDuyet.GhiChu = this.DeXuatKhac;
                    tkDuyet.QuocTichPTVT_ID = this.QuocTichPTVT_ID;
                    tkDuyet.TenDoanhNghiep = this.TenDoanhNghiep;

                    
                    if (tkDuyet.MaLoaiHinh.StartsWith("XSX"))
                        tkDuyet.TrangThai = (tkDuyetTemp != null ? tkDuyetTemp.TrangThai : 1); //1 = chua dc phan bo

                    //Cap nhat to khai tren du lieu DATABSE TQDT.
                    tkDuyet.InsertUpdateTransaction(transaction);

                    #endregion

                    #region HANG

                    string maNPL = "";

                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        BLL.SXXK.ToKhai.HangMauDich hmdDuyet = new Company.BLL.SXXK.ToKhai.HangMauDich();
                        hmdDuyet.DonGiaKB = hmd.DonGiaKB;
                        hmdDuyet.DonGiaTT = hmd.DonGiaTT;
                        hmdDuyet.DVT_ID = hmd.DVT_ID;
                        hmdDuyet.MaHaiQuan = this.MaHaiQuan.Trim();
                        hmdDuyet.MaHS = hmd.MaHS;
                        hmdDuyet.MaLoaiHinh = this.MaLoaiHinh;
                        hmdDuyet.MaPhu = hmd.MaPhu;
                        hmdDuyet.MienThue = hmd.MienThue;
                        hmdDuyet.NamDangKy = (short)this.NgayDangKy.Year;
                        hmdDuyet.NuocXX_ID = hmd.NuocXX_ID;
                        hmdDuyet.PhuThu = hmd.PhuThu;
                        hmdDuyet.SoLuong = hmd.SoLuong;
                        hmdDuyet.SoThuTuHang = (short)hmd.SoThuTuHang;
                        hmdDuyet.SoToKhai = this.SoToKhai;
                        hmdDuyet.TenHang = hmd.TenHang;
                        hmdDuyet.ThueGTGT = hmd.ThueGTGT;
                        hmdDuyet.ThueSuatGTGT = hmd.ThueSuatGTGT;
                        hmdDuyet.ThueSuatTTDB = hmd.ThueSuatTTDB;
                        hmdDuyet.ThueSuatXNK = hmd.ThueSuatXNK;
                        hmdDuyet.ThueTTDB = hmd.ThueTTDB;
                        hmdDuyet.ThueXNK = hmd.ThueXNK;
                        hmdDuyet.TriGiaKB = hmd.TriGiaKB;
                        hmdDuyet.TriGiaKB_VND = hmd.TriGiaKB_VND;
                        hmdDuyet.TriGiaThuKhac = hmd.TriGiaThuKhac;
                        hmdDuyet.TriGiaTT = hmd.TriGiaTT;
                        hmdDuyet.TyLeThuKhac = hmd.TyLeThuKhac;

                        //TO KHAI NHAP
                        if (tkDuyet.MaLoaiHinh.Substring(0, 1) == "N")
                        {
                            //Kiem tra NPL trong danh sach
                            BLL.SXXK.ToKhai.HangMauDich hmdDuyetTemp = FindHMDDangKy(hmdDuyetCollections, hmdDuyet.MaHaiQuan, hmdDuyet.SoToKhai, hmdDuyet.MaLoaiHinh, hmdDuyet.NamDangKy, hmdDuyet.MaPhu);

                            if (hmdDuyetTemp != null)
                            {
                                hmdDuyet.SoLuong += hmdDuyetTemp.SoLuong;
                                hmdDuyet.ThueXNK += hmdDuyetTemp.ThueXNK;
                                hmdDuyet.TriGiaKB += hmdDuyetTemp.TriGiaKB;

                                hmdDuyet.DonGiaKB = hmdDuyet.TriGiaKB / hmdDuyet.SoLuong;
                                hmdDuyet.DonGiaTT = hmdDuyet.DonGiaKB * tkDuyet.TyGiaTinhThue;

                                if (hmdDuyet.ThueXNK > 0)
                                    hmdDuyet.TriGiaTT = hmdDuyet.ThueXNK * 100 / hmdDuyet.ThueSuatXNK;
                                else
                                    hmdDuyet.TriGiaTT = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;

                                hmdDuyet.TriGiaKB_VND = hmdDuyet.TriGiaKB * tkDuyet.TyGiaTinhThue;

                                //Update obj
                                hmdDuyetTemp.SoLuong = hmdDuyet.SoLuong;
                                hmdDuyetTemp.ThueXNK = hmdDuyet.ThueXNK;
                                hmdDuyetTemp.TriGiaKB = hmdDuyet.TriGiaKB;
                                hmdDuyetTemp.DonGiaKB = hmdDuyet.DonGiaKB;
                                hmdDuyetTemp.DonGiaTT = hmdDuyet.DonGiaTT;
                                hmdDuyetTemp.TriGiaTT = hmdDuyet.TriGiaTT;
                                hmdDuyetTemp.TriGiaKB_VND = hmdDuyet.TriGiaKB_VND;
                            }
                            else
                                hmdDuyetCollections.Add(hmdDuyet);
                        }
                        else
                            hmdDuyetCollections.Add(hmdDuyet);
                    }

                    foreach (BLL.SXXK.ToKhai.HangMauDich obj in hmdDuyetCollections)
                    {
                        //Luu hang mau dich cua to khai.
                        obj.InsertUpdateTransaction(transaction);
                    }


                    //Updated 06/07/2012. Xoa thong tin hang trong to khai da dang ky neu khong co tren to khai khai bao sua doi bo sung.
                    DataSet dsHangBiXoa = ToKhaiSua_KiemTra_ThongTinHang(transaction, (long)tkDuyet.SoToKhai, tkDuyet.MaLoaiHinh, tkDuyet.NamDangKy);

                    if (dsHangBiXoa != null && dsHangBiXoa.Tables.Count > 0)
                    {
                        Company.BLL.SXXK.ToKhai.HangMauDich hangXoa = new Company.BLL.SXXK.ToKhai.HangMauDich();
                        string maHang = "";

                        foreach (DataRow dr in dsHangBiXoa.Tables[0].Rows)
                        {
                            maHang = dr["MaPhu"].ToString();

                            hangXoa.DeleteTransaction(transaction, (long)tkDuyet.SoToKhai, tkDuyet.MaLoaiHinh, tkDuyet.NamDangKy, tkDuyet.MaHaiQuan, maHang);

                            Company.KDT.SHARE.Components.Globals.SaveMessage("", this.ID, this.GUIDSTR, this.MaLoaiHinh.Substring(0, 1) == "N" ? MessageTypes.ToKhaiNhap : MessageTypes.ToKhaiXuat, MessageFunctions.SuaToKhai, Company.KDT.SHARE.Components.MessageTitle.ToKhaiSuaDuocDuyet,
                                string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nXóa thông tin mã: {4}", this.SoToKhai, this.NgayDangKy.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuan.Trim(), maHang));
                        }
                    }

                    #endregion

                    //TO KHAI NHAP
                    if (tkDuyet.MaLoaiHinh.Substring(0, 1) == "N")
                    {
                        string noiDunng = "";
                        foreach (BLL.SXXK.ToKhai.HangMauDich hmdTemp in hmdDuyetCollections)
                        {
                            noiDunng = "";
                            //I. Cap nhat bang Thanh ly NPL ton
                            //Lay thong tin So luong & Ton cua NPL
                            nplTon = FindNPLNhapTon(nplNhapTonCollections, this.SoToKhai, this.MaLoaiHinh, this.MaHaiQuan, this.NgayDangKy.Year, hmdTemp.MaPhu);
                            if (nplTon != null)
                            {
                                #region nplTon

                                //Kiem tra va cap nhat lai thong tin So luong & Ton cho Nguyen phu lieu TK NHAP

                                luongChenhLech = hmdTemp.SoLuong - nplTon.Luong;
                                thueChenhLech = (double)hmdTemp.ThueXNK - nplTon.ThueXNK;

                                if (luongChenhLech != 0 || thueChenhLech != 0)
                                {
                                    //Cap nhat lai thong tin luong & ton
                                    nplTon.Luong += luongChenhLech;
                                    nplTon.Ton += luongChenhLech;
                                    //Cap nhat lai thong tin thue XNK & thue ton XNK
                                    nplTon.ThueXNK += thueChenhLech;
                                    nplTon.ThueXNKTon += thueChenhLech;

                                    nplTon.UpdateTransaction(transaction);

                                    //Ghi lai ket qua xu ly
                                    KetQuaXuLy kqxl = new KetQuaXuLy();
                                    kqxl.ItemID = this.ID;
                                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                                    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiNPLNhapTon;

                                    noiDunng = string.Format("-> Cập nhật Nguyên phụ liệu nhập tồn:\r\nHàng tờ khai: {0}\r\nSố lượng cũ: {1} - Số lượng mới: {2}\r\nTồn cũ: {3} - Tồn mới: {4}\r\nThuế cũ: {5} - Thuế mới: {6}\r\nThuế tồn cũ: {7} - Thuế tồn mới: {8}",
                                        hmdTemp.MaPhu, nplTon.Luong - luongChenhLech, nplTon.Luong, nplTon.Ton - luongChenhLech, nplTon.Ton, nplTon.ThueXNK - thueChenhLech, nplTon.ThueXNK, nplTon.ThueXNKTon - thueChenhLech, nplTon.ThueXNKTon);
                                    kqxl.NoiDung = noiDunng;

                                    kqxl.Ngay = DateTime.Now;
                                    kqxl.Insert();

                                    //TODO: Hungtq updated 01/03/2012. Luu log
                                    Company.KDT.SHARE.Components.Globals.SaveMessage("", this.ID, this.GUIDSTR,
                                        this.MaHaiQuan, this.MaDoanhNghiep,
                                        tkDuyet.MaLoaiHinh.Substring(0, 1) == "N" ? Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap : Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat,
                                        Company.KDT.SHARE.Components.MessageFunctions.LayPhanHoi,
                                        this.LyDoSua.Trim().Length > 0 ? Company.KDT.SHARE.Components.MessageTitle.ToKhaiSuaDuocDuyet : Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo, noiDunng);


                                    //II. Cap nhat bang NPL ton thuc te
                                    nptTonThucTe = Company.BLL.SXXK.NPLNhapTonThucTe.Load(this.SoToKhai, this.MaLoaiHinh, (short)this.NgayDangKy.Year, this.MaHaiQuan, hmdTemp.MaPhu, transaction);
                                    if (nptTonThucTe != null)
                                    {
                                        #region nptTonThucTe

                                        //Cap nhat lai thong tin luong & ton
                                        nptTonThucTe.Luong += luongChenhLech;
                                        nptTonThucTe.Ton += luongChenhLech;
                                        //Cap nhat lai thong tin thue XNK & thue ton XNK
                                        nptTonThucTe.ThueXNK += thueChenhLech;
                                        nptTonThucTe.ThueXNKTon += thueChenhLech;

                                        nptTonThucTe.Update(transaction);

                                        //Ghi lai ket qua xu ly
                                        kqxl = new KetQuaXuLy();
                                        kqxl.ItemID = this.ID;
                                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiNPLTonThucte;

                                        noiDunng = string.Format("-> Cập nhật Nguyên phụ liệu tồn thực tế:\r\nHàng tờ khai: {0}\r\nSố lượng cũ: {1} - Số lượng mới: {2}\r\nTồn cũ: {3} - Tồn mới: {4}\r\nThuế cũ: {5} - Thuế mới: {6}\r\nThuế tồn cũ: {7} - Thuế tồn mới: {8}",
                                             hmdTemp.MaPhu, nptTonThucTe.Luong - luongChenhLech, nptTonThucTe.Luong, nptTonThucTe.Ton - luongChenhLech, nptTonThucTe.Ton, nptTonThucTe.ThueXNK - thueChenhLech, nptTonThucTe.ThueXNK, nptTonThucTe.ThueXNKTon - thueChenhLech, nptTonThucTe.ThueXNKTon);
                                        kqxl.NoiDung = noiDunng;

                                        kqxl.Ngay = DateTime.Now;
                                        kqxl.Insert();

                                        //TODO: Hungtq updated 01/03/2012. Luu log
                                        Company.KDT.SHARE.Components.Globals.SaveMessage("", this.ID, this.GUIDSTR,
                                           this.MaHaiQuan, this.MaDoanhNghiep,
                                           tkDuyet.MaLoaiHinh.Substring(0, 1) == "N" ? Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap : Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat,
                                           Company.KDT.SHARE.Components.MessageFunctions.LayPhanHoi,
                                           this.LyDoSua.Trim().Length > 0 ? Company.KDT.SHARE.Components.MessageTitle.ToKhaiSuaDuocDuyet : Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo, noiDunng);

                                        #endregion
                                    }
                                }

                                #endregion
                            }
                            //Hungtq updated 05/09/2012.
                            //Bổ sung thêm vao NPL tồn khi tờ khai sửa có thay đổi mã NPL mới
                            else
                            {
                                #region nplTon
                                //cap nhat vao hang dieu chinh
                                BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                                nplNhapTon.SoToKhai = tkDuyet.SoToKhai;
                                nplNhapTon.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                                nplNhapTon.NamDangKy = tkDuyet.NamDangKy;
                                nplNhapTon.MaHaiQuan = tkDuyet.MaHaiQuan;
                                nplNhapTon.MaNPL = hmdTemp.MaPhu;
                                nplNhapTon.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                                nplNhapTon.Luong = hmdTemp.SoLuong;
                                nplNhapTon.Ton = hmdTemp.SoLuong;
                                nplNhapTon.ThueXNK = Convert.ToDouble(hmdTemp.ThueXNK);
                                nplNhapTon.ThueXNKTon = Convert.ToDouble(hmdTemp.ThueXNK);
                                nplNhapTon.ThueTTDB = Convert.ToDouble(hmdTemp.ThueTTDB);
                                nplNhapTon.ThueVAT = Convert.ToDouble(hmdTemp.ThueGTGT);
                                nplNhapTon.PhuThu = Convert.ToDouble(hmdTemp.PhuThu);
                                nplNhapTon.ThueCLGia = Convert.ToDouble(hmdTemp.TriGiaThuKhac);
                                nplNhapTon.NgayDangKy = tkDuyet.NgayDangKy;

                                //Luu NPL ton cua to khai tren bang du lieu cung DATABSE TQDT.
                                nplNhapTon.InsertUpdateTransaction(transaction);
                                #endregion

                                #region nplTon Thuc te
                                //CAP NHAT VAO LUONG TON THUC TE
                                BLL.SXXK.NPLNhapTonThucTe nplNhapTonThucTe = new Company.BLL.SXXK.NPLNhapTonThucTe();
                                nplNhapTonThucTe.SoToKhai = tkDuyet.SoToKhai;
                                nplNhapTonThucTe.MaLoaiHinh = tkDuyet.MaLoaiHinh;
                                nplNhapTonThucTe.NamDangKy = tkDuyet.NamDangKy;
                                nplNhapTonThucTe.MaHaiQuan = tkDuyet.MaHaiQuan;
                                nplNhapTonThucTe.MaNPL = hmdTemp.MaPhu;
                                nplNhapTonThucTe.MaDoanhNghiep = tkDuyet.MaDoanhNghiep;
                                nplNhapTonThucTe.Luong = hmdTemp.SoLuong;
                                nplNhapTonThucTe.Ton = hmdTemp.SoLuong;
                                nplNhapTonThucTe.ThueXNK = Convert.ToDouble(hmdTemp.ThueXNK);
                                nplNhapTonThucTe.ThueXNKTon = Convert.ToDouble(hmdTemp.ThueXNK);
                                nplNhapTonThucTe.ThueTTDB = Convert.ToDouble(hmdTemp.ThueTTDB);
                                nplNhapTonThucTe.ThueVAT = Convert.ToDouble(hmdTemp.ThueGTGT);
                                nplNhapTonThucTe.PhuThu = Convert.ToDouble(hmdTemp.PhuThu);
                                nplNhapTonThucTe.ThueCLGia = Convert.ToDouble(hmdTemp.TriGiaThuKhac); ;
                                nplNhapTonThucTe.NgayDangKy = tkDuyet.NgayDangKy;

                                //Luu NPL ton thuc te cua to khai tren bang du lieu cung DATABSE TQDT.
                                nplNhapTonThucTe.InsertUpdate(transaction);
                                #endregion
                            }
                        }
                    }

                    success = true;
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    success = false;
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return success;
        }

        private BLL.SXXK.ToKhai.HangMauDich FindHMDDangKy(BLL.SXXK.ToKhai.HangMauDichCollection hmdCollections,
            string maHaiQuan, int soToKhai, string maLoaiHinh, int namDangKy, string maPhu)
        {
            foreach (BLL.SXXK.ToKhai.HangMauDich hmd in hmdCollections)
            {
                if (hmd.SoToKhai == soToKhai && hmd.MaLoaiHinh == maLoaiHinh && hmd.NamDangKy == namDangKy && hmd.MaPhu == maPhu && hmd.MaHaiQuan == maHaiQuan)
                {
                    return hmd;
                }
            }

            return null;
        }

        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTon FindNPLNhapTon(Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection nplNhapTonCollections,
            int soToKhai, string maLoaihinh, string maHaiQuan, int namDangKy, string maNPL)
        {
            foreach (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon item in nplNhapTonCollections)
            {
                if (item.SoToKhai == soToKhai && item.MaLoaiHinh == maLoaihinh && item.NamDangKy == namDangKy && item.MaHaiQuan.Trim() == maHaiQuan.Trim() && item.MaNPL == maNPL)
                {
                    return item;
                }
            }

            return null;
        }

        #region ĐỒNG BỘ DỮ LIỆU

        /// <summary>
        /// H.N.Khánh 24-05-2012
        /// insert update to khai Dong Bo Du Lieu
        /// </summary>
        /// <returns> lỗi </returns>
        public string InsertFullFromISyncDaTa()
        {
            string error = string.Empty;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.ID = 0;
                    this.ID = this.InsertTransaction(transaction);
                    int i = 1;
                    #region Hàng mậu dịch
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        hmd.SoThuTuHang = i++;
                        hmd.TKMD_ID = this.ID;
                        hmd.ID = hmd.Insert(transaction);
                        if (this.MaLoaiHinh.Substring(0, 1).ToUpper() == "N")
                        {
                            Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                            npl = Company.BLL.SXXK.NguyenPhuLieu.getNguyenPhuLieu(this.MaHaiQuan, this.MaDoanhNghiep, hmd.MaPhu);
                            if (npl == null || string.IsNullOrEmpty(npl.Ma))
                            {
                                npl.Ma = hmd.MaPhu;
                                npl.MaDoanhNghiep = this.MaDoanhNghiep;
                                npl.MaHaiQuan = this.MaHaiQuan;
                                npl.MaHS = hmd.MaHS;
                                npl.Ten = hmd.TenHang;
                                npl.DVT_ID = hmd.DVT_ID;
                                npl.InsertTransaction(transaction);
                            }
                        }
                        else
                        {
                            Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                            sp = Company.BLL.SXXK.SanPham.getSanPham(this.MaHaiQuan, this.MaDoanhNghiep, hmd.MaPhu);
                            if (sp == null || string.IsNullOrEmpty(sp.Ma))
                            {
                                sp.Ma = hmd.MaPhu;
                                sp.MaDoanhNghiep = this.MaDoanhNghiep;
                                sp.MaHaiQuan = this.MaHaiQuan;
                                sp.MaHS = hmd.MaHS;
                                sp.Ten = hmd.TenHang;
                                sp.DVT_ID = hmd.DVT_ID;
                                sp.InsertTransaction(transaction);
                            }
                        }

                    }
                    #endregion


                    #region chứng từ hải quan
                    i = 1;
                    foreach (ChungTu ct in this.ChungTuTKCollection)
                    {
                        ct.STTHang = i++;
                        ct.ID = 0;
                        ct.Master_ID = this.ID;
                        ct.ID = ct.InsertTransaction(transaction);

                    }
                    i = 1;
                    foreach (CO co in this.ListCO)
                    {
                        co.TKMD_ID = this.ID;
                        co.ID = 0;
                        co.ID = co.InsertTransaction(transaction, db);
                        foreach (HangCoDetail item in co.ListHMDofCo)
                        {
                            item.Co_ID = co.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (ChungTuNo chungtuno in this.ChungTuNoCollection)
                    {
                        chungtuno.ID = 0;
                        chungtuno.TKMDID = this.ID;
                        chungtuno.InsertTransaction(transaction, db);
                    }
                    foreach (ChungTuKem ctk in this.ChungTuKemCollection)
                    {
                        ctk.ID = 0;
                        ctk.TKMDID = this.ID;
                        ctk.ID = ctk.InsertTransaction(transaction, db);
                        foreach (ChungTuKemChiTiet item in ctk.listCTChiTiet)
                        {
                            item.ID = 0;
                            item.ChungTuKemID = ctk.ID;
                            item.InsertTransaction(transaction, db);
                        }
                    }
                    foreach (GiayPhep gp in this.GiayPhepCollection)
                    {
                        gp.ID = 0;
                        gp.TKMD_ID = this.ID;
                        gp.ID = gp.InsertTransaction(transaction, db);
                        foreach (HangGiayPhepDetail item in gp.ListHMDofGiayPhep)
                        {
                            item.ID = 0;
                            item.GiayPhep_ID = gp.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (HoaDonThuongMai hdtm in this.HoaDonThuongMaiCollection)
                    {
                        hdtm.ID = 0;
                        hdtm.TKMD_ID = this.ID;
                        hdtm.ID = hdtm.InsertTransaction(transaction, db);
                        foreach (HoaDonThuongMaiDetail item in hdtm.ListHangMDOfHoaDon)
                        {
                            item.ID = 0;
                            item.HoaDonTM_ID = hdtm.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (HopDongThuongMai hopdong in this.HopDongThuongMaiCollection)
                    {
                        hopdong.ID = 0;
                        hopdong.TKMD_ID = this.ID;
                        hopdong.InsertTransaction(transaction, db);
                        foreach (HopDongThuongMaiDetail item in hopdong.ListHangMDOfHopDong)
                        {
                            item.ID = 0;
                            item.HopDongTM_ID = hopdong.ID;
                            foreach (HangMauDich hmd in this.HMDCollection)
                                if (hmd.MaPhu == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.Insert(transaction);
                        }
                    }
                    foreach (DeNghiChuyenCuaKhau denghi in this.listChuyenCuaKhau)
                    {
                        denghi.ID = 0;
                        denghi.TKMD_ID = this.ID;
                        denghi.InsertTransaction(transaction, db);
                    }
                    foreach (NoiDungDieuChinhTK noidungsua in this.NoiDungDieuChinhTKCollection)
                    {
                        noidungsua.ID = 0;
                        noidungsua.TKMD_ID = this.ID;
                        noidungsua.Insert(transaction);
                    }
                    if (VanTaiDon != null)
                    {
                        VanTaiDon.ID = 0;
                        VanTaiDon.TKMD_ID = this.ID;
                        VanTaiDon.Insert(transaction);
                    }

                    #endregion


                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    error = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            return error;
        }
        /// <summary>
        /// Đếm số tờ khai đã thông quan
        /// </summary>
        /// <param name="madoanhnghiep"></param>
        /// <returns></returns>
        public long SelectCountSoTKThongQuan(string madoanhnghiep)
        {
            string where = "MaDoanhNghiep='" + madoanhnghiep + "' AND TrangThaiXuLy = 1 AND HUONGDAN<>'' AND MSG_STATUS IS NULL";
            ToKhaiMauDichCollection tkmdco = new ToKhaiMauDichCollection();
            tkmdco = SelectCollectionDynamicDongBoDuLieu(where, null);
            if (tkmdco == null || tkmdco.Count == 0)
            {
                return 0;
            }
            else
                return tkmdco.Count;
        }
        /// <summary>
        /// Tìm kiếm số tờ khai đã duyệt để đồng bộ dữ liệu
        /// </summary>
        /// <param name="whereCondition"></param>
        /// <param name="orderByExpression"></param>
        /// <returns></returns>
        public IDataReader SelectDynamicDongBoDuLieu(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectDynamicDongBoDuLieu]";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }
        public int TrangThaiDongBo;
        public ToKhaiMauDichCollection SelectCollectionDynamicDongBoDuLieu(string whereCondition, string orderByExpression)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            IDataReader reader = this.SelectDynamicDongBoDuLieu(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_STATUS"))) entity.TrangThaiDongBo = reader.GetInt32(reader.GetOrdinal("MSG_STATUS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChucVu"))) entity.ChucVu = reader.GetString(reader.GetOrdinal("ChucVu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public ToKhaiMauDichCollection seachTKMDDBDL(string MaDoanhNghiep, DateTime FromDay, DateTime ToDay, string userNameDL)
        {
            string spName = "[dbo].[p_KDT_ToKhaiMauDich_SeachDBDL]";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@UserName", SqlDbType.NVarChar, userNameDL);
            db.AddInParameter(dbCommand, "@FromDay", SqlDbType.DateTime, FromDay);
            db.AddInParameter(dbCommand, "@ToDay", SqlDbType.DateTime, ToDay);
            IDataReader reader = db.ExecuteReader(dbCommand);
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiDongBoTaiXuong"))) entity.TrangThaiDongBoTaiXuong = reader.GetInt32(reader.GetOrdinal("TrangThaiDongBoTaiXuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiDongBoTaiLen"))) entity.TrangThaiDongBoTaiLen = reader.GetInt32(reader.GetOrdinal("TrangThaiDongBoTaiLen"));
                if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.TenDaiLyKhaiBao = reader.GetString(reader.GetOrdinal("UserName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion
    }
}
