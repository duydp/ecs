﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL.SXXK;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;

namespace Company.Interface.GC
{
    partial class PhieuNhapKhoForm
    {
        private UIGroupBox uiGroupBox3;
        private ImageList ImageList1;
        private IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhieuNhapKhoForm));
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoPhieu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongVAT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTyGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrNgoaiTe = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtLyDo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNhapTaiKho = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoBL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoInvoice = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNhaCungCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtKhachHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayInvoice = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayNhapKho = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDong = new Janus.Windows.EditControls.UIButton();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.btnIn = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtThanhTien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongChungTu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongThucNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrMaTienTe = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnThem = new Janus.Windows.EditControls.UIButton();
            this.txtMoTaHH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMSVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMa2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSize = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMauSac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnIn);
            this.grbMain.Controls.Add(this.btnLuu);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.btnDong);
            this.grbMain.Size = new System.Drawing.Size(941, 594);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgList);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(9, 168);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(893, 204);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 6;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(3, 8);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.ScrollBarWidth = 17;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(887, 193);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtSoPhieu);
            this.uiGroupBox4.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox4.Controls.Add(this.txtTongVAT);
            this.uiGroupBox4.Controls.Add(this.txtSoKien);
            this.uiGroupBox4.Controls.Add(this.txtTyGia);
            this.uiGroupBox4.Controls.Add(this.ctrNgoaiTe);
            this.uiGroupBox4.Controls.Add(this.txtLyDo);
            this.uiGroupBox4.Controls.Add(this.txtNhapTaiKho);
            this.uiGroupBox4.Controls.Add(this.txtSoBL);
            this.uiGroupBox4.Controls.Add(this.txtSoInvoice);
            this.uiGroupBox4.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox4.Controls.Add(this.txtNhaCungCap);
            this.uiGroupBox4.Controls.Add(this.txtKhachHang);
            this.uiGroupBox4.Controls.Add(this.clcNgayInvoice);
            this.uiGroupBox4.Controls.Add(this.clcNgayNhapKho);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.label16);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Controls.Add(this.label15);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.label11);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Location = new System.Drawing.Point(14, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(913, 165);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtSoPhieu
            // 
            this.txtSoPhieu.DecimalDigits = 20;
            this.txtSoPhieu.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoPhieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoPhieu.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoPhieu.Location = new System.Drawing.Point(544, 13);
            this.txtSoPhieu.MaxLength = 15;
            this.txtSoPhieu.Name = "txtSoPhieu";
            this.txtSoPhieu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoPhieu.Size = new System.Drawing.Size(118, 21);
            this.txtSoPhieu.TabIndex = 28;
            this.txtSoPhieu.Text = "0";
            this.txtSoPhieu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoPhieu.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoPhieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 20;
            this.txtSoToKhai.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhai.Location = new System.Drawing.Point(544, 70);
            this.txtSoToKhai.MaxLength = 15;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhai.Size = new System.Drawing.Size(118, 21);
            this.txtSoToKhai.TabIndex = 28;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtTongVAT
            // 
            this.txtTongVAT.DecimalDigits = 20;
            this.txtTongVAT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongVAT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongVAT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongVAT.Location = new System.Drawing.Point(770, 127);
            this.txtTongVAT.MaxLength = 15;
            this.txtTongVAT.Name = "txtTongVAT";
            this.txtTongVAT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongVAT.Size = new System.Drawing.Size(118, 21);
            this.txtTongVAT.TabIndex = 28;
            this.txtTongVAT.Text = "0";
            this.txtTongVAT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongVAT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongVAT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoKien
            // 
            this.txtSoKien.DecimalDigits = 20;
            this.txtSoKien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoKien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoKien.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoKien.Location = new System.Drawing.Point(544, 128);
            this.txtSoKien.MaxLength = 15;
            this.txtSoKien.Name = "txtSoKien";
            this.txtSoKien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoKien.Size = new System.Drawing.Size(118, 21);
            this.txtSoKien.TabIndex = 28;
            this.txtSoKien.Text = "0";
            this.txtSoKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoKien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtTyGia
            // 
            this.txtTyGia.DecimalDigits = 20;
            this.txtTyGia.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTyGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGia.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTyGia.Location = new System.Drawing.Point(268, 100);
            this.txtTyGia.MaxLength = 15;
            this.txtTyGia.Name = "txtTyGia";
            this.txtTyGia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGia.Size = new System.Drawing.Size(115, 21);
            this.txtTyGia.TabIndex = 28;
            this.txtTyGia.Text = "0";
            this.txtTyGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTyGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyGia.VisualStyleManager = this.vsmMain;
            // 
            // ctrNgoaiTe
            // 
            this.ctrNgoaiTe.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrNgoaiTe.Appearance.Options.UseBackColor = true;
            this.ctrNgoaiTe.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrNgoaiTe.Code = "";
            this.ctrNgoaiTe.ColorControl = System.Drawing.Color.Empty;
            this.ctrNgoaiTe.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNgoaiTe.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNgoaiTe.IsOnlyWarning = false;
            this.ctrNgoaiTe.IsValidate = true;
            this.ctrNgoaiTe.Location = new System.Drawing.Point(112, 100);
            this.ctrNgoaiTe.Name = "ctrNgoaiTe";
            this.ctrNgoaiTe.Name_VN = "";
            this.ctrNgoaiTe.SetOnlyWarning = false;
            this.ctrNgoaiTe.SetValidate = false;
            this.ctrNgoaiTe.ShowColumnCode = true;
            this.ctrNgoaiTe.ShowColumnName = false;
            this.ctrNgoaiTe.Size = new System.Drawing.Size(79, 21);
            this.ctrNgoaiTe.TabIndex = 23;
            this.ctrNgoaiTe.TagName = "";
            this.ctrNgoaiTe.Where = null;
            this.ctrNgoaiTe.WhereCondition = "";
            // 
            // txtLyDo
            // 
            this.txtLyDo.Location = new System.Drawing.Point(112, 128);
            this.txtLyDo.Name = "txtLyDo";
            this.txtLyDo.Size = new System.Drawing.Size(271, 21);
            this.txtLyDo.TabIndex = 21;
            this.txtLyDo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLyDo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtNhapTaiKho
            // 
            this.txtNhapTaiKho.Location = new System.Drawing.Point(112, 73);
            this.txtNhapTaiKho.Name = "txtNhapTaiKho";
            this.txtNhapTaiKho.Size = new System.Drawing.Size(271, 21);
            this.txtNhapTaiKho.TabIndex = 21;
            this.txtNhapTaiKho.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNhapTaiKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoBL
            // 
            this.txtSoBL.Location = new System.Drawing.Point(770, 70);
            this.txtSoBL.Name = "txtSoBL";
            this.txtSoBL.Size = new System.Drawing.Size(118, 21);
            this.txtSoBL.TabIndex = 21;
            this.txtSoBL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoBL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoInvoice
            // 
            this.txtSoInvoice.Location = new System.Drawing.Point(544, 100);
            this.txtSoInvoice.Name = "txtSoInvoice";
            this.txtSoInvoice.Size = new System.Drawing.Size(118, 21);
            this.txtSoInvoice.TabIndex = 21;
            this.txtSoInvoice.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoInvoice.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(770, 12);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(118, 21);
            this.txtSoHopDong.TabIndex = 21;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtNhaCungCap
            // 
            this.txtNhaCungCap.Location = new System.Drawing.Point(544, 42);
            this.txtNhaCungCap.Name = "txtNhaCungCap";
            this.txtNhaCungCap.Size = new System.Drawing.Size(344, 21);
            this.txtNhaCungCap.TabIndex = 21;
            this.txtNhaCungCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNhaCungCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtKhachHang
            // 
            this.txtKhachHang.Location = new System.Drawing.Point(112, 43);
            this.txtKhachHang.Name = "txtKhachHang";
            this.txtKhachHang.Size = new System.Drawing.Size(271, 21);
            this.txtKhachHang.TabIndex = 21;
            this.txtKhachHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKhachHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // clcNgayInvoice
            // 
            this.clcNgayInvoice.Location = new System.Drawing.Point(770, 100);
            this.clcNgayInvoice.Name = "clcNgayInvoice";
            this.clcNgayInvoice.ReadOnly = false;
            this.clcNgayInvoice.Size = new System.Drawing.Size(118, 21);
            this.clcNgayInvoice.TabIndex = 20;
            this.clcNgayInvoice.TagName = "";
            this.clcNgayInvoice.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayInvoice.WhereCondition = "";
            // 
            // clcNgayNhapKho
            // 
            this.clcNgayNhapKho.Location = new System.Drawing.Point(112, 13);
            this.clcNgayNhapKho.Name = "clcNgayNhapKho";
            this.clcNgayNhapKho.ReadOnly = false;
            this.clcNgayNhapKho.Size = new System.Drawing.Size(126, 21);
            this.clcNgayNhapKho.TabIndex = 20;
            this.clcNgayNhapKho.TagName = "";
            this.clcNgayNhapKho.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayNhapKho.WhereCondition = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(455, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Số tờ khai";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Ngày nhập kho";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(455, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Nhà cung cấp";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(689, 133);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Tổng VAT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(455, 133);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Số kiện";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(689, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Số B/L";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(689, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Ngày Invoice";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(455, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Số Invoice";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(689, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Số hợp đồng";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 133);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Lý do";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(224, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tỷ giá";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Ngoại tệ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Nhập tại kho";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(455, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số phiếu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Khách hàng";
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDong.Icon")));
            this.btnDong.Location = new System.Drawing.Point(850, 557);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(64, 25);
            this.btnDong.TabIndex = 6;
            this.btnDong.Text = "Đóng";
            this.btnDong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnDong.VisualStyleManager = this.vsmMain;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLuu.Icon")));
            this.btnLuu.Location = new System.Drawing.Point(722, 557);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(58, 25);
            this.btnLuu.TabIndex = 7;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnIn
            // 
            this.btnIn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIn.Enabled = false;
            this.btnIn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIn.Image = global::Company.Interface.Properties.Resources.fileprint;
            this.btnIn.Location = new System.Drawing.Point(786, 557);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(58, 25);
            this.btnIn.TabIndex = 7;
            this.btnIn.Text = "In";
            this.btnIn.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnIn.Click += new System.EventHandler(this.btnIn_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtThanhTien);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongChungTu);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongThucNhan);
            this.uiGroupBox1.Controls.Add(this.txtDonGia);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.ctrMaTienTe);
            this.uiGroupBox1.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.btnThem);
            this.uiGroupBox1.Controls.Add(this.txtMoTaHH);
            this.uiGroupBox1.Controls.Add(this.txtMSVT);
            this.uiGroupBox1.Controls.Add(this.txtMa2);
            this.uiGroupBox1.Controls.Add(this.txtSize);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.txtMauSac);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label21);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label22);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.label25);
            this.uiGroupBox1.Controls.Add(this.label26);
            this.uiGroupBox1.Controls.Add(this.label24);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Location = new System.Drawing.Point(15, 171);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(914, 380);
            this.uiGroupBox1.TabIndex = 10;
            this.uiGroupBox1.Text = "Thông tin hàng";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtThanhTien
            // 
            this.txtThanhTien.DecimalDigits = 20;
            this.txtThanhTien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThanhTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhTien.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThanhTien.Location = new System.Drawing.Point(330, 105);
            this.txtThanhTien.MaxLength = 15;
            this.txtThanhTien.Name = "txtThanhTien";
            this.txtThanhTien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThanhTien.Size = new System.Drawing.Size(105, 21);
            this.txtThanhTien.TabIndex = 27;
            this.txtThanhTien.Text = "0";
            this.txtThanhTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThanhTien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThanhTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoLuongChungTu
            // 
            this.txtSoLuongChungTu.DecimalDigits = 20;
            this.txtSoLuongChungTu.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongChungTu.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongChungTu.Location = new System.Drawing.Point(111, 75);
            this.txtSoLuongChungTu.MaxLength = 15;
            this.txtSoLuongChungTu.Name = "txtSoLuongChungTu";
            this.txtSoLuongChungTu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongChungTu.Size = new System.Drawing.Size(93, 21);
            this.txtSoLuongChungTu.TabIndex = 27;
            this.txtSoLuongChungTu.Text = "0";
            this.txtSoLuongChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongChungTu.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoLuongThucNhan
            // 
            this.txtSoLuongThucNhan.DecimalDigits = 20;
            this.txtSoLuongThucNhan.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongThucNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongThucNhan.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongThucNhan.Location = new System.Drawing.Point(330, 75);
            this.txtSoLuongThucNhan.MaxLength = 15;
            this.txtSoLuongThucNhan.Name = "txtSoLuongThucNhan";
            this.txtSoLuongThucNhan.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongThucNhan.Size = new System.Drawing.Size(105, 21);
            this.txtSoLuongThucNhan.TabIndex = 27;
            this.txtSoLuongThucNhan.Text = "0";
            this.txtSoLuongThucNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongThucNhan.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongThucNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtDonGia
            // 
            this.txtDonGia.DecimalDigits = 20;
            this.txtDonGia.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGia.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGia.Location = new System.Drawing.Point(111, 104);
            this.txtDonGia.MaxLength = 15;
            this.txtDonGia.Name = "txtDonGia";
            this.txtDonGia.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGia.Size = new System.Drawing.Size(93, 21);
            this.txtDonGia.TabIndex = 27;
            this.txtDonGia.Text = "0";
            this.txtDonGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDonGia.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaTienTe
            // 
            this.ctrMaTienTe.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTienTe.Appearance.Options.UseBackColor = true;
            this.ctrMaTienTe.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTienTe.Code = "";
            this.ctrMaTienTe.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTienTe.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTienTe.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTienTe.IsOnlyWarning = false;
            this.ctrMaTienTe.IsValidate = true;
            this.ctrMaTienTe.Location = new System.Drawing.Point(769, 73);
            this.ctrMaTienTe.Name = "ctrMaTienTe";
            this.ctrMaTienTe.Name_VN = "";
            this.ctrMaTienTe.SetOnlyWarning = false;
            this.ctrMaTienTe.SetValidate = false;
            this.ctrMaTienTe.ShowColumnCode = true;
            this.ctrMaTienTe.ShowColumnName = false;
            this.ctrMaTienTe.Size = new System.Drawing.Size(56, 21);
            this.ctrMaTienTe.TabIndex = 22;
            this.ctrMaTienTe.TagName = "";
            this.ctrMaTienTe.Where = null;
            this.ctrMaTienTe.WhereCondition = "";
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDVTLuong1.Appearance.Options.UseBackColor = true;
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsOnlyWarning = false;
            this.ctrDVTLuong1.IsValidate = true;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(545, 73);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.SetOnlyWarning = false;
            this.ctrDVTLuong1.SetValidate = false;
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = false;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(65, 21);
            this.ctrDVTLuong1.TabIndex = 22;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.Where = null;
            this.ctrDVTLuong1.WhereCondition = "";
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = global::Company.Interface.Properties.Resources.clear;
            this.btnXoa.Location = new System.Drawing.Point(832, 137);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(55, 25);
            this.btnXoa.TabIndex = 5;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Image = global::Company.Interface.Properties.Resources._83;
            this.btnThem.Location = new System.Drawing.Point(766, 137);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(58, 25);
            this.btnThem.TabIndex = 5;
            this.btnThem.Text = "Ghi";
            this.btnThem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtMoTaHH
            // 
            this.txtMoTaHH.Location = new System.Drawing.Point(111, 47);
            this.txtMoTaHH.Name = "txtMoTaHH";
            this.txtMoTaHH.Size = new System.Drawing.Size(324, 21);
            this.txtMoTaHH.TabIndex = 21;
            this.txtMoTaHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtMSVT
            // 
            this.txtMSVT.Location = new System.Drawing.Point(111, 20);
            this.txtMSVT.Name = "txtMSVT";
            this.txtMSVT.Size = new System.Drawing.Size(93, 21);
            this.txtMSVT.TabIndex = 21;
            this.txtMSVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMSVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtMa2
            // 
            this.txtMa2.Location = new System.Drawing.Point(330, 20);
            this.txtMa2.Name = "txtMa2";
            this.txtMa2.Size = new System.Drawing.Size(105, 21);
            this.txtMa2.TabIndex = 21;
            this.txtMa2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSize
            // 
            this.txtSize.Location = new System.Drawing.Point(769, 46);
            this.txtSize.Name = "txtSize";
            this.txtSize.Size = new System.Drawing.Size(118, 21);
            this.txtSize.TabIndex = 21;
            this.txtSize.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSize.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(545, 102);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(342, 21);
            this.txtGhiChu.TabIndex = 21;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtMauSac
            // 
            this.txtMauSac.Location = new System.Drawing.Point(545, 45);
            this.txtMauSac.Name = "txtMauSac";
            this.txtMauSac.Size = new System.Drawing.Size(116, 21);
            this.txtMauSac.TabIndex = 21;
            this.txtMauSac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMauSac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(454, 80);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "ĐVT số lượng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(688, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Size";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(264, 110);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "Thành tiền";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(5, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Đơn giá";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(454, 107);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "Ghi chú";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(223, 80);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(101, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Số lượng thực nhận";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(5, 80);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "Số lượng chứng từ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(688, 78);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Mã tiền tệ ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(5, 52);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(58, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Tên vật tư";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(454, 52);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "Màu";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(292, 25);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(30, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Mã 2";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(5, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã số vật tư";
            // 
            // PhieuNhapKhoForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(941, 594);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PhieuNhapKhoForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Phiếu nhập kho";
            this.Load += new System.EventHandler(this.PhieuNhapKhoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private UIGroupBox uiGroupBox4;
        private UIButton btnDong;
        private Label label3;
        private Label label6;
        private Label label1;
        private Label label2;
        private Label label9;
        private Label label16;
        private Label label10;
        private Label label15;
        private Label label14;
        private Label label13;
        private Label label11;
        private Label label12;
        private Label label8;
        private Label label7;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayNhapKho;
        private EditBox txtLyDo;
        private EditBox txtNhapTaiKho;
        private EditBox txtSoBL;
        private EditBox txtSoInvoice;
        private EditBox txtSoHopDong;
        private EditBox txtNhaCungCap;
        private EditBox txtKhachHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayInvoice;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrNgoaiTe;
        public UIButton btnLuu;
        public UIButton btnIn;
        private UIGroupBox uiGroupBox1;
        private NumericEditBox txtThanhTien;
        private NumericEditBox txtSoLuongChungTu;
        private NumericEditBox txtSoLuongThucNhan;
        private NumericEditBox txtDonGia;
        private GridEX dgList;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        public UIButton btnXoa;
        public UIButton btnThem;
        private EditBox txtMoTaHH;
        private EditBox txtSize;
        private EditBox txtGhiChu;
        private EditBox txtMauSac;
        private Label label4;
        private Label label21;
        private Label label5;
        private Label label22;
        private Label label20;
        private Label label17;
        private Label label18;
        private Label label25;
        private Label label26;
        private Label label27;
        private Label label23;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTienTe;
        private NumericEditBox txtTyGia;
        private NumericEditBox txtSoPhieu;
        private NumericEditBox txtSoToKhai;
        private NumericEditBox txtSoKien;
        private EditBox txtMa2;
        private Label label24;
        private NumericEditBox txtTongVAT;
        private Label label28;
        private EditBox txtMSVT;
    }
}