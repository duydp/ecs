﻿using System;
using System.Drawing;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface.KDT.GC
{
    public partial class HangChuyenTiepEditForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public Company.GC.BLL.KDT.GC.HangChuyenTiep HCT = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        public bool IsEdited = false;
        public bool IsDeleted = false;
        public string MaLoaiHinh = string.Empty;
        private bool IsExit = true;
        public string MaNguyenTe;

        //-----------------------------------------------------------------------------------------

        public HangChuyenTiepEditForm()
        {
            InitializeComponent();
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            switch (this.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "SP":
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                    break;
                case "TB":
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
            }
        }

        private bool checkMaHangExit(string maHang)
        {
            foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep hct in this.TKCT.HCTCollection)
            {
                if (hct.MaHang.Trim() == maHang) return true;
            }
            return false;
        }
        private void KiemTraTonTaiHang()
        {
            switch (this.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang,setText( "Không tồn tại nguyên phụ liệu này.","This value is not exist" ));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "SP":
                    if (this.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text;
                        if (npl1.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang,setText( "Không tồn tại thiết bị này.","This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }
        }
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("X"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(TKCT.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai chuyển tiếp có id = " + TKCT.ID + " này đã được phân bổ nên không thể xóa được.", false);
                        return;
                    }
                }
            }
            KiemTraTonTaiHang();
            txtTGNT.Value = (decimal)txtLuong.Value * (decimal)txtDGNT.Value;
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (!IsExit) return;
            this.TKCT.HCTCollection.Remove(HCT);
            if (checkMaHangExit(txtMaHang.Text.Trim()))
            {
                showMsg("MSG_2702006");
                //ShowMessage("Mặt hàng này đã có rồi, hãy chọn mặt hàng khác", false);
                if (HCT.MaHang.Trim().Length > 0)
                    this.TKCT.HCTCollection.Add(HCT);
                return;
            }
            this.HCT.MaHS = txtMaHS.Text;
            this.HCT.MaHang = txtMaHang.Text;
            this.HCT.TenHang = txtTenHang.Text;
            this.HCT.ID_NuocXX = ctrNuocXX.Ma;
            this.HCT.ID_DVT = cbDonViTinh.SelectedValue.ToString();
            this.HCT.SoLuong = Convert.ToDecimal(txtLuong.Text);
            this.HCT.DonGia = Convert.ToDecimal(txtDGNT.Text);
            this.HCT.TriGia = Convert.ToDecimal(txtTGNT.Text);
            this.TKCT.HCTCollection.Add(HCT);
            this.IsEdited = true;
            this.Close();
        }


        private void HangChuyenTiepEditForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            lblMaNT1.Text = lblMaNT2.Text = "(" + this.MaNguyenTe + ")";
            txtMaHang.Text = this.HCT.MaHang;
            txtTenHang.Text = this.HCT.TenHang;
            txtMaHS.Text = this.HCT.MaHS;
            cbDonViTinh.SelectedValue = this.HCT.ID_DVT;
            ctrNuocXX.Ma = this.HCT.ID_NuocXX;
            txtLuong.Value = this.HCT.SoLuong;
            txtDGNT.Value = this.HCT.DonGia;
            txtTGNT.Value = this.HCT.TriGia;
            if (!MaLoaiHinh.EndsWith("N"))
            {
                lblLuongCon.Text =setText( "Lượng còn được giao","Quantity can be exported");
            }
            txtMaHang_Leave(null, null);
            if (this.TKCT.TrangThaiXuLy != -1)
            {
                btnAddNew.Enabled = false;
            }
        }



        //-----------------------------------------------------------------------------------------------

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {

            switch (this.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":

                    Company.Interface.GC.NguyenPhuLieuRegistedForm fNPL = new Company.Interface.GC.NguyenPhuLieuRegistedForm();
                    fNPL.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                    fNPL.isBrower = true;
                    fNPL.ShowDialog();
                    if (fNPL.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = fNPL.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = fNPL.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = fNPL.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = fNPL.NguyenPhuLieuSelected.DVT_ID;
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = fNPL.NguyenPhuLieuSelected.SoLuongDangKy - fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == fNPL.NguyenPhuLieuSelected.Ma.ToUpper().Trim())
                                SoLuongConDuocNhap += HCT.SoLuong;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongDaDung;// +fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == fNPL.NguyenPhuLieuSelected.Ma.ToUpper().Trim())
                                SoLuongConDuocGiao += HCT.SoLuong;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }
                        txtLuongCon.Focus();
                    }
                    break;
                case "SP":
                    if (this.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.Interface.GC.NguyenPhuLieuRegistedForm fNPL1 = new Company.Interface.GC.NguyenPhuLieuRegistedForm();
                        fNPL1.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                        fNPL1.isBrower = true;
                        fNPL1.ShowDialog();
                        if (fNPL1.NguyenPhuLieuSelected.Ma != "")
                        {
                            txtMaHang.Text = fNPL1.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = fNPL1.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = fNPL1.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = fNPL1.NguyenPhuLieuSelected.DVT_ID;
                            if (this.MaLoaiHinh.EndsWith("N"))
                            {
                                decimal SoLuongConDuocNhap = fNPL1.NguyenPhuLieuSelected.SoLuongDangKy - fNPL1.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL1.NguyenPhuLieuSelected.SoLuongCungUng;
                                if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == fNPL1.NguyenPhuLieuSelected.Ma.ToUpper().Trim())
                                    SoLuongConDuocNhap += HCT.SoLuong;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }
                            else
                            {
                                decimal SoLuongConDuocGiao = fNPL1.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL1.NguyenPhuLieuSelected.SoLuongDaDung;// +fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                                if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == fNPL1.NguyenPhuLieuSelected.Ma.ToUpper().Trim())
                                    SoLuongConDuocGiao += HCT.SoLuong;
                                txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                            }
                            txtLuongCon.Focus();
                        }
                    }
                    else
                    {
                        Company.Interface.GC.SanPhamRegistedForm fsp = new Company.Interface.GC.SanPhamRegistedForm();
                        fsp.SanPhamSelected.HopDong_ID = HD.ID;
                        fsp.isBrower = true;
                        fsp.ShowDialog();
                        if (fsp.SanPhamSelected.Ma != "")
                        {
                            txtMaHang.Text = fsp.SanPhamSelected.Ma;
                            txtTenHang.Text = fsp.SanPhamSelected.Ten;
                            txtMaHS.Text = fsp.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = fsp.SanPhamSelected.DVT_ID;
                            if (this.MaLoaiHinh.EndsWith("X"))
                            {
                                decimal SoLuongConDuocNhap = fsp.SanPhamSelected.SoLuongDangKy - fsp.SanPhamSelected.SoLuongDaXuat;
                                if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == fsp.SanPhamSelected.Ma.ToUpper().Trim())
                                    SoLuongConDuocNhap += HCT.SoLuong;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }
                            txtLuongCon.Focus();
                        }
                    }
                    break;
                case "TB":
                    Company.Interface.GC.ThietBiRegistedForm ftb = new Company.Interface.GC.ThietBiRegistedForm();
                    ftb.ThietBiSelected.HopDong_ID = HD.ID;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (ftb.ThietBiSelected.Ma != "")
                    {
                        txtMaHang.Text = ftb.ThietBiSelected.Ma;
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                        cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = ftb.ThietBiSelected.SoLuongDangKy - ftb.ThietBiSelected.SoLuongDaNhap;
                            if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == ftb.ThietBiSelected.Ma.ToUpper().Trim())
                                SoLuongConDuocNhap += HCT.SoLuong;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = ftb.ThietBiSelected.SoLuongDaNhap;
                            if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == ftb.ThietBiSelected.Ma.ToUpper().Trim())
                                SoLuongConDuocGiao += HCT.SoLuong;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuongCon.Focus();
                    }
                    break;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.IsDeleted = true;
            this.Close();
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            txtTGNT.Value = (decimal)txtLuong.Value * (decimal)txtDGNT.Value;
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            txtTGNT.Value = (decimal)txtLuong.Value * (decimal)txtDGNT.Value;
        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            switch (this.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    NguyenPhuLieu npl = new NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = npl.Ma;
                        txtTenHang.Text = npl.Ten;
                        txtMaHS.Text = npl.MaHS;
                        cbDonViTinh.SelectedValue = npl.DVT_ID;
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                            if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == npl.Ma.ToUpper().Trim())
                                SoLuongConDuocNhap += HCT.SoLuong;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = npl.SoLuongDaNhap - npl.SoLuongDaDung + npl.SoLuongCungUng;
                            if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == npl.Ma.ToUpper().Trim())
                                SoLuongConDuocGiao += HCT.SoLuong;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }
                        txtLuong.Focus();
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang,setText( "Không tồn tại nguyên phụ liệu này.","This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "SP":
                    if (this.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text.Trim();
                        if (npl1.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = npl1.Ma;
                            txtTenHang.Text = npl1.Ten;
                            txtMaHS.Text = npl1.MaHS;
                            cbDonViTinh.SelectedValue = npl1.DVT_ID;
                            if (this.MaLoaiHinh.EndsWith("N"))
                            {
                                decimal SoLuongConDuocNhap = npl1.SoLuongDangKy - npl1.SoLuongDaNhap - npl1.SoLuongCungUng;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }

                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại mã hàng này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        SanPham sp = new SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = sp.Ma;
                            txtTenHang.Text = sp.Ten;
                            txtMaHS.Text = sp.MaHS;
                            cbDonViTinh.SelectedValue = sp.DVT_ID;
                            if (this.MaLoaiHinh.EndsWith("X"))
                            {
                                decimal SoLuongConDuocNhap = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                                if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == sp.Ma.ToUpper().Trim())
                                    SoLuongConDuocNhap += HCT.SoLuong;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }
                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "TB":
                    ThietBi tb = new ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = tb.Ma;
                        txtTenHang.Text = tb.Ten;
                        txtMaHS.Text = tb.MaHS;
                        cbDonViTinh.SelectedValue = tb.DVT_ID;
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                            if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == tb.Ma.ToUpper().Trim())
                                SoLuongConDuocNhap += HCT.SoLuong;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = tb.SoLuongDaNhap;
                            if (HCT.ID > 0 && HCT.MaHang.Trim().ToUpper() == tb.Ma.ToUpper().Trim())
                                SoLuongConDuocGiao += HCT.SoLuong;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuong.Focus();
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang,setText( "Không tồn tại thiết bị này.","This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }
        }

    }
}