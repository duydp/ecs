using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.SXXK.ToKhai
{
    public partial class HangMauDichDieuChinh : EntityBase
    {
        #region Private members.

        protected int _SoToKhai;
        protected string _MaLoaiHinh = String.Empty;
        protected string _MaHaiQuan = String.Empty;
        protected short _NamDangKy;
        protected string _MaHang = String.Empty;
        protected int _LanDieuChinh;
        protected string _MA_NPL_SP = String.Empty;
        protected short _STTHang;
        protected string _MaHSKB = String.Empty;
        protected string _MaHS = String.Empty;
        protected string _TenHang = String.Empty;
        protected int _DinhMuc;
        protected string _NuocXX_ID = String.Empty;
        protected string _DVT_ID = String.Empty;
        protected decimal _Luong;
        protected double _DGIA_KB;
        protected double _DGIA_TT;
        protected string _MA_DG = String.Empty;
        protected double _TRIGIA_KB;
        protected double _TRIGIA_TT;
        protected double _TGKB_VND;
        protected byte _LOAITSXNK;
        protected decimal _TS_XNK;
        protected decimal _TS_TTDB;
        protected decimal _TS_VAT;
        protected double _THUE_XNK;
        protected double _THUE_TTDB;
        protected double _THUE_VAT;
        protected double _PHU_THU;
        protected byte _MIENTHUE;
        protected decimal _TL_QUYDOI;
        protected string _MA_THKE = String.Empty;
        protected byte _CHOXULY;
        protected decimal _TYLE_THUKHAC;
        protected double _TRIGIA_THUKHAC;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value.Trim(); }
            get { return this._MaHaiQuan.Trim(); }
        }
        public short NamDangKy
        {
            set { this._NamDangKy = value; }
            get { return this._NamDangKy; }
        }
        public string MaHang
        {
            set { this._MaHang = value; }
            get { return this._MaHang; }
        }
        public int LanDieuChinh
        {
            set { this._LanDieuChinh = value; }
            get { return this._LanDieuChinh; }
        }
        public string MA_NPL_SP
        {
            set { this._MA_NPL_SP = value; }
            get { return this._MA_NPL_SP; }
        }
        public short STTHang
        {
            set { this._STTHang = value; }
            get { return this._STTHang; }
        }
        public string MaHSKB
        {
            set { this._MaHSKB = value; }
            get { return this._MaHSKB; }
        }
        public string MaHS
        {
            set { this._MaHS = value; }
            get { return this._MaHS; }
        }
        public string TenHang
        {
            set { this._TenHang = value; }
            get { return this._TenHang; }
        }
        public int DinhMuc
        {
            set { this._DinhMuc = value; }
            get { return this._DinhMuc; }
        }
        public string NuocXX_ID
        {
            set { this._NuocXX_ID = value; }
            get { return this._NuocXX_ID; }
        }
        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }
        public decimal Luong
        {
            set { this._Luong = value; }
            get { return this._Luong; }
        }
        public double DGIA_KB
        {
            set { this._DGIA_KB = value; }
            get { return this._DGIA_KB; }
        }
        public double DGIA_TT
        {
            set { this._DGIA_TT = value; }
            get { return this._DGIA_TT; }
        }
        public string MA_DG
        {
            set { this._MA_DG = value; }
            get { return this._MA_DG; }
        }
        public double TRIGIA_KB
        {
            set { this._TRIGIA_KB = value; }
            get { return this._TRIGIA_KB; }
        }
        public double TRIGIA_TT
        {
            set { this._TRIGIA_TT = value; }
            get { return this._TRIGIA_TT; }
        }
        public double TGKB_VND
        {
            set { this._TGKB_VND = value; }
            get { return this._TGKB_VND; }
        }
        public byte LOAITSXNK
        {
            set { this._LOAITSXNK = value; }
            get { return this._LOAITSXNK; }
        }
        public decimal TS_XNK
        {
            set { this._TS_XNK = value; }
            get { return this._TS_XNK; }
        }
        public decimal TS_TTDB
        {
            set { this._TS_TTDB = value; }
            get { return this._TS_TTDB; }
        }
        public decimal TS_VAT
        {
            set { this._TS_VAT = value; }
            get { return this._TS_VAT; }
        }
        public double THUE_XNK
        {
            set { this._THUE_XNK = value; }
            get { return this._THUE_XNK; }
        }
        public double THUE_TTDB
        {
            set { this._THUE_TTDB = value; }
            get { return this._THUE_TTDB; }
        }
        public double THUE_VAT
        {
            set { this._THUE_VAT = value; }
            get { return this._THUE_VAT; }
        }
        public double PHU_THU
        {
            set { this._PHU_THU = value; }
            get { return this._PHU_THU; }
        }
        public byte MIENTHUE
        {
            set { this._MIENTHUE = value; }
            get { return this._MIENTHUE; }
        }
        public decimal TL_QUYDOI
        {
            set { this._TL_QUYDOI = value; }
            get { return this._TL_QUYDOI; }
        }
        public string MA_THKE
        {
            set { this._MA_THKE = value; }
            get { return this._MA_THKE; }
        }
        public byte CHOXULY
        {
            set { this._CHOXULY = value; }
            get { return this._CHOXULY; }
        }
        public decimal TYLE_THUKHAC
        {
            set { this._TYLE_THUKHAC = value; }
            get { return this._TYLE_THUKHAC; }
        }
        public double TRIGIA_THUKHAC
        {
            set { this._TRIGIA_THUKHAC = value; }
            get { return this._TRIGIA_THUKHAC; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.SmallInt, this._STTHang);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) this._MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) this._LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_NPL_SP"))) this._MA_NPL_SP = reader.GetString(reader.GetOrdinal("MA_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt16(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSKB"))) this._MaHSKB = reader.GetString(reader.GetOrdinal("MaHSKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) this._DinhMuc = reader.GetInt32(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) this._Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_KB"))) this._DGIA_KB = reader.GetDouble(reader.GetOrdinal("DGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_TT"))) this._DGIA_TT = reader.GetDouble(reader.GetOrdinal("DGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_DG"))) this._MA_DG = reader.GetString(reader.GetOrdinal("MA_DG"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_KB"))) this._TRIGIA_KB = reader.GetDouble(reader.GetOrdinal("TRIGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_TT"))) this._TRIGIA_TT = reader.GetDouble(reader.GetOrdinal("TRIGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TGKB_VND"))) this._TGKB_VND = reader.GetDouble(reader.GetOrdinal("TGKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("LOAITSXNK"))) this._LOAITSXNK = reader.GetByte(reader.GetOrdinal("LOAITSXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_XNK"))) this._TS_XNK = reader.GetDecimal(reader.GetOrdinal("TS_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_TTDB"))) this._TS_TTDB = reader.GetDecimal(reader.GetOrdinal("TS_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_VAT"))) this._TS_VAT = reader.GetDecimal(reader.GetOrdinal("TS_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) this._THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) this._THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) this._THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) this._PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
                if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) this._MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
                if (!reader.IsDBNull(reader.GetOrdinal("TL_QUYDOI"))) this._TL_QUYDOI = reader.GetDecimal(reader.GetOrdinal("TL_QUYDOI"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_THKE"))) this._MA_THKE = reader.GetString(reader.GetOrdinal("MA_THKE"));
                if (!reader.IsDBNull(reader.GetOrdinal("CHOXULY"))) this._CHOXULY = reader.GetByte(reader.GetOrdinal("CHOXULY"));
                if (!reader.IsDBNull(reader.GetOrdinal("TYLE_THUKHAC"))) this._TYLE_THUKHAC = reader.GetDecimal(reader.GetOrdinal("TYLE_THUKHAC"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) this._TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
                return true;
            }
            return false;
        }

        //---------------------------------------------------------------------------------------------
        public HangMauDichDieuChinhCollection SelectCollectionBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_LanDieuChinh()
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_SelectBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_LanDieuChinh";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);

            HangMauDichDieuChinhCollection collection = new HangMauDichDieuChinhCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                HangMauDichDieuChinh entity = new HangMauDichDieuChinh();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_NPL_SP"))) entity.MA_NPL_SP = reader.GetString(reader.GetOrdinal("MA_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt16(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSKB"))) entity.MaHSKB = reader.GetString(reader.GetOrdinal("MaHSKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetInt32(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_KB"))) entity.DGIA_KB = reader.GetDouble(reader.GetOrdinal("DGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_TT"))) entity.DGIA_TT = reader.GetDouble(reader.GetOrdinal("DGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_DG"))) entity.MA_DG = reader.GetString(reader.GetOrdinal("MA_DG"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_KB"))) entity.TRIGIA_KB = reader.GetDouble(reader.GetOrdinal("TRIGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_TT"))) entity.TRIGIA_TT = reader.GetDouble(reader.GetOrdinal("TRIGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TGKB_VND"))) entity.TGKB_VND = reader.GetDouble(reader.GetOrdinal("TGKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("LOAITSXNK"))) entity.LOAITSXNK = reader.GetByte(reader.GetOrdinal("LOAITSXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_XNK"))) entity.TS_XNK = reader.GetDecimal(reader.GetOrdinal("TS_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_TTDB"))) entity.TS_TTDB = reader.GetDecimal(reader.GetOrdinal("TS_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_VAT"))) entity.TS_VAT = reader.GetDecimal(reader.GetOrdinal("TS_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) entity.PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
                if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) entity.MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
                if (!reader.IsDBNull(reader.GetOrdinal("TL_QUYDOI"))) entity.TL_QUYDOI = reader.GetDecimal(reader.GetOrdinal("TL_QUYDOI"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_THKE"))) entity.MA_THKE = reader.GetString(reader.GetOrdinal("MA_THKE"));
                if (!reader.IsDBNull(reader.GetOrdinal("CHOXULY"))) entity.CHOXULY = reader.GetByte(reader.GetOrdinal("CHOXULY"));
                if (!reader.IsDBNull(reader.GetOrdinal("TYLE_THUKHAC"))) entity.TYLE_THUKHAC = reader.GetDecimal(reader.GetOrdinal("TYLE_THUKHAC"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) entity.TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_LanDieuChinh()
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_SelectBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_LanDieuChinh";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public HangMauDichDieuChinhCollection SelectCollectionAll()
        {
            HangMauDichDieuChinhCollection collection = new HangMauDichDieuChinhCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                HangMauDichDieuChinh entity = new HangMauDichDieuChinh();

                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_NPL_SP"))) entity.MA_NPL_SP = reader.GetString(reader.GetOrdinal("MA_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt16(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSKB"))) entity.MaHSKB = reader.GetString(reader.GetOrdinal("MaHSKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetInt32(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_KB"))) entity.DGIA_KB = reader.GetDouble(reader.GetOrdinal("DGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_TT"))) entity.DGIA_TT = reader.GetDouble(reader.GetOrdinal("DGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_DG"))) entity.MA_DG = reader.GetString(reader.GetOrdinal("MA_DG"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_KB"))) entity.TRIGIA_KB = reader.GetDouble(reader.GetOrdinal("TRIGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_TT"))) entity.TRIGIA_TT = reader.GetDouble(reader.GetOrdinal("TRIGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TGKB_VND"))) entity.TGKB_VND = reader.GetDouble(reader.GetOrdinal("TGKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("LOAITSXNK"))) entity.LOAITSXNK = reader.GetByte(reader.GetOrdinal("LOAITSXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_XNK"))) entity.TS_XNK = reader.GetDecimal(reader.GetOrdinal("TS_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_TTDB"))) entity.TS_TTDB = reader.GetDecimal(reader.GetOrdinal("TS_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_VAT"))) entity.TS_VAT = reader.GetDecimal(reader.GetOrdinal("TS_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) entity.PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
                if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) entity.MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
                if (!reader.IsDBNull(reader.GetOrdinal("TL_QUYDOI"))) entity.TL_QUYDOI = reader.GetDecimal(reader.GetOrdinal("TL_QUYDOI"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_THKE"))) entity.MA_THKE = reader.GetString(reader.GetOrdinal("MA_THKE"));
                if (!reader.IsDBNull(reader.GetOrdinal("CHOXULY"))) entity.CHOXULY = reader.GetByte(reader.GetOrdinal("CHOXULY"));
                if (!reader.IsDBNull(reader.GetOrdinal("TYLE_THUKHAC"))) entity.TYLE_THUKHAC = reader.GetDecimal(reader.GetOrdinal("TYLE_THUKHAC"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) entity.TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public HangMauDichDieuChinhCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            HangMauDichDieuChinhCollection collection = new HangMauDichDieuChinhCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                HangMauDichDieuChinh entity = new HangMauDichDieuChinh();

                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_NPL_SP"))) entity.MA_NPL_SP = reader.GetString(reader.GetOrdinal("MA_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt16(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSKB"))) entity.MaHSKB = reader.GetString(reader.GetOrdinal("MaHSKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetInt32(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_KB"))) entity.DGIA_KB = reader.GetDouble(reader.GetOrdinal("DGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_TT"))) entity.DGIA_TT = reader.GetDouble(reader.GetOrdinal("DGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_DG"))) entity.MA_DG = reader.GetString(reader.GetOrdinal("MA_DG"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_KB"))) entity.TRIGIA_KB = reader.GetDouble(reader.GetOrdinal("TRIGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_TT"))) entity.TRIGIA_TT = reader.GetDouble(reader.GetOrdinal("TRIGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TGKB_VND"))) entity.TGKB_VND = reader.GetDouble(reader.GetOrdinal("TGKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("LOAITSXNK"))) entity.LOAITSXNK = reader.GetByte(reader.GetOrdinal("LOAITSXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_XNK"))) entity.TS_XNK = reader.GetDecimal(reader.GetOrdinal("TS_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_TTDB"))) entity.TS_TTDB = reader.GetDecimal(reader.GetOrdinal("TS_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_VAT"))) entity.TS_VAT = reader.GetDecimal(reader.GetOrdinal("TS_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) entity.PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
                if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) entity.MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
                if (!reader.IsDBNull(reader.GetOrdinal("TL_QUYDOI"))) entity.TL_QUYDOI = reader.GetDecimal(reader.GetOrdinal("TL_QUYDOI"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_THKE"))) entity.MA_THKE = reader.GetString(reader.GetOrdinal("MA_THKE"));
                if (!reader.IsDBNull(reader.GetOrdinal("CHOXULY"))) entity.CHOXULY = reader.GetByte(reader.GetOrdinal("CHOXULY"));
                if (!reader.IsDBNull(reader.GetOrdinal("TYLE_THUKHAC"))) entity.TYLE_THUKHAC = reader.GetDecimal(reader.GetOrdinal("TYLE_THUKHAC"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) entity.TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
                collection.Add(entity);
            }
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
            this.db.AddInParameter(dbCommand, "@MA_NPL_SP", SqlDbType.VarChar, this._MA_NPL_SP);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.SmallInt, this._STTHang);
            this.db.AddInParameter(dbCommand, "@MaHSKB", SqlDbType.VarChar, this._MaHSKB);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Int, this._DinhMuc);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@DGIA_KB", SqlDbType.Float, this._DGIA_KB);
            this.db.AddInParameter(dbCommand, "@DGIA_TT", SqlDbType.Float, this._DGIA_TT);
            this.db.AddInParameter(dbCommand, "@MA_DG", SqlDbType.Char, this._MA_DG);
            this.db.AddInParameter(dbCommand, "@TRIGIA_KB", SqlDbType.Float, this._TRIGIA_KB);
            this.db.AddInParameter(dbCommand, "@TRIGIA_TT", SqlDbType.Float, this._TRIGIA_TT);
            this.db.AddInParameter(dbCommand, "@TGKB_VND", SqlDbType.Float, this._TGKB_VND);
            this.db.AddInParameter(dbCommand, "@LOAITSXNK", SqlDbType.TinyInt, this._LOAITSXNK);
            this.db.AddInParameter(dbCommand, "@TS_XNK", SqlDbType.Decimal, this._TS_XNK);
            this.db.AddInParameter(dbCommand, "@TS_TTDB", SqlDbType.Decimal, this._TS_TTDB);
            this.db.AddInParameter(dbCommand, "@TS_VAT", SqlDbType.Decimal, this._TS_VAT);
            this.db.AddInParameter(dbCommand, "@THUE_XNK", SqlDbType.Float, this._THUE_XNK);
            this.db.AddInParameter(dbCommand, "@THUE_TTDB", SqlDbType.Float, this._THUE_TTDB);
            this.db.AddInParameter(dbCommand, "@THUE_VAT", SqlDbType.Float, this._THUE_VAT);
            this.db.AddInParameter(dbCommand, "@PHU_THU", SqlDbType.Float, this._PHU_THU);
            this.db.AddInParameter(dbCommand, "@MIENTHUE", SqlDbType.TinyInt, this._MIENTHUE);
            this.db.AddInParameter(dbCommand, "@TL_QUYDOI", SqlDbType.Decimal, this._TL_QUYDOI);
            this.db.AddInParameter(dbCommand, "@MA_THKE", SqlDbType.VarChar, this._MA_THKE);
            this.db.AddInParameter(dbCommand, "@CHOXULY", SqlDbType.TinyInt, this._CHOXULY);
            this.db.AddInParameter(dbCommand, "@TYLE_THUKHAC", SqlDbType.Decimal, this._TYLE_THUKHAC);
            this.db.AddInParameter(dbCommand, "@TRIGIA_THUKHAC", SqlDbType.Float, this._TRIGIA_THUKHAC);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(HangMauDichDieuChinhCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangMauDichDieuChinh item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, HangMauDichDieuChinhCollection collection)
        {
            foreach (HangMauDichDieuChinh item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
            this.db.AddInParameter(dbCommand, "@MA_NPL_SP", SqlDbType.VarChar, this._MA_NPL_SP);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.SmallInt, this._STTHang);
            this.db.AddInParameter(dbCommand, "@MaHSKB", SqlDbType.VarChar, this._MaHSKB);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Int, this._DinhMuc);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@DGIA_KB", SqlDbType.Float, this._DGIA_KB);
            this.db.AddInParameter(dbCommand, "@DGIA_TT", SqlDbType.Float, this._DGIA_TT);
            this.db.AddInParameter(dbCommand, "@MA_DG", SqlDbType.Char, this._MA_DG);
            this.db.AddInParameter(dbCommand, "@TRIGIA_KB", SqlDbType.Float, this._TRIGIA_KB);
            this.db.AddInParameter(dbCommand, "@TRIGIA_TT", SqlDbType.Float, this._TRIGIA_TT);
            this.db.AddInParameter(dbCommand, "@TGKB_VND", SqlDbType.Float, this._TGKB_VND);
            this.db.AddInParameter(dbCommand, "@LOAITSXNK", SqlDbType.TinyInt, this._LOAITSXNK);
            this.db.AddInParameter(dbCommand, "@TS_XNK", SqlDbType.Decimal, this._TS_XNK);
            this.db.AddInParameter(dbCommand, "@TS_TTDB", SqlDbType.Decimal, this._TS_TTDB);
            this.db.AddInParameter(dbCommand, "@TS_VAT", SqlDbType.Decimal, this._TS_VAT);
            this.db.AddInParameter(dbCommand, "@THUE_XNK", SqlDbType.Float, this._THUE_XNK);
            this.db.AddInParameter(dbCommand, "@THUE_TTDB", SqlDbType.Float, this._THUE_TTDB);
            this.db.AddInParameter(dbCommand, "@THUE_VAT", SqlDbType.Float, this._THUE_VAT);
            this.db.AddInParameter(dbCommand, "@PHU_THU", SqlDbType.Float, this._PHU_THU);
            this.db.AddInParameter(dbCommand, "@MIENTHUE", SqlDbType.TinyInt, this._MIENTHUE);
            this.db.AddInParameter(dbCommand, "@TL_QUYDOI", SqlDbType.Decimal, this._TL_QUYDOI);
            this.db.AddInParameter(dbCommand, "@MA_THKE", SqlDbType.VarChar, this._MA_THKE);
            this.db.AddInParameter(dbCommand, "@CHOXULY", SqlDbType.TinyInt, this._CHOXULY);
            this.db.AddInParameter(dbCommand, "@TYLE_THUKHAC", SqlDbType.Decimal, this._TYLE_THUKHAC);
            this.db.AddInParameter(dbCommand, "@TRIGIA_THUKHAC", SqlDbType.Float, this._TRIGIA_THUKHAC);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(HangMauDichDieuChinhCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangMauDichDieuChinh item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
            this.db.AddInParameter(dbCommand, "@MA_NPL_SP", SqlDbType.VarChar, this._MA_NPL_SP);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.SmallInt, this._STTHang);
            this.db.AddInParameter(dbCommand, "@MaHSKB", SqlDbType.VarChar, this._MaHSKB);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Int, this._DinhMuc);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@DGIA_KB", SqlDbType.Float, this._DGIA_KB);
            this.db.AddInParameter(dbCommand, "@DGIA_TT", SqlDbType.Float, this._DGIA_TT);
            this.db.AddInParameter(dbCommand, "@MA_DG", SqlDbType.Char, this._MA_DG);
            this.db.AddInParameter(dbCommand, "@TRIGIA_KB", SqlDbType.Float, this._TRIGIA_KB);
            this.db.AddInParameter(dbCommand, "@TRIGIA_TT", SqlDbType.Float, this._TRIGIA_TT);
            this.db.AddInParameter(dbCommand, "@TGKB_VND", SqlDbType.Float, this._TGKB_VND);
            this.db.AddInParameter(dbCommand, "@LOAITSXNK", SqlDbType.TinyInt, this._LOAITSXNK);
            this.db.AddInParameter(dbCommand, "@TS_XNK", SqlDbType.Decimal, this._TS_XNK);
            this.db.AddInParameter(dbCommand, "@TS_TTDB", SqlDbType.Decimal, this._TS_TTDB);
            this.db.AddInParameter(dbCommand, "@TS_VAT", SqlDbType.Decimal, this._TS_VAT);
            this.db.AddInParameter(dbCommand, "@THUE_XNK", SqlDbType.Float, this._THUE_XNK);
            this.db.AddInParameter(dbCommand, "@THUE_TTDB", SqlDbType.Float, this._THUE_TTDB);
            this.db.AddInParameter(dbCommand, "@THUE_VAT", SqlDbType.Float, this._THUE_VAT);
            this.db.AddInParameter(dbCommand, "@PHU_THU", SqlDbType.Float, this._PHU_THU);
            this.db.AddInParameter(dbCommand, "@MIENTHUE", SqlDbType.TinyInt, this._MIENTHUE);
            this.db.AddInParameter(dbCommand, "@TL_QUYDOI", SqlDbType.Decimal, this._TL_QUYDOI);
            this.db.AddInParameter(dbCommand, "@MA_THKE", SqlDbType.VarChar, this._MA_THKE);
            this.db.AddInParameter(dbCommand, "@CHOXULY", SqlDbType.TinyInt, this._CHOXULY);
            this.db.AddInParameter(dbCommand, "@TYLE_THUKHAC", SqlDbType.Decimal, this._TYLE_THUKHAC);
            this.db.AddInParameter(dbCommand, "@TRIGIA_THUKHAC", SqlDbType.Float, this._TRIGIA_THUKHAC);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(HangMauDichDieuChinhCollection collection, SqlTransaction transaction)
        {
            foreach (HangMauDichDieuChinh item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_HangMauDichDieuChinh_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.SmallInt, this._STTHang);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(HangMauDichDieuChinhCollection collection, SqlTransaction transaction)
        {
            foreach (HangMauDichDieuChinh item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(HangMauDichDieuChinhCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangMauDichDieuChinh item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion

        public static void updateDatabase(DataSet ds, SqlTransaction trangsaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HangMauDichDieuChinh hmd = new HangMauDichDieuChinh();
                if (row["CHOXULY"].ToString() != "")
                    hmd.CHOXULY = Convert.ToByte(row["CHOXULY"].ToString());
                if (row["DGIA_KB"].ToString() != "")
                    hmd.DGIA_KB = Convert.ToDouble(row["DGIA_KB"]);
                if (row["DGIA_TT"].ToString() != "")
                    hmd.DGIA_TT = Convert.ToDouble(row["DGIA_TT"]);
                hmd.DinhMuc = Convert.ToInt32(row["DINHMUC"]);
                hmd.DVT_ID = (row["MA_DVT"].ToString());
                hmd.LanDieuChinh = Convert.ToInt32(row["LAN_DC"].ToString());
                if (row["LOAITSXNK"].ToString() != "")
                    hmd.LOAITSXNK = Convert.ToByte(row["LOAITSXNK"].ToString());
                hmd.Luong = Convert.ToDecimal(row["LUONG"]);
                hmd.MA_DG = (row["MA_DG"].ToString());
                hmd.MA_NPL_SP = (row["MA_NPL_SP"].ToString());
                if (row["MA_THKE"].ToString() != "")
                    hmd.MA_THKE = (row["MA_THKE"].ToString());
                hmd.MaHaiQuan = (row["MA_HQ"].ToString());
                hmd.MaHang = (row["MA_PHU"].ToString());
                hmd.MaHS = (row["MA_HANG"].ToString());
                //if (hmd.MaHS.Length < 10)
                //{
                //    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                //        hmd.MaHS += "0";
                //}
                hmd.MaHSKB = (row["MA_HANGKB"].ToString());
                if (hmd.MaHSKB.Length < 10)
                {
                    for (int i = 1; i <= 10 - hmd.MaHSKB.Length; ++i)
                        hmd.MaHSKB += "0";
                }
                hmd.MaLoaiHinh = (row["MA_LH"].ToString());
                hmd.MIENTHUE = Convert.ToByte(row["MIENTHUE"].ToString());
                hmd.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                hmd.NuocXX_ID = (row["NUOC_XX"].ToString());
                if (row["PHU_THU"].ToString() != "")
                    hmd.PHU_THU = Convert.ToDouble(row["PHU_THU"]);
                hmd.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                hmd.STTHang = Convert.ToInt16(row["STTHANG"].ToString());
                hmd.TenHang = (row["TEN_HANG"].ToString());
                hmd.TGKB_VND = Convert.ToDouble(row["TGKB_VND"]);
                if (row["THUE_TTDB"].ToString() != "")
                    hmd.THUE_TTDB = Convert.ToDouble(row["THUE_TTDB"]);
                if (row["THUE_VAT"].ToString() != "")
                    hmd.THUE_VAT = Convert.ToDouble(row["THUE_VAT"]);
                if (row["THUE_XNK"].ToString() != "")
                    hmd.THUE_XNK = Convert.ToDouble(row["THUE_XNK"]);
                if (row["TL_QUYDOI"].ToString() != "")
                    hmd.TL_QUYDOI = Convert.ToDecimal(row["TL_QUYDOI"]);
                hmd.TRIGIA_KB = Convert.ToDouble(row["TRIGIA_KB"].ToString());
                if (row["TRIGIA_THUKHAC"].ToString() != "")
                    hmd.TRIGIA_THUKHAC = Convert.ToDouble(row["TRIGIA_THUKHAC"]);
                hmd.TRIGIA_TT = Convert.ToDouble(row["TRIGIA_TT"]);
                if (row["TS_TTDB"].ToString() != "")
                    hmd.TS_TTDB = Convert.ToDecimal(row["TS_TTDB"]);
                if (row["TS_VAT"].ToString() != "")
                    hmd.TS_VAT = Convert.ToDecimal(row["TS_VAT"]);
                if (row["TS_XNK"].ToString() != "")
                    hmd.TS_XNK = Convert.ToDecimal(row["TS_XNK"]);
                if (row["TYLE_THUKHAC"].ToString() != "")
                    hmd.TYLE_THUKHAC = Convert.ToDecimal(row["TYLE_THUKHAC"]);
                hmd.InsertUpdateTransaction(trangsaction);
            }
        }
        public HangMauDichDieuChinhCollection SelectCollectionByLanDieuChinhMax()
        {
            string spName = "SELECT  * FROM v_SXXK_HangMauDichDieuChinhMoiNhat where SoToKhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and NamDangKy=@NamDangKy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            HangMauDichDieuChinhCollection collection = new HangMauDichDieuChinhCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                HangMauDichDieuChinh entity = new HangMauDichDieuChinh();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_NPL_SP"))) entity.MA_NPL_SP = reader.GetString(reader.GetOrdinal("MA_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt16(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSKB"))) entity.MaHSKB = reader.GetString(reader.GetOrdinal("MaHSKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetInt32(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_KB"))) entity.DGIA_KB = reader.GetDouble(reader.GetOrdinal("DGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DGIA_TT"))) entity.DGIA_TT = reader.GetDouble(reader.GetOrdinal("DGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_DG"))) entity.MA_DG = reader.GetString(reader.GetOrdinal("MA_DG"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_KB"))) entity.TRIGIA_KB = reader.GetDouble(reader.GetOrdinal("TRIGIA_KB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_TT"))) entity.TRIGIA_TT = reader.GetDouble(reader.GetOrdinal("TRIGIA_TT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TGKB_VND"))) entity.TGKB_VND = reader.GetDouble(reader.GetOrdinal("TGKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("LOAITSXNK"))) entity.LOAITSXNK = reader.GetByte(reader.GetOrdinal("LOAITSXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_XNK"))) entity.TS_XNK = reader.GetDecimal(reader.GetOrdinal("TS_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_TTDB"))) entity.TS_TTDB = reader.GetDecimal(reader.GetOrdinal("TS_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TS_VAT"))) entity.TS_VAT = reader.GetDecimal(reader.GetOrdinal("TS_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) entity.PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
                if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) entity.MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
                if (!reader.IsDBNull(reader.GetOrdinal("TL_QUYDOI"))) entity.TL_QUYDOI = reader.GetDecimal(reader.GetOrdinal("TL_QUYDOI"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_THKE"))) entity.MA_THKE = reader.GetString(reader.GetOrdinal("MA_THKE"));
                if (!reader.IsDBNull(reader.GetOrdinal("CHOXULY"))) entity.CHOXULY = reader.GetByte(reader.GetOrdinal("CHOXULY"));
                if (!reader.IsDBNull(reader.GetOrdinal("TYLE_THUKHAC"))) entity.TYLE_THUKHAC = reader.GetDecimal(reader.GetOrdinal("TYLE_THUKHAC"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) entity.TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
                collection.Add(entity);
            }
            return collection;
        }
        public void FixHangDieuChinh(int soToKhai , int namDangKy)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {

                    HangMauDichDieuChinh HDC = new HangMauDichDieuChinh();
                    HangMauDichDieuChinhCollection HDCCollection = HDC.SelectCollectionDynamic(" MaLoaiHinh LIKE 'NSX%' AND SoToKhai = " + soToKhai + " AND NamDangKy = " + namDangKy ," SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, Ma_NPL_SP");
                    HangMauDichDieuChinhCollection HDCFix = new HangMauDichDieuChinhCollection();
                    HangMauDichDieuChinh HDCTemp = new HangMauDichDieuChinh();
                    double tyGiaTT = 0;
                    foreach (HangMauDichDieuChinh item in HDCCollection)
                    {
                        if (item.LanDieuChinh == HDCTemp.LanDieuChinh && item.SoToKhai == HDCTemp.SoToKhai && item.MaLoaiHinh == HDCTemp.MaLoaiHinh && item.NamDangKy == HDCTemp.NamDangKy && item.MaHaiQuan == HDCTemp.MaHaiQuan && item.MA_NPL_SP == HDCTemp.MA_NPL_SP)
                        {
                            HDC.Luong += item.Luong;
                            HDC.DGIA_KB += item.DGIA_KB * (double)item.Luong;
                        }
                        else
                        {
                            if (item != HDCCollection[0])
                            {
                                HDC.DGIA_KB = HDC.DGIA_KB / (double)HDC.Luong;
                                HDC.DGIA_TT = HDC.DGIA_KB * tyGiaTT;
                                HDC.TRIGIA_KB = HDC.DGIA_KB * (double)HDC.Luong;
                                HDC.TRIGIA_TT = HDC.DGIA_TT * (double)HDC.Luong;
                                HDC.TGKB_VND = HDC.TRIGIA_TT;
                                HDC.THUE_XNK = HDC.TRIGIA_TT * (double)HDC.TS_XNK / 100;
                                HDCFix.Add(HDC);
                            }
                            HDC = new HangMauDichDieuChinh();
                            //HDC.SoToKhai = item.SoToKhai;
                            //HDC.MaLoaiHinh = item.MaLoaiHinh;
                            //HDC.NamDangKy = item.NamDangKy;
                            //HDC.MaHaiQuan = item.MaHaiQuan;
                            //HDC.MaHang = item.MaHang;
                            //HDC.LanDieuChinh = item.LanDieuChinh;
                            //HDC.MA_NPL_SP = item.MA_NPL_SP;
                            //HDC.STTHang = item.STTHang;
                            //HDC.MaHSKB = HDC.
                            HDC = (HangMauDichDieuChinh)item.MemberwiseClone();
                            tyGiaTT = HDC.DGIA_TT / HDC.DGIA_KB;
                            HDC.Luong = item.Luong;
                            HDC.DGIA_KB = item.DGIA_KB * (double)item.Luong;
                            HDCTemp.SoToKhai = item.SoToKhai;
                            HDCTemp.MaLoaiHinh = item.MaLoaiHinh;
                            HDCTemp.NamDangKy = item.NamDangKy;
                            HDCTemp.MaHaiQuan = item.MaHaiQuan;
                            HDCTemp.MA_NPL_SP = item.MA_NPL_SP;

                        }
                    }
                    HDC.DGIA_KB = HDC.DGIA_KB / (double)HDC.Luong;
                    HDC.DGIA_TT = HDC.DGIA_KB * tyGiaTT;
                    HDC.TRIGIA_KB = HDC.DGIA_KB * (double)HDC.Luong;
                    HDC.TRIGIA_TT = HDC.DGIA_TT * (double)HDC.Luong;
                    HDC.TGKB_VND = HDC.TRIGIA_TT;
                    HDC.THUE_XNK = HDC.TRIGIA_TT * (double)HDC.TS_XNK / 100;
                    HDCFix.Add(HDC);
                    HDC.DeleteCollection(HDCCollection, transaction);
                    HDC.InsertTransaction(transaction,HDCFix);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                    
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public static void updateDatabaseMoi(DataSet ds, SqlTransaction trangsaction, DataSet dsToKhaiCu)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HangMauDichDieuChinh hmd = new HangMauDichDieuChinh();
                if (row["CHOXULY"].ToString() != "")
                    hmd.CHOXULY = Convert.ToByte(row["CHOXULY"].ToString());
                if (row["DGIA_KB"].ToString() != "")
                    hmd.DGIA_KB = Convert.ToDouble(row["DGIA_KB"]);
                if (row["DGIA_TT"].ToString() != "")
                    hmd.DGIA_TT = Convert.ToDouble(row["DGIA_TT"]);
                hmd.DinhMuc = Convert.ToInt32(row["DINHMUC"]);
                hmd.DVT_ID = (row["MA_DVT"].ToString());
                hmd.LanDieuChinh = Convert.ToInt32(row["LAN_DC"].ToString());
                if (row["LOAITSXNK"].ToString() != "")
                    hmd.LOAITSXNK = Convert.ToByte(row["LOAITSXNK"].ToString());
                hmd.Luong = Convert.ToDecimal(row["LUONG"]);
                hmd.MA_DG = (row["MA_DG"].ToString());
                hmd.MA_NPL_SP = (row["MA_NPL_SP"].ToString());
                if (row["MA_THKE"].ToString() != "")
                    hmd.MA_THKE = (row["MA_THKE"].ToString());
                hmd.MaHaiQuan = (row["MA_HQ"].ToString());
                hmd.MaHang = (row["MA_PHU"].ToString());
                hmd.MaHS = (row["MA_HANG"].ToString());
                //if (hmd.MaHS.Length < 10)
                //{
                //    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                //        hmd.MaHS += "0";
                //}
                hmd.MaHSKB = (row["MA_HANGKB"].ToString());
                if (hmd.MaHSKB.Length < 10)
                {
                    for (int i = 1; i <= 10 - hmd.MaHSKB.Length; ++i)
                        hmd.MaHSKB += "0";
                }
                hmd.MaLoaiHinh = (row["MA_LH"].ToString());
                hmd.MIENTHUE = Convert.ToByte(row["MIENTHUE"].ToString());
                hmd.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                hmd.NuocXX_ID = (row["NUOC_XX"].ToString());
                if (row["PHU_THU"].ToString() != "")
                    hmd.PHU_THU = Convert.ToDouble(row["PHU_THU"]);
                hmd.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                hmd.STTHang = Convert.ToInt16(row["STTHANG"].ToString());
                hmd.TenHang = (row["TEN_HANG"].ToString());
                hmd.TGKB_VND = Convert.ToDouble(row["TGKB_VND"]);
                if (row["THUE_TTDB"].ToString() != "")
                    hmd.THUE_TTDB = Convert.ToDouble(row["THUE_TTDB"]);
                if (row["THUE_VAT"].ToString() != "")
                    hmd.THUE_VAT = Convert.ToDouble(row["THUE_VAT"]);
                if (row["THUE_XNK"].ToString() != "")
                    hmd.THUE_XNK = Convert.ToDouble(row["THUE_XNK"]);
                if (row["TL_QUYDOI"].ToString() != "")
                    hmd.TL_QUYDOI = Convert.ToDecimal(row["TL_QUYDOI"]);
                hmd.TRIGIA_KB = Convert.ToDouble(row["TRIGIA_KB"].ToString());
                if (row["TRIGIA_THUKHAC"].ToString() != "")
                    hmd.TRIGIA_THUKHAC = Convert.ToDouble(row["TRIGIA_THUKHAC"]);
                hmd.TRIGIA_TT = Convert.ToDouble(row["TRIGIA_TT"]);
                if (row["TS_TTDB"].ToString() != "")
                    hmd.TS_TTDB = Convert.ToDecimal(row["TS_TTDB"]);
                if (row["TS_VAT"].ToString() != "")
                    hmd.TS_VAT = Convert.ToDecimal(row["TS_VAT"]);
                if (row["TS_XNK"].ToString() != "")
                    hmd.TS_XNK = Convert.ToDecimal(row["TS_XNK"]);
                if (row["TYLE_THUKHAC"].ToString() != "")
                    hmd.TYLE_THUKHAC = Convert.ToDecimal(row["TYLE_THUKHAC"]);
             
                try
                {
                    DataRow[] rowTTDM = dsToKhaiCu.Tables[0].Select("MaLoaiHinh='" + hmd.MaLoaiHinh + "' and NamDangKy=" + hmd.NamDangKy + " and MaHaiQuan='" + hmd.MaHaiQuan + "' and  SoToKhai=" + hmd.SoToKhai);
                    if (rowTTDM == null || rowTTDM.Length == 0)
                    hmd.InsertTransaction(trangsaction);
                }
                catch { }
            }
        }
    }
}