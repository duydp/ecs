﻿using System;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Text;
using System.Xml;
using System.Collections;
using System.Globalization;
using Company.BLL.Utils;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;
namespace Company.BLL.KDT.SXXK
{
    public partial class DinhMucDangKy
    {
        #region Fileds
        private DinhMucCollection _DMCollection = new DinhMucCollection();
        #endregion

        #region Property
        public DinhMucCollection DMCollection
        {
            set { this._DMCollection = value; }
            get { return this._DMCollection; }
        }
        #endregion


        //-----------------------------------------------------------------------------------------
        public long SoTiepNhanMax()
        {
            string sql = "SELECT MAX(SoTiepNhan) FROM T_KDT_SXXK_DinhMucDangKy";
            SqlDatabase sqlData = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand sqlCommand = (SqlCommand)sqlData.GetSqlStringCommand(sql);

            object obj = sqlData.ExecuteScalar(sqlCommand);
            if (obj == null || String.IsNullOrEmpty(obj.ToString()))
            {
                return 0;
            }
            else
            {
                return (Int64)obj;
            }
        }
        public void LoadDMCollection()
        {
            this._DMCollection = DinhMuc.SelectCollectionBy_Master_ID(this.ID);
        }

        public void LoadDMCollection(SqlTransaction trans)
        {
            this._DMCollection = DinhMuc.SelectCollectionBy_Master_ID(this.ID, trans);
        }

        public DataSet getDinhMucOfSanPham(string masp, string mahaiquan, string madv)
        {
            /* string sql = "SELECT MaNguyenPhuLieu,STTHang,dvt.Ten as DVT_ID,DinhMucSuDung,TyLeHaoHut,(DinhMucSuDung+DinhMucSuDung*TyLeHaoHut/100)as DinhMucChung,npl.Ten as TenNPL " +
                         "FROM [dbo].[t_KDT_SXXK_DinhMuc] dm inner join t_SXXK_NguyenPhuLieu npl " +
                         "on dm.MaNguyenPhuLieu=npl.Ma " +
                         "inner join t_HaiQuan_DonViTinh dvt on dvt.id=npl.DVT_ID " +
                          "WHERE [Master_ID] = @Master_ID and [MaSanPham]=@MaSanPham and npl.MaDoanhNghiep=@MaDoanhNghiep and npl.MaHaiQuan=@MaHaiQuan";
                          */
            DataSet ds = new DataSet();
            try
            {
                string pSql = "p_SXXK_DinhMucOfSanPham";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(pSql);
                db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this.ID);
                db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, masp);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahaiquan);

                ds = db.ExecuteDataSet(dbCommand);

                dbCommand.Connection.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return ds;
        }

        public DataSet getSanPhamOfDinhMuc(string mahaiquan, string madv)
        {
            string sql = "SELECT distinct sp.*,dvt.ten as tendvt from t_SXXK_SanPham sp inner join " +
                        " [dbo].[t_KDT_SXXK_DinhMuc] dm  " +
                        "on dm.MaSanPham=sp.Ma " +
                        "inner join t_HaiQuan_DonViTinh dvt on dvt.id=sp.DVT_ID " +
                         "WHERE [Master_ID] = @Master_ID  and sp.MaDoanhNghiep=@MaDoanhNghiep and sp.MaHaiQuan=@MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this.ID);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahaiquan);

            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            dbCommand.Connection.Close();
            return ds;
        }
        public bool CloneToDB(SqlTransaction transaction)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand("p_KDT_SXXK_DinhMucDangKy_Copy");
                db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
                if (transaction != null)
                    db.ExecuteNonQuery(dbCommand, transaction);
                else
                    db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static DataSet getNPLofSP(string maSP, string mahaiquan, string madv)
        {
            string sql = "select * from t_KDT_SXXK_NguyenPhuLieu";
            sql += "where ma in (";
            sql += "select MaNguyenPhuLieu from t_KDT_SXXK_DinhMuc as dm inner join t_KDT_SXXK_DinhMucDangKy as dmdk";
            sql += "on dm.Master_ID = dmdk.ID";
            sql += "where dm.masanpham=@MaSanPham";
            sql += " AND dmdk.TrangThaiXuLy = 1 and MaHaiQuan=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep";
            sql += ")";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahaiquan);

            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            dbCommand.Connection.Close();
            return ds;
        }

        public static DataSet getSanPhamDaDKDinhMuc(string maHaiQuan, string maDoanhNghiep)
        {
            string sql = "SELECT * from t_KDT_SXXK_SanPham where ma in (";
            sql += "select masanpham from t_KDT_SXXK_DinhMuc as dm inner join t_KDT_SXXK_DinhMucDangKy as dmdk ";
            sql += "on dm.Master_ID = dmdk.ID ";
            sql += "where dmdk.TrangThaiXuLy = 1 and MaHaiQuan=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep";
            sql += ")";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);

            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            dbCommand.Connection.Close();
            return ds;
        }

        public static DataSet getNPL(string maHaiQuan, string maDoanhNghiep)
        {
            string sql = "SELECT * from t_KDT_SXXK_NguyenPhuLieu as npl inner join t_KDT_SXXK_NguyenPhuLieuDangKy as npldk ";
            sql += " on npl.Master_ID = npldk.ID ";
            sql += " where npldk.MaHaiQuan=@MaHaiQuan and npldk.MaDoanhNghiep=@MaDoanhNghiep";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);

            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            dbCommand.Connection.Close();
            return ds;
        }
        public static DataTable getDinhMucSanPham(string WhereCondition, string OrderByExpression)
        {
            string sql = "[dbo].[p_KDT_SXXK_DinhMucDangKy_DinhMucChung]"; ;

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(sql);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.VarChar, WhereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.VarChar, OrderByExpression);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public static DataSet getDinhMucDaDK_OfSanPham(string masp, string mahaiquan, string madv)
        {
            string sql = "SELECT dm.ID as ID, MaSanPham, MaNguyenPhuLieu, DinhMucSuDung,TyLeHaoHut,(DinhMucSuDung+DinhMucSuDung*TyLeHaoHut/100)as DinhMucChung,npl.Ten as TenNPL, npl.DVT_ID as DVT_ID " +
                       "FROM [dbo].[t_KDT_SXXK_DinhMuc] dm inner join t_SXXK_NguyenPhuLieu npl " +
                       "on dm.MaNguyenPhuLieu=npl.Ma " +
                //"inner join t_HaiQuan_DonViTinh dvt on dvt.id=npl.DVT_ID " +
                        "WHERE [MaSanPham]=@MaSanPham";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, masp);

            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);

            dbCommand.Connection.Close();
            return ds;
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {

                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    string masp = "";
                    int i = 0;
                    foreach (DinhMuc dmDetail in this._DMCollection)
                    {
                        if (dmDetail.MaSanPham != masp)
                        {
                            masp = dmDetail.MaSanPham;
                            i = 0;
                        }
                        dmDetail.STTHang = ++i;
                        if (dmDetail.ID == 0)
                        {
                            dmDetail.Master_ID = this._ID;
                            dmDetail.ID = dmDetail.InsertTransaction(transaction);
                        }
                        else
                        {
                            dmDetail.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFull(string connectionString)
        {
            bool ret;
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    //Kiem tra ton tai Dinh muc DK tren Database Target
                    DinhMucDangKy dmTemp = DinhMucDangKy.Load(this._SoTiepNhan, this._NgayTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, this._TrangThaiXuLy, connectionString);

                    //KDT Dinh muc dang ky
                    if (dmTemp == null || dmTemp.ID == 0)
                        this._ID = this.InsertTransaction(transaction, db);
                    else
                    {
                        this._ID = dmTemp.ID;
                        this.UpdateTransaction(transaction, db);
                    }
                    //KDT Dinh muc
                    string masp = "";
                    int i = 0;
                    foreach (DinhMuc dmDetail in this._DMCollection)
                    {
                        if (dmDetail.MaSanPham != masp)
                        {
                            masp = dmDetail.MaSanPham;
                            i = 0;
                        }
                        dmDetail.STTHang = ++i;

                        /***/
                        dmDetail.Master_ID = this._ID;

                        dmDetail.InsertUpdateTransactionBy(transaction, db);
                    }
                    //SXXK Dinh muc
                    TransferDataToSXXK(transaction, db);

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this._ID == 0)
                this._ID = this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);

            foreach (DinhMuc dmDetail in this._DMCollection)
            {
                if (dmDetail.ID == 0)
                {
                    dmDetail.Master_ID = this._ID;
                    dmDetail.ID = dmDetail.InsertTransaction(transaction);
                }
                else
                {
                    dmDetail.UpdateTransaction(transaction);
                }
            }
        }

        public void DeleteDMCollection(SqlTransaction transaction)
        {
            string sql = "delete from t_KDT_SXXK_DinhMuc where master_id=@ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                db.ExecuteNonQuery(dbCommand, transaction);
            else
                db.ExecuteNonQuery(dbCommand);
        }

        public static void DongBoDuLieuPhongKhai(DinhMucDangKyCollection dmDKCollection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (DinhMucDangKy dmDangKy in dmDKCollection)
                    {
                        foreach (DinhMuc dm in dmDangKy.DMCollection)
                        {
                            dm.ID = 0;
                        }
                        MsgSend msg = new MsgSend();
                        msg.master_id = dmDangKy.ID;
                        msg.LoaiHS = "DM";
                        msg.DeleteTransaction(transaction);
                        dmDangKy.DeleteDMCollection(transaction);
                        dmDangKy.InsertUpdateFull(transaction);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }


        }

        #region Webservice của Hải quan
        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            //set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            //LanTN không cho phép tạo mới GUIDSTR
            //this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            this.Update();
            return doc.InnerXml;
        }
        private string ConfigPhongBiPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            //this.Update();
            return doc.InnerXml;
        }
        #region Khai báo danh sách sản phẩm
        public string WSSendXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DinhMuc, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXML());

            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                Globals.SaveMessage(doc.InnerXml, this.ID, MessageTitle.KhaiBaoDinhMuc);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    //XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    //XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";
        }
        private string ConvertCollectionToXML()
        {
            //load du lieu
            XmlDocument docDM = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docDM.Load(path + "\\TemplateXML\\KhaiBaoDinhMuc.xml");
            XmlNode DinhMucNode = docDM.GetElementsByTagName("DINH_MUC")[0];

            XmlNode nodeHQNhan = docDM.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            XmlNode nodeDN = docDM.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DoiTac.GetName(this.MaDoanhNghiep);

            if (this.DMCollection == null || this.DMCollection.Count == 0)
            {
                this.DMCollection = DinhMuc.SelectCollectionBy_Master_ID(this.ID);
            }
            string st = "";
            XmlNode nodeSP = null;
            CultureInfo culture = new CultureInfo("vi-VN");
            NumberFormatInfo f = new NumberFormatInfo();
            if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            {
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";
            }
            foreach (DinhMuc dm in this.DMCollection)
            {
                if (dm.MaSanPham != st)
                {
                    st = dm.MaSanPham;
                    nodeSP = docDM.CreateElement("DINH_MUC.SP");
                    XmlAttribute attMASP = docDM.CreateAttribute("MA_SP");
                    attMASP.Value = dm.MaSanPham;
                    nodeSP.Attributes.Append(attMASP);
                    DinhMucNode.AppendChild(nodeSP);
                }
                XmlNode nodeNPL = docDM.CreateElement("DINH_MUC.SP.ITEM");

                XmlAttribute sttAtt = docDM.CreateAttribute("STT");
                sttAtt.Value = dm.STTHang.ToString();
                nodeNPL.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docDM.CreateAttribute("MA_NPL");
                maAtt.Value = dm.MaNguyenPhuLieu;
                nodeNPL.Attributes.Append(maAtt);

                XmlAttribute DinhMucSDAtt = docDM.CreateAttribute("DINH_MUC_SD");
                DinhMucSDAtt.Value = dm.DinhMucSuDung.ToString(f);
                nodeNPL.Attributes.Append(DinhMucSDAtt);

                XmlAttribute TyLeHHAtt = docDM.CreateAttribute("TY_LE_HH");
                TyLeHHAtt.Value = dm.TyLeHaoHut.ToString(f);
                nodeNPL.Attributes.Append(TyLeHHAtt);
                nodeSP.AppendChild(nodeNPL);
            }
            return docDM.InnerXml;
        }
        #endregion Khai báo danh sách nguyên phụ liệu

        #region Huy khai báo danh mục nguyên phụ liệu

        public string WSCancelXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DinhMuc, MessgaseFunction.HuyKhaiBao));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\HuyKhaiBaoDinhMuc.xml");

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                doc.InnerText.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoHuyDinhMuc);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoHuyDinhMuc);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";

        }

        #endregion Huy khai báo danh mục nguyên phụ liệu

        #region Lấy phản hồi

        public string WSRequestXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.HoiTrangThai));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\LayPhanHoiDinhMuc.xml");

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";


        }
        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            //doc.GetElementsByTagName("function")[0].InnerText = "5";
            doc.GetElementsByTagName("function")[0].InnerText = MessgaseFunction.LayPhanHoi.ToString();
            //doc.GetElementsByTagName("type")[0].InnerText = MessgaseType.ThongTin.ToString();

            string kq = "";
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
                    }
                }
                catch
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;
            try
            {
                XmlNode nodeRoot1 = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
                {
                    XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                    if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.DINH_MUC)
                    {
                        #region Lấy số tiếp nhận của danh sách NPL

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.TrangThaiXuLy = 0;
                            //this.NamDK = 
                            this.Update();
                        }
                        #endregion Lấy số tiếp nhận của danh sách NPLx
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoLayPhanHoiDinhMuc, string.Format("Số tiếp nhận {0},ngày tiếp nhận {1},trạng thái xử lý {2}", this.SoTiepNhan, this.NgayTiepNhan, this.TrangThaiXuLy));

                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.DINH_MUC)
                    {
                        #region Hủy khai báo DINH MUC

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            this.SoTiepNhan = 0;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                            this.Update();
                        }

                        #endregion Hủy khai báo NPL
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQHuyDinhMuc);

                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == LAY_THONG_TIN.DINH_MUC)
                    {
                        #region Nhận trạng thái hồ sơ DM
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "DA_XU_LY")
                        {
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                            XmlNode nodeDM = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/DINH_MUC");
                            TransgferDataToSXXK(nodeDM);
                            Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQDaXyLyDinhMuc, string.Format("Số tiếp nhận {0}, năm đăng ký {1},ngày tiếp nhận {2}, trạng thái xử lý {3}", this.SoTiepNhan, this.NamDK, this.NgayTiepNhan, this.TrangThaiXuLy));

                        }
                        else if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "TU_CHOI")
                        {
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                            this.Update();
                            Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQTuChoiDinhMuc, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                        }
                        #endregion Nhận trạng thái hồ sơ NPL
                    }
                }
                else if (nodeRoot1 != null && nodeRoot1.Attributes.GetNamedItem("TuChoi") != null
                    && nodeRoot1.Attributes["TuChoi"].Value == "yes")// Từ chối
                {
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 01, 01);
                    this.HUONGDAN = nodeRoot1.InnerText.ToString();
                    this.Update();
                    Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQTuChoiDinhMuc, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                }
                else if (nodeRoot1 != null && nodeRoot1.SelectSingleNode("PHAN_LUONG") != null)
                {

                    XmlNode nodePhanLuong = docNPL.SelectSingleNode("Envelope/Body/Content/Root").SelectSingleNode("PHAN_LUONG");
                    this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                    this.HUONGDAN = nodePhanLuong.Attributes["HUONGDAN"].Value;

                    this.Update();
                }
                else if (nodeRoot1 != null && nodeRoot1.Attributes.GetNamedItem("TrangThai") != null && nodeRoot1.Attributes["TrangThai"].Value == "yes"
                    && nodeRoot1.Attributes.GetNamedItem("SOTN") != null && nodeRoot1.Attributes["SOTN"].Value != "")
                {
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                    this.Update();
                    //XmlNode nodeDM = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/DINH_MUC");
                    //TransgferDataToSXXK(nodeDM);
                    TransferDataToSXXK();
                    Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQDuyetDinhMuc);

                }
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
                {
                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                    string errorSt = "";
                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";
                    Globals.SaveMessage(kq, this.ID, errorSt, docNPL.InnerXml);
                    throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Không thể xử lý message kết quả", new Exception(kq));
                throw ex;
            }
            return "";

        }

        #endregion Lấy phản hồi
        #endregion Webservice của Hải quan

        public void TransferDataToSXXK()
        {
            if (this.DMCollection.Count == 0)
            {
                this.LoadDMCollection();
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    //cap nhat vao nbang thong tin dinh muc
                    foreach (DinhMuc dm1 in this.DMCollection)
                    {
                        SXXK_ThongTinDinhMuc ttdm = new SXXK_ThongTinDinhMuc();
                        //BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                        ttdm.MaDoanhNghiep = this.MaDoanhNghiep;
                        ttdm.MaHaiQuan = this.MaHaiQuan;
                        ttdm.MaSanPham = dm1.MaSanPham;
                        ttdm.NgayDangKy = this.NgayTiepNhan.Year == 1900 ? DateTime.Now : this.NgayTiepNhan;
                        ttdm.NgayApDung = this.NgayTiepNhan.Year == 1900 ? DateTime.Now : this.NgayTiepNhan;
                        ttdm.NgayHetHan = this.NgayTiepNhan.Year == 1900 ? DateTime.Now : this.NgayTiepNhan;
                        ttdm.ThanhLy = 0;
                        ttdm.SoDinhMuc = this.NgayTiepNhan.Year == 1900 ? Convert.ToInt32(DateTime.Now.Year) : Convert.ToInt32(this.NgayTiepNhan.Year);
                        ttdm.LenhSanXuat_ID = dm1.MaDinhDanhLenhSX == null ? 0 : KDT_LenhSanXuat.GetID(dm1.MaDinhDanhLenhSX);
                        ttdm.InsertUpdate(transaction);
                    }
                    //cp nhat vao bang dinh muc

                    foreach (DinhMuc dm in this.DMCollection)
                    {
                        SXXK_DinhMuc dmSXXK = new SXXK_DinhMuc();
                        //BLL.SXXK.DinhMuc dmSXXK = new Company.BLL.SXXK.DinhMuc();
                        dmSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                        dmSXXK.MaHaiQuan = this.MaHaiQuan;
                        dmSXXK.MaSanPham = dm.MaSanPham;
                        dmSXXK.MaNguyenPhuLieu = dm.MaNguyenPhuLieu;
                        dmSXXK.DinhMucSuDung = dm.DinhMucSuDung;
                        dmSXXK.TyLeHaoHut = dm.TyLeHaoHut;
                        dmSXXK.DinhMucChung = dm.DinhMucSuDung * (dm.TyLeHaoHut / 100 + 1);
                        dmSXXK.GhiChu = dm.GhiChu;
                        dmSXXK.IsFromVietNam = dm.IsFromVietNam;
                        dmSXXK.MaDinhDanh = dm.MaDinhDanhLenhSX;
                        dmSXXK.SoTiepNhan = this.SoTiepNhan;
                        dmSXXK.NgayTiepNhan = this.NgayTiepNhan.Year == 1900 ? DateTime.Now : this.NgayTiepNhan;
                        dmSXXK.TrangThaiXuLy = this.TrangThaiXuLy;
                        dmSXXK.LenhSanXuat_ID = dm.MaDinhDanhLenhSX == null ? 0 :  KDT_LenhSanXuat.GetID(dm.MaDinhDanhLenhSX);
                        dmSXXK.GuidString = this.GUIDSTR;
                        dmSXXK.InsertUpdate(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void DeleteFull()
        {
            if (this.DMCollection.Count == 0)
            {
                this.LoadDMCollection();
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (SXXK_DinhMuc item in SXXK_DinhMuc.SelectCollectionAll())
                    {
                        foreach (DinhMuc dm in this.DMCollection)
                        {
                            if (item.MaSanPham == dm.MaSanPham)
                            {
                                item.Delete();
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void TransferDataToSXXK(SqlTransaction transaction, SqlDatabase db)
        {
            if (this.DMCollection.Count == 0)
                this.LoadDMCollection(transaction);

            //cap nhat vao nbang thong tin dinh muc
            foreach (DinhMuc dm1 in this.DMCollection)
            {
                BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                ttdm.MaDoanhNghiep = this.MaDoanhNghiep;
                ttdm.MaHaiQuan = this.MaHaiQuan;
                ttdm.MaSanPham = dm1.MaSanPham;
                ttdm.NgayDangKy = DateTime.Now;
                ttdm.ThanhLy = 0;
                ttdm.InsertUpdateTransaction(transaction, db);
            }

            //cp nhat vao bang dinh muc
            int i = 0;
            foreach (DinhMuc dm in this.DMCollection)
            {
                BLL.SXXK.DinhMuc dmSXXK = new Company.BLL.SXXK.DinhMuc();
                dmSXXK.DinhMucSuDung = dm.DinhMucSuDung;
                dmSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                dmSXXK.MaHaiQuan = this.MaHaiQuan;
                dmSXXK.MaNguyenPhuLieu = dm.MaNguyenPhuLieu;
                dmSXXK.MaSanPHam = dm.MaSanPham;
                dmSXXK.TyLeHaoHut = dm.TyLeHaoHut;
                dmSXXK.DinhMucChung = dm.DinhMucSuDung * (dm.TyLeHaoHut / 100 + 1);
                dmSXXK.GhiChu = "";
                dmSXXK.IsFromVietNam = dm.IsFromVietNam;
                dmSXXK.InsertUpdateTransaction(transaction, db);

                //TODO: HungTQ, Update 27/04/2010.
                Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("{0}.KDT Dinh muc: MaHaiQuan={1}, MaDoanhNghiep={2}, MaSanPHam={3}, MaNguyenPhuLieu={4}, TenNPL={5}, DinhMucSuDung={6}, DinhMucChung={7}, TyLeHaoHut={8} ", i++, dmSXXK.MaHaiQuan, dmSXXK.MaDoanhNghiep, dmSXXK.MaSanPHam, dmSXXK.MaNguyenPhuLieu, dmSXXK.TenNPL, dmSXXK.DinhMucSuDung, dmSXXK.DinhMucChung, dmSXXK.TyLeHaoHut)));

            }
        }

        private void TransgferDataToSXXK(XmlNode nodeDM)
        {
            if (DMCollection.Count == 0)
                this.LoadDMCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    //cap nhat vao nbang thong tin dinh muc
                    foreach (XmlNode node in nodeDM.ChildNodes)
                    {
                        BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                        ttdm.MaDoanhNghiep = this.MaDoanhNghiep;
                        ttdm.MaHaiQuan = this.MaHaiQuan;
                        ttdm.MaSanPham = node.Attributes["MA_SP"].Value;
                        ttdm.NgayDangKy = Convert.ToDateTime(node.Attributes["NGAY_DK"].Value);
                        ttdm.SoDinhMuc = Convert.ToInt32(node.Attributes["SO_DM"].Value);
                        ttdm.ThanhLy = 0;
                        ttdm.InsertUpdateTransaction(transaction);
                    }
                    //cp nhat vao bang dinh muc

                    foreach (DinhMuc dm in this.DMCollection)
                    {
                        BLL.SXXK.DinhMuc dmSXXK = new Company.BLL.SXXK.DinhMuc();
                        dmSXXK.DinhMucSuDung = dm.DinhMucSuDung;
                        dmSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                        dmSXXK.MaHaiQuan = this.MaHaiQuan;
                        dmSXXK.MaNguyenPhuLieu = dm.MaNguyenPhuLieu;
                        dmSXXK.MaSanPHam = dm.MaSanPham;
                        dmSXXK.TyLeHaoHut = dm.TyLeHaoHut;
                        dmSXXK.DinhMucChung = dm.DinhMucSuDung * (dm.TyLeHaoHut / 100 + 1);
                        dmSXXK.GhiChu = "";
                        dmSXXK.IsFromVietNam = dm.IsFromVietNam;
                        dmSXXK.InsertUpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        #region TQDT :
        public string TQDTWSLayPhanHoi(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.HoiTrangThai));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\TemplateXML\\LayPhanHoiDaduyet.xml");

            //XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            //nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            //nodeHQNhan.Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            //XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            //nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            //nodeDN.Attributes["TEN_DV"].Value = "";

            //XmlNode nodeDuLieu = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU");
            //nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            //nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = "";
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";


        }
        public string TQDTLayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.GetElementsByTagName("function")[0].InnerText = "5";
            string kq = "";
            int i = 0;
            for (i = 1; i <= 3; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
                    }
                }
                catch
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 3)
                return doc.InnerXml;

            try
            {
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
                {
                    XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                    if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.DINH_MUC)
                    {
                        #region Lấy số tiếp nhận của danh sách DM

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.TrangThaiXuLy = 0;
                            this.NamDK = short.Parse(nodeDuLieu.Attributes["NAM_TN"].Value);
                            this.Update();
                        }
                        #endregion Lấy số tiếp nhận của danh sách DM
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoLayPhanHoiDinhMuc, string.Format("Số tiếp nhận {0}, năm đăng ký {1},ngày tiếp nhận {2}", this.SoTiepNhan, this.NamDK, this.NgayTiepNhan));

                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.DINH_MUC)
                    {
                        #region Hủy khai báo DM

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            this.SoTiepNhan = 0;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                            this.Update();
                        }

                        #endregion Hủy khai báo DM
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQHuyDinhMuc, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == LAY_THONG_TIN.DINH_MUC)
                    {
                        #region Nhận trạng thái hồ sơ DM
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "DA_XU_LY")
                        {
                            string phanHoi = TQDTWSLayPhanHoi(pass);

                            if (phanHoi.Length != 0)
                            {
                                XmlDocument docPH = new XmlDocument();
                                docPH.LoadXml(phanHoi);

                                XmlNode nodeTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                                if (nodeTuChoi.Attributes.GetNamedItem("TuChoi") != null
                                    && nodeTuChoi.Attributes["TuChoi"].Value == "yes")
                                {
                                    //nodeTuChoi.InnerText;
                                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                                    this.Update();
                                }
                            }
                            else
                            {
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                XmlNode nodeDM = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/DINH_MUC");
                                TransgferDataToSXXK(nodeDM);
                                this.Update();
                            }
                            Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQDaXyLyDinhMuc, string.Format("Số tiếp nhận {0}, năm đăng ký {1},ngày tiếp nhận {2}, trạng thái xử lý {3}", this.SoTiepNhan, this.NamDK, this.NgayTiepNhan, this.TrangThaiXuLy));


                        }
                        else if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "TU_CHOI")
                        {
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                            this.Update();
                            Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQTuChoiDinhMuc, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                        }
                        #endregion Nhận trạng thái hồ sơ DM
                    }
                }
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
                {
                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                    string errorSt = "";
                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";
                    throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
                }
                /*Kiem tra PHAN LUONG*/
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTN") != null)
                {
                    #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                    XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                    this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                    this.NgayTiepNhan = DateTime.Today;
                    this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                    /*Lay thong tin phan luong*/
                    if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                    {
                        XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                        this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                        this.Huongdan_PL = nodePhanLuong.Attributes["HUONGDAN"].Value;
                    }

                    this.Update();

                    #endregion Lấy số tiếp nhận của danh sách NPL
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Không thể xử lý message kết quả", new Exception(kq));
                throw ex;
            }
            return "";

        }
        #endregion

        public static DinhMucDangKy Load(long iD)
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMucDangKy_Load";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            DinhMucDangKy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new DinhMucDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanChungTu"))) entity.SoTiepNhanChungTu = reader.GetInt64(reader.GetOrdinal("SoTiepNhanChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) entity.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) entity.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                // Edit by Khanh - 28/02/2012 - Load thêm cột GUIDSTR
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }

        public static DinhMucDangKy Load(long SoTiepNhan, DateTime NgayTiepNhan, string MaHaiQuan, string MaDoanhNghiep, int TrangThaiXuLy, string connectionString)
        {
            SqlDatabase db = new SqlDatabase(connectionString);
            string spName = "[dbo].p_KDT_SXXK_DinhMucDangKy_LoadBy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);

            DinhMucDangKy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new DinhMucDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanChungTu"))) entity.SoTiepNhanChungTu = reader.GetInt64(reader.GetOrdinal("SoTiepNhanChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) entity.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) entity.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
            }
            reader.Close();
            dbCommand.Connection.Close();

            return entity;
        }


        public bool Load(long SoTiepNhan, DateTime NgayTiepNhan, string MaHaiQuan, string MaDoanhNghiep, int TrangThaiXuLy)
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMucDangKy_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) this.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanChungTu"))) this.SoTiepNhanChungTu = reader.GetInt64(reader.GetOrdinal("SoTiepNhanChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) this.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) this.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public static IDataReader SelectReaderDynamic1(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMucDangKy_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
        public static DataSet SelectDynamicFull(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].[p_KDT_SXXK_DinhMucDangKy_SelectDynamicFull]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
        public static IDataReader SelectReaderDynamicAll(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].[p_KDT_SXXK_DinhMucDangKy_DinhMucChung]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
        public static DataSet SelectDynamicAll(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].[p_KDT_SXXK_DinhMucDangKy_DinhMucChung]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
        //Phiph--- tiem kiem sp va Npl--------------------------------------------------------- 
        public static IDataReader SelectReaderDynamic_SP_NPL(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMucDangKy_SelectDynamic_SP_NPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //tiem kiem sp va Npl DinhMucMaHang--------------------------------------------------------- 
        public static IDataReader SelectReaderDynamic_SP_NPL_DinhMucMaHang(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].p_KDT_SXXK_DinhMucDangKy_SelectDynamic_SP_NPL_DMMH";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DinhMucDangKyCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            DinhMucDangKyCollection collection = new DinhMucDangKyCollection();

            IDataReader reader = SelectReaderDynamic1(whereCondition, orderByExpression);
            while (reader.Read())
            {
                DinhMucDangKy entity = new DinhMucDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanChungTu"))) entity.SoTiepNhanChungTu = reader.GetInt64(reader.GetOrdinal("SoTiepNhanChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) entity.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) entity.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static DinhMucDangKyCollection SelectCollectionDynamicAll(string whereCondition, string orderByExpression)
        {
            DinhMucDangKyCollection collection = new DinhMucDangKyCollection();

            IDataReader reader = SelectReaderDynamicAll(whereCondition, orderByExpression);
            while (reader.Read())
            {
                DinhMucDangKy entity = new DinhMucDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_SP"))) entity.DVT_SP = reader.GetString(reader.GetOrdinal("DVT_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetString(reader.GetOrdinal("MuaVN"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //----------------------------------------------------------------------------------------------
        public static DinhMucDangKyCollection SelectCollectionMaSanPham(string whereCondition, string orderByExpression)
        {
            DinhMucDangKyCollection collection = new DinhMucDangKyCollection();

            IDataReader reader = SelectReaderDynamic_SP_NPL(whereCondition, orderByExpression);
            while (reader.Read())
            {
                DinhMucDangKy entity = new DinhMucDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanChungTu"))) entity.SoTiepNhanChungTu = reader.GetInt64(reader.GetOrdinal("SoTiepNhanChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) entity.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) entity.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //------------------------- DINHMUCMAHANG ----------------------------- 
        public static DinhMucDangKyCollection SelectCollectionMaSanPham_DinhMucMaHang(string whereCondition, string orderByExpression)
        {
            DinhMucDangKyCollection collection = new DinhMucDangKyCollection();

            IDataReader reader = SelectReaderDynamic_SP_NPL_DinhMucMaHang(whereCondition, orderByExpression);
            while (reader.Read())
            {
                DinhMucDangKy entity = new DinhMucDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                //if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                // if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                //if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanChungTu"))) entity.SoTiepNhanChungTu = reader.GetInt64(reader.GetOrdinal("SoTiepNhanChungTu"));
                //if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) entity.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                // if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                // if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) entity.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                // if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public int DeleteDynamicTransaction(string where, SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_DinhMucDangKy_DeleteDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, where);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_SXXK_DinhMucDangKy_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@SoTiepNhanChungTu", SqlDbType.BigInt, this._SoTiepNhanChungTu);
            db.AddInParameter(dbCommand, "@SoDinhMuc", SqlDbType.Int, this._SoDinhMuc);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            db.AddInParameter(dbCommand, "@NgayApDung", SqlDbType.DateTime, this._NgayApDung);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_SXXK_DinhMucDangKy_Update";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@SoTiepNhanChungTu", SqlDbType.BigInt, this._SoTiepNhanChungTu);
            db.AddInParameter(dbCommand, "@SoDinhMuc", SqlDbType.Int, this._SoDinhMuc);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            db.AddInParameter(dbCommand, "@NgayApDung", SqlDbType.DateTime, this._NgayApDung);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public bool InsertUpdateFullDaiLy()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DinhMucDangKy objTemp = new DinhMucDangKy();
                    objTemp.Load(SoTiepNhan, NgayTiepNhan, MaHaiQuan, MaDoanhNghiep, TrangThaiXuLy);

                    if (objTemp.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.ID = objTemp.ID;
                        this.UpdateTransaction(transaction);
                    }

                    string masp = "";
                    int i = 0;
                    foreach (DinhMuc dmDetail in this._DMCollection)
                    {
                        if (dmDetail.MaSanPham != masp)
                        {
                            masp = dmDetail.MaSanPham;
                            i = 0;
                        }
                        dmDetail.STTHang = ++i;
                        if (dmDetail.ID == 0)
                        {
                            dmDetail.Master_ID = this._ID;
                            dmDetail.ID = dmDetail.InsertTransaction(transaction);
                        }
                        else
                        {
                            dmDetail.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public static DataSet getDinhMucDaDK_OfSanPham(long masterID, string mahaiquan, string madv)
        {
            string sql = "SELECT dm.ID as ID, MaSanPham, MaNguyenPhuLieu, DinhMucSuDung,TyLeHaoHut,(DinhMucSuDung+DinhMucSuDung*TyLeHaoHut/100)as DinhMucChung " +
                        ", npl.Ten as TenNPL, npl.DVT_ID as DVT_ID " +
                       "FROM [dbo].[t_KDT_SXXK_DinhMuc] dm " +
                       "inner join t_SXXK_NguyenPhuLieu npl " +
                       "on dm.MaNguyenPhuLieu=npl.Ma  and npl.MaHaiQuan=@MaHaiQuan " +
                //"inner join t_HaiQuan_DonViTinh dvt on dvt.id=npl.DVT_ID " +[MaSanPham]=@MaSanPham and
                        "WHERE  dm.[Master_ID] = @Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.VarChar, masterID);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, madv);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, mahaiquan);

            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);

            dbCommand.Connection.Close();
            return ds;
        }
        #region Đồng bộ dữ liệu
        public static DinhMucDangKy LoadbyMaSanPham(string MaSanPham)
        {
            string sql = @"SELECT  DISTINCT t_KDT_SXXK_DinhMucDangKy.*
                            FROM         t_KDT_SXXK_DinhMucDangKy INNER JOIN
                                         t_KDT_SXXK_DinhMuc ON t_KDT_SXXK_DinhMucDangKy.ID = t_KDT_SXXK_DinhMuc.Master_ID
                            where t_KDT_SXXK_DinhMuc.MaSanPham = @MaSanPham";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            DinhMucDangKy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new DinhMucDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanChungTu"))) entity.SoTiepNhanChungTu = reader.GetInt64(reader.GetOrdinal("SoTiepNhanChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) entity.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) entity.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                // Edit by Khanh - 28/02/2012 - Load thêm cột GUIDSTR
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
            }
            reader.Close();
            dbCommand.Connection.Close();
            return entity;
        }

        public string InsertFullFromISyncDaTa()
        {
            string error = string.Empty;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.ID = 0;
                    this.ID = this.InsertTransaction(transaction);
                    foreach (DinhMuc dm in this.DMCollection)
                    {
                        dm.Master_ID = this.ID;
                        dm.ID = 0;
                        dm.InsertTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    error = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            return error;
        }


        #endregion

        public void DeleteDinhMucGC()
        {
            if (this.DMCollection.Count == 0)
                this.LoadDMCollection();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    foreach (DinhMuc dmInfo in this.DMCollection)
                    {
                        Company.BLL.SXXK.DinhMuc dmDuyet = new Company.BLL.SXXK.DinhMuc();
                        long id = dmInfo.ID;
                        dmDuyet.MaNguyenPhuLieu = dmInfo.MaNguyenPhuLieu;
                        dmDuyet.MaSanPHam = dmInfo.MaSanPham;

                        dmDuyet.DeleletDM(id, dmDuyet.MaNguyenPhuLieu, dmDuyet.MaSanPHam, transaction);

                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void DeleteDinhMucSXXK()
        {
            if (this.DMCollection.Count == 0)
                this.LoadDMCollection();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    foreach (DinhMuc dmInfo in this.DMCollection)
                    {
                        Company.BLL.SXXK.DinhMuc dmDuyet = new Company.BLL.SXXK.DinhMuc();
                        //long id = dmInfo.ID;
                        //dmDuyet.MaNguyenPhuLieu = dmInfo.MaNguyenPhuLieu;
                        dmDuyet.MaSanPHam = dmInfo.MaSanPham;

                        dmDuyet.DeleletDM(dmDuyet.MaSanPHam, transaction);

                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}