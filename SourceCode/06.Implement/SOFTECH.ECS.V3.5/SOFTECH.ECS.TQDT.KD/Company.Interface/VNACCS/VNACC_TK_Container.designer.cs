﻿namespace Company.Interface
{
    partial class VNACC_TK_Container
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TK_Container));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDiaDiem5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ctrMaDiaDiem4 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiem3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiem2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDiaDiem1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDiaChiDiaDiem = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDiaDiem = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 499), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 499);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 475);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 475);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grList);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(679, 499);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem5);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem4);
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem3);
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem2);
            this.uiGroupBox1.Controls.Add(this.ctrMaDiaDiem1);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtDiaChiDiaDiem);
            this.uiGroupBox1.Controls.Add(this.txtSoContainer);
            this.uiGroupBox1.Controls.Add(this.txtTenDiaDiem);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(679, 226);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Địa điểm xếp hàng lên xe chở hàng";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDiaDiem5
            // 
            this.ctrMaDiaDiem5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDiaDiem5.Appearance.Options.UseBackColor = true;
            this.ctrMaDiaDiem5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem5.Code = "";
            this.ctrMaDiaDiem5.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDiaDiem5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem5.IsOnlyWarning = false;
            this.ctrMaDiaDiem5.IsValidate = true;
            this.ctrMaDiaDiem5.Location = new System.Drawing.Point(431, 21);
            this.ctrMaDiaDiem5.Name = "ctrMaDiaDiem5";
            this.ctrMaDiaDiem5.Name_VN = "";
            this.ctrMaDiaDiem5.SetOnlyWarning = false;
            this.ctrMaDiaDiem5.SetValidate = false;
            this.ctrMaDiaDiem5.ShowColumnCode = true;
            this.ctrMaDiaDiem5.ShowColumnName = false;
            this.ctrMaDiaDiem5.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem5.TabIndex = 4;
            this.ctrMaDiaDiem5.TagName = "";
            this.ctrMaDiaDiem5.Where = null;
            this.ctrMaDiaDiem5.WhereCondition = "";
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGhi.Location = new System.Drawing.Point(79, 185);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ctrMaDiaDiem4
            // 
            this.ctrMaDiaDiem4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDiaDiem4.Appearance.Options.UseBackColor = true;
            this.ctrMaDiaDiem4.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem4.Code = "";
            this.ctrMaDiaDiem4.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDiaDiem4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem4.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem4.IsOnlyWarning = false;
            this.ctrMaDiaDiem4.IsValidate = true;
            this.ctrMaDiaDiem4.Location = new System.Drawing.Point(343, 21);
            this.ctrMaDiaDiem4.Name = "ctrMaDiaDiem4";
            this.ctrMaDiaDiem4.Name_VN = "";
            this.ctrMaDiaDiem4.SetOnlyWarning = false;
            this.ctrMaDiaDiem4.SetValidate = false;
            this.ctrMaDiaDiem4.ShowColumnCode = true;
            this.ctrMaDiaDiem4.ShowColumnName = false;
            this.ctrMaDiaDiem4.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem4.TabIndex = 3;
            this.ctrMaDiaDiem4.TagName = "";
            this.ctrMaDiaDiem4.Where = null;
            this.ctrMaDiaDiem4.WhereCondition = "";
            // 
            // ctrMaDiaDiem3
            // 
            this.ctrMaDiaDiem3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDiaDiem3.Appearance.Options.UseBackColor = true;
            this.ctrMaDiaDiem3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem3.Code = "";
            this.ctrMaDiaDiem3.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDiaDiem3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem3.IsOnlyWarning = false;
            this.ctrMaDiaDiem3.IsValidate = true;
            this.ctrMaDiaDiem3.Location = new System.Drawing.Point(255, 21);
            this.ctrMaDiaDiem3.Name = "ctrMaDiaDiem3";
            this.ctrMaDiaDiem3.Name_VN = "";
            this.ctrMaDiaDiem3.SetOnlyWarning = false;
            this.ctrMaDiaDiem3.SetValidate = false;
            this.ctrMaDiaDiem3.ShowColumnCode = true;
            this.ctrMaDiaDiem3.ShowColumnName = false;
            this.ctrMaDiaDiem3.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem3.TabIndex = 2;
            this.ctrMaDiaDiem3.TagName = "";
            this.ctrMaDiaDiem3.Where = null;
            this.ctrMaDiaDiem3.WhereCondition = "";
            // 
            // ctrMaDiaDiem2
            // 
            this.ctrMaDiaDiem2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDiaDiem2.Appearance.Options.UseBackColor = true;
            this.ctrMaDiaDiem2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem2.Code = "";
            this.ctrMaDiaDiem2.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDiaDiem2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem2.IsOnlyWarning = false;
            this.ctrMaDiaDiem2.IsValidate = true;
            this.ctrMaDiaDiem2.Location = new System.Drawing.Point(167, 21);
            this.ctrMaDiaDiem2.Name = "ctrMaDiaDiem2";
            this.ctrMaDiaDiem2.Name_VN = "";
            this.ctrMaDiaDiem2.SetOnlyWarning = false;
            this.ctrMaDiaDiem2.SetValidate = false;
            this.ctrMaDiaDiem2.ShowColumnCode = true;
            this.ctrMaDiaDiem2.ShowColumnName = false;
            this.ctrMaDiaDiem2.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem2.TabIndex = 1;
            this.ctrMaDiaDiem2.TagName = "";
            this.ctrMaDiaDiem2.Where = null;
            this.ctrMaDiaDiem2.WhereCondition = "";
            // 
            // ctrMaDiaDiem1
            // 
            this.ctrMaDiaDiem1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDiaDiem1.Appearance.Options.UseBackColor = true;
            this.ctrMaDiaDiem1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDiaDiem1.Code = "";
            this.ctrMaDiaDiem1.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDiaDiem1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDiaDiem1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDiaDiem1.IsOnlyWarning = false;
            this.ctrMaDiaDiem1.IsValidate = true;
            this.ctrMaDiaDiem1.Location = new System.Drawing.Point(81, 21);
            this.ctrMaDiaDiem1.Name = "ctrMaDiaDiem1";
            this.ctrMaDiaDiem1.Name_VN = "";
            this.ctrMaDiaDiem1.SetOnlyWarning = false;
            this.ctrMaDiaDiem1.SetValidate = false;
            this.ctrMaDiaDiem1.ShowColumnCode = true;
            this.ctrMaDiaDiem1.ShowColumnName = false;
            this.ctrMaDiaDiem1.Size = new System.Drawing.Size(80, 24);
            this.ctrMaDiaDiem1.TabIndex = 0;
            this.ctrMaDiaDiem1.TagName = "";
            this.ctrMaDiaDiem1.Where = null;
            this.ctrMaDiaDiem1.WhereCondition = "";
            this.ctrMaDiaDiem1.Leave += new System.EventHandler(this.ctrMaDiaDiem1_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Số Container";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Địa chỉ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã";
            // 
            // txtDiaChiDiaDiem
            // 
            this.txtDiaChiDiaDiem.Location = new System.Drawing.Point(79, 85);
            this.txtDiaChiDiaDiem.Multiline = true;
            this.txtDiaChiDiaDiem.Name = "txtDiaChiDiaDiem";
            this.txtDiaChiDiaDiem.Size = new System.Drawing.Size(430, 46);
            this.txtDiaChiDiaDiem.TabIndex = 6;
            this.txtDiaChiDiaDiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoContainer
            // 
            this.txtSoContainer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoContainer.Location = new System.Drawing.Point(79, 145);
            this.txtSoContainer.Name = "txtSoContainer";
            this.txtSoContainer.Size = new System.Drawing.Size(256, 21);
            this.txtSoContainer.TabIndex = 5;
            this.txtSoContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDiaDiem
            // 
            this.txtTenDiaDiem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenDiaDiem.Location = new System.Drawing.Point(79, 54);
            this.txtTenDiaDiem.Name = "txtTenDiaDiem";
            this.txtTenDiaDiem.Size = new System.Drawing.Size(430, 21);
            this.txtTenDiaDiem.TabIndex = 5;
            this.txtTenDiaDiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.btnXoa);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 452);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(679, 47);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(589, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(508, 14);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 1;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // grList
            // 
            this.grList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.ColumnAutoResize = true;
            grList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grList_DesignTimeLayout_Reference_0.Instance")));
            grList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grList_DesignTimeLayout_Reference_0});
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.GroupByBoxVisible = false;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(0, 226);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.grList.Size = new System.Drawing.Size(679, 226);
            this.grList.TabIndex = 1;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grList_DeletingRecord);
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdPrint});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.LockCommandBars = true;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend1,
            this.cmdFeedback1,
            this.cmdResult1,
            this.cmdPrint1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(682, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Hỏi phản hồi qua KVGS";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "Nhận phản hồi qua KVGS";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Text = "In bảng kê mã vạch phương tiện chứa hàng";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(885, 32);
            // 
            // VNACC_TK_Container
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 537);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_TK_Container";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Danh sách Container";
            this.Load += new System.EventHandler(this.VNACC_TK_Container_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VNACC_TK_Container_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public Janus.Windows.GridEX.GridEX grList;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDiaDiem;
        public Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiem;
        public Janus.Windows.EditControls.UIButton btnClose;
        public Janus.Windows.EditControls.UIButton btnGhi;
        public Janus.Windows.EditControls.UIButton btnXoa;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem1;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem4;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem3;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem2;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDiaDiem5;
        public System.Windows.Forms.Label label4;
        public Janus.Windows.GridEX.EditControls.EditBox txtSoContainer;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
    }
}