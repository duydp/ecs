-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_SelectBy_HMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_SelectBy_HMD_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_MienGiamThue_DeleteBy_HMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_MienGiamThue_DeleteBy_HMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_Insert]
	@ID bigint,
	@HMD_ID bigint,
	@SoVanBanMienGiam nvarchar(35),
	@ThueSuatTruocGiam float,
	@TyLeMienGiam float
AS
INSERT INTO [dbo].[t_KDT_MienGiamThue]
(
	[ID],
	[HMD_ID],
	[SoVanBanMienGiam],
	[ThueSuatTruocGiam],
	[TyLeMienGiam]
)
VALUES
(
	@ID,
	@HMD_ID,
	@SoVanBanMienGiam,
	@ThueSuatTruocGiam,
	@TyLeMienGiam
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_Update]
	@ID bigint,
	@HMD_ID bigint,
	@SoVanBanMienGiam nvarchar(35),
	@ThueSuatTruocGiam float,
	@TyLeMienGiam float
AS

UPDATE
	[dbo].[t_KDT_MienGiamThue]
SET
	[HMD_ID] = @HMD_ID,
	[SoVanBanMienGiam] = @SoVanBanMienGiam,
	[ThueSuatTruocGiam] = @ThueSuatTruocGiam,
	[TyLeMienGiam] = @TyLeMienGiam
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_InsertUpdate]
	@ID bigint,
	@HMD_ID bigint,
	@SoVanBanMienGiam nvarchar(35),
	@ThueSuatTruocGiam float,
	@TyLeMienGiam float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_MienGiamThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_MienGiamThue] 
		SET
			[HMD_ID] = @HMD_ID,
			[SoVanBanMienGiam] = @SoVanBanMienGiam,
			[ThueSuatTruocGiam] = @ThueSuatTruocGiam,
			[TyLeMienGiam] = @TyLeMienGiam
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_MienGiamThue]
	(
			[ID],
			[HMD_ID],
			[SoVanBanMienGiam],
			[ThueSuatTruocGiam],
			[TyLeMienGiam]
	)
	VALUES
	(
			@ID,
			@HMD_ID,
			@SoVanBanMienGiam,
			@ThueSuatTruocGiam,
			@TyLeMienGiam
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_MienGiamThue]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_DeleteBy_HMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_DeleteBy_HMD_ID]
	@HMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_MienGiamThue]
WHERE
	[HMD_ID] = @HMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_MienGiamThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[SoVanBanMienGiam],
	[ThueSuatTruocGiam],
	[TyLeMienGiam]
FROM
	[dbo].[t_KDT_MienGiamThue]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_SelectBy_HMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_SelectBy_HMD_ID]
	@HMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[SoVanBanMienGiam],
	[ThueSuatTruocGiam],
	[TyLeMienGiam]
FROM
	[dbo].[t_KDT_MienGiamThue]
WHERE
	[HMD_ID] = @HMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HMD_ID],
	[SoVanBanMienGiam],
	[ThueSuatTruocGiam],
	[TyLeMienGiam]
FROM [dbo].[t_KDT_MienGiamThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_MienGiamThue_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_MienGiamThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[SoVanBanMienGiam],
	[ThueSuatTruocGiam],
	[TyLeMienGiam]
FROM
	[dbo].[t_KDT_MienGiamThue]	

GO

