using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
    public partial class HangChuyenTiep
    {
        #region Properties.

        public long ID { set; get; }
        public long Master_ID { set; get; }
        public int SoThuTuHang { set; get; }
        public string MaHang { set; get; }
        public string TenHang { set; get; }
        public string MaHS { set; get; }
        public decimal SoLuong { set; get; }
        public string ID_NuocXX { set; get; }
        public string ID_DVT { set; get; }
        public decimal DonGia { set; get; }
        public decimal TriGia { set; get; }
        public decimal TrongLuong { set; get; }
        public decimal DonGiaTT { set; get; }
        public decimal TriGiaTT { set; get; }
        public decimal TriGiaKB_VND { set; get; }
        public decimal ThueSuatXNK { set; get; }
        public decimal ThueSuatTTDB { set; get; }
        public decimal ThueSuatGTGT { set; get; }
        public decimal ThueXNK { set; get; }
        public decimal ThueTTDB { set; get; }
        public decimal ThueGTGT { set; get; }
        public decimal PhuThu { set; get; }
        public decimal TyLeThuKhac { set; get; }
        public decimal TriGiaThuKhac { set; get; }
        public byte MienThue { set; get; }
        public string Ma_HTS { set; get; }
        public string DVT_HTS { set; get; }
        public decimal SoLuong_HTS { set; get; }
        public string ThueSuatXNKGiam { set; get; }
        public string ThueSuatTTDBGiam { set; get; }
        public string ThueSuatVATGiam { set; get; }

        #endregion
    }
}