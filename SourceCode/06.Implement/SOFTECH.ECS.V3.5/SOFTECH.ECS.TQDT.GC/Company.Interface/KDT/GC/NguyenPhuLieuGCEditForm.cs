﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Common;
using Company.QuanTri;

namespace Company.Interface.KDT.GC
{
    public partial class NguyenPhuLieuGCEditForm : BaseFormHaveGuidPanel
    {
        public HopDong HD = new HopDong();
        public NguyenPhuLieu npl = new NguyenPhuLieu();
        public List<NguyenPhuLieuBoSung> nplBoSungCollections = new List<NguyenPhuLieuBoSung>();
        public NguyenPhuLieuBoSung nplBoSung = new NguyenPhuLieuBoSung();
        public LogHistory Log = new LogHistory();
        public bool isBrower = false;
        public String Caption;
        public bool IsChange;
        public NguyenPhuLieuGCEditForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = HD.NPLCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void SetData()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = npl.Ma.Trim();
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = npl.Ten.Trim();
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = npl.MaHS.Trim();
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                cbDonViTinh.SelectedValue = npl.DVT_ID;
                cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

                txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChu.Text = npl.GhiChu;
                txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                txtNguonCungCap.TextChanged -= new EventHandler(txt_TextChanged);
                txtNguonCungCap.Text = npl.NguonCungCap;
                txtNguonCungCap.TextChanged += new EventHandler(txt_TextChanged);

                chkTCU.Checked = npl.TuCungUng;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void NguyenPhuLieuGCEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                Caption = this.Text;
                if (!isBrower)
                {
                    txtMa.Focus();
                    this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                    cbDonViTinh.DataSource = this._DonViTinh;
                    cbDonViTinh.DisplayMember = "Ten";
                    cbDonViTinh.ValueMember = "ID";

                    if (!string.IsNullOrEmpty(npl.Ma))
                    {
                        SetData();
                        nplBoSungCollections = (List<NguyenPhuLieuBoSung>)NguyenPhuLieuBoSung.SelectCollectionBy_HopDong_ID_Ma(HD.ID, npl.Ma);
                        if (nplBoSungCollections != null && nplBoSungCollections.Count > 0)
                            nplBoSung = nplBoSungCollections[0];
                    }
                    else
                    {
                        cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                        cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
                        cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);
                    }
                    if (this.OpenType == OpenFormType.View)
                    {
                        btnAdd.Enabled = false;
                        btnXoa.Enabled = false;
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    }

                }
                else
                {
                    uiGroupBox2.Visible = false;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    this.Width = dgList.Width;
                    this.Height = dgList.Height;
                    btnAdd.Visible = false;
                    btnClose.Visible = false;
                    HD = HopDong.Load(HD.ID);

                    HD.LoadCollection();
                }
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = MaHS.SelectAll();
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaHS.AutoCompleteCustomSource = col;
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!MaHS.Validate(txtMaHS.Text, 10))
                {
                    error.SetIconPadding(txtMaHS, -8);
                    error.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                    txtMaHS.Focus();
                }
                else
                {
                    error.SetError(txtMaHS, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void setErro()
        {
            try
            {
                error.Clear();
                error.SetError(txtMa, null);
                error.SetError(txtMaHS, null);
                error.SetError(txtTen, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, "Mã nguyên phụ liệu", isOnlyWarning);
                isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã nguyên phụ liệu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, "Tên/Mô tả nguyên phụ liệu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHS, errorProvider, "Mã HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbDonViTinh, errorProvider, "Đơn vị tính", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNguonCungCap, errorProvider, "Nguồn cung cấp", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                txtMa.Focus();
                if (!ValidateForm(false))
                    return;
                if (!MaHS.Validate(txtMaHS.Text.Trim(), 8))
                {
                    error.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                    return;
                }
                checkExitsNPLAndSTTHang();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void reset()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = "";
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = "";
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = "";
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChu.Text = "";
                txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                error.SetError(txtMaHS, null);
                error.SetError(txtMa, null);
                error.SetError(txtTen, null);
                BindData();
                npl = new NguyenPhuLieu();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void checkExitsNPLAndSTTHang()
        {
            try
            {
                HD.NPLCollection.Remove(npl);
                foreach (NguyenPhuLieu nguyenPL in HD.NPLCollection)
                {
                    if (nguyenPL.Ma.Trim().ToUpper() == txtMa.Text.Trim().ToUpper())
                    {
                        showMsg("MSG_2702057");
                        //MLMessages("Đã có nguyên phụ liệu này trong danh sách.","MSG_WRN06","",false);
                        if (npl.Ma != "")
                            HD.NPLCollection.Add(npl);
                        return;
                    }
                }
                npl.Ten = txtTen.Text.Trim();
                npl.Ma = txtMa.Text.Trim();
                npl.MaHS = txtMaHS.Text.Trim();
                npl.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                npl.HopDong_ID = HD.ID;
                npl.GhiChu = txtGhiChu.Text.Trim();

                npl.NguonCungCap = txtNguonCungCap.Text;
                npl.TuCungUng = chkTCU.Checked;

                HD.NPLCollection.Add(npl);

                reset();
                this.setErro();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                npl = (NguyenPhuLieu)e.Row.DataRow;
                if (!isBrower)
                {
                    SetData();
                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private NguyenPhuLieuBoSung FindNPLBoSung(string maNPL)
        {
            try
            {
                foreach (NguyenPhuLieuBoSung item in HD.NPLBoSungCollection)
                {
                    if (item.Ma == maNPL)
                    {
                        return item;
                    }
                }

                return new NguyenPhuLieuBoSung();
            }
            catch (Exception ex)
            {;
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
                return new NguyenPhuLieuBoSung();
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.Edit)
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {
                        GridEXSelectedItemCollection items = dgList.SelectedItems;
                        List<NguyenPhuLieu> NPLCollectionTMP = new List<NguyenPhuLieu>();
                        foreach (GridEXSelectedItem row in items)
                        {
                            Company.GC.BLL.KDT.GC.NguyenPhuLieu nplDelete = (Company.GC.BLL.KDT.GC.NguyenPhuLieu)row.GetRow().DataRow;
                            if (NguyenPhuLieu.CheckExitNPLinDinhMuc(nplDelete.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI, this.HD.ID))
                            {
                                if (ShowMessage("Nguyên phụ liệu :  " + nplDelete.Ma + " đã khai định mức. Doanh nghiệp có muốn xóa không", true) == "Yes")
                                {
                                    nplDelete.Delete();
                                    Company.GC.BLL.GC.GC_NguyenPhuLieu.DeleteGC_NguyenPhuLieu(HD.ID, nplDelete.Ma);
                                    HD.NPLCollection.Remove(nplDelete);
                                    NPLCollectionTMP.Add(nplDelete);
                                }
                            }
                            else
                            {
                                nplDelete.Delete();
                                Company.GC.BLL.GC.GC_NguyenPhuLieu.DeleteGC_NguyenPhuLieu(HD.ID, nplDelete.Ma);
                                HD.NPLCollection.Remove(nplDelete);
                                NPLCollectionTMP.Add(nplDelete);
                            }
                        }
                        if(NPLCollectionTMP.Count >=1)
                            Log.LogHDGC(HD, NPLCollectionTMP, MessageTitle.XoaNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        BindData();
                        this.SetChange(true);
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }    
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
                e.Cancel = true;
            }
        }

        private void txtSoLuong_Leave(object sender, EventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    List<NguyenPhuLieu> NPLCollectionTMP = new List<NguyenPhuLieu>();
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.NguyenPhuLieu nplDelete = (Company.GC.BLL.KDT.GC.NguyenPhuLieu)row.GetRow().DataRow;
                        if (NguyenPhuLieu.CheckExitNPLinDinhMuc(nplDelete.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI, this.HD.ID))
                        {
                            if (ShowMessage("Nguyên phụ liệu :  " + nplDelete.Ma + " đã khai định mức. Doanh nghiệp có muốn xóa không", true) == "Yes")
                            {
                                nplDelete.Delete();
                                Company.GC.BLL.GC.GC_NguyenPhuLieu.DeleteGC_NguyenPhuLieu(HD.ID, nplDelete.Ma);
                                NPLCollectionTMP.Add(nplDelete);
                            }
                        }
                        else
                        {
                            nplDelete.Delete();
                            Company.GC.BLL.GC.GC_NguyenPhuLieu.DeleteGC_NguyenPhuLieu(HD.ID, nplDelete.Ma);
                            NPLCollectionTMP.Add(nplDelete);
                        }
                    }
                    foreach (NguyenPhuLieu NPL in NPLCollectionTMP)
                        HD.NPLCollection.Remove(NPL);
                    if (NPLCollectionTMP.Count >=1)
                        Log.LogHDGC(HD, NPLCollectionTMP, MessageTitle.XoaNguyenPhuLieu, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    this.setErro();
                    BindData();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void chkTCU_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                txtNguonCungCap.Text = chkTCU.Checked == false ? "Bên thuê gia công cung cấp" : "Tự cung ứng";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            npl = (NguyenPhuLieu)i.GetRow().DataRow;
                            if (!isBrower)
                            {
                                SetData();
                            }
                            else
                            {
                                this.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaNPL.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}