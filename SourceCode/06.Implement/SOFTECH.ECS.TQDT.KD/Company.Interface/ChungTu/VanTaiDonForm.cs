﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL;
using Company.Interface;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class VanTaiDonForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        public VanTaiDonForm()
        {
            InitializeComponent();
        }

        private void VanTaiDonForm_Load(object sender, EventArgs e)
        {
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            //DATLMQ bổ sung Lấy thông tin từ TKMD đưa vào Vận đơn 13/12/2010
            if (TKMD.SoVanDon.Length >= 0 && TKMD.VanTaiDon == null)
            {
                txtSoVanDon.Text = TKMD.SoVanDon;
                if (TKMD.NgayVanDon.Year == 1900)
                    ccNgayVanDon.Text = DateTime.Now.ToShortDateString();
                else
                    ccNgayVanDon.Text = TKMD.NgayVanDon.ToShortDateString();
                try
                {
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        txtMaNguoiNhanHang.Text = TKMD.MaDoanhNghiep;
                        txtTenNguoiNhanHang.Text = TKMD.TenDoanhNghiep;
                        txtTenNguoiGiaoHang.Text = TKMD.TenDonViDoiTac;
                    }
                    else
                    {
                        txtTenNguoiNhanHang.Text = TKMD.TenDonViDoiTac;
                        txtMaNguoiGiaoHang.Text = TKMD.MaDoanhNghiep;
                        txtTenNguoiGiaoHang.Text = TKMD.TenDoanhNghiep;

                        uiGroupBox5.Text = "Cửa khẩu xuất";
                        uiGroupBox4.Text = "Cửa khẩu nhập";
                        uiGroupBox8.Enabled = false;
                    }
                    cuaKhauControl1.Ma = TKMD.CuaKhau_ID;
                    txtMaDiaDiemDoHang.Text = TKMD.CuaKhau_ID;
                    txtTenDiaDiemDoHang.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);
                    txtTenDiaDiemXepHang.Text = TKMD.DiaDiemXepHang;
                    //DATLMQ update NgayTauDen 17/01/2011
                    ccNgayDen.Text = TKMD.NgayDenPTVT.ToShortDateString();
                    //DATLMQ update NoiDi, CuaKhauXuat 17/01/2011
                    txtNoiDi.Text = txtCuaKhauXuat.Text = txtTenDiaDiemXepHang.Text;
                    //DATLMQ update NgayKhoiHanh 17/01/2011
                    ccNgayKhoiHanh.Text = TKMD.NgayVanDon.ToShortDateString();
                    //DATLMQ update DiaDiemGiaoHang 17/01/2011
                    txtDiaDiemGiaoHang.Text = txtTenDiaDiemDoHang.Text;
                }
                catch
                {
                    ShowMessage("Cần lưu thông tin tờ khai trước khi mở Vận đơn", false);
                    this.Dispose();
                }
            }

            if (TKMD.VanTaiDon != null)
            {
                cuaKhauControl1.Ma = TKMD.VanTaiDon.CuaKhauNhap_ID;
                txtCuaKhauXuat.Text = TKMD.VanTaiDon.CuaKhauXuat;
                cbDKGH.SelectedValue = TKMD.VanTaiDon.DKGH_ID;
                chkHangRoi.Checked = TKMD.VanTaiDon.HangRoi;
                txtMaDiaDiemDoHang.Text = TKMD.VanTaiDon.MaCangDoHang;
                txtMaDiaDiemXepHang.Text = TKMD.VanTaiDon.MaCangXepHang;
                txtMaHangVT.Text = TKMD.VanTaiDon.MaHangVT;
                txtMaNguoiGiaoHang.Text = TKMD.VanTaiDon.MaNguoiGiaoHang;
                txtMaNguoiNhanHang.Text = TKMD.VanTaiDon.MaNguoiNhanHang;
                txtMaNguoiNhanHangTG.Text = TKMD.VanTaiDon.MaNguoiNhanHangTrungGian;
                //ccNgayDen.Value = TKMD.VanTaiDon.NgayDenPTVT;
                ccNgayDen.Text = TKMD.VanTaiDon.NgayDenPTVT.ToShortDateString();
                //ccNgayVanDon.Value = TKMD.VanTaiDon.NgayVanDon;
                ccNgayVanDon.Text = TKMD.VanTaiDon.NgayVanDon.ToShortDateString();
                ctrNuocXuat.Ma = TKMD.VanTaiDon.NuocXuat_ID;
                ctrQuocTichPTVT.Ma = TKMD.VanTaiDon.QuocTichPTVT;
                txtSoHieuPTVT.Text = TKMD.VanTaiDon.SoHieuPTVT;
                txtSoVanDon.Text = TKMD.VanTaiDon.SoVanDon;
                txtTenDiaDiemDoHang.Text = TKMD.VanTaiDon.TenCangDoHang;
                txtTenDiaDiemXepHang.Text = TKMD.VanTaiDon.TenCangXepHang;
                txtTenHangVT.Text = TKMD.VanTaiDon.TenHangVT;
                txtTenNguoiGiaoHang.Text = TKMD.VanTaiDon.TenNguoiGiaoHang;
                //txtTenNguoiNhanHang.Text = TKMD.TenDonViDoiTac;
                txtTenNguoiNhanHang.Text = TKMD.VanTaiDon.TenNguoiNhanHang;
                txtTenNguoiNhanHangTG.Text = TKMD.VanTaiDon.TenNguoiNhanHangTrungGian;
                txtTenPTVT.Text = TKMD.VanTaiDon.TenPTVT;
                txtNoiDi.Text = TKMD.VanTaiDon.NoiDi;
                txtSoHieuChuyenDi.Text = TKMD.VanTaiDon.SoHieuChuyenDi;
                //ccNgayKhoiHanh.Value = TKMD.VanTaiDon.NgayKhoiHanh;
                ccNgayKhoiHanh.Text = TKMD.VanTaiDon.NgayKhoiHanh.ToShortDateString();
                txtDiaDiemGiaoHang.Text = TKMD.VanTaiDon.DiaDiemGiaoHang;

                if (TKMD.VanTaiDon.ContainerCollection == null)
                    TKMD.VanTaiDon.ContainerCollection = new List<Container>();
                dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                Ghi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdXoaVanDon.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemContainerExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                TaoContainer.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Xoa.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                )
            {
                Ghi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdXoaVanDon.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemContainerExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                TaoContainer.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Xoa.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
        }

        private void Save()
        {
            cvError.Validate();
            if (!cvError.IsValid)
            {
                return;
            }

            if (!ValidateVanDon())
                return;

            if (TKMD.VanTaiDon == null)
                TKMD.VanTaiDon = new VanDon();
            TKMD.CuaKhau_ID = TKMD.VanTaiDon.CuaKhauNhap_ID = cuaKhauControl1.Ma;
            TKMD.VanTaiDon.CuaKhauXuat = txtCuaKhauXuat.Text.Trim();
            TKMD.DKGH_ID = TKMD.VanTaiDon.DKGH_ID = cbDKGH.SelectedValue.ToString();
            TKMD.VanTaiDon.HangRoi = chkHangRoi.Checked;
            TKMD.VanTaiDon.MaCangDoHang = txtMaDiaDiemDoHang.Text.Trim();
            TKMD.VanTaiDon.MaCangXepHang = txtMaDiaDiemXepHang.Text.Trim();
            TKMD.VanTaiDon.MaHangVT = txtMaHangVT.Text.Trim();
            TKMD.VanTaiDon.MaNguoiGiaoHang = txtMaNguoiGiaoHang.Text.Trim();
            TKMD.VanTaiDon.MaNguoiNhanHang = txtMaNguoiNhanHang.Text.Trim();
            TKMD.VanTaiDon.MaNguoiNhanHangTrungGian = txtMaNguoiNhanHangTG.Text.Trim();
            TKMD.NgayDenPTVT = TKMD.VanTaiDon.NgayDenPTVT = ccNgayDen.Value;
            TKMD.NgayVanDon = TKMD.VanTaiDon.NgayVanDon = ccNgayVanDon.Value;
            TKMD.NuocXK_ID = TKMD.VanTaiDon.NuocXuat_ID = ctrNuocXuat.Ma;
            if (chkQuocTich.Checked)
                TKMD.VanTaiDon.QuocTichPTVT = "JP";
            else
                TKMD.VanTaiDon.QuocTichPTVT = ctrQuocTichPTVT.Ma;
            TKMD.VanTaiDon.SoHieuPTVT = txtSoHieuPTVT.Text.Trim();
            TKMD.SoVanDon = TKMD.VanTaiDon.SoVanDon = txtSoVanDon.Text.Trim();
            TKMD.VanTaiDon.TenCangDoHang = txtTenDiaDiemDoHang.Text.Trim();
            TKMD.DiaDiemXepHang = TKMD.VanTaiDon.TenCangXepHang = txtTenDiaDiemXepHang.Text.Trim();
            TKMD.VanTaiDon.TenHangVT = txtTenHangVT.Text.Trim();
            TKMD.VanTaiDon.TenNguoiGiaoHang = txtTenNguoiGiaoHang.Text.Trim();
            TKMD.VanTaiDon.TenNguoiNhanHang = txtTenNguoiNhanHang.Text.Trim();
            TKMD.VanTaiDon.TenNguoiNhanHangTrungGian = txtTenNguoiNhanHangTG.Text;
            TKMD.SoHieuPTVT = TKMD.VanTaiDon.TenPTVT = txtTenPTVT.Text.Trim();
            TKMD.VanTaiDon.TKMD_ID = TKMD.ID;
            TKMD.VanTaiDon.DiaDiemGiaoHang = txtDiaDiemGiaoHang.Text.Trim();
            TKMD.VanTaiDon.NoiDi = txtNoiDi.Text.Trim();
            TKMD.VanTaiDon.SoHieuChuyenDi = txtSoHieuChuyenDi.Text.Trim();
            TKMD.VanTaiDon.NgayKhoiHanh = ccNgayKhoiHanh.Value;
            TKMD.VanTaiDon.TenCangXepHang = txtTenDiaDiemXepHang.Text;
            if (TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count > 0)
            {
                TKMD.SoContainer20 = 0;
                TKMD.SoContainer40 = 0;
                foreach (Container c in TKMD.VanTaiDon.ContainerCollection)
                {
                    if (c.LoaiContainer == "2")
                        TKMD.SoContainer20++;
                    else
                        TKMD.SoContainer40++;
                }
            }
            ShowMessage("Lưu thành công Vận tải đơn.\nLưu ý: Nhớ lưu lại thông tin tờ khai.", false);
            ToKhaiMauDichForm.tenPTVT = txtTenPTVT.Text;
            this.Close();
        }

        private void XoaVanDon()
        {
            try
            {
                if (TKMD.VanTaiDon == null) return;
                else if (TKMD.VanTaiDon.ID == 0)
                {
                    TKMD.VanTaiDon = null;
                }
                else
                    if (ShowMessage("Bạn có muốn xóa vận đơn này không?", true) == "Yes")
                    {
                        //Xoa container
                        foreach (Container c in TKMD.VanTaiDon.ContainerCollection)
                        {
                            try
                            {
                                if (c.ID > 0)
                                {
                                    c.Delete();
                                }
                                TKMD.VanTaiDon.ContainerCollection.Remove(c);
                            }
                            catch (Exception ex) { throw ex; }
                        }

                        //Xoa van don
                        TKMD.VanTaiDon.Delete();
                        TKMD.VanTaiDon = null;

                        ShowMessage("Xóa vận đơn thành công.", false);


                    }
                this.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); ShowMessage(ex.Message, false, true, ex.StackTrace); }
        }


        private void XoaContainer()
        {
            List<Container> ContainerCollection = new List<Container>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa Container này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Container container = (Container)i.GetRow().DataRow;
                        ContainerCollection.Add(container);
                    }
                }
                foreach (Container c in ContainerCollection)
                {
                    try
                    {
                        if (c.ID > 0)
                        {
                            c.Delete();
                        }
                        TKMD.VanTaiDon.ContainerCollection.Remove(c);
                    }
                    catch { }
                }
            }
            dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void AddContainer()
        {
            AddContainerForm f = new AddContainerForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

        }
        private void AddContainerExcel()
        {
            ReadExcContainerForm rexcelForm = new ReadExcContainerForm();
            rexcelForm.TKMD = TKMD;
            dgList.DataSource = rexcelForm.TKMD.VanTaiDon.ContainerCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            rexcelForm.ShowDialog();
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Ghi": Save();
                    break;
                case "cmdXoaVanDon": XoaVanDon();
                    break;
                case "Xoa": XoaContainer();
                    break;
                case "TaoContainer": AddContainer();
                    break;
                case "ThemContainerExcel": AddContainerExcel();
                    break;
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            List<Container> ContainerCollection = new List<Container>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa Container này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Container ContainerTMP = (Container)i.GetRow().DataRow;
                        ContainerCollection.Add(ContainerTMP);
                    }
                }
                foreach (Container item in ContainerCollection)
                {
                    try
                    {
                        if (item.ID > 0)
                        {
                            item.Delete();
                        }
                        TKMD.VanTaiDon.ContainerCollection.Remove(item);
                    }
                    catch { }
                }
                dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
            else
                e.Cancel = true;

        }

        private void chkQuocTich_CheckedChanged(object sender, EventArgs e)
        {
            if (chkQuocTich.Checked)
                ctrQuocTichPTVT.Enabled = false;
            else
                ctrQuocTichPTVT.Enabled = true;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["LoaiContainer"].Text == "2")
                e.Row.Cells["LoaiContainer"].Text = "Container20";
            if (e.Row.Cells["LoaiContainer"].Text == "4")
                e.Row.Cells["LoaiContainer"].Text = "Container40";
            if (e.Row.Cells["Trang_thai"].Text == "0")
                e.Row.Cells["Trang_thai"].Text = "Rỗng";
            if (e.Row.Cells["Trang_thai"].Text == "1")
                e.Row.Cells["Trang_thai"].Text = "Đầy";
        }

        #region Begin VALIDATE VAN DON

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateVanDon()
        {
            bool isValid = true;

            try
            {
                //So_Chung_Tu(35)
                isValid = Globals.ValidateLength(txtSoVanDon, 35, err, "Số vận đơn");

                //Ma_PTVT	char(3)

                //So_Hieu_PTVT	varchar(25)
                isValid &= Globals.ValidateLength(txtSoHieuPTVT, 25, err, "Số hiệu phương tiện vận tải");

                //Ten_PTVT varchar(255)
                isValid &= Globals.ValidateLength(txtTenPTVT, 255, err, "Tên phương tiện vận tải");

                //Ma_Hang_Van_Tai	varchar(17)
                isValid &= Globals.ValidateLength(txtMaHangVT, 17, err, "Mã hãng vận tải");

                //Ten_Hang_Van_Tai	varchar(35)
                isValid &= Globals.ValidateLength(txtTenHangVT, 35, err, "Tên hãng vận tải");

                //Ma_Nguoi_Nhan_Hang	varchar(17)
                isValid &= Globals.ValidateLength(txtMaNguoiNhanHang, 17, err, "Mã người nhận hàng");

                //Ma_Nguoi_Giao_Hang	varchar(17)
                isValid &= Globals.ValidateLength(txtMaNguoiGiaoHang, 17, err, "Mã người giao hàng");

                //Ma_Nguoi_Nhan_Hang_TG	varchar(17)
                isValid &= Globals.ValidateLength(txtMaNguoiNhanHangTG, 17, err, "Mã người nhận hàng trung gian");

                //Ma_DKGH	varchar(7)
                isValid &= Globals.ValidateLength(cbDKGH, 7, err, "Điều kiện giao hàng");

                //Ma_Cang_Do_Hang	nvarchar(50)
                isValid &= Globals.ValidateLength(txtMaDiaDiemDoHang, 50, err, "Mã địa điểm dỡ hàng");

                //Ma_Cang_Xep_Hang	varchar(11)
                isValid &= Globals.ValidateLength(txtMaDiaDiemXepHang, 11, err, "Mã địa điểm xếp hàng");

                //Ten_Cang_Xep_Hang	varchar(40)
                isValid &= Globals.ValidateLength(txtTenDiaDiemXepHang, 40, err, "Tên địa điểm xếp hàng");

                //Dia Diem Giao hang	varchar(60)
                isValid &= Globals.ValidateLength(txtDiaDiemGiaoHang, 60, err, "Địa điểm giao hàng");

            }
            catch (Exception ex) { }

            return isValid;
        }

        #endregion End VALIDATE VAN DONs

        private void dgList_DoubleClick(object sender, EventArgs e)
        {
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                return;
            if (dgList.SelectedItems.Count == 0)
                return;
            Container cont = (Container)dgList.SelectedItems[0].GetRow().DataRow;
            AddContainerForm f = new AddContainerForm();
            f.TKMD = this.TKMD;
            f.container = cont;
            f.ShowDialog();
            dgList.DataSource = TKMD.VanTaiDon.ContainerCollection;
            TinhSoCont();
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void TinhSoCont()
        {
            if (TKMD.VanTaiDon.ContainerCollection != null || TKMD.VanTaiDon.ContainerCollection.Count > 0)
            {
                TKMD.SoContainer40 = 0;
                TKMD.SoContainer20 = 0;
                foreach (Container cont in TKMD.VanTaiDon.ContainerCollection)
                {
                    if (cont.LoaiContainer == "2")
                    {
                        TKMD.SoContainer20++;
                    }
                    else if (cont.LoaiContainer == "4")
                    {
                        TKMD.SoContainer40++;
                    }
                }
            }
        }

    }
}

