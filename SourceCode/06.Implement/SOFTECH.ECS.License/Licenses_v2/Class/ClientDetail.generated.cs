using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Activation.Server
{
	public partial class ClientDetail
	{
		#region Properties.
		
		public string ProductsID { set; get; }
		public string MaMay { set; get; }
		public string SerialCode { set; get; }
		public string MaHaiQuan { set; get; }
		public string ServerDBName { set; get; }
		public string UserNameDB { set; get; }
		public string PasswordDB { set; get; }
		public string DBName { set; get; }
		public DateTime NgayOnline { set; get; }
		public DateTime NgayDangKy { set; get; }
		public DateTime NgayKichHoat { set; get; }
		public DateTime NgayHetHan { set; get; }
		public string DiaChiHaiQuan { set; get; }
		public string TenDichVu { set; get; }
		public string HostProxy { set; get; }
		public string PortProxy { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ClientDetail Load(string productsID, string maMay)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ProductsID", SqlDbType.VarChar, productsID);
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, maMay);
			ClientDetail entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new ClientDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ProductsID"))) entity.ProductsID = reader.GetString(reader.GetOrdinal("ProductsID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaMay"))) entity.MaMay = reader.GetString(reader.GetOrdinal("MaMay"));
				if (!reader.IsDBNull(reader.GetOrdinal("SerialCode"))) entity.SerialCode = reader.GetString(reader.GetOrdinal("SerialCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ServerDBName"))) entity.ServerDBName = reader.GetString(reader.GetOrdinal("ServerDBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameDB"))) entity.UserNameDB = reader.GetString(reader.GetOrdinal("UserNameDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("PasswordDB"))) entity.PasswordDB = reader.GetString(reader.GetOrdinal("PasswordDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DBName"))) entity.DBName = reader.GetString(reader.GetOrdinal("DBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayOnline"))) entity.NgayOnline = reader.GetDateTime(reader.GetOrdinal("NgayOnline"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKichHoat"))) entity.NgayKichHoat = reader.GetDateTime(reader.GetOrdinal("NgayKichHoat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiHaiQuan"))) entity.DiaChiHaiQuan = reader.GetString(reader.GetOrdinal("DiaChiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDichVu"))) entity.TenDichVu = reader.GetString(reader.GetOrdinal("TenDichVu"));
				if (!reader.IsDBNull(reader.GetOrdinal("HostProxy"))) entity.HostProxy = reader.GetString(reader.GetOrdinal("HostProxy"));
				if (!reader.IsDBNull(reader.GetOrdinal("PortProxy"))) entity.PortProxy = reader.GetString(reader.GetOrdinal("PortProxy"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_MaMay(string maMay)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectBy_MaMay]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, maMay);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_MaMay(string maMay)
		{
			const string spName = "p_LICENSE_ClientDetail_SelectBy_MaMay";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, maMay);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertClientDetail(string productsID, string maMay, string serialCode, string maHaiQuan, string serverDBName, string userNameDB, string passwordDB, string dBName, DateTime ngayOnline, DateTime ngayDangKy, DateTime ngayKichHoat, DateTime ngayHetHan, string diaChiHaiQuan, string tenDichVu, string hostProxy, string portProxy)
		{
			ClientDetail entity = new ClientDetail();	
			entity.ProductsID = productsID;
			entity.MaMay = maMay;
			entity.SerialCode = serialCode;
			entity.MaHaiQuan = maHaiQuan;
			entity.ServerDBName = serverDBName;
			entity.UserNameDB = userNameDB;
			entity.PasswordDB = passwordDB;
			entity.DBName = dBName;
			entity.NgayOnline = ngayOnline;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayKichHoat = ngayKichHoat;
			entity.NgayHetHan = ngayHetHan;
			entity.DiaChiHaiQuan = diaChiHaiQuan;
			entity.TenDichVu = tenDichVu;
			entity.HostProxy = hostProxy;
			entity.PortProxy = portProxy;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LICENSE_ClientDetail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ProductsID", SqlDbType.VarChar, ProductsID);
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, MaMay);
			db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@ServerDBName", SqlDbType.NVarChar, ServerDBName);
			db.AddInParameter(dbCommand, "@UserNameDB", SqlDbType.NVarChar, UserNameDB);
			db.AddInParameter(dbCommand, "@PasswordDB", SqlDbType.NVarChar, PasswordDB);
			db.AddInParameter(dbCommand, "@DBName", SqlDbType.NVarChar, DBName);
			db.AddInParameter(dbCommand, "@NgayOnline", SqlDbType.DateTime, NgayOnline.Year <= 1753 ? DBNull.Value : (object) NgayOnline);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayKichHoat", SqlDbType.DateTime, NgayKichHoat.Year <= 1753 ? DBNull.Value : (object) NgayKichHoat);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@DiaChiHaiQuan", SqlDbType.NVarChar, DiaChiHaiQuan);
			db.AddInParameter(dbCommand, "@TenDichVu", SqlDbType.NVarChar, TenDichVu);
			db.AddInParameter(dbCommand, "@HostProxy", SqlDbType.NVarChar, HostProxy);
			db.AddInParameter(dbCommand, "@PortProxy", SqlDbType.NVarChar, PortProxy);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateClientDetail(string productsID, string maMay, string serialCode, string maHaiQuan, string serverDBName, string userNameDB, string passwordDB, string dBName, DateTime ngayOnline, DateTime ngayDangKy, DateTime ngayKichHoat, DateTime ngayHetHan, string diaChiHaiQuan, string tenDichVu, string hostProxy, string portProxy)
		{
			ClientDetail entity = new ClientDetail();			
			entity.ProductsID = productsID;
			entity.MaMay = maMay;
			entity.SerialCode = serialCode;
			entity.MaHaiQuan = maHaiQuan;
			entity.ServerDBName = serverDBName;
			entity.UserNameDB = userNameDB;
			entity.PasswordDB = passwordDB;
			entity.DBName = dBName;
			entity.NgayOnline = ngayOnline;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayKichHoat = ngayKichHoat;
			entity.NgayHetHan = ngayHetHan;
			entity.DiaChiHaiQuan = diaChiHaiQuan;
			entity.TenDichVu = tenDichVu;
			entity.HostProxy = hostProxy;
			entity.PortProxy = portProxy;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LICENSE_ClientDetail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ProductsID", SqlDbType.VarChar, ProductsID);
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, MaMay);
			db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@ServerDBName", SqlDbType.NVarChar, ServerDBName);
			db.AddInParameter(dbCommand, "@UserNameDB", SqlDbType.NVarChar, UserNameDB);
			db.AddInParameter(dbCommand, "@PasswordDB", SqlDbType.NVarChar, PasswordDB);
			db.AddInParameter(dbCommand, "@DBName", SqlDbType.NVarChar, DBName);
			db.AddInParameter(dbCommand, "@NgayOnline", SqlDbType.DateTime, NgayOnline.Year == 1753 ? DBNull.Value : (object) NgayOnline);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year == 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayKichHoat", SqlDbType.DateTime, NgayKichHoat.Year == 1753 ? DBNull.Value : (object) NgayKichHoat);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@DiaChiHaiQuan", SqlDbType.NVarChar, DiaChiHaiQuan);
			db.AddInParameter(dbCommand, "@TenDichVu", SqlDbType.NVarChar, TenDichVu);
			db.AddInParameter(dbCommand, "@HostProxy", SqlDbType.NVarChar, HostProxy);
			db.AddInParameter(dbCommand, "@PortProxy", SqlDbType.NVarChar, PortProxy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateClientDetail(string productsID, string maMay, string serialCode, string maHaiQuan, string serverDBName, string userNameDB, string passwordDB, string dBName, DateTime ngayOnline, DateTime ngayDangKy, DateTime ngayKichHoat, DateTime ngayHetHan, string diaChiHaiQuan, string tenDichVu, string hostProxy, string portProxy)
		{
			ClientDetail entity = new ClientDetail();			
			entity.ProductsID = productsID;
			entity.MaMay = maMay;
			entity.SerialCode = serialCode;
			entity.MaHaiQuan = maHaiQuan;
			entity.ServerDBName = serverDBName;
			entity.UserNameDB = userNameDB;
			entity.PasswordDB = passwordDB;
			entity.DBName = dBName;
			entity.NgayOnline = ngayOnline;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayKichHoat = ngayKichHoat;
			entity.NgayHetHan = ngayHetHan;
			entity.DiaChiHaiQuan = diaChiHaiQuan;
			entity.TenDichVu = tenDichVu;
			entity.HostProxy = hostProxy;
			entity.PortProxy = portProxy;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ProductsID", SqlDbType.VarChar, ProductsID);
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, MaMay);
			db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@ServerDBName", SqlDbType.NVarChar, ServerDBName);
			db.AddInParameter(dbCommand, "@UserNameDB", SqlDbType.NVarChar, UserNameDB);
			db.AddInParameter(dbCommand, "@PasswordDB", SqlDbType.NVarChar, PasswordDB);
			db.AddInParameter(dbCommand, "@DBName", SqlDbType.NVarChar, DBName);
			db.AddInParameter(dbCommand, "@NgayOnline", SqlDbType.DateTime, NgayOnline.Year == 1753 ? DBNull.Value : (object) NgayOnline);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year == 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayKichHoat", SqlDbType.DateTime, NgayKichHoat.Year == 1753 ? DBNull.Value : (object) NgayKichHoat);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@DiaChiHaiQuan", SqlDbType.NVarChar, DiaChiHaiQuan);
			db.AddInParameter(dbCommand, "@TenDichVu", SqlDbType.NVarChar, TenDichVu);
			db.AddInParameter(dbCommand, "@HostProxy", SqlDbType.NVarChar, HostProxy);
			db.AddInParameter(dbCommand, "@PortProxy", SqlDbType.NVarChar, PortProxy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteClientDetail(string productsID, string maMay)
		{
			ClientDetail entity = new ClientDetail();
			entity.ProductsID = productsID;
			entity.MaMay = maMay;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ProductsID", SqlDbType.VarChar, ProductsID);
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, MaMay);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_MaMay(string maMay)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_DeleteBy_MaMay]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaMay", SqlDbType.VarChar, maMay);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
	}	
}