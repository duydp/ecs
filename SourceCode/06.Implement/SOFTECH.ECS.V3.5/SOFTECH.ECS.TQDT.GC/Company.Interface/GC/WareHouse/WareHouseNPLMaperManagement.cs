﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseNPLMaperManagement : Company.Interface.BaseForm
    {
        public T_KHOKETOAN_NGUYENPHULIEU NPL = new T_KHOKETOAN_NGUYENPHULIEU();
        public T_KHOKETOAN_NGUYENPHULIEU_MAP NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
        public NguyenPhuLieu NPLHopDong = new NguyenPhuLieu();
        public WareHouseNPLMaperManagement()
        {
            InitializeComponent();

            cbHopDong.DataSource = HopDong.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'","ID");
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = -1;
        }
        private void BinDataNPL()
        {
            try
            {
                grListNPL.Refetch();
                grListNPL.DataSource = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("HOPDONG_ID = " + cbHopDong.Value , "");
                grListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);              
            }
        }
        private void BindDataNPLMap()
        {
            try
            {
                grListNPLMap.Refetch();
                grListNPLMap.DataSource = NPL.NPLMapCollection;
                grListNPLMap.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void WareHouseNPLMaperManagement_Load(object sender, EventArgs e)
        {
            BinDataNPL();
        }

        private void btnSearchNPL_Click(object sender, EventArgs e)
        {
            try
            {
                grListNPL.Refetch();
                grListNPL.DataSource = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("MANPL LIKE '%" + txtNPL.Text.ToString() + "%' AND HOPDONG_ID = " + cbHopDong.Value, "");
                grListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchNPLMap_Click(object sender, EventArgs e)
        {
            try
            {
                grListNPLMap.Refetch();
                grListNPLMap.DataSource = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("MANPLMAP LIKE '%" + txtNPLMap.Text.ToString() + "%' AND HOPDONG_ID = " + cbHopDong.Value, "");
                grListNPLMap.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPL_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPLMap_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPLMap_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                NPLMap = (T_KHOKETOAN_NGUYENPHULIEU_MAP)e.Row.DataRow;
                HopDong HD = HopDong.Load(NPLMap.HOPDONG_ID);
                NPL = T_KHOKETOAN_NGUYENPHULIEU.Load(NPLMap.KHONPL_ID);
                NPL.NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionBy_KHONPL_ID(NPL.ID);
                WareHouseNPLMaper f = new WareHouseNPLMaper();
                f.HD = HD;
                f.NPL = NPL;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPL_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                NPL = new T_KHOKETOAN_NGUYENPHULIEU();
                NPL = (T_KHOKETOAN_NGUYENPHULIEU)e.Row.DataRow;
                NPL.NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionBy_KHONPL_ID(NPL.ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            BinDataNPL();
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            try
            {
                HopDong HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
                WareHouseNPLAutoMaperForm f = new WareHouseNPLAutoMaperForm();
                f.Type = "NPL";
                f.HD = HD;
                f.ShowDialog(this);
                BinDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                HopDong HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
                WareHouseNPLMaper f = new WareHouseNPLMaper();
                f.HD = HD;
                f.ShowDialog(this);
                BinDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPL_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListNPL.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NPL = (T_KHOKETOAN_NGUYENPHULIEU)i.GetRow().DataRow;
                        NPL.NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionBy_KHONPL_ID(NPL.ID);
                    }
                }
                BindDataNPLMap();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListNPL.SelectedItems;
                List<T_KHOKETOAN_NGUYENPHULIEU> ItemColl = new List<T_KHOKETOAN_NGUYENPHULIEU>();
                if (grListNPL.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_NGUYENPHULIEU)i.GetRow().DataRow);
                        }

                    }
                    string error = "";
                    string errorToTal = "";
                    foreach (T_KHOKETOAN_NGUYENPHULIEU item in ItemColl)
                    {
                        if (item.ID > 0)
                        {
                            List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> HHCollection = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectCollectionDynamic("MAHANG ='" + item.MANPL + "'", "");
     
                            if (HHCollection.Count > 0)
                            {
                                foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA HH in HHCollection)
                                {
                                    T_KHOKETOAN_PHIEUXNKHO PhieuXNK = T_KHOKETOAN_PHIEUXNKHO.Load(HH.PHIEUXNKHO_ID);
                                    error += "[" + HH.STT + "]-[" + HH.MAHANG + "]-[" + PhieuXNK.SOCT + "]-[" + PhieuXNK.NGAYCT.ToString("dd/MM/yyyy") + "]\n";
                                }
                            }
                            else
                            {
                                List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLMAPCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionBy_KHONPL_ID(item.ID);
                                foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP NPL in NPLMAPCollection)
                                {
                                    NPL.Delete();
                                }
                                item.Delete();
                            }
                        }
                    }
                    if (!String.IsNullOrEmpty(error))
                    {
                        errorToTal += "MÃ NPL ĐÃ SỬ DỤNG TRONG PHIẾU XUẤT NHẬP KHO - KHÔNG ĐƯỢC XÓA .\n[STT]-[MÃ NPL]-[SỐ CHỨNG TỪ]-[NGÀY CHỨNG TỪ]\n" + error;
                        ShowMessageTQDT(errorToTal, false);
                    }
                    else
                    {
                        ShowMessageTQDT("Xóa thành công !", false);
                    }
                    btnSearchNPL_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
