﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using DaiLy = Company.KDT.SHARE.Components.DaiLy;

namespace Company.Interface.DongBoDuLieu_New.Admin
{
    public partial class QuanLyNguoiDung : BaseForm
    {
        private ISyncData_SXXK myService =  IsyncDaTa_V3.SyncService();
        string maDoanhNghiep = "", passwordLogin = "", userLogin = "";
        List<Company.KDT.SHARE.Components.DaiLy> daiLyCollection;
        public QuanLyNguoiDung()
        {
            InitializeComponent();

//             myService.Url = WebService.LoadConfigure("WS_URL");
//             maDoanhNghiep = WebService.LoadConfigure("MaDoanhNghiep");
            dgList.RootTable.Columns["MaDoanhNghiep"].SortIndicator = SortIndicator.Descending;
            
        }
        public bool checkService(ISyncData_SXXK myService)
        {
            try
            {
                DaiLy daiLy = Helpers.Deserialize<DaiLy>(myService.LoginDaiLy("", ""));
                return true;
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Unable to connect to the remote server"))
                    MLMessages("Không thể kết nối đến webservice!", "", "", false);
                else if (ex.ToString().Contains("time out"))
                    MLMessages("Thời gian kết nối đến service đồng bộ dữ liệu quá lâu!", "", "", false);
                else
                    MLMessages("Xảy ra lỗi: " + ex.ToString(), "", "", false);
                return false;
            }
        }
        private void QuanLyNguoiDung_Load(object sender, EventArgs e)
         {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (!checkService(myService))
                    return;
                //Load ds Đại lý từ WS
                search();
            }
            catch (Exception ex)
            {
                MLMessages("Xảy ra lỗi: " + ex.ToString(), "", "", false);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            if (e.Command.Key == "TaoMoi")
            {
                if (Login())
                {
                    NguoiDungEditForm f = new NguoiDungEditForm();
                    f.userLogin = userLogin;
                    f.passwordLogin = passwordLogin;
                    if (userLogin.Equals("administrator"))
                        f.isCheck = true;
                    f.ShowDialog();
                    search();
                    try { dgList.Refetch(); }
                    catch { dgList.Refresh(); }
                }
            }
        }
        private bool Login()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                WSForm_DBDL wsForm = new WSForm_DBDL();
                if (Config.Pass == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return false;
                }
                passwordLogin = Config.Pass != "" ? Config.Pass : wsForm.txtMatKhau.Text.Trim();
                userLogin = Config.User != "" ? Config.User : wsForm.txtMaDoanhNghiep.Text.Trim();

                Company.KDT.SHARE.Components.DaiLy userDaiLy = Helpers.Deserialize<DaiLy>(myService.LoginDaiLy(userLogin, passwordLogin));
                if (userDaiLy == null || (userDaiLy.MaDoanhNghiep != WebService.LoadConfigure("MaDoanhNghiep") && !userDaiLy.USER_NAME.Equals("administrator")))
                {
                    MLMessages("Người dùng không hợp lệ!", "", "", false);
                    userLogin = "";
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            DaiLy user = (DaiLy)e.Row.DataRow;
            NguoiDungEditForm f = new NguoiDungEditForm();
            f.user = user;
            f.ShowDialog();
            search();
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void search() {
            daiLyCollection = Helpers.Deserialize<List<DaiLy>>(myService.SelectUserDaiLyAll());
            dgList.DataSource = daiLyCollection;
            
        }
        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (MLMessages("Bạn có muốn xóa người dùng này không ?", "MSG_USER04", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DaiLy u = (DaiLy)i.GetRow().DataRow;
                        bool ok;
                        if (u.isAdmin != true)
                        {
                            if (userLogin == "" && !Login())
                            {
                                return;
                            }else
                                ok = myService.DeleteUserDaiLy(userLogin, passwordLogin, u.USER_NAME, u.PASSWORD);
                            
                        }
                        else
                        {
                            MLMessages("Đây là người dùng mặc định của hệ thống. Không xóa thể xóa người dùng này.", "MSG_USER03", "", false);
                            e.Cancel = true;
                        }
                    }
                }
                //Load ds Đại lý từ WS
                search();
            }
            else
                e.Cancel = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            switch (Convert.ToInt32(e.Row.Cells["isAdmin"].Value))
            {
                case 0:
                    e.Row.Cells["isAdmin"].Text = "Đại lý";
                    break;
                case 1:
                    e.Row.Cells["isAdmin"].Text = "Admin";
                    break;
            }
        }
    }
}

