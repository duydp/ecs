﻿namespace Company.Interface.SXXK
{
    partial class ChungTuThanhToanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChungTuThanhToanForm));
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.uiCommandManager = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCmdMain = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdQuanLyHopDongXuatKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyHopDongXuatKhau");
            this.cmdQuanLyChungTu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyChungTu");
            this.cmdTaoCTTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTaoCTTT");
            this.cmdQuanLyHopDongXuatKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyHopDongXuatKhau");
            this.cmdQuanLyChungTu = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyChungTu");
            this.cmdTaoCTTT = new Janus.Windows.UI.CommandBars.UICommand("cmdTaoCTTT");
            this.imgSmall = new System.Windows.Forms.ImageList(this.components);
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.btnDelete);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.uiButton2);
            this.grbMain.Controls.Add(this.uiButton1);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(919, 369);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(833, 345);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 6;
            this.uiButton2.Text = "Đóng";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(538, 345);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(122, 23);
            this.uiButton1.TabIndex = 4;
            this.uiButton1.Text = "Xuất báo cáo";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // dgList
            // 
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.Location = new System.Drawing.Point(3, 12);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(904, 325);
            this.dgList.TabIndex = 0;
            this.dgList.TabStop = false;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(668, 345);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(749, 345);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(3, 343);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(235, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = " Bấm \"Ctrl + Enter\" để đánh xuống dòng.\r\n Bấm \"Delete\" để xóa dòng trong lưới.";
            // 
            // uiCommandManager
            // 
            this.uiCommandManager.BottomRebar = this.BottomRebar1;
            this.uiCommandManager.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCmdMain});
            this.uiCommandManager.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdQuanLyHopDongXuatKhau,
            this.cmdQuanLyChungTu,
            this.cmdTaoCTTT});
            this.uiCommandManager.ContainerControl = this;
            this.uiCommandManager.Id = new System.Guid("7dc5cb1e-ad95-457a-a83e-06ae21390bc8");
            this.uiCommandManager.ImageList = this.imgSmall;
            this.uiCommandManager.LeftRebar = this.LeftRebar1;
            this.uiCommandManager.RightRebar = this.RightRebar1;
            this.uiCommandManager.Tag = null;
            this.uiCommandManager.TopRebar = this.TopRebar1;
            this.uiCommandManager.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandManager_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.uiCommandManager;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 396);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(911, 0);
            // 
            // uiCmdMain
            // 
            this.uiCmdMain.CommandManager = this.uiCommandManager;
            this.uiCmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdQuanLyHopDongXuatKhau1,
            this.cmdQuanLyChungTu1,
            this.cmdTaoCTTT1});
            this.uiCmdMain.Key = "uiCmdMain";
            this.uiCmdMain.Location = new System.Drawing.Point(0, 0);
            this.uiCmdMain.Name = "uiCmdMain";
            this.uiCmdMain.RowIndex = 0;
            this.uiCmdMain.Size = new System.Drawing.Size(584, 28);
            this.uiCmdMain.Text = "CommandBar1";
            // 
            // cmdQuanLyHopDongXuatKhau1
            // 
            this.cmdQuanLyHopDongXuatKhau1.Key = "cmdQuanLyHopDongXuatKhau";
            this.cmdQuanLyHopDongXuatKhau1.Name = "cmdQuanLyHopDongXuatKhau1";
            // 
            // cmdQuanLyChungTu1
            // 
            this.cmdQuanLyChungTu1.Key = "cmdQuanLyChungTu";
            this.cmdQuanLyChungTu1.Name = "cmdQuanLyChungTu1";
            // 
            // cmdTaoCTTT1
            // 
            this.cmdTaoCTTT1.Key = "cmdTaoCTTT";
            this.cmdTaoCTTT1.Name = "cmdTaoCTTT1";
            // 
            // cmdQuanLyHopDongXuatKhau
            // 
            this.cmdQuanLyHopDongXuatKhau.ImageIndex = 0;
            this.cmdQuanLyHopDongXuatKhau.Key = "cmdQuanLyHopDongXuatKhau";
            this.cmdQuanLyHopDongXuatKhau.Name = "cmdQuanLyHopDongXuatKhau";
            this.cmdQuanLyHopDongXuatKhau.Text = "Quản lý hợp đồng xuất khẩu";
            // 
            // cmdQuanLyChungTu
            // 
            this.cmdQuanLyChungTu.ImageIndex = 0;
            this.cmdQuanLyChungTu.Key = "cmdQuanLyChungTu";
            this.cmdQuanLyChungTu.Name = "cmdQuanLyChungTu";
            this.cmdQuanLyChungTu.Text = "Chi tiết chứng từ thanh toán";
            // 
            // cmdTaoCTTT
            // 
            this.cmdTaoCTTT.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTaoCTTT.Icon")));
            this.cmdTaoCTTT.Key = "cmdTaoCTTT";
            this.cmdTaoCTTT.Name = "cmdTaoCTTT";
            this.cmdTaoCTTT.Text = "Tạo mới thông tin chứng từ thanh toán";
            // 
            // imgSmall
            // 
            this.imgSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgSmall.ImageStream")));
            this.imgSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.imgSmall.Images.SetKeyName(0, "");
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.uiCommandManager;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 368);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.uiCommandManager;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(911, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 368);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCmdMain});
            this.TopRebar1.CommandManager = this.uiCommandManager;
            this.TopRebar1.Controls.Add(this.uiCmdMain);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(919, 28);
            // 
            // ChungTuThanhToanForm
            // 
            this.ClientSize = new System.Drawing.Size(919, 397);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChungTuThanhToanForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Bảng tổng hợp chứng từ thanh toán";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ChungTuThanhToanForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnSave;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.UI.CommandBars.UICommandManager uiCommandManager;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCmdMain;
        private Janus.Windows.UI.CommandBars.UICommand cmdQuanLyHopDongXuatKhau1;
        private Janus.Windows.UI.CommandBars.UICommand cmdQuanLyChungTu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdQuanLyHopDongXuatKhau;
        private Janus.Windows.UI.CommandBars.UICommand cmdQuanLyChungTu;
        private System.Windows.Forms.ImageList imgSmall;
        private Janus.Windows.UI.CommandBars.UICommand cmdTaoCTTT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTaoCTTT;
    }
}
