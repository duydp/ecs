﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class VNACC_HangMauDichNhapForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public KDT_VNACC_HangMauDich HMD = null;
        private KDT_VNACC_HangMauDich_ThueThuKhac ThuThuKhac = null;
        DataTable dt = null;


        public VNACC_HangMauDichNhapForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.IDA.ToString());
        }

        private void LoadData()
        {
            grList.DataSource = TKMD.HangCollection;
            //khoitao_DuLieuChuan();

        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {

                bool isAddNew = false;
                if (HMD == null)
                {
                    HMD = new KDT_VNACC_HangMauDich();
                    isAddNew = true;
                }
                GetHangMD();
                if (isAddNew)
                    TKMD.HangCollection.Add(HMD);
                grList.DataSource = TKMD.HangCollection;
                grList.Refetch();
                HMD = new KDT_VNACC_HangMauDich();
                SetHangMD();
                HMD = null;
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi khi lưu hàng: " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void GetHangMD()
        {
            HMD.MaSoHang = ctrMaSoHang.Code;
            HMD.MaQuanLy = txtMaQuanLy.Text;
            HMD.TenHang = txtTenHang.Text;
            HMD.ThueSuat = Convert.ToDecimal(txtThueSuat.Value);
            HMD.ThueSuatTuyetDoi = Convert.ToDecimal(txtThueSuatTuyetDoi.Value);
            HMD.MaDVTTuyetDoi = HMD.ThueSuatTuyetDoi > 0 ? txtMaDVTTuyetDoi.Text : string.Empty;
            HMD.MaTTTuyetDoi = HMD.ThueSuatTuyetDoi > 0 ? ctrMaTTTuyetDoi.Code : string.Empty;
            HMD.MaMienGiamThue = txtMaMienGiam.Text;
            HMD.SoTienGiamThue = Convert.ToDecimal(txtSoTienMienGiam.Value);
            HMD.SoLuong1 = Convert.ToDecimal(txtSoLuong1.Value);
            HMD.DVTLuong1 = ctrDVTLuong1.Code;
            HMD.SoLuong2 = Convert.ToDecimal(txtSoLuong2.Value);
            HMD.DVTLuong2 = ctrDVTLuong2.Code;
            HMD.TriGiaHoaDon = Convert.ToDecimal(txtTriGiaHoaDon.Value);
            HMD.DonGiaHoaDon = Convert.ToDecimal(txtDonGiaHoaDon.Value);
            HMD.DVTDonGia = txtDVTDonGia.Text;
            HMD.MaTTDonGia = ctrMaTTDonGia.Code;
            HMD.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Value);
            HMD.MaTTTriGiaTinhThue = ctrMaTTTriGiaTinhThue.Code;
            HMD.SoTTDongHangTKTNTX = Convert.ToDecimal(txtSoTTDongHangTKTNTX.Value) > 0 ? txtSoTTDongHangTKTNTX.Text : string.Empty;

            HMD.NuocXuatXu = ctrNuocXuatXu.Code;
            HMD.MaThueNKTheoLuong = txtMaMucThueTuyetDoi.Text;
            HMD.MaBieuThueNK = txtMaBieuThueNK.Text;
            HMD.MaHanNgach = txtMaHanNgach.Text;
            HMD.SoDMMienThue = Convert.ToDecimal(txtSoDMMienThue.Value);
            HMD.SoDongDMMienThue = txtSoDongDMMienThue.Text;
            HMD.SoMucKhaiKhoanDC = txtSoMucKhaiKhoanDC1.Text + txtSoMucKhaiKhoanDC2.Text + txtSoMucKhaiKhoanDC3.Text + txtSoMucKhaiKhoanDC4.Text + txtSoMucKhaiKhoanDC5.Text;
            //So cua muc khaon dieu chinh

            // thue va thu khac
            GetThueThuKhac();





        }

        private void SetHangMD()
        {
            ctrMaSoHang.Code = HMD.MaSoHang;
            txtMaQuanLy.Text = HMD.MaQuanLy;
            txtTenHang.Text = HMD.TenHang;
            txtThueSuat.Text = HMD.ThueSuat.ToString();
            txtThueSuatTuyetDoi.Text = HMD.ThueSuatTuyetDoi.ToString();
            txtMaDVTTuyetDoi.Text = HMD.MaDVTTuyetDoi;
            ctrMaTTTuyetDoi.Code = HMD.MaTTTuyetDoi;
            txtMaMienGiam.Text = HMD.MaMienGiamThue;
            txtSoTienMienGiam.Text = HMD.SoTienGiamThue.ToString();
            txtSoLuong1.Text = HMD.SoLuong1.ToString();
            ctrDVTLuong1.Code = HMD.DVTLuong1;
            txtSoLuong2.Text = HMD.SoLuong2.ToString();
            ctrDVTLuong2.Code = HMD.DVTLuong2;
            txtTriGiaHoaDon.Text = HMD.TriGiaHoaDon.ToString();
            txtDonGiaHoaDon.Text = HMD.DonGiaHoaDon.ToString();
            txtDVTDonGia.Text = HMD.DVTDonGia;
            ctrMaTTDonGia.Code = HMD.MaTTDonGia;
            txtTriGiaTinhThue.Text = HMD.TriGiaTinhThue.ToString();
            ctrMaTTTriGiaTinhThue.Code = HMD.MaTTTriGiaTinhThue;
            txtSoTTDongHangTKTNTX.Text = HMD.SoTTDongHangTKTNTX;

            ctrNuocXuatXu.Code = HMD.NuocXuatXu;
            txtMaMucThueTuyetDoi.Text = HMD.MaThueNKTheoLuong;
            txtMaBieuThueNK.Text = HMD.MaBieuThueNK;
            txtMaHanNgach.Text = HMD.MaHanNgach;
            txtSoDMMienThue.Text = HMD.SoDMMienThue.ToString();
            txtSoDongDMMienThue.Text = HMD.SoDongDMMienThue;
            if (HMD.SoMucKhaiKhoanDC == null)
                HMD.SoMucKhaiKhoanDC = "00000";
            txtSoMucKhaiKhoanDC1.Text = HMD.SoMucKhaiKhoanDC.Substring(0, 1);
            txtSoMucKhaiKhoanDC2.Text = HMD.SoMucKhaiKhoanDC.Substring(1, 1);
            txtSoMucKhaiKhoanDC3.Text = HMD.SoMucKhaiKhoanDC.Substring(2, 1);
            txtSoMucKhaiKhoanDC4.Text = HMD.SoMucKhaiKhoanDC.Substring(3, 1);
            txtSoMucKhaiKhoanDC5.Text = HMD.SoMucKhaiKhoanDC.Substring(4, 1);


            txtDonGiaTinhThue.Text = HMD.DonGiaTinhThue.ToString();
            ctrDV_SL_TrongDonGiaTinhThue.Code = HMD.DV_SL_TrongDonGiaTinhThue;
            txtTriGiaTinhThueS.Text = HMD.TriGiaTinhThueS.ToString();
            txtSoLuongTinhThue.Text = HMD.SoLuongTinhThue.ToString();
            ctrMaDVTDanhThue.Code = HMD.MaDVTDanhThue;
            //thue va thu khac
            SetThueThuKhac();
        }

        private void VNACC_HangMauDichNhapForm_Load(object sender, EventArgs e)
        {
            SetIDControl();
            LoadData();

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_HangMauDich> ItemColl = new List<KDT_VNACC_HangMauDich>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_HangMauDich)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangMauDich item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKMD.HangCollection.Remove(item);
                    }

                    grList.DataSource = TKMD.HangCollection;
                    grList.Refetch();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void SetThueThuKhac()
        {
            dt = new DataTable();
            dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(HMD.ThueThuKhacCollection);
            grListThueThuKhac.DataSource = dt;
        }

        private void GetThueThuKhac()
        {
            HMD.ThueThuKhacCollection.Clear();
            foreach (DataRow row in dt.Rows)
            {
                ThuThuKhac = new KDT_VNACC_HangMauDich_ThueThuKhac();
                if (row["ID"].ToString().Length != 0)
                {
                    ThuThuKhac.ID = Convert.ToInt64(row["ID"].ToString());
                }
                ThuThuKhac.MaTSThueThuKhac = row["MaTSThueThuKhac"].ToString();
                ThuThuKhac.MaMGThueThuKhac = row["MaMGThueThuKhac"].ToString();
                ThuThuKhac.SoTienGiamThueThuKhac = Convert.ToDecimal(row["SoTienGiamThueThuKhac"].ToString());
                HMD.ThueThuKhacCollection.Add(ThuThuKhac);
            }
        }

        private void grList_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (grList.GetRows().Length < 0) return;

                HMD = (KDT_VNACC_HangMauDich)grList.CurrentRow.DataRow;
                SetHangMD();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void grListThueThuKhac_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                long ID = Convert.ToInt64(grListThueThuKhac.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa thuế và thu khác này không?", true) == "Yes")
                    {
                        KDT_VNACC_HangMauDich_ThueThuKhac.DeleteDynamic("ID=" + ID);
                        grListThueThuKhac.CurrentRow.Delete();
                    }
                }
                else
                {
                    grList.CurrentRow.Delete();
                }
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                ctrMaSoHang.TagName = "CMD"; //Mã số hàng hóa
                txtMaQuanLy.Tag = "GZC"; //Mã quản lý riêng
                txtThueSuat.Tag = "TXA"; //Thuế suất
                txtThueSuatTuyetDoi.Tag = "TXB"; //Mức thuế tuyệt đối
                txtMaDVTTuyetDoi.Tag = "TXC"; //Mã đơn vị tính thuế tuyệt đối
                ctrMaTTTuyetDoi.TagName = "TXD"; //Mã đồng tiền của mức thuế tuyệt đối
                txtTenHang.Tag = "CMN"; //Mô tả hàng hóa
                ctrNuocXuatXu.TagCode = "OR"; //Mã nước xuất xứ
                txtMaBieuThueNK.Tag = "ORS"; //Mã biểu thuế nhập khẩu 
                txtMaHanNgach.Tag = "KWS"; //Mã ngoài hạn ngạch
                //txtMaThueNKTheoLuong.Tag = "SPD"; //Mã xác định mức thuế nhập khẩu theo lượng
                txtSoLuong1.Tag = "QN1"; //Số lượng (1)
                ctrDVTLuong1.TagName = "QT1"; //Mã đơn vị tính (1)
                txtSoLuong2.Tag = "QN2"; //Số lượng (2)
                ctrDVTLuong2.TagName = "QT2"; //Mã đơn vị tính (2)
                txtTriGiaHoaDon.Tag = "BPR"; //Trị giá hóa đơn
                txtDonGiaHoaDon.Tag = "UPR"; //Đơn giá hóa đơn
                ctrMaTTDonGia.TagName = "UPC"; //Mã đồng tiền của đơn giá hóa đơn
                txtDVTDonGia.Tag = "TSC"; //Đơn vị của đơn giá hóa đơn và số lượng
                ctrMaTTTriGiaTinhThue.TagName = "BPC"; //Mã đồng tiền trị giá tính thuế
                txtTriGiaTinhThue.Tag = "DPR"; //Trị giá tính thuế
                txtSoMucKhaiKhoanDC1.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoMucKhaiKhoanDC2.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoMucKhaiKhoanDC3.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoMucKhaiKhoanDC4.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoMucKhaiKhoanDC5.Tag = "VN_"; //Số của mục khai khoản điều chỉnh
                txtSoTTDongHangTKTNTX.Tag = "TDL"; //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
                txtSoDMMienThue.Tag = "TXN"; //Số đăng ký danh mục miễn thuế nhập khẩu
                txtSoDongDMMienThue.Tag = "TXR"; //Số dòng tương ứng trong danh mục miễn thuế nhập khẩu
                txtMaMienGiam.Tag = "RE"; //Mã miễn / Giảm / Không chịu thuế nhập khẩu
                txtSoTienMienGiam.Tag = "REG"; //Số tiền giảm thuế nhập khẩu
                grListThueThuKhac.Tag = "TX_";
                //txtMaTSThueThuKhac1.Tag = "TX_"; //Mã áp dụng thuế suất / Mức thuế và thu khác
                //txtMaMGThueThuKhac1.Tag = "TR_"; //Mã miễn / Giảm / Không chịu thuế và thu khác
                //txtSoTienMienGiamThuKhac1.Tag = "TG_"; //Số tiền giảm thuế và thu khác


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }


    }
}
