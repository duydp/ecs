﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.SXXK
{
  public partial  class LogHistory
    {
        public KDT_SXXK_LogKhaiBao logKhaiBao = new KDT_SXXK_LogKhaiBao();
        public KDT_SXXK_DinhMuc_Log log = new KDT_SXXK_DinhMuc_Log();
        public KDT_SXXK_SanPham_Log SanPhamLog = new KDT_SXXK_SanPham_Log();
        public KDT_SXXK_NguyenPhuLieu_Log NPLLog = new KDT_SXXK_NguyenPhuLieu_Log();
        #region Properties.
        public string Status { set; get; }
        public string Notes { set; get; }
        public string UserName { set; get; }
        public List<KDT_SXXK_DinhMuc_Log> DMCollection = new List<KDT_SXXK_DinhMuc_Log>();
        #endregion

        public void LogDinhMuc(DinhMucDangKy dmDangKy, String Notes, String UserName)
        {
            try
            {
                logKhaiBao = new KDT_SXXK_LogKhaiBao();
                logKhaiBao.ID_DK = dmDangKy.ID;
                logKhaiBao.GUIDSTR_DK = dmDangKy.GUIDSTR;
                logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.DinhMuc;
                logKhaiBao.UserNameKhaiBao = UserName;
                logKhaiBao.NgayKhaiBao = DateTime.Now;
                logKhaiBao.UserNameSuaDoi = UserName;
                logKhaiBao.NgaySuaDoi = DateTime.Now;
                logKhaiBao.GhiChu = Notes;
                foreach (DinhMuc item in dmDangKy.DMCollection)
                {
                    log = new KDT_SXXK_DinhMuc_Log();
                    log.MaSanPham = item.MaSanPham;
                    log.MaNguyenPhuLieu = item.MaNguyenPhuLieu;
                    log.DVT_ID = item.DVT_ID;
                    log.DinhMucSuDung = item.DinhMucSuDung;
                    log.TyLeHaoHut = item.TyLeHaoHut;
                    log.GhiChu = item.GhiChu;
                    log.STTHang = item.STTHang;
                    log.IsFromVietNam = item.IsFromVietNam;
                    log.MaDinhDanhLenhSX = item.MaDinhDanhLenhSX;
                    log.DateLog = DateTime.Now;
                    log.Status = Notes;
                    logKhaiBao.DMCollection.Add(log);
                }
                logKhaiBao.InsertUpdateFull();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void LogDinhMuc(DinhMucDangKy dmDangKy, DinhMucCollection DMCollectionLog, String Notes, String UserName)
        {
            try
            {
                logKhaiBao = new KDT_SXXK_LogKhaiBao();
                logKhaiBao.ID_DK = dmDangKy.ID;
                logKhaiBao.GUIDSTR_DK = dmDangKy.GUIDSTR;
                logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.DinhMuc;
                logKhaiBao.UserNameKhaiBao = UserName;
                logKhaiBao.NgayKhaiBao = DateTime.Now;
                logKhaiBao.UserNameSuaDoi = UserName;
                logKhaiBao.NgaySuaDoi = DateTime.Now;
                logKhaiBao.GhiChu = Notes;
                foreach (DinhMuc item in DMCollectionLog)
                {
                    log = new KDT_SXXK_DinhMuc_Log();
                    log.MaSanPham = item.MaSanPham;
                    log.MaNguyenPhuLieu = item.MaNguyenPhuLieu;
                    log.DVT_ID = item.DVT_ID;
                    log.DinhMucSuDung = item.DinhMucSuDung;
                    log.TyLeHaoHut = item.TyLeHaoHut;
                    log.GhiChu = item.GhiChu;
                    log.STTHang = item.STTHang;
                    log.IsFromVietNam = item.IsFromVietNam;
                    log.MaDinhDanhLenhSX = item.MaDinhDanhLenhSX;
                    log.DateLog = DateTime.Now;
                    log.Status = Notes;
                    logKhaiBao.DMCollection.Add(log);
                }
                logKhaiBao.InsertUpdateFull();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public void LogDinhMucTT(KDT_SXXK_DinhMucThucTeDangKy dmDangKy, String Notes, String UserName)
        {
            try
            {
                logKhaiBao = new KDT_SXXK_LogKhaiBao();
                logKhaiBao.ID_DK = dmDangKy.ID;
                logKhaiBao.GUIDSTR_DK = dmDangKy.GuidString;
                logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.DinhMuc;
                logKhaiBao.UserNameKhaiBao = UserName;
                logKhaiBao.NgayKhaiBao = DateTime.Now;
                logKhaiBao.UserNameSuaDoi = UserName;
                logKhaiBao.NgaySuaDoi = DateTime.Now;
                logKhaiBao.GhiChu = Notes;
                //foreach (DinhMuc item in dmDangKy.DMCollection)
                //{
                //    log = new KDT_SXXK_DinhMuc_Log();
                //    log.MaSanPham = item.MaSanPham;
                //    log.MaNguyenPhuLieu = item.MaNguyenPhuLieu;
                //    log.DVT_ID = item.DVT_ID;
                //    log.DinhMucSuDung = item.DinhMucSuDung;
                //    log.TyLeHaoHut = item.TyLeHaoHut;
                //    log.GhiChu = item.GhiChu;
                //    log.STTHang = item.STTHang;
                //    log.IsFromVietNam = item.IsFromVietNam;
                //    log.MaDinhDanhLenhSX = item.MaDinhDanhLenhSX;
                //    log.DateLog = DateTime.Now;
                //    log.Status = Notes;
                //    logKhaiBao.DMCollection.Add(log);
                //}
                logKhaiBao.InsertUpdateFull();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void LogDinhMucTT(KDT_SXXK_DinhMucThucTeDangKy dmDangKy, DinhMucCollection DMCollectionLog, String Notes, String UserName)
        {
            try
            {
                logKhaiBao = new KDT_SXXK_LogKhaiBao();
                logKhaiBao.ID_DK = dmDangKy.ID;
                logKhaiBao.GUIDSTR_DK = dmDangKy.GuidString;
                logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.DinhMuc;
                logKhaiBao.UserNameKhaiBao = UserName;
                logKhaiBao.NgayKhaiBao = DateTime.Now;
                logKhaiBao.UserNameSuaDoi = UserName;
                logKhaiBao.NgaySuaDoi = DateTime.Now;
                logKhaiBao.GhiChu = Notes;
                //foreach (DinhMuc item in DMCollectionLog)
                //{
                //    log = new KDT_SXXK_DinhMuc_Log();
                //    log.MaSanPham = item.MaSanPham;
                //    log.MaNguyenPhuLieu = item.MaNguyenPhuLieu;
                //    log.DVT_ID = item.DVT_ID;
                //    log.DinhMucSuDung = item.DinhMucSuDung;
                //    log.TyLeHaoHut = item.TyLeHaoHut;
                //    log.GhiChu = item.GhiChu;
                //    log.STTHang = item.STTHang;
                //    log.IsFromVietNam = item.IsFromVietNam;
                //    log.MaDinhDanhLenhSX = item.MaDinhDanhLenhSX;
                //    log.DateLog = DateTime.Now;
                //    log.Status = Notes;
                //    logKhaiBao.DMCollection.Add(log);
                //}
                logKhaiBao.InsertUpdateFull();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void LogNguyenPhuLieu(NguyenPhuLieuDangKy nplDangKy, String Notes, String UserName)
        {
            try
            {
                logKhaiBao = new KDT_SXXK_LogKhaiBao();
                logKhaiBao.ID_DK = nplDangKy.ID;
                logKhaiBao.GUIDSTR_DK = nplDangKy.GUIDSTR;
                logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.NguyenPhuLieu;
                logKhaiBao.UserNameKhaiBao = UserName;
                logKhaiBao.NgayKhaiBao = DateTime.Now;
                logKhaiBao.UserNameSuaDoi = UserName;
                logKhaiBao.NgaySuaDoi = DateTime.Now;
                logKhaiBao.GhiChu = Notes;
                foreach (NguyenPhuLieu item in nplDangKy.NPLCollection)
                {
                    NPLLog = new KDT_SXXK_NguyenPhuLieu_Log();
                    NPLLog.Ma = item.Ma;
                    NPLLog.Ten = item.Ten;
                    NPLLog.MaHS = item.MaHS;
                    NPLLog.DVT_ID = item.DVT_ID;
                    NPLLog.STTHang = item.STTHang;
                    NPLLog.DateLog = DateTime.Now;
                    NPLLog.Status = Notes;
                    logKhaiBao.NPLCollection.Add(NPLLog);
                }
                logKhaiBao.InsertUpdateFullNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void LogNguyenPhuLieu(NguyenPhuLieuDangKy nplDangKy, NguyenPhuLieuCollection NPLCollectionLog, String Notes, String UserName)
        {
            try
            {
                logKhaiBao = new KDT_SXXK_LogKhaiBao();
                logKhaiBao.ID_DK = nplDangKy.ID;
                logKhaiBao.GUIDSTR_DK = nplDangKy.GUIDSTR;
                logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.NguyenPhuLieu;
                logKhaiBao.UserNameKhaiBao = UserName;
                logKhaiBao.NgayKhaiBao = DateTime.Now;
                logKhaiBao.UserNameSuaDoi = UserName;
                logKhaiBao.NgaySuaDoi = DateTime.Now;
                logKhaiBao.GhiChu = Notes;
                foreach (NguyenPhuLieu item in NPLCollectionLog)
                {
                    NPLLog = new KDT_SXXK_NguyenPhuLieu_Log();
                    NPLLog.Ma = item.Ma;
                    NPLLog.Ten = item.Ten;
                    NPLLog.MaHS = item.MaHS;
                    NPLLog.DVT_ID = item.DVT_ID;
                    NPLLog.STTHang = item.STTHang;
                    NPLLog.DateLog = DateTime.Now;
                    NPLLog.Status = Notes;
                    logKhaiBao.NPLCollection.Add(NPLLog);
                }
                logKhaiBao.InsertUpdateFullNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public void LogSanPham(SanPhamDangKy spDangKy, String Notes, String UserName)
        {
            try
            {
                logKhaiBao = new KDT_SXXK_LogKhaiBao();
                logKhaiBao.ID_DK = spDangKy.ID;
                logKhaiBao.GUIDSTR_DK = spDangKy.GUIDSTR;
                logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.SanPham;
                logKhaiBao.UserNameKhaiBao = UserName;
                logKhaiBao.NgayKhaiBao = DateTime.Now;
                logKhaiBao.UserNameSuaDoi = UserName;
                logKhaiBao.NgaySuaDoi = DateTime.Now;
                logKhaiBao.GhiChu = Notes;
                foreach (SanPham item in spDangKy.SPCollection)
                {
                    SanPhamLog = new KDT_SXXK_SanPham_Log();
                    SanPhamLog.Ma = item.Ma;
                    SanPhamLog.Ten = item.Ten;
                    SanPhamLog.MaHS = item.MaHS;
                    SanPhamLog.DVT_ID = item.DVT_ID;
                    SanPhamLog.STTHang = item.STTHang;
                    SanPhamLog.DateLog = DateTime.Now;
                    SanPhamLog.Status = Notes;
                    logKhaiBao.SanPhamCollection.Add(SanPhamLog);
                }
                logKhaiBao.InsertUpdateFullSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void LogSanPham(SanPhamDangKy spDangKy, SanPhamCollection SPCollectionLog, String Notes, String UserName)
        {
            try
            {
                logKhaiBao = new KDT_SXXK_LogKhaiBao();
                logKhaiBao.ID_DK = spDangKy.ID;
                logKhaiBao.GUIDSTR_DK = spDangKy.GUIDSTR;
                logKhaiBao.LoaiKhaiBao = LoaiKhaiBao.SanPham;
                logKhaiBao.UserNameKhaiBao = UserName;
                logKhaiBao.NgayKhaiBao = DateTime.Now;
                logKhaiBao.UserNameSuaDoi = UserName;
                logKhaiBao.NgaySuaDoi = DateTime.Now;
                logKhaiBao.GhiChu = Notes;
                foreach (SanPham item in SPCollectionLog)
                {
                    SanPhamLog = new KDT_SXXK_SanPham_Log();
                    SanPhamLog.Ma = item.Ma;
                    SanPhamLog.Ten = item.Ten;
                    SanPhamLog.MaHS = item.MaHS;
                    SanPhamLog.DVT_ID = item.DVT_ID;
                    SanPhamLog.STTHang = item.STTHang;
                    SanPhamLog.DateLog = DateTime.Now;
                    SanPhamLog.Status = Notes;
                    logKhaiBao.SanPhamCollection.Add(SanPhamLog);
                }
                logKhaiBao.InsertUpdateFullSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
