﻿using System;
using System.Data;
using Company.GC.BLL.Utils;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.Components.Utils;
using System.Collections.Generic;

namespace Company.GC.BLL.GC
{
    public partial class NguyenPhuLieu
    {
        //public static DataSet WS_GetDanhSachDaDangKy(string maHaiQuan, string maDoanhNghiep)
        //{
        //    GCService service = new GCService();
        //    return service.NguyenPhuLieu_GetDanhSach(maHaiQuan, maDoanhNghiep);
        //}

        //public static bool UpdateRegistedToDatabase(string maHaiQuan, string maDoanhNghiep, SqlTransaction transaction)
        //{
        //    // Lấy danh sách từ WEB SERVICE.
        //    DataSet ds = WS_GetDanhSachDaDangKy(maHaiQuan, maDoanhNghiep);

        //    // Cập nhật vào CSDL.
        //    foreach (DataRow row in ds.Tables[0].Rows)
        //    {
        //        NguyenPhuLieu npl = new NguyenPhuLieu();
        //        npl.SoHopDong = row["SoHopDong"].ToString();
        //        npl.MaHaiQuan = row["MaHaiQuan"].ToString();
        //        npl.MaDoanhNghiep = row["MaDoanhNghiep"].ToString();
        //        npl.NgayKy = Convert.ToDateTime(row["NgayKy"]);
        //        npl.Ma = row["Ma"].ToString().Substring(1);
        //        npl.Ten = FontConverter.TCVN2Unicode(row["Ten"].ToString());
        //        npl.MaHS = row["MaHS"].ToString();
        //        npl.DVT_ID = row["DVT_ID"].ToString();
        //        npl.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
        //        npl.InsertUpdateTransaction(transaction);
        //    }

        //    return true;
        //}

        //Load dữ liệu :
        //public List<NguyenPhuLieu> GetNPLByHopDong()
        //    {

        //    }

        //ThoiLV Edit :
        public bool UpdateRegistedToDatabaseNPLGG(string maHaiQuanHD, string maDoanhNghiep, DataSet ds)
        {
            NguyenPhuLieuCollection nplCollection = new NguyenPhuLieuCollection();
            // Cập nhật vào CSDL.
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();


                //npl.NgayKy = Convert.ToDateTime(row["NgayKy"]);
                npl.Ma = row["Ma"].ToString().Substring(1);
                npl.Ten = FontConverter.TCVN2Unicode(row["Ten"].ToString());
                npl.MaHS = row["MaHS"].ToString();
                npl.DVT_ID = row["DVT_ID"].ToString();
                try
                {
                    npl.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
                }
                catch { }
                try
                {
                    npl.SoLuongCungUng = Convert.ToDecimal(row["SoLuongCungUng"]);
                }
                catch { }
                try
                {
                    npl.SoLuongDaDung = Convert.ToDecimal(row["SoLuongDaDung"]);
                }
                catch { }
                try
                {
                    npl.SoLuongDaNhap = Convert.ToDecimal(row["SoLuongDaNhap"]);
                }
                catch { }
                npl.TrangThai = 1;

                nplCollection.Add(npl);
            }

            return this.InsertUpdate(nplCollection);
        }
        public bool Load(SqlTransaction transaction)
        {
            string spName = "p_GC_NguyenPhuLieu_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) this._SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) this._SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) this._SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public DataSet BaoCaoTT74(long idHD, string MaHaiQuan, string MaDoanhNghiep)
        {
            string spName = "p_GC_BC05TT74";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public DataSet BaoCaoBC03HSTK_GC_TT117(long idHD, string MaHaiQuan, string MaDoanhNghiep)
        {
            int maxRow = 0;
            string spName = "p_GC_BC03HSTK_GC_TT117";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            spName = "p_GC_BC03HSTK_GC_TT117_Tong";
            DataSet ds = this.db.ExecuteDataSet(dbCommand);
            dbCommand.CommandText = spName;

            DataSet ds1 = this.db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = "Trang 1";
            ds1.Tables[0].TableName = "Trang 2";
            DataTable dt = ds1.Tables[0].Clone();
            maxRow = ds.Tables[0].Rows.Count;
            dt = ds1.Tables[0].Copy();
            foreach (DataRow item in dt.Rows)
            {
                item["Stt"] = int.Parse(item["Stt"].ToString()) + maxRow;
            }
            ds.Tables.Add(dt);
            return ds;
        }

        public DataSet BaoCaoBC01HSTK_GC_TT117(long idHD, string MaHaiQuan, string MaDoanhNghiep)
        {
            string spName = "p_GC_BC01HSTK_GC_TT117";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, idHD);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            spName = "p_GC_BC01HSTK_GC_TT117_Tong";
            DataSet ds = this.db.ExecuteDataSet(dbCommand);
            dbCommand.CommandText = spName;

            DataSet ds1 = this.db.ExecuteDataSet(dbCommand);
            ds.Tables[0].TableName = "Trang 1";
            ds1.Tables[0].TableName = "Trang 2";
            DataTable dt = ds1.Tables[0].Clone();
            dt = ds1.Tables[0].Copy();
            ds.Tables.Add(dt);
            return ds;
        }
        public static DataSet GetLuongNguyenPhuLieuTheoDM(string maSP, decimal soluong, long HopDong_ID)
        {
            string sql = "SELECT MaSanPham,MaNguyenPhuLieu,dinhMucChung,(dinhMucChung * @SoLuong) as LuongCanDung " +
                         "from   t_View_DinhMuc " +
                         "where MaSanPham=@MaSanPham and HopDong_ID=@HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, soluong);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public static DataSet GetLuongNguyenPhuLieuTheoDM(string maSP, decimal soluong, long HopDong_ID, int SoTPNPL)
        {
            string sql = "SELECT MaSanPham,MaNguyenPhuLieu,dinhMucChung,round((dinhMucChung * @SoLuong),@SoTPDM) as LuongCanDung " +
                         "from   t_View_DinhMuc " +
                         "where MaSanPham=@MaSanPham and HopDong_ID=@HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, soluong);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@SoTPDM", SqlDbType.Int, SoTPNPL);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public static DataSet GetLuongNguyenPhuLieuTheoDM(string maSP, decimal soluong, long HopDong_ID, int SoTPNPL, SqlTransaction trans, string dbName)
        {
            string sql = "SELECT MaSanPham,MaNguyenPhuLieu,dinhMucChung,round((dinhMucChung * @SoLuong),@SoTPDM) as LuongCanDung " +
                         "from   t_View_DinhMuc " +
                         "where MaSanPham=@MaSanPham and HopDong_ID=@HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(dbName);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, soluong);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@SoTPDM", SqlDbType.Int, SoTPNPL);
            DataSet ds = new DataSet();
            if (trans != null)
                ds = db.ExecuteDataSet(dbCommand, trans);
            else
                ds = db.ExecuteDataSet(dbCommand);

            return ds;
        }
        public static DataSet GetLuongNguyenPhuLieuTheoDM(long TKMD_ID)
        {
            string select = "SELECT * ";
            string from = "from   t_View_CanDoiXuatNhap ";
            string where = "where TKMD_ID=@TKMD_ID";
            string sql = select + from + where;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public static DataSet GetLuongNguyenPhuLieuTheoDM(string maSP, decimal soluong, long HopDong_ID, SqlTransaction trans)
        {
            string sql = "SELECT MaSanPham,MaNguyenPhuLieu,(dinhMucChung * @SoLuong) as LuongCanDung " +
                         "from   t_View_DinhMuc " +
                         "where MaSanPham=@MaSanPham and HopDong_ID=@HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, soluong);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand, trans);
            return ds;
        }

        public static DataSet GetLuongNguyenPhuLieuTon(long HopDong_ID)
        {
            string sql = "SELECT MaNguyenPhuLieu,LuongTon " +
                         "from   t_View_NguyenPhuLieuTon " +
                         "where  HopDong_ID=@HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        //

        public static DataSet GetDanhSachNPLCU(long HopDong_ID)
        {

            string sql = " SELECT  Sum(v.DonGia*v.LuongCung)/sum(LuongCung) as GiaTB, v.MaNguyenPhuLieu, t.Ten, v.HinhThuCungUng, t.DVT_ID,t.Ma  "
                        + " FROM t_View_KDT_NPLCungUng v INNER JOIN (SELECT * FROM  t_GC_NguyenPhuLieu WHERE HopDong_ID = @HopDong_ID) t "
                        + " ON v.MaNguyenPhuLieu = t.Ma  "
                        + " WHERE ( v.TKMD_ID in (select ID from dbo.t_KDT_ToKhaiMauDich where IDHopDong = @HopDong_ID)  "
                        + " OR v.TKCT_ID in (select ID from dbo.t_KDT_GC_ToKhaiChuyenTiep where IDHopDong = @HopDong_ID) ) "
                        + " Group by v.MaNguyenPhuLieu, t.Ten , t.DVT_ID  , v.HinhThuCungUng , t.Ma, t.HopDong_ID ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet GetLuongNPLCU(long HopDong_ID)
        {
            //string sql = " SELECT * " +
            //             " from   t_GC_NguyenPhuLieu " +
            //             " where  HopDong_ID=@HopDong_ID ";
            string sql = " SELECT   Sum(v.DonGia*v.LuongCung)/sum(LuongCung) as GiaTB,t.Ma,t.HopDong_ID , "
                        + " sum(LuongCung) as   TongLuongCung, v.MaNguyenPhuLieu, t.Ten, v.HinhThuCungUng, t.DVT_ID  "
                        + " FROM t_View_KDT_NPLCungUng v INNER JOIN (SELECT * FROM  t_GC_NguyenPhuLieu WHERE HopDong_ID = @HopDong_ID) t "
                        + " ON v.MaNguyenPhuLieu = t.Ma  "
                        + " WHERE v.TKMD_ID in (select ID from dbo.t_KDT_ToKhaiMauDich where IDHopDong = @HopDong_ID)  "
                        + " OR v.TKCT_ID in (select ID from dbo.t_KDT_GC_ToKhaiChuyenTiep where IDHopDong = @HopDong_ID) "
                        + " Group by v.MaNguyenPhuLieu, t.Ten , t.DVT_ID  , v.HinhThuCungUng , t.Ma, t.HopDong_ID ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public static decimal GetLuongNPLCUBySPAndNPL(long HopDong_ID, string maNPL, string maSP)
        {

            string sql = " SELECT  sum(LuongCung) as   TongLuongCung "
                        + " FROM t_View_KDT_NPLCungUng v INNER JOIN (SELECT * FROM  t_GC_NguyenPhuLieu WHERE HopDong_ID = @HopDong_ID) t "
                        + " ON v.MaNguyenPhuLieu = t.Ma  "
                        + " WHERE ( v.TKMD_ID in (select ID from dbo.t_KDT_ToKhaiMauDich where IDHopDong = @HopDong_ID)  "
                        + " OR v.TKCT_ID in (select ID from dbo.t_KDT_GC_ToKhaiChuyenTiep where IDHopDong = @HopDong_ID) )  AND v.MaNguyenPhuLieu =@MaNguyenPhuLieu AND v.MaSanPham =@MaSanPham "
                        + " Group by v.MaNguyenPhuLieu ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, maNPL);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
            IDataReader ireader = db.ExecuteReader(dbCommand);
            decimal luongcung = 0;
            try
            {
                if (ireader.Read())
                {

                    luongcung = ireader.GetDecimal(0);
                }

            }
            catch { }
            return luongcung;
        }
        //public static decimal  GetLuongNPLCUBySPAndNPL(long HopDong_ID, string maNPL, string maSP)
        //{

        //    string sql = " SELECT   v.LuongCung)/sum(LuongCung) as GiaTB,t.Ma,t.HopDong_ID , "
        //                + " sum(LuongCung) as   TongLuongCung, v.MaNguyenPhuLieu, t.Ten, v.HinhThuCungUng, t.DVT_ID  "
        //                + " FROM t_View_KDT_NPLCungUng v INNER JOIN (SELECT * FROM  t_GC_NguyenPhuLieu WHERE HopDong_ID = @HopDong_ID) t "
        //                + " ON v.MaNguyenPhuLieu = t.Ma  "
        //                + " WHERE ( v.TKMD_ID in (select ID from dbo.t_KDT_ToKhaiMauDich where IDHopDong = @HopDong_ID)  "
        //                + " OR v.TKCT_ID in (select ID from dbo.t_KDT_GC_ToKhaiChuyenTiep where IDHopDong = @HopDong_ID) )  AND v.MaNguyenPhuLieu =@MaNguyenPhuLieu AND v.MaSanPham =@MaSanPham "
        //                + " Group by v.MaNguyenPhuLieu, t.Ten , t.DVT_ID  , v.HinhThuCungUng , t.Ma, t.HopDong_ID ";
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

        //    db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
        //    db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, maNPL);
        //    db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);
        //    DataSet ds = new DataSet();
        //    ds = db.ExecuteDataSet(dbCommand);
        //    return ds;
        //}
        public static decimal SelectLuongMuaVNByMaNPLAndSoHopDong(Company.GC.BLL.KDT.GC.HopDong HD, string MaNPL)
        {
            string sql = "select Mua_VN from DNPLHD where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD and P_Code=@P_Code";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("SLXNK");
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            db.AddInParameter(dbCommand, "@P_Code", SqlDbType.VarChar, "N" + MaNPL);
            object o = db.ExecuteScalar(dbCommand);
            if (o != null)
                return Convert.ToDecimal(o);
            return 0;
        }
        public static decimal[] SelectLuongMuaVNAndDonGiaByMaNPLAndSoHopDong(Company.GC.BLL.KDT.GC.HopDong HD, string MaNPL)
        {
            string sql = "select Mua_VN,DonGia from DNPLHD where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD and P_Code=@P_Code";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("SLXNK");
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            db.AddInParameter(dbCommand, "@P_Code", SqlDbType.VarChar, "N" + MaNPL);
            IDataReader read = db.ExecuteReader(dbCommand);
            decimal[] array = new decimal[2];
            if (read.Read())
            {
                if (!read.IsDBNull(read.GetOrdinal("Mua_VN")))
                    array[0] = read.GetDecimal(0);
                if (!read.IsDBNull(read.GetOrdinal("DonGia")))
                    array[1] = read.GetDecimal(1);
            }
            else
                array = null;
            read.Close();
            return array;
        }

        public List<NguyenPhuLieu> SelectCollectionDaDuyetBy_HopDong_ID()
        {
            string sql = "select * from t_GC_NguyenPhuLieu where trangthai=0 and HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            List<NguyenPhuLieu> collection = new List<NguyenPhuLieu>();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public List<NguyenPhuLieu> SelectCollectionDaDuyetAndBoSungBy_HopDong_ID()
        {
            string sql = "select * from t_GC_NguyenPhuLieu where trangthai=0 and HopDong_ID=@HopDong_ID or  trangthai=2 and HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            List<NguyenPhuLieu> collection = new List<NguyenPhuLieu>();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public List<NguyenPhuLieu> DisplayNPLNotExistInTK(long idHD)
        {
            string sql = "select * from t_GC_NguyenPhuLieu " +
                         "where Ma not in (select MaPhu from t_KDT_HangMauDich " +
                         "where TKMD_ID in (select ID from t_KDT_ToKhaiMauDich where SoHopDong = (select SoHopDong from t_KDT_GC_HopDong " +
                         "where ID = " + idHD + ") and trangthaixuly = 1)) and HopDong_ID = " + idHD;
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, idHD);

            List<NguyenPhuLieu> collection = new List<NguyenPhuLieu>();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        // Get so hang TK :
        public DataSet GetNPLTK(long IDHopDong)
        {
            string sql = " SELECT * "
          + " FROM t_GC_NguyenPhuLieu  "
          + " Where HopDong_ID = @HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetNguyenPhuLieuDangKy(long IDHopDong)
        {
            string sql = " SELECT * "
          + " FROM v_NguyenPhuLieu_DK  "
          + " Where HopDong_ID = @HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public string GetTenByMa(string Ma, long idHopDong)
        {
            string sql = "Select Ten from t_GC_NguyenPhuLieu where Ma = @Ma AND HopDong_ID = @HopDong_ID ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, idHopDong);
            return db.ExecuteScalar(dbCommand).ToString();
        }

        public string GetDVTByMa(string Ma, long idHopDong)
        {
            string sql = "Select DVT_ID from t_GC_NguyenPhuLieu where Ma = @Ma AND HopDong_ID = @HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, idHopDong);
            return db.ExecuteScalar(dbCommand).ToString();
        }

        public DataSet GetNPL(string Ma, long HopDong_ID)
        {
            string sql = " SELECT Ten, DVT_ID "
          + " FROM t_GC_NguyenPhuLieu  "
          + " Where Ma = @Ma and HopDong_ID = @HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetNPLTKJOINNew(long IDHopDong)
        {
            string sql = " SELECT * "
          + " FROM t_GC_NguyenPhuLieu N inner join t_KDT_ToKhaiMauDich T"
          + " On N.HopDong_ID = T.IDHopDong"
          + " Where T.MaLoaiHinh like 'N%' AND T.TrangThaiXuLy =1 AND T.IDHopDong = @HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public DataSet GetNPLDMJOIN(long IDHopDong)
        {
            string sql = " SELECT * "
          + " FROM t_GC_NguyenPhuLieu N inner join t_GC_DinhMuc D "
          + " On D.HopDong_ID = N.HopDong_ID  "
          + " AND D.MaNguyenPhuLieu =N.Ma "
          + " Where N.HopDong_ID = @HopDong_ID ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetNPLDM(long IDHopDong)
        {
            string sql = @" SELECT * 
                            FROM t_GC_NguyenPhuLieu  
                            Where HopDong_ID = @HopDong_ID ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public static DataSet GetNPLQuyDoiTheoToKhaiXuat(long IDHopDong, string MaNPL)
        {
            string sql = " SELECT * "
          + " FROM t_View_LuongNPLTrongTKXuat   "
          + " Where IDHopDong = @HopDong_ID and MaNguyenPhuLieu=@MaNguyenPhuLieu";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNPL);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public decimal GetNPLCungUngPhanBo()
        {
            string sql = " SELECT sum(CungUng) FROM ( SELECT sum(LuongPhanBo) + sum(LuongCungUng)  as CungUng FROM v_GC_PhanBo WHERE IDHopDong = @HopDong_ID AND MaNPL = @MaNPL AND MaLoaiHinhXuat!='XGC04' AND MaLoaiHinhXuat!='PHPLX' AND MaLoaiHinhXuat!='XGC18' and  (MaLoaiHinhNhap LIKE 'NSX%' OR LuongCungUng > 0)  GROUP By MaLoaiHinhNhap )a ";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            dbCommand.CommandTimeout = 10000;
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this.HopDong_ID);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this.Ma);
            decimal temp = 0;
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return temp;

        }

        //update by HUNGTQ 06/05/2011.
        public bool Load(string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_GC_NguyenPhuLieu_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) this._SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) this._SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) this._SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) this._TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public DataSet SelectDynamic1(long ID)
        {
            //string spName = "p_GC_NguyenPhuLieu_SelectDynamic";
            string spName = "select * from t_GC_NguyenPhuLieu where HopDong_ID =@HopDong_ID ";
            //SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, ID);
            // this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, ma);

            return this.db.ExecuteDataSet(dbCommand);
        }

        public DataSet SelectDynamic1(long ID, string databaseName)
        {
            string spName = "select * from t_GC_NguyenPhuLieu where HopDong_ID =@HopDong_ID ";

            //update by HUNGTQ 06/05/2011.
            SetDabaseMoi(databaseName);

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, ID);

            return this.db.ExecuteDataSet(dbCommand);
        }

        public int DeleteBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //update by HUNGTQ 06/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_GC_NguyenPhuLieu_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            //update by HUNGTQ 06/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_GC_NguyenPhuLieu_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, this._SoLuongDaDung);
            this.db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, this._SoLuongCungUng);
            this.db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, this._TongNhuCau);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_NguyenPhuLieu_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, this._SoLuongDaDung);
            this.db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, this._SoLuongCungUng);
            this.db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, this._TongNhuCau);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public NguyenPhuLieuCollection SelectCollectionBy_HopDong_ID(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_NguyenPhuLieu_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public int UpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_NguyenPhuLieu_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, this._SoLuongDaDung);
            this.db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, this._SoLuongCungUng);
            this.db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, this._TongNhuCau);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public static NguyenPhuLieu Clone(NguyenPhuLieu obj)
        {
            NguyenPhuLieu item = new NguyenPhuLieu();

            item.HopDong_ID = obj.HopDong_ID;
            item.Ma = obj.Ma;
            item.Ten = obj.Ten;
            item.MaHS = obj.MaHS;
            item.DVT_ID = obj.DVT_ID;
            item.SoLuongDangKy = obj.SoLuongDangKy;
            item.SoLuongDaNhap = 0;
            item.SoLuongDaDung = 0;
            item.SoLuongCungUng = 0;
            item.TongNhuCau = obj.TongNhuCau;
            item.STTHang = obj.STTHang;
            item.TrangThai = obj.TrangThai;
            item.DonGia = obj.DonGia;
            item.LuongTonChuaHH = obj.LuongTonChuaHH;
            item.SoLuongConLai = obj.SoLuongConLai;
            item.LuongTonNhuCau = obj.LuongTonNhuCau;

            return item;
        }
    }
}