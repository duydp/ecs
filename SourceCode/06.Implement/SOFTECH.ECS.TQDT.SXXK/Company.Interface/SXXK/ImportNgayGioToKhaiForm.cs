using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using GemBox.Spreadsheet;
namespace Company.Interface.SXXK
{
    public partial class ImportNgayGioToKhaiForm : BaseForm
    {
        public Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
        public Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDReadCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
        public Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDChuaCoCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
        public string maLoaiHinh = "N";

        public ImportNgayGioToKhaiForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        private void ImportHSTKTKNForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            openFileDialog1.InitialDirectory = Application.StartupPath;

            cboDateFormat.SelectedIndex = 0;
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid) return;
                int beginRow = Convert.ToInt32(txtRow.Value) - 1;
                if (beginRow < 0)
                {
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                    }
                    else
                    {
                        error.SetError(txtRow, "Begin row must be greater than zero ");
                    }
                    error.SetIconPadding(txtRow, 8);
                    return;

                }
                this.Cursor = Cursors.WaitCursor;
                btnReadFile.Enabled = false;
                Workbook wb = new Workbook();
                Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text, true);
                }
                catch
                {
                    MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    this.Cursor = Cursors.Default;
                    btnReadFile.Enabled = true;
                    return;
                }
                try
                {
                    ws = wb.Worksheets[txtSheet.Text];
                }
                catch
                {
                    MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                    this.Cursor = Cursors.Default;
                    btnReadFile.Enabled = true;
                    return;
                }

                TKMDCollection.Clear();
                TKMDChuaCoCollection.Clear();
                this.TKMDReadCollection.Clear();
                WorksheetRowCollection wsrc = ws.Rows;
                char soTKColumn = Convert.ToChar(txtSTKColumn.Text);
                int soTKCol = ConvertCharToInt(soTKColumn);
                char maLHColumn = Convert.ToChar(txtMLHColumn.Text);
                int maLHCol = ConvertCharToInt(maLHColumn);
                char ngayDKColumn = Convert.ToChar(txtNgayDKColumn.Text);
                int ngayDKCol = ConvertCharToInt(ngayDKColumn);
                char ngayHTColumn = Convert.ToChar(txtNgayHoanThanhColumn.Text);
                int ngayHTCol = ConvertCharToInt(ngayHTColumn);
                //char ngayThucNhapXuatColumn = Convert.ToChar(txtNgayThucNhapXuatColumn.Text);
                //int ngayThucNhapXuatCol = ConvertCharToInt(ngayThucNhapXuatColumn);

                int soTKChuaCo = 0;
                string thongBao = "";

                //Hungtq update 23/03/2011.
                System.Globalization.CultureInfo cul = null;
                if (cboDateFormat.SelectedItem.ToString() == "dd/MM/yyyy")
                    cul = new System.Globalization.CultureInfo("vi-VN");
                else if (cboDateFormat.SelectedItem.ToString() == "MM/dd/yyyy")
                    cul = new System.Globalization.CultureInfo("en-US");

                StringBuilder sb = new StringBuilder();
                int soTKDaDongHoSo = 0;
                string thongBaoTKDongHoSo = "";
                int cnt = 0; //Bien dem tong so to khai
                Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmdTemp = null;
                bool isExixts = false;

                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmd = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();

                            tkmd.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            tkmd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;

                            if (wsr.Cells[soTKCol].Value == null)
                                break;

                            tkmd.SoToKhai = Convert.ToInt32(wsr.Cells[soTKCol].Value.ToString().Trim());
                            //if (tkmd.SoToKhai == 1981)
                            //{ }

                            tkmd.MaLoaiHinh = wsr.Cells[maLHCol].Value.ToString().Trim();

                            //Ngay dang ky
                            if (wsr.Cells[ngayDKCol].Value.GetType().Name == "String")
                                tkmd.NgayDangKy = Convert.ToDateTime(wsr.Cells[ngayDKCol].Value, cul);
                            else if (wsr.Cells[ngayDKCol].Value.GetType().Name == "Double")
                                tkmd.NgayDangKy = System.DateTime.FromOADate((double)wsr.Cells[ngayDKCol].Value);
                            DateTime ngayDKTemp = tkmd.NgayDangKy.Date;

                            //Ngay hoan thanh
                            if (wsr.Cells[ngayHTCol].Value.GetType().Name == "String")
                            {
                                if (wsr.Cells[ngayHTCol].Value == null || wsr.Cells[ngayHTCol].Value.ToString() == "NULL")
                                {
                                    //Khong cap nhat gia tri ngay hoan thanh
                                }
                                else
                                    tkmd.NgayHoanThanh = Convert.ToDateTime(wsr.Cells[ngayHTCol].Value, cul);
                            }
                            else if (wsr.Cells[ngayHTCol].Value.GetType().Name == "Double")
                                tkmd.NgayHoanThanh = System.DateTime.FromOADate((double)wsr.Cells[ngayHTCol].Value);
                            DateTime ngayHoanThanhTemp = tkmd.NgayHoanThanh;

                            //Ngay thuc nhap/ xuat
                            //if (wsr.Cells[ngayThucNhapXuatCol].Value.GetType().Name == "String")
                            //    tkmd.NGAY_THN_THX = Convert.ToDateTime(wsr.Cells[ngayThucNhapXuatCol].Value, cul);
                            //else if (wsr.Cells[ngayThucNhapXuatCol].Value.GetType().Name == "Double")
                            //    tkmd.NGAY_THN_THX = System.DateTime.FromOADate((double)wsr.Cells[ngayThucNhapXuatCol].Value);
                            //DateTime ngayThucNhapXuatTemp = tkmd.NGAY_THN_THX;

                            tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;

                            tkmdTemp = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                            tkmdTemp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            tkmdTemp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            tkmdTemp.SoToKhai = tkmd.SoToKhai;
                            tkmdTemp.MaLoaiHinh = tkmd.MaLoaiHinh;
                            tkmdTemp.NamDangKy = tkmd.NamDangKy;
                            isExixts = tkmdTemp.Load();

                            if (isExixts)// có TK trong đã đăng ký thì mới thêm vào
                            {
                                //tkmd.NgayDangKy = ngayDKTemp; //Khong cap nhat lai ngay Dang ky
                                tkmd.NgayHoanThanh = ngayHoanThanhTemp;
                                //tkmd.NGAY_THN_THX = ngayThucNhapXuatTemp;

                                //Kiem tra to khai NHAP co tham gia thanh khoan va da dong ho so chua?
                                if (tkmd.MaLoaiHinh.Substring(0, 1) == "N")
                                {
                                    if (!tkmd.IsToKhaiDaThanhKhoanVaDongHoSo())
                                    {
                                        this.TKMDReadCollection.Add(tkmd);
                                    }
                                    //Trường hợp tờ khai đã tham gia thanh khoản và đóng hồ sơ cho 1 NPL 
                                    //và tờ khai đó tham gia thanh khoản cho 1 NPL khác nhưng chưa đóng hồ sơ 
                                    //-> cho phép cập nhật ngày hoàn thành.
                                    else if (tkmd.IsToKhaiDaThanhKhoanVaDongHoSo() == true && tkmd.IsToKhaiDaChayThanhKhoanVaChuaDongHoSo() == true)
                                    {
                                        this.TKMDReadCollection.Add(tkmd);
                                    }
                                    else if (tkmd.IsToKhaiDaThanhKhoanVaDongHoSo() == true && tkmd.IsToKhaiDaChayThanhKhoanVaChuaDongHoSo() == false)
                                    {
                                        soTKDaDongHoSo++;
                                        thongBaoTKDongHoSo += "\r\nTờ khai số: " + tkmd.SoToKhai + ", Mã LH: " + tkmd.MaLoaiHinh + ", ngày đk: " + tkmd.NgayDangKy;
                                    }
                                }

                                else if (tkmd.MaLoaiHinh.Substring(0, 1) == "X")
                                {
                                    if (!tkmd.IsToKhaiXuatDaThanhKhoanVaDongHoSo())
                                    {
                                        this.TKMDReadCollection.Add(tkmd);
                                    }
                                    else
                                    {
                                        soTKDaDongHoSo++;
                                        thongBaoTKDongHoSo += "\r\nTờ khai số: " + tkmd.SoToKhai + ", Mã LH: " + tkmd.MaLoaiHinh + ", ngày đk: " + tkmd.NgayDangKy;
                                    }
                                }
                            }
                            else
                            {
                                soTKChuaCo++;
                                thongBao += "\r\nTờ khai số: " + tkmd.SoToKhai + ", Mã LH: " + tkmd.MaLoaiHinh + ", ngày đk: " + tkmd.NgayDangKy;
                                this.TKMDChuaCoCollection.Add(tkmd);
                            }

                            cnt += 1;
                        }
                        catch
                        {
                            if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", "", true) != "Yes")
                            {
                                this.TKMDReadCollection.Clear();
                                this.Cursor = Cursors.Default;
                                btnReadFile.Enabled = true;
                                return;
                            }
                        }
                    }
                }

                if (soTKChuaCo > 0)
                {
                    thongBao = "Tổng cộng có: " + soTKChuaCo + "/" + cnt + " tờ khai chưa có trong hệ thống:\r\n" + thongBao;
                }

                if (soTKDaDongHoSo > 0)
                {
                    thongBaoTKDongHoSo = "Tổng cộng có: " + soTKDaDongHoSo + "/" + cnt + " tờ khai đã tham gia thanh khoản và đã đóng hồ sơ:\r\n" + thongBaoTKDongHoSo;
                }

                if (chkHienThi.Checked)
                {
                    MLMessages(thongBaoTKDongHoSo + "\r\n-------------------------------\n" + thongBao, "", "", false);
                }

                //Binding Grid
                dgList.DataSource = this.TKMDReadCollection;
                dgList.Refetch();

                this.Cursor = Cursors.Default;
                btnReadFile.Enabled = true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; btnReadFile.Enabled = true; }
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            btnSave.Enabled = false;
            bool isExixts = false;
            Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmdTemp = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();

            try
            {
                if (this.TKMDReadCollection.Count > 0)
                {

                    //Loc khong lay to khai nao chua co ngay hoan thanh va khong cap nhat vao CSDL.
                    //Va loc cac tk khac ngay gio hoan thanh.
                    foreach (Company.BLL.SXXK.ToKhai.ToKhaiMauDich tk in this.TKMDReadCollection)
                    {
                        if (tk.SoToKhai == 2391)
                        {

                        }
                        else
                            continue;

                        tkmdTemp = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        tkmdTemp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        tkmdTemp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        tkmdTemp.SoToKhai = tk.SoToKhai;
                        tkmdTemp.MaLoaiHinh = tk.MaLoaiHinh;
                        tkmdTemp.NamDangKy = tk.NamDangKy;
                        isExixts = tkmdTemp.Load();

                        if (tk.NgayHoanThanh.Year != 1900 && (isExixts && tkmdTemp.NgayHoanThanh != tk.NgayHoanThanh))
                            TKMDCollection.Add(tk);
                    }

                    if (this.TKMDCollection.Count == 0)
                    {
                        ShowMessage("Không có sự khác nhau giữa dữ liệu tờ khai Import từ file Excel và dữ liệu tờ khai trên hệ thống.", false);
                        return;
                    }

                    if (new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().InsertUpdate(this.TKMDCollection))
                    {
                        ShowMessage("Cập nhật thành công cho " + this.TKMDCollection.Count + " tờ khai có ngày hoàn thành.", false);
                    }
                    else
                        ShowMessage("Cập nhật không thành công.", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Cập nhật không thành công:\r\n" + ex.Message, false);
            }
            finally
            {
                btnSave.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //if (e.Row.RowType == RowType.Record)
            //{
            //    if (e.Row.Cells["TenNPL"].Text == "")
            //    {
            //        e.Row.RowStyle = new GridEXFormatStyle();
            //        e.Row.RowStyle.ForeColor = Color.Red;
            //    }
            //}
        }
    }
}