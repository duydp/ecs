using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAF8030 : BasicVNACC
    {
       
        public void LoadVAF8030(string strResult)
        {
            try
            {

                this.GetObject<VAF8030>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAF8030, false, VAF8030.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAF8030.TongSoByte);
               
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }

}
