﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.Messages.GoodsItem;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface.GC
{
    public partial class BaoCaoQuyetToanMMTB : BaseFormHaveGuidPanel
    {
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public bool IsActive = false;
        public bool IsReadExcel = false;
        public string LIST_HOPDONG_ID = "";
        public KDT_VNACCS_BaoCaoQuyetToan_MMTB goodItem = new KDT_VNACCS_BaoCaoQuyetToan_MMTB();
        public KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail();
        public KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail goodItemHDDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();
        public BaoCaoQuyetToanMMTB()
        {
            InitializeComponent();
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    Add();
                    break;
                case "cmdSend":
                    Send("Send");
                    break;
                case "cmdSendEdit":
                    Send("Edit");
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
                default:
                    break;
            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = goodItem.ID;
                f.loaiKhaiBao = LoaiKhaiBao.GoodItem;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", goodItem.ID, LoaiKhaiBao.ContractReference), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageBCQTMMTB(goodItem);
                    goodItem.InsertUpdateFull();
                    ShowMessage("Cập nhật thông tin thành công .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void setCommandStatus()
        {
            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = goodItem.SoTN.ToString();
                dateNgayTN.Value = goodItem.NgayTN;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || goodItem.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;


                dateNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;


                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTiepNhan.Text = goodItem.SoTN.ToString();
                dateNgayTN.Value = goodItem.NgayTN;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || goodItem.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || goodItem.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = goodItem.SoTN.ToString();
                dateNgayTN.Value = goodItem.NgayTN;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = goodItem.SoTN.ToString();
                dateNgayTN.Value = goodItem.NgayTN;
                lblTrangThai.Text = goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Not declared") : setText("Chờ duyệt Sửa / Hủy", "Not declared");
                this.OpenType = OpenFormType.View;
            }

        }
        private void BindDataTotal()
        {
            try
            {
                // Xử lý dữ liệu
                List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail> ToTalHangHoaCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail>();
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail HD in goodItem.GoodItemCollection)
                {
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail item in HD.GoodItemDetailCollection)
                    {
                        ToTalHangHoaCollection.Add(item);
                    }
                }
                dgListMMTB.Refresh();
                dgListMMTB.DataSource = ToTalHangHoaCollection;
                dgListMMTB.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Add()
        {
            BaoCaoQuyetToanMMTB_HangHoaForm f = new BaoCaoQuyetToanMMTB_HangHoaForm();
            f.goodItem = goodItem;
            f.OpenType = this.OpenType;
            f.ShowDialog(this);
            BindDataHDGoodItem();
            BindDataTotal();
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_BaoCaoQuyetToan_MMTB", "", Convert.ToInt32(goodItem.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void BindDataHDGoodItem()
        {
            try
            {
                dgListHD.Refresh();
                dgListHD.DataSource = goodItem.GoodItemCollection;
                dgListHD.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void BindDataGoodItem()
        {
            try
            {
                dgListMMTB.Refresh();
                dgListMMTB.DataSource = goodItem.GoodItemDetailCollection;
                dgListMMTB.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void Save()
        {
            try
            {
                GetGoodItem();
                //if (goodItem.ID !=0)
                //{                    
                //    goodItemDetail.DeleteBy_Contract_ID(goodItemHDDetail.ID);
                //    goodItemHDDetail.DeleteBy_GoodItems(goodItem.ID);
                //}
                goodItem.InsertUpdateFull();
                BindDataHDGoodItem();
                BindDataTotal();
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void GetGoodItem()
        {
            try
            {
                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                goodItem.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                goodItem.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void SetGoodItem()
        {
            try
            {
                string TrangThai = goodItem.TrangThaiXuLy.ToString();
                switch (TrangThai)
                {
                    case "0":
                        lblTrangThai.Text = "Chờ duyệt";
                        break;
                    case "-1":
                        lblTrangThai.Text = "Chưa khai báo";
                        break;
                    case "1":
                        lblTrangThai.Text = "Đã duyệt";
                        break;
                    case "2":
                        lblTrangThai.Text = "Không phê duyệt";
                        break;

                }
                txtSoTiepNhan.Text = goodItem.SoTN.ToString();
                dateNgayTN.Value = goodItem.NgayTN;
                goodItem.GoodItemCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail.SelectCollectionDynamic("GoodItems = " + goodItem.ID, " ");
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                {
                    item.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionBy_Contract_ID(item.ID);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void Send(string Status)
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (goodItem.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    goodItem = KDT_VNACCS_BaoCaoQuyetToan_MMTB.Load(goodItem.ID);
                    goodItem.GoodItemCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail.SelectCollectionBy_GoodItems(goodItem.ID);
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                    {
                        item.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionBy_Contract_ID(item.ID);
                    }
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ContractReference;
                sendXML.master_id = goodItem.ID;

                if (sendXML.Load())
                {
                    if (Status == "Send")
                    {
                        if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            return;
                        }
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    goodItem.GuidStr = Guid.NewGuid().ToString();
                    ContractReference_VNACCS contractReference_VNACCS = new ContractReference_VNACCS();
                    contractReference_VNACCS = Mapper_V4.ToDataTransferContractReferences(goodItem, goodItemDetail, GlobalSettings.DIA_CHI, GlobalSettings.TEN_DON_VI, Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = goodItem.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ)),
                              Identity = goodItem.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = contractReference_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = goodItem.GuidStr,
                          },
                          contractReference_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterContractRefrence);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.ContractReference;
                        sendXML.master_id = goodItem.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();

                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            goodItem.Update();
                            setCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            setCommandStatus();
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            goodItem.Update();
                            setCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            ShowMessageTQDT(msgInfor, false);
                            Feedback();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterContractRefrence);
                        ShowMessageTQDT(msgInfor, false);
                        setCommandStatus();
                    }
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.ContractReference;
            sendXML.master_id = goodItem.ID;
            if (!sendXML.Load())
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                return;
            }
            while (isFeedBack)
            {
                string reference = goodItem.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.ContractReference,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.ContractReference,
                };
                subjectBase.Type =DeclarationIssuer.ContractReference;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = goodItem.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ.Trim())),
                                                  Identity = goodItem.MaHQ
                                              }, subjectBase, null);
                if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                    {
                        goodItem.Update();
                        isFeedBack = true;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        goodItem.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        goodItem.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = goodItem.ID;
                form.DeclarationIssuer = DeclarationIssuer.ContractReference;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void BaoCaoQuyetToanMMTB_Load(object sender, EventArgs e)
        {
            try
            {
                #region
                //if (IsActive == true)
                //{
                //    goodItem.GoodItemCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail.SelectCollectionDynamic("GoodItems = " + goodItem.ID, " ");
                //    string List_ID = "";
                //    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                //    {
                //        List_ID += item.ID.ToString() + ",";
                //    }
                //    if (List_ID.Length > 0)
                //    {
                //        List_ID = List_ID.Substring(0, List_ID.Length - 1);
                //        goodItem.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionDynamic("Contract_ID IN (" + List_ID + ")", " ");
                //    }
                //    BindDataGoodItem();
                //    BindDataHDGoodItem();
                //}
                //else
                //{
                //    if (LIST_HOPDONG_ID.Length > 0)
                //    {
                //        LIST_HOPDONG_ID = LIST_HOPDONG_ID.Substring(0, LIST_HOPDONG_ID.Length - 1);
                //        goodItem.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionDynamic("ID_HopDong IN (" + LIST_HOPDONG_ID + ")", " ");
                //        goodItem.GoodItemCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail.SelectCollectionDynamic("ID_HopDong IN (" + LIST_HOPDONG_ID+")","");
                //    }
                //    BindDataGoodItem();
                //    BindDataHDGoodItem();
                //}
                #endregion
                if (goodItem.ID==0)
                {
                    goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
                SetGoodItem();
                setCommandStatus();
                BindDataHDGoodItem();
                BindDataTotal();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = uiTab1.SelectedTab.Text + "_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ReadExcelFile_Click(object sender, EventArgs e)
        {
            try
            {
                goodItem.GoodItemDetailCollection.Clear();
                goodItem.GoodItemCollection.Clear();
                IsReadExcel = true;
                ReadExcelFormMMTB f = new ReadExcelFormMMTB();
                f.goodItem = goodItem;
                f.ShowDialog(this);
                BindDataGoodItem();
                BindDataHDGoodItem();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ContractRefrenceSendHandler(goodItem, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel(dgListMMTB);
        }

        private void dgListMMTB_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            Add();
        }

        private void dgListMMTB_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                Int64 Contract_ID = Convert.ToInt64(e.Row.Cells["ID_HopDong"].Value.ToString());
                if (Contract_ID > 0)
                {
                    KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail HD = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();
                    HD = KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail.Load(Contract_ID);
                    e.Row.Cells["ID_HopDong"].Text = Company.GC.BLL.KDT.GC.HopDong.GetNameHDRent(HD.ID_HopDong);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListHD_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            Add();
        }
    }
}
