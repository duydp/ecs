﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace LicenseCode
{
	public partial class LM_Product : ICloneable
	{
		#region Properties.
		
		public string Product_ID { set; get; }
		public long Group_ID { set; get; }
		public string Name { set; get; }
		public string Description { set; get; }
		public string Prefix { set; get; }
		public string Creator { set; get; }
		public DateTime CreatedDate { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<LM_Product> ConvertToCollection(IDataReader reader)
		{
			List<LM_Product> collection = new List<LM_Product>();
			while (reader.Read())
			{
				LM_Product entity = new LM_Product();
				if (!reader.IsDBNull(reader.GetOrdinal("Product_ID"))) entity.Product_ID = reader.GetString(reader.GetOrdinal("Product_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Group_ID"))) entity.Group_ID = reader.GetInt64(reader.GetOrdinal("Group_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
				if (!reader.IsDBNull(reader.GetOrdinal("Description"))) entity.Description = reader.GetString(reader.GetOrdinal("Description"));
				if (!reader.IsDBNull(reader.GetOrdinal("Prefix"))) entity.Prefix = reader.GetString(reader.GetOrdinal("Prefix"));
				if (!reader.IsDBNull(reader.GetOrdinal("Creator"))) entity.Creator = reader.GetString(reader.GetOrdinal("Creator"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<LM_Product> collection, string product_ID)
        {
            foreach (LM_Product item in collection)
            {
                if (item.Product_ID == product_ID)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_LM_Products VALUES(@Product_ID, @Group_ID, @Name, @Description, @Prefix, @Creator, @CreatedDate)";
            string update = "UPDATE t_LM_Products SET Group_ID = @Group_ID, Name = @Name, Description = @Description, Prefix = @Prefix, Creator = @Creator, CreatedDate = @CreatedDate WHERE Product_ID = @Product_ID";
            string delete = "DELETE FROM t_LM_Products WHERE Product_ID = @Product_ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@Product_ID", SqlDbType.NVarChar, "Product_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Group_ID", SqlDbType.BigInt, "Group_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Name", SqlDbType.NVarChar, "Name", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Description", SqlDbType.NVarChar, "Description", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Prefix", SqlDbType.VarChar, "Prefix", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Creator", SqlDbType.NVarChar, "Creator", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@Product_ID", SqlDbType.NVarChar, "Product_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Group_ID", SqlDbType.BigInt, "Group_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Name", SqlDbType.NVarChar, "Name", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Description", SqlDbType.NVarChar, "Description", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Prefix", SqlDbType.VarChar, "Prefix", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Creator", SqlDbType.NVarChar, "Creator", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@Product_ID", SqlDbType.NVarChar, "Product_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_LM_Products VALUES(@Product_ID, @Group_ID, @Name, @Description, @Prefix, @Creator, @CreatedDate)";
            string update = "UPDATE t_LM_Products SET Group_ID = @Group_ID, Name = @Name, Description = @Description, Prefix = @Prefix, Creator = @Creator, CreatedDate = @CreatedDate WHERE Product_ID = @Product_ID";
            string delete = "DELETE FROM t_LM_Products WHERE Product_ID = @Product_ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@Product_ID", SqlDbType.NVarChar, "Product_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Group_ID", SqlDbType.BigInt, "Group_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Name", SqlDbType.NVarChar, "Name", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Description", SqlDbType.NVarChar, "Description", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Prefix", SqlDbType.VarChar, "Prefix", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Creator", SqlDbType.NVarChar, "Creator", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@Product_ID", SqlDbType.NVarChar, "Product_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Group_ID", SqlDbType.BigInt, "Group_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Name", SqlDbType.NVarChar, "Name", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Description", SqlDbType.NVarChar, "Description", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Prefix", SqlDbType.VarChar, "Prefix", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Creator", SqlDbType.NVarChar, "Creator", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@Product_ID", SqlDbType.NVarChar, "Product_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static LM_Product Load(string product_ID)
		{
			const string spName = "[dbo].[p_LM_Product_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, product_ID);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<LM_Product> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<LM_Product> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<LM_Product> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<LM_Product> SelectCollectionBy_Group_ID(long group_ID)
		{
            IDataReader reader = SelectReaderBy_Group_ID(group_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Group_ID(long group_ID)
		{
			const string spName = "[dbo].[p_LM_Product_SelectBy_Group_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, group_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LM_Product_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Product_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LM_Product_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Product_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_Group_ID(long group_ID)
		{
			const string spName = "p_LM_Product_SelectBy_Group_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, group_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertLM_Product(string product_ID, long group_ID, string name, string description, string prefix, string creator, DateTime createdDate)
		{
			LM_Product entity = new LM_Product();	
            entity.Product_ID = product_ID;
			entity.Group_ID = group_ID;
			entity.Name = name;
			entity.Description = description;
			entity.Prefix = prefix;
			entity.Creator = creator;
			entity.CreatedDate = createdDate;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LM_Product_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, Product_ID);
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, Group_ID);
			db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
			db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
			db.AddInParameter(dbCommand, "@Prefix", SqlDbType.VarChar, Prefix);
			db.AddInParameter(dbCommand, "@Creator", SqlDbType.NVarChar, Creator);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<LM_Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Product item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateLM_Product(string product_ID, long group_ID, string name, string description, string prefix, string creator, DateTime createdDate)
		{
			LM_Product entity = new LM_Product();			
			entity.Product_ID = product_ID;
			entity.Group_ID = group_ID;
			entity.Name = name;
			entity.Description = description;
			entity.Prefix = prefix;
			entity.Creator = creator;
			entity.CreatedDate = createdDate;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LM_Product_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, Product_ID);
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, Group_ID);
			db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
			db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
			db.AddInParameter(dbCommand, "@Prefix", SqlDbType.VarChar, Prefix);
			db.AddInParameter(dbCommand, "@Creator", SqlDbType.NVarChar, Creator);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<LM_Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Product item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateLM_Product(string product_ID, long group_ID, string name, string description, string prefix, string creator, DateTime createdDate)
		{
			LM_Product entity = new LM_Product();			
			entity.Product_ID = product_ID;
			entity.Group_ID = group_ID;
			entity.Name = name;
			entity.Description = description;
			entity.Prefix = prefix;
			entity.Creator = creator;
			entity.CreatedDate = createdDate;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Product_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, Product_ID);
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, Group_ID);
			db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
			db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
			db.AddInParameter(dbCommand, "@Prefix", SqlDbType.VarChar, Prefix);
			db.AddInParameter(dbCommand, "@Creator", SqlDbType.NVarChar, Creator);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<LM_Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Product item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteLM_Product(string product_ID)
		{
			LM_Product entity = new LM_Product();
			entity.Product_ID = product_ID;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Product_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Product_ID", SqlDbType.NVarChar, Product_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_Group_ID(long group_ID)
		{
			const string spName = "[dbo].[p_LM_Product_DeleteBy_Group_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Group_ID", SqlDbType.BigInt, group_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LM_Product_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<LM_Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_Product item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}