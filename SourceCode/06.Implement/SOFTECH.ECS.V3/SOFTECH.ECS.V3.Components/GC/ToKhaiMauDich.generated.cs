﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class ToKhaiMauDich
    {
        #region Properties.

        public long ID { set; get; }
        public long SoTiepNhan { set; get; }
        public DateTime NgayTiepNhan { set; get; }
        public string MaHaiQuan { set; get; }
        public int SoToKhai { set; get; }
        public string MaLoaiHinh { set; get; }
        public DateTime NgayDangKy { set; get; }
        public string MaDoanhNghiep { set; get; }
        public string TenDoanhNghiep { set; get; }
        public string MaDaiLyTTHQ { set; get; }
        public string TenDaiLyTTHQ { set; get; }
        public string TenDonViDoiTac { set; get; }
        public string ChiTietDonViDoiTac { set; get; }
        public string SoGiayPhep { set; get; }
        public DateTime NgayGiayPhep { set; get; }
        public DateTime NgayHetHanGiayPhep { set; get; }
        public string SoHopDong { set; get; }
        public DateTime NgayHopDong { set; get; }
        public DateTime NgayHetHanHopDong { set; get; }
        public string SoHoaDonThuongMai { set; get; }
        public DateTime NgayHoaDonThuongMai { set; get; }
        public string PTVT_ID { set; get; }
        public string SoHieuPTVT { set; get; }
        public DateTime NgayDenPTVT { set; get; }
        public string QuocTichPTVT_ID { set; get; }
        public string LoaiVanDon { set; get; }
        public string SoVanDon { set; get; }
        public DateTime NgayVanDon { set; get; }
        public string NuocXK_ID { set; get; }
        public string NuocNK_ID { set; get; }
        public string DiaDiemXepHang { set; get; }
        public string CuaKhau_ID { set; get; }
        public string DKGH_ID { set; get; }
        public string NguyenTe_ID { set; get; }
        public decimal TyGiaTinhThue { set; get; }
        public decimal TyGiaUSD { set; get; }
        public string PTTT_ID { set; get; }
        public short SoHang { set; get; }
        public short SoLuongPLTK { set; get; }
        public string TenChuHang { set; get; }
        public decimal SoContainer20 { set; get; }
        public decimal SoContainer40 { set; get; }
        public decimal SoKien { set; get; }
        public decimal TrongLuong { set; get; }
        public decimal TongTriGiaKhaiBao { set; get; }
        public decimal TongTriGiaTinhThue { set; get; }
        public string LoaiToKhaiGiaCong { set; get; }
        public decimal LePhiHaiQuan { set; get; }
        public decimal PhiBaoHiem { set; get; }
        public decimal PhiVanChuyen { set; get; }
        public decimal PhiXepDoHang { set; get; }
        public decimal PhiKhac { set; get; }
        public string CanBoDangKy { set; get; }
        public bool QuanLyMay { set; get; }
        public int TrangThaiXuLy { set; get; }
        public string LoaiHangHoa { set; get; }
        public string GiayTo { set; get; }
        public string PhanLuong { set; get; }
        public string MaDonViUT { set; get; }
        public string TenDonViUT { set; get; }
        public double TrongLuongNet { set; get; }
        public decimal SoTienKhoan { set; get; }
        public long IDHopDong { set; get; }
        public string MaMid { set; get; }
        public DateTime Ngay_THN_THX { set; get; }
        public int TrangThaiPhanBo { set; get; }
        public string GUIDSTR { set; get; }
        public string DeXuatKhac { set; get; }
        public string LyDoSua { set; get; }
        public short ActionStatus { set; get; }
        public string GuidReference { set; get; }
        public int NamDK { set; get; }
        public string HUONGDAN { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<ToKhaiMauDich> ConvertToCollection(IDataReader reader)
        {
            IList<ToKhaiMauDich> collection = new List<ToKhaiMauDich>();
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) entity.Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) entity.TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static ToKhaiMauDich Load(long id)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<ToKhaiMauDich> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<ToKhaiMauDich> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<ToKhaiMauDich> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<ToKhaiMauDich> SelectCollectionBy_IDHopDong(long iDHopDong)
        {
            IDataReader reader = SelectReaderBy_IDHopDong(iDHopDong);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_IDHopDong(long iDHopDong)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectBy_IDHopDong]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_IDHopDong(long iDHopDong)
        {
            const string spName = "p_KDT_ToKhaiMauDich_SelectBy_IDHopDong";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertToKhaiMauDich(long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, int soToKhai, string maLoaiHinh, DateTime ngayDangKy, string maDoanhNghiep, string tenDoanhNghiep, string maDaiLyTTHQ, string tenDaiLyTTHQ, string tenDonViDoiTac, string chiTietDonViDoiTac, string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHanGiayPhep, string soHopDong, DateTime ngayHopDong, DateTime ngayHetHanHopDong, string soHoaDonThuongMai, DateTime ngayHoaDonThuongMai, string pTVT_ID, string soHieuPTVT, DateTime ngayDenPTVT, string quocTichPTVT_ID, string loaiVanDon, string soVanDon, DateTime ngayVanDon, string nuocXK_ID, string nuocNK_ID, string diaDiemXepHang, string cuaKhau_ID, string dKGH_ID, string nguyenTe_ID, decimal tyGiaTinhThue, decimal tyGiaUSD, string pTTT_ID, short soHang, short soLuongPLTK, string tenChuHang, decimal soContainer20, decimal soContainer40, decimal soKien, decimal trongLuong, decimal tongTriGiaKhaiBao, decimal tongTriGiaTinhThue, string loaiToKhaiGiaCong, decimal lePhiHaiQuan, decimal phiBaoHiem, decimal phiVanChuyen, decimal phiXepDoHang, decimal phiKhac, string canBoDangKy, bool quanLyMay, int trangThaiXuLy, string loaiHangHoa, string giayTo, string phanLuong, string maDonViUT, string tenDonViUT, double trongLuongNet, decimal soTienKhoan, long iDHopDong, string maMid, DateTime ngay_THN_THX, int trangThaiPhanBo, string gUIDSTR, string deXuatKhac, string lyDoSua, short actionStatus, string guidReference, int namDK, string hUONGDAN)
        {
            ToKhaiMauDich entity = new ToKhaiMauDich();
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.MaHaiQuan = maHaiQuan;
            entity.SoToKhai = soToKhai;
            entity.MaLoaiHinh = maLoaiHinh;
            entity.NgayDangKy = ngayDangKy;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.TenDoanhNghiep = tenDoanhNghiep;
            entity.MaDaiLyTTHQ = maDaiLyTTHQ;
            entity.TenDaiLyTTHQ = tenDaiLyTTHQ;
            entity.TenDonViDoiTac = tenDonViDoiTac;
            entity.ChiTietDonViDoiTac = chiTietDonViDoiTac;
            entity.SoGiayPhep = soGiayPhep;
            entity.NgayGiayPhep = ngayGiayPhep;
            entity.NgayHetHanGiayPhep = ngayHetHanGiayPhep;
            entity.SoHopDong = soHopDong;
            entity.NgayHopDong = ngayHopDong;
            entity.NgayHetHanHopDong = ngayHetHanHopDong;
            entity.SoHoaDonThuongMai = soHoaDonThuongMai;
            entity.NgayHoaDonThuongMai = ngayHoaDonThuongMai;
            entity.PTVT_ID = pTVT_ID;
            entity.SoHieuPTVT = soHieuPTVT;
            entity.NgayDenPTVT = ngayDenPTVT;
            entity.QuocTichPTVT_ID = quocTichPTVT_ID;
            entity.LoaiVanDon = loaiVanDon;
            entity.SoVanDon = soVanDon;
            entity.NgayVanDon = ngayVanDon;
            entity.NuocXK_ID = nuocXK_ID;
            entity.NuocNK_ID = nuocNK_ID;
            entity.DiaDiemXepHang = diaDiemXepHang;
            entity.CuaKhau_ID = cuaKhau_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.TyGiaTinhThue = tyGiaTinhThue;
            entity.TyGiaUSD = tyGiaUSD;
            entity.PTTT_ID = pTTT_ID;
            entity.SoHang = soHang;
            entity.SoLuongPLTK = soLuongPLTK;
            entity.TenChuHang = tenChuHang;
            entity.SoContainer20 = soContainer20;
            entity.SoContainer40 = soContainer40;
            entity.SoKien = soKien;
            entity.TrongLuong = trongLuong;
            entity.TongTriGiaKhaiBao = tongTriGiaKhaiBao;
            entity.TongTriGiaTinhThue = tongTriGiaTinhThue;
            entity.LoaiToKhaiGiaCong = loaiToKhaiGiaCong;
            entity.LePhiHaiQuan = lePhiHaiQuan;
            entity.PhiBaoHiem = phiBaoHiem;
            entity.PhiVanChuyen = phiVanChuyen;
            entity.PhiXepDoHang = phiXepDoHang;
            entity.PhiKhac = phiKhac;
            entity.CanBoDangKy = canBoDangKy;
            entity.QuanLyMay = quanLyMay;
            entity.TrangThaiXuLy = trangThaiXuLy;
            entity.LoaiHangHoa = loaiHangHoa;
            entity.GiayTo = giayTo;
            entity.PhanLuong = phanLuong;
            entity.MaDonViUT = maDonViUT;
            entity.TenDonViUT = tenDonViUT;
            entity.TrongLuongNet = trongLuongNet;
            entity.SoTienKhoan = soTienKhoan;
            entity.IDHopDong = iDHopDong;
            entity.MaMid = maMid;
            entity.Ngay_THN_THX = ngay_THN_THX;
            entity.TrangThaiPhanBo = trangThaiPhanBo;
            entity.GUIDSTR = gUIDSTR;
            entity.DeXuatKhac = deXuatKhac;
            entity.LyDoSua = lyDoSua;
            entity.ActionStatus = actionStatus;
            entity.GuidReference = guidReference;
            entity.NamDK = namDK;
            entity.HUONGDAN = hUONGDAN;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object)NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, ChiTietDonViDoiTac);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year <= 1753 ? DBNull.Value : (object)NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, NgayHetHanGiayPhep.Year <= 1753 ? DBNull.Value : (object)NgayHetHanGiayPhep);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object)NgayHopDong);
            db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, NgayHetHanHopDong.Year <= 1753 ? DBNull.Value : (object)NgayHetHanHopDong);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, SoHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, NgayHoaDonThuongMai.Year <= 1753 ? DBNull.Value : (object)NgayHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, PTVT_ID);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, QuocTichPTVT_ID);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, LoaiVanDon);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, NuocNK_ID);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, CuaKhau_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, TyGiaTinhThue);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, SoHang);
            db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, SoLuongPLTK);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, TenChuHang);
            db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, SoContainer20);
            db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, SoContainer40);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, TongTriGiaKhaiBao);
            db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, TongTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, LoaiToKhaiGiaCong);
            db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, LePhiHaiQuan);
            db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, PhiBaoHiem);
            db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, PhiVanChuyen);
            db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, PhiXepDoHang);
            db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, PhiKhac);
            db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, CanBoDangKy);
            db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, QuanLyMay);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, LoaiHangHoa);
            db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, GiayTo);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, MaDonViUT);
            db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, TenDonViUT);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, TrongLuongNet);
            db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, SoTienKhoan);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, MaMid);
            db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, Ngay_THN_THX.Year <= 1753 ? DBNull.Value : (object)Ngay_THN_THX);
            db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, TrangThaiPhanBo);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<ToKhaiMauDich> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ToKhaiMauDich item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateToKhaiMauDich(long id, long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, int soToKhai, string maLoaiHinh, DateTime ngayDangKy, string maDoanhNghiep, string tenDoanhNghiep, string maDaiLyTTHQ, string tenDaiLyTTHQ, string tenDonViDoiTac, string chiTietDonViDoiTac, string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHanGiayPhep, string soHopDong, DateTime ngayHopDong, DateTime ngayHetHanHopDong, string soHoaDonThuongMai, DateTime ngayHoaDonThuongMai, string pTVT_ID, string soHieuPTVT, DateTime ngayDenPTVT, string quocTichPTVT_ID, string loaiVanDon, string soVanDon, DateTime ngayVanDon, string nuocXK_ID, string nuocNK_ID, string diaDiemXepHang, string cuaKhau_ID, string dKGH_ID, string nguyenTe_ID, decimal tyGiaTinhThue, decimal tyGiaUSD, string pTTT_ID, short soHang, short soLuongPLTK, string tenChuHang, decimal soContainer20, decimal soContainer40, decimal soKien, decimal trongLuong, decimal tongTriGiaKhaiBao, decimal tongTriGiaTinhThue, string loaiToKhaiGiaCong, decimal lePhiHaiQuan, decimal phiBaoHiem, decimal phiVanChuyen, decimal phiXepDoHang, decimal phiKhac, string canBoDangKy, bool quanLyMay, int trangThaiXuLy, string loaiHangHoa, string giayTo, string phanLuong, string maDonViUT, string tenDonViUT, double trongLuongNet, decimal soTienKhoan, long iDHopDong, string maMid, DateTime ngay_THN_THX, int trangThaiPhanBo, string gUIDSTR, string deXuatKhac, string lyDoSua, short actionStatus, string guidReference, int namDK, string hUONGDAN)
        {
            ToKhaiMauDich entity = new ToKhaiMauDich();
            entity.ID = id;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.MaHaiQuan = maHaiQuan;
            entity.SoToKhai = soToKhai;
            entity.MaLoaiHinh = maLoaiHinh;
            entity.NgayDangKy = ngayDangKy;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.TenDoanhNghiep = tenDoanhNghiep;
            entity.MaDaiLyTTHQ = maDaiLyTTHQ;
            entity.TenDaiLyTTHQ = tenDaiLyTTHQ;
            entity.TenDonViDoiTac = tenDonViDoiTac;
            entity.ChiTietDonViDoiTac = chiTietDonViDoiTac;
            entity.SoGiayPhep = soGiayPhep;
            entity.NgayGiayPhep = ngayGiayPhep;
            entity.NgayHetHanGiayPhep = ngayHetHanGiayPhep;
            entity.SoHopDong = soHopDong;
            entity.NgayHopDong = ngayHopDong;
            entity.NgayHetHanHopDong = ngayHetHanHopDong;
            entity.SoHoaDonThuongMai = soHoaDonThuongMai;
            entity.NgayHoaDonThuongMai = ngayHoaDonThuongMai;
            entity.PTVT_ID = pTVT_ID;
            entity.SoHieuPTVT = soHieuPTVT;
            entity.NgayDenPTVT = ngayDenPTVT;
            entity.QuocTichPTVT_ID = quocTichPTVT_ID;
            entity.LoaiVanDon = loaiVanDon;
            entity.SoVanDon = soVanDon;
            entity.NgayVanDon = ngayVanDon;
            entity.NuocXK_ID = nuocXK_ID;
            entity.NuocNK_ID = nuocNK_ID;
            entity.DiaDiemXepHang = diaDiemXepHang;
            entity.CuaKhau_ID = cuaKhau_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.TyGiaTinhThue = tyGiaTinhThue;
            entity.TyGiaUSD = tyGiaUSD;
            entity.PTTT_ID = pTTT_ID;
            entity.SoHang = soHang;
            entity.SoLuongPLTK = soLuongPLTK;
            entity.TenChuHang = tenChuHang;
            entity.SoContainer20 = soContainer20;
            entity.SoContainer40 = soContainer40;
            entity.SoKien = soKien;
            entity.TrongLuong = trongLuong;
            entity.TongTriGiaKhaiBao = tongTriGiaKhaiBao;
            entity.TongTriGiaTinhThue = tongTriGiaTinhThue;
            entity.LoaiToKhaiGiaCong = loaiToKhaiGiaCong;
            entity.LePhiHaiQuan = lePhiHaiQuan;
            entity.PhiBaoHiem = phiBaoHiem;
            entity.PhiVanChuyen = phiVanChuyen;
            entity.PhiXepDoHang = phiXepDoHang;
            entity.PhiKhac = phiKhac;
            entity.CanBoDangKy = canBoDangKy;
            entity.QuanLyMay = quanLyMay;
            entity.TrangThaiXuLy = trangThaiXuLy;
            entity.LoaiHangHoa = loaiHangHoa;
            entity.GiayTo = giayTo;
            entity.PhanLuong = phanLuong;
            entity.MaDonViUT = maDonViUT;
            entity.TenDonViUT = tenDonViUT;
            entity.TrongLuongNet = trongLuongNet;
            entity.SoTienKhoan = soTienKhoan;
            entity.IDHopDong = iDHopDong;
            entity.MaMid = maMid;
            entity.Ngay_THN_THX = ngay_THN_THX;
            entity.TrangThaiPhanBo = trangThaiPhanBo;
            entity.GUIDSTR = gUIDSTR;
            entity.DeXuatKhac = deXuatKhac;
            entity.LyDoSua = lyDoSua;
            entity.ActionStatus = actionStatus;
            entity.GuidReference = guidReference;
            entity.NamDK = namDK;
            entity.HUONGDAN = hUONGDAN;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_ToKhaiMauDich_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object)NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, ChiTietDonViDoiTac);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year <= 1753 ? DBNull.Value : (object)NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, NgayHetHanGiayPhep.Year <= 1753 ? DBNull.Value : (object)NgayHetHanGiayPhep);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object)NgayHopDong);
            db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, NgayHetHanHopDong.Year <= 1753 ? DBNull.Value : (object)NgayHetHanHopDong);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, SoHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, NgayHoaDonThuongMai.Year <= 1753 ? DBNull.Value : (object)NgayHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, PTVT_ID);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, QuocTichPTVT_ID);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, LoaiVanDon);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, NuocNK_ID);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, CuaKhau_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, TyGiaTinhThue);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, SoHang);
            db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, SoLuongPLTK);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, TenChuHang);
            db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, SoContainer20);
            db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, SoContainer40);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, TongTriGiaKhaiBao);
            db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, TongTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, LoaiToKhaiGiaCong);
            db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, LePhiHaiQuan);
            db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, PhiBaoHiem);
            db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, PhiVanChuyen);
            db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, PhiXepDoHang);
            db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, PhiKhac);
            db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, CanBoDangKy);
            db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, QuanLyMay);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, LoaiHangHoa);
            db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, GiayTo);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, MaDonViUT);
            db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, TenDonViUT);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, TrongLuongNet);
            db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, SoTienKhoan);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, MaMid);
            db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, Ngay_THN_THX.Year <= 1753 ? DBNull.Value : (object)Ngay_THN_THX);
            db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, TrangThaiPhanBo);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<ToKhaiMauDich> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ToKhaiMauDich item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateToKhaiMauDich(long id, long soTiepNhan, DateTime ngayTiepNhan, string maHaiQuan, int soToKhai, string maLoaiHinh, DateTime ngayDangKy, string maDoanhNghiep, string tenDoanhNghiep, string maDaiLyTTHQ, string tenDaiLyTTHQ, string tenDonViDoiTac, string chiTietDonViDoiTac, string soGiayPhep, DateTime ngayGiayPhep, DateTime ngayHetHanGiayPhep, string soHopDong, DateTime ngayHopDong, DateTime ngayHetHanHopDong, string soHoaDonThuongMai, DateTime ngayHoaDonThuongMai, string pTVT_ID, string soHieuPTVT, DateTime ngayDenPTVT, string quocTichPTVT_ID, string loaiVanDon, string soVanDon, DateTime ngayVanDon, string nuocXK_ID, string nuocNK_ID, string diaDiemXepHang, string cuaKhau_ID, string dKGH_ID, string nguyenTe_ID, decimal tyGiaTinhThue, decimal tyGiaUSD, string pTTT_ID, short soHang, short soLuongPLTK, string tenChuHang, decimal soContainer20, decimal soContainer40, decimal soKien, decimal trongLuong, decimal tongTriGiaKhaiBao, decimal tongTriGiaTinhThue, string loaiToKhaiGiaCong, decimal lePhiHaiQuan, decimal phiBaoHiem, decimal phiVanChuyen, decimal phiXepDoHang, decimal phiKhac, string canBoDangKy, bool quanLyMay, int trangThaiXuLy, string loaiHangHoa, string giayTo, string phanLuong, string maDonViUT, string tenDonViUT, double trongLuongNet, decimal soTienKhoan, long iDHopDong, string maMid, DateTime ngay_THN_THX, int trangThaiPhanBo, string gUIDSTR, string deXuatKhac, string lyDoSua, short actionStatus, string guidReference, int namDK, string hUONGDAN)
        {
            ToKhaiMauDich entity = new ToKhaiMauDich();
            entity.ID = id;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.MaHaiQuan = maHaiQuan;
            entity.SoToKhai = soToKhai;
            entity.MaLoaiHinh = maLoaiHinh;
            entity.NgayDangKy = ngayDangKy;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.TenDoanhNghiep = tenDoanhNghiep;
            entity.MaDaiLyTTHQ = maDaiLyTTHQ;
            entity.TenDaiLyTTHQ = tenDaiLyTTHQ;
            entity.TenDonViDoiTac = tenDonViDoiTac;
            entity.ChiTietDonViDoiTac = chiTietDonViDoiTac;
            entity.SoGiayPhep = soGiayPhep;
            entity.NgayGiayPhep = ngayGiayPhep;
            entity.NgayHetHanGiayPhep = ngayHetHanGiayPhep;
            entity.SoHopDong = soHopDong;
            entity.NgayHopDong = ngayHopDong;
            entity.NgayHetHanHopDong = ngayHetHanHopDong;
            entity.SoHoaDonThuongMai = soHoaDonThuongMai;
            entity.NgayHoaDonThuongMai = ngayHoaDonThuongMai;
            entity.PTVT_ID = pTVT_ID;
            entity.SoHieuPTVT = soHieuPTVT;
            entity.NgayDenPTVT = ngayDenPTVT;
            entity.QuocTichPTVT_ID = quocTichPTVT_ID;
            entity.LoaiVanDon = loaiVanDon;
            entity.SoVanDon = soVanDon;
            entity.NgayVanDon = ngayVanDon;
            entity.NuocXK_ID = nuocXK_ID;
            entity.NuocNK_ID = nuocNK_ID;
            entity.DiaDiemXepHang = diaDiemXepHang;
            entity.CuaKhau_ID = cuaKhau_ID;
            entity.DKGH_ID = dKGH_ID;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.TyGiaTinhThue = tyGiaTinhThue;
            entity.TyGiaUSD = tyGiaUSD;
            entity.PTTT_ID = pTTT_ID;
            entity.SoHang = soHang;
            entity.SoLuongPLTK = soLuongPLTK;
            entity.TenChuHang = tenChuHang;
            entity.SoContainer20 = soContainer20;
            entity.SoContainer40 = soContainer40;
            entity.SoKien = soKien;
            entity.TrongLuong = trongLuong;
            entity.TongTriGiaKhaiBao = tongTriGiaKhaiBao;
            entity.TongTriGiaTinhThue = tongTriGiaTinhThue;
            entity.LoaiToKhaiGiaCong = loaiToKhaiGiaCong;
            entity.LePhiHaiQuan = lePhiHaiQuan;
            entity.PhiBaoHiem = phiBaoHiem;
            entity.PhiVanChuyen = phiVanChuyen;
            entity.PhiXepDoHang = phiXepDoHang;
            entity.PhiKhac = phiKhac;
            entity.CanBoDangKy = canBoDangKy;
            entity.QuanLyMay = quanLyMay;
            entity.TrangThaiXuLy = trangThaiXuLy;
            entity.LoaiHangHoa = loaiHangHoa;
            entity.GiayTo = giayTo;
            entity.PhanLuong = phanLuong;
            entity.MaDonViUT = maDonViUT;
            entity.TenDonViUT = tenDonViUT;
            entity.TrongLuongNet = trongLuongNet;
            entity.SoTienKhoan = soTienKhoan;
            entity.IDHopDong = iDHopDong;
            entity.MaMid = maMid;
            entity.Ngay_THN_THX = ngay_THN_THX;
            entity.TrangThaiPhanBo = trangThaiPhanBo;
            entity.GUIDSTR = gUIDSTR;
            entity.DeXuatKhac = deXuatKhac;
            entity.LyDoSua = lyDoSua;
            entity.ActionStatus = actionStatus;
            entity.GuidReference = guidReference;
            entity.NamDK = namDK;
            entity.HUONGDAN = hUONGDAN;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object)NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, MaDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, TenDaiLyTTHQ);
            db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
            db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, ChiTietDonViDoiTac);
            db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, SoGiayPhep);
            db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, NgayGiayPhep.Year <= 1753 ? DBNull.Value : (object)NgayGiayPhep);
            db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, NgayHetHanGiayPhep.Year <= 1753 ? DBNull.Value : (object)NgayHetHanGiayPhep);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object)NgayHopDong);
            db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, NgayHetHanHopDong.Year <= 1753 ? DBNull.Value : (object)NgayHetHanHopDong);
            db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, SoHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, NgayHoaDonThuongMai.Year <= 1753 ? DBNull.Value : (object)NgayHoaDonThuongMai);
            db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, PTVT_ID);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, QuocTichPTVT_ID);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, LoaiVanDon);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, NuocXK_ID);
            db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, NuocNK_ID);
            db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, CuaKhau_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, TyGiaTinhThue);
            db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, SoHang);
            db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, SoLuongPLTK);
            db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, TenChuHang);
            db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, SoContainer20);
            db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, SoContainer40);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, TongTriGiaKhaiBao);
            db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, TongTriGiaTinhThue);
            db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, LoaiToKhaiGiaCong);
            db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, LePhiHaiQuan);
            db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, PhiBaoHiem);
            db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, PhiVanChuyen);
            db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, PhiXepDoHang);
            db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, PhiKhac);
            db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, CanBoDangKy);
            db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, QuanLyMay);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, LoaiHangHoa);
            db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, GiayTo);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, MaDonViUT);
            db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, TenDonViUT);
            db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, TrongLuongNet);
            db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, SoTienKhoan);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, MaMid);
            db.AddInParameter(dbCommand, "@Ngay_THN_THX", SqlDbType.DateTime, Ngay_THN_THX.Year <= 1753 ? DBNull.Value : (object)Ngay_THN_THX);
            db.AddInParameter(dbCommand, "@TrangThaiPhanBo", SqlDbType.Int, TrangThaiPhanBo);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<ToKhaiMauDich> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ToKhaiMauDich item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteToKhaiMauDich(long id)
        {
            ToKhaiMauDich entity = new ToKhaiMauDich();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_IDHopDong(long iDHopDong)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_DeleteBy_IDHopDong]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, iDHopDong);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<ToKhaiMauDich> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ToKhaiMauDich item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}