-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Insert]
	@Master_ID bigint,
	@SoTTVanDon varchar(1),
	@SoVanDon varchar(35),
	@NgayPhatHanhVD datetime,
	@MoTaHangHoa nvarchar(70),
	@MaHS varchar(4),
	@KyHieuVaSoHieu varchar(140),
	@NgayNhapKhoHQLanDau datetime,
	@PhanLoaiSanPhan varchar(1),
	@MaNuocSanXuat varchar(2),
	@TenNuocSanXuat varchar(7),
	@MaDiaDiemXuatPhatVC varchar(6),
	@TenDiaDiemXuatPhatVC varchar(35),
	@MaDiaDiemDichVC varchar(6),
	@TenDiaDiemDichVC varchar(35),
	@LoaiHangHoa varchar(1),
	@MaPhuongTienVC varchar(40),
	@TenPhuongTienVC varchar(40),
	@NgayHangDuKienDenDi datetime,
	@MaNguoiNhapKhau varchar(13),
	@TenNguoiNhapKhau nvarchar(100),
	@DiaChiNguoiNhapKhau nvarchar(100),
	@MaNguoiXuatKhua varchar(13),
	@TenNguoiXuatKhau nvarchar(100),
	@DiaChiNguoiXuatKhau nvarchar(100),
	@MaNguoiUyThac varchar(13),
	@TenNguoiUyThac nvarchar(100),
	@DiaChiNguoiUyThac nvarchar(100),
	@MaVanBanPhapLuat1 varchar(2),
	@MaVanBanPhapLuat2 varchar(2),
	@MaVanBanPhapLuat3 varchar(2),
	@MaVanBanPhapLuat4 varchar(2),
	@MaVanBanPhapLuat5 varchar(2),
	@MaDVTTriGia varchar(3),
	@TriGia numeric(20, 4),
	@SoLuong numeric(8, 4),
	@MaDVTSoLuong varchar(3),
	@TongTrongLuong numeric(10, 4),
	@MaDVTTrongLuong varchar(3),
	@TheTich numeric(10, 4),
	@MaDVTTheTich varchar(3),
	@MaDanhDauDDKH1 varchar(5),
	@MaDanhDauDDKH2 varchar(5),
	@MaDanhDauDDKH3 varchar(5),
	@MaDanhDauDDKH4 varchar(5),
	@MaDanhDauDDKH5 varchar(5),
	@SoGiayPhep varchar(11),
	@NgayCapPhep datetime,
	@NgayHetHanCapPhep datetime,
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TKVC_VanDon]
(
	[Master_ID],
	[SoTTVanDon],
	[SoVanDon],
	[NgayPhatHanhVD],
	[MoTaHangHoa],
	[MaHS],
	[KyHieuVaSoHieu],
	[NgayNhapKhoHQLanDau],
	[PhanLoaiSanPhan],
	[MaNuocSanXuat],
	[TenNuocSanXuat],
	[MaDiaDiemXuatPhatVC],
	[TenDiaDiemXuatPhatVC],
	[MaDiaDiemDichVC],
	[TenDiaDiemDichVC],
	[LoaiHangHoa],
	[MaPhuongTienVC],
	[TenPhuongTienVC],
	[NgayHangDuKienDenDi],
	[MaNguoiNhapKhau],
	[TenNguoiNhapKhau],
	[DiaChiNguoiNhapKhau],
	[MaNguoiXuatKhua],
	[TenNguoiXuatKhau],
	[DiaChiNguoiXuatKhau],
	[MaNguoiUyThac],
	[TenNguoiUyThac],
	[DiaChiNguoiUyThac],
	[MaVanBanPhapLuat1],
	[MaVanBanPhapLuat2],
	[MaVanBanPhapLuat3],
	[MaVanBanPhapLuat4],
	[MaVanBanPhapLuat5],
	[MaDVTTriGia],
	[TriGia],
	[SoLuong],
	[MaDVTSoLuong],
	[TongTrongLuong],
	[MaDVTTrongLuong],
	[TheTich],
	[MaDVTTheTich],
	[MaDanhDauDDKH1],
	[MaDanhDauDDKH2],
	[MaDanhDauDDKH3],
	[MaDanhDauDDKH4],
	[MaDanhDauDDKH5],
	[SoGiayPhep],
	[NgayCapPhep],
	[NgayHetHanCapPhep],
	[GhiChu]
)
VALUES 
(
	@Master_ID,
	@SoTTVanDon,
	@SoVanDon,
	@NgayPhatHanhVD,
	@MoTaHangHoa,
	@MaHS,
	@KyHieuVaSoHieu,
	@NgayNhapKhoHQLanDau,
	@PhanLoaiSanPhan,
	@MaNuocSanXuat,
	@TenNuocSanXuat,
	@MaDiaDiemXuatPhatVC,
	@TenDiaDiemXuatPhatVC,
	@MaDiaDiemDichVC,
	@TenDiaDiemDichVC,
	@LoaiHangHoa,
	@MaPhuongTienVC,
	@TenPhuongTienVC,
	@NgayHangDuKienDenDi,
	@MaNguoiNhapKhau,
	@TenNguoiNhapKhau,
	@DiaChiNguoiNhapKhau,
	@MaNguoiXuatKhua,
	@TenNguoiXuatKhau,
	@DiaChiNguoiXuatKhau,
	@MaNguoiUyThac,
	@TenNguoiUyThac,
	@DiaChiNguoiUyThac,
	@MaVanBanPhapLuat1,
	@MaVanBanPhapLuat2,
	@MaVanBanPhapLuat3,
	@MaVanBanPhapLuat4,
	@MaVanBanPhapLuat5,
	@MaDVTTriGia,
	@TriGia,
	@SoLuong,
	@MaDVTSoLuong,
	@TongTrongLuong,
	@MaDVTTrongLuong,
	@TheTich,
	@MaDVTTheTich,
	@MaDanhDauDDKH1,
	@MaDanhDauDDKH2,
	@MaDanhDauDDKH3,
	@MaDanhDauDDKH4,
	@MaDanhDauDDKH5,
	@SoGiayPhep,
	@NgayCapPhep,
	@NgayHetHanCapPhep,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoTTVanDon varchar(1),
	@SoVanDon varchar(35),
	@NgayPhatHanhVD datetime,
	@MoTaHangHoa nvarchar(70),
	@MaHS varchar(4),
	@KyHieuVaSoHieu varchar(140),
	@NgayNhapKhoHQLanDau datetime,
	@PhanLoaiSanPhan varchar(1),
	@MaNuocSanXuat varchar(2),
	@TenNuocSanXuat varchar(7),
	@MaDiaDiemXuatPhatVC varchar(6),
	@TenDiaDiemXuatPhatVC varchar(35),
	@MaDiaDiemDichVC varchar(6),
	@TenDiaDiemDichVC varchar(35),
	@LoaiHangHoa varchar(1),
	@MaPhuongTienVC varchar(40),
	@TenPhuongTienVC varchar(40),
	@NgayHangDuKienDenDi datetime,
	@MaNguoiNhapKhau varchar(13),
	@TenNguoiNhapKhau nvarchar(100),
	@DiaChiNguoiNhapKhau nvarchar(100),
	@MaNguoiXuatKhua varchar(13),
	@TenNguoiXuatKhau nvarchar(100),
	@DiaChiNguoiXuatKhau nvarchar(100),
	@MaNguoiUyThac varchar(13),
	@TenNguoiUyThac nvarchar(100),
	@DiaChiNguoiUyThac nvarchar(100),
	@MaVanBanPhapLuat1 varchar(2),
	@MaVanBanPhapLuat2 varchar(2),
	@MaVanBanPhapLuat3 varchar(2),
	@MaVanBanPhapLuat4 varchar(2),
	@MaVanBanPhapLuat5 varchar(2),
	@MaDVTTriGia varchar(3),
	@TriGia numeric(20, 4),
	@SoLuong numeric(8, 4),
	@MaDVTSoLuong varchar(3),
	@TongTrongLuong numeric(10, 4),
	@MaDVTTrongLuong varchar(3),
	@TheTich numeric(10, 4),
	@MaDVTTheTich varchar(3),
	@MaDanhDauDDKH1 varchar(5),
	@MaDanhDauDDKH2 varchar(5),
	@MaDanhDauDDKH3 varchar(5),
	@MaDanhDauDDKH4 varchar(5),
	@MaDanhDauDDKH5 varchar(5),
	@SoGiayPhep varchar(11),
	@NgayCapPhep datetime,
	@NgayHetHanCapPhep datetime,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TKVC_VanDon]
SET
	[Master_ID] = @Master_ID,
	[SoTTVanDon] = @SoTTVanDon,
	[SoVanDon] = @SoVanDon,
	[NgayPhatHanhVD] = @NgayPhatHanhVD,
	[MoTaHangHoa] = @MoTaHangHoa,
	[MaHS] = @MaHS,
	[KyHieuVaSoHieu] = @KyHieuVaSoHieu,
	[NgayNhapKhoHQLanDau] = @NgayNhapKhoHQLanDau,
	[PhanLoaiSanPhan] = @PhanLoaiSanPhan,
	[MaNuocSanXuat] = @MaNuocSanXuat,
	[TenNuocSanXuat] = @TenNuocSanXuat,
	[MaDiaDiemXuatPhatVC] = @MaDiaDiemXuatPhatVC,
	[TenDiaDiemXuatPhatVC] = @TenDiaDiemXuatPhatVC,
	[MaDiaDiemDichVC] = @MaDiaDiemDichVC,
	[TenDiaDiemDichVC] = @TenDiaDiemDichVC,
	[LoaiHangHoa] = @LoaiHangHoa,
	[MaPhuongTienVC] = @MaPhuongTienVC,
	[TenPhuongTienVC] = @TenPhuongTienVC,
	[NgayHangDuKienDenDi] = @NgayHangDuKienDenDi,
	[MaNguoiNhapKhau] = @MaNguoiNhapKhau,
	[TenNguoiNhapKhau] = @TenNguoiNhapKhau,
	[DiaChiNguoiNhapKhau] = @DiaChiNguoiNhapKhau,
	[MaNguoiXuatKhua] = @MaNguoiXuatKhua,
	[TenNguoiXuatKhau] = @TenNguoiXuatKhau,
	[DiaChiNguoiXuatKhau] = @DiaChiNguoiXuatKhau,
	[MaNguoiUyThac] = @MaNguoiUyThac,
	[TenNguoiUyThac] = @TenNguoiUyThac,
	[DiaChiNguoiUyThac] = @DiaChiNguoiUyThac,
	[MaVanBanPhapLuat1] = @MaVanBanPhapLuat1,
	[MaVanBanPhapLuat2] = @MaVanBanPhapLuat2,
	[MaVanBanPhapLuat3] = @MaVanBanPhapLuat3,
	[MaVanBanPhapLuat4] = @MaVanBanPhapLuat4,
	[MaVanBanPhapLuat5] = @MaVanBanPhapLuat5,
	[MaDVTTriGia] = @MaDVTTriGia,
	[TriGia] = @TriGia,
	[SoLuong] = @SoLuong,
	[MaDVTSoLuong] = @MaDVTSoLuong,
	[TongTrongLuong] = @TongTrongLuong,
	[MaDVTTrongLuong] = @MaDVTTrongLuong,
	[TheTich] = @TheTich,
	[MaDVTTheTich] = @MaDVTTheTich,
	[MaDanhDauDDKH1] = @MaDanhDauDDKH1,
	[MaDanhDauDDKH2] = @MaDanhDauDDKH2,
	[MaDanhDauDDKH3] = @MaDanhDauDDKH3,
	[MaDanhDauDDKH4] = @MaDanhDauDDKH4,
	[MaDanhDauDDKH5] = @MaDanhDauDDKH5,
	[SoGiayPhep] = @SoGiayPhep,
	[NgayCapPhep] = @NgayCapPhep,
	[NgayHetHanCapPhep] = @NgayHetHanCapPhep,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoTTVanDon varchar(1),
	@SoVanDon varchar(35),
	@NgayPhatHanhVD datetime,
	@MoTaHangHoa nvarchar(70),
	@MaHS varchar(4),
	@KyHieuVaSoHieu varchar(140),
	@NgayNhapKhoHQLanDau datetime,
	@PhanLoaiSanPhan varchar(1),
	@MaNuocSanXuat varchar(2),
	@TenNuocSanXuat varchar(7),
	@MaDiaDiemXuatPhatVC varchar(6),
	@TenDiaDiemXuatPhatVC varchar(35),
	@MaDiaDiemDichVC varchar(6),
	@TenDiaDiemDichVC varchar(35),
	@LoaiHangHoa varchar(1),
	@MaPhuongTienVC varchar(40),
	@TenPhuongTienVC varchar(40),
	@NgayHangDuKienDenDi datetime,
	@MaNguoiNhapKhau varchar(13),
	@TenNguoiNhapKhau nvarchar(100),
	@DiaChiNguoiNhapKhau nvarchar(100),
	@MaNguoiXuatKhua varchar(13),
	@TenNguoiXuatKhau nvarchar(100),
	@DiaChiNguoiXuatKhau nvarchar(100),
	@MaNguoiUyThac varchar(13),
	@TenNguoiUyThac nvarchar(100),
	@DiaChiNguoiUyThac nvarchar(100),
	@MaVanBanPhapLuat1 varchar(2),
	@MaVanBanPhapLuat2 varchar(2),
	@MaVanBanPhapLuat3 varchar(2),
	@MaVanBanPhapLuat4 varchar(2),
	@MaVanBanPhapLuat5 varchar(2),
	@MaDVTTriGia varchar(3),
	@TriGia numeric(20, 4),
	@SoLuong numeric(8, 4),
	@MaDVTSoLuong varchar(3),
	@TongTrongLuong numeric(10, 4),
	@MaDVTTrongLuong varchar(3),
	@TheTich numeric(10, 4),
	@MaDVTTheTich varchar(3),
	@MaDanhDauDDKH1 varchar(5),
	@MaDanhDauDDKH2 varchar(5),
	@MaDanhDauDDKH3 varchar(5),
	@MaDanhDauDDKH4 varchar(5),
	@MaDanhDauDDKH5 varchar(5),
	@SoGiayPhep varchar(11),
	@NgayCapPhep datetime,
	@NgayHetHanCapPhep datetime,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TKVC_VanDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TKVC_VanDon] 
		SET
			[Master_ID] = @Master_ID,
			[SoTTVanDon] = @SoTTVanDon,
			[SoVanDon] = @SoVanDon,
			[NgayPhatHanhVD] = @NgayPhatHanhVD,
			[MoTaHangHoa] = @MoTaHangHoa,
			[MaHS] = @MaHS,
			[KyHieuVaSoHieu] = @KyHieuVaSoHieu,
			[NgayNhapKhoHQLanDau] = @NgayNhapKhoHQLanDau,
			[PhanLoaiSanPhan] = @PhanLoaiSanPhan,
			[MaNuocSanXuat] = @MaNuocSanXuat,
			[TenNuocSanXuat] = @TenNuocSanXuat,
			[MaDiaDiemXuatPhatVC] = @MaDiaDiemXuatPhatVC,
			[TenDiaDiemXuatPhatVC] = @TenDiaDiemXuatPhatVC,
			[MaDiaDiemDichVC] = @MaDiaDiemDichVC,
			[TenDiaDiemDichVC] = @TenDiaDiemDichVC,
			[LoaiHangHoa] = @LoaiHangHoa,
			[MaPhuongTienVC] = @MaPhuongTienVC,
			[TenPhuongTienVC] = @TenPhuongTienVC,
			[NgayHangDuKienDenDi] = @NgayHangDuKienDenDi,
			[MaNguoiNhapKhau] = @MaNguoiNhapKhau,
			[TenNguoiNhapKhau] = @TenNguoiNhapKhau,
			[DiaChiNguoiNhapKhau] = @DiaChiNguoiNhapKhau,
			[MaNguoiXuatKhua] = @MaNguoiXuatKhua,
			[TenNguoiXuatKhau] = @TenNguoiXuatKhau,
			[DiaChiNguoiXuatKhau] = @DiaChiNguoiXuatKhau,
			[MaNguoiUyThac] = @MaNguoiUyThac,
			[TenNguoiUyThac] = @TenNguoiUyThac,
			[DiaChiNguoiUyThac] = @DiaChiNguoiUyThac,
			[MaVanBanPhapLuat1] = @MaVanBanPhapLuat1,
			[MaVanBanPhapLuat2] = @MaVanBanPhapLuat2,
			[MaVanBanPhapLuat3] = @MaVanBanPhapLuat3,
			[MaVanBanPhapLuat4] = @MaVanBanPhapLuat4,
			[MaVanBanPhapLuat5] = @MaVanBanPhapLuat5,
			[MaDVTTriGia] = @MaDVTTriGia,
			[TriGia] = @TriGia,
			[SoLuong] = @SoLuong,
			[MaDVTSoLuong] = @MaDVTSoLuong,
			[TongTrongLuong] = @TongTrongLuong,
			[MaDVTTrongLuong] = @MaDVTTrongLuong,
			[TheTich] = @TheTich,
			[MaDVTTheTich] = @MaDVTTheTich,
			[MaDanhDauDDKH1] = @MaDanhDauDDKH1,
			[MaDanhDauDDKH2] = @MaDanhDauDDKH2,
			[MaDanhDauDDKH3] = @MaDanhDauDDKH3,
			[MaDanhDauDDKH4] = @MaDanhDauDDKH4,
			[MaDanhDauDDKH5] = @MaDanhDauDDKH5,
			[SoGiayPhep] = @SoGiayPhep,
			[NgayCapPhep] = @NgayCapPhep,
			[NgayHetHanCapPhep] = @NgayHetHanCapPhep,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TKVC_VanDon]
		(
			[Master_ID],
			[SoTTVanDon],
			[SoVanDon],
			[NgayPhatHanhVD],
			[MoTaHangHoa],
			[MaHS],
			[KyHieuVaSoHieu],
			[NgayNhapKhoHQLanDau],
			[PhanLoaiSanPhan],
			[MaNuocSanXuat],
			[TenNuocSanXuat],
			[MaDiaDiemXuatPhatVC],
			[TenDiaDiemXuatPhatVC],
			[MaDiaDiemDichVC],
			[TenDiaDiemDichVC],
			[LoaiHangHoa],
			[MaPhuongTienVC],
			[TenPhuongTienVC],
			[NgayHangDuKienDenDi],
			[MaNguoiNhapKhau],
			[TenNguoiNhapKhau],
			[DiaChiNguoiNhapKhau],
			[MaNguoiXuatKhua],
			[TenNguoiXuatKhau],
			[DiaChiNguoiXuatKhau],
			[MaNguoiUyThac],
			[TenNguoiUyThac],
			[DiaChiNguoiUyThac],
			[MaVanBanPhapLuat1],
			[MaVanBanPhapLuat2],
			[MaVanBanPhapLuat3],
			[MaVanBanPhapLuat4],
			[MaVanBanPhapLuat5],
			[MaDVTTriGia],
			[TriGia],
			[SoLuong],
			[MaDVTSoLuong],
			[TongTrongLuong],
			[MaDVTTrongLuong],
			[TheTich],
			[MaDVTTheTich],
			[MaDanhDauDDKH1],
			[MaDanhDauDDKH2],
			[MaDanhDauDDKH3],
			[MaDanhDauDDKH4],
			[MaDanhDauDDKH5],
			[SoGiayPhep],
			[NgayCapPhep],
			[NgayHetHanCapPhep],
			[GhiChu]
		)
		VALUES 
		(
			@Master_ID,
			@SoTTVanDon,
			@SoVanDon,
			@NgayPhatHanhVD,
			@MoTaHangHoa,
			@MaHS,
			@KyHieuVaSoHieu,
			@NgayNhapKhoHQLanDau,
			@PhanLoaiSanPhan,
			@MaNuocSanXuat,
			@TenNuocSanXuat,
			@MaDiaDiemXuatPhatVC,
			@TenDiaDiemXuatPhatVC,
			@MaDiaDiemDichVC,
			@TenDiaDiemDichVC,
			@LoaiHangHoa,
			@MaPhuongTienVC,
			@TenPhuongTienVC,
			@NgayHangDuKienDenDi,
			@MaNguoiNhapKhau,
			@TenNguoiNhapKhau,
			@DiaChiNguoiNhapKhau,
			@MaNguoiXuatKhua,
			@TenNguoiXuatKhau,
			@DiaChiNguoiXuatKhau,
			@MaNguoiUyThac,
			@TenNguoiUyThac,
			@DiaChiNguoiUyThac,
			@MaVanBanPhapLuat1,
			@MaVanBanPhapLuat2,
			@MaVanBanPhapLuat3,
			@MaVanBanPhapLuat4,
			@MaVanBanPhapLuat5,
			@MaDVTTriGia,
			@TriGia,
			@SoLuong,
			@MaDVTSoLuong,
			@TongTrongLuong,
			@MaDVTTrongLuong,
			@TheTich,
			@MaDVTTheTich,
			@MaDanhDauDDKH1,
			@MaDanhDauDDKH2,
			@MaDanhDauDDKH3,
			@MaDanhDauDDKH4,
			@MaDanhDauDDKH5,
			@SoGiayPhep,
			@NgayCapPhep,
			@NgayHetHanCapPhep,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TKVC_VanDon]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TKVC_VanDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoTTVanDon],
	[SoVanDon],
	[NgayPhatHanhVD],
	[MoTaHangHoa],
	[MaHS],
	[KyHieuVaSoHieu],
	[NgayNhapKhoHQLanDau],
	[PhanLoaiSanPhan],
	[MaNuocSanXuat],
	[TenNuocSanXuat],
	[MaDiaDiemXuatPhatVC],
	[TenDiaDiemXuatPhatVC],
	[MaDiaDiemDichVC],
	[TenDiaDiemDichVC],
	[LoaiHangHoa],
	[MaPhuongTienVC],
	[TenPhuongTienVC],
	[NgayHangDuKienDenDi],
	[MaNguoiNhapKhau],
	[TenNguoiNhapKhau],
	[DiaChiNguoiNhapKhau],
	[MaNguoiXuatKhua],
	[TenNguoiXuatKhau],
	[DiaChiNguoiXuatKhau],
	[MaNguoiUyThac],
	[TenNguoiUyThac],
	[DiaChiNguoiUyThac],
	[MaVanBanPhapLuat1],
	[MaVanBanPhapLuat2],
	[MaVanBanPhapLuat3],
	[MaVanBanPhapLuat4],
	[MaVanBanPhapLuat5],
	[MaDVTTriGia],
	[TriGia],
	[SoLuong],
	[MaDVTSoLuong],
	[TongTrongLuong],
	[MaDVTTrongLuong],
	[TheTich],
	[MaDVTTheTich],
	[MaDanhDauDDKH1],
	[MaDanhDauDDKH2],
	[MaDanhDauDDKH3],
	[MaDanhDauDDKH4],
	[MaDanhDauDDKH5],
	[SoGiayPhep],
	[NgayCapPhep],
	[NgayHetHanCapPhep],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACC_TKVC_VanDon]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoTTVanDon],
	[SoVanDon],
	[NgayPhatHanhVD],
	[MoTaHangHoa],
	[MaHS],
	[KyHieuVaSoHieu],
	[NgayNhapKhoHQLanDau],
	[PhanLoaiSanPhan],
	[MaNuocSanXuat],
	[TenNuocSanXuat],
	[MaDiaDiemXuatPhatVC],
	[TenDiaDiemXuatPhatVC],
	[MaDiaDiemDichVC],
	[TenDiaDiemDichVC],
	[LoaiHangHoa],
	[MaPhuongTienVC],
	[TenPhuongTienVC],
	[NgayHangDuKienDenDi],
	[MaNguoiNhapKhau],
	[TenNguoiNhapKhau],
	[DiaChiNguoiNhapKhau],
	[MaNguoiXuatKhua],
	[TenNguoiXuatKhau],
	[DiaChiNguoiXuatKhau],
	[MaNguoiUyThac],
	[TenNguoiUyThac],
	[DiaChiNguoiUyThac],
	[MaVanBanPhapLuat1],
	[MaVanBanPhapLuat2],
	[MaVanBanPhapLuat3],
	[MaVanBanPhapLuat4],
	[MaVanBanPhapLuat5],
	[MaDVTTriGia],
	[TriGia],
	[SoLuong],
	[MaDVTSoLuong],
	[TongTrongLuong],
	[MaDVTTrongLuong],
	[TheTich],
	[MaDVTTheTich],
	[MaDanhDauDDKH1],
	[MaDanhDauDDKH2],
	[MaDanhDauDDKH3],
	[MaDanhDauDDKH4],
	[MaDanhDauDDKH5],
	[SoGiayPhep],
	[NgayCapPhep],
	[NgayHetHanCapPhep],
	[GhiChu]
FROM [dbo].[t_KDT_VNACC_TKVC_VanDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoTTVanDon],
	[SoVanDon],
	[NgayPhatHanhVD],
	[MoTaHangHoa],
	[MaHS],
	[KyHieuVaSoHieu],
	[NgayNhapKhoHQLanDau],
	[PhanLoaiSanPhan],
	[MaNuocSanXuat],
	[TenNuocSanXuat],
	[MaDiaDiemXuatPhatVC],
	[TenDiaDiemXuatPhatVC],
	[MaDiaDiemDichVC],
	[TenDiaDiemDichVC],
	[LoaiHangHoa],
	[MaPhuongTienVC],
	[TenPhuongTienVC],
	[NgayHangDuKienDenDi],
	[MaNguoiNhapKhau],
	[TenNguoiNhapKhau],
	[DiaChiNguoiNhapKhau],
	[MaNguoiXuatKhua],
	[TenNguoiXuatKhau],
	[DiaChiNguoiXuatKhau],
	[MaNguoiUyThac],
	[TenNguoiUyThac],
	[DiaChiNguoiUyThac],
	[MaVanBanPhapLuat1],
	[MaVanBanPhapLuat2],
	[MaVanBanPhapLuat3],
	[MaVanBanPhapLuat4],
	[MaVanBanPhapLuat5],
	[MaDVTTriGia],
	[TriGia],
	[SoLuong],
	[MaDVTSoLuong],
	[TongTrongLuong],
	[MaDVTTrongLuong],
	[TheTich],
	[MaDVTTheTich],
	[MaDanhDauDDKH1],
	[MaDanhDauDDKH2],
	[MaDanhDauDDKH3],
	[MaDanhDauDDKH4],
	[MaDanhDauDDKH5],
	[SoGiayPhep],
	[NgayCapPhep],
	[NgayHetHanCapPhep],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACC_TKVC_VanDon]	

GO

