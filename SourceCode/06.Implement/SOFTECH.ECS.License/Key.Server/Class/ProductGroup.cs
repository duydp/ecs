﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace KeyServer
{
    public partial class ProductGroup
    {
        public List<Product> ProductCollection { set; get; }
        public void LoadProduct()
        {
            if (this.ID > 0)
                this.ProductCollection = Product.SelectCollectionDynamic("Group_ID = " + this.ID, null);
        }
        public static List<ProductGroup> LoadByPermission(int PermissionID)
        {
            List<ProductGroup> collection = new List<ProductGroup>();

            const string spName = "[dbo].[p_LM_ProductGroup_LoadByPermission]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Permission", SqlDbType.BigInt, PermissionID);
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ProductGroup entity = new ProductGroup();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
                if (!reader.IsDBNull(reader.GetOrdinal("Description"))) entity.Description = reader.GetString(reader.GetOrdinal("Description"));
                if (!reader.IsDBNull(reader.GetOrdinal("Manager"))) entity.Manager = reader.GetString(reader.GetOrdinal("Manager"));
                if (!reader.IsDBNull(reader.GetOrdinal("Creator"))) entity.Creator = reader.GetString(reader.GetOrdinal("Creator"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("temp1"))) entity.temp1 = reader.GetString(reader.GetOrdinal("temp1"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;		
        }
    }
}
