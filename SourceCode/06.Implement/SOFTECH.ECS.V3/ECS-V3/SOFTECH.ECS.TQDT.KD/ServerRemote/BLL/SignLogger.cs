﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace ServerRemote.BLL
{
    public partial class SignLogger
    {

        public static List<SignLogger> SelectTop50()
        {
            string querry = "select top 50 * from t_SignLogger order by ID desc";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbcommand = (SqlCommand)db.GetSqlStringCommand(querry);
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbcommand);
            List<SignLogger> collection = new List<SignLogger>();
            while (reader.Read())
            {
                SignLogger entity = new SignLogger();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TIME_PROCESS"))) entity.TIME_PROCESS = reader.GetDateTime(reader.GetOrdinal("TIME_PROCESS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MESSAGE"))) entity.MESSAGE = reader.GetString(reader.GetOrdinal("MESSAGE"));
                if (!reader.IsDBNull(reader.GetOrdinal("DATECREATE"))) entity.DATECREATE = reader.GetDateTime(reader.GetOrdinal("DATECREATE"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;			

        }
       
   

    }
}
