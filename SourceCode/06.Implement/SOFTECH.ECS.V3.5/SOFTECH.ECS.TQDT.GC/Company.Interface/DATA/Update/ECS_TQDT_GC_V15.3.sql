
Delete t_VNACC_Category_Cargo where BondedAreaCode='34CESCE'

INSERT INTO t_VNACC_Category_Cargo(TableID,[BondedAreaCode],[BondedAreaName]) VALUES('A202A','34CESCE',N'CCHQ CANG DA NANG')

-- Update ma loai Hinh

update t_VNACC_Category_Common set Name_VN=N'Nhập kinh doanh tiêu dùng' where ReferenceDB='E001' and Code ='A11'
update t_VNACC_Category_Common set Name_VN=N'Nhập kinh doanh sản xuất' where ReferenceDB='E001' and Code ='A12'
update t_VNACC_Category_Common set Name_VN=N'Chuyển tiêu thụ nội địa từ nguồn tạm nhập' where ReferenceDB='E001' and Code ='A21'
update t_VNACC_Category_Common set Name_VN=N'Nhập hàng XK bị trả lại' where ReferenceDB='E001' and Code ='A31'
update t_VNACC_Category_Common set Name_VN=N'Nhập kinh doanh của doanh nghiệp đầu tư' where ReferenceDB='E001' and Code ='A41'
update t_VNACC_Category_Common set Name_VN=N'Chuyển tiêu thụ nội địa khác' where ReferenceDB='E001' and Code ='A42'
update t_VNACC_Category_Common set Name_VN=N'Không dùng/dự phòng' where ReferenceDB='E001' and Code ='A43'
update t_VNACC_Category_Common set Name_VN=N'Nhập vào khu phi thuế quan từ nội địa' where ReferenceDB='E001' and Code ='A44  '
update t_VNACC_Category_Common set Name_VN=N'Loại hình dành cho doanh nghiệp ưu tiên' where ReferenceDB='E001' and Code ='AEO'
update t_VNACC_Category_Common set Name_VN=N'Xuất kinh doanh, Xuất khẩu của doanh nghiệp đầu tư' where ReferenceDB='E001' and Code ='B11'
update t_VNACC_Category_Common set Name_VN=N'Xuất sau khi đã tạm xuất' where ReferenceDB='E001' and Code ='B12'
update t_VNACC_Category_Common set Name_VN=N'Xuất trả hàng nhập khẩu' where ReferenceDB='E001' and Code ='B13'
update t_VNACC_Category_Common set Name_VN=N'Hàng gửi kho ngoại quan' where ReferenceDB='E001' and Code ='C11  '
update t_VNACC_Category_Common set Name_VN=N'Hàng xuất kho ngoại quan' where ReferenceDB='E001' and Code ='C12  '
update t_VNACC_Category_Common set Name_VN=N'Hàng đưa vào khu phi thuế quan' where ReferenceDB='E001' and Code ='C21  '
update t_VNACC_Category_Common set Name_VN=N'Xuất nguyên liệu gia công từ hợp đồng khác sang' where ReferenceDB='E001' and Code ='E54'
update t_VNACC_Category_Common set Name_VN=N'Xuất sản phẩm gia công giao hàng tại nội địa' where ReferenceDB='E001' and Code ='E56'
update t_VNACC_Category_Common set Name_VN=N'Xuất sản phẩm Sản xuất xuất khẩu' where ReferenceDB='E001' and Code ='E62'
update t_VNACC_Category_Common set Name_VN=N'Không dùng/dự phòng' where ReferenceDB='E001' and Code ='E64'
update t_VNACC_Category_Common set Name_VN=N'Xuất nguyên liệu, vật tư thuê gia công ở nước ngoài' where ReferenceDB='E001' and Code ='E82'
update t_VNACC_Category_Common set Name_VN=N'Tạm nhập hàng kinh doanh tạm nhập tái xuất' where ReferenceDB='E001' and Code ='G11'
update t_VNACC_Category_Common set Name_VN=N'Tạm nhập máy móc, thiết bị phục vụ thực hiện các dự án có thời hạn' where ReferenceDB='E001' and Code ='G12'
update t_VNACC_Category_Common set Name_VN=N'Tạm nhập miễn thuế' where ReferenceDB='E001' and Code ='G13'
update t_VNACC_Category_Common set Name_VN=N'Tạm nhập khác' where ReferenceDB='E001' and Code ='G14'
update t_VNACC_Category_Common set Name_VN=N'Tái xuất hàng kinh doanh tạm nhập tái xuất' where ReferenceDB='E001' and Code ='G21'
update t_VNACC_Category_Common set Name_VN=N'Tái xuất thiết bị, máy móc phục vụ dự án có thời hạn' where ReferenceDB='E001' and Code ='G22'
update t_VNACC_Category_Common set Name_VN=N'Tái xuất miễn thuế hàng tạm nhập' where ReferenceDB='E001' and Code ='G23'
update t_VNACC_Category_Common set Name_VN=N'Tái xuất khác' where ReferenceDB='E001' and Code ='G24'
update t_VNACC_Category_Common set Name_VN=N'Tái nhập hàng đã tạm xuất' where ReferenceDB='E001' and Code ='G51'
update t_VNACC_Category_Common set Name_VN=N'Tạm xuất hàng hóa' where ReferenceDB='E001' and Code ='G61'
update t_VNACC_Category_Common set Name_VN=N'Hàng đưa ra khỏi khu phi thuế quan' where ReferenceDB='E001' and Code ='C22  '
update t_VNACC_Category_Common set Name_VN=N'Nhập nguyên liệu của doanh nghiệp chế xuất' where ReferenceDB='E001' and Code ='E11'
update t_VNACC_Category_Common set Name_VN=N'Nhập tạo tài sản cố định của doanh nghiệp chế xuất' where ReferenceDB='E001' and Code ='E13  '
update t_VNACC_Category_Common set Name_VN=N'Nhập nguyên liệu của doanh nghiệp chế xuất từ nội địa' where ReferenceDB='E001' and Code ='E15'
update t_VNACC_Category_Common set Name_VN=N'Nhập nguyên liệu để gia công cho thương nhân nước ngoài' where ReferenceDB='E001' and Code ='E21'
update t_VNACC_Category_Common set Name_VN=N'Nhập nguyên liệu gia công từ hợp đồng khác chuyển sang' where ReferenceDB='E001' and Code ='E23'
update t_VNACC_Category_Common set Name_VN=N'Không dùng/dự phòng' where ReferenceDB='E001' and Code ='E25'
update t_VNACC_Category_Common set Name_VN=N'Nhập nguyên liệu sản xuất xuất khẩu' where ReferenceDB='E001' and Code ='E31'
update t_VNACC_Category_Common set Name_VN=N'Không dùng/dự phòng' where ReferenceDB='E001' and Code ='E33'
update t_VNACC_Category_Common set Name_VN=N'Nhập sản phẩm thuê gia công ở nước ngoài' where ReferenceDB='E001' and Code ='E41'
update t_VNACC_Category_Common set Name_VN=N'Xuất sản phẩm của Doanh nghiệp chế xuất' where ReferenceDB='E001' and Code ='E42  '
update t_VNACC_Category_Common set Name_VN=N'Không dùng/dự phòng' where ReferenceDB='E001' and Code ='E44'
update t_VNACC_Category_Common set Name_VN=N'Hàng của Doanh nghiệp chế xuất vào nội địa để gia công' where ReferenceDB='E001' and Code ='E46'
update t_VNACC_Category_Common set Name_VN=N'Xuất sản phẩm gia công cho thương nhân nước ngoài' where ReferenceDB='E001' and Code ='E52'



IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '15.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('15.3', GETDATE(), N'Them moi du lieu cac danh muc Ma Loai Hinh VNACCS')
END	

