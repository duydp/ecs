namespace Licenses_v2.Module
{
    partial class FrmSerialInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSerialInfo));
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblCaption = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtSerEmployeeID = new DevExpress.XtraEditors.TextEdit();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnGenCode = new DevExpress.XtraEditors.SimpleButton();
            this.txtSerCreateDate = new DevExpress.XtraEditors.TextEdit();
            this.txtSerLicense = new DevExpress.XtraEditors.TextEdit();
            this.txtSerProduct = new DevExpress.XtraEditors.TextEdit();
            this.checkSerStatus = new DevExpress.XtraEditors.CheckEdit();
            this.cboxSerLicense = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gNClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gTerm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cboxSerProduct = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.g1ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.g1Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtSerSerial = new DevExpress.XtraEditors.TextEdit();
            this.groupSerialInfo = new DevExpress.XtraEditors.GroupControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerEmployeeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerCreateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerLicense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSerStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboxSerLicense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboxSerProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerSerial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupSerialInfo)).BeginInit();
            this.groupSerialInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelTop.Controls.Add(this.pnlTop);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(429, 67);
            this.panelTop.TabIndex = 3;
            // 
            // pnlTop
            // 
            this.pnlTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTop.BackgroundImage")));
            this.pnlTop.Controls.Add(this.lblCaption);
            this.pnlTop.Controls.Add(this.pictureBox1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(429, 67);
            this.pnlTop.TabIndex = 9;
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCaption.Appearance.Options.UseFont = true;
            this.lblCaption.Appearance.Options.UseForeColor = true;
            this.lblCaption.Location = new System.Drawing.Point(72, 8);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(190, 31);
            this.lblCaption.TabIndex = 2;
            this.lblCaption.Text = "Thông tin Serial";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(69, 67);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // txtSerEmployeeID
            // 
            this.txtSerEmployeeID.Location = new System.Drawing.Point(86, 114);
            this.txtSerEmployeeID.Name = "txtSerEmployeeID";
            this.txtSerEmployeeID.Properties.ReadOnly = true;
            this.txtSerEmployeeID.Size = new System.Drawing.Size(114, 20);
            this.txtSerEmployeeID.TabIndex = 13;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(356, 303);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(61, 22);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(280, 303);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(70, 22);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(218, 303);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(56, 22);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.Location = new System.Drawing.Point(156, 303);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(56, 22);
            this.btnEdit.TabIndex = 5;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnGenCode
            // 
            this.btnGenCode.Image = ((System.Drawing.Image)(resources.GetObject("btnGenCode.Image")));
            this.btnGenCode.Location = new System.Drawing.Point(242, 86);
            this.btnGenCode.Name = "btnGenCode";
            this.btnGenCode.Size = new System.Drawing.Size(76, 22);
            this.btnGenCode.TabIndex = 4;
            this.btnGenCode.Text = "Sinh mã";
            this.btnGenCode.Click += new System.EventHandler(this.btnGenCode_Click);
            // 
            // txtSerCreateDate
            // 
            this.txtSerCreateDate.Location = new System.Drawing.Point(86, 87);
            this.txtSerCreateDate.Name = "txtSerCreateDate";
            this.txtSerCreateDate.Properties.ReadOnly = true;
            this.txtSerCreateDate.Size = new System.Drawing.Size(114, 20);
            this.txtSerCreateDate.TabIndex = 12;
            // 
            // txtSerLicense
            // 
            this.txtSerLicense.Location = new System.Drawing.Point(86, 61);
            this.txtSerLicense.Name = "txtSerLicense";
            this.txtSerLicense.Properties.ReadOnly = true;
            this.txtSerLicense.Size = new System.Drawing.Size(114, 20);
            this.txtSerLicense.TabIndex = 11;
            // 
            // txtSerProduct
            // 
            this.txtSerProduct.Location = new System.Drawing.Point(86, 35);
            this.txtSerProduct.Name = "txtSerProduct";
            this.txtSerProduct.Properties.ReadOnly = true;
            this.txtSerProduct.Size = new System.Drawing.Size(114, 20);
            this.txtSerProduct.TabIndex = 10;
            // 
            // checkSerStatus
            // 
            this.checkSerStatus.Location = new System.Drawing.Point(84, 140);
            this.checkSerStatus.Name = "checkSerStatus";
            this.checkSerStatus.Properties.Caption = "Trạng thái";
            this.checkSerStatus.Size = new System.Drawing.Size(72, 19);
            this.checkSerStatus.TabIndex = 3;
            // 
            // cboxSerLicense
            // 
            this.cboxSerLicense.Location = new System.Drawing.Point(206, 61);
            this.cboxSerLicense.Name = "cboxSerLicense";
            this.cboxSerLicense.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboxSerLicense.Properties.HideSelection = false;
            this.cboxSerLicense.Properties.NullText = "(Chưa chọn bản quyền)";
            this.cboxSerLicense.Properties.View = this.gridLookUpEdit2View;
            this.cboxSerLicense.Size = new System.Drawing.Size(187, 20);
            this.cboxSerLicense.TabIndex = 2;
            this.cboxSerLicense.EditValueChanged += new System.EventHandler(this.cboxSerLicense_EditValueChanged);
            // 
            // gridLookUpEdit2View
            // 
            this.gridLookUpEdit2View.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridLookUpEdit2View.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridLookUpEdit2View.Appearance.Preview.BackColor = System.Drawing.Color.OldLace;
            this.gridLookUpEdit2View.Appearance.Preview.Options.UseBackColor = true;
            this.gridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gID,
            this.gName,
            this.gNClient,
            this.gTerm});
            this.gridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit2View.GroupPanelText = "Chọn trường di chuyển lên đây để nhóm";
            this.gridLookUpEdit2View.Name = "gridLookUpEdit2View";
            this.gridLookUpEdit2View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit2View.OptionsView.ShowChildrenInGroupPanel = true;
            this.gridLookUpEdit2View.OptionsView.ShowGroupedColumns = true;
            this.gridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit2View.OptionsView.ShowPreview = true;
            this.gridLookUpEdit2View.PreviewFieldName = "Description";
            // 
            // gID
            // 
            this.gID.Caption = "Mã";
            this.gID.FieldName = "ID";
            this.gID.Name = "gID";
            this.gID.Visible = true;
            this.gID.VisibleIndex = 0;
            this.gID.Width = 66;
            // 
            // gName
            // 
            this.gName.Caption = "Tên";
            this.gName.FieldName = "Name";
            this.gName.Name = "gName";
            this.gName.Visible = true;
            this.gName.VisibleIndex = 1;
            this.gName.Width = 119;
            // 
            // gNClient
            // 
            this.gNClient.Caption = "Số máy";
            this.gNClient.FieldName = "NumberClient";
            this.gNClient.Name = "gNClient";
            this.gNClient.Visible = true;
            this.gNClient.VisibleIndex = 2;
            this.gNClient.Width = 80;
            // 
            // gTerm
            // 
            this.gTerm.Caption = "Thời hạn";
            this.gTerm.FieldName = "Term";
            this.gTerm.Name = "gTerm";
            this.gTerm.Visible = true;
            this.gTerm.VisibleIndex = 3;
            this.gTerm.Width = 80;
            // 
            // cboxSerProduct
            // 
            this.cboxSerProduct.Location = new System.Drawing.Point(206, 35);
            this.cboxSerProduct.Name = "cboxSerProduct";
            this.cboxSerProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboxSerProduct.Properties.HideSelection = false;
            this.cboxSerProduct.Properties.NullText = "(Chưa chọn sản phẩm)";
            this.cboxSerProduct.Properties.View = this.gridLookUpEdit1View;
            this.cboxSerProduct.Size = new System.Drawing.Size(187, 20);
            this.cboxSerProduct.TabIndex = 1;
            this.cboxSerProduct.EditValueChanged += new System.EventHandler(this.cboxSerProduct_EditValueChanged);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridLookUpEdit1View.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridLookUpEdit1View.Appearance.Preview.BackColor = System.Drawing.Color.OldLace;
            this.gridLookUpEdit1View.Appearance.Preview.Options.UseBackColor = true;
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.g1ID,
            this.g1Name});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.GroupPanelText = "Di chuyển trường lên đây đế nhóm";
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowChildrenInGroupPanel = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupedColumns = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowPreview = true;
            this.gridLookUpEdit1View.PreviewFieldName = "Description";
            // 
            // g1ID
            // 
            this.g1ID.Caption = "Mã:";
            this.g1ID.FieldName = "ID";
            this.g1ID.Name = "g1ID";
            this.g1ID.OptionsColumn.AllowEdit = false;
            this.g1ID.Visible = true;
            this.g1ID.VisibleIndex = 0;
            // 
            // g1Name
            // 
            this.g1Name.Caption = "Tên Sản phẩm";
            this.g1Name.FieldName = "Name";
            this.g1Name.Name = "g1Name";
            this.g1Name.OptionsColumn.AllowEdit = false;
            this.g1Name.Visible = true;
            this.g1Name.VisibleIndex = 1;
            this.g1Name.Width = 150;
            // 
            // txtSerSerial
            // 
            this.txtSerSerial.Location = new System.Drawing.Point(60, 87);
            this.txtSerSerial.Name = "txtSerSerial";
            this.txtSerSerial.Properties.ReadOnly = true;
            this.txtSerSerial.Size = new System.Drawing.Size(176, 20);
            this.txtSerSerial.TabIndex = 9;
            // 
            // groupSerialInfo
            // 
            this.groupSerialInfo.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupSerialInfo.AppearanceCaption.Options.UseFont = true;
            this.groupSerialInfo.Controls.Add(this.labelControl4);
            this.groupSerialInfo.Controls.Add(this.labelControl3);
            this.groupSerialInfo.Controls.Add(this.labelControl2);
            this.groupSerialInfo.Controls.Add(this.labelControl1);
            this.groupSerialInfo.Controls.Add(this.txtSerEmployeeID);
            this.groupSerialInfo.Controls.Add(this.txtSerProduct);
            this.groupSerialInfo.Controls.Add(this.txtSerCreateDate);
            this.groupSerialInfo.Controls.Add(this.checkSerStatus);
            this.groupSerialInfo.Controls.Add(this.cboxSerProduct);
            this.groupSerialInfo.Controls.Add(this.cboxSerLicense);
            this.groupSerialInfo.Controls.Add(this.txtSerLicense);
            this.groupSerialInfo.Location = new System.Drawing.Point(12, 132);
            this.groupSerialInfo.Name = "groupSerialInfo";
            this.groupSerialInfo.Size = new System.Drawing.Size(406, 165);
            this.groupSerialInfo.TabIndex = 17;
            this.groupSerialInfo.Text = "Thông tin Serial";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(16, 117);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(68, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Mã nhân viên:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(36, 90);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(48, 13);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Ngày tạo:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(29, 64);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(55, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Bản quyền:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(33, 38);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(51, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Sản phẩm:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(28, 90);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(26, 13);
            this.labelControl5.TabIndex = 18;
            this.labelControl5.Text = "Serial";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(60, 113);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(265, 13);
            this.labelControl6.TabIndex = 18;
            this.labelControl6.Text = "Chỉ sinh mã được khi đã hoàn thành thông tin bên dưới.";
            // 
            // FrmSerialInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 333);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.groupSerialInfo);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.btnGenCode);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtSerSerial);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnEdit);
            this.Name = "FrmSerialInfo";
            this.Text = "Thông tin Serial";
            this.Load += new System.EventHandler(this.FrmSerialInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerEmployeeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerCreateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerLicense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSerStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboxSerLicense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboxSerProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerSerial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupSerialInfo)).EndInit();
            this.groupSerialInfo.ResumeLayout(false);
            this.groupSerialInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.CheckEdit checkSerStatus;
        private DevExpress.XtraEditors.GridLookUpEdit cboxSerLicense;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit2View;
        private DevExpress.XtraEditors.GridLookUpEdit cboxSerProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.TextEdit txtSerSerial;
        private DevExpress.XtraEditors.TextEdit txtSerCreateDate;
        private DevExpress.XtraEditors.TextEdit txtSerLicense;
        private DevExpress.XtraEditors.TextEdit txtSerProduct;
        private DevExpress.XtraEditors.SimpleButton btnGenCode;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gID;
        private DevExpress.XtraGrid.Columns.GridColumn gName;
        private DevExpress.XtraGrid.Columns.GridColumn gNClient;
        private DevExpress.XtraGrid.Columns.GridColumn gTerm;
        private DevExpress.XtraGrid.Columns.GridColumn g1ID;
        private DevExpress.XtraGrid.Columns.GridColumn g1Name;
        private DevExpress.XtraEditors.TextEdit txtSerEmployeeID;
        private System.Windows.Forms.Panel pnlTop;
        private DevExpress.XtraEditors.LabelControl lblCaption;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.GroupControl groupSerialInfo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
    }
}