﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.KDT.SHARE.Components;
using Company.Controls;
using Company.Interface.TTDaiLy;

namespace Company.Interface
{
    public partial class FrmDaiLy : Form
    {
        protected MessageBoxControl _MsgBox;
        public DataSet _data_set = new DataSet();
        public static SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        public FrmDaiLy()
        {
            InitializeComponent();
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void FrmDaiLy_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            Company.KDT.SHARE.Components.HeThongPhongKhai htpk = new Company.KDT.SHARE.Components.HeThongPhongKhai();
            
            _data_set.Clear();
            _data_set = SelectAll();
            dgList.DataSource = _data_set;
            dgList.DataMember = _data_set.Tables[0].TableName;
           
            
        }

        public static DataSet SelectAll()
        {
            string query = "SELECT *  FROM t_HeThongPhongKhai where Key_Config='MA_HAI_QUAN'";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }
        public static DataSet SelectDaiLy()
        {
            string query = "SELECT *  FROM t_KDT ";
            DbCommand dbCommandKDT = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommandKDT);
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {

            FrmDaiLy_ChiTiet f = new FrmDaiLy_ChiTiet();
            f.ShowDialog();
            LoadData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string madoanhnghiep = dgList.CurrentRow.Cells["Ma"].Text;
            //DialogResult dialogResult = MessageBox.Show("Bạn muốn xóa Mã doanh nghiệp : " + madoanhnghiep + " này ?", "Xóa mã doanh nghiệp", MessageBoxButtons.YesNo);
            //MessageBoxButtons.YesNo("Bạn muốn xóa Ma doanh nghiệp : " + madoanhnghiep+" này ?");
            if (ShowMessage("Bạn muốn xóa Mã doanh nghiệp : " + madoanhnghiep + " này ?", true) == "Yes")
            {
                try
                {
                    DeleteDN(madoanhnghiep);
                    ShowMessage("Xóa mã doanh nghiệp : " + madoanhnghiep + " thành công.", false);

                }
                catch (Exception ex)
                {
                   ShowMessage("Xãy ra lỗi. /n//n" + ex, false);
                }

                LoadData();
            }
        }
        public static DataSet DeleteDN(string madoanhnghiep)
        {
            string query = "DELETE FROM t_HeThongPhongKhai WHERE MaDoanhNghiep='" + madoanhnghiep + "'";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }

        private void dgList_DoubleClick(object sender, EventArgs e)
        {
             string maDN = dgList.CurrentRow.Cells["Ma"].Text;
           
            FrmDaiLy_ChiTiet f = new FrmDaiLy_ChiTiet();
            f.MaDN = maDN;
            f.trangthai = 1;
            f.ShowDialog();
        }

        private void btnDaiLy_Click(object sender, EventArgs e)
        {
            FrmInfo frm = new FrmInfo();
            frm.trangthai = 1;
            frm.ShowDialog();
        }


      
    }
}
