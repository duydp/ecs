namespace Company.Interface.Report.SXXK
{
    partial class ToKhaiCT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiCT));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblNguyenTe2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguyenTe1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHetHanHDNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHDNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHopDongNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHetHanHDGiao = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHDGiao = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHopDongGiao = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemGiaoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiChiDinhNhanHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiChiDinhGiaoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiNhanHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiNhanHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiaoTB = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiaoNPL = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiaoSP = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNhanTB = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNhanNPL = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiGiaoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiGiaoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNhanSP = new DevExpress.XtraReports.UI.XRLabel();
            this.ptbImage = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNguyenTe2,
            this.lblNguyenTe1,
            this.lblNgayHetHanHDNhan,
            this.lblNgayHDNhan,
            this.lblSoHopDongNhan,
            this.lblNgayHetHanHDGiao,
            this.lblNgayHDGiao,
            this.lblSoHopDongGiao,
            this.lblMaDaiLyTTHQ,
            this.lblTenDaiLyTTHQ,
            this.lblDiaDiemGiaoHang,
            this.lblNguoiChiDinhNhanHang,
            this.lblNguoiChiDinhGiaoHang,
            this.lblTenNguoiNhanHang,
            this.lblMaNguoiNhanHang,
            this.lblGiaoTB,
            this.lblGiaoNPL,
            this.lblGiaoSP,
            this.lblNhanTB,
            this.lblNhanNPL,
            this.lblMaNguoiGiaoHang,
            this.lblTenNguoiGiaoHang,
            this.lblSoTiepNhan,
            this.xrTable1,
            this.lblNhanSP,
            this.ptbImage});
            this.Detail.Height = 1120;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNguyenTe2
            // 
            this.lblNguyenTe2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblNguyenTe2.Location = new System.Drawing.Point(675, 508);
            this.lblNguyenTe2.Multiline = true;
            this.lblNguyenTe2.Name = "lblNguyenTe2";
            this.lblNguyenTe2.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguyenTe2.Size = new System.Drawing.Size(92, 25);
            this.lblNguyenTe2.Text = "(USD)";
            this.lblNguyenTe2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNguyenTe1
            // 
            this.lblNguyenTe1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblNguyenTe1.Location = new System.Drawing.Point(550, 508);
            this.lblNguyenTe1.Multiline = true;
            this.lblNguyenTe1.Name = "lblNguyenTe1";
            this.lblNguyenTe1.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguyenTe1.Size = new System.Drawing.Size(92, 25);
            this.lblNguyenTe1.Text = "(USD)";
            this.lblNguyenTe1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHetHanHDNhan
            // 
            this.lblNgayHetHanHDNhan.Font = new System.Drawing.Font("Times New Roman", 6.5F);
            this.lblNgayHetHanHDNhan.Location = new System.Drawing.Point(567, 267);
            this.lblNgayHetHanHDNhan.Multiline = true;
            this.lblNgayHetHanHDNhan.Name = "lblNgayHetHanHDNhan";
            this.lblNgayHetHanHDNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNgayHetHanHDNhan.Size = new System.Drawing.Size(75, 16);
            this.lblNgayHetHanHDNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHDNhan
            // 
            this.lblNgayHDNhan.Font = new System.Drawing.Font("Times New Roman", 6.5F);
            this.lblNgayHDNhan.Location = new System.Drawing.Point(567, 236);
            this.lblNgayHDNhan.Multiline = true;
            this.lblNgayHDNhan.Name = "lblNgayHDNhan";
            this.lblNgayHDNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNgayHDNhan.Size = new System.Drawing.Size(75, 16);
            this.lblNgayHDNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoHopDongNhan
            // 
            this.lblSoHopDongNhan.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.lblSoHopDongNhan.Location = new System.Drawing.Point(550, 210);
            this.lblSoHopDongNhan.Multiline = true;
            this.lblSoHopDongNhan.Name = "lblSoHopDongNhan";
            this.lblSoHopDongNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblSoHopDongNhan.Size = new System.Drawing.Size(92, 25);
            this.lblSoHopDongNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHetHanHDGiao
            // 
            this.lblNgayHetHanHDGiao.Font = new System.Drawing.Font("Times New Roman", 6.5F);
            this.lblNgayHetHanHDGiao.Location = new System.Drawing.Point(567, 175);
            this.lblNgayHetHanHDGiao.Multiline = true;
            this.lblNgayHetHanHDGiao.Name = "lblNgayHetHanHDGiao";
            this.lblNgayHetHanHDGiao.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNgayHetHanHDGiao.Size = new System.Drawing.Size(75, 16);
            this.lblNgayHetHanHDGiao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHDGiao
            // 
            this.lblNgayHDGiao.Font = new System.Drawing.Font("Times New Roman", 6.5F);
            this.lblNgayHDGiao.Location = new System.Drawing.Point(567, 143);
            this.lblNgayHDGiao.Multiline = true;
            this.lblNgayHDGiao.Name = "lblNgayHDGiao";
            this.lblNgayHDGiao.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNgayHDGiao.Size = new System.Drawing.Size(75, 16);
            this.lblNgayHDGiao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoHopDongGiao
            // 
            this.lblSoHopDongGiao.Font = new System.Drawing.Font("Times New Roman", 6F);
            this.lblSoHopDongGiao.Location = new System.Drawing.Point(550, 117);
            this.lblSoHopDongGiao.Multiline = true;
            this.lblSoHopDongGiao.Name = "lblSoHopDongGiao";
            this.lblSoHopDongGiao.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblSoHopDongGiao.Size = new System.Drawing.Size(92, 25);
            this.lblSoHopDongGiao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDaiLyTTHQ
            // 
            this.lblMaDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDaiLyTTHQ.Location = new System.Drawing.Point(542, 392);
            this.lblMaDaiLyTTHQ.Name = "lblMaDaiLyTTHQ";
            this.lblMaDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 2, 0, 0, 100F);
            this.lblMaDaiLyTTHQ.Size = new System.Drawing.Size(250, 24);
            this.lblMaDaiLyTTHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenDaiLyTTHQ
            // 
            this.lblTenDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDaiLyTTHQ.Location = new System.Drawing.Point(383, 425);
            this.lblTenDaiLyTTHQ.Multiline = true;
            this.lblTenDaiLyTTHQ.Name = "lblTenDaiLyTTHQ";
            this.lblTenDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDaiLyTTHQ.Size = new System.Drawing.Size(384, 58);
            this.lblTenDaiLyTTHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblDiaDiemGiaoHang
            // 
            this.lblDiaDiemGiaoHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblDiaDiemGiaoHang.Location = new System.Drawing.Point(383, 315);
            this.lblDiaDiemGiaoHang.Multiline = true;
            this.lblDiaDiemGiaoHang.Name = "lblDiaDiemGiaoHang";
            this.lblDiaDiemGiaoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblDiaDiemGiaoHang.Size = new System.Drawing.Size(384, 75);
            this.lblDiaDiemGiaoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNguoiChiDinhNhanHang
            // 
            this.lblNguoiChiDinhNhanHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNguoiChiDinhNhanHang.Location = new System.Drawing.Point(33, 408);
            this.lblNguoiChiDinhNhanHang.Multiline = true;
            this.lblNguoiChiDinhNhanHang.Name = "lblNguoiChiDinhNhanHang";
            this.lblNguoiChiDinhNhanHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguoiChiDinhNhanHang.Size = new System.Drawing.Size(342, 75);
            this.lblNguoiChiDinhNhanHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNguoiChiDinhGiaoHang
            // 
            this.lblNguoiChiDinhGiaoHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNguoiChiDinhGiaoHang.Location = new System.Drawing.Point(33, 315);
            this.lblNguoiChiDinhGiaoHang.Multiline = true;
            this.lblNguoiChiDinhGiaoHang.Name = "lblNguoiChiDinhGiaoHang";
            this.lblNguoiChiDinhGiaoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguoiChiDinhGiaoHang.Size = new System.Drawing.Size(342, 75);
            this.lblNguoiChiDinhGiaoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTenNguoiNhanHang
            // 
            this.lblTenNguoiNhanHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenNguoiNhanHang.Location = new System.Drawing.Point(33, 233);
            this.lblTenNguoiNhanHang.Multiline = true;
            this.lblTenNguoiNhanHang.Name = "lblTenNguoiNhanHang";
            this.lblTenNguoiNhanHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenNguoiNhanHang.Size = new System.Drawing.Size(342, 58);
            this.lblTenNguoiNhanHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaNguoiNhanHang
            // 
            this.lblMaNguoiNhanHang.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaNguoiNhanHang.Location = new System.Drawing.Point(150, 200);
            this.lblMaNguoiNhanHang.Name = "lblMaNguoiNhanHang";
            this.lblMaNguoiNhanHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 2, 0, 0, 100F);
            this.lblMaNguoiNhanHang.Size = new System.Drawing.Size(233, 24);
            this.lblMaNguoiNhanHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblGiaoTB
            // 
            this.lblGiaoTB.CanGrow = false;
            this.lblGiaoTB.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lblGiaoTB.Location = new System.Drawing.Point(378, 170);
            this.lblGiaoTB.Name = "lblGiaoTB";
            this.lblGiaoTB.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiaoTB.Size = new System.Drawing.Size(17, 16);
            this.lblGiaoTB.Text = "×";
            this.lblGiaoTB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblGiaoTB.Visible = false;
            // 
            // lblGiaoNPL
            // 
            this.lblGiaoNPL.CanGrow = false;
            this.lblGiaoNPL.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lblGiaoNPL.Location = new System.Drawing.Point(378, 153);
            this.lblGiaoNPL.Name = "lblGiaoNPL";
            this.lblGiaoNPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiaoNPL.Size = new System.Drawing.Size(17, 16);
            this.lblGiaoNPL.Text = "×";
            this.lblGiaoNPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblGiaoNPL.Visible = false;
            // 
            // lblGiaoSP
            // 
            this.lblGiaoSP.CanGrow = false;
            this.lblGiaoSP.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lblGiaoSP.Location = new System.Drawing.Point(378, 133);
            this.lblGiaoSP.Name = "lblGiaoSP";
            this.lblGiaoSP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiaoSP.Size = new System.Drawing.Size(17, 16);
            this.lblGiaoSP.Text = "×";
            this.lblGiaoSP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblGiaoSP.Visible = false;
            // 
            // lblNhanTB
            // 
            this.lblNhanTB.CanGrow = false;
            this.lblNhanTB.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lblNhanTB.Location = new System.Drawing.Point(378, 266);
            this.lblNhanTB.Name = "lblNhanTB";
            this.lblNhanTB.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNhanTB.Size = new System.Drawing.Size(17, 16);
            this.lblNhanTB.Text = "×";
            this.lblNhanTB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNhanTB.Visible = false;
            // 
            // lblNhanNPL
            // 
            this.lblNhanNPL.CanGrow = false;
            this.lblNhanNPL.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lblNhanNPL.Location = new System.Drawing.Point(378, 247);
            this.lblNhanNPL.Name = "lblNhanNPL";
            this.lblNhanNPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNhanNPL.Size = new System.Drawing.Size(17, 16);
            this.lblNhanNPL.Text = "×";
            this.lblNhanNPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNhanNPL.Visible = false;
            // 
            // lblMaNguoiGiaoHang
            // 
            this.lblMaNguoiGiaoHang.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaNguoiGiaoHang.Location = new System.Drawing.Point(150, 108);
            this.lblMaNguoiGiaoHang.Name = "lblMaNguoiGiaoHang";
            this.lblMaNguoiGiaoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 2, 0, 0, 100F);
            this.lblMaNguoiGiaoHang.Size = new System.Drawing.Size(233, 17);
            this.lblMaNguoiGiaoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenNguoiGiaoHang
            // 
            this.lblTenNguoiGiaoHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenNguoiGiaoHang.Location = new System.Drawing.Point(33, 138);
            this.lblTenNguoiGiaoHang.Multiline = true;
            this.lblTenNguoiGiaoHang.Name = "lblTenNguoiGiaoHang";
            this.lblTenNguoiGiaoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenNguoiGiaoHang.Size = new System.Drawing.Size(342, 58);
            this.lblTenNguoiGiaoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblSoTiepNhan.Location = new System.Drawing.Point(642, 17);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhan.Size = new System.Drawing.Size(142, 25);
            this.lblSoTiepNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(67, 535);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20});
            this.xrTable1.Size = new System.Drawing.Size(716, 532);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang1,
            this.MaHS1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 0.050751879699248117;
            // 
            // TenHang1
            // 
            this.TenHang1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang1.Multiline = true;
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang1.Tag = "Tên hàng 1";
            this.TenHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang1.Weight = 0.3016759776536313;
            this.TenHang1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS1
            // 
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS1.Weight = 0.13966480446927373;
            // 
            // Luong1
            // 
            this.Luong1.Multiline = true;
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong1.Weight = 0.0935754189944134;
            // 
            // DVT1
            // 
            this.DVT1.Multiline = true;
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT1.Weight = 0.12849162011173185;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT1.Weight = 0.15083798882681565;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT1.Weight = 0.18575418994413409;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang2,
            this.MaHS2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.050751879699248117;
            // 
            // TenHang2
            // 
            this.TenHang2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang2.Multiline = true;
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang2.Tag = "Tên hàng 2";
            this.TenHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang2.Weight = 0.3016759776536313;
            this.TenHang2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS2
            // 
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS2.Weight = 0.13966480446927373;
            // 
            // Luong2
            // 
            this.Luong2.Multiline = true;
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong2.Weight = 0.0935754189944134;
            // 
            // DVT2
            // 
            this.DVT2.Multiline = true;
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT2.Weight = 0.12849162011173185;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT2.Weight = 0.15083798882681565;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT2.Weight = 0.18575418994413409;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang3,
            this.MaHS3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 0.050751879699248117;
            // 
            // TenHang3
            // 
            this.TenHang3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang3.Multiline = true;
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang3.Tag = "Tên hàng 3";
            this.TenHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang3.Weight = 0.3016759776536313;
            this.TenHang3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS3
            // 
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS3.Weight = 0.13966480446927373;
            // 
            // Luong3
            // 
            this.Luong3.Multiline = true;
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong3.Weight = 0.0935754189944134;
            // 
            // DVT3
            // 
            this.DVT3.Multiline = true;
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT3.Weight = 0.12849162011173185;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT3.Weight = 0.15083798882681565;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT3.Weight = 0.18575418994413409;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang4,
            this.MaHS4,
            this.Luong4,
            this.DVT4,
            this.DonGiaNT4,
            this.TriGiaNT4});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.050751879699248117;
            // 
            // TenHang4
            // 
            this.TenHang4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang4.Multiline = true;
            this.TenHang4.Name = "TenHang4";
            this.TenHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang4.Tag = "Tên hàng 4";
            this.TenHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang4.Weight = 0.3016759776536313;
            this.TenHang4.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS4
            // 
            this.MaHS4.Name = "MaHS4";
            this.MaHS4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS4.Weight = 0.13966480446927373;
            // 
            // Luong4
            // 
            this.Luong4.Multiline = true;
            this.Luong4.Name = "Luong4";
            this.Luong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong4.Weight = 0.0935754189944134;
            // 
            // DVT4
            // 
            this.DVT4.Multiline = true;
            this.DVT4.Name = "DVT4";
            this.DVT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT4.Weight = 0.12849162011173185;
            // 
            // DonGiaNT4
            // 
            this.DonGiaNT4.Name = "DonGiaNT4";
            this.DonGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT4.Weight = 0.15083798882681565;
            // 
            // TriGiaNT4
            // 
            this.TriGiaNT4.Name = "TriGiaNT4";
            this.TriGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT4.Weight = 0.18575418994413409;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang5,
            this.MaHS5,
            this.Luong5,
            this.DVT5,
            this.DonGiaNT5,
            this.TriGiaNT5});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 0.050751879699248117;
            // 
            // TenHang5
            // 
            this.TenHang5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang5.Multiline = true;
            this.TenHang5.Name = "TenHang5";
            this.TenHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang5.Tag = "Tên hàng 5";
            this.TenHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang5.Weight = 0.3016759776536313;
            this.TenHang5.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS5
            // 
            this.MaHS5.Name = "MaHS5";
            this.MaHS5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS5.Weight = 0.13966480446927373;
            // 
            // Luong5
            // 
            this.Luong5.Multiline = true;
            this.Luong5.Name = "Luong5";
            this.Luong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong5.Weight = 0.0935754189944134;
            // 
            // DVT5
            // 
            this.DVT5.Multiline = true;
            this.DVT5.Name = "DVT5";
            this.DVT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT5.Weight = 0.12849162011173185;
            // 
            // DonGiaNT5
            // 
            this.DonGiaNT5.Name = "DonGiaNT5";
            this.DonGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT5.Weight = 0.15083798882681565;
            // 
            // TriGiaNT5
            // 
            this.TriGiaNT5.Name = "TriGiaNT5";
            this.TriGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT5.Weight = 0.18575418994413409;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang6,
            this.MaHS6,
            this.Luong6,
            this.DVT6,
            this.DonGiaNT6,
            this.TriGiaNT6});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 0.050751879699248117;
            // 
            // TenHang6
            // 
            this.TenHang6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang6.Multiline = true;
            this.TenHang6.Name = "TenHang6";
            this.TenHang6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang6.Tag = "Tên hàng 6";
            this.TenHang6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang6.Weight = 0.3016759776536313;
            this.TenHang6.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS6
            // 
            this.MaHS6.Name = "MaHS6";
            this.MaHS6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS6.Weight = 0.13966480446927373;
            // 
            // Luong6
            // 
            this.Luong6.Multiline = true;
            this.Luong6.Name = "Luong6";
            this.Luong6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong6.Weight = 0.0935754189944134;
            // 
            // DVT6
            // 
            this.DVT6.Multiline = true;
            this.DVT6.Name = "DVT6";
            this.DVT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT6.Weight = 0.12849162011173185;
            // 
            // DonGiaNT6
            // 
            this.DonGiaNT6.Name = "DonGiaNT6";
            this.DonGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT6.Weight = 0.15083798882681565;
            // 
            // TriGiaNT6
            // 
            this.TriGiaNT6.Name = "TriGiaNT6";
            this.TriGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT6.Weight = 0.18575418994413409;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang7,
            this.MaHS7,
            this.Luong7,
            this.DVT7,
            this.DonGiaNT7,
            this.TriGiaNT7});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 0.048872180451127817;
            // 
            // TenHang7
            // 
            this.TenHang7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang7.Multiline = true;
            this.TenHang7.Name = "TenHang7";
            this.TenHang7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang7.Tag = "Tên hàng 7";
            this.TenHang7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang7.Weight = 0.3016759776536313;
            this.TenHang7.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS7
            // 
            this.MaHS7.Name = "MaHS7";
            this.MaHS7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS7.Weight = 0.13966480446927373;
            // 
            // Luong7
            // 
            this.Luong7.Multiline = true;
            this.Luong7.Name = "Luong7";
            this.Luong7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong7.Weight = 0.0935754189944134;
            // 
            // DVT7
            // 
            this.DVT7.Multiline = true;
            this.DVT7.Name = "DVT7";
            this.DVT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT7.Weight = 0.12849162011173185;
            // 
            // DonGiaNT7
            // 
            this.DonGiaNT7.Name = "DonGiaNT7";
            this.DonGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT7.Weight = 0.15083798882681565;
            // 
            // TriGiaNT7
            // 
            this.TriGiaNT7.Name = "TriGiaNT7";
            this.TriGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT7.Weight = 0.18575418994413409;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang8,
            this.MaHS8,
            this.Luong8,
            this.DVT8,
            this.DonGiaNT8,
            this.TriGiaNT8});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 0.048872180451127817;
            // 
            // TenHang8
            // 
            this.TenHang8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang8.Multiline = true;
            this.TenHang8.Name = "TenHang8";
            this.TenHang8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang8.Tag = "Tên hàng 8";
            this.TenHang8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang8.Weight = 0.3016759776536313;
            this.TenHang8.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS8
            // 
            this.MaHS8.Name = "MaHS8";
            this.MaHS8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS8.Weight = 0.13966480446927373;
            // 
            // Luong8
            // 
            this.Luong8.Multiline = true;
            this.Luong8.Name = "Luong8";
            this.Luong8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong8.Weight = 0.0935754189944134;
            // 
            // DVT8
            // 
            this.DVT8.Multiline = true;
            this.DVT8.Name = "DVT8";
            this.DVT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT8.Weight = 0.12849162011173185;
            // 
            // DonGiaNT8
            // 
            this.DonGiaNT8.Name = "DonGiaNT8";
            this.DonGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT8.Weight = 0.15083798882681565;
            // 
            // TriGiaNT8
            // 
            this.TriGiaNT8.Name = "TriGiaNT8";
            this.TriGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT8.Weight = 0.18575418994413409;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang9,
            this.MaHS9,
            this.Luong9,
            this.DVT9,
            this.DonGiaNT9,
            this.TriGiaNT9});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow9.Weight = 0.048872180451127817;
            // 
            // TenHang9
            // 
            this.TenHang9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang9.Multiline = true;
            this.TenHang9.Name = "TenHang9";
            this.TenHang9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang9.Tag = "Tên hàng 9";
            this.TenHang9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang9.Weight = 0.3016759776536313;
            this.TenHang9.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS9
            // 
            this.MaHS9.Name = "MaHS9";
            this.MaHS9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS9.Weight = 0.13966480446927373;
            // 
            // Luong9
            // 
            this.Luong9.Multiline = true;
            this.Luong9.Name = "Luong9";
            this.Luong9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong9.Weight = 0.0935754189944134;
            // 
            // DVT9
            // 
            this.DVT9.Multiline = true;
            this.DVT9.Name = "DVT9";
            this.DVT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT9.Weight = 0.12849162011173185;
            // 
            // DonGiaNT9
            // 
            this.DonGiaNT9.Name = "DonGiaNT9";
            this.DonGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT9.Weight = 0.15083798882681565;
            // 
            // TriGiaNT9
            // 
            this.TriGiaNT9.Name = "TriGiaNT9";
            this.TriGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT9.Weight = 0.18575418994413409;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang10,
            this.MaHS10,
            this.Luong10,
            this.DVT10,
            this.DonGiaNT10,
            this.TriGiaNT10});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 0.048872180451127817;
            // 
            // TenHang10
            // 
            this.TenHang10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang10.Name = "TenHang10";
            this.TenHang10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang10.Tag = "Tên hàng 10";
            this.TenHang10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang10.Weight = 0.3016759776536313;
            this.TenHang10.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS10
            // 
            this.MaHS10.Name = "MaHS10";
            this.MaHS10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS10.Weight = 0.13966480446927373;
            // 
            // Luong10
            // 
            this.Luong10.Name = "Luong10";
            this.Luong10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong10.Weight = 0.0935754189944134;
            // 
            // DVT10
            // 
            this.DVT10.Name = "DVT10";
            this.DVT10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT10.Weight = 0.12849162011173185;
            // 
            // DonGiaNT10
            // 
            this.DonGiaNT10.Name = "DonGiaNT10";
            this.DonGiaNT10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT10.Weight = 0.15083798882681565;
            // 
            // TriGiaNT10
            // 
            this.TriGiaNT10.Name = "TriGiaNT10";
            this.TriGiaNT10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT10.Weight = 0.18575418994413409;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang11,
            this.MaHS11,
            this.Luong11,
            this.DVT11,
            this.DonGiaNT11,
            this.TriGiaNT11});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 0.048872180451127817;
            // 
            // TenHang11
            // 
            this.TenHang11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang11.Name = "TenHang11";
            this.TenHang11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang11.Tag = "Tên hàng 11";
            this.TenHang11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang11.Weight = 0.3016759776536313;
            this.TenHang11.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS11
            // 
            this.MaHS11.Name = "MaHS11";
            this.MaHS11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS11.Weight = 0.13966480446927373;
            // 
            // Luong11
            // 
            this.Luong11.Name = "Luong11";
            this.Luong11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong11.Weight = 0.0935754189944134;
            // 
            // DVT11
            // 
            this.DVT11.Name = "DVT11";
            this.DVT11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT11.Weight = 0.12849162011173185;
            // 
            // DonGiaNT11
            // 
            this.DonGiaNT11.Name = "DonGiaNT11";
            this.DonGiaNT11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT11.Weight = 0.15083798882681565;
            // 
            // TriGiaNT11
            // 
            this.TriGiaNT11.Name = "TriGiaNT11";
            this.TriGiaNT11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT11.Weight = 0.18575418994413409;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang12,
            this.MaHS12,
            this.Luong12,
            this.DVT12,
            this.DonGiaNT12,
            this.TriGiaNT12});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow12.Weight = 0.050751879699248117;
            // 
            // TenHang12
            // 
            this.TenHang12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang12.Name = "TenHang12";
            this.TenHang12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang12.Tag = "Tên hàng 12";
            this.TenHang12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang12.Weight = 0.3016759776536313;
            this.TenHang12.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS12
            // 
            this.MaHS12.Name = "MaHS12";
            this.MaHS12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS12.Weight = 0.13966480446927373;
            // 
            // Luong12
            // 
            this.Luong12.Name = "Luong12";
            this.Luong12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong12.Weight = 0.0935754189944134;
            // 
            // DVT12
            // 
            this.DVT12.Name = "DVT12";
            this.DVT12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT12.Weight = 0.12849162011173185;
            // 
            // DonGiaNT12
            // 
            this.DonGiaNT12.Name = "DonGiaNT12";
            this.DonGiaNT12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT12.Weight = 0.15083798882681565;
            // 
            // TriGiaNT12
            // 
            this.TriGiaNT12.Name = "TriGiaNT12";
            this.TriGiaNT12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT12.Weight = 0.18575418994413409;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang13,
            this.MaHS13,
            this.Luong13,
            this.DVT13,
            this.DonGiaNT13,
            this.TriGiaNT13});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow13.Weight = 0.050751879699248117;
            // 
            // TenHang13
            // 
            this.TenHang13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang13.Name = "TenHang13";
            this.TenHang13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang13.Tag = "Tên hàng 13";
            this.TenHang13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang13.Weight = 0.3016759776536313;
            this.TenHang13.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS13
            // 
            this.MaHS13.Name = "MaHS13";
            this.MaHS13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS13.Weight = 0.13966480446927373;
            // 
            // Luong13
            // 
            this.Luong13.Name = "Luong13";
            this.Luong13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong13.Weight = 0.0935754189944134;
            // 
            // DVT13
            // 
            this.DVT13.Name = "DVT13";
            this.DVT13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT13.Weight = 0.12849162011173185;
            // 
            // DonGiaNT13
            // 
            this.DonGiaNT13.Name = "DonGiaNT13";
            this.DonGiaNT13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT13.Weight = 0.15083798882681565;
            // 
            // TriGiaNT13
            // 
            this.TriGiaNT13.Name = "TriGiaNT13";
            this.TriGiaNT13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT13.Weight = 0.18575418994413409;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang14,
            this.MaHS14,
            this.Luong14,
            this.DVT14,
            this.DonGiaNT14,
            this.TriGiaNT14});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow14.Weight = 0.050751879699248117;
            // 
            // TenHang14
            // 
            this.TenHang14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang14.Name = "TenHang14";
            this.TenHang14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang14.Tag = "Tên hàng 14";
            this.TenHang14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang14.Weight = 0.3016759776536313;
            this.TenHang14.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS14
            // 
            this.MaHS14.Name = "MaHS14";
            this.MaHS14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS14.Weight = 0.13966480446927373;
            // 
            // Luong14
            // 
            this.Luong14.Name = "Luong14";
            this.Luong14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong14.Weight = 0.0935754189944134;
            // 
            // DVT14
            // 
            this.DVT14.Name = "DVT14";
            this.DVT14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT14.Weight = 0.12849162011173185;
            // 
            // DonGiaNT14
            // 
            this.DonGiaNT14.Name = "DonGiaNT14";
            this.DonGiaNT14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT14.Weight = 0.15083798882681565;
            // 
            // TriGiaNT14
            // 
            this.TriGiaNT14.Name = "TriGiaNT14";
            this.TriGiaNT14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT14.Weight = 0.18575418994413409;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang15,
            this.MaHS15,
            this.Luong15,
            this.DVT15,
            this.DonGiaNT15,
            this.TriGiaNT15});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow15.Weight = 0.050751879699248117;
            // 
            // TenHang15
            // 
            this.TenHang15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang15.Name = "TenHang15";
            this.TenHang15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang15.Tag = "Tên hàng 15";
            this.TenHang15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang15.Weight = 0.3016759776536313;
            this.TenHang15.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS15
            // 
            this.MaHS15.Name = "MaHS15";
            this.MaHS15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS15.Weight = 0.13966480446927373;
            // 
            // Luong15
            // 
            this.Luong15.Name = "Luong15";
            this.Luong15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong15.Weight = 0.0935754189944134;
            // 
            // DVT15
            // 
            this.DVT15.Name = "DVT15";
            this.DVT15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT15.Weight = 0.12849162011173185;
            // 
            // DonGiaNT15
            // 
            this.DonGiaNT15.Name = "DonGiaNT15";
            this.DonGiaNT15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT15.Weight = 0.15083798882681565;
            // 
            // TriGiaNT15
            // 
            this.TriGiaNT15.Name = "TriGiaNT15";
            this.TriGiaNT15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT15.Weight = 0.18575418994413409;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang16,
            this.MaHS16,
            this.Luong16,
            this.DVT16,
            this.DonGiaNT16,
            this.TriGiaNT16});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow16.Weight = 0.050751879699248117;
            // 
            // TenHang16
            // 
            this.TenHang16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang16.Name = "TenHang16";
            this.TenHang16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang16.Tag = "Tên hàng 16";
            this.TenHang16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang16.Weight = 0.3016759776536313;
            this.TenHang16.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS16
            // 
            this.MaHS16.Name = "MaHS16";
            this.MaHS16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS16.Weight = 0.13966480446927373;
            // 
            // Luong16
            // 
            this.Luong16.Name = "Luong16";
            this.Luong16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong16.Weight = 0.0935754189944134;
            // 
            // DVT16
            // 
            this.DVT16.Name = "DVT16";
            this.DVT16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT16.Weight = 0.12849162011173185;
            // 
            // DonGiaNT16
            // 
            this.DonGiaNT16.Name = "DonGiaNT16";
            this.DonGiaNT16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT16.Weight = 0.15083798882681565;
            // 
            // TriGiaNT16
            // 
            this.TriGiaNT16.Name = "TriGiaNT16";
            this.TriGiaNT16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT16.Weight = 0.18575418994413409;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang17,
            this.MaHS17,
            this.Luong17,
            this.DVT17,
            this.DonGiaNT17,
            this.TriGiaNT17});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow17.Weight = 0.050751879699248117;
            // 
            // TenHang17
            // 
            this.TenHang17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang17.Name = "TenHang17";
            this.TenHang17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang17.Tag = "Tên hàng 17";
            this.TenHang17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang17.Weight = 0.3016759776536313;
            this.TenHang17.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS17
            // 
            this.MaHS17.Name = "MaHS17";
            this.MaHS17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS17.Weight = 0.13966480446927373;
            // 
            // Luong17
            // 
            this.Luong17.Name = "Luong17";
            this.Luong17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong17.Weight = 0.0935754189944134;
            // 
            // DVT17
            // 
            this.DVT17.Name = "DVT17";
            this.DVT17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT17.Weight = 0.12849162011173185;
            // 
            // DonGiaNT17
            // 
            this.DonGiaNT17.Name = "DonGiaNT17";
            this.DonGiaNT17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT17.Weight = 0.15083798882681565;
            // 
            // TriGiaNT17
            // 
            this.TriGiaNT17.Name = "TriGiaNT17";
            this.TriGiaNT17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT17.Weight = 0.18575418994413409;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang18,
            this.MaHS18,
            this.Luong18,
            this.DVT18,
            this.DonGiaNT18,
            this.TriGiaNT18});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow18.Weight = 0.048872180451127817;
            // 
            // TenHang18
            // 
            this.TenHang18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang18.Name = "TenHang18";
            this.TenHang18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang18.Tag = "Tên hàng 18";
            this.TenHang18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang18.Weight = 0.3016759776536313;
            this.TenHang18.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS18
            // 
            this.MaHS18.Name = "MaHS18";
            this.MaHS18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS18.Weight = 0.13966480446927373;
            // 
            // Luong18
            // 
            this.Luong18.Name = "Luong18";
            this.Luong18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong18.Weight = 0.0935754189944134;
            // 
            // DVT18
            // 
            this.DVT18.Name = "DVT18";
            this.DVT18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT18.Weight = 0.12849162011173185;
            // 
            // DonGiaNT18
            // 
            this.DonGiaNT18.Name = "DonGiaNT18";
            this.DonGiaNT18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT18.Weight = 0.15083798882681565;
            // 
            // TriGiaNT18
            // 
            this.TriGiaNT18.Name = "TriGiaNT18";
            this.TriGiaNT18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT18.Weight = 0.18575418994413409;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang19,
            this.MaHS19,
            this.Luong19,
            this.DVT19,
            this.DonGiaNT19,
            this.TriGiaNT19});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow19.Weight = 0.048872180451127817;
            // 
            // TenHang19
            // 
            this.TenHang19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang19.Name = "TenHang19";
            this.TenHang19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang19.Tag = "Tên hàng 19";
            this.TenHang19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang19.Weight = 0.3016759776536313;
            this.TenHang19.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS19
            // 
            this.MaHS19.Name = "MaHS19";
            this.MaHS19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS19.Weight = 0.13966480446927373;
            // 
            // Luong19
            // 
            this.Luong19.Name = "Luong19";
            this.Luong19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong19.Weight = 0.0935754189944134;
            // 
            // DVT19
            // 
            this.DVT19.Name = "DVT19";
            this.DVT19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT19.Weight = 0.12849162011173185;
            // 
            // DonGiaNT19
            // 
            this.DonGiaNT19.Name = "DonGiaNT19";
            this.DonGiaNT19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT19.Weight = 0.15083798882681565;
            // 
            // TriGiaNT19
            // 
            this.TriGiaNT19.Name = "TriGiaNT19";
            this.TriGiaNT19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT19.Weight = 0.18575418994413409;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang20,
            this.MaHS20,
            this.Luong20,
            this.DVT20,
            this.DonGiaNT20,
            this.TriGiaNT20});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow20.Weight = 0.048872180451127817;
            // 
            // TenHang20
            // 
            this.TenHang20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.TenHang20.Name = "TenHang20";
            this.TenHang20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang20.Tag = "Tên hàng 20";
            this.TenHang20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang20.Weight = 0.3016759776536313;
            this.TenHang20.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS20
            // 
            this.MaHS20.Name = "MaHS20";
            this.MaHS20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS20.Weight = 0.13966480446927373;
            // 
            // Luong20
            // 
            this.Luong20.Name = "Luong20";
            this.Luong20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Luong20.Weight = 0.0935754189944134;
            // 
            // DVT20
            // 
            this.DVT20.Name = "DVT20";
            this.DVT20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DVT20.Weight = 0.12849162011173185;
            // 
            // DonGiaNT20
            // 
            this.DonGiaNT20.Name = "DonGiaNT20";
            this.DonGiaNT20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT20.Weight = 0.15083798882681565;
            // 
            // TriGiaNT20
            // 
            this.TriGiaNT20.Name = "TriGiaNT20";
            this.TriGiaNT20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT20.Weight = 0.18575418994413409;
            // 
            // lblNhanSP
            // 
            this.lblNhanSP.CanGrow = false;
            this.lblNhanSP.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.lblNhanSP.Location = new System.Drawing.Point(378, 227);
            this.lblNhanSP.Name = "lblNhanSP";
            this.lblNhanSP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNhanSP.Size = new System.Drawing.Size(17, 16);
            this.lblNhanSP.Text = "×";
            this.lblNhanSP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNhanSP.Visible = false;
            // 
            // ptbImage
            // 
            this.ptbImage.Image = ((System.Drawing.Image)(resources.GetObject("ptbImage.Image")));
            this.ptbImage.Location = new System.Drawing.Point(0, 0);
            this.ptbImage.Name = "ptbImage";
            this.ptbImage.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ptbImage.Size = new System.Drawing.Size(811, 1110);
            this.ptbImage.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // ToKhaiCT
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(0, 6, 9, 19);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel lblNhanSP;
        public DevExpress.XtraReports.UI.XRPictureBox ptbImage;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell TenHang4;
        private DevExpress.XtraReports.UI.XRTableCell MaHS4;
        private DevExpress.XtraReports.UI.XRTableCell Luong4;
        private DevExpress.XtraReports.UI.XRTableCell DVT4;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell TenHang5;
        private DevExpress.XtraReports.UI.XRTableCell MaHS5;
        private DevExpress.XtraReports.UI.XRTableCell Luong5;
        private DevExpress.XtraReports.UI.XRTableCell DVT5;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell TenHang6;
        private DevExpress.XtraReports.UI.XRTableCell MaHS6;
        private DevExpress.XtraReports.UI.XRTableCell Luong6;
        private DevExpress.XtraReports.UI.XRTableCell DVT6;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell TenHang7;
        private DevExpress.XtraReports.UI.XRTableCell MaHS7;
        private DevExpress.XtraReports.UI.XRTableCell Luong7;
        private DevExpress.XtraReports.UI.XRTableCell DVT7;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell TenHang8;
        private DevExpress.XtraReports.UI.XRTableCell MaHS8;
        private DevExpress.XtraReports.UI.XRTableCell Luong8;
        private DevExpress.XtraReports.UI.XRTableCell DVT8;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell TenHang9;
        private DevExpress.XtraReports.UI.XRTableCell MaHS9;
        private DevExpress.XtraReports.UI.XRTableCell Luong9;
        private DevExpress.XtraReports.UI.XRTableCell DVT9;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT9;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhan;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiGiaoHang;
        private DevExpress.XtraReports.UI.XRLabel lblTenNguoiGiaoHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell TenHang10;
        private DevExpress.XtraReports.UI.XRTableCell MaHS10;
        private DevExpress.XtraReports.UI.XRTableCell Luong10;
        private DevExpress.XtraReports.UI.XRTableCell DVT10;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT10;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell TenHang11;
        private DevExpress.XtraReports.UI.XRTableCell MaHS11;
        private DevExpress.XtraReports.UI.XRTableCell Luong11;
        private DevExpress.XtraReports.UI.XRTableCell DVT11;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT11;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell TenHang12;
        private DevExpress.XtraReports.UI.XRTableCell MaHS12;
        private DevExpress.XtraReports.UI.XRTableCell Luong12;
        private DevExpress.XtraReports.UI.XRTableCell DVT12;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT12;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell TenHang13;
        private DevExpress.XtraReports.UI.XRTableCell MaHS13;
        private DevExpress.XtraReports.UI.XRTableCell Luong13;
        private DevExpress.XtraReports.UI.XRTableCell DVT13;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT13;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell TenHang14;
        private DevExpress.XtraReports.UI.XRTableCell MaHS14;
        private DevExpress.XtraReports.UI.XRTableCell Luong14;
        private DevExpress.XtraReports.UI.XRTableCell DVT14;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT14;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell TenHang15;
        private DevExpress.XtraReports.UI.XRTableCell MaHS15;
        private DevExpress.XtraReports.UI.XRTableCell Luong15;
        private DevExpress.XtraReports.UI.XRTableCell DVT15;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT15;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell TenHang16;
        private DevExpress.XtraReports.UI.XRTableCell MaHS16;
        private DevExpress.XtraReports.UI.XRTableCell Luong16;
        private DevExpress.XtraReports.UI.XRTableCell DVT16;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT16;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell TenHang17;
        private DevExpress.XtraReports.UI.XRTableCell MaHS17;
        private DevExpress.XtraReports.UI.XRTableCell Luong17;
        private DevExpress.XtraReports.UI.XRTableCell DVT17;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT17;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell TenHang18;
        private DevExpress.XtraReports.UI.XRTableCell MaHS18;
        private DevExpress.XtraReports.UI.XRTableCell Luong18;
        private DevExpress.XtraReports.UI.XRTableCell DVT18;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT18;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell TenHang19;
        private DevExpress.XtraReports.UI.XRTableCell MaHS19;
        private DevExpress.XtraReports.UI.XRTableCell Luong19;
        private DevExpress.XtraReports.UI.XRTableCell DVT19;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT19;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell TenHang20;
        private DevExpress.XtraReports.UI.XRTableCell MaHS20;
        private DevExpress.XtraReports.UI.XRTableCell Luong20;
        private DevExpress.XtraReports.UI.XRTableCell DVT20;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT20;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT20;
        private DevExpress.XtraReports.UI.XRLabel lblNhanNPL;
        private DevExpress.XtraReports.UI.XRLabel lblGiaoTB;
        private DevExpress.XtraReports.UI.XRLabel lblGiaoNPL;
        private DevExpress.XtraReports.UI.XRLabel lblGiaoSP;
        private DevExpress.XtraReports.UI.XRLabel lblNhanTB;
        private DevExpress.XtraReports.UI.XRLabel lblTenNguoiNhanHang;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiNhanHang;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHetHanHDGiao;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHDGiao;
        private DevExpress.XtraReports.UI.XRLabel lblSoHopDongGiao;
        private DevExpress.XtraReports.UI.XRLabel lblMaDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblTenDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemGiaoHang;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiChiDinhNhanHang;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiChiDinhGiaoHang;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHetHanHDNhan;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHDNhan;
        private DevExpress.XtraReports.UI.XRLabel lblSoHopDongNhan;
        private DevExpress.XtraReports.UI.XRLabel lblNguyenTe2;
        private DevExpress.XtraReports.UI.XRLabel lblNguyenTe1;
    }
}
