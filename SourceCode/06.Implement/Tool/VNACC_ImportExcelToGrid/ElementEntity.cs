﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Utils
{
    public class ElementEntity : ICloneable
    {
        public string Data { get; set; }
        public string No { get; set; }
        public string Column { get; set; }
        public string ElementNameJP { get; set; }
        public string ElementNameEN { get; set; }
        public string ElementNameVN { get; set; }
        public string ID { get; set; }
        public string Attribute { get; set; }
        public string Digit { get; set; }
        public string Repetition1 { get; set; }
        public string Condition1 { get; set; }
        public string Condition2 { get; set; }
        public string Condition3 { get; set; }
        public string Code { get; set; }
        public string Format { get; set; }
        public string ReferenceDB { get; set; }
        public string InputConditionJP { get; set; }
        public string InputConditionEN { get; set; }

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }

    public class LoopElement
    {
        public bool IsLoopColumn;
        public bool IsLoopRepetition;
        public int LoopCount;
        public int IndexInsertNo;
        public int FirstNo;
        public int LastNo;
        public List<ElementEntity> Elements = new List<ElementEntity>();
    }
}
