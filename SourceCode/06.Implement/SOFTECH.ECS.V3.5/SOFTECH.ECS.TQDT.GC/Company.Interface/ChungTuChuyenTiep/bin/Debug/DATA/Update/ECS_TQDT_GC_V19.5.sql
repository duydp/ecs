alter table t_kdt_gc_dinhmuc
alter column GhiChu nvarchar(250)null
Go
/****** Object:  StoredProcedure [dbo].[p_KDT_GC_DinhMuc_Insert]    Script Date: 09/06/2014 15:12:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Insert]
-- Database: HaiQuanPhongKhaiGC
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 10, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Insert]
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(80),
	@TenNPL nvarchar(80),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_GC_DinhMuc]
(
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL]
)
VALUES 
(
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DVT_ID,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@GhiChu,
	@STTHang,
	@Master_ID,
	@NPL_TuCungUng,
	@TenSanPham,
	@TenNPL
)

SET @ID = SCOPE_IDENTITY()

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]    Script Date: 09/06/2014 15:12:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]
-- Database: HaiQuanPhongKhaiGC
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 10, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]
	@ID bigint,
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(80),
	@TenNPL nvarchar(80)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_DinhMuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_DinhMuc] 
		SET
			[MaSanPham] = @MaSanPham,
			[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
			[DVT_ID] = @DVT_ID,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[GhiChu] = @GhiChu,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID,
			[NPL_TuCungUng] = @NPL_TuCungUng,
			[TenSanPham] = @TenSanPham,
			[TenNPL] = @TenNPL
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_DinhMuc]
		(
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DVT_ID],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[GhiChu],
			[STTHang],
			[Master_ID],
			[NPL_TuCungUng],
			[TenSanPham],
			[TenNPL]
		)
		VALUES 
		(
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DVT_ID,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@GhiChu,
			@STTHang,
			@Master_ID,
			@NPL_TuCungUng,
			@TenSanPham,
			@TenNPL
		)		
	END
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_DinhMuc_InsertUpdateBy]    Script Date: 09/06/2014 15:13:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]  
-- Database: HaiQuanPhongKhaiGC  
-- Author: Ngo Thanh Tung  
-- Time created: Friday, October 10, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_KDT_GC_DinhMuc_InsertUpdateBy]  
 @ID bigint,  
 @MaSanPham varchar(30),  
 @MaNguyenPhuLieu varchar(30),  
 @DVT_ID char(3),  
 @DinhMucSuDung numeric(18, 8),  
 @TyLeHaoHut numeric(18, 8),  
 @GhiChu nvarchar(250),  
 @STTHang int,  
 @Master_ID bigint,  
 @NPL_TuCungUng numeric(18, 8),  
 @TenSanPham nvarchar(80),  
 @TenNPL nvarchar(80)  
AS  
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_DinhMuc] WHERE MaSanPham = @MaSanPham AND MaNguyenPhuLieu = @MaNguyenPhuLieu AND Master_ID = @Master_ID)  
 BEGIN  
  UPDATE  
   [dbo].[t_KDT_GC_DinhMuc]   
  SET  
   [MaSanPham] = @MaSanPham,  
   [MaNguyenPhuLieu] = @MaNguyenPhuLieu,  
   [DVT_ID] = @DVT_ID,  
   [DinhMucSuDung] = @DinhMucSuDung,  
   [TyLeHaoHut] = @TyLeHaoHut,  
   [GhiChu] = @GhiChu,  
   [STTHang] = @STTHang,  
   [Master_ID] = @Master_ID,  
   [NPL_TuCungUng] = @NPL_TuCungUng,  
   [TenSanPham] = @TenSanPham,  
   [TenNPL] = @TenNPL  
  WHERE  
   [ID] = @ID  
 END  
ELSE  
 BEGIN  
    
  INSERT INTO [dbo].[t_KDT_GC_DinhMuc]  
  (  
   [MaSanPham],  
   [MaNguyenPhuLieu],  
   [DVT_ID],  
   [DinhMucSuDung],  
   [TyLeHaoHut],  
   [GhiChu],  
   [STTHang],  
   [Master_ID],  
   [NPL_TuCungUng],  
   [TenSanPham],  
   [TenNPL]  
  )  
  VALUES   
  (  
   @MaSanPham,  
   @MaNguyenPhuLieu,  
   @DVT_ID,  
   @DinhMucSuDung,  
   @TyLeHaoHut,  
   @GhiChu,  
   @STTHang,  
   @Master_ID,  
   @NPL_TuCungUng,  
   @TenSanPham,  
   @TenNPL  
  )    
 END  
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_GC_DinhMuc_Update]    Script Date: 09/06/2014 15:13:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Update]
-- Database: HaiQuanPhongKhaiGC
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 10, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Update]
	@ID bigint,
	@MaSanPham varchar(30),
	@MaNguyenPhuLieu varchar(30),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(80),
	@TenNPL nvarchar(80)
AS
UPDATE
	[dbo].[t_KDT_GC_DinhMuc]
SET
	[MaSanPham] = @MaSanPham,
	[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
	[DVT_ID] = @DVT_ID,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[GhiChu] = @GhiChu,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID,
	[NPL_TuCungUng] = @NPL_TuCungUng,
	[TenSanPham] = @TenSanPham,
	[TenNPL] = @TenNPL
WHERE
	[ID] = @ID

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.5',GETDATE(), N' Cập nhật table t_KDT_GC_DinhMuc')
END	
