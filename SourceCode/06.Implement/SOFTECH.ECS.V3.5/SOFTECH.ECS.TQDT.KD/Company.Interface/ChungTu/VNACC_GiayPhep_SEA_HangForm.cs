﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class VNACC_GiayPhep_SEA_HangForm : BaseFormHaveGuidPanel
    {
        private KDT_VNACC_HangGiayPhep HangGiayPhep = null;
        public KDT_VNACC_GiayPhep_SEA GiayPhep_SEA;
        private EGiayPhep GiayPhepType = EGiayPhep.SEA;
        private bool isAddNew = true;

        private DataTable dtHS = new DataTable();

        public VNACC_GiayPhep_SEA_HangForm()
        {
            InitializeComponent();

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_OGAProcedure.SEA.ToString());
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateForm(false))
                    return;

                if (isAddNew)
                {
                    HangGiayPhep = new KDT_VNACC_HangGiayPhep();

                    GetHangGiayPhep(HangGiayPhep);

                    GiayPhep_SEA.HangCollection.Add(HangGiayPhep);
                }
                else
                {
                    GetHangGiayPhep(HangGiayPhep);
                }

                grdHang.DataSource = GiayPhep_SEA.HangCollection;
                grdHang.Refetch();

                HangGiayPhep = new KDT_VNACC_HangGiayPhep();
                SetHangGiayPhep(HangGiayPhep);
                isAddNew = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetHangGiayPhep(KDT_VNACC_HangGiayPhep hangGiayPhep)
        {
            errorProvider.Clear();

            hangGiayPhep.GiayPhepType = GiayPhepType.ToString();
            hangGiayPhep.TenHangHoa = txtTenHang.Text;
            hangGiayPhep.MaSoHangHoa = txtMaSoHangHoa.Text;
            hangGiayPhep.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
            hangGiayPhep.DonVitinhSoLuong = ucDonViTinhSoLuong.Code;
        }

        private void SetHangGiayPhep(KDT_VNACC_HangGiayPhep hangGiayPhep)
        {
            errorProvider.Clear();

            txtTenHang.Text = hangGiayPhep.TenHangHoa;
            txtMaSoHangHoa.Text = hangGiayPhep.MaSoHangHoa;
            txtSoLuong.Value = hangGiayPhep.SoLuong;
            ucDonViTinhSoLuong.Code = hangGiayPhep.DonVitinhSoLuong;
        }

        private void VNACC_GiayPhep_SEA_HangForm_Load(object sender, EventArgs e)
        {
            SetIDControl();

            SetMaxLengthControl();

            Khoitao_DuLieuChuan();

            grdHang.DataSource = GiayPhep_SEA.HangCollection;

            ValidateForm(true);
        }

        private void Khoitao_DuLieuChuan()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                dtHS = MaHS.SelectAll();
                foreach (DataRow dr in dtHS.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaSoHangHoa.AutoCompleteCustomSource = col;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                List<KDT_VNACC_HangGiayPhep> hangColl = new List<KDT_VNACC_HangGiayPhep>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hangColl.Add((KDT_VNACC_HangGiayPhep)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangGiayPhep hmd in hangColl)
                    {
                        if (hmd.ID > 0)
                            hmd.Delete();
                        GiayPhep_SEA.HangCollection.Remove(hmd);
                    }

                    grdHang.DataSource = GiayPhep_SEA.HangCollection;
                    try
                    {
                        grdHang.Refetch();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdHang_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                // List<KDT_VNACC_HangGiayPhep> hangColl = new List<KDT_VNACC_HangGiayPhep>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                HangGiayPhep = (KDT_VNACC_HangGiayPhep)items[0].GetRow().DataRow;
                SetHangGiayPhep(HangGiayPhep);
                isAddNew = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void txtMaSoHangHoa_Leave(object sender, EventArgs e)
        {
            try
            {
                string MoTa = MaHS.CheckExist(txtMaSoHangHoa.Text);
                if (MoTa == "")
                {
                    txtMaSoHangHoa.Focus();
                    epError.SetIconPadding(txtMaSoHangHoa, -8);
                    epError.SetError(txtMaSoHangHoa, setText("Mã HS không có trong danh mục mã HS.", "This HS is not exist"));
                }
                else
                {
                    epError.SetError(txtMaSoHangHoa, string.Empty);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtTenHang.Tag = "GDN";
                txtMaSoHangHoa.Tag = "HSC";
                txtSoLuong.Tag = "GW";
                ucDonViTinhSoLuong.TagName = "GWU";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtTenHang.MaxLength = 768;
                txtMaSoHangHoa.MaxLength = 12;
                txtSoLuong.MaxLength = 10;
                txtSoLuong.DecimalDigits = 3;
                //txtDonVitinh.MaxLength = 3;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                isValid &= ValidateControl.ValidateNull(txtTenHang, errorProvider, "Tên hàng hóa", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

    }
}
