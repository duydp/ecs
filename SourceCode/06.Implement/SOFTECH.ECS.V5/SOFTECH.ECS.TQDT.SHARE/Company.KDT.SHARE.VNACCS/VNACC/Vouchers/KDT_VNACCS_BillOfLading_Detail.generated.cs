﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_BillOfLading_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long BillOfLading_ID { set; get; }
		public string SoVanDon { set; get; }
		public DateTime NgayVanDon { set; get; }
		public string NuocPhatHanh { set; get; }
		public string DiaDiemCTQC { set; get; }
		public decimal LoaiVanDon { set; get; }
        public List<KDT_VNACCS_BillOfLading_Detail> ListBillOfLading = new List<KDT_VNACCS_BillOfLading_Detail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_BillOfLading_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_BillOfLading_Detail> collection = new List<KDT_VNACCS_BillOfLading_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_BillOfLading_Detail entity = new KDT_VNACCS_BillOfLading_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BillOfLading_ID"))) entity.BillOfLading_ID = reader.GetInt64(reader.GetOrdinal("BillOfLading_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocPhatHanh"))) entity.NuocPhatHanh = reader.GetString(reader.GetOrdinal("NuocPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemCTQC"))) entity.DiaDiemCTQC = reader.GetString(reader.GetOrdinal("DiaDiemCTQC"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetDecimal(reader.GetOrdinal("LoaiVanDon"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_BillOfLading_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_BillOfLading_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BillOfLading_Details VALUES(@BillOfLading_ID, @SoVanDon, @NgayVanDon, @NuocPhatHanh, @DiaDiemCTQC, @LoaiVanDon)";
            string update = "UPDATE t_KDT_VNACCS_BillOfLading_Details SET BillOfLading_ID = @BillOfLading_ID, SoVanDon = @SoVanDon, NgayVanDon = @NgayVanDon, NuocPhatHanh = @NuocPhatHanh, DiaDiemCTQC = @DiaDiemCTQC, LoaiVanDon = @LoaiVanDon WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BillOfLading_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BillOfLading_ID", SqlDbType.BigInt, "BillOfLading_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocPhatHanh", SqlDbType.VarChar, "NuocPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemCTQC", SqlDbType.NVarChar, "DiaDiemCTQC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiVanDon", SqlDbType.Decimal, "LoaiVanDon", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BillOfLading_ID", SqlDbType.BigInt, "BillOfLading_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocPhatHanh", SqlDbType.VarChar, "NuocPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemCTQC", SqlDbType.NVarChar, "DiaDiemCTQC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiVanDon", SqlDbType.Decimal, "LoaiVanDon", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BillOfLading_Details VALUES(@BillOfLading_ID, @SoVanDon, @NgayVanDon, @NuocPhatHanh, @DiaDiemCTQC, @LoaiVanDon)";
            string update = "UPDATE t_KDT_VNACCS_BillOfLading_Details SET BillOfLading_ID = @BillOfLading_ID, SoVanDon = @SoVanDon, NgayVanDon = @NgayVanDon, NuocPhatHanh = @NuocPhatHanh, DiaDiemCTQC = @DiaDiemCTQC, LoaiVanDon = @LoaiVanDon WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BillOfLading_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BillOfLading_ID", SqlDbType.BigInt, "BillOfLading_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocPhatHanh", SqlDbType.VarChar, "NuocPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemCTQC", SqlDbType.NVarChar, "DiaDiemCTQC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiVanDon", SqlDbType.Decimal, "LoaiVanDon", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BillOfLading_ID", SqlDbType.BigInt, "BillOfLading_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocPhatHanh", SqlDbType.VarChar, "NuocPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemCTQC", SqlDbType.NVarChar, "DiaDiemCTQC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiVanDon", SqlDbType.Decimal, "LoaiVanDon", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_BillOfLading_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_BillOfLading_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_BillOfLading_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_BillOfLading_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_BillOfLading_Detail> SelectCollectionBy_BillOfLading_ID(long billOfLading_ID)
		{
            IDataReader reader = SelectReaderBy_BillOfLading_ID(billOfLading_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_BillOfLading_ID(long billOfLading_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectBy_BillOfLading_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BillOfLading_ID", SqlDbType.BigInt, billOfLading_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_BillOfLading_ID(long billOfLading_ID)
		{
			const string spName = "p_KDT_VNACCS_BillOfLading_Detail_SelectBy_BillOfLading_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BillOfLading_ID", SqlDbType.BigInt, billOfLading_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_BillOfLading_Detail(long billOfLading_ID, string soVanDon, DateTime ngayVanDon, string nuocPhatHanh, string diaDiemCTQC, decimal loaiVanDon)
		{
			KDT_VNACCS_BillOfLading_Detail entity = new KDT_VNACCS_BillOfLading_Detail();	
			entity.BillOfLading_ID = billOfLading_ID;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.NuocPhatHanh = nuocPhatHanh;
			entity.DiaDiemCTQC = diaDiemCTQC;
			entity.LoaiVanDon = loaiVanDon;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@BillOfLading_ID", SqlDbType.BigInt, BillOfLading_ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@NuocPhatHanh", SqlDbType.VarChar, NuocPhatHanh);
			db.AddInParameter(dbCommand, "@DiaDiemCTQC", SqlDbType.NVarChar, DiaDiemCTQC);
			db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.Decimal, LoaiVanDon);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_BillOfLading_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BillOfLading_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_BillOfLading_Detail(long id, long billOfLading_ID, string soVanDon, DateTime ngayVanDon, string nuocPhatHanh, string diaDiemCTQC, decimal loaiVanDon)
		{
			KDT_VNACCS_BillOfLading_Detail entity = new KDT_VNACCS_BillOfLading_Detail();			
			entity.ID = id;
			entity.BillOfLading_ID = billOfLading_ID;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.NuocPhatHanh = nuocPhatHanh;
			entity.DiaDiemCTQC = diaDiemCTQC;
			entity.LoaiVanDon = loaiVanDon;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_BillOfLading_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BillOfLading_ID", SqlDbType.BigInt, BillOfLading_ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@NuocPhatHanh", SqlDbType.VarChar, NuocPhatHanh);
			db.AddInParameter(dbCommand, "@DiaDiemCTQC", SqlDbType.NVarChar, DiaDiemCTQC);
			db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.Decimal, LoaiVanDon);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_BillOfLading_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BillOfLading_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_BillOfLading_Detail(long id, long billOfLading_ID, string soVanDon, DateTime ngayVanDon, string nuocPhatHanh, string diaDiemCTQC, decimal loaiVanDon)
		{
			KDT_VNACCS_BillOfLading_Detail entity = new KDT_VNACCS_BillOfLading_Detail();			
			entity.ID = id;
			entity.BillOfLading_ID = billOfLading_ID;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.NuocPhatHanh = nuocPhatHanh;
			entity.DiaDiemCTQC = diaDiemCTQC;
			entity.LoaiVanDon = loaiVanDon;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BillOfLading_ID", SqlDbType.BigInt, BillOfLading_ID);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@NuocPhatHanh", SqlDbType.VarChar, NuocPhatHanh);
			db.AddInParameter(dbCommand, "@DiaDiemCTQC", SqlDbType.NVarChar, DiaDiemCTQC);
			db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.Decimal, LoaiVanDon);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_BillOfLading_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BillOfLading_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_BillOfLading_Detail(long id)
		{
			KDT_VNACCS_BillOfLading_Detail entity = new KDT_VNACCS_BillOfLading_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public  int DeleteBy_BillOfLading_ID(long billOfLading_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteBy_BillOfLading_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BillOfLading_ID", SqlDbType.BigInt, billOfLading_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public  int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BillOfLading_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_BillOfLading_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BillOfLading_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}