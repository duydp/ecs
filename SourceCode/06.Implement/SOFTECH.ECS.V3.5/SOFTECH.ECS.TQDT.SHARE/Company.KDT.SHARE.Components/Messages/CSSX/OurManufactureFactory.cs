﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class OurManufactureFactory
    {
        /// <summary>
        /// Loại cơ sở sản xuất
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        /// <summary>
        /// Địa chỉ CSSX
        /// </summary>
        [XmlElement("address")]
        public string Address { get; set; }

        /// <summary>
        /// Địa chỉ trụ sở chính (Loại)
        /// </summary>
        [XmlElement("addressType")]
        public string AddressType { get; set; }


        /// <summary>
        /// Diện tích nhà xưởng
        /// </summary>
        [XmlElement("square")]
        public string Square { get; set; }

        /// <summary>
        /// Số lượng công nhân
        /// </summary>
        [XmlElement("workerQuantity")]
        public string WorkerQuantity { get; set; }

        /// <summary>
        /// Số lượng máy móc, dây truyền trang thiết bị
        /// </summary>
        [XmlElement("Machine")]
        public Company.KDT.SHARE.Components.Messages.CSSX.Machine Machine { get; set; }

        /// <summary>
        /// Ngành nghề
        /// </summary>
        [XmlElement("Careers")]
        public Careers Careers { get; set; }

        /// <summary>
        /// Chu kỳ sản xuất
        /// </summary>
        [XmlElement("Period")]
        public Products Period { get; set; }

        /// <summary>
        /// Năng lực sản xuất
        /// </summary>
        [XmlElement("ProductionCapacity")]
        public Products ProductionCapacity { get; set; }
    }
}
