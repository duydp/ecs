﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class File
    {
        /// <summary>
        /// Loại 
        /// 1: Tờ khai Hải quan
        /// 2. Vận đơn
        /// 9. Chứng từ khác
        /// </summary>
       [XmlElement("type")]
       public string Type { get; set; }

       /// <summary>
       /// Tên file
       /// </summary>
       [XmlElement("name")]
       public string Name { get; set; }

       /// <summary>
       /// Nội dung file
       /// </summary>
       [XmlElement("content")]
       public string Content { get; set; }

    }
}
