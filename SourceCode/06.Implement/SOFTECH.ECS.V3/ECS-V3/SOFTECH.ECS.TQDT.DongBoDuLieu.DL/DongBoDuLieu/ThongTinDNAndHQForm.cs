﻿using System;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using System.Threading;
using System.Data;
using System.Configuration;
using Company.KDT.SHARE.Components;

namespace DongBoDuLieu
{
    public partial class ThongTinDNAndHQForm : BaseForm
    {
        public WS.Service myService = new WS.Service();

        public ThongTinDNAndHQForm()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            txtMaDN.Text = WebService.LoadConfigure("MaDoanhNghiep");// GlobalSettings.MA_DON_VI;
            txtTenDN.Text = WebService.LoadConfigure("TenDoanhNghiep");// GlobalSettings.TEN_DON_VI;
            txtMaCuc.Text = WebService.LoadConfigure("MaHaiQuan"); // GlobalSettings.MA_CUC_HAI_QUAN;
            chChecked.Checked = bool.Parse(WebService.LoadConfigure("CHUNGTUDINHKEM"));

            //chkSoTK.Checked = bool.Parse(WebService.LoadConfigure("SuDungSoTK"));
            //txtSoTK.Value = Int32.Parse(WebService.LoadConfigure("SoTK"));
            string suDungSoTK = myService.CheckCauHinhSoToKhai(txtMaDN.Text, "SuDungSoTK");
            chkSoTK.Checked = suDungSoTK == "true" ? true : false;
            string soTK = myService.CheckCauHinhSoToKhai(txtMaDN.Text, "SoTK");
            txtSoTK.Value = soTK;
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            string mesage = "";

            try
            {
                containerValidator1.Validate();
                if (!containerValidator1.IsValid)
                    return;
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings.Remove("MaHaiQuan");
                config.AppSettings.Settings.Add("MaHaiQuan", txtMaCuc.Text);

                config.AppSettings.Settings.Remove("MaDoanhNghiep");
                config.AppSettings.Settings.Add("MaDoanhNghiep", txtMaDN.Text);

                config.AppSettings.Settings.Remove("TenDoanhNghiep");
                config.AppSettings.Settings.Add("TenDoanhNghiep", txtTenDN.Text);
                config.AppSettings.Settings.Remove("CHUNGTUDINHKEM");
                config.AppSettings.Settings.Add("CHUNGTUDINHKEM", chChecked.Checked.ToString());

                config.AppSettings.Settings.Remove("SuDungSoTK");
                config.AppSettings.Settings.Add("SuDungSoTK", chkSoTK.Checked.ToString());
                config.AppSettings.Settings.Remove("SoTK");
                config.AppSettings.Settings.Add("SoTK", txtSoTK.Value.ToString());

                myService.InsertUpdateCauHinhDaiLy(txtMaDN.Text, txtTenDN.Text, "SuDungSoTK", chkSoTK.Checked ? "true" : "false", "");
                myService.InsertUpdateCauHinhDaiLy(txtMaDN.Text, txtTenDN.Text, "SoTK", txtSoTK.Text, "");

                config.Save(ConfigurationSaveMode.Full);
                ConfigurationManager.RefreshSection("appSettings");
                //this.Close();

                mesage = "Lưu thông tin thành công!.";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                mesage = "Lưu thông tin không thành công!.";
            }

            MessageBox.Show(mesage, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}