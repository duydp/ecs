﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Helper.Controls;
using System.Security.Cryptography;


namespace Company.Interface.DongBoDuLieu_New
{
    public class ServiceDongBoDuLieu
    {
        public static Company.Interface.wssyndata.Service myService = new Company.Interface.wssyndata.Service();

        public static string GetUrlServiceDongBoDuLieu()
        {
            string url = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("WS_SyncData");
            ServiceDongBoDuLieu.myService.Url = url;

            return url;
        }

        public static bool CheckServiceDongBoDuLieu()
        {
            try
            {
                GetUrlServiceDongBoDuLieu();

               //Company.Interface.DaiLy daiLy = ServiceDongBoDuLieu.myService.LoginDaiLy("", "");


               //if (daiLy == null)
               // {
               //     MessageBoxControlV.ShowMessage("Không thể kết nối đến webservice!", false);
               // }

                //return daiLy != null;
                return false;
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Unable to connect to the remote server"))
                    MessageBoxControlV.ShowMessage("Không thể kết nối đến webservice!", false);
                else if (ex.ToString().Contains("time out"))
                    MessageBoxControlV.ShowMessage("Thời gian kết nối đến service đồng bộ dữ liệu quá lâu!", false);
                else
                    MessageBoxControlV.ShowMessage("Xảy ra lỗi: " + ex.ToString(), false);
                return false;
            }
        }

        public static string GetMD5Value(string data)
        {
            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();
        }

        public static string EncryptPassword(string password)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] hashBytes = encoding.GetBytes(password);

            // compute SHA-1 hash.
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytePassword = sha1.ComputeHash(hashBytes);
            string cryptPassword = Convert.ToBase64String(bytePassword);
            return cryptPassword;
        }
    }
}
