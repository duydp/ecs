﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif

namespace Company.Interface
{
    public partial class ListHoaDonThuongMaiTKMDForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public bool isBrowse = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListHoaDonThuongMaiTKMDForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NguyenTe_ID"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value.ToString());
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKMD.HoaDonThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            //if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    btnTaoMoi.Enabled = false;
            //    btnXoa.Enabled = false;
            //}
            SetButtonStateHOADON(TKMD, isKhaiBoSung);
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOADON(ToKhaiMauDich tkmd, bool isKhaiBoSung)
        {
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    );

                btnXoa.Enabled = status;
                btnTaoMoi.Enabled = status;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    status = true;

                    btnXoa.Enabled = status;
                    btnTaoMoi.Enabled = status;
                }
            }

            return true;
        }

        #endregion


        private void btnXoa_Click(object sender, EventArgs e)
        {

            List<HoaDonThuongMai> listHoaDon = new List<HoaDonThuongMai>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonThuongMai hd = (HoaDonThuongMai)i.GetRow().DataRow;
                        listHoaDon.Add(hd);
                    }
                }
            }
            //Edit by KHANHHN - 03/03/2012
            // Kiểm tra rồi mới xóa
            foreach (HoaDonThuongMai hd in listHoaDon)
            {
                if (hd.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || hd.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    hd.DeleteAll();
                    TKMD.HoaDonThuongMaiCollection.Remove(hd);
                }
                else
                {
                    this.ShowMessage(string.Format("Hợp đồng thương mại bổ sung số : {0} đã được gửi đến hải quan. Không thể xóa", hd.SoHoaDon), false);
                }
            }

            dgList.DataSource = TKMD.HoaDonThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }


        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            HoaDonThuongMaiForm f = new HoaDonThuongMaiForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog(this);
            dgList.DataSource = TKMD.HoaDonThuongMaiCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //if (!isBrowse)
            //{
                HoaDonThuongMaiForm f = new HoaDonThuongMaiForm();
                f.TKMD = TKMD;
                f.isKhaiBoSung = this.isKhaiBoSung;
                f.HDonTM = (HoaDonThuongMai)e.Row.DataRow;
                f.ShowDialog(this);
                dgList.DataSource = TKMD.HoaDonThuongMaiCollection;
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            //}
            //else
            //{
            //    HoaDonThuongMai hoadonTM = (HoaDonThuongMai)e.Row.DataRow;
            //    TKMD.SoHoaDonThuongMai = hoadonTM.SoHoaDon;
            //    TKMD.NgayHoaDonThuongMai = hoadonTM.NgayHoaDon;
            //    Close();
            //}
        }




    }
}
