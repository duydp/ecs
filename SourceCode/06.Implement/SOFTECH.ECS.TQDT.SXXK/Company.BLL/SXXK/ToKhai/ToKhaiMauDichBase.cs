﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.BLL.SXXK.ThanhKhoan;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using Company.KDT.SHARE.Components.Utils;
namespace Company.BLL.SXXK.ToKhai
{
    public partial class ToKhaiMauDich : EntityBase
    {
        #region Private members.

        protected string _MaHaiQuan = String.Empty;
        protected int _SoToKhai;
        protected string _MaLoaiHinh = String.Empty;
        protected short _NamDangKy;
        protected DateTime _NgayDangKy = new DateTime(1900, 01, 01);
        protected string _MaDoanhNghiep = String.Empty;
        protected string _TenDoanhNghiep = String.Empty;
        protected string _MaDaiLyTTHQ = String.Empty;
        protected string _TenDaiLyTTHQ = String.Empty;
        protected string _TenDonViDoiTac = String.Empty;
        protected string _ChiTietDonViDoiTac = String.Empty;
        protected string _SoGiayPhep = String.Empty;
        protected DateTime _NgayGiayPhep = new DateTime(1900, 01, 01);
        protected DateTime _NgayHetHanGiayPhep = new DateTime(1900, 01, 01);
        protected string _SoHopDong = String.Empty;
        protected DateTime _NgayHopDong = new DateTime(1900, 01, 01);
        protected DateTime _NgayHetHanHopDong = new DateTime(1900, 01, 01);
        protected string _SoHoaDonThuongMai = String.Empty;
        protected DateTime _NgayHoaDonThuongMai = new DateTime(1900, 01, 01);
        protected string _PTVT_ID = String.Empty;
        protected string _SoHieuPTVT = String.Empty;
        protected DateTime _NgayDenPTVT = new DateTime(1900, 01, 01);
        protected string _QuocTichPTVT_ID = String.Empty;
        protected string _LoaiVanDon = String.Empty;
        protected string _SoVanDon = String.Empty;
        protected DateTime _NgayVanDon = new DateTime(1900, 01, 01);
        protected string _NuocXK_ID = String.Empty;
        protected string _NuocNK_ID = String.Empty;
        protected string _DiaDiemXepHang = String.Empty;
        protected string _CuaKhau_ID = String.Empty;
        protected string _DKGH_ID = String.Empty;
        protected string _NguyenTe_ID = String.Empty;
        protected decimal _TyGiaTinhThue;
        protected decimal _TyGiaUSD;
        protected string _PTTT_ID = String.Empty;
        protected short _SoHang;
        protected short _SoLuongPLTK;
        protected string _TenChuHang = String.Empty;
        protected decimal _SoContainer20;
        protected decimal _SoContainer40;
        protected decimal _SoKien;
        protected decimal _TrongLuong;
        protected decimal _TongTriGiaKhaiBao;
        protected decimal _TongTriGiaTinhThue;
        protected string _LoaiToKhaiGiaCong = String.Empty;
        protected decimal _LePhiHaiQuan;
        protected decimal _PhiBaoHiem;
        protected decimal _PhiVanChuyen;
        protected decimal _PhiXepDoHang;
        protected decimal _PhiKhac;
        protected string _Xuat_NPL_SP = String.Empty;
        protected string _ThanhLy = String.Empty;
        protected DateTime _NGAY_THN_THX = new DateTime(1900, 01, 01);
        protected DateTime _NGayHoanThanh = new DateTime(1900, 01, 01);
        protected string _MaDonViUT = String.Empty;
        private HangMauDichCollection _HMDCollection = new HangMauDichCollection();
        protected string _TrangThaiThanhKhoan = String.Empty;
        protected string _ChungTu = String.Empty;
        protected string _PhanLuong = String.Empty;
        protected int _TrangThai;
        protected double _TrongLuongNet;
        protected decimal _SoTienKhoan = 0;
        protected string _GhiChu = "";
        protected double _HeSoNhan = 1;
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.
        public double HeSoNhan
        {
            set { this._HeSoNhan = value; }
            get { return this._HeSoNhan; }
        }
        public DateTime NgayHoanThanh
        {
            set { this._NGayHoanThanh = value; }
            get { return this._NGayHoanThanh; }
        }


        public string PhanLuong
        {
            set { this._PhanLuong = value; }
            get { return this._PhanLuong; }
        }

        public string ChungTu
        {
            set { this._ChungTu = value; }
            get { return this._ChungTu; }
        }
        public HangMauDichCollection HMDCollection
        {
            set { this._HMDCollection = value; }
            get { return this._HMDCollection; }
        }

        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }
        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public short NamDangKy
        {
            set { this._NamDangKy = value; }
            get { return this._NamDangKy; }
        }
        public DateTime NgayDangKy
        {
            set { this._NgayDangKy = value; }
            get { return this._NgayDangKy; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public string TenDoanhNghiep
        {
            set { this._TenDoanhNghiep = value; }
            get { return this._TenDoanhNghiep; }
        }
        public string MaDaiLyTTHQ
        {
            set { this._MaDaiLyTTHQ = value; }
            get { return this._MaDaiLyTTHQ; }
        }
        public string TenDaiLyTTHQ
        {
            set { this._TenDaiLyTTHQ = value; }
            get { return this._TenDaiLyTTHQ; }
        }
        public string TenDonViDoiTac
        {
            set { this._TenDonViDoiTac = value; }
            get { return this._TenDonViDoiTac; }
        }
        public string ChiTietDonViDoiTac
        {
            set { this._ChiTietDonViDoiTac = value; }
            get { return this._ChiTietDonViDoiTac; }
        }
        public string SoGiayPhep
        {
            set { this._SoGiayPhep = value; }
            get { return this._SoGiayPhep; }
        }
        public DateTime NgayGiayPhep
        {
            set { this._NgayGiayPhep = value; }
            get { return this._NgayGiayPhep; }
        }
        public DateTime NgayHetHanGiayPhep
        {
            set { this._NgayHetHanGiayPhep = value; }
            get { return this._NgayHetHanGiayPhep; }
        }
        public string SoHopDong
        {
            set { this._SoHopDong = value; }
            get { return this._SoHopDong; }
        }
        public DateTime NgayHopDong
        {
            set { this._NgayHopDong = value; }
            get { return this._NgayHopDong; }
        }
        public DateTime NgayHetHanHopDong
        {
            set { this._NgayHetHanHopDong = value; }
            get { return this._NgayHetHanHopDong; }
        }
        public string SoHoaDonThuongMai
        {
            set { this._SoHoaDonThuongMai = value; }
            get { return this._SoHoaDonThuongMai; }
        }
        public DateTime NgayHoaDonThuongMai
        {
            set { this._NgayHoaDonThuongMai = value; }
            get { return this._NgayHoaDonThuongMai; }
        }
        public string PTVT_ID
        {
            set { this._PTVT_ID = value; }
            get { return this._PTVT_ID; }
        }
        public string SoHieuPTVT
        {
            set { this._SoHieuPTVT = value; }
            get { return this._SoHieuPTVT; }
        }
        public DateTime NgayDenPTVT
        {
            set { this._NgayDenPTVT = value; }
            get { return this._NgayDenPTVT; }
        }
        public string QuocTichPTVT_ID
        {
            set { this._QuocTichPTVT_ID = value; }
            get { return this._QuocTichPTVT_ID; }
        }
        public string LoaiVanDon
        {
            set { this._LoaiVanDon = value; }
            get { return this._LoaiVanDon; }
        }
        public string SoVanDon
        {
            set { this._SoVanDon = value; }
            get { return this._SoVanDon; }
        }
        public DateTime NgayVanDon
        {
            set { this._NgayVanDon = value; }
            get { return this._NgayVanDon; }
        }
        public string NuocXK_ID
        {
            set { this._NuocXK_ID = value; }
            get { return this._NuocXK_ID; }
        }
        public string NuocNK_ID
        {
            set { this._NuocNK_ID = value; }
            get { return this._NuocNK_ID; }
        }
        public string DiaDiemXepHang
        {
            set { this._DiaDiemXepHang = value; }
            get { return this._DiaDiemXepHang; }
        }
        public string CuaKhau_ID
        {
            set { this._CuaKhau_ID = value; }
            get { return this._CuaKhau_ID; }
        }
        public string DKGH_ID
        {
            set { this._DKGH_ID = value; }
            get { return this._DKGH_ID; }
        }
        public string NguyenTe_ID
        {
            set { this._NguyenTe_ID = value; }
            get { return this._NguyenTe_ID; }
        }
        public decimal TyGiaTinhThue
        {
            set { this._TyGiaTinhThue = value; }
            get { return this._TyGiaTinhThue; }
        }
        public decimal TyGiaUSD
        {
            set { this._TyGiaUSD = value; }
            get { return this._TyGiaUSD; }
        }
        public string PTTT_ID
        {
            set { this._PTTT_ID = value; }
            get { return this._PTTT_ID; }
        }
        public short SoHang
        {
            set { this._SoHang = value; }
            get { return this._SoHang; }
        }
        public short SoLuongPLTK
        {
            set { this._SoLuongPLTK = value; }
            get { return this._SoLuongPLTK; }
        }
        public string TenChuHang
        {
            set { this._TenChuHang = value; }
            get { return this._TenChuHang; }
        }
        public decimal SoContainer20
        {
            set { this._SoContainer20 = value; }
            get { return this._SoContainer20; }
        }
        public decimal SoContainer40
        {
            set { this._SoContainer40 = value; }
            get { return this._SoContainer40; }
        }
        public decimal SoKien
        {
            set { this._SoKien = value; }
            get { return this._SoKien; }
        }
        public decimal TrongLuong
        {
            set { this._TrongLuong = value; }
            get { return this._TrongLuong; }
        }
        public decimal TongTriGiaKhaiBao
        {
            set { this._TongTriGiaKhaiBao = value; }
            get { return this._TongTriGiaKhaiBao; }
        }
        public decimal TongTriGiaTinhThue
        {
            set { this._TongTriGiaTinhThue = value; }
            get { return this._TongTriGiaTinhThue; }
        }
        public string LoaiToKhaiGiaCong
        {
            set { this._LoaiToKhaiGiaCong = value; }
            get { return this._LoaiToKhaiGiaCong; }
        }
        public decimal LePhiHaiQuan
        {
            set { this._LePhiHaiQuan = value; }
            get { return this._LePhiHaiQuan; }
        }
        public decimal PhiBaoHiem
        {
            set { this._PhiBaoHiem = value; }
            get { return this._PhiBaoHiem; }
        }
        public decimal PhiVanChuyen
        {
            set { this._PhiVanChuyen = value; }
            get { return this._PhiVanChuyen; }
        }
        public decimal PhiXepDoHang
        {
            set { this._PhiXepDoHang = value; }
            get { return this._PhiXepDoHang; }
        }
        public decimal PhiKhac
        {
            set { this._PhiKhac = value; }
            get { return this._PhiKhac; }
        }
        public string Xuat_NPL_SP
        {
            set { this._Xuat_NPL_SP = value; }
            get { return this._Xuat_NPL_SP; }
        }
        public string ThanhLy
        {
            set { this._ThanhLy = value; }
            get { return this._ThanhLy; }
        }
        public DateTime NGAY_THN_THX
        {
            set { this._NGAY_THN_THX = value; }
            get { return this._NGAY_THN_THX; }
        }
        public string MaDonViUT
        {
            set { this._MaDonViUT = value; }
            get { return this._MaDonViUT; }
        }
        public string TrangThaiThanhKhoan
        {
            set { this._TrangThaiThanhKhoan = value; }
            get { return this._TrangThaiThanhKhoan; }
        }
        public double TrongLuongNet
        {
            set { this._TrongLuongNet = value; }
            get { return this._TrongLuongNet; }
        }
        public decimal SoTienKhoan
        {
            set { this._SoTienKhoan = value; }
            get { return this._SoTienKhoan; }
        }

        //Bổ sung
        public int TrangThai
        {
            set { this._TrangThai = value; }
            get { return this._TrangThai; }
        }
        public string GhiChu
        {
            set { this._GhiChu = value; }
            get { return this._GhiChu; }
        }
        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.
        public void LoadHMDCollection()
        {
            HangMauDich hmd = new HangMauDich();
            hmd.MaHaiQuan = this.MaHaiQuan;
            hmd.MaLoaiHinh = this.MaLoaiHinh;
            hmd.SoToKhai = this.SoToKhai;
            hmd.NamDangKy = this.NamDangKy;
            this.HMDCollection = hmd.SelectCollectionBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy();
        }
        public bool Load()
        {
            string spName = "p_SXXK_ToKhaiMauDich_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) this._Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) this._NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) this.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) this.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) this._TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));

                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) this._HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet GetThongBao(uint hanthanhkhoan, uint thoigiantk)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            string sql = "SELECT *, (@HanThanhKhoan - DATEDIFF(day, NgayDangKy, GETDATE())) as songay " +
                        "FROM         t_SXXK_ToKhaiMauDich " +
                        "WHERE     (275 - DATEDIFF(day, NgayDangKy, GETDATE()) <=@ThoiGianTK) AND ThanhLy!='H' AND" +
                        " maloaihinh like 'NSX%'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HanThanhKhoan", SqlDbType.Int, hanthanhkhoan);
            db.AddInParameter(dbCommand, "@ThoiGianTK", SqlDbType.Int, thoigiantk);
            return this.db.ExecuteDataSet(dbCommand);
        }

        public DataSet SelectAll()
        {
            string spName = "p_SXXK_ToKhaiMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ToKhaiMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ToKhaiMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ToKhaiMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKNChuaThanhLy(string maDoanhNghiep, string maHaiQuan, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKNChuaThanhLy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@maxDate", SqlDbType.DateTime, maxDate);

            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXChuaThanhLy(string maDoanhNghiep, string maHaiQuan)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXChuaThanhLyMinDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLyMinDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@MinDate", SqlDbType.DateTime, minDate);

            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLyDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);

            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXGCDaPhanBoByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKXGCDaPhanBoDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);

            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKNChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate, string userName, string tenChuHang)
        {
            string spName = "p_SXXK_GetDSTKNChuaThanhLyDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);
            this.db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, userName);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, tenChuHang);
            return this.db.ExecuteReader(dbCommand);
        }

        public ToKhaiMauDichCollection GetDSTKNChuaThanhLyByDate2(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate, string userName, string tenChuHang)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            string spName = "SELECT " +
                             "A.MaHaiQuan, " +
                             "A.SoToKhai, " +
                             "A.MaLoaiHinh, " +
                             "A.NamDangKy, " +
                             "A.NgayDangKy, " +
                             "CASE A.NGAY_THN_THX " +
                                "WHEN '1/1/1900' THEN A.NgayDangKy " +
                                "ELSE A.NGAY_THN_THX " +
                             "END AS NGAY_THN_THX " +
                        "FROM t_SXXK_ToKhaiMauDich A " +
                            "LEFT JOIN (SELECT DISTINCT " +
                                        "SoToKhai, " +
                                        "MaLoaiHinh, " +
                                        "NamDangKy, " +
                                        "MaHaiQuan " +
                                    "FROM t_KDT_SXXK_BKToKhaiNhap " +
                                    " WHERE " +
                                        " UserName != @UserName) D ON " +
                                "A.SoToKhai=D.SoToKhai AND " +
                                "A.MaLoaiHinh=D.MaLoaiHinh AND " +
                                "A.NamDangKy=D.NamDangKy AND " +
                                "A.MaHaiQuan=D.MaHaiQuan " +
                        "WHERE " +
                            "A.MaDoanhNghiep = @MaDoanhNghiep AND " +
                            "A.MaHaiQuan	= @MaHaiQuan AND " +
                            "D.SoToKhai IS NULL AND " +
                            "A.MaLoaiHinh LIKE 'N%'AND " +
                            "A.TenChuHang LIKE '%' + @TenChuHang + '%' AND " +
                            "A.NgayDangKy between @TuNgay and @DenNgay AND  " +
                            "A.THANHLY <> 'H' " +
                        "ORDER BY NGAY_THN_THX, NgayDangKy, SoToKhai";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);
            this.db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, userName);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, tenChuHang);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }


        public DataSet GetDSTKNTonChuaThanhLyByDateAndTenChuHang(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate, string userName, string tenChuHang)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            string spName = "SELECT " +
                             "A.MaHaiQuan, " +
                             "A.SoToKhai, " +
                             "A.MaLoaiHinh, " +
                             "A.NamDangKy, " +
                             "T.TON," +
                             "T.MANPL," +
                             "CASE A.NGAY_THN_THX " +
                                "WHEN '1/1/1900' THEN A.NgayDangKy " +
                                "ELSE A.NGAY_THN_THX " +
                             "END AS NgayThucNhap " +
                        "FROM t_SXXK_ToKhaiMauDich A " +
                            "LEFT JOIN (SELECT DISTINCT " +
                                        "SoToKhai, " +
                                        "MaLoaiHinh, " +
                                        "NamDangKy, " +
                                        "MaHaiQuan " +
                                    "FROM t_KDT_SXXK_BKToKhaiNhap " +
                                    " WHERE " +
                                        " UserName != @UserName) D ON " +
                                "A.SoToKhai=D.SoToKhai AND " +
                                "A.MaLoaiHinh=D.MaLoaiHinh AND " +
                                "A.NamDangKy=D.NamDangKy AND " +
                                "A.MaHaiQuan=D.MaHaiQuan " +
                            "left join t_SXXK_ThanhLy_NPLNhapTon T ON " +
                              "A.SoToKhai=T.SoToKhai AND " +
                                "A.MaLoaiHinh=T.MaLoaiHinh AND " +
                                "A.NamDangKy=T.NamDangKy AND " +
                                "A.MaHaiQuan=T.MaHaiQuan " +
                        "WHERE " +
                            "A.MaDoanhNghiep = @MaDoanhNghiep AND " +
                            "A.MaHaiQuan	= @MaHaiQuan AND " +
                            "D.SoToKhai IS NULL AND " +
                            "A.MaLoaiHinh LIKE 'N%'AND " +
                            "A.TenChuHang LIKE '%' + @TenChuHang + '%' AND " +
                            "A.NgayDangKy between @TuNgay and @DenNgay AND  " +
                            "A.THANHLY <> 'H' and " +
                            "T.TON > 0 " +
                        "ORDER BY NGAY_THN_THX, NgayDangKy, SoToKhai";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);
            this.db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, userName);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, tenChuHang);


            return db.ExecuteDataSet(dbCommand);
        }

        public IDataReader SelectReaderDSTKNCuaToKhaiXuatDaPhanBo(string maDoanhNghiep, string maHaiQuan, string where)
        {
            Database dbGC = DatabaseFactory.CreateDatabase("ECS_GC");
            string sql = "SELECT DISTINCT SoToKhaiNhap, MaLoaiHinhNhap, NamDangKyNhap, NgayDangKyNhap, MaHaiQuanNhap FROM v_GC_PhanBo WHERE " + where;

            DbCommand dbCommand = dbGC.GetSqlStringCommand(sql);
            dbGC.AddInParameter(dbCommand, "@MaDoanhNghiep", DbType.String, maDoanhNghiep);
            dbGC.AddInParameter(dbCommand, "@MaHaiQuan", DbType.String, maHaiQuan);

            return dbGC.ExecuteReader(dbCommand);
        }
        public DataTable SelectPhanBoCuaToKhaiXuat(Company.BLL.KDT.SXXK.BKToKhaiXuatCollection bkTKXCollection)
        {

            Database dbGC = DatabaseFactory.CreateDatabase("ECS_GC");
            string sql = "SELECT SoToKhaiXuat,MaLoaiHinhXuat, MaHaiQuanXuat,NamDangKyXuat, " +
                        "SoToKhaiNhap,MaLoaiHinhNhap, MaHaiQuanNhap,NamDangKyNhap, MaNPL, NgayDangKyNhap , Sum(LuongPhanBo)as LuongPhanBo " +
                        "FROM v_GC_PhanBo " +
                        "WHERE MaLoaiHinhNhap LIKE 'NSX%' AND ";

            string where = " CAST(SoToKhaiXuat as nvarchar(5)) + Cast(MaLoaiHinhXuat as nvarchar(5)) + CAST(NamDangKyXuat as Nvarchar(4)) IN (";
            foreach (Company.BLL.KDT.SXXK.BKToKhaiXuat bktkx in bkTKXCollection)
            {
                where += "'" + bktkx.SoToKhai + bktkx.MaLoaiHinh + bktkx.NamDangKy + "',";
            }
            where = where.Remove(where.Length - 1);
            where += ")";
            sql += where;
            sql += " GROUP BY SoToKhaiXuat,MaLoaiHinhXuat, MaHaiQuanXuat,NamDangKyXuat, " +
                  "SoToKhaiNhap,MaLoaiHinhNhap, MaHaiQuanNhap,NamDangKyNhap, MaNPL ,NgayDangKyNhap ";
            DbCommand dbCommand = dbGC.GetSqlStringCommand(sql);
            return dbGC.ExecuteDataSet(dbCommand).Tables[0];
        }
        //---------------------------------------------------------------------------------------------

        public ToKhaiMauDichCollection SelectCollectionAll()
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiMauDichCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("HeSoNhan"))) entity.HeSoNhan = reader.GetDouble(reader.GetOrdinal("HeSoNhan"));
                collection.Add(entity);

            }
            reader.Close();
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKNChuaThanhLy(string maDoanhNghiep, string maHaiQuan, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKNChuaThanhLy(maDoanhNghiep, maHaiQuan, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLy(string maDoanhNghiep, string maHaiQuan)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLy(maDoanhNghiep, maHaiQuan);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLyMinDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLyMinDate(maDoanhNghiep, maHaiQuan, minDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLyByDate(maDoanhNghiep, maHaiQuan, minDate, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXGCDaPhanBoByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXGCDaPhanBoByDate(maDoanhNghiep, maHaiQuan, minDate, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKNChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate, string userName, string tenChuHang)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKNChuaThanhLyByDate(maDoanhNghiep, maHaiQuan, minDate, maxDate, userName, tenChuHang);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public Company.BLL.KDT.SXXK.BKToKhaiNhapCollection GetDSTKNCuaToKhaiXuatDaPhanBo(string maDoanhNghiep, string maHaiQuan, Company.BLL.KDT.SXXK.BKToKhaiXuatCollection bkTKXCollection)
        {
            string where = " CAST(SoToKhaiXuat as nvarchar(5)) + Cast(MaLoaiHinhXuat as nvarchar(5)) + CAST(NamDangKyXuat as Nvarchar(4)) IN (";
            foreach (Company.BLL.KDT.SXXK.BKToKhaiXuat bktkx in bkTKXCollection)
            {
                where += "'" + bktkx.SoToKhai + bktkx.MaLoaiHinh + bktkx.NamDangKy + "',";
            }
            where = where.Remove(where.Length - 1);
            where += ") AND MaLoaiHinhNhap LIKE 'NSX%'";
            Company.BLL.KDT.SXXK.BKToKhaiNhapCollection collection = new Company.BLL.KDT.SXXK.BKToKhaiNhapCollection();
            IDataReader reader = this.SelectReaderDSTKNCuaToKhaiXuatDaPhanBo(maDoanhNghiep, maHaiQuan, where);
            while (reader.Read())
            {
                Company.BLL.KDT.SXXK.BKToKhaiNhap entity = new Company.BLL.KDT.SXXK.BKToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayThucNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ToKhaiMauDich_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
            this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
            this.db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            this.db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@HeSoNhan", SqlDbType.Float, this._HeSoNhan);
            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(ToKhaiMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiMauDich item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, ToKhaiMauDichCollection collection)
        {
            foreach (ToKhaiMauDich item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        //**************** InsertUpdateFull() ******************
        public void InsertUpdateFull()
        {
            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    // this.InsertUpdateTransaction(transaction);
                    //this.UpdateTransaction(transaction);
                    this.NamDangKy = (short)this.NgayDangKy.Year;
                    this.InsertUpdateTransaction(transaction);

                    HangMauDich HMDDelete = new HangMauDich();
                    HMDDelete.MaHaiQuan = this.MaHaiQuan;
                    HMDDelete.MaLoaiHinh = this.MaLoaiHinh;
                    HMDDelete.SoToKhai = this.SoToKhai;
                    HMDDelete.NamDangKy = (short)this.NgayDangKy.Year;

                    HMDDelete.DeleteByForeigKeyTransaction(transaction);
                    short STTHang = 1;
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {

                        hmd.MaHaiQuan = this.MaHaiQuan;
                        hmd.MaLoaiHinh = this.MaLoaiHinh;

                        hmd.NamDangKy = (short)this.NgayDangKy.Year;
                        hmd.SoThuTuHang = STTHang;
                        hmd.SoToKhai = this.SoToKhai;
                        STTHang++;
                    }
                    if (this.MaLoaiHinh.StartsWith("N"))
                    {
                        NPLNhapTon nplDelete = new NPLNhapTon();
                        nplDelete.SoToKhai = this.SoToKhai;
                        nplDelete.MaLoaiHinh = this.MaLoaiHinh;
                        nplDelete.NamDangKy = this.NamDangKy;
                        nplDelete.MaHaiQuan = this.MaHaiQuan;
                        nplDelete.DeleteTransactionByToKhai(transaction);

                        NPLNhapTonThucTe nplThucTeDelete = new NPLNhapTonThucTe();
                        nplThucTeDelete.SoToKhai = this.SoToKhai;
                        nplThucTeDelete.MaLoaiHinh = this.MaLoaiHinh;
                        nplThucTeDelete.NamDangKy = this.NamDangKy;
                        nplThucTeDelete.MaHaiQuan = this.MaHaiQuan;
                        nplThucTeDelete.DeleteTransactionByToKhai(transaction);
                    }
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {

                        hmd.InsertUpdateTransaction(transaction);
                        if (this.MaLoaiHinh.StartsWith("N"))
                        {

                            //cap nhat vao hang NPL nhap ton
                            BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                            nplNhapTon.SoToKhai = this.SoToKhai;
                            nplNhapTon.MaLoaiHinh = this.MaLoaiHinh;
                            nplNhapTon.NamDangKy = this.NamDangKy;
                            nplNhapTon.MaHaiQuan = this.MaHaiQuan;
                            nplNhapTon.MaNPL = hmd.MaPhu;
                            nplNhapTon.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplNhapTon.Luong = hmd.SoLuong;
                            nplNhapTon.Ton = hmd.Ton; //
                            nplNhapTon.ThueXNK = Convert.ToDouble(hmd.ThueXNK);
                            nplNhapTon.ThueTTDB = Convert.ToDouble(hmd.ThueTTDB);
                            nplNhapTon.ThueVAT = Convert.ToDouble(hmd.ThueGTGT);
                            nplNhapTon.PhuThu = Convert.ToDouble(hmd.PhuThu);
                            nplNhapTon.ThueCLGia = Convert.ToDouble(hmd.TriGiaThuKhac);
                            nplNhapTon.InsertUpdateTransaction(transaction);

                            NPLNhapTonThucTe nplNhapTonThucTe = new NPLNhapTonThucTe();
                            nplNhapTonThucTe.SoToKhai = this.SoToKhai;
                            nplNhapTonThucTe.MaLoaiHinh = this.MaLoaiHinh;
                            nplNhapTonThucTe.NamDangKy = this.NamDangKy;
                            nplNhapTonThucTe.MaHaiQuan = this.MaHaiQuan;
                            nplNhapTonThucTe.MaNPL = hmd.MaPhu;
                            nplNhapTonThucTe.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplNhapTonThucTe.Luong = hmd.SoLuong;
                            nplNhapTonThucTe.Ton = hmd.Ton; //
                            nplNhapTonThucTe.ThueXNK = Convert.ToDouble(hmd.ThueXNK);
                            nplNhapTonThucTe.ThueTTDB = Convert.ToDouble(hmd.ThueTTDB);
                            nplNhapTonThucTe.ThueVAT = Convert.ToDouble(hmd.ThueGTGT);
                            nplNhapTonThucTe.PhuThu = Convert.ToDouble(hmd.PhuThu);
                            nplNhapTonThucTe.ThueCLGia = Convert.ToDouble(hmd.TriGiaThuKhac);
                            nplNhapTonThucTe.NgayDangKy = this.NgayDangKy;

                            nplNhapTonThucTe.InsertUpdate(transaction);

                        }

                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        //***********************************
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ToKhaiMauDich_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
            this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
            this.db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            this.db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@HeSoNhan", SqlDbType.Float, this._HeSoNhan);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(ToKhaiMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiMauDich item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ToKhaiMauDich_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
            this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
            this.db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            this.db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
            this.db.AddInParameter(dbCommand, "@TrongLuongNet", SqlDbType.Float, this._TrongLuongNet);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@HeSoNhan", SqlDbType.Float, this._HeSoNhan);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(ToKhaiMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiMauDich item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }
        public void UpdateCollection(ToKhaiMauDichCollection collection)
        {
            using (SqlConnection connection = (SqlConnection)this.db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (ToKhaiMauDich item in collection)
                    {
                        item.UpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.



        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ToKhaiMauDich_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(ToKhaiMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiMauDich item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------


        #endregion

        #region WS



        public static long DongBoDuLieuHaiQuanAll(string mahaiquan, string madv, DataSet ds, DataSet dsHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            DataRow row2 = null;
            //if (ds.Tables[0].Rows.Count > 0)
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        if (ds != null)

                            //ds.WriteXml("c:\\DongBoToKhai_2010.xml");

                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                                tkmd.MaLoaiHinh = Convert.ToString(row["Ma_LH"]);
                                tkmd.MaDaiLyTTHQ = Convert.ToString(row["MA_DVKT"]);
                                tkmd.MaHaiQuan = mahaiquan;
                                //tkmd.TenDaiLyTTHQ = Convert.ToString(row[0]["TenDLTTHQ"]);
                                tkmd.MaDonViUT = Convert.ToString(row["MA_DVUT"]);
                                tkmd.TenDonViDoiTac = Convert.ToString(row["DV_DT"]);
                                tkmd.SoGiayPhep = Convert.ToString(row["So_GP"]);
                                tkmd.SoToKhai = Convert.ToInt32(row["SOTK"]);
                                tkmd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                                tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;
                                try
                                {
                                    tkmd.NGAY_THN_THX = Convert.ToDateTime(row["NGAY_THN_THX"]);
                                }
                                catch { tkmd.NGAY_THN_THX = new DateTime(1900, 1, 1); }
                                if (row["Ngay_GP"].ToString() != "")
                                    tkmd.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                                try
                                {
                                    if (row["Ngay_HHGP"].ToString() != "")
                                        tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                                }
                                catch (Exception exx) { }
                                tkmd.SoHopDong = Convert.ToString(row["So_HD"].ToString());
                                if (row["Ngay_HD"].ToString() != "")
                                    tkmd.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                                if (row["Ngay_HHHD"].ToString() != "")
                                    tkmd.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                                tkmd.SoHoaDonThuongMai = Convert.ToString(row["So_HDTM"]);
                                if (row["Ngay_HDTM"].ToString() != "")
                                    tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM".ToString()]);
                                tkmd.PTVT_ID = Convert.ToString(row["Ma_PTVT"]);
                                tkmd.SoHieuPTVT = Convert.ToString(row["Ten_PTVT"]);
                                if (row["NgayDen"].ToString() != "")
                                    tkmd.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                                tkmd.LoaiVanDon = Convert.ToString(row["Van_Don"]);
                                if (row["Ngay_VanDon"].ToString() != "")
                                    tkmd.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                                tkmd.NuocXK_ID = Convert.ToString(row["Nuoc_XK"]);
                                tkmd.NuocNK_ID = Convert.ToString(row["Nuoc_NK"]);
                                tkmd.DiaDiemXepHang = Convert.ToString(row["CangNN"]);
                                tkmd.DKGH_ID = Convert.ToString(row["Ma_GH"]);
                                tkmd.CuaKhau_ID = Convert.ToString(row["Ma_CK"]);
                                tkmd.NguyenTe_ID = Convert.ToString(row["Ma_NT"]);
                                tkmd.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                                if (row["TyGia_USD"].ToString() != "")
                                    tkmd.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                                tkmd.PTTT_ID = Convert.ToString(row["Ma_PTTT"]);
                                if (row["SoHang"].ToString() != "")
                                    tkmd.SoHang = Convert.ToInt16(row["SoHang"].ToString());
                                if (row["So_PLTK"].ToString() != "")
                                    tkmd.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                                tkmd.TenChuHang = Convert.ToString(row["TenCH"]);
                                if (row["So_Container"].ToString() != "")
                                    tkmd.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                                if (row["So_Container40"].ToString() != "")
                                    tkmd.SoContainer40 = Convert.ToDecimal(row["So_Container40"].ToString());
                                if (row["So_Kien"].ToString() != "")
                                    tkmd.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                                if (row["Tr_Luong"].ToString() != "")
                                    tkmd.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                                if (row["TongTGKB"].ToString() != "")
                                    tkmd.TongTriGiaKhaiBao = Convert.ToDecimal(row["TongTGKB"].ToString());
                                if (row["TongTGTT"].ToString() != "")
                                    tkmd.TongTriGiaTinhThue = Convert.ToDecimal(row["TongTGTT"]);
                                //tkmd.LoaiToKhaiGiaCong = Convert.ToString(row[0]["LoaiTKGC"]);
                                tkmd.MaDoanhNghiep = Convert.ToString(row["Ma_DV"]);
                                if (row["LePhi_HQ"].ToString() != "")
                                    tkmd.LePhiHaiQuan = Convert.ToDecimal(row["LePhi_HQ"].ToString());
                                if (row["Phi_BH"].ToString() != "")
                                    tkmd.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                                if (row["Phi_VC"].ToString() != "")
                                    tkmd.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                                tkmd.ThanhLy = row["THANH_LY"].ToString();
                                tkmd.Xuat_NPL_SP = row["XUAT_NPL_SP"].ToString();
                                tkmd.TrangThaiThanhKhoan = row["TTTK"].ToString();
                                tkmd.ChungTu = FontConverter.TCVN2Unicode(row["GIAYTO"].ToString());
                                if (row["NGAY_HOANTHANH"].ToString() != "")
                                    tkmd.NgayHoanThanh = Convert.ToDateTime(row["NGAY_HOANTHANH"].ToString());
                                tkmd.InsertUpdateTransaction(transaction);
                            }
                        // xoa hang cu                        
                        //cap nnhat hang mau dich    
                        short stt = 1;
                        string mahq = "";
                        string maloaihinh = "";
                        short namdk = 0;
                        int sotk = 0;
                        if (dsHang != null)
                            foreach (DataRow row in dsHang.Tables[0].Rows)
                            {
                                row2 = row;

                                HangMauDich hmd = new HangMauDich();
                                hmd.MaHaiQuan = row["MA_HQ"].ToString();
                                hmd.MaLoaiHinh = row["MA_LH"].ToString();
                                hmd.NamDangKy = (short)Convert.ToInt32(row["NAMDK"].ToString());
                                hmd.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());

                                if (mahq.Trim().ToUpper() != hmd.MaHaiQuan.Trim().ToUpper() || maloaihinh.Trim().ToUpper() != hmd.MaLoaiHinh.Trim().ToUpper() || sotk != hmd.SoToKhai || namdk != hmd.NamDangKy)
                                {
                                    hmd.DeleteByForeigKeyTransaction(transaction);
                                    stt = 1;
                                    mahq = hmd.MaHaiQuan;
                                    maloaihinh = hmd.MaLoaiHinh;
                                    sotk = hmd.SoToKhai;
                                    namdk = hmd.NamDangKy;
                                }
                                hmd.MaPhu = row["MA_NPL_SP"].ToString();
                                hmd.TenHang = FontConverter.TCVN2Unicode(row["Ten_Hang"].ToString());
                                hmd.MaHS = row["MA_HANGKB"].ToString();
                                //if (hmd.MaHS.Length < 10)
                                //{
                                //    for (int i = 1; i <= 10 - hmd.MaHS.Length; ++i)
                                //        hmd.MaHS += "0";
                                //}
                                hmd.DVT_ID = row["Ma_DVT"].ToString();
                                hmd.NuocXX_ID = row["Nuoc_XX"].ToString();
                                hmd.SoLuong = Convert.ToDecimal(row["Luong"]);
                                try
                                {
                                    hmd.DonGiaKB = Convert.ToDecimal(row["DGia_KB"]);
                                }
                                catch (Exception exx) { }
                                try
                                {
                                    hmd.DonGiaTT = Convert.ToDecimal(row["DGia_TT"]);
                                }
                                catch (Exception exx) { }
                                hmd.TriGiaKB = Convert.ToDecimal(row["TriGia_KB"]);
                                hmd.TriGiaTT = Convert.ToDecimal(row["TriGia_TT"]);
                                hmd.TriGiaKB_VND = Convert.ToDecimal(row["TGKB_VND"]);
                                if (row["TS_XNK"].ToString() != "")
                                    hmd.ThueSuatXNK = Convert.ToDecimal(row["TS_XNK"]);
                                if (row["TS_TTDB"].ToString() != "")
                                    hmd.ThueSuatTTDB = Convert.ToDecimal(row["TS_TTDB"]);
                                if (row["TS_VAT"].ToString() != "")
                                    hmd.ThueSuatGTGT = Convert.ToDecimal(row["TS_VAT"]);
                                if (row["Thue_XNK"].ToString() != "")
                                    hmd.ThueXNK = Convert.ToDecimal(row["Thue_XNK"]);
                                if (row["Thue_TTDB"].ToString() != "")
                                    hmd.ThueTTDB = Convert.ToDecimal(row["Thue_TTDB"]);
                                if (row["Thue_VAT"].ToString() != "")
                                    hmd.ThueGTGT = Convert.ToDecimal(row["Thue_VAT"]);
                                if (row["Phu_Thu"].ToString() != "")
                                    hmd.PhuThu = Convert.ToDecimal(row["Phu_Thu"]);
                                if (row["MienThue"].ToString() != "")
                                    hmd.MienThue = Convert.ToByte(row["MienThue"]);
                                if (row["TyLe_ThuKhac"].ToString() != "")
                                    hmd.TyLeThuKhac = Convert.ToDecimal(row["TyLe_ThuKhac"]);
                                if (row["TriGia_ThuKhac"].ToString() != "")
                                    hmd.TriGiaThuKhac = Convert.ToDecimal(row["TriGia_ThuKhac"]);
                                hmd.SoThuTuHang = Convert.ToInt16(row["STTHANG"]);
                                hmd.InsertTransaction(transaction);
                            }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return 1;//;ds.Tables[0].Rows.Count;
        }
        #endregion

    }

}

