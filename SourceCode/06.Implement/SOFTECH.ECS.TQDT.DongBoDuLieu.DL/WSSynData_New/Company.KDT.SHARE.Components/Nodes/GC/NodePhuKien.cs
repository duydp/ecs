﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.GC
{
    public class NodePhuKien
    {
        /// <summary>
        /// Nguyên liệu bổ sung || Nguyên liệu Sửa đổi
        /// </summary>
        public const string Material = "Material";
        /// <summary>
        /// Nguyên Phụ liệu || Sản phẩm || Thiết bị
        /// </summary>
        public const string Commodity = "Commodity";
        /// <summary>
        /// Tên/Mô tả nguyên phụ liệu bổ sung || Tên/Mô tả Sản phẩm bổ sung || Tên/Mô tả Thiết bị bổ sung || Tên/Mô tả nguyên phụ liệu sửa đổi || Mô tả khác về phụ kiện (văn bản, ghi chú …)
        /// </summary>
        public const string description = "description";
        /// <summary>
        /// Mã nguyên phụ liệu bổ sung || Mã Sản phẩm bổ sung ||  Mã Thiết bị bổ sung || Mã nguyên phụ liệu sửa đổi || Mã sản phẩm xin hủy
        /// </summary>
        public const string identification = "identification";
        /// <summary>
        /// Mã HS
        /// </summary>
        public const string tariffClassification = "tariffClassification";
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";
        /// <summary>
        /// Sản phẩm bổ sung 
        /// </summary>
        public const string Product = "Product";        
        /// <summary>
        /// Mã nhóm sản phẩm
        /// </summary>
        public const string productGroup = "productGroup";
        /// <summary>
        /// Thiết bị bổ sung 
        /// </summary>
        public const string Equipment = "Equipment";
        
        /// <summary>
        /// Số lượng
        /// </summary>
        public const string quantity = "quantity";
        /// <summary>
        /// Nước xuất xứ
        /// </summary>
        public const string Origin = "Origin";
        /// <summary>
        /// Nước xuất xứ
        /// </summary>
        public const string originCountry = "originCountry";
        /// <summary>
        /// Nguyên tệ
        /// </summary>
        public const string CurrencyExchange = "CurrencyExchange";
        /// <summary>
        /// Nguyên tệ
        /// </summary>
        public const string currencyType = "currencyType";
        /// <summary>
        /// Trị giá
        /// </summary>
        public const string CustomsValue = "CustomsValue";
        /// <summary>
        /// Trạng thái (mới = 0; cũ =1) || Loại (xem AgentStauts)
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Trị giá
        /// </summary>
        public const string unitPrice = "unitPrice";
        /// <summary>
        /// Ngày hợp đồng cũ
        /// </summary>
        public const string oldExpire = "oldExpire";
        /// <summary>
        /// Ngày gia hạn mới
        /// </summary>
        public const string newExpire = "newExpire";
        /// <summary>
        /// Lý do hủy hợp đồng || Nội dung phụ kiện
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Mã thiết bị muốn sửa đổi || Mã nguyên phụ liệu muốn sửa đổi || Mã sản phẩm muốn sửa đổi
        /// </summary>
        public const string preIdentification = "preIdentification";  
        /// <summary>
        /// Ngày khai báo || Ngày hợp đồng || Ngày phụ kiện
        /// </summary>
        public const string issue = "issue";  
        /// <summary>
        /// Chức năng (= 8)
        /// </summary>
        public const string function = "function";  
        /// <summary>
        /// Nơi khai báo
        /// </summary>
        public const string issueLocation = "issueLocation";  
        /// <summary>
        /// Số tiếp nhận chứng từ
        /// </summary>
        public const string customsReference = "customsReference"; 
        /// <summary>
        /// Ngày tiếp nhận chứng từ
        /// </summary>
        public const string acceptance = "acceptance";  
        /// <summary>
        /// Đơn vị HQ khai báo || Mã đơn vị Hải quan đăng ký
        /// </summary>
        public const string declarationOffice = "declarationOffice"; 
        /// <summary>
        /// Đơn vị khai báo
        /// </summary>
        public const string Agent = "Agent"; 
        /// <summary>
        /// Tên đơn vi khai báo || Tên đơn vị XNK || Người nhận gia công || Người thuê gia công
        /// </summary>
        public const string name = "name"; 
        /// <summary>
        /// Mã đơn vị khai báo || Mã đơn vị XNK || Mã người nhận gia công || Mã người thuê gia công
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Đơn vị XNK
        /// </summary>
		public const string Importer = "Importer";		
        /// <summary>
        /// Hợp đồng
        /// </summary>
		public const string ContractReference = "ContractReference";	
	    /// <summary>
	    /// Số hợp đồng || Số tham chiếu tờ khai
	    /// </summary>
        public const string reference = "reference";	
        /// <summary>
        /// Phụ kiện hợp đồng || Số phụ kiện
        /// </summary>
        public const string SubContract = "SubContract";	
        /// <summary>
        /// Mã loại phụ kiện (sửa đổi, bổ sung …)
        /// </summary>
        public const string statement = "statement";	
        /// <summary>
        /// Nội dung của loại phụ kiện (sửa đổi, bổ sung …) || Nội dung ghi chú khác
        /// </summary>
        public const string Content = "Content";	
        /// <summary>
        /// Loại chứng từ (= 602) 
        /// </summary>
        public const string issuer = "issuer";	
        /// <summary>
        /// Phương thức thanh toán
        /// </summary>
        public const string Payment = "Payment";	
        /// <summary>
        /// Phương thức thanh toán
        /// </summary>
        public const string method = "method";
        /// <summary>
        /// Địa chỉ người nhận gia công || Địa chỉ người thuê gia công
        /// </summary>
	    public const string address = "address";
        /// <summary>
        /// Xuất khẩu
        /// </summary>
        public const string Exporter = "Exporter";
        /// <summary>
        /// Tổng trị giá thanh toán
        /// </summary>
        public const string totalPaymentValue = "totalPaymentValue";
        /// <summary>
        /// Tổng trị giá tiền công
        /// </summary>
        public const string totalProductValue = "totalProductValue";
        /// <summary>
        /// Nước nhận gia công
        /// </summary>
        public const string importationCountry = "importationCountry";
        /// <summary>
        /// Nước thuê gia công
        /// </summary>
        public const string exportationCountry = "exportationCountry";
    }
}
