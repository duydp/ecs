﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.KDT.SHARE.VNACCS
{
    public class MIMEHeader
    {
        public string Content_Type { get; set; }
        public string micalg { get; set; }
        public string boundary { get; set; }
        public string protocol { get; set; }
        public string Host { get; set; }
        public string Cache_Control { get; set; }
        public string Pragma { get; set; }
        public string Content_Length { get; set; }
        public string Expect { get; set; }
        public string Connection { get; set; }

        public MIMEHeader(string host, string Length, string GuidStr)
        {
            Content_Type = "multipart/signed";
            micalg = "sha1";
            boundary = GuidStr;
            protocol = "application/x-pkcs7-signature";
            Host = host;
            Content_Length = Length;
            Cache_Control = "no-store,no-cache";
            Pragma = "no-cache";
            Expect = "100-continue";
            Connection = "Close";
        }
        public StringBuilder ToEDI (StringBuilder edi)
        {
            StringBuilder header = edi;
            string cont_type = string.Format(@"Content-Type: {0}; micalg=""{1}""; boundary= ""{2}""; protocol= ""{3}""", Content_Type, micalg, boundary, protocol);
            header.Append(cont_type).AppendLine();
            header.Append("Host: " + Host).AppendLine();
            header.Append("Cache-Control: " + Cache_Control).AppendLine();
            header.Append("Pragma: " + Pragma).AppendLine();
            header.Append("Content-Length: " + Content_Length).AppendLine();
            header.Append("Expect: " + Expect).AppendLine();
            header.Append("Connection: " + Connection).AppendLine();
            return header;

        }

    }
}
