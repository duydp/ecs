﻿using System;
using System.Drawing;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ManageCOForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------

        public bool isBrower = false;
        public long TKMD_ID;
        public CO Co;
        public ManageCOForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["LoaiCO"].Text = this.LoaiCO_GetName(e.Row.Cells["LoaiCO"].Value.ToString());
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindData()
        {
            if (!isBrower)
            {
                dgList.DataSource = CO.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
            }
            else
            {
                dgList.DataSource = CO.SelectListCOByMaDanhNghiepAndKhacTKMD(TKMD_ID, GlobalSettings.MA_DON_VI);
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            BindData();
        }

  
      

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<CO> listCO = new List<CO>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        CO co = (CO)i.GetRow().DataRow;
                        co.Delete();
                    }
                }
            }
         
            BindData();
        }

      
      

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                Co = (CO)e.Row.DataRow;
                this.Close();
            }
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            BindData();
        }

     


    }
}
