﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.Report.SXXK
{
    public partial class PhanBoTKXReport : DevExpress.XtraReports.UI.XtraReport
    {
        public int SoToKhai;
        public short NamDangKy;
        public string MaHaiQuan;
        public string MaLoaiHinh;
        public PhanBoTKXReport()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            DataTable dt = new Company.BLL.SXXK.ThanhKhoan.BCXuatNhapTon().GetPhanBoToKhaiXuat(this.SoToKhai, this.MaLoaiHinh, this.NamDangKy, this.MaHaiQuan.Trim()).Tables[0];
            string MaSP = "";
            string MaNPL = "";
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["MaSP"].ToString() == MaSP && dr["MaNPL"].ToString() == MaNPL)
                {
                    dr["MaNPL"] = "";
                    dr["DinhMuc"] = DBNull.Value;
                    dr["NhuCau"] = DBNull.Value;
                }
                else
                {
                    MaSP = dr["MaSP"].ToString();
                    MaNPL = dr["MaNPL"].ToString(); ;
                }
            }
            dt.TableName = "PhanBoToKhaiXuat";
            this.DataSource = dt;
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            
            lblToKhaiXuat.Text = "CHO TỜ KHAI XUẤT " + this.SoToKhai + "/" + this.MaHaiQuan.Trim().Trim() +"/" + this.NamDangKy;
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoThuTuHang");
            lblMaSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            lblTenSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenHang");
            lblLuongSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSP", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongNPLCan.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NhuCau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPL", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ToKhaiNhap");

        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("SoToKhaiXuat").ToString() == "") e.Cancel = true;
        }

    }
}
