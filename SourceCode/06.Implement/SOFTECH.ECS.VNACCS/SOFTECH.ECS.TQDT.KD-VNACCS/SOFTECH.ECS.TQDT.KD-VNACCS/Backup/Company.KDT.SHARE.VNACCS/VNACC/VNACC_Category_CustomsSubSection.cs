using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_CustomsSubSection
    {
        protected static List<VNACC_Category_CustomsSubSection> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_CustomsSubSection> collection = new List<VNACC_Category_CustomsSubSection>();
            while (reader.Read())
            {
                VNACC_Category_CustomsSubSection entity = new VNACC_Category_CustomsSubSection();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomsSubSectionCode"))) entity.CustomsSubSectionCode = reader.GetString(reader.GetOrdinal("CustomsSubSectionCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomsCode"))) entity.CustomsCode = reader.GetString(reader.GetOrdinal("CustomsCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("ImportExportClassification"))) entity.ImportExportClassification = reader.GetString(reader.GetOrdinal("ImportExportClassification"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_CustomsSubSection> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [ID], [Name], CustomsSubSectionCode, CustomsCode, ImportExportClassification FROM [dbo].[t_VNACC_Category_CustomsSubSection]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_CustomsSubSection> SelectCollectionBy(List<VNACC_Category_CustomsSubSection> collections, string tableID, string importType)
        {
            return collections.FindAll(delegate(VNACC_Category_CustomsSubSection o)
            {
                return o.TableID.Contains(tableID) && o.ImportExportClassification == importType;
            });
        }

        public static List<VNACC_Category_CustomsSubSection> SelectCollectionByCustomsCode(List<VNACC_Category_CustomsSubSection> collections, string tableID, string importType, string customsCode)
        {
            return collections.FindAll(delegate(VNACC_Category_CustomsSubSection o)
            {
                return o.TableID.Contains(tableID) && o.ImportExportClassification == importType && o.CustomsCode == customsCode;
            });
        }

        //public static VNACC_Category_CustomsSubSection SelectByCustomsCode(VNACC_Category_CustomsSubSection collections, string tableID, string customsCode)
        //{
        //    return collections.FindAll(delegate(VNACC_Category_CustomsSubSection o)
        //    {
        //        return o.TableID.Contains(tableID) && o.ImportExportClassification == importType && o.CustomsCode == customsCode;
        //    });
        //}
    }
}