using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace Company.GC.BLL.DuLieuChuan
{
    public class LoaiPhuKien : BaseClass
    {
        public static DataTable SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_LoaiPhuKien order by ID_LoaiPhuKien asc";            
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static string GetName(string id)
        {
            string query = string.Format("SELECT Ten FROM t_HaiQuan_LoaiPhuKien WHERE [ID_LoaiPhuKien] = '{0}'", id);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                return reader["TenLoaiPhuKien"].ToString();
            }
            return string.Empty;
        }
    }
}
