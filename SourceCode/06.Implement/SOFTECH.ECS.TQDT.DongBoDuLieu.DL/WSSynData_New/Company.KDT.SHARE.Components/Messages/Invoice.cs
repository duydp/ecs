﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
  public  class Invoice : IssueBase
    {
      /// <summary>
      /// Mã loại hóa đơn thương mại
      /// </summary>
      [XmlElement("type")]
      public string Type { get; set; }

      /// <summary>
      /// Mã loại hình
      /// </summary>
      [XmlElement("natureOfTransaction")]
      public string NatureOfTransaction { get; set; }
    }
}
