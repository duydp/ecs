﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DongBoDuLieu.BLL.SXXK
{
    public class HMD
    {
        public static DataSet GetALLDanhSachHMD(string MaHaiQuan, string MaDN, int NamDangKy, string matKhau, bool isPhongKhai, long soTK)
        {

            string query =
                           "SELECT H.* " +
                           " from DTOKHAIMD T  inner join DHANGMDDK H" +
                           " on H.SOTK=T.SOTK and H.MA_LH=T.MA_LH and H.MA_HQ=T.MA_HQ " +
                           " and H.NAMDK=T.NAMDK where T.NAMDK >=" + NamDangKy + " and T.MA_HQ = '" + MaHaiQuan + "' AND T.MA_DV = '" + MaDN + "'";

            DataSet ds = null;
            //for (int i = 0; i < 10; i++)
            //{
            if (!isPhongKhai)
                ds = WebServiceDB.getDataBySQL(query, MaHaiQuan, "SXXK", MaDN, matKhau);
            else
                ds = WebServiceDB.getDataBySQLDatabase(query, MaHaiQuan, "SXXK");

            //    if (ds.Tables[0].Rows.Count != 0)
            //        break;
            //}
            return ds;
        }

        //HUNGTQ Update 25/10/2010
        public static DataSet GetALLDanhSachHMD(string MaHaiQuan, string MaDN, DateTime tuNgay, DateTime denNgay, string matKhau, bool isPhongKhai, long soTK)
        {

            string query =
                           "SELECT H.* " +
                           " from DTOKHAIMD T  inner join DHANGMDDK H" +
                           " on H.SOTK=T.SOTK and H.MA_LH=T.MA_LH and H.MA_HQ=T.MA_HQ " +
                           " and H.NAMDK=T.NAMDK where (T.NGAY_DK >= '" + tuNgay + "' AND T.NGAY_DK <= '" + denNgay + "') and T.MA_HQ = '" + MaHaiQuan + "' AND T.MA_DV = '" + MaDN + "'";

            DataSet ds = null;
            //for (int i = 0; i < 10; i++)
            //{
            if (!isPhongKhai)
                ds = WebServiceDB.getDataBySQL(query, MaHaiQuan, "SXXK", MaDN, matKhau);
            else
                ds = WebServiceDB.getDataBySQLDatabase(query, MaHaiQuan, "SXXK");

            //    if (ds.Tables[0].Rows.Count != 0)
            //        break;
            //}
            return ds;
        }

        public static DataSet GetALLDanhSachHMDLocal(string MaHaiQuan, string MaDN, DateTime tuNgay, DateTime denNgay, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = " SELECT H.[SoToKhai] AS SOTK, H.[MaLoaiHinh] AS MA_LH, H.[MaHaiQuan] AS MA_HQ, H.[NamDangKy] AS NAMDK, H.[SoThuTuHang],  " +
                            " H.[MaHS] AS MA_HANGKB, H.[MaPhu] AS MA_NPL_SP, H.[TenHang] AS Ten_Hang, H.[NuocXX_ID] AS Nuoc_XX,  " +
                            " H.[DVT_ID] AS Ma_DVT, H.[SoLuong] AS Luong, H.[DonGiaKB] AS DGia_KB, H.[DonGiaTT] AS DGia_TT, H.[TriGiaKB] AS TriGia_KB,  " +
                            " H.[TriGiaTT] AS TriGia_TT, H.[TriGiaKB_VND] AS TGKB_VND, H.[ThueSuatXNK] AS TS_XNK, H.[ThueSuatTTDB] AS TS_TTDB,  " +
                            " H.[ThueSuatGTGT] AS TS_VAT, H.[ThueXNK] AS Thue_XNK, H.[ThueTTDB] AS Thue_TTDB, H.[ThueGTGT] AS Thue_VAT,  " +
                            " H.[PhuThu] AS Phu_Thu, H.[TyLeThuKhac] AS TyLe_ThuKhac, H.[TriGiaThuKhac] AS TriGia_ThuKhac, H.[MienThue] AS MienThue, H.SoThuTuHang AS STTHANG  " +
                           " from dbo.t_SXXK_ToKhaiMauDich T  inner join dbo.t_SXXK_HangMauDich H" +
                           " on H.SoToKhai=T.SoToKhai and H.MaLoaiHinh=T.MaLoaiHinh and H.MaHaiQuan=T.MaHaiQuan and H.NamDangKy=T.NamDangKy " +
                           " where (T.NgayDangKy >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND T.NgayDangKy <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "') and T.MaHaiQuan = '" + MaHaiQuan + "' AND T.MaDoanhNghiep = '" + MaDN + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.SoToKhai , " +
                        " temp.NamDangKy , " +
                        " temp.MaLoaiHinh , " +
                        " temp.MaHaiQuan , " +
                        " temp.MaDoanhNghiep " +
                        "  FROM   [" + databaseNameTarget + "].dbo.t_SXXK_ToKhaiMauDich temp " +
                        "  WHERE  temp.SoToKhai = T.SoToKhai " +
                        " AND YEAR(temp.NgayDangKy) = YEAR(T.NgayDangKy) " +
                        " AND temp.MaLoaiHinh = T.MaLoaiHinh " +
                        " AND temp.MaHaiQuan = T.MaHaiQuan " +
                        " AND temp.MaDoanhNghiep = T.MaDoanhNghiep ) ";
            query += " ORDER BY  T.NgayDangKy";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);

            return ds;
        }

        public static DataSet GetALLDanhSachHMDLocalTQDT(string MaHaiQuan, string MaDN, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = "SELECT  t.[SoToKhai] AS SOTK  , " +
                            "t.[MaLoaiHinh] AS MA_LH  , " +
                            "t.[MaHaiQuan] AS MA_HQ  , " +
                            "YEAR(t.[NgayDangKy]) AS NAMDK  , " +
                            "H.[SoThuTuHang]  , " +
                            "H.[MaHS] AS MA_HANGKB  , " +
                            "H.[MaPhu] AS MA_NPL_SP  , " +
                            "H.[TenHang] AS Ten_Hang  , " +
                            "H.[NuocXX_ID] AS Nuoc_XX  , " +
                            "H.[DVT_ID] AS Ma_DVT  , " +
                            "H.[SoLuong] AS Luong  , " +
                            "H.[DonGiaKB] AS DGia_KB  , " +
                            "H.[DonGiaTT] AS DGia_TT  , " +
                            "H.[TriGiaKB] AS TriGia_KB  , " +
                            "H.[TriGiaTT] AS TriGia_TT  , " +
                            "H.[TriGiaKB_VND] AS TGKB_VND  , " +
                            "H.[ThueSuatXNK] AS TS_XNK  , " +
                            "H.[ThueSuatTTDB] AS TS_TTDB  , " +
                            "H.[ThueSuatGTGT] AS TS_VAT  , " +
                            "H.[ThueXNK] AS Thue_XNK  , " +
                            "H.[ThueTTDB] AS Thue_TTDB  , " +
                            "H.[ThueGTGT] AS Thue_VAT  , " +
                            "H.[PhuThu] AS Phu_Thu  , " +
                            "H.[TyLeThuKhac] AS TyLe_ThuKhac  , " +
                            "H.[TriGiaThuKhac] AS TriGia_ThuKhac  , " +
                            "H.[MienThue] AS MienThue  , " +
                            "H.SoThuTuHang AS STTHANG  " +
                            "FROM    dbo.t_KDT_ToKhaiMauDich T " +
                            "INNER JOIN dbo.t_KDT_HangMauDich H ON T.ID = H.TKMD_ID " +
                           " where T.MaHaiQuan = '" + MaHaiQuan + "' AND T.MaDoanhNghiep = '" + MaDN + "'";
            if (trangThaiDaDuyet)
                query += " AND T.TrangThaiXuLy = 1";
            if (tuNgay > DateTime.MinValue)
                query += " and (T.NgayDangKy >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND T.NgayDangKy <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "')";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.SoToKhai , " +
                        " temp.NamDK , " +
                        " temp.MaLoaiHinh , " +
                        " temp.MaHaiQuan , " +
                        " temp.MaDoanhNghiep " +
                        "  FROM   [" + databaseNameTarget + "].dbo.t_KDT_ToKhaiMauDich temp " +
                        "  WHERE  temp.SoToKhai = T.SoToKhai " +
                        " AND YEAR(temp.NgayDangKy) = YEAR(T.NgayDangKy) " +
                        " AND temp.MaLoaiHinh = T.MaLoaiHinh " +
                        " AND temp.MaHaiQuan = T.MaHaiQuan " +
                        " AND temp.MaDoanhNghiep = T.MaDoanhNghiep ) ";
            query += " ORDER BY  T.NgayDangKy";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);

            return ds;
        }

        public DataSet GetALLDanhSachHMDByNgay(string MaHaiQuan, string MaDN, DateTime ngayBatDau, DateTime ngayKetThuc, string matKhau, bool isPhongKhai)
        {

            string query =
                           "SELECT H.* " +
                           " from DTOKHAIMD T  inner join DHANGMDDK H" +
                           " on H.SOTK=T.SOTK and H.MA_LH=T.MA_LH and H.MA_HQ=T.MA_HQ " +
                           " and H.NAMDK=T.NAMDK where T.MA_HQ = '" + MaHaiQuan + "' AND T.MA_DV = '" + MaDN + "'";
            query += " and T.NgayDK >=" + ngayBatDau.ToString("MM/dd/yyyy") + " and NgayDK <=" + ngayKetThuc.ToString("MM/dd/yyyy");
            query += " ORDER BY NGAY_DK";
            DataSet ds = null;
            if (!isPhongKhai)
                ds = WebServiceDB.getDataBySQL(query, MaHaiQuan, "SXXK", MaDN, matKhau);
            else
                ds = WebServiceDB.getDataBySQLDatabase(query, MaHaiQuan, "SXXK");
            return ds;
        }
    }
}
