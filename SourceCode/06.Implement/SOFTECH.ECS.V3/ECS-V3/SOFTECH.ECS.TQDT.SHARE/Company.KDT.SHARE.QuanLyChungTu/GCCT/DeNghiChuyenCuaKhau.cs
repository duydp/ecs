﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
    /// <summary>
    /// XML root cho việc Serializer ra XML đồng bộ dữ liệu
    /// </summary>
    [System.Xml.Serialization.XmlRoot(Namespace = "Company.KDT.SHARE.QuanLyChungTu.GCCT")]
    public partial class DeNghiChuyenCuaKhau
    {
    }
}