﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LicenseCode
{
    public partial class InfoOnline : Form
    {

        public InfoOnline()
        {
            InitializeComponent();
        }
        public void SetText(string text)
        {
            rtfInfo.Text = text;
        }
        public void SetRTF(string sfmtRTF)
        {
            rtfInfo.Rtf = sfmtRTF;
        }
    }
}
