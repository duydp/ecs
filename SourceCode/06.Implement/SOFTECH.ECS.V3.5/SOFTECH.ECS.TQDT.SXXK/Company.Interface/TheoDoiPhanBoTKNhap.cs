﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ThanhKhoan;
using Company.BLL.SXXK.ToKhai;
using Company.BLL;
using Janus.Windows.GridEX;
using Company.QuanTri;
namespace Company.Interface
{
    public partial class TheoDoiPhanBoTKNhap : BaseForm
    {
        public TheoDoiPhanBoTKNhap()
        {
            InitializeComponent();
        }
        private Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection tkmdCollection = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();
        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTon clsnhapton = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();       
       
        private void HangTon_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
            dgList.Tables[0].Columns["LuongTonDau"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["LuongPhanBo"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["LuongCungUng"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["LuongTonCuoi"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["SoLuongXuat"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;            
        }
      
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
       
        }
             

        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "MaDoanhNghiep = '" + MaDoanhNghiep +"'";
            where += string.Format(" AND MaHaiQuanNhap = '{0}'", GlobalSettings.MA_HAI_QUAN);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoToKhaiNhap = " + txtSoTiepNhan.Value;
            }

            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND NamDangKyNhap = " + txtNamTiepNhan.Value;
            }
            if (txtMaNPL.Text.Length > 0)
            {
                where += " AND MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
            }
            
           
            // Thực hiện tìm kiếm.            
            dgList.DataSource = Company.BLL.SXXK.PhanBoToKhaiNhap.SelectViewPhanBoToKhaiNhap(where, "").Tables[0];
            dgList.Refetch();
        }
            
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void Thongke()
        {
            //Company.BLL.SXXK.ThanhKhoan.NPLNhapTon clsNhapton = new NPLNhapTon();
            //NPLNhapTonCollection NhaptonCollection = new NPLNhapTonCollection();
            //foreach (NPLNhapTon nplnhapton in NhaptonCollection)
            //{
                
            //}
        }


        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                npl.Ma = e.Row.Cells["MaNPL"].Text;
                npl.Load();
                e.Row.Cells["TenNPL"].Text = npl.Ten;
            }
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            clsnhapton.InsertUpdate(tkmdCollection);
            this.search();
            //ShowMessage("Cập nhật thông tin thành công", false);
            MLMessages("Cập nhật thông tin thành công", "MSG_THK64", "", false);
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            try
            {
                new Company.BLL.KDT.SXXK.HoSoThanhLyDangKy().FixOldData(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
              //  ShowMessage("Điều chỉnh thành công.", false);
                MLMessages("Điều chỉnh thành công.", "MSG_THK99", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
        }

        private void cbTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.search();
        }

      
    }
}