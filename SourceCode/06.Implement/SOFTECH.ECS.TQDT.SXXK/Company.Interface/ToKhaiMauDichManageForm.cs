﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.BLL.KDT.SXXK;
using System.Xml.Serialization;
using System.Data;
using System.Xml;
using System.Collections.Generic;
using System.Globalization;
using Company.KD.BLL.KDT;
using Infragistics.Excel;

namespace Company.Interface
{
    public partial class ToKhaiMauDichManageForm : BaseForm
    {
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        private ToKhaiMauDichCollection tmpCollection = new ToKhaiMauDichCollection();
        private string xmlCurrent = "";
        public string nhomLoaiHinh = "";
        public ToKhaiMauDichManageForm()
        {
            InitializeComponent();

            btnTransfer.Visible = false;

            mniImport.Visible = mniExport.Visible = mniCapNhatToKhaiSua.Visible = GlobalSettings.Import;

            SetTransferEvent();

            SetComboTrangthai(cbStatus);
        }

        #region IMPORT/ EXPORT TO KHAI TU XML

        private void mniImportTK_Click(object sender, EventArgs e)
        {
            ImportToKhai();
        }

        private void mniImportMSG_Click(object sender, EventArgs e)
        {
            ImportMSG();
        }

        private void mniExportTK_Click(object sender, EventArgs e)
        {
            ExportToKhai();
        }

        private void ExportToKhai()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();

            try
            {
                string xmlFileName = "";
                //SaveFileDialog saveFileDialog1 = new OpenFileDialog();
                string filePath = Application.StartupPath;
                saveFileDialog1.InitialDirectory = filePath;
                saveFileDialog1.Filter = "XML (*.xml)|*.xml|All file (*)|*.*";
                saveFileDialog1.DefaultExt = ".xml";
                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    xmlFileName = saveFileDialog1.FileName;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Export error:\r\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private ToKhaiMauDich ImportToKhai()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();

            try
            {
                string xmlFileName = "";
                OpenFileDialog opendlg = new OpenFileDialog();
                string filePath = Application.StartupPath;
                opendlg.InitialDirectory = filePath;
                opendlg.Filter = "XML (*.xml)|*.xml|All file (*)|*.*";
                opendlg.DefaultExt = ".xml";
                if (opendlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    xmlFileName = opendlg.FileName;


                    if (!System.IO.File.Exists(xmlFileName)) return tkmd;

                    //Load du lieu tu file XML
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlFileName);

                    //Lay thong tin chung to khai
                    tkmd = GetThongTinChung(doc);

                    //Lay thong tin hang mau dich
                    GetHangMauDichXML(tkmd, doc);

                    //Lay thong tin van don
                    GetVanDonXML(tkmd, doc);

                    //Lay thong tin giay phep
                    GetGiayPhepXML(tkmd, doc);

                    //Lay thong hoa don thuong mai
                    GetHoaDonThuongMaiXML(tkmd, doc);

                    //Lay thong tin hop dong thuong mai
                    GetHopDongThuongMaiXML(tkmd, doc);

                    //Lay thong tin giay phep
                    GetCOXML(tkmd, doc);

                    //Lay thong tin de nghi chuyen cua khau
                    GetDeNghiChuyenCuaKhauXML(tkmd, doc);

                    //Lay thong tin chung tu dinh kem
                    GetChungTuKemXML(tkmd, doc);

                    //SAVE TO KHAI
                    SaveToKhaiImport(tkmd);

                    ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                    f.TKMD = tkmd;
                    f.OpenType = OpenFormType.Edit;
                    f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                    f._bNew = false;
                    f.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Import error:\r\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return tkmd;
        }

        private bool SaveToKhaiImport(BLL.KDT.ToKhaiMauDich tkmd)
        {
            try
            {
                #region SAVE

                //SAVE To Khai
                tkmd.Insert();

                //Save Hang mau dich
                foreach (BLL.KDT.HangMauDich hmdItem in tkmd.HMDCollection)
                {
                    hmdItem.TKMD_ID = tkmd.ID;
                    hmdItem.Insert();
                }

                //Save van don
                if (tkmd.VanTaiDon != null)
                {
                    tkmd.VanTaiDon.TKMD_ID = tkmd.ID;
                    tkmd.VanTaiDon.Insert();

                    //Save Container
                    foreach (Company.KDT.SHARE.QuanLyChungTu.Container containerItem in tkmd.VanTaiDon.ContainerCollection)
                    {
                        containerItem.VanDon_ID = tkmd.VanTaiDon.ID;
                        containerItem.Insert();
                    }
                }

                //Save Giay phep
                foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhepItem in tkmd.GiayPhepCollection)
                {
                    giayPhepItem.TKMD_ID = tkmd.ID;
                    giayPhepItem.Insert();

                    //Save hang cua giay phep
                    foreach (BLL.KDT.HangMauDich hangGiayPhepItem in tkmd.HMDCollection)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail gpDetail = new Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail();
                        gpDetail.GiayPhep_ID = giayPhepItem.ID;
                        gpDetail.HMD_ID = hangGiayPhepItem.ID;
                        gpDetail.MaNguyenTe = tkmd.NguyenTe_ID;
                        gpDetail.Insert();
                    }
                }

                //Save Hoa don thuong mai
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonItem in tkmd.HoaDonThuongMaiCollection)
                {
                    hoaDonItem.TKMD_ID = tkmd.ID;
                    hoaDonItem.Insert();

                    //Save hang cua hoa don
                    foreach (BLL.KDT.HangMauDich hangHoaDonItem in tkmd.HMDCollection)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail hoadonDetail = new Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail();
                        hoadonDetail.HoaDonTM_ID = hoaDonItem.ID;
                        hoadonDetail.HMD_ID = hangHoaDonItem.ID;
                        hoadonDetail.Insert();
                    }
                }

                //Save Hop dong thuong mai
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopDongItem in tkmd.HopDongThuongMaiCollection)
                {
                    hopDongItem.TKMD_ID = tkmd.ID;
                    hopDongItem.Insert();

                    //Save hang cua hop dong
                    foreach (BLL.KDT.HangMauDich hangHopDongItem in tkmd.HMDCollection)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail hopDongDetail = new Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail();
                        hopDongDetail.HopDongTM_ID = hopDongItem.ID;
                        hopDongDetail.HMD_ID = hangHopDongItem.ID;
                        hopDongDetail.Insert();
                    }
                }

                //Save CO
                foreach (Company.KDT.SHARE.QuanLyChungTu.CO coItem in tkmd.COCollection)
                {
                    coItem.TKMD_ID = tkmd.ID;
                    coItem.Insert();
                }

                //Save de nghi chuyen cua khau
                foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau cckItem in tkmd.listChuyenCuaKhau)
                {
                    cckItem.TKMD_ID = tkmd.ID;
                    cckItem.Insert();
                }

                //Save chung tu kem
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem ctkItem in tkmd.ChungTuKemCollection)
                {
                    ctkItem.TKMDID = tkmd.ID;
                    ctkItem.Insert();

                    foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet ctkDetailItem in ctkItem.listCTChiTiet)
                    {
                        ctkDetailItem.ChungTuKemID = ctkItem.ID;
                        ctkDetailItem.Insert();
                    }
                }

                #endregion

                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return false; }
        }

        private ToKhaiMauDich GetThongTinChung(XmlDocument doc)
        {
            ToKhaiMauDich obj = new ToKhaiMauDich();

            try
            {
                NumberFormatInfo f = new NumberFormatInfo();
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";

                CultureInfo c = new CultureInfo("en-US");

                /*
                 * <reference>40573f90-6839-4eb1-bd06-f86b88c86e4d</reference>
                 * 
                 * <THONG_TIN>
                    <DON_VI_GUI MA_DV="0400101556" TEN_DV="Tổng Công Ty Cổ Phần Dệt May Hòa Thọ" />
                    <HQ_NHAN MA_HQ="C34C" TEN_HQ="HQ CK Cảng Đà Nẵng KV I" />
                    <TIEP_NHAN MA_HQTN="C34C" SO_TN="2051" NAM_TN="2010" />
                  </THONG_TIN>
                 */

                XmlNode nodeReference = doc.SelectSingleNode("Envelope/Header/Subject/reference");
                obj.GUIDSTR = nodeReference.InnerXml;

                //Lay du lieu node Root
                XmlNode nodeHQNhan = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/THONG_TIN/HQ_NHAN");
                obj.MaHaiQuan = nodeHQNhan.Attributes["MA_HQ"].Value;

                XmlNode nodeDN = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/THONG_TIN/DON_VI_GUI");
                obj.MaDoanhNghiep = nodeDN.Attributes["MA_DV"].Value;

                /* Khong lay tu day, Lay so tiep nhan tu msg -> de insert ket qua xu ly
                XmlNode nodeTN = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/THONG_TIN/TIEP_NHAN");
                if (nodeTN != null)
                {
                    obj.SoTiepNhan = Convert.ToInt64(nodeTN.Attributes["SO_TN"].Value);
                    obj.NamDK = Convert.ToInt32(nodeTN.Attributes["NAM_TN"].Value);
                    obj.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                }
                */

                //<TO_KHAI MA_LH="NSX01" MA_HQ="C34C" MA_DV_NK="0400101556" MA_DV_UT="" MA_DV_XK="" TEN_DV_XK="MARUBENI CORPORATION&#xA;&#xD;4-2 OHTE" SO_GP="" NGAY_GP="" NGAY_HHGP="" SO_HD="AU791100" NGAY_HD="2010-05-20" NGAY_HHHD="2010-12-31" SO_HDTM="7N-395/10" NGAY_HDTM="2010-05-20" MA_PTVT="002" TEN_PTVT="CAPE FULMAR S005" NGAY_DEN="2010-06-02" SO_VANDON="HEB110050221" NGAY_VANDON="2010-05-25" NUOC_XK="JP" CANG_XUAT="KOBE" MA_CK_NHAP="C021" MA_DKGH="CIF" MA_NGTE="JPY" TY_GIA_VND="202.930" MA_PTTT="TTR" SO_PLTK="0" CHUNGTU_KEM="" SO_CONT20="1" SO_CONT40="0" SO_KIEN="20" TRONG_LUONG="438.50" PHI_BH="0.00" PHI_VC="0.00" LE_PHI_HQ="0.00" DE_XUAT_KHAC="khong co de xuat khac" CHU_HANG="KHOA">

                XmlNode nodeTOKHAI = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TO_KHAI_NK/TO_KHAI");
                obj.MaLoaiHinh = nodeTOKHAI.Attributes["MA_LH"].Value;
                obj.MaHaiQuan = nodeTOKHAI.Attributes["MA_HQ"].Value;
                obj.MaDoanhNghiep = nodeTOKHAI.Attributes["MA_DV_NK"].Value;
                obj.MaDonViUT = nodeTOKHAI.Attributes["MA_DV_UT"].Value;
                //obj.MaDaiLyTTHQ = nodeTOKHAI.Attributes["MA_DV_KT"].Value;
                //nodeTOKHAI.Attributes["MA_DV_XK"].Value;                
                obj.TenDonViDoiTac = nodeTOKHAI.Attributes["TEN_DV_XK"].Value;

                obj.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                obj.SoGiayPhep = nodeTOKHAI.Attributes["SO_GP"].Value;

                if (nodeTOKHAI.Attributes["NGAY_GP"].Value != "")
                    obj.NgayGiayPhep = Convert.ToDateTime(nodeTOKHAI.Attributes["NGAY_GP"].Value, c);

                if (nodeTOKHAI.Attributes["NGAY_HHGP"].Value != "")
                    obj.NgayHetHanGiayPhep = Convert.ToDateTime(nodeTOKHAI.Attributes["NGAY_HHGP"].Value, c);

                obj.SoHopDong = nodeTOKHAI.Attributes["SO_HD"].Value;

                if (nodeTOKHAI.Attributes["NGAY_HD"].Value != "")
                    obj.NgayHopDong = Convert.ToDateTime(nodeTOKHAI.Attributes["NGAY_HD"].Value, c);

                if (nodeTOKHAI.Attributes["NGAY_HHHD"].Value != "")
                    obj.NgayHetHanHopDong = Convert.ToDateTime(nodeTOKHAI.Attributes["NGAY_HHHD"].Value, c);

                obj.SoHoaDonThuongMai = nodeTOKHAI.Attributes["SO_HDTM"].Value;

                if (nodeTOKHAI.Attributes["NGAY_HDTM"].Value != "")
                    obj.NgayHoaDonThuongMai = Convert.ToDateTime(nodeTOKHAI.Attributes["NGAY_HDTM"].Value, c);

                obj.PTVT_ID = nodeTOKHAI.Attributes["MA_PTVT"].Value;
                obj.SoHieuPTVT = nodeTOKHAI.Attributes["TEN_PTVT"].Value;

                if (nodeTOKHAI.Attributes["NGAY_DEN"].Value != "")
                    obj.NgayDenPTVT = Convert.ToDateTime(nodeTOKHAI.Attributes["NGAY_DEN"].Value, c);

                obj.SoVanDon = nodeTOKHAI.Attributes["SO_VANDON"].Value;

                if (nodeTOKHAI.Attributes["NGAY_VANDON"].Value != "")
                    obj.NgayVanDon = Convert.ToDateTime(nodeTOKHAI.Attributes["NGAY_VANDON"].Value, c);

                obj.LoaiHangHoa = "N";
                obj.NuocNK_ID = "VN";
                obj.NuocXK_ID = nodeTOKHAI.Attributes["NUOC_XK"].Value;
                obj.DiaDiemXepHang = nodeTOKHAI.Attributes["CANG_XUAT"].Value;
                obj.CuaKhau_ID = nodeTOKHAI.Attributes["MA_CK_NHAP"].Value;
                obj.DKGH_ID = nodeTOKHAI.Attributes["MA_DKGH"].Value;
                obj.NguyenTe_ID = nodeTOKHAI.Attributes["MA_NGTE"].Value;
                obj.TyGiaTinhThue = Convert.ToDecimal(nodeTOKHAI.Attributes["TY_GIA_VND"].Value, f);
                obj.TyGiaUSD = GlobalSettings.TY_GIA_USD;
                obj.PTTT_ID = nodeTOKHAI.Attributes["MA_PTTT"].Value;
                obj.GiayTo = nodeTOKHAI.Attributes["CHUNGTU_KEM"].Value;
                obj.SoContainer20 = Convert.ToUInt32(nodeTOKHAI.Attributes["SO_CONT20"].Value);
                obj.SoContainer40 = Convert.ToUInt32(nodeTOKHAI.Attributes["SO_CONT40"].Value);
                obj.SoLuongPLTK = Convert.ToInt16(nodeTOKHAI.Attributes["SO_PLTK"].Value);
                obj.SoKien = Convert.ToUInt32(nodeTOKHAI.Attributes["SO_KIEN"].Value);
                obj.TrongLuong = Convert.ToDecimal(nodeTOKHAI.Attributes["TRONG_LUONG"].Value, f);
                obj.PhiBaoHiem = Convert.ToDecimal(nodeTOKHAI.Attributes["PHI_BH"].Value, f);
                obj.PhiVanChuyen = Convert.ToDecimal(nodeTOKHAI.Attributes["PHI_VC"].Value, f);
                obj.LePhiHaiQuan = Convert.ToDecimal(nodeTOKHAI.Attributes["LE_PHI_HQ"].Value, f);
                obj.TenChuHang = nodeTOKHAI.Attributes["CHU_HANG"].Value;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return obj;
        }

        private Company.KDT.SHARE.QuanLyChungTu.VanDon GetVanDonXML(ToKhaiMauDich tkmd, XmlDocument doc)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";

            CultureInfo c = new CultureInfo("en-US");

            //Lay du lieu node Root
            XmlNode nodeDULIEU = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");

            //Lay thong tin van don
            XmlNode nodeVANDON = null;
            if (nodeDULIEU.SelectSingleNode("CHUNG_TU_VAN_DON") != null)
                nodeVANDON = nodeDULIEU.SelectSingleNode("CHUNG_TU_VAN_DON");

            //<CHUNG_TU_VAN_DON SO_CHUNG_TU="HEB110050221" NGAY_CHUNG_TU="2010-05-25" NGAY_HH="" TEN_NGUOI_NHAN_HANG="HOATHO" MA_NGUOI_NHAN_HANG="HOATHO" TEN_NGUOI_GIAO_HANG="MARUBENI" MA_NGUOI_GIAO_HANG="MARUBENI" CUA_KHAU_NHAP="C021" CUA_KHAU_XUAT="KOBE" MA_DKGH="CIF" TEN_NGUOI_NHAN_HANG_TG="" MA_NGUOI_NHAN_HANG_TG="" DIA_DIEM_GIAO_HANG="TIENSA" CONTAINER="1" SO_HIEU_PTVT="CAPE FULMAR S005" TEN_PTVT="CAPE FULMAR S005" SO_HIEU_CHUYEN_DI="CAPE FULMAR S005" QUOC_TICH_PTVT="JP" TEN_CANG_DO_HANG="TIENSA" MA_CANG_DO_HANG="TIENSA" NGAY_DEN_PTVT="2010-06-02" TEN_CANG_XEP_HANG="KOBE" MA_CANG_XEP_HANG="KOBE" TEN_HANG_VAN_TAI="WANHAI" MA_HANG_VAN_TAI="WANHAI" NUOC_XN="TW" NOI_PHAT_HANH="KOBE" NGAY_KHOI_HANH="2010-05-25" TONG_SO_KIEN="" KIEU_DONG_GOI="" MA_PTVT="">
            //  <CHUNG_TU_VAN_DON.CONT CONTAINER_NO="OLHU3710585" CONTAINER_TYPE="2" SEAL_NO="WH09471021" TRANG_THAI="1" />
            //</CHUNG_TU_VAN_DON>

            Company.KDT.SHARE.QuanLyChungTu.VanDon vd = new Company.KDT.SHARE.QuanLyChungTu.VanDon();

            try
            {
                vd.TKMD_ID = tkmd.ID;

                //Lay thong tin van don
                vd.SoVanDon = nodeVANDON.Attributes["SO_CHUNG_TU"].Value;
                if (nodeVANDON.Attributes["NGAY_CHUNG_TU"].Value != "")
                    vd.NgayVanDon = Convert.ToDateTime(nodeVANDON.Attributes["NGAY_CHUNG_TU"].Value, c);
                //if (nodeVANDON.Attributes["NGAY_HH"].Value != "")
                //    vd.NgayHetHan = Convert.ToDateTime(nodeVANDON.Attributes["NGAY_HH"].Value);

                vd.TenNguoiNhanHang = nodeVANDON.Attributes["TEN_NGUOI_NHAN_HANG"].Value;
                vd.MaNguoiNhanHang = nodeVANDON.Attributes["MA_NGUOI_NHAN_HANG"].Value;

                vd.TenNguoiNhanHangTrungGian = nodeVANDON.Attributes["TEN_NGUOI_NHAN_HANG_TG"].Value;
                vd.MaNguoiNhanHangTrungGian = nodeVANDON.Attributes["MA_NGUOI_NHAN_HANG"].Value;

                vd.TenNguoiGiaoHang = nodeVANDON.Attributes["TEN_NGUOI_GIAO_HANG"].Value;
                vd.MaNguoiGiaoHang = nodeVANDON.Attributes["MA_NGUOI_NHAN_HANG_TG"].Value;

                vd.CuaKhauNhap_ID = nodeVANDON.Attributes["CUA_KHAU_NHAP"].Value;
                vd.CuaKhauXuat = nodeVANDON.Attributes["CUA_KHAU_XUAT"].Value;

                vd.DKGH_ID = nodeVANDON.Attributes["MA_DKGH"].Value;
                vd.DiaDiemGiaoHang = nodeVANDON.Attributes["DIA_DIEM_GIAO_HANG"].Value;

                vd.SoHieuPTVT = nodeVANDON.Attributes["SO_HIEU_PTVT"].Value;
                vd.TenPTVT = nodeVANDON.Attributes["TEN_PTVT"].Value;
                vd.SoHieuChuyenDi = nodeVANDON.Attributes["SO_HIEU_CHUYEN_DI"].Value;
                vd.QuocTichPTVT = nodeVANDON.Attributes["QUOC_TICH_PTVT"].Value;

                vd.TenCangDoHang = nodeVANDON.Attributes["TEN_CANG_DO_HANG"].Value;
                vd.MaCangDoHang = nodeVANDON.Attributes["MA_CANG_DO_HANG"].Value;

                if (nodeVANDON.Attributes["NGAY_DEN_PTVT"].Value != "")
                    vd.NgayDenPTVT = Convert.ToDateTime(nodeVANDON.Attributes["NGAY_DEN_PTVT"].Value, c);
                vd.TenCangXepHang = nodeVANDON.Attributes["TEN_CANG_XEP_HANG"].Value;
                vd.MaCangXepHang = nodeVANDON.Attributes["MA_CANG_XEP_HANG"].Value;
                vd.TenHangVT = nodeVANDON.Attributes["TEN_HANG_VAN_TAI"].Value;
                vd.MaHangVT = nodeVANDON.Attributes["MA_HANG_VAN_TAI"].Value;

                vd.NuocXuat_ID = nodeVANDON.Attributes["NUOC_XN"].Value;
                vd.NoiDi = nodeVANDON.Attributes["NOI_PHAT_HANH"].Value;

                if (nodeVANDON.Attributes["NGAY_KHOI_HANH"].Value != "")
                    vd.NgayKhoiHanh = Convert.ToDateTime(nodeVANDON.Attributes["NGAY_KHOI_HANH"].Value, c);

                //Lay thong tin Container
                for (int i = 0; i < nodeVANDON.ChildNodes.Count; i++)
                {
                    XmlNode nodeCont = nodeVANDON.ChildNodes[i];

                    Company.KDT.SHARE.QuanLyChungTu.Container cont = new Company.KDT.SHARE.QuanLyChungTu.Container();
                    cont.SoHieu = nodeCont.Attributes["CONTAINER_NO"].Value;
                    cont.LoaiContainer = nodeCont.Attributes["CONTAINER_TYPE"].Value;
                    cont.Seal_No = nodeCont.Attributes["SEAL_NO"].Value;
                    cont.Trang_thai = Convert.ToInt32(nodeCont.Attributes["TRANG_THAI"].Value);

                    vd.ContainerCollection.Add(cont);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            tkmd.VanTaiDon = vd;

            return vd;
        }

        private BLL.KDT.HangMauDichCollection GetHangMauDichXML(ToKhaiMauDich tkmd, XmlDocument doc)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";

            //Lay du lieu node Root
            XmlNode nodeTOKHAI = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TO_KHAI_NK/TO_KHAI");

            //Lay thong tin van don
            XmlNode nodeHANG = null;
            if (nodeTOKHAI.SelectSingleNode("HANG") != null)
                nodeHANG = nodeTOKHAI.SelectSingleNode("HANG");
            /*
            <TO_KHAI_NK>
               <TO_KHAI MA_LH="NSX01" MA_HQ="C34C" MA_DV_NK="0400101556" MA_DV_UT="" MA_DV_XK="" TEN_DV_XK="MARUBENI CORPORATION&#xA;&#xD;4-2 OHTE" SO_GP="" NGAY_GP="" NGAY_HHGP="" SO_HD="AU791100" NGAY_HD="2010-05-20" NGAY_HHHD="2010-12-31" SO_HDTM="7N-395/10" NGAY_HDTM="2010-05-20" MA_PTVT="002" TEN_PTVT="CAPE FULMAR S005" NGAY_DEN="2010-06-02" SO_VANDON="HEB110050221" NGAY_VANDON="2010-05-25" NUOC_XK="JP" CANG_XUAT="KOBE" MA_CK_NHAP="C021" MA_DKGH="CIF" MA_NGTE="JPY" TY_GIA_VND="202.930" MA_PTTT="TTR" SO_PLTK="0" CHUNGTU_KEM="" SO_CONT20="1" SO_CONT40="0" SO_KIEN="20" TRONG_LUONG="438.50" PHI_BH="0.00" PHI_VC="0.00" LE_PHI_HQ="0.00" DE_XUAT_KHAC="khong co de xuat khac" CHU_HANG="KHOA">
                 <HANG>
                   <HANG.ITEM STT_HANG="1" MA_HANG="daykeo" MA_HS="9607190000" TEN_HANG="Dây kéo dưới 30cm" NUOC_XX="JP " LUONG="5076.000" MA_DVT="11         " DGIA_NT="23.148936200000000" TGIA_NT="117504.000151200000000" DGIA_TT_VND="4697.613623066000" TGIA_TT_VND="23845086.750683016000000" TS_XNK="23.00" TS_TTDB="0.00" TS_VAT="0.0000" TL_PHU_THU="0.00" THUE_XNK="5484370.0000" THUE_TTDB="0.0000" THUE_VAT="0.0000" PHU_THU="0.0000" />
                 </HANG>                
               </TO_KHAI>
             </TO_KHAI_NK>
             */

            BLL.KDT.HangMauDichCollection hmdCollection = new HangMauDichCollection();

            try
            {
                for (int i = 0; i < nodeHANG.ChildNodes.Count; i++)
                {
                    XmlNode nodeHangItem = nodeHANG.ChildNodes[i];
                    BLL.KDT.HangMauDich hmd = new BLL.KDT.HangMauDich();
                    hmd.TKMD_ID = tkmd.ID;

                    hmd.SoThuTuHang = Convert.ToInt32(nodeHangItem.Attributes["STT_HANG"].Value);
                    hmd.MaPhu = nodeHangItem.Attributes["MA_HANG"].Value;
                    hmd.MaHS = nodeHangItem.Attributes["MA_HS"].Value;
                    hmd.TenHang = nodeHangItem.Attributes["TEN_HANG"].Value;
                    hmd.NuocXX_ID = nodeHangItem.Attributes["NUOC_XX"].Value;
                    hmd.SoLuong = Convert.ToDecimal(nodeHangItem.Attributes["LUONG"].Value, f);
                    hmd.DVT_ID = nodeHangItem.Attributes["MA_DVT"].Value;
                    hmd.DonGiaKB = Convert.ToDecimal(nodeHangItem.Attributes["DGIA_NT"].Value, f);
                    hmd.TriGiaKB = Convert.ToDecimal(nodeHangItem.Attributes["TGIA_NT"].Value, f);
                    hmd.DonGiaTT = Convert.ToDecimal(nodeHangItem.Attributes["DGIA_TT_VND"].Value, f);
                    hmd.TriGiaTT = Convert.ToDecimal(nodeHangItem.Attributes["TGIA_TT_VND"].Value, f);
                    hmd.ThueSuatXNK = Convert.ToDecimal(nodeHangItem.Attributes["TS_XNK"].Value, f);
                    hmd.ThueSuatTTDB = Convert.ToDecimal(nodeHangItem.Attributes["TS_TTDB"].Value, f);
                    hmd.ThueGTGT = Convert.ToDecimal(nodeHangItem.Attributes["TS_VAT"].Value, f);
                    hmd.TyLeThuKhac = Convert.ToDecimal(nodeHangItem.Attributes["TL_PHU_THU"].Value, f);
                    hmd.ThueXNK = Convert.ToDecimal(nodeHangItem.Attributes["THUE_XNK"].Value, f);
                    hmd.ThueTTDB = Convert.ToDecimal(nodeHangItem.Attributes["THUE_TTDB"].Value, f);
                    hmd.ThueGTGT = Convert.ToDecimal(nodeHangItem.Attributes["THUE_VAT"].Value, f);
                    hmd.PhuThu = Convert.ToDecimal(nodeHangItem.Attributes["PHU_THU"].Value, f);

                    hmdCollection.Add(hmd);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            tkmd.HMDCollection = hmdCollection;

            return hmdCollection;
        }

        private List<Company.KDT.SHARE.QuanLyChungTu.GiayPhep> GetGiayPhepXML(ToKhaiMauDich tkmd, XmlDocument doc)
        {
            CultureInfo c = new CultureInfo("en-US");

            //Lay du lieu node Root
            XmlNode nodeDULIEU = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");

            //Lay thong tin hoa don
            XmlNode nodeGIAYPHEP = null;
            if (nodeDULIEU.SelectSingleNode("CHUNG_TU_GIAYPHEP") != null)
                nodeGIAYPHEP = nodeDULIEU.SelectSingleNode("CHUNG_TU_GIAYPHEP");
            /*
            <CHUNG_TU_GIAYPHEP>
              <CHUNG_TU_GIAYPHEP.ITEM SOGP="gp01" NGAYGP="06/03/2010" NGAYHHGP="06/04/2010" NOICAP="noicap" NGUOI_CAP="nguoicap" MA_NGUOI_DUOC_CAP="MURAKAMI" TEN_NGUOI_DUOC_CAP="MURAKAMI INTERNATIONAL LTD" MA_CQC="Macqcap" TEN_CQC="tenCQCap" HINH_THUC_TL="" GHI_CHU="">
                <CHUNG_TU_GIAYPHEP.HANG MA_NPL_SP="nhanchinhn" TEN_HANG="nh·n chÝnh nhùa" MA_HANGKB="3926209000" MA_DVT="11" LUONG="19411.000" TRIGIA_KB="2739.75000000001" MA_NT="USD" MA_CN="" />
              </CHUNG_TU_GIAYPHEP.ITEM>
            </CHUNG_TU_GIAYPHEP>
             */

            List<Company.KDT.SHARE.QuanLyChungTu.GiayPhep> giayPhepCollection = new List<Company.KDT.SHARE.QuanLyChungTu.GiayPhep>();

            try
            {
                //Lay hoa don
                for (int i = 0; i < nodeGIAYPHEP.ChildNodes.Count; i++)
                {
                    XmlNode nodeGiayPhepItem = nodeGIAYPHEP.ChildNodes[i];
                    Company.KDT.SHARE.QuanLyChungTu.GiayPhep gp = new Company.KDT.SHARE.QuanLyChungTu.GiayPhep();
                    gp.TKMD_ID = tkmd.ID;

                    gp.SoGiayPhep = nodeGiayPhepItem.Attributes["SOGP"].Value;
                    if (nodeGiayPhepItem.Attributes["NGAYGP"].Value != "")
                        gp.NgayGiayPhep = Convert.ToDateTime(nodeGiayPhepItem.Attributes["NGAYGP"].Value, c);
                    if (nodeGiayPhepItem.Attributes["NGAYHHGP"].Value != "")
                        gp.NgayHetHan = Convert.ToDateTime(nodeGiayPhepItem.Attributes["NGAYHHGP"].Value, c);
                    gp.NoiCap = nodeGiayPhepItem.Attributes["NOICAP"].Value;
                    gp.NguoiCap = nodeGiayPhepItem.Attributes["NGUOI_CAP"].Value;
                    gp.MaDonViDuocCap = nodeGiayPhepItem.Attributes["MA_NGUOI_DUOC_CAP"].Value;

                    gp.TenDonViDuocCap = nodeGiayPhepItem.Attributes["TEN_NGUOI_DUOC_CAP"].Value;
                    gp.MaCoQuanCap = nodeGiayPhepItem.Attributes["MA_CQC"].Value;

                    gp.TenQuanCap = nodeGiayPhepItem.Attributes["TEN_CQC"].Value;
                    //nodeGiayPhepItem.Attributes["HINH_THUC_TL"].Value;

                    gp.ThongTinKhac = nodeGiayPhepItem.Attributes["GHI_CHU"].Value;

                    //Lay hang cua giay phep: lay thong tin hang tu hang mau dich cua to khai (Neu Co)

                    giayPhepCollection.Add(gp);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            tkmd.GiayPhepCollection = giayPhepCollection;

            return giayPhepCollection;
        }

        private List<Company.KDT.SHARE.QuanLyChungTu.CO> GetCOXML(ToKhaiMauDich tkmd, XmlDocument doc)
        {
            CultureInfo c = new CultureInfo("en-US");

            //Lay du lieu node Root
            XmlNode nodeDULIEU = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");

            //Lay thong tin hoa don
            XmlNode nodeCO = null;
            if (nodeDULIEU.SelectSingleNode("CHUNG_TU_CO") != null)
                nodeCO = nodeDULIEU.SelectSingleNode("CHUNG_TU_CO");
            /*
            <CHUNG_TU_CO>
              <CHUNG_TU_CO.ITEM SO_CO="co01" NUOC_CO="US" NGAY_CAP_CO="06/03/2010" NGUOI_KY="nguoikyco" NOI_PHAT_HANH="ToChucCap" MA_LOAI_CO="007" NGUOI_XUAT="MURAKAMI INTERNATIONAL LTD. FLAT D,12F., BOLD WIN INDUSTRIAL BUILDING,16-18 WAH SING ST-KWAI CHUNG N.T HONG KONG" MA_NUOC_XUAT="US" NGUOI_NHAP="RUDHOLM & HAAK(H.K) LTD. UNIT 2502-06,TOWER B,REGENT CENTRE,70 TA CHUE PING ROAD-KWAI CHUNG,N.T HONG KONG" MA_NUOC_NHAP="US" NOI_DUNG="motachitiet" NGAY_KHOI_HANH="" GHI_CHU="" NOP_SAU="1" THOI_HAN_NOP="06/04/2010" />
            </CHUNG_TU_CO>
             */

            List<Company.KDT.SHARE.QuanLyChungTu.CO> coCollection = new List<Company.KDT.SHARE.QuanLyChungTu.CO>();

            try
            {
                //Lay hoa don
                for (int i = 0; i < nodeCO.ChildNodes.Count; i++)
                {
                    XmlNode nodeGiayPhepItem = nodeCO.ChildNodes[i];
                    Company.KDT.SHARE.QuanLyChungTu.CO co = new Company.KDT.SHARE.QuanLyChungTu.CO();
                    co.TKMD_ID = tkmd.ID;

                    co.SoCO = nodeGiayPhepItem.Attributes["SO_CO"].Value;
                    if (nodeGiayPhepItem.Attributes["NGAY_CAP_CO"].Value != "")
                        co.NgayCO = Convert.ToDateTime(nodeGiayPhepItem.Attributes["NGAY_CAP_CO"].Value, c);
                    co.NuocCapCO = nodeGiayPhepItem.Attributes["NUOC_CO"].Value;
                    co.NguoiKy = nodeGiayPhepItem.Attributes["NGUOI_KY"].Value;
                    co.ToChucCap = nodeGiayPhepItem.Attributes["NOI_PHAT_HANH"].Value;

                    co.LoaiCO = nodeGiayPhepItem.Attributes["MA_LOAI_CO"].Value;
                    co.TenDiaChiNguoiXK = nodeGiayPhepItem.Attributes["NGUOI_XUAT"].Value;

                    co.MaNuocXKTrenCO = nodeGiayPhepItem.Attributes["MA_NUOC_XUAT"].Value;
                    co.TenDiaChiNguoiNK = nodeGiayPhepItem.Attributes["NGUOI_NHAP"].Value;

                    co.MaNuocNKTrenCO = nodeGiayPhepItem.Attributes["MA_NUOC_NHAP"].Value;
                    co.ThongTinMoTaChiTiet = nodeGiayPhepItem.Attributes["NOI_DUNG"].Value;

                    //if (nodeGiayPhepItem.Attributes["NGAY_KHOI_HANH"].Value != "")
                    //    co. = Convert.ToDateTime(nodeGiayPhepItem.Attributes["NGAY_KHOI_HANH"].Value, c);

                    //co. = nodeGiayPhepItem.Attributes["GHI_CHU"].Value;

                    //Lay hang cua giay phep: lay thong tin hang tu hang mau dich cua to khai (Neu Co)

                    co.NoCo = Convert.ToInt32(nodeGiayPhepItem.Attributes["NOP_SAU"].Value);
                    if (nodeGiayPhepItem.Attributes["NGAY_CAP_CO"].Value != "")
                        co.ThoiHanNop = Convert.ToDateTime(nodeGiayPhepItem.Attributes["THOI_HAN_NOP"].Value, c);

                    coCollection.Add(co);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            tkmd.COCollection = coCollection;

            return coCollection;
        }

        private List<Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai> GetHoaDonThuongMaiXML(ToKhaiMauDich tkmd, XmlDocument doc)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";

            CultureInfo c = new CultureInfo("en-US");

            //Lay du lieu node Root
            XmlNode nodeDULIEU = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");

            //Lay thong tin hoa don
            XmlNode nodeHOADON = null;
            if (nodeDULIEU.SelectSingleNode("CHUNG_TU_HOADON") != null)
                nodeHOADON = nodeDULIEU.SelectSingleNode("CHUNG_TU_HOADON");
            /*
            <CHUNG_TU_HOADON>
              <CHUNG_TU_HOADON.ITEM SO_CT="7N-395/10" NGAY_CT="05/20/2010" MA_GH="CIF" MA_PTTT="TTR" MA_NT="USD" MA_DV="HOATHO" TEN_DV="HOATHO" MA_DV_DT="MARUBENI" TEN_DV_DT="MARUBENI" GHI_CHU="">
                <CHUNG_TU_HOADON.HANG MA_NPL_SP="daykeo" TEN_HANG="D©y kÐo d­íi 30cm" MA_HANGKB="9607190000" NUOC_XX="JP " MA_DVT="11" LUONG="5076.000" DGIA_KB="23.1489362" TRIGIA_KB="117504.0001512" GIATRITANG="0" GIATRIGIAM="0" GHI_CHU="" />
              </CHUNG_TU_HOADON.ITEM>
            </CHUNG_TU_HOADON>
             */

            List<Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai> hoaDonCollection = new List<Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai>();

            try
            {
                //Lay hoa don
                for (int i = 0; i < nodeHOADON.ChildNodes.Count; i++)
                {
                    XmlNode nodeHoaDonItem = nodeHOADON.ChildNodes[i];
                    Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hd = new Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai();
                    hd.TKMD_ID = tkmd.ID;

                    hd.SoHoaDon = nodeHoaDonItem.Attributes["SO_CT"].Value;
                    if (nodeHoaDonItem.Attributes["NGAY_CT"].Value != "")
                        hd.NgayHoaDon = Convert.ToDateTime(nodeHoaDonItem.Attributes["NGAY_CT"].Value, c);
                    hd.DKGH_ID = nodeHoaDonItem.Attributes["MA_GH"].Value;
                    hd.PTTT_ID = nodeHoaDonItem.Attributes["MA_PTTT"].Value;
                    hd.NguyenTe_ID = nodeHoaDonItem.Attributes["MA_NT"].Value;

                    hd.MaDonViMua = nodeHoaDonItem.Attributes["MA_DV"].Value;
                    hd.TenDonViMua = nodeHoaDonItem.Attributes["TEN_DV"].Value;

                    hd.MaDonViBan = nodeHoaDonItem.Attributes["MA_DV_DT"].Value;
                    hd.TenDonViBan = nodeHoaDonItem.Attributes["TEN_DV_DT"].Value;

                    hd.ThongTinKhac = nodeHoaDonItem.Attributes["GHI_CHU"].Value;

                    //Lay hang cua hoa don: lay thong tin hang tu hang mau dich cua to khai (Neu Co)

                    hoaDonCollection.Add(hd);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            tkmd.HoaDonThuongMaiCollection = hoaDonCollection;

            return hoaDonCollection;
        }

        private List<Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai> GetHopDongThuongMaiXML(ToKhaiMauDich tkmd, XmlDocument doc)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";

            CultureInfo c = new CultureInfo("en-US");

            //Lay du lieu node Root
            XmlNode nodeDULIEU = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");

            //Lay thong tin van don
            XmlNode nodeHOPDONG = null;
            if (nodeDULIEU.SelectSingleNode("CHUNG_TU_HOPDONG") != null)
                nodeHOPDONG = nodeDULIEU.SelectSingleNode("CHUNG_TU_HOPDONG");
            /*
            <CHUNG_TU_HOPDONG>
              <CHUNG_TU_HOPDONG.ITEM SO_CT="AU791100" NGAY_CT="05/20/2010" THOI_HAN_TT="12/31/2010" MA_GH="CIF" DIADIEM_GH="DANANG" MA_PTTT="TT" MA_NT="USD" TONGTRIGIA="829510.0000" MA_DV="HOATHO" TEN_DV="HOATHO" MA_DV_DT="MARUBENI" TEN_DV_DT="MARUBENI" GHI_CHU="">
                <CHUNG_TU_HOPDONG.HANG MA_NPL_SP="daykeo" TEN_HANG="D©y kÐo d­íi 30cm" MA_HANGKB="9607190000" MA_DVT="11" LUONG="5076.000" DGIA_KB="23.1489362" TRIGIA_KB="117504.0001512" GHI_CHU="" NUOC_XX="JP " />
              </CHUNG_TU_HOPDONG.ITEM>
            </CHUNG_TU_HOPDONG>
             */

            List<Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai> hopDongCollection = new List<Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai>();

            try
            {
                for (int i = 0; i < nodeHOPDONG.ChildNodes.Count; i++)
                {
                    XmlNode nodeHopDongItem = nodeHOPDONG.ChildNodes[i];
                    Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong = new Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai();
                    hopdong.TKMD_ID = tkmd.ID;

                    hopdong.SoHopDongTM = nodeHopDongItem.Attributes["SO_CT"].Value;
                    if (nodeHopDongItem.Attributes["NGAY_CT"].Value != "")
                        hopdong.NgayHopDongTM = Convert.ToDateTime(nodeHopDongItem.Attributes["NGAY_CT"].Value, c);
                    if (nodeHopDongItem.Attributes["THOI_HAN_TT"].Value != "")
                        hopdong.ThoiHanThanhToan = Convert.ToDateTime(nodeHopDongItem.Attributes["THOI_HAN_TT"].Value, c);
                    hopdong.PTTT_ID = nodeHopDongItem.Attributes["MA_PTTT"].Value;
                    hopdong.DKGH_ID = nodeHopDongItem.Attributes["MA_GH"].Value;
                    hopdong.DiaDiemGiaoHang = nodeHopDongItem.Attributes["DIADIEM_GH"].Value;
                    hopdong.NguyenTe_ID = nodeHopDongItem.Attributes["MA_NT"].Value;
                    hopdong.TongTriGia = Convert.ToDecimal(nodeHopDongItem.Attributes["TONGTRIGIA"].Value, f);
                    hopdong.MaDonViMua = nodeHopDongItem.Attributes["MA_DV"].Value;
                    hopdong.TenDonViMua = nodeHopDongItem.Attributes["TEN_DV"].Value;
                    hopdong.MaDonViBan = nodeHopDongItem.Attributes["MA_DV_DT"].Value;
                    hopdong.TenDonViBan = nodeHopDongItem.Attributes["TEN_DV_DT"].Value;
                    hopdong.ThongTinKhac = nodeHopDongItem.Attributes["GHI_CHU"].Value;

                    //Lay hang cua hoa don: lay thong tin hang tu hang mau dich cua to khai (Neu Co)

                    hopDongCollection.Add(hopdong);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            tkmd.HopDongThuongMaiCollection = hopDongCollection;

            return hopDongCollection;
        }

        private List<Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau> GetDeNghiChuyenCuaKhauXML(ToKhaiMauDich tkmd, XmlDocument doc)
        {
            CultureInfo c = new CultureInfo("en-US");

            //Lay du lieu node Root
            XmlNode nodeDULIEU = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");

            //Lay thong tin van don
            XmlNode nodeCHUYENCUAKHAU = null;
            if (nodeDULIEU.SelectSingleNode("CHUNG_TU_DENGHICHUYENCK") != null)
                nodeCHUYENCUAKHAU = nodeDULIEU.SelectSingleNode("CHUNG_TU_DENGHICHUYENCK");
            /*
            <CHUNG_TU_DENGHICHUYENCK>
              <CHUNG_TU_DENGHICHUYENCK.ITEM>
                <DoanhNghiep MaDN="0400101556" TenDN="0400101556" />
                <VanDon SoVanDon="HEB110050211" NgayVanDon="05/25/2010" />
                <NoiDung>GOI THEM CTU ATTACH NEU TK LUONG VANG. TONG CONG 20 KIEN =438.5KGS; HANG CHUNG BILL VOI TK ...; ...; ...; ...;...; ... NGC</NoiDung>
                <DiaDiemKiemTra>TIENSA</DiaDiemKiemTra>
                <ThoiGianDen>05/31/2010</ThoiGianDen>
                <TuyenDuong>TIENSA -  HOATHO</TuyenDuong>
              </CHUNG_TU_DENGHICHUYENCK.ITEM>
            </CHUNG_TU_DENGHICHUYENCK>
             */

            List<Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau> deNghiCollection = new List<Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau>();

            try
            {
                //Lay chung tu de nghi chuyen cua khau
                for (int i = 0; i < nodeCHUYENCUAKHAU.ChildNodes.Count; i++)
                {
                    XmlNode nodeCCK = nodeCHUYENCUAKHAU.ChildNodes[i];

                    Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau cck = new Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau();
                    cck.TKMD_ID = tkmd.ID;

                    cck.MaDoanhNghiep = nodeCCK.SelectSingleNode("DoanhNghiep").Attributes["MaDN"].Value;
                    cck.MaDoanhNghiep = nodeCCK.SelectSingleNode("DoanhNghiep").Attributes["TenDN"].Value;

                    cck.SoVanDon = nodeCCK.SelectSingleNode("VanDon").Attributes["SoVanDon"].Value;
                    if (nodeCCK.SelectSingleNode("VanDon").Attributes["NgayVanDon"].Value != "")
                        cck.NgayVanDon = Convert.ToDateTime(nodeCCK.SelectSingleNode("VanDon").Attributes["NgayVanDon"].Value, c);

                    cck.TuyenDuong = nodeCCK.SelectSingleNode("TuyenDuong").InnerXml;
                    if (nodeCCK.SelectSingleNode("ThoiGianDen").InnerXml != "")
                        cck.ThoiGianDen = Convert.ToDateTime(nodeCCK.SelectSingleNode("ThoiGianDen").InnerXml, c);
                    cck.DiaDiemKiemTra = nodeCCK.SelectSingleNode("DiaDiemKiemTra").InnerXml;
                    cck.ThongTinKhac = nodeCCK.SelectSingleNode("NoiDung").InnerXml;

                    deNghiCollection.Add(cck);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            tkmd.listChuyenCuaKhau = deNghiCollection;

            return deNghiCollection;
        }

        private List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKem> GetChungTuKemXML(ToKhaiMauDich tkmd, XmlDocument doc)
        {
            CultureInfo c = new CultureInfo("en-US");

            //Lay du lieu node Root
            XmlNode nodeDULIEU = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");

            //Lay thong tin van don
            XmlNode nodeChUNGTUKEM = null;
            if (nodeDULIEU.SelectSingleNode("CHUNG_TU_KEM") != null)
                nodeChUNGTUKEM = nodeDULIEU.SelectSingleNode("CHUNG_TU_KEM");
            /*
            <CHUNG_TU_KEM>
              <CHUNG_TU_KEM.ITEM>
                <SO_CT>20k</SO_CT>
                <NGAY_CT>2010-06-01</NGAY_CT>
                <MA_LOAI_CT>999</MA_LOAI_CT>
                <DIENGIAI></DIENGIAI>
                <DINH_KEM>
                  <DINH_KEM.ITEM>
                    <TENFILE>AU791100.tif</TENFILE>
                    <NOIDUNG xmlns:dt="urn:schemas-microsoft-com:datatypes" dt="bin.base64">
                    </NOIDUNG>
                    </DINH_KEM.ITEM>
                </DINH_KEM>
                </CHUNG_TU_KEM.ITEM>
            </CHUNG_TU_KEM>
             */

            List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKem> chungTuKemCollection = new List<Company.KDT.SHARE.QuanLyChungTu.ChungTuKem>();

            try
            {
                //lay chung tu kem
                for (int i = 0; i < nodeChUNGTUKEM.ChildNodes.Count; i++)
                {
                    XmlNode nodeChungTuItem = nodeChUNGTUKEM.ChildNodes[i];

                    Company.KDT.SHARE.QuanLyChungTu.ChungTuKem ctk = new Company.KDT.SHARE.QuanLyChungTu.ChungTuKem();
                    ctk.TKMDID = tkmd.ID;

                    ctk.SO_CT = nodeChungTuItem.SelectSingleNode("SO_CT").InnerXml;
                    if (nodeChungTuItem.SelectSingleNode("NGAY_CT").InnerXml != "")
                        ctk.NGAY_CT = Convert.ToDateTime(nodeChungTuItem.SelectSingleNode("NGAY_CT").InnerXml, c);
                    ctk.MA_LOAI_CT = nodeChungTuItem.SelectSingleNode("MA_LOAI_CT").InnerXml;
                    ctk.DIENGIAI = nodeChungTuItem.SelectSingleNode("DIENGIAI").InnerXml;

                    XmlNode nodeDinhKem = nodeChungTuItem.SelectSingleNode("DINH_KEM");

                    //Lay noi dung chi tiet chung tu kem
                    for (int j = 0; j < nodeDinhKem.ChildNodes.Count; j++)
                    {
                        XmlNode nodeChungTuDinhKem = nodeDinhKem.ChildNodes[j];
                        Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet ctkCT = new Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet();
                        ctkCT.FileName = nodeChungTuDinhKem.SelectSingleNode("TENFILE").InnerXml;
                        ctkCT.NoiDung = Convert.FromBase64String(nodeChungTuDinhKem.SelectSingleNode("NOIDUNG").InnerXml);
                        ctkCT.FileSize = ctkCT.NoiDung.Length;

                        ctk.listCTChiTiet.Add(ctkCT);
                    }

                    chungTuKemCollection.Add(ctk);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            tkmd.ChungTuKemCollection = chungTuKemCollection;

            return chungTuKemCollection;
        }

        private void ImportMSG()
        {
            OpenFileDialog opendlg = new OpenFileDialog();
            string filePath = Application.StartupPath;
            opendlg.InitialDirectory = filePath;
            opendlg.Filter = "XML (*.xml)|*.xml|All file (*)|*.*";
            opendlg.DefaultExt = ".xml";
            if (opendlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(opendlg.FileName);

                BLL.KDT.ToKhaiMauDich tkmd = new ToKhaiMauDich();

                XmlNode Result = doc.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result == null)
                    return;

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count > 0)
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            tkmd = (BLL.KDT.ToKhaiMauDich)i.GetRow().DataRow;

                            XuLyMessage(doc, tkmd);
                        }
                    }
                }

                /* Co the su dung o tren thay the
                if (dgList.CurrentRow.RowType == RowType.Record)
                {
                    long id = Convert.ToInt64(dgList.CurrentRow.Cells["ID"].Value);
                    tkmd.ID = id;
                    tkmd.Load();

                    XuLyMessage(doc, tkmd);
                }
                */
            }
        }

        private static void XuLyMessage(XmlDocument doc, BLL.KDT.ToKhaiMauDich tkmd)
        {
            string errorSt = ""; // Luu thong tin loi
            try
            {
                #region XU LY MESSAGE
                /*HUNGTQ, Update 19/05/2010, XU LY THONG BAO TRA VE*/
                //Kiem tra thong tin tu choi TK?
                if (doc.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("TuChoi") != null)
                {
                    #region TK bi Tu choi tiep nhan
                    if (doc.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TuChoi"].Value == "yes")
                    {
                        //nodeTuChoi.InnerText;
                        tkmd.HUONGDAN = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(doc.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = tkmd.ID;
                        kqxl.ReferenceID = new Guid(tkmd.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                        kqxl.NoiDung = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(doc.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                        tkmd.TrangThaiXuLy = BLL.TrangThaiXuLy.KHONG_PHE_DUYET;
                        tkmd.ActionStatus = 2;
                        tkmd.Update();
                    }
                    #endregion
                }
                else if (doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
                {
                    XmlNode nodeTrangthai = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                    if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.TOKHAINHAP || nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.TOKHAIXUAT
                        || nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.CAPNHATTOKHAINHAP || nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.CAPNHATTOKHAIXUAT)
                    {
                        #region Lấy số tiếp nhận

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            tkmd.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            tkmd.NamDK = Convert.ToInt32(nodeDuLieu.Attributes["NAM_TN"].Value);
                            tkmd.NgayTiepNhan = DateTime.Today;
                            tkmd.ActionStatus = 0;
                            tkmd.TrangThaiXuLy = Company.BLL.TrangThaiXuLy.CHO_DUYET;
                            tkmd.Update();

                            KetQuaXuLy kqxl = new KetQuaXuLy();
                            kqxl.ItemID = tkmd.ID;
                            kqxl.ReferenceID = new Guid(tkmd.GUIDSTR);
                            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", tkmd.SoTiepNhan, tkmd.NgayTiepNhan.ToShortDateString());
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();
                        }
                        #endregion
                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.TOKHAINHAP || nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.TOKHAIXUAT)
                    {
                        #region Hủy khai báo

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            KetQuaXuLy kqxl = new KetQuaXuLy();
                            kqxl.ItemID = tkmd.ID;
                            kqxl.ReferenceID = new Guid(tkmd.GUIDSTR);
                            kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                            kqxl.LoaiThongDiep = "Hủy khai báo";
                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nNgày hủy: {2}", tkmd.SoTiepNhan, tkmd.NgayTiepNhan.ToShortDateString(), DateTime.Now.ToString())
                                + "\r\n" + Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(doc.SelectSingleNode("Envelope/Body/Content/Root").InnerText); kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();

                            tkmd.HUONGDAN = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(doc.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                            tkmd.TrangThaiXuLy = BLL.TrangThaiXuLy.CHUA_KHAI_BAO;
                            tkmd.ActionStatus = -1;
                            tkmd.SoTiepNhan = 0;
                            tkmd.NgayTiepNhan = new DateTime(1900, 1, 1);
                            tkmd.Update();
                        }

                        #endregion
                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == LAY_THONG_TIN.TOKHAI)
                    {
                    }
                }
                else if (doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
                {
                    #region LOI Khai bao TK
                    XmlNode nodeMota = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = doc.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;

                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";

                    KetQuaXuLy kqxl = new KetQuaXuLy();
                    kqxl.ItemID = tkmd.ID;
                    kqxl.ReferenceID = new Guid(tkmd.GUIDSTR);
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai + " " + tkmd.MaLoaiHinh;
                    kqxl.LoaiThongDiep = errorSt; //KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                    kqxl.NoiDung = doc.SelectSingleNode("Envelope/Body/Content/Root").InnerText; // Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(doc.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    errorSt = errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi;

                    throw new Exception(errorSt);
                    #endregion
                }
                /*Kiem tra PHAN LUONG*/
                else if (doc.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTN") != null)
                {
                    #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                    XmlNode nodeRoot = doc.SelectSingleNode("Envelope/Body/Content/Root");

                    if (nodeRoot.SelectSingleNode("PHAN_LUONG") == null) //Neu message chua phan luong -> cap nhat SO TN.
                    {
                        tkmd.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                        tkmd.NgayTiepNhan = DateTime.Today;
                        tkmd.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);

                        tkmd.ActionStatus = 0;
                        tkmd.TrangThaiXuLy = BLL.TrangThaiXuLy.CHO_DUYET;


                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = tkmd.ID;
                        kqxl.ReferenceID = new Guid(tkmd.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                        kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", tkmd.SoTiepNhan, tkmd.NgayTiepNhan.ToShortDateString());
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                    }

                    /*Lay thong tin phan luong*/
                    if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                    {
                        XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                        tkmd.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                        tkmd.HUONGDAN = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                        tkmd.ActionStatus = 1;
                        tkmd.TrangThaiXuLy = BLL.TrangThaiXuLy.DA_DUYET;

                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = tkmd.ID;
                        kqxl.ReferenceID = new Guid(tkmd.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocPhanLuong;

                        string tenluong = "Xanh";

                        if (tkmd.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                            tenluong = "Vàng";
                        else if (tkmd.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                            tenluong = "Đỏ";

                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}", tkmd.SoToKhai, tkmd.NgayDangKy.ToShortDateString(), tkmd.MaLoaiHinh.Trim(), tkmd.MaHaiQuan.Trim(), tenluong, tkmd.HUONGDAN);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                    }

                    /*Kiem tra co So to khai*/
                    else if (doc.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTK") != null)
                    {
                        tkmd.SoToKhai = Convert.ToInt32(nodeRoot.Attributes["SOTK"].Value);
                        if (doc.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("NGAYDK") != null)
                            tkmd.NgayDangKy = Convert.ToDateTime(nodeRoot.Attributes["NGAYDK"].Value);
                        tkmd.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMDK"].Value);

                        tkmd.ActionStatus = 1;
                        tkmd.TrangThaiXuLy = BLL.TrangThaiXuLy.DA_DUYET;

                        KetQuaXuLy kqxl = new KetQuaXuLy();
                        kqxl.ItemID = tkmd.ID;
                        kqxl.ReferenceID = new Guid(tkmd.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", tkmd.SoToKhai, tkmd.NgayDangKy.ToShortDateString(), tkmd.MaLoaiHinh.Trim(), tkmd.MaHaiQuan.Trim());
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                    }

                    //Cap nhat to khai
                    tkmd.Update();

                    #endregion Lấy số tiếp nhận của danh sách NPL
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("Loi xu ly message:" + ex.Message);
                throw new Exception(errorSt);
            }
        }

        #endregion

        #region COMBO TRANG THAI

        /// <summary>
        /// Bổ sung thêm trạng thái cho combo Trạng thái tờ khai
        /// </summary>
        /// <param name="cboTrangThai"></param>
        private void SetComboTrangthai(Janus.Windows.EditControls.UIComboBox cboTrangThai)
        {
            cboTrangThai.Items.Add("Sửa tờ khai", Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET);
            cboTrangThai.Items.Add("Chờ hủy", Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY);
            cboTrangThai.Items.Add("Đã hủy", Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY);
        }

        #endregion

        //-----------------------------------------------------------------------------------------

        private void setCommandStatus()
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            DongBoDuLieu.Enabled = InheritableBoolean.True;
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSingleDownload.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = InheritableBoolean.True;
                khaibaoCTMenu.Enabled = false;
                cmdSend.Enabled = InheritableBoolean.False;
                xóaTờKhaiToolStripMenuItem.Enabled = false;
                btnDelete.Enabled = false;
                NhanDuLieuCTMenu.Enabled = true;
                HuyCTMenu.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                print.Enabled = true;
                cmdXuatToKhaiChoPhongKhai.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = true;
                PhieuTNMenuItem.Enabled = true;
                mnuPhanLuong.Enabled = true;
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.False;
                khaibaoCTMenu.Enabled = false;
                NhanDuLieuCTMenu.Enabled = false;
                HuyCTMenu.Enabled = false;
                xóaTờKhaiToolStripMenuItem.Enabled = false;
                btnDelete.Enabled = false;
                print.Enabled = true;
                cmdXuatToKhaiChoPhongKhai.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                mnuCSDaDuyet.Enabled = false;
                PhieuTNMenuItem.Enabled = true;

                mnuPhanLuong.Enabled = true;
                mnuSuaToKhai.Enabled = true;
                mnuHuyToKhai.Enabled = true;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = InheritableBoolean.False;
                khaibaoCTMenu.Enabled = true;
                NhanDuLieuCTMenu.Enabled = false;
                HuyCTMenu.Enabled = false;
                xóaTờKhaiToolStripMenuItem.Enabled = true;
                btnDelete.Enabled = true;
                print.Enabled = true;
                cmdXuatToKhaiChoPhongKhai.Enabled = InheritableBoolean.True;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                mnuCSDaDuyet.Enabled = true;

                PhieuTNMenuItem.Enabled = false;
                mnuPhanLuong.Enabled = false;
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = InheritableBoolean.True;
                khaibaoCTMenu.Enabled = true;
                NhanDuLieuCTMenu.Enabled = true;
                HuyCTMenu.Enabled = true;
                xóaTờKhaiToolStripMenuItem.Enabled = false;
                btnDelete.Enabled = false;
                cmdXuatToKhaiChoPhongKhai.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                mnuCSDaDuyet.Enabled = false;
                PhieuTNMenuItem.Enabled = true;
                print.Enabled = true;

                mnuPhanLuong.Enabled = false;
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
            {
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
            {
                mnuSuaToKhai.Enabled = false;
                mnuHuyToKhai.Enabled = true;
            }
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)) && !MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuXuat)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    uiCommandBar1.Visible = false;
                    this.dgList.ContextMenuStrip = null;
                    DongBoDuLieu.Enabled = InheritableBoolean.False;
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        //Lypt create date 20/01/2010
        private void setDataToCboPhanLuong()
        {
            DataTable dtPL = new DataTable();
            dtPL.Columns.Add("ID", typeof(System.String));
            dtPL.Columns.Add("GhiChu", typeof(System.String));
            DataRow drNull = dtPL.NewRow();
            drNull["ID"] = -1;
            drNull["GhiChu"] = "--Tất cả--";
            dtPL.Rows.Add(drNull);
            for (int i = 1; i <= 3; i++)
            {
                DataRow dr = dtPL.NewRow();
                dr["ID"] = i.ToString();
                if (i == 1)
                    dr["GhiChu"] = "Luồng xanh";
                if (i == 2)
                    dr["GhiChu"] = "Luồng vàng";
                if (i == 3)
                    dr["GhiChu"] = "Luồng đỏ";
                dtPL.Rows.Add(dr);
            }
            cboPhanLuong.DataSource = dtPL;
            cboPhanLuong.DisplayMember = dtPL.Columns["GhiChu"].ToString();
            cboPhanLuong.ValueMember = dtPL.Columns["ID"].ToString();

        }

        private void setDataToCboNguoiKB()
        {
            DataTable dt = new Company.QuanTri.User().SelectAll().Tables[0];
            DataRow dr = dt.NewRow();
            dr["USER_NAME"] = -1;
            dr["HO_TEN"] = "--Tất cả--";
            dt.Rows.InsertAt(dr, 0);
            cbUserKB.DataSource = dt;
            cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                txtNamTiepNhan.Text = string.Empty;
                txtNamTiepNhan.Value = 0;
                txtNamTiepNhan.Enabled = false;
                txtSoTiepNhan.Value = 0;
                txtSoTiepNhan.Text = string.Empty;
                txtSoTiepNhan.Enabled = false;
                lblSoTK.Enabled = false;
                txtSoTK.Enabled = false;
                chkDate.Checked = false;
                chkDate.Enabled = false;
                lblTuNgay.Enabled = false;
                dtTuNgay.Enabled = false;
                lblDenNgay.Enabled = false;
                dtDenNgay.Enabled = false;
                btnExportExcel.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET)
            {
                txtSoTK.Text = "";
                lblSoTK.Enabled = true;
                txtSoTK.Enabled = true;
                chkDate.Checked = false;
                chkDate.Enabled = false;
                lblTuNgay.Enabled = false;
                dtTuNgay.Enabled = false;
                lblDenNgay.Enabled = false;
                dtDenNgay.Enabled = false;
                btnExportExcel.Enabled = false;
            }
            else
            {
                if (txtNamTiepNhan.Text == "")
                    txtNamTiepNhan.Value = DateTime.Today.Year;
                txtNamTiepNhan.Enabled = true;
                txtSoTiepNhan.Enabled = true;
                lblSoTK.Enabled = true;
                txtSoTK.Enabled = true;
                lblTuNgay.Enabled = false;
                dtTuNgay.Enabled = false;
                lblDenNgay.Enabled = false;
                dtDenNgay.Enabled = false;
                btnExportExcel.Enabled = true;
                chkDate.Checked = false;
                chkDate.Enabled = true;
            }
            this.search();
        }

        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                cbStatus.SelectedValueChanged += new EventHandler(cbStatus_SelectedValueChanged);
                cboPhanLuong.SelectedValueChanged -= new EventHandler(cboPhanLuong_SelectedValueChanged);

                chkDate.Checked = false;
                cbStatus.SelectedIndex = 0;
                cbUserKB.SelectedIndex = 0;
                this.setDataToCboPhanLuong();
                this.setDataToCboNguoiKB();
                cboPhanLuong.SelectedIndex = 0;

                dgList.RootTable.RowHeaderWidth = 70;

                if (MainForm.versionHD == 0)
                {
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)) && !MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuXuat)))
                    {
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        uiCommandBar1.Visible = false;
                        this.dgList.ContextMenuStrip = null;
                    }
                }
                this.search();
                setCommandStatus();

                //Update by Hungtq, 17/10/2011. Cap nhat vi tri cot.
                dgList.RootTable.Columns["SoTiepNhan"].Position = 2;
                dgList.RootTable.Columns["NgayTiepNhan"].Position = 3;
                dgList.RootTable.Columns["SoToKhai"].Position = 4;
                dgList.RootTable.Columns["NgayDangKy"].Position = 5;
                dgList.RootTable.Columns["SoHopDong"].Position = 6;
                dgList.RootTable.Columns["TrangThaiXuLy"].Position = 7;
                dgList.RootTable.Columns["PhanLuong"].Position = 8;

                dgList.RootTable.Columns["NgayDangKy"].Width = 120;

                cbStatus.SelectedValueChanged += new EventHandler(cbStatus_SelectedValueChanged);
                cboPhanLuong.SelectedValueChanged += new EventHandler(cboPhanLuong_SelectedValueChanged);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private ToKhaiMauDich getTKMDByID(long id)
        {
            foreach (ToKhaiMauDich tk in this.tkmdCollection)
            {
                if (tk.ID == id) return tk;
            }
            return null;
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() != TrangThaiXuLy.DA_DUYET.ToString())
                {
                    long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                    f.OpenType = OpenFormType.Edit;
                    f.TKMD = this.getTKMDByID(id);
                    int tt = f.TKMD.TrangThaiXuLy;
                    //if (f.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && loaiWS == "1")
                    //{
                    //    f.OpenType = OpenFormType.View;
                    //}
                    f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                    f._bNew = false;
                    f.ShowDialog();
                    if (tt != f.TKMD.TrangThaiXuLy)
                        search();
                }
                else
                {
                    long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                    f.OpenType = OpenFormType.View;
                    f.TKMD = this.getTKMDByID(id);
                    f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                    f._bNew = false;
                    f.ShowDialog();
                    try
                    {
                        f.Dispose();
                    }
                    catch { }
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.

                string where = "1 = 1";
                where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);
                where += string.Format(" AND MaDoanhNghiep = '{0}'", GlobalSettings.MA_DON_VI);
                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan = " + txtSoTiepNhan.Value;
                }

                if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                    }
                }
                if (cbStatus.SelectedValue != null)
                    where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;
                if (this.nhomLoaiHinh != "") where += " AND MaLoaiHinh LIKE '%" + this.nhomLoaiHinh + "%'";
                if (cboPhanLuong.SelectedValue != null && cboPhanLuong.SelectedIndex != 0)
                    where += " AND PhanLuong=" + cboPhanLuong.SelectedValue;
                if (txtSoTK.TextLength > 0)
                    where += " AND SoToKhai = " + txtSoTK.Value;
                if (chkDate.Checked == true)
                    where += string.Format(" AND NgayDangKy >= '{0}' AND NgayDangKy <= '{1}'", dtTuNgay.Value.ToString("yyyy-MM-dd"), dtDenNgay.Value.ToString("yyyy-MM-dd"));

                //Hungtq Update 17/01/2012.
                if (txtSoHopDong.TextLength > 0)
                    where += " AND SoHopDong like '%" + txtSoHopDong.Text + "%'";
                if (txtSoHoaDon.TextLength > 0)
                    where += " AND SoHoaDonThuongMai like '%" + txtSoHoaDon.Text + "%'";

                if (cbUserKB.SelectedValue != null && cbUserKB.SelectedIndex != 0)
                {
                    #region Old_Code
                    //string dsTK = "";
                    //if (listLog.Count == 0)
                    //    where += " AND ID = 0";
                    //else if (listLog.Count == 1)
                    //    where += " AND ID = " + listLog[0].ID_DK.ToString();
                    //else
                    //{
                    //    for (int i = 0; i < listLog.Count; i++)
                    //    {
                    //        if (i == listLog.Count - 1)
                    //            dsTK += listLog[i].ID_DK.ToString();
                    //        else
                    //            dsTK += listLog[i].ID_DK.ToString() + ",";
                    //    }
                    //    where += " AND ID IN (" + dsTK + ")";
                    //}
                    #endregion

                    this.tkmdCollection = new ToKhaiMauDich().searchTKMDByUserName(cbUserKB.SelectedItem.Value.ToString(), where);
                    dgList.DataSource = this.tkmdCollection;
                    this.setCommandStatus();
                    return;
                }
                // Thực hiện tìm kiếm.            
                this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");
                dgList.DataSource = this.tkmdCollection;

                this.setCommandStatus();

                //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi trong quá trình tìm kiếm dữ liệu.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                string maLH = e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim();
                e.Row.Cells["MaLoaiHinh"].Text = maLH + " - " + this.LoaiHinhMauDich_GetName(maLH);
                if (e.Row.Cells["NgayTiepNhan"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }

                DateTime dtNgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                if (dtNgayDangKy.Year <= 1900)
                    e.Row.Cells["NgayDangKy"].Text = "";

                #region Begin TrangThaiXuLy
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        // e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Information has not yet sent";

                        }
                        break;
                    case 0:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for approval";

                        }
                        break;
                    case 1:
                        {
                            //e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            }
                            else
                            {
                                e.Row.Cells["TrangThaiXuLy"].Text = "Approved";

                            }

                        }
                        break;
                    case 2:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not Approved";

                        }
                        break;
                    case 5:
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Sửa tờ khai";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Edit";
                        break;
                    case 10:
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Deleted";
                        break;
                    case 11:
                        if (GlobalSettings.NGON_NGU == "0")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                        else
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for delete";
                        break;
                }
                #endregion End TrangThaiXuLy

                #region Begin PhanLuong
                if (e.Row.Cells["PhanLuong"].Value != null && e.Row.Cells["PhanLuong"].Value != "")
                {
                    switch (Convert.ToInt32(e.Row.Cells["PhanLuong"].Value))
                    {
                        case 1:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["PhanLuong"].Text = "Luồng xanh";
                            else
                                e.Row.Cells["PhanLuong"].Text = "Green";
                            break;
                        case 2:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["PhanLuong"].Text = "Luồng vàng";
                            else
                                e.Row.Cells["PhanLuong"].Text = "Yellow";
                            break;
                        case 3:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["PhanLuong"].Text = "Luồng đỏ";
                            else
                                e.Row.Cells["PhanLuong"].Text = "Red";
                            break;
                    }

                    e.Row.Cells["HuongDan"].ToolTipText = e.Row.Cells["HuongDan"].Text;
                }
                #endregion End PhanLuong
            }
        }

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSingleDownload":
                    this.downloadItemsSelect();
                    break;
                case "cmdCancel":
                    this.cancelItemsSelect();
                    break;
                case "cmdSend":
                    this.sendItemsSelect();
                    break;
                case "SaoChep":
                    this.SaoChepToKhaiMD();
                    break;
                case "SaoChepALL":
                    this.SaoChepALLHang();
                    break;
                case "SaoChepToKhaiHang":
                    this.SaoChepToKhaiMD();
                    break;
                case "XacNhan":
                    this.XacNhanThongTin();
                    break;
                //case "Export":
                //    this.ExportData();
                //    break;
                //case "Import":
                //    this.ImportData();
                //    break;
                case "cmdXuatToKhaiChoPhongKhai":
                    XuatToKhaiChoPhongKhai();
                    break;
                case "cmdCSDaDuyet":
                    this.ChuyenTrangThai();
                    break;
                case "cmdLayPhanHoi":
                    this.LayPhanHoiDN();
                    break;
                case "cmdNhanTrangThaiPhanLuong":
                    this.NhanPhanLuong();
                    break;
            }
        }


        private void LayPhanHoiDN()
        {
            FormSendToKhai f = new FormSendToKhai();
            f.MaHaiQuan = donViHaiQuanNewControl1.Ma;
            f.btnNhan.Enabled = true;
            f.ShowDialog();
            this.search();
        }

        private void inPhieuTN()
        {
            if (dgList.GetRows().Length < 1) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "TỜ KHAI";
            Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = -1;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    ToKhaiMauDich tkDangKySelected = (ToKhaiMauDich)i.GetRow().DataRow;
                    j++;
                    arrPhieuTN[j, 0] = tkDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = tkDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();
        }
        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count == 1)
            {
                ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        ChuyenTrangThaiTK obj = new ChuyenTrangThaiTK((ToKhaiMauDich)grItem.GetRow().DataRow);
                        obj.ShowDialog();
                    }
                }
                this.search();
            }
            else if (dgList.SelectedItems.Count > 1)
            {
                ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();

                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        tkmdColl.Add((ToKhaiMauDich)grItem.GetRow().DataRow);
                    }
                }
                ChuyenTrangThaiMultiForm obj = new ChuyenTrangThaiMultiForm(tkmdColl);
                obj.ShowDialog();
                this.search();
            }
            else
            {
                // ShowMessage("Không có dữ liệu được chọn", false);
                MLMessages("Chưa có dữ liệu được chọn", "MSG_REC02", "", false);

            }
        }

        private void XuatToKhaiChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách tờ khai cần xuất.", false);
                MLMessages("Chưa chọn danh sách tờ khai cần xuất.", "MSG_REC02", "", false);
                return;
            }
            try
            {
                ToKhaiMauDichCollection col = new ToKhaiMauDichCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sotokhai = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tokhaiSelected = (ToKhaiMauDich)i.GetRow().DataRow;
                            tokhaiSelected.LoadHMDCollection();
                            col.Add(tokhaiSelected);
                            sotokhai++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    //ShowMessage("Xuất thành công " + sotokhai + " tờ khai.", false);
                    MLMessages("Xuất thành công " + sotokhai + " tờ khai.", "MSG_NPL05", "" + sotokhai, false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }

        private string checkDataImport(ToKhaiMauDichCollection collection)
        {
            string st = "";
            foreach (ToKhaiMauDich tkmd in collection)
            {
                ToKhaiMauDich tkmdInDatabase = new ToKhaiMauDich();
                tkmdInDatabase.ID = (tkmd.ID);
                tkmdInDatabase.Load();
                if (tkmdInDatabase != null)
                {
                    if (tkmdInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        //st += "Danh sách có ID=" + tkmd.ID + " đã được duyệt.\n";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st += "Danh sách có ID=" + tkmd.ID + " đã được duyệt.\n";
                        }
                        else
                        {
                            st += "List has ID=" + tkmd.ID + " already approved.\n";

                        }
                    }
                    else
                    {
                        tmpCollection.Add(tkmd);
                    }
                }
                else
                {
                    if (tkmd.ID > 0)
                        tkmd.ID = 0;
                    tmpCollection.Add(tkmd);
                }
            }
            return st;
        }
        //private void ImportData()
        //{
        //    if (openFileDialog1.ShowDialog() == DialogResult.OK)
        //    {
        //        tmpCollection.Clear();
        //        try
        //        {
        //            XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
        //            FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
        //            ToKhaiMauDichCollection tkmdCollection = (ToKhaiMauDichCollection)serializer.Deserialize(fs);
        //            fs.Close();
        //            string st = checkDataImport(tkmdCollection);
        //            if (st != "")
        //            {
        //                if (ShowMessage("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", true) == "Yes")
        //                {
        //                    ToKhaiMauDich.DongBoDuLieuPhongKhai(tmpCollection);
        //                    ShowMessage("Import thành công", false);
        //                }
        //            }
        //            else
        //            {
        //                ToKhaiMauDich.DongBoDuLieuPhongKhai(tmpCollection);
        //                ShowMessage("Import thành công", false);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ShowMessage("Lỗi : " + ex.Message, false);
        //        }
        //    }
        //}
        //private void ExportData()
        //{
        //    GridEXSelectedItemCollection items = dgList.SelectedItems;
        //    if (items.Count == 0)
        //    {
        //        ShowMessage("Chưa chọn danh sách tờ khai", false);
        //        return;
        //    }
        //    try
        //    {
        //        ToKhaiMauDichCollection col = new ToKhaiMauDichCollection();
        //        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
        //        {
        //            XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
        //            FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

        //            foreach (GridEXSelectedItem i in items)
        //            {
        //                if (i.RowType == RowType.Record)
        //                {
        //                    ToKhaiMauDich tkmdSelected = (ToKhaiMauDich)i.GetRow().DataRow;
        //                    tkmdSelected.LoadHMDCollection();
        //                    col.Add(tkmdSelected);
        //                }
        //            }
        //            serializer.Serialize(fs, col);
        //            fs.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ShowMessage("Lỗi : " + ex.Message, false);
        //    }

        //}
        private void SaoChepALLHang()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuHaiQuan();

            foreach (HangMauDich hmd in tkmd.HMDCollection)
            {
                hmd.ID = 0;
            }

            tkmd.SoToKhai = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            tkmd.LyDoSua = "";
            tkmd.DeXuatKhac = "";

            tkmd.PhanLuong = string.Empty;
            tkmd.NgayDangKy = new DateTime(1900, 1, 1);
            tkmd.NgayTiepNhan = new DateTime(1900, 1, 1);
            tkmd.HUONGDAN = string.Empty;
            tkmd.NamDK = 0;
            tkmd.ActionStatus = 0;

            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f._bNew = true;
            f.pTKMD_ID = tkmd.ID;
            tkmd.ID = 0;
            tkmd.GUIDSTR = Guid.NewGuid().ToString();
            f.OpenType = OpenFormType.Edit;
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;
            f.TKMD.HMDCollection = tkmd.HMDCollection;
            f.Show();
        }
        private void SaoChepToKhaiMD()
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.SoToKhai = 0;
            tkmd.ID = 0;
            tkmd.SoTiepNhan = 0;
            tkmd.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            tkmd.GUIDSTR = Guid.NewGuid().ToString();
            tkmd.PhanLuong = string.Empty;
            tkmd.NgayDangKy = new DateTime(1900, 1, 1);
            tkmd.NgayTiepNhan = new DateTime(1900, 1, 1);
            tkmd.HUONGDAN = string.Empty;
            tkmd.NamDK = 0;
            tkmd.ActionStatus = 0;

            ToKhaiMauDichForm f = new ToKhaiMauDichForm();
            f.OpenType = OpenFormType.Edit;
            f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
            f.MdiParent = this.ParentForm;
            f.TKMD = tkmd;
            f.Show();
        }
        private void LaySoTiepNhanDT()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = "";
            try
            {
                if (dgList.GetRow() != null)
                {
                    tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                    sendXML.LoaiHS = "TK";
                    sendXML.master_id = tkmd.ID;
                    if (!sendXML.Load())
                    {
                        // ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                        MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_STN01", "", false);
                        return;
                    }
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = tkmd.LayPhanHoi(password, sendXML.msg);
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        // string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                        string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                        if (kq == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        return;
                    }

                    if (sendXML.func == 1)
                    {
                        // ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + tkmd.SoTiepNhan, false);
                        MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + tkmd.SoTiepNhan, "MSG_SEN05", "" + tkmd.SoTiepNhan, false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        //  ShowMessage("Đã hủy tờ khai này", false);
                        MLMessages("Đã hủy tờ khai này", "MSG_SEN06", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (tkmd.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            // ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số tờ khai : " + tkmd.SoToKhai.ToString(), false);                           
                            MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số tờ khai : " + tkmd.SoToKhai.ToString(), "MSG_SEN18", "" + tkmd.SoToKhai.ToString(), false);
                            this.search();
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            // ShowMessage("Hải quan chưa xử lý tờ khai này!", false);
                            MLMessages("Hải quan chưa xử lý tờ khai này!", "MSG_SEN08", "", false);
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            // ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                            MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_STN09", "", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = tkmd.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = tkmd.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                            {
                                sendXML.Delete();
                            }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            MsgSend sendXML = new MsgSend();
            try
            {

                if (dgList.GetRow() != null)
                {
                    tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                    sendXML.LoaiHS = "TK";
                    sendXML.master_id = tkmd.ID;
                    sendXML.Load();
                    this.Cursor = Cursors.WaitCursor;
                    //xmlCurrent = tkmd.LayPhanHoi(pass, sendXML.msg);
                    xmlCurrent = tkmd.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                        if (kq == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        return;
                    }
                    string mess = "";
                    if (sendXML.func == 1)
                    {

                        mess = "Khai báo thông tin thành công ! \r\n------------------------------------- \r\n " + "Số tiếp nhận : " + tkmd.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + tkmd.MaLoaiHinh.ToString() + "\r\n Ngày tiếp nhận : " + tkmd.NgayTiepNhan.ToShortDateString();
                        ShowMessageTQDT(mess, false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        ShowMessageTQDT("Hủy tờ khai thành công", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (tkmd.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số tờ khai : "+tkmd.SoToKhai.ToString(), false);                            
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số tờ khai : " + tkmd.SoToKhai.ToString(), "MSG_SEN18", "" + tkmd.SoToKhai.ToString(), false);
                            ShowMessageTQDT("Trạng thái chứng từ đã thay đổi:\n\nTrạng thái : Đã duyệt chính thức.\nSố tờ khai : " + tkmd.SoToKhai.ToString() + "\nNgày đăng ký : " + tkmd.NgayDangKy.ToString("dd/MM/yyyy"), false);
                            this.search();
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            //ShowMessage("Hải quan chưa xử lý tờ khai này!", false);
                            MLMessages("Hải quan chưa xử lý tờ khai này!", "MSG_SEN08", "", false);
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            //ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", false);
                            MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN09", "", false);
                            this.search();
                        }
                    }
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                    setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Khai báo không thành công.Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = tkmd.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = tkmd.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else if (msg[1] == "DATA_LEVEL")
                        {
                            ShowMessageTQDT(msg[0], false);
                            return;
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. \n\n" + ex.Message, "MSG_SEN20", "", false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void download()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            string password = "";
            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                if (sendXML.Load())
                {
                    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                    return;
                }
            }
            else
            {
                //ShowMessage("Chưa chọn thông tin nhận trạng thái.", false);
                MLMessages("Chưa chọn thông tin nhận trạng thái.", "MSG_REC01", "", false);
                return;
            }
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                if (tkmd.MaLoaiHinh.IndexOf("SX") > 0)
                    xmlCurrent = tkmd.WSRequestXML(password);

                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = tkmd.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = tkmd.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong nhận thông tin : " + msg[0], false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định.", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        //------------------
        private void LayPhanHoiPhanLuong(string pass)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            MsgSend sendXML = new MsgSend();
            try
            {

                if (dgList.GetRow() != null)
                {
                    tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                    sendXML.LoaiHS = "TK";
                    sendXML.master_id = tkmd.ID;
                    sendXML.Load();
                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = tkmd.WSRequestPhanLuong(pass); //.LayPhanHoi(pass, sendXML.msg);
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        //  string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                        string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                        if (kq == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoiPhanLuong(pass);
                        }
                        return;
                    }
                    string mess;
                    if (tkmd.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                    {
                        mess = " Loại phân luồng : " + "Luồng xanh" + ".\r\n" + " Hướng dẫn của Hải quan : " + tkmd.HUONGDAN.ToString() + "\r\n";
                        this.ShowMessage("Thông báo trả về từ hệ thống Hải quan :\r\n " + "Số tờ khai : " + tkmd.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + tkmd.MaLoaiHinh.Substring(0, 3) + "\r\n" + mess, false);
                        this.search();
                    }
                    else if (tkmd.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                    {
                        mess = "Loại phân luồng : " + "Luồng vàng" + "\r\n" + " Hướng dẫn của Hải quan : " + tkmd.HUONGDAN.ToString() + "\r\n";
                        this.ShowMessage("Thông báo trả về từ hệ thống Hải quan :\r\n " + mess, false);
                    }
                    else if (tkmd.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                    {
                        mess = "Loại phân luồng : " + "Luồng đỏ" + "\r\n" + " Hướng dẫn của Hải quan : " + tkmd.HUONGDAN.ToString() + "\r\n";
                        this.ShowMessage("Thông báo trả về từ hệ thống Hải quan :\r\n " + mess, false);
                        this.search();
                    }
                    else
                    {
                        if (tkmd.SoToKhai == 0 && tkmd.ActionStatus == 1)
                        {
                            this.ShowMessage("Thông báo trả về từ hệ thống Hải quan ! \r\n Nội dung : Tờ khai chưa được duyệt.", false);
                        }
                        else if (tkmd.SoToKhai == 0 && tkmd.ActionStatus == -1)
                        {

                            this.ShowMessage("Chưa nhận được thông tin phản hồi từ hệ thống Hải quan.", false);
                        }
                        else
                        {
                            this.ShowMessage("Thông báo trả về từ hệ thống Hải quan :\r\n " + "Số tờ khai : " + tkmd.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + tkmd.MaLoaiHinh.Substring(0, 3) + "\r\n Năm đăng ký : " + tkmd.NamDK.ToString(), false);
                        }
                    }

                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Khai báo không thành công.Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = tkmd.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = tkmd.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void NhanPhanLuong()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            string password = "";
            MsgSend sendXML = new MsgSend();
            WSForm wsForm = new WSForm();
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                //if (sendXML.Load())
                //{
                //    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                //    MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                //    return;
                //}
            }
            else
            {
                MLMessages("Chưa chọn thông tin nhận trạng thái.", "MSG_REC01", "", false);
                return;
            }
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                if (tkmd.MaLoaiHinh.IndexOf("SX") > 0)
                    //xmlCurrent = tkmd.WSRequestXML(password);
                    xmlCurrent = tkmd.WSRequestPhanLuong(password);

                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = tkmd.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = tkmd.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            ShowMessage("Có lỗi trong nhận thông tin : " + msg[0], false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        ShowMessage("Xảy ra lỗi không xác định.", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //------------------

        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                //if (sendXML.Load())
                //{
                //    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                //    MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                //    return;
                //}
            }
            else
            {
                // ShowMessage("Chưa chọn thông tin nhận trạng thái.", false);
                MLMessages("Chưa chọn thông tin để gửi.", "MSG_PUB01", "", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                if (MainForm.versionHD != 2)
                {
                    try
                    {
                        string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                        if (st != "")
                        {
                            ShowMessage(st, false);
                            this.Cursor = Cursors.Default;
                            return;
                        }

                    }
                    catch { }
                }
                tkmd.LoadHMDCollection();
                tkmd.LoadChungTuTKCollection();
                if (tkmd.HMDCollection.Count == 0)
                {
                    //ShowMessage("Danh sách hàng hóa rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    MLMessages("Danh sách hàng hóa rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }
                if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                {
                    xmlCurrent = tkmd.WSSendXMLNHAP(password);
                }
                else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                {
                    xmlCurrent = tkmd.WSSendXMLXuat(password, GlobalSettings.MaMID);
                }

                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;


                // Thực hiện kiểm tra.                   
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Khai báo không thành công.Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = tkmd.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = tkmd.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.KHAI_BAO;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : \n" + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {

                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Hủy thông tin đã đăng ký.
        /// </summary>
        private void cancel()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            string password = "";
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                if (sendXML.Load())
                {
                    //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                    //return;
                }
            }
            else
            {
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                    xmlCurrent = tkmd.WSCancelXMLNhap(password);
                else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                    xmlCurrent = tkmd.WSCancelXMLXuat(password);
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password); ;

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = tkmd.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = tkmd.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void uiCommandBar1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void khaibaoCTMenu_Click(object sender, EventArgs e)
        {
            send();
        }

        private void NhanDuLieuCTMenu_Click(object sender, EventArgs e)
        {
            download();
        }

        private void HuyCTMenu_Click(object sender, EventArgs e)
        {
            cancel();
        }

        private void saoChepTK_Click(object sender, EventArgs e)
        {
            SaoChepToKhaiMD();
        }

        private void saoChepHH_Click(object sender, EventArgs e)
        {
            SaoChepALLHang();
        }

        private void downloadItemsSelect()
        {
            //try
            //{
            //    WSForm wsForm = new WSForm();
            //    wsForm.ShowDialog(this);
            //    if (!wsForm.IsReady) return;
            //    bool ok = true;
            //    int k = 0;
            //    int item = 0;
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        if (i.RowType == RowType.Record)
            //        {
            //            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
            //            tkmd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //            try
            //            {
            //                k++;
            //                item++;                          
            //                {
            //                    xmlCurrent= tkmd.WSRequestXML(wsForm.txtMatKhau.Text.Trim());
            //                }
            //                if (xmlCurrent == "")
            //                {
            //                    ;
            //                }
            //                else
            //                {
            //                    MsgSend sendXML = new MsgSend();
            //                    sendXML.LoaiHS = "TK";
            //                    sendXML.master_id = tkmd.ID;
            //                    sendXML.msg = xmlCurrent;
            //                    sendXML.func = 2;
            //                    xmlCurrent = "";
            //                    sendXML.Insert();
            //                }      

            //            }
            //            catch (Exception ex)
            //            {
            //                this.Cursor = Cursors.Default;
            //                ok = false;
            //                item--;
            //                string message = "";
            //                if (k == items.Count)
            //                    message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " không nhận dữ liệu về được?\nBạn có muốn đưa vào hàng đợi không";
            //                else
            //                    message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " không nhận dữ liệu về được?\nBạn có muốn đưa vào hàng đợi và \ntiếp tục nhận dữ liệu của các bản ghi tiếp không";
            //                string st = ShowMessage(message, true);
            //                if (st != "Yes")
            //                    break;
            //                else
            //                {
            //                    if (k < items.Count)
            //                        ok = true;
            //                    else
            //                        ok = false;
            //                    Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
            //                    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
            //                    hd.TrangThai = TrangThaiXuLy.CHO_DUYET;
            //                    hd.ChucNang = ChucNang.NHAN_THONG_TIN;
            //                    hd.PassWord = wsForm.txtMatKhau.Text.Trim();
            //                    hd.ID = tkmd.ID;                               
            //                    MainForm.AddToQueueForm(hd);
            //                    MainForm.ShowQueueForm();
            //                }
            //                StreamWriter write = File.AppendText("Error.txt");
            //                write.WriteLine("--------------------------------");
            //                write.WriteLine("Lỗi khi nhận dữ liệu danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //                write.WriteLine(ex.StackTrace);
            //                write.WriteLine("Lỗi là : ");
            //                write.WriteLine(ex.Message);
            //                write.WriteLine("--------------------------------");
            //                write.Flush();
            //                write.Close();


            //            }

            //        }
            //    }
            //    this.Cursor = Cursors.Default;
            //    if (ok)
            //    {
            //        if (item > 0)
            //            ShowMessage("Nhận dữ liệu thành công " + item.ToString() + " tờ khai", false);
            //        else
            //            ShowMessage("Không nhận dữ liệu tờ khai nào cả", false);
            //    }
            //    this.search();
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    ShowMessage(ex.Message, false);
            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi khi nhận dữ liệu khai báo danh sách tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            FormSendToKhai f = new FormSendToKhai();
            f.btnNhan.Enabled = true;
            f.ShowDialog();
            this.search();
        }

        private void sendItemsSelect()
        {
            //try
            //{
            //    WSForm wsForm = new WSForm();
            //    wsForm.ShowDialog(this);
            //    if (!wsForm.IsReady) return;
            //    bool ok = true;
            //    int k = 0;
            //    int itemOK = 0;
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        if (i.RowType == RowType.Record)
            //        {
            //            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
            //            tkmd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //            tkmd.LoadHMDCollection();
            //            tkmd.LoadChungTuTKCollection();
            //            try
            //            {
            //                k++;
            //                itemOK++;                          
            //                {

            //                    if (tkmd.MaLoaiHinh.StartsWith("NSX"))
            //                    {
            //                        if (tkmd.SoTiepNhan == 0)
            //                           xmlCurrent= tkmd.WSSendXMLNHAP(wsForm.txtMatKhau.Text.Trim());
            //                        else
            //                           xmlCurrent = tkmd.WSUpdateXMLNHAP(wsForm.txtMatKhau.Text.Trim());
            //                    }
            //                    else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
            //                    {

            //                        if (tkmd.SoTiepNhan == 0)
            //                            xmlCurrent = tkmd.WSSendXMLXuat(wsForm.txtMatKhau.Text.Trim());
            //                        else
            //                            xmlCurrent = tkmd.WSUpdateXMLXUAT(wsForm.txtMatKhau.Text.Trim());
            //                    }
            //                    if (xmlCurrent == "")
            //                    {
            //                        ;
            //                    }
            //                    else
            //                    {
            //                        MsgSend sendXML = new MsgSend();
            //                        sendXML.LoaiHS = "TK";
            //                        sendXML.master_id = tkmd.ID;
            //                        sendXML.msg = xmlCurrent;
            //                        sendXML.func = 1;
            //                        xmlCurrent = "";
            //                        sendXML.Insert();
            //                    }      
            //                }                            
            //            }
            //            catch (Exception ex)
            //            {
            //                itemOK--;
            //                this.Cursor = Cursors.Default;
            //                ok = false;
            //                string message = "";
            //                if (k == items.Count)
            //                {
            //                    message = "Tờ khai thứ " + k.ToString() + " không khai báo được?\nBạn có muốn đưa vào hàng đợi không";                               
            //                }
            //                else
            //                {
            //                    message = "Tờ khai thứ " + k.ToString() + " không khai báo được?\nBạn có muốn đưa vào hàng đợi và \ntiếp tục gửi dữ liệu của các tờ khai tiếp không";                                
            //                }
            //                string st = ShowMessage(message, true);
            //                if (st != "Yes")
            //                    break;
            //                else
            //                {
            //                    if (k < items.Count)
            //                        ok = true;
            //                    else
            //                        ok = false;
            //                    Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
            //                    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
            //                    hd.TrangThai = TrangThaiXuLy.CHO_DUYET;
            //                    hd.ChucNang = ChucNang.KHAI_BAO;
            //                    hd.PassWord = wsForm.txtMatKhau.Text.Trim();
            //                    hd.ID = tkmd.ID;
            //                    MainForm.AddToQueueForm(hd);
            //                    MainForm.ShowQueueForm();
            //                }
            //                StreamWriter write = File.AppendText("Error.txt");
            //                write.WriteLine("--------------------------------");
            //                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //                write.WriteLine(ex.StackTrace);
            //                write.WriteLine("Lỗi là : ");
            //                write.WriteLine(ex.Message);
            //                write.WriteLine("--------------------------------");
            //                write.Flush();
            //                write.Close();

            //            }

            //        }
            //    }
            //    this.Cursor = Cursors.Default;
            //    if (ok)
            //    {
            //        if (itemOK > 0)
            //            ShowMessage("Khai báo thành công " + itemOK.ToString() + " tờ khai", false);
            //        else
            //            ShowMessage("Không khai báo được dữ liệu của tờ khai nào cả ! ", false);
            //    }
            //    else
            //    {
            //        if (itemOK > 0)
            //            ShowMessage("Khai báo thành công " + itemOK.ToString() + " tờ khai", false);
            //    }
            //    this.search();
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    ShowMessage(ex.Message, false);
            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi khi khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            if (MainForm.versionHD != 2)
            {
                try
                {
                    string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                    if (st != "")
                    {
                        ShowMessage(st, false);
                        this.Cursor = Cursors.Default;
                        return;
                    }

                }
                catch { }
            }
            FormSendToKhai f = new FormSendToKhai();
            f.btnSend.Enabled = true;
            f.ShowDialog();
            this.search();
        }

        private void cancelItemsSelect()
        {
            //try
            //{
            //    WSForm wsForm = new WSForm();
            //    wsForm.ShowDialog(this);
            //    if (!wsForm.IsReady) return;
            //    bool ok = true;
            //    int k = 0;
            //    int itemOK = 0;
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        this.Cursor = Cursors.WaitCursor;
            //        if (i.RowType == RowType.Record)
            //        {
            //            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
            //            tkmd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;                        
            //            try
            //            {
            //                k++;
            //                itemOK++;                          
            //                {
            //                    if (tkmd.MaLoaiHinh.StartsWith("NSX"))
            //                      xmlCurrent=  tkmd.WSCancelXMLNhap(wsForm.txtMatKhau.Text.Trim());
            //                    else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
            //                      xmlCurrent = tkmd.WSCancelXMLXuat(wsForm.txtMatKhau.Text.Trim());
            //                  if (xmlCurrent == "")
            //                  {
            //                      ;
            //                  }
            //                  else
            //                  {                                  
            //                      MsgSend sendXML = new MsgSend();
            //                      sendXML.LoaiHS = "TK";
            //                      sendXML.master_id = tkmd.ID;
            //                      sendXML.msg = xmlCurrent;
            //                      sendXML.func = 3;
            //                      xmlCurrent = "";
            //                      sendXML.Insert();
            //                  }      
            //                }                             
            //            }
            //            catch (Exception ex)
            //            {
            //                itemOK--;
            //                this.Cursor = Cursors.Default;
            //                ok = false;
            //                string message = "";
            //                if (k == items.Count)
            //                {
            //                    message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " không hủy khai báo được?\nBạn có muốn đưa vào hàng đợi không";                               
            //                }
            //                else
            //                {
            //                    message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " hủy khai báo được ?\nBạn có muốn đưa vào hàng đợi và \ntiếp tục hủy các tờ khai tiếp không";                                
            //                }
            //                string st = ShowMessage(message, true);
            //                if (st != "Yes")
            //                    break;
            //                else
            //                {
            //                    if (k < items.Count)
            //                        ok = true;
            //                    else
            //                        ok = false;
            //                    Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
            //                    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
            //                    hd.TrangThai = tkmd.TrangThaiXuLy;
            //                    hd.ChucNang = ChucNang.HUY_KHAI_BAO;
            //                    hd.PassWord = wsForm.txtMatKhau.Text.Trim();
            //                    hd.ID = tkmd.ID;
            //                    MainForm.AddToQueueForm(hd);
            //                    MainForm.ShowQueueForm();
            //                }
            //                StreamWriter write = File.AppendText("Error.txt");
            //                write.WriteLine("--------------------------------");
            //                write.WriteLine("Lỗi khi Hủy khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //                write.WriteLine(ex.StackTrace);
            //                write.WriteLine("Lỗi là : ");
            //                write.WriteLine(ex.Message);
            //                write.WriteLine("--------------------------------");
            //                write.Flush();
            //                write.Close();


            //            }

            //        }
            //    }
            //    this.Cursor = Cursors.Default;
            //    if (ok)
            //    {
            //        if (itemOK > 0)
            //            ShowMessage("Hủy khai báo thành công " + itemOK.ToString() + " tờ khai", false);
            //        else
            //            ShowMessage("Không hủy được khai báo của tờ khai nào cả ! ", false);
            //    }
            //    else
            //    {
            //        if (itemOK > 0)
            //            ShowMessage("Hủy khai báo thành công " + itemOK.ToString() + " tờ khai", false);                                            
            //    }
            //    this.search();
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    ShowMessage(ex.Message, false);
            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi khi hủy dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
            FormSendToKhai f = new FormSendToKhai();
            f.btnHuy.Enabled = true;
            f.ShowDialog();
            this.search();
        }

        private void uiContextMenu1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count > 0)
            {
                if (ShowMessage("Bạn có muốn xóa các tờ khai này không?", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "TK";
                            sendXML.master_id = tkmd.ID;
                            if (sendXML.Load())
                            {
                                //ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
                                MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_NPL03", "" + i.Position + "", false);
                            }
                            else
                            {
                                if (tkmd.SoTiepNhan > 0 || tkmd.SoToKhai > 0)
                                    MLMessages("Tờ khai ID = " + tkmd.ID + "đã khai báo, không được phép xóa.", "", "", false);
                                else
                                    tkmd.Delete();
                            }
                        }
                    }
                }
                else
                    e.Cancel = true;
            }
        }

        //private void ToKhaiMenuItem_Click(object sender, EventArgs e)
        //{
        //    if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
        //        return;
        //    ToKhaiMauDich tkmd = new ToKhaiMauDich();
        //    tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
        //    tkmd.Load();
        //    tkmd.LoadHMDCollection();
        //    tkmd.LoadChungTuTKCollection();
        //    switch (tkmd.MaLoaiHinh.Substring(0, 3))
        //    {
        //        case "NSX":
        //            ReportViewTKNForm f = new ReportViewTKNForm();
        //            f.TKMD = tkmd;
        //            f.Show();
        //            break;

        //        case "XSX":
        //            ReportViewTKXForm f1 = new ReportViewTKXForm();
        //            f1.TKMD = tkmd;
        //            f1.Show();
        //            break;
        //    }

        //}

        private void SaoChepCha_Click(object sender, EventArgs e)
        {

        }

        private void xacnhanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaySoTiepNhanDT();
        }
        private void XacNhanThongTin()
        {
            FormSendToKhai f = new FormSendToKhai();
            f.btnXacNhan.Enabled = true;
            f.ShowDialog();
            this.search();
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count > 0)
            {
                //if (ShowMessage("Bạn có muốn xóa các tờ khai này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa các tờ khai này không?", "MSG_DEL01", "", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "TK";
                            sendXML.master_id = tkmd.ID;
                            if (sendXML.Load())
                            {
                                // ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
                                MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_NPL03", "" + i.Position + "", false);
                            }
                            else
                            {
                                if (tkmd.SoTiepNhan > 0 || tkmd.SoToKhai > 0)
                                    MLMessages("Tờ khai ID = " + tkmd.ID + "đã khai báo, không được phép xóa.", "", "", false);
                                else
                                    tkmd.Delete();

                                //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                                try
                                {
                                    string where = "1 = 1";
                                    where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", tkmd.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                                    List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                                    if (listLog.Count > 0)
                                    {
                                        long idLog = listLog[0].IDLog;
                                        string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                        long idDK = listLog[0].ID_DK;
                                        string guidstr = listLog[0].GUIDSTR_DK;
                                        string userKhaiBao = listLog[0].UserNameKhaiBao;
                                        DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                        string userSuaDoi = GlobalSettings.UserLog;
                                        DateTime ngaySuaDoi = DateTime.Now;
                                        string ghiChu = listLog[0].GhiChu;
                                        bool isDelete = true;
                                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                                    userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                                    return;
                                }
                            }
                        }
                    }
                    this.search();
                }
            }
        }

        private void xóaTờKhaiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count > 0)
                {
                    // if (ShowMessage("Bạn có muốn xóa các tờ khai này không?", true) == "Yes")
                    if (MLMessages("Bạn có muốn xóa các tờ khai này không?", "MSG_DEL01", "", true) == "Yes")
                    {

                        foreach (GridEXSelectedItem i in items)
                        {
                            if (i.RowType == RowType.Record)
                            {
                                ToKhaiMauDich tkmd = (ToKhaiMauDich)i.GetRow().DataRow;
                                MsgSend sendXML = new MsgSend();
                                sendXML.LoaiHS = "TK";
                                sendXML.master_id = tkmd.ID;
                                if (sendXML.Load())
                                {
                                    //  ShowMessage("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
                                    MLMessages("Danh sách thứ " + i.Position + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_NPL03", "" + i.Position + "", false);
                                }
                                else
                                {
                                    if (tkmd.SoTiepNhan > 0 || tkmd.SoToKhai > 0)
                                        MLMessages("Tờ khai ID = " + tkmd.ID + "đã khai báo, không được phép xóa.", "", "", false);
                                    else
                                    {
                                        tkmd.CloneToDB(null);
                                        tkmd.Delete();
                                    }
                                }
                            }
                        }
                    }
                    this.search();
                }
            }
            catch (Exception ex) { throw ex; }
        }

        private void mnuCSDaDuyet_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangThai();
        }

        private void ToKhaiMenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            switch (tkmd.MaLoaiHinh.Substring(0, 3))
            {
                case "NSX":
                    ReportViewTKNForm f = new ReportViewTKNForm();
                    f.TKMD = tkmd;
                    f.Show();
                    break;

                case "XSX":
                    ReportViewTKXForm f1 = new ReportViewTKXForm();
                    f1.TKMD = tkmd;
                    f1.Show();
                    break;


            }
        }

        private void PhieuTNMenuItem_Click(object sender, EventArgs e)
        {
            this.inPhieuTN();
        }

        private void ToKhaiA4MenuItem_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            switch (tkmd.MaLoaiHinh.Substring(0, 3))
            {
                case "NSX":
                    ReportViewTKNA4Form f = new ReportViewTKNA4Form();
                    f.TKMD = tkmd;
                    f.Show();
                    break;

                case "XSX":
                    ReportViewTKXA4Form f1 = new ReportViewTKXA4Form();
                    f1.TKMD = tkmd;
                    f1.Show();
                    break;
                default:
                    //ReportViewTKNXTCA4Form f2 = new ReportViewTKNXTCA4Form();
                    //f2.TKMD = tkmd;
                    //f2.Show();
                    break;
            }
        }

        private void mnuPhanLuong_Click(object sender, EventArgs e)
        {
            NhanPhanLuong();
        }

        private void cboPhanLuong_SelectedValueChanged(object sender, EventArgs e)
        {
            this.search();
        }

        private void ToKhaiTQDTMenuItem_Click(object sender, EventArgs e)
        {

            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            HangMauDich hangmd = new HangMauDich();
            tkmd.ID = ((ToKhaiMauDich)dgList.GetRow().DataRow).ID;
            tkmd.Load();
            tkmd.LoadHMDCollection();
            tkmd.LoadChungTuTKCollection();
            if (tkmd.MaLoaiHinh.StartsWith("N"))
            {
                ReportViewTKNTQDTForm f = new ReportViewTKNTQDTForm();
                f.TKMD = tkmd;
                f.ShowDialog();
            }
            else
            {
                ReportViewTKXTQDTForm f = new ReportViewTKXTQDTForm();
                f.TKMD = tkmd;
                f.ShowDialog();
            }


        }

        private void mnuHuongDan_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            if (tkmd.HUONGDAN != "")
            {
                ShowMessageTQDT("Hướng dẫn làm thủ tục Hải quan", tkmd.HUONGDAN.ToString(), false);

            }
            else
            {
                ShowMessageTQDT("Hướng dẫn làm thủ tục Hải quan", "Chưa có hướng dẫn của hải quan", false);
            }
        }

        private void mniInBangKe_Click(object sender, EventArgs e)
        {
            InContainer();
        }

        private void InContainer()
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            }

            if (tkmd.ID == 0)
                return;

            Company.Interface.Report.SXXK.BangkeSoContainer f = new Company.Interface.Report.SXXK.BangkeSoContainer();
            f.tkmd = tkmd;
            f.BindReport();
            f.ShowPreview();
        }

        #region TRANSFER dữ liệu TQDT sang KTX
        /*HUNGTQ, Update 23/06/2010*/

        /// <summary>
        /// Thiet lap su kien Click cho nut Dong bo du lieu...
        /// </summary>
        private void SetTransferEvent()
        {
            btnTransfer.Click += new EventHandler(btnTransfer_Click);
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = ValidateConfigConnectSXXK();
                if (connectionString == "")
                {
                    Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Chưa có cấu hình kết nối đến cơ sở dữ liệu khai từ xa 'SXXK'.", false);
                    return;
                }

                Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu dongBo = new Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu();

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.GroupHeader)
                    {
                        Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Chưa chọn thông tin tờ khai.", false);
                        break;
                    }

                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiMauDich tkmdDongBo = (ToKhaiMauDich)i.GetRow().DataRow;

                        //Kiem tra trang thai to khai da duoc dong bo chua?.                        
                        dongBo = Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu.Load(tkmdDongBo.ID, tkmdDongBo.SoToKhai, tkmdDongBo.NamDK, tkmdDongBo.MaHaiQuan, tkmdDongBo.MaDoanhNghiep);
                        if (dongBo != null)
                        {
                            if (dongBo.DaDongBo == true)
                            {
                                Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Tờ khai có số ID: " + tkmdDongBo.ID + "\r\nSố tờ khai: " + tkmdDongBo.SoToKhai + "\r\nNăm đăng ký: " + tkmdDongBo.NamDK + " \r\nđã được đồng bộ. Bạn hãy chọn tờ khai khác.", false);
                                return;
                            }
                        }

                        if (tkmdDongBo.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Tờ khai có số ID: " + tkmdDongBo.ID + " chưa duyệt, không thể đồng bộ dữ liệu.\r\nBạn hãy chọn tờ khai khác.", false);
                            return;
                        }
                        else if (tkmdDongBo.SoToKhai == 0)
                        {
                            Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Tờ khai có số ID: " + tkmdDongBo.ID + " chưa cấp 'Số tờ khai', không thể đồng bộ dữ liệu.\r\nBạn hãy chọn tờ khai khác.", false);
                            return;
                        }

                        //Thuc hien dong bo du lieu cho to khai
                        tkmdDongBo.TransgferDataToSXXK(connectionString, tkmdDongBo);

                        if (dongBo == null)
                            dongBo = new Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu();
                        dongBo.MaHaiQuan = tkmdDongBo.MaHaiQuan;
                        dongBo.MaDoanhNghiep = tkmdDongBo.MaDoanhNghiep;
                        dongBo.DaDongBo = true;
                        dongBo.TKMD_ID = tkmdDongBo.ID;
                        dongBo.SoToKhai = tkmdDongBo.SoToKhai;
                        dongBo.NamDK = tkmdDongBo.NamDK;
                        dongBo.InsertUpdate();

                        Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Đã đồng bộ dữ liệu thành công tờ khai:\r\nSố tờ khai: " + tkmdDongBo.SoToKhai + "\r\nNăm đăng ký: " + tkmdDongBo.NamDK, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Lỗi trong quá trình thực hiện:\r\n" + ex.Message + "\nChi tiết:" + ex.StackTrace, false);
            }
        }

        /// <summary>
        /// Kiem tra thiet lap cau hinh ket noi den co so du lieu khai tu xa.
        /// </summary>
        /// <returns></returns>
        private string ValidateConfigConnectSXXK()
        {
            string configName = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\eDeclaration2007.exe";
            System.Configuration.Configuration cm = System.Configuration.ConfigurationManager.OpenExeConfiguration(configName);

            string connectionString = "";
            try
            {
                if (cm.ConnectionStrings.ConnectionStrings["SXXK"] != null)
                {
                    connectionString = cm.ConnectionStrings.ConnectionStrings["SXXK"].ConnectionString;
                }
            }
            catch (Exception ex) { return ""; }

            return connectionString;
        }

        #endregion

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Lay thong tin to khai mau dich duoc chon tren luoi
            if (dgList.SelectedItems.Count > 0)
            {
                ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        ToKhaiMauDich obj = (ToKhaiMauDich)grItem.GetRow().DataRow;

                        //Hien thij menu 'Chuyen trang thai' neu to khai da co So tiep nhan.
                        mnuCSDaDuyet.Visible = true;
                        mnuCSDaDuyet.Enabled = (obj.TrangThaiXuLy != 1
                           && obj.TrangThaiXuLy != TrangThaiXuLy.KHONG_PHE_DUYET
                           && obj.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET_DASUACHUA);

                        xóaTờKhaiToolStripMenuItem.Enabled = (obj.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);
                    }
                }
            }
        }

        private void contextMenuStrip1_MouseEnter(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void mnuSuaTK_Click(object sender, EventArgs e)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            tkmd.LoadHMDCollection();
            string msg = "Bạn có muốn chuyển trạng thái của tờ khai đã duyệt sang tờ khai sửa không?";
            msg += "\n\nSố tờ khai: " + tkmd.SoToKhai.ToString();
            msg += "\n----------------------";
            msg += "\nCó " + tkmd.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
            {
                long id = tkmd.ID;
                tkmd.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                tkmd.Update();
                ToKhaiMauDichForm f = new ToKhaiMauDichForm();
                f.OpenType = OpenFormType.Edit;
                f.TKMD = this.getTKMDByID(id);
                f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }

        private void mnuHuyTK_Click(object sender, EventArgs e)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            tkmd.LoadHMDCollection();

            string msg = "Bạn có muốn hủy tờ khai đã duyệt này không?";
            msg += "\n\nSố tờ khai: " + tkmd.SoToKhai.ToString();
            msg += "\n----------------------";
            msg += "\nCó " + tkmd.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    HuyToKhaiForm f = new HuyToKhaiForm();
                    f.TKMD = tkmd;
                    f.ShowDialog();
                }
            }
            else
            {
                HuyToKhaiForm f = new HuyToKhaiForm();
                f.TKMD = tkmd;
                f.ShowDialog();
            }
        }

        private void dtTuNgay_ValueChanged(object sender, EventArgs e)
        {
            DateTime tuNgay = dtTuNgay.Value;
            DateTime denNgay = DateTime.Now;
            if (tuNgay > denNgay)
            {
                ShowMessage("Có lỗi: Khoảng thời gian không hợp lệ.\t\nVui lòng chọn lại khoảng thời gian thích hợp", false);
                return;
            }
        }

        private void dtDenNgay_ValueChanged(object sender, EventArgs e)
        {
            DateTime tuNgay = dtTuNgay.Value;
            DateTime denNgay = dtDenNgay.Value;
            if (tuNgay > denNgay)
            {
                ShowMessage("Có lỗi: Khoảng thời gian không hợp lệ.\t\nVui lòng chọn lại khoảng thời gian thích hợp", false);
                return;
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            int stt = 1;
            //Lấy đường dẫn file gốc
            string destFile = "";
            string fileName = "Danh_Sach_TK.xls";
            string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            try
            {
                Workbook workBook = Workbook.Load(sourcePath);
                Worksheet workSheet = workBook.Worksheets[0];

                int row = 2;
                //int col = 0;
                foreach (ToKhaiMauDich TKMD in tkmdCollection)
                {
                    workSheet.Rows[row].Cells[0].Value = stt;
                    workSheet.Rows[row].Cells[0].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[0].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[1].Value = TKMD.ID.ToString();
                    workSheet.Rows[row].Cells[1].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[1].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[2].Value = TKMD.SoTiepNhan.ToString();
                    workSheet.Rows[row].Cells[2].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[2].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[3].Value = TKMD.NgayTiepNhan.ToShortDateString();
                    workSheet.Rows[row].Cells[3].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[3].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[4].Value = TKMD.SoToKhai.ToString();
                    workSheet.Rows[row].Cells[4].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[4].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[5].Value = TKMD.NgayDangKy.ToShortDateString();
                    workSheet.Rows[row].Cells[5].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[5].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[6].Value = TKMD.SoHopDong;
                    workSheet.Rows[row].Cells[6].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[6].CellFormat.Font.Height = 10 * 20;
                    workSheet.Rows[row].Cells[5].CellFormat.Alignment = HorizontalCellAlignment.Fill;
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        workSheet.Rows[row].Cells[7].Value = "VN";
                        workSheet.Rows[row].Cells[7].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[row].Cells[7].CellFormat.Font.Height = 10 * 20;

                        workSheet.Rows[row].Cells[8].Value = TKMD.NuocXK_ID;
                        workSheet.Rows[row].Cells[8].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[row].Cells[8].CellFormat.Font.Height = 10 * 20;
                    }
                    else
                    {
                        workSheet.Rows[row].Cells[7].Value = TKMD.NuocNK_ID;
                        workSheet.Rows[row].Cells[7].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[row].Cells[7].CellFormat.Font.Height = 10 * 20;

                        workSheet.Rows[row].Cells[8].Value = "VN";
                        workSheet.Rows[row].Cells[8].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[row].Cells[8].CellFormat.Font.Height = 10 * 20;
                    }

                    workSheet.Rows[row].Cells[9].Value = cbStatus.SelectedItem.Text.Trim();
                    workSheet.Rows[row].Cells[9].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[9].CellFormat.Font.Height = 10 * 20;

                    string phanluong = "";
                    if (TKMD.PhanLuong.Equals(Company.KDT.SHARE.Components.TrangThaiPhanLuong.LUONG_XANH))
                        phanluong = "Luồng xanh";
                    else if (TKMD.PhanLuong.Equals(Company.KDT.SHARE.Components.TrangThaiPhanLuong.LUONG_VANG))
                        phanluong = "Luồng vàng";
                    else
                        phanluong = "Luồng đỏ";
                    workSheet.Rows[row].Cells[10].Value = phanluong;
                    workSheet.Rows[row].Cells[10].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[10].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[row].Cells[11].Value = TKMD.HUONGDAN;
                    workSheet.Rows[row].Cells[11].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[11].CellFormat.Font.Height = 10 * 20;
                    //TODO:Cao Hữu Tú: updated 08-09-2011
                    //contents: format lại nội dung của ô nếu nội dung quá dài thì xuống hàng mới
                    workSheet.Rows[row].Cells[11].CellFormat.VerticalAlignment = VerticalCellAlignment.Justify;


                    row++;
                    stt++;
                }

                //Chọn đường dẫn file cần lưu
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                string dateNow = DateTime.Now.ToShortDateString().Replace("/", "");
                if (chkDate.Checked)
                    saveFileDialog1.FileName = "Danh_Sach_TK_TuNgay_" + dtTuNgay.Value.ToString("yyyy-MM-dd") + "_DenNgay_" + dtDenNgay.Value.ToString("yyyy-MM-dd");
                else
                    saveFileDialog1.FileName = "Danh_Sach_TK_" + System.DateTime.Now.ToString("yyyy-MM-dd");

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    destFile = saveFileDialog1.FileName;
                    //Ghi nội dung file gốc vào file cần lưu
                    try
                    {
                        byte[] sourceFile = Globals.ReadFile(sourcePath);
                        Globals.WriteFile(destFile, sourceFile);
                        workBook.Save(destFile);
                        ShowMessage("Save file thành công", false);
                        System.Diagnostics.Process.Start(destFile);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi: Không thể xuất Excel.\t\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }

        private void chkDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDate.Checked)
            {
                lblTuNgay.Enabled = true;
                dtTuNgay.Enabled = true;
                lblDenNgay.Enabled = true;
                dtDenNgay.Enabled = true;
                btnExportExcel.Enabled = true;
            }
            else
            {
                lblTuNgay.Enabled = false;
                dtTuNgay.Enabled = false;
                lblDenNgay.Enabled = false;
                dtDenNgay.Enabled = false;
                btnExportExcel.Enabled = true;
            }
        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() == null) return;
            ToKhaiMauDich tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
            Globals.KetQuaXuLy(tkmd);
        }

        private void mniCapNhatToKhaiSua_Click(object sender, EventArgs e)
        {
            try
            {
                //Lay thong tin to khai mau dich duoc chon tren luoi
                if (dgList.SelectedItems.Count > 0)
                {
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        if (grItem.RowType == RowType.Record)
                        {
                            ToKhaiMauDich obj = (ToKhaiMauDich)grItem.GetRow().DataRow;

                            if (obj.CapNhatThongTinHangToKhaiSua())
                            {
                                string trangThaiCu = "Sửa tờ khai";//Company.KDT.SHARE.Components.Globals.GetTrangThaiXuLyTK(obj.TrangThaiXuLy);

                                obj.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                obj.Update();

                                Company.KDT.SHARE.Components.MessageTypes msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap;
                                if (obj.MaLoaiHinh.Substring(0, 1) == "X")
                                    msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat;

                                Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty, obj.ID, obj.GUIDSTR,
                                    msgType,
                                    Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                                    Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay + string.Format(" từ trạng thái {0} -> {1}", trangThaiCu, Company.KDT.SHARE.Components.Globals.GetTrangThaiXuLyTK(obj.TrangThaiXuLy)),
                                    string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}", obj.SoToKhai, obj.NgayDangKy.ToShortDateString(), obj.MaLoaiHinh.Trim(), obj.MaHaiQuan.Trim(), Company.KDT.SHARE.Components.Globals.GetPhanLuong(obj.PhanLuong)));

                                ShowMessage("Thực hiện cập nhật dữ liệu tờ khai sửa, chuyển trạng thái thành công.", false, false, "");
                            }
                            else
                                ShowMessage("Lỗi thực hiện cập nhật dữ liệu tờ khai sửa.", false, false, "");
                        }
                    }

                    search();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}
