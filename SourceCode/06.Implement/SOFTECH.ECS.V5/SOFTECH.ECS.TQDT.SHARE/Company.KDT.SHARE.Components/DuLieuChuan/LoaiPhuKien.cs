using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class LoaiPhuKien : BaseClass
    {
        public static DataTable SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_LoaiPhuKien";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static string GetName(string id)
        {
            //string query = string.Format("SELECT Ten FROM t_HaiQuan_LoaiPhuKien WHERE [ID_LoaiPhuKien] = '{0}'", id);
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //IDataReader reader = db.ExecuteReader(dbCommand);
            //if (reader.Read())
            //{
            //    return reader["TenLoaiPhuKien"].ToString();
            //}
            //return string.Empty;

            try
            {
                string query = string.Format("[ID_LoaiPhuKien] = '{0}'", id.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["LoaiPhuKien"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["TenLoaiPhuKien"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }
    }
}
