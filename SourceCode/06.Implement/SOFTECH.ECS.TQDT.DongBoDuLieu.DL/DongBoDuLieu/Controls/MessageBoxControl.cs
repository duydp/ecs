using System;
using System.Windows.Forms;
using DongBoDuLieu;

namespace DongBoDuLieu.Controls
{
    public partial class MessageBoxControl : BaseForm
    {
        public string ReturnValue = "Cancel";
        public bool ShowYesNoButton
        {
            set 
            { 
                this.btnNo.Visible = this.btnYes.Visible = value;
                this.btnCancel.Visible = !value;
                if (!value) this.CancelButton = btnCancel;
                else this.CancelButton = btnNo;
            }
        }

        public string MessageString
        {
            set { this.txtMessage.Text = value; }
        }

        public MessageBoxControl()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "Yes";
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "No";
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MessageBoxControl_Load(object sender, EventArgs e)
        {

        }
    }
}