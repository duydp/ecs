﻿using System;
using System.Xml;
using System.Windows.Forms;
#if KD_V3
using Company.KD.BLL.SXXK;
using Company.KD.BLL.SXXK.ToKhai;
#elif SXXK_V3
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
#elif GV_V3
using Company.GC.BLL.SXXK;
using Company.GC.BLL.SXXK.ToKhai;
#endif
using System.Threading;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Sockets;

namespace Company.Interface
{
    public partial class ThongTinDNAndHQForm : BaseForm
    {
        string user = "";
        string pw = "";

        public static int versionHD = 0;

        public ThongTinDNAndHQForm()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\Config.xml");
                XmlNode node = doc.SelectSingleNode("Root/Type");
                versionHD = Convert.ToInt32(node.InnerText);
                //Chi hien thi Tab DBDL cho phia Doanh nghiep - Server
                uiTabDBDL.TabVisible = !GlobalSettings.IsDaiLy;
                cbFont.SelectedValue = Company.KDT.SHARE.Components.Globals.FontName;
                string MaDN = GlobalSettings.MA_DON_VI;
                Company.KDT.SHARE.Components.HeThongPhongKhai htpk = new Company.KDT.SHARE.Components.HeThongPhongKhai();

                if (versionHD == 1) //La DAI LY
                {
                    try
                    {
                        //Thông tin doanh nghiệp
                        txtMaDN.ReadOnly = true;
                        txtMaDN.Text = MaDN;
                        txtTenDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "CauHinh").TenDoanhNghiep.ToString();
                        txtDiaChi.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "DIA_CHI").Value_Config.ToString();
                        txtDienThoaiDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "ĐIEN_THOAI").Value_Config.ToString();
                        txtSoFaxDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "SO_FAX").Value_Config.ToString();
                        txtNguoiLienHeDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "NGUOI_LIEN_HE").Value_Config.ToString();
                        txtChucVu.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "CHUC_VU").Value_Config.ToString();
                        txtMailDoanhNghiep.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MailDoanhNghiep").Value_Config.ToString();
                        txtMaMid.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MaMID").Value_Config.ToString();

                        //Thông tin hải quan
                        txtMaCuc.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MA_CUC_HAI_QUAN").Value_Config.ToString();
                        donViHaiQuanControl1.Ma = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MA_HAI_QUAN").Value_Config.ToString();
                        txtTenCucHQ.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_HAI_QUAN").Value_Config.ToString();
                        txtTenNganHQ.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_HAI_QUAN_NGAN").Value_Config.ToString();
                        txtMailHaiQuan.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MailHaiQuan").Value_Config.ToString();

                        //Thông tin kết nối service hải quan
                        txtDiaChiHQ.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "DIA_CHI_HQ").Value_Config.ToString();
                        txtTenDichVu.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_DICH_VU").Value_Config.ToString();
                        chkCKS.Checked = Convert.ToBoolean(Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "CHU_KY_SO").Value_Config.ToString());
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
                else
                {
                    //Thông tin doanh nghiệp
                    txtMaDN.Text = GlobalSettings.MA_DON_VI;
                    txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                    txtDiaChi.Text = GlobalSettings.DIA_CHI;
                    txtDienThoaiDN.Text = GlobalSettings.SoDienThoaiDN;
                    txtSoFaxDN.Text = GlobalSettings.SoFaxDN;
                    txtNguoiLienHeDN.Text = GlobalSettings.NguoiLienHe;
                    txtChucVu.Text = GlobalSettings.ChucVu;
                    txtMailDoanhNghiep.Text = GlobalSettings.MailDoanhNghiep;
                    txtMaMid.Text = GlobalSettings.MaMID;
                    //thông tin Hải quan
                    txtMaCuc.Text = GlobalSettings.MA_CUC_HAI_QUAN.Trim();
                    donViHaiQuanControl1.Ma = GlobalSettings.MA_HAI_QUAN.Trim();
                    donViHaiQuanControl1.ReadOnly = false;
                    txtTenCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN;
                    txtTenNganHQ.Text = GlobalSettings.TEN_HAI_QUAN_NGAN;
                    txtMailHaiQuan.Text = GlobalSettings.MailHaiQuan;
                    //Thông tin kết nối service hải quan
                    txtDiaChiHQ.Text = GlobalSettings.DiaChiWS_Host;
                    txtTenDichVu.Text = GlobalSettings.DiaChiWS_Name;
                    chkCKS.Checked = Company.KDT.SHARE.Components.Globals.SuDungHttps;
                }

                txtThongBaoHetHan.Text = GlobalSettings.ThongBaoHetHan.ToString();
                txtSoTienKhoanTKN.Text = GlobalSettings.SoTienKhoanTKN.ToString();
                txtSoTienKhoanTKX.Text = GlobalSettings.SoTienKhoanTKX.ToString();

                //chkOnlyMe.Enabled = GlobalSettings.IsDaiLy;
                //chkOnlyMe.Checked = GlobalSettings.IsOnlyMe;

                CultureInfo cultureVN = new CultureInfo("vi-VN");
                if (Thread.CurrentThread.CurrentCulture.Equals(cultureVN))
                {
                    opVietNam.Checked = true;
                }
                else
                    opTiengAnh.Checked = true;

#if (KD_V3 || GC_V3)
            txtSoTienKhoanTKN.Enabled = false;
            txtSoTienKhoanTKX.Enabled = false;
            txtThongBaoHetHan.Enabled = false;
#elif (KD_V3 || GC_V3 || KD_V4 || GC_V4)
            chkLaDNCX.Enabled = false;
#elif (SXXK_V3 || SXXK_V4)
                chkLaDNCX.Enabled = true;
#endif
                chkLaDNCX.Checked = Company.KDT.SHARE.Components.Globals.LaDNCX;

                //if (!GlobalSettings.IsDaiLy)
                //{
                //    //Cau hinh dai ly
                //    Company.KDT.SHARE.Components.ISyncData myService = Company.KDT.SHARE.Components.WebService.SyncService();
                //    string check = myService.CheckCauHinhDL(GlobalSettings.MA_DON_VI);
                //    GlobalSettings.SOTOKHAI_DONGBO = 0;

                //    if (int.TryParse(check, out GlobalSettings.SOTOKHAI_DONGBO))
                //    {
                //        GlobalSettings.SOTOKHAI_DONGBO = Convert.ToInt32(check);
                //    }
                //    else
                //    {
                //        if (check == "NULL")
                //            ShowMessage("Doanh nghiệp chưa cấu hình giới hạn số tờ khai cho đại lý", false);
                //        else
                //            ShowMessage("Lỗi kết nối kiểm tra cấu hình: " + check, false);
                //        GlobalSettings.ISKHAIBAO = false;
                //    }
                //    txtSoTKDBDL.Text = GlobalSettings.SOTOKHAI_DONGBO.ToString();
                //}

                donViHaiQuanControl1.Leave += new EventHandler(donViHaiQuanControl1_Leave);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Company.Interface.Globals.ValidateNull(donViHaiQuanControl1, errorProvider1, "Mã chi cục Hải quan"))
                {
                    uiTab1.TabPages[0].Selected = true;
                    return;
                }

                containerValidator1.Validate();
                if (!containerValidator1.IsValid)
                {
                    uiTab1.TabPages[1].Selected = true;
                    txtMaDN.Focus();
                    return;
                }


                if (!Company.Interface.Globals.ValidateNull(txtDienThoaiDN, errorProvider1, "Điện thoại"))
                    return;
                if (!Company.Interface.Globals.ValidateNull(txtMailDoanhNghiep, errorProvider1, "Email (Thư điện tử)"))
                    return;
                Company.KDT.SHARE.Components.Globals.FontName = cbFont.SelectedValue.ToString();
                #region OldCode


                //Hungtq 14/01/2011. Luu cau hinh
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DIA_CHI", txtDiaChi.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_DON_VI", txtMaDN.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_HAI_QUAN", donViHaiQuanControl1.Ma.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN", donViHaiQuanControl1.Ten);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN_NGAN", txtTenNganHQ.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DON_VI", txtTenDN.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_CUC_HAI_QUAN", txtMaCuc.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_CUC_HAI_QUAN", txtTenCucHQ.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailHaiQuan", txtMailHaiQuan.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MaMID", txtMaMid.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ThongBaoHetHan", txtThongBaoHetHan.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTienKhoanTKN", txtSoTienKhoanTKN.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTienKhoanTKX", txtSoTienKhoanTKX.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailDoanhNghiep", txtMailDoanhNghiep.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FromMail", txtMailDoanhNghiep.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontName", Company.KDT.SHARE.Components.Globals.FontName);


                #endregion
                if (versionHD == 0)
                {
                    /*DATLMQ update Lưu cấu hình vào file config 18/01/2011.*/
                    XmlDocument doc = new XmlDocument();
                    string path = Company.KDT.SHARE.Components.Globals.GetPathProgram() + "\\ConfigDoanhNghiep";
                    //Hungtq update 28/01/2011.
                    string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

                    doc.Load(fileName);

                    //HUNGTQ Updated 07/06/2011
                    //Set thông tin MaCucHQ
                    GlobalSettings.MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaCucHQ").InnerText = txtMaCuc.Text.Trim();
                    //Set thông tin TenCucHQ
                    GlobalSettings.TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenCucHQ").InnerText = txtTenCucHQ.Text.Trim();
                    //Set thông tin MaChiCucHQ
                    GlobalSettings.MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaChiCucHQ").InnerText = donViHaiQuanControl1.Ma.Trim();
                    //Set thông tin TenChiCucHQ
                    GlobalSettings.TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenChiCucHQ").InnerText = donViHaiQuanControl1.Ten.Trim();
                    //Set thông tin TenNganChiCucHQ
                    GlobalSettings.TEN_HAI_QUAN_NGAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenNganChiCucHQ").InnerText = txtTenNganHQ.Text.Trim();
                    //Set thông tin MailHQ
                    GlobalSettings.MailHaiQuan = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailHQ").InnerText = txtMailHaiQuan.Text.Trim();
                    //Set thông tin MaDoanhNghiep
                    GlobalSettings.MA_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaDoanhNghiep").InnerText = txtMaDN.Text.Trim();
                    //Set thông tin TenDoanhNghiep
                    GlobalSettings.TEN_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenDoanhNghiep").InnerText = txtTenDN.Text.Trim();
                    //Set thông tin DiaChiDoanhNghiep
                    GlobalSettings.DIA_CHI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DiaChiDoanhNghiep").InnerText = txtDiaChi.Text.Trim();
                    //Set thông tin MaMid
                    GlobalSettings.MaMID = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaMID").InnerText = txtMaMid.Text.Trim();
                    //Set thông tin Email doanh nghiep/ ca nhan
                    GlobalSettings.MailDoanhNghiep = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailDoanhNghiep").InnerText = txtMailDoanhNghiep.Text.Trim();
                    //Set thông tin Dien thoai
                    GlobalSettings.SoDienThoaiDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai").InnerText = txtDienThoaiDN.Text.Trim();
                    //Set thông tin Fax
                    GlobalSettings.SoFaxDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax").InnerText = txtSoFaxDN.Text.Trim();
                    //Set thông tin Nguoi lien he
                    GlobalSettings.NguoiLienHe = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "NguoiLienHe").InnerText = txtNguoiLienHeDN.Text.Trim();

                    GlobalSettings.ChucVu = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "ChucVu").InnerText = txtChucVu.Text.Trim();

                    //GlobalSettings.IsOnlyMe = chkOnlyMe.Checked;
                    //Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "OnlyMe").InnerText = chkOnlyMe.Checked.ToString();

                    //-----------------------------------------------------------------------
                    //HungTQ Updated 05/11/2012

                    //if (!URI.Contains("http:"))
                    //URI = "http://" + lblWS.Text.Trim();
                    string URI = URI = lblWS.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaChiWS", URI);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("Company_BLL_WS_KhaiDienTu_KDTService", URI);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("QuanLyChungTu_WS_KDTService", URI);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WS_URL", URI);

                    try
                    {
                        if (txtHost.Text.Trim().Equals("") || txtPort.Text.Trim().Equals(""))
                        {
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsUseProxy", "False");
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Host", "");
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Port", "");
                        }
                        else
                        {
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsUseProxy", "True");
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Host", txtHost.Text);
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Port", txtPort.Text);
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi thiết lập Proxy.\nChi tiết lỗi: " + ex.Message, false);
                        return;
                    }

                    //HUNGTQ Updated 07/06/2011
                    //Set thong tin WS_Host
                    XmlNode nodeWS_Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Host");
                    //if (txtDiaChiHQ.Text.Contains("http:") || txtDiaChiHQ.Text.Contains("https:"))
                    GlobalSettings.DiaChiWS_Host = nodeWS_Host.InnerText = txtDiaChiHQ.Text.Trim();
                    //else
                    //    GlobalSettings.DiaChiWS_Host = nodeWS_Host.InnerText = "http://" + txtDiaChiHQ.Text.Trim();
                    //Set thong tin WS_Name
                    XmlNode nodeWS_Name = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Name");
                    GlobalSettings.DiaChiWS_Name = nodeWS_Name.InnerText = txtTenDichVu.Text.Trim();
                    //Set thong tin WebService
                    XmlNode nodeWebService = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS");
                    GlobalSettings.DiaChiWS = nodeWebService.InnerText = URI;

                    //Hungtq updated 03/04/2012.
                    //Company.KDT.SHARE.Components.Globals.SuDungHttps = chkCKS.Checked;
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SuDungHttps", chkCKS.Checked == true ? "true" : "false");
                    Company.KDT.SHARE.Components.Globals.SuDungHttps = chkCKS.Checked;

                    //Hungtq updated 05/01/2013.
#if (SXXK_V3 || SXXK_V4)
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("LaDNCX", chkLaDNCX.Checked == true ? "true" : "false");
                    Company.KDT.SHARE.Components.Globals.LaDNCX = chkLaDNCX.Checked;
#endif
                    //Lưu file cấu hình
                    doc.Save(fileName);
                }

                #region Luu cau hinh cho DAI LY

                // luu cau hinh danh rieng
                if (versionHD == 1)
                {
                    string maDN = txtMaDN.Text;
                    string tenDN = txtTenDN.Text;
                    string diaChi = txtDiaChi.Text;
                    string nguoiLH = txtNguoiLienHeDN.Text;
                    string eMail = txtMailDoanhNghiep.Text;
                    string maHQ = donViHaiQuanControl1.Ma.ToString();
                    string mailHQ = txtMailHaiQuan.Text;
                    string midDN = txtMaMid.Text;
                    string maCuc = txtMaCuc.Text;
                    string tenCuc = donViHaiQuanControl1.cbTen.Text;
                    string tenHQ = txtTenCucHQ.Text;
                    string tenNganHQ = txtTenNganHQ.Text;
                    string dienthoai = txtDienThoaiDN.Text;
                    string sofax = txtSoFaxDN.Text;
                    string diaChiHQ = txtDiaChiHQ.Text;
                    string tenDichVu = txtTenDichVu.Text;
                    string chucVu = txtChucVu.Text;


                    //inser update du lieu
                    try
                    {
                        Company.KDT.SHARE.Components.HeThongPhongKhai htpk = new Company.KDT.SHARE.Components.HeThongPhongKhai();
                        htpk.MaDoanhNghiep = maDN;
                        htpk.TenDoanhNghiep = tenDN;
                        htpk.PassWord = "0";

                        htpk.Key_Config = "CauHinh";
                        htpk.Value_Config = "1";
                        htpk.InsertUpdate();
                        htpk.Key_Config = "DIA_CHI";
                        htpk.Value_Config = diaChi;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "ĐIEN_THOAI";
                        htpk.Value_Config = dienthoai;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "SO_FAX";
                        htpk.Value_Config = sofax;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "NGUOI_LIEN_HE";
                        htpk.Value_Config = nguoiLH;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "CHUC_VU";
                        htpk.Value_Config = chucVu;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "MailDoanhNghiep";
                        htpk.Value_Config = eMail;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "MaMID";
                        htpk.Value_Config = midDN;
                        htpk.InsertUpdate();

                        //Lưu thông tin hải quan
                        htpk.Key_Config = "MA_CUC_HAI_QUAN";
                        htpk.Value_Config = maCuc;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "MA_HAI_QUAN";
                        htpk.Value_Config = maHQ;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "MailHaiQuan";
                        htpk.Value_Config = mailHQ;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "TEN_CUC_HAI_QUAN";
                        htpk.Value_Config = tenCuc;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "TEN_HAI_QUAN_NGAN";
                        htpk.Value_Config = tenNganHQ;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "TEN_HAI_QUAN";
                        htpk.Value_Config = tenHQ;
                        htpk.InsertUpdate();

                        //Save Dai chi khai bao hai quan
                        htpk.Key_Config = "DIA_CHI_HQ";
                        htpk.Value_Config = diaChiHQ;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "TEN_DICH_VU";
                        htpk.Value_Config = tenDichVu;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "CHU_KY_SO";
                        htpk.Value_Config = Convert.ToString(chkCKS.Checked);
                        htpk.InsertUpdate();
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi cập nhật thông tin vào hệ thống phồng khai xãy ra lỗi. /n/n" + ex, false);
                    }
                }

                #endregion

                ShowMessage("Lưu file cấu hình Thông tin Doanh nghiệp và Hải quan thành công.", false);

                GlobalSettings.RefreshKey();
                //kiểm tra cấu hình Chữ ký số.Phi

                if (chkCKS.Checked == true)
                {
                    bool KyTrucTiep = Company.KDT.SHARE.Components.Globals.IsSignature;
                    bool KyTuXa = Company.KDT.SHARE.Components.Globals.IsSignRemote;
                    bool KyMangLan = Company.KDT.SHARE.Components.Globals.IsSignOnLan;
                    if (KyTrucTiep == false && KyTuXa == false && KyMangLan == false)
                    {
                        ShowMessage("Chưa cấu hình chữ ký số. Xin vui long chọn cấu hình.", false);
                        FrmCauHinhChuKySo frm = new FrmCauHinhChuKySo();
                        frm.ShowDialog();
                    }
                    if (Company.KDT.SHARE.Components.Globals.IsSignature == true || Company.KDT.SHARE.Components.Globals.IsSignRemote == true || Company.KDT.SHARE.Components.Globals.IsSignOnLan == true)
                    {
                        this.Close();
                    }

                }
                else
                {
                    Company.KDT.SHARE.Components.Globals.IsSignature = false;
                    Company.KDT.SHARE.Components.Globals.IsSignRemote = false;
                    Company.KDT.SHARE.Components.Globals.IsSignOnLan = false;
                    this.Close();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        void frm_HandlerConfig(object sender, EventArgs e)
        {

        }

        private void donViHaiQuanControl1_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(donViHaiQuanControl1.Ma)) return;
            string maCuc = donViHaiQuanControl1.Ma.Substring(1, 2);
            txtMaCuc.Text = maCuc;

            Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan objDonViHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.Load("Z" + maCuc + "Z");

            txtTenCucHQ.Text = objDonViHQ != null ? objDonViHQ.Ten : "";
            txtTenNganHQ.Text = donViHaiQuanControl1.Ten;
            txtMailHaiQuan.Text = "";

            //Updated by Hungtq 2903/2012.
            Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc objCucHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.Load(maCuc);
            if (objCucHQ != null)
            {
                txtDiaChiHQ.Text = (objCucHQ.IPService != "" ? objCucHQ.IPService : "?");

                if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                {
                    if (GlobalSettings.SuDungChuKySo)
                        txtTenDichVu.Text = objCucHQ.ServicePathCKS;
                    else
                        txtTenDichVu.Text = objCucHQ.ServicePathV3;
                }
                else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                {
                    txtTenDichVu.Text = objCucHQ.ServicePathV2;
                }
            }

            //GetIPWebService(donViHaiQuanControl1.Ma);
        }

        private void GetIPWebService(string maChiCucHQ)
        {
            try
            {
                //if (string.IsNullOrEmpty(maChiCucHQ)) return;

                //Company.KDT.SHARE.Components.Globals.GetIPWebService(donViHaiQuanControl1.Ma);

                //txtMaCuc.Text = Company.KDT.SHARE.Components.Globals.IPServiceHQ.MaCuc;
                //txtTenCucHQ.Text = Company.KDT.SHARE.Components.Globals.IPServiceHQ.TenCuc;
                //txtTenNganHQ.Text = Company.KDT.SHARE.Components.Globals.IPServiceHQ.TenChiCuc;
                //if (Company.KDT.SHARE.Components.Globals.IPServiceHQ.IP != null)
                //    txtDiaChiHQ.Text = Company.KDT.SHARE.Components.Globals.IPServiceHQ.DiaChi;
                //if (Company.KDT.SHARE.Components.Globals.IPServiceHQ.TenDichVu != null)
                //    txtTenDichVu.Text = Company.KDT.SHARE.Components.Globals.IPServiceHQ.TenDichVu;
                //txtMailHaiQuan.Text = "";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(maChiCucHQ, ex); }
        }

        private void txtTenDichVu_TextChanged(object sender, EventArgs e)
        {
            txtDiaChiHQ_TextChanged(null, null);
        }

        private void txtDiaChiHQ_TextChanged(object sender, EventArgs e)
        {
            string url = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                url = string.Format("http://{0}", url); lblWS.Text = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
            lblWS.Text = url;

            //if (Company.KDT.SHARE.Components.Globals.IPServiceHQ.WebService == null)
            //{
            //    string url = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
            //    if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
            //        url = string.Format("http://{0}", url); lblWS.Text = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
            //    lblWS.Text = url;
            //}
            //else
            //    lblWS.Text = Company.KDT.SHARE.Components.Globals.IPServiceHQ.WebService;
        }

        private void btnOpenWS_Click(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.Globals.OpenWebrowser(lblWS.Text.Trim());
        }

        private void chkCKS_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                bool KyTrucTiep = Company.KDT.SHARE.Components.Globals.IsSignature;
                bool KyTuXa = Company.KDT.SHARE.Components.Globals.IsSignRemote;
                bool KyMangLan = Company.KDT.SHARE.Components.Globals.IsSignOnLan;
                if (chkCKS.Checked == false && (KyTrucTiep == true || KyTuXa == true || KyMangLan == true))
                {
                    if (ShowMessage("Bạn không muốn sữ dụng chử ký sô? ", true) == "Yes")
                    {
                        Company.KDT.SHARE.Components.Globals.IsSignature = false;
                        Company.KDT.SHARE.Components.Globals.IsSignRemote = false;
                        Company.KDT.SHARE.Components.Globals.IsSignOnLan = false;
                    }
                    else
                    {
                        chkCKS.Checked = true;
                    }
                }
                if (string.IsNullOrEmpty(donViHaiQuanControl1.Ma)) return;
                string maCuc = donViHaiQuanControl1.Ma.Substring(1, 2);
                txtMaCuc.Text = maCuc;

                Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc objCucHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.Load(maCuc);
                if (objCucHQ == null)
                {
                    if (chkCKS.Checked)
                    {
                        txtDiaChiHQ.Text = "https://tqdttntt.customs.gov.vn";
                        txtTenCucHQ.Text = "KDTService/Service.asmx";
                    }
                    return;
                };

                if (chkCKS.Checked)
                {
                    txtDiaChiHQ.Text = (objCucHQ.IPServiceCKS != "" ? objCucHQ.IPServiceCKS : "?");

                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    {
                        txtTenDichVu.Text = objCucHQ.ServicePathCKS;
                    }
                    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                    {
                        txtTenDichVu.Text = objCucHQ.ServicePathV2;
                    }
                }
                else
                {
                    txtDiaChiHQ.Text = (objCucHQ.IPService != "" ? objCucHQ.IPService : "?");

                    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                    {
                        if (GlobalSettings.SuDungChuKySo)
                            txtTenDichVu.Text = objCucHQ.ServicePathCKS;
                        else
                            txtTenDichVu.Text = objCucHQ.ServicePathV3;
                    }
                    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                    {
                        txtTenDichVu.Text = objCucHQ.ServicePathV2;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnCapNhatDBDL_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(GlobalSettings.USERNAME_DONGBO) || string.IsNullOrEmpty(GlobalSettings.PASSWOR_DONGBO))
            {
                WSDBDLForm f = new WSDBDLForm();
                f.ShowDialog();
                if (!f.IsReady) return;
                user = f.txtMaDoanhNghiep.Text;
                pw = f.txtMatKhau.Text;
            }
            else
            {
                user = GlobalSettings.USERNAME_DONGBO;
                pw = GlobalSettings.PASSWOR_DONGBO;
            }
            pw = Company.KDT.SHARE.Components.Helpers.GetMD5Value(pw);

            //Luu server cau hinih dai ly
            Company.KDT.SHARE.Components.ISyncData myService = Company.KDT.SHARE.Components.WebService.SyncService();
            bool successfull = myService.InsertUpdateCauHinhDaiLy(user, pw, GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI, Convert.ToInt32(txtSoTKDBDL.Text), "", 1);

            if (successfull == false)
            {
                ShowMessage("Lưu cấu hình số tờ khai Đại lý phải đồng bộ dữ liệu cho Doanh nghiệp thất bại.", false);
            }
            else
                ShowMessage("Lưu cấu hình số tờ khai Đại lý phải đồng bộ dữ liệu cho Doanh nghiệp thành công.", false);
        }
    }
}