IF OBJECT_ID('t_KDT_LenhSanXuat_SP') IS NOT NULL
DROP TABLE t_KDT_LenhSanXuat_SP
GO
IF OBJECT_ID('t_KDT_LenhSanXuat') IS NOT NULL
DROP TABLE t_KDT_LenhSanXuat
GO
IF OBJECT_ID('t_KDT_LenhSanXuat_QuyTac') IS NOT NULL
DROP TABLE t_KDT_LenhSanXuat_QuyTac
GO
CREATE TABLE t_KDT_LenhSanXuat
(
	ID BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[HopDong_ID] [bigint] NOT NULL,
	MaDoanhNghiep VARCHAR(20) NOT NULL,
	TenDoanhNghiep NVARCHAR(255) NOT NULL,
	MaHaiQuan VARCHAR(6) NOT NULL,
	SoLenhSanXuat NVARCHAR(50) NOT NULL,
	TuNgay DATETIME NOT NULL,
	DenNgay DATETIME NOT NULL,
	SoDonHang NVARCHAR(255) NULL,
	TinhTrang INT NULL,
	GhiChu NVARCHAR(255),
)
GO
CREATE TABLE t_KDT_LenhSanXuat_SP
(
	ID BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	LenhSanXuat_ID BIGINT NOT NULL FOREIGN KEY REFERENCES t_KDT_LenhSanXuat(ID),
	[MaSanPham] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	GhiChu NVARCHAR(255),
	TrangThai INT NOT NULL DEFAULT (0)
)
GO
CREATE TABLE t_KDT_LenhSanXuat_QuyTac
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[TienTo] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TinhTrang] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiHinh] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GiaTriPhanSo] [bigint] NULL,
[DoDaiSo] [bigint] NULL,
Nam DATETIME NULL,
[SoHopDong] NVARCHAR(255) NULL,
HopDong_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_GC_HopDong(ID),
[HienThi] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_Insert]
	@HopDong_ID bigint,
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@SoLenhSanXuat nvarchar(50),
	@TuNgay datetime,
	@DenNgay datetime,
	@SoDonHang nvarchar(255),
	@TinhTrang int,
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_LenhSanXuat]
(
	[HopDong_ID],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[SoLenhSanXuat],
	[TuNgay],
	[DenNgay],
	[SoDonHang],
	[TinhTrang],
	[GhiChu]
)
VALUES 
(
	@HopDong_ID,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaHaiQuan,
	@SoLenhSanXuat,
	@TuNgay,
	@DenNgay,
	@SoDonHang,
	@TinhTrang,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_Update]
	@ID bigint,
	@HopDong_ID bigint,
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@SoLenhSanXuat nvarchar(50),
	@TuNgay datetime,
	@DenNgay datetime,
	@SoDonHang nvarchar(255),
	@TinhTrang int,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_LenhSanXuat]
SET
	[HopDong_ID] = @HopDong_ID,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaHaiQuan] = @MaHaiQuan,
	[SoLenhSanXuat] = @SoLenhSanXuat,
	[TuNgay] = @TuNgay,
	[DenNgay] = @DenNgay,
	[SoDonHang] = @SoDonHang,
	[TinhTrang] = @TinhTrang,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_InsertUpdate]
	@ID bigint,
	@HopDong_ID bigint,
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@SoLenhSanXuat nvarchar(50),
	@TuNgay datetime,
	@DenNgay datetime,
	@SoDonHang nvarchar(255),
	@TinhTrang int,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_LenhSanXuat] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_LenhSanXuat] 
		SET
			[HopDong_ID] = @HopDong_ID,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaHaiQuan] = @MaHaiQuan,
			[SoLenhSanXuat] = @SoLenhSanXuat,
			[TuNgay] = @TuNgay,
			[DenNgay] = @DenNgay,
			[SoDonHang] = @SoDonHang,
			[TinhTrang] = @TinhTrang,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_LenhSanXuat]
		(
			[HopDong_ID],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaHaiQuan],
			[SoLenhSanXuat],
			[TuNgay],
			[DenNgay],
			[SoDonHang],
			[TinhTrang],
			[GhiChu]
		)
		VALUES 
		(
			@HopDong_ID,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaHaiQuan,
			@SoLenhSanXuat,
			@TuNgay,
			@DenNgay,
			@SoDonHang,
			@TinhTrang,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_LenhSanXuat]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_LenhSanXuat] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[SoLenhSanXuat],
	[TuNgay],
	[DenNgay],
	[SoDonHang],
	[TinhTrang],
	[GhiChu]
FROM
	[dbo].[t_KDT_LenhSanXuat]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HopDong_ID],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[SoLenhSanXuat],
	[TuNgay],
	[DenNgay],
	[SoDonHang],
	[TinhTrang],
	[GhiChu]
FROM [dbo].[t_KDT_LenhSanXuat] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[SoLenhSanXuat],
	[TuNgay],
	[DenNgay],
	[SoDonHang],
	[TinhTrang],
	[GhiChu]
FROM
	[dbo].[t_KDT_LenhSanXuat]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_SelectBy_LenhSanXuat_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_SelectBy_LenhSanXuat_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_SP_DeleteBy_LenhSanXuat_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_DeleteBy_LenhSanXuat_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_Insert]
	@LenhSanXuat_ID bigint,
	@MaSanPham nvarchar(50),
	@TenSanPham nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@GhiChu nvarchar(255),
	@TrangThai int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_LenhSanXuat_SP]
(
	[LenhSanXuat_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
)
VALUES 
(
	@LenhSanXuat_ID,
	@MaSanPham,
	@TenSanPham,
	@MaHS,
	@DVT_ID,
	@GhiChu,
	@TrangThai
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_Update]
	@ID bigint,
	@LenhSanXuat_ID bigint,
	@MaSanPham nvarchar(50),
	@TenSanPham nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@GhiChu nvarchar(255),
	@TrangThai int
AS

UPDATE
	[dbo].[t_KDT_LenhSanXuat_SP]
SET
	[LenhSanXuat_ID] = @LenhSanXuat_ID,
	[MaSanPham] = @MaSanPham,
	[TenSanPham] = @TenSanPham,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[GhiChu] = @GhiChu,
	[TrangThai] = @TrangThai
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_InsertUpdate]
	@ID bigint,
	@LenhSanXuat_ID bigint,
	@MaSanPham nvarchar(50),
	@TenSanPham nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@GhiChu nvarchar(255),
	@TrangThai int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_LenhSanXuat_SP] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_LenhSanXuat_SP] 
		SET
			[LenhSanXuat_ID] = @LenhSanXuat_ID,
			[MaSanPham] = @MaSanPham,
			[TenSanPham] = @TenSanPham,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[GhiChu] = @GhiChu,
			[TrangThai] = @TrangThai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_LenhSanXuat_SP]
		(
			[LenhSanXuat_ID],
			[MaSanPham],
			[TenSanPham],
			[MaHS],
			[DVT_ID],
			[GhiChu],
			[TrangThai]
		)
		VALUES 
		(
			@LenhSanXuat_ID,
			@MaSanPham,
			@TenSanPham,
			@MaHS,
			@DVT_ID,
			@GhiChu,
			@TrangThai
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_LenhSanXuat_SP]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_DeleteBy_LenhSanXuat_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_DeleteBy_LenhSanXuat_ID]
	@LenhSanXuat_ID bigint
AS

DELETE FROM [dbo].[t_KDT_LenhSanXuat_SP]
WHERE
	[LenhSanXuat_ID] = @LenhSanXuat_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_LenhSanXuat_SP] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LenhSanXuat_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM
	[dbo].[t_KDT_LenhSanXuat_SP]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_SelectBy_LenhSanXuat_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_SelectBy_LenhSanXuat_ID]
	@LenhSanXuat_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LenhSanXuat_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM
	[dbo].[t_KDT_LenhSanXuat_SP]
WHERE
	[LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LenhSanXuat_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM [dbo].[t_KDT_LenhSanXuat_SP] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_SP_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_SP_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LenhSanXuat_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM
	[dbo].[t_KDT_LenhSanXuat_SP]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_LenhSanXuat_QuyTac_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_Insert]
	@TienTo nvarchar(200),
	@TinhTrang nvarchar(50),
	@LoaiHinh nvarchar(50),
	@GiaTriPhanSo bigint,
	@DoDaiSo bigint,
	@Nam datetime,
	@SoHopDong nvarchar(255),
	@HopDong_ID bigint,
	@HienThi nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_LenhSanXuat_QuyTac]
(
	[TienTo],
	[TinhTrang],
	[LoaiHinh],
	[GiaTriPhanSo],
	[DoDaiSo],
	[Nam],
	[SoHopDong],
	[HopDong_ID],
	[HienThi]
)
VALUES 
(
	@TienTo,
	@TinhTrang,
	@LoaiHinh,
	@GiaTriPhanSo,
	@DoDaiSo,
	@Nam,
	@SoHopDong,
	@HopDong_ID,
	@HienThi
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_Update]
	@ID bigint,
	@TienTo nvarchar(200),
	@TinhTrang nvarchar(50),
	@LoaiHinh nvarchar(50),
	@GiaTriPhanSo bigint,
	@DoDaiSo bigint,
	@Nam datetime,
	@SoHopDong nvarchar(255),
	@HopDong_ID bigint,
	@HienThi nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_LenhSanXuat_QuyTac]
SET
	[TienTo] = @TienTo,
	[TinhTrang] = @TinhTrang,
	[LoaiHinh] = @LoaiHinh,
	[GiaTriPhanSo] = @GiaTriPhanSo,
	[DoDaiSo] = @DoDaiSo,
	[Nam] = @Nam,
	[SoHopDong] = @SoHopDong,
	[HopDong_ID] = @HopDong_ID,
	[HienThi] = @HienThi
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_InsertUpdate]
	@ID bigint,
	@TienTo nvarchar(200),
	@TinhTrang nvarchar(50),
	@LoaiHinh nvarchar(50),
	@GiaTriPhanSo bigint,
	@DoDaiSo bigint,
	@Nam datetime,
	@SoHopDong nvarchar(255),
	@HopDong_ID bigint,
	@HienThi nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_LenhSanXuat_QuyTac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_LenhSanXuat_QuyTac] 
		SET
			[TienTo] = @TienTo,
			[TinhTrang] = @TinhTrang,
			[LoaiHinh] = @LoaiHinh,
			[GiaTriPhanSo] = @GiaTriPhanSo,
			[DoDaiSo] = @DoDaiSo,
			[Nam] = @Nam,
			[SoHopDong] = @SoHopDong,
			[HopDong_ID] = @HopDong_ID,
			[HienThi] = @HienThi
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_LenhSanXuat_QuyTac]
		(
			[TienTo],
			[TinhTrang],
			[LoaiHinh],
			[GiaTriPhanSo],
			[DoDaiSo],
			[Nam],
			[SoHopDong],
			[HopDong_ID],
			[HienThi]
		)
		VALUES 
		(
			@TienTo,
			@TinhTrang,
			@LoaiHinh,
			@GiaTriPhanSo,
			@DoDaiSo,
			@Nam,
			@SoHopDong,
			@HopDong_ID,
			@HienThi
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_LenhSanXuat_QuyTac]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_LenhSanXuat_QuyTac]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_LenhSanXuat_QuyTac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TienTo],
	[TinhTrang],
	[LoaiHinh],
	[GiaTriPhanSo],
	[DoDaiSo],
	[Nam],
	[SoHopDong],
	[HopDong_ID],
	[HienThi]
FROM
	[dbo].[t_KDT_LenhSanXuat_QuyTac]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TienTo],
	[TinhTrang],
	[LoaiHinh],
	[GiaTriPhanSo],
	[DoDaiSo],
	[Nam],
	[SoHopDong],
	[HopDong_ID],
	[HienThi]
FROM
	[dbo].[t_KDT_LenhSanXuat_QuyTac]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TienTo],
	[TinhTrang],
	[LoaiHinh],
	[GiaTriPhanSo],
	[DoDaiSo],
	[Nam],
	[SoHopDong],
	[HopDong_ID],
	[HienThi]
FROM [dbo].[t_KDT_LenhSanXuat_QuyTac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LenhSanXuat_QuyTac_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LenhSanXuat_QuyTac_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TienTo],
	[TinhTrang],
	[LoaiHinh],
	[GiaTriPhanSo],
	[DoDaiSo],
	[Nam],
	[SoHopDong],
	[HopDong_ID],
	[HienThi]
FROM
	[dbo].[t_KDT_LenhSanXuat_QuyTac]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.2',GETDATE(), N'CẬP NHẬT PROCDEDURE LỆNH SẢN XUẤT VÀ QUY TẮC')
END