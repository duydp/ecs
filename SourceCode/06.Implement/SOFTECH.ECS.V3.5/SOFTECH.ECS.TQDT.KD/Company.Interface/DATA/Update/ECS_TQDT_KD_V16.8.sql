


delete t_VNACC_Category_Cargo where BondedAreaCode in('30CFC01','30CFC02','30CFC03','30CFC04','30CFC05','30CFOZZ','30CFS01','30CFS02','30CFS03','30CFS04','30CFSCF')
delete t_VNACC_Category_Cargo where BondedAreaCode like '60NC%'

IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='40B1GB1')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','40B1GB1','DOI NV 1 BUPRANG')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='40B1OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','40B1OZZ','DIEM LUU HH XK 40B1')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='40B2GB2')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','40B2GB2','DOI NV 2 BUPRANG')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='40B2OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','40B2OZZ','DIEM LUU HH XK 40B2')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='40D1CD1')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','40D1CD1','DOI NV DA LAT')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='40D1OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','40D1OZZ','DIEM LUU HH XK 40D1')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='40D2CD2')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','40D2CD2','DOI NV 2 DA LAT')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='40D2OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','40D2OZZ','DIEM LUU HH XK 40D2')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='38B1GB1')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','38B1GB1','DOI NV CK LE THANH')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='38B1OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','38B1OZZ','DIEM LUU HH XK 38B1')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='38B2GB2')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','38B2GB2','DOI TT CK LE THANH')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='38B2OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','38B2OZZ','DIEM LUU HH XK 38B2')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01E1Z01')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01E1Z01','CTY SXTM TIEN THANH')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01E1Z02')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01E1Z02','CTY MAY DAI NGHIA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01E1Z03')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01E1Z03','CTY TRUNG HUNG HANOI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01E1Z04')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01E1Z04','CTY TNHH SOVINA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01E1Z05')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01E1Z05','CTY MAY THANH TRI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01E1Z06')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01E1Z06','CTY CP DET 10-10')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01E1Z07')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01E1Z07','CTY CP DET 10-10')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01IKZ01')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01IKZ01','CTY AKEBONO KASEI VN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ29')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ29','CT PARKER PROCESSING')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ30')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ30','CTY SAKURA HONGMINH')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ31')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ31','CTY DAIWA PLASTICS')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ32')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ32','CT NIPPON KOUATSU VN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ33')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ33','CTY ENKEI VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ34')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ34','CTY VN NIPPON SEIKI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ35')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ35','CT PANA-APPLIANCES')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ36')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ36','CTY DAIICHI KINZOKU')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ37')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ37','CTY BROAD BRI-SAKURA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ38')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ38','CTY SATO VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ39')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ39','CTY SD VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ40')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ40','CT SUMITOMO H-INDUS')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ41')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ41','CT RYONAN ELECTRIC')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ42')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ42','CTY KYOEI DIETECH VN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ43')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ43','CTY KISHIRO VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ44')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ44','CT HOKUYO PRECISION')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ45')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ45','CTY KAI VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ46')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ46','CTY FUJIPLA VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ47')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ47','CTY LINH DIEN TU SEI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ48')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ48','CTY TAKARA TOOL-DIE')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ49')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ49','CTY KOSAI VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ50')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ50','CT STANDARD U-SUPPLY')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ51')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ51','CTY SEED VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ52')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ52','CTY JTEC HANOI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ53')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ53','CTY TNHH HOEV')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ54')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ54','CTY EIWO RUBBER MFG')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ55')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ55','CTY TOKYO MICRO VN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ56')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ56','CTY IKEICHI VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ57')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ57','CTY TOA VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ58')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ58','CTY MOLEX VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ59')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ59','CTY HONEST VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ60')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ60','CTY MHI AEROSPACE VN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ61')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ61','CTY VIETNAM IRITANI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ62')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ62','CTY VIETNAM IRITANI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ63')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ63','CTY VINA OSHOE')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ64')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ64','CTY CN SPINDEX HANOI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01NVZ65')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01NVZ65','CTY TOYODA GIKEN VN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PLZ02')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PLZ02','CTY MAY CHIEN THANG')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ01')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ01','CTY PIAGGIO VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ02')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ02','CTY CN CHINH XAC VN1')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ03')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ03','CTY NS TECH VINA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ04')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ04','CTY CP WOODSLAND')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ05')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ05','CTY MIDORI APPREL VN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ06')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ06','CTY VINAKOREA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ07')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ07','CTY ABEISM VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ08')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ08','CTY INKEL VIETNAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ09')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ09','CTY CMS VINA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='01PRZ10')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','01PRZ10','CTY CP THONGMINH MK')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='31D1OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','31D1OZZ','DIEM LUU HH XK 31D1')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='31D1SD1')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','31D1SD1','DOI NV CANG HON LA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='31D2OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','31D2OZZ','DIEM LUU HH XK 31D2')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='31D2SD2')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','31D2SD2','DOI NV DONG HOI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='31D3OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','31D3OZZ','DIEM LUU HH XK 31D3')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='31D3SD3')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','31D3SD3','DOI NV CANG GIANH')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='60C1C26')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','60C1C26','XN LDS TAM KY')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='60C1C27')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','60C1C27','XN MOC VIET DUC')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='27B1GB1')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','27B1GB1','CCHQCK NA MEO TH')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='27B1OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','27B1OZZ','DIEM LUU HH XK 27B1')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='27B2GB2')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','27B2GB2','DOI THU TUC TEN TAN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='27B2OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','27B2OZZ','DIEM LUU HH XK 27B2')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='33A1GA1')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','33A1GA1','DOI NV CK ADOT')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='33A1OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','33A1OZZ','DIEM LUU HH XK 33A1')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='33A2GA2')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','33A2GA2','DOI NV HONG VAN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='33A2OZZ')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','33A2OZZ','DIEM LUU HH XK 33A2')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C01')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C01','NM NUOC KY LOI - VA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C02')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C02','HUNG NGHIEP HOA THAI')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C03')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C03','CTY LILAMA 5 - NDVA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C04')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C04','CTY TS NAM HA TINH')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C05')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C05','CTY SAMSUNG C&T CORP')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C06')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C06','CTY A DONG VIET NAM')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C07')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C07','NL GIAY VIET NHAT')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C08')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C08','CTY XM LIEN THANH')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C09')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C09','CTY  NL GIAY HAVIHAN')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C10')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C10','KHO XANG DAU V.ANG')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C11')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C11','TCTY KSTM HA TINH 3')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C12')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C12','TCTY LAP MAY VN-NDVA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C13')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C13','VPDH CHIMNEYS NDVA 1')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1C14')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1C14','CTY LUYEN KIM 5 TQ')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1S01')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1S01','TCTY KSTM HA TINH 1')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F1S02')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F1S02','TCTY KSTM HA TINH 2')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F2C01')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F2C01','GANG THEP HN FORMOSA')
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Cargo where BondedAreaCode ='30F2S01')
 Insert into t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) values ('A202A','30F2S01','CANG W7 SON DUONG')

Go

Update t_VNACC_Category_Cargo set BondedAreaName='DIEM LUU HH XK 30F1' where BondedAreaCode='30F1OZZ'
Update t_VNACC_Category_Cargo set BondedAreaName='DOI TT CANG VUNG ANG' where BondedAreaCode='30F1SF1'
Update t_VNACC_Category_Cargo set BondedAreaName='DIEM LUU HH XK 30F2' where BondedAreaCode='30F2OZZ'
Update t_VNACC_Category_Cargo set BondedAreaName='DOITT CANG SON DUONG' where BondedAreaCode='30F2SF2'
Go
--------Cap nhat ma don vị tinh
 IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_QuantityUnit where Code ='INC2')
  INSERT INTO t_VNACC_Category_QuantityUnit VALUES ('A501A','INC2',N'Inch2',N'Inch2',null,null,null)
  Go
   DELETE t_VNACCS_Mapper Where CodeV4='N60C'
  INSERT INTO t_VNACCS_Mapper VALUES ('N60C',N'Chi cụcHQ KCN ĐNam ĐNgọc QN','60C1',N'Đội NV-KCN ĐNam ĐNgọc QN','MaHQ',N'')
  go
  
  Alter table t_KDT_VNACC_ChungTuDinhKem
  Alter column SoTiepNhan numeric(12,0) null
  go
  -- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 2),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(12, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
(
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
)
VALUES 
(
	@LoaiChungTu,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@TrangThaiXuLy,
	@CoQuanHaiQuan,
	@NhomXuLyHoSo,
	@TieuDe,
	@SoToKhai,
	@GhiChu,
	@PhanLoaiThuTucKhaiBao,
	@SoDienThoaiNguoiKhaiBao,
	@SoQuanLyTrongNoiBoDoanhNghiep,
	@MaKetQuaXuLy,
	@TenThuTucKhaiBao,
	@NgayKhaiBao,
	@TrangThaiKhaiBao,
	@NgaySuaCuoiCung,
	@TenNguoiKhaiBao,
	@DiaChiNguoiKhaiBao,
	@SoDeLayTepDinhKem,
	@NgayHoanThanhKiemTraHoSo,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TongDungLuong,
	@PhanLuong,
	@HuongDan,
	@TKMD_ID,
	@NgayBatDau,
	@NgayCapNhatCuoiCung,
	@CanCuPhapLenh,
	@CoBaoXoa,
	@GhiChuHaiQuan,
	@HinhThucTimKiem,
	@LyDoChinhSua,
	@MaDiaDiemDen,
	@MaNhaVanChuyen,
	@MaPhanLoaiDangKy,
	@MaPhanLoaiXuLy,
	@MaThongTinXuat,
	@NgayCapPhep,
	@NgayThongBao,
	@NguoiGui,
	@NoiDungChinhSua,
	@PhuongTienVanChuyen,
	@SoChuyenDiBien,
	@SoSeri,
	@SoTiepNhanToKhaiPhoThong,
	@TrangThaiXuLyHaiQuan,
	@NguoiCapNhatCuoiCung,
	@NhomXuLyHoSoID
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 2),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(12, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
SET
	[LoaiChungTu] = @LoaiChungTu,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHoSo] = @NhomXuLyHoSo,
	[TieuDe] = @TieuDe,
	[SoToKhai] = @SoToKhai,
	[GhiChu] = @GhiChu,
	[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
	[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
	[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
	[NgayKhaiBao] = @NgayKhaiBao,
	[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
	[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
	[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
	[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
	[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
	[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TongDungLuong] = @TongDungLuong,
	[PhanLuong] = @PhanLuong,
	[HuongDan] = @HuongDan,
	[TKMD_ID] = @TKMD_ID,
	[NgayBatDau] = @NgayBatDau,
	[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
	[CanCuPhapLenh] = @CanCuPhapLenh,
	[CoBaoXoa] = @CoBaoXoa,
	[GhiChuHaiQuan] = @GhiChuHaiQuan,
	[HinhThucTimKiem] = @HinhThucTimKiem,
	[LyDoChinhSua] = @LyDoChinhSua,
	[MaDiaDiemDen] = @MaDiaDiemDen,
	[MaNhaVanChuyen] = @MaNhaVanChuyen,
	[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
	[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
	[MaThongTinXuat] = @MaThongTinXuat,
	[NgayCapPhep] = @NgayCapPhep,
	[NgayThongBao] = @NgayThongBao,
	[NguoiGui] = @NguoiGui,
	[NoiDungChinhSua] = @NoiDungChinhSua,
	[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
	[SoChuyenDiBien] = @SoChuyenDiBien,
	[SoSeri] = @SoSeri,
	[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
	[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
	[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
	[NhomXuLyHoSoID] = @NhomXuLyHoSoID
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 2),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(12, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuDinhKem] 
		SET
			[LoaiChungTu] = @LoaiChungTu,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHoSo] = @NhomXuLyHoSo,
			[TieuDe] = @TieuDe,
			[SoToKhai] = @SoToKhai,
			[GhiChu] = @GhiChu,
			[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
			[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
			[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
			[NgayKhaiBao] = @NgayKhaiBao,
			[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
			[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
			[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
			[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
			[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
			[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TongDungLuong] = @TongDungLuong,
			[PhanLuong] = @PhanLuong,
			[HuongDan] = @HuongDan,
			[TKMD_ID] = @TKMD_ID,
			[NgayBatDau] = @NgayBatDau,
			[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
			[CanCuPhapLenh] = @CanCuPhapLenh,
			[CoBaoXoa] = @CoBaoXoa,
			[GhiChuHaiQuan] = @GhiChuHaiQuan,
			[HinhThucTimKiem] = @HinhThucTimKiem,
			[LyDoChinhSua] = @LyDoChinhSua,
			[MaDiaDiemDen] = @MaDiaDiemDen,
			[MaNhaVanChuyen] = @MaNhaVanChuyen,
			[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
			[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
			[MaThongTinXuat] = @MaThongTinXuat,
			[NgayCapPhep] = @NgayCapPhep,
			[NgayThongBao] = @NgayThongBao,
			[NguoiGui] = @NguoiGui,
			[NoiDungChinhSua] = @NoiDungChinhSua,
			[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
			[SoChuyenDiBien] = @SoChuyenDiBien,
			[SoSeri] = @SoSeri,
			[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
			[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
			[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
			[NhomXuLyHoSoID] = @NhomXuLyHoSoID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
		(
			[LoaiChungTu],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[TrangThaiXuLy],
			[CoQuanHaiQuan],
			[NhomXuLyHoSo],
			[TieuDe],
			[SoToKhai],
			[GhiChu],
			[PhanLoaiThuTucKhaiBao],
			[SoDienThoaiNguoiKhaiBao],
			[SoQuanLyTrongNoiBoDoanhNghiep],
			[MaKetQuaXuLy],
			[TenThuTucKhaiBao],
			[NgayKhaiBao],
			[TrangThaiKhaiBao],
			[NgaySuaCuoiCung],
			[TenNguoiKhaiBao],
			[DiaChiNguoiKhaiBao],
			[SoDeLayTepDinhKem],
			[NgayHoanThanhKiemTraHoSo],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TongDungLuong],
			[PhanLuong],
			[HuongDan],
			[TKMD_ID],
			[NgayBatDau],
			[NgayCapNhatCuoiCung],
			[CanCuPhapLenh],
			[CoBaoXoa],
			[GhiChuHaiQuan],
			[HinhThucTimKiem],
			[LyDoChinhSua],
			[MaDiaDiemDen],
			[MaNhaVanChuyen],
			[MaPhanLoaiDangKy],
			[MaPhanLoaiXuLy],
			[MaThongTinXuat],
			[NgayCapPhep],
			[NgayThongBao],
			[NguoiGui],
			[NoiDungChinhSua],
			[PhuongTienVanChuyen],
			[SoChuyenDiBien],
			[SoSeri],
			[SoTiepNhanToKhaiPhoThong],
			[TrangThaiXuLyHaiQuan],
			[NguoiCapNhatCuoiCung],
			[NhomXuLyHoSoID]
		)
		VALUES 
		(
			@LoaiChungTu,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@TrangThaiXuLy,
			@CoQuanHaiQuan,
			@NhomXuLyHoSo,
			@TieuDe,
			@SoToKhai,
			@GhiChu,
			@PhanLoaiThuTucKhaiBao,
			@SoDienThoaiNguoiKhaiBao,
			@SoQuanLyTrongNoiBoDoanhNghiep,
			@MaKetQuaXuLy,
			@TenThuTucKhaiBao,
			@NgayKhaiBao,
			@TrangThaiKhaiBao,
			@NgaySuaCuoiCung,
			@TenNguoiKhaiBao,
			@DiaChiNguoiKhaiBao,
			@SoDeLayTepDinhKem,
			@NgayHoanThanhKiemTraHoSo,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TongDungLuong,
			@PhanLuong,
			@HuongDan,
			@TKMD_ID,
			@NgayBatDau,
			@NgayCapNhatCuoiCung,
			@CanCuPhapLenh,
			@CoBaoXoa,
			@GhiChuHaiQuan,
			@HinhThucTimKiem,
			@LyDoChinhSua,
			@MaDiaDiemDen,
			@MaNhaVanChuyen,
			@MaPhanLoaiDangKy,
			@MaPhanLoaiXuLy,
			@MaThongTinXuat,
			@NgayCapPhep,
			@NgayThongBao,
			@NguoiGui,
			@NoiDungChinhSua,
			@PhuongTienVanChuyen,
			@SoChuyenDiBien,
			@SoSeri,
			@SoTiepNhanToKhaiPhoThong,
			@TrangThaiXuLyHaiQuan,
			@NguoiCapNhatCuoiCung,
			@NhomXuLyHoSoID
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]	

GO

  
  

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '16.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('16.8',GETDATE(), N' Cap nhat ma luu kho cho thong quan, don vi tinh, alter table t_kdt_Vnacc_chungtudinhkem')
END	
