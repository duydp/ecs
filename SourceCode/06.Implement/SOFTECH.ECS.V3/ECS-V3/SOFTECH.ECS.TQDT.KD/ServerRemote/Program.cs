﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using Company.KDT.SHARE.Components;
using SignRemote;
using ServerRemote.BLL;
using System.Diagnostics;
using Company.Interface;

namespace ServerRemote
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            bool isData = true;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Mutex MyApplicationMutex = new Mutex(true, "SOFTECH.ECS.TQDT.SIGNREMOTE");
            if (!MyApplicationMutex.WaitOne(0, false))
            {
                foreach (Process process in Process.GetProcessesByName("SOFTECH.ECS.TQDT.SIGNREMOTE"))
                {
                    process.Kill();
                }
            }
            #region Comment Code
//            Application.DoEvents();
//            StartSplash();
//            SplashScreen.UpdateStatusText("Kiểm tra hệ thống");
//            Thread.Sleep(500);
//            ISignMessage service = WebService.SignMessage();
//            if (!Company.KDT.SHARE.Components.Globals.IsSignOnLan)
//            {
//                SplashScreen.UpdateStatusTextWithStatus("Đăng nhập hệ thống", TypeOfMessage.Success);

//                bool valid = false; ;
//                try
//                {
//                    valid = Globals.IsRememberLogin && service.IsExist(Globals.UserNameLogin, Globals.PasswordLogin);
//                }
//                catch (Exception ex)
//                {
//                    MessageBoxControl.ShowMessage(ex.Message);
//                }
//                if (!valid)
//                {
//                    SplashScreen.UpdateStatusTextWithStatus("Đăng nhập sai", TypeOfMessage.Error);
//                    Thread.Sleep(200);
//                    SplashScreen.CloseSplashScreen();
//                    if (new frmLogin().ShowDialog() != DialogResult.OK)
//                        Application.Exit();
//                    else
//                        StartSplash();
//                }
//                SplashScreen.UpdateStatusTextWithStatus("Đăng nhập hệ thống thành công", TypeOfMessage.Success);
//                Thread.Sleep(300);
//            }
//            SplashScreen.UpdateStatusTextWithStatus("Kiểm tra thiết bị ký số", TypeOfMessage.Success);
//            if (!Company.KDT.SHARE.Components.Globals.IsSignOnLan)
//                try
//                {
//                    Helpers.GetSignature("Test");
//                    SignRemote.MessageSend msg = new SignRemote.MessageSend()
//                    {
//                        From = Globals.UserNameLogin,
//                        Password = Globals.PasswordLogin,
//                        SendStatus = SendStatusInfo.ServerLogin,
//                        Warrning = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")
//                    };
//                    service.ServerSend(Helpers.Serializer(msg));
//                }
//                catch /*(Exception ex)*/
//                {
//                    //Logger.LocalLogger.Instance().WriteMessage(ex);
//                    SplashScreen.UpdateStatusTextWithStatus("Chữ ký số không hợp lệ", TypeOfMessage.Error);
//                    Thread.Sleep(400);
//                    SplashScreen.CloseSplashScreen();
//                    if (new frmLoginUSB().ShowDialog() != DialogResult.OK)
//                    {
//                        Application.ExitThread();
//                        return;
//                    }
//                }
//            else
//            {
//#if DEBUG
//                Thread.Sleep(400);
//                SplashScreen.CloseSplashScreen();
//#endif
//                try
//                {
//                    MessageSignature msg = new MessageSignature();
//                    msg = Helpers.GetSignature("Test");
//                    if (msg.Data == null || msg.FileCert == null)
//                    {
//                        if (new frmLoginUSB().ShowDialog() != DialogResult.OK)
//                        {
//                            return;
//                        }
//                    }
//                    else
//                    {
//                        Thread.Sleep(400);
//                        SplashScreen.CloseSplashScreen();
//                    }
//                }
//                catch (Exception)
//                {
//                    SplashScreen.UpdateStatusTextWithStatus("Chữ ký số không hợp lệ", TypeOfMessage.Error);
//                    Thread.Sleep(400);
//                    SplashScreen.CloseSplashScreen();
//                    if (new frmLoginUSB().ShowDialog() != DialogResult.OK)
//                    {
//                        return;
//                    }
//                }
//            }
//            SplashScreen.UpdateStatusTextWithStatus("Chữ ký số hợp lệ", TypeOfMessage.Success);
//            Thread.Sleep(500);
//            SplashScreen.UpdateStatusTextWithStatus("Kết nối cơ sở dữ liệu", TypeOfMessage.Success);
//            try
//            {
//                Thread.Sleep(500);
//                ////                if (Globals.IsSignOnLan)
//                //SignLogger.Load(0);
//            }
//            catch (Exception ex)
//            {
//                Logger.LocalLogger.Instance().WriteMessage(ex);
//                if (Globals.IsSignOnLan)
//                {
//                    if (MessageBoxControl.ShowMessageConfirm("Không thể kết nối cơ sở dữ liệu . Doanh nghiệp có muốn cấu hình Cơ sở dữ liệu không ?") == "Yes")
//                    {
//                        ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
//                        f.ShowDialog();
//                    }
//                }
//                else
//                {
//                    isData = false;
//                    try
//                    {
//                        SignLogger.SelectCollectionAccess(Application.StartupPath, 1, "", "");
//                    }
//                    catch (System.Exception e)
//                    {
//                        Logger.LocalLogger.Instance().WriteMessage(e);
//                        if (e.Message.Contains("Microsoft.ACE.OLEDB.12.0"))
//                        {
//                            if (MessageBox.Show("Thiết lập cơ sở dữ liệu", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
//                            {
//                                string command = Application.StartupPath + "\\DATA\\AccessDatabaseEngine.exe";
//                                string argss = " /passive";

//                                System.Diagnostics.Process process = new System.Diagnostics.Process();
//                                process.StartInfo.FileName = command;
//                                process.StartInfo.Arguments = argss;
//                                process.Start();
//                            }
//                        }
//                    }
//                }
//            }

//            SplashScreen.CloseSplashScreen();
//            string arg = string.Empty;
//            if (args.Length > 0)
//                arg = args[0];
            #endregion
            Application.Run(new MainForm());


        }
        private static void StartSplash()
        {
            Thread splashthread = new Thread(new ThreadStart(ServerRemote.SplashScreen.ShowSplashScreen));
            splashthread.IsBackground = true;
            splashthread.Start();
        }
    }
}
