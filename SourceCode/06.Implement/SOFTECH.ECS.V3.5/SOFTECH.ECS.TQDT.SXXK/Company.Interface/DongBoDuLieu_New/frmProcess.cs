﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Xml;

namespace Company.Interface.DongBoDuLieu_New
{
    public partial class frmProcess : Form
    {
        public frmProcess()
        {
            InitializeComponent();
        }
        public string NoiDungQuerry;
        public string UrlWS;
        public string Tempuri;
        public string result;

        private void LoadServices(string messagesQuery)
        {
            WebClient client = new WebClient();
            client.Headers.Add(HttpRequestHeader.ContentType, "text/xml");
            client.Headers.Add("SOAPAction", "http://tempuri.org/"+Tempuri);
            client.UploadProgressChanged += new UploadProgressChangedEventHandler(client_UploadProgressChanged);
            client.UploadStringCompleted += new UploadStringCompletedEventHandler(client_UploadStringCompleted);
            byte[] data = System.Text.Encoding.ASCII.GetBytes(NoiDungQuerry);
            client.UploadStringAsync(new Uri(UrlWS), messagesQuery);
        }

        void client_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                result = e.Result;
                this.Close();
            }
            else
            {
                result = string.Empty;
                MessageBox.Show(e.Error.Message);
                this.Close();
            }
        }


        void client_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {

            uiProgressBar1.Value = (e.ProgressPercentage - 50) * 2;
            lblTrangThai.Text = uiProgressBar1.Value.ToString("N0") + " %";

        }
//         private static XmlDocument CreateSoapEnvelope()
//         {
//             string.Format("");
//             XmlDocument soapEnvelop = new XmlDocument();
//             soapEnvelop.LoadXml(string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?>
// <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
//   <soap:Body>
//   <SeachTKMD_SXXK xmlns=""http://tempuri.org/"">
//       <userNameLogin>{0}</userNameLogin>
//       <passWordLogin>{1}</passWordLogin>
//       <listDaiLy>{2}</listDaiLy>
//       <MaDoanhNghiep>{3}</MaDoanhNghiep>
//       <FromNgayDongBo>{4}</FromNgayDongBo>
//       <ToNgayDongBo>{5}</ToNgayDongBo>
//     </SeachTKMD_SXXK>
//   </soap:Body>
// </soap:Envelope>"));
//             return soapEnvelop;
//         }

        private void frmProcess_Load(object sender, EventArgs e)
        {
            LoadServices(NoiDungQuerry);
        }


    }
}
