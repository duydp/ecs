﻿-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_SanPham_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_DeleteBy_HopDong_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_Insert]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_Insert]
	@HopDong_ID bigint,
	@MaSP varchar(30),
	@TenSP nvarchar(80),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@LuongXuatDK numeric(18, 5),
	@TriGiaXuatDK numeric(18, 5),
	@LuongXuatTK numeric(18, 5),
	@TriGiaXuatTK numeric(18, 5),
	@LuongTonCK numeric(18, 5),
	@TriGiaTonCK numeric(18, 5),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_QuyetToan_SanPham]
(
	[HopDong_ID],
	[MaSP],
	[TenSP],
	[MaHS],
	[DVT_ID],
	[LuongXuatDK],
	[TriGiaXuatDK],
	[LuongXuatTK],
	[TriGiaXuatTK],
	[LuongTonCK],
	[TriGiaTonCK]
)
VALUES 
(
	@HopDong_ID,
	@MaSP,
	@TenSP,
	@MaHS,
	@DVT_ID,
	@LuongXuatDK,
	@TriGiaXuatDK,
	@LuongXuatTK,
	@TriGiaXuatTK,
	@LuongTonCK,
	@TriGiaTonCK
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_Update]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_Update]
	@ID bigint,
	@HopDong_ID bigint,
	@MaSP varchar(30),
	@TenSP nvarchar(80),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@LuongXuatDK numeric(18, 5),
	@TriGiaXuatDK numeric(18, 5),
	@LuongXuatTK numeric(18, 5),
	@TriGiaXuatTK numeric(18, 5),
	@LuongTonCK numeric(18, 5),
	@TriGiaTonCK numeric(18, 5)
AS

UPDATE
	[dbo].[t_KDT_GC_QuyetToan_SanPham]
SET
	[HopDong_ID] = @HopDong_ID,
	[MaSP] = @MaSP,
	[TenSP] = @TenSP,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[LuongXuatDK] = @LuongXuatDK,
	[TriGiaXuatDK] = @TriGiaXuatDK,
	[LuongXuatTK] = @LuongXuatTK,
	[TriGiaXuatTK] = @TriGiaXuatTK,
	[LuongTonCK] = @LuongTonCK,
	[TriGiaTonCK] = @TriGiaTonCK
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_InsertUpdate]
	@ID bigint,
	@HopDong_ID bigint,
	@MaSP varchar(30),
	@TenSP nvarchar(80),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@LuongXuatDK numeric(18, 5),
	@TriGiaXuatDK numeric(18, 5),
	@LuongXuatTK numeric(18, 5),
	@TriGiaXuatTK numeric(18, 5),
	@LuongTonCK numeric(18, 5),
	@TriGiaTonCK numeric(18, 5)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_QuyetToan_SanPham] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_QuyetToan_SanPham] 
		SET
			[HopDong_ID] = @HopDong_ID,
			[MaSP] = @MaSP,
			[TenSP] = @TenSP,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[LuongXuatDK] = @LuongXuatDK,
			[TriGiaXuatDK] = @TriGiaXuatDK,
			[LuongXuatTK] = @LuongXuatTK,
			[TriGiaXuatTK] = @TriGiaXuatTK,
			[LuongTonCK] = @LuongTonCK,
			[TriGiaTonCK] = @TriGiaTonCK
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_QuyetToan_SanPham]
		(
			[HopDong_ID],
			[MaSP],
			[TenSP],
			[MaHS],
			[DVT_ID],
			[LuongXuatDK],
			[TriGiaXuatDK],
			[LuongXuatTK],
			[TriGiaXuatTK],
			[LuongTonCK],
			[TriGiaTonCK]
		)
		VALUES 
		(
			@HopDong_ID,
			@MaSP,
			@TenSP,
			@MaHS,
			@DVT_ID,
			@LuongXuatDK,
			@TriGiaXuatDK,
			@LuongXuatTK,
			@TriGiaXuatTK,
			@LuongTonCK,
			@TriGiaTonCK
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_Delete]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_QuyetToan_SanPham]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_QuyetToan_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_QuyetToan_SanPham] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_Load]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[MaSP],
	[TenSP],
	[MaHS],
	[DVT_ID],
	[LuongXuatDK],
	[TriGiaXuatDK],
	[LuongXuatTK],
	[TriGiaXuatTK],
	[LuongTonCK],
	[TriGiaTonCK]
FROM
	[dbo].[t_KDT_GC_QuyetToan_SanPham]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[MaSP],
	[TenSP],
	[MaHS],
	[DVT_ID],
	[LuongXuatDK],
	[TriGiaXuatDK],
	[LuongXuatTK],
	[TriGiaXuatTK],
	[LuongTonCK],
	[TriGiaTonCK]
FROM
	[dbo].[t_KDT_GC_QuyetToan_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HopDong_ID],
	[MaSP],
	[TenSP],
	[MaHS],
	[DVT_ID],
	[LuongXuatDK],
	[TriGiaXuatDK],
	[LuongXuatTK],
	[TriGiaXuatTK],
	[LuongTonCK],
	[TriGiaTonCK]
FROM [dbo].[t_KDT_GC_QuyetToan_SanPham] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_SanPham_SelectAll]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_SanPham_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[MaSP],
	[TenSP],
	[MaHS],
	[DVT_ID],
	[LuongXuatDK],
	[TriGiaXuatDK],
	[LuongXuatTK],
	[TriGiaXuatTK],
	[LuongTonCK],
	[TriGiaTonCK]
FROM
	[dbo].[t_KDT_GC_QuyetToan_SanPham]	

GO

