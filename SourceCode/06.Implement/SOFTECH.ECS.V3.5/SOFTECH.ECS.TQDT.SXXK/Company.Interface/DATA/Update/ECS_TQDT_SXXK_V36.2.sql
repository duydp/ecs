GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name='LoaiDinhMuc')
	ALTER TABLE dbo.t_KDT_SXXK_DinhMucDangKy
	ADD LoaiDinhMuc INT DEFAULT (0) NULL

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaDaiLy varchar(14),
	@TrangThaiXuLy int,
	@SoTiepNhanChungTu bigint,
	@SoDinhMuc int,
	@NgayDangKy datetime,
	@NgayApDung datetime,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus int,
	@GuidReference nvarchar(500),
	@NamDK smallint,
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@Huongdan_PL nvarchar(500),
	@LoaiDinhMuc int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_DinhMucDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[TrangThaiXuLy],
	[SoTiepNhanChungTu],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[LoaiDinhMuc]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@MaDaiLy,
	@TrangThaiXuLy,
	@SoTiepNhanChungTu,
	@SoDinhMuc,
	@NgayDangKy,
	@NgayApDung,
	@GUIDSTR,
	@DeXuatKhac,
	@LyDoSua,
	@ActionStatus,
	@GuidReference,
	@NamDK,
	@HUONGDAN,
	@PhanLuong,
	@Huongdan_PL,
	@LoaiDinhMuc
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaDaiLy varchar(14),
	@TrangThaiXuLy int,
	@SoTiepNhanChungTu bigint,
	@SoDinhMuc int,
	@NgayDangKy datetime,
	@NgayApDung datetime,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus int,
	@GuidReference nvarchar(500),
	@NamDK smallint,
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@Huongdan_PL nvarchar(500),
	@LoaiDinhMuc int
AS

UPDATE
	[dbo].[t_KDT_SXXK_DinhMucDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[MaDaiLy] = @MaDaiLy,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTiepNhanChungTu] = @SoTiepNhanChungTu,
	[SoDinhMuc] = @SoDinhMuc,
	[NgayDangKy] = @NgayDangKy,
	[NgayApDung] = @NgayApDung,
	[GUIDSTR] = @GUIDSTR,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ActionStatus] = @ActionStatus,
	[GuidReference] = @GuidReference,
	[NamDK] = @NamDK,
	[HUONGDAN] = @HUONGDAN,
	[PhanLuong] = @PhanLuong,
	[Huongdan_PL] = @Huongdan_PL,
	[LoaiDinhMuc] = @LoaiDinhMuc
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaDaiLy varchar(14),
	@TrangThaiXuLy int,
	@SoTiepNhanChungTu bigint,
	@SoDinhMuc int,
	@NgayDangKy datetime,
	@NgayApDung datetime,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus int,
	@GuidReference nvarchar(500),
	@NamDK smallint,
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@Huongdan_PL nvarchar(500),
	@LoaiDinhMuc int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_DinhMucDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_DinhMucDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[MaDaiLy] = @MaDaiLy,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTiepNhanChungTu] = @SoTiepNhanChungTu,
			[SoDinhMuc] = @SoDinhMuc,
			[NgayDangKy] = @NgayDangKy,
			[NgayApDung] = @NgayApDung,
			[GUIDSTR] = @GUIDSTR,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ActionStatus] = @ActionStatus,
			[GuidReference] = @GuidReference,
			[NamDK] = @NamDK,
			[HUONGDAN] = @HUONGDAN,
			[PhanLuong] = @PhanLuong,
			[Huongdan_PL] = @Huongdan_PL,
			[LoaiDinhMuc] = @LoaiDinhMuc
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_DinhMucDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[MaDaiLy],
			[TrangThaiXuLy],
			[SoTiepNhanChungTu],
			[SoDinhMuc],
			[NgayDangKy],
			[NgayApDung],
			[GUIDSTR],
			[DeXuatKhac],
			[LyDoSua],
			[ActionStatus],
			[GuidReference],
			[NamDK],
			[HUONGDAN],
			[PhanLuong],
			[Huongdan_PL],
			[LoaiDinhMuc]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@MaDaiLy,
			@TrangThaiXuLy,
			@SoTiepNhanChungTu,
			@SoDinhMuc,
			@NgayDangKy,
			@NgayApDung,
			@GUIDSTR,
			@DeXuatKhac,
			@LyDoSua,
			@ActionStatus,
			@GuidReference,
			@NamDK,
			@HUONGDAN,
			@PhanLuong,
			@Huongdan_PL,
			@LoaiDinhMuc
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_DinhMucDangKy]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_DinhMucDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[TrangThaiXuLy],
	[SoTiepNhanChungTu],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[LoaiDinhMuc]
FROM
	[dbo].[t_KDT_SXXK_DinhMucDangKy]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[TrangThaiXuLy],
	[SoTiepNhanChungTu],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[LoaiDinhMuc]
FROM [dbo].[t_KDT_SXXK_DinhMucDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_SelectAll]





















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaDaiLy],
	[TrangThaiXuLy],
	[SoTiepNhanChungTu],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[LoaiDinhMuc]
FROM
	[dbo].[t_KDT_SXXK_DinhMucDangKy]	

GO



IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '36.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('36.2',GETDATE(), N'CẬP NHẬT SỬA KHAI BÁO ĐỊNH MỨC THEO TT MỚI')
END