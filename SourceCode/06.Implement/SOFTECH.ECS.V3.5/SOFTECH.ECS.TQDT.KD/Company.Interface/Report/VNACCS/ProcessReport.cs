﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.Interface.Report;
using System.Data;
using Company.KDT.SHARE.VNACCS.Messages.Recived;

namespace Company.Interface
{
    public class ProcessReport
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public static void ShowReport(long idMsg, string MaNghiepVu)
        {
            ShowReport(idMsg, MaNghiepVu, null);
        }

        public static void ShowReport(long idMsg, string MaNghiepVu,ReturnMessages rMsg)
        {
            MsgLog Log = MsgLog.Load(idMsg);
            if (Log != null)
            {
                try
                {
                    if (rMsg == null)
                        rMsg = new ReturnMessages(Log.Log_Messages);
                    if (rMsg.Body == null)
                    {
                        string[] lines = Log.Log_Messages.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                        rMsg = new ReturnMessages(lines);
                    }
                    if (rMsg != null)
                    {
                        if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanXacNhan.Contains(MaNghiepVu))
                        {

                            VAE1LD0 vae = new VAE1LD0();
                            vae.LoadVAE1LD0(rMsg.Body.ToString());
                            ReportViewVNACCSToKhaiXuat f = new ReportViewVNACCSToKhaiXuat(false);
                            f.VAE = vae;
                            f.maNV = MaNghiepVu;
                            if (MaNghiepVu == "VAE3LE0")
                                f.IsKhaiBaoSua = true;
                            else
                                f.IsKhaiBaoSua = false;
                            f.ShowDialog();
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanChapNhanThongQuan.Contains(MaNghiepVu))
                        {
                            VAE1LF0 vae1lf = new VAE1LF0();
                            vae1lf.LoadVAE1LF0(rMsg.Body.ToString());
                            ReportViewVNACCSToKhaiXuat f = new ReportViewVNACCSToKhaiXuat(true);
                            f.IsThongQuan = true;
                            f.maNV = MaNghiepVu;
                            f.VAE1LF = vae1lf;
                            f.ShowDialog();

                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanXacNhan.Contains(MaNghiepVu))
                        {
                            VAD1AC0 vad = new VAD1AC0();
                            vad.LoadVAD1AC0(rMsg.Body.ToString());
                            ReportViewVNACCSToKhaiNhap f = new ReportViewVNACCSToKhaiNhap(false);
                            f.Vad1ac0 = vad;
                            f.maNV = MaNghiepVu;
                            if (MaNghiepVu == "VAD2AE0") f.IsKhaiBaoSua = true; else f.IsKhaiBaoSua = false;
                            f.ShowDialog();

                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanChapNhanThongQuan.Contains(MaNghiepVu))
                        {
                            VAD1AG0 vad1ag = new VAD1AG0();
                            vad1ag.LoadVAD1AG0(rMsg.Body.ToString());
                            ReportViewVNACCSToKhaiNhap f = new ReportViewVNACCSToKhaiNhap(true);
                            f.IsThongQuan = true;
                            f.maNV = MaNghiepVu;
                            f.Vad1ag = vad1ag;
                            //f.GetAllReport(f.IsThongQuan);
                            //f.ExportAll_Pdf_ItemClick(null, null);
                            f.ShowDialog();
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiBiSung_BanXacNhan.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAF8010")
                            {
                                VAF8010 vaf801 = new VAF8010();
                                vaf801.LoadVAF8010(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vaf801 = vaf801;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                            else if (MaNghiepVu == "VAF8020")
                            {
                                VAF8020 vaf802 = new VAF8020();
                                vaf802.LoadVAF8020(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vaf802 = vaf802;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                            else if (MaNghiepVu == "VAF8030")
                            {
                                VAF8030 vaf803 = new VAF8030();
                                vaf803.LoadVAF8030(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vaf803 = vaf803;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                            else if (MaNghiepVu == "VAF8040")
                            {
                                VAF8040 vaf804 = new VAF8040();
                                vaf804.LoadVAF8040(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vaf804 = vaf804;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                            else if (MaNghiepVu == "VAD4870" || MaNghiepVu == "VAE4740")
                            {
                                VAD4870 vad487 = new VAD4870();
                                vad487.LoadVAD4870(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vad487 = vad487;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }



                        }
                        else if (MaNghiepVu == EnumNghiepVuPhanHoi.VAD8000)
                        {
                            VAD8000 vad = new VAD8000();
                            vad.GetObject<VAD8000>(rMsg.Body.ToString(), EnumNghiepVuPhanHoi.VAD8000, false, VAD8000.TongSoByte);
                            ReportViewVNACCS f = new ReportViewVNACCS(false);
                            f.vad8000 = vad;
                            f.manghiepvu = MaNghiepVu;
                            f.ShowDialog();
                        }
                        else if (MaNghiepVu == EnumNghiepVuPhanHoi.VAE8000)
                        {
                            VAE8000 vae = new VAE8000();
                            vae.GetObject<VAE8000>(rMsg.Body.ToString(), EnumNghiepVuPhanHoi.VAE8000, false, VAE8000.TongSoByte);
                            ReportViewVNACCS f = new ReportViewVNACCS(false);
                            f.Vae8000 = vae;
                            f.manghiepvu = MaNghiepVu;
                            f.ShowDialog();
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_HoaDon_HuongDan.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAL0870")
                            {
                                VAL0870_IVA val087 = new VAL0870_IVA();
                                val087.LoadVAL0870_IVA(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Val087_IVA = val087;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_TKVC_PhanHoiPheDuyet.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAS5040")
                            {
                                VAS5050 vas = new VAS5050();
                                vas.LoadVAS5050(rMsg.Body.ToString());

                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vas5050 = vas;
                                f.manghiepvu = MaNghiepVu;
                                //f.TongSoTrang = TongSoTrang;
                                f.ShowDialog();
                            }
                        }
                        else if (MaNghiepVu == EnumNghiepVuPhanHoi.VAS501)
                        {
                            VAS501 vas = new VAS501();
                            vas.LoadVAS501(rMsg.Body.ToString());
                            ReportViewVNACCS f = new ReportViewVNACCS(false);
                            f.Vas5010 = vas;
                            f.manghiepvu = MaNghiepVu;
                            //f.TongSoTrang = TongSoTrang;
                            f.ShowDialog();
                        }
                        else if (MaNghiepVu == EnumNghiepVuPhanHoi.VAS5030)
                        {
                            VAS5030 vas = new VAS5030();
                            vas.LoadVAS5030(rMsg.Body.ToString());
                            ReportViewVNACCS f = new ReportViewVNACCS(false);
                            f.Vas5030 = vas;
                            f.manghiepvu = MaNghiepVu;
                            //f.TongSoTrang = TongSoTrang;
                            f.ShowDialog();
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ThongTinTKDinhKem.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAL0020" || MaNghiepVu == "VAL0040")
                            {
                                VAL0020 Val0020 = new VAL0020();
                                Val0020.LoadVAL0020(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Val0020 = Val0020;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ThongTinTKDinhKem.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAL2800" || MaNghiepVu == "VAL2900")
                            {
                                VAL2800 Val2800 = new VAL2800();
                                Val2800.LoadVAL2800(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Val2800 = Val2800;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_HoaDon_HuongDan.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAL0870")
                            {
                                VAL0870_IVA val087 = new VAL0870_IVA();
                                val087.LoadVAL0870_IVA(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Val087_IVA = val087;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_HoaDon_HuongDan.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAL0870")
                            {
                                VAL0870_IVA val087 = new VAL0870_IVA();
                                val087.LoadVAL0870_IVA(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Val087_IVA = val087;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_HoaDon_HuongDan.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAL0870")
                            {
                                VAL0870_IVA val087 = new VAL0870_IVA();
                                val087.LoadVAL0870_IVA(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Val087_IVA = val087;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiBoSung_BanChapNhanThongQuan.Contains(MaNghiepVu))
                        {
                            VAL8000 val800 = new VAL8000();
                            val800.LoadVAL8000(rMsg.Body.ToString());
                            ReportViewVNACCS f = new ReportViewVNACCS(false);
                            f.Val8000 = val800;
                            f.manghiepvu = MaNghiepVu;
                            f.ShowDialog();

                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_DMMT.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAE8010" || MaNghiepVu == "VAD8010")
                            {
                                VAD8010 vad8010 = new VAD8010();
                                vad8010.LoadVAD8010(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.vad8010 = vad8010;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                            if (MaNghiepVu == "VAD8020")
                            {
                                VAD8020 vad8020 = new VAD8020();
                                vad8020.LoadVAD8020(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.vad8020 = vad8020;
                                //VAD8010 vad8010 = new VAD8010();
                                //vad8010.LoadVAD8010(rMsg.Body.ToString());
                                //ReportViewVNACCS f = new ReportViewVNACCS(false);
                                //f.vad8010 = vad8010;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                            if (MaNghiepVu == "VAD8030")
                            {
                                VAD8030 vad8020 = new VAD8030();
                                vad8020.LoadVAD8030(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.vad8030 = vad8020;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_GiayPhep_BanXacNhan.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAHP010")
                            {
                                VAHP010 vahp = new VAHP010();
                                vahp.LoadVAHP010(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vahp010 = vahp;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                            if (MaNghiepVu == "VAGP010")
                            {
                                VAGP010 vagp = new VAGP010();
                                vagp.LoadVAGP010(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vagp010 = vagp;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                            if (MaNghiepVu == "VAJP010")
                            {
                                VAJP010 vajp = new VAJP010();
                                vajp.LoadVAJP010(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vajp010 = vajp;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                            if (MaNghiepVu == "VAGP110")
                            {
                                VAGP110 vagp = new VAGP110();
                                vagp.LoadVAGP110(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vagp110 = vagp;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }




                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_GiayPhep_BanDuyet.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAHP030")
                            {
                                VAHP030 vahp = new VAHP030();
                                vahp.LoadVAHP030(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vahp030 = vahp;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }
                        else if (EnumNghiepVuPhanHoi.GroupPhanHoi_GiayPhep_HuongDan.Contains(MaNghiepVu))
                        {
                            if (MaNghiepVu == "VAHP060")
                            {
                                VAHP060 vahp = new VAHP060();
                                vahp.LoadVAHP060(rMsg.Body.ToString());
                                ReportViewVNACCS f = new ReportViewVNACCS(false);
                                f.Vahp060 = vahp;
                                f.manghiepvu = MaNghiepVu;
                                f.ShowDialog();
                            }
                        }
                    }

                }
                catch (System.Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }


            }




        }
        public static void ShowReport(KDT_VNACC_ToKhaiMauDich TKMD, string MaNghiepVu)
        {
            if (MaNghiepVu == "ReportViewVNACCSToKhaiNhap")
            {
                ReportViewVNACCSToKhaiNhap f = new ReportViewVNACCSToKhaiNhap(false);
                f.tkmd = TKMD;
                f.maNV = MaNghiepVu;
                f.IsPrint = true;
                f.ShowDialog();
            }
            else if (MaNghiepVu == "ReportViewVNACCSToKhaiXuat")
            {
                ReportViewVNACCSToKhaiXuat f = new ReportViewVNACCSToKhaiXuat(false);
                f.tkmd = TKMD;
                f.maNV = MaNghiepVu;
                f.IsPrint = true;
                f.ShowDialog();
            }
        }
        public static   DataTable ConvertListToTable( GroupAttribute group, int loop)
        {
            int STT = 0;
            DataTable gr = new DataTable();
            foreach (PropertiesAttribute attribute in group.listAttribute)
            {
                gr.Columns.Add(attribute.GroupID, attribute.OfType == typeof(int) ? typeof(decimal) : attribute.OfType );
            }
            gr.Columns.Add("STT", typeof(string));
            for (int i = 0; i < loop; i++)
            {
                STT++;
                DataRow dr = gr.NewRow();                       
                foreach (PropertiesAttribute attribute in group.listAttribute)
                {
                    
                    if (attribute.OfType == typeof(DateTime))
                    {
                        if (System.Convert.ToDateTime(attribute.GetValueCollection(i)).Year > 1900)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else if (attribute.OfType == typeof(decimal) || attribute.OfType == typeof(int))
                    {
                        if (System.Convert.ToDecimal(attribute.GetValueCollection(i)) != 0)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else
                        dr[attribute.GroupID] = attribute.GetValueCollection(i);
                   
                }
                dr["STT"] = STT.ToString().ToUpper();
                gr.Rows.Add(dr);
            }
            return gr;
        }



    }
}
