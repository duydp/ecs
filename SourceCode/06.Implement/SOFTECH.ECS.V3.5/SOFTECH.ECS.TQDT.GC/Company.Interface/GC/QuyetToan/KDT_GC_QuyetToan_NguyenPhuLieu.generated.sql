﻿-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_DeleteBy_HopDong_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Insert]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Insert]
	@HopDong_ID bigint,
	@MaNPL varchar(30),
	@TenNPL nvarchar(80),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@LuongNhapDK numeric(18, 6),
	@TriGiaNhapDK numeric(18, 6),
	@LuongCungUngDK numeric(18, 6),
	@TriGiaCungUngDK numeric(18, 6),
	@LuongHuyDK numeric(18, 6),
	@TriGiaHuyDK numeric(18, 6),
	@LuongSuDungDK numeric(18, 6),
	@TriGiaSuDungDK numeric(18, 6),
	@LuongNhapTK numeric(18, 6),
	@TriGiaNhapTK numeric(18, 6),
	@LuongCungUngTK numeric(18, 6),
	@TriGiaCungUngTK numeric(18, 6),
	@LuongHuyTK numeric(18, 6),
	@TriGiaHuyTK numeric(18, 6),
	@LuongSuDungTK numeric(18, 6),
	@TriGiaSuDungTK numeric(18, 6),
	@LuongTonCK numeric(18, 6),
	@TriGiaTonCK numeric(18, 6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu]
(
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongNhapDK],
	[TriGiaNhapDK],
	[LuongCungUngDK],
	[TriGiaCungUngDK],
	[LuongHuyDK],
	[TriGiaHuyDK],
	[LuongSuDungDK],
	[TriGiaSuDungDK],
	[LuongNhapTK],
	[TriGiaNhapTK],
	[LuongCungUngTK],
	[TriGiaCungUngTK],
	[LuongHuyTK],
	[TriGiaHuyTK],
	[LuongSuDungTK],
	[TriGiaSuDungTK],
	[LuongTonCK],
	[TriGiaTonCK]
)
VALUES 
(
	@HopDong_ID,
	@MaNPL,
	@TenNPL,
	@MaHS,
	@DVT_ID,
	@LuongNhapDK,
	@TriGiaNhapDK,
	@LuongCungUngDK,
	@TriGiaCungUngDK,
	@LuongHuyDK,
	@TriGiaHuyDK,
	@LuongSuDungDK,
	@TriGiaSuDungDK,
	@LuongNhapTK,
	@TriGiaNhapTK,
	@LuongCungUngTK,
	@TriGiaCungUngTK,
	@LuongHuyTK,
	@TriGiaHuyTK,
	@LuongSuDungTK,
	@TriGiaSuDungTK,
	@LuongTonCK,
	@TriGiaTonCK
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Update]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Update]
	@ID bigint,
	@HopDong_ID bigint,
	@MaNPL varchar(30),
	@TenNPL nvarchar(80),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@LuongNhapDK numeric(18, 6),
	@TriGiaNhapDK numeric(18, 6),
	@LuongCungUngDK numeric(18, 6),
	@TriGiaCungUngDK numeric(18, 6),
	@LuongHuyDK numeric(18, 6),
	@TriGiaHuyDK numeric(18, 6),
	@LuongSuDungDK numeric(18, 6),
	@TriGiaSuDungDK numeric(18, 6),
	@LuongNhapTK numeric(18, 6),
	@TriGiaNhapTK numeric(18, 6),
	@LuongCungUngTK numeric(18, 6),
	@TriGiaCungUngTK numeric(18, 6),
	@LuongHuyTK numeric(18, 6),
	@TriGiaHuyTK numeric(18, 6),
	@LuongSuDungTK numeric(18, 6),
	@TriGiaSuDungTK numeric(18, 6),
	@LuongTonCK numeric(18, 6),
	@TriGiaTonCK numeric(18, 6)
AS

UPDATE
	[dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu]
SET
	[HopDong_ID] = @HopDong_ID,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[LuongNhapDK] = @LuongNhapDK,
	[TriGiaNhapDK] = @TriGiaNhapDK,
	[LuongCungUngDK] = @LuongCungUngDK,
	[TriGiaCungUngDK] = @TriGiaCungUngDK,
	[LuongHuyDK] = @LuongHuyDK,
	[TriGiaHuyDK] = @TriGiaHuyDK,
	[LuongSuDungDK] = @LuongSuDungDK,
	[TriGiaSuDungDK] = @TriGiaSuDungDK,
	[LuongNhapTK] = @LuongNhapTK,
	[TriGiaNhapTK] = @TriGiaNhapTK,
	[LuongCungUngTK] = @LuongCungUngTK,
	[TriGiaCungUngTK] = @TriGiaCungUngTK,
	[LuongHuyTK] = @LuongHuyTK,
	[TriGiaHuyTK] = @TriGiaHuyTK,
	[LuongSuDungTK] = @LuongSuDungTK,
	[TriGiaSuDungTK] = @TriGiaSuDungTK,
	[LuongTonCK] = @LuongTonCK,
	[TriGiaTonCK] = @TriGiaTonCK
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_InsertUpdate]
	@ID bigint,
	@HopDong_ID bigint,
	@MaNPL varchar(30),
	@TenNPL nvarchar(80),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@LuongNhapDK numeric(18, 6),
	@TriGiaNhapDK numeric(18, 6),
	@LuongCungUngDK numeric(18, 6),
	@TriGiaCungUngDK numeric(18, 6),
	@LuongHuyDK numeric(18, 6),
	@TriGiaHuyDK numeric(18, 6),
	@LuongSuDungDK numeric(18, 6),
	@TriGiaSuDungDK numeric(18, 6),
	@LuongNhapTK numeric(18, 6),
	@TriGiaNhapTK numeric(18, 6),
	@LuongCungUngTK numeric(18, 6),
	@TriGiaCungUngTK numeric(18, 6),
	@LuongHuyTK numeric(18, 6),
	@TriGiaHuyTK numeric(18, 6),
	@LuongSuDungTK numeric(18, 6),
	@TriGiaSuDungTK numeric(18, 6),
	@LuongTonCK numeric(18, 6),
	@TriGiaTonCK numeric(18, 6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu] 
		SET
			[HopDong_ID] = @HopDong_ID,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[LuongNhapDK] = @LuongNhapDK,
			[TriGiaNhapDK] = @TriGiaNhapDK,
			[LuongCungUngDK] = @LuongCungUngDK,
			[TriGiaCungUngDK] = @TriGiaCungUngDK,
			[LuongHuyDK] = @LuongHuyDK,
			[TriGiaHuyDK] = @TriGiaHuyDK,
			[LuongSuDungDK] = @LuongSuDungDK,
			[TriGiaSuDungDK] = @TriGiaSuDungDK,
			[LuongNhapTK] = @LuongNhapTK,
			[TriGiaNhapTK] = @TriGiaNhapTK,
			[LuongCungUngTK] = @LuongCungUngTK,
			[TriGiaCungUngTK] = @TriGiaCungUngTK,
			[LuongHuyTK] = @LuongHuyTK,
			[TriGiaHuyTK] = @TriGiaHuyTK,
			[LuongSuDungTK] = @LuongSuDungTK,
			[TriGiaSuDungTK] = @TriGiaSuDungTK,
			[LuongTonCK] = @LuongTonCK,
			[TriGiaTonCK] = @TriGiaTonCK
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu]
		(
			[HopDong_ID],
			[MaNPL],
			[TenNPL],
			[MaHS],
			[DVT_ID],
			[LuongNhapDK],
			[TriGiaNhapDK],
			[LuongCungUngDK],
			[TriGiaCungUngDK],
			[LuongHuyDK],
			[TriGiaHuyDK],
			[LuongSuDungDK],
			[TriGiaSuDungDK],
			[LuongNhapTK],
			[TriGiaNhapTK],
			[LuongCungUngTK],
			[TriGiaCungUngTK],
			[LuongHuyTK],
			[TriGiaHuyTK],
			[LuongSuDungTK],
			[TriGiaSuDungTK],
			[LuongTonCK],
			[TriGiaTonCK]
		)
		VALUES 
		(
			@HopDong_ID,
			@MaNPL,
			@TenNPL,
			@MaHS,
			@DVT_ID,
			@LuongNhapDK,
			@TriGiaNhapDK,
			@LuongCungUngDK,
			@TriGiaCungUngDK,
			@LuongHuyDK,
			@TriGiaHuyDK,
			@LuongSuDungDK,
			@TriGiaSuDungDK,
			@LuongNhapTK,
			@TriGiaNhapTK,
			@LuongCungUngTK,
			@TriGiaCungUngTK,
			@LuongHuyTK,
			@TriGiaHuyTK,
			@LuongSuDungTK,
			@TriGiaSuDungTK,
			@LuongTonCK,
			@TriGiaTonCK
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Delete]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Load]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongNhapDK],
	[TriGiaNhapDK],
	[LuongCungUngDK],
	[TriGiaCungUngDK],
	[LuongHuyDK],
	[TriGiaHuyDK],
	[LuongSuDungDK],
	[TriGiaSuDungDK],
	[LuongNhapTK],
	[TriGiaNhapTK],
	[LuongCungUngTK],
	[TriGiaCungUngTK],
	[LuongHuyTK],
	[TriGiaHuyTK],
	[LuongSuDungTK],
	[TriGiaSuDungTK],
	[LuongTonCK],
	[TriGiaTonCK]
FROM
	[dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongNhapDK],
	[TriGiaNhapDK],
	[LuongCungUngDK],
	[TriGiaCungUngDK],
	[LuongHuyDK],
	[TriGiaHuyDK],
	[LuongSuDungDK],
	[TriGiaSuDungDK],
	[LuongNhapTK],
	[TriGiaNhapTK],
	[LuongCungUngTK],
	[TriGiaCungUngTK],
	[LuongHuyTK],
	[TriGiaHuyTK],
	[LuongSuDungTK],
	[TriGiaSuDungTK],
	[LuongTonCK],
	[TriGiaTonCK]
FROM
	[dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongNhapDK],
	[TriGiaNhapDK],
	[LuongCungUngDK],
	[TriGiaCungUngDK],
	[LuongHuyDK],
	[TriGiaHuyDK],
	[LuongSuDungDK],
	[TriGiaSuDungDK],
	[LuongNhapTK],
	[TriGiaNhapTK],
	[LuongCungUngTK],
	[TriGiaCungUngTK],
	[LuongHuyTK],
	[TriGiaHuyTK],
	[LuongSuDungTK],
	[TriGiaSuDungTK],
	[LuongTonCK],
	[TriGiaTonCK]
FROM [dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectAll]
-- Database: ECS_TQDT_GC_V4_TLTH_06_09_2016
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 28, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectAll]
























AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongNhapDK],
	[TriGiaNhapDK],
	[LuongCungUngDK],
	[TriGiaCungUngDK],
	[LuongHuyDK],
	[TriGiaHuyDK],
	[LuongSuDungDK],
	[TriGiaSuDungDK],
	[LuongNhapTK],
	[TriGiaNhapTK],
	[LuongCungUngTK],
	[TriGiaCungUngTK],
	[LuongHuyTK],
	[TriGiaHuyTK],
	[LuongSuDungTK],
	[TriGiaSuDungTK],
	[LuongTonCK],
	[TriGiaTonCK]
FROM
	[dbo].[t_KDT_GC_QuyetToan_NguyenPhuLieu]	

GO

