﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Feedback.Components;

namespace Company.Interface.Feedback
{
    public partial class LinhVucForm : TuDienBaseForm
    {
        private DataTable dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];

        public LinhVucForm()
        {
            InitializeComponent();

            this.Load += new EventHandler(Form_Load);
            this.FormClosing += new FormClosingEventHandler(Form_FormClosing);
            this.btnReset.Click += new EventHandler(btnReset_Click);
            this.btnSave.Click += new EventHandler(btnSave_Click);
            this.btnDelete.Click += new EventHandler(btnDelete_Click);
            this.dgList.DeletingRecords += new CancelEventHandler(dgList_DeletingRecords);
            this.dgList.UpdatingCell += new Janus.Windows.GridEX.UpdatingCellEventHandler(dgList_UpdatingCell);
        }

        private void Form_Load(object sender, EventArgs e)
        {
            base.Title = Text;

            LoadData();
        }

        void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (base.IsChanged)
            {
                if (Globals.ShowMessage("Bạn chưa lưu thay đổi thông tin trên form, bạn có muốn lưu không?", true) == "Yes")
                {
                    Save();
                }
            }
        }

        private void LoadData()
        {

            dgList.DataSource = dt;
            CreateColumnCombo(dgList);
            dt.RowChanging += new DataRowChangeEventHandler(ds_RowChanging);

            SetChanged(false);
        }

        public void CreateColumnCombo(Janus.Windows.GridEX.GridEX grid)
        {
          
            Janus.Windows.GridEX.GridEXColumn col = grid.RootTable.Columns["ParentID"];
            col.EditType = Janus.Windows.GridEX.EditType.DropDownList;
            col.Caption = "Thư mục cha";
            col.Width = 100;
            
            //Since the column will be bound to a list containing DataRowView objects, we must specify which
            //field in the DataRowView will be used as value 
            col.MultipleValueDataMember = "ID";
           
            //Fill the ValueList with the categories table
            col.HasValueList = true;
            col.ValueList.PopulateValueList(dt.DefaultView, "ID", "Ten");
            col.DefaultGroupInterval = Janus.Windows.GridEX.GroupInterval.Text;
            col.CompareTarget = Janus.Windows.GridEX.ColumnCompareTarget.Text;
            //col.DefaultValue = 0;
        }

        void ds_RowChanging(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Add || e.Action == DataRowAction.Change)
            {
                if (e.Row["Ma"].ToString() == "")
                {
                    e.Row.SetColumnError("Ma", "Mã không được rỗng");
                }
                else
                {
                    e.Row.SetColumnError("Ma", "");
                }

                if (e.Row["Ten"].ToString() == "")
                {
                    e.Row.SetColumnError("Ten", "Tên không được rỗng");
                }
                else
                {
                    e.Row.SetColumnError("Ten", "");
                }

                if (e.Row["ThuTu"].ToString() == "")
                {
                    e.Row.SetColumnError("ThuTu", "Số thứ tự không được rỗng");
                }
                else
                {
                    e.Row.SetColumnError("ThuTu", "");
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            try
            {
                if (dt.HasErrors || dt.Rows.Count == 0)
                    return;

                FeedbackGlobalSettings.LuuDanhMucLinhVuc();

                Globals.ShowMessage("Cập nhật thành công.", false);

                SetChanged(false);
            }
            catch (Exception ex)
            {
                Globals.ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (Globals.ShowMessage("Bạn có muốn xóa các lĩnh vực đã chọn này không?", true) != "Yes") e.Cancel = true;
            else
                XoaDuLieu();
        }

        private bool CheckID(string Id)
        {
            if (dt.Select(" Ma= '" + Id.Trim() + "'").Length == 0) return false;
            else return true;
        }

        private void dgList_UpdatingCell(object sender, Janus.Windows.GridEX.UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "Ma")
            {
                string s = e.Value.ToString();
                if (CheckID(s))
                {
                    Globals.ShowMessage("Mã lĩnh vực này đã có.", false);
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    Globals.ShowMessage("Mã lĩnh vực không được rỗng.", false);
                    e.Cancel = true;
                }
            }

            SetChanged(true);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.Feedback.Components.FeedbackGlobalSettings.LamMoiDanhMucLinhVuc();

            LoadData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Globals.ShowMessage("Bạn có muốn xóa các lĩnh vực này không?", true) == "Yes")
            {
                XoaDuLieu();
            }
        }

        private void XoaDuLieu()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Janus.Windows.GridEX.GridEXSelectedItemCollection items = dgList.SelectedItems;

                if (items.Count == 0)
                    return;

                List<string> listMA = new List<string>();
                DataRow[] rowDelete;
                int cnt = 0;

                foreach (Janus.Windows.GridEX.GridEXSelectedItem i in items)
                {
                    if (i.RowType == Janus.Windows.GridEX.RowType.Record)
                    {
                        DataRowView drv = (DataRowView)i.GetRow().DataRow;

                        listMA.Add( drv["ID"].ToString());
                    }
                }

                foreach (string ID in listMA)
                {
                    //Check link data
                    if (CauHoiDoanhNghiep.SelectCollectionBy_LinhVuc(ID).Count > 0)
                    {
                        Globals.ShowMessage("Thông tin bậc/ hệ '" + ID + "' đang được sử dụng trong thông tin Câu hỏi Doanh nghiệp, không thể xóa.", false);
                        break;
                    }
                    else if (ThuVienCauHoi.SelectCollectionBy_LinhVuc_ID(Convert.ToInt32(ID)).Count > 0)
                    {
                        Globals.ShowMessage("Thông tin bậc/ hệ '" + ID + "' đang được sử dụng trong thông tin Thư viện câu hỏi & giải đáp, không thể xóa.", false);
                        break;
                    }

                    //Delete in memory
                    rowDelete = dt.Select("ID = " + ID +" ");

                    if (rowDelete.Length > 0)
                        dt.Rows.Remove(rowDelete[0]);

                    //Delete in database
                    if (FeedbackGlobalSettings.XoaDanhMucLinhVuc(ID))
                        cnt += 1;
                }

                if (cnt > 0)
                    Globals.ShowMessage("Đã thực hiện xóa thành công.", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage(ex.Message, false);
            }
            finally
            {
                Cursor = Cursors.Default;

                LoadData();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
      
    }
}
