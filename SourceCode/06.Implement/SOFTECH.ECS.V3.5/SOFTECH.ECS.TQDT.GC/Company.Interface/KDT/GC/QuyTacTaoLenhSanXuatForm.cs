﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.KDT.GC
{
    public partial class QuyTacTaoLenhSanXuatForm : Company.Interface.BaseForm
    {
        public KDT_LenhSanXuat_QuyTac Config = new KDT_LenhSanXuat_QuyTac();
        public HopDong HD = new HopDong();
        public bool isAdd = true;
        public QuyTacTaoLenhSanXuatForm()
        {
            InitializeComponent();
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HD = new HopDong();
                        HD = (HopDong)i.GetRow().DataRow;
                    }
                }
                txtSoHopDong.Text = HD.SoHopDong.ToString();
                clcYear.Value = HD.NgayKy;
                BindDataConfig();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataConfig()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = KDT_LenhSanXuat_QuyTac.SelectCollectionBy_HopDong_ID(HD.ID);
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = Company.GC.BLL.Globals.GetDanhSachHopDongDaDangKy(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "", 0);
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    Config = new KDT_LenhSanXuat_QuyTac();
                    Config = (KDT_LenhSanXuat_QuyTac)e.Row.DataRow;
                    SetConfigPrefix();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void DoWork(object obj)
        {
            try
            {
                SetError(string.Empty);
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    HopDong HD = (HopDong)item.DataRow;
                    //Tạo cấu hình cho trạng thái lệnh sản xuất là : Tạo mới
                    try
                    {
                        Config = new KDT_LenhSanXuat_QuyTac();
                        Config.TienTo = txtTienTo.Text.ToString().Trim();
                        Config.TinhTrang = "M";
                        Config.LoaiHinh = cbbLoaiHinh.SelectedValue.ToString();
                        Config.GiaTriPhanSo = Convert.ToInt64(txtPhanSo.Text.ToString());
                        Config.DoDaiSo = Convert.ToInt64(txtDoDaiSo.Text.ToString());
                        Config.Nam = HD.NgayKy;
                        Config.SoHopDong = HD.SoHopDong;
                        Config.HopDong_ID = HD.ID;
                        string PrefixM = "";
                        PrefixM = Config.TienTo.ToString().Trim() + Config.TinhTrang.ToString() + Config.LoaiHinh.ToString() + (Config.GiaTriPhanSo + 1) + "-" + Config.Nam.Year.ToString() + "-" + Config.SoHopDong.ToString();
                        Config.HienThi = PrefixM;
                        bool isExits = false;
                        foreach (KDT_LenhSanXuat_QuyTac items in KDT_LenhSanXuat_QuyTac.SelectCollectionBy_HopDong_ID(HD.ID))
                        {
                            isExits = false;
                            if (Config.TinhTrang == items.TinhTrang)
                            {
                                isExits = true;
                                Config.ID = items.ID;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            Config.Insert();
                        }
                        else
                        {
                            Config.Update();
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    //Tạo cấu hình cho trạng thái lệnh sản xuất là : Đang sản xuất
                    try
                    {
                        Config = new KDT_LenhSanXuat_QuyTac();
                        Config.TienTo = txtTienTo.Text.ToString().Trim();
                        Config.TinhTrang = "Đ";
                        Config.LoaiHinh = cbbLoaiHinh.SelectedValue.ToString();
                        Config.GiaTriPhanSo = Convert.ToInt64(txtPhanSo.Text.ToString());
                        Config.DoDaiSo = Convert.ToInt64(txtDoDaiSo.Text.ToString());
                        Config.Nam = HD.NgayKy;
                        Config.SoHopDong = HD.SoHopDong;
                        Config.HopDong_ID = HD.ID;
                        string PrefixĐ = "";
                        PrefixĐ = Config.TienTo.ToString().Trim() + Config.TinhTrang.ToString() + Config.LoaiHinh.ToString() + (Config.GiaTriPhanSo + 1) + "-" + Config.Nam.Year.ToString() + "-" + Config.SoHopDong.ToString();
                        Config.HienThi = PrefixĐ;
                        bool isExits = false;
                        foreach (KDT_LenhSanXuat_QuyTac items in KDT_LenhSanXuat_QuyTac.SelectCollectionBy_HopDong_ID(HD.ID))
                        {
                            isExits = false;
                            if (Config.TinhTrang == items.TinhTrang)
                            {
                                isExits = true;
                                Config.ID = items.ID;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            Config.Insert();
                        }
                        else
                        {
                            Config.Update();
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    //Tạo cấu hình cho trạng thái lệnh sản xuất là : Hoàn thành
                    try
                    {
                        Config = new KDT_LenhSanXuat_QuyTac();
                        Config.TienTo = txtTienTo.Text.ToString().Trim();
                        Config.TinhTrang = "H";
                        Config.LoaiHinh = cbbLoaiHinh.SelectedValue.ToString();
                        Config.GiaTriPhanSo = Convert.ToInt64(txtPhanSo.Text.ToString());
                        Config.DoDaiSo = Convert.ToInt64(txtDoDaiSo.Text.ToString());
                        Config.SoHopDong = HD.SoHopDong;
                        Config.Nam = HD.NgayKy;
                        Config.HopDong_ID = HD.ID;
                        string PrefixH = "";
                        PrefixH = Config.TienTo.ToString().Trim() + Config.TinhTrang.ToString() + Config.LoaiHinh.ToString() + (Config.GiaTriPhanSo + 1) + "-" + Config.Nam.Year.ToString() + "-" + Config.SoHopDong.ToString();
                        Config.HienThi = PrefixH;
                        bool isExits = false;
                        foreach (KDT_LenhSanXuat_QuyTac items in KDT_LenhSanXuat_QuyTac.SelectCollectionBy_HopDong_ID(HD.ID))
                        {
                            isExits = false;
                            if (Config.TinhTrang == items.TinhTrang)
                            {
                                isExits = true;
                                Config.ID = items.ID;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            Config.Insert();
                        }
                        else
                        {
                            Config.Update();
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    SetProcessBar((i * 100 / listChecked.Length));
                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        BindData();
                        bttAuto.Enabled = true; btnClose.Enabled = true;

                    }));
                }
                else
                {
                    BindData();
                    bttAuto.Enabled = true; btnClose.Enabled = true;
                }
                SetProcessBar(0);
                SetError("Hoàn thành");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SetError(string.Empty);
            }  
        }
        private void bttAuto_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(false))
                return;
            bttAuto.Enabled = false;
            btnClose.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtTienTo, errorProvider, "Tiền tố", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtDoDaiSo, errorProvider, "Độ dài số");
                isValid &= ValidateControl.ValidateNull(txtPhanSo, errorProvider, "Phần số");
                isValid &= ValidateControl.ValidateNull(cbbTinhTrang, errorProvider, "Tình trạng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHinh, errorProvider, "Loại hình");
                isValid &= ValidateControl.ValidateNull(clcYear, errorProvider, "Năm");
                isValid &= ValidateControl.ValidateNull(txtSoHopDong, errorProvider, "Số hợp đồng");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void GetConfigPrefix()
        {
            try
            {
                Config.TienTo = txtTienTo.Text.ToString().Trim();
                Config.TinhTrang = cbbTinhTrang.SelectedValue.ToString();
                Config.LoaiHinh = cbbLoaiHinh.SelectedValue.ToString();
                Config.GiaTriPhanSo = Convert.ToInt64(txtPhanSo.Text.ToString());
                Config.DoDaiSo = Convert.ToInt64(txtDoDaiSo.Text.ToString());
                Config.Nam = clcYear.Value;
                Config.SoHopDong = txtSoHopDong.Text;
                Config.HopDong_ID = HD.ID;
                Config.HienThi = txtHienThi.Text.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetConfigPrefix()
        {
            try
            {
                txtTienTo.Text = Config.TienTo.ToString();
                cbbTinhTrang.SelectedValue = Config.TinhTrang.ToString();
                cbbLoaiHinh.SelectedValue = Config.LoaiHinh.ToString();
                txtPhanSo.Text = Config.GiaTriPhanSo.ToString();
                txtDoDaiSo.Text = Config.DoDaiSo.ToString();
                clcYear.Value = Config.Nam;
                txtSoHopDong.Text = Config.SoHopDong.ToString();
                txtHienThi.Text = Config.HienThi.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetConfigPrefix();
                if (isAdd)
                {
                    //Kiểm tra tồn tại Cấu hình chứng từ
                    foreach (KDT_LenhSanXuat_QuyTac item in KDT_LenhSanXuat_QuyTac.SelectCollectionBy_HopDong_ID(HD.ID))
                    {
                        if (item.TinhTrang == cbbTinhTrang.SelectedValue.ToString())
                        {
                            errorProvider.SetError(cbbTinhTrang, "Tình trạng : ''" + cbbTinhTrang.Text.ToString() + "'' này đã được Cấu hình");
                            return;
                        }
                    }
                    Config.Insert();
                }
                else
                {
                    Config.Update();
                }
                BindDataConfig();
                Config = new KDT_LenhSanXuat_QuyTac();
                txtTienTo.Text = String.Empty;
                txtHienThi.Text = String.Empty;
                isAdd = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtTienTo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoHopDong.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbTinhTrang_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoHopDong.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLoaiHinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoHopDong.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtPhanSo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                txtPhanSo.MaxLength = Convert.ToInt32(txtDoDaiSo.Text.ToString()) - 1;
                if (Convert.ToInt32(txtDoDaiSo.Text.ToString()) != txtPhanSo.Text.Length + 1)
                {
                    //ShowMessage("Giá trị phần số vượt quá hoặc nhỏ hơn Độ dài số ", false);
                    errorProvider.SetError(txtPhanSo, setText("Giá trị phần số vượt quá hoặc nhỏ hơn Độ dài số ", "This value must be greater than 0 "));
                    return;
                }
                txtPhanSo.MaxLength = Convert.ToInt32(txtDoDaiSo.Text.ToString());
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoHopDong.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtDoDaiSo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                txtPhanSo.MaxLength = Convert.ToInt32(txtDoDaiSo.Text.ToString()) - 1;
                if (Convert.ToInt32(txtDoDaiSo.Text.ToString()) != txtPhanSo.Text.Length + 1)
                {
                    errorProvider.SetError(txtDoDaiSo, setText("Giá trị Độ dài số vượt quá hoặc nhỏ hơn Giá trị Phần số ", "This value must be greater than 0 "));
                    return;
                }
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoHopDong.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string TinhTrang = e.Row.Cells["TinhTrang"].Value.ToString();
                    switch (TinhTrang)
                    {
                        case "M":
                            e.Row.Cells["TinhTrang"].Text = "Tạo mới";
                            break;
                        case "Đ":
                            e.Row.Cells["TinhTrang"].Text = "Đang sản xuất";
                            break;
                        case "H":
                            e.Row.Cells["TinhTrang"].Text = "Hoàn thành";
                            break;

                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void QuyTacTaoLenhSanXuatForm_Load(object sender, EventArgs e)
        {
            clcYear.ReadOnly = false;
            txtSoHopDong.ReadOnly = false;
            BindData();
        }

        private void txtSoHopDong_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoHopDong.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void clcYear_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) +"-"  + clcYear.Value.Year + "-"+ txtSoHopDong.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["SoHopDong"], ConditionOperator.Contains, txtSoHD.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
