﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;

using Company.Interface.KDT.GC;
using Janus.Windows.ExplorerBar;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.LookAndFeel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report;
using Company.Interface.DanhMucChuan;
using System.IO;
using System.Xml.Serialization;
using Company.QuanTri;
using Company.Interface.QuanTri;
using Company.Interface.GC;
using System.Globalization;
using System.Threading;
using System.Resources;
using Company.Interface.PhongKhai;
using System.Xml;
using Company.KDT.SHARE.Components.Utils;

namespace Company.Interface
{
    public partial class MainForm : BaseForm
    {
        public static int flag = 0;
        public static bool isLoginSuccess = false;
        HtmlDocument docHTML = null;
        WebBrowser wbManin = new WebBrowser();
        public static int versionHD = 0;
        public static int typeLogin = 0;
        private int soLanThucHien = 0;
        public int soLanLayTyGia = 0;
        private FrmQuerySQL frmQuery;

        #region SXXK
        private static QueueForm queueForm = new QueueForm();

        // Tờ khai mậu dịch.
        private ToKhaiMauDichForm tkmdForm;
        private ToKhaiMauDichManageForm tkmdManageForm;
        Company.Interface.GC.ToKhaiMauDichRegistedForm tkmdRegisterForm = new Company.Interface.GC.ToKhaiMauDichRegistedForm();

        #endregion

        //-----------------------------------------------------------------------------------------------

        #region GiaCong
        // Hợp đồng.
        private HopDongRegistedForm hdgcRegistedForm;
        private HopDongManageForm hdgcManageForm;
        private HopDongEditForm hdgcForm;

        //Định mức
        private DinhMucGCManageForm dmgcManageForm;
        private DinhMucGCSendForm dmgcSendForm;
        private DinhMucGCRegistedForm dmgcRegistedForm;

        //Phụ kiện
        private PhuKienGCSendForm pkgcSendForm;
        private PhuKienGCManageForm pkgcManageForm;
        private PhuKienGCRegistedForm pkgcRegistedForm;

        //tờ khai phiếu chuyển tiếp gia công
        private ToKhaiGCChuyenTiepNhapForm tkgcCTNhapForm;
        private ToKhaiGCCTManagerForm theodoiTKGCCTForm;
        private ToKhaiGCCTRegistedForm tkctRegistedForm;

        //Bảng kê NPL cung ứng
        private NPLCungUngTheoTKSendForm nplCUSendForm;
        private NPLCungUngManageForm nplCUSTheoDoiForm;
        private NPLCungUngRegistedForm nplCURegisredForm;




        //Phan bo to khai 
        private ThongkeHangTonForm tkHangTon;
        #endregion

        #region Du lieu Chuan
        private MaHSForm maHSForm;
        private PTTTForm ptttForm;
        private PTVTForm ptvtForm;
        private DonViTinhForm dvtForm;
        private DonViHaiQuanForm dvhqForm;
        private CuaKhauForm ckForm;
        private NguyenTeForm ntForm;
        private DKGHForm dkghForm;
        private NuocForm nuocForm;
        #endregion

        #region Quản trị
        public static ECSPrincipal EcsQuanTri;
        QuanLyNguoiDung NguoiDung;
        QuanLyNhomNguoiDung NhomNguoiDung;
        #endregion Quản trị

        QuanLyMessage qlMess;
        private void khoitao_GiaTriMacDinh()
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            if (versionHD == 0)
            {
                string statusStr1 = setText("Người dùng : " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI), "User : " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Company : {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI));
                string statusStr2 = setText(string.Format("Đơn vị hải quan : {0} - {1}. Service khai báo: {2}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN, GlobalSettings.DiaChiWS), string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN));
                statusBar.Panels["DoanhNghiep"].ToolTipText = statusBar.Panels["DoanhNghiep"].Text = statusStr1;
                statusBar.Panels["HaiQuan"].ToolTipText = statusBar.Panels["HaiQuan"].Text = statusStr2;
            }
            else
            {

                //this.Text = "Thông quan điện tử - Gia công";
                statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                statusBar.Panels["HaiQuan"].Text = statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Đơn vị Hải quan: {0} - {1}. Service khai báo: {2}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN, GlobalSettings.DiaChiWS);
            }


        }
        private void LoadDongBoDuLieuDN()
        {
            LoadDongBoDuLieuForm dongboDNForm = new LoadDongBoDuLieuForm();
            dongboDNForm.ShowDialog();
        }
        public static void LoadECSPrincipal(long id)
        {
            EcsQuanTri = new ECSPrincipal(id);
        }
        public static void ShowQueueForm()
        {
            queueForm.Show();
        }
        public static void AddToQueueForm(HangDoi hd)
        {
            queueForm.HDCollection.Add(hd);
            queueForm.RefreshQueue();
        }
        void wbManin_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                //docHTML = wbManin.Document;
                //HtmlElement itemTyGia = null;
                //foreach (HtmlElement item in docHTML.GetElementsByTagName("table"))
                //{
                //    if (item.GetAttribute("id") == "AutoNumber6")
                //    {
                //        itemTyGia = item;
                //        break;
                //    }
                //}
                //if (itemTyGia == null)
                //{
                //    MainForm.flag = 0;
                //    return;
                //}
                //int i = 1;
                //string stUSD = "";
                //try
                //{
                //    foreach (HtmlElement itemTD in docHTML.GetElementsByTagName("TD"))
                //    {
                //        if (itemTD.InnerText != null)
                //        {

                //        }
                //        if (itemTD.InnerText != null && itemTD.InnerText.IndexOf("1 USD=") > -1)
                //        {
                //            stUSD = itemTD.InnerText.Trim();
                //        }
                //    }
                //    if (stUSD != "")
                //    {
                //        stUSD = stUSD.Substring(6).Trim();
                //        stUSD = stUSD.Substring(0, stUSD.Length - 3).Trim();
                //        Company.GC.BLL.DuLieuChuan.NguyenTe.Update(null, "USD", Convert.ToDecimal(stUSD));

                //    }
                //}
                //catch { }
                //XmlDocument doc = new XmlDocument();
                //XmlElement root = doc.CreateElement("Root");
                //doc.AppendChild(root);
                //foreach (HtmlElement itemTD in itemTyGia.GetElementsByTagName("TR"))
                //{
                //    if (i == 1)
                //    {
                //        i++;
                //        continue;
                //    }
                //    HtmlElementCollection collection = itemTD.GetElementsByTagName("TD");
                //    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                //    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                //    itemMaNT.InnerText = collection[1].InnerText;

                //    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                //    itemTenNT.InnerText = collection[2].InnerText;

                //    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                //    itemTyNT.InnerText = collection[3].InnerText;

                //    itemNT.AppendChild(itemMaNT);
                //    itemNT.AppendChild(itemTenNT);
                //    itemNT.AppendChild(itemTyNT);
                //    root.AppendChild(itemNT);
                //}
                //Company.GC.BLL.DuLieuChuan.NguyenTe.UpdateTyGia(root);
                //doc.Save("TyGia.xml");
                //timer1.Enabled = false;
            }
            catch { MainForm.flag = 0; }
            //timer1.Enabled = false;
        }
        public MainForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            wbManin.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbManin_DocumentCompleted);

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            timer1.Enabled = false;
            timer1.Stop();
        }
        private void ShowLoginForm()
        {
            // this.Hide();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Company.GC.BLL.GC.NguyenPhuLieu abc = new Company.GC.BLL.GC.NguyenPhuLieu();
                //abc.HopDong_ID = 507;
                //abc.Ma = "2";

                //Hungtq complemented 14/12/2010
                timer1.Enabled = false; //Disable timer get Exchange Rate

                string strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                this.Text += " - Build " + strVersion;

                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Application.StartupPath + "\\Config.xml");
                    XmlNode node = doc.SelectSingleNode("Root/Type");
                    MainForm.versionHD = Convert.ToInt32(node.InnerText);
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }


                if (MainForm.versionHD == 2)
                {

                    cmdNhapDuLieuTuDoangNghiep.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdXuatDuLieuPhongKHai.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdXuatDN.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhapXuat.Visible = Janus.Windows.UI.InheritableBoolean.False;

                }
                else
                {
                    cmdXuatDuLieuPhongKHai.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdNhapDuLieuTuDoangNghiep.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdXuatDN.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdNhapXuat.Visible = Janus.Windows.UI.InheritableBoolean.True;
                }
                try
                {
                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecse.jpg");

                    }
                    else
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecsv.png");
                        this.BackgroundImageLayout = ImageLayout.Stretch;
                    }
                    this.BackgroundImageLayout = ImageLayout.Stretch;
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

                this.Hide();
                if (versionHD == 0)
                {
                    Login login = new Login();
                    login.ShowDialog();
                }
                else if (versionHD == 1)
                {
                    Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                    login.ShowDialog();
                }
                else if (versionHD == 2)
                {
                    Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                    login.ShowDialog();
                }
                if (isLoginSuccess)
                {
                    this.Show();
                    
                    //Hungtq updated 21/12/2011. Cau hinh Ecs Express
                    Express();

                    if (versionHD == 0)
                    {
                        if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                        {
                            QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                            //ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //cmbMenu.Commands[0].Commands[0].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //cmbMenu.Commands[0].Commands[1].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //cmbMenu.Commands[0].Commands[2].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[0].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[1].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[2].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            // cmbMenu.Commands[0].Commands[2].Commands[3].Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                    }
                    else if (versionHD == 2)
                    {
                        switch (typeLogin)
                        {
                            case 1: // User Da Cau Hinh
                                this.Show();
                                this.khoitao_GiaTriMacDinh();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 2: // User Chua Cau Hinh
                                this.Show();
                                DangKyForm dangKyForm = new DangKyForm();
                                dangKyForm.ShowDialog();
                                this.khoitao_GiaTriMacDinh();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 3:// Admin
                                this.Hide();
                                CreatAccountForm fAdminForm = new CreatAccountForm();
                                fAdminForm.ShowDialog();
                                this.LoginUserKhac();
                                break;
                        }
                    }
                    //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
                    //{
                    // cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //}
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    vsmMain.DefaultColorScheme = GlobalSettings.GiaoDien.Id;
                    if (GlobalSettings.GiaoDien.Id == "Office2007")
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    this.khoitao_GiaTriMacDinh();


                    //MessageBox.Show("ok");
                    //LoginForm f = new LoginForm();
                    //f.ShowDialog(this);
                    //if (!f.IsLogin)
                    //{
                    //    this.Close();
                    //}
                    if (GlobalSettings.NGAYSAOLUU != null && GlobalSettings.NGAYSAOLUU != "")
                    {
                        string st = GlobalSettings.NGAYSAOLUU;
                        DateTime time = Convert.ToDateTime(GlobalSettings.NGAYSAOLUU);
                        int NHAC_NHO_SAO_LUU = Convert.ToInt32(GlobalSettings.NHAC_NHO_SAO_LUU);
                        TimeSpan time1 = DateTime.Today.Subtract(time);
                        int ngay = NHAC_NHO_SAO_LUU - time1.Days;
                        if (ngay <= 0)
                            ShowBackupAndRestore(true);
                    }

                    Show_LeftPanel();

                    #region active mới
                    try
                    {
                        this.requestActivate_new();
                        int dayInt = 7;
                        int dateTrial = KeySecurity.Active.Install.License.KeyInfo.TrialDays;
                        if (!string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Key))
                        {
                            cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            if (dayInt > dateTrial)
                                ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty Cổ phẩn SOFTECH để tiếp tục sử dụng", false);
                            this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");
                        }
                        else
                        {
                            this.Text += setText(" - Bản dùng thử.", " - Trial.");
                        }

                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        ShowMessageTQDT("Thông Báo", "Lỗi khi kiểm tra bản quyền: " + ex.Message, false);
                        Application.ExitThread();
                        Application.Exit();
                    }



                    #endregion


                    #region active key cũ
                    
                    // linhhtn - thêm thông báo sắp hết hạn sử dụng - 20110309
                    //int dayInt = 7;
                    //int dateTrial = int.Parse(Program.lic.dateTrial);
                    //try
                    //{
                    //    string day = new License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ExpiredDate"));
                    //    dayInt = int.Parse(day);
                    //}
                    //catch (Exception) { }
                    //if (Program.isActivated)
                    //{
                    //    cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //    CultureInfo en = new CultureInfo("en-US");
                    //    for (int k = 0; k < dayInt; k++)
                    //    {
                    //        //Kiem tra Culture, neu la vi-VN -> chuyen doi ve cung 1 chuan en-US.
                    //        string dateString = DateTime.Now.Date.ToString("MM/dd/yyyy");

                    //        if (System.DateTime.Parse(dateString, en).AddDays(k) == System.DateTime.Parse(System.Convert.ToDateTime(Program.lic.dayExpires).ToString("MM/dd/yyyy"), en).Date)
                    //        {
                    //            int ngayConLai = k + 1;
                    //            ShowMessage("Phần mềm còn " + ngayConLai + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", false);
                    //            break;
                    //        }
                    //    }
                    //    this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");

                    //}
                    //else
                    //{
                    //    this.Text += setText(" - Bản dùng thử.", " - Trial.");

                    //    if (dateTrial <= 0)
                    //        this.requestActivate();
                    //    else if (dateTrial <= dayInt)
                    //        ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", false);
                    //}
                    #endregion


                    //if (GlobalSettings.MA_DON_VI == "") //Comment by HungTQ 22/12/2010
                    if (GlobalSettings.MA_DON_VI == "" || GlobalSettings.MA_DON_VI == "?")
                    {
                        ShowThietLapThongTinDNAndHQ();
                    }
                    backgroundWorker1.RunWorkerAsync();

                    if (!(GlobalSettings.LastBackup.Equals("")) && !(GlobalSettings.NHAC_NHO_SAO_LUU.Equals("")))
                        if (Convert.ToDateTime(GlobalSettings.LastBackup).Add(TimeSpan.Parse(GlobalSettings.NHAC_NHO_SAO_LUU)) == DateTime.Today)
                        {
                            ShowBackupAndRestore(true);
                        }
                }
                else
                    Application.Exit();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void requestActivate()
        {
            //if (ShowMessage("Đã hết hạn dùng thử.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phầm Mềm Đà Nẵng để tiếp tục sử dụng", true) == "Yes")
            if (showMsg("MSG_0203082", true) == "Yes")
            {
                ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
                obj.ShowDialog();
                requestActivate();
            }
            else
            {
                Application.Exit();
            }
        }

        //Kiem tra phan quyen hien thi he thong
        private void Show_LeftPanel()
        {
            if (versionHD == 0)
            {
                //Kiem tra permission hop dong
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.XemDuLieu)))
                {
                    this.explorerBar1.Groups[0].Enabled = false;
                    this.explorerBar1.Groups[0].Expanded = false;
                }
                else
                {
                    this.explorerBar1.Groups[0].Enabled = true;
                    this.explorerBar1.Groups[0].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.KhaiDienTu)))
                    this.explorerBar1.Groups[0].Items[0].Enabled = false;
                else
                    this.explorerBar1.Groups[0].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.CapNhatDuLieu)))
                    this.explorerBar1.Groups[0].Items[1].Enabled = false;
                else
                    this.explorerBar1.Groups[0].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleHopDong.XemDuLieu)))
                    this.explorerBar1.Groups[0].Items[2].Enabled = false;
                else
                    this.explorerBar1.Groups[0].Items[2].Enabled = true;
                //Ket thuc kiem tra hop dong

                //Kiem tra permission dinh muc
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.XemDuLieu)))
                {
                    this.explorerBar1.Groups[1].Enabled = false;
                    this.explorerBar1.Groups[1].Expanded = false;
                }
                else
                {
                    this.explorerBar1.Groups[1].Enabled = true;
                    this.explorerBar1.Groups[1].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)))
                    this.explorerBar1.Groups[1].Items[0].Enabled = false;
                else
                    this.explorerBar1.Groups[1].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.CapNhatDuLieu)))
                    this.explorerBar1.Groups[1].Items[1].Enabled = false;
                else
                    this.explorerBar1.Groups[1].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.XemDuLieu)))
                    this.explorerBar1.Groups[1].Items[2].Enabled = false;
                else
                    this.explorerBar1.Groups[1].Items[2].Enabled = true;
                //Ket thuc kiem tra dinh muc

                //Kiem tra permission phu kien
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.XemDuLieu)))
                {
                    this.explorerBar1.Groups[2].Enabled = false;
                    this.explorerBar1.Groups[2].Expanded = false;
                }
                else
                {
                    this.explorerBar1.Groups[2].Enabled = true;
                    this.explorerBar1.Groups[2].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.KhaiDienTu)))
                    this.explorerBar1.Groups[2].Items[0].Enabled = false;
                else
                    this.explorerBar1.Groups[2].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.CapNhatDuLieu)))
                    this.explorerBar1.Groups[2].Items[1].Enabled = false;
                else
                    this.explorerBar1.Groups[2].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.XemDuLieu)))
                    this.explorerBar1.Groups[2].Items[2].Enabled = false;
                else
                    this.explorerBar1.Groups[2].Items[2].Enabled = true;
                //Ket thuc kiem tra phu kien

                //Kiem tra permission phu kien
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.XemDuLieu)))
                {
                    this.explorerBar1.Groups[2].Enabled = false;
                    this.explorerBar1.Groups[2].Expanded = false;
                }
                else
                {
                    this.explorerBar1.Groups[2].Enabled = true;
                    this.explorerBar1.Groups[2].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.KhaiDienTu)))
                    this.explorerBar1.Groups[2].Items[0].Enabled = false;
                else
                    this.explorerBar1.Groups[2].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.CapNhatDuLieu)))
                    this.explorerBar1.Groups[2].Items[1].Enabled = false;
                else
                    this.explorerBar1.Groups[2].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RolePhuKien.XemDuLieu)))
                    this.explorerBar1.Groups[2].Items[2].Enabled = false;
                else
                    this.explorerBar1.Groups[2].Items[2].Enabled = true;
                //Ket thuc kiem tra phu kien

                //Kiem tra permission to khai co HD
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuNhap)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuXuat)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.XemDuLieu)))
                {
                    this.explorerBar1.Groups[3].Enabled = false;
                    this.explorerBar1.Groups[3].Expanded = false;
                }
                else
                {
                    this.explorerBar1.Groups[3].Enabled = true;
                    this.explorerBar1.Groups[3].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuNhap)))
                    this.explorerBar1.Groups[3].Items[0].Enabled = false;
                else
                    this.explorerBar1.Groups[3].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuXuat)))
                    this.explorerBar1.Groups[3].Items[1].Enabled = false;
                else
                    this.explorerBar1.Groups[3].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.CapNhatDuLieu)))
                    this.explorerBar1.Groups[3].Items[2].Enabled = false;
                else
                    this.explorerBar1.Groups[3].Items[2].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.XemDuLieu)))
                    this.explorerBar1.Groups[3].Items[3].Enabled = false;
                else
                    this.explorerBar1.Groups[3].Items[3].Enabled = true;
                //Ket thuc kiem tra to khai co HD

                //Kiem tra permission to khai co GCCT
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.KhaiDienTuNhap)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.KhaiDienTuXuat)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.XemDuLieu)))
                {
                    this.explorerBar1.Groups[4].Enabled = false;
                    this.explorerBar1.Groups[4].Expanded = false;
                }
                else
                {
                    this.explorerBar1.Groups[4].Enabled = true;
                    this.explorerBar1.Groups[4].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.KhaiDienTuNhap)))
                    this.explorerBar1.Groups[4].Items[0].Enabled = false;
                else
                    this.explorerBar1.Groups[4].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.KhaiDienTuXuat)))
                    this.explorerBar1.Groups[4].Items[1].Enabled = false;
                else
                    this.explorerBar1.Groups[4].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.CapNhatDuLieu)))
                    this.explorerBar1.Groups[4].Items[2].Enabled = false;
                else
                    this.explorerBar1.Groups[4].Items[2].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoGCCT.XemDuLieu)))
                    this.explorerBar1.Groups[4].Items[3].Enabled = false;
                else
                    this.explorerBar1.Groups[4].Items[3].Enabled = true;
                //Ket thuc kiem tra to khai co GCCT

                //Kiem tra permission NPL cung ung
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.KhaiDienTu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.XemDuLieu)))
                {
                    this.explorerBar1.Groups[5].Enabled = false;
                    this.explorerBar1.Groups[5].Expanded = false;
                }
                else
                {
                    this.explorerBar1.Groups[5].Enabled = true;
                    this.explorerBar1.Groups[5].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.KhaiDienTu)))
                    this.explorerBar1.Groups[5].Items[0].Enabled = false;
                else
                    this.explorerBar1.Groups[5].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.CapNhatDuLieu)))
                    this.explorerBar1.Groups[5].Items[1].Enabled = false;
                else
                    this.explorerBar1.Groups[5].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleNPLTuCungUng.XemDuLieu)))
                    this.explorerBar1.Groups[5].Items[2].Enabled = false;
                else
                    this.explorerBar1.Groups[5].Items[2].Enabled = true;
                //Ket thuc kiem tra NPLP cung ung

                //Kiem tra permission to khai khong co HD
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.KhaiDienTuNhap)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.KhaiDienTuXuat)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.CapNhatDuLieu)) && !EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.XemDuLieu)))
                {
                    this.explorerBar1.Groups[6].Enabled = false;
                    this.explorerBar1.Groups[6].Expanded = false;
                }
                else
                {
                    this.explorerBar1.Groups[6].Enabled = true;
                    this.explorerBar1.Groups[6].Expanded = true;
                }
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.KhaiDienTuNhap)))
                    this.explorerBar1.Groups[6].Items[0].Enabled = false;
                else
                    this.explorerBar1.Groups[6].Items[0].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.KhaiDienTuXuat)))
                    this.explorerBar1.Groups[6].Items[1].Enabled = false;
                else
                    this.explorerBar1.Groups[6].Items[1].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.CapNhatDuLieu)))
                    this.explorerBar1.Groups[6].Items[2].Enabled = false;
                else
                    this.explorerBar1.Groups[6].Items[2].Enabled = true;
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiKhongCoHD.XemDuLieu)))
                    this.explorerBar1.Groups[6].Items[3].Enabled = false;
                else
                    this.explorerBar1.Groups[6].Items[3].Enabled = true;
                //Ket thuc kiem tra to khai khong co HD
            }
        }
        //Ket thuc phan quyen kiem tra he thong

        private void expSXXK_ItemClick(object sender, ItemEventArgs e)
        {

        }

        private void show_ToKhaiSXXKDangKy()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

        }

        private void show_ToKhaiSXXKManage()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.nhomLoaiHinh = "SX";
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Text = setText("Theo dõi tờ khai SXXK", "Exporting Production declaration tracking");
            tkmdManageForm.Show();
        }

        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiMauDichForm(string nhomLoaiHinh)
        {
            // if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
            //{
            //   ShowMessage("Bạn không có quyền thực hiện chức năng này.", false);
            //   return;
            //}   
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals(nhomLoaiHinh))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdForm = new ToKhaiMauDichForm();
            tkmdForm.OpenType = OpenFormType.Insert;
            // tkmdForm.Name = nhomLoaiHinh;
            tkmdForm.NhomLoaiHinh = nhomLoaiHinh;
            tkmdForm.MdiParent = this;
            tkmdForm.Show();
        }

        //-----------------------------------------------------------------------------------------

        private void show_ToKhaiMauDichManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Show();
        }

        private void expKhaiBao_TheoDoi_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "ThucHienPhanBo":
                    this.ThucHienPhanBoToKhaiXuat();
                    break;
                case "TheoDoiPhanBo":
                    showMsg("MSG_0203055");
                    //ShowMessage("Chức năng đang được xây dựng.", false);
                    break;
            }
        }
        private void show_ToKhaiMauDichRegisterForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new Company.Interface.GC.ToKhaiMauDichRegistedForm();
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.Show();
        }
        private void expGiaCong_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                //Hơp đồng
                case "hdgcNhap":
                    this.show_HopDongForm();
                    break;
                case "hdgcRegisted":
                    this.show_HopDongRegistedForm();
                    break;
                case "hdgcManage":
                    this.show_HopDongManageForm();
                    break;

                //Định mức
                case "dmSend":
                    this.show_DinhMucGCSendForm();
                    break;
                case "dmManage":
                    this.show_DinhMucGCManageForm();
                    break;
                case "dmRegisted":
                    this.show_DinhMucGCRegistedForm();
                    break;

                //Phụ kiện
                case "pkgcNhap":
                    this.show_PhuKienGCSendForm();
                    break;
                case "pkgcManage":
                    this.show_PhuKienGCManageForm();
                    break;
                case "pkgcRegisted":
                    this.show_PhuKienGCRegistedForm();
                    break;

                //Tờ khai có HĐ
                case "tkNhapKhau_GC":
                    this.show_ToKhaiMauDichForm("NGC");
                    break;
                case "tkXuatKhau_GC":
                    this.show_ToKhaiMauDichForm("XGC");
                    break;
                case "tkTheoDoi":
                    this.show_ToKhaiMauDichManageForm();
                    break;
                case "tkDaDangKy":
                    this.show_ToKhaiMauDichRegisterForm();
                    break;

                //Tờ khai GCCT
                case "tkGCCTNhap":
                    this.show_ToKhaiGCChuyenTiepNhap("N");
                    break;
                case "tkGCCTXuat":
                    this.show_ToKhaiGCChuyenTiepNhap("X");
                    break;
                case "theodoiTKCT":
                    this.show_ToKhaiGCCTManagerForm();
                    break;
                case "tkGCCTDaDangKy":
                    this.show_ToKhaiGCCTDaDangKyForm();
                    break;

                //Bảng kê NPL cung ứng
                case "dmCUKhaibao":
                    this.show_KhaiBaoDMCU();
                    break;
                case "dmCUTheoDoi":
                    this.show_TheoDoiDMCU();
                    break;
                case "dmCUDaDangKy":
                    this.show_DaDangKyDMCU();
                    break;



                case "thanhKhoan":
                    this.show_HopDongRegistedForm();
                    break;
                case "theodoiThanhKhoan":
                    this.show_HopDongManageForm();
                    break;

            }
        }

        private void show_DaDangKyDMCU()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NPLCungUngRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplCURegisredForm = new NPLCungUngRegistedForm();
            nplCURegisredForm.MdiParent = this;
            nplCURegisredForm.Show();
        }
        private void show_KhaiBaoDMCU()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NPLCungUngTheoTKSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplCUSendForm = new NPLCungUngTheoTKSendForm();
            nplCUSendForm.MdiParent = this;
            nplCUSendForm.Show();
        }
        private void show_TheoDoiDMCU()
        {

            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NPLCungUngManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplCUSTheoDoiForm = new NPLCungUngManageForm();
            nplCUSTheoDoiForm.MdiParent = this;
            nplCUSTheoDoiForm.Show();
        }
        private void show_CapnhatHSTK()
        {
            //HoSoThanhKhoanForm nplCUSTheoDoiForm ;
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("HoSoThanhKhoanForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            //nplCUSTheoDoiForm = new HoSoThanhKhoanForm();
            //nplCUSTheoDoiForm.MdiParent = this;
            //nplCUSTheoDoiForm.Show();
        }
        private void show_ToKhaiGCCTDaDangKyForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiGCCTRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkctRegistedForm = new ToKhaiGCCTRegistedForm();
            tkctRegistedForm.MdiParent = this;
            tkctRegistedForm.Show();
        }
        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiGCCTManagerForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiGCCTManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            theodoiTKGCCTForm = new ToKhaiGCCTManagerForm();
            theodoiTKGCCTForm.MdiParent = this;
            theodoiTKGCCTForm.Show();
        }

        //-----------------------------------------------------------------------------------------


        //-----------------------------------------------------------------------------------------
        private void show_HopDongRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HopDongRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hdgcRegistedForm = new HopDongRegistedForm();
            hdgcRegistedForm.MdiParent = this;
            hdgcRegistedForm.Show();
        }
        private void show_HopDongManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HopDongManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hdgcManageForm = new HopDongManageForm();
            hdgcManageForm.MdiParent = this;
            hdgcManageForm.Show();
        }
        private void show_HopDongForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HopDongEditForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            hdgcForm = new HopDongEditForm();
            hdgcForm.OpenType = OpenFormType.Insert;
            hdgcForm.MdiParent = this;
            hdgcForm.Show();
        }
        private void show_DinhMucGCSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            dmgcSendForm = new DinhMucGCSendForm();
            f.IsBrowseForm = true;
            f.ShowDialog();
            if (f.HopDongSelected.ID > 0)
            {
                dmgcSendForm.dmDangKy.ID_HopDong = f.HopDongSelected.ID;
                dmgcSendForm.OpenType = OpenFormType.Insert;
                dmgcSendForm.MdiParent = this;
                dmgcSendForm.Show();
            }


        }
        private void show_ToKhaiGCChuyenTiepNhap(string nhomLoaiHinh)
        {
            Form[] forms = this.MdiChildren;

            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TKCX" + nhomLoaiHinh))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkgcCTNhapForm = new ToKhaiGCChuyenTiepNhapForm();
            tkgcCTNhapForm.NhomLoaiHinh = nhomLoaiHinh;
            // tkgcCTNhapForm.Name = "TKCX"+ nhomLoaiHinh;
            tkgcCTNhapForm.OpenType = OpenFormType.Insert;
            tkgcCTNhapForm.MdiParent = this;
            tkgcCTNhapForm.Show();
        }
        //private void show_ToKhaiGCChuyenTiepXuat()
        //{
        //    Form[] forms = this.MdiChildren;
        //    for (int i = 0; i < forms.Length; i++)
        //    {
        //        if (forms[i].Name.ToString().Equals("ToKhaiGCChuyenTiepXuatForm"))
        //        {
        //            forms[i].Activate();
        //            return;
        //        }
        //    }
        //    tkgcCTXuatForm = new ToKhaiGIaCongChuyenTiepXuatForm();
        //    tkgcCTXuatForm.MdiParent = this;
        //    tkgcCTXuatForm.Show();
        //}
        private void show_DinhMucGCManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmgcManageForm = new DinhMucGCManageForm();
            dmgcManageForm.MdiParent = this;
            dmgcManageForm.Show();
        }
        private void show_DinhMucGCRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dmgcRegistedForm = new DinhMucGCRegistedForm();
            dmgcRegistedForm.MdiParent = this;
            dmgcRegistedForm.Show();
        }
        private void show_PhuKienGCSendForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhuKienGCSendForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (f.HopDongSelected.SoHopDong.Trim() != "")
            {
                pkgcSendForm = new PhuKienGCSendForm();
                pkgcSendForm.HD.ID = f.HopDongSelected.ID;
                pkgcSendForm.HD.Load();
                pkgcSendForm.OpenType = OpenFormType.Insert;
                pkgcSendForm.MdiParent = this;
                pkgcSendForm.Show();
            }

        }
        private void show_PhuKienGCManageForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhuKienGCManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            pkgcManageForm = new PhuKienGCManageForm();
            pkgcManageForm.MdiParent = this;
            pkgcManageForm.Show();
        }
        private void show_PhuKienGCRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhuKienGCRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            pkgcRegistedForm = new PhuKienGCRegistedForm();
            pkgcRegistedForm.MdiParent = this;
            pkgcRegistedForm.Show();
        }
        //hangton      

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (queueForm.HDCollection.Count > 0)
            //{
            //    XmlSerializer serializer = new XmlSerializer(typeof(HangDoiCollection));
            //    FileStream fs = new FileStream("HangDoi.xml", FileMode.Create);
            //    serializer.Serialize(fs, queueForm.HDCollection);
            //}
            //queueForm.Dispose();
            //Application.Exit();
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdTLTTDN":
                    {
                        this.Show_ThietLapTTDN();
                    }
                    break;
                case "cmdThoat":
                    {
                        this.Close();
                    }
                    break;
                case "DongBoDuLieu":
                    {
                        this.DongBoDuLieuDN();
                    }
                    break;
                case "cmd2007":
                    if (cmd20071.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    GlobalSettings.Luu_GiaoDien("Office2007");
                    UpdateStyleForAllForm("Office2007");
                    //this.UpdateStyles();

                    break;
                case "cmd2003":
                    if (cmd20031.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    GlobalSettings.Luu_GiaoDien("Office2003");
                    UpdateStyleForAllForm("Office2003");
                    //this.UpdateStyles();
                    break;
                case "cmdEng":
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        try
                        {
                            this.BackgroundImage = System.Drawing.Image.FromFile("ecse.jpg");
                            this.BackgroundImageLayout = ImageLayout.Stretch;
                            this.Text = "Management System for outsourcing - exporting " + Application.ProductVersion;
                            statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = "User: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Enterprise: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                            statusBar.Panels["HaiQuan"].Text = statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                        }
                        catch { }
                        GlobalSettings.NGON_NGU = "1";
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NGON_NGU", 1);
                        CultureInfo culture = new CultureInfo("en-US");
                        //     Thread.CurrentThread.CurrentCulture = culture;
                        //     Thread.CurrentThread.CurrentUICulture = culture;  
                        Form[] forms = this.MdiChildren;
                        for (int i = 0; i < forms.Length; i++)
                        {
                            ((BaseForm)forms[i]).InitCulture("en-US", "Company.Interface.LanguageResource.Language");
                        }
                        this.InitCulture("en-US", "Company.Interface.LanguageResource.Language");
                    }
                    break;
                case "cmdVN":
                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        if (MLMessages("Chương trình sẽ khởi động lại và chuyển giao diện sang tiếng Việt,hãy lưu lại các công việc đang làm.\nBạn muốn chuyển giao diện không?", "MSG_0203092", "", true) == "Yes")
                        {
                            GlobalSettings.NGON_NGU = "0";
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NGON_NGU", 0);
                            CultureInfo culture = new CultureInfo("vi-VN");
                            Application.Restart();
                        }
                    }
                    break;

                case "cmdHelp":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\Huong dan su dung phan mem ECS_TQDT_GC.pdf");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                    }
                    break;
                case "cmdMaHS":
                    ShowMaHSForm();
                    break;
                case "cmdHaiQuan":
                    ShowDVHQForm();
                    break;
                case "cmdNuoc":
                    ShowNuocForm();
                    break;
                case "cmdNguyenTe":
                    ShowNguyenTeForm();
                    break;
                case "cmdPTTT":
                    ShowPTTTForm();
                    break;
                case "cmdPTVT":
                    ShowPTVTForm();
                    break;
                case "cmdDKGH":
                    ShowDKGHForm();
                    break;
                case "cmdDVT":
                    ShowDVTForm();
                    break;
                case "cmdCuaKhau":
                    ShowCuaKhauForm();
                    break;
                case "cmdRestore":
                    ShowRestoreForm();
                    break;
                case "cmdBackUp":
                    ShowBackupAndRestore(true);
                    break;
                case "ThongSoKetNoi":
                    ShowThietLapThongSoKetNoi();
                    break;
                case "TLThongTinDNHQ":
                    ShowThietLapThongTinDNAndHQ();
                    break;
                case "cmdThietLapIn":
                    ShowThietLapInBaoCao();
                    break;
                case "cmdCauHinhToKhai":
                    ShowCauHinhToKhai();
                    break;
                case "QuanLyNguoiDung":
                    ShowQuanLyNguoiDung();
                    break;
                case "QuanLyNhom":
                    ShowQuanLyNhomNguoiDung();
                    break;
                case "LoginUser":
                    LoginUserKhac();
                    break;
                case "cmdChangePass":
                    ChangePassForm();
                    break;
                case "TraCuuMaHS":
                    MaHSForm();
                    break;
                case "cmdAutoUpdate":
                    AutoUpdate("MSG");
                    break;
                case "NhapHDXML":
                    NhapHopDongXML();
                    break;
                case "NhapPKXML":
                    NhapPhuKienXML();
                    break;
                case "NhapDMXML":
                    NhapDinhMucXML();
                    break;
                case "NhapTKMD":
                    this.NhapTKMDXML();
                    break;
                case "NhapTKGCCT":
                    this.NhapTKCTXML();
                    break;
                case "cmdXuatHopDong":
                    XuatHopDongChoPhongKhai();
                    break;
                case "cmdXuatPhuKien":
                    XuatPhuKienChoPhongKhai();
                    break;
                case "cmdXuatDinhMuc":
                    XuatDinhMucChoPhongKhai();
                    break;
                case "XuatTKMD":
                    XuatToKhaiMauDichChoPhongKhai();
                    break;
                case "XuatTKGCCT":
                    XuatToKhaiGCCTChoPhongKhai();
                    break;
                case "cmdAbout":
                    this.dispInfo();
                    break;
                case "cmdActivate":
                    this.dispActivate();
                    break;
                case "cmdDMSPGC":
                    this.ShowFormDMSPGC();
                    break;
                case "NhapXML":
                    this.ShowFormChonThuMuc();
                    break;
                case "cmdCloseMe":
                    this.doCloseMe();
                    break;
                case "cmdCloseAllButMe":
                    this.doCloseAllButMe();
                    break;
                case "cmdCloseAll":
                    this.doCloseAll();
                    break;
                case "cmdXuatDN":
                    this.XuatDuLieuChoDoanhNghiep();
                    break;

                case "cmdNhapHopDong":
                    NhapHopDongTuDoanhNghiep();
                    break;
                case "cmdNhapDinhMuc":
                    NhapDinhMucTuDoanhNghiep();
                    break;
                case "cmdNhapPhuKien":
                    NhapPhuKienTuDoanhNghiep();
                    break;
                case "cmdNhapToKhaiMauDich":
                    NhapToKhaiMauDichTuDoanhNghiep();
                    break;
                case "cmdNhapToKhaiGCCT":
                    NhapToKhaiGCCTTuDoanhNghiep();
                    break;
                case "QuanLyMess":
                    WSForm2 login1 = new WSForm2();
                    login1.ShowDialog();
                    if (WSForm2.IsSuccess == true)
                    {
                        ShowQuanLyMess();
                    }
                    break;
                case "mnuQuerySQL":
                    //DATLMQ comment ngày 01/04/2011
                    //Form[] frms = this.MdiChildren;
                    //for (int i = 0; i < frms.Length; i++)
                    //{
                    //    if (frms[i].Name.ToString().Equals("FrmQuerySQL"))
                    //    {
                    //        frms[i].Activate();
                    //        return;
                    //    }
                    //}

                    WSForm2 login2 = new WSForm2();
                    login2.ShowDialog();
                    if (WSForm2.IsSuccess == true)
                    {
                        //string fileName = Application.StartupPath + "\\MiniSQL\\MiniSqlQuery.exe";
                        //if (System.IO.File.Exists(fileName))
                        //{
                        //    MiniSqlQuery.Core.DbConnectionDefinition conn = new MiniSqlQuery.Core.DbConnectionDefinition();
                        //    conn.ConnectionString = "Data Source=" + GlobalSettings.SERVER_NAME + ";Initial Catalog=" + GlobalSettings.DATABASE_NAME +
                        //                            ";User ID=" + GlobalSettings.USER + ";Password=" + GlobalSettings.PASS;
                        //    conn.Name = GlobalSettings.DATABASE_NAME;
                        //    System.Diagnostics.Process.Start(fileName);
                        //}

                        QuerySQL();
                    }
                    break;
                case "cmdLog":
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\Error.log");
                    break;
            }

        }

        private void QuerySQL()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("FrmQuerySQL"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            frmQuery = new FrmQuerySQL();
            frmQuery.MdiParent = this;
            frmQuery.Show();
        }

        private void Show_ThietLapTTDN()
        {
            DangKyForm fDK = new DangKyForm();
            fDK.ShowDialog();
        }
        private void ShowQuanLyMess()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            qlMess = new QuanLyMessage();
            qlMess.MdiParent = this;
            qlMess.Show();
        }
        /// <summary>
        /// Bốn phương thức dùng để nhập tờ khai chuyển tiếp từ doanh nghiệp
        /// </summary>
        public void NhapToKhaiGCCTTuDoanhNghiep()
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new ToKhaiChuyenTiepCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKCTCollection = (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection)serializer.Deserialize(fs);
                    fs.Close();

                    //KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    //KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    //IDHDCollection = IDHD.SelectCollectionAll();
                    //foreach (ToKhaiChuyenTiep TKCT_EDIT in TKCTCollection)
                    //{
                    //    foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                    //    {
                    //        if (TKCT_EDIT.IDHopDong == IDHD_EDIT.ID_CU)
                    //            TKCT_EDIT.IDHopDong = IDHD_EDIT.ID_MOI;
                    //    }
                    //}

                    string message = "";
                    string stTKCTHopDong = KiemTraTKCTHopDong(TKCTCollection);
                    if (stTKCTHopDong != "")
                    {
                        message += "Hợp đồng " + stTKCTHopDong + "chưa có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (message != "")
                        ShowMessage(message, false);
                    if (TKCTCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
                        {
                            if (TKCT.InsertDongBoDuLieu())
                                i++;
                        }
                        if (i > 0)
                            ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }


        }

        /// <summary>
        /// Bốn phương thức dùng để nhập tờ khai mậu dịch từ doanh nghiệp
        /// </summary>        
        public void NhapToKhaiMauDichTuDoanhNghiep()
        {
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection = new Company.GC.BLL.KDT.ToKhaiMauDichCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.ToKhaiMauDichCollection));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKMDCollection = (Company.GC.BLL.KDT.ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();

                    //KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    //KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    //IDHDCollection = IDHD.SelectCollectionAll();
                    //foreach (ToKhaiMauDich TKMD_EDIT in TKMDCollection)
                    //{
                    //    foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                    //    {
                    //        if (TKMD_EDIT.IDHopDong == IDHD_EDIT.ID_CU)
                    //            TKMD_EDIT.IDHopDong = IDHD_EDIT.ID_MOI;
                    //    }
                    //}

                    string message = "";
                    string stTKMDHopDong = KiemTraTKMDHopDong(TKMDCollection);
                    if (stTKMDHopDong != "")
                    {
                        message += "Hợp đồng " + stTKMDHopDong + ", chưa có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (message != "")
                        ShowMessage(message, false);
                    if (TKMDCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiMauDich TKMD in TKMDCollection)
                        {
                            if (TKMD.InsertDongBoDuLieu())
                                i++;
                        }
                        if (i > 0)
                            ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        /// <summary>
        /// Hai phương thức dùng để nhập định mức từ doanh nghiệp
        /// </summary>
        public void NhapDinhMucTuDoanhNghiep()
        {

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    DinhMucDangKyCollection DMDKCollection = (DinhMucDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string message = "";

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    DinhMucDangKy DMDK = new DinhMucDangKy();
                    foreach (DinhMucDangKy DM_EDIT in DMDKCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (DM_EDIT.ID_HopDong == IDHD_EDIT.ID_CU)
                                DM_EDIT.ID_HopDong = IDHD_EDIT.ID_MOI;
                        }
                    }


                    string stDMHopDong = KiemTraDinhMucHopDong(DMDKCollection);


                    if (stDMHopDong != "")
                    {
                        message += "Hợp đồng có mã " + stDMHopDong + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }

                    if (DMDKCollection.Count != 0)
                    {
                        if (DMDK.InsertUpdate(DMDKCollection))
                            ShowMessage("Nhập thành công " + DMDKCollection.Count.ToString() + " thông tin định mức.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private string KiemTraDinhMucHopDong(DinhMucDangKyCollection DMDKCollection)
        {
            string st = "";
            string mahopdong = "";
            DinhMucDangKyCollection DMDKCollectionHD = new DinhMucDangKyCollection();
            string where = "MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'";
            HopDongCollection HDCollection = new HopDong().SelectCollectionDynamic(where, "");
            foreach (DinhMucDangKy DMDK in DMDKCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (DMDK.SoHopDong.Trim().ToUpper() == HDExist.SoHopDong.Trim().ToUpper())
                    {
                        DMDK.ID_HopDong = HDExist.ID;
                        DMDKCollectionHD.Add(DMDK);
                        break;
                    }
                }
            }
            foreach (DinhMucDangKy DMDK in DMDKCollectionHD)
            {
                DMDKCollection.Remove(DMDK);
            }
            foreach (DinhMucDangKy DMDK in DMDKCollection)
            {
                st += DMDK.SoHopDong + ", ";
            }
            DMDKCollection.Clear();
            foreach (DinhMucDangKy DMDK in DMDKCollectionHD)
            {
                DMDKCollection.Add(DMDK);
            }
            return st;
        }

        /// <summary>
        /// Bốn phương thức dùng để nhập phụ kiện từ doanh nghiệp
        /// </summary>
        private void NhapPhuKienTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.GC.PhuKienDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.GC.BLL.KDT.GC.PhuKienDangKyCollection PKCollection = (Company.GC.BLL.KDT.GC.PhuKienDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();

                    //KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    //KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    //IDHDCollection = IDHD.SelectCollectionAll();
                    //Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                    //foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy PK_EDIT in PKCollection)
                    //{
                    //    foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                    //    {
                    //        if (PK_EDIT.HopDong_ID == IDHD_EDIT.ID_CU)
                    //            PK_EDIT.HopDong_ID = IDHD_EDIT.ID_MOI;
                    //    }
                    //}

                    string message = "";
                    string stPKHopDong = KiemTraPhuKienHopDong(PKCollection);
                    if (stPKHopDong != "")
                    {
                        message += "Hợp đồng có mã " + stPKHopDong + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }

                    if (PKCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy PK in PKCollection)
                        {
                            PK.InsertPhuKienDongBoDuLieu();
                            i++;
                        }
                        if (i > 0)
                            ShowMessage("Nhập thành công " + i + " phụ kiện.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        public string KiemTraPhuKienHopDong(PhuKienDangKyCollection PKCollection)
        {
            string st = "";
            string mahopdong = "";
            PhuKienDangKyCollection PKCollectionHD = new PhuKienDangKyCollection();
            string where = "MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'";
            HopDongCollection HDCollection = new HopDong().SelectCollectionDynamic(where, "");
            foreach (PhuKienDangKy PK in PKCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (PK.SoHopDong.Trim().ToUpper() == HDExist.SoHopDong.Trim().ToUpper())
                    {
                        PK.HopDong_ID = HDExist.ID;
                        PKCollectionHD.Add(PK);
                        break;
                    }
                }
            }
            foreach (PhuKienDangKy PK in PKCollectionHD)
            {
                PKCollection.Remove(PK);
            }
            foreach (PhuKienDangKy PK in PKCollection)
            {
                if (mahopdong.Trim().ToUpper() != PK.HopDong_ID.ToString().Trim().ToUpper())
                {
                    mahopdong = PK.HopDong_ID.ToString();
                    st += mahopdong + ", ";
                }
            }
            PKCollection.Clear();
            foreach (PhuKienDangKy PK in PKCollectionHD)
            {
                PKCollection.Add(PK);
            }
            return st;
        }


        /// <summary>
        /// Ba phương thức dùng để nhập hợp đồng từ doanh nghiệp
        /// </summary>
        public void NhapHopDongTuDoanhNghiep()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.GC.HopDongCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.GC.BLL.KDT.GC.HopDongCollection HDCollection = (Company.GC.BLL.KDT.GC.HopDongCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string stExist = KiemTraTonTaiHopDongNhap(HDCollection);
                    string message = "";
                    if (stExist != "")
                    {
                        message += "Hợp đồng số " + stExist + " đã có trong hệ thống nên sẽ được bỏ qua.\n";
                    }

                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }
                    HopDong HD = new HopDong();
                    KDT_GC_IDHopDongCollection IDCollection = new KDT_GC_IDHopDongCollection();
                    foreach (HopDong HopDongDK in HDCollection)
                    {
                        KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                        IDHD.SoHopDong = HopDongDK.SoHopDong;
                        IDHD.ID_CU = HopDongDK.ID;
                        IDCollection.Add(IDHD);
                    }
                    if (HDCollection.Count != 0)
                    {
                        foreach (HopDong HDNhap in HDCollection)
                        {
                            HDNhap.InsertHopDongDongBoDuLieu(IDCollection);

                        }
                        KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                        if (IDHD.InsertUpdateNew(IDCollection))
                            ShowMessage("Nhập thành công " + HDCollection.Count.ToString() + " hợp đồng.", false);
                    }
                    else
                    {
                        ShowMessage("Không có dữ liệu để nhập", false);
                    }

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private void XuatDuLieuChoDoanhNghiep()
        {
            XuatDuLieuChoDNForm form = new XuatDuLieuChoDNForm();
            form.ShowDialog();
        }

        private void ShowFormChonThuMuc()
        {
            ChonThuMucDaiLyForm f = new ChonThuMucDaiLyForm();
            f.ShowDialog();
        }

        private void ShowFormDMSPGC()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DanhMucLoaiSPForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DanhMucLoaiSPForm dmloaisp = new DanhMucLoaiSPForm();
            dmloaisp.MdiParent = this;
            dmloaisp.Show();
        }
        private void dispActivate()
        {
            ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
            obj.Show();
        }

        private void dispInfo()
        {
            ProdInfo.frmInfo obj = new Company.Interface.ProdInfo.frmInfo();
            obj.ShowDialog();
        }

        private void XuatToKhaiGCCTChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiGCCTManagerForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ToKhaiGCCTManagerForm f = new ToKhaiGCCTManagerForm();

            f.MdiParent = this;
            f.Show();
        }

        private void XuatToKhaiMauDichChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ToKhaiMauDichManageForm f = new ToKhaiMauDichManageForm();

            f.MdiParent = this;
            f.Show();
        }

        private void XuatDinhMucChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DinhMucGCManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            DinhMucGCManageForm f = new DinhMucGCManageForm();

            f.MdiParent = this;
            f.Show();
        }

        private void XuatPhuKienChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PhuKienGCManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            PhuKienGCManageForm f = new PhuKienGCManageForm();

            f.MdiParent = this;
            f.Show();
        }

        private void XuatHopDongChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("HopDongManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();

            f.MdiParent = this;
            f.Show();
        }

        private void ThucHienPhanBoToKhaiXuat()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongkeHangTonForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (f.HopDongSelected.SoHopDong.Trim() != "")
            {
                tkHangTon = new ThongkeHangTonForm();
                tkHangTon.HD = new HopDong();
                tkHangTon.HD.ID = f.HopDongSelected.ID;
                tkHangTon.HD.Load();
                tkHangTon.MdiParent = this;
                tkHangTon.Show();
            }


        }

        #region NHẬP DỮ LIỆU TỪ XML

        /// <summary>
        /// Bốn phương thức dùng để nhập tờ khai chuyển tiếp từ XML
        /// </summary>

        public void NhapTKCTXML()
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new ToKhaiChuyenTiepCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKCTCollection = (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection)serializer.Deserialize(fs);
                    fs.Close();

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    foreach (ToKhaiChuyenTiep TKCT_EDIT in TKCTCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (TKCT_EDIT.IDHopDong == IDHD_EDIT.ID_CU)
                                TKCT_EDIT.IDHopDong = IDHD_EDIT.ID_MOI;
                        }
                    }

                    string message = "";
                    string stExist = KiemTraTonTaiTKCTNhap(TKCTCollection);
                    string stTKCTHopDong = KiemTraTKCTHopDong(TKCTCollection);
                    string stTKCTDoanhNghiep = KiemTraTKCTDoanhNghiep(TKCTCollection);
                    if (stExist != "")
                    {
                        message += "Tờ khai số " + stExist + ", đã có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (stTKCTHopDong != "")
                    {
                        message += "Hợp đồng " + stTKCTHopDong + ", chưa có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (stTKCTDoanhNghiep != "")
                    {
                        message += "Tờ khai số" + stTKCTDoanhNghiep + ", không phải của doanh nghiệp nên sẽ bị loại.\n";
                    }
                    if (message != "")
                        ShowMessage(message, false);
                    if (TKCTCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
                        {
                            if (TKCT.InsertDongBoDuLieu())
                                i++;
                        }
                        if (i > 0)
                            showMsg("MSG_0203056", i);
                        //ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu.", false);
                    }
                    else
                        showMsg("MSG_0203058");
                    //ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }


        }

        public string KiemTraTonTaiTKCTNhap(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollectionExist = new ToKhaiChuyenTiepCollection();
            foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                if (TKCT.CheckExistToKhaiChuyenTiep(TKCT.MaHaiQuanTiepNhan, TKCT.MaLoaiHinh, TKCT.SoToKhai, TKCT.NgayDangKy))
                {
                    sotokhai = TKCT.SoToKhai + setText("/Ngày ", "/Date") + TKCT.NgayDangKy;
                    st += sotokhai;
                    TKCTCollectionExist.Add(TKCT);
                }
            }
            foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT in TKCTCollectionExist)
            {
                TKCTCollection.Remove(TKCT);
            }
            return st;
        }

        public string KiemTraTKCTHopDong(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection)
        {
            string st = "";
            string mahopdong = "";
            ToKhaiChuyenTiepCollection TKCTCollectionHD = new ToKhaiChuyenTiepCollection();
            HopDongCollection HDCollection = new HopDongCollection();
            HopDong HD = new HopDong();
            HDCollection = HD.SelectCollectionAll();
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (TKCT.IDHopDong == HDExist.ID)
                    {
                        TKCTCollectionHD.Add(TKCT);
                        break;
                    }
                }
            }
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollectionHD)
            {
                TKCTCollection.Remove(TKCT);
            }
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                if (mahopdong.Trim().ToUpper() != TKCT.IDHopDong.ToString().Trim().ToUpper())
                {
                    mahopdong = TKCT.IDHopDong.ToString();
                    st += mahopdong;
                }
            }
            TKCTCollection.Clear();
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollectionHD)
            {
                TKCTCollection.Add(TKCT);
            }
            return st;
        }

        public string KiemTraTKCTDoanhNghiep(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollectionExit = new ToKhaiChuyenTiepCollection();
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollection)
            {
                if (TKCT.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    TKCTCollectionExit.Add(TKCT);
                    if (sotokhai.Trim().ToUpper() != TKCT.SoToKhai.ToString().ToUpper())
                    {
                        sotokhai = TKCT.SoToKhai.ToString();
                        st += sotokhai;
                    }
                }
            }
            foreach (ToKhaiChuyenTiep TKCT in TKCTCollectionExit)
            {
                TKCTCollection.Remove(TKCT);
            }
            return st;
        }




        /// <summary>
        /// Bốn phương thức dùng để nhập tờ khai mậu dịch từ XML
        /// </summary>


        public void NhapTKMDXML()
        {
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection = new Company.GC.BLL.KDT.ToKhaiMauDichCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.ToKhaiMauDichCollection));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKMDCollection = (Company.GC.BLL.KDT.ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    foreach (ToKhaiMauDich TKMD_EDIT in TKMDCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (TKMD_EDIT.IDHopDong == IDHD_EDIT.ID_CU)
                                TKMD_EDIT.IDHopDong = IDHD_EDIT.ID_MOI;
                        }
                    }

                    string message = "";
                    string stExist = KiemTraTonTaiTKMDNhap(TKMDCollection);
                    string stTKMDHopDong = KiemTraTKMDHopDong(TKMDCollection);
                    string stTKMDDoanhNghiep = KiemTraTKMDDoanhNghiep(TKMDCollection);
                    if (stExist != "")
                    {
                        message += "Tờ khai số " + stExist + ", đã có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (stTKMDHopDong != "")
                    {
                        message += "Hợp đồng " + stTKMDHopDong + ", chưa có trong hệ thống nên sẽ bị bỏ qua.\n";
                    }
                    if (stTKMDDoanhNghiep != "")
                    {
                        message += "Tờ khai số" + stTKMDDoanhNghiep + ", không phải của doanh nghiệp nên sẽ bị loại.\n";
                    }
                    if (message != "")
                        ShowMessage(message, false);
                    if (TKMDCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiMauDich TKMD in TKMDCollection)
                        {
                            if (TKMD.InsertDongBoDuLieu())
                                i++;
                        }
                        if (i > 0)
                            showMsg("MSG_0203056", i);
                        //ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu.", false);
                    }
                    else
                        showMsg("MSG_0203058");
                    //ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        public string KiemTraTonTaiTKMDNhap(Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollectionExist = new ToKhaiMauDichCollection();
            foreach (Company.GC.BLL.KDT.ToKhaiMauDich TKMD in TKMDCollection)
            {
                if (TKMD.CheckExistToKhaiMauDich(TKMD.MaHaiQuan, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NgayDangKy))
                {
                    sotokhai = TKMD.SoToKhai + "/Ngày " + TKMD.NgayDangKy;
                    st += sotokhai;
                    TKMDCollectionExist.Add(TKMD);
                }
            }
            foreach (Company.GC.BLL.KDT.ToKhaiMauDich TKMD in TKMDCollectionExist)
            {
                TKMDCollection.Remove(TKMD);
            }
            return st;
        }

        public string KiemTraTKMDHopDong(Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string st = "";
            string mahopdong = "";
            ToKhaiMauDichCollection TKMDCollectionHD = new ToKhaiMauDichCollection();
            HopDongCollection HDCollection = new HopDongCollection();
            HopDong HD = new HopDong();
            HDCollection = HD.SelectCollectionAll();
            foreach (ToKhaiMauDich TKMD in TKMDCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (TKMD.IDHopDong == HDExist.ID)
                    {
                        TKMDCollectionHD.Add(TKMD);
                        break;
                    }
                }
            }
            foreach (ToKhaiMauDich TKMD in TKMDCollectionHD)
            {
                TKMDCollection.Remove(TKMD);
            }
            foreach (ToKhaiMauDich TKMD in TKMDCollection)
            {
                if (mahopdong.Trim().ToUpper() != TKMD.IDHopDong.ToString().Trim().ToUpper())
                {
                    mahopdong = TKMD.IDHopDong.ToString();
                    st += mahopdong;
                }
            }
            TKMDCollection.Clear();
            foreach (ToKhaiMauDich TKMD in TKMDCollectionHD)
            {
                TKMDCollection.Add(TKMD);
            }
            return st;
        }

        public string KiemTraTKMDDoanhNghiep(Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string st = "";
            string sotokhai = "";
            Company.GC.BLL.KDT.ToKhaiMauDichCollection TKMDCollectionExit = new ToKhaiMauDichCollection();
            foreach (ToKhaiMauDich TKMD in TKMDCollection)
            {
                if (TKMD.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    TKMDCollectionExit.Add(TKMD);
                    if (sotokhai.Trim().ToUpper() != TKMD.SoToKhai.ToString().ToUpper())
                    {
                        sotokhai = TKMD.SoToKhai.ToString();
                        st += sotokhai;
                    }
                }
            }
            foreach (ToKhaiMauDich TKMD in TKMDCollectionExit)
            {
                TKMDCollection.Remove(TKMD);
            }
            return st;
        }



        /// <summary>
        /// Ba phương thức dùng để nhập hợp đồng từ XML
        /// </summary>

        public void NhapHopDongXML()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.GC.HopDongCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.GC.BLL.KDT.GC.HopDongCollection HDCollection = (Company.GC.BLL.KDT.GC.HopDongCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string stExist = KiemTraTonTaiHopDongNhap(HDCollection);
                    string stMember = KiemTraHopDongDoanhNghiep(HDCollection);
                    string message = "";
                    if (stExist != "")
                    {
                        message += "Hợp đồng số " + stExist + " đã có trong hệ thống nên sẽ được bỏ qua.\n";
                    }
                    if (stMember != "")
                    {
                        message += "Hợp đồng số " + stMember + "không phải của doanh nghiệp nên bị loại bỏ.";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }
                    HopDong HD = new HopDong();
                    KDT_GC_IDHopDongCollection IDCollection = new KDT_GC_IDHopDongCollection();
                    foreach (HopDong HopDongDK in HDCollection)
                    {
                        KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                        IDHD.SoHopDong = HopDongDK.SoHopDong;
                        IDHD.ID_CU = HopDongDK.ID;
                        IDCollection.Add(IDHD);
                    }
                    if (HDCollection.Count != 0)
                    {
                        foreach (HopDong HDNhap in HDCollection)
                        {
                            HDNhap.InsertHopDongDongBoDuLieu(IDCollection);

                        }
                        KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                        if (IDHD.InsertUpdateNew(IDCollection))
                            showMsg("MSG_0203059", HDCollection.Count);
                        //ShowMessage("Nhập thành công " + HDCollection.Count.ToString() + " hợp đồng.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu", false);
                    }
                    else
                    {
                        showMsg("MSG_0203058");
                        //ShowMessage("Không có dữ liệu để nhập", false);
                    }

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        public string KiemTraTonTaiHopDongNhap(HopDongCollection HDCollection)
        {
            string st = "";
            string sohopdong = "";
            Company.GC.BLL.KDT.GC.HopDongCollection HDCollectionExit = new HopDongCollection();
            foreach (HopDong HopDongDK in HDCollection)
            {
                if (HopDongDK.checkSoHopDongExit(HopDongDK.SoHopDong, HopDongDK.MaHaiQuan, GlobalSettings.MA_DON_VI))
                {
                    HDCollectionExit.Add(HopDongDK);
                    if (sohopdong.Trim().ToUpper() != HopDongDK.SoHopDong.ToUpper())
                    {
                        sohopdong = HopDongDK.SoHopDong;
                        st += sohopdong + ", ";
                    }
                }
            }
            foreach (HopDong HopDongDK in HDCollectionExit)
            {
                HDCollection.Remove(HopDongDK);
            }
            return st;
        }

        public string KiemTraHopDongDoanhNghiep(HopDongCollection HDCollection)
        {
            string st = "";
            string sohopdong = "";
            Company.GC.BLL.KDT.GC.HopDongCollection HDCollectionExit = new HopDongCollection();
            foreach (HopDong HopDongDK in HDCollection)
            {
                if (HopDongDK.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    HDCollectionExit.Add(HopDongDK);
                    if (sohopdong.Trim().ToUpper() != HopDongDK.SoHopDong.ToUpper())
                    {
                        sohopdong = HopDongDK.SoHopDong;
                        st += sohopdong + ", ";
                    }
                }
            }
            foreach (HopDong HopDongDK in HDCollectionExit)
            {
                HDCollection.Remove(HopDongDK);
            }
            return st;
        }



        /// <summary>
        /// Năm phương thức dùng để nhập định mức từ XML
        /// </summary>

        public void NhapDinhMucXML()
        {

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.GC.DinhMucCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.GC.BLL.GC.DinhMucCollection DMCollection = (Company.GC.BLL.GC.DinhMucCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string message = "";

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                    foreach (Company.GC.BLL.GC.DinhMuc DM_EDIT in DMCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (DM_EDIT.HopDong_ID == IDHD_EDIT.ID_CU)
                                DM_EDIT.HopDong_ID = IDHD_EDIT.ID_MOI;
                        }
                    }

                    string stExist = KiemTraTonTaiDinhMucNhap(DMCollection);
                    string stDMSanPham = KiemTraDinhMucSanPham(DMCollection);
                    string stDMHopDong = KiemTraDinhMucHopDong(DMCollection);
                    string stDMNguyenPhuLieu = KiemTraDinhMucNguyenPhuLieu(DMCollection);
                    if (stExist != "")
                    {
                        message += "Sản phẩm có mã " + stExist + "đã có định mức trong hệ thống nên sẽ được bỏ qua.\n";
                    }
                    if (stDMHopDong != "")
                    {
                        message += "Hợp đồng có mã " + stDMHopDong + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (stDMSanPham != "")
                    {
                        message += "Sản phẩm có mã " + stDMSanPham + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (stDMNguyenPhuLieu != "")
                    {
                        message += "Nguyên phụ liệu có mã " + stDMNguyenPhuLieu + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }

                    if (DMCollection.Count != 0)
                    {
                        if (DM.InsertUpdate(DMCollection))
                            showMsg("MSG_0203060", DMCollection.Count);
                        //ShowMessage("Nhập thành công "+ DMCollection.Count.ToString() + " thông tin định mức.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu.", false);
                    }
                    else
                        showMsg("MSG_0203058");
                    //ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        private string KiemTraTonTaiDinhMucNhap(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string masanpham = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionExit = new Company.GC.BLL.GC.DinhMucCollection();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (DM.CheckExitsDinhMucSanPhamNguyenPhuLieu())
                {
                    DMCollectionExit.Add(DM);
                    if (masanpham.Trim().ToUpper() != DM.MaSanPham.Trim().ToUpper())
                    {
                        masanpham = DM.MaSanPham;
                        st += masanpham + ", ";
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionExit)
            {
                DMCollection.Remove(DM);
            }
            return st;
        }

        private string KiemTraDinhMucSanPham(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string masanpham = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionSP = new Company.GC.BLL.GC.DinhMucCollection();
            Company.GC.BLL.GC.SanPhamCollection SPCollection = new Company.GC.BLL.GC.SanPhamCollection();
            Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
            SPCollection = SP.SelectCollectionAll();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                foreach (Company.GC.BLL.GC.SanPham SPExist in SPCollection)
                {
                    if (DM.MaSanPham == SPExist.Ma)
                    {
                        DMCollectionSP.Add(DM);
                        break;
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionSP)
            {
                DMCollection.Remove(DM);
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (masanpham.Trim().ToUpper() != DM.MaSanPham.Trim().ToUpper())
                {
                    masanpham = DM.MaSanPham;
                    st += masanpham + ", ";
                }
            }
            DMCollection.Clear();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionSP)
            {
                DMCollection.Add(DM);
            }
            return st;
        }

        private string KiemTraDinhMucHopDong(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string mahopdong = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionHD = new Company.GC.BLL.GC.DinhMucCollection();
            HopDongCollection HDCollection = new HopDongCollection();
            HopDong HD = new HopDong();
            HDCollection = HD.SelectCollectionAll();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                foreach (HopDong HDExist in HDCollection)
                {
                    if (DM.HopDong_ID == HDExist.ID)
                    {
                        DMCollectionHD.Add(DM);
                        break;
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionHD)
            {
                DMCollection.Remove(DM);
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (mahopdong.Trim().ToUpper() != DM.HopDong_ID.ToString().Trim().ToUpper())
                {
                    mahopdong = DM.HopDong_ID.ToString();
                    st += mahopdong + ", ";
                }
            }
            DMCollection.Clear();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionHD)
            {
                DMCollection.Add(DM);
            }
            return st;
        }

        private string KiemTraDinhMucNguyenPhuLieu(Company.GC.BLL.GC.DinhMucCollection DMCollection)
        {
            string st = "";
            string manguyenphulieu = "";
            Company.GC.BLL.GC.DinhMucCollection DMCollectionNPL = new Company.GC.BLL.GC.DinhMucCollection();
            Company.GC.BLL.GC.NguyenPhuLieuCollection NPLCollection = new Company.GC.BLL.GC.NguyenPhuLieuCollection();
            Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLCollection = NPL.SelectCollectionAll();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLExist in NPLCollection)
                {
                    if (DM.MaNguyenPhuLieu == NPLExist.Ma)
                    {
                        DMCollectionNPL.Add(DM);
                        break;
                    }
                }
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionNPL)
            {
                DMCollection.Remove(DM);
            }
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (manguyenphulieu.Trim().ToUpper() != DM.MaNguyenPhuLieu.Trim().ToUpper())
                {
                    manguyenphulieu = DM.MaNguyenPhuLieu;
                    st += manguyenphulieu + ", ";
                }
            }
            DMCollection.Clear();
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollectionNPL)
            {
                DMCollection.Add(DM);
            }
            return st;
        }



        /// <summary>
        /// Bốn phương thức dùng để nhập phụ kiện từ XML
        /// </summary>

        public void NhapPhuKienXML()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.GC.PhuKienDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    Company.GC.BLL.KDT.GC.PhuKienDangKyCollection PKCollection = (Company.GC.BLL.KDT.GC.PhuKienDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();

                    KDT_GC_IDHopDongCollection IDHDCollection = new KDT_GC_IDHopDongCollection();
                    KDT_GC_IDHopDong IDHD = new KDT_GC_IDHopDong();
                    IDHDCollection = IDHD.SelectCollectionAll();
                    Company.GC.BLL.GC.DinhMuc DM = new Company.GC.BLL.GC.DinhMuc();
                    foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy PK_EDIT in PKCollection)
                    {
                        foreach (KDT_GC_IDHopDong IDHD_EDIT in IDHDCollection)
                        {
                            if (PK_EDIT.HopDong_ID == IDHD_EDIT.ID_CU)
                                PK_EDIT.HopDong_ID = IDHD_EDIT.ID_MOI;
                        }
                    }

                    string message = "";
                    string stExist = KiemTraTonTaiPhuKienNhap(PKCollection);
                    string stPKHopDong = KiemTraPhuKienHopDong(PKCollection);
                    string stPKDoanhNghiep = KiemTraPhuKienDoanhNghiep(PKCollection);
                    if (stExist != "")
                    {
                        message += "Phụ kiện có mã " + stExist + "đã có trong hệ thống nên sẽ được bỏ qua.\n";
                    }
                    if (stPKHopDong != "")
                    {
                        message += "Hợp đồng có mã " + stPKHopDong + "chưa tồn tại trong hệ thống.\n";
                    }
                    if (stPKDoanhNghiep != "")
                    {
                        message += "Phụ kiện có mã " + stPKDoanhNghiep + "không phải của doanh nghiệp nên bị bỏ.\n";
                    }
                    if (message != "")
                    {
                        ShowMessage(message, false);
                    }

                    if (PKCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy PK in PKCollection)
                        {
                            PK.InsertPhuKienDongBoDuLieu();
                            i++;
                        }
                        if (i > 0)
                            showMsg("MSG_0203061", i);
                        //ShowMessage("Nhập thành công " + i + " phụ kiện.", false);
                        else
                            showMsg("MSG_0203057");
                        //ShowMessage("Lỗi trong quá trình nhập dữ liệu.", false);
                    }
                    else
                        showMsg("MSG_0203058");
                    //ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }

        public string KiemTraTonTaiPhuKienNhap(PhuKienDangKyCollection PKCollection)
        {
            string st = "";
            string sophukien = "";
            Company.GC.BLL.KDT.GC.PhuKienDangKyCollection PKCollectionExit = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy PK in PKCollection)
            {
                if (PK.checkExitsSoPK(PK.SoPhuKien, pkgcManageForm.MaHaiQuan, GlobalSettings.MA_DON_VI))
                {
                    PKCollectionExit.Add(PK);
                    if (sophukien.Trim().ToUpper() != PK.SoPhuKien.ToUpper())
                    {
                        sophukien = PK.SoPhuKien;
                        st += sophukien + ", ";
                    }
                }
            }
            foreach (PhuKienDangKy PK in PKCollectionExit)
            {
                PKCollection.Remove(PK);
            }
            return st;
        }


        public string KiemTraPhuKienDoanhNghiep(PhuKienDangKyCollection PKCollection)
        {
            string st = "";
            string sophukien = "";
            PhuKienDangKyCollection PKCollectionExit = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy PK in PKCollection)
            {
                if (PK.MaDoanhNghiep != GlobalSettings.MA_DON_VI)
                {
                    PKCollectionExit.Add(PK);
                    if (sophukien.Trim().ToUpper() != PK.SoPhuKien.ToUpper())
                    {
                        sophukien = PK.SoPhuKien;
                        st += sophukien + ", ";
                    }
                }
            }
            foreach (PhuKienDangKy PK in PKCollectionExit)
            {
                PKCollection.Remove(PK);
            }
            return st;
        }

        #endregion

        //----------------------------------------------------------------------------

        private void ChangePassForm()
        {
            ChangePassForm f = new ChangePassForm();
            f.ShowDialog();
        }
        private void LoginUserKhac()
        {
            this.Hide();
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
            MainForm.EcsQuanTri = null;
            isLoginSuccess = false;
            if (versionHD == 0)
            {
                Login login = new Login();
                login.ShowDialog();
            }
            else if (versionHD == 1)
            {
                Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                login.ShowDialog();
            }
            else if (versionHD == 2)
            {
                Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                login.ShowDialog();
            }
            if (isLoginSuccess)
            {
                //statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = "Người dùng: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                this.Show();
                if (versionHD == 0)
                {
                    if (MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    else
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhaiCoHD.KhaiDienTuNhap)))
                    {
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else if (versionHD == 1)
                {
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                }
                else if (versionHD == 2)
                {
                    switch (typeLogin)
                    {
                        case 1: // User Da Cau Hinh
                            this.Show();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 2: // User Chua Cau Hinh
                            this.Show();
                            DangKyForm dangKyForm = new DangKyForm();
                            dangKyForm.ShowDialog();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 3:// Admin
                            this.Hide();
                            CreatAccountForm fAdminForm = new CreatAccountForm();
                            fAdminForm.ShowDialog();
                            this.ShowLoginForm();
                            break;
                    }
                }
                khoitao_GiaTriMacDinh();
            }
            else
                Application.Exit();
        }
        private void ShowQuanLyNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            NguoiDung = new QuanLyNguoiDung();
            NguoiDung.MdiParent = this;
            NguoiDung.Show();
        }
        private void MaHSForm()
        {
            HaiQuan.HS.MainForm f = new HaiQuan.HS.MainForm();
            f.Show();
        }
        private void ShowQuanLyNhomNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNhomNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            NhomNguoiDung = new QuanLyNhomNguoiDung();
            NhomNguoiDung.MdiParent = this;
            NhomNguoiDung.Show();
        }
        private void ShowCauHinhToKhai()
        {
            CauHinhToKhaiForm f = new CauHinhToKhaiForm();
            f.ShowDialog();
        }
        private void ShowThietLapInBaoCao()
        {
            CauHinhInForm f = new CauHinhInForm();
            f.ShowDialog();
        }
        private void ShowRestoreForm()
        {
            RestoreForm f = new RestoreForm();
            f.ShowDialog();
        }
        private void ShowThietLapThongTinDNAndHQ()
        {
            ThongTinDNAndHQForm f = new ThongTinDNAndHQForm();
            f.ShowDialog();
            khoitao_GiaTriMacDinh();
        }
        private void ShowThietLapThongSoKetNoi()
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();

        }
        private void ShowBackupAndRestore(bool isBackUp)
        {
            //BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            //f.isBackUp = isBackUp;
            //f.ShowDialog();
            BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            f.isBackUp = isBackUp;
            f.ShowDialog();
        }
        private void ShowCuaKhauForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CuaKhauForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ckForm = new CuaKhauForm();
            ckForm.MdiParent = this;
            ckForm.Show();
        }

        private void ShowDVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DonViTinhForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dvtForm = new DonViTinhForm();
            dvtForm.MdiParent = this;
            dvtForm.Show();
        }

        private void ShowDKGHForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DKGHForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dkghForm = new DKGHForm();
            dkghForm.MdiParent = this;
            dkghForm.Show();
        }
        private void ShowPTVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PTVTForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ptvtForm = new PTVTForm();
            ptvtForm.MdiParent = this;
            ptvtForm.Show();
        }
        private void ShowPTTTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PTTTForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ptttForm = new PTTTForm();
            ptttForm.MdiParent = this;
            ptttForm.Show();
        }

        private void ShowNguyenTeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenTeForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ntForm = new NguyenTeForm();
            ntForm.MdiParent = this;
            ntForm.Show();
        }

        private void ShowNuocForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NuocForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            nuocForm = new NuocForm();
            nuocForm.MdiParent = this;
            nuocForm.Show();
        }

        private void ShowDVHQForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DonViHaiQuanForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dvhqForm = new DonViHaiQuanForm();
            dvhqForm.MdiParent = this;
            dvhqForm.Show();
        }

        private void ShowMaHSForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("MaHSForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            maHSForm = new MaHSForm();
            maHSForm.MdiParent = this;
            maHSForm.Show();
        }

        private void DongBoDuLieuDN()
        {
            BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            f.ShowDialog();
        }
        private void UpdateStyleForAllForm(string style)
        {
            this.vsmMain.DefaultColorScheme = style;
            queueForm.vsmMain.DefaultColorScheme = style;
            if (tkmdForm != null)
                tkmdForm.vsmMain.DefaultColorScheme = style;
            if (tkmdManageForm != null)
                tkmdManageForm.vsmMain.DefaultColorScheme = style;
            if (maHSForm != null)
                maHSForm.vsmMain.DefaultColorScheme = style;
            if (ptttForm != null)
                ptttForm.vsmMain.DefaultColorScheme = style;
            if (ptvtForm != null)
                ptvtForm.vsmMain.DefaultColorScheme = style;
            if (dvtForm != null)
                dvtForm.vsmMain.DefaultColorScheme = style;
            if (dvhqForm != null)
                dvhqForm.vsmMain.DefaultColorScheme = style;
            if (ckForm != null)
                ckForm.vsmMain.DefaultColorScheme = style;
            if (ntForm != null)
                ntForm.vsmMain.DefaultColorScheme = style;
            if (dkghForm != null)
                dkghForm.vsmMain.DefaultColorScheme = style;
            if (nuocForm != null)
                nuocForm.vsmMain.DefaultColorScheme = style;

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (MainForm.flag == 0 && soLanThucHien <= 3)
            {
                soLanLayTyGia++;

                if (!backgroundWorker1.IsBusy)
                    backgroundWorker1.RunWorkerAsync();

            }
        }

        private void cmbMenu_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void explorerBar1_ItemClick(object sender, ItemEventArgs e)
        {

        }

        private void uiPanel0_SelectedPanelChanged(object sender, Janus.Windows.UI.Dock.PanelActionEventArgs e)
        {

        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void pmMain_MdiTabMouseDown(object sender, Janus.Windows.UI.Dock.MdiTabMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                e.Tab.Form.Activate();
                if (this.MdiChildren.Length == 1)
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                mnuRightClick.Show(this, e.X + pmMain.MdiTabGroups[0].Location.X, e.Y + 6);
            }
        }

        private void doCloseMe()
        {
            Form form = pmMain.MdiTabGroups[0].SelectedTab.Form;
            form.Close();
        }

        private void doCloseAllButMe()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i] != pmMain.MdiTabGroups[0].SelectedTab.Form)
                {
                    forms[i].Close();
                }
            }
        }

        private void doCloseAll()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {

                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdDataVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != null || dataVersion != "" ? dataVersion : "?"));

                long sotk = ToKhaiMauDich.SelectCountSoTK(GlobalSettings.MA_DON_VI);
                if ((sotk % 2 == 0 && sotk > 0) || (sotk % 10 == 0 && sotk >= 10))
                {
                    string loaihinh = "ECSGC";
                    if (versionHD == 1)
                        loaihinh = "ECSDLGC";
                    if (versionHD != 2)
                        WebServiceConnection.sendThongTinDN(GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, "SoTK=" + sotk, loaihinh);
                }
                string error = KeySecurity.Active.Install.ECS_updateInfo(UpdateInfo(sotk.ToString()));
                if (!string.IsNullOrEmpty(error))
                    Logger.LocalLogger.Instance().WriteMessage("Lỗi cập nhật thông tin doanh nghiệp: " + error, null);
                if (!e.Cancel)
                {
                    //Hungtq complemented 14/12/2010
                    if (Company.KDT.SHARE.Components.Globals.GetTyGia() == true)
                    {
                        timer1.Enabled = false;
                        MainForm.flag = 1;
                    }
                    else
                    {
                        MainForm.flag = 0;
                        soLanLayTyGia = 0;
                        Thread.Sleep(15000);
                    }
                }

                AutoUpdate(string.Empty);

            }
            catch { }
        }

        #region AutoUpdate ONLINE



        private void AutoUpdate(string args)
        {
            Company.KDT.SHARE.Components.DownloadUpdate dl = new Company.KDT.SHARE.Components.DownloadUpdate(args);
            dl.DoDownload();  

        }

        #region TẠO COMMAND TOOLBAR

        private delegate void CreateCommandCallback(string lastestVersion);
        private void CreateCommand(string lastestVersion)
        {
            try
            {
                this.cmbMenu.CommandManager = this.cmMain;
                this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                    this.cmdNewVersion
                });

                if (!this.cmMain.Commands.Contains("cmdNewVersion"))
                    this.cmMain.Commands.Add(this.cmdNewVersion);

                // 
                // cmdNewVersion
                // 
                this.cmdNewVersion.Key = "cmdNewVersion";
                this.cmdNewVersion.Name = "cmdNewVersion";
                this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;
                this.cmdNewVersion.TextAlignment = Janus.Windows.UI.CommandBars.ContentAlignment.MiddleCenter;
                this.cmdNewVersion.TextImageRelation = Janus.Windows.UI.CommandBars.TextImageRelation.ImageBeforeText;
                this.cmdNewVersion.ImageReplaceableColor = System.Drawing.Color.Pink;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //HUNGTQ, Update 11/06/2010.

        private Janus.Windows.UI.CommandBars.UICommand cmdNewVersion;

        /// <summary>
        /// Tạo command chứng từ đính kèm bổ sung.
        /// </summary>
        private void CreateCommandBosung(string lastestVersion)
        {
            #region Initialize
            //Tao nut command tren toolbar.
            this.cmdNewVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdNewVersion");

            //this.cmbMenu.CommandManager = this.cmMain;
            //this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            //    this.cmdNewVersion
            //});

            //if (!this.cmMain.Commands.Contains("cmdNewVersion"))
            //    this.cmMain.Commands.Add(this.cmdNewVersion);

            //// 
            //// cmdNewVersion
            //// 
            //this.cmdNewVersion.Key = "cmdNewVersion";
            //this.cmdNewVersion.Name = "cmdNewVersion";
            //this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;

            // Invoke the method that updates the form's label
            this.Invoke(new CreateCommandCallback(this.CreateCommand), new object[] { lastestVersion });

            #endregion

            cmdNewVersion.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdNewVersion_Click);

        }

        private void cmdNewVersion_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                switch (e.Command.Key)
                {
                    case "cmdNewVersion":
                        AutoUpdate("MSG");
                        break;
                }
            }
            catch (Exception ex) { }
        }

        #endregion

        #endregion

        #region Begin ECS Express

        private void Express()
        {
            try
            {
                string val = new License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Ep"));

                if (!string.IsNullOrEmpty(val) && bool.Parse(val) == true)
                {
                    SetExpress(false);
                }
                else
                    SetExpress(true);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void SetExpress(bool visible)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Janus.Windows.UI.InheritableBoolean status = visible == true ? Janus.Windows.UI.InheritableBoolean.False : Janus.Windows.UI.InheritableBoolean.True;

                /*Menu He thong*/
                cmdDongBoPhongKhai.Visible = cmdNhapXuat.Visible = status;    //Nhap du lieu tu doanh nghiep
                cmdRestore.Visible = status;            //Phuc hoi du lieu

                /*Menu Giao dien*/
                cmdEng1.Visible = status;

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        #endregion End ECS Express


        private void requestActivate_new()
        {

            string sfmtMsg = "{0}\nVui lòng liên hệ với Công ty cổ phẩn SOFTECH để tiếp tục sử dụng";
            bool isShowActive = false;

            if (KeySecurity.Active.Install.License.KeyInfo == null)
            {
                sfmtMsg = string.Format(sfmtMsg, "Bạn chưa đăng ký sử dụng phần mềm.");
                isShowActive = true;
            }
            //else if (Helpers.GetMD5Value(Security.Active.Install.License.KeyInfo.ProductId) != "ECS_TQDT_KD")
            //{
            //    sfmtMsg = string.Format(sfmtMsg, "Mã sản phẩm không hợp lệ.");
            //    isShowActive = true;
            //}
            else if (KeySecurity.Active.Install.License.KeyInfo.TrialDays == 0)
            {
                sfmtMsg = string.Format(sfmtMsg, "Đã hết hạn dùng phần mềm");
                isShowActive = true;
            }

            if (isShowActive)
            {
                if (ShowMessage(sfmtMsg, true) == "Yes")
                {
                    ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
                    if (obj.ShowDialog(this) == DialogResult.OK)
                    {
                        //Company.Interface.ProdInfo.frmInfo f = new Company.Interface.ProdInfo.frmInfo();
                        //f.ShowDialog();
                        ShowMessage("Kích hoạt thành công. Chương trình sẽ tự khởi động lại", false);
                        Application.ExitThread();
                        Application.Restart();
                    }
                    else
                    {
                        ShowMessage("Kích hoạt không thành công, chương trình sẽ tự đóng", false);
                        Application.ExitThread();
                        Application.Exit();
                    }

                }
                else
                {
                    Application.ExitThread();
                    Application.Exit();
                }
            }
            if (KeySecurity.Active.Install.License.KeyInfo.IsShow
                && !string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Notice))
            {
                InfoOnline inf = new InfoOnline();
                inf.SetRTF(KeySecurity.Active.Install.License.KeyInfo.Notice);
                inf.ShowDialog();
            }
        }

        #region Cập nhật thông tin

        private string UpdateInfo(string soTK)
        {
            Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer info = new Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer();
            #region Tạo thông tin update
            info.Id = GlobalSettings.MA_DON_VI;
            info.Name = GlobalSettings.TEN_DON_VI;
            info.Address = GlobalSettings.DIA_CHI;
            info.Phone = GlobalSettings.SoDienThoaiDN;
            info.Contact_Person = GlobalSettings.NguoiLienHe;
            info.Email = GlobalSettings.MailDoanhNghiep;
            info.Id_Customs = GlobalSettings.MA_HAI_QUAN;
            info.IP_Customs = GlobalSettings.DiaChiWS;
            info.ServerName = GlobalSettings.SERVER_NAME;
            info.Data_Name = GlobalSettings.DATABASE_NAME;
            info.Data_User = GlobalSettings.USER;
            info.Data_Pass = GlobalSettings.PASS;
            info.Data_Version = Company.KDT.SHARE.Components.Version.GetVersion();
            info.App_Version = Application.ProductVersion;
            info.LastCheck = DateTime.Now;
            info.temp1 = GlobalSettings.NguoiLienHe;
            info.MachineCode = KeySecurity.KeyCode.ProcessInfo();
            info.Product_ID = "ECS_TQDT_GC_V2";
            info.RecordCount = soTK;
            #endregion
            return Company.KDT.SHARE.Components.Helpers.Serializer(info);
        }
        #endregion

    }
}