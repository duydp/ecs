using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class HangMau
	{
		#region Properties.
		
		public long HopDong_ID { set; get; }
		public string Ma { set; get; }
        public string MaMoi { set; get; }
		public string Ten { set; get; }
		public string MaHS { set; get; }
		public string DVT_ID { set; get; }
		public decimal SoLuongDangKy { set; get; }
		public decimal SoLuongDaNhap { set; get; }
		public decimal SoLuongDaDung { set; get; }
		public decimal SoLuongCungUng { set; get; }
		public decimal TongNhuCau { set; get; }
		public int STTHang { set; get; }
		public int TrangThai { set; get; }
		public decimal DonGia { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HangMau Load(long hopDong_ID, string ma)
		{
			const string spName = "[dbo].[p_GC_HangMau_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, ma);
			HangMau entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new HangMau();
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
			}
			reader.Close();
			return entity;
		}

        public bool  Load()
        {
            const string spName = "[dbo].[p_GC_HangMau_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt,this.HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar,this.Ma);
            HangMau entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new HangMau();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public HangMau Load(string Ma, long HopDong_ID)
        {
            const string spName = "[dbo].[p_GC_HangMau_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            HangMau entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new HangMau();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                reader.Close();
                return entity;
            }
            reader.Close();
            return entity;
        }
        public bool Load_HM()
        {
            const string spName = "[dbo].[p_GC_HangMau_Load_HM]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this.HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this.Ma);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this.MaHS);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this.Ten);
            HangMau entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new HangMau();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
		
		//---------------------------------------------------------------------------------------------
		public static List<HangMau> SelectCollectionAll()
		{
			List<HangMau> collection = new List<HangMau>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				HangMau entity = new HangMau();
				
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<HangMau> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<HangMau> collection = new List<HangMau>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HangMau entity = new HangMau();
				
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
        public List<HangMau> SelectCollectionDynamic_1(string whereCondition, string orderByExpression)
        {
            List<HangMau> collection = new List<HangMau>();

            SqlDataReader reader = (SqlDataReader)SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                HangMau entity = new HangMau();

                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_GC_HangMau_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GC_HangMau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_GC_HangMau_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GC_HangMau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertHangMau(long hopDong_ID, string ma, string ten, string maHS, string dVT_ID, decimal soLuongDangKy, decimal soLuongDaNhap, decimal soLuongDaDung, decimal soLuongCungUng, decimal tongNhuCau, int sTTHang, int trangThai, decimal donGia)
		{
			HangMau entity = new HangMau();	
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.SoLuongDangKy = soLuongDangKy;
			entity.SoLuongDaNhap = soLuongDaNhap;
			entity.SoLuongDaDung = soLuongDaDung;
			entity.SoLuongCungUng = soLuongCungUng;
			entity.TongNhuCau = tongNhuCau;
			entity.STTHang = sTTHang;
			entity.TrangThai = trangThai;
			entity.DonGia = donGia;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_GC_HangMau_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
			db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, SoLuongDaNhap);
			db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, SoLuongDaDung);
			db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, SoLuongCungUng);
			db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, TongNhuCau);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HangMau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangMau item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHangMau(long hopDong_ID, string ma, string ten, string maHS, string dVT_ID, decimal soLuongDangKy, decimal soLuongDaNhap, decimal soLuongDaDung, decimal soLuongCungUng, decimal tongNhuCau, int sTTHang, int trangThai, decimal donGia)
		{
			HangMau entity = new HangMau();			
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.SoLuongDangKy = soLuongDangKy;
			entity.SoLuongDaNhap = soLuongDaNhap;
			entity.SoLuongDaDung = soLuongDaDung;
			entity.SoLuongCungUng = soLuongCungUng;
			entity.TongNhuCau = tongNhuCau;
			entity.STTHang = sTTHang;
			entity.TrangThai = trangThai;
			entity.DonGia = donGia;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_GC_HangMau_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
			db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, SoLuongDaNhap);
			db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, SoLuongDaDung);
			db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, SoLuongCungUng);
			db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, TongNhuCau);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HangMau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangMau item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHangMau(long hopDong_ID, string ma, string ten, string maHS, string dVT_ID, decimal soLuongDangKy, decimal soLuongDaNhap, decimal soLuongDaDung, decimal soLuongCungUng, decimal tongNhuCau, int sTTHang, int trangThai, decimal donGia)
		{
			HangMau entity = new HangMau();			
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.SoLuongDangKy = soLuongDangKy;
			entity.SoLuongDaNhap = soLuongDaNhap;
			entity.SoLuongDaDung = soLuongDaDung;
			entity.SoLuongCungUng = soLuongCungUng;
			entity.TongNhuCau = tongNhuCau;
			entity.STTHang = sTTHang;
			entity.TrangThai = trangThai;
			entity.DonGia = donGia;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GC_HangMau_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
			db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, SoLuongDaNhap);
			db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, SoLuongDaDung);
			db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, SoLuongCungUng);
			db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, TongNhuCau);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
        public int UpdateNew(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_HangMau_Update_New]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@MaMoi", SqlDbType.VarChar, MaMoi);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
            db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, SoLuongDaNhap);
            db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, SoLuongDaDung);
            db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, SoLuongCungUng);
            db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, TongNhuCau);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HangMau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangMau item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHangMau(long hopDong_ID, string ma)
		{
			HangMau entity = new HangMau();
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GC_HangMau_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_GC_HangMau_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HangMau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangMau item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}