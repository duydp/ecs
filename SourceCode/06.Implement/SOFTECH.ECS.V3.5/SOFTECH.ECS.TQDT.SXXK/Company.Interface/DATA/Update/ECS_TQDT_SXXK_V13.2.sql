/*
Run this script on:

        192.168.72.100\ECSEXPRESS.ECS_TQDT_SXXK_V4_UPGRADE    -  This database will be modified

to synchronize it with:

        113.160.225.156,2461.ECS_TQDT_SXXK_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 12/06/2013 10:33:20 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF not exists(select * from sys.columns 
            where Name = N'LoaiChungTu' and Object_ID = Object_ID(N't_CTTT_ChungTu'))
BEGIN          
--GO	  
PRINT N'Dropping foreign keys from [dbo].[t_CTTT_ChiPhiKhac]'
--GO
ALTER TABLE [dbo].[t_CTTT_ChiPhiKhac] DROP
CONSTRAINT [FK_t_CTTT_ChiPhiKhac_t_CTTT_ChungTu]
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
PRINT N'Dropping foreign keys from [dbo].[t_CTTT_ChungTuChiTiet]'
--GO
ALTER TABLE [dbo].[t_CTTT_ChungTuChiTiet] DROP
CONSTRAINT [FK_t_CTTT_ChungTuChiTiet_t_CTTT_ChungTu]
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
PRINT N'Dropping constraints from [dbo].[t_CTTT_ChungTu]'
--GO
ALTER TABLE [dbo].[t_CTTT_ChungTu] DROP CONSTRAINT [PK_t_CTTT_ChungTu]
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
PRINT N'Rebuilding [dbo].[t_CTTT_ChungTu]'
--GO
CREATE TABLE [dbo].[tmp_rg_xx_t_CTTT_ChungTu]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[LoaiChungTu] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LanThanhLy] [int] NOT NULL,
[SoChungTu] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayChungTu] [datetime] NOT NULL,
[HinhThucThanhToan] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TongTriGia] [numeric] (18, 5) NOT NULL,
[ConLai] [numeric] (18, 5) NOT NULL,
[GhiChu] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DongTienThanhToan] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_t_CTTT_ChungTu] ON
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
INSERT INTO [dbo].[tmp_rg_xx_t_CTTT_ChungTu]([ID],[LoaiChungTu], [LanThanhLy], [SoChungTu], [NgayChungTu], [HinhThucThanhToan], [TongTriGia], [ConLai], [GhiChu], [DongTienThanhToan]) SELECT [ID],'X',[LanThanhLy], [SoChungTu], [NgayChungTu], [HinhThucThanhToan], [TongTriGia], [ConLai], [GhiChu], [DongTienThanhToan] FROM [dbo].[t_CTTT_ChungTu]
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_t_CTTT_ChungTu] OFF
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
DECLARE @idVal INT
SELECT @idVal = IDENT_CURRENT(N't_CTTT_ChungTu')
DBCC CHECKIDENT([tmp_rg_xx_t_CTTT_ChungTu], RESEED, @idVal)
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
DROP TABLE [dbo].[t_CTTT_ChungTu]
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
EXEC sp_rename N'[dbo].[tmp_rg_xx_t_CTTT_ChungTu]', N't_CTTT_ChungTu'
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
PRINT N'Creating primary key [PK_t_CTTT_ChungTu] on [dbo].[t_CTTT_ChungTu]'
--GO
ALTER TABLE [dbo].[t_CTTT_ChungTu] ADD CONSTRAINT [PK_t_CTTT_ChungTu] PRIMARY KEY CLUSTERED  ([ID])
--GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

END

GO
PRINT N'Altering [dbo].[p_CTTT_ChungTu_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Insert]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTu_Insert]
	@LoaiChungTu varchar(1),
	@LanThanhLy int,
	@SoChungTu nvarchar(150),
	@NgayChungTu datetime,
	@HinhThucThanhToan varchar(10),
	@TongTriGia numeric(18, 5),
	@ConLai numeric(18, 5),
	@GhiChu nvarchar(150),
	@DongTienThanhToan varchar(5),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_ChungTu]
(
	[LoaiChungTu],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu],
	[DongTienThanhToan]
)
VALUES 
(
	@LoaiChungTu,
	@LanThanhLy,
	@SoChungTu,
	@NgayChungTu,
	@HinhThucThanhToan,
	@TongTriGia,
	@ConLai,
	@GhiChu,
	@DongTienThanhToan
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTu_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Update]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTu_Update]
	@ID int,
	@LoaiChungTu varchar(1),
	@LanThanhLy int,
	@SoChungTu nvarchar(150),
	@NgayChungTu datetime,
	@HinhThucThanhToan varchar(10),
	@TongTriGia numeric(18, 5),
	@ConLai numeric(18, 5),
	@GhiChu nvarchar(150),
	@DongTienThanhToan varchar(5)
AS

UPDATE
	[dbo].[t_CTTT_ChungTu]
SET
	[LoaiChungTu] = @LoaiChungTu,
	[LanThanhLy] = @LanThanhLy,
	[SoChungTu] = @SoChungTu,
	[NgayChungTu] = @NgayChungTu,
	[HinhThucThanhToan] = @HinhThucThanhToan,
	[TongTriGia] = @TongTriGia,
	[ConLai] = @ConLai,
	[GhiChu] = @GhiChu,
	[DongTienThanhToan] = @DongTienThanhToan
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTu_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTu_InsertUpdate]
	@ID int,
	@LoaiChungTu varchar(1),
	@LanThanhLy int,
	@SoChungTu nvarchar(150),
	@NgayChungTu datetime,
	@HinhThucThanhToan varchar(10),
	@TongTriGia numeric(18, 5),
	@ConLai numeric(18, 5),
	@GhiChu nvarchar(150),
	@DongTienThanhToan varchar(5)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_CTTT_ChungTu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_ChungTu] 
		SET
			[LoaiChungTu] = @LoaiChungTu,
			[LanThanhLy] = @LanThanhLy,
			[SoChungTu] = @SoChungTu,
			[NgayChungTu] = @NgayChungTu,
			[HinhThucThanhToan] = @HinhThucThanhToan,
			[TongTriGia] = @TongTriGia,
			[ConLai] = @ConLai,
			[GhiChu] = @GhiChu,
			[DongTienThanhToan] = @DongTienThanhToan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_ChungTu]
		(
			[LoaiChungTu],
			[LanThanhLy],
			[SoChungTu],
			[NgayChungTu],
			[HinhThucThanhToan],
			[TongTriGia],
			[ConLai],
			[GhiChu],
			[DongTienThanhToan]
		)
		VALUES 
		(
			@LoaiChungTu,
			@LanThanhLy,
			@SoChungTu,
			@NgayChungTu,
			@HinhThucThanhToan,
			@TongTriGia,
			@ConLai,
			@GhiChu,
			@DongTienThanhToan
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTu_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Load]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTu_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu],
	[DongTienThanhToan]
FROM
	[dbo].[t_CTTT_ChungTu]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTu_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu],
	[DongTienThanhToan]
FROM
	[dbo].[t_CTTT_ChungTu]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_CTTT_ChungTuChiTiet]'
GO
if not exists(select * from sys.columns 
            where Name = N'MaHaiQuan' and Object_ID = Object_ID(N't_CTTT_ChungTuChiTiet'))
BEGIN  
	
ALTER TABLE [dbo].[t_CTTT_ChungTuChiTiet] ADD
[MaHaiQuan] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
--GO
--GO	  
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END



END
GO

PRINT N'Altering [dbo].[p_CTTT_ChungTuChiTiet_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Insert]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Insert]
	@IDCT int,
	@SoToKhai int,
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@TriGia numeric(18, 5),
	@GhiChu nvarchar(150),
	@MaHaiQuan varchar(10),
	@IDChiTiet int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_ChungTuChiTiet]
(
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu],
	[MaHaiQuan]
)
VALUES 
(
	@IDCT,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@TriGia,
	@GhiChu,
	@MaHaiQuan
)

SET @IDChiTiet = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTuChiTiet_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Update]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Update]
	@IDChiTiet int,
	@IDCT int,
	@SoToKhai int,
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@TriGia numeric(18, 5),
	@GhiChu nvarchar(150),
	@MaHaiQuan varchar(10)
AS

UPDATE
	[dbo].[t_CTTT_ChungTuChiTiet]
SET
	[IDCT] = @IDCT,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[TriGia] = @TriGia,
	[GhiChu] = @GhiChu,
	[MaHaiQuan] = @MaHaiQuan
WHERE
	[IDChiTiet] = @IDChiTiet

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]
	@IDChiTiet int,
	@IDCT int,
	@SoToKhai int,
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@TriGia numeric(18, 5),
	@GhiChu nvarchar(150),
	@MaHaiQuan varchar(10)
AS
IF EXISTS(SELECT [IDChiTiet] FROM [dbo].[t_CTTT_ChungTuChiTiet] WHERE [IDChiTiet] = @IDChiTiet)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_ChungTuChiTiet] 
		SET
			[IDCT] = @IDCT,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[TriGia] = @TriGia,
			[GhiChu] = @GhiChu,
			[MaHaiQuan] = @MaHaiQuan
		WHERE
			[IDChiTiet] = @IDChiTiet
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_ChungTuChiTiet]
		(
			[IDCT],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[TriGia],
			[GhiChu],
			[MaHaiQuan]
		)
		VALUES 
		(
			@IDCT,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@TriGia,
			@GhiChu,
			@MaHaiQuan
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTuChiTiet_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Load]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Load]
	@IDChiTiet int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu],
	[MaHaiQuan]
FROM
	[dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDChiTiet] = @IDChiTiet
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]
	@IDCT int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu],
	[MaHaiQuan]
FROM
	[dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDCT] = @IDCT

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTuChiTiet_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu],
	[MaHaiQuan]
FROM
	[dbo].[t_CTTT_ChungTuChiTiet]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_CTTT_ChiPhiKhac]'
GO
ALTER TABLE [dbo].[t_CTTT_ChiPhiKhac] ALTER COLUMN [TriGia] [numeric] (18, 6) NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_Insert]'
GO
SET QUOTED_IDENTIFIER OFF
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Insert]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Insert]
	@IDCT int,
	@MaChiPhi varchar(10),
	@TriGia numeric(18, 6),
	@MaToanTu varchar(50),
	@GhiChu nvarchar(150),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_ChiPhiKhac]
(
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
)
VALUES 
(
	@IDCT,
	@MaChiPhi,
	@TriGia,
	@MaToanTu,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Update]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Update]
	@ID int,
	@IDCT int,
	@MaChiPhi varchar(10),
	@TriGia numeric(18, 6),
	@MaToanTu varchar(50),
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_CTTT_ChiPhiKhac]
SET
	[IDCT] = @IDCT,
	[MaChiPhi] = @MaChiPhi,
	[TriGia] = @TriGia,
	[MaToanTu] = @MaToanTu,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]
	@ID int,
	@IDCT int,
	@MaChiPhi varchar(10),
	@TriGia numeric(18, 6),
	@MaToanTu varchar(50),
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_CTTT_ChiPhiKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_ChiPhiKhac] 
		SET
			[IDCT] = @IDCT,
			[MaChiPhi] = @MaChiPhi,
			[TriGia] = @TriGia,
			[MaToanTu] = @MaToanTu,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_ChiPhiKhac]
		(
			[IDCT],
			[MaChiPhi],
			[TriGia],
			[MaToanTu],
			[GhiChu]
		)
		VALUES 
		(
			@IDCT,
			@MaChiPhi,
			@TriGia,
			@MaToanTu,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Delete]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]
	@IDCT int
AS

DELETE FROM [dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[IDCT] = @IDCT

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Load]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]
	@IDCT int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[IDCT] = @IDCT

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChiPhiKhac]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM [dbo].[t_CTTT_ChiPhiKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]'
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu],
	[MaHaiQuan]
FROM [dbo].[t_CTTT_ChungTuChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChungTu_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 04, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChungTu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiChungTu],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu],
	[DongTienThanhToan]
FROM [dbo].[t_CTTT_ChungTu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]'
GO
SET QUOTED_IDENTIFIER OFF
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 27, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_CTTT_ChiPhiKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET QUOTED_IDENTIFIER ON
GO
PRINT N'Adding foreign keys to [dbo].[t_CTTT_ChiPhiKhac]'
GO
IF(NOT EXISTS (SELECT
    * 
    FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_t_CTTT_ChiPhiKhac_t_CTTT_ChungTu'
    ))
    BEGIN
ALTER TABLE [dbo].[t_CTTT_ChiPhiKhac] ADD
CONSTRAINT [FK_t_CTTT_ChiPhiKhac_t_CTTT_ChungTu] FOREIGN KEY ([IDCT]) REFERENCES [dbo].[t_CTTT_ChungTu] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_CTTT_ChungTuChiTiet]'
GO
IF(NOT EXISTS (SELECT
    * 
    FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS 
    WHERE CONSTRAINT_NAME ='FK_t_CTTT_ChungTuChiTiet_t_CTTT_ChungTu'
    ))
    BEGIN
ALTER TABLE [dbo].[t_CTTT_ChungTuChiTiet] ADD
CONSTRAINT [FK_t_CTTT_ChungTuChiTiet_t_CTTT_ChungTu] FOREIGN KEY ([IDCT]) REFERENCES [dbo].[t_CTTT_ChungTu] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors

GO
UPDATE [dbo].[t_CTTT_ChungTuChiTiet] SET [MaHaiQuan] = (SELECT TOP 1 MaHaiQuan FROM [dbo].[t_kdt_tokhaimaudich] ORDER BY  id DESC) WHERE MaHaiQuan IS NULL OR MaHaiQuan = ''
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '13.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('13.2', GETDATE(), N'Cap nhat chung tu thanh toan')
END	
