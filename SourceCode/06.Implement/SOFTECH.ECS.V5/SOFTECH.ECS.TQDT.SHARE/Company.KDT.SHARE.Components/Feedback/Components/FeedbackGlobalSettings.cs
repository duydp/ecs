﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.Feedback.Components
{
    public class FeedbackGlobalSettings
    {
        /*Gop y*/
        public static DataSet DataSetFeedbackGlobal = new DataSet("Feedback");

        public static void KhoiTao_GopYMacDinh()
        {
            try
            {
                ISyncData syndata = WebService.SyncService();
                DataSetFeedbackGlobal.Tables.Clear();

                DataTable dt = syndata.LayDanhMucLinhVuc("daibangtungcanh", DateTime.Now).Tables[0];
                dt.TableName = "LinhVuc";
                DataSetFeedbackGlobal.Tables.Add(dt.Copy());

                DateTime dateLastUpdated = Globals.GetMaxDateModified("t_GopY_CauHoiDoanhNghiep", "NgayTraLoi");
                dt = syndata.LayDanhMucCauHoiDoanhNghiep("daibangtungcanh", dateLastUpdated).Tables[0];
                dt.TableName = "CauHoiDoanhNghiep";
                DataSetFeedbackGlobal.Tables.Add(dt.Copy());

                dateLastUpdated = Globals.GetMaxDateModified("t_GopY_ThuVienCauHoi", "NgayCapNhat");
                dt = syndata.LayDanhMucThuVien("daibangtungcanh", dateLastUpdated).Tables[0];
                dt.TableName = "ThuVienCauHoi";
                DataSetFeedbackGlobal.Tables.Add(dt.Copy());

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static void LamMoiDanhMucLinhVuc()
        {
            ISyncData syndata = WebService.SyncService();
            DataSetFeedbackGlobal.Tables["LinhVuc"].Clear();
            DataTable dt = syndata.LayDanhMucLinhVuc("daibangtungcanh", DateTime.Now).Tables[0];
            DataSetFeedbackGlobal.Tables["LinhVuc"].Merge(dt);
        }

        public static void LamMoiDanhMucCauHoiDoanhNghiep()
        {
            ISyncData syndata = WebService.SyncService();
            DataSetFeedbackGlobal.Tables["CauHoiDoanhNghiep"].Clear();
            DateTime dateLastUpdated = Globals.GetMaxDateModified("t_GopY_CauHoiDoanhNghiep", "NgayTraLoi");
            DataTable dt = syndata.LayDanhMucCauHoiDoanhNghiep("daibangtungcanh", dateLastUpdated).Tables[0];
            DataSetFeedbackGlobal.Tables["CauHoiDoanhNghiep"].Merge(dt);
        }

        public static void LayDanhMucThuVienCauHoi()
        {
            ISyncData syndata = WebService.SyncService();
            DataSetFeedbackGlobal.Tables["ThuVienCauHoi"].Clear();
            DateTime dateLastUpdated = Globals.GetMaxDateModified("t_GopY_ThuVienCauHoi", "NgayCapNhat");
            DataTable dt = syndata.LayDanhMucThuVien("daibangtungcanh", dateLastUpdated).Tables[0];
            DataSetFeedbackGlobal.Tables["ThuVienCauHoi"].Merge(dt);
        }

        public static bool XoaDanhMucLinhVuc(string maLinhVuc)
        {
            ISyncData syndata = WebService.SyncService();
            return syndata.XoaDanhMucLinhVuc("daibangtungcanh", maLinhVuc);
        }

        public static bool LuuDanhMucLinhVuc()
        {
            ISyncData syndata = WebService.SyncService();
            return syndata.LuuDanhMucLinhVuc("daibangtungcanh", DataSetFeedbackGlobal.Tables["LinhVuc"]);
        }

        public static string TaoSuaCauHoi(ref ThuVienCauHoi objCauHoi)
        {
            ISyncData syndata = WebService.SyncService();
            return syndata.TaoSuaCauHoi("daibangtungcanh", ref objCauHoi);
        }

        public static string XoaCauHoi(string maCauHoi)
        {
            ISyncData syndata = WebService.SyncService();
            return syndata.XoaCauHoi("daibangtungcanh", maCauHoi);
        }

    }
}
