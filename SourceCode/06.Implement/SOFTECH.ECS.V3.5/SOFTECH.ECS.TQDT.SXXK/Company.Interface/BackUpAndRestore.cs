﻿using System;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using System.Threading;
using System.Data;
using SQLDMO;
using System.Reflection;
using System.Configuration;

namespace Company.Interface
{
    public partial class BackUpAndReStoreForm : BaseForm
    {
        public bool isBackUp = true;
        public static DateTime lastBackUp = new DateTime(1900, 01, 01);
        SQLDMO.BackupClass backupClass;
        //SQLDMO.RestoreClass restoreClass = new SQLDMO.RestoreClass();

        public BackUpAndReStoreForm()
        {
            InitializeComponent();
        }

        private void BackUpAndReStoreForm_Load(object sender, EventArgs e)
        {
            if (!GlobalSettings.PathBackup.Equals(""))
                txtPath.Text = GlobalSettings.PathBackup.Trim();

            editBox1.Text = GlobalSettings.DATABASE_NAME + "_" + DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString("00") + DateTime.Today.Day.ToString("00") + ".BAK";

            if (GlobalSettings.NGAYSAOLUU == "")
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;

            string nhacNhoSaoLuu = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHAC_NHO_SAO_LUU");

            if (GlobalSettings.NGON_NGU == "0")
            {
                if (isBackUp)
                {
                    uiComboBox1.SelectedValue = nhacNhoSaoLuu != null || nhacNhoSaoLuu != "" ? nhacNhoSaoLuu : "7";
                    //uiButton1.Text = "Sao lưu";
                }
                //else
                //{
                //    label2.Visible = label3.Visible = label4.Visible = uiComboBox1.Visible = false;
                //    uiButton1.Text = "Phục hồi";
                //}
            }
            else
            {
                if (isBackUp)
                {
                    uiComboBox1.SelectedValue = nhacNhoSaoLuu != null || nhacNhoSaoLuu != "" ? nhacNhoSaoLuu : "7";
                    //uiButton1.Text = "Backup";
                }
                //else
                //{
                //    label2.Visible = label3.Visible = label4.Visible = uiComboBox1.Visible = false;
                //    uiButton1.Text = "Restore";
                //}

            }
            uiCheckBox1.Checked = nhacNhoSaoLuu != null || nhacNhoSaoLuu != "" ? true : false;

            uiCheckBox1_CheckedChanged(null, EventArgs.Empty);

            backupClass = new SQLDMO.BackupClass();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                #region Old_Code
                //SQLDMO.SQLServer2Class server = new SQLDMO.SQLServer2Class();
                //server.Connect(GlobalSettings.SERVER_NAME, GlobalSettings.USER, GlobalSettings.PASS);
                //if (isBackUp)
                //{
                //    backupClass.Action = SQLDMO.SQLDMO_BACKUP_TYPE.SQLDMOBackup_Database;
                //    backupClass.Database = GlobalSettings.DATABASE_NAME;
                //    //backupClass.Files = editBox1.Text.Trim();
                //    SaveFileDialog saveFileDlg = new SaveFileDialog();
                //    saveFileDlg.Filter = "BAK files (*.BAK)|*.txt|All files (*.*)|*.*";
                //    saveFileDlg.RestoreDirectory = true;
                //    if (saveFileDlg.ShowDialog(this) == DialogResult.OK)
                //    {
                //        backupClass.Files = saveFileDlg.FileName + editBox1.Text.Trim();
                //        backupClass.BackupSetDescription = "Full BackUp";
                //        backupClass.BackupSetName = "CUONGNH";
                //        backupClass.SQLBackup(server);
                //    }
                //backupClass.BackupSetDescription = "Full BackUp";
                //backupClass.BackupSetName = "CUONGNH";
                //backupClass.SQLBackup(server);
                //if (uiCheckBox1.Checked)
                //{
                //    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                //    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                //}
                //else
                //{
                //    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                //    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                //}
                //lastBackUp = DateTime.Now;
                //GlobalSettings.RefreshKey();

                //BackupLog.addToLog(GlobalSettings.DATABASE_NAME, editBox1.Text.Trim(), DateTime.Now);
                //ShowMessage("Sao lưu dữ liệu thành công.", false);
                //}
                //else
                //{
                //restoreClass.Action = SQLDMO.SQLDMO_RESTORE_TYPE.SQLDMORestore_Database;
                //restoreClass.Database = GlobalSettings.DATABASE_NAME;
                //restoreClass.Files = editBox1.Text.Trim();
                //restoreClass.SQLRestore(server);
                //ShowMessage("Phục hồi dữ liệu thành công.", false);
                //}
                #endregion

                //bool ok = true;
                //string serverName = GlobalSettings.SERVER_NAME.Split('\\')[0].Trim();

                if (txtPath.Text.Equals("") || editBox1.Text.Equals(""))
                    ShowMessage("Đường dẫn hoặc tên file không được để trống", false);
                else
                {
                    #region Backup SQL 2008
                    //ServerConnection srvCon = new ServerConnection(GlobalSettings.SERVER_NAME);
                    //srvCon.LoginSecure = false;
                    //srvCon.Login = GlobalSettings.USER;
                    //srvCon.Password = GlobalSettings.PASS;
                    //Server srvSql = new Server(srvCon);

                    //SaveFileDialog saveBackupDialog = new SaveFileDialog();
                    //saveBackupDialog.Filter = "BAK files (*.BAK)|*.BAK|All files (*.*)|*.*";
                    //saveBackupDialog.FileName = txtPath.Text + "\\" + editBox1.Text;

                    ////saveBackupDialog.RestoreDirectory = true;
                    ////if (saveBackupDialog.ShowDialog(this) == DialogResult.OK)
                    ////{
                    //Backup backupDatabase = new Backup();
                    //backupDatabase.Action = BackupActionType.Database;
                    //backupDatabase.Database = GlobalSettings.DATABASE_NAME;

                    //BackupDeviceItem bkdevice = new BackupDeviceItem(saveBackupDialog.FileName, DeviceType.File);
                    //backupDatabase.Devices.Add(bkdevice);
                    //backupDatabase.Incremental = false;
                    //backupDatabase.Initialize = true;
                    //backupDatabase.SqlBackup(srvSql);
                    //}
                    //else
                    //ok = false;
                    #endregion

                    SQLDMO.SQLServer2Class server = new SQLDMO.SQLServer2Class();
                    server.Connect(GlobalSettings.SERVER_NAME, GlobalSettings.USER, GlobalSettings.PASS);
                    if (isBackUp)
                    {
                        backupClass.Action = SQLDMO.SQLDMO_BACKUP_TYPE.SQLDMOBackup_Database;
                        backupClass.Database = GlobalSettings.DATABASE_NAME;
                        SaveFileDialog saveFileDlg = new SaveFileDialog();
                        saveFileDlg.Filter = "BAK files (*.BAK)|*.txt|All files (*.*)|*.*";
                        saveFileDlg.FileName = txtPath.Text.Trim() + "\\" + editBox1.Text.Trim();
                        saveFileDlg.RestoreDirectory = true;
                        //if (saveFileDlg.ShowDialog(this) == DialogResult.OK)
                        //{
                        backupClass.Files = saveFileDlg.FileName;
                        backupClass.BackupSetDescription = "Full BackUp";
                        backupClass.BackupSetName = "ECS_TQDT_SXXK";
                        backupClass.SQLBackup(server);
                        //}
                    }

                    if (uiCheckBox1.Checked)
                    {
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                    }
                    else
                    {
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                    }

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("LastBackup", DateTime.Now.ToString("dd/MM/yyyy"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PathBackup", txtPath.Text.Trim());
                    this.Cursor = Cursors.Default;
                    //if (ok)
                    ShowMessage("Sao lưu dữ liệu thành công.", false);
                    GlobalSettings.RefreshKey();
                    server.DisConnect();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                string st = "";
                if (isBackUp)
                    st = "Lỗi khi sao lưu dữ liệu.";
                else
                    st = "Lỗi khi phục hồi dữ liệu.";
                ShowMessage(st + " " + ex.Message, false);

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { this.Cursor = Cursors.WaitCursor; }
        }

        private void editBox1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void uiCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (uiCheckBox1.Checked == false)
            {
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;
            }
            else
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = true;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FrmFolderBrowse frm = new FrmFolderBrowse(GlobalSettings.USER, GlobalSettings.PASS, GlobalSettings.SERVER_NAME);
            frm.ShowDialog(this);
            txtPath.Text = FrmFolderBrowse.pathSelected;
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (uiCheckBox1.Checked)
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                }
                else
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                }

                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PathBackup", txtPath.Text.Trim());

                ShowMessage("Lưu cấu hình thành công.", false);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

    }
}