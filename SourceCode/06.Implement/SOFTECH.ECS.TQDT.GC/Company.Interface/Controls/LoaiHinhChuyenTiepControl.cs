using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;
using Company.Interface;

namespace Company.Interface.Controls
{
    public partial class LoaiHinhChuyenTiepControl : UserControl
    {
        string nhom = "N";        
        public LoaiHinhChuyenTiepControl()
        {
            this.InitializeComponent();
        }

        public string Ma
        {
            set
            {
                this.txtMa.Text = value;
                this.cbTen.Value = this.txtMa.Text.PadRight(5);                
            }
            get { return this.txtMa.Text; }
        }
        public string Nhom
        {
            set
            {
                nhom = value;
                if (!this.DesignMode)
                {
                    this.loadData();
                }
            }
            get { return nhom; }
        }
        public string Ten
        {
            get { return this.cbTen.Text; }
        }
        public bool ReadOnly
        {
            set
            {
                this.cbTen.ReadOnly = value;
                this.txtMa.ReadOnly = value;
            }
            get { return this.cbTen.ReadOnly; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.cbTen.VisualStyleManager = value;
                this.txtMa.VisualStyleManager = value;
            }
            get
            {
                return this.txtMa.VisualStyleManager;
            }
        }
      

        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);       

        private void loadData()
        {         
            DataTable dt=LoaiPhieuChuyenTiep.SelectBy_ID(nhom).Tables[0];            
            dtLHChuyenTiep.Rows.Clear();
            foreach (DataRow row in dt.Rows)
            {
                this.dtLHChuyenTiep.ImportRow(row);
            }
            if (Ma.Trim().Length > 0)
                txtMa_Leave(null, null);
            else
            {
                Ma = dt.Rows[0][0].ToString();
            }
        }

        private void DonViHaiQuanControl_Load(object sender, EventArgs e)
        {
            
        }

        private void cbTen_ValueChanged(object sender, EventArgs e)
        {
            this.txtMa.Text = this.cbTen.Value.ToString().Trim();
            if (this.ValueChanged != null) this.ValueChanged(sender, e);
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {
            this.txtMa.Text = this.txtMa.Text.Trim();
            this.cbTen.Value = this.txtMa.Text.PadRight(5);
        }
    }
}