﻿using System;
using System.Windows.Forms;
using Company.QuanTri;

namespace Company.Interface
{
    public partial class Login : Form
    {
        private Company.Controls.MessageBoxControl _msgBox;

        public Login()
        {
            InitializeComponent();
        }

        private string ShowMessage(string message, bool showYesNoButton)
        {
            _msgBox = new Company.Controls.MessageBoxControl
                          {
                              ShowYesNoButton = showYesNoButton,
                              MessageString = message
                          };
            _msgBox.ShowDialog();
            var st = _msgBox.ReturnValue;
            _msgBox.Dispose();
            return st;
        }

        private static string SetText(string vnText, string enText)
        {
            return GlobalSettings.NGON_NGU == "1" ? enText : vnText;
        }

        private void UiButton2Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void UiButton3Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                    string msg = "Đăng nhập không thành công.";
                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        msg = "Login failed.";
                    }
                    ShowMessage(msg, false);
                    MainForm.isLoginSuccess = false;
                }
                else
                {
                    MainForm.isLoginSuccess = true;
                    GlobalSettings.UserLog = txtUser.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("UserLog", txtUser.Text.Trim());
                    GlobalSettings.PassLog = txtMatKhau.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PassLog", txtMatKhau.Text.Trim());
                    GlobalSettings.RefreshKey();
                    Close();
                }
            }
            catch
            {
                MainForm.isLoginSuccess = false;
                if (ShowMessage("Lỗi kết nối CSDL. Bạn có muốn cấu hình kết nối CSDL không?", true) == "Yes")
                {
                    var f = new ThietLapThongSoKBForm();
                    f.ShowDialog();
                }
                return;
            }
        }
        
        
        private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var f = new ThietLapThongSoKBForm();
            f.ShowDialog();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtMatKhau.Text = txtUser.Text = string.Empty;
            txtUser.Focus();
            try
            {
                uiButton3.Text = SetText("Đăng nhập", "Login");
                uiButton2.Text = SetText("Thoát", "Exit");
                linkLabel1.Text = SetText("Cấu hình thông số kết nối", "Configuration of connected parameters");

                BackgroundImage = System.Drawing.Image.FromFile(GlobalSettings.NGON_NGU == "1" ? "LOGIN-GIACONG-eng.png" : "LOGIN-GIACONG.png");

                BackgroundImageLayout = ImageLayout.Stretch;
            }
            catch (Exception)
            { }
        }      
    }
}