﻿

using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean=Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.GC.BLL.KDT.SXXK;
using System.Xml.Serialization;
using Company.Interface.KDT.GC;

namespace Company.Interface
{
	public partial class ToKhaiMauDichRegistedForm : BaseForm
	{
        public System.Data.DataTable dt = new System.Data.DataTable();
        public ToKhaiMauDichRegistedForm()
		{
			InitializeComponent();
		}

		//-----------------------------------------------------------------------------------------

        
		private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
		{
            dgList.DataSource = this.dt;
        }





        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>



        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetName(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
            }
        }


        
	}
}