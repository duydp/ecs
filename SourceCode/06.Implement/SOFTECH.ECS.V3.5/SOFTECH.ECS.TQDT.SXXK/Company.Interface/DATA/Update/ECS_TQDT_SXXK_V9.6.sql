
/****** Object:  StoredProcedure [dbo].[p_LayThongTin_TKSua_ChuaCapNhat]    Script Date: 10/01/2013 12:28:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_LayThongTin_TKSua_ChuaCapNhat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_LayThongTin_TKSua_ChuaCapNhat]
GO



/****** Object:  StoredProcedure [dbo].[p_LayThongTin_TKSua_ChuaCapNhat]    Script Date: 10/01/2013 12:28:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[p_LayThongTin_TKSua_ChuaCapNhat]  
(  
 @MaHaiQuan VARCHAR(10),  
 @MaDoanhNghiep VARCHAR(30)  
)  
AS  
/*  
Hungtq created 24/08/2012.  
Lay danh sach to khai sua da duyet chua cap nhat thong tin moi so voi to khai chinh.  
*/  
begin  
  
SELECT MaHaiQuan, SoToKhai, MaLoaiHinh, NamDangKy   
FROM   
(  
 select b.MaHaiQuan, b.SoToKhai, b.MaLoaiHinh, b.NamDangKy, b.MaPhu, b.SoLuong AS SoLuongMoi, a.SoLuong AS SoLuongCu, a.SoThuTuHang from   
 (  
  select t.SoToKhai, t.MaLoaiHinh, year(t.NgayDangKy) as NamDangKy, h.MaPhu, t.MaHaiQuan, t.MaDoanhNghiep, h.SoLuong, h.SoThuTuHang  
  from (select  MaPhu, SUM(SoLuong)as SoLuong,MIN(SoThuTuhang)as SoThuTuHang, TKMD_ID from t_KDT_HangMauDich group by MaPhu,TKMD_ID)as h   
   inner join t_KDT_ToKhaiMauDich t on h.TKMD_ID = t.ID  
  WHERE t.ID IN (SELECT ItemID FROM dbo.t_KDT_Messages WHERE (TieuDeThongBao LIKE N'Tờ khai sửa được duyệt' OR TieuDeThongBao LIKE N'Tờ khai được cấp số') GROUP BY ItemID )   
   AND t.TrangThaiXuLy = 1  
 ) as a   
 full join  
 (  
  select h.SoToKhai, h.MaLoaiHinh, h.NamDangKy, h.MaPhu, h.MaHaiQuan, sum(h.SoLuong) AS SoLuong  
  from t_sxxk_HangMauDich h   GROUP BY h.SoToKhai,h.MaLoaiHinh,h.NamDangKy,h.MaPhu,h.MaHaiQuan
 ) as b  
 on a.SoToKhai = b.SoToKhai and a.MaLoaiHinh = b.MaLoaiHinh and a.NamDangKy = b.NamDangKy  
  and a.MaHaiQuan = b.MaHaiQuan and a.MaPhu = b.MaPhu 
 where a.SoLuong <> b.SoLuong  
 AND a.MaHaiQuan = @MaHaiQuan AND a.MaDoanhNghiep = @MaDoanhNghiep  
) AS v  
GROUP BY MaHaiQuan, SoToKhai, MaLoaiHinh, NamDangKy  
  
END  

GO



/****** Object:  StoredProcedure [dbo].[p_SXXK_HangMauDich_InsertUpdate]    Script Date: 10/01/2013 12:29:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_HangMauDich_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_HangMauDich_InsertUpdate]
GO



/****** Object:  StoredProcedure [dbo].[p_SXXK_HangMauDich_InsertUpdate]    Script Date: 10/01/2013 12:29:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
      ------------------------------------------------------------------------------------------------------------------------            
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_InsertUpdate]            
-- Database: HaiQuan            
-- Author: Ngo Thanh Tung            
-- Time created: Tuesday, October 28, 2008            
------------------------------------------------------------------------------------------------------------------------            
            
CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_InsertUpdate]        
    @SoToKhai INT ,        
    @MaLoaiHinh CHAR(5) ,        
    @MaHaiQuan CHAR(6) ,        
    @NamDangKy SMALLINT ,        
    @SoThuTuHang SMALLINT ,        
    @MaHS VARCHAR(12) ,        
    @MaPhu VARCHAR(30) ,        
    @TenHang NVARCHAR(80) ,        
    @NuocXX_ID CHAR(3) ,        
    @DVT_ID CHAR(3) ,        
    @SoLuong NUMERIC(18, 5) ,        
    @DonGiaKB NUMERIC(38, 15) ,        
    @DonGiaTT NUMERIC(38, 15) ,        
    @TriGiaKB NUMERIC(38, 15) ,        
    @TriGiaTT NUMERIC(38, 15) ,        
    @TriGiaKB_VND NUMERIC(38, 15) ,        
    @ThueSuatXNK NUMERIC(5, 2) ,        
    @ThueSuatTTDB NUMERIC(5, 2) ,        
    @ThueSuatGTGT NUMERIC(5, 2) ,        
    @ThueXNK MONEY ,        
    @ThueTTDB MONEY ,        
    @ThueGTGT MONEY ,        
    @PhuThu MONEY ,        
    @TyLeThuKhac NUMERIC(5, 2) ,        
    @TriGiaThuKhac MONEY ,        
    @MienThue TINYINT        
AS         
    declare @Suffix varchar(1)
    
    set @Suffix = SUBSTRING(@MaLoaiHinh, 1, 1)
    
if @Suffix = 'N'
	begin    
		IF EXISTS ( SELECT  [SoToKhai] ,        
							[MaLoaiHinh] ,        
							[MaHaiQuan] ,        
							[NamDangKy] ,        
							[SoThuTuHang]        
					FROM    [dbo].[t_SXXK_HangMauDich]        
					WHERE   [SoToKhai] = @SoToKhai        
							AND [MaLoaiHinh] = @MaLoaiHinh        
							AND [MaHaiQuan] = @MaHaiQuan        
							AND [NamDangKy] = @NamDangKy        
							AND MaPhu = @MaPhu )         
			BEGIN            
				UPDATE  [dbo].[t_SXXK_HangMauDich]        
				SET     [MaHS] = @MaHS ,        
						[MaPhu] = @MaPhu ,        
						[TenHang] = @TenHang ,        
						[NuocXX_ID] = @NuocXX_ID ,        
						[DVT_ID] = @DVT_ID ,        
						[SoLuong] = @SoLuong ,        
						[DonGiaKB] = @DonGiaKB ,        
						[DonGiaTT] = @DonGiaTT ,        
						[TriGiaKB] = @TriGiaKB ,        
						[TriGiaTT] = @TriGiaTT ,        
						[TriGiaKB_VND] = @TriGiaKB_VND ,        
						[ThueSuatXNK] = @ThueSuatXNK ,        
						[ThueSuatTTDB] = @ThueSuatTTDB ,        
						[ThueSuatGTGT] = @ThueSuatGTGT ,        
						[ThueXNK] = @ThueXNK ,        
						[ThueTTDB] = @ThueTTDB ,        
						[ThueGTGT] = @ThueGTGT ,        
						[PhuThu] = @PhuThu ,        
						[TyLeThuKhac] = @TyLeThuKhac ,        
						[TriGiaThuKhac] = @TriGiaThuKhac ,        
						[MienThue] = @MienThue        
				WHERE   [SoToKhai] = @SoToKhai        
						AND [MaLoaiHinh] = @MaLoaiHinh        
						AND [MaHaiQuan] = @MaHaiQuan        
						AND [NamDangKy] = @NamDangKy        
						AND MaPhu = @MaPhu        
			END            
		ELSE         
			BEGIN            
				INSERT  INTO [dbo].[t_SXXK_HangMauDich]        
						( [SoToKhai] ,        
						  [MaLoaiHinh] ,        
						  [MaHaiQuan] ,        
						  [NamDangKy] ,        
						  [SoThuTuHang] ,        
						  [MaHS] ,        
						  [MaPhu] ,        
						  [TenHang] ,        
						  [NuocXX_ID] ,        
		 [DVT_ID] ,        
						  [SoLuong] ,        
						  [DonGiaKB] ,        
						  [DonGiaTT] ,        
						  [TriGiaKB] ,        
					   [TriGiaTT] ,        
						  [TriGiaKB_VND] ,        
						  [ThueSuatXNK] ,        
						  [ThueSuatTTDB] ,        
		 [ThueSuatGTGT] ,        
						  [ThueXNK] ,        
						  [ThueTTDB] ,        
						  [ThueGTGT] ,        
						  [PhuThu] ,        
						  [TyLeThuKhac] ,        
						  [TriGiaThuKhac] ,        
						  [MienThue]            
						)        
				VALUES  ( @SoToKhai ,        
						  @MaLoaiHinh ,        
						  @MaHaiQuan ,        
						  @NamDangKy ,        
						  @SoThuTuHang ,        
						  @MaHS ,        
						  @MaPhu ,        
						  @TenHang ,        
						  @NuocXX_ID ,        
						  @DVT_ID ,        
						  @SoLuong ,        
						  @DonGiaKB ,        
						  @DonGiaTT ,        
						  @TriGiaKB ,        
						  @TriGiaTT ,        
						  @TriGiaKB_VND ,        
						  @ThueSuatXNK ,        
						  @ThueSuatTTDB ,        
						  @ThueSuatGTGT ,        
						  @ThueXNK ,        
						  @ThueTTDB ,        
						  @ThueGTGT ,        
						  @PhuThu ,        
						  @TyLeThuKhac ,        
						  @TriGiaThuKhac ,        
						  @MienThue            
						)             
			END  
	end
else
	begin
		IF EXISTS ( SELECT  [SoToKhai] ,        
							[MaLoaiHinh] ,        
							[MaHaiQuan] ,        
							[NamDangKy] ,        
							[SoThuTuHang]        
					FROM    [dbo].[t_SXXK_HangMauDich]        
					WHERE   [SoToKhai] = @SoToKhai        
							AND [MaLoaiHinh] = @MaLoaiHinh        
							AND [MaHaiQuan] = @MaHaiQuan        
							AND [NamDangKy] = @NamDangKy        
							AND MaPhu = @MaPhu 
							AND SoThuTuHang = @SoThuTuHang )         
			BEGIN            
				UPDATE  [dbo].[t_SXXK_HangMauDich]        
				SET     [MaHS] = @MaHS ,        
						[MaPhu] = @MaPhu ,        
						[TenHang] = @TenHang ,        
						[NuocXX_ID] = @NuocXX_ID ,        
						[DVT_ID] = @DVT_ID ,        
						[SoLuong] = @SoLuong ,        
						[DonGiaKB] = @DonGiaKB ,        
						[DonGiaTT] = @DonGiaTT ,        
						[TriGiaKB] = @TriGiaKB ,        
						[TriGiaTT] = @TriGiaTT ,        
						[TriGiaKB_VND] = @TriGiaKB_VND ,        
						[ThueSuatXNK] = @ThueSuatXNK ,        
						[ThueSuatTTDB] = @ThueSuatTTDB ,        
						[ThueSuatGTGT] = @ThueSuatGTGT ,        
						[ThueXNK] = @ThueXNK ,        
						[ThueTTDB] = @ThueTTDB ,        
						[ThueGTGT] = @ThueGTGT ,        
						[PhuThu] = @PhuThu ,        
						[TyLeThuKhac] = @TyLeThuKhac ,        
						[TriGiaThuKhac] = @TriGiaThuKhac ,        
						[MienThue] = @MienThue        
				WHERE   [SoToKhai] = @SoToKhai        
						AND [MaLoaiHinh] = @MaLoaiHinh        
						AND [MaHaiQuan] = @MaHaiQuan        
						AND [NamDangKy] = @NamDangKy        
						AND MaPhu = @MaPhu
						AND SoThuTuHang = @SoThuTuHang        
			END            
		ELSE         
			BEGIN            
				INSERT  INTO [dbo].[t_SXXK_HangMauDich]        
						( [SoToKhai] ,        
						  [MaLoaiHinh] ,        
						  [MaHaiQuan] ,        
						  [NamDangKy] ,        
						  [SoThuTuHang] ,        
						  [MaHS] ,        
						  [MaPhu] ,        
						  [TenHang] ,        
						  [NuocXX_ID] ,        
		 [DVT_ID] ,        
						  [SoLuong] ,        
						  [DonGiaKB] ,        
						  [DonGiaTT] ,        
						  [TriGiaKB] ,        
					   [TriGiaTT] ,        
						  [TriGiaKB_VND] ,        
						  [ThueSuatXNK] ,        
						  [ThueSuatTTDB] ,        
		 [ThueSuatGTGT] ,        
						  [ThueXNK] ,        
						  [ThueTTDB] ,        
						  [ThueGTGT] ,        
						  [PhuThu] ,        
						  [TyLeThuKhac] ,        
						  [TriGiaThuKhac] ,        
						  [MienThue]            
						)        
				VALUES  ( @SoToKhai ,        
						  @MaLoaiHinh ,        
						  @MaHaiQuan ,        
						  @NamDangKy ,        
						  @SoThuTuHang ,        
						  @MaHS ,        
						  @MaPhu ,        
						  @TenHang ,        
						  @NuocXX_ID ,        
						  @DVT_ID ,        
						  @SoLuong ,        
						  @DonGiaKB ,        
						  @DonGiaTT ,        
						  @TriGiaKB ,        
						  @TriGiaTT ,        
						  @TriGiaKB_VND ,        
						  @ThueSuatXNK ,        
						  @ThueSuatTTDB ,        
						  @ThueSuatGTGT ,        
						  @ThueXNK ,        
						  @ThueTTDB ,        
						  @ThueGTGT ,        
						  @PhuThu ,        
						  @TyLeThuKhac ,        
						  @TriGiaThuKhac ,        
						  @MienThue            
						)             
			END 
	end         


GO



GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '9.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('9.6',GETDATE(), N'Cập nhật thanh khoản')
END