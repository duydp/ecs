using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
namespace Activation
{
    /// <summary>
    /// Summary description for SerialInfo
    /// </summary>
    public class SerialInfo
    {
        private string licenseName;
        private int numberClient;
        private string dateExpires;

        public string LicenseName
        {
            get { return licenseName; }
            set { licenseName = value; }
        }

        public int NumberClient
        {
            get { return numberClient; }
            set { numberClient = value; }
        }

        public string DateExpires
        {
            get { return dateExpires; }
            set { dateExpires = value; }
        }

        public SerialInfo()
        {

        }
    }
}