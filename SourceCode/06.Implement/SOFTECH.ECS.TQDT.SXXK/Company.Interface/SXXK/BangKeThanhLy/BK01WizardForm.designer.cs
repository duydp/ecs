namespace Company.Interface.SXXK.BangKe
{
    partial class BK01WizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgTKN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKNTL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK01WizardForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnBack = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnFinish = new Janus.Windows.EditControls.UIButton();
            this.btnNext = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTenChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.ccToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.ccFromDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblChenhLechNgay = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGoTo = new Janus.Windows.EditControls.UIButton();
            this.dgTKN = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgTKNTL = new Janus.Windows.GridEX.GridEX();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.label5 = new System.Windows.Forms.Label();
            this.btnLayTKN = new Janus.Windows.EditControls.UIButton();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPhanBoTuDong = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKNTL)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnPhanBoTuDong);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.btnLayTKN);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.btnDelete);
            this.grbMain.Controls.Add(this.uiButton2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.btnNext);
            this.grbMain.Controls.Add(this.btnBack);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnFinish);
            this.grbMain.Size = new System.Drawing.Size(756, 442);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Icon = ((System.Drawing.Icon)(resources.GetObject("btnBack.Icon")));
            this.btnBack.Location = new System.Drawing.Point(408, 412);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(70, 23);
            this.btnBack.TabIndex = 297;
            this.btnBack.Text = "Trở lại";
            this.btnBack.VisualStyleManager = this.vsmMain;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(680, 412);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(64, 23);
            this.btnClose.TabIndex = 296;
            this.btnClose.Text = "Đóng";
            this.btnClose.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinish.Icon = ((System.Drawing.Icon)(resources.GetObject("btnFinish.Icon")));
            this.btnFinish.Location = new System.Drawing.Point(577, 412);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(93, 23);
            this.btnFinish.TabIndex = 295;
            this.btnFinish.Text = "Hoàn thành";
            this.btnFinish.VisualStyleManager = this.vsmMain;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Enabled = false;
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Icon = ((System.Drawing.Icon)(resources.GetObject("btnNext.Icon")));
            this.btnNext.Location = new System.Drawing.Point(490, 412);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 304;
            this.btnNext.Text = "Tiếp tục";
            this.btnNext.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtTenChuHang);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.ccToDate);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.ccFromDate);
            this.uiGroupBox4.Controls.Add(this.lblChenhLechNgay);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(10, 10);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(734, 73);
            this.uiGroupBox4.TabIndex = 305;
            this.uiGroupBox4.Text = "Tìm kiếm";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(15, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(440, 13);
            this.label8.TabIndex = 275;
            this.label8.Text = "Lưu ý: Chênh lệch ngày giữa Tờ khai Xuất/ Nhập đã thiết lập trong cấu hình = ";
            // 
            // txtTenChuHang
            // 
            this.txtTenChuHang.Location = new System.Drawing.Point(483, 21);
            this.txtTenChuHang.Name = "txtTenChuHang";
            this.txtTenChuHang.Size = new System.Drawing.Size(100, 21);
            this.txtTenChuHang.TabIndex = 274;
            this.txtTenChuHang.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(395, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 273;
            this.label1.Text = "Tên chủ hàng";
            // 
            // btnSearch
            // 
            this.btnSearch.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSearch.Icon")));
            this.btnSearch.Location = new System.Drawing.Point(609, 20);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 23);
            this.btnSearch.TabIndex = 272;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ccToDate
            // 
            this.ccToDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccToDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccToDate.DropDownCalendar.Name = "";
            this.ccToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccToDate.Location = new System.Drawing.Point(295, 20);
            this.ccToDate.Name = "ccToDate";
            this.ccToDate.Nullable = true;
            this.ccToDate.NullButtonText = "Xóa";
            this.ccToDate.ShowNullButton = true;
            this.ccToDate.Size = new System.Drawing.Size(79, 21);
            this.ccToDate.TabIndex = 271;
            this.ccToDate.TodayButtonText = "Hôm nay";
            this.ccToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(231, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 270;
            this.label7.Text = "đến ngày";
            // 
            // ccFromDate
            // 
            this.ccFromDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccFromDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccFromDate.DropDownCalendar.Name = "";
            this.ccFromDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccFromDate.Location = new System.Drawing.Point(145, 20);
            this.ccFromDate.Name = "ccFromDate";
            this.ccFromDate.Nullable = true;
            this.ccFromDate.NullButtonText = "Xóa";
            this.ccFromDate.ShowNullButton = true;
            this.ccFromDate.Size = new System.Drawing.Size(80, 21);
            this.ccFromDate.TabIndex = 269;
            this.ccFromDate.TodayButtonText = "Hôm nay";
            this.ccFromDate.Value = new System.DateTime(2008, 1, 1, 0, 0, 0, 0);
            this.ccFromDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.VisualStyleManager = this.vsmMain;
            // 
            // lblChenhLechNgay
            // 
            this.lblChenhLechNgay.AutoSize = true;
            this.lblChenhLechNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblChenhLechNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChenhLechNgay.ForeColor = System.Drawing.Color.Red;
            this.lblChenhLechNgay.Location = new System.Drawing.Point(461, 50);
            this.lblChenhLechNgay.Name = "lblChenhLechNgay";
            this.lblChenhLechNgay.Size = new System.Drawing.Size(58, 13);
            this.lblChenhLechNgay.TabIndex = 269;
            this.lblChenhLechNgay.Text = "0 (ngày).";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 269;
            this.label6.Text = "Chọn tờ khai từ ngày";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(7, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(294, 14);
            this.label4.TabIndex = 307;
            this.label4.Text = "Danh sách tờ khai nhập chưa thanh khoản hết";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.btnGoTo);
            this.uiGroupBox2.Controls.Add(this.dgTKN);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(10, 108);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(341, 298);
            this.uiGroupBox2.TabIndex = 308;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Location = new System.Drawing.Point(128, 15);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(100, 21);
            this.txtSoToKhai.TabIndex = 312;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.Value = 0;
            this.txtSoToKhai.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(6, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 311;
            this.label2.Text = "Tìm đến tờ khai số:";
            // 
            // btnGoTo
            // 
            this.btnGoTo.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGoTo.Icon")));
            this.btnGoTo.Location = new System.Drawing.Point(234, 14);
            this.btnGoTo.Name = "btnGoTo";
            this.btnGoTo.Size = new System.Drawing.Size(89, 23);
            this.btnGoTo.TabIndex = 310;
            this.btnGoTo.Text = "Tìm";
            this.btnGoTo.VisualStyleManager = this.vsmMain;
            this.btnGoTo.Click += new System.EventHandler(this.btnGoTo_Click);
            // 
            // dgTKN
            // 
            this.dgTKN.AlternatingColors = true;
            this.dgTKN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTKN.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKN.ColumnAutoResize = true;
            dgTKN_DesignTimeLayout.LayoutString = resources.GetString("dgTKN_DesignTimeLayout.LayoutString");
            this.dgTKN.DesignTimeLayout = dgTKN_DesignTimeLayout;
            this.dgTKN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKN.GroupByBoxVisible = false;
            this.dgTKN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKN.ImageList = this.ImageList1;
            this.dgTKN.Location = new System.Drawing.Point(2, 41);
            this.dgTKN.Name = "dgTKN";
            this.dgTKN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKN.Size = new System.Drawing.Size(338, 255);
            this.dgTKN.TabIndex = 19;
            this.dgTKN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKN.VisualStyleManager = this.vsmMain;
            this.dgTKN.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKN_RowDoubleClick);
            this.dgTKN.RecordUpdated += new System.EventHandler(this.dgTKN_RecordUpdated);
            this.dgTKN.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKN_LoadingRow);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgTKNTL);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(390, 108);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(354, 269);
            this.uiGroupBox3.TabIndex = 309;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgTKNTL
            // 
            this.dgTKNTL.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKNTL.AlternatingColors = true;
            this.dgTKNTL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTKNTL.AutomaticSort = false;
            this.dgTKNTL.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKNTL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKNTL.ColumnAutoResize = true;
            dgTKNTL_DesignTimeLayout.LayoutString = resources.GetString("dgTKNTL_DesignTimeLayout.LayoutString");
            this.dgTKNTL.DesignTimeLayout = dgTKNTL_DesignTimeLayout;
            this.dgTKNTL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKNTL.GroupByBoxVisible = false;
            this.dgTKNTL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKNTL.ImageList = this.ImageList1;
            this.dgTKNTL.Location = new System.Drawing.Point(2, 8);
            this.dgTKNTL.Name = "dgTKNTL";
            this.dgTKNTL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKNTL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKNTL.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKNTL.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKNTL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKNTL.Size = new System.Drawing.Size(351, 260);
            this.dgTKNTL.TabIndex = 19;
            this.dgTKNTL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKNTL.VisualStyleManager = this.vsmMain;
            this.dgTKNTL.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgTKNTL_DeletingRecord);
            this.dgTKNTL.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKN_RowDoubleClick);
            this.dgTKNTL.RecordUpdated += new System.EventHandler(this.dgTKNTL_RecordUpdated);
            this.dgTKNTL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKNTL_LoadingRow);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(356, 263);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(28, 23);
            this.btnDelete.TabIndex = 311;
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(356, 229);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(28, 23);
            this.uiButton2.TabIndex = 310;
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(387, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(289, 14);
            this.label5.TabIndex = 312;
            this.label5.Text = "Danh sách tờ khai nhập đưa vào thanh khoản";
            // 
            // btnLayTKN
            // 
            this.btnLayTKN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLayTKN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayTKN.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLayTKN.Icon")));
            this.btnLayTKN.Location = new System.Drawing.Point(10, 412);
            this.btnLayTKN.Name = "btnLayTKN";
            this.btnLayTKN.Size = new System.Drawing.Size(170, 23);
            this.btnLayTKN.TabIndex = 313;
            this.btnLayTKN.Text = "Lấy TKN từ bảng phân bổ";
            this.btnLayTKN.VisualStyleManager = this.vsmMain;
            this.btnLayTKN.Click += new System.EventHandler(this.btnLayTKN_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(390, 387);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(354, 20);
            this.label3.TabIndex = 314;
            this.label3.Text = "Ghi chú: Kích đôi vào tờ khai để xem thông tin chi tiết tờ khai";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnPhanBoTuDong
            // 
            this.btnPhanBoTuDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPhanBoTuDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhanBoTuDong.Icon = ((System.Drawing.Icon)(resources.GetObject("btnPhanBoTuDong.Icon")));
            this.btnPhanBoTuDong.Location = new System.Drawing.Point(191, 412);
            this.btnPhanBoTuDong.Name = "btnPhanBoTuDong";
            this.btnPhanBoTuDong.Size = new System.Drawing.Size(120, 23);
            this.btnPhanBoTuDong.TabIndex = 315;
            this.btnPhanBoTuDong.Text = "Phân bổ tự động";
            this.btnPhanBoTuDong.VisualStyleManager = this.vsmMain;
            this.btnPhanBoTuDong.Click += new System.EventHandler(this.btnPhanBoTuDong_Click);
            // 
            // BK01WizardForm
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(756, 442);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK01WizardForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Bảng kê tờ khai nhập đưa vào thanh khoản";
            this.Load += new System.EventHandler(this.BK01WizardForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKNTL)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnBack;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnFinish;
        private Janus.Windows.EditControls.UIButton btnNext;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.CalendarCombo.CalendarCombo ccToDate;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFromDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgTKN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgTKNTL;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChuHang;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIButton btnGoTo;
        private Janus.Windows.EditControls.UIButton btnLayTKN;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton btnPhanBoTuDong;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblChenhLechNgay;
    }
}