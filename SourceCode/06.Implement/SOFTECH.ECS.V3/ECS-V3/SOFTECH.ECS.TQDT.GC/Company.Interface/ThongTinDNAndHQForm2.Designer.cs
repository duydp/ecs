﻿namespace Company.Interface
{
    partial class ThongTinDNAndHQForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongTinDNAndHQForm2));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNguoiLienHeDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtSoFaxDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtDienThoaiDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaMid = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMailDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMaCuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMailHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenCucHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenNganHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label7 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.rfvDiaChi = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenNganHQ = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.containerValidator1 = new Company.Controls.CustomValidation.ContainerValidator();
            this.ep = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvTenCuc = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMacuc = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDienThoai = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvEmailDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNguoiLienHe = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNganHQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMacuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDienThoai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(455, 446);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiButton1);
            this.uiGroupBox1.Controls.Add(this.uiButton2);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(455, 446);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiButton1
            // 
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(176, 412);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(75, 23);
            this.uiButton1.TabIndex = 11;
            this.uiButton1.Text = "Lưu";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click_1);
            // 
            // uiButton2
            // 
            this.uiButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(261, 412);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 12;
            this.uiButton2.Text = "Đóng";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.uiGroupBox4.Controls.Add(this.txtNguoiLienHeDN);
            this.uiGroupBox4.Controls.Add(this.label19);
            this.uiGroupBox4.Controls.Add(this.txtSoFaxDN);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.txtDienThoaiDN);
            this.uiGroupBox4.Controls.Add(this.label17);
            this.uiGroupBox4.Controls.Add(this.label15);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtMaMid);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtMailDoanhNghiep);
            this.uiGroupBox4.Controls.Add(this.txtDiaChi);
            this.uiGroupBox4.Controls.Add(this.txtTenDN);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.txtMaDN);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Location = new System.Drawing.Point(9, 166);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(437, 240);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Thông tin doanh nghiệp";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiLienHeDN
            // 
            this.txtNguoiLienHeDN.Location = new System.Drawing.Point(141, 201);
            this.txtNguoiLienHeDN.Name = "txtNguoiLienHeDN";
            this.txtNguoiLienHeDN.Size = new System.Drawing.Size(279, 21);
            this.txtNguoiLienHeDN.TabIndex = 19;
            this.txtNguoiLienHeDN.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(16, 206);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "Người liên hệ:";
            // 
            // txtSoFaxDN
            // 
            this.txtSoFaxDN.Location = new System.Drawing.Point(317, 171);
            this.txtSoFaxDN.Name = "txtSoFaxDN";
            this.txtSoFaxDN.Size = new System.Drawing.Size(103, 21);
            this.txtSoFaxDN.TabIndex = 17;
            this.txtSoFaxDN.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(282, 176);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 16;
            this.label18.Text = "Fax:";
            // 
            // txtDienThoaiDN
            // 
            this.txtDienThoaiDN.Location = new System.Drawing.Point(141, 171);
            this.txtDienThoaiDN.Name = "txtDienThoaiDN";
            this.txtDienThoaiDN.Size = new System.Drawing.Size(135, 21);
            this.txtDienThoaiDN.TabIndex = 15;
            this.txtDienThoaiDN.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(16, 176);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "Điện thoại:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(421, 93);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "*";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(421, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(421, 39);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(16, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Mã MID";
            // 
            // txtMaMid
            // 
            this.txtMaMid.Location = new System.Drawing.Point(141, 115);
            this.txtMaMid.Name = "txtMaMid";
            this.txtMaMid.Size = new System.Drawing.Size(279, 21);
            this.txtMaMid.TabIndex = 9;
            this.txtMaMid.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(16, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Mail doanh nghiệp";
            // 
            // txtMailDoanhNghiep
            // 
            this.txtMailDoanhNghiep.Location = new System.Drawing.Point(141, 142);
            this.txtMailDoanhNghiep.Name = "txtMailDoanhNghiep";
            this.txtMailDoanhNghiep.Size = new System.Drawing.Size(279, 21);
            this.txtMailDoanhNghiep.TabIndex = 10;
            this.txtMailDoanhNghiep.VisualStyleManager = this.vsmMain;
            this.txtMailDoanhNghiep.Leave += new System.EventHandler(this.txtMailDoanhNghiep_Leave);
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(141, 88);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(279, 21);
            this.txtDiaChi.TabIndex = 8;
            this.txtDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(141, 61);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(279, 21);
            this.txtTenDN.TabIndex = 7;
            this.txtTenDN.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(16, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Địa chỉ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(16, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên doanh nghiệp";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(141, 34);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(279, 21);
            this.txtMaDN.TabIndex = 6;
            this.txtMaDN.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(16, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã doanh nghiệp";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.label12);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.txtMaCuc);
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.txtMailHaiQuan);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.txtTenCucHQ);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtTenNganHQ);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Location = new System.Drawing.Point(9, 9);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(437, 151);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Thông tin hải quan";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(421, 62);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(421, 91);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(421, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "*";
            // 
            // txtMaCuc
            // 
            this.txtMaCuc.Location = new System.Drawing.Point(141, 30);
            this.txtMaCuc.Name = "txtMaCuc";
            this.txtMaCuc.Size = new System.Drawing.Size(47, 21);
            this.txtMaCuc.TabIndex = 1;
            this.txtMaCuc.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(17, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Mã cục hải quan:";
            // 
            // txtMailHaiQuan
            // 
            this.txtMailHaiQuan.Location = new System.Drawing.Point(141, 114);
            this.txtMailHaiQuan.Name = "txtMailHaiQuan";
            this.txtMailHaiQuan.Size = new System.Drawing.Size(279, 21);
            this.txtMailHaiQuan.TabIndex = 5;
            this.txtMailHaiQuan.VisualStyleManager = this.vsmMain;
            this.txtMailHaiQuan.Leave += new System.EventHandler(this.txtMailHaiQuan_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(16, 119);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Mail Hải quan";
            // 
            // txtTenCucHQ
            // 
            this.txtTenCucHQ.Location = new System.Drawing.Point(252, 30);
            this.txtTenCucHQ.Name = "txtTenCucHQ";
            this.txtTenCucHQ.Size = new System.Drawing.Size(168, 21);
            this.txtTenCucHQ.TabIndex = 2;
            this.txtTenCucHQ.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(198, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Tên cục";
            // 
            // txtTenNganHQ
            // 
            this.txtTenNganHQ.Location = new System.Drawing.Point(141, 86);
            this.txtTenNganHQ.Name = "txtTenNganHQ";
            this.txtTenNganHQ.Size = new System.Drawing.Size(279, 21);
            this.txtTenNganHQ.TabIndex = 4;
            this.txtTenNganHQ.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(16, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tên ngắn hải quan";
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(141, 57);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(293, 22);
            this.donViHaiQuanControl1.TabIndex = 3;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(16, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Chọn hải quan khai báo";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(200, 100);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "uiGroupBox2";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // rfvDiaChi
            // 
            this.rfvDiaChi.ControlToValidate = this.txtDiaChi;
            this.rfvDiaChi.ErrorMessage = "\"Địa chỉ\" không được để trống";
            this.rfvDiaChi.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaChi.Icon")));
            this.rfvDiaChi.Tag = "rfvDiaChi";
            // 
            // rfvMaDN
            // 
            this.rfvMaDN.ControlToValidate = this.txtMaDN;
            this.rfvMaDN.ErrorMessage = "\"Mã doanh nghiệp\" không được trống";
            this.rfvMaDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaDN.Icon")));
            this.rfvMaDN.Tag = "rfvMaDN";
            // 
            // rfvTenDN
            // 
            this.rfvTenDN.ControlToValidate = this.txtTenDN;
            this.rfvTenDN.ErrorMessage = "\"Tên doanh nghiệp \" không được trống";
            this.rfvTenDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDN.Icon")));
            this.rfvTenDN.Tag = "rfvTenDN";
            // 
            // rfvTenNganHQ
            // 
            this.rfvTenNganHQ.ControlToValidate = this.txtTenNganHQ;
            this.rfvTenNganHQ.ErrorMessage = "Chưa nhập tên ngắn hải quan";
            this.rfvTenNganHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenNganHQ.Icon")));
            this.rfvTenNganHQ.Tag = "rfvTenNganHQ";
            // 
            // containerValidator1
            // 
            this.containerValidator1.ContainerToValidate = this;
            this.containerValidator1.HostingForm = this;
            // 
            // ep
            // 
            this.ep.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
            this.ep.ContainerControl = this;
            // 
            // rfvTenCuc
            // 
            this.rfvTenCuc.ControlToValidate = this.txtTenCucHQ;
            this.rfvTenCuc.ErrorMessage = "Chưa nhập tên cục hải quan";
            this.rfvTenCuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenCuc.Icon")));
            this.rfvTenCuc.Tag = "rfvTenCuc";
            // 
            // rfvMacuc
            // 
            this.rfvMacuc.ControlToValidate = this.txtMaCuc;
            this.rfvMacuc.ErrorMessage = "\"Mã cục hải quan\" buộc phải nhập";
            this.rfvMacuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMacuc.Icon")));
            // 
            // rfvDienThoai
            // 
            this.rfvDienThoai.ControlToValidate = this.txtDienThoaiDN;
            this.rfvDienThoai.ErrorMessage = "\"Điện thoại\" buộc phải nhập";
            this.rfvDienThoai.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDienThoai.Icon")));
            // 
            // rfvEmailDN
            // 
            this.rfvEmailDN.ControlToValidate = this.txtMailDoanhNghiep;
            this.rfvEmailDN.ErrorMessage = "\"Email doanh nghiệp\" buộc phải nhập";
            this.rfvEmailDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvEmailDN.Icon")));
            // 
            // rfvNguoiLienHe
            // 
            this.rfvNguoiLienHe.ControlToValidate = this.txtNguoiLienHeDN;
            this.rfvNguoiLienHe.ErrorMessage = "\"Người liên hệ\" buộc phải nhập";
            this.rfvNguoiLienHe.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiLienHe.Icon")));
            // 
            // ThongTinDNAndHQForm2
            // 
            this.AcceptButton = this.uiButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.uiButton2;
            this.ClientSize = new System.Drawing.Size(455, 446);
            this.Controls.Add(this.uiGroupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ThongTinDNAndHQForm2";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin doanh nghiệp và hải quan";
            this.Load += new System.EventHandler(this.SendForm_Load);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNganHQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMacuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDienThoai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaChi;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenNganHQ;
        private Company.Controls.CustomValidation.ContainerValidator containerValidator1;
        private System.Windows.Forms.ErrorProvider ep;
        private System.Windows.Forms.Label label7;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHQ;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCucHQ;
        private System.Windows.Forms.Label label5;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenCuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailHaiQuan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailDoanhNghiep;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMid;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaCuc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMacuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiLienHeDN;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxDN;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoaiDN;
        private System.Windows.Forms.Label label17;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDienThoai;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvEmailDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNguoiLienHe;

    }
}