using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Collections.Generic;

namespace Company.Interface.GC
{
    public partial class SanPhamRegistedForm : BaseForm
    {
        public SanPham SanPhamSelected;
        public bool isBrower = true;
        public List<SanPham> SanPhamSelectCollection = new List<SanPham>();
        //2 hien da duyet nhung chua dieu chinh
        public int isDisplayAll = 0;//0 hien tat ca . 1 hien cac npl da duyet nhhung chua dieu chinh va bo sung . 
        public string whereCondition;
        public SanPhamRegistedForm()
        {
            InitializeComponent();
            SanPhamSelected = new SanPham();
        }

        public void BindData()
        {          
            if (isDisplayAll == 0)
                dgList.DataSource = SanPhamSelected.SelectCollectionBy_HopDong_ID();
            else if (isDisplayAll == 1)
                dgList.DataSource = SanPhamSelected.SelectCollectionDaDuyetVaBoSungBy_HopDong_ID();
            else if (isDisplayAll == 2)
                dgList.DataSource = SanPhamSelected.SelectCollectionDaDuyetBy_HopDong_ID();
            else if (isDisplayAll == 3)
                dgList.DataSource = SanPhamSelected.SelectCollectionDynamic(whereCondition, "");
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void SanPhamRegistedForm_Load(object sender, EventArgs e)
        {
            // Sản phẩm đã đăng ký.
            lblHint.Visible = !isBrower;
            BindData();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                e.Row.Cells["NhomSanPham_ID"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value.ToString());
            }

        }

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    this.SanPhamSelected = (SanPham)e.Row.DataRow;
                    this.Close();
                }
            }
            else
            {

            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string where = "HopDong_ID =" + SanPhamSelected.HopDong_ID;
                if (!String.IsNullOrEmpty(txtMaSP.Text))
                {
                    where += " AND Ma LIKE '%" + txtMaSP.Text.ToString() + "%'";
                }
                if (!String.IsNullOrEmpty(txtTenSP.Text))
                {
                    where += " AND Ten LIKE N'%" + txtTenSP.Text.ToString() + "%'";
                }
                if (isDisplayAll == 0)
                {
                    if (where.Contains("LIKE"))
                    {
                        dgList.DataSource = SanPhamSelected.SelectCollectionDynamic(where,"Ma");
                    }
                    else
                    {
                        dgList.DataSource = SanPhamSelected.SelectCollectionBy_HopDong_ID();
                    }
                }
                else if (isDisplayAll == 1)
                    dgList.DataSource = SanPhamSelected.SelectCollectionDaDuyetVaBoSungBy_HopDong_ID();
                else if (isDisplayAll == 2)
                    dgList.DataSource = SanPhamSelected.SelectCollectionDaDuyetBy_HopDong_ID();
                else if (isDisplayAll == 3)
                {
                    string whereConditionSearch = "";
                    where = "";
                    whereConditionSearch = whereCondition;
                    if (!String.IsNullOrEmpty(txtMaSP.Text))
                    {
                        where += " AND Ma LIKE '%" + txtMaSP.Text.ToString() + "%'";
                    }
                    if (!String.IsNullOrEmpty(txtTenSP.Text))
                    {
                        where += " AND Ten LIKE N'%" + txtTenSP.Text.ToString() + "%'";
                    }
                    if (!String.IsNullOrEmpty(where))
                    {
                        whereConditionSearch += where;
                    }
                    dgList.DataSource = SanPhamSelected.SelectCollectionDynamic(whereConditionSearch, "");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    SanPham sanPhamSelect = (SanPham)item.DataRow;
                    SanPhamSelectCollection.Add(sanPhamSelect);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
    }
}