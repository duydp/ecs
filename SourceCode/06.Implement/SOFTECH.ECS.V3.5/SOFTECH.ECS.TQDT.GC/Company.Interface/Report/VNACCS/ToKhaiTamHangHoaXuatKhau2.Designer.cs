﻿namespace Company.Interface.Report.VNACCS
{
    partial class ToKhaiTamHangHoaXuatKhau2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotokhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotokhaidautien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSonhanhtokhaichianho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongsotokhaichianho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSotokhaitamnhaptaixuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaphanloaikiemtra = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaloaihinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaphanloaihanghoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMahieuphuongthucvanchuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMasothuedaidien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTencoquanhaiquantiepnhantokhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNhomxulyhoso = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaydangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiodangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaythaydoidangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiothaydoidangky = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoihantainhapxuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBieuthitruonghophethan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaxephang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaxephang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaxephang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaxephang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaxephang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTendiachixephang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiachixephang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanloai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTTChiThiHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaychithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoidungchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.HeightF = 151.8333F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 22F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTenThongTinXuat,
            this.xrTable5});
            this.ReportHeader.HeightF = 34.79166F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.DetailReport1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.Detail1.HeightF = 11F;
            this.Detail1.Name = "Detail1";
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader1});
            this.DetailReport1.Level = 0;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail2.Name = "Detail2";
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.ReportHeader1.HeightF = 21.99999F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(772.9167F, 3.54166F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable5.SizeF = new System.Drawing.SizeF(47.06976F, 21.25001F);
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoTrang});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.6;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoTrang.StylePriority.UsePadding = false;
            this.lblSoTrang.Text = "2/3";
            this.lblSoTrang.Weight = 3;
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(160.4167F, 3.54166F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(491.875F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Tờ khai tạm hàng hóa xuất khẩu (thông báo kết quả phân luồng)";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow20,
            this.xrTableRow23,
            this.xrTableRow22,
            this.xrTableRow5,
            this.xrTableRow70,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow1,
            this.xrTableRow8,
            this.xrTableRow10,
            this.xrTableRow15});
            this.xrTable1.SizeF = new System.Drawing.SizeF(796.0001F, 149F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.lblSotokhai,
            this.xrTableCell41,
            this.xrTableCell47,
            this.xrTableCell176,
            this.lblSotokhaidautien,
            this.xrTableCell177,
            this.lblSonhanhtokhaichianho,
            this.xrTableCell178,
            this.lblTongsotokhaichianho});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.44000000000000017;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.22150588621117057;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "Số tờ khai";
            this.xrTableCell38.Weight = 0.69519988887066164;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Weight = 0.050000010094922054;
            // 
            // lblSotokhai
            // 
            this.lblSotokhai.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSotokhai.Name = "lblSotokhai";
            this.lblSotokhai.StylePriority.UseFont = false;
            this.lblSotokhai.Text = "NNNNNNNNN1NE";
            this.lblSotokhai.Weight = 1.2357998850163763;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.13791076673899538;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Số tờ khai đầu tiên";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell47.Weight = 1.1215999280343758;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Weight = 0.11289999999180062;
            // 
            // lblSotokhaidautien
            // 
            this.lblSotokhaidautien.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSotokhaidautien.Name = "lblSotokhaidautien";
            this.lblSotokhaidautien.StylePriority.UseFont = false;
            this.lblSotokhaidautien.Text = "XXXXXXXXX1XE";
            this.lblSotokhaidautien.Weight = 1.2273997652086184;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Text = "-";
            this.xrTableCell177.Weight = 0.13370001591400016;
            // 
            // lblSonhanhtokhaichianho
            // 
            this.lblSonhanhtokhaichianho.Name = "lblSonhanhtokhaichianho";
            this.lblSonhanhtokhaichianho.Text = "NE";
            this.lblSonhanhtokhaichianho.Weight = 0.25870000156096395;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Text = "/";
            this.xrTableCell178.Weight = 0.12330006867640209;
            // 
            // lblTongsotokhaichianho
            // 
            this.lblTongsotokhaichianho.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotokhaichianho.Name = "lblTongsotokhaichianho";
            this.lblTongsotokhaichianho.StylePriority.UseFont = false;
            this.lblTongsotokhaichianho.Text = "NE";
            this.lblTongsotokhaichianho.Weight = 2.6419840903833505;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.lblSotokhaitamnhaptaixuat});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.44000000000000017;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Weight = 0.22150588621117057;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrTableCell169.Weight = 1.9809941137888296;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 0.13791625976562494;
            // 
            // lblSotokhaitamnhaptaixuat
            // 
            this.lblSotokhaitamnhaptaixuat.Name = "lblSotokhaitamnhaptaixuat";
            this.lblSotokhaitamnhaptaixuat.Text = "NNNNNNNNN1NE";
            this.lblSotokhaitamnhaptaixuat.Weight = 5.6195840469360121;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell189,
            this.lblMaphanloaikiemtra,
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.lblMaloaihinh,
            this.lblMaphanloaihanghoa,
            this.lblMahieuphuongthucvanchuyen,
            this.xrTableCell195,
            this.xrTableCell200,
            this.xrTableCell199,
            this.lblMasothuedaidien});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 0.44000000000000017;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 0.22150588621117057;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Text = "Mã phân loại kiểm tra";
            this.xrTableCell188.Weight = 1.1741210374921556;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Weight = 0.12520612862617997;
            // 
            // lblMaphanloaikiemtra
            // 
            this.lblMaphanloaikiemtra.Multiline = true;
            this.lblMaphanloaikiemtra.Name = "lblMaphanloaikiemtra";
            this.lblMaphanloaikiemtra.Text = "XX E";
            this.lblMaphanloaikiemtra.Weight = 0.58166698359474844;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Weight = 0.10000563116833086;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Text = "Mã loại hình";
            this.xrTableCell192.Weight = 0.926099862744497;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Weight = 0.10239998208045287;
            // 
            // lblMaloaihinh
            // 
            this.lblMaloaihinh.Name = "lblMaloaihinh";
            this.lblMaloaihinh.Text = "XXE";
            this.lblMaloaihinh.Weight = 0.3437999682619991;
            // 
            // lblMaphanloaihanghoa
            // 
            this.lblMaphanloaihanghoa.Name = "lblMaphanloaihanghoa";
            this.lblMaphanloaihanghoa.Text = "X";
            this.lblMaphanloaihanghoa.Weight = 0.20489999031460793;
            // 
            // lblMahieuphuongthucvanchuyen
            // 
            this.lblMahieuphuongthucvanchuyen.Name = "lblMahieuphuongthucvanchuyen";
            this.lblMahieuphuongthucvanchuyen.Text = "X";
            this.lblMahieuphuongthucvanchuyen.Weight = 0.23789830042940907;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Weight = 0.29108181177870329;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.xrTableCell200.Weight = 1.876132594131821;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Weight = 0.10000044333862937;
            // 
            // lblMasothuedaidien
            // 
            this.lblMasothuedaidien.Name = "lblMasothuedaidien";
            this.lblMasothuedaidien.Text = "XXXE";
            this.lblMasothuedaidien.Weight = 1.6751816865289324;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell179,
            this.xrTableCell180,
            this.lblTencoquanhaiquantiepnhantokhai,
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185,
            this.lblNhomxulyhoso});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.44000000000000017;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 0.22150588621117057;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrTableCell180.Weight = 2.1189103767918023;
            // 
            // lblTencoquanhaiquantiepnhantokhai
            // 
            this.lblTencoquanhaiquantiepnhantokhai.Name = "lblTencoquanhaiquantiepnhantokhai";
            this.lblTencoquanhaiquantiepnhantokhai.Text = "XXXXXXXXXE";
            this.lblTencoquanhaiquantiepnhantokhai.Weight = 1.6771876179622236;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Weight = 0.29108152551845734;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "Mã bộ phận xử lý tờ khai";
            this.xrTableCell184.Weight = 1.6905344940545168;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Weight = 0.28559812693840819;
            // 
            // lblNhomxulyhoso
            // 
            this.lblNhomxulyhoso.Name = "lblNhomxulyhoso";
            this.lblNhomxulyhoso.Text = "XE";
            this.lblNhomxulyhoso.Weight = 1.6751822792250581;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.lblNgaydangky,
            this.xrTableCell15,
            this.lblGiodangky,
            this.xrTableCell19,
            this.xrTableCell54,
            this.xrTableCell55,
            this.lblNgaythaydoidangky,
            this.lblGiothaydoidangky});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.Weight = 0.43999999999999984;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.221505962505113;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "Ngày đăng ký";
            this.xrTableCell50.Weight = 1.4243273888274213;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.1316668607398559;
            // 
            // lblNgaydangky
            // 
            this.lblNgaydangky.Name = "lblNgaydangky";
            this.lblNgaydangky.Text = "dd/MM/yyyy";
            this.lblNgaydangky.Weight = 0.7104164944365341;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.14999983592969796;
            // 
            // lblGiodangky
            // 
            this.lblGiodangky.Name = "lblGiodangky";
            this.lblGiodangky.Text = "hh:mm:ss";
            this.lblGiodangky.Weight = 1.3796874825087764;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.29108149102965164;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Ngày thay đổi đăng ký";
            this.xrTableCell54.Weight = 1.3252201662307113;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.65091239333803363;
            // 
            // lblNgaythaydoidangky
            // 
            this.lblNgaythaydoidangky.Name = "lblNgaythaydoidangky";
            this.lblNgaythaydoidangky.Text = "dd/MM/yyyy";
            this.lblNgaythaydoidangky.Weight = 0.7948821329231;
            // 
            // lblGiothaydoidangky
            // 
            this.lblGiothaydoidangky.Name = "lblGiothaydoidangky";
            this.lblGiothaydoidangky.Text = "hh:mm:ss";
            this.lblGiothaydoidangky.Weight = 0.88030009823274147;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell5,
            this.xrTableCell7,
            this.lblThoihantainhapxuat,
            this.xrTableCell16,
            this.lblBieuthitruonghophethan,
            this.xrTableCell48});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 0.43999999999999984;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 0.221505962505113;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrTableCell5.Weight = 1.4243271599456118;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 0.13166701332772868;
            // 
            // lblThoihantainhapxuat
            // 
            this.lblThoihantainhapxuat.Name = "lblThoihantainhapxuat";
            this.lblThoihantainhapxuat.Text = "dd/MM/yyyy";
            this.lblThoihantainhapxuat.Weight = 0.7104164944365341;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "-";
            this.xrTableCell16.Weight = 0.14999998851757096;
            // 
            // lblBieuthitruonghophethan
            // 
            this.lblBieuthitruonghophethan.Name = "lblBieuthitruonghophethan";
            this.lblBieuthitruonghophethan.Text = "X";
            this.lblBieuthitruonghophethan.Weight = 1.6707692209361045;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 3.6513144670329742;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell44});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 0.44000000000000006;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 0.221506343974825;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Text = "Vanning";
            this.xrTableCell44.Weight = 7.7384939627268121;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell62});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.44000000000000006;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 0.3499999961853027;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "Địa điểm xếp hàng lên xe chở hàng";
            this.xrTableCell62.Weight = 7.6100003105163339;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell6,
            this.xrTableCell9,
            this.xrTableCell8,
            this.lblMaxephang1,
            this.xrTableCell4,
            this.lblMaxephang2,
            this.xrTableCell12,
            this.lblMaxephang3,
            this.xrTableCell17,
            this.lblMaxephang4,
            this.xrTableCell14,
            this.lblMaxephang5});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.44000000000000006;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Weight = 0.61041668381243486;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "Mã";
            this.xrTableCell6.Weight = 0.504165735018022;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 0.10000090177144683;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "1";
            this.xrTableCell8.Weight = 0.17999997705826487;
            // 
            // lblMaxephang1
            // 
            this.lblMaxephang1.Name = "lblMaxephang1";
            this.lblMaxephang1.Text = "XXXE";
            this.lblMaxephang1.Weight = 0.64999992309084864;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "2";
            this.xrTableCell4.Weight = 0.17999997705826498;
            // 
            // lblMaxephang2
            // 
            this.lblMaxephang2.Name = "lblMaxephang2";
            this.lblMaxephang2.Text = "XXXE";
            this.lblMaxephang2.Weight = 0.64999992081748625;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "3";
            this.xrTableCell12.Weight = 0.179999974784902;
            // 
            // lblMaxephang3
            // 
            this.lblMaxephang3.Name = "lblMaxephang3";
            this.lblMaxephang3.Text = "XXXE";
            this.lblMaxephang3.Weight = 0.64999992422753;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Text = "4";
            this.xrTableCell17.Weight = 0.17999997819494634;
            // 
            // lblMaxephang4
            // 
            this.lblMaxephang4.Name = "lblMaxephang4";
            this.lblMaxephang4.Text = "XXXE";
            this.lblMaxephang4.Weight = 0.64999992422753006;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "5";
            this.xrTableCell14.Weight = 0.17999997819494629;
            // 
            // lblMaxephang5
            // 
            this.lblMaxephang5.Name = "lblMaxephang5";
            this.lblMaxephang5.Text = "XXXE";
            this.lblMaxephang5.Weight = 3.2454174084450127;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.lblTendiachixephang});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.44000000000000017;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.61041660751849836;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Text = "Tên";
            this.xrTableCell64.Weight = 0.5041660589580963;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.099999620358145275;
            // 
            // lblTendiachixephang
            // 
            this.lblTendiachixephang.Font = new System.Drawing.Font("Arial", 8F);
            this.lblTendiachixephang.Name = "lblTendiachixephang";
            this.lblTendiachixephang.StylePriority.UseFont = false;
            this.lblTendiachixephang.StylePriority.UsePadding = false;
            this.lblTendiachixephang.StylePriority.UseTextAlignment = false;
            this.lblTendiachixephang.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTendiachixephang.Weight = 6.7454180198668974;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.lblDiachixephang});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1.12;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Weight = 0.61041660751849836;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Text = "Địa chỉ";
            this.xrTableCell72.Weight = 0.5041660589580963;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 0.099999620358145275;
            // 
            // lblDiachixephang
            // 
            this.lblDiachixephang.Font = new System.Drawing.Font("Arial", 8F);
            this.lblDiachixephang.Name = "lblDiachixephang";
            this.lblDiachixephang.StylePriority.UseFont = false;
            this.lblDiachixephang.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiachixephang.Weight = 6.7454180198668974;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 0.44000000000000017;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 0.22150573362328574;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Text = "Số container";
            this.xrTableCell82.Weight = 7.7384945730783512;
            // 
            // xrTable4
            // 
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable4.SizeF = new System.Drawing.SizeF(795.9999F, 11F);
            this.xrTable4.StylePriority.UsePadding = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell157,
            this.lblSTT1,
            this.lblSocontainer1,
            this.lblSTT2,
            this.lblSocontainer2,
            this.lblSTT3,
            this.lblSocontainer3,
            this.lblSTT4,
            this.lblSocontainer4,
            this.lblSTT5,
            this.lblSocontainer5});
            this.xrTableRow25.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.StylePriority.UseFont = false;
            this.xrTableRow25.Weight = 0.43999999999999995;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Weight = 0.12814071334307597;
            // 
            // lblSTT1
            // 
            this.lblSTT1.Name = "lblSTT1";
            this.lblSTT1.Text = "1";
            this.lblSTT1.Weight = 0.06783920118162845;
            // 
            // lblSocontainer1
            // 
            this.lblSocontainer1.Name = "lblSocontainer1";
            this.lblSocontainer1.Text = "XXXXXXXXX1XE";
            this.lblSocontainer1.Weight = 0.43341711866040394;
            // 
            // lblSTT2
            // 
            this.lblSTT2.Name = "lblSTT2";
            this.lblSTT2.Text = "2";
            this.lblSTT2.Weight = 0.067839143673625035;
            // 
            // lblSocontainer2
            // 
            this.lblSocontainer2.Name = "lblSocontainer2";
            this.lblSocontainer2.Text = "XXXXXXXXX1XE";
            this.lblSocontainer2.Weight = 0.43341723367641077;
            // 
            // lblSTT3
            // 
            this.lblSTT3.Name = "lblSTT3";
            this.lblSTT3.Text = "3";
            this.lblSTT3.Weight = 0.067839201181628339;
            // 
            // lblSocontainer3
            // 
            this.lblSocontainer3.Name = "lblSocontainer3";
            this.lblSocontainer3.Text = "XXXXXXXXX1XE";
            this.lblSocontainer3.Weight = 0.43341723367641077;
            // 
            // lblSTT4
            // 
            this.lblSTT4.Name = "lblSTT4";
            this.lblSTT4.Text = "4";
            this.lblSTT4.Weight = 0.067839086165621676;
            // 
            // lblSocontainer4
            // 
            this.lblSocontainer4.Name = "lblSocontainer4";
            this.lblSocontainer4.Text = "XXXXXXXXX1XE";
            this.lblSocontainer4.Weight = 0.43341717616840741;
            // 
            // lblSTT5
            // 
            this.lblSTT5.Name = "lblSTT5";
            this.lblSTT5.Text = "5";
            this.lblSTT5.Weight = 0.067839431213642;
            // 
            // lblSocontainer5
            // 
            this.lblSocontainer5.Name = "lblSocontainer5";
            this.lblSocontainer5.Text = "XXXXXXXXX1XE";
            this.lblSocontainer5.Weight = 0.79899446105914562;
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow26});
            this.xrTable3.SizeF = new System.Drawing.SizeF(796.0001F, 21.99999F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell174,
            this.xrTableCell2,
            this.lblPhanloai,
            this.xrTableCell11});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.StylePriority.UseBorders = false;
            this.xrTableRow24.Weight = 1;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Weight = 0.08489868207249654;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.Text = "Chỉ thị của Hải quan";
            this.xrTableCell2.Weight = 0.545914061678115;
            // 
            // lblPhanloai
            // 
            this.lblPhanloai.Name = "lblPhanloai";
            this.lblPhanloai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblPhanloai.StylePriority.UsePadding = false;
            this.lblPhanloai.Text = "X";
            this.lblPhanloai.Weight = 1.4200856937493884;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 1;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell175,
            this.xrTableCell28,
            this.xrTableCell164});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.20313762296340487;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseTextAlignment = false;
            this.xrTableCell175.Text = "Ngày";
            this.xrTableCell175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell175.Weight = 0.2585535228568695;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "Tên";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell28.Weight = 0.799735054697419;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseTextAlignment = false;
            this.xrTableCell164.Text = "Nội dung";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell164.Weight = 1.7894722369823066;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable2.SizeF = new System.Drawing.SizeF(797F, 45F);
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163,
            this.lblSTTChiThiHQ,
            this.lblNgaychithihaiquan1,
            this.lblTenchithihaiquan1,
            this.lblNoidungchithihaiquan1});
            this.xrTableRow21.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.StylePriority.UseFont = false;
            this.xrTableRow21.Weight = 1.7999999999999998;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Weight = 0.12830196571191049;
            // 
            // lblSTTChiThiHQ
            // 
            this.lblSTTChiThiHQ.Name = "lblSTTChiThiHQ";
            this.lblSTTChiThiHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSTTChiThiHQ.StylePriority.UsePadding = false;
            this.lblSTTChiThiHQ.Text = "1";
            this.lblSTTChiThiHQ.Weight = 0.067924511091161821;
            // 
            // lblNgaychithihaiquan1
            // 
            this.lblNgaychithihaiquan1.Name = "lblNgaychithihaiquan1";
            this.lblNgaychithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNgaychithihaiquan1.Text = "dd/MM/yyyy";
            this.lblNgaychithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaychithihaiquan1.Weight = 0.25833324632305432;
            // 
            // lblTenchithihaiquan1
            // 
            this.lblTenchithihaiquan1.Name = "lblTenchithihaiquan1";
            this.lblTenchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblTenchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
            this.lblTenchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTenchithihaiquan1.Weight = 0.78738225493945191;
            // 
            // lblNoidungchithihaiquan1
            // 
            this.lblNoidungchithihaiquan1.Multiline = true;
            this.lblNoidungchithihaiquan1.Name = "lblNoidungchithihaiquan1";
            this.lblNoidungchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNoidungchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWW0WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3";
            this.lblNoidungchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNoidungchithihaiquan1.Weight = 1.7656047299442748;
            // 
            // ToKhaiTamHangHoaXuatKhau2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(12, 16, 22, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTrang;
        internal DevExpress.XtraReports.UI.XRTable xrTable1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell lblSotokhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell lblSotokhaidautien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell lblSonhanhtokhaichianho;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell lblTongsotokhaichianho;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell lblSotokhaitamnhaptaixuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell lblMaphanloaikiemtra;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell lblMaloaihinh;
        private DevExpress.XtraReports.UI.XRTableCell lblMaphanloaihanghoa;
        private DevExpress.XtraReports.UI.XRTableCell lblMahieuphuongthucvanchuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell lblMasothuedaidien;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell lblTencoquanhaiquantiepnhantokhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell lblNhomxulyhoso;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaydangky;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblGiodangky;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaythaydoidangky;
        private DevExpress.XtraReports.UI.XRTableCell lblGiothaydoidangky;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell lblThoihantainhapxuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell lblBieuthitruonghophethan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblMaxephang1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaxephang2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell lblMaxephang3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblMaxephang4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell lblMaxephang5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell lblTendiachixephang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell lblDiachixephang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT1;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer1;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT2;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer2;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT3;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer3;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT4;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer4;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT5;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer5;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTChiThiHQ;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaychithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell lblTenchithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell lblNoidungchithihaiquan1;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanloai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
    }
}
