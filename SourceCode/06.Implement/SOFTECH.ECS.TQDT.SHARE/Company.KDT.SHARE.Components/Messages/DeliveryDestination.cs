﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class DeliveryDestination
    {
        /// <summary>
        /// địa điểm giao hàng
        /// </summary>
        [XmlElement("line")]
        public string Line { get; set; }

    }
}
