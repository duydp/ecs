﻿namespace Company.Interface.Report
{
    partial class ToKhaiTNTX_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSoHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongBanDau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTaiNX = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongConLai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable17,
            this.xrTable14,
            this.xrTable13,
            this.xrTable12,
            this.xrTable11,
            this.xrTable10,
            this.xrTable9,
            this.xrTable8,
            this.xrTable7,
            this.xrTable6,
            this.xrTable5,
            this.xrTable1,
            this.xrTable4,
            this.xrTable3,
            this.xrTable2,
            this.xrTable15});
            this.Detail.HeightF = 1135.417F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 78.33331F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTable4.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.Text = "15";
            this.xrTableCell64.Weight = 0.38541664123535158;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Text = "Mã số hàng hóa ";
            this.xrTableCell65.Weight = 1.1083332061767577;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell66.Weight = 1.9564575195312495;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Text = "Số lượng ban đầu";
            this.xrTableCell67.Weight = 1.9404174804687502;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Text = "123.456.789.012.345";
            this.xrTableCell68.Weight = 1.5078125000000004;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Text = "-";
            this.xrTableCell69.Weight = 0.33203132629394516;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell70.Multiline = true;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Text = "XXXE\r\n";
            this.xrTableCell70.Weight = 0.55953071594238279;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.Weight = 3.4502073669433586;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell72.Weight = 1.9404174804687502;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Text = "123.456.789.012.345";
            this.xrTableCell73.Weight = 1.5078125000000004;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Text = "-";
            this.xrTableCell74.Weight = 0.33203132629394516;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell75.Multiline = true;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.Text = "XXXE\r\n";
            this.xrTableCell75.Weight = 0.55953071594238279;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.StylePriority.UseBorders = false;
            this.xrTableRow18.Weight = 1;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseBorders = false;
            this.xrTableCell76.Weight = 3.4502073669433586;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Text = "Số lượng còn lại";
            this.xrTableCell77.Weight = 1.9404174804687502;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "123.456.789.012.345";
            this.xrTableCell78.Weight = 1.5078125000000004;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Text = "-";
            this.xrTableCell79.Weight = 0.33203132629394516;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell80.Multiline = true;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Text = "XXXE\r\n";
            this.xrTableCell80.Weight = 0.55953071594238279;
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 148.3333F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15});
            this.xrTable3.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.xrTableCell38,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Text = "16";
            this.xrTableCell36.Weight = 0.38541664123535158;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "Mã số hàng hóa ";
            this.xrTableCell38.Weight = 1.1083332061767577;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell44.Weight = 1.9564575195312495;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "Số lượng ban đầu";
            this.xrTableCell45.Weight = 1.9404174804687502;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Text = "123.456.789.012.345";
            this.xrTableCell51.Weight = 1.5078125000000004;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Text = "-";
            this.xrTableCell52.Weight = 0.33203132629394516;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell53.Multiline = true;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.Text = "XXXE\r\n";
            this.xrTableCell53.Weight = 0.55953071594238279;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.Weight = 3.4502073669433586;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell55.Weight = 1.9404174804687502;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Text = "123.456.789.012.345";
            this.xrTableCell56.Weight = 1.5078125000000004;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Text = "-";
            this.xrTableCell57.Weight = 0.33203132629394516;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell58.Multiline = true;
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.Text = "XXXE\r\n";
            this.xrTableCell58.Weight = 0.55953071594238279;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 1;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.Weight = 3.4502073669433586;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Text = "Số lượng còn lại";
            this.xrTableCell60.Weight = 1.9404174804687502;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Text = "123.456.789.012.345";
            this.xrTableCell61.Weight = 1.5078125000000004;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "-";
            this.xrTableCell62.Weight = 0.33203132629394516;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Multiline = true;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.Text = "XXXE\r\n";
            this.xrTableCell63.Weight = 0.55953071594238279;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.BorderWidth = 1;
            this.xrTable2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 8.333333F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow12,
            this.xrTableRow11});
            this.xrTable2.SizeF = new System.Drawing.SizeF(778.9999F, 70F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseBorderWidth = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24,
            this.lblMaSoHangHoa,
            this.xrTableCell31,
            this.lblSoLuongBanDau,
            this.xrTableCell35,
            this.xrTableCell27});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.StylePriority.UseBorders = false;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "14";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell23.Weight = 0.38541664123535158;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "Mã số hàng hóa ";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell24.Weight = 1.1083332061767577;
            // 
            // lblMaSoHangHoa
            // 
            this.lblMaSoHangHoa.Name = "lblMaSoHangHoa";
            this.lblMaSoHangHoa.StylePriority.UseTextAlignment = false;
            this.lblMaSoHangHoa.Text = "XXXX.XX.XX.X1XF";
            this.lblMaSoHangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMaSoHangHoa.Weight = 1.9564575195312495;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "Số lượng ban đầu";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell31.Weight = 1.9404174804687502;
            // 
            // lblSoLuongBanDau
            // 
            this.lblSoLuongBanDau.Name = "lblSoLuongBanDau";
            this.lblSoLuongBanDau.StylePriority.UseTextAlignment = false;
            this.lblSoLuongBanDau.Text = "123.456.789.012.345";
            this.lblSoLuongBanDau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoLuongBanDau.Weight = 1.5078125000000004;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "-";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell35.Weight = 0.33203132629394516;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "XXXE\r\n";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell27.Weight = 0.55953071594238279;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46,
            this.xrTableCell47,
            this.lblSoLuongTaiNX,
            this.xrTableCell49,
            this.xrTableCell50});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 1;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Weight = 3.4502073669433586;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell47.Weight = 1.9404174804687502;
            // 
            // lblSoLuongTaiNX
            // 
            this.lblSoLuongTaiNX.Name = "lblSoLuongTaiNX";
            this.lblSoLuongTaiNX.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTaiNX.Text = "123.456.789.012.345";
            this.lblSoLuongTaiNX.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoLuongTaiNX.Weight = 1.5078125000000004;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = "-";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell49.Weight = 0.33203132629394516;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell50.Multiline = true;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.Text = "XXXE\r\n";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell50.Weight = 0.55953071594238279;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell40,
            this.lblSoLuongConLai,
            this.xrTableCell42,
            this.xrTableCell43});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.StylePriority.UseBorders = false;
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.Weight = 3.4502073669433586;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.Text = "Số lượng còn lại";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell40.Weight = 1.9404174804687502;
            // 
            // lblSoLuongConLai
            // 
            this.lblSoLuongConLai.Name = "lblSoLuongConLai";
            this.lblSoLuongConLai.StylePriority.UseTextAlignment = false;
            this.lblSoLuongConLai.Text = "123.456.789.012.345";
            this.lblSoLuongConLai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoLuongConLai.Weight = 1.5078125000000004;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.Text = "-";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell42.Weight = 0.33203132629394516;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Multiline = true;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.Text = "XXXE\r\n";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell43.Weight = 0.55953071594238279;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 22F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable16,
            this.lblTenThongTinXuat});
            this.ReportHeader.HeightF = 31.25002F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable16
            // 
            this.xrTable16.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(740.625F, 0F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow52});
            this.xrTable16.SizeF = new System.Drawing.SizeF(47.06976F, 21.25001F);
            this.xrTable16.StylePriority.UseFont = false;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoTrang});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 0.6;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoTrang.StylePriority.UsePadding = false;
            this.lblSoTrang.Text = "2/2";
            this.lblSoTrang.Weight = 3;
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(78.125F, 9.536743E-06F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(491.875F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Tờ khai bổ sung thông tin quản lý hàng TNTX, TXTN";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 218.3333F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3});
            this.xrTable1.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Text = "17";
            this.xrTableCell1.Weight = 0.38541664123535158;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Mã số hàng hóa ";
            this.xrTableCell2.Weight = 1.1083332061767577;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell3.Weight = 1.9564575195312495;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "Số lượng ban đầu";
            this.xrTableCell4.Weight = 1.9404174804687502;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "123.456.789.012.345";
            this.xrTableCell5.Weight = 1.5078125000000004;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "-";
            this.xrTableCell6.Weight = 0.33203132629394516;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Text = "XXXE\r\n";
            this.xrTableCell7.Weight = 0.55953071594238279;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Weight = 3.4502073669433586;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell9.Weight = 1.9404174804687502;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "123.456.789.012.345";
            this.xrTableCell10.Weight = 1.5078125000000004;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "-";
            this.xrTableCell11.Weight = 0.33203132629394516;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell12.Multiline = true;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.Text = "XXXE\r\n";
            this.xrTableCell12.Weight = 0.55953071594238279;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Weight = 3.4502073669433586;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "Số lượng còn lại";
            this.xrTableCell14.Weight = 1.9404174804687502;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Text = "123.456.789.012.345";
            this.xrTableCell15.Weight = 1.5078125000000004;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "-";
            this.xrTableCell16.Weight = 0.33203132629394516;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Multiline = true;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Text = "XXXE\r\n";
            this.xrTableCell17.Weight = 0.55953071594238279;
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 288.3333F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6});
            this.xrTable5.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell25,
            this.xrTableCell26});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.Text = "18";
            this.xrTableCell18.Weight = 0.38541664123535158;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "Mã số hàng hóa ";
            this.xrTableCell19.Weight = 1.1083332061767577;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell20.Weight = 1.9564575195312495;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Text = "Số lượng ban đầu";
            this.xrTableCell21.Weight = 1.9404174804687502;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "123.456.789.012.345";
            this.xrTableCell22.Weight = 1.5078125000000004;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Text = "-";
            this.xrTableCell25.Weight = 0.33203132629394516;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Text = "XXXE\r\n";
            this.xrTableCell26.Weight = 0.55953071594238279;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell37});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Weight = 3.4502073669433586;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell30.Weight = 1.9404174804687502;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Text = "123.456.789.012.345";
            this.xrTableCell33.Weight = 1.5078125000000004;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Text = "-";
            this.xrTableCell34.Weight = 0.33203132629394516;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell37.Multiline = true;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Text = "XXXE\r\n";
            this.xrTableCell37.Weight = 0.55953071594238279;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell84,
            this.xrTableCell85});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Weight = 3.4502073669433586;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Text = "Số lượng còn lại";
            this.xrTableCell82.Weight = 1.9404174804687502;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Text = "123.456.789.012.345";
            this.xrTableCell83.Weight = 1.5078125000000004;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Text = "-";
            this.xrTableCell84.Weight = 0.33203132629394516;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell85.Multiline = true;
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.Text = "XXXE\r\n";
            this.xrTableCell85.Weight = 0.55953071594238279;
            // 
            // xrTable6
            // 
            this.xrTable6.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 358.3333F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrTable6.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.Text = "19";
            this.xrTableCell86.Weight = 0.38541664123535158;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Text = "Mã số hàng hóa ";
            this.xrTableCell87.Weight = 1.1083332061767577;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell88.Weight = 1.9564575195312495;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Text = "Số lượng ban đầu";
            this.xrTableCell89.Weight = 1.9404174804687502;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Text = "123.456.789.012.345";
            this.xrTableCell90.Weight = 1.5078125000000004;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Text = "-";
            this.xrTableCell91.Weight = 0.33203132629394516;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell92.Multiline = true;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Text = "XXXE\r\n";
            this.xrTableCell92.Weight = 0.55953071594238279;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 3.4502073669433586;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell94.Weight = 1.9404174804687502;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Text = "123.456.789.012.345";
            this.xrTableCell95.Weight = 1.5078125000000004;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Text = "-";
            this.xrTableCell96.Weight = 0.33203132629394516;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell97.Multiline = true;
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.Text = "XXXE\r\n";
            this.xrTableCell97.Weight = 0.55953071594238279;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell98,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 1;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.Weight = 3.4502073669433586;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Text = "Số lượng còn lại";
            this.xrTableCell99.Weight = 1.9404174804687502;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Text = "123.456.789.012.345";
            this.xrTableCell100.Weight = 1.5078125000000004;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Text = "-";
            this.xrTableCell101.Weight = 0.33203132629394516;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell102.Multiline = true;
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseBorders = false;
            this.xrTableCell102.Text = "XXXE\r\n";
            this.xrTableCell102.Weight = 0.55953071594238279;
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 428.3333F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21});
            this.xrTable7.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell109});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Text = "20";
            this.xrTableCell103.Weight = 0.38541664123535158;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Text = "Mã số hàng hóa ";
            this.xrTableCell104.Weight = 1.1083332061767577;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell105.Weight = 1.9564575195312495;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Text = "Số lượng ban đầu";
            this.xrTableCell106.Weight = 1.9404174804687502;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Text = "123.456.789.012.345";
            this.xrTableCell107.Weight = 1.5078125000000004;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Text = "-";
            this.xrTableCell108.Weight = 0.33203132629394516;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell109.Multiline = true;
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Text = "XXXE\r\n";
            this.xrTableCell109.Weight = 0.55953071594238279;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell110,
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.Weight = 3.4502073669433586;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell111.Weight = 1.9404174804687502;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Text = "123.456.789.012.345";
            this.xrTableCell112.Weight = 1.5078125000000004;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Text = "-";
            this.xrTableCell113.Weight = 0.33203132629394516;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell114.Multiline = true;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Text = "XXXE\r\n";
            this.xrTableCell114.Weight = 0.55953071594238279;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell115,
            this.xrTableCell116,
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.StylePriority.UseBorders = false;
            this.xrTableRow21.Weight = 1;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Weight = 3.4502073669433586;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Text = "Số lượng còn lại";
            this.xrTableCell116.Weight = 1.9404174804687502;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Text = "123.456.789.012.345";
            this.xrTableCell117.Weight = 1.5078125000000004;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Text = "-";
            this.xrTableCell118.Weight = 0.33203132629394516;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell119.Multiline = true;
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Text = "XXXE\r\n";
            this.xrTableCell119.Weight = 0.55953071594238279;
            // 
            // xrTable8
            // 
            this.xrTable8.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 498.3334F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24});
            this.xrTable8.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable8.StylePriority.UseFont = false;
            this.xrTable8.StylePriority.UseTextAlignment = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell120,
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell125,
            this.xrTableCell126});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.Text = "21";
            this.xrTableCell120.Weight = 0.38541664123535158;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Text = "Mã số hàng hóa ";
            this.xrTableCell121.Weight = 1.1083332061767577;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell122.Weight = 1.9564575195312495;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Text = "Số lượng ban đầu";
            this.xrTableCell123.Weight = 1.9404174804687502;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Text = "123.456.789.012.345";
            this.xrTableCell124.Weight = 1.5078125000000004;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Text = "-";
            this.xrTableCell125.Weight = 0.33203132629394516;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell126.Multiline = true;
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.Text = "XXXE\r\n";
            this.xrTableCell126.Weight = 0.55953071594238279;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.xrTableCell128,
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell131});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.Weight = 3.4502073669433586;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell128.Weight = 1.9404174804687502;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Text = "123.456.789.012.345";
            this.xrTableCell129.Weight = 1.5078125000000004;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "-";
            this.xrTableCell130.Weight = 0.33203132629394516;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell131.Multiline = true;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.Text = "XXXE\r\n";
            this.xrTableCell131.Weight = 0.55953071594238279;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell135,
            this.xrTableCell136});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.StylePriority.UseBorders = false;
            this.xrTableRow24.Weight = 1;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Weight = 3.4502073669433586;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Text = "Số lượng còn lại";
            this.xrTableCell133.Weight = 1.9404174804687502;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Text = "123.456.789.012.345";
            this.xrTableCell134.Weight = 1.5078125000000004;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Text = "-";
            this.xrTableCell135.Weight = 0.33203132629394516;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell136.Multiline = true;
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.StylePriority.UseBorders = false;
            this.xrTableCell136.Text = "XXXE\r\n";
            this.xrTableCell136.Weight = 0.55953071594238279;
            // 
            // xrTable9
            // 
            this.xrTable9.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 568.3334F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27});
            this.xrTable9.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable9.StylePriority.UseFont = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell143});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.StylePriority.UseBorders = false;
            this.xrTableCell137.Text = "22";
            this.xrTableCell137.Weight = 0.38541664123535158;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "Mã số hàng hóa ";
            this.xrTableCell138.Weight = 1.1083332061767577;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell139.Weight = 1.9564575195312495;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Text = "Số lượng ban đầu";
            this.xrTableCell140.Weight = 1.9404174804687502;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Text = "123.456.789.012.345";
            this.xrTableCell141.Weight = 1.5078125000000004;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Text = "-";
            this.xrTableCell142.Weight = 0.33203132629394516;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell143.Multiline = true;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.Text = "XXXE\r\n";
            this.xrTableCell143.Weight = 0.55953071594238279;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144,
            this.xrTableCell145,
            this.xrTableCell146,
            this.xrTableCell147,
            this.xrTableCell148});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.Weight = 3.4502073669433586;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell145.Weight = 1.9404174804687502;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Text = "123.456.789.012.345";
            this.xrTableCell146.Weight = 1.5078125000000004;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Text = "-";
            this.xrTableCell147.Weight = 0.33203132629394516;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell148.Multiline = true;
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBorders = false;
            this.xrTableCell148.Text = "XXXE\r\n";
            this.xrTableCell148.Weight = 0.55953071594238279;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell151,
            this.xrTableCell152,
            this.xrTableCell153});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.StylePriority.UseBorders = false;
            this.xrTableRow27.Weight = 1;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBorders = false;
            this.xrTableCell149.Weight = 3.4502073669433586;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Text = "Số lượng còn lại";
            this.xrTableCell150.Weight = 1.9404174804687502;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Text = "123.456.789.012.345";
            this.xrTableCell151.Weight = 1.5078125000000004;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Text = "-";
            this.xrTableCell152.Weight = 0.33203132629394516;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell153.Multiline = true;
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.Text = "XXXE\r\n";
            this.xrTableCell153.Weight = 0.55953071594238279;
            // 
            // xrTable10
            // 
            this.xrTable10.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 638.3334F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30});
            this.xrTable10.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable10.StylePriority.UseFont = false;
            this.xrTable10.StylePriority.UseTextAlignment = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell154,
            this.xrTableCell155,
            this.xrTableCell156,
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell159,
            this.xrTableCell160});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseBorders = false;
            this.xrTableCell154.Text = "23";
            this.xrTableCell154.Weight = 0.38541664123535158;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Text = "Mã số hàng hóa ";
            this.xrTableCell155.Weight = 1.1083332061767577;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell156.Weight = 1.9564575195312495;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Text = "Số lượng ban đầu";
            this.xrTableCell157.Weight = 1.9404174804687502;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Text = "123.456.789.012.345";
            this.xrTableCell158.Weight = 1.5078125000000004;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Text = "-";
            this.xrTableCell159.Weight = 0.33203132629394516;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell160.Multiline = true;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.Text = "XXXE\r\n";
            this.xrTableCell160.Weight = 0.55953071594238279;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell161,
            this.xrTableCell162,
            this.xrTableCell163,
            this.xrTableCell164,
            this.xrTableCell165});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.StylePriority.UseBorders = false;
            this.xrTableCell161.Weight = 3.4502073669433586;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell162.Weight = 1.9404174804687502;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Text = "123.456.789.012.345";
            this.xrTableCell163.Weight = 1.5078125000000004;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Text = "-";
            this.xrTableCell164.Weight = 0.33203132629394516;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell165.Multiline = true;
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.StylePriority.UseBorders = false;
            this.xrTableCell165.Text = "XXXE\r\n";
            this.xrTableCell165.Weight = 0.55953071594238279;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell166,
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.StylePriority.UseBorders = false;
            this.xrTableRow30.Weight = 1;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.Weight = 3.4502073669433586;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Text = "Số lượng còn lại";
            this.xrTableCell167.Weight = 1.9404174804687502;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Text = "123.456.789.012.345";
            this.xrTableCell168.Weight = 1.5078125000000004;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Text = "-";
            this.xrTableCell169.Weight = 0.33203132629394516;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell170.Multiline = true;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Text = "XXXE\r\n";
            this.xrTableCell170.Weight = 0.55953071594238279;
            // 
            // xrTable11
            // 
            this.xrTable11.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 708.3333F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33});
            this.xrTable11.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable11.StylePriority.UseFont = false;
            this.xrTable11.StylePriority.UseTextAlignment = false;
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171,
            this.xrTableCell172,
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell177});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.StylePriority.UseBorders = false;
            this.xrTableCell171.Text = "24";
            this.xrTableCell171.Weight = 0.38541664123535158;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Text = "Mã số hàng hóa ";
            this.xrTableCell172.Weight = 1.1083332061767577;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell173.Weight = 1.9564575195312495;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Text = "Số lượng ban đầu";
            this.xrTableCell174.Weight = 1.9404174804687502;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Text = "123.456.789.012.345";
            this.xrTableCell175.Weight = 1.5078125000000004;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Text = "-";
            this.xrTableCell176.Weight = 0.33203132629394516;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell177.Multiline = true;
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.StylePriority.UseBorders = false;
            this.xrTableCell177.Text = "XXXE\r\n";
            this.xrTableCell177.Weight = 0.55953071594238279;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell178,
            this.xrTableCell179,
            this.xrTableCell180,
            this.xrTableCell181,
            this.xrTableCell182});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.Weight = 3.4502073669433586;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell179.Weight = 1.9404174804687502;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "123.456.789.012.345";
            this.xrTableCell180.Weight = 1.5078125000000004;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Text = "-";
            this.xrTableCell181.Weight = 0.33203132629394516;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell182.Multiline = true;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.Text = "XXXE\r\n";
            this.xrTableCell182.Weight = 0.55953071594238279;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell187});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.StylePriority.UseBorders = false;
            this.xrTableRow33.Weight = 1;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.StylePriority.UseBorders = false;
            this.xrTableCell183.Weight = 3.4502073669433586;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "Số lượng còn lại";
            this.xrTableCell184.Weight = 1.9404174804687502;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Text = "123.456.789.012.345";
            this.xrTableCell185.Weight = 1.5078125000000004;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Text = "-";
            this.xrTableCell186.Weight = 0.33203132629394516;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell187.Multiline = true;
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.StylePriority.UseBorders = false;
            this.xrTableCell187.Text = "XXXE\r\n";
            this.xrTableCell187.Weight = 0.55953071594238279;
            // 
            // xrTable12
            // 
            this.xrTable12.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 778.3333F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36});
            this.xrTable12.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable12.StylePriority.UseFont = false;
            this.xrTable12.StylePriority.UseTextAlignment = false;
            this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell188,
            this.xrTableCell189,
            this.xrTableCell190,
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseBorders = false;
            this.xrTableCell188.Text = "25";
            this.xrTableCell188.Weight = 0.38541664123535158;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Text = "Mã số hàng hóa ";
            this.xrTableCell189.Weight = 1.1083332061767577;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell190.Weight = 1.9564575195312495;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Text = "Số lượng ban đầu";
            this.xrTableCell191.Weight = 1.9404174804687502;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Text = "123.456.789.012.345";
            this.xrTableCell192.Weight = 1.5078125000000004;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Text = "-";
            this.xrTableCell193.Weight = 0.33203132629394516;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell194.Multiline = true;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.StylePriority.UseBorders = false;
            this.xrTableCell194.Text = "XXXE\r\n";
            this.xrTableCell194.Weight = 0.55953071594238279;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell199});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.StylePriority.UseBorders = false;
            this.xrTableCell195.Weight = 3.4502073669433586;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell196.Weight = 1.9404174804687502;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Text = "123.456.789.012.345";
            this.xrTableCell197.Weight = 1.5078125000000004;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Text = "-";
            this.xrTableCell198.Weight = 0.33203132629394516;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell199.Multiline = true;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.StylePriority.UseBorders = false;
            this.xrTableCell199.Text = "XXXE\r\n";
            this.xrTableCell199.Weight = 0.55953071594238279;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell202,
            this.xrTableCell203,
            this.xrTableCell204});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.StylePriority.UseBorders = false;
            this.xrTableRow36.Weight = 1;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.StylePriority.UseBorders = false;
            this.xrTableCell200.Weight = 3.4502073669433586;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Text = "Số lượng còn lại";
            this.xrTableCell201.Weight = 1.9404174804687502;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Text = "123.456.789.012.345";
            this.xrTableCell202.Weight = 1.5078125000000004;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Text = "-";
            this.xrTableCell203.Weight = 0.33203132629394516;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell204.Multiline = true;
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.StylePriority.UseBorders = false;
            this.xrTableCell204.Text = "XXXE\r\n";
            this.xrTableCell204.Weight = 0.55953071594238279;
            // 
            // xrTable13
            // 
            this.xrTable13.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 848.3333F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39});
            this.xrTable13.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable13.StylePriority.UseFont = false;
            this.xrTable13.StylePriority.UseTextAlignment = false;
            this.xrTable13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell205,
            this.xrTableCell206,
            this.xrTableCell207,
            this.xrTableCell208,
            this.xrTableCell209,
            this.xrTableCell210,
            this.xrTableCell211});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.StylePriority.UseBorders = false;
            this.xrTableCell205.Text = "26";
            this.xrTableCell205.Weight = 0.38541664123535158;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Text = "Mã số hàng hóa ";
            this.xrTableCell206.Weight = 1.1083332061767577;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell207.Weight = 1.9564575195312495;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Text = "Số lượng ban đầu";
            this.xrTableCell208.Weight = 1.9404174804687502;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Text = "123.456.789.012.345";
            this.xrTableCell209.Weight = 1.5078125000000004;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Text = "-";
            this.xrTableCell210.Weight = 0.33203132629394516;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell211.Multiline = true;
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.StylePriority.UseBorders = false;
            this.xrTableCell211.Text = "XXXE\r\n";
            this.xrTableCell211.Weight = 0.55953071594238279;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell214,
            this.xrTableCell215,
            this.xrTableCell216});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.StylePriority.UseBorders = false;
            this.xrTableCell212.Weight = 3.4502073669433586;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell213.Weight = 1.9404174804687502;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Text = "123.456.789.012.345";
            this.xrTableCell214.Weight = 1.5078125000000004;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Text = "-";
            this.xrTableCell215.Weight = 0.33203132629394516;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell216.Multiline = true;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.StylePriority.UseBorders = false;
            this.xrTableCell216.Text = "XXXE\r\n";
            this.xrTableCell216.Weight = 0.55953071594238279;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell217,
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTableCell220,
            this.xrTableCell221});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.StylePriority.UseBorders = false;
            this.xrTableRow39.Weight = 1;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.StylePriority.UseBorders = false;
            this.xrTableCell217.Weight = 3.4502073669433586;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Text = "Số lượng còn lại";
            this.xrTableCell218.Weight = 1.9404174804687502;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Text = "123.456.789.012.345";
            this.xrTableCell219.Weight = 1.5078125000000004;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Text = "-";
            this.xrTableCell220.Weight = 0.33203132629394516;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell221.Multiline = true;
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseBorders = false;
            this.xrTableCell221.Text = "XXXE\r\n";
            this.xrTableCell221.Weight = 0.55953071594238279;
            // 
            // xrTable14
            // 
            this.xrTable14.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 918.3332F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42});
            this.xrTable14.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable14.StylePriority.UseFont = false;
            this.xrTable14.StylePriority.UseTextAlignment = false;
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell222,
            this.xrTableCell223,
            this.xrTableCell224,
            this.xrTableCell225,
            this.xrTableCell226,
            this.xrTableCell227,
            this.xrTableCell228});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.StylePriority.UseBorders = false;
            this.xrTableCell222.Text = "27";
            this.xrTableCell222.Weight = 0.38541664123535158;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Text = "Mã số hàng hóa ";
            this.xrTableCell223.Weight = 1.1083332061767577;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell224.Weight = 1.9564575195312495;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Text = "Số lượng ban đầu";
            this.xrTableCell225.Weight = 1.9404174804687502;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Text = "123.456.789.012.345";
            this.xrTableCell226.Weight = 1.5078125000000004;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Text = "-";
            this.xrTableCell227.Weight = 0.33203132629394516;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell228.Multiline = true;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseBorders = false;
            this.xrTableCell228.Text = "XXXE\r\n";
            this.xrTableCell228.Weight = 0.55953071594238279;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell229,
            this.xrTableCell230,
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTableCell233});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.Weight = 3.4502073669433586;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell230.Weight = 1.9404174804687502;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Text = "123.456.789.012.345";
            this.xrTableCell231.Weight = 1.5078125000000004;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Text = "-";
            this.xrTableCell232.Weight = 0.33203132629394516;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell233.Multiline = true;
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseBorders = false;
            this.xrTableCell233.Text = "XXXE\r\n";
            this.xrTableCell233.Weight = 0.55953071594238279;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell234,
            this.xrTableCell235,
            this.xrTableCell236,
            this.xrTableCell237,
            this.xrTableCell238});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.StylePriority.UseBorders = false;
            this.xrTableRow42.Weight = 1;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.StylePriority.UseBorders = false;
            this.xrTableCell234.Weight = 3.4502073669433586;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Text = "Số lượng còn lại";
            this.xrTableCell235.Weight = 1.9404174804687502;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Text = "123.456.789.012.345";
            this.xrTableCell236.Weight = 1.5078125000000004;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Text = "-";
            this.xrTableCell237.Weight = 0.33203132629394516;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell238.Multiline = true;
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseBorders = false;
            this.xrTableCell238.Text = "XXXE\r\n";
            this.xrTableCell238.Weight = 0.55953071594238279;
            // 
            // xrTable15
            // 
            this.xrTable15.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 988.3332F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45});
            this.xrTable15.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable15.StylePriority.UseFont = false;
            this.xrTable15.StylePriority.UseTextAlignment = false;
            this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239,
            this.xrTableCell240,
            this.xrTableCell241,
            this.xrTableCell242,
            this.xrTableCell243,
            this.xrTableCell244,
            this.xrTableCell245});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseBorders = false;
            this.xrTableCell239.Text = "28";
            this.xrTableCell239.Weight = 0.38541664123535158;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Text = "Mã số hàng hóa ";
            this.xrTableCell240.Weight = 1.1083332061767577;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell241.Weight = 1.9564575195312495;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.Text = "Số lượng ban đầu";
            this.xrTableCell242.Weight = 1.9404174804687502;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Text = "123.456.789.012.345";
            this.xrTableCell243.Weight = 1.5078125000000004;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Text = "-";
            this.xrTableCell244.Weight = 0.33203132629394516;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell245.Multiline = true;
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseBorders = false;
            this.xrTableCell245.Text = "XXXE\r\n";
            this.xrTableCell245.Weight = 0.55953071594238279;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTableCell250});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.StylePriority.UseBorders = false;
            this.xrTableCell246.Weight = 3.4502073669433586;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell247.Weight = 1.9404174804687502;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Text = "123.456.789.012.345";
            this.xrTableCell248.Weight = 1.5078125000000004;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Text = "-";
            this.xrTableCell249.Weight = 0.33203132629394516;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell250.Multiline = true;
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBorders = false;
            this.xrTableCell250.Text = "XXXE\r\n";
            this.xrTableCell250.Weight = 0.55953071594238279;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell251,
            this.xrTableCell252,
            this.xrTableCell253,
            this.xrTableCell254,
            this.xrTableCell255});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.StylePriority.UseBorders = false;
            this.xrTableRow45.Weight = 1;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.StylePriority.UseBorders = false;
            this.xrTableCell251.Weight = 3.4502073669433586;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Text = "Số lượng còn lại";
            this.xrTableCell252.Weight = 1.9404174804687502;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Text = "123.456.789.012.345";
            this.xrTableCell253.Weight = 1.5078125000000004;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Text = "-";
            this.xrTableCell254.Weight = 0.33203132629394516;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell255.Multiline = true;
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.StylePriority.UseBorders = false;
            this.xrTableCell255.Text = "XXXE\r\n";
            this.xrTableCell255.Weight = 0.55953071594238279;
            // 
            // xrTable17
            // 
            this.xrTable17.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(8.694768F, 1058.333F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48});
            this.xrTable17.SizeF = new System.Drawing.SizeF(779F, 70F);
            this.xrTable17.StylePriority.UseFont = false;
            this.xrTable17.StylePriority.UseTextAlignment = false;
            this.xrTable17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell256,
            this.xrTableCell257,
            this.xrTableCell258,
            this.xrTableCell259,
            this.xrTableCell260,
            this.xrTableCell261,
            this.xrTableCell262});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.StylePriority.UseBorders = false;
            this.xrTableCell256.Text = "29";
            this.xrTableCell256.Weight = 0.38541664123535158;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Text = "Mã số hàng hóa ";
            this.xrTableCell257.Weight = 1.1083332061767577;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Text = "XXXX.XX.XX.X1XF";
            this.xrTableCell258.Weight = 1.9564575195312495;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Text = "Số lượng ban đầu";
            this.xrTableCell259.Weight = 1.9404174804687502;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.Text = "123.456.789.012.345";
            this.xrTableCell260.Weight = 1.5078125000000004;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.Text = "-";
            this.xrTableCell261.Weight = 0.33203132629394516;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell262.Multiline = true;
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.StylePriority.UseBorders = false;
            this.xrTableCell262.Text = "XXXE\r\n";
            this.xrTableCell262.Weight = 0.55953071594238279;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell265,
            this.xrTableCell266,
            this.xrTableCell267});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBorders = false;
            this.xrTableCell263.Weight = 3.4502073669433586;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.Text = "Số lượng đã tái nhập/tái xuất";
            this.xrTableCell264.Weight = 1.9404174804687502;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.Text = "123.456.789.012.345";
            this.xrTableCell265.Weight = 1.5078125000000004;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.Text = "-";
            this.xrTableCell266.Weight = 0.33203132629394516;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell267.Multiline = true;
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBorders = false;
            this.xrTableCell267.Text = "XXXE\r\n";
            this.xrTableCell267.Weight = 0.55953071594238279;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell268,
            this.xrTableCell269,
            this.xrTableCell270,
            this.xrTableCell271,
            this.xrTableCell272});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.StylePriority.UseBorders = false;
            this.xrTableRow48.Weight = 1;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.StylePriority.UseBorders = false;
            this.xrTableCell268.Weight = 3.4502073669433586;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.Text = "Số lượng còn lại";
            this.xrTableCell269.Weight = 1.9404174804687502;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.Text = "123.456.789.012.345";
            this.xrTableCell270.Weight = 1.5078125000000004;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.Text = "-";
            this.xrTableCell271.Weight = 0.33203132629394516;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell272.Multiline = true;
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseBorders = false;
            this.xrTableCell272.Text = "XXXE\r\n";
            this.xrTableCell272.Weight = 0.55953071594238279;
            // 
            // ToKhaiTNTX_1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.Margins = new System.Drawing.Printing.Margins(15, 25, 22, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSoHangHoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongBanDau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTaiNX;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongConLai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTrang;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
    }
}
