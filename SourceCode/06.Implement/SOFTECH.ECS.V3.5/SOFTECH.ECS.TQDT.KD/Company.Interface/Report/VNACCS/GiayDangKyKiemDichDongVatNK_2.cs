using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class GiayDangKyKiemDichDongVatNK_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public GiayDangKyKiemDichDongVatNK_2()
        {
            InitializeComponent();
        }
        public void BindingReport(VAJP010 vajp)
        {
            int HangHoa = vajp.HangHoa.Count;
            if (HangHoa <= 5)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa /5, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblTenTau.Text = vajp.SPN.GetValue().ToString().ToUpper();
            lblQuocTich.Text = vajp.NTN.GetValue().ToString().ToUpper();
            lblTenThuyenTruong.Text = vajp.CAN.GetValue().ToString().ToUpper();
            lblTenBacSi.Text = vajp.DON.GetValue().ToString().ToUpper();
            lblSoThuyenVien.Text = vajp.CRN.GetValue().ToString().ToUpper();
            lblSoHanhKhach.Text = vajp.PAN.GetValue().ToString().ToUpper();
            lblCangRoiCuoiCung.Text = vajp.LDP.GetValue().ToString().ToUpper();
            lblCangDenTiepTheo.Text = vajp.NAP.GetValue().ToString().ToUpper();
            lblCangBocHangDauTien.Text = vajp.DCP.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.DOD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayRoiCang.Text = Convert.ToDateTime(vajp.DOD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayRoiCang.Text = "";
            }
            lblTenHangCangDau.Text = vajp.GFP.GetValue().ToString().ToUpper();
            lblSoLuongHangCangDau.Text = vajp.QFP.GetValue().ToString().ToUpper();
            lblDVTSoLuongCangDau.Text = vajp.QFU.GetValue().ToString().ToUpper();
            lblKhoiLuongHangCangDau.Text = vajp.WFP.GetValue().ToString().ToUpper();
            lblDVTKhoiLuongCangDau.Text = vajp.WFU.GetValue().ToString().ToUpper();

            
            DataTable dt = ConvertListToTable(vajp.G01, 10);
            BindingReportHang(dt);

            lblTenHangCanBoc.Text = vajp.DGD.GetValue().ToString().ToUpper();
            lblSoLuongHangCanBoc.Text = vajp.DGQ.GetValue().ToString().ToUpper();
            lblDVTSoLuongHangCanBoc.Text = vajp.DQU.GetValue().ToString().ToUpper();
            lblKhoiLuongHangCanBoc.Text = vajp.DGW.GetValue().ToString().ToUpper();
            lblDVTKhoiLuongHangCanBoc.Text = vajp.DWU.GetValue().ToString().ToUpper();
            lblTenTT.Text = vajp.CAN.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.DOS.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vajp.DOS.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
        }
        private DataTable ConvertListToTable( GroupAttribute group, int loop)
        {
            int STT = 0;
            DataTable gr = new DataTable();
            foreach (PropertiesAttribute attribute in group.listAttribute)
            {
                gr.Columns.Add(attribute.GroupID, attribute.OfType == typeof(int) ? typeof(decimal) : attribute.OfType );
            }
            gr.Columns.Add("STT", typeof(string));
            for (int i = 0; i < loop; i++)
            {
                STT++;
                DataRow dr = gr.NewRow();                       
                foreach (PropertiesAttribute attribute in group.listAttribute)
                {
                    
                    if (attribute.OfType == typeof(DateTime))
                    {
                        if (System.Convert.ToDateTime(attribute.GetValueCollection(i)).Year > 1900)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else if (attribute.OfType == typeof(decimal) || attribute.OfType == typeof(int))
                    {
                        if (System.Convert.ToDecimal(attribute.GetValueCollection(i)) != 0)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else
                        dr[attribute.GroupID] = attribute.GetValueCollection(i);
                   
                }
                dr["STT"] = STT.ToString().ToUpper();
                gr.Rows.Add(dr);
            }
            return gr;
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSoTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblTenHangCangTrungGian.DataBindings.Add("Text", DetailReport.DataSource, "VAJP010_G01");
            lblSoLuongHangCangTrungGian.DataBindings.Add("Text", DetailReport.DataSource, "VAJP010_Q01");
            lblDVTSoLuongCangTrungGian.DataBindings.Add("Text", DetailReport.DataSource, "VAJP010_QU01");
            lblKhoiLuongHangCangTrungGian.DataBindings.Add("Text", DetailReport.DataSource, "VAJP010_W01");
            lblDVTKhoiLuongHangCangTrungGian.DataBindings.Add("Text", DetailReport.DataSource, "VAJP010_WU01");
            lblTenCangTrungGian.DataBindings.Add("Text", DetailReport.DataSource, "VAJP010_P01");
        }

    }
}
