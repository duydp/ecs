﻿namespace Company.Interface
{
    partial class VNACC_TEAForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TEAForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grNguoiXNK_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grDieuChinh_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdNguoiXNK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNguoiXNK");
            this.cmdDieuChinh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDieuChinh");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdNguoiXNK = new Janus.Windows.UI.CommandBars.UICommand("cmdNguoiXNK");
            this.cmdDieuChinh = new Janus.Windows.UI.CommandBars.UICommand("cmdDieuChinh");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cbbPhanLoaiXuatNhapKhau = new Janus.Windows.EditControls.UIComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCoQuanHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtPhamViDangKyDMMT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.clcNgayDuKienXNK = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtMaMienGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDuAnDauTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaDiemXayDungDuAn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMucTieuDuAn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtCapBoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtGP_GCNDauTuSo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayChungNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label11 = new System.Windows.Forms.Label();
            this.clcThoiHanMienThue = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtCamKetSuDung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiCuaNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSDTCuaNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSoDanhMucMienThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel0Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.grHang = new Janus.Windows.GridEX.GridEX();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.grNguoiXNK = new Janus.Windows.GridEX.GridEX();
            this.uiPanel2 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel2Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.grDieuChinh = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            this.uiPanel0Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grNguoiXNK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).BeginInit();
            this.uiPanel2.SuspendLayout();
            this.uiPanel2Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grDieuChinh)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(3, 31);
            this.grbMain.Size = new System.Drawing.Size(693, 385);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdThemHang,
            this.cmdNguoiXNK,
            this.cmdDieuChinh});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.TopRebar1;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdNguoiXNK1,
            this.cmdDieuChinh1,
            this.cmdLuu1,
            this.cmdKhaiBao1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(421, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdNguoiXNK1
            // 
            this.cmdNguoiXNK1.Key = "cmdNguoiXNK";
            this.cmdNguoiXNK1.Name = "cmdNguoiXNK1";
            // 
            // cmdDieuChinh1
            // 
            this.cmdDieuChinh1.Key = "cmdDieuChinh";
            this.cmdDieuChinh1.Name = "cmdDieuChinh1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLuu.Icon")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdNguoiXNK
            // 
            this.cmdNguoiXNK.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdNguoiXNK.Icon")));
            this.cmdNguoiXNK.Key = "cmdNguoiXNK";
            this.cmdNguoiXNK.Name = "cmdNguoiXNK";
            this.cmdNguoiXNK.Text = "Người XNK";
            // 
            // cmdDieuChinh
            // 
            this.cmdDieuChinh.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdDieuChinh.Icon")));
            this.cmdDieuChinh.Key = "cmdDieuChinh";
            this.cmdDieuChinh.Name = "cmdDieuChinh";
            this.cmdDieuChinh.Text = "Điều chỉnh GP";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(699, 28);
            // 
            // cbbPhanLoaiXuatNhapKhau
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Xuất khẩu";
            uiComboBoxItem1.Value = "E";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Nhập khẩu";
            uiComboBoxItem2.Value = "I";
            this.cbbPhanLoaiXuatNhapKhau.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbPhanLoaiXuatNhapKhau.Location = new System.Drawing.Point(137, 11);
            this.cbbPhanLoaiXuatNhapKhau.Name = "cbbPhanLoaiXuatNhapKhau";
            this.cbbPhanLoaiXuatNhapKhau.SelectedIndex = 1;
            this.cbbPhanLoaiXuatNhapKhau.Size = new System.Drawing.Size(84, 21);
            this.cbbPhanLoaiXuatNhapKhau.TabIndex = 0;
            this.cbbPhanLoaiXuatNhapKhau.Text = "Nhập khẩu";
            this.cbbPhanLoaiXuatNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 15);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(128, 13);
            this.label20.TabIndex = 37;
            this.label20.Text = "Phân loại xuất nhập khẩu";
            // 
            // txtCoQuanHaiQuan
            // 
            this.txtCoQuanHaiQuan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCoQuanHaiQuan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCoQuanHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCoQuanHaiQuan.Location = new System.Drawing.Point(396, 10);
            this.txtCoQuanHaiQuan.MaxLength = 12;
            this.txtCoQuanHaiQuan.Name = "txtCoQuanHaiQuan";
            this.txtCoQuanHaiQuan.Size = new System.Drawing.Size(132, 21);
            this.txtCoQuanHaiQuan.TabIndex = 1;
            this.txtCoQuanHaiQuan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCoQuanHaiQuan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(301, 14);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(92, 13);
            this.label27.TabIndex = 39;
            this.label27.Text = "Cơ quan Hải quan";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.clcThoiHanMienThue);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtSoDanhMucMienThue);
            this.uiGroupBox1.Controls.Add(this.txtCoQuanHaiQuan);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.cbbPhanLoaiXuatNhapKhau);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(693, 385);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtPhamViDangKyDMMT);
            this.uiGroupBox5.Controls.Add(this.label8);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Controls.Add(this.clcNgayDuKienXNK);
            this.uiGroupBox5.Controls.Add(this.txtMaMienGiam);
            this.uiGroupBox5.Controls.Add(this.label9);
            this.uiGroupBox5.Location = new System.Drawing.Point(348, 77);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(335, 116);
            this.uiGroupBox5.TabIndex = 2;
            this.uiGroupBox5.Text = "Danh mục miễn thuế";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtPhamViDangKyDMMT
            // 
            this.txtPhamViDangKyDMMT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPhamViDangKyDMMT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPhamViDangKyDMMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhamViDangKyDMMT.Location = new System.Drawing.Point(94, 40);
            this.txtPhamViDangKyDMMT.MaxLength = 100;
            this.txtPhamViDangKyDMMT.Multiline = true;
            this.txtPhamViDangKyDMMT.Name = "txtPhamViDangKyDMMT";
            this.txtPhamViDangKyDMMT.Size = new System.Drawing.Size(235, 40);
            this.txtPhamViDangKyDMMT.TabIndex = 1;
            this.txtPhamViDangKyDMMT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhamViDangKyDMMT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Ngày dự kiến";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "Phạm vi đăng ký";
            // 
            // clcNgayDuKienXNK
            // 
            // 
            // 
            // 
            this.clcNgayDuKienXNK.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayDuKienXNK.DropDownCalendar.Name = "";
            this.clcNgayDuKienXNK.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayDuKienXNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayDuKienXNK.IsNullDate = true;
            this.clcNgayDuKienXNK.Location = new System.Drawing.Point(94, 86);
            this.clcNgayDuKienXNK.Name = "clcNgayDuKienXNK";
            this.clcNgayDuKienXNK.Nullable = true;
            this.clcNgayDuKienXNK.NullButtonText = "Xóa";
            this.clcNgayDuKienXNK.ShowNullButton = true;
            this.clcNgayDuKienXNK.Size = new System.Drawing.Size(86, 21);
            this.clcNgayDuKienXNK.TabIndex = 2;
            this.clcNgayDuKienXNK.TodayButtonText = "Hôm nay";
            this.clcNgayDuKienXNK.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtMaMienGiam
            // 
            this.txtMaMienGiam.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaMienGiam.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaMienGiam.Location = new System.Drawing.Point(94, 12);
            this.txtMaMienGiam.MaxLength = 12;
            this.txtMaMienGiam.Name = "txtMaMienGiam";
            this.txtMaMienGiam.Size = new System.Drawing.Size(235, 21);
            this.txtMaMienGiam.TabIndex = 0;
            this.txtMaMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "Mã miễn thuế";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.txtTenDuAnDauTu);
            this.uiGroupBox4.Controls.Add(this.txtDiaDiemXayDungDuAn);
            this.uiGroupBox4.Controls.Add(this.txtMucTieuDuAn);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Location = new System.Drawing.Point(8, 165);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(335, 150);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Dự án đầu tư";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDuAnDauTu
            // 
            this.txtTenDuAnDauTu.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenDuAnDauTu.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenDuAnDauTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDuAnDauTu.Location = new System.Drawing.Point(66, 11);
            this.txtTenDuAnDauTu.MaxLength = 100;
            this.txtTenDuAnDauTu.Multiline = true;
            this.txtTenDuAnDauTu.Name = "txtTenDuAnDauTu";
            this.txtTenDuAnDauTu.Size = new System.Drawing.Size(263, 40);
            this.txtTenDuAnDauTu.TabIndex = 0;
            this.txtTenDuAnDauTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDuAnDauTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaDiemXayDungDuAn
            // 
            this.txtDiaDiemXayDungDuAn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemXayDungDuAn.Location = new System.Drawing.Point(66, 56);
            this.txtDiaDiemXayDungDuAn.MaxLength = 100;
            this.txtDiaDiemXayDungDuAn.Multiline = true;
            this.txtDiaDiemXayDungDuAn.Name = "txtDiaDiemXayDungDuAn";
            this.txtDiaDiemXayDungDuAn.Size = new System.Drawing.Size(263, 40);
            this.txtDiaDiemXayDungDuAn.TabIndex = 1;
            this.txtDiaDiemXayDungDuAn.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemXayDungDuAn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMucTieuDuAn
            // 
            this.txtMucTieuDuAn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMucTieuDuAn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMucTieuDuAn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMucTieuDuAn.Location = new System.Drawing.Point(66, 102);
            this.txtMucTieuDuAn.MaxLength = 100;
            this.txtMucTieuDuAn.Multiline = true;
            this.txtMucTieuDuAn.Name = "txtMucTieuDuAn";
            this.txtMucTieuDuAn.Size = new System.Drawing.Size(263, 40);
            this.txtMucTieuDuAn.TabIndex = 2;
            this.txtMucTieuDuAn.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMucTieuDuAn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 44;
            this.label1.Text = "Địa điểm XD";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 45;
            this.label3.Text = "Tên dự án ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "Mục tiêu";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.txtCapBoi);
            this.uiGroupBox3.Controls.Add(this.label12);
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.txtGP_GCNDauTuSo);
            this.uiGroupBox3.Controls.Add(this.clcNgayChungNhan);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Location = new System.Drawing.Point(348, 195);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(335, 120);
            this.uiGroupBox3.TabIndex = 3;
            this.uiGroupBox3.Text = "Giấy phép đầu tư";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtCapBoi
            // 
            this.txtCapBoi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCapBoi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCapBoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCapBoi.Location = new System.Drawing.Point(94, 70);
            this.txtCapBoi.MaxLength = 100;
            this.txtCapBoi.Multiline = true;
            this.txtCapBoi.Name = "txtCapBoi";
            this.txtCapBoi.Size = new System.Drawing.Size(235, 40);
            this.txtCapBoi.TabIndex = 2;
            this.txtCapBoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCapBoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(2, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 13);
            this.label12.TabIndex = 45;
            this.label12.Text = "Ngày chứng nhận";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(2, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "Cấp bởi";
            // 
            // txtGP_GCNDauTuSo
            // 
            this.txtGP_GCNDauTuSo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtGP_GCNDauTuSo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtGP_GCNDauTuSo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGP_GCNDauTuSo.Location = new System.Drawing.Point(94, 19);
            this.txtGP_GCNDauTuSo.MaxLength = 12;
            this.txtGP_GCNDauTuSo.Name = "txtGP_GCNDauTuSo";
            this.txtGP_GCNDauTuSo.Size = new System.Drawing.Size(118, 21);
            this.txtGP_GCNDauTuSo.TabIndex = 0;
            this.txtGP_GCNDauTuSo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGP_GCNDauTuSo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayChungNhan
            // 
            // 
            // 
            // 
            this.clcNgayChungNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayChungNhan.DropDownCalendar.Name = "";
            this.clcNgayChungNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayChungNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayChungNhan.IsNullDate = true;
            this.clcNgayChungNhan.Location = new System.Drawing.Point(94, 46);
            this.clcNgayChungNhan.Name = "clcNgayChungNhan";
            this.clcNgayChungNhan.Nullable = true;
            this.clcNgayChungNhan.NullButtonText = "Xóa";
            this.clcNgayChungNhan.ShowNullButton = true;
            this.clcNgayChungNhan.Size = new System.Drawing.Size(86, 21);
            this.clcNgayChungNhan.TabIndex = 1;
            this.clcNgayChungNhan.TodayButtonText = "Hôm nay";
            this.clcNgayChungNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(2, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 13);
            this.label11.TabIndex = 39;
            this.label11.Text = "Giấy phép số";
            // 
            // clcThoiHanMienThue
            // 
            // 
            // 
            // 
            this.clcThoiHanMienThue.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcThoiHanMienThue.DropDownCalendar.Name = "";
            this.clcThoiHanMienThue.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcThoiHanMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcThoiHanMienThue.IsNullDate = true;
            this.clcThoiHanMienThue.Location = new System.Drawing.Point(442, 38);
            this.clcThoiHanMienThue.Name = "clcThoiHanMienThue";
            this.clcThoiHanMienThue.Nullable = true;
            this.clcThoiHanMienThue.NullButtonText = "Xóa";
            this.clcThoiHanMienThue.ShowNullButton = true;
            this.clcThoiHanMienThue.Size = new System.Drawing.Size(86, 21);
            this.clcThoiHanMienThue.TabIndex = 3;
            this.clcThoiHanMienThue.TodayButtonText = "Hôm nay";
            this.clcThoiHanMienThue.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.txtCamKetSuDung);
            this.uiGroupBox7.Location = new System.Drawing.Point(348, 315);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(335, 64);
            this.uiGroupBox7.TabIndex = 4;
            this.uiGroupBox7.Text = "Cam kết sử dụng đúng mục đích";
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // txtCamKetSuDung
            // 
            this.txtCamKetSuDung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCamKetSuDung.Location = new System.Drawing.Point(10, 16);
            this.txtCamKetSuDung.MaxLength = 100;
            this.txtCamKetSuDung.Multiline = true;
            this.txtCamKetSuDung.Name = "txtCamKetSuDung";
            this.txtCamKetSuDung.Size = new System.Drawing.Size(319, 40);
            this.txtCamKetSuDung.TabIndex = 0;
            this.txtCamKetSuDung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCamKetSuDung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.txtGhiChu);
            this.uiGroupBox6.Location = new System.Drawing.Point(8, 315);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(335, 64);
            this.uiGroupBox6.TabIndex = 5;
            this.uiGroupBox6.Text = "Ghi chú (dành cho người khai)";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(9, 16);
            this.txtGhiChu.MaxLength = 100;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(320, 40);
            this.txtGhiChu.TabIndex = 0;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.txtDiaChiCuaNguoiKhai);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.txtSDTCuaNguoiKhai);
            this.uiGroupBox2.Location = new System.Drawing.Point(8, 77);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(335, 86);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Người khai";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiCuaNguoiKhai
            // 
            this.txtDiaChiCuaNguoiKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiCuaNguoiKhai.Location = new System.Drawing.Point(72, 13);
            this.txtDiaChiCuaNguoiKhai.MaxLength = 100;
            this.txtDiaChiCuaNguoiKhai.Multiline = true;
            this.txtDiaChiCuaNguoiKhai.Name = "txtDiaChiCuaNguoiKhai";
            this.txtDiaChiCuaNguoiKhai.Size = new System.Drawing.Size(257, 40);
            this.txtDiaChiCuaNguoiKhai.TabIndex = 0;
            this.txtDiaChiCuaNguoiKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiCuaNguoiKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiCuaNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Điện thoại";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Địa chỉ";
            // 
            // txtSDTCuaNguoiKhai
            // 
            this.txtSDTCuaNguoiKhai.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSDTCuaNguoiKhai.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSDTCuaNguoiKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSDTCuaNguoiKhai.Location = new System.Drawing.Point(72, 58);
            this.txtSDTCuaNguoiKhai.MaxLength = 12;
            this.txtSDTCuaNguoiKhai.Name = "txtSDTCuaNguoiKhai";
            this.txtSDTCuaNguoiKhai.Size = new System.Drawing.Size(132, 21);
            this.txtSDTCuaNguoiKhai.TabIndex = 1;
            this.txtSDTCuaNguoiKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSDTCuaNguoiKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(303, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Thời hạn miễn thuế";
            // 
            // txtSoDanhMucMienThue
            // 
            this.txtSoDanhMucMienThue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDanhMucMienThue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDanhMucMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDanhMucMienThue.Location = new System.Drawing.Point(137, 37);
            this.txtSoDanhMucMienThue.MaxLength = 12;
            this.txtSoDanhMucMienThue.Name = "txtSoDanhMucMienThue";
            this.txtSoDanhMucMienThue.Size = new System.Drawing.Size(132, 21);
            this.txtSoDanhMucMienThue.TabIndex = 2;
            this.txtSoDanhMucMienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDanhMucMienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(7, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(124, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "Số doanh mục miễn thuế";
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanelManager1.Tag = null;
            this.uiPanel0.Id = new System.Guid("6f104850-2bf5-47e2-80ae-53ce535b1e55");
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            this.uiPanel1.Id = new System.Guid("59ca139e-72e9-413f-8452-0b04180a7dd4");
            this.uiPanelManager1.Panels.Add(this.uiPanel1);
            this.uiPanel2.Id = new System.Guid("9f098dc4-19a4-4cbd-beb3-698c76237bcd");
            this.uiPanelManager1.Panels.Add(this.uiPanel2);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("6f104850-2bf5-47e2-80ae-53ce535b1e55"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(200, 136), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("59ca139e-72e9-413f-8452-0b04180a7dd4"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(200, 146), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("9f098dc4-19a4-4cbd-beb3-698c76237bcd"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(200, 133), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("6f104850-2bf5-47e2-80ae-53ce535b1e55"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("59ca139e-72e9-413f-8452-0b04180a7dd4"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("9f098dc4-19a4-4cbd-beb3-698c76237bcd"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.AutoHide = true;
            this.uiPanel0.InnerContainer = this.uiPanel0Container;
            this.uiPanel0.Location = new System.Drawing.Point(0, 295);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(200, 136);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Thông tin hàng";
            // 
            // uiPanel0Container
            // 
            this.uiPanel0Container.Controls.Add(this.grHang);
            this.uiPanel0Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel0Container.Name = "uiPanel0Container";
            this.uiPanel0Container.Size = new System.Drawing.Size(198, 108);
            this.uiPanel0Container.TabIndex = 0;
            // 
            // grHang
            // 
            this.grHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grHang_DesignTimeLayout.LayoutString = resources.GetString("grHang_DesignTimeLayout.LayoutString");
            this.grHang.DesignTimeLayout = grHang_DesignTimeLayout;
            this.grHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grHang.GroupByBoxVisible = false;
            this.grHang.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grHang.Location = new System.Drawing.Point(0, 0);
            this.grHang.Name = "grHang";
            this.grHang.RecordNavigator = true;
            this.grHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grHang.Size = new System.Drawing.Size(198, 108);
            this.grHang.TabIndex = 2;
            this.grHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grHang.VisualStyleManager = this.vsmMain;
            // 
            // uiPanel1
            // 
            this.uiPanel1.AutoHide = true;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 149);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(200, 146);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Danh sách người XNK";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.grNguoiXNK);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(198, 118);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // grNguoiXNK
            // 
            this.grNguoiXNK.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grNguoiXNK.ColumnAutoResize = true;
            grNguoiXNK_DesignTimeLayout.LayoutString = resources.GetString("grNguoiXNK_DesignTimeLayout.LayoutString");
            this.grNguoiXNK.DesignTimeLayout = grNguoiXNK_DesignTimeLayout;
            this.grNguoiXNK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grNguoiXNK.GroupByBoxVisible = false;
            this.grNguoiXNK.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grNguoiXNK.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grNguoiXNK.Location = new System.Drawing.Point(0, 0);
            this.grNguoiXNK.Name = "grNguoiXNK";
            this.grNguoiXNK.RecordNavigator = true;
            this.grNguoiXNK.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grNguoiXNK.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grNguoiXNK.Size = new System.Drawing.Size(198, 118);
            this.grNguoiXNK.TabIndex = 2;
            this.grNguoiXNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grNguoiXNK.VisualStyleManager = this.vsmMain;
            // 
            // uiPanel2
            // 
            this.uiPanel2.AutoHide = true;
            this.uiPanel2.InnerContainer = this.uiPanel2Container;
            this.uiPanel2.Location = new System.Drawing.Point(0, 165);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(200, 133);
            this.uiPanel2.TabIndex = 4;
            this.uiPanel2.Text = "Điều chỉnh GP";
            // 
            // uiPanel2Container
            // 
            this.uiPanel2Container.Controls.Add(this.grDieuChinh);
            this.uiPanel2Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel2Container.Name = "uiPanel2Container";
            this.uiPanel2Container.Size = new System.Drawing.Size(198, 105);
            this.uiPanel2Container.TabIndex = 0;
            // 
            // grDieuChinh
            // 
            this.grDieuChinh.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grDieuChinh_DesignTimeLayout.LayoutString = resources.GetString("grDieuChinh_DesignTimeLayout.LayoutString");
            this.grDieuChinh.DesignTimeLayout = grDieuChinh_DesignTimeLayout;
            this.grDieuChinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grDieuChinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grDieuChinh.GroupByBoxVisible = false;
            this.grDieuChinh.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grDieuChinh.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grDieuChinh.Location = new System.Drawing.Point(0, 0);
            this.grDieuChinh.Name = "grDieuChinh";
            this.grDieuChinh.RecordNavigator = true;
            this.grDieuChinh.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grDieuChinh.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grDieuChinh.Size = new System.Drawing.Size(198, 105);
            this.grDieuChinh.TabIndex = 1;
            this.grDieuChinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grDieuChinh.VisualStyleManager = this.vsmMain;
            // 
            // VNACC_TEAForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 437);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_TEAForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đăng ký hàng miễn thuế";
            this.Load += new System.EventHandler(this.VNACC_TEAForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            this.uiPanel0Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grNguoiXNK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).EndInit();
            this.uiPanel2.ResumeLayout(false);
            this.uiPanel2Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grDieuChinh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNguoiXNK;
        private Janus.Windows.UI.CommandBars.UICommand cmdNguoiXNK1;
        private Janus.Windows.EditControls.UIComboBox cbbPhanLoaiXuatNhapKhau;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtCoQuanHaiQuan;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayDuKienXNK;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhamViDangKyDMMT;
        private Janus.Windows.GridEX.EditControls.EditBox txtMucTieuDuAn;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDuAnDauTu;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemXayDungDuAn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiCuaNguoiKhai;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtSDTCuaNguoiKhai;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMienGiam;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtCapBoi;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtGP_GCNDauTuSo;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayChungNhan;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.CalendarCombo.CalendarCombo clcThoiHanMienThue;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.EditControls.EditBox txtCamKetSuDung;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDanhMucMienThue;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.UI.Dock.UIPanel uiPanel0;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel0Container;
        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.UI.Dock.UIPanel uiPanel1;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel1Container;
        private Janus.Windows.UI.CommandBars.UICommand cmdDieuChinh1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDieuChinh;
        private Janus.Windows.GridEX.GridEX grHang;
        private Janus.Windows.GridEX.GridEX grNguoiXNK;
        private Janus.Windows.UI.Dock.UIPanel uiPanel2;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel2Container;
        private Janus.Windows.GridEX.GridEX grDieuChinh;
    }
}