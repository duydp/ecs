﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using System;
//
using System.Collections;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Tab;
using System.Globalization;
using Janus.Windows.ExplorerBar;
using System.Collections.Generic;
using System.Threading;
using System.Resources;
using Janus.Windows.UI.Dock;

using Janus.Windows.FilterEditor;
using Janus.Windows.EditControls;
using Janus.Windows.UI.StatusBar;
using Janus.Windows.GridEX;
using PDFSignedDigital;
namespace PDFSignedDigital
{
    public partial class BaseForm : Form
    {
        public string MaDoanhNghiep;
        public string MaHaiQuan;
        public string CalledForm = string.Empty;

        protected MessageBoxControl _MsgBox;
        private IContainer components = null;
        public string language = String.Empty;
        public ResourceManager resource = null;

        #region OverLoad function ShowMsg

        #region Format String Message with Args
        private string formatMsg(string Key, string[] args)
        {
            string str = "";
            try
            {
                str = resource.GetString(Key);
            }
            catch (Exception ex)
            {
                //Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            if (str == "") str = Key;
            else if (args != null)
            {
                if (str.IndexOf("}") > -1 && args.Length > 0)
                {
                    try
                    {
                        str = string.Format(str, args);
                    }
                    catch
                    {
                        str = Key;
                    }
                    #region
                    //for (int i = 0; i < args.Length; i++)
                    //{

                    //    //Get String with 1 arg
                    //    int end = str.IndexOf("}") + 1;

                    //    //Change arg to {0}
                    //    s += str.Substring(0, end);
                    //    s = s.Substring(0, s.IndexOf("{")) + "{0}";

                    //    //str last
                    //    str = str.Substring(end, str.Length - end);

                    //    //format String return
                    //    try
                    //    {
                    //        s = string.Format(s, args[i]);
                    //    }
                    //    catch
                    //    {
                    //        return Key;
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        str += " " + args[i];
                    }
                }
            }
            #region
            //str = string.Format(str, Environment.NewLine);
            //s += str;
            //return str;
            #endregion
            return str.Replace(@"\n", "\n");
        }
        #endregion

        #endregion

        public BaseForm()
        {
            InitializeComponent();
        }

        #region setlanguage

        // Get all controls in form.
        public List<Control> GetAllControls(IList controls)
        {
            List<Control> allControls = new List<Control>();
            foreach (Control control in controls)
            {
                allControls.Add(control);
                List<Control> subControls = GetAllControls(control.Controls);
                allControls.AddRange(subControls);
            }

            return allControls;
        }
        public List<Component> GetAllComponent()
        {
            List<Component> allComponent = new List<Component>();
            int i = this.components.Components.Count;
            foreach (Component component in this.components.Components)
            {
                allComponent.Add(component);
            }
            //int j = this.
            return allComponent;
        }
        public void forGrid(Control c, ResourceManager rm, string formName)
        {
            GridEX grid = (GridEX)c;
            GridEXColumnCollection columnColl = grid.RootTable.Columns;
            string columnKey = "";
            string key = String.Empty;
            foreach (GridEXColumn column in columnColl)
            {
                key = columnKey = "";
                columnKey = column.Key;
                key = formName + "_" + c.Name + "_" + columnKey;
                try
                {
                    if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                    {
                        column.Caption = rm.GetString(key);
                    }
                }
                catch
                { }
            }
            if (grid.RootTable.Groups != null)
            {
                foreach (GridEXGroup group in grid.RootTable.Groups)
                {

                    key = "";
                    key = formName + "_" + grid.Name + "_group" + group.Index.ToString();
                    try
                    {
                        if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                        {
                            group.HeaderCaption = rm.GetString(key);
                        }
                    }
                    catch { }
                }
            }
            if (grid.RootTable.GroupHeaderTotals != null)
            {
                foreach (GridEXGroupHeaderTotal groupHeaderTotal in grid.RootTable.GroupHeaderTotals)
                {

                    key = "";
                    key = formName + "_" + grid.Name + "_" + groupHeaderTotal.Key;
                    try
                    {
                        if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                        {
                            groupHeaderTotal.TotalSuffix = rm.GetString(key);
                        }
                    }
                    catch
                    { }
                }
            }
            if (grid.ContextMenuStrip != null)
            {
                forToolStripMenuItem(grid.ContextMenuStrip.Items, rm, formName, grid.ContextMenuStrip.Name);
            }
        }
        public void forToolStripMenuItem(ToolStripItemCollection itemColl, ResourceManager rm, string formName, string contextMenuName)
        {
            foreach (ToolStripItem menuItem in itemColl)
            {
                string key = "";
                key = formName + "_" + contextMenuName + "_" + menuItem.Name;
                try
                {
                    if (rm.GetString(key) != null && rm.GetString(key).Trim().Length >= 1)
                    {
                        menuItem.Text = rm.GetString(key);

                    }

                    try
                    {
                        if (menuItem.GetType().Name == "ToolStripMenuItem")
                        {
                            ToolStripMenuItem mni = (ToolStripMenuItem)menuItem;
                            if (mni.DropDownItems != null && mni.DropDownItems.Count > 0) forToolStripMenuItem(mni.DropDownItems, rm, formName, contextMenuName);
                        }
                    }
                    catch { }
                }
                catch (Exception ex) { 
                    //Logger.LocalLogger.Instance().WriteMessage(ex); 
                }
            }
        }
        public List<UIPanelBase> getUIPanelChild(Janus.Windows.UI.Dock.UIPanelCollection panelColl, UIPanelGroup panelGroup)
        {
            List<UIPanelBase> allPanels = new List<UIPanelBase>();
            List<UIPanelBase> allPanels2 = new List<UIPanelBase>();
            if (panelColl != null)//coll
            {
                foreach (UIPanelGroup panelG in panelColl)
                {
                    string n = panelG.Name;
                    allPanels.Add(panelG);
                    getUIPanelChild(null, panelG);
                }
            }
            if (panelGroup != null)//group
            {
                if (panelGroup.Panels.Count > 0)
                {
                    for (int i = 0; i < (int)panelGroup.Panels.Count; i++)
                    {
                        if (panelGroup.Panels[i].GetType() == typeof(UIPanelGroup))
                        {
                            UIPanelGroup tmppanelGroup = (UIPanelGroup)panelGroup.Panels[i];
                            string n = tmppanelGroup.Name;
                            allPanels.Add(tmppanelGroup);
                            allPanels2 = this.getUIPanelChild(null, tmppanelGroup);
                            allPanels.AddRange(allPanels2);
                        }
                        else
                        {
                            string panelName = panelGroup.Panels[i].Name;
                            allPanels.Add(panelGroup.Panels[i]);
                        }
                    }
                }
            }
            return allPanels;
        }
        public List<UIPanelBase> getUIPanelChild2(Janus.Windows.UI.Dock.UIPanelCollection panelC, UIPanelGroup panelG)
        {
            List<UIPanelBase> allPanels = new List<UIPanelBase>();
            List<UIPanelBase> allPanels2 = new List<UIPanelBase>();
            UIPanelGroup panelGroup;
            if (panelC != null)//coll
            {
                panelGroup = (UIPanelGroup)panelC[0];
                allPanels.Add(panelGroup);
            }
            else
            {
                panelGroup = panelG;
            }
            string na = panelGroup.Name;
            if (panelGroup.Panels.Count > 0)
            {
                for (int i = 0; i < (int)panelGroup.Panels.Count; i++)
                {
                    if (panelGroup.Panels[i].GetType() == typeof(UIPanelGroup))
                    {
                        UIPanelGroup tmppanelGroup = (UIPanelGroup)panelGroup.Panels[i];
                        string n = tmppanelGroup.Name;
                        allPanels.Add(tmppanelGroup);
                        allPanels2 = this.getUIPanelChild(null, tmppanelGroup);
                        allPanels.AddRange(allPanels2);
                    }
                    else
                    {
                        string panelName = panelGroup.Panels[i].Name;
                        allPanels.Add(panelGroup.Panels[i]);
                    }
                }
            }
            return allPanels;
        }
        public void forPanel(Component c, ResourceManager rm, string formName)
        {
            if (c.GetType().ToString().Contains("PanelManager"))
            {
                Janus.Windows.UI.Dock.UIPanelManager p = (Janus.Windows.UI.Dock.UIPanelManager)c;
                Janus.Windows.UI.Dock.UIPanelCollection panelColl = p.Panels;
                List<UIPanelBase> panels = getUIPanelChild2(panelColl, null);
                foreach (Janus.Windows.UI.Dock.UIPanelBase panel in panels)
                {
                    string panelName = panel.Name;
                    string panelText = panel.Text;
                    string key = formName + "_" + panelName;
                    try
                    {
                        if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                        {
                            panel.Text = rm.GetString(key);
                        }
                    }
                    catch { }
                }

            }
            else
            {

            }

        }// no need
        public void forExplore(Component c, ResourceManager rm, string formName)
        {

            ExplorerBar e = (ExplorerBar)c;
            string eName = e.Name;
            foreach (ExplorerBarGroup eGroup in e.Groups)
            {
                string groupKey = eGroup.Key;
                // allExploreGroup.Add(eGroup);     
                try
                {
                    if (rm.GetString(formName + "_" + groupKey) != null && rm.GetString(formName + "_" + groupKey).Trim() != "")
                    {
                        eGroup.Text = rm.GetString(formName + "_" + groupKey);
                    }
                }
                catch { }
                foreach (ExplorerBarItem eItem in eGroup.Items)
                {
                    string itemKey = eItem.Key;
                    //  allExploreItem.Add(eItem);
                    string key = formName + "_" + groupKey + "_" + itemKey;
                    try
                    {
                        if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                        {
                            eItem.Text = rm.GetString(key);
                        }
                    }
                    catch
                    {

                    }
                }


            }
        }
        public List<UICommand> forCommandBar(Control c, UICommand cm, ResourceManager rm, string formName)
        {
            List<UICommand> allCommand = new List<UICommand>();
            List<UICommand> suballCommand = new List<UICommand>();
            UICommandBar cmdBar = (UICommandBar)c;
            if (c != null)
            {
                //contextMenu
                UICommandManager cmdMangager = cmdBar.CommandManager;
                UIContextMenuCollection contextMenuColl = cmdMangager.ContextMenus;
                if (contextMenuColl != null)
                {
                    for (int i = 0; i < cmdMangager.ContextMenus.Count; i++)
                    {
                        UICommandCollection cmdColl = cmdMangager.ContextMenus[i].Commands;
                        if (cmdColl != null && cmdColl.Count > 0)
                        {
                            foreach (UICommand cmd in cmdColl)
                            {
                                try
                                {
                                    string key = formName + "_" + cmd.Key;
                                    if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                                    {
                                        cmd.Text = rm.GetString(key);

                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
                //if (cmdMangager.Commands != null)
                //{
                //    foreach (UICommand cmd in cmdMangager.Commands)
                //    {
                //        string key = formName + "_" + cmd.Key;
                //        if (rm.GetString(key) != null && rm.GetString(key).Trim() != "")
                //        {
                //            cmd.Text = rm.GetString(key);

                //        }
                //    }

                //}
                //Command
                string cmdBarKey = cmdBar.Name;
                foreach (UICommand cmd in cmdBar.Commands)
                {
                    string cmdKey = cmd.Key;
                    try
                    {
                        if (rm.GetString(formName + "_" + cmdKey) != null && rm.GetString(formName + "_" + cmdKey).Trim() != "")
                        {
                            cmd.Text = rm.GetString(formName + "_" + cmdKey);
                        }
                        if (cmd.Commands.Count > 0)
                        {
                            suballCommand = forCommandBar(null, cmd, rm, formName);
                            allCommand.AddRange(suballCommand);
                        }
                        else
                        {
                            allCommand.Add(cmd);
                        }
                    }
                    catch { }
                }
            }
            if (cm != null)
            {
                string cmdKey = cm.Key;
                allCommand.Add(cm);
                foreach (UICommand cmd in cm.Commands)
                {
                    string Key = cmd.Key;
                    //cmd.Text;
                    try
                    {
                        if (rm.GetString(formName + "_" + Key) != null && rm.GetString(formName + "_" + Key) != "")
                        {
                            cmd.Text = rm.GetString(formName + "_" + Key);
                        }
                        if (cmd.Commands.Count > 0)
                        {
                            suballCommand = forCommandBar(null, cmd, rm, formName);
                            allCommand.AddRange(suballCommand);
                        }
                        else
                        {
                            allCommand.Add(cmd);
                        }
                    }
                    catch
                    { }
                }
            }
            return allCommand;

        }
        public void forChildControlInpanel(UIPanelBase panel, ResourceManager rm, string formname)
        {
            if (panel.GetType() == typeof(UIPanelGroup))
            {
                UIPanelGroup panelG = (UIPanelGroup)panel;
                foreach (UIPanelBase pBase in panelG.Panels)
                {
                    forChildControlInpanel(pBase, rm, formname);
                }
            }
            else//UIPanel
            {
                string key = String.Empty;
                Control tmpCtr = panel.GetNextControl(new Control(), true);
                if (tmpCtr.GetType() == typeof(UIPanelInnerContainer))
                {
                    UIPanelInnerContainer panelContainer = (UIPanelInnerContainer)tmpCtr;
                    if (panelContainer.Controls != null)
                    {
                        foreach (Control control in panelContainer.Controls)
                        {
                            if (control.GetType() == typeof(GridEX))
                            {
                                this.forGrid(control, rm, formname);
                            }
                            else
                            {
                                key = String.Empty;
                                key = formname + "_" + panel.Name;
                                try
                                {
                                    if (rm.GetString(key) != null && rm.GetString(key) != "")
                                    {
                                        control.Text = rm.GetString(key);
                                    }
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
                else//UiPanel 
                {
                    forChildControlInpanel((UIPanel)tmpCtr, rm, formname);
                }
            }

        }
        #endregion

    }
}