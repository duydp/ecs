using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.BLL.SXXK.ThanhKhoan
{
    public partial class NPLNhapTon : EntityBase
    {
        #region Private members.

        protected int _SoToKhai;
        protected string _MaLoaiHinh = String.Empty;
        protected short _NamDangKy;
        protected string _MaHaiQuan = String.Empty;
        protected string _MaNPL = String.Empty;
        protected string _TenNPL = String.Empty;
        protected string _TenDVT = String.Empty;
        protected string _MaDoanhNghiep = String.Empty;
        protected decimal _Luong;
        protected decimal _Ton;
        protected double _ThueXNK;
        protected double _ThueTTDB;
        protected double _ThueVAT;
        protected double _PhuThu;
        protected double _ThueCLGia;
        protected double _ThueXNKTon;


        //Add Extra
        protected double _ThueTon;
        protected decimal _TotalNPL;
        protected string  _MaSP;
        protected decimal _DinhMucSuDung;
        protected decimal _TyLeHaoHut;
        protected decimal _DinhMucChung;
        protected string _MaNguyenPhuLieu;
        protected DateTime _NgayDangKy;



        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public short NamDangKy
        {
            set { this._NamDangKy = value; }
            get { return this._NamDangKy; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
        public string TenNPL
        {
            set { this._TenNPL = value; }
            get { return this._TenNPL; }
        }
        public string TenDVT
        {
            set { this._TenDVT = value; }
            get { return this._TenDVT; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public decimal Luong
        {
            set { this._Luong = value; }
            get { return this._Luong; }
        }
        public decimal Ton
        {
            set { this._Ton = value; }
            get { return this._Ton; }
        }
        public double ThueXNK
        {
            set { this._ThueXNK = value; }
            get { return this._ThueXNK; }
        }
        public double ThueTTDB
        {
            set { this._ThueTTDB = value; }
            get { return this._ThueTTDB; }
        }
        public double ThueVAT
        {
            set { this._ThueVAT = value; }
            get { return this._ThueVAT; }
        }
        public double PhuThu
        {
            set { this._PhuThu = value; }
            get { return this._PhuThu; }
        }
        public double ThueCLGia
        {
            set { this._ThueCLGia = value; }
            get { return this._ThueCLGia; }
        }
        public double ThueXNKTon
        {
            set { this._ThueXNKTon = value; }
            get { return this._ThueXNKTon; }
        }
        //add extra
        public double ThueTon
        {
            set { this._ThueTon = value; }
            get { return this._ThueTon; }
        }

        public decimal TotalNPL
        {
            set { this._TotalNPL  = value; }
            get { return this._TotalNPL; }
        }

        //add dinh muc
        public decimal DinhMucSuDung
        {
            set { this._DinhMucSuDung  = value; }
            get { return this._DinhMucSuDung; }
        }

        public decimal DinhMucChung
        {
            set { this._DinhMucChung  = value; }
            get { return this._DinhMucChung; }
        }
        public decimal TyLeHaoHut
        {
            set { this._TyLeHaoHut = value; }
            get { return this._TyLeHaoHut; }
        }

        public string MaNguyenPhuLieu
        {
            set { this._MaNguyenPhuLieu = value; }
            get { return this._MaNguyenPhuLieu; }
        }

        public string MaSP
        {
            set { this._MaSP  = value; }
            get { return this._MaSP; }
        }
        public DateTime NgayDangKy
        {
            set { this._NgayDangKy = value; }
            get { return this._NgayDangKy; }
        }
        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) this._Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) this._Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this._ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) this._ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this._PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueCLGia"))) this._ThueCLGia = reader.GetDouble(reader.GetOrdinal("ThueCLGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) this._ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool Load(SqlTransaction trans)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);

            IDataReader reader = this.db.ExecuteReader(dbCommand,trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) this._Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) this._Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this._ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) this._ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this._PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) this._ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public NPLNhapTonCollection SelectCollectionBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            NPLNhapTonCollection collection = new NPLNhapTonCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLNhapTon entity = new NPLNhapTon();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy()
        {
            string sql = "SELECT * FROM t_SXXK_ThanhLy_NPLNhapTon WHERE SoToKhai = @SoToKhai AND MaHaiQuan = @MaHaiQuan AND MaLoaiHinh =@MaLoaiHinh AND NamDangKy=@NamDangKy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public NPLNhapTonCollection SelectCollectionAll()
        {
            NPLNhapTonCollection collection = new NPLNhapTonCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                NPLNhapTon entity = new NPLNhapTon();

                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueCLGia"))) entity.ThueCLGia = reader.GetDouble(reader.GetOrdinal("ThueCLGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public NPLNhapTonCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            NPLNhapTonCollection collection = new NPLNhapTonCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            ToKhai.ToKhaiMauDich TKMD = new ToKhai.ToKhaiMauDich();
            BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
            while (reader.Read())
            {
                NPLNhapTon entity = new NPLNhapTon();
                Company.BLL.SXXK.ToKhai.HangMauDich HMD = new Company.BLL.SXXK.ToKhai.HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai")))HMD.SoToKhai= TKMD.SoToKhai = entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh")))HMD.MaLoaiHinh= TKMD.MaLoaiHinh = entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) HMD.NamDangKy=TKMD.NamDangKy = entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan")))HMD.MaHaiQuan= TKMD.MaHaiQuan = entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL")))HMD.MaPhu= entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueCLGia"))) entity.ThueCLGia = reader.GetDouble(reader.GetOrdinal("ThueCLGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                TKMD.Load();
                npl.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                npl.MaHaiQuan = TKMD.MaHaiQuan;
                npl.Ma = entity.MaNPL;
                npl.Ten = "";
                npl.Load();
                entity.TenNPL = npl.Ten;
                entity.TenDVT = Company.BLL.DuLieuChuan.DonViTinh.GetName(npl.DVT_ID);
                entity.NgayDangKy = TKMD.NgayDangKy;
                HMD.LoadByMaHang();
                entity.NguyenTe_ID = TKMD.NguyenTe_ID;
                entity.NuocXX_ID = HMD.NuocXX_ID;
                entity.TriGiaKB = HMD.TriGiaKB;
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, this._Ton);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, this._ThueVAT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@ThueCLGia", SqlDbType.Float, this._ThueCLGia);
            this.db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, this._ThueXNKTon);
            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(NPLNhapTonCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLNhapTon item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, NPLNhapTonCollection collection)
        {
            foreach (NPLNhapTon item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, this._Ton);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, this._ThueVAT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@ThueCLGia", SqlDbType.Float, this._ThueCLGia);
            this.db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, Math.Round((double)this._Ton * this._ThueXNK / (double)this._Luong, 0));
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        public int InsertUpdateTransaction(SqlTransaction transaction, bool TuDongTinhThue)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, this._Ton);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, this._ThueVAT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@ThueCLGia", SqlDbType.Float, this._ThueCLGia);
            if(TuDongTinhThue)
                this.db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, Math.Round((double)this._Ton * this._ThueXNK / (double)this._Luong, 0));
            else
                this.db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, this._ThueXNKTon);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(NPLNhapTonCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLNhapTon item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate(NPLNhapTonCollection collection, bool TuDongTinhThue)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLNhapTon item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction,TuDongTinhThue) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            this.db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, this._Ton);
            this.db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, this._ThueXNK);
            this.db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, this._ThueTTDB);
            this.db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, this._ThueVAT);
            this.db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, this._PhuThu);
            this.db.AddInParameter(dbCommand, "@ThueCLGia", SqlDbType.Float, this._ThueCLGia);
            this.db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, this._ThueXNKTon);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(NPLNhapTonCollection collection, SqlTransaction transaction)
        {
            foreach (NPLNhapTon item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(NPLNhapTonCollection collection, SqlTransaction transaction)
        {
            foreach (NPLNhapTon item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(NPLNhapTonCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLNhapTon item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion

        public static void Updatebase(DataSet ds, SqlTransaction transaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NPLNhapTon nplNhapTon = new NPLNhapTon();
                nplNhapTon.MaDoanhNghiep = row["MA_DV"].ToString();
                nplNhapTon.MaHaiQuan = row["MA_HQ"].ToString();
                nplNhapTon.MaLoaiHinh = row["MA_LH"].ToString();
                nplNhapTon.MaNPL = row["MA_NPL"].ToString();
                nplNhapTon.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                try
                {
                    if (row["PHU_THU"].ToString() != "")
                        nplNhapTon.PhuThu = Convert.ToDouble(row["PHU_THU"]);
                }
                catch { }
                nplNhapTon.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                try
                {
                    if (row["TRIGIA_THUKHAC"].ToString() != "")
                        nplNhapTon.ThueCLGia = Convert.ToDouble(row["TRIGIA_THUKHAC"]);
                }
                catch { }
                try
                {
                    if (row["THUE_TTDB"].ToString() != "")
                        nplNhapTon.ThueTTDB = Convert.ToDouble(row["THUE_TTDB"]);
                }
                catch { }
                try
                {
                    if (row["THUE_VAT"].ToString() != "")
                        nplNhapTon.ThueVAT = Convert.ToDouble(row["THUE_VAT"]);
                }
                catch { }
                try
                {
                    if (row["THUE_XNK"].ToString() != "")
                        nplNhapTon.ThueXNK = Convert.ToDouble(row["THUE_XNK"]);
                }
                catch { }
                nplNhapTon.Ton = Convert.ToDecimal(row["Ton"]);
                nplNhapTon.Luong = Convert.ToDecimal(row["Luong"]);
                nplNhapTon.ThueXNKTon = Math.Round(Convert.ToDouble(nplNhapTon.Ton) * nplNhapTon.ThueXNK / Convert.ToDouble(nplNhapTon.Luong), MidpointRounding.AwayFromZero);
                nplNhapTon.InsertUpdateTransaction(transaction);
            }
        }

    }
}