﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class ContractItem
    {
        /// <summary>
        /// Đơn giá n..16,2
        /// </summary>
        [XmlElement("unitPrice")]
        public string unitPrice { get; set; }
        /// <summary>
        /// Trị Giá n..16,2
        /// </summary>
        [XmlElement("statisticalValue")]
        public string statisticalValue { get; set; }
        /// <summary>
        /// Chi tiết hàng ( thuế)
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
        /// <summary>
        /// Xuất xứ
        /// </summary>
        [XmlElement("Origin")]
        public Origin Origin { get; set; }
        /// <summary>
        /// Chi tiết hàng
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }
        /// <summary>
        /// .....
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }


    }
}
