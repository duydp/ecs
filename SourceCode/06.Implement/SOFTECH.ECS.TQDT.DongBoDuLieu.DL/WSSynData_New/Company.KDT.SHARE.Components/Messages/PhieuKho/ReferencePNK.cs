﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.PhieuKho
{
    [XmlRoot("Reference")]
    public class ReferencePNK
    {

        /// <summary>
        /// Mã DN
        /// </summary>
        [XmlElement("indentity")]
        public string Indentity { get; set; }
        /// <summary> 
        /// Mã HQ quản lý
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }
        /// <summary>
        /// Mã Kho
        /// </summary>
        [XmlElement("warehouse")]
        public string Warehouse { get; set; }
        /// <summary>
        /// DS TK
        /// </summary>
        [XmlElement("DeclarationList")]
        //public DeclarationList DeclarationList { get; set; }
        //Nhiều tờ khai
        public List<DeclarationList> DeclarationList { get; set; }
    }
}
