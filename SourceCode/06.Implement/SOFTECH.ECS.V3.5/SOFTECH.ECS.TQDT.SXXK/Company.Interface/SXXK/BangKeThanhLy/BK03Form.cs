using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.Interface.SXXK.BangKeThanhLy;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK03Form : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKNPLChuaThanhLyCollection bkCollection = new BKNPLChuaThanhLyCollection();
        private int SoToKhai;
        private decimal SoToKhaiVNACCS;
        private string MaLoaiHinh;
        private DateTime NgayDangKy;
        private string TenNPL;
        private string DVT_ID;
        private decimal SoLuong;
        private int Postion;
        private int STTHang;
        public BK03Form()
        {
            InitializeComponent();
        }

        private void BK03Form_Load(object sender, EventArgs e)
        {
            txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            dgList2.Tables[0].Columns["Luong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList1.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            if (this.HSTL.TrangThaiThanhKhoan == 401)
            {
                btnAdd.Enabled = cmdSave.Enabled = false;
                dgList2.AllowDelete = InheritableBoolean.False;
            }
            dgList1.DataSource = new BKToKhaiNhap().getNPL_BKToKhaiNhap(this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].ID).Tables[0];
            dgList2.DataSource = this.bkCollection;
        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhaiVNACCS"].Text = e.Row.Cells["SoToKhaiVNACCS"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            }
        }

        private void dgList1_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (this.HSTL.TrangThaiThanhKhoan == 401) return;
            if (e.Row.RowType == RowType.Record)
            {
                this.SoToKhaiVNACCS = Convert.ToDecimal(e.Row.Cells["SoToKhaiVNACCS"].Value);
                this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.TenNPL = e.Row.Cells["TenHang"].Text;
                this.DVT_ID = Convert.ToString(e.Row.Cells["DVT_ID"].Value);
                this.SoLuong = Convert.ToDecimal(e.Row.Cells["Ton"].Value);
                this.MaHaiQuan = Convert.ToString(e.Row.Cells["MaHaiQuan"].Value);
               // txtSoToKhai.Text = e.Row.Cells["SoToKhaiVNACCS"].Text;
                txtSoToKhai.Text = e.Row.Cells["SoToKhaiVNACCS"].Text;
                txtMaNPL.Text = e.Row.Cells["Ma"].Text;
                txtLuong.Value = this.SoLuong;
                txtTenNPL.Text = e.Row.Cells["TenHang"].Text;
                txtDonViTinh.Text = e.Row.Cells["DVT_ID"].Text;
                this.STTHang = Convert.ToInt32(e.Row.Cells["SoThuTuHang"].Value);
                btnSave.Enabled = false;
                btnAdd.Enabled = true;
                //BKNPLChuaThanhLy bk = (BKNPLChuaThanhLy)e.Row.DataRow;
                //donViTinhQuyDoiControl1.DVT_QuyDoi = bk.DVT_QuyDoi;
            }
        }
        private bool checkExit(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL)
        {
            foreach (BKNPLChuaThanhLy bk in this.bkCollection)
            {
                if (bk.SoToKhai == soToKhai && bk.MaLoaiHinh == maLoaiHinh && bk.NamDangKy == namDangKy && bk.MaHaiQuan == maHaiQuan && bk.MaNPL == maNPL)
                    return true;
            }
            return false;
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {

            cvError.Validate();
            if (cvError.IsValid)
            {
                //datcv hàm này có thể sai trong trường hợp tờ khai đó có các dòng hàng cùng mã hàng
                //khi đó, các dòng hàng có cùng mã sẽ không thể thêm vào bảng kê không thanh lý được
                //cần phải có một hàm khác để kiểm tra trong trường hợp này

                // -- Before --
                //if (checkExit(this.SoToKhai, this.MaLoaiHinh, Convert.ToInt16(this.NgayDangKy.Year), this.MaHaiQuan.Trim(), txtMaNPL.Text))
                // -- Before --

                // -- After --
                if (DaTonTaiNPL(this.SoToKhai, this.MaLoaiHinh, Convert.ToInt16(this.NgayDangKy.Year), this.MaHaiQuan.Trim(), txtMaNPL.Text, Convert.ToDecimal(txtLuong.Text), txtTenNPL.Text))
                // -- After --
                {
                    // ShowMessage("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác", false);
                    MLMessages("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác", "MSG_PUB25", "", false);
                    txtSoToKhai.Text = "";
                    txtLuong.Value = 0;
                    txtMaNPL.Text = "";
                    return;
                }
                //-------------------------------------------------
                if (this.SoLuong < Convert.ToDecimal(txtLuong.Value))
                {
                    // ShowMessage("Lượng chưa thanh lý phải nhỏ hơn lượng tồn nguyên phụ liệu của tờ khai.", false);
                    MLMessages("Lượng chưa thanh lý phải nhỏ hơn lượng tồn nguyên phụ liệu của tờ khai.", "MSG_PUB25", "", false);
                    txtLuong.Value = 0;
                    txtLuong.Focus();
                    return;
                }
                BKNPLChuaThanhLy bk = new BKNPLChuaThanhLy();
                bk.BangKeHoSoThanhLy_ID = 0;
                bk.SoToKhai = this.SoToKhai;
                bk.SoToKhaiVNACCS = this.SoToKhaiVNACCS;
                bk.MaLoaiHinh = this.MaLoaiHinh;
                bk.NgayDangKy = this.NgayDangKy;
                bk.MaHaiQuan = this.MaHaiQuan.Trim();
                bk.NamDangKy = Convert.ToInt16(this.NgayDangKy.Year);
                bk.MaNPL = txtMaNPL.Text;
                bk.TenNPL = this.TenNPL;
                bk.DVT_ID = this.DVT_ID;
                bk.TenDVT = DonViTinh_GetName(this.DVT_ID);
                bk.Luong = Convert.ToDecimal(txtLuong.Value);
                if (donViTinhQuyDoiControl1.IsEnable)
                    bk.DVT_QuyDoi = donViTinhQuyDoiControl1.DVT_QuyDoi;
                else
                    bk.DVT_QuyDoi = null;
                this.bkCollection.Add(bk);
                dgList2.Refetch();
                txtSoToKhai.Text = "";
                txtMaNPL.Text = "";
                txtLuong.Value = 0;
                donViTinhQuyDoiControl1.Reset();
            }
        }

        /// <summary>
        /// Khi tờ khai có các mã hàng giống nhau, thì tên hàng và lượng nhập là 2 yếu tố tiếp theo
        /// có thể để kiểm tra xem dòng hàng đó đã có trong bảng kê hay chưa
        /// Hàm này kiểm tra một NPL đã tồn tại trong BKNPLChuaThanhLy hay chua?
        /// </summary>        
        /// <param name="luongNhap">Lượng nhập của NPL tương ứng trong tờ khai</param>
        /// <param name="tenNPL">tên NPL tương ứng với dòng hàng</param>
        /// <author>datcv</author>
        /// <returns></returns>
        private bool DaTonTaiNPL(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL, decimal luongTon, string tenNPL)
        {
            foreach (BKNPLChuaThanhLy bk in this.bkCollection)
            {
                if (bk.SoToKhai == soToKhai && bk.MaLoaiHinh == maLoaiHinh && bk.NamDangKy == namDangKy && bk.MaHaiQuan == maHaiQuan && bk.MaNPL == maNPL && bk.Luong == luongTon && bk.TenNPL == tenNPL)
                    return true;
            }
            return false;
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (this.bkCollection.Count == 0)
            {
                ShowMessage("Bạn chưa nhập NPL chưa thanh lý.", false);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (this.HSTL.getBKNPLChuaThanhLY() < 0)
                {
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.MaterID = this.HSTL.ID;
                    bk.STTHang = this.HSTL.BKCollection.Count + 1;
                    bk.MaBangKe = "DTLNPLCHUATL";
                    bk.TenBangKe = "DTLNPLCHUATL";
                    bk.SoHSTL = this.HSTL.SoHoSo;
                    bk.TrangThaiXL = this.HSTL.TrangThaiXuLy;
                    bk.bkNPLCTLCollection = this.bkCollection;
                    this.HSTL.BKCollection.Add(bk);
                }
                else
                {
                    this.HSTL.BKCollection[this.HSTL.getBKNPLChuaThanhLY()].bkNPLCTLCollection = this.bkCollection;
                }
                this.HSTL.BKCollection[this.HSTL.getBKNPLChuaThanhLY()].InsertUpdate_BKNPLCHUATL(this.bkCollection);
                ShowMessage("Lưu thông tin thành công", false);
                //this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi lưu bảng kê: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList2_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["SoToKhaiVNACCS"].Text = e.Row.Cells["SoToKhaiVNACCS"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //private void dgList2_UpdatingCell(object sender, UpdatingCellEventArgs e)
        //{
        //    if (e.Column.Key == "Luong")
        //    {
        //        dgList1.get
        //        if (Convert.ToDecimal(e.Value) > this.SoLuong)
        //        {
        //            ShowMessage("Lượng chưa thanh lý lớn hơn số lượng nguyên phụ liệu của tờ khai.", false);
        //            e.Cancel = true;
        //        }

        //    }
        //}

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (this.HSTL.TrangThaiThanhKhoan == 401) return;
            txtSoToKhai.Text = e.Row.Cells["SoToKhaiVNACCS"].Text;
            this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
            this.MaLoaiHinh = Convert.ToString(e.Row.Cells["MaLoaiHinh"].Value);
            this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
            txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
            this.TenNPL = e.Row.Cells["TenNPL"].Text;
            this.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
            this.DVT_ID = Convert.ToString(e.Row.Cells["DVT_ID"].Value);
            txtLuong.Value = Convert.ToDecimal(e.Row.Cells["Luong"].Value);
            txtTenNPL.Text = e.Row.Cells["TenNPL"].Text;
            txtDonViTinh.Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
            this.Postion = e.Row.Position;
            btnSave.Enabled = true;
            btnAdd.Enabled = false;
            this.SoLuong = new BKToKhaiNhap().getLuongTon(this.SoToKhai, this.MaLoaiHinh, this.MaHaiQuan.Trim(), Convert.ToInt16(this.NgayDangKy.Year), txtMaNPL.Text);
            BKNPLChuaThanhLy bk = (BKNPLChuaThanhLy)e.Row.DataRow;
            #region Đơn vị tính quy đổi
            if (bk.DVT_QuyDoi != null)
            {
                donViTinhQuyDoiControl1.DVT_QuyDoi = bk.DVT_QuyDoi;
                donViTinhQuyDoiControl1.Master_Id = 0;
            }
            else if (bk.ID > 0)
            {
                donViTinhQuyDoiControl1.Master_Id = bk.ID;
                donViTinhQuyDoiControl1.Type = "NPL_CTL";
            }
            else
                donViTinhQuyDoiControl1.DVT_QuyDoi = null;
            donViTinhQuyDoiControl1.DonViTinhQuyDoiControl_Load(null, null);
            //donViTinhQuyDoiControl1.DVT_QuyDoi = bk.DVT_QuyDoi;
            #endregion
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (this.SoLuong < Convert.ToDecimal(txtLuong.Value))
                {
                    // ShowMessage("Lượng chưa thanh lý phải nhỏ hơn lượng tồn nguyên phụ liệu của tờ khai.", false);
                    MLMessages("Lượng chưa thanh lý phải nhỏ hơn lượng tồn nguyên phụ liệu của tờ khai.", "MSG_PUB25", "", false);
                    txtLuong.Value = 0;
                    txtLuong.Focus();
                    return;
                }
                BKNPLChuaThanhLy bk = new BKNPLChuaThanhLy();
                bk.BangKeHoSoThanhLy_ID = 0;
                bk.SoToKhai = this.SoToKhai;
                bk.SoToKhaiVNACCS = this.SoToKhaiVNACCS;
                bk.MaLoaiHinh = this.MaLoaiHinh;
                bk.NgayDangKy = this.NgayDangKy;
                bk.MaHaiQuan = this.MaHaiQuan.Trim();
                bk.NamDangKy = Convert.ToInt16(this.NgayDangKy.Year);
                bk.MaNPL = txtMaNPL.Text;
                bk.TenNPL = this.TenNPL;
                bk.DVT_ID = this.DVT_ID;
                bk.TenDVT = DonViTinh_GetName(this.DVT_ID);
                bk.Luong = Convert.ToDecimal(txtLuong.Value);
                if (donViTinhQuyDoiControl1.IsEnable)
                    bk.DVT_QuyDoi = donViTinhQuyDoiControl1.DVT_QuyDoi;
                else
                    bk.DVT_QuyDoi = null;
                this.bkCollection[this.Postion] = bk;
                dgList2.Refetch();
                txtSoToKhai.Text = "";
                txtMaNPL.Text = "";
                txtLuong.Value = 0;
                donViTinhQuyDoiControl1.Reset();
            }
        }

        private void txtSoToKhai_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmdThuTucHQTruocDo_Click(object sender, EventArgs e)
        {
            if (this.HSTL.getBKNPLChuaThanhLY() < 0)
                ShowMessage("Vui lòng lưu thông tin bảng kê trước khi thêm chứng từ HQ trước đó", false);
            else
            {
                ListChungTuTruocDoForm f = new ListChungTuTruocDoForm();
                f.Master_ID = this.HSTL.getBKNPLChuaThanhLY();
                f.Type = "BK_NPLCTL";
                f.ShowDialog(this);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "NGUYÊN PHỤ LIỆU CHƯA THANH LÝ " + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList2;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            ImportExcelBK03Form f = new ImportExcelBK03Form();
            f.bkCollection = bkCollection;
            f.ShowDialog(this);
            dgList2.Refetch();
        }

    }
}
