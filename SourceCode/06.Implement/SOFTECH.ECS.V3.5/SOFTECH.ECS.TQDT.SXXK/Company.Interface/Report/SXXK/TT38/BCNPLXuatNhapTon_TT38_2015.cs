using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Linq;

namespace Company.Interface.Report.SXXK
{
    public partial class BCNPLXuatNhapTon_TT38_2015 : DevExpress.XtraReports.UI.XtraReport
    {
        public int BangKeHSTL_ID;
        public int LanThanhLy;
        private int STT = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        private DataTable DtGhiChu = new DataTable();
        private DataTable dt = new DataTable();
        private decimal SoToKhaiNhap = 0;
        private decimal SoToKhaiXuat = 0;
        private decimal SoToKhaiXuatTemp = 0;
        private string MaNPLGroup = "";
        private string MaNPLTempGroup = "";
        private string MaNPLKeTiepGroup = "";
        private string MaSPGroup = "";
        private string MaNPL2TempGroup = "";
        private decimal SumTongLuongNhap = 0;
        private decimal SumTongLuongTonDau = 0;
        private decimal SumTongLuongTonCuoi = 0;
        private decimal SumTongLuongSuDung = 0;
        private decimal temp = 0;

        public BCNPLXuatNhapTon_TT38_2015()
        {
            InitializeComponent();
        }

        public void BindReport(string where)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";
            int sapxeptkx = Properties.Settings.Default.TKX;
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                if (sapxeptkx == 2)
                    order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    //order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                    //TODO: Update by Hungtq 08/06/2012. Bỏ sắp xếp theo "ngayhoanthanhxuat" 
                    //dùng hiển thị đúng thứ tự tờ khai âm kế tiếp. Ngày của tờ khai âm kế tiếp có thế < tờ khai trước đó.
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
                else
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";

            }


            dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, order).Tables[0];
            int i = 1;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                {
                    dr["STT"] = i;
                }
                else
                {

                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    i++;
                    dr["STT"] = i;
                }
            }
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;
            lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //Bo sung by Hungtq 21/06/2012
            //lblTongLuongNhapF2.Summary.Running = SummaryRunning.Group;
            //lblTongLuongNhapF2.Summary.Func = SummaryFunc.Sum;
            //lblTongLuongNhapF2.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //lblTongTonDauF2.Summary.Running = SummaryRunning.Group;
            //lblTongTonDauF2.Summary.Func = SummaryFunc.Sum;
            //lblTongTonDauF2.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //lblTongTonCuoiF2.Summary.Running = SummaryRunning.Group;
            //lblTongTonCuoiF2.Summary.Func = SummaryFunc.Sum;
            //lblTongTonCuoiF2.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //lblTongLuongNPLSuDungF2.Summary.Running = SummaryRunning.Group;
            //lblTongLuongNPLSuDungF2.Summary.Func = SummaryFunc.Sum;
            //lblTongLuongNPLSuDungF2.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL"),
                new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
                //new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                //new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                //new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")
            }
            else
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL"),
                new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});

            }
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            if (SoHoSo > 0)
            {
                lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
                if (!string.IsNullOrEmpty(GlobalSettings.NoiDungSoHoSoThanhKhoan))
                {
                    lblSHSTK.Text = lblSHSTK.Text + GlobalSettings.NoiDungSoHoSoThanhKhoan;
                }
            }
            else
                lblSHSTK.Text = "";
            lblSHSTK.Width = 300;
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            //lblMaNPL.DataBindings.Add("Text", this.DataSource,  dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblMaSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            if (GlobalSettings.SoThapPhan.MauBC04 == 0)
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            else
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            lblLuongSPXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSPXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
            lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
            lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
            lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep", "{0:n3}");
            //lblChuyenMucDichKhac.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChuyenMucDichKhac", "{0:n3}");
            lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");

            //Bo sung by Hungtq 21/06/2012
            //lblTongLuongNhapF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonDauF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonCuoiF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTenDVT_NPLF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblTongLuongNPLSuDungF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

            //HQ
            xrLabel76.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            xrLabel77.Text = GlobalSettings.TieudeNgay;

            //Bo sung them cot Index tren table
            DataColumn colIndex = new DataColumn("Index", typeof(int));
            dt.Columns.Add(colIndex);

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                dt.Rows[j]["Index"] = j;
            }
        }

        public void BindReportByMaNPL(string where, string MaNPL)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";
            int sapxeptkx = Properties.Settings.Default.TKX;
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                if (sapxeptkx == 2)
                    order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    //order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                    //TODO: Update by Hungtq 08/06/2012. Bỏ sắp xếp theo "ngayhoanthanhxuat" 
                    //dùng hiển thị đúng thứ tự tờ khai âm kế tiếp. Ngày của tờ khai âm kế tiếp có thế < tờ khai trước đó.
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
                else
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";

            }


            dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaNPL ='" + MaNPL + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, order).Tables[0];
            int i = 1;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                {
                    dr["STT"] = i;
                }
                else
                {
                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    i++;
                    dr["STT"] = i;
                }
            }
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;
            lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //Bo sung by Hungtq 21/06/2012
            //lblTongLuongNhapF2.Summary.Running = SummaryRunning.Group;
            //lblTongLuongNhapF2.Summary.Func = SummaryFunc.Sum;
            //lblTongLuongNhapF2.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //lblTongTonDauF2.Summary.Running = SummaryRunning.Group;
            //lblTongTonDauF2.Summary.Func = SummaryFunc.Sum;
            //lblTongTonDauF2.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //lblTongTonCuoiF2.Summary.Running = SummaryRunning.Group;
            //lblTongTonCuoiF2.Summary.Func = SummaryFunc.Sum;
            //lblTongTonCuoiF2.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            //lblTongLuongNPLSuDungF2.Summary.Running = SummaryRunning.Group;
            //lblTongLuongNPLSuDungF2.Summary.Func = SummaryFunc.Sum;
            //lblTongLuongNPLSuDungF2.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap")});
                //new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                //new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                //new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")
            }
            else
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});

            }
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            if (SoHoSo > 0)
            {
                lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
                if (!string.IsNullOrEmpty(GlobalSettings.NoiDungSoHoSoThanhKhoan))
                {
                    lblSHSTK.Text = lblSHSTK.Text + GlobalSettings.NoiDungSoHoSoThanhKhoan;
                }

            }
            else
                lblSHSTK.Text = string.Empty;
            lblSHSTK.Width = 300;
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            //lblMaNPL.DataBindings.Add("Text", this.DataSource,  dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblMaSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            if (GlobalSettings.SoThapPhan.MauBC04 == 0)
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            else
                lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            lblLuongSPXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongSPXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
            lblDinhMuc.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMuc", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
            lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
            lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep", "{0:n3}");
            //lblChuyenMucDichKhac.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChuyenMucDichKhac", "{0:n3}");
            lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");

            //Bo sung by Hungtq 21/06/2012
            //lblTongLuongNhapF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonDauF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonCuoiF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTenDVT_NPLF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            //lblTongLuongNPLSuDungF2.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

            //HQ
            xrLabel76.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            xrLabel77.Text = GlobalSettings.TieudeNgay;

            //Bo sung them cot Index tren table
            DataColumn colIndex = new DataColumn("Index", typeof(int));
            dt.Columns.Add(colIndex);

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                dt.Rows[j]["Index"] = j;
            }
        }

        private void lblMaSP_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (GlobalSettings.MA_DON_VI == "0400101242")//Rieng cong ty Luoi
            //{
            //    string[] temp = GetCurrentColumnValue("TenSP").ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            //    if (temp.Length > 0)
            //        lblMaSP.Text = temp[0] + " " + GetCurrentColumnValue("MaSP").ToString();
            //}
            //else
            //{
            lblMaSP.Text = GetCurrentColumnValue("TenSP").ToString();
            if (GetCurrentColumnValue("MaSP").ToString().Trim().Length > 0)
                lblMaSP.Text = lblMaSP.Text + " / " + GetCurrentColumnValue("MaSP").ToString();
            //}
        }

        private void lblSoToKhaiTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0)
                lblSoToKhaiTaiXuat1.Text = "";
        }

        private void lblLuongNPLTaiXuat_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0)
                lblLuongNPLTaiXuat1.Text = "";
        }

        private void lblToKhaiNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblToKhaiNhap.Text = "Tổng lượng tờ khai " + Convert.ToString(GetCurrentColumnValue("SoToKhaiNhap")) + "/" + GetCurrentColumnValue("MaLoaiHinhNhap").ToString() + "/" + Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyNhap")).Year;
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        }

        private void BCNPLXuatNhapTon_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
            this.DtGhiChu = new DataTable();
            DataColumn[] cols = new DataColumn[3];
            cols[0] = new DataColumn("MaNPL", typeof(string));
            cols[1] = new DataColumn("From", typeof(decimal));
            cols[2] = new DataColumn("To", typeof(decimal));
            this.DtGhiChu.Columns.AddRange(cols);
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            if (lblSoToKhaiNhap.Text != "")
                SoToKhaiNhap = Convert.ToDecimal(lblSoToKhaiNhap.Text);
            //if(GetCurrentColumnValue("MaNPL").ToString() == "Dung100P150".ToUpper())
            //{

            //};
            MaNPLGroup = GetCurrentColumnValue("MaNPL").ToString();

            if (MaNPLTempGroup == "") MaNPLTempGroup = MaNPLGroup;

            if (MaNPLGroup != MaNPL2TempGroup )
            {
                MaNPL2TempGroup = MaNPLGroup;
                SumTongLuongSuDung = 0;
            }
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSTT.Text = this.STT + "";
        }

        private void lblSoToKhaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) > 0) ((XRTableCell)sender).Text = "";
            if (((XRTableCell)sender).Name == "lblLuongSPXuat")
            {
                if (Convert.ToDecimal(GetCurrentColumnValue("LuongSPXuat")) == 0) ((XRTableCell)sender).Text = "";
            }
        }

        private void lblSoToKhaiTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiTaiXuat")) == 0) lblSoToKhaiTaiXuat.Text = "";
        }

        private void lblNgayTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDateTime(GetCurrentColumnValue("NgayTaiXuat")).Year == 1900) lblNgayTaiXuat.Text = "";
        }

        private void lblLuongNPLTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0) lblLuongNPLTaiXuat.Text = "";
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (GetCurrentColumnValue("MaSP").ToString() == "") e.Cancel = true;

            SoToKhaiXuat = System.Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiTaiXuat"));

            if (GetCurrentColumnValue("MaSP").ToString() != MaSPGroup
                && SoToKhaiXuat != SoToKhaiXuatTemp)
            {
                MaSPGroup = GetCurrentColumnValue("MaSP").ToString();
                SoToKhaiXuatTemp = SoToKhaiXuat;
                SumTongLuongSuDung += System.Convert.ToDecimal(GetCurrentColumnValue("LuongNPLSuDung"));
            }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
              
            }
            
        }

        private void lblThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if ((temp - Convert.ToDecimal(GetCurrentColumnValue("LuongNPLSuDung"))) != Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi")))
            {
                decimal from = GetFrom(GetCurrentColumnValue("MaNPL").ToString(), Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiNhap")));
                if (from > 0)
                    lblThanhKhoanTiep.Text = "Chuyển từ TK " + from;
                else
                    lblThanhKhoanTiep.Text = "";
            }
            else
                lblThanhKhoanTiep.Text = "";
            temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
        }

        private decimal GetFrom(string maNPL, decimal to)
        {
            foreach (DataRow dr in this.DtGhiChu.Rows)
            {
                if (dr["MaNPL"].ToString() == maNPL && to == Convert.ToDecimal(dr["To"]))
                    return Convert.ToDecimal(dr["From"]); 
            }
            return 0;
        }

        private void lblTongThanhKhoanTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblTongThanhKhoanTiep.Text.Contains("Chuyển TK"))
            {
                DataRow dr = this.DtGhiChu.NewRow();
                dr["MaNPL"] = GetCurrentColumnValue("MaNPL").ToString();
                dr["From"] = Convert.ToDecimal(GetCurrentColumnValue("SoToKhaiNhap"));
                dr["To"] = Convert.ToDecimal(lblTongThanhKhoanTiep.Text.Replace("Chuyển TK ", ""));
                this.DtGhiChu.Rows.Add(dr);
            }
        }

        private void lblDinhMuc_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDinhMuc.Text = GetCurrentColumnValue("DinhMuc").ToString() + " / " + GetCurrentColumnValue("TenDVT_SP").ToString();
        }

        private void SetTextLabel(string maNPL, string tenNPL, decimal luongNhap, decimal luongTonDau, decimal luongTonCuoi, decimal luongSuDung)
        {
            //lblMaNPLF2.Text = tenNPL + " / " + maNPL;
            //lblTenDVT_NPLF2.Text = GetCurrentColumnValue("TenDVT_NPL").ToString();
            //lblTongLuongNhapF2.Text = luongNhap.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.LuongNPL));
            //lblTongTonDauF2.Text = luongTonDau.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.LuongNPL));
            //lblTongLuongNPLSuDungF2.Text = luongSuDung.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.LuongNPL));
            lblTongTonCuoi.Text = (luongTonDau-luongSuDung).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.LuongNPL));
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                DataRow[] rowsFilter = dt.Select(string.Format("Index = {0}", System.Convert.ToInt64(GetCurrentColumnValue("Index")) + 1));

                if (rowsFilter.Length > 0)
                    MaNPLKeTiepGroup = rowsFilter[0]["MaNPL"].ToString();

                if (MaNPLTempGroup != "" && MaNPLGroup != "" && MaNPLGroup != MaNPLTempGroup)
                {
                    AssignSumGroup();

                    MaNPLTempGroup = MaNPLGroup;

                    //xrTable4.Visible = (MaNPLTempGroup != MaNPLKeTiepGroup);
                    //xrTable4.Height = 25;

                    goto Update;
                }
                else if (MaNPLTempGroup != "" && MaNPLGroup != ""
                    && MaNPLGroup == MaNPLTempGroup
                    && GetCurrentColumnValue("SoToKhaiNhap").ToString() != SoToKhaiNhap.ToString())
                {
                    CalculateSumGroup();

                    //xrTable4.Visible = (MaNPLTempGroup != MaNPLKeTiepGroup) || (rowsFilter.Length == 0);
                    //xrTable4.Height = 25;

                    goto Update;
                }
                else
                {
                    MaNPLTempGroup = MaNPLGroup;

                    AssignSumGroup();

                    //xrTable4.Visible = false;
                    //xrTable4.Height = 25;

                    //goto Update;
                }

            Update:
                SetTextLabel(
                    GetCurrentColumnValue("MaNPL").ToString(),
                    GetCurrentColumnValue("TenNPL").ToString(),
                    SumTongLuongNhap,
                    SumTongLuongTonDau,
                    SumTongLuongTonCuoi,
                    SumTongLuongSuDung);
                lblTongTonCuoi.Text = (SumTongLuongTonDau - SumTongLuongSuDung).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void AssignSumGroup()
        {
            SumTongLuongNhap = System.Convert.ToDecimal(GetCurrentColumnValue("LuongNhap"));
            SumTongLuongTonDau = System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            SumTongLuongTonCuoi = System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
            //SumTongLuongSuDung = System.Convert.ToDecimal(GetCurrentColumnValue("LuongNPLSuDung"));
        }

        private void CalculateSumGroup()
        {
            SumTongLuongNhap += System.Convert.ToDecimal(GetCurrentColumnValue("LuongNhap"));
            SumTongLuongTonDau += System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            SumTongLuongTonCuoi = System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
            //SumTongLuongSuDung += System.Convert.ToDecimal(GetCurrentColumnValue("LuongNPLSuDung"));
        }
    }
}
