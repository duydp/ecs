﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.GC.BLL.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC.PCTFormTemplate
{
    public partial class FormBoSungNhomSP : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        public bool boolFlag;      
        public FormBoSungNhomSP()
        {
            InitializeComponent();           
        }
    

        private void LoadLoaiSanPham()
        {          

            
        }
        private void LoaiSanPhamGCEditForm_Load(object sender, EventArgs e)
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "S15")
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (pkdk.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                btnAdd.Enabled = false;
                btnXoa.Enabled = false;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            //Registered
            if (boolFlag == true)
            {
                btnAdd.Enabled = true;
                //dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            LoaiPK.MaPhuKien = "S15";
            dgList.DataSource = LoaiPK.HPKCollection;
        }
        
        private void btnAdd_Click(object sender, EventArgs e)
        {
            cbTen.Focus();
            {
                if (Convert.ToDecimal(txtSoLuong.Text) == 0)
                {
                    error.SetError(txtSoLuong,setText( "Số lượng phải lớn hơn 0.","This value must be greater than 0"));
                    return;
                }
                if (Convert.ToDecimal(txtGia.Text) == 0)
                {
                    error.SetError(txtGia, setText("Giá gia công phải lớn hơn 0.", "This value must be greater than 0"));
                    return;
                }
                if (cbDonViTinh.Text == "")
                {
                    error.SetError(cbDonViTinh,setText( " \"Đơn vị tính\" không được để trống","This VietNam must be filled "));
                    return;
                }
                checkExitsNhomSanPhamAndSTTHang();                
            }
           
            
        }
        private void checkExitsNhomSanPhamAndSTTHang()
        {
            try
            {
                Company.GC.BLL.GC.NhomSanPham nhomsp = new Company.GC.BLL.GC.NhomSanPham();
                nhomsp.HopDong_ID = this.HD.ID;
                nhomsp.MaSanPham = cbTen.Ma.ToString();
                if (nhomsp.Load())
                {
                    showMsg("MSG_240214");
                    //ShowMessage("Loại sản phẩm này đã được khai báo.", false);
                    cbTen.Focus();
                    return;
                }
                LoaiPK.HPKCollection.Remove(HangPK);
                foreach (HangPhuKien p in LoaiPK.HPKCollection)
                {
                    if (p.MaHang.Trim() == cbTen.Ma.ToString().Trim())
                    {
                        //ShowMessage("Đã có loại sản phẩm này trong danh sách.", false);
                        showMsg("MSG_240215");
                        if (HangPK.MaHang.Trim() != "")
                            LoaiPK.HPKCollection.Add(HangPK);
                        cbTen.Focus();
                        return;
                    }
                }
                HangPK.SoLuong = Convert.ToDecimal(txtSoLuong.Text);
                HangPK.DonGia = Convert.ToDouble(txtGia.Text);
                HangPK.MaHang = cbTen.Ma.ToString();
                HangPK.TenHang = cbTen.Ten.Trim();
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                if (boolFlag == true)
                {
                    LoaiPK.InsertUpdateBoSungNhomSamPhamGiaCongDaDangKy(pkdk.HopDong_ID);
                    boolFlag = false;
                }
                else
                {
                    LoaiPK.InsertUpdateBoSungNhomSamPhamGiaCong(pkdk.HopDong_ID);
                }

                //PhuKienGCSendForm.isEdit = true;
                dgList.DataSource = LoaiPK.HPKCollection;
                reset();
                this.setErro();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
        private void setErro()
        {
            //error.Clear();
            error.SetError(txtGia , null);
            error.SetError(txtSoLuong , null);
            error.SetError(cbDonViTinh , null);          
        }

        private void dgNhomSanPham_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            HangPK = (Company.GC.BLL.KDT.GC.HangPhuKien)e.Row.DataRow;
            cbDonViTinh.SelectedValue = HangPK.DVT_ID;
            cbTen.Ma = HangPK.MaHang.Trim();
            txtGia.Text = HangPK.DonGia.ToString();
            txtSoLuong.Text = HangPK.SoLuong.ToString();
        }

      
   
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);        
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien,pkdk.HopDong_ID);
                        }
                    }
                }

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void FormBoSungNhomSP_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (LoaiPK.HPKCollection.Count > 0)
            {
                LoaiPK.MaPhuKien = "S15";
                LoaiPK.NoiDung = LoaiPhuKien_GetName("S15");
                pkdk.PKCollection.Remove(LoaiPK);
                pkdk.PKCollection.Add(LoaiPK);
            }
            else
            {
                pkdk.PKCollection.Remove(LoaiPK);
                LoaiPK.Delete(pkdk.HopDong_ID);;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            HangPhuKienCollection hpkColl = new HangPhuKienCollection();
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;

            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa nhóm sản phẩm gia công này không?", true) == "Yes")
            {

                
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                        }
                        hpkColl.Add(pkDelete);
                        //try { LoaiPK.HPKCollection.Remove (pkDelete); }
                        //catch { }
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.HangPhuKien hpkk in hpkColl)
                {
                    try
                    {
                        LoaiPK.HPKCollection.Remove(hpkk);
                    }
                    catch { }

                }
                dgList.DataSource = LoaiPK.HPKCollection;
                this.reset();
                this.setErro();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }

            }
        }

        private void reset()
        {
            txtGia.Text = "0";
            txtSoLuong.Text = "0";           
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            cbTen.SelectIndex = 0;
            try
            {
                dgList.Refetch();                
            }
            catch
            {
                dgList.Refresh();
            }
            HangPK = new HangPhuKien();
        }

        private void txtGia_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtGia.Text) <= 0 || Convert.ToDecimal(txtGia.Text) > 1000000000000 )
            {
                error.SetError(txtGia, setText("Giá gia công không hợp lệ", "Invalid value"));
                txtGia.Focus();
                return;
            }else
            {
                error.SetError(txtGia, null);
            }
        }

        private void txtSoLuong_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtSoLuong.Text) <= 0 || Convert.ToDecimal(txtSoLuong.Text) > 100000000000000)
            {
                error.SetError(txtSoLuong, setText("Số lượng không hợp lệ", "Invalid value"));
                txtSoLuong.Focus();
                return;
            }else
            {
                error.SetError(txtSoLuong, null);
            }
        }

       

    }
}