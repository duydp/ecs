﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System.Collections;
using Janus.Windows.EditControls;
using Company.BLL.SXXK;

namespace Company.Interface.Report
{
    
    public partial class ReportViewNPLForm : BaseForm
    {
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        //private ReportNPL reportNPL = new ReportNPL();
        //private ReportNPL_TT196 reportNPL = new ReportNPL_TT196();
        private ReportNPL_TT128_2013 reportNPL = new ReportNPL_TT128_2013();

        public ReportViewNPLForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            this.reportNPL.NPLCollection = this.NPLCollection;
            this.reportNPL.BindReportDinhMucDaDangKy();
            printControl1.PrintingSystem = this.reportNPL.PrintingSystem;
            this.reportNPL.CreateDocument();
        }     

        private void btnPrint_Click(object sender, EventArgs e)
        {            
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }                
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.reportNPL.BindHD(txtHDNhapKhau.Text);
            this.reportNPL.CreateDocument();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {            
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }   
        }
    
    }
}