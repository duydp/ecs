
--TP.HCM
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = 'hqhcm.com', ServicePathV4 = 'CISService/CISService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '02'
GO

ALTER TABLE dbo.t_KDT_DeNghiChuyenCuaKhau
ADD DiaDiemKiemTraCucHQThanhPho NVARCHAR(500) NULL
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]    Script Date: 03/04/2013 17:13:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID],
	[DiaDiemKiemTraCucHQThanhPho]
FROM
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID],
	[DiaDiemKiemTraCucHQThanhPho]
FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID],
	[DiaDiemKiemTraCucHQThanhPho]
FROM
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[TKMD_ID] = @TKMD_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID],
	[DiaDiemKiemTraCucHQThanhPho]
FROM
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[TKMD_ID] = @TKMD_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]
	@ID bigint,
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@SoVanDon varchar(500),
	@NgayVanDon datetime,
	@ThoiGianDen datetime,
	@DiaDiemKiemTra nvarchar(500),
	@TuyenDuong nvarchar(500),
	@PTVT_ID varchar(3),
	@DiaDiemKiemTraCucHQThanhPho nvarchar(500)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_DeNghiChuyenCuaKhau] 
		SET
			[ThongTinKhac] = @ThongTinKhac,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[ThoiGianDen] = @ThoiGianDen,
			[DiaDiemKiemTra] = @DiaDiemKiemTra,
			[TuyenDuong] = @TuyenDuong,
			[PTVT_ID] = @PTVT_ID,
			[DiaDiemKiemTraCucHQThanhPho] = @DiaDiemKiemTraCucHQThanhPho
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_DeNghiChuyenCuaKhau]
		(
			[ThongTinKhac],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan],
			[SoVanDon],
			[NgayVanDon],
			[ThoiGianDen],
			[DiaDiemKiemTra],
			[TuyenDuong],
			[PTVT_ID],
			[DiaDiemKiemTraCucHQThanhPho]
		)
		VALUES 
		(
			@ThongTinKhac,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan,
			@SoVanDon,
			@NgayVanDon,
			@ThoiGianDen,
			@DiaDiemKiemTra,
			@TuyenDuong,
			@PTVT_ID,
			@DiaDiemKiemTraCucHQThanhPho
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]
	@ID bigint,
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@SoVanDon varchar(500),
	@NgayVanDon datetime,
	@ThoiGianDen datetime,
	@DiaDiemKiemTra nvarchar(500),
	@TuyenDuong nvarchar(500),
	@PTVT_ID varchar(3),
	@DiaDiemKiemTraCucHQThanhPho nvarchar(500)
AS

UPDATE
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
SET
	[ThongTinKhac] = @ThongTinKhac,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[ThoiGianDen] = @ThoiGianDen,
	[DiaDiemKiemTra] = @DiaDiemKiemTra,
	[TuyenDuong] = @TuyenDuong,
	[PTVT_ID] = @PTVT_ID,
	[DiaDiemKiemTraCucHQThanhPho] = @DiaDiemKiemTraCucHQThanhPho
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]    Script Date: 03/04/2013 17:13:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@SoVanDon varchar(500),
	@NgayVanDon datetime,
	@ThoiGianDen datetime,
	@DiaDiemKiemTra nvarchar(500),
	@TuyenDuong nvarchar(500),
	@PTVT_ID varchar(3),
	@DiaDiemKiemTraCucHQThanhPho nvarchar(500),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_DeNghiChuyenCuaKhau]
(
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID],
	[DiaDiemKiemTraCucHQThanhPho]
)
VALUES 
(
	@ThongTinKhac,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan,
	@SoVanDon,
	@NgayVanDon,
	@ThoiGianDen,
	@DiaDiemKiemTra,
	@TuyenDuong,
	@PTVT_ID,
	@DiaDiemKiemTraCucHQThanhPho
)

SET @ID = SCOPE_IDENTITY()


GO
UPDATE dbo.t_HaiQuan_Version SET [Version] = '7.8', [Date] = GETDATE(), Notes = N''