//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
   TListBox *lstOutput;
   TButton *btnWrite;
   TButton *btnRead;
   TButton *btnTransform;
   TButton *btnEncrypt;
   TButton *btnDecrypt;
   TButton *btnWriteMap;
   TButton *btnReadMap;
   void __fastcall btnWriteClick(TObject *Sender);
   void __fastcall btnReadClick(TObject *Sender);
   void __fastcall btnTransformClick(TObject *Sender);
   void __fastcall btnEncryptClick(TObject *Sender);
   void __fastcall btnDecryptClick(TObject *Sender);
   void __fastcall btnWriteMapClick(TObject *Sender);
   void __fastcall btnReadMapClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
