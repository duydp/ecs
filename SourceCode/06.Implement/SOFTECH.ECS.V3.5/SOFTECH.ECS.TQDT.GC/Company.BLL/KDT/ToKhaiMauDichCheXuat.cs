﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using System.Threading;
using System.Collections;
using System.Text;
using Company.GC.BLL.KDT.SXXK;
using System.Data.Common;
using Company.KDT.SHARE.Components.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
namespace Company.GC.BLL.KDT
{
    public partial class ToKhaiMauDichCheXuat
    {
        private HangMauDichCheXuatCollection _HMDCollection = new HangMauDichCheXuatCollection();
        public HangMauDichCheXuatCollection HMDCollection
        {
            set { this._HMDCollection = value; }
            get { return this._HMDCollection; }
        }
    
        public IDataReader SelectReaderDynamic(string sql, DateTime from, DateTime to)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@From", SqlDbType.DateTime, from);
            db.AddInParameter(dbCommand, "@To", SqlDbType.DateTime, to);

            return db.ExecuteReader(dbCommand);
        }
        //-----------------------------------------------------------------------------------------
        public ToKhaiMauDichCheXuatCollection SelectCollectionDynamic(string sql, DateTime from, DateTime to)
        {
            ToKhaiMauDichCheXuatCollection collection = new ToKhaiMauDichCheXuatCollection();

            IDataReader reader = this.SelectReaderDynamic(sql, from, to);
            while (reader.Read())
            {
                ToKhaiMauDichCheXuat entity = new ToKhaiMauDichCheXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));                
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));                
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

   
        //-----------------------------------------------------------------------------------------

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            long id = this.ID;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    int i = 1;
                    foreach (HangMauDichCheXuat hmd in this.HMDCollection)
                    {
                        if (id == 0)
                            hmd.ID = 0;
                        hmd.SoThuTuHang = i++;
                        if (hmd.ID == 0)
                        {
                            hmd.TKMD_ID = this.ID;
                            hmd.ID = hmd.InsertTransaction(transaction);
                        }
                        else
                        {
                            hmd.UpdateTransaction(transaction);
                        }
                    }
                    i = 1;                                   
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
  
        //-----------------------------------------------------------------------------------------  
        public void LoadHMDCollection()
        {
            HangMauDichCheXuat hmd = new HangMauDichCheXuat();
            hmd.TKMD_ID = this.ID;
            this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
        }
       
   
        //-----------------------------------------------------------------------------------------
    
    


        #region DongBoDuLieuPhongKhai
        public void DongBoDuLieuKhaiDTByIDTKDK(ToKhaiMauDich TKMD, long IDTKOfHQ, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select * from HQQLHGCTEMPT.dbo.DHangMDDK where  SoTK=@SoTK and Ma_LH=@Ma_LH and Ma_HQ=@Ma_HQ and NamDK=@NamDK";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, TKMD.SoTiepNhan);
            db.AddInParameter(dbCommand, "@Ma_LH", SqlDbType.VarChar, TKMD.MaLoaiHinh);
            db.AddInParameter(dbCommand, "@Ma_HQ", SqlDbType.VarChar, TKMD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, TKMD.NgayDangKy.Year);

            DataSet dsHang = db.ExecuteDataSet(dbCommand);
            TKMD.HMDCollection = new List<HangMauDich>();
            foreach (DataRow row in dsHang.Tables[0].Rows)
            {
                HangMauDich hang = new HangMauDich();
                if (row["DGia_KB"].ToString() != "")
                    hang.DonGiaKB = Convert.ToDecimal(row["DGia_KB"].ToString());
                if (row["DGia_TT"].ToString() != "")
                    hang.DonGiaTT = Convert.ToDecimal(row["DGia_TT"].ToString());
                hang.DVT_ID = row["Ma_DVT"].ToString();
                hang.MaHS = row["Ma_HangKB"].ToString().Trim();
                hang.MaPhu = row["Ma_Phu"].ToString().Substring(1).Trim();
                TKMD.LoaiHangHoa = row["Ma_Phu"].ToString().Substring(0, 1);
                if (row["MienThue"].ToString() != "")
                    hang.MienThue = Convert.ToByte(row["MienThue"].ToString());
                hang.NuocXX_ID = row["Nuoc_XX"].ToString();
                if (row["Phu_Thu"].ToString() != "")
                    hang.PhuThu = Convert.ToDecimal(row["Phu_Thu"].ToString());
                if (row["Luong"].ToString() != "")
                    hang.SoLuong = Convert.ToDecimal(row["Luong"].ToString());
                hang.TenHang = row["Ten_Hang"].ToString();
                if (row["Thue_VAT"].ToString() != "")
                    hang.ThueGTGT = Convert.ToDecimal(row["Thue_VAT"].ToString());
                if (row["TS_VAT"].ToString() != "")
                    hang.ThueSuatGTGT = Convert.ToDecimal(row["TS_VAT"].ToString());
                if (row["TS_TTDB"].ToString() != "")
                    hang.ThueSuatTTDB = Convert.ToDecimal(row["TS_TTDB"].ToString());
                if (row["TS_XNK"].ToString() != "")
                    hang.ThueSuatXNK = Convert.ToDecimal(row["TS_XNK"].ToString());
                if (row["Thue_TTDB"].ToString() != "")
                    hang.ThueTTDB = Convert.ToDecimal(row["Thue_TTDB"].ToString());
                if (row["Thue_XNK"].ToString() != "")
                    hang.ThueXNK = Convert.ToDecimal(row["Thue_XNK"].ToString());
                if (row["TriGia_KB"].ToString() != "")
                    hang.TriGiaKB = Convert.ToDecimal(row["TriGia_KB"].ToString());
                if (row["TGKB_VND"].ToString() != "")
                    hang.TriGiaKB_VND = Convert.ToDecimal(row["TGKB_VND"].ToString());
                if (row["TriGia_ThuKhac"].ToString() != "")
                    hang.TriGiaThuKhac = Convert.ToDecimal(row["TriGia_ThuKhac"].ToString());
                if (row["TriGia_TT"].ToString() != "")
                    hang.TriGiaTT = Convert.ToDecimal(row["TriGia_TT"].ToString());
                if (row["TyLe_ThuKhac"].ToString() != "")
                    hang.TyLeThuKhac = Convert.ToDecimal(row["TyLe_ThuKhac"].ToString());
                TKMD.HMDCollection.Add(hang);
            }
           



        }
        public static void DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlDelete = "delete t_KDT_ToKhaiMauDich where MaDoanhNghiep=@DVGC and TrangThaiXuLy<>1 and MaHaiQuan=@Ma_HQHD";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.ExecuteNonQuery(dbCommandDelete);

            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select HQQLHGCTEMPT.dbo.DToKhaiMD.* from DToKhaiMD where Ma_DV=@DVGC and TrangThai<>2 and Ma_HQ=@Ma_HQHD";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.CuaKhau_ID = row["Ma_CK"].ToString().Trim();
                    TKMD.DiaDiemXepHang = row["CangNN"].ToString();
                    TKMD.DKGH_ID = row["Ma_GH"].ToString();
                    TKMD.GiayTo = row["GiayTo"].ToString();
                    TKMD.SoVanDon = row["Van_Don"].ToString();
                    TKMD.MaDoanhNghiep = MaDoanhNghiep;
                    TKMD.MaHaiQuan = MaHaiQuan;
                    TKMD.MaLoaiHinh = row["Ma_LH"].ToString();
                    TKMD.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"].ToString());
                    if (row["NgayDen"].ToString() != "")
                        TKMD.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                    if (row["Ngay_GP"].ToString() != "")
                        TKMD.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                    if (row["Ngay_HHGP"].ToString() != "")
                        TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                    if (row["Ngay_HHHD"].ToString() != "")
                        TKMD.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                    if (row["Ngay_HDTM"].ToString() != "")
                        TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM"].ToString());
                    if (row["Ngay_HD"].ToString() != "")
                        TKMD.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                    TKMD.NgayTiepNhan = Convert.ToDateTime(row["Ngay_DK"].ToString());
                    if (row["Ngay_VanDon"].ToString() != "")
                        TKMD.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                    TKMD.NguyenTe_ID = row["Ma_NT"].ToString();
                    if (TKMD.MaLoaiHinh.IndexOf("N") == 0)
                    {
                        TKMD.NuocNK_ID = row["Nuoc_NK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_XK"].ToString();
                    }
                    else
                    {
                        TKMD.NuocNK_ID = row["Nuoc_XK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_NK"].ToString();
                    }
                    if (row["Phi_BH"].ToString() != "")
                        TKMD.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                    if (row["Phi_VC"].ToString() != "")
                        TKMD.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                    TKMD.PTTT_ID = row["Ma_PTTT"].ToString();
                    TKMD.PTVT_ID = row["Ma_PTVT"].ToString();
                    if (row["So_Container"].ToString() != "")
                        TKMD.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                    if (row["So_container40"].ToString() != "")
                        TKMD.SoContainer40 = Convert.ToDecimal(row["So_container40"].ToString());
                    TKMD.SoGiayPhep = row["So_GP"].ToString();
                    TKMD.SoHieuPTVT = (row["Ten_PTVT"].ToString());
                    TKMD.SoHoaDonThuongMai = row["So_HDTM"].ToString();
                    TKMD.SoHopDong = row["So_HD"].ToString();
                    if (row["So_Kien"].ToString() != "")
                        TKMD.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                    if (row["So_PLTK"].ToString() != "")
                        TKMD.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                    TKMD.SoTiepNhan = Convert.ToInt64(row["SoTK"].ToString());
                    TKMD.TenChuHang = row["TenCH"].ToString();
                    TKMD.TrangThaiXuLy = 0;
                    if (row["Tr_Luong"].ToString() != "")
                        TKMD.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                    if (row["TyGia_VND"].ToString() != "")
                        TKMD.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                    if (row["TyGia_USD"].ToString() != "")
                        TKMD.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                    TKMD.TenDonViDoiTac = row["DV_DT"].ToString();
                    long idTK = Convert.ToInt64(row["TKID"].ToString());
                    //TKMD.DongBoDuLieuKhaiDTByIDTKDK(TKMD, idTK, nameConnectKDT);
                    TKMD.TrangThaiXuLy = 0;
                    TKMD.IDHopDong = (new Company.GC.BLL.KDT.GC.HopDong()).GetIDHopDongExit(TKMD.SoHopDong, MaHaiQuan, MaDoanhNghiep, TKMD.NgayHopDong);
                    try
                    {
                        TKMD.InsertUpdateFull();
                    }
                    catch { }
                }
            }

        }

        public static ToKhaiMauDich DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT, long SoTiepNhan, string MaLH,short NamDK)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlGet = "select id from  t_KDT_ToKhaiMauDich where MaDoanhNghiep=@DVGC and MaHaiQuan=@Ma_HQHD and SoTiepNhan=@SoTiepNhan and MaLoaiHinh=@MaLoaiHinh and year(NgayTiepNhan)=@NamDK";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlGet);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommandDelete, "@SoTiepNhan", SqlDbType.Int, SoTiepNhan);
            db.AddInParameter(dbCommandDelete, "@MaLoaiHinh", SqlDbType.VarChar, MaLH);
            db.AddInParameter(dbCommandDelete, "@NamDK", SqlDbType.SmallInt, NamDK);
            object o= db.ExecuteScalar(dbCommandDelete);
            if (o != null)
            {
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.ID = Convert.ToInt64(o);
                tkmd.Load();
                return tkmd;
            }

            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select HQQLHGCTEMPT.dbo.DToKhaiMD.* from DToKhaiMD where Ma_DV=@DVGC and Ma_HQ=@Ma_HQHD and  SoTK=@SoTK and Ma_LH=@Ma_LH and NamDK=@NamDK";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTiepNhan);
            db.AddInParameter(dbCommand, "@Ma_LH", SqlDbType.VarChar, MaLH);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
            
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.CuaKhau_ID = row["Ma_CK"].ToString().Trim();
                    TKMD.DiaDiemXepHang = row["CangNN"].ToString();
                    TKMD.DKGH_ID = row["Ma_GH"].ToString();
                    TKMD.GiayTo = row["GiayTo"].ToString();
                    TKMD.SoVanDon = row["Van_Don"].ToString();
                    TKMD.MaDoanhNghiep = MaDoanhNghiep;
                    TKMD.MaHaiQuan = MaHaiQuan;
                    TKMD.MaLoaiHinh = row["Ma_LH"].ToString();
                    TKMD.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"].ToString());
                    if (row["NgayDen"].ToString() != "")
                        TKMD.NgayDenPTVT = Convert.ToDateTime(row["NgayDen"].ToString());
                    if (row["Ngay_GP"].ToString() != "")
                        TKMD.NgayGiayPhep = Convert.ToDateTime(row["Ngay_GP"].ToString());
                    if (row["Ngay_HHGP"].ToString() != "")
                        TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(row["Ngay_HHGP"].ToString());
                    if (row["Ngay_HHHD"].ToString() != "")
                        TKMD.NgayHetHanHopDong = Convert.ToDateTime(row["Ngay_HHHD"].ToString());
                    if (row["Ngay_HDTM"].ToString() != "")
                        TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(row["Ngay_HDTM"].ToString());
                    if (row["Ngay_HD"].ToString() != "")
                        TKMD.NgayHopDong = Convert.ToDateTime(row["Ngay_HD"].ToString());
                    TKMD.NgayTiepNhan = Convert.ToDateTime(row["Ngay_DK"].ToString());
                    if (row["Ngay_VanDon"].ToString() != "")
                        TKMD.NgayVanDon = Convert.ToDateTime(row["Ngay_VanDon"].ToString());
                    TKMD.NguyenTe_ID = row["Ma_NT"].ToString();
                    if (TKMD.MaLoaiHinh.IndexOf("N") == 0)
                    {
                        TKMD.NuocNK_ID = row["Nuoc_NK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_XK"].ToString();
                    }
                    else
                    {
                        TKMD.NuocNK_ID = row["Nuoc_XK"].ToString();
                        TKMD.NuocXK_ID = row["Nuoc_NK"].ToString();
                    }
                    if (row["Phi_BH"].ToString() != "")
                        TKMD.PhiBaoHiem = Convert.ToDecimal(row["Phi_BH"].ToString());
                    if (row["Phi_VC"].ToString() != "")
                        TKMD.PhiVanChuyen = Convert.ToDecimal(row["Phi_VC"].ToString());
                    TKMD.PTTT_ID = row["Ma_PTTT"].ToString();
                    TKMD.PTVT_ID = row["Ma_PTVT"].ToString();
                    if (row["So_Container"].ToString() != "")
                        TKMD.SoContainer20 = Convert.ToDecimal(row["So_Container"].ToString());
                    if (row["So_container40"].ToString() != "")
                        TKMD.SoContainer40 = Convert.ToDecimal(row["So_container40"].ToString());
                    TKMD.SoGiayPhep = row["So_GP"].ToString();
                    TKMD.SoHieuPTVT = (row["Ten_PTVT"].ToString());
                    TKMD.SoHoaDonThuongMai = row["So_HDTM"].ToString();
                    TKMD.SoHopDong = row["So_HD"].ToString();
                    if (row["So_Kien"].ToString() != "")
                        TKMD.SoKien = Convert.ToDecimal(row["So_Kien"].ToString());
                    if (row["So_PLTK"].ToString() != "")
                        TKMD.SoLuongPLTK = Convert.ToInt16(row["So_PLTK"].ToString());
                    TKMD.SoTiepNhan = Convert.ToInt64(row["SoTK"].ToString());
                    TKMD.TenChuHang = row["TenCH"].ToString();
                    TKMD.TrangThaiXuLy = 0;
                    if (row["Tr_Luong"].ToString() != "")
                        TKMD.TrongLuong = Convert.ToDecimal(row["Tr_Luong"].ToString());
                    if (row["TyGia_VND"].ToString() != "")
                        TKMD.TyGiaTinhThue = Convert.ToDecimal(row["TyGia_VND"].ToString());
                    if (row["TyGia_USD"].ToString() != "")
                        TKMD.TyGiaUSD = Convert.ToDecimal(row["TyGia_USD"].ToString());
                    TKMD.TenDonViDoiTac = row["DV_DT"].ToString();
                    long idTK = Convert.ToInt64(row["TKID"].ToString());
                   // TKMD.DongBoDuLieuKhaiDTByIDTKDK(TKMD, idTK, nameConnectKDT);
                    return TKMD;
                }
            }
            return null;

        }

        #endregion DongBoDuLieuPhongKhai

    


        #region Webservice của hải quan

        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;

            return doc.InnerXml;
        }

     


        #region GC

        #region Khai báo tờ khai nhập

        public string WSSendXMLNHAPGC(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiNhap, (int)MessageFunctions.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXMLNHAPGC());
            docNPL.InnerXml = docNPL.InnerXml.Replace("dt=\"bin.base64\"", "dt:dt=\"bin.base64\"");
            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);            
           
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        private string ConvertCollectionToXMLNHAPGC()
        {
            //load du lieu
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                HangMauDichCheXuat hmd = new HangMauDichCheXuat();
                hmd.TKMD_ID = this.ID;
                this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
            }

            NumberFormatInfo f = new NumberFormatInfo();
            CultureInfo culture = new CultureInfo("vi-VN");
            if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            {
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";
            }

            XmlDocument docNPL = new XmlDocument();
            docNPL.Load("B03GiaCong\\khai bao to khai nhap.xml");

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/tokhai/MA_HQ");
            nodeHQNhan.InnerText = this.MaHaiQuan;

            XmlNode nodeSoTK = docNPL.SelectSingleNode("Root/tokhai/SoTK");
            nodeSoTK.InnerText = this.ID.ToString();

            XmlNode nodeMaLH = docNPL.SelectSingleNode("Root/tokhai/Ma_LH");
            nodeMaLH.InnerText = this.MaLoaiHinh;

            XmlNode nodeNamDK = docNPL.SelectSingleNode("Root/tokhai/NamDK");
            nodeNamDK.InnerText = DateTime.Today.Year.ToString();

            XmlNode nodeNgayDK = docNPL.SelectSingleNode("Root/tokhai/Ngay_DK");
            nodeNgayDK.InnerText = "";

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/tokhai/Ma_DV");
            nodeDN.InnerText = this.MaDoanhNghiep;

            XmlNode nodeDVUT = docNPL.SelectSingleNode("Root/tokhai/Ma_DVUT");
            nodeDVUT.InnerText = this.MaDonViUT;

            XmlNode nodeDVDoiTac = docNPL.SelectSingleNode("Root/tokhai/DV_TD");
            if (this.TenDonViDoiTac.Length <= 40)
                nodeDVDoiTac.InnerText = FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            else
                nodeDVDoiTac.InnerText = FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 40));
            XmlNode nodePTVT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTVT");
            nodePTVT.InnerText = this.PTVT_ID;

            XmlNode nodeSoHieuPTVT = docNPL.SelectSingleNode("Root/tokhai/Ten_PTVT");
            if (this.SoHieuPTVT.Length <= 20)
                nodeSoHieuPTVT.InnerText = FontConverter.Unicode2TCVN(this.SoHieuPTVT);
            else
                nodeSoHieuPTVT.InnerText = FontConverter.Unicode2TCVN(this.SoHieuPTVT.Substring(0, 20));
            XmlNode nodeNgayDen = docNPL.SelectSingleNode("Root/tokhai/NgayDen");
            if (this.NgayDenPTVT.Year > 1900)
                nodeNgayDen.InnerText = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeNgayDen.InnerText = "";

            XmlNode nodeVanDon = docNPL.SelectSingleNode("Root/tokhai/Van_Don");
            if (this.SoVanDon.Length <= 20)
                nodeVanDon.InnerText = FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeVanDon.InnerText = FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));

            XmlNode nodeCuaKhau = docNPL.SelectSingleNode("Root/tokhai/Ma_CK");
            nodeCuaKhau.InnerText = this.CuaKhau_ID;

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/tokhai/CangNN");
            if (this.DiaDiemXepHang.Length <= 40)
                nodeDiaDiemXepHang.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeDiaDiemXepHang.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));
            XmlNode nodeSoGiayPhep = docNPL.SelectSingleNode("Root/tokhai/So_GP");
            if (this.SoGiayPhep.Length <= 80)
                nodeSoGiayPhep.InnerText = FontConverter.Unicode2TCVN(this.SoGiayPhep);
            else
                nodeSoGiayPhep.InnerText = FontConverter.Unicode2TCVN(this.SoGiayPhep.Substring(0, 80));
            XmlNode nodeNgayGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_GP");
            if (NgayGiayPhep.Year > 1900)
                nodeNgayGiayPhep.InnerText = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayGiayPhep.InnerText = "";

            XmlNode nodeNgayHHGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHGP");
            if (NgayHetHanGiayPhep.Year > 1900)
                nodeNgayHHGiayPhep.InnerText = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayHHGiayPhep.InnerText = "";

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/tokhai/So_HD");
            if (this.SoHopDong.Length <= 40)
                nodeSoHopDong.InnerText = FontConverter.Unicode2TCVN(this.SoHopDong);
            else
                nodeSoHopDong.InnerText = FontConverter.Unicode2TCVN(this.SoHopDong.Substring(0, 40));
            XmlNode nodeTyGiaUSD = docNPL.SelectSingleNode("Root/tokhai/TyGia_USD");
            nodeTyGiaUSD.InnerText = this.TyGiaUSD.ToString(f);

            XmlNode nodeNgayHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HD");
            if (NgayHopDong.Year > 1900)
                nodeNgayHopDong.InnerText = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHopDong.InnerText = "";

            XmlNode nodeNgayHHHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHHD");
            if (NgayHetHanHopDong.Year > 1900)
                nodeNgayHHHopDong.InnerText = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHHHopDong.InnerText = "";

            XmlNode nodeNuocXK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_XK");
            nodeNuocXK.InnerText = this.NuocXK_ID;

            XmlNode nodeNuocNK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_NK");
            nodeNuocNK.InnerText = "VN";

            XmlNode nodeDieuKienGiaoHang = docNPL.SelectSingleNode("Root/tokhai/MA_GH");
            nodeDieuKienGiaoHang.InnerText = this.DKGH_ID;

            XmlNode nodeSoHang = docNPL.SelectSingleNode("Root/tokhai/SoHang");
            nodeSoHang.InnerText = this.HMDCollection.Count.ToString();

            XmlNode nodePTTT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTTT");
            nodePTTT.InnerText = this.PTTT_ID;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/tokhai/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;

            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/tokhai/TyGia_VND");
            nodeTyGiaVND.InnerText = this.TyGiaTinhThue.ToString(f);

            XmlNode nodeGiayTo = docNPL.SelectSingleNode("Root/tokhai/GiayTo");
            if (GiayTo.Length > 40)
                nodeGiayTo.InnerText = FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeGiayTo.InnerText = FontConverter.Unicode2TCVN(this.GiayTo);

            XmlNode nodeSoKien = docNPL.SelectSingleNode("Root/tokhai/So_Kien");
            nodeSoKien.InnerText = Convert.ToUInt32(this.SoKien).ToString();

            XmlNode nodeSoContainer20 = docNPL.SelectSingleNode("Root/tokhai/So_Container");
            nodeSoContainer20.InnerText = Convert.ToUInt64(this.SoContainer20).ToString();

            XmlNode nodeHDTM = docNPL.SelectSingleNode("Root/tokhai/So_HDTM");
            if (SoHoaDonThuongMai.Length <= 30)
                nodeHDTM.InnerText = FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai);
            else
                nodeHDTM.InnerText = FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai.Substring(0, 30));

            XmlNode nodeNgayHDTM = docNPL.SelectSingleNode("Root/tokhai/Ngay_HDTM");
            if (NgayHoaDonThuongMai.Year > 1900)
                nodeNgayHDTM.InnerText = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeNgayHDTM.InnerText = "";

            XmlNode nodeSoPLTK = docNPL.SelectSingleNode("Root/tokhai/So_PLTK");
            nodeSoPLTK.InnerText = this.SoLuongPLTK.ToString();

            XmlNode nodeChuHang = docNPL.SelectSingleNode("Root/tokhai/ChuHang");
            if (TenChuHang.Length > 30)
                nodeChuHang.InnerText = FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeChuHang.InnerText = FontConverter.Unicode2TCVN(this.TenChuHang);
            XmlNode nodeNgayVanDon = docNPL.SelectSingleNode("Root/tokhai/Ngay_VanDon");
            if (NgayVanDon.Year > 1900)
                nodeNgayVanDon.InnerText = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeNgayVanDon.InnerText = "";

            XmlNode nodeTrongLuong = docNPL.SelectSingleNode("Root/tokhai/TrangLuong");
            nodeTrongLuong.InnerText = this.TrongLuong.ToString(f);

            XmlNode nodeSoContainer40 = docNPL.SelectSingleNode("Root/tokhai/SoContainer40");
            nodeSoContainer40.InnerText = Convert.ToUInt64(this.SoContainer40).ToString();

            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/tokhai/CTKT");
            nodeCTKT.InnerText = "";

            XmlNode nodeHang = docNPL.SelectSingleNode("Root/hangTKs");
            foreach (HangMauDichCheXuat hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("hangTK");

                XmlNode HangDetails = docNPL.CreateElement("HangDetails");
                HangDetails.InnerText = "";
                node.AppendChild(HangDetails);

                XmlNode MaHSAtt = docNPL.CreateElement("MaHS");
                MaHSAtt.InnerText = hmd.MaHS;
                node.AppendChild(MaHSAtt);

                XmlNode maAtt = docNPL.CreateElement("Ma_Hang");
                maAtt.InnerText = this.LoaiHangHoa + hmd.MaPhu;
                node.AppendChild(maAtt);

                XmlNode TenAtt = docNPL.CreateElement("Ten_Hang");
                TenAtt.InnerText = FontConverter.Unicode2TCVN(hmd.TenHang);
                node.AppendChild(TenAtt);

                XmlNode NuocAtt = docNPL.CreateElement("Nuoc_XX");
                NuocAtt.InnerText = hmd.NuocXX_ID;
                node.AppendChild(NuocAtt);

                XmlNode Ma_DVTAtt = docNPL.CreateElement("Ma_DVT");
                Ma_DVTAtt.InnerText = hmd.DVT_ID;
                node.AppendChild(Ma_DVTAtt);

                XmlNode SoLuongAtt = docNPL.CreateElement("Luong");
                SoLuongAtt.InnerText = hmd.SoLuong.ToString(f);
                node.AppendChild(SoLuongAtt);

                XmlNode DonGiaAtt = docNPL.CreateElement("DGia_KB");
                DonGiaAtt.InnerText = hmd.DonGiaKB.ToString(f);
                node.AppendChild(DonGiaAtt);

                XmlNode TriGiaAtt = docNPL.CreateElement("TriGia_KB");
                TriGiaAtt.InnerText = hmd.TriGiaKB.ToString(f); ;
                node.AppendChild(TriGiaAtt);

                XmlNode TriGiaVNDAtt = docNPL.CreateElement("TGTTVND");
                TriGiaVNDAtt.InnerText = "0";// hmd.TriGiaTT.ToString(f); ;
                node.AppendChild(TriGiaVNDAtt);

                XmlNode DonGiaVNDAtt = docNPL.CreateElement("TGKBVND");
                DonGiaVNDAtt.InnerText = "0";//hmd.TriGiaKB_VND.ToString(f); ;
                node.AppendChild(DonGiaVNDAtt);

                XmlNode MienThue = docNPL.CreateElement("MienThue");
                MienThue.InnerText = "0";// hmd.MienThue.ToString(f);
                node.AppendChild(MienThue);

                XmlNode TS_XNKAtt = docNPL.CreateElement("TS_XNK");
                TS_XNKAtt.InnerText = "0";// hmd.ThueSuatXNK.ToString(f);
                node.AppendChild(TS_XNKAtt);

                XmlNode ThueXNKAtt = docNPL.CreateElement("Thue_XNK");
                ThueXNKAtt.InnerText = "0";//hmd.ThueXNK.ToString(f);
                node.AppendChild(ThueXNKAtt);

                XmlNode TS_VSTAtt = docNPL.CreateElement("TS_VAT");
                TS_VSTAtt.InnerText = "0";//hmd.ThueGTGT.ToString(f);
                node.AppendChild(TS_VSTAtt);

                XmlNode ThueVATAtt = docNPL.CreateElement("Thue_VAT");
                ThueVATAtt.InnerText = "0";// hmd.ThueGTGT.ToString(f);
                node.AppendChild(ThueVATAtt);

                XmlNode TL_PhuThuAtt = docNPL.CreateElement("TyLe_ThuKhac");
                TL_PhuThuAtt.InnerText = "0";//hmd.TyLeThuKhac.ToString(f);
                node.AppendChild(TL_PhuThuAtt);

                XmlNode TriGiaTK = docNPL.CreateElement("TriGia_ThuKhac");
                TriGiaTK.InnerText = "0";//hmd.TriGiaThuKhac.ToString(f);
                node.AppendChild(TriGiaTK);

                XmlNode sttAtt = docNPL.CreateElement("Index");
                sttAtt.InnerText = hmd.SoThuTuHang.ToString();
                node.AppendChild(sttAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }

        #endregion Khai báo tờ khai nhập

        #region Khai báo tờ khai xuất

        public string WSSendXMLXuatGC(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiXuat, (int)MessageFunctions.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXMLXUATGC());
            docNPL.InnerXml = docNPL.InnerXml.Replace("dt=\"bin.base64\"", "dt:dt=\"bin.base64\"");
            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        private string ConvertCollectionToXMLXUATGC()
        {
            if (this.HMDCollection == null || this.HMDCollection.Count == 0)
            {
                HangMauDichCheXuat hmd = new HangMauDichCheXuat();
                hmd.TKMD_ID = this.ID;
                this.HMDCollection = hmd.SelectCollectionBy_TKMD_ID();
            }

            NumberFormatInfo f = new NumberFormatInfo();
            CultureInfo culture = new CultureInfo("vi-VN");
            if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            {
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";
            }

            XmlDocument docNPL = new XmlDocument();
            docNPL.Load("B03GiaCong\\khai bao to khai xuat.xml");

            //thong tin hai quan nhan
            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/tokhai/MA_HQ");
            nodeHQNhan.InnerText = this.MaHaiQuan;

            XmlNode nodeSoTK = docNPL.SelectSingleNode("Root/tokhai/SoTK");
            nodeSoTK.InnerText = this.ID.ToString();

            XmlNode nodeMaLH = docNPL.SelectSingleNode("Root/tokhai/Ma_LH");
            nodeMaLH.InnerText = this.MaLoaiHinh;

            XmlNode nodeNamDK = docNPL.SelectSingleNode("Root/tokhai/NamDK");
            nodeNamDK.InnerText = DateTime.Today.Year.ToString();

            XmlNode nodeNgayDK = docNPL.SelectSingleNode("Root/tokhai/Ngay_DK");
            nodeNgayDK.InnerText = "";

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/tokhai/Ma_DV");
            nodeDN.InnerText = this.MaDoanhNghiep;

            XmlNode nodeDVUT = docNPL.SelectSingleNode("Root/tokhai/Ma_DVUT");
            nodeDVUT.InnerText = this.MaDonViUT;

            XmlNode nodeDVDoiTac = docNPL.SelectSingleNode("Root/tokhai/DV_TD");
            if (this.TenDonViDoiTac.Length <= 40)
                nodeDVDoiTac.InnerText = FontConverter.Unicode2TCVN(this.TenDonViDoiTac);
            else
                nodeDVDoiTac.InnerText = FontConverter.Unicode2TCVN(this.TenDonViDoiTac.Substring(0, 40));
            XmlNode nodePTVT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTVT");
            nodePTVT.InnerText = this.PTVT_ID;

            XmlNode nodeSoHieuPTVT = docNPL.SelectSingleNode("Root/tokhai/Ten_PTVT");
            if (this.SoHieuPTVT.Length <= 20)
                nodeSoHieuPTVT.InnerText = FontConverter.Unicode2TCVN(this.SoHieuPTVT);
            else
                nodeSoHieuPTVT.InnerText = FontConverter.Unicode2TCVN(this.SoHieuPTVT.Substring(0, 20));

            XmlNode nodeNgayDen = docNPL.SelectSingleNode("Root/tokhai/NgayDen");
            if (this.NgayDenPTVT.Year > 1900)
                nodeNgayDen.InnerText = this.NgayDenPTVT.ToString("MM/dd/yyyy");
            else
                nodeNgayDen.InnerText = "";

            XmlNode nodeVanDon = docNPL.SelectSingleNode("Root/tokhai/Van_Don");
            if (this.SoVanDon.Length <= 20)
                nodeVanDon.InnerText = FontConverter.Unicode2TCVN(this.SoVanDon);
            else
                nodeVanDon.InnerText = FontConverter.Unicode2TCVN(this.SoVanDon.Substring(0, 20));


            XmlNode nodeCuaKhau = docNPL.SelectSingleNode("Root/tokhai/Ma_CK");
            nodeCuaKhau.InnerText = this.CuaKhau_ID;

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/tokhai/CangNN");
            if (this.DiaDiemXepHang.Length <= 40)
                nodeDiaDiemXepHang.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
            else
                nodeDiaDiemXepHang.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));

            XmlNode nodeSoGiayPhep = docNPL.SelectSingleNode("Root/tokhai/So_GP");
            if (this.SoGiayPhep.Length <= 80)
                nodeSoGiayPhep.InnerText = FontConverter.Unicode2TCVN(this.SoGiayPhep);
            else
                nodeSoGiayPhep.InnerText = FontConverter.Unicode2TCVN(this.SoGiayPhep.Substring(0, 80));

            XmlNode nodeNgayGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_GP");
            if (NgayGiayPhep.Year > 1900)
                nodeNgayGiayPhep.InnerText = this.NgayGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayGiayPhep.InnerText = "";

            XmlNode nodeNgayHHGiayPhep = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHGP");
            if (NgayHetHanGiayPhep.Year > 1900)
                nodeNgayHHGiayPhep.InnerText = this.NgayHetHanGiayPhep.ToString("MM/dd/yyyy");
            else
                nodeNgayHHGiayPhep.InnerText = "";

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/tokhai/So_HD");
            if (SoHopDong.Length <= 40)
                nodeSoHopDong.InnerText = FontConverter.Unicode2TCVN(this.SoHopDong);
            else
                nodeSoHopDong.InnerText = FontConverter.Unicode2TCVN(this.SoHopDong.Substring(0, 40));
            XmlNode nodeTyGiaUSD = docNPL.SelectSingleNode("Root/tokhai/TyGia_USD");
            nodeTyGiaUSD.InnerText = this.TyGiaUSD.ToString(f);

            XmlNode nodeNgayHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HD");
            if (NgayHopDong.Year > 1900)
                nodeNgayHopDong.InnerText = this.NgayHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHopDong.InnerText = "";

            XmlNode nodeNgayHHHopDong = docNPL.SelectSingleNode("Root/tokhai/Ngay_HHHD");
            if (NgayHetHanHopDong.Year > 1900)
                nodeNgayHHHopDong.InnerText = this.NgayHetHanHopDong.ToString("MM/dd/yyyy");
            else
                nodeNgayHHHopDong.InnerText = "";

            XmlNode nodeNuocXK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_XK");
            nodeNuocXK.InnerText = this.NuocXK_ID;

            XmlNode nodeNuocNK = docNPL.SelectSingleNode("Root/tokhai/Nuoc_NK");
            nodeNuocNK.InnerText = "VN";

            XmlNode nodeDieuKienGiaoHang = docNPL.SelectSingleNode("Root/tokhai/MA_GH");
            nodeDieuKienGiaoHang.InnerText = this.DKGH_ID;

            XmlNode nodeSoHang = docNPL.SelectSingleNode("Root/tokhai/SoHang");
            nodeSoHang.InnerText = this.HMDCollection.Count.ToString();

            XmlNode nodePTTT = docNPL.SelectSingleNode("Root/tokhai/Ma_PTTT");
            nodePTTT.InnerText = this.PTTT_ID;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/tokhai/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;

            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/tokhai/TyGia_VND");
            nodeTyGiaVND.InnerText = this.TyGiaTinhThue.ToString(f);

            XmlNode nodeGiayTo = docNPL.SelectSingleNode("Root/tokhai/GiayTo");
            if (GiayTo.Length > 40)
                nodeGiayTo.InnerText = FontConverter.Unicode2TCVN(this.GiayTo.Substring(0, 40));
            else
                nodeGiayTo.InnerText = FontConverter.Unicode2TCVN(this.GiayTo);
            XmlNode nodeTenChuHang = docNPL.SelectSingleNode("Root/tokhai/TenCH");
            nodeTenChuHang.InnerText = "";
            XmlNode nodeSoKien = docNPL.SelectSingleNode("Root/tokhai/So_Kien");
            nodeSoKien.InnerText = Convert.ToUInt32(this.SoKien).ToString();

            XmlNode nodeSoContainer20 = docNPL.SelectSingleNode("Root/tokhai/So_Container");
            nodeSoContainer20.InnerText = Convert.ToUInt64(this.SoContainer20).ToString();

            XmlNode nodeHDTM = docNPL.SelectSingleNode("Root/tokhai/So_HDTM");
            if (SoHoaDonThuongMai.Length <= 30)
                nodeHDTM.InnerText = FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai);
            else
                nodeHDTM.InnerText = FontConverter.Unicode2TCVN(this.SoHoaDonThuongMai.Substring(0, 30));


            XmlNode nodeNgayHDTM = docNPL.SelectSingleNode("Root/tokhai/Ngay_HDTM");
            if (NgayHoaDonThuongMai.Year > 1900)
                nodeNgayHDTM.InnerText = this.NgayHoaDonThuongMai.ToString("MM/dd/yyyy");
            else
                nodeNgayHDTM.InnerText = "";

            XmlNode nodeSoPLTK = docNPL.SelectSingleNode("Root/tokhai/So_PLTK");
            nodeSoPLTK.InnerText = this.SoLuongPLTK.ToString();

            XmlNode nodeChuHang = docNPL.SelectSingleNode("Root/tokhai/ChuHang");
            if (TenChuHang.Length > 30)
                nodeChuHang.InnerText = FontConverter.Unicode2TCVN(this.TenChuHang.Substring(0, 30));
            else
                nodeChuHang.InnerText = FontConverter.Unicode2TCVN(this.TenChuHang);
            XmlNode nodeNgayVanDon = docNPL.SelectSingleNode("Root/tokhai/Ngay_VanDon");
            if (NgayVanDon.Year > 1900)
                nodeNgayVanDon.InnerText = this.NgayVanDon.ToString("MM/dd/yyyy");
            else
                nodeNgayVanDon.InnerText = "";

            XmlNode nodeTrongLuong = docNPL.SelectSingleNode("Root/tokhai/TrangLuong");
            nodeTrongLuong.InnerText = this.TrongLuong.ToString(f);

            XmlNode nodeSoContainer40 = docNPL.SelectSingleNode("Root/tokhai/SoContainer40");
            nodeSoContainer40.InnerText = Convert.ToUInt64(this.SoContainer40).ToString();

            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/tokhai/CTKT");
            nodeCTKT.InnerText = "";

            XmlNode nodeHang = docNPL.SelectSingleNode("Root/hangTKs");
            foreach (HangMauDichCheXuat hmd in this.HMDCollection)
            {
                XmlNode node = docNPL.CreateElement("hangTK");

                XmlNode HangDetails = docNPL.CreateElement("HangDetails");
                HangDetails.InnerText = "";
                node.AppendChild(HangDetails);

                XmlNode MaHSAtt = docNPL.CreateElement("MaHS");
                MaHSAtt.InnerText = hmd.MaHS;
                node.AppendChild(MaHSAtt);

                XmlNode maAtt = docNPL.CreateElement("Ma_Hang");
                maAtt.InnerText = this.LoaiHangHoa + hmd.MaPhu;
                node.AppendChild(maAtt);

                XmlNode TenAtt = docNPL.CreateElement("Ten_Hang");
                TenAtt.InnerText = FontConverter.Unicode2TCVN(hmd.TenHang);
                node.AppendChild(TenAtt);

                XmlNode NuocAtt = docNPL.CreateElement("Nuoc_XX");
                NuocAtt.InnerText = hmd.NuocXX_ID;
                node.AppendChild(NuocAtt);

                XmlNode Ma_DVTAtt = docNPL.CreateElement("Ma_DVT");
                Ma_DVTAtt.InnerText = hmd.DVT_ID;
                node.AppendChild(Ma_DVTAtt);

                XmlNode SoLuongAtt = docNPL.CreateElement("Luong");
                SoLuongAtt.InnerText = hmd.SoLuong.ToString(f);
                node.AppendChild(SoLuongAtt);

                XmlNode DonGiaAtt = docNPL.CreateElement("DGia_KB");
                DonGiaAtt.InnerText = hmd.DonGiaKB.ToString(f);
                node.AppendChild(DonGiaAtt);

                XmlNode TriGiaAtt = docNPL.CreateElement("TriGia_KB");
                TriGiaAtt.InnerText = hmd.TriGiaKB.ToString(f); ;
                node.AppendChild(TriGiaAtt);

                XmlNode TriGiaVNDAtt = docNPL.CreateElement("TGTTVND");
                TriGiaVNDAtt.InnerText = "0";// hmd.TriGiaTT.ToString(f); ;
                node.AppendChild(TriGiaVNDAtt);

                XmlNode DonGiaVNDAtt = docNPL.CreateElement("TGKBVND");
                DonGiaVNDAtt.InnerText = "0";//hmd.TriGiaKB_VND.ToString(f); ;
                node.AppendChild(DonGiaVNDAtt);

                XmlNode MienThue = docNPL.CreateElement("MienThue");
                MienThue.InnerText = "0";// hmd.MienThue.ToString(f);
                node.AppendChild(MienThue);

                XmlNode TS_XNKAtt = docNPL.CreateElement("TS_XNK");
                TS_XNKAtt.InnerText = "0";// hmd.ThueSuatXNK.ToString(f);
                node.AppendChild(TS_XNKAtt);

                XmlNode ThueXNKAtt = docNPL.CreateElement("Thue_XNK");
                ThueXNKAtt.InnerText = "0";//hmd.ThueXNK.ToString(f);
                node.AppendChild(ThueXNKAtt);

                XmlNode TS_VSTAtt = docNPL.CreateElement("TS_VAT");
                TS_VSTAtt.InnerText = "0";//hmd.ThueGTGT.ToString(f);
                node.AppendChild(TS_VSTAtt);

                XmlNode ThueVATAtt = docNPL.CreateElement("Thue_VAT");
                ThueVATAtt.InnerText = "0";// hmd.ThueGTGT.ToString(f);
                node.AppendChild(ThueVATAtt);

                XmlNode TL_PhuThuAtt = docNPL.CreateElement("TyLe_ThuKhac");
                TL_PhuThuAtt.InnerText = "0";//hmd.TyLeThuKhac.ToString(f);
                node.AppendChild(TL_PhuThuAtt);

                XmlNode TriGiaTK = docNPL.CreateElement("TriGia_ThuKhac");
                TriGiaTK.InnerText = "0";//hmd.TriGiaThuKhac.ToString(f);
                node.AppendChild(TriGiaTK);

                XmlNode sttAtt = docNPL.CreateElement("Index");
                sttAtt.InnerText = hmd.SoThuTuHang.ToString();
                node.AppendChild(sttAtt);

                nodeHang.AppendChild(node);
            }
            return docNPL.InnerXml;
        }


        #endregion Khai báo tờ khai xuất



        #endregion GC


        #region Huy khai báo tờ khai

        public string WSCancelXMLGC(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.StartsWith("N"))
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiNhap, (int)MessageFunctions.HuyKhaiBao));
            else
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiXuat, (int)MessageFunctions.HuyKhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.Load("B03GiaCong\\HuyKhaiBaoToKhai.XML");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";

        }



        #endregion Huy khai báo tờ khai

        #region Lấy phản hồi trạng thái

        public string WSRequestXMLGC(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.StartsWith("N"))
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiNhap, (int)MessageFunctions.HoiTrangThai));
            else
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiXuat, (int)MessageFunctions.HoiTrangThai));
            XmlDocument docNPL = new XmlDocument();
            docNPL.Load("B03GiaCong\\LayPhanHoiToKkhai.XML");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";

        }

        #endregion Lấy phản hồi

        public string LayPhanHoiGC(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            doc.GetElementsByTagName("function")[0].InnerText = "5";
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);
                
                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                    }
                    else
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));   
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            bool ok = false;
            if (function == (int)MessageFunctions.KhaiBao)
            {
                if (this.MaLoaiHinh.IndexOf("GC") > 0)
                {
                    this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SoTN"].Value);
                    this.NgayTiepNhan =DateTime.Today;
                }
                else
                {
                    this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan =DateTime.Today;
                }
                this.TrangThaiXuLy = 0;
                ok = true;
            }
            else if (function == (int)MessageFunctions.HuyKhaiBao)
            {
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.TrangThaiXuLy = -1;
                ok = true;
            }
            else if (function == (int)MessageFunctions.HoiTrangThai)
            {
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").InnerText == "2")
                {
                    this.TrangThaiXuLy = 1;
                    this.SoToKhai = Convert.ToInt32(docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").Attributes["SoTK"].Value);
                    CultureInfo culture = new CultureInfo("vi-VN");
                    this.NgayDangKy = Convert.ToDateTime(docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").Attributes["NgayDK"].Value, culture.DateTimeFormat);
                    ok = true;
                }
            }
            if (ok)
                this.Update();
            return "";

        }
        #endregion Webservice của hải quan
     
    }
}