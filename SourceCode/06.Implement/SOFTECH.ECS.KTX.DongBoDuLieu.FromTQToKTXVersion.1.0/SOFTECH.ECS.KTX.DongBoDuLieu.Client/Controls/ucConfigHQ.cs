using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DongBoDuLieu.BLL;
using System.Security.Cryptography;
namespace DongBo.Controls
{
    public partial class ucConfigHQ : ucDesign
    {
        frmMain frmParent = null;
        DataSet dsHQ = new DataSet();

        public void saveConfigHaiQuan()
        {
            GlobalSettings.saveConfigHQ(txtDiaChiHQDT.Text, cboxMaHQ.Text, txtTenTruyCap.Text, txtTenTruyCap.Text, txtMatKhau.Text, cboType.SelectedItem.ToString(), radSynFromHQ.Checked);
        }

        public ucConfigHQ()
        {
            InitializeComponent();
            frmParent = (frmMain)Application.OpenForms["frmMain"];
            this.DispHaiQuan();
        }

        private void txtDiaChiHQDT_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.DiaChiWSDongBo = txtDiaChiHQDT.Text;
            this.validControl();
        }

        private void txtTenTruyCap_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.TenTruyCap = txtTenTruyCap.Text;
            this.validControl();
        }

        private void txtMatKhau_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.MatKhau = txtMatKhau.Text.Trim(); // GetMD5Value(txtTenTruyCap.Text.Trim() + "+" + txtMatKhau.Text.Trim());
            this.validControl();
        }
        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }
        private void ucConfigHQ_Load(object sender, EventArgs e)
        {
            try
            {
                if (bool.Parse(GlobalSettings.ReadNodeXmlAppSettings("DN")) == true)
                {
                    radSynFromHQ.Visible = false;
                    radSynLocal.Checked = true;
                    txtDiaChiHQDT.Enabled = false;

                    GlobalSettings.radSynFromHQ = false;
                    GlobalSettings.radSynLocal = true;
                }

                txtDiaChiHQDT.Text = GlobalSettings.DiaChiWSDongBo;

                cboxMaHQ.Text = GlobalSettings.MaHaiQuan;
                GetTenHQ();

                txtTenTruyCap.Text = GlobalSettings.TenTruyCap;
                txtMatKhau.Text = GlobalSettings.MatKhau;

                cboType.SelectedIndex = (GlobalSettings.PhienBan == "KTX" ? 0 : 1);
                cboType.Enabled = !GlobalSettings.IsSynFromHQ;

                label1.Enabled = txtDiaChiHQDT.Enabled = GlobalSettings.IsSynFromHQ;

                radSynFromHQ.CheckedChanged -= new EventHandler(radSynFromHQ_CheckedChanged);
                radSynLocal.CheckedChanged -= new EventHandler(radSynLocal_CheckedChanged);

                radSynFromHQ.Checked = GlobalSettings.IsSynFromHQ;
                radSynLocal.Checked = !GlobalSettings.IsSynFromHQ;

                radSynFromHQ.CheckedChanged += new EventHandler(radSynFromHQ_CheckedChanged);
                radSynLocal.CheckedChanged += new EventHandler(radSynLocal_CheckedChanged);

                this.validControl();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void DispHaiQuan()
        {
            dsHQ.Clear();
            dsHQ.ReadXml("HaiQuan.xml");
            cboxMaHQ.DataSource = dsHQ.Tables["DonViHaiQuan"];
            cboxMaHQ.ValueMember = "Ten";
            cboxMaHQ.DisplayMember = "ID";
        }

        private void validControl()
        {
            if (txtDiaChiHQDT.Text.Trim() != "" && txtTenTruyCap.Text.Trim() != ""
                && txtMatKhau.Text.Trim() != "")
            {
                frmParent.btnNext.Enabled = true;
            }
            else
            {
                if (!GlobalSettings.IsSynFromHQ)
                    frmParent.btnNext.Enabled = true;
                else
                    frmParent.btnNext.Enabled = false;
            }
        }

        private void cboxMaHQ_SelectedValueChanged(object sender, EventArgs e)
        {
            txtTenHQ.Text = cboxMaHQ.SelectedValue.ToString();
            if (cboxMaHQ.Text != "System.Data.DataRowView")
            {
                GlobalSettings.MaHaiQuan = cboxMaHQ.Text;
            }
        }

        private void saveConfigHQ1()
        {
            this.Cursor = Cursors.WaitCursor;
            //Properties.Settings.Default.MaHaiQuan = frmParent.MaHaiQuan;
            //Properties.Settings.Default.WSDongBo = frmParent.DiaChiWSDongBo;
            //WebServiceDB.SetWSHaiQuan(frmParent.DiaChiWSDongBo);
            //Properties.Settings.Default.TenTruyCap = frmParent.TenTruyCap;
            //Properties.Settings.Default.MatKhau = frmParent.MatKhau;
            //Properties.Settings.Default.Save();

            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(config.FilePath);
            doc.SelectSingleNode("//appSettings//add[@key='WSDongBo']").SelectSingleNode("@value").Value = GlobalSettings.DiaChiWSDongBo;
            doc.SelectSingleNode("//appSettings//add[@key='MaHaiQuan']").SelectSingleNode("@value").Value = GlobalSettings.MaHaiQuan;
            doc.SelectSingleNode("//appSettings//add[@key='MaDoanhNghiep']").SelectSingleNode("@value").Value = GlobalSettings.MaDoanhNghiep;
            doc.SelectSingleNode("//appSettings//add[@key='TenTruyCap']").SelectSingleNode("@value").Value = GlobalSettings.TenTruyCap;
            doc.SelectSingleNode("//appSettings//add[@key='MatKhau']").SelectSingleNode("@value").Value = GlobalSettings.MatKhau;
            doc.Save(config.FilePath);

            this.Cursor = Cursors.Default;
        }
        private void btnChangePass_Click(object sender, EventArgs e)
        {
            saveConfigHQ1();
            DongBo.ChangePassForm dbForm = new ChangePassForm();
            dbForm.maHQ = GlobalSettings.MaHaiQuan;
            dbForm.maDN = GlobalSettings.MaDoanhNghiep;
            dbForm.matKhau = GlobalSettings.MatKhau;
            dbForm.ShowDialog();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveConfigHaiQuan();
        }

        private void cboxMaHQ_Leave(object sender, EventArgs e)
        {
            GetTenHQ();
        }

        private void GetTenHQ()
        {
            cboxMaHQ.Text = cboxMaHQ.Text.Trim().ToUpper();
            DataRow[] rows = dsHQ.Tables["DonViHaiQuan"].Select("ID = '" + cboxMaHQ.Text + "'");
            if (rows.Length > 0)
                txtTenHQ.Text = rows[0]["Ten"].ToString();
            if (cboxMaHQ.Text != "System.Data.DataRowView")
            {
                GlobalSettings.MaHaiQuan = cboxMaHQ.Text;
            }
        }

        private void radSynFromHQ_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.radSynFromHQ = radSynFromHQ.Checked;
            GlobalSettings.radSynLocal = radSynLocal.Checked;

            GlobalSettings.IsSynFromHQ = radSynFromHQ.Checked;

            label1.Enabled = txtDiaChiHQDT.Enabled = GlobalSettings.IsSynFromHQ;

            if (GlobalSettings.PhienBan == "KTX")
            {
                cboType.SelectedIndex = 0;
            }
            else
            {
                cboType.SelectedIndex = 1;
            }

            cboType.Enabled = !GlobalSettings.IsSynFromHQ;
        }

        private void radSynLocal_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.radSynFromHQ = radSynFromHQ.Checked;
            GlobalSettings.radSynLocal = radSynLocal.Checked;
            GlobalSettings.IsSynFromHQ = radSynFromHQ.Checked;

            label1.Enabled = txtDiaChiHQDT.Enabled = GlobalSettings.IsSynFromHQ;

            cboType.Enabled = !GlobalSettings.IsSynFromHQ;
        }

    }
}
