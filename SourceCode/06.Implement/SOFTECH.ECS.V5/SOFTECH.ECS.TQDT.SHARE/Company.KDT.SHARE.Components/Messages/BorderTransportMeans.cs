﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class BorderTransportMeans
    {
        /// <summary>
        /// Số hiệu PTVT
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; } 
        /// <summary> 
        /// Tên PTVT an..35
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }
        /// <summary>
        /// Số hiệu chuyến đi
        /// </summary>
        [XmlElement("journey")]
        public string Journey { get; set; }
        /// <summary>
        /// Kiểu PTVT
        /// </summary>
        [XmlElement("modeAndType")]
        public string ModeAndType { get; set; }
        /// <summary>
        /// Ngày khởi hành YYYY-MM-DD
        /// </summary>
        [XmlElement("departure")]
        public string Departure { get; set; }
        /// <summary>
        /// Quốc tịch PTVT a2
        /// </summary>
        [XmlElement("registrationNationality")]
        public string RegistrationNationality { get; set; }

    }
}
