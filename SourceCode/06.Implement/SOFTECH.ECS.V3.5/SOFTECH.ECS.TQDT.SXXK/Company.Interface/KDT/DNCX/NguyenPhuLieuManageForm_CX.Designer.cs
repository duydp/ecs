﻿using System.ComponentModel;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;

namespace Company.Interface.KDT.SXXK
{
    partial class NguyenPhuLieuManageForm_CX
    {
        private UIGroupBox uiGroupBox1;
        private ImageList ImageList1;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NguyenPhuLieuManageForm_CX));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label5 = new System.Windows.Forms.Label();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbtimkiem = new Janus.Windows.EditControls.UIGroupBox();
            this.clcTuNgay = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ucCalendar1 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ckbTimKiem = new Janus.Windows.EditControls.UICheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbStatus = new Janus.Windows.EditControls.UIComboBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.txtNamTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.donViHaiQuanNewControl1 = new Company.Interface.Controls.DonViHaiQuanNewControl();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.uiContextMenu1 = new Janus.Windows.UI.CommandBars.UIContextMenu();
            this.cmdUpdateStatus1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateStatus");
            this.cmdCSDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.InPhieuTN2 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdMessage2 = new Janus.Windows.UI.CommandBars.UICommand("cmdMessage");
            this.cmdExportExcel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdXoa2 = new Janus.Windows.UI.CommandBars.UICommand("cmdXoa");
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdCSDaDuyet2 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdMessage1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMessage");
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdXoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXoa");
            this.cmdSingleDownload = new Janus.Windows.UI.CommandBars.UICommand("cmdSingleDownload");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.LaySoTiepNhan = new Janus.Windows.UI.CommandBars.UICommand("LaySoTiepNhan");
            this.DongBo = new Janus.Windows.UI.CommandBars.UICommand("DongBo");
            this.Export1 = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.uiCommand2 = new Janus.Windows.UI.CommandBars.UICommand("Import");
            this.Export = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.uiCommand1 = new Janus.Windows.UI.CommandBars.UICommand("Import");
            this.cmdXoa = new Janus.Windows.UI.CommandBars.UICommand("cmdXoa");
            this.cmdCSDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdXuatNPLChoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPLChoPhongKhai");
            this.InPhieuTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdMessage = new Janus.Windows.UI.CommandBars.UICommand("cmdMessage");
            this.cmdExportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdUpdateStatus = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateStatus");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdCancel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.cmdSend3 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).BeginInit();
            this.grbtimkiem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiContextMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 34);
            this.grbMain.Size = new System.Drawing.Size(1128, 707);
            // 
            // vsmMain
            // 
            this.vsmMain.DefaultColorScheme = "Office2003";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 34);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1128, 707);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowCardSizing = false;
            this.dgList.AllowColumnDrag = false;
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.AutomaticSort = false;
            this.dgList.ColumnAutoResize = true;
            this.cmMain.SetContextMenu(this.dgList, this.uiContextMenu1);
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 132);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1128, 533);
            this.dgList.TabIndex = 17;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.SelectionChanged += new System.EventHandler(this.dgList_SelectionChanged);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecord);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "report.png");
            this.ImageList1.Images.SetKeyName(5, "printer.png");
            this.ImageList1.Images.SetKeyName(6, "page_edit.png");
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.btnDelete);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 665);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1128, 42);
            this.uiGroupBox3.TabIndex = 16;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1041, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(12, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(176, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Hướng dẫn: Kích đôi để xem chi tiết";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDelete.Location = new System.Drawing.Point(965, 13);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 23);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtMaNPL);
            this.uiGroupBox2.Controls.Add(this.grbtimkiem);
            this.uiGroupBox2.Controls.Add(this.cbStatus);
            this.uiGroupBox2.Controls.Add(this.btnSearch);
            this.uiGroupBox2.Controls.Add(this.txtNamTiepNhan);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.donViHaiQuanNewControl1);
            this.uiGroupBox2.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1128, 132);
            this.uiGroupBox2.TabIndex = 15;
            this.uiGroupBox2.Text = "Thông tin tìm kiếm";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Mã nguyên phụ liệu";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNPL.Location = new System.Drawing.Point(119, 85);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(203, 21);
            this.txtMaNPL.TabIndex = 32;
            this.txtMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNPL.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // grbtimkiem
            // 
            this.grbtimkiem.Controls.Add(this.clcTuNgay);
            this.grbtimkiem.Controls.Add(this.ucCalendar1);
            this.grbtimkiem.Controls.Add(this.ckbTimKiem);
            this.grbtimkiem.Controls.Add(this.label9);
            this.grbtimkiem.Controls.Add(this.label7);
            this.grbtimkiem.Enabled = false;
            this.grbtimkiem.Location = new System.Drawing.Point(593, 30);
            this.grbtimkiem.Name = "grbtimkiem";
            this.grbtimkiem.Size = new System.Drawing.Size(200, 76);
            this.grbtimkiem.TabIndex = 29;
            this.grbtimkiem.VisualStyleManager = this.vsmMain;
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.Location = new System.Drawing.Point(87, 15);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.ReadOnly = false;
            this.clcTuNgay.Size = new System.Drawing.Size(90, 21);
            this.clcTuNgay.TabIndex = 19;
            this.clcTuNgay.TagName = "";
            this.clcTuNgay.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcTuNgay.WhereCondition = "";
            // 
            // ucCalendar1
            // 
            this.ucCalendar1.Location = new System.Drawing.Point(87, 45);
            this.ucCalendar1.Name = "ucCalendar1";
            this.ucCalendar1.ReadOnly = false;
            this.ucCalendar1.Size = new System.Drawing.Size(90, 21);
            this.ucCalendar1.TabIndex = 19;
            this.ucCalendar1.TagName = "";
            this.ucCalendar1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ucCalendar1.WhereCondition = "";
            // 
            // ckbTimKiem
            // 
            this.ckbTimKiem.Location = new System.Drawing.Point(3, 0);
            this.ckbTimKiem.Margin = new System.Windows.Forms.Padding(0);
            this.ckbTimKiem.Name = "ckbTimKiem";
            this.ckbTimKiem.Size = new System.Drawing.Size(15, 15);
            this.ckbTimKiem.TabIndex = 30;
            this.ckbTimKiem.VisualStyleManager = this.vsmMain;
            this.ckbTimKiem.CheckedChanged += new System.EventHandler(this.ckbTimKiem_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(31, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Từ ngày";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Đến ngày";
            // 
            // cbStatus
            // 
            this.cbStatus.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this.cbStatus, "");
            this.helpProvider1.SetHelpNavigator(this.cbStatus, System.Windows.Forms.HelpNavigator.Topic);
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Chưa khai báo";
            uiComboBoxItem1.Value = -1;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Đã khai báo";
            uiComboBoxItem2.Value = "-3";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Chờ duyệt";
            uiComboBoxItem3.Value = 0;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Đã duyệt";
            uiComboBoxItem4.Value = 1;
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Không phê duyệt";
            uiComboBoxItem5.Value = "2";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Đã Hủy";
            uiComboBoxItem6.Value = ((short)(10));
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Đã khai báo sửa";
            uiComboBoxItem7.Value = ((short)(4));
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Đang sửa";
            uiComboBoxItem8.Value = ((short)(5));
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Đã khai báo hủy";
            uiComboBoxItem9.Value = "-4";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Đang hủy";
            uiComboBoxItem10.Value = "-5";
            this.cbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10});
            this.cbStatus.Location = new System.Drawing.Point(396, 85);
            this.cbStatus.Name = "cbStatus";
            this.helpProvider1.SetShowHelp(this.cbStatus, true);
            this.cbStatus.Size = new System.Drawing.Size(182, 21);
            this.cbStatus.TabIndex = 28;
            this.cbStatus.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbStatus.SelectedIndexChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(799, 83);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(96, 23);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtNamTiepNhan
            // 
            this.txtNamTiepNhan.DecimalDigits = 0;
            this.txtNamTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamTiepNhan.FormatString = "####";
            this.txtNamTiepNhan.Location = new System.Drawing.Point(339, 50);
            this.txtNamTiepNhan.MaxLength = 4;
            this.txtNamTiepNhan.Name = "txtNamTiepNhan";
            this.txtNamTiepNhan.Size = new System.Drawing.Size(92, 21);
            this.txtNamTiepNhan.TabIndex = 5;
            this.txtNamTiepNhan.Text = "2018";
            this.txtNamTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNamTiepNhan.Value = ((short)(2018));
            this.txtNamTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtNamTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNamTiepNhan.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Số tiếp nhận";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(257, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Năm tiếp nhận";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hải quan tiếp nhận";
            // 
            // donViHaiQuanNewControl1
            // 
            this.donViHaiQuanNewControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanNewControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanNewControl1.Location = new System.Drawing.Point(119, 20);
            this.donViHaiQuanNewControl1.Ma = "";
            this.donViHaiQuanNewControl1.MaCuc = "";
            this.donViHaiQuanNewControl1.Name = "donViHaiQuanNewControl1";
            this.donViHaiQuanNewControl1.ReadOnly = true;
            this.donViHaiQuanNewControl1.Size = new System.Drawing.Size(459, 22);
            this.donViHaiQuanNewControl1.TabIndex = 1;
            this.donViHaiQuanNewControl1.Ten = "";
            this.donViHaiQuanNewControl1.VisualStyleManager = null;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "#####";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(119, 50);
            this.txtSoTiepNhan.MaxLength = 12;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(121, 21);
            this.txtSoTiepNhan.TabIndex = 3;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(334, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Trạng thái";
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSingleDownload,
            this.cmdSend,
            this.cmdCancel,
            this.LaySoTiepNhan,
            this.DongBo,
            this.Export,
            this.uiCommand1,
            this.cmdXoa,
            this.cmdCSDaDuyet,
            this.cmdXuatNPLChoPhongKhai,
            this.InPhieuTN,
            this.cmdMessage,
            this.cmdExportExcel,
            this.cmdUpdateStatus});
            this.cmMain.ContainerControl = this;
            this.cmMain.ContextMenus.AddRange(new Janus.Windows.UI.CommandBars.UIContextMenu[] {
            this.uiContextMenu1});
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.ShowShortcutInToolTips = true;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // uiContextMenu1
            // 
            this.uiContextMenu1.CommandManager = this.cmMain;
            this.uiContextMenu1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdUpdateStatus1,
            this.cmdCSDaDuyet1,
            this.InPhieuTN2,
            this.cmdMessage2,
            this.cmdExportExcel2,
            this.cmdXoa2});
            this.uiContextMenu1.Key = "ContextMenu1";
            // 
            // cmdUpdateStatus1
            // 
            this.cmdUpdateStatus1.Key = "cmdUpdateStatus";
            this.cmdUpdateStatus1.Name = "cmdUpdateStatus1";
            // 
            // cmdCSDaDuyet1
            // 
            this.cmdCSDaDuyet1.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet1.Name = "cmdCSDaDuyet1";
            // 
            // InPhieuTN2
            // 
            this.InPhieuTN2.Image = ((System.Drawing.Image)(resources.GetObject("InPhieuTN2.Image")));
            this.InPhieuTN2.Key = "InPhieuTN";
            this.InPhieuTN2.Name = "InPhieuTN2";
            // 
            // cmdMessage2
            // 
            this.cmdMessage2.Key = "cmdMessage";
            this.cmdMessage2.Name = "cmdMessage2";
            // 
            // cmdExportExcel2
            // 
            this.cmdExportExcel2.Key = "cmdExportExcel";
            this.cmdExportExcel2.Name = "cmdExportExcel2";
            // 
            // cmdXoa2
            // 
            this.cmdXoa2.Key = "cmdXoa";
            this.cmdXoa2.Name = "cmdXoa2";
            this.cmdXoa2.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCSDaDuyet2,
            this.InPhieuTN1,
            this.cmdMessage1,
            this.cmdExportExcel1,
            this.cmdXoa1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.MergeRowOrder = 1;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1128, 34);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.Wrappable = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // cmdCSDaDuyet2
            // 
            this.cmdCSDaDuyet2.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet2.Name = "cmdCSDaDuyet2";
            this.cmdCSDaDuyet2.ToolTipText = "Ctrl + T";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // cmdMessage1
            // 
            this.cmdMessage1.Key = "cmdMessage";
            this.cmdMessage1.Name = "cmdMessage1";
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Key = "cmdExportExcel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdXoa1
            // 
            this.cmdXoa1.Key = "cmdXoa";
            this.cmdXoa1.Name = "cmdXoa1";
            // 
            // cmdSingleDownload
            // 
            this.cmdSingleDownload.Image = ((System.Drawing.Image)(resources.GetObject("cmdSingleDownload.Image")));
            this.cmdSingleDownload.Key = "cmdSingleDownload";
            this.cmdSingleDownload.Name = "cmdSingleDownload";
            this.cmdSingleDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdSingleDownload.Text = "Nhận dữ liệu";
            this.cmdSingleDownload.ToolTipText = "Nhận dữ liệu chứng từ đang chọn (Ctrl + D)";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSend.Text = "Khai báo";
            this.cmdSend.ToolTipText = "Khai báo (Ctrl + S)";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Image = ((System.Drawing.Image)(resources.GetObject("cmdCancel.Image")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Shortcut = System.Windows.Forms.Shortcut.CtrlC;
            this.cmdCancel.Text = "Hủy khai báo";
            this.cmdCancel.ToolTipText = "Hủy khai báo (Ctrl + C)";
            // 
            // LaySoTiepNhan
            // 
            this.LaySoTiepNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("LaySoTiepNhan.Icon")));
            this.LaySoTiepNhan.Key = "LaySoTiepNhan";
            this.LaySoTiepNhan.Name = "LaySoTiepNhan";
            this.LaySoTiepNhan.Text = "Xác nhận khai báo";
            // 
            // DongBo
            // 
            this.DongBo.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Export1,
            this.uiCommand2});
            this.DongBo.Key = "DongBo";
            this.DongBo.Name = "DongBo";
            this.DongBo.Text = "Đồng bộ dữ liệu với phòng khai";
            // 
            // Export1
            // 
            this.Export1.Icon = ((System.Drawing.Icon)(resources.GetObject("Export1.Icon")));
            this.Export1.Key = "Export";
            this.Export1.Name = "Export1";
            // 
            // uiCommand2
            // 
            this.uiCommand2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiCommand2.Icon")));
            this.uiCommand2.Key = "Import";
            this.uiCommand2.Name = "uiCommand2";
            // 
            // Export
            // 
            this.Export.Key = "Export";
            this.Export.Name = "Export";
            this.Export.Text = "Export dữ liệu";
            // 
            // uiCommand1
            // 
            this.uiCommand1.Key = "Import";
            this.uiCommand1.Name = "uiCommand1";
            this.uiCommand1.Text = "Import dữ liệu";
            // 
            // cmdXoa
            // 
            this.cmdXoa.Image = ((System.Drawing.Image)(resources.GetObject("cmdXoa.Image")));
            this.cmdXoa.Key = "cmdXoa";
            this.cmdXoa.Name = "cmdXoa";
            this.cmdXoa.Text = "Xóa NPL";
            this.cmdXoa.ToolTipText = "Xóa NPL chưa khai báo";
            // 
            // cmdCSDaDuyet
            // 
            this.cmdCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet.Image")));
            this.cmdCSDaDuyet.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Name = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdCSDaDuyet.Text = "Chuyển Trạng Thái";
            this.cmdCSDaDuyet.ToolTipText = "Chuyển sang trạng thái đã duyệt";
            // 
            // cmdXuatNPLChoPhongKhai
            // 
            this.cmdXuatNPLChoPhongKhai.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatNPLChoPhongKhai.Image")));
            this.cmdXuatNPLChoPhongKhai.Key = "cmdXuatNPLChoPhongKhai";
            this.cmdXuatNPLChoPhongKhai.Name = "cmdXuatNPLChoPhongKhai";
            this.cmdXuatNPLChoPhongKhai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // InPhieuTN
            // 
            this.InPhieuTN.Image = ((System.Drawing.Image)(resources.GetObject("InPhieuTN.Image")));
            this.InPhieuTN.Key = "InPhieuTN";
            this.InPhieuTN.Name = "InPhieuTN";
            this.InPhieuTN.Text = "In phiếu tiếp nhận";
            // 
            // cmdMessage
            // 
            this.cmdMessage.Image = ((System.Drawing.Image)(resources.GetObject("cmdMessage.Image")));
            this.cmdMessage.IsEditableControl = Janus.Windows.UI.InheritableBoolean.True;
            this.cmdMessage.Key = "cmdMessage";
            this.cmdMessage.Name = "cmdMessage";
            this.cmdMessage.Shortcut = System.Windows.Forms.Shortcut.CtrlM;
            this.cmdMessage.Text = "Kết quả xử lý";
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportExcel.Image")));
            this.cmdExportExcel.Key = "cmdExportExcel";
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Text = "Xuất Excel";
            // 
            // cmdUpdateStatus
            // 
            this.cmdUpdateStatus.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateStatus.Image")));
            this.cmdUpdateStatus.Key = "cmdUpdateStatus";
            this.cmdUpdateStatus.Name = "cmdUpdateStatus";
            this.cmdUpdateStatus.Text = "Cập nhật trạng thái";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1128, 34);
            // 
            // cmdCancel1
            // 
            this.cmdCancel1.Key = "cmdCancel";
            this.cmdCancel1.Name = "cmdCancel1";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "ECS files|*.ECS";
            this.saveFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // cmdSend3
            // 
            this.cmdSend3.Key = "cmdSend";
            this.cmdSend3.Name = "cmdSend3";
            // 
            // NguyenPhuLieuManageForm_CX
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(1128, 741);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "NguyenPhuLieuManageForm_CX";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "NguyenPhuLieuManageForm";
            this.Text = "Theo dõi danh sách nguyên phụ liệu khai báo";
            this.Load += new System.EventHandler(this.NguyenPhuLieuManageForm_Load);
            this.Shown += new System.EventHandler(this.NguyenPhuLieuManageForm_Shown);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).EndInit();
            this.grbtimkiem.ResumeLayout(false);
            this.grbtimkiem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiContextMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Label label2;
        private Label label1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private Label label3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamTiepNhan;
        private UIButton btnSearch;
        private Label label4;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Label label5;
        private Janus.Windows.UI.CommandBars.UICommand cmdSingleDownload;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel1;
        private Janus.Windows.UI.CommandBars.UIContextMenu uiContextMenu1;
        private Janus.Windows.UI.CommandBars.UICommand LaySoTiepNhan;
        private SaveFileDialog saveFileDialog1;
        private OpenFileDialog openFileDialog1;
        private Janus.Windows.UI.CommandBars.UICommand DongBo;
        private Janus.Windows.UI.CommandBars.UICommand Export1;
        private Janus.Windows.UI.CommandBars.UICommand uiCommand2;
        private Janus.Windows.UI.CommandBars.UICommand Export;
        private Janus.Windows.UI.CommandBars.UICommand uiCommand1;
        private UIButton btnClose;
        private Company.Interface.Controls.DonViHaiQuanNewControl donViHaiQuanNewControl1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXoa;
        private Janus.Windows.UI.CommandBars.UICommand cmdXoa2;
        private UIButton btnDelete;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet2;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatNPLChoPhongKhai;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN1;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN2;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend3;
        private Janus.Windows.UI.CommandBars.UICommand cmdMessage;
        private UIGroupBox uiGroupBox2;
        private UIGroupBox uiGroupBox3;
        private GridEX dgList;
        private Janus.Windows.UI.CommandBars.UICommand cmdMessage1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXoa1;
        private Janus.Windows.UI.CommandBars.UICommand cmdMessage2;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel2;
        private UIComboBox cbStatus;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateStatus;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateStatus1;
        private UICheckBox ckbTimKiem;
        private UIGroupBox grbtimkiem;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcTuNgay;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar ucCalendar1;
        private Label label9;
        private Label label7;
        private Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
    }
}
