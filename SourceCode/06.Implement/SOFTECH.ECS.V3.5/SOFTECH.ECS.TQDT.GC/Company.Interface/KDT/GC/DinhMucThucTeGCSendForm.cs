﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.Report.GC.TT39;

namespace Company.Interface.KDT.GC
{
    public partial class DinhMucThucTeGCSendForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_GC_DinhMucThucTeDangKy dinhMucThucTeDangKy = new KDT_GC_DinhMucThucTeDangKy();
        public KDT_GC_DinhMucThucTe_SP DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
        public KDT_GC_DinhMucThucTe_DinhMuc DinhMucThucTeDinhMuc = new KDT_GC_DinhMucThucTe_DinhMuc();
        public HopDong HD; 
        public bool isAdd = true;
        public bool isExits = false;
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public bool isEdit = false;
        public bool isCancel = false;

        public DinhMucThucTeGCSendForm()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinhNPL.DataSource = this._DonViTinh;
            txtDonViTinhNPL.DisplayMember = "Ten";
            txtDonViTinhNPL.ValueMember = "ID";
            txtDonViTinhNPL.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
        }

        private void LenhSanXuatGCForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (dinhMucThucTeDangKy.ID == 0)
                {
                    cbbLenhSanXuat.DataSource = KDT_LenhSanXuat.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' AND HopDong_ID = " + HD.ID + "", "ID");
                    cbbLenhSanXuat.DisplayMember = "SoLenhSanXuat";
                    cbbLenhSanXuat.ValueMember = "ID";
                    dinhMucThucTeDangKy.GuidString = Guid.NewGuid().ToString();
                    txtGuidString.Text = dinhMucThucTeDangKy.GuidString.ToString().ToUpper();
                    if (isEdit)
                    {
                        dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else if(isCancel)
                    {
                        dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    else
                    {
                        dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    dinhMucThucTeDangKy.HopDong_ID = HD.ID;
                }
                else
                {

                    Set();
                    HD = HopDong.Load(dinhMucThucTeDangKy.HopDong_ID);
                    cbbLenhSanXuat.DataSource = KDT_LenhSanXuat.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' AND HopDong_ID = " + HD.ID + "", "ID");
                    cbbLenhSanXuat.DisplayMember = "SoLenhSanXuat";
                    cbbLenhSanXuat.ValueMember = "ID";
                    dinhMucThucTeDangKy.SPCollection = KDT_GC_DinhMucThucTe_SP.SelectCollectionBy_DinhMucThucTe_ID(dinhMucThucTeDangKy.ID);
                    foreach (KDT_GC_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                    {
                        item.DMCollection = KDT_GC_DinhMucThucTe_DinhMuc.SelectCollectionBy_DinhMucThucTeSP_ID(item.ID);
                    }
                    BindDataSP();
                    cbbLenhSanXuat.ReadOnly = true;
                }
                LoadDataDefault();
                setCommandStatus();
                CountToTalRow();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadDataDefault()
        {
            try
            {

                cbbMaNPL.DataSource = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(HD.ID);
                cbbMaNPL.DisplayMember = "Ma";
                cbbMaNPL.ValueMember = "Ma";

                txtSoHopDong.Text = HD.SoHopDong;
                clcNgayHĐ.Value = HD.NgayKy;
                clcNgayHH.Value = HD.NgayGiaHan.Year == 1900 ? HD.NgayHetHan : HD.NgayGiaHan;
            }
            catch (Exception ex)
            {                
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataSP()
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = dinhMucThucTeDangKy.SPCollection;
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataNPL()
        {
            try
            {
                dgListNPL.Refetch();
                dgListNPL.DataSource = DinhMucThucTeSP.DMCollection;
                dgListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void setCommandStatus()
        {
            if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
                btnSelect.Enabled = false;
                btnDeleteSP.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = true;
                btnDelete.Enabled = true;
                btnSelect.Enabled = true;
                btnDeleteSP.Enabled = true;

                clcNgayTiepNhan.Value = DateTime.Now;
                lblTrangThai.Text = dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
                btnSelect.Enabled = false;
                btnDeleteSP.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
                btnSelect.Enabled = false;
                btnDeleteSP.Enabled = false;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Đã khai báo", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                btnAdd.Enabled = true;
                btnDelete.Enabled = true;
                btnSelect.Enabled = true;
                btnDeleteSP.Enabled = true;

                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã sửa , Chưa khai báo", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                btnAdd.Enabled = true;
                btnDelete.Enabled = true;
                btnSelect.Enabled = true;
                btnDeleteSP.Enabled = true;

                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }

        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAddExcel":
                    btnImportExcel_Click(null,null);
                    break;
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdSend":
                    this.SendV5(false);
                    break;
                case "cmdSendEdit":
                    dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    this.SendV5(false);
                    break;
                case "cmdCancel":
                    dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    this.SendV5(true);
                    break;
                case "cmdFeedback":
                    this.FeedBackV5();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGuidString();
                    break;
                case "cmdPrint":
                    this.Print();
                    break;
            }
        }

        private void Print()
        {
            try
            {
                Report_16_ĐMTT_GSQL f = new Report_16_ĐMTT_GSQL();
                f.BindReport(dinhMucThucTeDangKy);
                f.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateGuidString()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_GC_DinhMucThucTeDangKy", "", Convert.ToInt32(dinhMucThucTeDangKy.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = dinhMucThucTeDangKy.ID;
                form.DeclarationIssuer = DeclarationIssuer.GC_DINH_MUC;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void SendV5(bool isCancel)
        {
            if (dinhMucThucTeDangKy.SPCollection.Count == 0)
            {
                ShowMessage("Doanh nghiệp chưa nhập danh sách sản phẩm khai báo định mức", false);
                return;
            }
            if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                ShowMessage("Hợp đồng nay chưa được khai báo đến hải quan . Doanh nghiệp hãy khai báo hợp đồng trước khi khai định mức", false);
                return;
            }
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dinhMucThucTeDangKy.ID;
            if (sendXML.Load())
            {
                if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "Khai báo đã được gửi đến hải quan. nhấn nút [Lấy phản hồi] để nhận thông tin từ hải quan", false);
                    return;
                }
            }
            try
            {
                if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                dinhMucThucTeDangKy.GuidString = Guid.NewGuid().ToString();
                txtGuidString.Text = dinhMucThucTeDangKy.GuidString.ToString().ToUpper();
                Company.KDT.SHARE.Components.GC_DinhMuc dm = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferDinhMuc(dinhMucThucTeDangKy, HD, GlobalSettings.TEN_DON_VI, isCancel);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dinhMucThucTeDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dinhMucThucTeDangKy.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dinhMucThucTeDangKy.MaHaiQuan).Trim() : dinhMucThucTeDangKy.MaHaiQuan.Trim()

                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC,
                                    Function = dm.Function,
                                    Reference = dinhMucThucTeDangKy.GuidString,
                                }
                                ,
                                dm);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(dinhMucThucTeDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoDinhMuc);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                    sendXML.master_id = dinhMucThucTeDangKy.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.func = Convert.ToInt32(dm.Function);
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            dinhMucThucTeDangKy.DeleteDinhMucGC();
                        }
                        else
                        {
                            dinhMucThucTeDangKy.TransferGC();
                        }
                        dinhMucThucTeDangKy.Update();
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        setCommandStatus();
                        FeedBackV5();
                    }
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                    sendForm.Message.XmlSaveMessage(dinhMucThucTeDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQTuChoiDinhMuc);
                    ShowMessageTQDT(msgInfor, false);
                    setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(dinhMucThucTeDangKy.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dinhMucThucTeDangKy, ref msgInfor, e);

        }
        private void FeedBackV5()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dinhMucThucTeDangKy.ID;
            if (!sendXML.Load())
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                return;
            }
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_DINH_MUC,
                Reference = dinhMucThucTeDangKy.GuidString,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_DINH_MUC,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = dinhMucThucTeDangKy.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dinhMucThucTeDangKy.MaHaiQuan.Trim()),
                                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dinhMucThucTeDangKy.MaHaiQuan).Trim() : dinhMucThucTeDangKy.MaHaiQuan.Trim()
                                          }, subjectBase, null);
            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            dinhMucThucTeDangKy.DeleteDinhMucGC();
                        }
                        else
                        {
                            dinhMucThucTeDangKy.TransferGC();
                        }
                        dinhMucThucTeDangKy.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        dinhMucThucTeDangKy.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }
        private bool Validate(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbLenhSanXuat, errorProvider, "LỆNH SẢN XUẤT", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcTuNgay, errorProvider, "TỪ NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcDenNgay, errorProvider, "ĐẾN NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbTinhTrang, errorProvider, "TÌNH TRẠNG", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void Get()
        {
            try
            {
                dinhMucThucTeDangKy.HopDong_ID = HD.ID;
                dinhMucThucTeDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                dinhMucThucTeDangKy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                dinhMucThucTeDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                dinhMucThucTeDangKy.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text);
                dinhMucThucTeDangKy.NgayTiepNhan = clcNgayTiepNhan.Value;
                dinhMucThucTeDangKy.GhiChu = txtGhiChu.Text.ToString();
                dinhMucThucTeDangKy.LenhSanXuat_ID = Convert.ToInt32(cbbLenhSanXuat.Value);
                dinhMucThucTeDangKy.GuidString = txtGuidString.Text.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }
        private void Set()
        {
            try
            {
                cbbLenhSanXuat.Value = dinhMucThucTeDangKy.LenhSanXuat_ID;
                txtGhiChu.Text = dinhMucThucTeDangKy.GhiChu;
                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                txtGuidString.Text = dinhMucThucTeDangKy.GuidString.ToString().ToUpper();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Save()
        {
            try
            {
                if (!Validate(false))
                    return;
                //foreach (KDT_GC_LenhSanXuat item in KDT_GC_LenhSanXuat.SelectCollectionDynamic("ID NOT IN (" + dinhMucThucTeDangKy.ID + ")", ""))
                //{
                //    if (item.SoLenhSanXuat == cbbLenhSanXuat.Text.ToString())
                //    {
                //        ShowMessageTQDT("Thông báo từ Hệ thống", String.Format("LỆNH SẢN XUẤT NÀY ĐÃ ĐƯỢC NHẬP LIỆU TRƯỚC ĐÓ VỚI THÔNG TIN  \r\n ID - SỐ TIẾP NHẬN - NGÀY TIẾP NHẬN -r\n - {0} - {1} - {2} \n ", item.ID, item.SoTiepNhan, item.NgayTiepNhan), false);
                //        return;
                //    }
                //}
                Get();
                if (dinhMucThucTeDangKy.SPCollection.Count == 0)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "Danh sách sản phẩm chưa được nhập", false);
                    return;
                }
                else
                {
                    string errorToTal = "";
                    string errorSP = "";
                    int i = 1;
                    foreach (KDT_GC_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                    {
                        if (item.DMCollection.Count==0)
                        {
                            errorSP += "" + i + " - " + item.MaSanPham + "\n";
                            i++;
                        }
                    }
                    if (!String.IsNullOrEmpty(errorSP))
                    {
                        errorToTal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSP + " CHƯA NHẬP ĐỊNH MỨC";
                        ShowMessageTQDT("Thông báo từ hệ thống", errorToTal, false);
                        return;
                    }
                }
                dinhMucThucTeDangKy.InsertUpdateFull();
                ShowMessageTQDT("Thông báo từ hệ thống", "Lưu thành công", false);
                cbbLenhSanXuat.ReadOnly = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                string where = " LenhSanXuat_ID = " + Convert.ToInt32(cbbLenhSanXuat.Value) + " AND MaSanPham NOT IN ('',";
                KDT_LenhSanXuat lenhsanxuat = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                SanPhamLenhSanXuatRegistedForm SPRegistedForm = new SanPhamLenhSanXuatRegistedForm();
                foreach (KDT_GC_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                {
                    where += "'" + item.MaSanPham + "',";
                }
                where = where.Substring(0, where.Length - 1);
                where += ")";
                SPRegistedForm.whereCondition = where;
                SPRegistedForm.lenhsanxuat = lenhsanxuat;
                SPRegistedForm.ShowDialog(this);
                if (SPRegistedForm.SanPhamSelectCollection.Count >=1)
                {
                    List<GC_NguyenPhuLieu> NPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(HD.ID);
                    foreach (KDT_LenhSanXuat_SP item in SPRegistedForm.SanPhamSelectCollection)
                    {
                        KDT_GC_DinhMucThucTe_SP DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
                        DinhMucThucTeSP.MaSanPham = item.MaSanPham;
                        DinhMucThucTeSP.TenSanPham = item.TenSanPham;
                        DinhMucThucTeSP.MaHS = item.MaHS;
                        DinhMucThucTeSP.DVT_ID = item.DVT_ID;
                        List<Company.GC.BLL.GC.GC_DinhMuc> dmCollection = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionDynamic("HopDong_ID = " + HD.ID + " AND MaSanPham = '" + item.MaSanPham + "' AND LenhSanXuat_ID = " + cbbLenhSanXuat.Value + "", "");
                        foreach (Company.GC.BLL.GC.GC_DinhMuc dinhmuc in dmCollection)
                        {
                            KDT_GC_DinhMucThucTe_DinhMuc DinhMucThucTeDinhMuc = new KDT_GC_DinhMucThucTe_DinhMuc();
                            DinhMucThucTeDinhMuc.MaSanPham = item.MaSanPham;
                            DinhMucThucTeDinhMuc.TenSanPham = item.TenSanPham;
                            DinhMucThucTeDinhMuc.DVT_SP = item.DVT_ID;
                            DinhMucThucTeDinhMuc.MaNPL = dinhmuc.MaNguyenPhuLieu;
                            DinhMucThucTeDinhMuc.TenNPL = dinhmuc.TenNPL;
                            foreach (GC_NguyenPhuLieu npl in NPLCollection)
                            {
                                if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                                {
                                    DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                    DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                    break;
                                }
                            }
                            DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                            if (dinhmuc.TyLeHaoHut > 0)
                            {
                                DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung + dinhmuc.DinhMucSuDung * dinhmuc.TyLeHaoHut / 100;
                                DinhMucThucTeDinhMuc.TyLeHaoHut = 0;
                            }
                            DinhMucThucTeSP.DMCollection.Add(DinhMucThucTeDinhMuc);
                        }
                        dinhMucThucTeDangKy.SPCollection.Add(DinhMucThucTeSP);   
                    }
                }
                BindDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            if(!Validate(false))
                return;
            ReadExcelDinhMucThucTeForm f = new ReadExcelDinhMucThucTeForm();
            f.dinhMucThucTeDangKy = dinhMucThucTeDangKy;
            f.ShowDialog(this);
            BindDataSP();
            CountToTalRow();
        }

        private void btnDeleteSP_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                List<KDT_GC_DinhMucThucTe_SP> ItemColl = new List<KDT_GC_DinhMucThucTe_SP>();
                if (dgListNPL.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_GC_DinhMucThucTe_SP)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_GC_DinhMucThucTe_SP item in ItemColl)
                    {
                        foreach (KDT_GC_DinhMucThucTe_DinhMuc itemss in item.DMCollection)
                        {
                            if(itemss.ID > 0)
                                itemss.Delete();                            
                        }
                        if (item.ID > 0)
                            item.Delete();
                        dinhMucThucTeDangKy.SPCollection.Remove(item);
                    }
                    BindDataSP();
                    CountToTalRow();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetDinhMuc()
        {
            try
            {
                DinhMucThucTeDinhMuc.MaSanPham = DinhMucThucTeSP.MaSanPham;
                DinhMucThucTeDinhMuc.TenSanPham = DinhMucThucTeSP.TenSanPham;
                DinhMucThucTeDinhMuc.DVT_SP = DinhMucThucTeSP.DVT_ID;
                DinhMucThucTeDinhMuc.MaNPL = cbbMaNPL.Value.ToString();
                DinhMucThucTeDinhMuc.TenNPL = txtTenNPL.Text.ToString();
                DinhMucThucTeDinhMuc.DVT_NPL = txtDonViTinhNPL.SelectedValue.ToString();
                DinhMucThucTeDinhMuc.MaHS = txtMaHSNPL.Text;
                DinhMucThucTeDinhMuc.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                DinhMucThucTeDinhMuc.GhiChu = txtGhiChuNPL.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetDinhMuc()
        {
            try
            {
                cbbMaNPL.Value = DinhMucThucTeDinhMuc.MaNPL;
                txtTenNPL.Text = DinhMucThucTeDinhMuc.TenNPL;
                txtDonViTinhNPL.SelectedValue = DinhMucThucTeDinhMuc.DVT_NPL;
                txtMaHSNPL.Text = DinhMucThucTeDinhMuc.MaHS;
                txtDinhMuc.Text = DinhMucThucTeDinhMuc.DinhMucSuDung.ToString();
                txtGhiChu.Text = DinhMucThucTeDinhMuc.GhiChu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbMaNPL, errorProvider, "MÃ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, "TÊN/MÔ TẢ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDinhMuc, errorProvider, "ĐỊNH MỨC SỬ DỤNG", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetDinhMuc();
                if (isAdd)
                {
                    foreach (KDT_GC_DinhMucThucTe_DinhMuc item in DinhMucThucTeSP.DMCollection)
                    {
                        if (item.MaNPL ==DinhMucThucTeDinhMuc.MaNPL)
                        {
                            errorProvider.SetError(cbbMaNPL, "Mã NPL đã nhập định mức trên lưới .");
                            return;
                        }
                    }
                    DinhMucThucTeSP.DMCollection.Add(DinhMucThucTeDinhMuc);
                }
                DinhMucThucTeDinhMuc = new KDT_GC_DinhMucThucTe_DinhMuc();
                isAdd = true;
                cbbMaNPL.Text = String.Empty;
                txtTenNPL.Text = String.Empty;
                txtDonViTinhNPL.Text = String.Empty;
                txtDinhMuc.Text = String.Empty;
                txtGhiChuNPL.Text = String.Empty;
                BindDataNPL();
                CountToTalRow();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void CountToTalRow()
        {
            try
            {
                int count = 0;
                foreach (KDT_GC_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                {
                    count += item.DMCollection.Count;
                }
                lblTotalRow.Text = count.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnExcel_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListNPL.SelectedItems;
                List<KDT_GC_DinhMucThucTe_DinhMuc> ItemColl = new List<KDT_GC_DinhMucThucTe_DinhMuc>();
                if (dgListNPL.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_GC_DinhMucThucTe_DinhMuc)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_GC_DinhMucThucTe_DinhMuc item in ItemColl)
                    {
                        if(item.ID > 0)
                            item.Delete();
                        DinhMucThucTeSP.DMCollection.Remove(item);
                    }
                    BindDataNPL();
                    CountToTalRow();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grListSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
                        DinhMucThucTeSP = (KDT_GC_DinhMucThucTe_SP)i.GetRow().DataRow;
                        Janus.Windows.GridEX.GridEXFormatStyle style = new Janus.Windows.GridEX.GridEXFormatStyle();
                        if (DinhMucThucTeSP.DMCollection.Count == 0)
                        {
                            style.BackColor = Color.GreenYellow;
                            i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                        }
                        else
                        {
                            style.BackColor = Color.Transparent;
                            i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                        }
                    }
                }
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(grListSP.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaSP.Text);
                grListSP.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListSP_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                    DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
                    DinhMucThucTeSP = (KDT_GC_DinhMucThucTe_SP)e.Row.DataRow;
                    Janus.Windows.GridEX.GridEXFormatStyle style = new Janus.Windows.GridEX.GridEXFormatStyle();
                    if (DinhMucThucTeSP.DMCollection.Count == 0)
                    {
                        style.BackColor = Color.GreenYellow;
                        e.Row.Cells["MaSanPham"].FormatStyle = style;
                    }
                    else
                    {
                        style.BackColor = Color.Transparent;
                        e.Row.Cells["MaSanPham"].FormatStyle = style;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    DinhMucThucTeDinhMuc = new KDT_GC_DinhMucThucTe_DinhMuc();
                    DinhMucThucTeDinhMuc = (KDT_GC_DinhMucThucTe_DinhMuc)e.Row.DataRow;
                    SetDinhMuc();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaNPL_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaNPL.Value != null)
                {
                    List<GC_NguyenPhuLieu> NPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(HD.ID);
                    foreach (GC_NguyenPhuLieu item in NPLCollection)
                    {
                        if (item.Ma == cbbMaNPL.Value.ToString())
                        {
                            txtTenNPL.Text = item.Ten.ToString();
                            txtDonViTinhNPL.SelectedValue = item.DVT_ID;
                            txtMaHSNPL.Text = item.MaHS;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_NPL"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_NPL"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListSP_FormattingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
                    DinhMucThucTeSP = (KDT_GC_DinhMucThucTe_SP)e.Row.DataRow;
                    if (DinhMucThucTeSP.DMCollection.Count == 0)
                    {
                        e.Row.Cells["MaSanPham"].FormatStyle = new GridEXFormatStyle() { BackColor = Color.GreenYellow };
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLenhSanXuat_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbLenhSanXuat.Value != null)
                {
                    if (Convert.ToInt64(cbbLenhSanXuat.Value) != dinhMucThucTeDangKy.LenhSanXuat_ID)
                    {
                        KDT_LenhSanXuat lenhSX = new KDT_LenhSanXuat();
                        lenhSX = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                        if (lenhSX.ID > 0)
                        {
                            clcTuNgay.Value = lenhSX.TuNgay;
                            clcDenNgay.Value = lenhSX.DenNgay;
                            txtPO.Text = lenhSX.SoDonHang;
                            cbbTinhTrang.SelectedValue = lenhSX.TinhTrang;
                            txtGhiChu.Text = lenhSX.GhiChu;
                            dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSX.ID;

                            List<KDT_GC_DinhMucThucTe_SP> SPCollection = new List<KDT_GC_DinhMucThucTe_SP>();
                            foreach (KDT_GC_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                            {
                                foreach (KDT_GC_DinhMucThucTe_DinhMuc itemss in item.DMCollection)
                                {
                                    if (itemss.ID > 0)
                                        itemss.Delete();
                                }
                                if (item.ID > 0)
                                    item.Delete();
                                SPCollection.Add(item);
                            }
                            foreach (KDT_GC_DinhMucThucTe_SP item in SPCollection)
                            {
                                dinhMucThucTeDangKy.SPCollection.Remove(item);
                            }
                            BindDataSP();
                        }
                    }
                    else
                    {
                        KDT_LenhSanXuat lenhSX = new KDT_LenhSanXuat();
                        lenhSX = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                        if (lenhSX.ID > 0)
                        {
                            clcTuNgay.Value = lenhSX.TuNgay;
                            clcDenNgay.Value = lenhSX.DenNgay;
                            txtPO.Text = lenhSX.SoDonHang;
                            cbbTinhTrang.SelectedValue = lenhSX.TinhTrang;
                            txtGhiChu.Text = lenhSX.GhiChu;
                            dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSX.ID;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
