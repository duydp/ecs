SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO

IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 't_KDT_SXXK_DinhMuc_Log'))
BEGIN
CREATE TABLE [dbo].[t_KDT_SXXK_DinhMuc_Log](
	[ID] [BIGINT] IDENTITY(1,1) NOT NULL,
	[MaSanPham] [NVARCHAR](50) NOT NULL,
	[MaNguyenPhuLieu] [NVARCHAR](50) NOT NULL,
	[DVT_ID] [CHAR](3) NOT NULL,
	[DinhMucSuDung] [NUMERIC](18, 10) NOT NULL,
	[TyLeHaoHut] [NUMERIC](18, 5) NOT NULL,
	[GhiChu] [VARCHAR](240) NULL,
	[STTHang] [INT] NOT NULL,
	[Master_ID] [BIGINT] NOT NULL,
	[IsFromVietNam] [BIT] NULL,
	[MaDinhDanhLenhSX] [NVARCHAR](50) NULL,
	[DateLog] [DATETIME] NULL,
	[Status] [NVARCHAR](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF @@ERROR <> 0 SET NOEXEC ON
ALTER TABLE [dbo].[t_KDT_SXXK_DinhMuc_Log] ADD  DEFAULT ((0)) FOR [IsFromVietNam]
END

GO


IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 't_KDT_SXXK_NguyenPhuLieu_Log'))
BEGIN
CREATE TABLE [dbo].[t_KDT_SXXK_NguyenPhuLieu_Log](
	[ID] [BIGINT] IDENTITY(1,1) NOT NULL,
	[Ma] [NVARCHAR](50) NOT NULL,
	[Ten] [NVARCHAR](254) NOT NULL,
	[MaHS] [VARCHAR](12) NULL,
	[DVT_ID] [CHAR](3) NOT NULL,
	[STTHang] [INT] NOT NULL,
	[Master_ID] [BIGINT] NOT NULL,
	[DateLog] [DATETIME] NULL,
	[Status] [NVARCHAR](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF @@ERROR <> 0 SET NOEXEC ON
GO
/****** Object:  Table [dbo].[t_KDT_SXXK_SanPham_Log]    Script Date: 3/22/2022 10:55:46 AM ******/
IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 't_KDT_SXXK_SanPham_Log'))
BEGIN
CREATE TABLE [dbo].[t_KDT_SXXK_SanPham_Log](
	[ID] [BIGINT] IDENTITY(1,1) NOT NULL,
	[Ma] [NVARCHAR](50) NOT NULL,
	[Ten] [NVARCHAR](254) NOT NULL,
	[MaHS] [VARCHAR](12) NULL,
	[DVT_ID] [CHAR](3) NOT NULL,
	[STTHang] [INT] NOT NULL,
	[Master_ID] [BIGINT] NOT NULL,
	[DateLog] [DATETIME] NULL,
	[Status] [NVARCHAR](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_Update]    Script Date: 3/22/2022 11:09:24 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Log_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectDynamic]    Script Date: 3/22/2022 11:09:24 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Log_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectAll]    Script Date: 3/22/2022 11:09:24 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Log_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_Load]    Script Date: 3/22/2022 11:09:24 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Log_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_InsertUpdate]    Script Date: 3/22/2022 11:09:24 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Log_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_Insert]    Script Date: 3/22/2022 11:09:24 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Log_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_DeleteDynamic]    Script Date: 3/22/2022 11:09:24 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Log_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_Delete]    Script Date: 3/22/2022 11:09:24 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Log_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_Delete]    Script Date: 3/22/2022 11:09:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Log_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_DinhMuc_Log]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_DeleteDynamic]    Script Date: 3/22/2022 11:09:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Log_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_DinhMuc_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_Insert]    Script Date: 3/22/2022 11:09:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Log_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_Insert]
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 10),
	@TyLeHaoHut numeric(18, 5),
	@GhiChu varchar(240),
	@STTHang int,
	@Master_ID bigint,
	@IsFromVietNam bit,
	@MaDinhDanhLenhSX nvarchar(50),
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_DinhMuc_Log]
(
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam],
	[MaDinhDanhLenhSX],
	[DateLog],
	[Status]
)
VALUES 
(
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DVT_ID,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@GhiChu,
	@STTHang,
	@Master_ID,
	@IsFromVietNam,
	@MaDinhDanhLenhSX,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_InsertUpdate]    Script Date: 3/22/2022 11:09:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Log_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_InsertUpdate]
	@ID bigint,
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 10),
	@TyLeHaoHut numeric(18, 5),
	@GhiChu varchar(240),
	@STTHang int,
	@Master_ID bigint,
	@IsFromVietNam bit,
	@MaDinhDanhLenhSX nvarchar(50),
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_DinhMuc_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_DinhMuc_Log] 
		SET
			[MaSanPham] = @MaSanPham,
			[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
			[DVT_ID] = @DVT_ID,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[GhiChu] = @GhiChu,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID,
			[IsFromVietNam] = @IsFromVietNam,
			[MaDinhDanhLenhSX] = @MaDinhDanhLenhSX,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_DinhMuc_Log]
		(
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DVT_ID],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[GhiChu],
			[STTHang],
			[Master_ID],
			[IsFromVietNam],
			[MaDinhDanhLenhSX],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DVT_ID,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@GhiChu,
			@STTHang,
			@Master_ID,
			@IsFromVietNam,
			@MaDinhDanhLenhSX,
			@DateLog,
			@Status
		)		
	END
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_Load]    Script Date: 3/22/2022 11:09:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Log_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam],
	[MaDinhDanhLenhSX],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_SXXK_DinhMuc_Log]
WHERE
	[ID] = @ID
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectAll]    Script Date: 3/22/2022 11:09:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam],
	[MaDinhDanhLenhSX],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_SXXK_DinhMuc_Log]	

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectDynamic]    Script Date: 3/22/2022 11:09:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam],
	[MaDinhDanhLenhSX],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_SXXK_DinhMuc_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_Log_Update]    Script Date: 3/22/2022 11:09:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Log_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Log_Update]
	@ID BIGINT,
	@MaSanPham NVARCHAR(50),
	@MaNguyenPhuLieu NVARCHAR(50),
	@DVT_ID CHAR(3),
	@DinhMucSuDung NUMERIC(18, 10),
	@TyLeHaoHut NUMERIC(18, 5),
	@GhiChu VARCHAR(240),
	@STTHang INT,
	@Master_ID BIGINT,
	@IsFromVietNam BIT,
	@MaDinhDanhLenhSX NVARCHAR(50),
	@DateLog DATETIME,
	@Status NVARCHAR(50)
AS

UPDATE
	[dbo].[t_KDT_SXXK_DinhMuc_Log]
SET
	[MaSanPham] = @MaSanPham,
	[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
	[DVT_ID] = @DVT_ID,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[GhiChu] = @GhiChu,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID,
	[IsFromVietNam] = @IsFromVietNam,
	[MaDinhDanhLenhSX] = @MaDinhDanhLenhSX,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Update]    Script Date: 3/22/2022 11:11:40 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectDynamic]    Script Date: 3/22/2022 11:11:40 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectAll]    Script Date: 3/22/2022 11:11:40 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Load]    Script Date: 3/22/2022 11:11:40 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Log_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_InsertUpdate]    Script Date: 3/22/2022 11:11:40 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Insert]    Script Date: 3/22/2022 11:11:40 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_DeleteDynamic]    Script Date: 3/22/2022 11:11:40 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Delete]    Script Date: 3/22/2022 11:11:40 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Delete]    Script Date: 3/22/2022 11:11:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_NguyenPhuLieu_Log]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_DeleteDynamic]    Script Date: 3/22/2022 11:11:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_NguyenPhuLieu_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Insert]    Script Date: 3/22/2022 11:11:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Insert]
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint,
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_NguyenPhuLieu_Log]
(
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID],
	[DateLog],
	[Status]
)
VALUES 
(
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@STTHang,
	@Master_ID,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_InsertUpdate]    Script Date: 3/22/2022 11:11:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_InsertUpdate]
	@ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint,
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_NguyenPhuLieu_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_NguyenPhuLieu_Log] 
		SET
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_NguyenPhuLieu_Log]
		(
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[STTHang],
			[Master_ID],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@STTHang,
			@Master_ID,
			@DateLog,
			@Status
		)		
	END
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Load]    Script Date: 3/22/2022 11:11:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_SXXK_NguyenPhuLieu_Log]
WHERE
	[ID] = @ID
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectAll]    Script Date: 3/22/2022 11:11:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_SXXK_NguyenPhuLieu_Log]	

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectDynamic]    Script Date: 3/22/2022 11:11:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_SXXK_NguyenPhuLieu_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Update]    Script Date: 3/22/2022 11:11:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Log_Update]
	@ID BIGINT,
	@Ma NVARCHAR(50),
	@Ten NVARCHAR(254),
	@MaHS VARCHAR(12),
	@DVT_ID CHAR(3),
	@STTHang INT,
	@Master_ID BIGINT,
	@DateLog DATETIME,
	@Status NVARCHAR(50)
AS

UPDATE
	[dbo].[t_KDT_SXXK_NguyenPhuLieu_Log]
SET
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_Update]    Script Date: 3/22/2022 11:14:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Log_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_SelectDynamic]    Script Date: 3/22/2022 11:14:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Log_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_SelectAll]    Script Date: 3/22/2022 11:14:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Log_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_Load]    Script Date: 3/22/2022 11:14:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Log_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_InsertUpdate]    Script Date: 3/22/2022 11:14:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Log_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_Insert]    Script Date: 3/22/2022 11:14:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Log_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_DeleteDynamic]    Script Date: 3/22/2022 11:14:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Log_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_Delete]    Script Date: 3/22/2022 11:14:41 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Log_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_Delete]    Script Date: 3/22/2022 11:14:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Log_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_SanPham_Log]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_DeleteDynamic]    Script Date: 3/22/2022 11:14:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Log_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_SanPham_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_Insert]    Script Date: 3/22/2022 11:14:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Log_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_Insert]
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint,
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_SanPham_Log]
(
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID],
	[DateLog],
	[Status]
)
VALUES 
(
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@STTHang,
	@Master_ID,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_InsertUpdate]    Script Date: 3/22/2022 11:14:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Log_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_InsertUpdate]
	@ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint,
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_SanPham_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_SanPham_Log] 
		SET
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_SanPham_Log]
		(
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[STTHang],
			[Master_ID],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@STTHang,
			@Master_ID,
			@DateLog,
			@Status
		)		
	END
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_Load]    Script Date: 3/22/2022 11:14:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Log_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_SXXK_SanPham_Log]
WHERE
	[ID] = @ID
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_SelectAll]    Script Date: 3/22/2022 11:14:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Log_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_SXXK_SanPham_Log]	

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_SelectDynamic]    Script Date: 3/22/2022 11:14:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Log_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_SXXK_SanPham_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_SanPham_Log_Update]    Script Date: 3/22/2022 11:14:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Log_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Log_Update]
	@ID BIGINT,
	@Ma NVARCHAR(50),
	@Ten NVARCHAR(254),
	@MaHS VARCHAR(12),
	@DVT_ID CHAR(3),
	@STTHang INT,
	@Master_ID BIGINT,
	@DateLog DATETIME,
	@Status NVARCHAR(50)
AS

UPDATE
	[dbo].[t_KDT_SXXK_SanPham_Log]
SET
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO




IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) BEGIN
	PRINT 'The database update succeeded'
	
	IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '42.2') = 0
	BEGIN
		INSERT INTO dbo.t_HaiQuan_Version VALUES('42.2',GETDATE(), N'CẬP NHẬT t_KDT_SXXK_DinhMuc_Log, t_KDT_SXXK_NguyenPhuLieu_Log, t_KDT_SXXK_SanPham_Log')
	END	

END
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO

