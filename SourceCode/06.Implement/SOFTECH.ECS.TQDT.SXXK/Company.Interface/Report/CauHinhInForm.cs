﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.Report
{
    public partial class CauHinhInForm : Company.Interface.BaseForm
    {
        public CauHinhInForm()
        {
            InitializeComponent();
        }

        private void CauHinhInForm_Load(object sender, EventArgs e)
        {
            try
            {
                GlobalSettings.KhoiTao_GiaTriMacDinh();
                //
                txtDinhMuc.Value = GlobalSettings.SoThapPhan.DinhMuc;
                txtLuongNPL.Value = GlobalSettings.SoThapPhan.LuongNPL;
                txtLuongSP.Value = GlobalSettings.SoThapPhan.LuongSP;
                txtTLHH.Value = GlobalSettings.SoThapPhan.TLHH;

                /*Updated by Hungtq 14/01/2011*/

                cbbSapXep.SelectedValue = GlobalSettings.SoThapPhan.SapXepTheoTK;
                cbNPLKoTK.SelectedValue = GlobalSettings.SoThapPhan.NPLKoTK;
                cbMauBC01.SelectedValue = GlobalSettings.SoThapPhan.MauBC01;
                cbTachLam2.SelectedValue = GlobalSettings.SoThapPhan.TachLam2;
                cbMauBC04.SelectedValue = GlobalSettings.SoThapPhan.MauBC04;

                txtTriGiaNT.Value = GlobalSettings.TriGiaNT;
                txtBCTK.Value = GlobalSettings.SoThapPhan_BaoCaoThanhKhoan;

                txtDiaPhuong.Text = GlobalSettings.DiaPhuong;
                rdNgayTuDong.Checked = GlobalSettings.NgayHeThong;
                rdNgayTuNhap.Checked = !rdNgayTuDong.Checked;
                cbbAmTKTiep.SelectedValue = GlobalSettings.AmTKTiep;
                cboChenhLech.SelectedValue = GlobalSettings.CHENHLECH_THN_THX;

                cboSTNDT.SelectedValue = GlobalSettings.InToKhai.SoTNDKDT;
                cboTGTT.SelectedValue = GlobalSettings.InToKhai.TriGiTT;
                cboLoaiHinh.SelectedValue = GlobalSettings.InToKhai.LoaiHinh;
                cboDVT.SelectedValue = GlobalSettings.InToKhai.DVTCon;

                cboTKX.SelectedValue = GlobalSettings.TKX;
                cboTKhaiKoTK.SelectedValue = GlobalSettings.ToKhaiKoTK;
                cboHienThiHD.SelectedValue = GlobalSettings.HienThiHD;
                cboChiPhi.SelectedValue = GlobalSettings.ChiPhiKhac;
                cboKCX.SelectedValue = GlobalSettings.TKhoanKCX;
                numericUpDown1.Value = (decimal)GlobalSettings.FontDongHang;
                numericFontBCXNT.Value = (decimal)GlobalSettings.FontBCXNT;

                cboChenhLech.ComboStyle = Janus.Windows.EditControls.ComboStyle.Combo;
                cboXuongDongThongTinDiaChi.SelectedValue = GlobalSettings.WordWrap;
                ch07DMDK.Checked = GlobalSettings.M07DMDK;

                string delay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TimeDelay");
                numTimeDelay.Value = Convert.ToDecimal(delay);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string dp = "";
                cvError.Validate();
                if (!cvError.IsValid) return;
                else
                {
                    GlobalSettings.Luu_SoThapPhan((int)txtDinhMuc.Value, (int)txtLuongNPL.Value, (int)txtLuongSP.Value, (int)cbbSapXep.SelectedValue, (int)cbNPLKoTK.SelectedValue, (int)cbMauBC01.SelectedValue, (int)cbTachLam2.SelectedValue, (int)cbMauBC04.SelectedValue, (int)txtTLHH.Value);
                    GlobalSettings.Luu_InToKhai((int)cboSTNDT.SelectedValue, (int)cboTGTT.SelectedValue, (int)cboLoaiHinh.SelectedValue, (int)cboDVT.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VisibleHD", cboHienThiHD.SelectedValue);

                    //Hungtq 22/12/2010. Luu cau hinh
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaPhuong", txtDiaPhuong.Text.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgayHeThong", rdNgayTuDong.Checked.ToString());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("AmTKTiep", cbbAmTKTiep.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CL_THN_THX", cboChenhLech.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TKX", cboTKX.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ToKhaiKoTK", (int)cboTKhaiKoTK.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ChiPhiKhac", (int)cboChiPhi.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontTenHang", (float)numericUpDown1.Value);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TKhoanKCX", (int)cboKCX.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontBCXNT", (float)numericFontBCXNT.Value);
                    //xuống dòng thông tin địa chỉ
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WordWrap", cboXuongDongThongTinDiaChi.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("07DMDK", ch07DMDK.Checked);

                    if (txtDiaPhuong.Text.Trim() == "")
                        dp = ".................";
                    else
                        dp = txtDiaPhuong.Text;

                    if (rdNgayTuDong.Checked == true)
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", dp + " ,ngày " + DateTime.Now.Day.ToString("00") + " tháng " + DateTime.Now.Month.ToString("00") + " năm " + DateTime.Now.Year.ToString("0000"));
                    else
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", dp + " ,ngày........tháng........năm........ ");

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TriGiaNT", txtTriGiaNT.Text.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("BCTK", txtBCTK.Text.Trim());

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TimeDelay", numTimeDelay.Value.ToString());

                    GlobalSettings.RefreshKey();

                    MLMessages("Lưu thông tin cấu hình thành công", "MSG_SAV03", "", false);
                    this.Close();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}

