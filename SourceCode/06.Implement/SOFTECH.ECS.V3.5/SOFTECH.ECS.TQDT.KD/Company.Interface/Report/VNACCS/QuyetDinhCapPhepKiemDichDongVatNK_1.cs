using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhCapPhepKiemDichDongVatNK_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhCapPhepKiemDichDongVatNK_1()
        {
            InitializeComponent();
        }
        public void BindingReport(VAJP030 vajp)
        {

            int HangHoa = vajp.HangHoa.Count;
            if (HangHoa <= 5)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 5, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblMaNguoiKhai.Text = vajp.SBC.GetValue().ToString().ToUpper();
            lblTenNguoiKhai.Text = vajp.SBN.GetValue().ToString().ToUpper();
            lblMaBuuChinhNguoiKhai.Text = vajp.SPC.GetValue().ToString().ToUpper();
            lblDiaChiNguoiKhai.Text = vajp.ADS.GetValue().ToString().ToUpper();
            lblMaNuocNguoiKhai.Text = vajp.SCC.GetValue().ToString().ToUpper();
            lblSoDienThoaiNguoiKhai.Text = vajp.SUN.GetValue().ToString().ToUpper();
            lblSoFaxNguoiKhai.Text = vajp.SFN.GetValue().ToString().ToUpper();
            lblEmailNguoiKhai.Text = vajp.SBE.GetValue().ToString().ToUpper();
            //lblNgayKhaiBao.Text = vajp.DOS.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.DOS.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vajp.DOS.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            lblSoDonXinCapPhep.Text = vajp.ONO.GetValue().ToString().ToUpper();
            lblChucNangChungTu.Text = vajp.FNC.GetValue().ToString().ToUpper();
            lblLoaiGiayPhep.Text = vajp.PRT.GetValue().ToString().ToUpper();
            lblMaDonViCapPhep.Text = vajp.OAC.GetValue().ToString().ToUpper();
            lblTenDonViCapPhep.Text = vajp.OAN.GetValue().ToString().ToUpper();
            lblKetQuaXuLy.Text = vajp.PCT.GetValue().ToString().ToUpper();
            lblSoGiayPhep.Text = vajp.PLN.GetValue().ToString().ToUpper();
            //lblNgayGiayPhep.Text = vajp.PLD.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.PLD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayGiayPhep.Text = Convert.ToDateTime(vajp.PLD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayGiayPhep.Text = "";
            }
            //lblNgayHieuLucGP.Text = vajp.EFD.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.EFD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHieuLucGP.Text = Convert.ToDateTime(vajp.EFD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHieuLucGP.Text = "";
            }
            //lblNgayHetHieuLucGP.Text = vajp.EPD.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.EPD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHetHieuLucGP.Text = Convert.ToDateTime(vajp.EPD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHetHieuLucGP.Text = "";
            }
            lblSoHopDong.Text = vajp.CNO.GetValue().ToString().ToUpper();
            lblToChucCaNhanXK.Text = vajp.EXP.GetValue().ToString().ToUpper();
            lblMaBuuChinhXK.Text = vajp.EPC.GetValue().ToString().ToUpper();
            lblSoNhaXK.Text = vajp.EBN.GetValue().ToString().ToUpper();
            lblPhuongXaXK.Text = vajp.ESN.GetValue().ToString().ToUpper();
            lblQuanHuyenXK.Text = vajp.EDN.GetValue().ToString().ToUpper();
            lblTinhThanhPhoXK.Text = vajp.ECN.GetValue().ToString().ToUpper();
            lblMaQuocGiaXK.Text = vajp.ERC.GetValue().ToString().ToUpper();
            lblSoDienThoaiXK.Text = vajp.EPN.GetValue().ToString().ToUpper();
            lblSoFaxXK.Text = vajp.EFX.GetValue().ToString().ToUpper();
            lblEmailXK.Text = vajp.EEM.GetValue().ToString().ToUpper();
            lblNuocXK.Text = vajp.ECC.GetValue().ToString().ToUpper();
            lblMaToChucCaNhanNK.Text = vajp.IMC.GetValue().ToString().ToUpper();
            lblTenNguoiNK.Text = vajp.IMN.GetValue().ToString().ToUpper();
            lblSoDangKyKD.Text = vajp.BRN.GetValue().ToString().ToUpper();
            lblMaBuuChinhNK.Text = vajp.BPC.GetValue().ToString().ToUpper();
            lblDiaChiNK.Text = vajp.AOI.GetValue().ToString().ToUpper();
            lblMaQuocGiaNK.Text = vajp.BCC.GetValue().ToString().ToUpper();
            lblSoDienThoaiNK.Text = vajp.BPN.GetValue().ToString().ToUpper();
            lblSoFaxNK.Text = vajp.BFX.GetValue().ToString().ToUpper();
            lblEmailNK.Text = vajp.IEM.GetValue().ToString().ToUpper();
            lblNuocQuaCanh.Text = vajp.TCC.GetValue().ToString().ToUpper();
            lblMaPhuongTienVC.Text = vajp.MTT.GetValue().ToString().ToUpper();
            lblTenPhuongTienVC.Text = vajp.MTN.GetValue().ToString().ToUpper();
            lblMaCuaKhauNhap.Text = vajp.IBC.GetValue().ToString().ToUpper();
            lblCuaKhauNhap.Text = vajp.IBN.GetValue().ToString().ToUpper();
            lblSoGiayCN.Text = vajp.PRN.GetValue().ToString().ToUpper();
            lblDiaDiemKiemDich.Text = vajp.QUL.GetValue().ToString().ToUpper();
            //lblNgayKiemDich.Text = vajp.QUT.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.QUT.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKiemDich.Text = Convert.ToDateTime(vajp.QUT.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKiemDich.Text = "";
            }
            //lblThoiGianKiemDich.Text = vajp.QUH.GetValue().ToString().ToUpper();
            if (vajp.QUH.GetValue().ToString().ToUpper() != "" && vajp.QUH.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiGianKiemDich.Text = vajp.QUH.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vajp.QUH.GetValue().ToString().ToUpper().Substring(2, 2);
            }
            else
            {
                lblThoiGianKiemDich.Text = "";
            }
            lblDiaDiemGiamSat.Text = vajp.CTL.GetValue().ToString().ToUpper();
            //lblNgayGiamSat.Text = vajp.CTT.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.CTT.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayGiamSat.Text = Convert.ToDateTime(vajp.CTT.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayGiamSat.Text = "";
            }
            //lblThoiGianGiamSat.Text = vajp.CTH.GetValue().ToString().ToUpper();
            if (vajp.CTH.GetValue().ToString().ToUpper() != "" && vajp.CTH.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiGianGiamSat.Text = vajp.CTH.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vajp.CTH.GetValue().ToString().ToUpper().Substring(2, 2);
            }
            else
            {
                lblThoiGianGiamSat.Text = "";
            }
            lblSoBanCanCap.Text = vajp.NOC.GetValue().ToString().ToUpper();
            lblNoiChuyenDen.Text = vajp.AVL.GetValue().ToString().ToUpper();
            lblTenTau.Text = vajp.SPN.GetValue().ToString().ToUpper();
            lblLyDo.Text = vajp.TRS.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.PLD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayCapPhep.Text = Convert.ToDateTime(vajp.PLD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayCapPhep.Text = "";
            }
            lblDaiDienDVCapPhep.Text = vajp.AUL.GetValue().ToString().ToUpper();

        }



    }
}
