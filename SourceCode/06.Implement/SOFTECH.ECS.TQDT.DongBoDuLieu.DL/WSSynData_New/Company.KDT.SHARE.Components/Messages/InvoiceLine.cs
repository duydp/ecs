﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
     public class InvoiceLine
    {
        /// <summary>
        /// ...
        /// </summary>
        [XmlElement("itemCharge")]
        public string ItemCharge { set; get; }
         /// <summary>
         /// ....
         /// </summary>
        [XmlElement("line")]
        public string Line { set; get; }
    }
}
