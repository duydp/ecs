GO
IF OBJECT_ID('trg_t_KDT_GC_HangPhuKien_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_HangPhuKien_Log
GO
IF OBJECT_ID('trg_t_KDT_GC_DinhMuc_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_DinhMuc_Log
GO
IF OBJECT_ID('trg_t_KDT_GC_LoaiPhuKien_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_LoaiPhuKien_Log
GO
IF OBJECT_ID('t_KDT_GC_DinhMuc_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_DinhMuc_Log
GO
IF OBJECT_ID('t_KDT_GC_HangPhuKien_Log') IS NOT NULL
DROP TABLE t_KDT_GC_HangPhuKien_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_DinhMuc_Log]
(
[ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
[MaSanPham] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNguyenPhuLieu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DinhMucSuDung] [numeric] (18, 8) NOT NULL,
[TyLeHaoHut] [numeric] (18, 8) NOT NULL,
[GhiChu] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STTHang] [int] NOT NULL,
[Master_ID] [bigint] NOT NULL,
[NPL_TuCungUng] [numeric] (18, 8) NOT NULL,
[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNPL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDDSX] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] [datetime] NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('t_KDT_GC_LoaiPhuKien_Log') IS NOT NULL
DROP TABLE t_KDT_GC_LoaiPhuKien_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_LoaiPhuKien_Log]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[MaPhuKien] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NoiDung] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThongTinMoi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Master_ID] [bigint] NULL,
[ThongTinCu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] [datetime] NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[t_KDT_GC_HangPhuKien_Log]
(
[ID] [bigint] NOT NULL IDENTITY(1,1) PRIMARY KEY,
[MaHang] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenHang] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ThongTinCu] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (18, 5) NOT NULL,
[DonGia] [float] NOT NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGia] [float] NOT NULL,
[NguyenTe_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STTHang] [int] NOT NULL,
[TinhTrang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Master_ID] [bigint] NOT NULL FOREIGN KEY REFERENCES dbo.t_KDT_GC_LoaiPhuKien_Log(ID) ,
[NhomSP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] [datetime] NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_GC_HopDong_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_HopDong_Log
GO
IF OBJECT_ID('trg_t_KDT_GC_NhomSanPham_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_NhomSanPham_Log
GO
IF OBJECT_ID('trg_t_KDT_GC_SanPham_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_SanPham_Log
GO
IF OBJECT_ID('trg_t_KDT_GC_NguyenPhuLieu_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_NguyenPhuLieu_Log
GO
IF OBJECT_ID('trg_t_KDT_GC_ThietBi_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_ThietBi_Log
GO
IF OBJECT_ID('trg_t_KDT_GC_HangMau_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_GC_HangMau_Log
GO
IF OBJECT_ID('t_KDT_GC_HopDong_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_HopDong_Log
GO
IF OBJECT_ID('t_KDT_GC_NhomSanPham_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_NhomSanPham_Log
GO
IF OBJECT_ID('t_KDT_GC_SanPham_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_SanPham_Log
GO
IF OBJECT_ID('t_KDT_GC_NguyenPhuLieu_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_NguyenPhuLieu_Log
GO
IF OBJECT_ID('t_KDT_GC_ThietBi_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_ThietBi_Log
GO
IF OBJECT_ID('t_KDT_GC_HangMau_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_GC_HangMau_Log
GO
CREATE TABLE [dbo].[t_KDT_GC_NhomSanPham_Log]
(
[ID] [bigint] IDENTITY(1,1) PRIMARY KEY ,
[Master_ID] [bigint] NOT NULL,
[MaSanPham] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuong] [numeric] (18, 5) NOT NULL,
[GiaGiaCong] [numeric] (18, 5) NOT NULL,
[STTHang] [int] NOT NULL,
[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGia] [float] NULL,
[DateLog] [datetime] NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[t_KDT_GC_SanPham_Log]
(
[ID] [bigint] IDENTITY(1,1) PRIMARY KEY,
[Master_ID] [bigint] NOT NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 6) NOT NULL,
[NhomSanPham_ID] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[STTHang] [int] NOT NULL,
[DonGia] [numeric] (18, 5) NULL,
[DateLog] [datetime] NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[t_KDT_GC_NguyenPhuLieu_Log]
(
[ID] [bigint] IDENTITY(1,1) PRIMARY KEY,
[Master_ID] [bigint] NOT NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 6) NOT NULL,
[STTHang] [int] NOT NULL,
[DonGia] [numeric] (18, 5) NULL,
[GhiChu] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] [datetime] NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[t_KDT_GC_ThietBi_Log]
(
[ID] [bigint] IDENTITY(1,1) PRIMARY KEY,
[Master_ID] [bigint] NOT NULL,
[Ma] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 5) NOT NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DonGia] [float] NOT NULL,
[TriGia] [float] NOT NULL,
[NguyenTe_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[STTHang] [int] NOT NULL,
[TinhTrang] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChu] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] [datetime] NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[t_KDT_GC_HangMau_Log]
(
[ID] [bigint] IDENTITY(1,1) PRIMARY KEY,
[Master_ID] [bigint] NOT NULL,
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongDangKy] [numeric] (18, 5) NOT NULL,
[STTHang] [int] NOT NULL,
[GhiChu] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] [datetime] NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Log_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Log_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Log_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Log_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Log_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Log_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Log_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Log_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Log_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_Insert]
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu varchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@MaDDSX nvarchar(50),
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_DinhMuc_Log]
(
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX],
	[DateLog],
	[Status]
)
VALUES 
(
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DVT_ID,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@GhiChu,
	@STTHang,
	@Master_ID,
	@NPL_TuCungUng,
	@TenSanPham,
	@TenNPL,
	@MaDDSX,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Log_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_Update]
	@ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu varchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@MaDDSX nvarchar(50),
	@DateLog datetime,
	@Status nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_DinhMuc_Log]
SET
	[MaSanPham] = @MaSanPham,
	[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
	[DVT_ID] = @DVT_ID,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[GhiChu] = @GhiChu,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID,
	[NPL_TuCungUng] = @NPL_TuCungUng,
	[TenSanPham] = @TenSanPham,
	[TenNPL] = @TenNPL,
	[MaDDSX] = @MaDDSX,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Log_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_InsertUpdate]
	@ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu varchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@MaDDSX nvarchar(50),
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_DinhMuc_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_DinhMuc_Log] 
		SET
			[MaSanPham] = @MaSanPham,
			[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
			[DVT_ID] = @DVT_ID,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[GhiChu] = @GhiChu,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID,
			[NPL_TuCungUng] = @NPL_TuCungUng,
			[TenSanPham] = @TenSanPham,
			[TenNPL] = @TenNPL,
			[MaDDSX] = @MaDDSX,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_DinhMuc_Log]
		(
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DVT_ID],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[GhiChu],
			[STTHang],
			[Master_ID],
			[NPL_TuCungUng],
			[TenSanPham],
			[TenNPL],
			[MaDDSX],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DVT_ID,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@GhiChu,
			@STTHang,
			@Master_ID,
			@NPL_TuCungUng,
			@TenSanPham,
			@TenNPL,
			@MaDDSX,
			@DateLog,
			@Status
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Log_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_DinhMuc_Log]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Log_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_DinhMuc_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Log_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_DinhMuc_Log]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Log_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_GC_DinhMuc_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Log_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Log_SelectAll]















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_DinhMuc_Log]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LogKhaiBao_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LogKhaiBao_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LogKhaiBao_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LogKhaiBao_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LogKhaiBao_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LogKhaiBao_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Insert]
	@LoaiKhaiBao varchar(50),
	@ID_DK bigint,
	@GUIDSTR_DK nvarchar(255),
	@UserNameKhaiBao nvarchar(50),
	@NgayKhaiBao datetime,
	@UserNameSuaDoi nvarchar(50),
	@NgaySuaDoi datetime,
	@GhiChu nvarchar(255),
	@IsDelete bit,
	@IDLog bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_LogKhaiBao]
(
	[LoaiKhaiBao],
	[ID_DK],
	[GUIDSTR_DK],
	[UserNameKhaiBao],
	[NgayKhaiBao],
	[UserNameSuaDoi],
	[NgaySuaDoi],
	[GhiChu],
	[IsDelete]
)
VALUES 
(
	@LoaiKhaiBao,
	@ID_DK,
	@GUIDSTR_DK,
	@UserNameKhaiBao,
	@NgayKhaiBao,
	@UserNameSuaDoi,
	@NgaySuaDoi,
	@GhiChu,
	@IsDelete
)

SET @IDLog = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Update]
	@IDLog bigint,
	@LoaiKhaiBao varchar(50),
	@ID_DK bigint,
	@GUIDSTR_DK nvarchar(255),
	@UserNameKhaiBao nvarchar(50),
	@NgayKhaiBao datetime,
	@UserNameSuaDoi nvarchar(50),
	@NgaySuaDoi datetime,
	@GhiChu nvarchar(255),
	@IsDelete bit
AS

UPDATE
	[dbo].[t_KDT_SXXK_LogKhaiBao]
SET
	[LoaiKhaiBao] = @LoaiKhaiBao,
	[ID_DK] = @ID_DK,
	[GUIDSTR_DK] = @GUIDSTR_DK,
	[UserNameKhaiBao] = @UserNameKhaiBao,
	[NgayKhaiBao] = @NgayKhaiBao,
	[UserNameSuaDoi] = @UserNameSuaDoi,
	[NgaySuaDoi] = @NgaySuaDoi,
	[GhiChu] = @GhiChu,
	[IsDelete] = @IsDelete
WHERE
	[IDLog] = @IDLog

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_InsertUpdate]
	@IDLog bigint,
	@LoaiKhaiBao varchar(50),
	@ID_DK bigint,
	@GUIDSTR_DK nvarchar(255),
	@UserNameKhaiBao nvarchar(50),
	@NgayKhaiBao datetime,
	@UserNameSuaDoi nvarchar(50),
	@NgaySuaDoi datetime,
	@GhiChu nvarchar(255),
	@IsDelete bit
AS
IF EXISTS(SELECT [IDLog] FROM [dbo].[t_KDT_SXXK_LogKhaiBao] WHERE [IDLog] = @IDLog)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_LogKhaiBao] 
		SET
			[LoaiKhaiBao] = @LoaiKhaiBao,
			[ID_DK] = @ID_DK,
			[GUIDSTR_DK] = @GUIDSTR_DK,
			[UserNameKhaiBao] = @UserNameKhaiBao,
			[NgayKhaiBao] = @NgayKhaiBao,
			[UserNameSuaDoi] = @UserNameSuaDoi,
			[NgaySuaDoi] = @NgaySuaDoi,
			[GhiChu] = @GhiChu,
			[IsDelete] = @IsDelete
		WHERE
			[IDLog] = @IDLog
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_LogKhaiBao]
		(
			[LoaiKhaiBao],
			[ID_DK],
			[GUIDSTR_DK],
			[UserNameKhaiBao],
			[NgayKhaiBao],
			[UserNameSuaDoi],
			[NgaySuaDoi],
			[GhiChu],
			[IsDelete]
		)
		VALUES 
		(
			@LoaiKhaiBao,
			@ID_DK,
			@GUIDSTR_DK,
			@UserNameKhaiBao,
			@NgayKhaiBao,
			@UserNameSuaDoi,
			@NgaySuaDoi,
			@GhiChu,
			@IsDelete
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Delete]
	@IDLog bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_LogKhaiBao]
WHERE
	[IDLog] = @IDLog

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_LogKhaiBao] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Load]
	@IDLog bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDLog],
	[LoaiKhaiBao],
	[ID_DK],
	[GUIDSTR_DK],
	[UserNameKhaiBao],
	[NgayKhaiBao],
	[UserNameSuaDoi],
	[NgaySuaDoi],
	[GhiChu],
	[IsDelete]
FROM
	[dbo].[t_KDT_SXXK_LogKhaiBao]
WHERE
	[IDLog] = @IDLog
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[IDLog],
	[LoaiKhaiBao],
	[ID_DK],
	[GUIDSTR_DK],
	[UserNameKhaiBao],
	[NgayKhaiBao],
	[UserNameSuaDoi],
	[NgaySuaDoi],
	[GhiChu],
	[IsDelete]
FROM [dbo].[t_KDT_SXXK_LogKhaiBao] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDLog],
	[LoaiKhaiBao],
	[ID_DK],
	[GUIDSTR_DK],
	[UserNameKhaiBao],
	[NgayKhaiBao],
	[UserNameSuaDoi],
	[NgaySuaDoi],
	[GhiChu],
	[IsDelete]
FROM
	[dbo].[t_KDT_SXXK_LogKhaiBao]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_SelectBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_SelectBy_Master_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Log_DeleteBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_DeleteBy_Master_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_Insert]
	@MaHang varchar(max),
	@TenHang nvarchar(max),
	@MaHS varchar(12),
	@ThongTinCu nvarchar(255),
	@SoLuong numeric(18, 5),
	@DonGia float,
	@DVT_ID char(3),
	@NuocXX_ID char(3),
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(255),
	@Master_ID bigint,
	@NhomSP varchar(50),
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_HangPhuKien_Log]
(
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP],
	[DateLog],
	[Status]
)
VALUES 
(
	@MaHang,
	@TenHang,
	@MaHS,
	@ThongTinCu,
	@SoLuong,
	@DonGia,
	@DVT_ID,
	@NuocXX_ID,
	@TriGia,
	@NguyenTe_ID,
	@STTHang,
	@TinhTrang,
	@Master_ID,
	@NhomSP,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_Update]
	@ID bigint,
	@MaHang varchar(max),
	@TenHang nvarchar(max),
	@MaHS varchar(12),
	@ThongTinCu nvarchar(255),
	@SoLuong numeric(18, 5),
	@DonGia float,
	@DVT_ID char(3),
	@NuocXX_ID char(3),
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(255),
	@Master_ID bigint,
	@NhomSP varchar(50),
	@DateLog datetime,
	@Status nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_HangPhuKien_Log]
SET
	[MaHang] = @MaHang,
	[TenHang] = @TenHang,
	[MaHS] = @MaHS,
	[ThongTinCu] = @ThongTinCu,
	[SoLuong] = @SoLuong,
	[DonGia] = @DonGia,
	[DVT_ID] = @DVT_ID,
	[NuocXX_ID] = @NuocXX_ID,
	[TriGia] = @TriGia,
	[NguyenTe_ID] = @NguyenTe_ID,
	[STTHang] = @STTHang,
	[TinhTrang] = @TinhTrang,
	[Master_ID] = @Master_ID,
	[NhomSP] = @NhomSP,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_InsertUpdate]
	@ID bigint,
	@MaHang varchar(max),
	@TenHang nvarchar(max),
	@MaHS varchar(12),
	@ThongTinCu nvarchar(255),
	@SoLuong numeric(18, 5),
	@DonGia float,
	@DVT_ID char(3),
	@NuocXX_ID char(3),
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(255),
	@Master_ID bigint,
	@NhomSP varchar(50),
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_HangPhuKien_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_HangPhuKien_Log] 
		SET
			[MaHang] = @MaHang,
			[TenHang] = @TenHang,
			[MaHS] = @MaHS,
			[ThongTinCu] = @ThongTinCu,
			[SoLuong] = @SoLuong,
			[DonGia] = @DonGia,
			[DVT_ID] = @DVT_ID,
			[NuocXX_ID] = @NuocXX_ID,
			[TriGia] = @TriGia,
			[NguyenTe_ID] = @NguyenTe_ID,
			[STTHang] = @STTHang,
			[TinhTrang] = @TinhTrang,
			[Master_ID] = @Master_ID,
			[NhomSP] = @NhomSP,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_HangPhuKien_Log]
		(
			[MaHang],
			[TenHang],
			[MaHS],
			[ThongTinCu],
			[SoLuong],
			[DonGia],
			[DVT_ID],
			[NuocXX_ID],
			[TriGia],
			[NguyenTe_ID],
			[STTHang],
			[TinhTrang],
			[Master_ID],
			[NhomSP],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@MaHang,
			@TenHang,
			@MaHS,
			@ThongTinCu,
			@SoLuong,
			@DonGia,
			@DVT_ID,
			@NuocXX_ID,
			@TriGia,
			@NguyenTe_ID,
			@STTHang,
			@TinhTrang,
			@Master_ID,
			@NhomSP,
			@DateLog,
			@Status
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_HangPhuKien_Log]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_DeleteBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_DeleteBy_Master_ID]
	@Master_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_HangPhuKien_Log]
WHERE
	[Master_ID] = @Master_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_HangPhuKien_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_HangPhuKien_Log]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_SelectBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_HangPhuKien_Log]
WHERE
	[Master_ID] = @Master_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_GC_HangPhuKien_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Log_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Log_SelectAll]

















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_HangPhuKien_Log]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_LoaiPhuKien_Log_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_LoaiPhuKien_Log_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_LoaiPhuKien_Log_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_LoaiPhuKien_Log_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_LoaiPhuKien_Log_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_LoaiPhuKien_Log_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_LoaiPhuKien_Log_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_LoaiPhuKien_Log_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_LoaiPhuKien_Log_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_Insert]
	@MaPhuKien char(4),
	@NoiDung nvarchar(255),
	@ThongTinMoi varchar(50),
	@Master_ID bigint,
	@ThongTinCu varchar(50),
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_LoaiPhuKien_Log]
(
	[MaPhuKien],
	[NoiDung],
	[ThongTinMoi],
	[Master_ID],
	[ThongTinCu],
	[DateLog],
	[Status]
)
VALUES 
(
	@MaPhuKien,
	@NoiDung,
	@ThongTinMoi,
	@Master_ID,
	@ThongTinCu,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_LoaiPhuKien_Log_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_Update]
	@ID bigint,
	@MaPhuKien char(4),
	@NoiDung nvarchar(255),
	@ThongTinMoi varchar(50),
	@Master_ID bigint,
	@ThongTinCu varchar(50),
	@DateLog datetime,
	@Status nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_LoaiPhuKien_Log]
SET
	[MaPhuKien] = @MaPhuKien,
	[NoiDung] = @NoiDung,
	[ThongTinMoi] = @ThongTinMoi,
	[Master_ID] = @Master_ID,
	[ThongTinCu] = @ThongTinCu,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_LoaiPhuKien_Log_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_InsertUpdate]
	@ID bigint,
	@MaPhuKien char(4),
	@NoiDung nvarchar(255),
	@ThongTinMoi varchar(50),
	@Master_ID bigint,
	@ThongTinCu varchar(50),
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_LoaiPhuKien_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_LoaiPhuKien_Log] 
		SET
			[MaPhuKien] = @MaPhuKien,
			[NoiDung] = @NoiDung,
			[ThongTinMoi] = @ThongTinMoi,
			[Master_ID] = @Master_ID,
			[ThongTinCu] = @ThongTinCu,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_LoaiPhuKien_Log]
		(
			[MaPhuKien],
			[NoiDung],
			[ThongTinMoi],
			[Master_ID],
			[ThongTinCu],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@MaPhuKien,
			@NoiDung,
			@ThongTinMoi,
			@Master_ID,
			@ThongTinCu,
			@DateLog,
			@Status
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_LoaiPhuKien_Log_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_LoaiPhuKien_Log]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_LoaiPhuKien_Log_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_LoaiPhuKien_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_LoaiPhuKien_Log_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaPhuKien],
	[NoiDung],
	[ThongTinMoi],
	[Master_ID],
	[ThongTinCu],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_LoaiPhuKien_Log]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_LoaiPhuKien_Log_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaPhuKien],
	[NoiDung],
	[ThongTinMoi],
	[Master_ID],
	[ThongTinCu],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_GC_LoaiPhuKien_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_LoaiPhuKien_Log_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_LoaiPhuKien_Log_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaPhuKien],
	[NoiDung],
	[ThongTinMoi],
	[Master_ID],
	[ThongTinCu],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_LoaiPhuKien_Log]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Log_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Log_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Log_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Log_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Log_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Log_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Log_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Log_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Log_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_Insert]
	@Master_ID bigint,
	@Ma varchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@STTHang int,
	@GhiChu nvarchar(1000),
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_HangMau_Log]
(
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[GhiChu],
	[DateLog],
	[Status]
)
VALUES 
(
	@Master_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@STTHang,
	@GhiChu,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Log_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_Update]
	@ID bigint,
	@Master_ID bigint,
	@Ma varchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@STTHang int,
	@GhiChu nvarchar(1000),
	@DateLog datetime,
	@Status nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_HangMau_Log]
SET
	[Master_ID] = @Master_ID,
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[STTHang] = @STTHang,
	[GhiChu] = @GhiChu,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Log_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@Ma varchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@STTHang int,
	@GhiChu nvarchar(1000),
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_HangMau_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_HangMau_Log] 
		SET
			[Master_ID] = @Master_ID,
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[STTHang] = @STTHang,
			[GhiChu] = @GhiChu,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_HangMau_Log]
		(
			[Master_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[STTHang],
			[GhiChu],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@Master_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@STTHang,
			@GhiChu,
			@DateLog,
			@Status
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Log_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_HangMau_Log]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Log_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_HangMau_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Log_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[GhiChu],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_HangMau_Log]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Log_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[GhiChu],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_GC_HangMau_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Log_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Log_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[GhiChu],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_HangMau_Log]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Log_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Log_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Log_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Log_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Log_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Log_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Log_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Log_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Insert]
	@Master_ID bigint,
	@Ma varchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@STTHang int,
	@DonGia numeric(18, 5),
	@GhiChu nvarchar(256),
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_NguyenPhuLieu_Log]
(
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[DonGia],
	[GhiChu],
	[DateLog],
	[Status]
)
VALUES 
(
	@Master_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@STTHang,
	@DonGia,
	@GhiChu,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Update]
	@ID bigint,
	@Master_ID bigint,
	@Ma varchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@STTHang int,
	@DonGia numeric(18, 5),
	@GhiChu nvarchar(256),
	@DateLog datetime,
	@Status nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_NguyenPhuLieu_Log]
SET
	[Master_ID] = @Master_ID,
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[STTHang] = @STTHang,
	[DonGia] = @DonGia,
	[GhiChu] = @GhiChu,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Log_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@Ma varchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@STTHang int,
	@DonGia numeric(18, 5),
	@GhiChu nvarchar(256),
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_NguyenPhuLieu_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_NguyenPhuLieu_Log] 
		SET
			[Master_ID] = @Master_ID,
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[STTHang] = @STTHang,
			[DonGia] = @DonGia,
			[GhiChu] = @GhiChu,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_NguyenPhuLieu_Log]
		(
			[Master_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[STTHang],
			[DonGia],
			[GhiChu],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@Master_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@STTHang,
			@DonGia,
			@GhiChu,
			@DateLog,
			@Status
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_NguyenPhuLieu_Log]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Log_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_NguyenPhuLieu_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[DonGia],
	[GhiChu],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_NguyenPhuLieu_Log]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Log_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[DonGia],
	[GhiChu],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_GC_NguyenPhuLieu_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Log_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Log_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[DonGia],
	[GhiChu],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_NguyenPhuLieu_Log]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Log_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Log_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Log_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Log_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Log_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Log_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Log_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Log_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Log_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_Insert]
	@Master_ID bigint,
	@MaSanPham varchar(50),
	@SoLuong numeric(18, 5),
	@GiaGiaCong numeric(18, 5),
	@STTHang int,
	@TenSanPham nvarchar(255),
	@TriGia float,
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_NhomSanPham_Log]
(
	[Master_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TriGia],
	[DateLog],
	[Status]
)
VALUES 
(
	@Master_ID,
	@MaSanPham,
	@SoLuong,
	@GiaGiaCong,
	@STTHang,
	@TenSanPham,
	@TriGia,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Log_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaSanPham varchar(50),
	@SoLuong numeric(18, 5),
	@GiaGiaCong numeric(18, 5),
	@STTHang int,
	@TenSanPham nvarchar(255),
	@TriGia float,
	@DateLog datetime,
	@Status nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_NhomSanPham_Log]
SET
	[Master_ID] = @Master_ID,
	[MaSanPham] = @MaSanPham,
	[SoLuong] = @SoLuong,
	[GiaGiaCong] = @GiaGiaCong,
	[STTHang] = @STTHang,
	[TenSanPham] = @TenSanPham,
	[TriGia] = @TriGia,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Log_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaSanPham varchar(50),
	@SoLuong numeric(18, 5),
	@GiaGiaCong numeric(18, 5),
	@STTHang int,
	@TenSanPham nvarchar(255),
	@TriGia float,
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_NhomSanPham_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_NhomSanPham_Log] 
		SET
			[Master_ID] = @Master_ID,
			[MaSanPham] = @MaSanPham,
			[SoLuong] = @SoLuong,
			[GiaGiaCong] = @GiaGiaCong,
			[STTHang] = @STTHang,
			[TenSanPham] = @TenSanPham,
			[TriGia] = @TriGia,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_NhomSanPham_Log]
		(
			[Master_ID],
			[MaSanPham],
			[SoLuong],
			[GiaGiaCong],
			[STTHang],
			[TenSanPham],
			[TriGia],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@Master_ID,
			@MaSanPham,
			@SoLuong,
			@GiaGiaCong,
			@STTHang,
			@TenSanPham,
			@TriGia,
			@DateLog,
			@Status
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Log_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_NhomSanPham_Log]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Log_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_NhomSanPham_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Log_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TriGia],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_NhomSanPham_Log]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Log_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TriGia],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_GC_NhomSanPham_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Log_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Log_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TriGia],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_NhomSanPham_Log]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Log_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Log_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Log_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Log_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Log_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Log_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Log_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Log_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Log_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_Insert]
	@Master_ID bigint,
	@Ma varchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@NhomSanPham_ID varchar(12),
	@STTHang int,
	@DonGia numeric(18, 5),
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_SanPham_Log]
(
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NhomSanPham_ID],
	[STTHang],
	[DonGia],
	[DateLog],
	[Status]
)
VALUES 
(
	@Master_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@NhomSanPham_ID,
	@STTHang,
	@DonGia,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Log_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_Update]
	@ID bigint,
	@Master_ID bigint,
	@Ma varchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@NhomSanPham_ID varchar(12),
	@STTHang int,
	@DonGia numeric(18, 5),
	@DateLog datetime,
	@Status nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_SanPham_Log]
SET
	[Master_ID] = @Master_ID,
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[NhomSanPham_ID] = @NhomSanPham_ID,
	[STTHang] = @STTHang,
	[DonGia] = @DonGia,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Log_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@Ma varchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@NhomSanPham_ID varchar(12),
	@STTHang int,
	@DonGia numeric(18, 5),
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_SanPham_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_SanPham_Log] 
		SET
			[Master_ID] = @Master_ID,
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[NhomSanPham_ID] = @NhomSanPham_ID,
			[STTHang] = @STTHang,
			[DonGia] = @DonGia,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_SanPham_Log]
		(
			[Master_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[NhomSanPham_ID],
			[STTHang],
			[DonGia],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@Master_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@NhomSanPham_ID,
			@STTHang,
			@DonGia,
			@DateLog,
			@Status
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Log_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_SanPham_Log]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Log_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_SanPham_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Log_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NhomSanPham_ID],
	[STTHang],
	[DonGia],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_SanPham_Log]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Log_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NhomSanPham_ID],
	[STTHang],
	[DonGia],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_GC_SanPham_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Log_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Log_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NhomSanPham_ID],
	[STTHang],
	[DonGia],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_SanPham_Log]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Log_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Log_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Log_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Log_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Log_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Log_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Log_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Log_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Log_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_Insert]
	@Master_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@NuocXX_ID char(3),
	@DonGia float,
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(50),
	@GhiChu nvarchar(256),
	@DateLog datetime,
	@Status nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_ThietBi_Log]
(
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NuocXX_ID],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[GhiChu],
	[DateLog],
	[Status]
)
VALUES 
(
	@Master_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@NuocXX_ID,
	@DonGia,
	@TriGia,
	@NguyenTe_ID,
	@STTHang,
	@TinhTrang,
	@GhiChu,
	@DateLog,
	@Status
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Log_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_Update]
	@ID bigint,
	@Master_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@NuocXX_ID char(3),
	@DonGia float,
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(50),
	@GhiChu nvarchar(256),
	@DateLog datetime,
	@Status nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_ThietBi_Log]
SET
	[Master_ID] = @Master_ID,
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[NuocXX_ID] = @NuocXX_ID,
	[DonGia] = @DonGia,
	[TriGia] = @TriGia,
	[NguyenTe_ID] = @NguyenTe_ID,
	[STTHang] = @STTHang,
	[TinhTrang] = @TinhTrang,
	[GhiChu] = @GhiChu,
	[DateLog] = @DateLog,
	[Status] = @Status
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Log_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@NuocXX_ID char(3),
	@DonGia float,
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(50),
	@GhiChu nvarchar(256),
	@DateLog datetime,
	@Status nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_ThietBi_Log] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_ThietBi_Log] 
		SET
			[Master_ID] = @Master_ID,
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[NuocXX_ID] = @NuocXX_ID,
			[DonGia] = @DonGia,
			[TriGia] = @TriGia,
			[NguyenTe_ID] = @NguyenTe_ID,
			[STTHang] = @STTHang,
			[TinhTrang] = @TinhTrang,
			[GhiChu] = @GhiChu,
			[DateLog] = @DateLog,
			[Status] = @Status
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_ThietBi_Log]
		(
			[Master_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[NuocXX_ID],
			[DonGia],
			[TriGia],
			[NguyenTe_ID],
			[STTHang],
			[TinhTrang],
			[GhiChu],
			[DateLog],
			[Status]
		)
		VALUES 
		(
			@Master_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@NuocXX_ID,
			@DonGia,
			@TriGia,
			@NguyenTe_ID,
			@STTHang,
			@TinhTrang,
			@GhiChu,
			@DateLog,
			@Status
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Log_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_ThietBi_Log]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Log_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_ThietBi_Log] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Log_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NuocXX_ID],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[GhiChu],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_ThietBi_Log]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Log_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NuocXX_ID],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[GhiChu],
	[DateLog],
	[Status]
FROM [dbo].[t_KDT_GC_ThietBi_Log] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Log_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Log_SelectAll]
















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NuocXX_ID],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[GhiChu],
	[DateLog],
	[Status]
FROM
	[dbo].[t_KDT_GC_ThietBi_Log]	

GO
ALTER PROCEDURE [dbo].[p_GC_SanPham_SelectDynamic]  
 @WhereCondition NVARCHAR(MAX),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT   
 [HopDong_ID],  
 [Ma],  
 [Ten],  
 [MaHS],  
 [DVT_ID],  
 [SoLuongDangKy],  
 [SoLuongDaXuat],  
 [NhomSanPham_ID],  
 [STTHang],  
 [TrangThai],  
 [DonGia]  
FROM [dbo].[t_GC_SanPham]   
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('41.1',GETDATE(), N'CẬP NHẬT LOG HỢP ĐỒNG GC')
END