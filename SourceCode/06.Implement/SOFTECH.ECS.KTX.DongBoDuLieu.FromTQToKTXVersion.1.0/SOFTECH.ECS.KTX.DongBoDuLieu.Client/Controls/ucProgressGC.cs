﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DongBoDuLieu.BLL.SXXK;
using System.Security.Cryptography;

namespace DongBo.Controls
{
    public partial class ucProgressGC : ucDesign
    {
        frmMain frmParent = null;
        delegate void SetTextCallback(string text);
        private int value = 0;
        private int step = 1;

        public ucProgressGC()
        {
            InitializeComponent();
            frmParent = (frmMain)Application.OpenForms["frmMain"];
        }

        private void ucProgressGC_Load(object sender, EventArgs e)
        {
            this.step = 1;
            this.validControl();
            if (!frmParent.btnNext.Enabled)
            {
                btnXulylai.Enabled = false;
                bgWorker.RunWorkerAsync();
            }
            else
            {
                pBar.Style = ProgressBarStyle.Blocks;
                pBar.Value = pBar.Maximum;
                this.setText("Dữ liệu đã được xử lý");
            }
        }

        private void setText(string str)
        {
            if (lblCaption.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(setText);
                this.Invoke(d, new object[] { str });
            }
            else
            {
                lblContent.Text = str;
            }
        }

        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }

        private void QueryData_KTX(string passDN)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                this.setText("Lỗi: " + ex.Message);
            }
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string passDN = this.GetMD5Value(GlobalSettings.MaDoanhNghiep + "+" + GlobalSettings.MatKhau);

                if (GlobalSettings.PhienBan == "KTX")
                    QueryData_KTX(passDN);
                //else if (GlobalSettings.PhienBan == "TQDT")
                //    QueryData_TQDT(passDN);
            }
            catch
            {
                this.setText("Xử lý không thành công");
                //btnXulylai.Enabled = true;
            }
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.step = 5;
            this.value = 100;
            pBar.Style = ProgressBarStyle.Blocks;
            pBar.Value = pBar.Maximum;
            this.setText("Đã hoàn thành việc xử lý dữ liệu");
            this.validControl();
            //btnXulylai.Enabled = true;
        }

        private void btnXulylai_Click(object sender, EventArgs e)
        {
            GlobalSettings.dsHQ = new DataSet[] { null, null, null, null, null };
            btnXulylai.Enabled = false;
            if (frmParent.btnNext.Enabled) frmParent.btnNext.Enabled = false;
            pBar.Style = ProgressBarStyle.Marquee;
            bgWorker.RunWorkerAsync();
        }

        private void validControl()
        {
            if (GlobalSettings.dsHQ[3] != null && GlobalSettings.dsHQ[0] != null && GlobalSettings.dsHQ[2] != null && GlobalSettings.dsHQ[1] != null && GlobalSettings.dsHQ[4] != null)
            {
                frmParent.btnNext.Enabled = true;
            }
            else
            {
                frmParent.btnNext.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.value < 100)
            {
                this.value += this.step;
                if (value > 99)
                    value = 100;

            }
            else
            {
                if (pBar.Value < 99) this.value = pBar.Value + 1;
            }
            pBar.Value = this.value;
        }

    }
}
