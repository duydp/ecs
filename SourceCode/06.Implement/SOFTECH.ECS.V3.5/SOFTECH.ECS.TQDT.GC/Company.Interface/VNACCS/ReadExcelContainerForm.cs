﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.VNACCS
{
    public partial class ReadExcelContainerForm : Company.Interface.BaseForm
    {
        public KDT_ContainerDangKy ContDK;
        public KDT_VNACCS_BranchDetail BranchDetai = null;
        public string Type;
        public bool ImportSuccess = false;
        public ReadExcelContainerForm()
        {
            InitializeComponent();
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                // Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //showMsg("MSG_0203008");
                    //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                //Dictionary<string, Worksheet> dict = new Dictionary<string, Worksheet>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                    //dict.Add(worksheet.Name, worksheet);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            //cvError.Validate();
            //if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRowIndex.Value) - 1;
            if (beginRow <= 0)
            {
                return;

            }
            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char SoVanDonColumn = Convert.ToChar(txtSoVanDon.Text.Trim());
            int SoVanDonCol = ConvertCharToInt(SoVanDonColumn);

            char SoContainerColumn = Convert.ToChar(txtSoCont.Text.Trim());
            int SoContainerCol = ConvertCharToInt(SoContainerColumn);

            char SoSealColumn = Convert.ToChar(txtSoSeal.Text.Trim());
            int SoSealCol = ConvertCharToInt(SoSealColumn);

            char GhiChuColumn = Convert.ToChar(txtGhiChu.Text.Trim());
            int GhiChuCol = ConvertCharToInt(GhiChuColumn);

            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        if (Type == "TKMD")
                        {
                            KDT_ContainerBS containerBS = new KDT_ContainerBS();
                            containerBS.SoVanDon = Convert.ToString(wsr.Cells[SoVanDonCol].Value);
                            if (containerBS.SoVanDon.ToString().Length == 0)
                            {
                                MLMessages("Số vận đơn ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }
                            containerBS.SoContainer = Convert.ToString(wsr.Cells[SoContainerCol].Value).Trim();
                            if (containerBS.SoContainer.Trim().Length == 0)
                            {
                                MLMessages("Số Container ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }
                            containerBS.SoSeal = Convert.ToString(wsr.Cells[SoSealCol].Value).Trim();
                            if (containerBS.SoSeal.Trim().Length == 0)
                            {
                                MLMessages("Số Seal ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }
                            containerBS.GhiChu = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();
                            ContDK.ListCont.Add(containerBS);
                        }
                        else
                        {
                            KDT_VNACCS_BranchDetail_TransportEquipment transportEquipment = new KDT_VNACCS_BranchDetail_TransportEquipment();
                            transportEquipment.SoContainer = Convert.ToString(wsr.Cells[SoContainerCol].Value).Trim();
                            if (transportEquipment.SoContainer.Trim().Length == 0)
                            {
                                MLMessages("Số Container ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }
                            transportEquipment.SoSeal = Convert.ToString(wsr.Cells[SoSealCol].Value).Trim();
                            if (transportEquipment.SoSeal.Trim().Length == 0)
                            {
                                MLMessages("Số Seal ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }
                            BranchDetai.Collection.Add(transportEquipment);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", "MSG_EXC06", Convert.ToString(wsr.Index + 1), true) != "Yes")
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            return;
                        }
                    }
                }
            }
            if (Type == "TKMD")
            {
                if (ContDK.ListCont.Count > 0)
                {
                    MLMessages("Nhập hàng từ Excel thành công ", "MSG_EXC06", "", false);
                    ImportSuccess = true;
                }
                else
                {
                    MLMessages("Nhập hàng từ Excel không thành công ", "MSG_EXC06", "", false);
                }
            }
            else
            {
                if (BranchDetai.Collection.Count > 0)
                {
                    MLMessages("Nhập hàng từ Excel thành công ", "MSG_EXC06", "", false);
                    ImportSuccess = true;
                }
                else
                {
                    MLMessages("Nhập hàng từ Excel không thành công ", "MSG_EXC06", "", false);
                }
            }
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkFileExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_DSContainerDinhKem();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
