﻿using System;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.VNACCS;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
using System.IO;
#elif GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
using System.IO;
#elif KD_V4
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
using System.IO;
#endif
namespace Company.Interface.VNACCS
{
    public partial class VNACC_COForm : BaseForm
    {
        public KDT_ContainerDangKy ContDK = new KDT_ContainerDangKy();
        private KDT_ContainerBS cont = new KDT_ContainerBS();
        private FeedBackContent feedbackContent = null;
        private Container_VNACCS feedbackContKVGS = null;
        private string msgInfor = string.Empty;
        bool isAdd = true;
        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public VNACC_COForm()
        {
            InitializeComponent();
            
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    Save();
                    break;
                case "cmdSend":
                    SendV5(false);
                    break;
                case "cmdFeedBack":
                    FeedBackV5(false);
                    break;
                case "cmdKetQuaXuLy":
                    KetQuaXuLyPK();
                    break;
            }

        }

        private void Save()
        {
            try
            {
                ContDK.MaHQ = ctrHaiQuan.Code;

                //if (TKMD.SoToKhai.ToString().Substring(1,1)=="3") 
                //{
                    try
                    {
                        ContDK.SoTiepNhan = long.Parse(txtSoTiepNhan.Text);
                    }
                    catch (Exception ex)
                    {
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi lưu container liệu phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                    }
                    
                //}
                ContDK.InsertUpdateFull();
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void LoadData()
        {
            
            dgList.DataSource = ContDK.ListCont;
            dgList.Refetch();
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            cont.SoContainer = txtToChucCap.Text;
            cont.SoVanDon = txtSoCO.Text;
            cont.SoSeal = txtNguoiCap.Text;
            cont.GhiChu = txtGhiChu.Text;

            if (isAdd)
                ContDK.ListCont.Add(cont);
            isAdd = true;
            cont = new KDT_ContainerBS();
            
            LoadData();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<KDT_ContainerBS> ItemColl = new List<KDT_ContainerBS>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa Container này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_ContainerBS)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_ContainerBS item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        ContDK.ListCont.Remove(item);
                    }

                }
                LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void VNACC_COForm_Load(object sender, EventArgs e)
        {
            txtSoTiepNhan.Text = ContDK.SoTiepNhan.ToString();
            clcNgayTN.Value = ContDK.NgayTiepNhan;
            //ctrHaiQuan.Code = ContDK.MaHQ;
            ctrHaiQuan.Code = TKMD.LoadToKhai(ContDK.TKMD_ID).CoQuanHaiQuan;
            if (ContDK.ID == 0)
                ContDK.TrangThaiXuLy = -1;
            if (ContDK.TrangThaiXuLy == -1)
                lblTrangThai.Text = "Chưa khai báo";
            else if (ContDK.TrangThaiXuLy == 0)
                lblTrangThai.Text = "Chờ duyệt";
            else if (ContDK.TrangThaiXuLy == 1)
                lblTrangThai.Text = "Đã duyệt";
            else if (ContDK.TrangThaiXuLy == 2)
                lblTrangThai.Text = "Không phê duyệt";

            LoadData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.BSContainerSendHandler(ContDK, ref msgInfor, e);
        }
        void SendKVGSHandler(object sender, SendEventArgs e)
        {
            feedbackContKVGS = SingleMessage.ContainerVnaccsSendHandler(ContDK, ref msgInfor, e);
        }
        void SendMessageKVGS(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendKVGSHandler),
                sender, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendContKVGS()
        {
            if (ShowMessage("Bạn có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {
                if (ContDK.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    ContDK = KDT_ContainerDangKy.LoadFull(ContDK.ID);
                }
                MsgSend sendXML = new MsgSend();
                
                sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;
                
                sendXML.master_id = ContDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    if (this.ContDK.ListCont.Count > 0)
                    {

                        string returnMessage = string.Empty;
                        ContDK.GuidStr = Guid.NewGuid().ToString();
                        //Container_VNACCS contVNACCS = new Container_VNACCS();
                        //contVNACCS = Mapper_V4.ToDataTransferContainer(ContDK, TKMD, GlobalSettings.TEN_DON_VI, xacnhanKVGS);


                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = ContDK.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ)),
                                  // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(ContDK.MaHQ).Trim() : ContDK.MaHQ
                                  Identity = ContDK.MaHQ
                              },
                              new SubjectBase()
                              {

                                  Type = DeclarationIssuer.Container_KVGS,
                                  Function = DeclarationFunction.KHAI_BAO,
                                  Reference = ContDK.GuidStr,
                              },
                              null
                            );
                        
                            msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                            // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(ContDK.MaHQ).Trim() : ContDK.MaHQ
                            msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                        
                        ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessageKVGS;
                        bool isSend = sendForm.DoSend(msgSend);
                        //if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        if (isSend )//&& feedbackContKVGS.Function == DeclarationFunction.THONG_QUAN)
                        {
                            sendForm.Message.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoBSContainer);
                            cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            btnAdd.Enabled = false;
                            btnView.Enabled = false;
                            
                            sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;
                            
                            sendXML.master_id = ContDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            ContDK.Update();
                            
                            Hoi_ContKVGS();
                            
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        private void SendV5(bool xacnhanKVGS)
        {
            if (ShowMessage("Bạn có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {
                if (ContDK.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    ContDK = KDT_ContainerDangKy.LoadFull(ContDK.ID);
                }
                MsgSend sendXML = new MsgSend();
                
                    sendXML.LoaiHS = LoaiKhaiBao.Container;
                
                sendXML.master_id = ContDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    if (this.ContDK.ListCont.Count > 0)
                    {

                        string returnMessage = string.Empty;
                        ContDK.GuidStr = Guid.NewGuid().ToString();
                        Container_VNACCS contVNACCS = new Container_VNACCS();
                        contVNACCS = Mapper_V4.ToDataTransferContainer(ContDK, TKMD, GlobalSettings.TEN_DON_VI, xacnhanKVGS);
                       
                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = ContDK.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ)),
                                  // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(ContDK.MaHQ).Trim() : ContDK.MaHQ
                                  Identity = ContDK.MaHQ
                              },
                              new SubjectBase()
                              {

                                  Type = contVNACCS.Issuer,
                                  Function = DeclarationFunction.KHAI_BAO,
                                  Reference = ContDK.GuidStr,
                              },
                              contVNACCS
                            );
                        ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoBSContainer);
                            cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            btnAdd.Enabled = false;
                            btnView.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.Container;
                            sendXML.master_id = ContDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();
                            ContDK.Update();
                            FeedBackV5(false);
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
        }
        public static Container_VNACCS ToDataTransferContainerKVGS(KDT_ContainerDangKy ContDK, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep)
        {
            string reference = string.Empty;
            if (string.IsNullOrEmpty(ContDK.GuidStr))
            {
                reference = Guid.NewGuid().ToString();
            }
            else
            {
                reference = ContDK.GuidStr;
            }
            Container_VNACCS Cont = new Container_VNACCS()
            {
                Issuer = DeclarationIssuer.Container_KVGS,
                Reference = reference,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = "",
                Status = "",
                CustomsReference = TKMD.SoToKhai.ToString(),
                Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime),
                DeclarationOffice = TKMD.CoQuanHaiQuan,
                NatureOfTransaction = TKMD.MaLoaiHinh,
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Identity = ContDK.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                }

            };
            Cont.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = ContDK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            return Cont;
        }
        private void FeedBackV5(bool KVGS)
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = ContDK.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.Container_KVGS,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.Container_KVGS,
                };
                if (KVGS)
                    subjectBase.Type = DeclarationIssuer.Container_KVGS;
                else
                    subjectBase.Type = DeclarationIssuer.Container;
                
                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = ContDK.MaDoanhNghiep,
                                                
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ.Trim())),
                                                  Identity = ContDK.MaHQ
                                              }, subjectBase, null);
                if (ContDK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
        }
        private void Hoi_ContKVGS()
        {
            #region Hỏi phải hổi KVGS
            if (ShowMessage("Bạn có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {
                if (ContDK.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    ContDK = KDT_ContainerDangKy.LoadFull(ContDK.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;
                sendXML.master_id = ContDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    if (this.ContDK.ListCont.Count > 0)
                    {

                        string returnMessage = string.Empty;

                        string reference = ContDK.GuidStr;

                        Container_VNACCS contVNACCS = new Container_VNACCS();
                        contVNACCS = ToDataTransferContainerKVGS(ContDK, TKMD, GlobalSettings.TEN_DON_VI);


                        //SubjectBase subjectBase = new SubjectBase()
                        //{
                        //    Issuer = DeclarationIssuer.Container_KVGS,
                        //    Reference = reference,
                        //    Issue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        //    Function = DeclarationFunction.KHAI_BAO,
                        //    IssueLocation = "",
                        //    Status = "",
                        //    CustomsReference = TKMD.SoToKhai.ToString(),
                        //    Acceptance = TKMD.NgayDangKy.ToString("yyyy-MM-dd HH:mm:ss"),
                        //    DeclarationOffice = TKMD.CoQuanHaiQuan,
                        //    NatureOfTransaction = TKMD.MaLoaiHinh
                        //};
                        SubjectBase subjectBase = new SubjectBase()
                        {
                            Issuer = DeclarationIssuer.Container_KVGS,
                            Reference = reference,
                            Function = DeclarationFunction.KHAI_BAO,
                            Type = DeclarationIssuer.Container_KVGS,
                        };
                        ObjectSend msgSend = new ObjectSend(
                                                    new NameBase()
                                                    {
                                                        Name = GlobalSettings.TEN_DON_VI,
                                                        Identity = ContDK.MaDoanhNghiep,
                                                        Status = "3"
                                                    },
                                                      new NameBase()
                                                      {
                                                          Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ.Trim())),
                                                          Identity = ContDK.MaHQ
                                                      }, subjectBase, contVNACCS);
                        if (ContDK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                            msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                        }

                        ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessageKVGS;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend)// && feedbackContKVGS.Function == "32")
                        {
                            sendForm.Message.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoBSContainer);
                            cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            btnAdd.Enabled = false;
                            btnView.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;
                            sendXML.master_id = ContDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            ContDK.Update();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
                }
            }
            #endregion
        }
        private void KetQuaXuLyPK()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.ContDK.ID;
            form.DeclarationIssuer = DeclarationIssuer.Container;
            form.ShowDialog(this);
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                cont = new KDT_ContainerBS();
                cont = (KDT_ContainerBS)e.Row.DataRow;
                txtToChucCap.Text = cont.SoContainer;
                txtSoCO.Text = cont.SoVanDon;
                txtNguoiCap.Text = cont.SoSeal;
                txtGhiChu.Text = cont.GhiChu;
                isAdd = false;
            }
        }
    }
}
