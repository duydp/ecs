using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.SXXK.BangKeThanhLy
{
    public partial class frmThemTKBK01 : BaseForm
    {
        public string MaNPL = "";
        ToKhaiMauDichCollection TKNColl = new ToKhaiMauDichCollection();
        public BKToKhaiNhapCollection BKTKNColl = new BKToKhaiNhapCollection();
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        BKToKhaiNhapCollection BKBandau = new BKToKhaiNhapCollection();

        public frmThemTKBK01()
        {
            InitializeComponent();
        }

        private void GetDSTKN()
        {
            TKNColl = new ToKhaiMauDichCollection();
            string dyn = " MaNPL = '" + this.MaNPL + "' AND MaHaiQuan like '%" + GlobalSettings.MA_HAI_QUAN.Trim() + "%' AND Ton > 0 ";
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection nplNTColl = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon().SelectCollectionDynamic(dyn, "MaNPL");

            foreach (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplNT in nplNTColl)
            {
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.MaHaiQuan = nplNT.MaHaiQuan;
                tkmd.SoToKhai = (int)nplNT.SoToKhai;

                tkmd.LoaiVanDon = nplNT.SoToKhaiVNACCS.ToString();

                tkmd.MaLoaiHinh = nplNT.MaLoaiHinh;
                if (!nplNT.MaLoaiHinh.Contains("V"))
                {
                    tkmd.LoaiVanDon = nplNT.SoToKhai.ToString();
                }
                tkmd.NamDangKy = nplNT.NamDangKy;
                if (tkmd.Load())
                {
                    if (tkmd.MaLoaiHinh.Contains("V"))
                        tkmd.GhiChu = nplNT.GhiChu;
                    else
                        tkmd.LoaiVanDon = nplNT.SoToKhai.ToString();
                    if (tkmd.NgayDangKy > ccFromDate.Value && tkmd.NgayDangKy < ccToDate.Value && tkmd.TenChuHang.ToUpper().Contains(txtTenChuHang.Text.ToUpper()))
                    {
                        TKNColl.Add(tkmd);
                        string a=TKNColl[0].GhiChu;
                    }
                }
            }
            ToKhaiMauDichCollection tkmdTemp = new ToKhaiMauDichCollection();

            foreach (BKToKhaiNhap bk in BKBandau)
            {

                ToKhaiMauDich tk = new ToKhaiMauDich();
                tk.MaHaiQuan = bk.MaHaiQuan;
                tk.SoToKhai = bk.SoToKhai;

                tk.LoaiVanDon = bk.SoToKhaiVNACCS.ToString();

                tk.MaLoaiHinh = bk.MaLoaiHinh;
                tk.NamDangKy = bk.NamDangKy;
                if (tk.Load())
                {
                    tkmdTemp.Add(tk);
                }
            }

            //for (int i = 0; i < dgBKTKN.RowCount; i++ )
            //{
            //    BKToKhaiNhap bk = (BKToKhaiNhap)dgBKTKN.GetRow(i).DataRow;
            //    ToKhaiMauDich tk = new ToKhaiMauDich();
            //    tk.MaHaiQuan = bk.MaHaiQuan;
            //    tk.SoToKhai = bk.SoToKhai;
            //    tk.MaLoaiHinh = bk.MaLoaiHinh;
            //    tk.NamDangKy = bk.NamDangKy;
            //    if (tk.Load())
            //    {
            //        tkmdTemp.Add(tk);
            //    }
            //}
            this.DeleteTKN(tkmdTemp);
            this.refreshData();
        }

        private void refreshData()
        {
            dgTKN.DataSource = this.TKNColl;
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }
            dgTKNTL.DataSource = this.BKTKNColl;
            try
            {
                dgTKNTL.Refetch();
            }
            catch
            {
                dgTKNTL.Refresh();
            }
        }

        private void MoveTKN()
        {
            ToKhaiMauDichCollection tkmds = new ToKhaiMauDichCollection();
            foreach (GridEXRow row in dgTKN.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    ToKhaiMauDich tkmd = (ToKhaiMauDich)row.DataRow;
                    BKToKhaiNhap bk = new BKToKhaiNhap();
                    bk.MaHaiQuan = tkmd.MaHaiQuan;
                    bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                    bk.NamDangKy = tkmd.NamDangKy;
                    bk.NgayDangKy = tkmd.NgayDangKy;
                    bk.NgayThucNhap = tkmd.NGAY_THN_THX;
                    bk.SoToKhai = tkmd.SoToKhai;

                    bk.SoToKhaiVNACCS = Convert.ToDecimal(tkmd.LoaiVanDon);
                    if (!tkmd.MaLoaiHinh.Contains("V"))
                    {
                        bk.SoToKhaiVNACCS = tkmd.SoToKhai;
                        bk.GhiChu = tkmd.GhiChu;
                    }
                    bk.GhiChu = tkmd.GhiChu;
                    bk.UserName = MainForm.EcsQuanTri.Identity.Name;
                    this.BKTKNColl.Add(bk);
                    tkmds.Add(tkmd);
                    decimal lt = Decimal.Parse(label16.Text) + this.getLuongTon(tkmd);
                    label16.Text = lt.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                }
            }
            this.DeleteTKN(tkmds);

            this.refreshData();
        }

        private void MoveBKTKN()
        {
            BKToKhaiNhapCollection bks = new BKToKhaiNhapCollection();
            foreach (GridEXRow row in dgTKNTL.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    BKToKhaiNhap bk = (BKToKhaiNhap)row.DataRow;
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.MaHaiQuan = bk.MaHaiQuan;
                    tkmd.SoToKhai = bk.SoToKhai;
                    tkmd.LoaiVanDon = bk.SoToKhaiVNACCS.ToString();
                    tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                    tkmd.NgayDangKy = bk.NgayDangKy;
                    tkmd.NamDangKy = bk.NamDangKy;
                    tkmd.NGAY_THN_THX = bk.NgayThucNhap;
                    if (tkmd.Load())
                    {
                        if (!tkmd.MaLoaiHinh.Contains("V"))
                            tkmd.LoaiVanDon = tkmd.SoToKhai.ToString();
                        tkmd.GhiChu = bk.GhiChu;
                        this.TKNColl.Add(tkmd);
                        bks.Add(bk);
                        decimal lt = Decimal.Parse(label16.Text) - this.getLuongTon(tkmd);
                        label16.Text = lt.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                    }
                }
            }
            this.DeleteBKTKN(bks);
            this.refreshData();
        }
        
        private void DeleteTKN(ToKhaiMauDichCollection tkmds)
        {
            if (this.TKNColl.Count > 0)
            {
                foreach (ToKhaiMauDich tkmd in tkmds)
                {
                    for (int i = 0; i < this.TKNColl.Count; i++)
                    {
                        if (this.TKNColl[i].MaLoaiHinh == tkmd.MaLoaiHinh && this.TKNColl[i].SoToKhai == tkmd.SoToKhai
                            && this.TKNColl[i].NamDangKy == tkmd.NamDangKy && this.TKNColl[i].MaHaiQuan == tkmd.MaHaiQuan)
                        {
                            this.TKNColl.Remove(this.TKNColl[i]);
                        }
                    }
                }
            }
        }

        private void DeleteBKTKN(BKToKhaiNhapCollection bks)
        {
            if (this.BKTKNColl.Count > 0)
            {
                foreach (BKToKhaiNhap bk in bks)
                {
                    for (int i = 0; i < this.BKTKNColl.Count; i++)
                    {
                        if (this.BKTKNColl[i].MaLoaiHinh == bk.MaLoaiHinh && this.BKTKNColl[i].SoToKhai == bk.SoToKhai
                            && this.BKTKNColl[i].NamDangKy == bk.NamDangKy && this.BKTKNColl[i].MaHaiQuan == bk.MaHaiQuan)
                        {
                            this.BKTKNColl.Remove(this.BKTKNColl[i]);
                        }
                    }
                }
            }
        }

        private void frmThemTKBK01_Load(object sender, EventArgs e)
        {
            dgTKN.RootTable.Columns["NGAY_THN_THX"].DefaultValue = DateTime.Today;
            dgTKNTL.RootTable.Columns["NGAY_THN_THX"].DefaultValue = DateTime.Today;
            dgBKTKN.RootTable.Columns["NGAY_THN_THX"].DefaultValue = DateTime.Today;

            this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].LoadChiTietBangKe();

            BKBandau = this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].bkTKNCollection;
            dgBKTKN.DataSource = BKBandau;

            this.GetDSTKN();
           // dgTKN.DataSource = this.TKNColl;
            string ghi = TKNColl[0].GhiChu;
            dgTKNTL.DataSource = this.BKTKNColl;
            this.HSTL.LoadBKCollection();
            
            lblTongSoTK.Text = this.TKNColl.Count.ToString();
            lblLuongTon.Text = Company.BLL.SXXK.ThanhKhoan.NPLNhapTon.getSumTon(this.MaNPL).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
        }

        private void moveRight_Click(object sender, EventArgs e)
        {
            this.MoveTKN();
        }

        private void dgTKN_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["Ngay_THN_THX"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["Ngay_THN_THX"].Text = "";
        }

        private void dgTKNTL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["Ngay_THN_THX"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["Ngay_THN_THX"].Text = "";
        }

        private void moveLeft_Click(object sender, EventArgs e)
        {
            this.MoveBKTKN();
        }

        private decimal getLuongTon(ToKhaiMauDich tkmd)
        {
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon obj = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
            obj.SoToKhai = tkmd.SoToKhai;
            obj.MaLoaiHinh = tkmd.MaLoaiHinh;
            obj.MaHaiQuan = tkmd.MaHaiQuan;
            obj.NamDangKy = tkmd.NamDangKy;
            obj.MaNPL = this.MaNPL;
            if (obj.Load())
            {
                return obj.Ton;
            }
            return 0;
        }

        private void dgTKN_SelectionChanged(object sender, EventArgs e)
        {
            if (dgTKN.SelectedItems.Count > 0)
            {
                ToKhaiMauDich tkmd = (ToKhaiMauDich)dgTKN.SelectedItems[0].GetRow().DataRow;
                lblNhap.Text = this.getLuongTon(tkmd).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            }
        }

        private void btnGoTo_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTKN.Find(dgTKN.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, Convert.ToInt64(txtSoToKhai.Value),-1, 1))
                {

                    dgTKN.CurrentRow.IsChecked = true;
                    this.MoveTKN();
                }
                else
                {
                    MLMessages("Không có tờ khai này trên lưới.", "MSG_THK79", "", false);
                }
            }
            catch
            { }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            if (dgTKNTL.RowCount > 0)
            {
                for (int i = 0; i < dgTKNTL.RowCount; i++)
                {
                    BKToKhaiNhap bk = (BKToKhaiNhap)dgTKNTL.GetRow(i).DataRow;
                    this.BKBandau.Add(bk);
                }

                this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].InsertUpdate_BKTKN(this.BKBandau);
                ShowMessage("Đã chuyển thành công danh sách tờ khai đã chọn vào hồ sơ thanh khoản.", false);
                this.Close();
            }
            else
            {
                ShowMessage("Không có tờ khai nào được chọn.", false);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtTenChuHang_Leave(object sender, EventArgs e)
        {
            this.GetDSTKN();
        }

        private void ccToDate_Leave(object sender, EventArgs e)
        {
            this.GetDSTKN();
        }

        private void ccFromDate_Leave(object sender, EventArgs e)
        {
            this.GetDSTKN();
        }
    }
}