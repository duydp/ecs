using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.DNCX
{
    public partial class HangTonDetailForm : BaseForm
    {
        public KDT_SXXK_HangTonDangKy hangTonDK = new KDT_SXXK_HangTonDangKy();
        private KDT_SXXK_HangTon hangTon = new KDT_SXXK_HangTon();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;

        public HangTonDetailForm()
        {
            InitializeComponent();
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (hangTon != null)
                {
                    GetHang();
                    hangTonDK.hangTonList.Add(hangTon);
                    hangTon = new KDT_SXXK_HangTon();
                    dgList2.DataSource = hangTonDK.hangTonList;
                    dgList2.Refetch();
                    SetHang();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void LoadData()
        {
            if (hangTonDK == null)
            {
                hangTonDK = new KDT_SXXK_HangTonDangKy();
            }
            if (hangTonDK.ID == 0)
            {
                hangTonDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            }
            txtNamQT.Value = hangTonDK.NamQuyetToan == 0 ? 2014 : hangTonDK.NamQuyetToan;
            cbbQuy.SelectedValue = hangTonDK.QuyQuyetToan == 0 ? 1 : hangTonDK.QuyQuyetToan;
            dgList2.DataSource = hangTonDK.hangTonList;
            dgList2.Refetch();

            
        }
        private void GetHang()
        {
            hangTon.LoaiHang = cbbLoaiHang.SelectedValue == null ? 1 :Convert.ToInt16(cbbLoaiHang.SelectedValue.ToString());
            hangTon.MaHang = txtMaNPL.Text;
            hangTon.TenHang = txtTenNPL.Text;
            hangTon.MaHS = txtMaHS.Text;
            hangTon.DVT_ID = ctrDVT.Code;
            hangTon.LuongTonSoSach = Convert.ToDecimal(txtLuongSoSach.Value);
            hangTon.LuongTonThucTe = Convert.ToDecimal(txtLuongThucTe.Value);
            hangTon.GhiChu = txtGiaiTrinh.Text;
        }
        private void SetHang()
        {
            txtMaNPL.Text = hangTon.MaHang;
            txtTenNPL.Text = hangTon.TenHang;
            txtMaHS.Text = hangTon.MaHS;
            ctrDVT.Code = hangTon.DVT_ID;
            cbbLoaiHang.SelectedValue = hangTon.LoaiHang;
            txtLuongSoSach.Value = hangTon.LuongTonSoSach;
            txtLuongThucTe.Value = hangTon.LuongTonThucTe;
            txtGiaiTrinh.Text = hangTon.GhiChu;
           
        }
       
       

        private void BKNPLCungUngDetailForm_Load(object sender, EventArgs e)
        {
            LoadData();
            cbbLoaiHang.SelectedIndexChanged += new EventHandler(cbbNhomLyDo_SelectedIndexChanged);
            cbbLoaiHang.SelectedIndex = 0;
            
        }

        void cbbNhomLyDo_SelectedIndexChanged(object sender, EventArgs e) {}

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                hangTon = (KDT_SXXK_HangTon)dgList2.CurrentRow.DataRow;
                SetHang();
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            if (cbbLoaiHang.SelectedValue != null)
            {
                switch (cbbLoaiHang.SelectedValue.ToString())
                {
                    case "1":
                        Company.Interface.SXXK.SelectNPLForm f = new Company.Interface.SXXK.SelectNPLForm();
                        f.ShowDialog();
                        Company.BLL.SXXK.NguyenPhuLieu npl = f.NPLCollectionSelect[0];
                        txtMaNPL.Text = npl.Ma;
                        txtTenNPL.Text = npl.Ten;
                        txtMaHS.Text = npl.MaHS;
                        ctrDVT.Code = VNACCS_Mapper.GetCodeVNACC(npl.DVT_ID.Trim());
                        break;
                    case"2":
                        Company.Interface.SXXK.SelectSanPhamForm sp = new Company.Interface.SXXK.SelectSanPhamForm();
                        sp.ShowDialog();
                        Company.BLL.SXXK.SanPham sanpham = sp.spCollectionSelect[0];
                        txtMaNPL.Text = sanpham.Ma;
                        txtTenNPL.Text = sanpham.Ten;
                        txtMaHS.Text = sanpham.MaHS;
                        ctrDVT.Code = VNACCS_Mapper.GetCodeVNACC(sanpham.DVT_ID.Trim());
                        break;
                }
            }
            else
            {
                ShowMessage("Bạn chưa chọn loại hàng ", false);
                cbbLoaiHang.Focus();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {

                GridEXSelectedItemCollection items = dgList2.SelectedItems;
                List<KDT_SXXK_HangTon> ItemColl = new List<KDT_SXXK_HangTon>();
                if (dgList2.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa NPL này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_SXXK_HangTon)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_SXXK_HangTon item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        hangTonDK.hangTonList.Remove(item);
                    }
                    dgList2.Refetch();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (cbbLoaiHang.SelectedValue != null)
            {
                switch (cbbLoaiHang.SelectedValue.ToString())
                {
                    case "1":
                        Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                        npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        npl.Ma = txtMaNPL.Text;
                        if (npl.Load())
                        {
                            txtMaNPL.Text = npl.Ma;
                            txtTenNPL.Text = npl.Ten;
                            txtMaHS.Text = npl.MaHS;
                            ctrDVT.Code = VNACCS_Mapper.GetCodeVNACC(npl.DVT_ID.Trim());
                        }
                        break;
                    case "2":
                        Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                        sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        sp.Ma = txtMaNPL.Text;
                        if (sp.Load())
                        {
                            txtMaNPL.Text = sp.Ma;
                            txtTenNPL.Text = sp.Ten;
                            txtMaHS.Text = sp.MaHS;
                            ctrDVT.Code = VNACCS_Mapper.GetCodeVNACC(sp.DVT_ID.Trim());
                        }
                        break;
                }
            }
            else
            {
                ShowMessage("Chưa chọn loại hàng", false);
                cbbLoaiHang.Focus();
            }

        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdLuu":
                    Save();
                    break;
                case "cmdKhaiBao":
                    SendV5();
                    break;
                case "cmdNhanPhanHoi":
                    FeedBackV5();
                    break;
                case "cmdKhaiBaoSua":
                    SendV5();
                    break;
                case "cmdKQXL":
                    KetQuaXuLyTCU();
                    break;
                case "cmdKhaihuy":
                    SendHuyV5();
                    break;

            }
        }
        private void SendHuyV5()
        {
            if (ShowMessage("Bạn có muốn khai báo hủy bảng kê tự cung ứng này ?", true) == "Yes")
            {
                //   bool isKhaiSua = false;
                if (hangTonDK.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    hangTonDK =KDT_SXXK_HangTonDangKy.LoadFull(hangTonDK.ID);
                    hangTonDK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    //if (BKCungUng.TrangThaiXuLy == 5)
                    //    isKhaiSua = true;
                    //else
                    //    isKhaiSua = false;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.HangTon;
                sendXML.master_id = hangTonDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdLuu.Enabled = cmdLuu1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    return;
                }
                try
                {
                    if (this.hangTonDK.hangTonList.Count > 0)
                    {
                        string returnMessage = string.Empty;
                        hangTonDK.GuidStr = Guid.NewGuid().ToString();
                        //SXXK_TuCungUng TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferTuCungUng(BKCungUng, GlobalSettings.TEN_DON_VI, isKhaiSua);
                        //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(BKCungUng, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = CancelMessageV5(hangTonDK);
                        //BKCungUng.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(hangTonDK.ID, "Hủy khai báo bảng kê Hàng tồn");
                            cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdLuu.Enabled = cmdLuu1.Enabled = cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.HangTon;
                            sendXML.master_id = hangTonDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            hangTonDK.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        public static ObjectSend CancelMessageV5(KDT_SXXK_HangTonDangKy BK)
        {
            //string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
            //string sfmtDate = "yyyy-MM-dd";
            //string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";
            ////bool IsToKhaiNhap = BK.MaLoaiHinh.StartsWith("N");
            //BK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
            ////bool IsHuyToKhai = (BK.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            //bool IsHuyToKhai = true;
            //string issuer = string.Empty;
            //issuer = DeclarationIssuer.SXCX_HangTon;

            //DeclarationBase declaredCancel = new DeclarationBase()
            //{
            //    Issuer = issuer,
            //    Reference = BK.GuidStr,
            //    IssueLocation = string.Empty,
            //    Agents = new List<Agent>(),

            //    Issue = DateTime.Now.ToString(sfmtDateTime),
            //    Function = DeclarationFunction.HUY,
            //    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
            //    CustomsReference = Helpers.FormatNumeric(BK.SoTiepNhan, 0),
            //    Acceptance = BK.NgayTiepNhan.ToString(sfmtDate),
            //    DeclarationOffice = BK.MaHaiQuan,
            //    Importer = new NameBase()
            //    {
            //        Name = GlobalSettings.TEN_DON_VI,
            //        Identity = BK.MaDoanhNghiep
            //    },
            //    AdditionalInformations = new List<AdditionalInformation>()
            //};
            //declaredCancel.Agents.Add(new Agent()
            //{
            //    Name = GlobalSettings.TEN_DON_VI,
            //    Identity = BK.MaDoanhNghiep,
            //    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            //});
            //declaredCancel.AdditionalInformations.Add(
            //    new AdditionalInformation()
            //    {
            //        Content = new Content() { Text = "Xin hủy bảng kê chốt tồn NPL do sai số liệu" }
            //    }
            //    );
            //if (IsHuyToKhai)
            //{
            //    //List<HuyToKhai> cancelContent = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_BK_ID(BK.ID);
            //    //if (cancelContent.Count > 0)
            //    //{
            //    //    declaredCancel.AdditionalInformations[0].Content.Text = cancelContent[0].LyDoHuy;
            //    //    declaredCancel.Reference = guidHuyTK;
            //    //}
            //}
            //ObjectSend objSend = new ObjectSend(new NameBase()
            //{
            //    Name = GlobalSettings.TEN_DON_VI,
            //    Identity = BK.MaDoanhNghiep
            //},
            //new NameBase()
            //{
            //    Name = DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN),
            //    Identity = BK.MaHaiQuan.Trim()
            //},
            //new SubjectBase()
            //{
            //    Type = issuer,
            //    Function = declaredCancel.Function,
            //    Reference = declaredCancel.Reference //BK.GUIDSTR
            //}, declaredCancel);
            string returnMessage = string.Empty;
            BK.GuidStr = Guid.NewGuid().ToString();
            SXXK_HangTon TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferHangTon(BK, GlobalSettings.TEN_DON_VI, false, true);
            //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(hangTonDK, GlobalSettings.TEN_DON_VI);

            ObjectSend msgSend = new ObjectSend(
                 new NameBase()
                 {
                     Name = GlobalSettings.TEN_DON_VI,
                     Identity = BK.MaDoanhNghiep
                 },
                  new NameBase()
                  {
                      Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BK.MaHaiQuan)),
                      Identity = BK.MaHaiQuan
                  },
                  new SubjectBase()
                  {

                      Type = DeclarationIssuer.SXCX_HangTon,
                      Function =DeclarationFunction.HUY,
                      Reference = BK.GuidStr,
                  },
                  TuCungUng
                );
            return msgSend;
        }
        private void Save()
        {
            try
            {
                hangTonDK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN_VNACCS;
                hangTonDK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                hangTonDK.NamQuyetToan = Convert.ToInt32(txtNamQT.Value);
                hangTonDK.QuyQuyetToan = cbbQuy.SelectedValue == null ? 1 : Convert.ToInt32(cbbQuy.SelectedValue);
                hangTonDK.InsertUpdatFull();
                LoadData();
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi khi lưu : " + ex, false);
            }
           

        }

        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.HangTonSendHandler(hangTonDK, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5()
        {
            if (ShowMessage("Bạn có muốn khai báo bảng kê tự cung ứng này đến Hải quan?", true) == "Yes")
            {
                bool isKhaiSua = false;
                if (hangTonDK.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    hangTonDK = KDT_SXXK_HangTonDangKy.LoadFull(hangTonDK.ID);
                    if (hangTonDK.TrangThaiXuLy == 5)
                        isKhaiSua = true;
                    else
                        isKhaiSua = false;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.HangTon;
                sendXML.master_id = hangTonDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    return;
                }
                try
                {
                    if (this.hangTonDK.hangTonList.Count > 0)
                    {
                        string returnMessage = string.Empty;
                        hangTonDK.GuidStr = Guid.NewGuid().ToString();
                        SXXK_HangTon TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferHangTon(hangTonDK, GlobalSettings.TEN_DON_VI, isKhaiSua,false);
                        //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(hangTonDK, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = hangTonDK.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(hangTonDK.MaHaiQuan)),
                                  Identity = hangTonDK.MaHaiQuan
                              },
                              new SubjectBase()
                              {

                                  Type = DeclarationIssuer.SXCX_HangTon,
                                  Function = isKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                                  Reference = hangTonDK.GuidStr,
                              },
                              TuCungUng
                            );
                        //hangTonDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(hangTonDK.ID, isKhaiSua ? MessageTitle.KhaiBaoSuaTCU : MessageTitle.KhaiBaoHangTon);
                            cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.HangTon;
                            sendXML.master_id = hangTonDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            hangTonDK.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }

        //private void SendHuyV5()
        //{
        //    if (ShowMessage("Bạn có muốn khai báo hủy bảng kê tự cung ứng này ?", true) == "Yes")
        //    {
        //        //   bool isKhaiSua = false;
        //        if (hangTonDK.ID == 0)
        //        {
        //            this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
        //            return;
        //        }
        //        else
        //        {
        //            hangTonDK = KDT_SXXK_HangTonDangKy.LoadFull(hangTonDK.ID);
        //            hangTonDK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
        //            //if (hangTonDK.TrangThaiXuLy == 5)
        //            //    isKhaiSua = true;
        //            //else
        //            //    isKhaiSua = false;
        //        }
        //        MsgSend sendXML = new MsgSend();
        //        sendXML.LoaiHS = LoaiKhaiBao.HangTon;
        //        sendXML.master_id = hangTonDK.ID;
        //        if (sendXML.Load())
        //        {
        //            ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
        //            cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //            cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

        //            return;
        //        }
        //        try
        //        {
        //            if (this.hangTonDK.hangTonList.Count > 0)
        //            {
        //                string returnMessage = string.Empty;
        //                hangTonDK.GuidStr = Guid.NewGuid().ToString();
        //                //SXXK_TuCungUng TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferTuCungUng(hangTonDK, GlobalSettings.TEN_DON_VI, isKhaiSua);
        //                //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(hangTonDK, GlobalSettings.TEN_DON_VI);

        //                ObjectSend msgSend = CancelMessageV5(hangTonDK);
        //                //hangTonDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
        //                SendMessageForm sendForm = new SendMessageForm();
        //                sendForm.Send += SendMessage;
        //                bool isSend = sendForm.DoSend(msgSend);
        //                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
        //                {
        //                    sendForm.Message.XmlSaveMessage(hangTonDK.ID, "Hủy khai báo bảng kê");
        //                    cmdNhanPhanHoi.Enabled = cmdNhanPhanHoi1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
        //                    cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        //                    //btnDelete.Enabled = false;
        //                    sendXML.LoaiHS = LoaiKhaiBao.HangTon;
        //                    sendXML.master_id = hangTonDK.ID;
        //                    sendXML.msg = msgSend.ToString();
        //                    sendXML.func = 1;
        //                    sendXML.InsertUpdate();

        //                    hangTonDK.Update();
        //                    FeedBackV5();
        //                }
        //                else if (!string.IsNullOrEmpty(msgInfor))
        //                    ShowMessageTQDT(msgInfor, false);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //            
        //        }
        //    }
        //}

        //public static ObjectSend CancelMessageV5(KDT_SXXK_HangTonDangKy BK)
        //{
        //    string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        //    string sfmtDate = "yyyy-MM-dd";
        //    string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";
        //    //bool IsToKhaiNhap = BK.MaLoaiHinh.StartsWith("N");
        //    BK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
        //    //bool IsHuyToKhai = (BK.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
        //    bool IsHuyToKhai = true;
        //    string issuer = string.Empty;
        //    issuer = DeclarationIssuer.SXCX_HangTon;

        //    DeclarationBase declaredCancel = new DeclarationBase()
        //    {
        //        Issuer = issuer,
        //        Reference = BK.GuidStr,
        //        IssueLocation = string.Empty,
        //        Agents = new List<Agent>(),

        //        Issue = DateTime.Now.ToString(sfmtDateTime),
        //        Function = DeclarationFunction.HUY,
        //        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
        //        CustomsReference = Helpers.FormatNumeric(BK.SoTiepNhan, 0),
        //        Acceptance = BK.NgayTiepNhan.ToString(sfmtDate),
        //        DeclarationOffice = BK.MaHaiQuan,
        //        Importer = new NameBase()
        //        {
        //            Name = GlobalSettings.TEN_DON_VI,
        //            Identity = BK.MaDoanhNghiep
        //        },
        //        AdditionalInformations = new List<AdditionalInformation>()
        //    };
        //    declaredCancel.Agents.Add(new Agent()
        //    {
        //        Name = GlobalSettings.TEN_DON_VI,
        //        Identity = BK.MaDoanhNghiep,
        //        Status = AgentsStatus.NGUOIKHAI_HAIQUAN
        //    });
        //    declaredCancel.AdditionalInformations.Add(
        //        new AdditionalInformation()
        //        {
        //            Content = new Content() { Text = "Xin hủy bảng kê NPL cung ứng do sai số liệu" }
        //        }
        //        );
        //    if (IsHuyToKhai)
        //    {
        //        //List<HuyToKhai> cancelContent = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_BK_ID(BK.ID);
        //        //if (cancelContent.Count > 0)
        //        //{
        //        //    declaredCancel.AdditionalInformations[0].Content.Text = cancelContent[0].LyDoHuy;
        //        //    declaredCancel.Reference = guidHuyTK;
        //        //}
        //    }
        //    ObjectSend objSend = new ObjectSend(new NameBase()
        //    {
        //        Name = GlobalSettings.TEN_DON_VI,
        //        Identity = BK.MaDoanhNghiep
        //    },
        //    new NameBase()
        //    {
        //        Name = DonViHaiQuan.GetName(BK.MaHaiQuan),
        //        Identity = BK.MaHaiQuan
        //    },
        //    new SubjectBase()
        //    {
        //        Type = issuer,
        //        Function = declaredCancel.Function,
        //        Reference = declaredCancel.Reference //BK.GUIDSTR
        //    }, declaredCancel);
        //    return objSend;
        //}

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            //MsgSend sendXML = new MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
            //sendXML.master_id = hangTonDK.ID;
            //if (!sendXML.Load())
            //{
            //    ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
            //    XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    return;
            //}
            while (isFeedBack)
            {
                string reference = hangTonDK.GuidStr;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXCX_HangTon,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXCX_HangTon,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = hangTonDK.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(hangTonDK.MaHaiQuan.Trim())),
                                                  Identity = hangTonDK.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }

            }
        }
        private void KetQuaXuLyTCU()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.hangTonDK.ID;
            form.DeclarationIssuer = DeclarationIssuer.SXCX_HangTon;
            form.ShowDialog(this);
        }

    }
}