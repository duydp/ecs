using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class COT : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute KND { get; set; }
public PropertiesAttribute OLT { get; set; }
public PropertiesAttribute IEI { get; set; }
public PropertiesAttribute CHC { get; set; }
public PropertiesAttribute TRC { get; set; }
public PropertiesAttribute TRN { get; set; }
public PropertiesAttribute TRA { get; set; }
public PropertiesAttribute TCN { get; set; }
public PropertiesAttribute TCD { get; set; }
public PropertiesAttribute EDC { get; set; }
public PropertiesAttribute BYA { get; set; }
public PropertiesAttribute OBJ { get; set; }
public PropertiesAttribute USS { get; set; }
public PropertiesAttribute SDT { get; set; }
public PropertiesAttribute SDH { get; set; }
public PropertiesAttribute EDT { get; set; }
public PropertiesAttribute EDH { get; set; }
public PropertiesAttribute DPD { get; set; }
public PropertiesAttribute ZZA { get; set; }
public GroupAttribute TR_ { get; set; }
public PropertiesAttribute ARR { get; set; }
public PropertiesAttribute ARB { get; set; }
public PropertiesAttribute ARP { get; set; }
public PropertiesAttribute ARN { get; set; }
public PropertiesAttribute ARA { get; set; }
public PropertiesAttribute ROU { get; set; }
public PropertiesAttribute TOS { get; set; }
public PropertiesAttribute SBC { get; set; }
public PropertiesAttribute RYA { get; set; }
public PropertiesAttribute SCM { get; set; }
public PropertiesAttribute SCN { get; set; }
public PropertiesAttribute SEP { get; set; }
public PropertiesAttribute NTA { get; set; }
public GroupAttribute CG_ { get; set; }
public GroupAttribute E__ { get; set; }

public COT()
        {
KND = new PropertiesAttribute(1, typeof(string));
OLT = new PropertiesAttribute(12, typeof(int));
IEI = new PropertiesAttribute(1, typeof(string));
CHC = new PropertiesAttribute(6, typeof(string));
TRC = new PropertiesAttribute(13, typeof(string));
TRN = new PropertiesAttribute(300, typeof(string));
TRA = new PropertiesAttribute(300, typeof(string));
TCN = new PropertiesAttribute(11, typeof(string));
TCD = new PropertiesAttribute(8, typeof(DateTime));
EDC = new PropertiesAttribute(8, typeof(DateTime));
BYA = new PropertiesAttribute(2, typeof(string));
OBJ = new PropertiesAttribute(3, typeof(string));
USS = new PropertiesAttribute(2, typeof(string));
SDT = new PropertiesAttribute(8, typeof(DateTime));
SDH = new PropertiesAttribute(2, typeof(string));
EDT = new PropertiesAttribute(8, typeof(DateTime));
EDH = new PropertiesAttribute(2, typeof(string));
DPD = new PropertiesAttribute(8, typeof(DateTime));
ZZA = new PropertiesAttribute(8, typeof(DateTime));
ARR = new PropertiesAttribute(7, typeof(string));
ARB = new PropertiesAttribute(6, typeof(string));
ARP = new PropertiesAttribute(6, typeof(string));
ARN = new PropertiesAttribute(105, typeof(string));
ARA = new PropertiesAttribute(8, typeof(DateTime));
ROU = new PropertiesAttribute(35, typeof(string));
TOS = new PropertiesAttribute(1, typeof(string));
SBC = new PropertiesAttribute(11, typeof(string));
RYA = new PropertiesAttribute(4, typeof(int));
SCM = new PropertiesAttribute(10, typeof(string));
SCN = new PropertiesAttribute(10, typeof(string));
SEP = new PropertiesAttribute(11, typeof(int));
NTA = new PropertiesAttribute(765, typeof(string));
#region TR_
List<PropertiesAttribute> listTR_ = new List<PropertiesAttribute>();
listTR_.Add(new PropertiesAttribute(7, 3, EnumGroupID.COT_TR_, typeof(string)));
listTR_.Add(new PropertiesAttribute(8, 3, EnumGroupID.COT_AR_, typeof(DateTime)));
listTR_.Add(new PropertiesAttribute(8, 3, EnumGroupID.COT_ZB_, typeof(DateTime)));
listTR_.Add(new PropertiesAttribute(8, 3, EnumGroupID.COT_DP_, typeof(DateTime)));
listTR_.Add(new PropertiesAttribute(8, 3, EnumGroupID.COT_ZC_, typeof(DateTime)));
TR_ = new GroupAttribute("TR_", 3, listTR_);
#endregion TR_
#region CG_
List<PropertiesAttribute> listCG_ = new List<PropertiesAttribute>();
listCG_.Add(new PropertiesAttribute(35, 5, EnumGroupID.COT_CG_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.COT_DB_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(210, 5, EnumGroupID.COT_CM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(4, 5, EnumGroupID.COT_HS_, typeof(string)));
listCG_.Add(new PropertiesAttribute(140, 5, EnumGroupID.COT_MR_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.COT_IS_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(1, 5, EnumGroupID.COT_RM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.COT_OR_, typeof(string)));
listCG_.Add(new PropertiesAttribute(6, 5, EnumGroupID.COT_LP_, typeof(string)));
listCG_.Add(new PropertiesAttribute(6, 5, EnumGroupID.COT_PA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(1, 5, EnumGroupID.COT_TM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(38, 5, EnumGroupID.COT_TE_, typeof(string)));
listCG_.Add(new PropertiesAttribute(35, 5, EnumGroupID.COT_SN_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.COT_DD_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(13, 5, EnumGroupID.COT_IM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.COT_IN_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.COT_IA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(13, 5, EnumGroupID.COT_EM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.COT_EN_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.COT_EA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(13, 5, EnumGroupID.COT_KM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.COT_KN_, typeof(string)));
listCG_.Add(new PropertiesAttribute(300, 5, EnumGroupID.COT_KA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.COT_OA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.COT_OB_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.COT_OC_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.COT_OD_, typeof(string)));
listCG_.Add(new PropertiesAttribute(2, 5, EnumGroupID.COT_OE_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.COT_PC_, typeof(int)));
listCG_.Add(new PropertiesAttribute(3, 5, EnumGroupID.COT_PU_, typeof(string)));
listCG_.Add(new PropertiesAttribute(10, 5, EnumGroupID.COT_GW_, typeof(int)));
listCG_.Add(new PropertiesAttribute(3, 5, EnumGroupID.COT_WU_, typeof(string)));
listCG_.Add(new PropertiesAttribute(10, 5, EnumGroupID.COT_VO_, typeof(int)));
listCG_.Add(new PropertiesAttribute(3, 5, EnumGroupID.COT_VU_, typeof(string)));
listCG_.Add(new PropertiesAttribute(20, 5, EnumGroupID.COT_PR_, typeof(int)));
listCG_.Add(new PropertiesAttribute(3, 5, EnumGroupID.COT_RT_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.COT_RA_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.COT_RB_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.COT_RC_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.COT_RD_, typeof(string)));
listCG_.Add(new PropertiesAttribute(5, 5, EnumGroupID.COT_RE, typeof(string)));
listCG_.Add(new PropertiesAttribute(11, 5, EnumGroupID.COT_PM_, typeof(string)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.COT_PD_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(8, 5, EnumGroupID.COT_EP_, typeof(DateTime)));
listCG_.Add(new PropertiesAttribute(765, 5, EnumGroupID.COT_NT_, typeof(string)));
CG_ = new GroupAttribute("CG_", 5, listCG_);
#endregion CG_
#region E__
List<PropertiesAttribute> listE__ = new List<PropertiesAttribute>();
listE__.Add(new PropertiesAttribute(12, 50, EnumGroupID.COT_E__, typeof(int)));
E__ = new GroupAttribute("E__", 50, listE__);
#endregion E__
TongSoByte = 19183;
}

}public partial class EnumGroupID
    {
public static readonly string COT_TR_ = "COT_TR_";
public static readonly string COT_AR_ = "COT_AR_";
public static readonly string COT_ZB_ = "COT_ZB_";
public static readonly string COT_DP_ = "COT_DP_";
public static readonly string COT_ZC_ = "COT_ZC_";
public static readonly string COT_CG_ = "COT_CG_";
public static readonly string COT_DB_ = "COT_DB_";
public static readonly string COT_CM_ = "COT_CM_";
public static readonly string COT_HS_ = "COT_HS_";
public static readonly string COT_MR_ = "COT_MR_";
public static readonly string COT_IS_ = "COT_IS_";
public static readonly string COT_RM_ = "COT_RM_";
public static readonly string COT_OR_ = "COT_OR_";
public static readonly string COT_LP_ = "COT_LP_";
public static readonly string COT_PA_ = "COT_PA_";
public static readonly string COT_TM_ = "COT_TM_";
public static readonly string COT_TE_ = "COT_TE_";
public static readonly string COT_SN_ = "COT_SN_";
public static readonly string COT_DD_ = "COT_DD_";
public static readonly string COT_IM_ = "COT_IM_";
public static readonly string COT_IN_ = "COT_IN_";
public static readonly string COT_IA_ = "COT_IA_";
public static readonly string COT_EM_ = "COT_EM_";
public static readonly string COT_EN_ = "COT_EN_";
public static readonly string COT_EA_ = "COT_EA_";
public static readonly string COT_KM_ = "COT_KM_";
public static readonly string COT_KN_ = "COT_KN_";
public static readonly string COT_KA_ = "COT_KA_";
public static readonly string COT_OA_ = "COT_OA_";
public static readonly string COT_OB_ = "COT_OB_";
public static readonly string COT_OC_ = "COT_OC_";
public static readonly string COT_OD_ = "COT_OD_";
public static readonly string COT_OE_ = "COT_OE_";
public static readonly string COT_PC_ = "COT_PC_";
public static readonly string COT_PU_ = "COT_PU_";
public static readonly string COT_GW_ = "COT_GW_";
public static readonly string COT_WU_ = "COT_WU_";
public static readonly string COT_VO_ = "COT_VO_";
public static readonly string COT_VU_ = "COT_VU_";
public static readonly string COT_PR_ = "COT_PR_";
public static readonly string COT_RT_ = "COT_RT_";
public static readonly string COT_RA_ = "COT_RA_";
public static readonly string COT_RB_ = "COT_RB_";
public static readonly string COT_RC_ = "COT_RC_";
public static readonly string COT_RD_ = "COT_RD_";
public static readonly string COT_RE = "COT_RE";
public static readonly string COT_PM_ = "COT_PM_";
public static readonly string COT_PD_ = "COT_PD_";
public static readonly string COT_EP_ = "COT_EP_";
public static readonly string COT_NT_ = "COT_NT_";
public static readonly string COT_E__ = "COT_E__";
}

}
