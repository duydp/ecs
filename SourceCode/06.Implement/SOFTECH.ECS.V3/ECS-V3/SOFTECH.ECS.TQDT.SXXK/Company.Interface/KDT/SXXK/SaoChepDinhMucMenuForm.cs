using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class SaoChepDinhMucMenuForm : BaseForm
    {
        public DinhMucCollection collectionCopy = new DinhMucCollection();
        public DinhMucDangKy DMDangKy = new DinhMucDangKy();
        public string MaSanPhamCopy = "";
        public SaoChepDinhMucMenuForm()
        {
            InitializeComponent();
            DMDangKy.DMCollection = new DinhMucCollection();
        }        
        private void btnClose_Click(object sender, EventArgs e)
        {
            collectionCopy.Clear();
            this.Close();
        }
     
     
        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {
            SelectSanPhamDMForm f = new SelectSanPhamDMForm();
            f.status = true;
            f.CalledForm = this.Name;
            f.ShowDialog(this);
            if (f.SanPhamSelected !=null && f.SanPhamSelected.Ma.Trim()!="")
            {
                foreach (DinhMuc dm in DMDangKy.DMCollection)
                {
                    if (dm.MaSanPham.Trim() == f.SanPhamSelected.Ma.Trim())
                    {
                        //ShowMessage("Đã khai báo định mức sản phẩm này trên lưới.", false);
                        MLMessages("Đã khai báo định mức sản phẩm này trên lưới.", "MSG_PUB10", "", false);
                        txtMaSP.Text = "";
                        return;
                    }
                }
                txtMaSP.Text = f.SanPhamSelected.Ma.Trim();                
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            foreach (DinhMuc dm in collectionCopy)
            {
                if (dm.DinhMucSuDung == 0)
                {
                   // ShowMessage("Định mức của nguyên phụ liệu :'"+dm.MaNguyenPhuLieu+"' không được = 0", false);
                    MLMessages("Định mức của nguyên phụ liệu :'" + dm.MaNguyenPhuLieu + "' không được = 0", "MSG_THK108", "", false);
                    return;
                }
            }
            this.Close();
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            if (txtMaSP.Text.Trim() == "")
                return;
            cvError.Validate();
            if (!cvError.IsValid) return;
            Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
            ttdm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            ttdm.MaSanPham = txtMaSP.Text.Trim();
            if (ttdm.Load())
            {
               // ShowMessage("Sản phẩm này đã khai báo định mức", false);
                MLMessages("Sản phẩm này đã khai báo định mức", "MSG_DMC05", "", false);
                return;
            }
            Company.BLL.SXXK.SanPham spDuyet = new Company.BLL.SXXK.SanPham();
            spDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            spDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            spDuyet.Ma = txtMaSP.Text.Trim();
            if (!spDuyet.Load())
            {
               // ShowMessage("Sản phẩm không có trong hệ thống.", false);
                MLMessages("Sản phẩm không có trong hệ thống.", "MSG_THK109", "", false);
                return;
            }
            foreach (DinhMuc dm in collectionCopy)
            {
                dm.MaSanPham = txtMaSP.Text.Trim();                
            }
        }

        private void SaoChepDinhMucMenuForm_Load(object sender, EventArgs e)
        {
            foreach (DinhMuc dm in DMDangKy.DMCollection)
            {
                if (dm.MaSanPham == MaSanPhamCopy)
                {
                    DinhMuc dmCopy=new DinhMuc();
                    dmCopy.MaNguyenPhuLieu=dm.MaNguyenPhuLieu;
                    dmCopy.DinhMucSuDung=dm.DinhMucSuDung;
                    dmCopy.TyLeHaoHut=dm.TyLeHaoHut;
                    collectionCopy.Add(dmCopy);
                }
            }
            dgList.DataSource = collectionCopy;
            dgList.Refetch();
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
        }

       
     
    }
}