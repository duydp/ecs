﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe01_DKNVL : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public int toso = 0;
        public BangKe01_DKNVL()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;


            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamDangKyInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            lblMathang.Text = loaiSP;
            //  lblAddThue.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;


            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[0].ColumnName);
            lblTenNVL.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[1].ColumnName, "{0}");
            lblMaNVL.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[2].ColumnName, "{0}");
            lblDonViTinh.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[3].ColumnName, "{0}");
            lblLuong.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[4].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTriGia.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[5].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLoaiNVL.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[6].ColumnName, "{0}");
            lblHinhThucCungCap.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[7].ColumnName, "{0}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[8].ColumnName, "{0}");
            lblNgayKi.Text = GlobalSettings.TieudeNgay;

        }

    }
}
