-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@NgayBatDauBC datetime,
	@NgayKetthucBC datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TenDoanhNghiep nvarchar(255),
	@TenKho nvarchar(255),
	@MaKho nvarchar(17),
	@SoTKChungTu nvarchar(30),
	@Loai numeric(2, 0),
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQTiepNhanHD nvarchar(6),
	@NgayHetHanHD datetime,
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseExport]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetthucBC],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[TenKho],
	[MaKho],
	[SoTKChungTu],
	[Loai],
	[SoHopDong],
	[NgayHopDong],
	[MaHQTiepNhanHD],
	[NgayHetHanHD],
	[GhiChuKhac],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@NgayBatDauBC,
	@NgayKetthucBC,
	@MaHQ,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@TenKho,
	@MaKho,
	@SoTKChungTu,
	@Loai,
	@SoHopDong,
	@NgayHopDong,
	@MaHQTiepNhanHD,
	@NgayHetHanHD,
	@GhiChuKhac,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@NgayBatDauBC datetime,
	@NgayKetthucBC datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TenDoanhNghiep nvarchar(255),
	@TenKho nvarchar(255),
	@MaKho nvarchar(17),
	@SoTKChungTu nvarchar(30),
	@Loai numeric(2, 0),
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQTiepNhanHD nvarchar(6),
	@NgayHetHanHD datetime,
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[T_KDT_VNACCS_WarehouseExport]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[NgayBatDauBC] = @NgayBatDauBC,
	[NgayKetthucBC] = @NgayKetthucBC,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[TenKho] = @TenKho,
	[MaKho] = @MaKho,
	[SoTKChungTu] = @SoTKChungTu,
	[Loai] = @Loai,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[MaHQTiepNhanHD] = @MaHQTiepNhanHD,
	[NgayHetHanHD] = @NgayHetHanHD,
	[GhiChuKhac] = @GhiChuKhac,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@NgayBatDauBC datetime,
	@NgayKetthucBC datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TenDoanhNghiep nvarchar(255),
	@TenKho nvarchar(255),
	@MaKho nvarchar(17),
	@SoTKChungTu nvarchar(30),
	@Loai numeric(2, 0),
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQTiepNhanHD nvarchar(6),
	@NgayHetHanHD datetime,
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_VNACCS_WarehouseExport] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_VNACCS_WarehouseExport] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[NgayBatDauBC] = @NgayBatDauBC,
			[NgayKetthucBC] = @NgayKetthucBC,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[TenKho] = @TenKho,
			[MaKho] = @MaKho,
			[SoTKChungTu] = @SoTKChungTu,
			[Loai] = @Loai,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[MaHQTiepNhanHD] = @MaHQTiepNhanHD,
			[NgayHetHanHD] = @NgayHetHanHD,
			[GhiChuKhac] = @GhiChuKhac,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseExport]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[NgayBatDauBC],
			[NgayKetthucBC],
			[MaHQ],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[TenKho],
			[MaKho],
			[SoTKChungTu],
			[Loai],
			[SoHopDong],
			[NgayHopDong],
			[MaHQTiepNhanHD],
			[NgayHetHanHD],
			[GhiChuKhac],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@NgayBatDauBC,
			@NgayKetthucBC,
			@MaHQ,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@TenKho,
			@MaKho,
			@SoTKChungTu,
			@Loai,
			@SoHopDong,
			@NgayHopDong,
			@MaHQTiepNhanHD,
			@NgayHetHanHD,
			@GhiChuKhac,
			@GuidStr
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_VNACCS_WarehouseExport]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseExport] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetthucBC],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[TenKho],
	[MaKho],
	[SoTKChungTu],
	[Loai],
	[SoHopDong],
	[NgayHopDong],
	[MaHQTiepNhanHD],
	[NgayHetHanHD],
	[GhiChuKhac],
	[GuidStr]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseExport]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetthucBC],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[TenKho],
	[MaKho],
	[SoTKChungTu],
	[Loai],
	[SoHopDong],
	[NgayHopDong],
	[MaHQTiepNhanHD],
	[NgayHetHanHD],
	[GhiChuKhac],
	[GuidStr]
FROM [dbo].[T_KDT_VNACCS_WarehouseExport] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectAll]



















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetthucBC],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[TenKho],
	[MaKho],
	[SoTKChungTu],
	[Loai],
	[SoHopDong],
	[NgayHopDong],
	[MaHQTiepNhanHD],
	[NgayHetHanHD],
	[GhiChuKhac],
	[GuidStr]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseExport]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectBy_WarehouseExport_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectBy_WarehouseExport_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteBy_WarehouseExport_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteBy_WarehouseExport_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Insert]
	@WarehouseExport_ID bigint,
	@STT numeric(5, 0),
	@SoPhieuXuat nvarchar(50),
	@NgayPhieuXuat datetime,
	@TenNguoiNhanHang nvarchar(255),
	@MaNguoiNhanHang nvarchar(17),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseExport_Details]
(
	[WarehouseExport_ID],
	[STT],
	[SoPhieuXuat],
	[NgayPhieuXuat],
	[TenNguoiNhanHang],
	[MaNguoiNhanHang]
)
VALUES 
(
	@WarehouseExport_ID,
	@STT,
	@SoPhieuXuat,
	@NgayPhieuXuat,
	@TenNguoiNhanHang,
	@MaNguoiNhanHang
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Update]
	@ID bigint,
	@WarehouseExport_ID bigint,
	@STT numeric(5, 0),
	@SoPhieuXuat nvarchar(50),
	@NgayPhieuXuat datetime,
	@TenNguoiNhanHang nvarchar(255),
	@MaNguoiNhanHang nvarchar(17)
AS

UPDATE
	[dbo].[T_KDT_VNACCS_WarehouseExport_Details]
SET
	[WarehouseExport_ID] = @WarehouseExport_ID,
	[STT] = @STT,
	[SoPhieuXuat] = @SoPhieuXuat,
	[NgayPhieuXuat] = @NgayPhieuXuat,
	[TenNguoiNhanHang] = @TenNguoiNhanHang,
	[MaNguoiNhanHang] = @MaNguoiNhanHang
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_InsertUpdate]
	@ID bigint,
	@WarehouseExport_ID bigint,
	@STT numeric(5, 0),
	@SoPhieuXuat nvarchar(50),
	@NgayPhieuXuat datetime,
	@TenNguoiNhanHang nvarchar(255),
	@MaNguoiNhanHang nvarchar(17)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_VNACCS_WarehouseExport_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_VNACCS_WarehouseExport_Details] 
		SET
			[WarehouseExport_ID] = @WarehouseExport_ID,
			[STT] = @STT,
			[SoPhieuXuat] = @SoPhieuXuat,
			[NgayPhieuXuat] = @NgayPhieuXuat,
			[TenNguoiNhanHang] = @TenNguoiNhanHang,
			[MaNguoiNhanHang] = @MaNguoiNhanHang
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseExport_Details]
		(
			[WarehouseExport_ID],
			[STT],
			[SoPhieuXuat],
			[NgayPhieuXuat],
			[TenNguoiNhanHang],
			[MaNguoiNhanHang]
		)
		VALUES 
		(
			@WarehouseExport_ID,
			@STT,
			@SoPhieuXuat,
			@NgayPhieuXuat,
			@TenNguoiNhanHang,
			@MaNguoiNhanHang
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_VNACCS_WarehouseExport_Details]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteBy_WarehouseExport_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteBy_WarehouseExport_ID]
	@WarehouseExport_ID bigint
AS

DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseExport_Details]
WHERE
	[WarehouseExport_ID] = @WarehouseExport_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseExport_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseExport_ID],
	[STT],
	[SoPhieuXuat],
	[NgayPhieuXuat],
	[TenNguoiNhanHang],
	[MaNguoiNhanHang]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseExport_Details]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectBy_WarehouseExport_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectBy_WarehouseExport_ID]
	@WarehouseExport_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseExport_ID],
	[STT],
	[SoPhieuXuat],
	[NgayPhieuXuat],
	[TenNguoiNhanHang],
	[MaNguoiNhanHang]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseExport_Details]
WHERE
	[WarehouseExport_ID] = @WarehouseExport_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[WarehouseExport_ID],
	[STT],
	[SoPhieuXuat],
	[NgayPhieuXuat],
	[TenNguoiNhanHang],
	[MaNguoiNhanHang]
FROM [dbo].[T_KDT_VNACCS_WarehouseExport_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseExport_ID],
	[STT],
	[SoPhieuXuat],
	[NgayPhieuXuat],
	[TenNguoiNhanHang],
	[MaNguoiNhanHang]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseExport_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectBy_WarehouseExport_Details_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectBy_WarehouseExport_Details_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_DeleteBy_WarehouseExport_Details_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_DeleteBy_WarehouseExport_Details_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Insert]
	@WarehouseExport_Details_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@LoaiHangHoa numeric(2, 0),
	@MaDinhDanhSX nvarchar(50),
	@MucDichSuDung numeric(3, 0),
	@SoLuongDuKienXuat numeric(18, 4),
	@SoLuongThucXuat numeric(18, 4),
	@DVT nvarchar(4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails]
(
	[WarehouseExport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[MucDichSuDung],
	[SoLuongDuKienXuat],
	[SoLuongThucXuat],
	[DVT]
)
VALUES 
(
	@WarehouseExport_Details_ID,
	@STT,
	@TenHangHoa,
	@MaHangHoa,
	@LoaiHangHoa,
	@MaDinhDanhSX,
	@MucDichSuDung,
	@SoLuongDuKienXuat,
	@SoLuongThucXuat,
	@DVT
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Update]
	@ID bigint,
	@WarehouseExport_Details_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@LoaiHangHoa numeric(2, 0),
	@MaDinhDanhSX nvarchar(50),
	@MucDichSuDung numeric(3, 0),
	@SoLuongDuKienXuat numeric(18, 4),
	@SoLuongThucXuat numeric(18, 4),
	@DVT nvarchar(4)
AS

UPDATE
	[dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails]
SET
	[WarehouseExport_Details_ID] = @WarehouseExport_Details_ID,
	[STT] = @STT,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[LoaiHangHoa] = @LoaiHangHoa,
	[MaDinhDanhSX] = @MaDinhDanhSX,
	[MucDichSuDung] = @MucDichSuDung,
	[SoLuongDuKienXuat] = @SoLuongDuKienXuat,
	[SoLuongThucXuat] = @SoLuongThucXuat,
	[DVT] = @DVT
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_InsertUpdate]
	@ID bigint,
	@WarehouseExport_Details_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@LoaiHangHoa numeric(2, 0),
	@MaDinhDanhSX nvarchar(50),
	@MucDichSuDung numeric(3, 0),
	@SoLuongDuKienXuat numeric(18, 4),
	@SoLuongThucXuat numeric(18, 4),
	@DVT nvarchar(4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails] 
		SET
			[WarehouseExport_Details_ID] = @WarehouseExport_Details_ID,
			[STT] = @STT,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[LoaiHangHoa] = @LoaiHangHoa,
			[MaDinhDanhSX] = @MaDinhDanhSX,
			[MucDichSuDung] = @MucDichSuDung,
			[SoLuongDuKienXuat] = @SoLuongDuKienXuat,
			[SoLuongThucXuat] = @SoLuongThucXuat,
			[DVT] = @DVT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails]
		(
			[WarehouseExport_Details_ID],
			[STT],
			[TenHangHoa],
			[MaHangHoa],
			[LoaiHangHoa],
			[MaDinhDanhSX],
			[MucDichSuDung],
			[SoLuongDuKienXuat],
			[SoLuongThucXuat],
			[DVT]
		)
		VALUES 
		(
			@WarehouseExport_Details_ID,
			@STT,
			@TenHangHoa,
			@MaHangHoa,
			@LoaiHangHoa,
			@MaDinhDanhSX,
			@MucDichSuDung,
			@SoLuongDuKienXuat,
			@SoLuongThucXuat,
			@DVT
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_DeleteBy_WarehouseExport_Details_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_DeleteBy_WarehouseExport_Details_ID]
	@WarehouseExport_Details_ID bigint
AS

DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails]
WHERE
	[WarehouseExport_Details_ID] = @WarehouseExport_Details_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseExport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[MucDichSuDung],
	[SoLuongDuKienXuat],
	[SoLuongThucXuat],
	[DVT]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectBy_WarehouseExport_Details_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectBy_WarehouseExport_Details_ID]
	@WarehouseExport_Details_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseExport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[MucDichSuDung],
	[SoLuongDuKienXuat],
	[SoLuongThucXuat],
	[DVT]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails]
WHERE
	[WarehouseExport_Details_ID] = @WarehouseExport_Details_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[WarehouseExport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[MucDichSuDung],
	[SoLuongDuKienXuat],
	[SoLuongThucXuat],
	[DVT]
FROM [dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseExport_GoodsDetail_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseExport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[MucDichSuDung],
	[SoLuongDuKienXuat],
	[SoLuongThucXuat],
	[DVT]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '26.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('26.4',GETDATE(), N'CẬP NHẬT PHIẾU NHẬP KHO VÀ XUẤT KHO THEO TT MỚI')
END