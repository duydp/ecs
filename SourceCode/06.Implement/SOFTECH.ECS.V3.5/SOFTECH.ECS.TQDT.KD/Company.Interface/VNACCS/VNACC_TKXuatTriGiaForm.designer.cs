﻿namespace Company.Interface
{
    partial class VNACC_TKXuatTriGiaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TKXuatTriGiaForm));
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdInTamMEC1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTamMEC");
            this.cmdThongTinHQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThongTinHQ");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdMEC1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMEC");
            this.cmdMEE1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMEE");
            this.cmdMEC2 = new Janus.Windows.UI.CommandBars.UICommand("cmdMEC");
            this.cmdMEE2 = new Janus.Windows.UI.CommandBars.UICommand("cmdMEE");
            this.cmdThongTinHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdThongTinHQ");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdInTamMEC = new Janus.Windows.UI.CommandBars.UICommand("cmdInTamMEC");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.grbDoiTac = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiDoiTac2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDiaChiDoiTac4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.grbDonVi = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoDienThoaiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoQuanLyNoiBoDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGiaKhaiBao = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoHouseAWB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMoTaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).BeginInit();
            this.grbDoiTac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).BeginInit();
            this.grbDonVi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 479), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 479);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 455);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 455);
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.Color.Gainsboro;
            this.grbMain.Controls.Add(this.uiGroupBox9);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(807, 479);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdMEC2,
            this.cmdMEE2,
            this.cmdThongTinHQ,
            this.cmdKetQuaXuLy,
            this.cmdInTamMEC});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.TopRebar = this.TopRebar1;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdLuu1,
            this.cmdKhaiBao1,
            this.cmdInTamMEC1,
            this.cmdThongTinHQ1,
            this.cmdKetQuaXuLy1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1013, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdInTamMEC1
            // 
            this.cmdInTamMEC1.Key = "cmdInTamMEC";
            this.cmdInTamMEC1.Name = "cmdInTamMEC1";
            // 
            // cmdThongTinHQ1
            // 
            this.cmdThongTinHQ1.Key = "cmdThongTinHQ";
            this.cmdThongTinHQ1.Name = "cmdThongTinHQ1";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Image = ((System.Drawing.Image)(resources.GetObject("cmdLuu.Image")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdMEC1,
            this.cmdMEE1});
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdMEC1
            // 
            this.cmdMEC1.Image = ((System.Drawing.Image)(resources.GetObject("cmdMEC1.Image")));
            this.cmdMEC1.Key = "cmdMEC";
            this.cmdMEC1.Name = "cmdMEC1";
            // 
            // cmdMEE1
            // 
            this.cmdMEE1.Image = ((System.Drawing.Image)(resources.GetObject("cmdMEE1.Image")));
            this.cmdMEE1.Key = "cmdMEE";
            this.cmdMEE1.Name = "cmdMEE1";
            // 
            // cmdMEC2
            // 
            this.cmdMEC2.Image = ((System.Drawing.Image)(resources.GetObject("cmdMEC2.Image")));
            this.cmdMEC2.Key = "cmdMEC";
            this.cmdMEC2.Name = "cmdMEC2";
            this.cmdMEC2.Text = "Khai báo tạm (MEC)";
            // 
            // cmdMEE2
            // 
            this.cmdMEE2.Image = ((System.Drawing.Image)(resources.GetObject("cmdMEE2.Image")));
            this.cmdMEE2.Key = "cmdMEE";
            this.cmdMEE2.Name = "cmdMEE2";
            this.cmdMEE2.Text = "Khai báo chính thức (MEE)";
            // 
            // cmdThongTinHQ
            // 
            this.cmdThongTinHQ.Image = ((System.Drawing.Image)(resources.GetObject("cmdThongTinHQ.Image")));
            this.cmdThongTinHQ.Key = "cmdThongTinHQ";
            this.cmdThongTinHQ.Name = "cmdThongTinHQ";
            this.cmdThongTinHQ.Text = "Thông tin hải quan";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaXuLy.Image")));
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdInTamMEC
            // 
            this.cmdInTamMEC.Image = ((System.Drawing.Image)(resources.GetObject("cmdInTamMEC.Image")));
            this.cmdInTamMEC.Key = "cmdInTamMEC";
            this.cmdInTamMEC.Name = "cmdInTamMEC";
            this.cmdInTamMEC.Text = "In tờ khai tạm MEC";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1013, 28);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label26);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox3.Controls.Add(this.label27);
            this.uiGroupBox3.Controls.Add(this.label19);
            this.uiGroupBox3.Location = new System.Drawing.Point(6, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(796, 43);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(156, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(90, 13);
            this.label26.TabIndex = 42;
            this.label26.Text = "Mã bộ phận xữ lý";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 12;
            this.txtSoToKhai.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhai.Enabled = false;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhai.Location = new System.Drawing.Point(56, 12);
            this.txtSoToKhai.MaxLength = 15;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhai.Size = new System.Drawing.Size(98, 21);
            this.txtSoToKhai.TabIndex = 0;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(447, 16);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(92, 13);
            this.label27.TabIndex = 33;
            this.label27.Text = "Cơ quan Hải quan";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Số tờ khai";
            // 
            // grbDoiTac
            // 
            this.grbDoiTac.BackColor = System.Drawing.Color.Transparent;
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac2);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac1);
            this.grbDoiTac.Controls.Add(this.txtMaDoiTac);
            this.grbDoiTac.Controls.Add(this.label6);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac4);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac3);
            this.grbDoiTac.Controls.Add(this.label15);
            this.grbDoiTac.Controls.Add(this.label14);
            this.grbDoiTac.Controls.Add(this.label13);
            this.grbDoiTac.Controls.Add(this.txtMaBuuChinhDoiTac);
            this.grbDoiTac.Controls.Add(this.label16);
            this.grbDoiTac.Controls.Add(this.txtTenDoiTac);
            this.grbDoiTac.Controls.Add(this.label8);
            this.grbDoiTac.Controls.Add(this.label9);
            this.grbDoiTac.Controls.Add(this.label10);
            this.grbDoiTac.Location = new System.Drawing.Point(6, 222);
            this.grbDoiTac.Name = "grbDoiTac";
            this.grbDoiTac.Size = new System.Drawing.Size(358, 252);
            this.grbDoiTac.TabIndex = 2;
            this.grbDoiTac.Text = "Người nhập khẩu";
            this.grbDoiTac.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac2
            // 
            this.txtDiaChiDoiTac2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac2.Location = new System.Drawing.Point(75, 146);
            this.txtDiaChiDoiTac2.MaxLength = 100;
            this.txtDiaChiDoiTac2.Multiline = true;
            this.txtDiaChiDoiTac2.Name = "txtDiaChiDoiTac2";
            this.txtDiaChiDoiTac2.Size = new System.Drawing.Size(275, 40);
            this.txtDiaChiDoiTac2.TabIndex = 3;
            this.txtDiaChiDoiTac2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac1
            // 
            this.txtDiaChiDoiTac1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac1.Location = new System.Drawing.Point(75, 98);
            this.txtDiaChiDoiTac1.MaxLength = 100;
            this.txtDiaChiDoiTac1.Multiline = true;
            this.txtDiaChiDoiTac1.Name = "txtDiaChiDoiTac1";
            this.txtDiaChiDoiTac1.Size = new System.Drawing.Size(275, 40);
            this.txtDiaChiDoiTac1.TabIndex = 2;
            this.txtDiaChiDoiTac1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoiTac
            // 
            this.txtMaDoiTac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDoiTac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoiTac.Location = new System.Drawing.Point(75, 17);
            this.txtMaDoiTac.MaxLength = 13;
            this.txtMaDoiTac.Name = "txtMaDoiTac";
            this.txtMaDoiTac.Size = new System.Drawing.Size(275, 21);
            this.txtMaDoiTac.TabIndex = 0;
            this.txtMaDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Địa chỉ (1)";
            // 
            // txtDiaChiDoiTac4
            // 
            this.txtDiaChiDoiTac4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac4.Location = new System.Drawing.Point(245, 193);
            this.txtDiaChiDoiTac4.MaxLength = 255;
            this.txtDiaChiDoiTac4.Name = "txtDiaChiDoiTac4";
            this.txtDiaChiDoiTac4.Size = new System.Drawing.Size(105, 21);
            this.txtDiaChiDoiTac4.TabIndex = 5;
            this.txtDiaChiDoiTac4.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac3
            // 
            this.txtDiaChiDoiTac3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac3.Location = new System.Drawing.Point(75, 194);
            this.txtDiaChiDoiTac3.MaxLength = 255;
            this.txtDiaChiDoiTac3.Name = "txtDiaChiDoiTac3";
            this.txtDiaChiDoiTac3.Size = new System.Drawing.Size(105, 21);
            this.txtDiaChiDoiTac3.TabIndex = 4;
            this.txtDiaChiDoiTac3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(194, 197);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "Quốc gia";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1, 197);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Tỉnh/Thành";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Địa chỉ (2)";
            // 
            // txtMaBuuChinhDoiTac
            // 
            this.txtMaBuuChinhDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDoiTac.Location = new System.Drawing.Point(75, 222);
            this.txtMaBuuChinhDoiTac.MaxLength = 255;
            this.txtMaBuuChinhDoiTac.Name = "txtMaBuuChinhDoiTac";
            this.txtMaBuuChinhDoiTac.Size = new System.Drawing.Size(79, 21);
            this.txtMaBuuChinhDoiTac.TabIndex = 6;
            this.txtMaBuuChinhDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(194, 226);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Mã nước";
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoiTac.Location = new System.Drawing.Point(75, 47);
            this.txtTenDoiTac.MaxLength = 100;
            this.txtTenDoiTac.Multiline = true;
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(275, 40);
            this.txtTenDoiTac.TabIndex = 1;
            this.txtTenDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, 226);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Mã bưu chính";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Mã";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Tên";
            // 
            // grbDonVi
            // 
            this.grbDonVi.BackColor = System.Drawing.Color.Transparent;
            this.grbDonVi.Controls.Add(this.txtDiaChiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaDonVi);
            this.grbDonVi.Controls.Add(this.label3);
            this.grbDonVi.Controls.Add(this.txtSoDienThoaiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaBuuChinhDonVi);
            this.grbDonVi.Controls.Add(this.label5);
            this.grbDonVi.Controls.Add(this.txtTenDonVi);
            this.grbDonVi.Controls.Add(this.label4);
            this.grbDonVi.Controls.Add(this.label1);
            this.grbDonVi.Controls.Add(this.label2);
            this.grbDonVi.Location = new System.Drawing.Point(6, 45);
            this.grbDonVi.Name = "grbDonVi";
            this.grbDonVi.Size = new System.Drawing.Size(358, 175);
            this.grbDonVi.TabIndex = 1;
            this.grbDonVi.Text = "Người xuất khẩu";
            this.grbDonVi.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiDonVi
            // 
            this.txtDiaChiDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDonVi.Location = new System.Drawing.Point(72, 94);
            this.txtDiaChiDonVi.MaxLength = 100;
            this.txtDiaChiDonVi.Multiline = true;
            this.txtDiaChiDonVi.Name = "txtDiaChiDonVi";
            this.txtDiaChiDonVi.Size = new System.Drawing.Size(278, 40);
            this.txtDiaChiDonVi.TabIndex = 2;
            this.txtDiaChiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(72, 17);
            this.txtMaDonVi.MaxLength = 13;
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.Size = new System.Drawing.Size(278, 21);
            this.txtMaDonVi.TabIndex = 0;
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Địa chỉ";
            // 
            // txtSoDienThoaiDonVi
            // 
            this.txtSoDienThoaiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDienThoaiDonVi.Location = new System.Drawing.Point(255, 144);
            this.txtSoDienThoaiDonVi.MaxLength = 255;
            this.txtSoDienThoaiDonVi.Name = "txtSoDienThoaiDonVi";
            this.txtSoDienThoaiDonVi.Size = new System.Drawing.Size(95, 21);
            this.txtSoDienThoaiDonVi.TabIndex = 4;
            this.txtSoDienThoaiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDienThoaiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaBuuChinhDonVi
            // 
            this.txtMaBuuChinhDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDonVi.Location = new System.Drawing.Point(72, 144);
            this.txtMaBuuChinhDonVi.MaxLength = 255;
            this.txtMaBuuChinhDonVi.Name = "txtMaBuuChinhDonVi";
            this.txtMaBuuChinhDonVi.Size = new System.Drawing.Size(106, 21);
            this.txtMaBuuChinhDonVi.TabIndex = 3;
            this.txtMaBuuChinhDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(202, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Điện thoại";
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(72, 46);
            this.txtTenDonVi.MaxLength = 100;
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.Size = new System.Drawing.Size(278, 40);
            this.txtTenDonVi.TabIndex = 1;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Mã bưu chính";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Mã";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Tên";
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.txtSoQuanLyNoiBoDN);
            this.uiGroupBox11.Location = new System.Drawing.Point(368, 432);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(434, 43);
            this.uiGroupBox11.TabIndex = 12;
            this.uiGroupBox11.Text = "Số quản lý nội bộ doanh nghiệp";
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // txtSoQuanLyNoiBoDN
            // 
            this.txtSoQuanLyNoiBoDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoQuanLyNoiBoDN.Location = new System.Drawing.Point(8, 16);
            this.txtSoQuanLyNoiBoDN.MaxLength = 255;
            this.txtSoQuanLyNoiBoDN.Name = "txtSoQuanLyNoiBoDN";
            this.txtSoQuanLyNoiBoDN.Size = new System.Drawing.Size(417, 21);
            this.txtSoQuanLyNoiBoDN.TabIndex = 0;
            this.txtSoQuanLyNoiBoDN.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoQuanLyNoiBoDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.txtGhiChu);
            this.uiGroupBox10.Location = new System.Drawing.Point(368, 355);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(434, 74);
            this.uiGroupBox10.TabIndex = 11;
            this.uiGroupBox10.Text = "Ghi chú";
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(6, 16);
            this.txtGhiChu.MaxLength = 100;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(419, 50);
            this.txtGhiChu.TabIndex = 0;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtGiaKhaiBao);
            this.uiGroupBox2.Controls.Add(this.txtTongTriGiaTinhThue);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Location = new System.Drawing.Point(368, 207);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(434, 66);
            this.uiGroupBox2.TabIndex = 9;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtGiaKhaiBao
            // 
            this.txtGiaKhaiBao.DecimalDigits = 20;
            this.txtGiaKhaiBao.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtGiaKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaKhaiBao.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtGiaKhaiBao.Location = new System.Drawing.Point(116, 39);
            this.txtGiaKhaiBao.MaxLength = 15;
            this.txtGiaKhaiBao.Name = "txtGiaKhaiBao";
            this.txtGiaKhaiBao.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtGiaKhaiBao.Size = new System.Drawing.Size(129, 21);
            this.txtGiaKhaiBao.TabIndex = 2;
            this.txtGiaKhaiBao.Text = "0";
            this.txtGiaKhaiBao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGiaKhaiBao.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtGiaKhaiBao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongTriGiaTinhThue
            // 
            this.txtTongTriGiaTinhThue.DecimalDigits = 20;
            this.txtTongTriGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongTriGiaTinhThue.Location = new System.Drawing.Point(116, 13);
            this.txtTongTriGiaTinhThue.MaxLength = 15;
            this.txtTongTriGiaTinhThue.Name = "txtTongTriGiaTinhThue";
            this.txtTongTriGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTriGiaTinhThue.Size = new System.Drawing.Size(129, 21);
            this.txtTongTriGiaTinhThue.TabIndex = 0;
            this.txtTongTriGiaTinhThue.Text = "0";
            this.txtTongTriGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTriGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(251, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "VNĐ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Giá khai báo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Tổng trị giá tính thuế";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.txtSoLuong);
            this.uiGroupBox6.Controls.Add(this.label18);
            this.uiGroupBox6.Location = new System.Drawing.Point(368, 44);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(138, 45);
            this.uiGroupBox6.TabIndex = 3;
            this.uiGroupBox6.Text = "Số lượng";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 20;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong.Location = new System.Drawing.Point(3, 17);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(109, 21);
            this.txtSoLuong.TabIndex = 0;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(113, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Gói";
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.txtTrongLuong);
            this.uiGroupBox13.Controls.Add(this.label20);
            this.uiGroupBox13.Location = new System.Drawing.Point(508, 44);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(143, 45);
            this.uiGroupBox13.TabIndex = 4;
            this.uiGroupBox13.Text = "Tổng trọng lượng hàng";
            this.uiGroupBox13.VisualStyleManager = this.vsmMain;
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 20;
            this.txtTrongLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTrongLuong.Location = new System.Drawing.Point(3, 17);
            this.txtTrongLuong.MaxLength = 15;
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTrongLuong.Size = new System.Drawing.Size(109, 21);
            this.txtTrongLuong.TabIndex = 0;
            this.txtTrongLuong.Text = "0";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(113, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(28, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "KGM";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtSoHouseAWB);
            this.uiGroupBox5.Location = new System.Drawing.Point(653, 44);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(151, 45);
            this.uiGroupBox5.TabIndex = 5;
            this.uiGroupBox5.Text = "Số House AWB";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHouseAWB
            // 
            this.txtSoHouseAWB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoHouseAWB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoHouseAWB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHouseAWB.Location = new System.Drawing.Point(3, 18);
            this.txtSoHouseAWB.MaxLength = 12;
            this.txtSoHouseAWB.Name = "txtSoHouseAWB";
            this.txtSoHouseAWB.Size = new System.Drawing.Size(143, 21);
            this.txtSoHouseAWB.TabIndex = 0;
            this.txtSoHouseAWB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHouseAWB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.label17);
            this.uiGroupBox7.Controls.Add(this.label53);
            this.uiGroupBox7.Controls.Add(this.label51);
            this.uiGroupBox7.Location = new System.Drawing.Point(368, 95);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(434, 111);
            this.uiGroupBox7.TabIndex = 7;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(0, 82);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(150, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "Địa điểm nhận hàng cuối cùng";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(0, 53);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(96, 13);
            this.label53.TabIndex = 45;
            this.label53.Text = "Địa điểm xếp hàng";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(4, 17);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(179, 13);
            this.label51.TabIndex = 46;
            this.label51.Text = "Mã địa điểm lưu kho chờ thông quan";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.txtMoTaHangHoa);
            this.uiGroupBox8.Controls.Add(this.label22);
            this.uiGroupBox8.Location = new System.Drawing.Point(368, 274);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(434, 80);
            this.uiGroupBox8.TabIndex = 10;
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // txtMoTaHangHoa
            // 
            this.txtMoTaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoTaHangHoa.Location = new System.Drawing.Point(7, 28);
            this.txtMoTaHangHoa.MaxLength = 100;
            this.txtMoTaHangHoa.Multiline = true;
            this.txtMoTaHangHoa.Name = "txtMoTaHangHoa";
            this.txtMoTaHangHoa.Size = new System.Drawing.Size(417, 43);
            this.txtMoTaHangHoa.TabIndex = 0;
            this.txtMoTaHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMoTaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(4, 11);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 13);
            this.label22.TabIndex = 42;
            this.label22.Text = "Mô tả hàng hóa";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox9.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox9.Controls.Add(this.grbDonVi);
            this.uiGroupBox9.Controls.Add(this.grbDoiTac);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox13);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox9.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox9.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox9.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(807, 479);
            this.uiGroupBox9.TabIndex = 13;
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // VNACC_TKXuatTriGiaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 513);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "VNACC_TKXuatTriGiaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai xuất trị giá thấp";
            this.Load += new System.EventHandler(this.VNACC_TKNhapTriGiaForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).EndInit();
            this.grbDoiTac.ResumeLayout(false);
            this.grbDoiTac.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).EndInit();
            this.grbDonVi.ResumeLayout(false);
            this.grbDonVi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            this.uiGroupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.EditControls.UIGroupBox grbDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac2;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoiTac;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac4;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDoiTac;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoiTac;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.EditControls.UIGroupBox grbDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonVi;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDonVi;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonVi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrNhomXuLyHS;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQuanLyNoiBoDN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaNuoc;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHouseAWB;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label51;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMoTaHangHoa;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtGiaKhaiBao;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGiaTinhThue;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTongTriGiaTinhThue;
        private System.Windows.Forms.Label label12;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private System.Windows.Forms.Label label17;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaDDLuuKho;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaDiaDiemXepHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaDDNhanHangCuoiCung;
        private Janus.Windows.UI.CommandBars.UICommand cmdMEC1;
        private Janus.Windows.UI.CommandBars.UICommand cmdMEC2;
        private Janus.Windows.UI.CommandBars.UICommand cmdMEE2;
        private Janus.Windows.UI.CommandBars.UICommand cmdMEE1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTamMEC1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThongTinHQ1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThongTinHQ;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTamMEC;
    }
}