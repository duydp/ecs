﻿namespace Company.Interface.KDT.GC
{
    partial class CungUngDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CungUngDetailForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout gdListChungTu_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbLoaiChungTu = new Janus.Windows.EditControls.UIComboBox();
            this.txtTriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.clcNgayChungTu = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNoiCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.editBox8 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.editBox7 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.gdListChungTu = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdListChungTu)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(721, 384);
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.Location = new System.Drawing.Point(557, 11);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 22;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(638, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 19;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.Location = new System.Drawing.Point(476, 11);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 18;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(721, 372);
            this.uiGroupBox1.TabIndex = 25;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.Controls.Add(this.cbbLoaiChungTu);
            this.uiGroupBox3.Controls.Add(this.txtTriGia);
            this.uiGroupBox3.Controls.Add(this.txtDonGia);
            this.uiGroupBox3.Controls.Add(this.txtSoLuong);
            this.uiGroupBox3.Controls.Add(this.clcNgayChungTu);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.txtNoiCap);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.txtSoChungTu);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(721, 119);
            this.uiGroupBox3.TabIndex = 27;
            this.uiGroupBox3.Text = "Thông tin chứng từ";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // cbbLoaiChungTu
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Hóa đơn mua nội địa";
            uiComboBoxItem1.Value = ((short)(1));
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Từ tờ khai nhập";
            uiComboBoxItem2.Value = ((short)(2));
            this.cbbLoaiChungTu.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbLoaiChungTu.Location = new System.Drawing.Point(85, 54);
            this.cbbLoaiChungTu.Name = "cbbLoaiChungTu";
            this.cbbLoaiChungTu.Size = new System.Drawing.Size(162, 21);
            this.cbbLoaiChungTu.TabIndex = 28;
            this.cbbLoaiChungTu.VisualStyleManager = this.vsmMain;
            // 
            // txtTriGia
            // 
            this.txtTriGia.DecimalDigits = 6;
            this.txtTriGia.Location = new System.Drawing.Point(545, 54);
            this.txtTriGia.Name = "txtTriGia";
            this.txtTriGia.Size = new System.Drawing.Size(130, 21);
            this.txtTriGia.TabIndex = 27;
            this.txtTriGia.Text = "0.000000";
            this.txtTriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            393216});
            this.txtTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtDonGia
            // 
            this.txtDonGia.DecimalDigits = 6;
            this.txtDonGia.Location = new System.Drawing.Point(545, 20);
            this.txtDonGia.Name = "txtDonGia";
            this.txtDonGia.Size = new System.Drawing.Size(130, 21);
            this.txtDonGia.TabIndex = 27;
            this.txtDonGia.Text = "0.000000";
            this.txtDonGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            393216});
            this.txtDonGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 4;
            this.txtSoLuong.Location = new System.Drawing.Point(332, 54);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(130, 21);
            this.txtSoLuong.TabIndex = 27;
            this.txtSoLuong.Text = "0.0000";
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtSoLuong.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayChungTu
            // 
            this.clcNgayChungTu.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.clcNgayChungTu.Appearance.Options.UseBackColor = false;
            this.clcNgayChungTu.Appearance.Options.UseBorderColor = false;
            this.clcNgayChungTu.Appearance.Options.UseFont = false;
            this.clcNgayChungTu.Appearance.Options.UseForeColor = false;
            this.clcNgayChungTu.Appearance.Options.UseImage = false;
            this.clcNgayChungTu.Appearance.Options.UseTextOptions = false;
            this.clcNgayChungTu.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.clcNgayChungTu.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Default;
            this.clcNgayChungTu.Location = new System.Drawing.Point(332, 20);
            this.clcNgayChungTu.Name = "clcNgayChungTu";
            this.clcNgayChungTu.ReadOnly = false;
            this.clcNgayChungTu.Size = new System.Drawing.Size(130, 21);
            this.clcNgayChungTu.TabIndex = 26;
            this.clcNgayChungTu.TagName = "";
            this.clcNgayChungTu.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayChungTu.WhereCondition = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Nơi cấp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(466, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Trị Giá";
            // 
            // txtNoiCap
            // 
            this.txtNoiCap.Location = new System.Drawing.Point(85, 90);
            this.txtNoiCap.Name = "txtNoiCap";
            this.txtNoiCap.Size = new System.Drawing.Size(590, 21);
            this.txtNoiCap.TabIndex = 23;
            this.txtNoiCap.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(466, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Đơn giá";
            // 
            // txtSoChungTu
            // 
            this.txtSoChungTu.Location = new System.Drawing.Point(85, 20);
            this.txtSoChungTu.Name = "txtSoChungTu";
            this.txtSoChungTu.Size = new System.Drawing.Size(162, 21);
            this.txtSoChungTu.TabIndex = 23;
            this.txtSoChungTu.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(253, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Số lượng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(253, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Ngày chứng từ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Loại chứng từ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Số chứng từ";
            // 
            // editBox8
            // 
            this.editBox8.Location = new System.Drawing.Point(69, 85);
            this.editBox8.Name = "editBox8";
            this.editBox8.Size = new System.Drawing.Size(49, 21);
            this.editBox8.TabIndex = 23;
            // 
            // editBox7
            // 
            this.editBox7.Location = new System.Drawing.Point(69, 53);
            this.editBox7.Name = "editBox7";
            this.editBox7.Size = new System.Drawing.Size(286, 21);
            this.editBox7.TabIndex = 23;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.btnGhi);
            this.uiGroupBox2.Controls.Add(this.btnXoa);
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 119);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(721, 40);
            this.uiGroupBox2.TabIndex = 28;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.gdListChungTu);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 159);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(721, 213);
            this.uiGroupBox4.TabIndex = 29;
            this.uiGroupBox4.Text = "Danh sách chứng từ";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // gdListChungTu
            // 
            this.gdListChungTu.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gdListChungTu.ColumnAutoResize = true;
            gdListChungTu_DesignTimeLayout.LayoutString = resources.GetString("gdListChungTu_DesignTimeLayout.LayoutString");
            this.gdListChungTu.DesignTimeLayout = gdListChungTu_DesignTimeLayout;
            this.gdListChungTu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gdListChungTu.GroupByBoxVisible = false;
            this.gdListChungTu.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gdListChungTu.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gdListChungTu.Location = new System.Drawing.Point(3, 17);
            this.gdListChungTu.Name = "gdListChungTu";
            this.gdListChungTu.RecordNavigator = true;
            this.gdListChungTu.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gdListChungTu.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gdListChungTu.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gdListChungTu.Size = new System.Drawing.Size(715, 193);
            this.gdListChungTu.TabIndex = 25;
            this.gdListChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gdListChungTu.VisualStyleManager = this.vsmMain;
            this.gdListChungTu.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gdListChungTu_RowDoubleClick);
            // 
            // CungUngDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 384);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "CungUngDetailForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chứng từ cung ứng";
            this.Load += new System.EventHandler(this.CungUngDetailForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdListChungTu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiCap;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox editBox8;
        private Janus.Windows.GridEX.EditControls.EditBox editBox7;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTu;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayChungTu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiChungTu;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGia;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGia;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.GridEX gdListChungTu;
    }
}