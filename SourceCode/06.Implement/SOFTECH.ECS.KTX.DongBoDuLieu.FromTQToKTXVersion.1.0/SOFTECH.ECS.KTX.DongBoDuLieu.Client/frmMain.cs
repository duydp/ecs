﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DongBo.Controls;
using DongBoDuLieu.BLL;
using System.Configuration;

namespace DongBo
{
    public partial class frmMain : Form
    {
        int Flag = 0;

        ucConfigHQ obj2 = null;
        ucConfigDN obj3 = null;

        public frmMain()
        {
            InitializeComponent();
        }

        private void dispControl()
        {

            pnlContent.Controls.Clear();
            switch (Flag)
            {
                case 0:
                    ucStart obj0 = new ucStart();
                    pnlContent.Controls.Add(obj0);
                    obj0.Dock = System.Windows.Forms.DockStyle.Fill;
                    break;
                case 1:
                    ucRules obj1 = new ucRules();
                    pnlContent.Controls.Add(obj1);
                    obj1.Dock = System.Windows.Forms.DockStyle.Fill;
                    break;
                case 2:
                    obj2 = new ucConfigHQ();
                    pnlContent.Controls.Add(obj2);
                    obj2.Dock = System.Windows.Forms.DockStyle.Fill;
                    break;
                case 3:
                    obj2.saveConfigHaiQuan();
                    obj3 = new ucConfigDN();
                    pnlContent.Controls.Add(obj3);
                    obj3.Dock = System.Windows.Forms.DockStyle.Fill;
                    break;
                case 4:
                    obj3.saveConfigDoanhNghiep();
                    this.disp4();
                    break;
                case 5:
                    this.disp5();
                    break;
                case 6:
                    this.disp6();
                    break;
                case 7:
                    this.disp7();
                    break;
                case 8:
                    this.disp8();
                    break;
            }
        }

        private void disp8()
        {
            if (GlobalSettings.Phanhe == 0)
            {
            }
            else
            {
                if (this.btnNext.Text == "Tiếp tục")
                {
                    this.Flag = 3;
                    this.dispControl();
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void disp7()
        {
            if (GlobalSettings.Phanhe == 0)
            {
            }
            else
            {
                ucFinalSXXK obj7 = new ucFinalSXXK();
                pnlContent.Controls.Add(obj7);
                obj7.Dock = System.Windows.Forms.DockStyle.Fill;
            }
        }

        private void disp6()
        {
            if (GlobalSettings.Phanhe == 0)
            {
            }
            else
            {
                ucInsertSXXK obj6 = new ucInsertSXXK();
                pnlContent.Controls.Add(obj6);
                obj6.Dock = System.Windows.Forms.DockStyle.Fill;
            }
        }

        private void disp5()
        {
            if (GlobalSettings.Phanhe == 0)
            {
                ucFinalSXXK obj7 = new ucFinalSXXK();
                pnlContent.Controls.Add(obj7);
                obj7.Dock = System.Windows.Forms.DockStyle.Fill;
            }
            else
            {
                ucSXXK obj5 = null;
                ucSXXK_TQ obj6 = null;
                if (GlobalSettings.PhienBan == "KTX")
                {
                    obj5 = new ucSXXK();
                    pnlContent.Controls.Add(obj5);
                    obj5.Dock = System.Windows.Forms.DockStyle.Fill;
                }
                else if (GlobalSettings.PhienBan == "TQDT")
                {
                    obj6 = new ucSXXK_TQ();
                    pnlContent.Controls.Add(obj6);
                    obj6.Dock = System.Windows.Forms.DockStyle.Fill;
                }
            }
        }

        private void disp4()
        {
            obj2.saveConfigHaiQuan();
            obj3.saveConfigDoanhNghiep();

            //ucGCs21 obj4 = null;
            //ucGC_TQ obj5 = null;
            if (GlobalSettings.UpdateToKhaiMauDich)
            {
                ucToKhaiMauDich obj4 = new ucToKhaiMauDich();
                pnlContent.Controls.Add(obj4);
                obj4.Dock = System.Windows.Forms.DockStyle.Fill;
            }
            else if (GlobalSettings.Phanhe == 0)
            {
                if (GlobalSettings.PhienBan == "KTX")
                {
                    ucGCs21 obj4 = new ucGCs21();
                    pnlContent.Controls.Add(obj4);
                    obj4.Dock = System.Windows.Forms.DockStyle.Fill;
                }
                else if (GlobalSettings.PhienBan == "TQDT")
                {
                    ucGC_TQ obj5 = new ucGC_TQ();
                    pnlContent.Controls.Add(obj5);
                    obj5.Dock = System.Windows.Forms.DockStyle.Fill;
                }
            }
            else
            {
                ucProcSXXK obj4 = new ucProcSXXK();
                pnlContent.Controls.Add(obj4);
                obj4.Dock = System.Windows.Forms.DockStyle.Fill;
            }
        }

        private bool TestConnection()
        {
            bool ret = false;
            string strSQL = "Server = " + GlobalSettings.DiaChiSQLSource + "; Database = " + GlobalSettings.DBSource + "; UID = " + GlobalSettings.UserSQLSource + "; PWD =" + GlobalSettings.PassSQLSource;
            if (GlobalSettings.radSynFromHQ == true)
            {
                strSQL = "Server = " + GlobalSettings.DiaChiSQLTarget + "; Database = " + GlobalSettings.DBTarget + "; UID = " + GlobalSettings.UserSQLTarget + "; PWD =" + GlobalSettings.PassSQLTarget;
            }
            SqlConnection conn = new SqlConnection(strSQL);
            try
            {
                conn.Open();
                ret = true;
                conn.Close();
            }
            catch
            {

            }

            if (GlobalSettings.Phanhe != 0 && ret)
            {
                strSQL = "Server = " + GlobalSettings.DiaChiSQLTarget + "; Database = " + GlobalSettings.DBTarget + "; UID = " + GlobalSettings.UserSQLTarget + "; PWD =" + GlobalSettings.PassSQLTarget;
                conn = new SqlConnection(strSQL);
                try
                {
                    conn.Open();
                    ret = true;
                    conn.Close();
                }
                catch
                {

                }
            }
            return ret;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Flag == 3)
            {
                if (this.TestConnection())
                {
                    Flag++;
                    this.dispControl();
                }
                else
                {
                    MessageBox.Show("Không kết máy chủ doanh nghiệp không thành công! \nVui lòng kiểm tra thông tin kết nối");
                }
            }
            else if (Flag == 5 && GlobalSettings.Phanhe == 0) //GC
            {
                Flag = 3;
                this.dispControl();
            }
            else
            {
                Flag++;
                this.dispControl();
            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            Flag--;
            this.dispControl();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.dispControl();
            GlobalSettings.RefreshKey();
        }

    }
}