GO
ALTER TABLE [dbo].[t_KDT_Messages] DROP CONSTRAINT [PK_t_KDT_ThongDiep]
GO
ALTER TABLE [dbo].[t_KDT_Messages] 
ALTER COLUMN [ID] BIGINT NOT NULL
GO
ALTER TABLE [dbo].[t_KDT_Messages] ADD CONSTRAINT [PK_t_KDT_ThongDiep] PRIMARY KEY CLUSTERED ([ID]) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------  
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_NPLNhapTon_SelectAll_ByYear]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_SelectAll_ByYear]
GO  
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_SelectAll_ByYear]  
 @WhereCondition nvarchar(max),  
 @OrderByExpression nvarchar(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(max)  
  
SET @SQL = 'SELECT  *  
FROM    dbo.v_HangTon t   
        WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
GO
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[f_KiemTra_TienTrinh_ThanhLy]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
GO  
GO
CREATE FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
(
	@MaHaiQuan VARCHAR(10),
	@MaDoanhNghiep VARCHAR(50),
	@SoToKhai INT,
	@MaLoaihinh VARCHAR(10),
	@NamDangKy INT,
	@MaNPL NVARCHAR(255),
	@SoThuTuHang INT,
	@LuongTonHienTai NUMERIC(18,8)
	
)
RETURNS NVARCHAR(500)
AS 
    
/*
@SOFTECH
HUNGTQ, CREATED 04/08/2011.
Kiem tra tien trinh thuc hien chay thanh ly: . 
Gia tri dung phai la so ton cuoi cua lan thanh ly dau = so ton dau cua lan thanh ly tiep theo. 
*/
	
BEGIN

--SET @MaHaiQuan = 'C34C'
--SET @MaDoanhNghiep = '0400101556'
--SET @SoToKhai = 152
--SET @MaLoaihinh = 'NSX01'
--SET @NamDangKy = 2010
--SET @MaNPL = 'NEPLUNG'
			
DECLARE
	@LanThanhLy INT,
	@Luong NUMERIC(18,8),
	@Luong_BanLuu NUMERIC(18,8),
	@Luong_NhapTon NUMERIC(18,8),
	@TonDau NUMERIC(18,8),
	@TonCuoi NUMERIC(18,8),
	@TonCuoi_BanLuu NUMERIC(18,8),
	@TonDauThueXNK NUMERIC(18,8),
	@TonCuoiThueXNK NUMERIC(18,8),
	@cnt1 INT,--Bien dem
	@str NVARCHAR(500),
	@ketqua NVARCHAR(500), --hien thi ket qua
	@SaiSoLuong INT,
	@Valid BIT,
	@Dem INT,
	@isDongHoSo INT
	-- Đếm số dòng thanh lý
	SELECT  @Dem = COUNT(*)
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon]
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND SoThuTuHang =@SoThuTuHang
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
/*<I> Cap nhat lech luong nhap do bi lam tron so thap phan*/	
DECLARE curThanhLy CURSOR FOR
	SELECT  nplTon.LanThanhLy, Luong,SoThuTuHang, TonDau, TonCuoi, TonDauThueXNK, TonCuoiThueXNK
	,CASE 
			WHEN  EXISTS(SELECT TrangThaiThanhKhoan  FROM t_KDT_SXXK_HoSoThanhLyDangKy hs WHERE hs.LanThanhLy = nplTon.LanThanhLy AND hs.TrangThaiThanhKhoan = 401) THEN 1
			ELSE  0
			END AS IsDongHoSo
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon] nplTon
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND SoThuTuHang=@SoThuTuHang
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
	ORDER BY TonCuoi DESC
	
	
	
--BEGIN TRANSACTION T1 ;

	SET @Valid = 1
	SET @SaiSoLuong = 0	
    SET @cnt1 = 0;
    
	SET @str = '....................................................................' + char(10)
	SET @str = @str + '		STT' + ' - ' + + 'SoToKhai' + ' - ' + 'MaLoaiHinh' + ' - ' + 'NamDangKy' + ' - ' + 'MaNPL' + char(10)
	SET @str = @str + STR(@cnt1) + ' - ' + STR(@SoToKhai) + ' - ' + @MaLoaiHinh + ' - ' + str(@NamDangKy)+ ' - ' + @MaNPL + char(10)
	--PRINT @str								
	
    OPEN curThanhLy 
    FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong,@SoThuTuHang, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK, @isDongHoSo
	--Fetch next record
    WHILE @@FETCH_STATUS = 0 
		BEGIN
							
			SET @cnt1 = @cnt1 + 1
						
			--PRINT STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau: ' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi: ' + cast(@TonCuoi AS VARCHAR(50))
										
			IF (@cnt1 = 1)
				BEGIN
					
					--Kiem tra so luong nhap dang ky ban dau so voi luong ton dau.
					IF (@Luong <> @TonDau)
						BEGIN
							SET @ketqua = N'Sai lượng nhập và lượng tồn đầu tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
							SET @Valid = 0
						END				
					
						  
					IF(@Valid = 1)
						BEGIN
							--Kiem tra so luong nhap dang ky ban dau so voi luong nhap luu trong ton.
							SELECT  @SaiSoLuong =  COUNT(*), @Luong_NhapTon = nt.Luong
								  FROM      [dbo].t_SXXK_ThanhLy_NPLNhapTon nt
											INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu
											                                       AND nt.SoThuTuHang = h.SoThuTuHang
																				   AND nt.SoToKhai = h.SoToKhai
																				   AND nt.MaLoaiHinh = h.MaLoaiHinh
																				   AND nt.NamDangKy = h.NamDangKy
																				   AND nt.MaHaiQuan = h.MaHaiQuan
								  WHERE     nt.SoToKhai = @SoToKhai
											AND nt.MaLoaiHinh = @MaLoaiHinh
											AND nt.NamDangKy = @NamDangKy
											AND nt.MaNPL = @MaNPL
											AND nt.SoThuTuHang=@SoThuTuHang
											AND nt.MaHaiQuan = @MaHaiQuan
											AND nt.MaDoanhNghiep = @MaDoanhNghiep
											AND nt.Luong <> h.SoLuong
								  GROUP BY nt.Luong
						  
							IF (@SaiSoLuong > 0 AND @Luong_NhapTon <> @Luong)
								BEGIN
									SET @ketqua = N'Sai lượng nhập tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
									SET @Valid = 0
								END
							ELSE
								BEGIN	  
									SET @ketqua = N'Thanh lý đúng'
									SET @Valid = 1
									SET @TonCuoi_BanLuu = @TonCuoi
								END
						END
				END
			
			--Kiem tra so ton
			IF (@cnt1 > 1 AND @Valid = 1) --kiem tra tu dong thu 2 tro di
				BEGIN

					SET @str = STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi' + cast(@TonCuoi AS VARCHAR(50))
					
					IF (@TonDau <> @TonCuoi_BanLuu)
						BEGIN
							
							SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@TonCuoi_BanLuu AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
						END
					ELSE
						SET @ketqua = N'Thanh lý đúng'
				END		
				
					--Kiem tra so ton dong cuoi				
			IF(@cnt1 = @Dem)
			BEGIN
				IF(@isDongHoSo != 1 AND  @LuongTonHienTai <> @TonDau)
				BEGIN
					SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@LuongTonHienTai AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai số liệu hiện tại' + cast(@LanThanhLy AS VARCHAR(50))
				END
				IF(@isDongHoSo = 1 AND  @LuongTonHienTai <> @TonCuoi)
				BEGIN
					SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@LuongTonHienTai AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai số liệu hiện tại' + cast(@LanThanhLy AS VARCHAR(50))
				END
			END
			--Luu lai ton cuoi cua row truoc do
			SET @TonCuoi_BanLuu = @TonCuoi
				
			FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong,@SoThuTuHang, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK, @isDongHoSo
        END
        
    CLOSE curThanhLy --Close cursor
    DEALLOCATE curThanhLy --Deallocate cursor
	
	--PRINT @ketqua
	
    --IF @@ERROR != 0 
    --    BEGIN
            --PRINT 'Qua trinh thuc hien xay ra loi.'
            --PRINT STR(@@ERROR)
            ----ROLLBACK TRANSACTION T1 ;
            --PRINT 'ROLLBACK TRANSACTION T1'
  --      END
  --  ELSE 
		--BEGIN
			----COMMIT TRANSACTION T1 ;	
			--PRINT ''
			----PRINT 'COMMIT TRANSACTION T1'
		--END
	
	RETURN @ketqua
	
END	

GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[v_HangTon]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP VIEW [dbo].[v_HangTon]
GO  
CREATE VIEW [dbo].[v_HangTon]

AS

SELECT     CASE WHEN t .MaLoaiHinh LIKE '%V%' THEN

                          (SELECT     TOP 1 SoTKVNACCS

                            FROM          t_VNACCS_CapSoToKhai

                            WHERE      SoTK = t .SoToKhai) ELSE t .SoToKhai END AS SoToKhaiVNACCS, t.MaDoanhNghiep, t.MaHaiQuan, t.SoToKhai, t.MaLoaiHinh, t.NamDangKy, t.MaNPL, 

                      hmd.TenHang AS TenNPL, hmd.SoThuTuHang,hmd.DVT_ID, dvt.Ten AS TenDVT, hmd.NuocXX_ID, hmd.TriGiaKB, t.Luong, t.Ton, t.ThueXNK, t.ThueXNKTon, t.ThueTTDB, t.ThueVAT, 

                      t.ThueCLGia, t.PhuThu,

                          (SELECT     NgayDangKy

                            FROM          dbo.t_SXXK_ToKhaiMauDich

                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 

                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS NgayDangKy,

                          (SELECT     COUNT(*) AS Expr1

                            FROM          dbo.t_KDT_SXXK_NPLNhapTon

                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND t_KDT_SXXK_NPLNhapTon.SoThuTuHang = t.SoThuTuHang  AND (TonCuoi < TonDau) AND 

                                                   (LanThanhLy IN

                                                       (SELECT     LanThanhLy

                                                         FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy)) AND (MaDoanhNghiep = t.MaDoanhNghiep)) AS SoLanThanhLy,

                          (SELECT     MAX(LanThanhLy) AS Expr1

                            FROM          dbo.t_KDT_SXXK_NPLNhapTon AS t_KDT_SXXK_NPLNhapTon_2

                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND t_KDT_SXXK_NPLNhapTon_2.SoThuTuHang = t.SoThuTuHang AND (TonCuoi < TonDau) AND 

                                                   (LanThanhLy IN

                                                       (SELECT     LanThanhLy

                                                         FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_3)) AND (MaDoanhNghiep = t.MaDoanhNghiep)) 

                      AS LanThanhLy,

                          (SELECT     TrangThaiThanhKhoan

                            FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_2

                            WHERE      (LanThanhLy =

                                                       (SELECT     MAX(LanThanhLy) AS Expr1

                                                         FROM          dbo.t_KDT_SXXK_NPLNhapTon AS t_KDT_SXXK_NPLNhapTon_1

                                                         WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND 

                                                                                (TonCuoi < TonDau) AND (LanThanhLy IN

                                                                                    (SELECT     LanThanhLy

                                                                                      FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_1)) AND 

                                                                                (MaDoanhNghiep = t.MaDoanhNghiep)))) AS TrangThaiThanhKhoan,

                          (SELECT     COUNT(*) AS Expr1

                            FROM          dbo.t_SXXK_ThanhLy_NPLNhapTon AS nt INNER JOIN

                                                   dbo.t_SXXK_HangMauDich AS h ON nt.MaNPL = h.MaPhu AND nt.SoThuTuHang = h.SoThuTuHang AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 

                                                   nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan AND nt.Luong <> h.SoLuong

                            WHERE      (nt.SoToKhai = t.SoToKhai) AND (nt.MaLoaiHinh = t.MaLoaiHinh) AND (nt.NamDangKy = t.NamDangKy) AND (nt.MaNPL = t.MaNPL) AND (nt.SoThuTuHang = t.SoThuTuHang) AND 

                                                   (nt.MaHaiQuan = t.MaHaiQuan) AND (nt.MaDoanhNghiep = t.MaDoanhNghiep) 
												   --minhnd: fix lỗi cảnh báo sai
												   AND (nt.Luong = h.SoLuong)
												   --minhnd: fix lỗi cảnh báo sai
												   ) AS SaiSoLuong,

                          (SELECT     h.SoLuong

                            FROM          dbo.t_SXXK_ThanhLy_NPLNhapTon AS nt INNER JOIN

                                                   dbo.t_SXXK_HangMauDich AS h ON nt.MaNPL = h.MaPhu AND nt.SoThuTuHang = h.SoThuTuHang AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 

                                                   nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan AND nt.Luong <> h.SoLuong

                            WHERE      (nt.SoToKhai = t.SoToKhai) AND (nt.MaLoaiHinh = t.MaLoaiHinh) AND (nt.NamDangKy = t.NamDangKy) AND (nt.MaNPL = t.MaNPL) AND (nt.SoThuTuHang =h.SoThuTuHang) AND 

                                                   (nt.MaHaiQuan = t.MaHaiQuan) AND (nt.MaDoanhNghiep = t.MaDoanhNghiep) 

                                                   and (nt.Luong = h.SoLuong)-- phi kiem tra to khai cung ma khac don gia

                                                   ) AS SoLuongDangKy, dbo.f_KiemTra_TienTrinh_ThanhLy(t.MaHaiQuan, 

                      t.MaDoanhNghiep, t.SoToKhai, t.MaLoaiHinh, t.NamDangKy, t.MaNPL,T.SoThuTuHang, t.Ton) AS TienTrinhChayThanhLy,

                          (SELECT     ThanhLy

                            FROM          dbo.t_SXXK_ToKhaiMauDich AS tk

                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 

                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS ThanhLy, CASE WHEN

                          ((SELECT     COUNT(*)

                              FROM         [dbo].t_SXXK_ThanhLy_NPLNhapTon nt INNER JOIN

                                                    dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu AND nt.SoThuTuHang=h.SoThuTuHang AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 

                                                    nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan

                              WHERE     nt.SoToKhai = t .SoToKhai AND nt.MaLoaiHinh = t .MaLoaiHinh AND nt.NamDangKy = t .NamDangKy AND nt.MaNPL = t .MaNPL AND nt.SoThuTuHang =t.SoThuTuHang AND 

                                                    nt.MaHaiQuan = t .MaHaiQuan AND nt.MaDoanhNghiep = t .MaDoanhNghiep AND nt.Luong <> h.SoLuong) = 0 AND

                          (SELECT     COUNT(*)

                            FROM          [dbo].[t_KDT_SXXK_NPLNhapTon]

                            WHERE      SoToKhai = t .SoToKhai AND MaLoaiHinh = t .MaLoaiHinh AND NamDangKy = t .NamDangKy AND MaNPL = t .MaNPL AND hmd.SoThuTuHang=t.SoThuTuHang AND TonCuoi < TonDau AND 

                                                   LanThanhLy IN

                                                       (SELECT     LanThanhLy

                                                         FROM          t_KDT_SXXK_HoSoThanhLyDangKy) AND MaDoanhNghiep = t .MaDoanhNghiep) = 0 AND (t .Luong <> t .Ton)) 

                      THEN 1 ELSE 0 END AS LechTon,

                          (SELECT     TenChuHang

                            FROM          dbo.t_SXXK_ToKhaiMauDich AS t_SXXK_ToKhaiMauDich_2

                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 

                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS TenChuHang,

                            Case when t.MaLoaiHinh like '%V%' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t.SoToKhai)) else (select top 1 SoHopDong from t_SXXK_ToKhaiMauDich where SoToKhai = t.SoToKhai)  end AS SoHopDong

FROM         dbo.t_SXXK_ThanhLy_NPLNhapTon AS t LEFT OUTER JOIN

                      dbo.t_SXXK_HangMauDich AS hmd ON t.MaHaiQuan = hmd.MaHaiQuan AND t.MaNPL = hmd.MaPhu AND t.SoThuTuHang=hmd.SoThuTuHang AND t.SoToKhai = hmd.SoToKhai AND 

                      t.MaLoaiHinh = hmd.MaLoaiHinh AND t.NamDangKy = hmd.NamDangKy AND t.Luong = hmd.SoLuong and round(t.ThueXNK,0)= round(hmd.ThueXNK,0) LEFT OUTER JOIN

                      dbo.t_HaiQuan_DonViTinh AS dvt ON hmd.DVT_ID = dvt.ID

WHERE     ((CAST(t.SoToKhai AS VARCHAR(10)) + t.MaLoaiHinh + CAST(t.NamDangKy AS VARCHAR(4)) + t.MaHaiQuan) IN

                          (SELECT     CAST(SoToKhai AS VARCHAR(10)) + MaLoaiHinh + CAST(NamDangKy AS VARCHAR(4)) + MaHaiQuan AS Expr1

                            FROM          dbo.t_SXXK_ToKhaiMauDich AS t_SXXK_ToKhaiMauDich_1

                            WHERE      (MaLoaiHinh LIKE 'N%')))




GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_NPLNhapTon_SelectDynamic]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_SelectDynamic]
GO  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_SXXK_NPLNhapTon_SelectDynamic]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, August 28, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_SelectDynamic]  
 @WhereCondition nvarchar(500),  
 @OrderByExpression nvarchar(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(3250)  
  
SET @SQL = 'SELECT  [SoToKhai],  
 [MaLoaiHinh],  
 [NamDangKy],  
 [MaHaiQuan],  
 [MaNPL],
 SoThuTuHang,  
 [LanThanhLy],  
 [Luong],  
 [TonDau],  
 [TonCuoi],  
 [ThueXNK],  
 [ThueTTDB],  
 [ThueVAT],  
 [PhuThu],  
 [ThueCLGia],  
 [TonDauThueXNK],  
 [TonCuoiThueXNK],  
 [MaDoanhNghiep]  
 FROM [dbo].[t_KDT_SXXK_NPLNhapTon] WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO
GO
IF NOT EXISTS   (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='t_KDT_SXXK_BCXuatNhapTon' AND COLUMN_NAME = 'SoThuTuHang')
ALTER TABLE t_KDT_SXXK_BCXuatNhapTon 
ADD SoThuTuHang INT
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]
GO
CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]  
 @WhereCondition nvarchar(max),  
 @OrderByExpression nvarchar(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(max)  
  
SET @SQL = 'SELECT  [ID],  
 [STT],  
 [LanThanhLy],  
 [NamThanhLy],  
 [MaDoanhNghiep],
 SoThuTuHang,  
 UPPER([MaNPL]) AS MaNPL,  
 [TenNPL],  
 Case When MaLoaiHinhNhap like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiNhap]) else [SoToKhaiNhap] end as SoToKhaiNhap ,  
 [NgayDangKyNhap],  
 case WHEN year(NgayHoanThanhNhap)<=1900 then NgayDangKyNhap else NgayHoanThanhNhap end as NgayHoanThanhNhap,  
 Case When MaLoaiHinhNhap like ''%V%'' Then Substring(MaLoaiHinhNhap,3,3) else MaLoaiHinhNhap end as MaLoaiHinhNhap,  
 [LuongNhap],  
 [LuongTonDau],  
 [TenDVT_NPL],  
 [MaSP],  
 [TenSP],  
 Case When MaLoaiHinhXuat like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiXuat]) else [SoToKhaiXuat] end as SoToKhaiXuat,  
 [NgayDangKyXuat],  
 case WHEN year(NgayHoanThanhXuat)<=1900 then NgayDangKyXuat else NgayHoanThanhXuat end as NgayHoanThanhXuat,  
 Case When MaLoaiHinhXuat like ''%V%'' Then Substring(MaLoaiHinhXuat,3,3) else MaLoaiHinhXuat end as MaLoaiHinhXuat,  
 [LuongSPXuat],  
 [TenDVT_SP],  
 [DinhMuc],  
 [LuongNPLSuDung],  
 --Case when [SoToKhaiTaiXuat]  > 0 Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiTaiXuat]) else 0 end as SoToKhaiTaiXuat,  
 [SoToKhaiTaiXuat],  
 [NgayTaiXuat],  
 [LuongNPLTaiXuat],  
 [LuongTonCuoi],  
 dbo.[f_Convert_GhiChuTK]([ThanhKhoanTiep],[LanThanhLy]) as ThanhKhoanTiep,  
 [ChuyenMucDichKhac],  
 [DonGiaTT],  
 [TyGiaTT],  
 [ThueSuat],  
 [ThueXNK],  
 [ThueXNKTon],  
[NgayThucXuat]  
 FROM [dbo].[t_KDT_SXXK_BCXuatNhapTon] WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO
   
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_BCXuatNhapTon_Insert]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_Insert]
GO  
-----------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXuatNhapTon_Insert]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, August 28, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_Insert]  
 @STT bigint,  
 @LanThanhLy int,  
 @NamThanhLy int,  
 @MaDoanhNghiep varchar(14),  
 @SoThuTuHang INT,
 @MaNPL varchar(30),  
 @TenNPL nvarchar(255),  
 @SoToKhaiNhap int,  
 @NgayDangKyNhap datetime,  
 @NgayHoanThanhNhap datetime,  
 @MaLoaiHinhNhap char(5),  
 @LuongNhap numeric(18, 8),  
 @LuongTonDau numeric(18, 8),  
 @TenDVT_NPL varchar(50),  
 @MaSP varchar(30),  
 @TenSP nvarchar(255),  
 @SoToKhaiXuat int,  
 @NgayDangKyXuat datetime,  
 @NgayHoanThanhXuat datetime,  
 @MaLoaiHinhXuat char(5),  
 @LuongSPXuat numeric(18, 8),  
 @TenDVT_SP varchar(50),  
 @DinhMuc numeric(18, 8),  
 @LuongNPLSuDung numeric(18, 8),  
 @SoToKhaiTaiXuat int,  
 @NgayTaiXuat datetime,  
 @LuongNPLTaiXuat numeric(18, 8),  
 @LuongTonCuoi numeric(18, 8),  
 @ThanhKhoanTiep nvarchar(255),  
 @ChuyenMucDichKhac nvarchar(255),  
 @DonGiaTT float,  
 @TyGiaTT money,  
 @ThueSuat numeric(5, 2),  
 @ThueXNK float,  
 @ThueXNKTon float,  
 @NgayThucXuat datetime,  
 @ID bigint OUTPUT  
AS  
INSERT INTO [dbo].[t_KDT_SXXK_BCXuatNhapTon]  
(  
 [STT],  
 [LanThanhLy],  
 [NamThanhLy],  
 [MaDoanhNghiep],
 SoThuTuHang,  
 [MaNPL],  
 [TenNPL],  
 [SoToKhaiNhap],  
 [NgayDangKyNhap],  
 [NgayHoanThanhNhap],  
 [MaLoaiHinhNhap],  
 [LuongNhap],  
 [LuongTonDau],  
 [TenDVT_NPL],  
 [MaSP],  
 [TenSP],  
 [SoToKhaiXuat],  
 [NgayDangKyXuat],  
 [NgayHoanThanhXuat],  
 [MaLoaiHinhXuat],  
 [LuongSPXuat],  
 [TenDVT_SP],  
 [DinhMuc],  
 [LuongNPLSuDung],  
 [SoToKhaiTaiXuat],  
 [NgayTaiXuat],  
 [LuongNPLTaiXuat],  
 [LuongTonCuoi],  
 [ThanhKhoanTiep],  
 [ChuyenMucDichKhac],  
 [DonGiaTT],  
 [TyGiaTT],  
 [ThueSuat],  
 [ThueXNK],  
 [ThueXNKTon],  
[NgayThucXuat]  
)  
VALUES   
(  
 @STT,  
 @LanThanhLy,  
 @NamThanhLy,  
 @MaDoanhNghiep, 
 @SoThuTuHang, 
 @MaNPL,  
 @TenNPL,  
 @SoToKhaiNhap,  
 @NgayDangKyNhap,  
 @NgayHoanThanhNhap,  
 @MaLoaiHinhNhap,  
 @LuongNhap,  
 @LuongTonDau,  
 @TenDVT_NPL,  
 @MaSP,  
 @TenSP,  
 @SoToKhaiXuat,  
 @NgayDangKyXuat,  
 @NgayHoanThanhXuat,  
 @MaLoaiHinhXuat,  
 @LuongSPXuat,  
 @TenDVT_SP,  
 @DinhMuc,  
 @LuongNPLSuDung,  
 @SoToKhaiTaiXuat,  
 @NgayTaiXuat,  
 @LuongNPLTaiXuat,  
 @LuongTonCuoi,  
 @ThanhKhoanTiep,  
 @ChuyenMucDichKhac,  
 @DonGiaTT,  
 @TyGiaTT,  
 @ThueSuat,  
 @ThueXNK,  
 @ThueXNKTon,  
 @NgayThucXuat  
)  
  
SET @ID = SCOPE_IDENTITY()  


GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_NPLNhapTon_Insert]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_Insert]
GO  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_SXXK_NPLNhapTon_Insert]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, August 28, 2008  
------------------------------------------------------------------------------------------------------------------------  
GO 
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_Insert]  
 @SoToKhai int,  
 @MaLoaiHinh char(5),  
 @NamDangKy smallint,  
 @MaHaiQuan char(6),  
 @MaNPL varchar(30), 
 @SoThuTuHang INT,  
 @LanThanhLy int,  
 @Luong numeric(18, 8),  
 @TonDau numeric(18, 8),  
 @TonCuoi numeric(18, 8),  
 @ThueXNK float,  
 @ThueTTDB float,  
 @ThueVAT float,  
 @PhuThu float,  
 @ThueCLGia float,  
 @TonDauThueXNK float,  
 @TonCuoiThueXNK float,  
 @MaDoanhNghiep varchar(14)  
AS  
INSERT INTO [dbo].[t_KDT_SXXK_NPLNhapTon]  
(  
 [SoToKhai],  
 [MaLoaiHinh],  
 [NamDangKy],  
 [MaHaiQuan],  
 [MaNPL],
 SoThuTuHang,  
 [LanThanhLy],  
 [Luong],  
 [TonDau],  
 [TonCuoi],  
 [ThueXNK],  
 [ThueTTDB],  
 [ThueVAT],  
 [PhuThu],  
 [ThueCLGia],  
 [TonDauThueXNK],  
 [TonCuoiThueXNK],  
 [MaDoanhNghiep]  
)  
VALUES  
(  
 @SoToKhai,  
 @MaLoaiHinh,  
 @NamDangKy,  
 @MaHaiQuan,  
 @MaNPL,
 @SoThuTuHang,  
 @LanThanhLy,  
 @Luong,  
 @TonDau,  
 @TonCuoi,  
 @ThueXNK,  
 @ThueTTDB,  
 @ThueVAT,  
 @PhuThu,  
 @ThueCLGia,  
 @TonDauThueXNK,  
 @TonCuoiThueXNK,  
 @MaDoanhNghiep  
)  
  
GO
IF NOT EXISTS   (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='t_KDT_SXXK_BCThueXNK' AND COLUMN_NAME = 'SoThuTuHang')
	ALTER TABLE  t_KDT_SXXK_BCThueXNK
	ADD SoThuTuHang INT
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]
GO  
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]      
-- Database: Haiquan      
-- Author: ecs  
-- Alter Phiph 13/01/2015  
-- Time created: Tuesday, July 01, 2008      
------------------------------------------------------------------------------------------------------------------------      
      
CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]      
 @WhereCondition nvarchar(max),      
 @OrderByExpression nvarchar(250) = NULL      
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL nvarchar(max)      
      
SET @SQL = 'SELECT a.ID,      
 a.STT,      
 a.LanThanhLy,      
 a.NamThanhLy,      
 a.MaDoanhNghiep,      
 Case When MaLoaiHinhNhap like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiNhap]) else [SoToKhaiNhap] end as SoToKhaiNhap ,      
 a.NgayDangKyNhap,      
 Case When MaLoaiHinhNhap like ''%V%'' Then Substring(MaLoaiHinhNhap,3,3) else MaLoaiHinhNhap end as MaLoaiHinhNhap,      
 --a.NgayThucNhap,    
 case WHEN year(a.NgayThucNhap)<=1900 then (select NgayHoanThanh from t_sxxk_tokhaimaudich where sotokhai = a.sotokhainhap and MaLoaiHinh= a.MaLoaiHinhNhap and Ngaydangky= a.Ngaydangkynhap ) else a.NgayThucNhap end as NgayThucNhap,      
 UPPER(a.MaNPL) AS MaNPL,      
 case WHEN a.TenNPL is NULL OR a.TenNPL ='''' then b.Ten else a.TenNPL end as TenNPL,
 a.SoThuTuHang,      
 a.LuongNhap,      
 a.TenDVT_NPL,      
 a.DonGiaTT,      
 a.TyGiaTT,      
 a.ThueSuat,      
 a.ThueNKNop,      
 Case When MaLoaiHinhXuat like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiXuat]) else [SoToKhaiXuat] end as SoToKhaiXuat,      
 a.NgayDangKyXuat,      
 case WHEN year(a.NgayThucXuat)<=1900 then null else a.NgayThucXuat end as NgayThucXuat,      
 Case When MaLoaiHinhXuat like ''%V%'' Then Substring(MaLoaiHinhXuat,3,3) else MaLoaiHinhXuat end as MaLoaiHinhXuat,      
 Case When a.LuongNPLSuDung+ a.LuongNPLTon>Luong then a.Luong - a.LuongNPLTon else a.LuongNPLSuDung end AS LuongNPLSuDung ,      
 --a.LuongNPLSuDung,      
 a.LuongNPLTon,      
 a.TienThueHoan,      
 a.TienThueTKTiep,      
 a.GhiChu,      
 a.ThueXNK,      
 a.Luong      
 FROM t_KDT_SXXK_BCThueXNK a      
 INNER JOIN t_SXXK_NguyenPhuLieu b      
 ON a.MaNPL  = b.Ma AND a.MaDoanhNghiep = b.MaDoanhNghiep WHERE ' + @WhereCondition      
      
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0      
BEGIN      
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression      
END      
      
EXEC sp_executesql @SQL      
      
GO
  
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_BCThueXNK_Insert]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BCThueXNK_Insert]
GO    
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCThueXNK_Insert]  
-- Database: Haiquan  
-- Author: Ngo Thanh Tung  
-- Time created: Tuesday, July 01, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCThueXNK_Insert]  
 @STT bigint,  
 @LanThanhLy int,  
 @NamThanhLy int,  
 @MaDoanhNghiep varchar(14),  
 @SoToKhaiNhap int,  
 @NgayDangKyNhap datetime,  
 @NgayThucNhap datetime,  
 @MaLoaiHinhNhap char(5),
 @SoThuTuHang INT,  
 @MaNPL varchar(30),  
 @LuongNhap numeric(18, 8),  
 @TenDVT_NPL varchar(50),  
 @DonGiaTT numeric(38, 15),  
 @TyGiaTT numeric(18, 8),  
 @ThueSuat numeric(18, 8),  
 @ThueNKNop numeric(18, 0),  
 @SoToKhaiXuat int,  
 @NgayDangKyXuat datetime,  
 @NgayThucXuat datetime,  
 @MaLoaiHinhXuat char(5),  
 @LuongNPLSuDung numeric(18, 8),  
 @LuongNPLTon numeric(18, 8),  
 @TienThueHoan numeric(18, 0),  
 @TienThueTKTiep numeric(18, 0),  
 @GhiChu nvarchar(250),  
 @TenNPL nvarchar(250),  
 @Luong numeric(18, 8),  
 @ThueXNK numeric(18, 0),  
 @ID bigint OUTPUT  
AS  
INSERT INTO [dbo].[t_KDT_SXXK_BCThueXNK]  
(  
 [STT],  
 [LanThanhLy],  
 [NamThanhLy],  
 [MaDoanhNghiep],  
 [SoToKhaiNhap],  
 [NgayDangKyNhap],  
 [NgayThucNhap],  
 [MaLoaiHinhNhap],
 SoThuTuHang,  
 [MaNPL],  
 [LuongNhap],  
 [TenDVT_NPL],  
 [DonGiaTT],  
 [TyGiaTT],  
 [ThueSuat],  
 [ThueNKNop],  
 [SoToKhaiXuat],  
 [NgayDangKyXuat],  
 [NgayThucXuat],  
 [MaLoaiHinhXuat],  
 [LuongNPLSuDung],  
 [LuongNPLTon],  
 [TienThueHoan],  
 [TienThueTKTiep],  
 [GhiChu],  
 [TenNPL],  
 [Luong],  
 [ThueXNK]  
)  
VALUES   
(  
 @STT,  
 @LanThanhLy,  
 @NamThanhLy,  
 @MaDoanhNghiep,  
 @SoToKhaiNhap,  
 @NgayDangKyNhap,  
 @NgayThucNhap,  
 @MaLoaiHinhNhap,
 @SoThuTuHang,  
 @MaNPL,  
 @LuongNhap,  
 @TenDVT_NPL,  
 @DonGiaTT,  
 @TyGiaTT,  
 @ThueSuat,  
 @ThueNKNop,  
 @SoToKhaiXuat,  
 @NgayDangKyXuat,  
 @NgayThucXuat,  
 @MaLoaiHinhXuat,  
 @LuongNPLSuDung,  
 @LuongNPLTon,  
 @TienThueHoan,  
 @TienThueTKTiep,  
 @GhiChu,  
 @TenNPL,  
 @Luong,  
 @ThueXNK  
)  
  
SET @ID = SCOPE_IDENTITY()  

GO
------------------------------------------------------------------------------------------------------------------------        
-- Stored procedure name: [dbo].[p_GC_DinhMucDangKy_SelectAll]        
-- Database: Haiquan        
-- Author: Ngo Thanh Tung        
-- Time created: Monday, March 31, 2008        
------------------------------------------------------------------------------------------------------------------------        
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_DanhSachNPLNhapTon]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DanhSachNPLNhapTon]
GO         
CREATE PROCEDURE [dbo].[p_KDT_SXXK_DanhSachNPLNhapTon]        
 @BangKeHoSoThanhLy_ID bigint,        
 @SoThapPhanNPL int        
AS        
        
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED        
SELECT        
 SoToKhai,        
 MaLoaiHinh,        
 NamDangKy,        
 NgayDangKy,        
 MaHaiQuan,  
 SoDong,      
 MaNPL,        
 TenNPL,        
 TenDVT_NPL,        
 round(Luong,@SoThapPhanNPL) as Luong,        
 round(TonDau,@SoThapPhanNPL) as TonDau ,        
 round(TonCuoi,@SoThapPhanNPL) as TonCuoi,        
 NgayThucNhap,        
 NgayHoanThanh        
    ,BangKeHoSoThanhLy_ID        
    ,round(ThueXNK,0)as ThueXNK        
    ,TonDauThueXNK        
    ,TonCuoiThueXNK        
    ,DonGiaTT        
    ,ThueSuat        
    ,TyGiaTT        
 ,0 as LanDieuChinh        
FROM        
 dbo.v_KDT_SXXK_NPLNhapTon        
where BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID        
ORDER BY       
NgayHoanThanh,--HungTQ Updated 29/10/2010. Bo sung tieu chi sap xep theo 'NgayHoanThanh' vi thuc hien thanh khoan uu tien TK co ngay hoan thanh nho nhat.      
--NgayThucNhap, --Comment by Hungtq 06/10/2011    
--NgayDangKy, --Comment by Hungtq 06/10/2011    
SoToKhai,MaNPL,DonGiaTT 

GO
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[v_KDT_SXXK_NPLNhapTon]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP VIEW [dbo].[v_KDT_SXXK_NPLNhapTon]
GO  
CREATE VIEW [dbo].[v_KDT_SXXK_NPLNhapTon]  
AS  
    SELECT  CASE WHEN d.Ten IS NULL  
                 THEN ( SELECT  Ten  
                        FROM    t_HaiQuan_DonViTinh  
                        WHERE   ID = CAST(v.DVT_ID AS CHAR(2))  
                      )  
                 ELSE d.Ten  
            END AS TenDVT_NPL ,  
            v.*  
    FROM    ( SELECT    a.SoToKhai ,  
                        a.MaLoaiHinh ,  
                        a.NamDangKy ,  
                        a.NgayDangKy ,  
                        a.MaHaiQuan ,  
                        b.SoThuTuHang AS SoDong,  
                        b.MaPhu AS MaNPL ,  
                        b.TenHang AS TenNPL ,   
        --d.Ten AS TenDVT_NPL,  
                        b.DVT_ID ,  
                        ISNULL(c.Luong, b.SoLuong) AS Luong ,  
                        ISNULL(c.Ton, b.SoLuong) AS TonDau ,  
                        ISNULL(c.Ton, b.SoLuong) AS TonCuoi ,  
                        CASE WHEN YEAR(e.NGAY_THN_THX) > 1900  
                             THEN e.NGAY_THN_THX  
                             ELSE a.NgayDangKy  
                        END AS NgayThucNhap ,  
                        a.BangKeHoSoThanhLy_ID ,  
                        b.ThueXNK ,  
                        b.ThueXNK AS TonDauThueXNK ,  
                        b.ThueXNK AS TonCuoiThueXNK ,  
                        b.DonGiaTT ,  
                        b.ThueSuatXNK AS ThueSuat ,  
                        e.TyGiaTinhThue AS TyGiaTT ,  
                        e.NgayHoanThanh  
              FROM      dbo.t_KDT_SXXK_BKToKhaiNhap AS a  
                        INNER JOIN dbo.t_SXXK_ToKhaiMauDich AS e ON a.SoToKhai = e.SoToKhai  
                                                              AND a.MaLoaiHinh = e.MaLoaiHinh  
                                                              AND a.NamDangKy = e.NamDangKy  
                                                              AND a.MaHaiQuan = e.MaHaiQuan  
                        INNER JOIN dbo.t_SXXK_HangMauDich AS b ON a.SoToKhai = b.SoToKhai  
                                                              AND a.MaLoaiHinh = b.MaLoaiHinh  
                                                              AND a.NamDangKy = b.NamDangKy  
                                                              AND a.MaHaiQuan = b.MaHaiQuan  
                        LEFT OUTER JOIN dbo.t_SXXK_ThanhLy_NPLNhapTon AS c ON a.SoToKhai = c.SoToKhai  
                                                              AND a.MaLoaiHinh = c.MaLoaiHinh  
                                                              AND a.NamDangKy = c.NamDangKy  
                                                              AND a.MaHaiQuan = c.MaHaiQuan  
                                                              AND b.MaPhu = c.MaNPL
                                                              AND b.SoThuTuHang=c.SoThuTuHang                                                                
                                                              AND CONVERT(DECIMAL(20,  
                                                              0), ROUND(b.ThueXNK  
                                                              / b.SoLuong, 0)) = CONVERT(DECIMAL(20,  
                                                              0), ROUND(c.ThueXNK  
                                                              / c.Luong, 0))  
        --INNER JOIN dbo.t_HaiQuan_DonViTinh AS d ON d.ID = b.DVT_ID  
 --ORDER BY a.NgayDangKy  
            ) AS v  
            LEFT JOIN dbo.t_HaiQuan_DonViTinh AS d ON d.ID = v.DVT_ID  

GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_BKToKhaiNhap_KiemTraChenhLechSoLuongHMD]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiNhap_KiemTraChenhLechSoLuongHMD]
GO 
CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiNhap_KiemTraChenhLechSoLuongHMD]     
 -- Add the parameters for the stored procedure here    
    @LanThanhLy INT  
AS  
    BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
        SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
        SELECT  HangNhap.SoToKhai ,  
                HangNhap.NamDangKy ,  
                HangNhap.MaLoaiHinh ,  
                HangNhap.MaPhu AS MaNPL , 
				HangNhap.SoThuTuHang, 
                HangNhap.SoLuong ,  
                KDT.SoLuongKB ,  
                HangNhap.TenHang  
        FROM    t_SXXK_HangMauDich AS HangNhap  
                INNER JOIN ( SELECT KDT_HMD.MaPhu ,
									KDT_HMD.SoThuTuHang,  
                                    KDT_HMD.DVT_ID ,  
                                    KDT_HMD.TKMD_ID ,  
                                    SUM(KDT_HMD.SoLuong) AS SoLuongKB ,  
                                    KDT_Tokhai.SoToKhai ,  
                                    KDT_Tokhai.MaHaiQuan ,  
                                    KDT_Tokhai.MaLoaiHinh ,  
                                    KDT_Tokhai.NamDangKy ,  
                                    DonGiaTT  
                             FROM   t_KDT_HangMauDich AS KDT_HMD  
                                    INNER JOIN ( SELECT SoToKhai ,  
                                                        MaLoaiHinh ,  
                                                        MaHaiQuan ,  
                                                        YEAR(NgayDangKy) AS NamDangKy ,  
                                                        ID  
                                                 FROM   t_KDT_ToKhaiMauDich  
                                                 WHERE  ( ID IN (  
                                                          SELECT  
                                                              KDT.ID  
                                                          FROM  
                                                              ( SELECT  
                                                              SoToKhai ,  
                                                              MaLoaiHinh ,  
                                                              NamDangKy ,  
                                                              MaHaiQuan  
                                                              FROM  
                                                              t_KDT_SXXK_BKToKhaiNhap  
                                                              WHERE  
                                                              ( BangKeHoSoThanhLy_ID = ( SELECT TOP ( 1 )  
                                                              ID  
                                                              FROM  
                                                              t_KDT_SXXK_BangKeHoSoThanhLy  
                                                              WHERE  
                                                              ( MaBangKe = 'DTLTKN' )  
                                                              AND ( MaterID = ( SELECT TOP ( 1 )  
                                                              ID  
                                                              FROM  
                                                              t_KDT_SXXK_HoSoThanhLyDangKy  
                                                              WHERE  
                                                              ( LanThanhLy = @LanThanhLy )  
                                                              ) )  
                                                              ) )  
                                                              ) AS ThanhKhoan  
                                                              INNER JOIN t_KDT_ToKhaiMauDich  
                                                              AS KDT ON KDT.SoToKhai = ThanhKhoan.SoToKhai  
                                                              AND KDT.MaLoaiHinh = ThanhKhoan.MaLoaiHinh  
                                                              AND YEAR(KDT.NgayDangKy) = ThanhKhoan.NamDangKy  
                                                              AND KDT.MaHaiQuan = ThanhKhoan.MaHaiQuan ) )  
                                               ) AS KDT_Tokhai ON KDT_HMD.TKMD_ID = KDT_Tokhai.ID  
                             GROUP BY KDT_HMD.TKMD_ID ,  
                                    KDT_Tokhai.SoToKhai ,  
                                    KDT_Tokhai.MaLoaiHinh ,  
                                    KDT_Tokhai.MaHaiQuan ,  
                                    KDT_Tokhai.NamDangKy ,  
                                    KDT_HMD.MaPhu ,
									KDT_HMD.SoThuTuHang,  
                                    KDT_HMD.DVT_ID ,  
                                    KDT_HMD.DonGiaTT  
                           ) AS KDT ON KDT.SoToKhai = HangNhap.SoToKhai  
                                       AND KDT.MaPhu = HangNhap.MaPhu  
									   AND KDT.SoThuTuHang = HangNhap.SoThuTuHang
                                       AND KDT.MaHaiQuan = HangNhap.MaHaiQuan  
                                       AND KDT.MaLoaiHinh = HangNhap.MaLoaiHinh  
                                       AND KDT.NamDangKy = HangNhap.NamDangKy  
                                       AND KDT.DonGiaTT = HangNhap.DonGiaTT  
                                       AND HangNhap.SoLuong <> KDT.SoLuongKB    
    
    END    
    

GO
GO
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_SXXK_NPLNhapTon_SelectDynamic]    
-- Database: HaiQuanLuoi    
-- Author: Ngo Thanh Tung    
-- Time created: Thursday, August 28, 2008    
------------------------------------------------------------------------------------------------------------------------    
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_SXXK_NPLNhapTon_SelectDynamic]') AND TYPE IN (N'P',N'PC',N'V',N'FN'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_SelectDynamic]
GO     
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NPLNhapTon_SelectDynamic]    
 @WhereCondition nvarchar(500),    
 @OrderByExpression nvarchar(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL nvarchar(3250)    
    
SET @SQL = 'SELECT  [SoToKhai],    
 [MaLoaiHinh],    
 [NamDangKy],    
 [MaHaiQuan],    
 [MaNPL],  
 SoThuTuHang,    
 [LanThanhLy],    
 [Luong],    
 [TonDau],    
 [TonCuoi],    
 [ThueXNK],    
 [ThueTTDB],    
 [ThueVAT],    
 [PhuThu],    
 [ThueCLGia],    
 [TonDauThueXNK],    
 [TonCuoiThueXNK],    
 [MaDoanhNghiep]    
 FROM [dbo].[t_KDT_SXXK_NPLNhapTon] WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL 
GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '34.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('34.8',GETDATE(), N'CẬP NHẬT KHAI BÁO BÁO BÁO CÁO QUYẾT TOÁN')
END