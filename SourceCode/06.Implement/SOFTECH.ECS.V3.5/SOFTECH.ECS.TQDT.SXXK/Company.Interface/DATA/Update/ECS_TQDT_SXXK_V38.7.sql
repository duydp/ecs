GO
IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_Voucher]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Voucher]
GO
CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Voucher]
AS 
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL nvarchar(MAX)      
      
SET @SQL = 'SELECT ID ,
       TrangThaiXuLy ,
       SoTN ,
       NgayTN ,
       MaHQ ,
       MaDoanhNghiep ,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE TKMD_ID END AS SoToKhai,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaLoaiHinh FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaLoaiHinh,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 NgayDangKy FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS NgayDangKy,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoHoaDon FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS SoHoaDon,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 HopDong_ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS HopDong_ID,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaPhanLoaiKiemTra FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaPhanLoaiKiemTra,
       TKMD_ID ,
       GhiChuKhac ,
       FileName ,
       Content ,
	   ''CertificateOfOrigin'' AS LoaiChungTu,
       GuidStr FROM dbo.t_KDT_VNACCS_CertificateOfOrigin WHERE TKMD_ID > 0 AND  TrangThaiXuLy NOT IN (''0'') AND SoTN = 0 AND GuidStr IS NOT NULL
UNION ALL
SELECT ID ,
       TrangThaiXuLy ,
       SoTN ,
       NgayTN ,
       MaHQ ,
       MaDoanhNghiep ,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE TKMD_ID END AS SoToKhai,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaLoaiHinh FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaLoaiHinh,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 NgayDangKy FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS NgayDangKy,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoHoaDon FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS SoHoaDon,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 HopDong_ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS HopDong_ID,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaPhanLoaiKiemTra FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaPhanLoaiKiemTra,
       TKMD_ID ,
       GhiChuKhac ,
       FileName ,
       Content ,
	   ''BillOfLading'' AS LoaiChungTu,
       GuidStr FROM dbo.t_KDT_VNACCS_BillOfLading WHERE TKMD_ID > 0 AND  TrangThaiXuLy NOT IN (''0'') AND SoTN = 0 AND GuidStr IS NOT NULL
UNION ALL
SELECT ID ,
       TrangThaiXuLy ,
       SoTN ,
       NgayTN ,
       MaHQ ,
       MaDoanhNghiep ,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE TKMD_ID END AS SoToKhai,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaLoaiHinh FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaLoaiHinh,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 NgayDangKy FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS NgayDangKy,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoHoaDon FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS SoHoaDon,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 HopDong_ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS HopDong_ID,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaPhanLoaiKiemTra FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaPhanLoaiKiemTra,
       TKMD_ID ,
       GhiChuKhac ,
       FileName ,
       Content ,
	   ''ContractDocument'' AS LoaiChungTu,
       GuidStr FROM dbo.t_KDT_VNACCS_ContractDocument WHERE TKMD_ID > 0 AND  TrangThaiXuLy NOT IN (''0'') AND SoTN = 0 AND GuidStr IS NOT NULL
UNION ALL 
SELECT ID ,
       TrangThaiXuLy ,
       SoTN ,
       NgayTN ,
       MaHQ ,
       MaDoanhNghiep ,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE TKMD_ID END AS SoToKhai,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaLoaiHinh FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaLoaiHinh,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 NgayDangKy FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS NgayDangKy,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoHoaDon FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS SoHoaDon,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 HopDong_ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS HopDong_ID,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaPhanLoaiKiemTra FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaPhanLoaiKiemTra,
       TKMD_ID ,
       GhiChuKhac ,
       FileName ,
       Content ,
	   ''CommercialInvoice'' AS LoaiChungTu,
       GuidStr FROM dbo.t_KDT_VNACCS_CommercialInvoice WHERE TKMD_ID > 0 AND  TrangThaiXuLy NOT IN (''0'') AND SoTN = 0 AND GuidStr IS NOT NULL
UNION ALL
SELECT ID ,
       TrangThaiXuLy ,
       SoTN ,
       NgayTN ,
       MaHQ ,
       MaDoanhNghiep ,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE TKMD_ID END AS SoToKhai,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaLoaiHinh FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaLoaiHinh,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 NgayDangKy FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS NgayDangKy,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoHoaDon FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS SoHoaDon,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 HopDong_ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS HopDong_ID,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaPhanLoaiKiemTra FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaPhanLoaiKiemTra,
       TKMD_ID ,
       GhiChuKhac ,
       FileName ,
       Content ,
	   ''License'' AS LoaiChungTu,
       GuidStr FROM dbo.t_KDT_VNACCS_License WHERE TKMD_ID > 0 AND  TrangThaiXuLy NOT IN (''0'') AND SoTN = 0 AND GuidStr IS NOT NULL
UNION ALL
SELECT ID ,
       TrangThaiXuLy ,
       SoTN ,
       NgayTN ,
       MaHQ ,
       MaDoanhNghiep ,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE TKMD_ID END AS SoToKhai,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaLoaiHinh FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaLoaiHinh,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 NgayDangKy FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS NgayDangKy,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoHoaDon FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS SoHoaDon,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 HopDong_ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS HopDong_ID,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaPhanLoaiKiemTra FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaPhanLoaiKiemTra,
       TKMD_ID ,
       GhiChuKhac ,
       FileName ,
       Content ,
	   ''Container'' AS LoaiChungTu,
       GuidStr FROM dbo.t_KDT_VNACCS_Container_Details 	WHERE TKMD_ID > 0  AND  TrangThaiXuLy NOT IN (''0'') AND SoTN = 0 AND GuidStr IS NOT NULL
UNION ALL
SELECT ID ,
       TrangThaiXuLy ,
       SoTN ,
       NgayTN ,
       MaHQ ,
       MaDoanhNghiep ,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE TKMD_ID END AS SoToKhai,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaLoaiHinh FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaLoaiHinh,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 NgayDangKy FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS NgayDangKy,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 SoHoaDon FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS SoHoaDon,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 HopDong_ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS HopDong_ID,
	   CASE WHEN TKMD_ID > 0 THEN (SELECT TOP 1 MaPhanLoaiKiemTra FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE TKMD_ID=ID) ELSE NULL END AS MaPhanLoaiKiemTra,
       TKMD_ID ,
       GhiChuKhac ,
       FileName ,
       Content ,
	   ''AdditionalDocument'' AS LoaiChungTu,
       GuidStr
        FROM dbo.t_KDT_VNACCS_AdditionalDocument WHERE TKMD_ID > 0 AND  TrangThaiXuLy NOT IN (''0'') AND SoTN = 0 AND GuidStr IS NOT NULL'      
              
EXEC sp_executesql @SQL   

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '38.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('38.7',GETDATE(), N'CẬP NHẬT PROCDURE AUTO FEEDBACK CHỨNG TỪ ')
END