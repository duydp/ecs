using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Activation.Server
{
    public partial class Invoice
    {
        private string _CustomerName = string.Empty;

        protected string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAllBy()
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_SelectAllBy]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }
    }
}