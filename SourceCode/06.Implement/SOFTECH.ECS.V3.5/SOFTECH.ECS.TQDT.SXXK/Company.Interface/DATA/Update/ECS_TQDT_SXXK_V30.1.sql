
DELETE t_VNACC_Category_Cargo where BondedAreaCode in('02B1A03','02B1A04','02B1AB1','02B1C01','02B1D03','02B1F03','02B1W05','02B1W06','02B1W07')

IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1A03')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1A03','KHO TCS        ')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1A04')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1A04','KHO SCSC      ')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1AB1')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1AB1','DOI TT HH XNK TSN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1C01')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1C01','KHO CFS SCSC ')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1D03')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1D03','MIEN THUE SASCO')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1F03')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1F03','BAO THUE CATERING')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1W05')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1W05','KNQ TCS   ')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1W06')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1W06','KNQ SCSC   ')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='02B1W07')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','02B1W07','KNQ WF41    ')

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.1',GETDATE(), N' Cap nhat ma luu kho cho thong quan')
END