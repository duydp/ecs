﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.Interface.VNACCS;
namespace Company.Interface
{
    public partial class VNACC_TKNhapTriGiaForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_TKTriGiaThap TKTriGiaThap = new KDT_VNACCS_TKTriGiaThap();
        public VNACC_TKNhapTriGiaForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.IDA.ToString());
            
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdLuu":
                    Save();
                    break;
                case "cmdMIC":
                    SendVnaccs(false);
                    break;
                case "cmdMIE":
                    SendVnaccs(true);
                    break;
                case "cmdMID":
                    //SendVnaccsMID();
                    break;
                case "cmdMEC":
                    //SendVnaccsMEC(false);
                    break;
                case "cmdMEE":
                    //SendVnaccsMEC(true);
                    break;
                case "cmdMED":
                    //SendVnaccsMED();
                    break;
                case "cmdThongTinHQ":
                    ShowKetQuaTraVe();
                    break;
                case "cmdKetQuaXuLy":
                    ShowKetQuaXuLy();
                    break;
                case "cmdInTamMIC":
                    InToKhaiTam();
                    break;  
            }
        }
        private void InToKhaiTam()
        {
            try
            {
                MsgLog msg = MsgLog.GetThongTinToKhaiTam(TKTriGiaThap.InputMessageID, "MIC");
                if (msg != null && msg.ID > 0)
                    ProcessReport.ShowReport(msg.ID, "VAD1AC0", new ResponseVNACCS(msg.Log_Messages).ResponseData);
                else
                    ShowMessage("Không tìm thấy bản khai tạm", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ShowKetQuaXuLy()
        {
            try
            {
                VNACC_KetQuaXuLy f = new VNACC_KetQuaXuLy(TKTriGiaThap.ID, "MIC");
                //f.master_id = TKMD.ID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ShowKetQuaTraVe()
        {
            try
            {
                SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
                f.isSend = false;
                f.isRep = true;
                f.inputMSGID = TKTriGiaThap.InputMessageID;
                f.SoToKhai = this.TKTriGiaThap.SoToKhai.ToString().Length > 11 ? this.TKTriGiaThap.SoToKhai.ToString().Substring(0, 11) : string.Empty;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void GetTKTG()
        {
            try
            {
                TKTriGiaThap.MaLoaiHinh = "I";
                //TKTriGiaThap.SoToKhai = Convert.ToInt32(txtSoToKhai.Value);
                TKTriGiaThap.PhanLoaiToChuc = ctrMaNuocDoiTac.Code;
                TKTriGiaThap.CoQuanHaiQuan = ctrCoQuanHaiQuan.Code;
                TKTriGiaThap.NhomXuLyHS = "";// ctrNhomXuLyHS.Code;

                //don vi
                TKTriGiaThap.MaDonVi = txtMaDonVi.Text;
                TKTriGiaThap.TenDonVi = txtTenDonVi.Text;
                TKTriGiaThap.MaBuuChinhDonVi = txtMaBuuChinhDonVi.Text;
                TKTriGiaThap.DiaChiDonVi = txtDiaChiDonVi.Text;
                TKTriGiaThap.SoDienThoaiDonVi = txtSoDienThoaiDonVi.Text;

                // doi tac
                TKTriGiaThap.MaDoiTac = txtMaDoiTac.Text;
                TKTriGiaThap.TenDoiTac = txtTenDoiTac.Text;
                TKTriGiaThap.MaBuuChinhDoiTac = txtMaBuuChinhDoiTac.Text;
                TKTriGiaThap.DiaChiDoiTac1 = txtDiaChiDoiTac1.Text;
                TKTriGiaThap.DiaChiDoiTac2 = txtDiaChiDoiTac2.Text;
                TKTriGiaThap.DiaChiDoiTac3 = txtDiaChiDoiTac3.Text;
                TKTriGiaThap.DiaChiDoiTac4 = txtDiaChiDoiTac4.Text;
                TKTriGiaThap.MaNuocDoiTac = ctrMaNuoc.Code;

                //so kien va trong luong
                TKTriGiaThap.SoLuong = Convert.ToInt32(txtSoLuong.Value);
                TKTriGiaThap.TrongLuong = Convert.ToInt32(txtTrongLuong.Value);
                TKTriGiaThap.SoHouseAWB = txtSoHouseAWB.Text;
                TKTriGiaThap.SoMasterAWB = txtSoMasterAWB.Text;
                // van don
                TKTriGiaThap.MaDDLuuKho = ctrMaDDLuuKho.Code;
                TKTriGiaThap.TenMayBayChoHang = txtTenMayBayChoHang.Text;
                TKTriGiaThap.NgayHangDen = clcNgayHangDen.Value;
                TKTriGiaThap.MaCangDoHang = ctrMaDiaDiemDoHang.Code;
                TKTriGiaThap.MaDiaDiemXepHang = ctrMaDiaDiemXepHang.Code;
                //Phi van chuyen, phi bao hiem
                TKTriGiaThap.MaPhanLoaiPhiVC = ctrMaPhiVC.Code;
                TKTriGiaThap.PhiVanChuyen = Convert.ToDecimal(txtSoTienPhiVC.Value);
                TKTriGiaThap.MaTTPhiVC = ctrMaTTPhiVC.Code;
                TKTriGiaThap.MaPhanLoaiPhiBH = ctrMaPhiBaoHiem.Code;
                TKTriGiaThap.PhiBaoHiem = Convert.ToDecimal(txtSoTienPhiBH.Value);
                TKTriGiaThap.MaTTPhiBH = ctrMaTTPhiBH.Code;

                //Mo ta Hang Hoa
                TKTriGiaThap.MoTaHangHoa = txtMoTaHangHoa.Text;
                TKTriGiaThap.MaNuocXuatXu = ctrNuocXuatXu.Code;
                TKTriGiaThap.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Value);
                //hoa don
                TKTriGiaThap.PhanLoaiGiaHD = ctrPhanLoaiGiaHD.Code;
                TKTriGiaThap.MaDieuKienGiaHD = txtMaDieuKienGiaHD.Text;
                TKTriGiaThap.TongTriGiaHD = Convert.ToDecimal(txtTongTriGiaHD.Value);
                TKTriGiaThap.MaTTHoaDon = ctrMaTTHoaDon.Code;

                TKTriGiaThap.GhiChu = txtGhiChu.Text;
                TKTriGiaThap.SoQuanLyNoiBoDN = txtSoQuanLyNoiBoDN.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void SetTKTG()
        {
            try
            {
                //txtSoToKhai.Text = TKTriGiaThap.SoToKhai.ToString();
                ctrMaNuocDoiTac.Code = TKTriGiaThap.PhanLoaiToChuc;
                ctrCoQuanHaiQuan.Code = TKTriGiaThap.CoQuanHaiQuan;
                ctrNhomXuLyHS.Code = TKTriGiaThap.NhomXuLyHS;
                //Don vi
                txtMaDonVi.Text = TKTriGiaThap.MaDonVi;
                txtTenDonVi.Text = TKTriGiaThap.TenDonVi;
                txtMaBuuChinhDonVi.Text = TKTriGiaThap.MaBuuChinhDonVi;
                txtDiaChiDonVi.Text = TKTriGiaThap.DiaChiDonVi;
                txtSoDienThoaiDonVi.Text = TKTriGiaThap.SoDienThoaiDonVi;
                //doi tac
                txtMaDoiTac.Text = TKTriGiaThap.MaDoiTac;
                txtTenDoiTac.Text = TKTriGiaThap.TenDoiTac;
                txtMaBuuChinhDoiTac.Text = TKTriGiaThap.MaBuuChinhDoiTac;
                txtDiaChiDoiTac1.Text = TKTriGiaThap.DiaChiDoiTac1;
                txtDiaChiDoiTac2.Text = TKTriGiaThap.DiaChiDoiTac2;
                txtDiaChiDoiTac3.Text = TKTriGiaThap.DiaChiDoiTac3;
                txtDiaChiDoiTac4.Text = TKTriGiaThap.DiaChiDoiTac4;
                ctrMaNuoc.Code = TKTriGiaThap.MaNuocDoiTac;
                //so kien va trong luong
                txtSoLuong.Text = TKTriGiaThap.SoLuong.ToString();
                txtTrongLuong.Text = TKTriGiaThap.TrongLuong.ToString();
                txtSoHouseAWB.Text = TKTriGiaThap.SoHouseAWB;
                txtSoMasterAWB.Text = TKTriGiaThap.SoMasterAWB;
                //van don
                ctrMaDDLuuKho.Code = TKTriGiaThap.MaDDLuuKho;
                txtTenMayBayChoHang.Text = TKTriGiaThap.TenMayBayChoHang;
                clcNgayHangDen.Value = TKTriGiaThap.NgayHangDen;
                ctrMaDiaDiemDoHang.Code = TKTriGiaThap.MaCangDoHang;
                ctrMaDiaDiemXepHang.Code = TKTriGiaThap.MaDiaDiemXepHang;
                //hoa don
                ctrPhanLoaiGiaHD.Code = TKTriGiaThap.PhanLoaiGiaHD;
                txtMaDieuKienGiaHD.Text = TKTriGiaThap.MaDieuKienGiaHD;
                ctrMaTTHoaDon.Code = TKTriGiaThap.MaTTHoaDon;
                txtTongTriGiaHD.Text = TKTriGiaThap.TongTriGiaHD.ToString();
                //phí van chuyen, phi bao hiem
                ctrMaPhiVC.Code = TKTriGiaThap.MaPhanLoaiPhiVC;
                txtSoTienPhiVC.Text = TKTriGiaThap.PhiVanChuyen.ToString();
                ctrMaTTPhiVC.Code = TKTriGiaThap.MaTTPhiVC;
                ctrMaPhiBaoHiem.Code = TKTriGiaThap.MaPhanLoaiPhiBH;
                txtSoTienPhiBH.Text = TKTriGiaThap.PhiBaoHiem.ToString();
                ctrMaTTPhiBH.Code = TKTriGiaThap.MaTTPhiBH;

                // Mo ta hang hoa
                txtMoTaHangHoa.Text = TKTriGiaThap.MoTaHangHoa;
                ctrNuocXuatXu.Code = TKTriGiaThap.MaNuocXuatXu;
                txtTriGiaTinhThue.Text = TKTriGiaThap.TriGiaTinhThue.ToString();

                txtGhiChu.Text = TKTriGiaThap.GhiChu;
                txtSoQuanLyNoiBoDN.Text = TKTriGiaThap.SoQuanLyNoiBoDN;
                //van don
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void Save()
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                GetTKTG();
                if (TKTriGiaThap.ID != 0)
                    TKTriGiaThap.Update();
                else
                    TKTriGiaThap.Insert();
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }


        }
        private void LoadData()
        {
            try
            {
                SetTKTG();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_TKNhapTriGiaForm_Load(object sender, EventArgs e)
        {
            try
            {
                SetIDControl();
                TuDongCapNhatThongTin();
                SetTKTG();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }


        #region Bổ sung nghiệp vụ

        private Janus.Windows.UI.CommandBars.UICommand cmdMIC;
        private Janus.Windows.UI.CommandBars.UICommand cmdMID;
        private Janus.Windows.UI.CommandBars.UICommand cmdMIE;

        private Janus.Windows.UI.CommandBars.UICommand cmdMEC;
        private Janus.Windows.UI.CommandBars.UICommand cmdMED;
        private Janus.Windows.UI.CommandBars.UICommand cmdMEE;
        private void BoSungNghiepVu(bool isToKhaiNhap)
        {
            this.cmdMIC = new Janus.Windows.UI.CommandBars.UICommand("cmdMIC");
            this.cmdMIC.Key = "cmdMIC";
            this.cmdMIC.Name = "cmdMIC";
            this.cmdMIC.Text = "nghiệp vụ MIC (Khai báo tờ khai)";

            this.cmdMID = new Janus.Windows.UI.CommandBars.UICommand("cmdMID");
            this.cmdMID.Key = "cmdMID";
            this.cmdMID.Name = "cmdMID";
            this.cmdMID.Text = "nghiệp vụ MID (Gọi tờ khai) ";

            this.cmdMIE = new Janus.Windows.UI.CommandBars.UICommand("cmdMIE");
            this.cmdMIE.Key = "cmdMIE";
            this.cmdMIE.Name = "cmdMIE";
            this.cmdMIE.Text = "nghiệp vụ MIE (Khai báo sửa tờ khai) ";

            this.cmdMIC = new Janus.Windows.UI.CommandBars.UICommand("cmdMEC");
            this.cmdMIC.Key = "cmdMEC";
            this.cmdMIC.Name = "cmdMEC";
            this.cmdMIC.Text = "nghiệp vụ MEC (Khai báo tờ khai)";

            this.cmdMID = new Janus.Windows.UI.CommandBars.UICommand("cmdMED");
            this.cmdMID.Key = "cmdMED";
            this.cmdMID.Name = "cmdMED";
            this.cmdMID.Text = "nghiệp vụ MED (Gọi tờ khai) ";

            this.cmdMIE = new Janus.Windows.UI.CommandBars.UICommand("cmdMEE");
            this.cmdMIE.Key = "cmdMEE";
            this.cmdMIE.Name = "cmdMEE";
            this.cmdMIE.Text = "nghiệp vụ MEE (Khai báo sửa tờ khai) ";

            /*this.cmd.Text = "Cập nhật thông tin ";*/
            if (isToKhaiNhap)
                this.cmdKhaiBao1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdMIC,
                this.cmdMID,
                this.cmdMIE
            });
            else
                this.cmdKhaiBao1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdMEC,
                this.cmdMED,
                this.cmdMEE
            });
        }

        #endregion Bổ sung nghiệp vụ

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            try
            {
                TKTriGiaThap = (KDT_VNACCS_TKTriGiaThap)ProcessMessages.GetDataResult_ToKhaiTriGiaThap(msgResult, EnumNghiepVu.MIC, TKTriGiaThap);
                //if (TKTriGiaThapNew.GetType() == typeof(KDT_VNACCS_TKTriGiaThap))
                //{
                //    TKTriGiaThap = HelperVNACCS.UpdateObject<KDT_VNACCS_TKTriGiaThap>(TKTriGiaThap, TKTriGiaThapNew);
                //}
                TKTriGiaThap.InsertUpdate();
                SetTKTG();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void TuDongCapNhatThongTin()
        {
            if (TKTriGiaThap != null && TKTriGiaThap.SoToKhai > 0 && TKTriGiaThap.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}'", TKTriGiaThap.SoToKhai.ToString()), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = "E";
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.Delete();
                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
        
                        msgPb.TrangThai = "E";
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }

            }
        }

        private void SendVnaccs(bool KhaiBaoSua)
        {
            try
            {
                if (TKTriGiaThap.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    MIC mic = VNACCMaperFromObject.MICMapper(TKTriGiaThap);
                    if (mic == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        TKTriGiaThap.InputMessageID = HelperVNACCS.NewInputMSGID();
                        TKTriGiaThap.InsertUpdate();
                        msg = MessagesSend.Load<MIC>(mic, TKTriGiaThap.InputMessageID);
                    }
                    else
                        msg = MessagesSend.Load<MIC>(mic, true, EnumNghiepVu.MIE, TKTriGiaThap.InputMessageID);

                    MsgLog.SaveMessages(msg, TKTriGiaThap.ID, EnumThongBao.SendMess, "");
                     SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = true;
                    f.inputMSGID = TKTriGiaThap.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result)
                    {
                        TuDongCapNhatThongTin();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        //private void SendVnaccsMID()
        //{
        //    try
        //    {
        //        if (TKTriGiaThap.ID == 0)
        //        {
        //            this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
        //            return;
        //        }
        //        if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
        //        {
        //            MID mid = VNACCMaperFromObject.MIDMapper(TKTriGiaThap.SoToKhai,"");
        //            if (mid == null)
        //            {
        //                this.ShowMessage("Lỗi khi tạo messages !", false);
        //                return;
        //            }
        //            MessagesSend msg;
        //            msg = MessagesSend.Load<MID>(mid,TKTriGiaThap.InputMessageID);
                    

        //            MsgLog.SaveMessages(msg, TKTriGiaThap.ID, EnumThongBao.SendMess, "");
        //            SendVNACCFrm f = new SendVNACCFrm(msg);
        //            f.ShowDialog(this);
        //            if (f.DialogResult == DialogResult.Cancel)
        //            {
        //                this.ShowMessage(f.msgFeedBack, false);
        //            }
        //            else
        //            {
        //                if (f.feedback.Error == null || f.feedback.Error.Count == 0)
        //                {

        //                    if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
        //                    {
        //                        CapNhatThongTin(f.feedback.ResponseData);
        //                        f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
        //                    }
        //                    else
        //                    {
        //                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
        //                        {
        //                            try
        //                            {
        //                                decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
        //                                TKTriGiaThap.SoToKhai = SoToKhai;
        //                                TKTriGiaThap.InsertUpdate();
        //                            }
        //                            catch (System.Exception ex)
        //                            {
        //                                f.msgFeedBack += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
        //                                Logger.LocalLogger.Instance().WriteMessage(ex);
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (ErrorVNACCS error in f.feedback.Error)
        //                    {
        //                        f.msgFeedBack += Environment.NewLine + "Lỗi : " + error.loadError();
        //                    }
        //                }

        //                this.ShowMessageTQDT(f.msgFeedBack, false);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.ShowMessage(ex.Message, false);
        //    }
        //}

        //private void SendVnaccsMED()
        //{
        //    try
        //    {
        //        if (TKTriGiaThap.ID == 0)
        //        {
        //            this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
        //            return;
        //        }
        //        if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
        //        {
        //            MED med = VNACCMaperFromObject.MEDMapper(TKTriGiaThap.SoToKhai);
        //            if (med == null)
        //            {
        //                this.ShowMessage("Lỗi khi tạo messages !", false);
        //                return;
        //            }
        //            MessagesSend msg;
        //            msg = MessagesSend.Load<MED>(med,TKTriGiaThap.InputMessageID);


        //            MsgLog.SaveMessages(msg, TKTriGiaThap.ID, EnumThongBao.SendMess, "");
        //            SendVNACCFrm f = new SendVNACCFrm(msg);
        //            f.ShowDialog(this);
        //            if (f.DialogResult == DialogResult.Cancel)
        //            {
        //                this.ShowMessage(f.msgFeedBack, false);
        //            }
        //            else
        //            {
        //                if (f.feedback.Error == null || f.feedback.Error.Count == 0)
        //                {

        //                    if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
        //                    {
        //                        CapNhatThongTin(f.feedback.ResponseData);
        //                        f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
        //                    }
        //                    else
        //                    {
        //                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
        //                        {
        //                            try
        //                            {
        //                                decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
        //                                TKTriGiaThap.SoToKhai = SoToKhai;
        //                                TKTriGiaThap.InsertUpdate();
        //                            }
        //                            catch (System.Exception ex)
        //                            {
        //                                f.msgFeedBack += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
        //                                Logger.LocalLogger.Instance().WriteMessage(ex);
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (ErrorVNACCS error in f.feedback.Error)
        //                    {
        //                        f.msgFeedBack += Environment.NewLine + "Lỗi : " + error.loadError();
        //                    }
        //                }

        //                this.ShowMessageTQDT(f.msgFeedBack, false);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.ShowMessage(ex.Message, false);
        //    }
        //}

        //private void SendVnaccsMEC(bool KhaiBaoSua)
        //{
        //    try
        //    {
        //        if (TKTriGiaThap.ID == 0)
        //        {
        //            this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
        //            return;
        //        }
        //        if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
        //        {
        //            MEC mic = VNACCMaperFromObject.MECMapper(TKTriGiaThap);
        //            if (mic == null)
        //            {
        //                this.ShowMessage("Lỗi khi tạo messages !", false);
        //                return;
        //            }
        //            MessagesSend msg;
        //            if (!KhaiBaoSua) msg = MessagesSend.Load<MEC>(mic,TKTriGiaThap.InputMessageID);
        //            else
        //                msg = MessagesSend.Load<MEC>(mic, true, EnumNghiepVu.MEE, TKTriGiaThap.InputMessageID);

        //            MsgLog.SaveMessages(msg, TKTriGiaThap.ID, EnumThongBao.SendMess, "");
        //            SendVNACCFrm f = new SendVNACCFrm(msg);
        //            f.ShowDialog(this);
        //            if (f.DialogResult == DialogResult.Cancel)
        //            {
        //                this.ShowMessage(f.msgFeedBack, false);
        //            }
        //            else
        //            {
        //                if (f.feedback.Error == null || f.feedback.Error.Count == 0)
        //                {
        //                    if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
        //                    {
        //                        CapNhatThongTin(f.feedback.ResponseData);
        //                        f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
        //                    }
        //                    else
        //                    {
        //                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
        //                        {
        //                            try
        //                            {
        //                                decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
        //                                TKTriGiaThap.SoToKhai = SoToKhai;
        //                                TKTriGiaThap.InsertUpdate();
        //                            }
        //                            catch (System.Exception ex)
        //                            {
        //                                f.msgFeedBack += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
        //                                Logger.LocalLogger.Instance().WriteMessage(ex);
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (ErrorVNACCS error in f.feedback.Error)
        //                    {
        //                        f.msgFeedBack += Environment.NewLine + "Lỗi : " + error.loadError();
        //                    }
        //                }

        //                this.ShowMessageTQDT(f.msgFeedBack, false);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.ShowMessage(ex.Message, false);
        //    }
        //}

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
               
                txtSoToKhai.Tag = "ICN"; //Số tờ khai
                //txtPhanLoaiBaoCaoSua.Tag = "JYO"; //Phân loại xuất báo cáo sửa đổi
                ctrMaNuocDoiTac.TagName = "SKB"; //Phân loại cá nhân/tổ chức
                ctrCoQuanHaiQuan.TagCode = "CH"; //Cơ quan Hải quan
                ctrNhomXuLyHS.Tag = "CHB"; //Nhóm xử lý hồ sơ
                txtMaDonVi.Tag = "IMC"; //Mã người nhập khẩu
                txtTenDonVi.Tag = "IMN"; //Tên người nhập khẩu
                txtMaBuuChinhDonVi.Tag = "IMY"; //Mã bưu chính
                txtDiaChiDonVi.Tag = "IMA"; //Địa chỉ người nhập khẩu
                txtSoDienThoaiDonVi.Tag = "IMT"; //Số điện thoại người nhập khẩu
                txtMaDoiTac.Tag = "EPC"; //Mã người xuất khẩu
                txtTenDoiTac.Tag = "EPN"; //Tên người xuất khẩu
                txtMaBuuChinhDoiTac.Tag = "EPY"; //Mã bưu chính
                txtDiaChiDoiTac1.Tag = "EPA"; //Địa chỉ 1(Street and number/P.O.BOX)
                txtDiaChiDoiTac2.Tag = "EP2"; //Địa chỉ 2(Street and number/P.O.BOX)
                txtDiaChiDoiTac3.Tag = "EP3"; //Địa chỉ 3(City name)
                txtDiaChiDoiTac4.Tag = "EP4"; //Địa chỉ 4(Country sub-entity, name)
                ctrMaNuocDoiTac.TagName = "EPO"; //Mã nước(Country, coded)
                txtSoHouseAWB.Tag = "HAB"; //Số House AWB
                txtSoMasterAWB.Tag = "MAB"; //Số Master AWB
                txtSoLuong.Tag = "NO"; //Số lượng
                txtTrongLuong.Tag = "GW"; //Tổng trọng lượng hàng (Gross)
                ctrMaDDLuuKho.TagCode = "ST"; //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
                txtTenMayBayChoHang.Tag = "VSN"; //Tên máy bay chở hàng
                clcNgayHangDen.Tag = "ARR"; //Ngày hàng đến
                ctrMaDiaDiemDoHang.TagCode = "DST"; //Mã cảng dỡ hàng
                ctrMaDiaDiemXepHang.TagCode = "PSC"; //Mã địa điểm xếp hàng
                ctrPhanLoaiGiaHD.TagName = "IP1"; //Mã phân loại giá hóa đơn
                txtMaDieuKienGiaHD.Tag = "IP2"; //Mã điều kiện giá hóa đơn
                ctrMaTTHoaDon.Tag = "IP3"; //Mã đồng tiền của hóa đơn
                txtTongTriGiaHD.Tag = "IP4"; //Tổng trị giá  hóa đơn
                ctrMaPhiVC.TagName = "FR1"; //Mã phân loại cước vận chuyển
                ctrMaTTPhiVC.TagName = "FR2"; //Mã tiền tệ cước vận chuyển
                txtSoTienPhiVC.Tag = "FR3"; //Phí vận chuyển
                ctrMaPhiBaoHiem.TagName = "IN1"; //Mã phân loại bảo hiểm
                ctrMaTTPhiBH.TagName = "IN2"; //Mã tiền tệ của tiền bảo hiểm
                txtSoTienPhiBH.Tag = "IN3"; //Phí bảo hiểm
                txtMoTaHangHoa.Tag = "CMN"; //Mô tả hàng hóa
                ctrMaNuoc.TagName = "OR"; //Mã nước xuất xứ
                txtTriGiaTinhThue.Tag = "DPR"; //Trị giá tính thuế
                txtGhiChu.Tag = "NT1"; //Phần ghi chú
                txtSoQuanLyNoiBoDN.Tag = "REF"; //Số quản lý của nội bộ doanh nghiệp

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }
    }
}
