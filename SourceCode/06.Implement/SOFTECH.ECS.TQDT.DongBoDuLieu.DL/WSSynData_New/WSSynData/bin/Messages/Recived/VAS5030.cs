﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAS5030 : BasicVNACC
    {
   
        public List<VAS5030_HANG> HangHoa{ get; set; }

        //public StringBuilder BuilEdiMessagesIVA(StringBuilder StrBuild)
        //{
        //    StringBuilder str = StrBuild;
        //    str = BuildEdiMessages<VAS5030>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5030");
        //    foreach (VAS5030_HANG item in HangHoa)
        //    {
        //        item.BuildEdiMessages<VAS5030_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5030_HANG");

        //    }
        //    return str;
        //}
        public void LoadVAS5030(string strResult)
        {
            try
            {
                this.GetObject<VAS5030>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAS5030, false, VAS5030.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAS5030.TongSoByte);
                while (true)
                {
                    VAS5030_HANG ivaHang = new VAS5030_HANG();
                    ivaHang.GetObject<VAS5030_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAS5030_HANG", false, VAS5030_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAS5030_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAS5030_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAS5030_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAS5030_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
