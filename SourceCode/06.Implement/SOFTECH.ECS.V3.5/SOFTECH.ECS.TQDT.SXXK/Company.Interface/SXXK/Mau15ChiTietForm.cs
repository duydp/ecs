﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.Interface.SXXK;
using System.IO;
using System.Diagnostics;

using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.Interface.Report.SXXK;
using System.Data.SqlClient;
using Logger;
using Company.KDT.SHARE.VNACCS;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.GC;
using System.Reflection;

namespace Company.Interface.SXXK
{
    public partial class Mau15ChiTietForm : BaseForm
    {
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP();
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
        DataTable dtTotal = new DataTable();
        public DataTable tb = new DataTable();
        private DataTable temp = new DataTable();
        public DataTable tbTong = new DataTable();
        private DataTable tbNPLChiTietXuat = new DataTable();
        private DataTable tbNPLTongXuat = new DataTable();
        private DataTable tbNPLCU_Details = new DataTable();
        private DataTable tbNPLCU_Total = new DataTable();
        public int namDK = 0;
        private static int totalRows = 0;
        private static int index = 0;
        public System.IO.FileInfo fin;
        public System.IO.FileStream fs;
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public long size;
        public byte[] data;
        public DataTable dtNPLTotal = new DataTable();
        public DataTable dtSPTotal = new DataTable();
        public bool IsActive = false;
        public bool IsReadExcel = false;
        public int NamQuyetToanOld;
        public int NamQuyetToan;
        public string NPL;
        public string NPLTemp;
        public string MaNPL;
        public string MaNPLTemp;
        public decimal LuongNhap;
        public decimal LuongTon;
        public decimal LuongXuat;
        public decimal LuongXuatTemp;
        public decimal LuongTonCuoiTemp;
        public decimal LuongXuatTotal;
        public decimal TriGiaXuatToTal;
        public decimal DonGiaNhap;
        public decimal TriGiaNhap;
        public decimal TriGiaTon;
        public decimal TriGiaXuat;
        public decimal TriGiaXuatTemp;
        public int k=0;

        public Mau15ChiTietForm()
        {
            InitializeComponent();

        }


        private DataTable GetDataSource()
        {

            string ngaychotton = dtpNgayChotTon.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string tuNgay = dtpTuNgay.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string denNgay = dtpDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
            try
            {
                #region sql
                string sql = @"SELECT  152 AS SoTaiKhoan ,
        t.[SoToKhaiVNACCS] ,
        t.NgayDangKy ,
        t.Ngay ,
        t.SoToKhai ,
        t.MaLoaiHinh ,
        t.MaNPL ,
        t.TenNPL ,
        t.TenDVT ,
        t.Luong ,
		0 AS LuongNPLCU,
        t.TriGiaKB ,
        CONVERT(DECIMAL(26, 6), ( t.TriGiaKB / t.Luong )) AS DonGiaKB ,
        ROUND(t.DonGiaTT, 6) AS DonGiaTT ,
        ROUND(t.TriGiaTT, 6) AS TriGiaTT ,
        CASE WHEN t.Ngay <= '" + ngaychotton + @"' THEN t.Luong
             ELSE 0
        END AS LuongNhapDauKy ,
		CASE WHEN t.Ngay <= '" + ngaychotton + @"' THEN t.Luong
             ELSE 0
        END AS LuongXuatDauKy ,
        CASE WHEN t.Ngay <= '" + ngaychotton + @"' THEN ( t.Ton )
             ELSE 0
        END AS TonDauKy ,
        CASE WHEN t.Ngay <= '" + ngaychotton + @"'
             THEN ( CASE WHEN t.Ton = 0 THEN ROUND(0, 6)
                         ELSE ROUND(( t.DonGiaTT * t.Ton ), 6)
                    END )
             ELSE 0
        END AS TriGiaTTTonDauKy ,
        CASE WHEN t.Ngay >= '" + ngaychotton + @"' THEN t.Luong
             ELSE 0
        END AS LuongNhapTrongKy ,
        CASE WHEN t.Ngay >= '" + ngaychotton + @"'
             THEN t.TriGiaTT
             ELSE 0
        END AS TriGiaTTNhapTrongKy ,
        CASE WHEN t.Ngay >= '" + ngaychotton + @"' THEN ( t.Luong - t.Ton )
             ELSE 0
        END AS LuongXuatTrongKy ,
        CASE WHEN t.Ngay >= '" + ngaychotton + @"'
             THEN ( CASE WHEN t.Ton = 0 THEN ROUND(t.TriGiaTT, 6)
                         ELSE ROUND(( t.DonGiaTT * ( t.Luong - t.Ton ) ), 6)
                    END )
             ELSE 0
        END AS TriGiaTTXuatTrongKy ,
        CASE WHEN t.Ngay >= '" + ngaychotton + @"' THEN t.Ton
             ELSE 0
        END AS TonCuoiKy ,
        CASE WHEN t.Ngay >= '" + ngaychotton + @"'
             THEN ( CASE WHEN t.Ton = 0 THEN ROUND(0, 6)
                         ELSE ROUND(( t.DonGiaTT * t.Ton ), 6)
                    END )
             ELSE 0
        END AS TriGiaTTTonCuoiKy ,
        CASE WHEN t.TyGia IS NULL
             THEN ( SELECT TOP 1
                            TyGiaTinhThue
                    FROM    dbo.t_SXXK_ToKhaiMauDich
                    WHERE   SoToKhai = t.SoToKhai
                            AND MaLoaiHinh = t.MaLoaiHinh
                            AND NamDangKy = YEAR(t.NgayDangKy)
                  )
             ELSE CONVERT(DECIMAL, t.TyGia)
        END AS TyGia ,
        CASE WHEN t.MaTyGia IS NULL THEN 'USD'
             ELSE ( CASE WHEN t.TyGia = 1 THEN 'VND'
                         ELSE t.MaTyGia
                    END )
        END AS MaTyGia
FROM    ( SELECT    CASE WHEN MaLoaiHinh LIKE '%V%'
                         THEN ( CONVERT(VARCHAR(18), [SoToKhaiVNACCS]) + '/'
                                + SUBSTRING(MaLoaiHinh, 3, 3) )
                         ELSE ( CONVERT(VARCHAR(18), [SoToKhaiVNACCS]) + '/'
                                + MaLoaiHinh )
                    END AS SoToKhaiVNACCS ,
                    [NgayDangKy] ,
                    CONVERT(DATETIME, [NgayDangKy], 103) AS Ngay ,
                    [SoToKhai] ,
                    [MaLoaiHinh] ,
                    [NamDangKy] ,
                    [MaNPL] ,
                    [TenNPL] ,
                    [TenDVT] ,
                    CONVERT(DECIMAL(19, 4), [Luong]) AS Luong ,
                    CONVERT(DECIMAL(26, 6), v_HangTon.TriGiaKB) AS TriGiaKB ,
                    CASE WHEN MaLoaiHinh LIKE '%V%'
                         THEN ROUND(( SELECT TOP 1
                                                CONVERT(DECIMAL(26, 6), h.DonGiaTT)
                                      FROM      t_SXXK_HangMauDich h
                                      WHERE     h.SoToKhai = v_HangTon.SoToKhai
                                                AND h.MaLoaiHinh LIKE '%V%'
                                                AND h.MaPhu = v_HangTon.MaNPL
                                    ), 6)
                         ELSE ROUND(( SELECT TOP 1
                                                CONVERT(DECIMAL(26, 6), h.DonGiaTT)
                                      FROM      t_SXXK_HangMauDich h
                                      WHERE     h.SoToKhai = v_HangTon.SoToKhai
                                                AND h.MaLoaiHinh LIKE 'N%'
                                                AND h.MaLoaiHinh NOT LIKE '%V%'
                                                AND h.NamDangKy = YEAR(v_HangTon.NgayDangKy)
                                                AND h.MaPhu = v_HangTon.MaNPL
                                    ), 6)
                    END AS DonGiaTT ,
                    CASE WHEN MaLoaiHinh LIKE '%V%'
                         THEN ROUND(( SELECT TOP 1
                                                CONVERT(DECIMAL(26, 6), TriGiaTT)
                                      FROM      t_SXXK_HangMauDich h
                                      WHERE     h.SoToKhai = v_HangTon.SoToKhai
                                                AND h.MaLoaiHinh LIKE '%V%'
                                                AND h.MaPhu = v_HangTon.MaNPL
                                    ), 6)
                         ELSE ROUND(( SELECT TOP 1
                                                CONVERT(DECIMAL(26, 6), TriGiaTT)
                                      FROM      t_SXXK_HangMauDich h
                                      WHERE     h.SoToKhai = v_HangTon.SoToKhai
                                                AND h.MaLoaiHinh LIKE 'N%'
                                                AND h.MaLoaiHinh NOT LIKE '%V%'
                                                AND h.NamDangKy = YEAR(v_HangTon.NgayDangKy)
                                                AND h.MaPhu = v_HangTon.MaNPL
                                    ), 6)
                    END AS TriGiaTT ,
                    CONVERT(DECIMAL(19, 4), [Ton]) AS Ton ,
                    CONVERT(DECIMAL(26, 6), [ThueXNK]) AS ThueXNK ,
                    CONVERT(DECIMAL(26, 6), [ThueXNKTon]) AS ThueXNKTon ,
                    CASE WHEN MaLoaiHinh LIKE '%V%'
                         THEN ( SELECT TOP 1
                                        TyGiaTinhThue
                                FROM    t_KDT_VNACC_TK_PhanHoi_TyGia
                                WHERE   Master_ID = ( SELECT TOP 1
                                                              ID
                                                      FROM    t_KDT_VNACC_ToKhaiMauDich t
                                                      WHERE   t.SoToKhai = v_HangTon.SoToKhaiVNACCS
                                                    )
                              )
                         ELSE ( SELECT TOP 1
                                        TyGiaTinhThue
                                FROM    dbo.t_KDT_ToKhaiMauDich
                                WHERE   SoToKhai = v_HangTon.SoToKhai
                                        AND NamDK = v_HangTon.NamDangKy
                                        AND MaLoaiHinh = v_HangTon.MaLoaiHinh
                              )
                    END AS TyGia ,
                    CASE WHEN MaLoaiHinh LIKE '%V%'
                         THEN ( SELECT TOP 1
                                        MaTTTyGiaTinhThue
                                FROM    t_KDT_VNACC_TK_PhanHoi_TyGia
                                WHERE   Master_ID = ( SELECT TOP 1
                                                              ID
                                                      FROM    t_KDT_VNACC_ToKhaiMauDich t
                                                      WHERE   t.SoToKhai = v_HangTon.SoToKhaiVNACCS
                                                    )
                              )
                         ELSE 'USD'
                    END AS MaTyGia
          FROM      ( SELECT    CASE WHEN t.MaLoaiHinh LIKE '%V%'
                                     THEN ( SELECT TOP 1
                                                    SoTKVNACCS
                                            FROM    t_VNACCS_CapSoToKhai
                                            WHERE   SoTK = t.SoToKhai
                                          )
                                     ELSE t.SoToKhai
                                END AS SoToKhaiVNACCS ,
                                t.MaDoanhNghiep ,
                                t.MaHaiQuan ,
                                t.SoToKhai ,
                                t.MaLoaiHinh ,
                                t.NamDangKy ,
                                t.MaNPL ,
                                CASE WHEN hmd.TenHang IS NULL
                                     THEN ( SELECT TOP 1
                                                    Ten
                                            FROM    dbo.t_SXXK_NguyenPhuLieu
                                            WHERE   Ma = t.MaNPL
                                          )
                                     ELSE hmd.TenHang
                                END AS TenNPL ,
                                CASE WHEN hmd.DVT_ID IS NULL
                                     THEN ( SELECT TOP 1
                                                    DVT_ID
                                            FROM    dbo.t_KDT_HangMauDich
                                            WHERE   TKMD_ID = ( SELECT TOP 1
                                                              ID
                                                              FROM
                                                              dbo.t_KDT_ToKhaiMauDich
                                                              WHERE
                                                              SoToKhai = t.SoToKhai
                                                              AND MaLoaiHinh = t.MaLoaiHinh
                                                              AND YEAR(NgayDangKy) = t.NamDangKy
                                                              )
                                                    AND MaPhu = t.MaNPL
                                                    AND SoLuong = t.Luong
                                          )
                                     ELSE hmd.DVT_ID
                                END AS DVT_ID ,
                                dvt.Ten AS TenDVT ,
                                CASE WHEN hmd.NuocXX_ID IS NULL
                                     THEN ( SELECT TOP 1
                                                    NuocXX_ID
                                            FROM    dbo.t_KDT_HangMauDich
                                            WHERE   TKMD_ID = ( SELECT TOP 1
                                                              ID
                                                              FROM
                                                              dbo.t_KDT_ToKhaiMauDich
                                                              WHERE
                                                              SoToKhai = t.SoToKhai
                                                              AND MaLoaiHinh = t.MaLoaiHinh
                                                              AND YEAR(NgayDangKy) = t.NamDangKy
                                                              )
                                                    AND MaPhu = t.MaNPL
                                                    AND SoLuong = t.Luong
                                          )
                                     ELSE hmd.NuocXX_ID
                                END AS NuocXX_ID ,
                                CASE WHEN hmd.TriGiaKB IS NULL
                                     THEN ( SELECT TOP 1
                                                    TriGiaKB
                                            FROM    dbo.t_KDT_HangMauDich
                                            WHERE   TKMD_ID = ( SELECT TOP 1
                                                              ID
                                                              FROM
                                                              dbo.t_KDT_ToKhaiMauDich
                                                              WHERE
                                                              SoToKhai = t.SoToKhai
                                                              AND MaLoaiHinh = t.MaLoaiHinh
                                                              AND YEAR(NgayDangKy) = t.NamDangKy
                                                              )
                                                    AND MaPhu = t.MaNPL
                                                    AND SoLuong = t.Luong
                                          )
                                     ELSE hmd.TriGiaKB
                                END AS TriGiaKB ,
                                t.Luong ,
                                t.Ton ,
                                t.ThueXNK ,
                                t.ThueXNKTon ,
                                t.ThueTTDB ,
                                t.ThueVAT ,
                                t.ThueCLGia ,
                                t.PhuThu ,
                                ( SELECT TOP 1
                                            NgayDangKy
                                  FROM      dbo.t_SXXK_ToKhaiMauDich
                                  WHERE     ( SoToKhai = t.SoToKhai )
                                            AND ( MaLoaiHinh = t.MaLoaiHinh )
                                            AND ( NamDangKy = t.NamDangKy )
                                            AND ( MaHaiQuan = t.MaHaiQuan )
                                            AND ( MaDoanhNghiep = t.MaDoanhNghiep )
                                ) AS NgayDangKy
                      FROM      dbo.t_SXXK_ThanhLy_NPLNhapTon AS t
                                LEFT OUTER JOIN dbo.t_SXXK_HangMauDich AS hmd ON t.MaHaiQuan = hmd.MaHaiQuan
                                                              AND t.MaNPL = hmd.MaPhu
                                                              AND t.SoToKhai = hmd.SoToKhai
                                                              AND t.MaLoaiHinh = hmd.MaLoaiHinh
                                                              AND t.NamDangKy = hmd.NamDangKy
                                                              AND t.Luong = hmd.SoLuong
                                                              AND ROUND(t.ThueXNK,
                                                              0) = ROUND(hmd.ThueXNK,
                                                              0)
                                LEFT OUTER JOIN dbo.t_HaiQuan_DonViTinh AS dvt ON hmd.DVT_ID = dvt.ID
                      WHERE     ( ( CAST(t.SoToKhai AS VARCHAR(10))
                                    + t.MaLoaiHinh
                                    + CAST(t.NamDangKy AS VARCHAR(4))
                                    + t.MaHaiQuan ) IN (
                                  SELECT    CAST(SoToKhai AS VARCHAR(10))
                                            + MaLoaiHinh
                                            + CAST(NamDangKy AS VARCHAR(4))
                                            + MaHaiQuan AS Expr1
                                  FROM      dbo.t_SXXK_ToKhaiMauDich AS t_SXXK_ToKhaiMauDich_1
                                  WHERE     ( MaLoaiHinh LIKE 'N%' ) ) )
                    ) AS v_HangTon
          WHERE     v_HangTon.NgayDangKy >= '" + tuNgay + @"'
                    AND v_HangTon.NgayDangKy <= '" + denNgay + @"'
                    AND YEAR(v_HangTon.NgayDangKy) >= YEAR('" + tuNgay + @"')
        ) AS t
UNION
SELECT  155 AS SoTaiKhoan ,
        CASE WHEN t.MaLoaiHinh LIKE 'XV%'
             THEN CONVERT(VARCHAR(18), ( SELECT SoTKVNACCS
                                         FROM   t_VNACCS_CapSoToKhai
                                         WHERE  SoTK = t.SoToKhai
                                       )) + '/' + SUBSTRING(t.MaLoaiHinh, 3, 3)
             ELSE CONVERT(VARCHAR(18), t.SoToKhai) + '/' + t.MaLoaiHinh
        END AS SoToKhaiVNACCS ,
        t.NgayDangKy ,
        CONVERT(DATETIME, t.NgayDangKy, 103) AS Ngay ,
        t.SoToKhai ,
        t.MaLoaiHinh ,
        h.MaPhu AS MaNPL ,
        h.TenHang AS TenNPL ,
        CASE WHEN h.DVT_ID <> '' THEN ( SELECT  Ten
                                        FROM    t_HaiQuan_DonViTinh
                                        WHERE   ID = h.DVT_ID
                                      )
             ELSE ''
        END AS TenDVT ,
        h.SoLuong AS Luong ,
		0 AS LuongNPLCU,
        CONVERT(DECIMAL(26, 6), h.TriGiaKB) AS TriGiaKB ,
        CONVERT(DECIMAL(26, 6), h.DonGiaKB) AS DonGiaKB ,
        CONVERT(DECIMAL(26, 6), h.DonGiaTT) AS DonGiaTT ,
        CONVERT(DECIMAL(26, 6), h.TriGiaTT) AS TriGiaTT ,
        0 AS LuongNhapDauKy ,
		0 AS LuongXuatDauKy,
        0 AS TonDauKy ,
        0 AS TriGiaTTTonDauKy ,
        0 AS LuongNhapTrongKy ,
        0 AS TriGiaTTNhapTrongKy ,
        CASE WHEN t.NgayDangKy > '" + ngaychotton + @"'
             THEN CONVERT(DECIMAL(26, 6), ( h.SoLuong ))
             ELSE 0
        END AS LuongXuatTrongKy ,
        CASE WHEN t.NgayDangKy > '" + ngaychotton + @"'
             THEN CONVERT(DECIMAL(26, 6), h.TriGiaTT)
             ELSE 0
        END AS TriGiaTTXuatTrongKy ,
        0 AS TonCuoiKy ,
        0 AS TriGiaTTTonCuoiKy ,
        CONVERT(DECIMAL, t.TyGiaTinhThue) AS TyGia ,
        t.NguyenTe_ID AS MaTyGia
FROM    t_SXXK_ToKhaiMauDich t
        JOIN t_SXXK_HangMauDich h ON t.SoToKhai = h.SoToKhai
                                     AND h.MaLoaiHinh = t.MaLoaiHinh
WHERE   t.MaLoaiHinh LIKE 'X%'
        AND t.NgayDangKy >= '" + tuNgay + @"'
        AND t.NgayDangKy <= '" + denNgay + @"'

		UNION
SELECT  152 AS SoTaiKhoan ,
        CONVERT(VARCHAR(18),cudk.ID)+'/PLCU'  AS SoToKhaiVNACCS ,
        cudk.NgayTiepNhan AS NgayDangKy ,
        cudk.NgayTiepNhan AS Ngay ,
        cudk.ID AS SoToKhai ,
        'PLCU' AS MaLoaiHinh ,
        cud.MaNPL ,
        cud.TenNPL ,
        CASE WHEN cud.DVT_ID <> ''
             THEN ( SELECT  dvt.Ten
                    FROM    dbo.t_HaiQuan_DonViTinh dvt
                    WHERE   dvt.ID = cud.DVT_ID
                  )
             ELSE cud.DVT_ID
        END AS TenDVT ,
        0 AS Luong ,
        cud.LuongNPL AS LuongNPLCU ,
        1 AS TriGiaKB ,
        1 AS DonGiaKB ,
        1 AS DonGiaTT ,
        1 AS TriGiaTT ,
		CASE WHEN cudk.NgayTiepNhan <= CONVERT(DATETIME,'" + ngaychotton + @"',103) THEN cud.LuongNPL
             ELSE 0
        END AS LuongNhapDauKy ,
        CASE WHEN cudk.NgayTiepNhan <= CONVERT(DATETIME,'" + ngaychotton + @"',103) THEN cud.LuongNPL
             ELSE 0
        END AS LuongXuatDauKy ,
        0 AS TonDauKy ,
        0 AS TriGiaTTTonDauKy ,
		CASE WHEN cudk.NgayTiepNhan >= CONVERT(DATETIME,'" + ngaychotton + @"',103) THEN cud.LuongNPL
             ELSE 0
        END AS  LuongNhapTrongKy ,
        0 AS TriGiaTTNhapTrongKy ,
		CASE WHEN cudk.NgayTiepNhan >= CONVERT(DATETIME,'" + ngaychotton + @"',103) THEN cud.LuongNPL
             ELSE 0
        END AS LuongXuatTrongKy ,
        0 AS TriGiaTTXuatTrongKy ,
        0 AS TonCuoiKy ,
        0 AS TriGiaTTTonCuoiKy ,
        1 AS TyGia ,
        'VND' AS MaTyGia
FROM    dbo.t_KDT_SXXK_BKNPLCungUngDangKy cudk
        JOIN dbo.t_KDT_SXXK_BKNPLCungUng cu ON cu.Master_id = cudk.ID
        JOIN dbo.t_KDT_SXXK_BKNPLCungUng_Detail cud ON cud.Master_id = cu.ID
        JOIN dbo.t_KDT_SXXK_HoSoThanhLyDangKy hs ON hs.LanThanhLy = cu.LanThanhLy
		WHERE cudk.NgayTiepNhan>='" + tuNgay + @"' AND cudk.NgayTiepNhan <='" + denNgay + @"'
		
ORDER BY SoTaiKhoan ASC ,
        MaNPL ,
        NgayDangKy;  ";
                #endregion

                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                //db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, Int32.Parse(txtNamTiepNhan.Text));
                dbCommand.CommandTimeout = 60000;
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private DataTable GetTongData()
        {
            string ngaychotton = dtpNgayChotTon.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string tuNgay = dtpTuNgay.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string denNgay = dtpDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
            try
            {
                #region sql
                string sql = @"SELECT  TB.SoTaiKhoan ,
        TB.MaNPL ,
        CASE WHEN TB.SoTaiKhoan = '152'
                  AND TB.MaNPL <> ''
             THEN ( SELECT TOP 1 npl.Ten
                    FROM    dbo.t_SXXK_NguyenPhuLieu npl
                    WHERE   npl.Ma = TB.MaNPL
                  )
             ELSE ( SELECT TOP 1  sp.Ten
                    FROM    dbo.t_SXXK_SanPham sp
                    WHERE   sp.Ma = TB.MaNPL
                  )
        END AS TenNPL ,
        CASE WHEN TB.SoTaiKhoan = '152'
                  AND TB.MaNPL <> ''
             THEN ( SELECT TOP 1 dvt.Ten
                    FROM    dbo.t_HaiQuan_DonViTinh dvt
                    WHERE   dvt.ID = ( SELECT TOP 1  npl.DVT_ID
                                       FROM     dbo.t_SXXK_NguyenPhuLieu npl
                                       WHERE    npl.Ma = TB.MaNPL
                                     )
                  )
             ELSE ( SELECT TOP 1 dvt.Ten
                    FROM    dbo.t_HaiQuan_DonViTinh dvt
                    WHERE   dvt.ID = ( SELECT TOP 1  sp.DVT_ID
                                       FROM     dbo.t_SXXK_SanPham sp
                                       WHERE    sp.Ma = TB.MaNPL
                                     )
                  )
        END AS TenDVT ,
        CONVERT(NUMERIC(18,4),SUM(TB.LuongNhapDauKy)) AS LuongNhapDauKy ,
        CONVERT(NUMERIC(18,4),SUM(TB.TonDauKy)) AS TonDauKy ,
        CONVERT(NUMERIC(18,4),SUM(TB.TriGiaTTTonDauKy)) AS TriGiaTTTonDauKy ,
        CONVERT(NUMERIC(18,4),SUM(TB.LuongNhapTrongKy)) AS LuongNhapTrongKy ,
        CONVERT(NUMERIC(18,4),SUM(TB.TriGiaTTNhapTrongKy)) AS TriGiaTTNhapTrongKy ,
        CONVERT(NUMERIC(18,4),SUM(TB.LuongXuatTrongKy)) AS LuongXuatTrongKy ,
        CONVERT(NUMERIC(18,4),SUM(TB.TriGiaTTXuatTrongKy)) AS TriGiaTTXuatTrongKy ,
        --CASE WHEN TB.SoTaiKhoan = '152'
        --     THEN ( SUM(TB.TonDauKy) + SUM(TB.LuongNhapTrongKy) )
        --          - SUM(TB.LuongXuatTrongKy)
        --     ELSE SUM(TB.TonCuoiKy)
        --END AS TonCuoiKy ,
        CONVERT(NUMERIC(18,4),SUM(TB.TonDauKy) + SUM(TB.LuongNhapTrongKy) - SUM(TB.LuongXuatTrongKy)) AS TonCuoiKy ,
        --CASE WHEN TB.SoTaiKhoan = '152'
        --     THEN ( SUM(TB.TriGiaTTTonDauKy) + SUM(TB.TriGiaTTNhapTrongKy) )
        --          - SUM(TB.TriGiaTTXuatTrongKy)
        --     ELSE SUM(TB.TriGiaTTTonCuoiKy)
        --END AS TriGiaTTTonCuoiKy
        CONVERT(NUMERIC(18,4),SUM(TB.TriGiaTTTonDauKy) + SUM(TB.TriGiaTTNhapTrongKy) - SUM(TB.TriGiaTTXuatTrongKy)) AS TriGiaTTTonCuoiKy
FROM    ( SELECT    152 AS SoTaiKhoan ,
                    t.[SoToKhaiVNACCS] ,
                    t.NgayDangKy ,
                    t.Ngay ,
                    t.SoToKhai ,
                    t.MaLoaiHinh ,
                    t.MaNPL ,
                    t.TenNPL ,
                    t.TenDVT ,
                    t.Luong ,
                    0 AS LuongNPLCU ,
                    t.TriGiaKB ,
                    CONVERT(DECIMAL(26, 6), ( t.TriGiaKB / t.Luong )) AS DonGiaKB ,
                    ROUND(t.DonGiaTT, 6) AS DonGiaTT ,
                    ROUND(t.TriGiaTT, 6) AS TriGiaTT ,
                    CASE WHEN t.Ngay <= '" + ngaychotton+@"' THEN t.Luong
                         ELSE 0
                    END AS LuongNhapDauKy ,
                    CASE WHEN t.Ngay <= '"+ngaychotton+@"' THEN t.Luong
                         ELSE 0
                    END AS LuongXuatDauKy ,
                    CASE WHEN t.Ngay <= '"+ngaychotton+@"' THEN ( t.Ton )
                         ELSE 0
                    END AS TonDauKy ,
                    CASE WHEN t.Ngay <= '"+ngaychotton+@"'
                         THEN ( CASE WHEN t.Ton = 0 THEN ROUND(0, 6)
                                     ELSE ROUND(( t.DonGiaTT * t.Ton ), 6)
                                END )
                         ELSE 0
                    END AS TriGiaTTTonDauKy ,
                    CASE WHEN t.Ngay >= '"+ngaychotton+@"' THEN t.Luong
                         ELSE 0
                    END AS LuongNhapTrongKy ,
                    CASE WHEN t.Ngay >= '"+ngaychotton+@"' THEN t.TriGiaTT
                         ELSE 0
                    END AS TriGiaTTNhapTrongKy ,
                    CASE WHEN t.Ngay >= '"+ngaychotton+@"' THEN ( t.Luong - t.Ton )
                         ELSE 0
                    END AS LuongXuatTrongKy ,
                    CASE WHEN t.Ngay >= '"+ngaychotton+@"'
                         THEN ( CASE WHEN t.Ton = 0 THEN ROUND(t.TriGiaTT, 6)
                                     ELSE ROUND(( t.DonGiaTT * ( t.Luong
                                                              - t.Ton ) ), 6)
                                END )
                         ELSE 0
                    END AS TriGiaTTXuatTrongKy ,
                    CASE WHEN t.Ngay >= '"+ngaychotton+@"' THEN t.Ton
                         ELSE 0
                    END AS TonCuoiKy ,
                    CASE WHEN t.Ngay >= '"+ngaychotton+ @"'
                         THEN ( CASE WHEN t.Ton = 0 THEN ROUND(0, 6)
                                     ELSE ROUND(( t.DonGiaTT * t.Ton ), 6)
                                END )
                         ELSE 0
                    END AS TriGiaTTTonCuoiKy ,
                    CASE WHEN t.TyGia IS NULL
                         THEN ( SELECT TOP 1
                                        TyGiaTinhThue
                                FROM    dbo.t_SXXK_ToKhaiMauDich
                                WHERE   SoToKhai = t.SoToKhai
                                        AND MaLoaiHinh = t.MaLoaiHinh
                                        AND NamDangKy = YEAR(t.NgayDangKy)
                              )
                         ELSE CONVERT(DECIMAL, t.TyGia)
                    END AS TyGia ,
                    CASE WHEN t.MaTyGia IS NULL THEN 'USD'
                         ELSE ( CASE WHEN t.TyGia = 1 THEN 'VND'
                                     ELSE t.MaTyGia
                                END )
                    END AS MaTyGia
          FROM      ( SELECT    CASE WHEN MaLoaiHinh LIKE '%V%'
                                     THEN ( CONVERT(VARCHAR(18), [SoToKhaiVNACCS])
                                            + '/' + SUBSTRING(MaLoaiHinh, 3, 3) )
                                     ELSE ( CONVERT(VARCHAR(18), [SoToKhaiVNACCS])
                                            + '/' + MaLoaiHinh )
                                END AS SoToKhaiVNACCS ,
                                [NgayDangKy] ,
                                CONVERT(DATETIME, [NgayDangKy], 103) AS Ngay ,
                                [SoToKhai] ,
                                [MaLoaiHinh] ,
                                [NamDangKy] ,
                                [MaNPL] ,
                                [TenNPL] ,
                                [TenDVT] ,
                                CONVERT(DECIMAL(19, 4), [Luong]) AS Luong ,
                                CONVERT(DECIMAL(26, 6), v_HangTon.TriGiaKB) AS TriGiaKB ,
                                CASE WHEN MaLoaiHinh LIKE '%V%'
                                     THEN ROUND(( SELECT TOP 1
                                                            CONVERT(DECIMAL(26,
                                                              6), h.DonGiaTT)
                                                  FROM      t_SXXK_HangMauDich h
                                                  WHERE     h.SoToKhai = v_HangTon.SoToKhai
                                                            AND h.MaLoaiHinh LIKE '%V%'
                                                            AND h.MaPhu = v_HangTon.MaNPL
                                                ), 6)
                                     ELSE ROUND(( SELECT TOP 1
                                                            CONVERT(DECIMAL(26,
                                                              6), h.DonGiaTT)
                                                  FROM      t_SXXK_HangMauDich h
                                                  WHERE     h.SoToKhai = v_HangTon.SoToKhai
                                                            AND h.MaLoaiHinh LIKE 'N%'
                                                            AND h.MaLoaiHinh NOT LIKE '%V%'
                                                            AND h.NamDangKy = YEAR(v_HangTon.NgayDangKy)
                                                            AND h.MaPhu = v_HangTon.MaNPL
                                                ), 6)
                                END AS DonGiaTT ,
                                CASE WHEN MaLoaiHinh LIKE '%V%'
                                     THEN ROUND(( SELECT TOP 1
                                                            CONVERT(DECIMAL(26,
                                                              6), TriGiaTT)
                                                  FROM      t_SXXK_HangMauDich h
                                                  WHERE     h.SoToKhai = v_HangTon.SoToKhai
                                                            AND h.MaLoaiHinh LIKE '%V%'
                                                            AND h.MaPhu = v_HangTon.MaNPL
                                                ), 6)
                                     ELSE ROUND(( SELECT TOP 1
                                                            CONVERT(DECIMAL(26,
                                                              6), TriGiaTT)
                                                  FROM      t_SXXK_HangMauDich h
                                                  WHERE     h.SoToKhai = v_HangTon.SoToKhai
                                                            AND h.MaLoaiHinh LIKE 'N%'
                                                            AND h.MaLoaiHinh NOT LIKE '%V%'
                                                            AND h.NamDangKy = YEAR(v_HangTon.NgayDangKy)
                                                            AND h.MaPhu = v_HangTon.MaNPL
                                                ), 6)
                                END AS TriGiaTT ,
                                CONVERT(DECIMAL(19, 4), [Ton]) AS Ton ,
                                CONVERT(DECIMAL(26, 6), [ThueXNK]) AS ThueXNK ,
                                CONVERT(DECIMAL(26, 6), [ThueXNKTon]) AS ThueXNKTon ,
                                CASE WHEN MaLoaiHinh LIKE '%V%'
                                     THEN ( SELECT TOP 1
                                                    TyGiaTinhThue
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia
                                            WHERE   Master_ID = ( SELECT TOP 1
                                                              ID
                                                              FROM
                                                              t_KDT_VNACC_ToKhaiMauDich t
                                                              WHERE
                                                              t.SoToKhai = v_HangTon.SoToKhaiVNACCS
                                                              )
                                          )
                                     ELSE ( SELECT TOP 1
                                                    TyGiaTinhThue
                                            FROM    dbo.t_KDT_ToKhaiMauDich
                                            WHERE   SoToKhai = v_HangTon.SoToKhai
                                                    AND NamDK = v_HangTon.NamDangKy
                                                    AND MaLoaiHinh = v_HangTon.MaLoaiHinh
                                          )
                                END AS TyGia ,
                                CASE WHEN MaLoaiHinh LIKE '%V%'
                                     THEN ( SELECT TOP 1
                                                    MaTTTyGiaTinhThue
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia
                                            WHERE   Master_ID = ( SELECT TOP 1
                                                              ID
                                                              FROM
                                                              t_KDT_VNACC_ToKhaiMauDich t
                                                              WHERE
                                                              t.SoToKhai = v_HangTon.SoToKhaiVNACCS
                                                              )
                                          )
                                     ELSE 'USD'
                                END AS MaTyGia
                      FROM      ( SELECT    CASE WHEN t.MaLoaiHinh LIKE '%V%'
                                                 THEN ( SELECT TOP 1
                                                              SoTKVNACCS
                                                        FROM  t_VNACCS_CapSoToKhai
                                                        WHERE SoTK = t.SoToKhai
                                                      )
                                                 ELSE t.SoToKhai
                                            END AS SoToKhaiVNACCS ,
                                            t.MaDoanhNghiep ,
                                            t.MaHaiQuan ,
                                            t.SoToKhai ,
                                            t.MaLoaiHinh ,
                                            t.NamDangKy ,
                                            t.MaNPL ,
                                            CASE WHEN hmd.TenHang IS NULL
                                                 THEN ( SELECT TOP 1
                                                              Ten
                                                        FROM  dbo.t_SXXK_NguyenPhuLieu
                                                        WHERE Ma = t.MaNPL
                                                      )
                                                 ELSE hmd.TenHang
                                            END AS TenNPL ,
                                            CASE WHEN hmd.DVT_ID IS NULL
                                                 THEN ( SELECT TOP 1
                                                              DVT_ID
                                                        FROM  dbo.t_KDT_HangMauDich
                                                        WHERE TKMD_ID = ( SELECT TOP 1
                                                              ID
                                                              FROM
                                                              dbo.t_KDT_ToKhaiMauDich
                                                              WHERE
                                                              SoToKhai = t.SoToKhai
                                                              AND MaLoaiHinh = t.MaLoaiHinh
                                                              AND YEAR(NgayDangKy) = t.NamDangKy
                                                              )
                                                              AND MaPhu = t.MaNPL
                                                              AND SoLuong = t.Luong
                                                      )
                                                 ELSE hmd.DVT_ID
                                            END AS DVT_ID ,
                                            dvt.Ten AS TenDVT ,
                                            CASE WHEN hmd.NuocXX_ID IS NULL
                                                 THEN ( SELECT TOP 1
                                                              NuocXX_ID
                                                        FROM  dbo.t_KDT_HangMauDich
                                                        WHERE TKMD_ID = ( SELECT TOP 1
                                                              ID
                                                              FROM
                                                              dbo.t_KDT_ToKhaiMauDich
                                                              WHERE
                                                              SoToKhai = t.SoToKhai
                                                              AND MaLoaiHinh = t.MaLoaiHinh
                                                              AND YEAR(NgayDangKy) = t.NamDangKy
                                                              )
                                                              AND MaPhu = t.MaNPL
                                                              AND SoLuong = t.Luong
                                                      )
                                                 ELSE hmd.NuocXX_ID
                                            END AS NuocXX_ID ,
                                            CASE WHEN hmd.TriGiaKB IS NULL
                                                 THEN ( SELECT TOP 1
                                                              TriGiaKB
                                                        FROM  dbo.t_KDT_HangMauDich
                                                        WHERE TKMD_ID = ( SELECT TOP 1
                                                              ID
                                                              FROM
                                                              dbo.t_KDT_ToKhaiMauDich
                                                              WHERE
                                                              SoToKhai = t.SoToKhai
                                                              AND MaLoaiHinh = t.MaLoaiHinh
                                                              AND YEAR(NgayDangKy) = t.NamDangKy
                                                              )
                                                              AND MaPhu = t.MaNPL
                                                              AND SoLuong = t.Luong
                                                      )
                                                 ELSE hmd.TriGiaKB
                                            END AS TriGiaKB ,
                                            t.Luong ,
                                            t.Ton ,
                                            t.ThueXNK ,
                                            t.ThueXNKTon ,
                                            t.ThueTTDB ,
                                            t.ThueVAT ,
                                            t.ThueCLGia ,
                                            t.PhuThu ,
                                            ( SELECT TOP 1
                                                        NgayDangKy
                                              FROM      dbo.t_SXXK_ToKhaiMauDich
                                              WHERE     ( SoToKhai = t.SoToKhai )
                                                        AND ( MaLoaiHinh = t.MaLoaiHinh )
                                                        AND ( NamDangKy = t.NamDangKy )
                                                        AND ( MaHaiQuan = t.MaHaiQuan )
                                                        AND ( MaDoanhNghiep = t.MaDoanhNghiep )
                                            ) AS NgayDangKy
                                  FROM      dbo.t_SXXK_ThanhLy_NPLNhapTon AS t
                                            LEFT OUTER JOIN dbo.t_SXXK_HangMauDich
                                            AS hmd ON t.MaHaiQuan = hmd.MaHaiQuan
                                                      AND t.MaNPL = hmd.MaPhu
                                                      AND t.SoToKhai = hmd.SoToKhai
                                                      AND t.MaLoaiHinh = hmd.MaLoaiHinh
                                                      AND t.NamDangKy = hmd.NamDangKy
                                                      AND t.Luong = hmd.SoLuong
                                                      AND ROUND(t.ThueXNK, 0) = ROUND(hmd.ThueXNK,
                                                              0)
                                            LEFT OUTER JOIN dbo.t_HaiQuan_DonViTinh
                                            AS dvt ON hmd.DVT_ID = dvt.ID
                                  WHERE     ( ( CAST(t.SoToKhai AS VARCHAR(10))
                                                + t.MaLoaiHinh
                                                + CAST(t.NamDangKy AS VARCHAR(4))
                                                + t.MaHaiQuan ) IN (
                                              SELECT    CAST(SoToKhai AS VARCHAR(10))
                                                        + MaLoaiHinh
                                                        + CAST(NamDangKy AS VARCHAR(4))
                                                        + MaHaiQuan AS Expr1
                                              FROM      dbo.t_SXXK_ToKhaiMauDich
                                                        AS t_SXXK_ToKhaiMauDich_1
                                              WHERE     ( MaLoaiHinh LIKE 'N%' ) ) )
                                ) AS v_HangTon
                      WHERE     v_HangTon.NgayDangKy >= '" + tuNgay+@"'
                                AND v_HangTon.NgayDangKy <= '"+denNgay+@"'
                                AND YEAR(v_HangTon.NgayDangKy) >= YEAR('"+tuNgay+ @"')
                    ) AS t
          UNION
          SELECT    155 AS SoTaiKhoan ,
                    CASE WHEN t.MaLoaiHinh LIKE 'XV%'
                         THEN CONVERT(VARCHAR(18), ( SELECT SoTKVNACCS
                                                     FROM   t_VNACCS_CapSoToKhai
                                                     WHERE  SoTK = t.SoToKhai
                                                   )) + '/'
                              + SUBSTRING(t.MaLoaiHinh, 3, 3)
                         ELSE CONVERT(VARCHAR(18), t.SoToKhai) + '/'
                              + t.MaLoaiHinh
                    END AS SoToKhaiVNACCS ,
                    t.NgayDangKy ,
                    CONVERT(DATETIME, t.NgayDangKy, 103) AS Ngay ,
                    t.SoToKhai ,
                    t.MaLoaiHinh ,
                    h.MaPhu AS MaNPL ,
                    h.TenHang AS TenNPL ,
                    CASE WHEN h.DVT_ID <> ''
                         THEN ( SELECT  Ten
                                FROM    t_HaiQuan_DonViTinh
                                WHERE   ID = h.DVT_ID
                              )
                         ELSE ''
                    END AS TenDVT ,
                    h.SoLuong AS Luong ,
                    0 AS LuongNPLCU ,
                    CONVERT(DECIMAL(26, 6), h.TriGiaKB) AS TriGiaKB ,
                    CONVERT(DECIMAL(26, 6), h.DonGiaKB) AS DonGiaKB ,
                    CONVERT(DECIMAL(26, 6), h.DonGiaTT) AS DonGiaTT ,
                    CONVERT(DECIMAL(26, 6), h.TriGiaTT) AS TriGiaTT ,
                    0 AS LuongNhapDauKy ,
                    0 AS LuongXuatDauKy ,
                    0 AS TonDauKy ,
                    0 AS TriGiaTTTonDauKy ,
                    CASE WHEN t.NgayDangKy > '" + ngaychotton + @"'
                         THEN CONVERT(DECIMAL(26, 6), ( h.SoLuong ))
                         ELSE 0
                    END AS LuongNhapTrongKy ,
                    CASE WHEN t.NgayDangKy > '" + ngaychotton + @"'
                         THEN CONVERT(DECIMAL(26, 6), h.TriGiaTT)
                         ELSE 0
                    END AS TriGiaTTNhapTrongKy ,
                    CASE WHEN t.NgayDangKy > '" + ngaychotton+@"'
                         THEN CONVERT(DECIMAL(26, 6), ( h.SoLuong ))
                         ELSE 0
                    END AS LuongXuatTrongKy ,
                    CASE WHEN t.NgayDangKy > '"+ngaychotton+@"'
                         THEN CONVERT(DECIMAL(26, 6), h.TriGiaTT)
                         ELSE 0
                    END AS TriGiaTTXuatTrongKy ,
                    0 AS TonCuoiKy ,
                    0 AS TriGiaTTTonCuoiKy ,
                    CONVERT(DECIMAL, t.TyGiaTinhThue) AS TyGia ,
                    t.NguyenTe_ID AS MaTyGia
          FROM      t_SXXK_ToKhaiMauDich t
                    JOIN t_SXXK_HangMauDich h ON t.SoToKhai = h.SoToKhai
                                                 AND h.MaLoaiHinh = t.MaLoaiHinh
          WHERE     t.MaLoaiHinh LIKE 'X%'
                    AND t.NgayDangKy >= '"+tuNgay+@"'
                    AND t.NgayDangKy <= '"+denNgay+@"'
          UNION
          SELECT    152 AS SoTaiKhoan ,
                    CONVERT(VARCHAR(18), cudk.ID) + '/PLCU' AS SoToKhaiVNACCS ,
                    cudk.NgayTiepNhan AS NgayDangKy ,
                    cudk.NgayTiepNhan AS Ngay ,
                    cudk.ID AS SoToKhai ,
                    'PLCU' AS MaLoaiHinh ,
                    cud.MaNPL ,
                    cud.TenNPL ,
                    CASE WHEN cud.DVT_ID <> ''
                         THEN ( SELECT  dvt.Ten
                                FROM    dbo.t_HaiQuan_DonViTinh dvt
                                WHERE   dvt.ID = cud.DVT_ID
                              )
                         ELSE cud.DVT_ID
                    END AS TenDVT ,
                    0 AS Luong ,
                    cud.LuongNPL AS LuongNPLCU ,
                    1 AS TriGiaKB ,
                    1 AS DonGiaKB ,
                    1 AS DonGiaTT ,
                    1 AS TriGiaTT ,
                    CASE WHEN cudk.NgayTiepNhan <= CONVERT(DATETIME, '"+ngaychotton+@"', 103)
                         THEN cud.LuongNPL
                         ELSE 0
                    END AS LuongNhapDauKy ,
                    CASE WHEN cudk.NgayTiepNhan <= CONVERT(DATETIME, '"+ngaychotton+@"', 103)
                         THEN cud.LuongNPL
                         ELSE 0
                    END AS LuongXuatDauKy ,
                    0 AS TonDauKy ,
                    0 AS TriGiaTTTonDauKy ,
                    CASE WHEN cudk.NgayTiepNhan >= CONVERT(DATETIME, '"+ngaychotton+@"', 103)
                         THEN cud.LuongNPL
                         ELSE 0
                    END AS LuongNhapTrongKy ,
                    0 AS TriGiaTTNhapTrongKy ,
                    CASE WHEN cudk.NgayTiepNhan >= CONVERT(DATETIME, '"+ngaychotton+@"', 103)
                         THEN cud.LuongNPL
                         ELSE 0
                    END AS LuongXuatTrongKy ,
                    0 AS TriGiaTTXuatTrongKy ,
                    0 AS TonCuoiKy ,
                    0 AS TriGiaTTTonCuoiKy ,
                    1 AS TyGia ,
                    'VND' AS MaTyGia
          FROM      dbo.t_KDT_SXXK_BKNPLCungUngDangKy cudk
                    JOIN dbo.t_KDT_SXXK_BKNPLCungUng cu ON cu.Master_id = cudk.ID
                    JOIN dbo.t_KDT_SXXK_BKNPLCungUng_Detail cud ON cud.Master_id = cu.ID
                    JOIN dbo.t_KDT_SXXK_HoSoThanhLyDangKy hs ON hs.LanThanhLy = cu.LanThanhLy
          WHERE     cudk.NgayTiepNhan >= '"+tuNgay+@"'
                    AND cudk.NgayTiepNhan <= '"+denNgay+@"'
        ) TB
GROUP BY TB.SoTaiKhoan ,
        TB.MaNPL
HAVING  SUM(TB.LuongNhapDauKy) <> 0
        OR SUM(TB.LuongNhapTrongKy) <> 0
        OR SUM(TB.LuongXuatTrongKy) <> 0
        OR SUM(TB.TonDauKy) <> 0
        OR SUM(TB.TonCuoiKy) <> 0
ORDER BY TB.SoTaiKhoan ,
        TB.MaNPL; 
        ";
                #endregion
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                //db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, Int32.Parse(txtNamTiepNhan.Text));
                dbCommand.CommandTimeout = 60000;
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private DataTable GetGroupedBy(DataTable dt, string columnNamesInDt, string groupByColumnNames, string typeOfCalculation)
        {
            //Return its own if the column names are empty
            if (columnNamesInDt == string.Empty || groupByColumnNames == string.Empty)
            {
                return dt;
            }

            //Once the columns are added find the distinct rows and group it bu the numbet
            DataTable _dt = dt.DefaultView.ToTable(true, groupByColumnNames);

            //The column names in data table
            string[] _columnNamesInDt = columnNamesInDt.Split(',');

            for (int i = 0; i < _columnNamesInDt.Length; i = i + 1)
            {
                if (_columnNamesInDt[i] != groupByColumnNames)
                {
                    _dt.Columns.Add(_columnNamesInDt[i]);
                }
            }

            //Gets the collection and send it back
            for (int i = 0; i < _dt.Rows.Count; i = i + 1)
            {
                for (int j = 0; j < _columnNamesInDt.Length; j = j + 1)
                {
                    if (_columnNamesInDt[j] != groupByColumnNames)
                    {
                        _dt.Rows[i][j] = dt.Compute(typeOfCalculation + "(" + _columnNamesInDt[j] + ")", groupByColumnNames + " = '" + _dt.Rows[i][groupByColumnNames].ToString() + "'");
                    }
                }
            }

            return _dt;
        }
        public void ProcessDataNPL()
        {
            int Percen;
            int TotalPercen;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    T_SXXK_NPL_QUYETTOAN NPLQuyetToan = new T_SXXK_NPL_QUYETTOAN();
                    List<T_SXXK_NPL_QUYETTOAN> NPLQTCollection = new List<T_SXXK_NPL_QUYETTOAN>();
                    T_SXXK_NPL_QUYETTOAN_CHITIET NPLQTChiTiet = new T_SXXK_NPL_QUYETTOAN_CHITIET();
                    List<T_SXXK_NPL_QUYETTOAN_CHITIET> NPLQTChiTietCollection = new List<T_SXXK_NPL_QUYETTOAN_CHITIET>();
                    DataSet ds = new DataSet();
                    dtTotal = new DataTable();
                    DataColumn dtColMANPL = new DataColumn("MANPL");
                    dtColMANPL.DataType = typeof(System.String);
                    DataColumn dtColTENNPL = new DataColumn("TENNPL");
                    dtColTENNPL.DataType = typeof(System.String);
                    DataColumn dtColDVT = new DataColumn("DVT");
                    dtColDVT.DataType = typeof(System.String);
                    DataColumn dtColLUONGTONDK = new DataColumn("LUONGTONDK");
                    dtColLUONGTONDK.DataType = typeof(System.Decimal);
                    DataColumn dtColTRIGIATONDK = new DataColumn("TRIGIATONDK");
                    dtColTRIGIATONDK.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGNHAPTK = new DataColumn("LUONGNHAPTK");
                    dtColLUONGNHAPTK.DataType = typeof(System.Decimal);
                    DataColumn dtColTRIGIANHAPTK = new DataColumn("TRIGIANHAPTK");
                    dtColTRIGIANHAPTK.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGXUATTK = new DataColumn("LUONGXUATTK");
                    dtColLUONGXUATTK.DataType = typeof(System.Decimal);
                    DataColumn dtColTRIGIAXUATTK = new DataColumn("TRIGIAXUATTK");
                    dtColTRIGIAXUATTK.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGTONCK = new DataColumn("LUONGTONCK");
                    dtColLUONGTONCK.DataType = typeof(System.Decimal);
                    DataColumn dtColTRIGIATONCK = new DataColumn("TRIGIATONCK");
                    dtColTRIGIATONCK.DataType = typeof(System.Decimal);
                    DataColumn dtColNAMQUYETTOAN = new DataColumn("NAMQUYETTOAN");
                    dtColNAMQUYETTOAN.DataType = typeof(System.Decimal);

                    dtTotal.Columns.Add(dtColMANPL);
                    dtTotal.Columns.Add(dtColTENNPL);
                    dtTotal.Columns.Add(dtColDVT);
                    dtTotal.Columns.Add(dtColLUONGTONDK);
                    dtTotal.Columns.Add(dtColTRIGIATONDK);
                    dtTotal.Columns.Add(dtColLUONGNHAPTK);
                    dtTotal.Columns.Add(dtColTRIGIANHAPTK);
                    dtTotal.Columns.Add(dtColLUONGXUATTK);
                    dtTotal.Columns.Add(dtColTRIGIAXUATTK);
                    dtTotal.Columns.Add(dtColLUONGTONCK);
                    dtTotal.Columns.Add(dtColTRIGIATONCK);
                    dtTotal.Columns.Add(dtColNAMQUYETTOAN);
                    //Xử lý lượng tồn đầu kỳ
                    #region Xử lý lượng tồn đầu kỳ
                    NamQuyetToan = Convert.ToInt32(txtNamQT.Text.ToString());
                    NamQuyetToanOld = NamQuyetToan - 1;
                    DataTable dtbNPLQTOld = NPLQuyetToan.SelectDynamic("YEAR(NGAYBATDAU) =" + NamQuyetToanOld, "MANPL").Tables[0];
                    DataTable dtbNPLQuyeToan = NPLQuyetToan.SelectDynamic("YEAR(NGAYBATDAU) =" + NamQuyetToan, "MANPL").Tables[0];
                    // Kiểm tra quyết toán là lần đầu tiên hay lần tiếp theo
                    if (dtbNPLQTOld.Rows.Count == 0)
                    {
                        // Lần đầu tiên thì tính toán số liệu tồn đầu kỳ
                        DataTable dtNPLTon = NPLNhapTon.SelectTotalNPLTonByYear(NamQuyetToan).Tables[0];

                        foreach (DataRow dr in dtNPLTon.Rows)
                        {
                            try
                            {

                                DataRow drNPLTon = dtTotal.NewRow();
                                drNPLTon["MANPL"] = dr["MANPL"];
                                drNPLTon["TENNPL"] = dr["TENNPL"].ToString();
                                drNPLTon["DVT"] = dr["DVT"].ToString();
                                drNPLTon["LUONGTONDK"] = dr["LUONGTONDK"];
                                drNPLTon["TRIGIATONDK"] = dr["TRIGIATONDK"];
                                drNPLTon["LUONGNHAPTK"] = 0;
                                drNPLTon["TRIGIANHAPTK"] = 0;
                                drNPLTon["LUONGXUATTK"] = 0;
                                drNPLTon["TRIGIAXUATTK"] = 0;
                                drNPLTon["LUONGTONCK"] = 0;
                                drNPLTon["TRIGIATONCK"] = 0;
                                drNPLTon["NAMQUYETTOAN"] = NamQuyetToan;
                                dtTotal.Rows.Add(drNPLTon);
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                    else
                    {
                        foreach (DataRow dr in dtbNPLQuyeToan.Rows)
                        {
                            try
                            {

                                DataRow drNPLTon = dtTotal.NewRow();
                                drNPLTon["MANPL"] = dr["MANPL"];
                                drNPLTon["TENNPL"] = dr["TENNPL"].ToString();
                                drNPLTon["DVT"] = dr["DVT"].ToString();
                                //Gán lượng tồn cuối kỳ năm QT trước cho tồn đầu kỳ năm QT này
                                drNPLTon["LUONGTONDK"] = drNPLTon["LUONGTONCK"];
                                drNPLTon["TRIGIATONDK"] = drNPLTon["TRIGIATONCK"];
                                drNPLTon["LUONGNHAPTK"] = 0;
                                drNPLTon["TRIGIANHAPTK"] = 0;
                                drNPLTon["LUONGXUATTK"] = 0;
                                drNPLTon["TRIGIAXUATTK"] = 0;
                                drNPLTon["LUONGTONCK"] = 0;
                                drNPLTon["TRIGIATONCK"] = 0;
                                drNPLTon["NAMQUYETTOAN"] = NamQuyetToan;
                                dtTotal.Rows.Add(drNPLTon);
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                    #endregion
                    #region Xử lý lượng nhập trong kỳ
                    DataTable dtNPLNhap = NPLNhapTon.SelectTotalNPLNhapByYear(NamQuyetToan).Tables[0];
                    int j = 0; //Biến đánh dấu Mã NPL nhập có tồn tại trong Table NPL Tổng
                    for (int i = 0; i < dtNPLNhap.Rows.Count; i++)
                    {
                        MaNPL = dtNPLNhap.Rows[i]["MANPL"].ToString();
                        for (int k = 0; k < dtTotal.Rows.Count; k++)
                        {
                            MaNPLTemp = dtTotal.Rows[k]["MANPL"].ToString();
                            if (MaNPL == MaNPLTemp)
                            {
                                j = 1;
                                dtTotal.Rows[k]["LUONGNHAPTK"] = dtNPLNhap.Rows[i]["LUONGNHAPTK"].ToString();
                                dtTotal.Rows[k]["TRIGIANHAPTK"] = dtNPLNhap.Rows[i]["TRIGIANHAPTK"].ToString();
                            }
                        }
                        if (j==0)
                        {
                            try
                            {
                                DataRow drNPLNhap = dtTotal.NewRow();
                                drNPLNhap["MANPL"] = dtNPLNhap.Rows[i]["MANPL"].ToString();
                                drNPLNhap["TENNPL"] = dtNPLNhap.Rows[i]["TENNPL"].ToString();
                                drNPLNhap["DVT"] = dtNPLNhap.Rows[i]["DVT"].ToString();
                                drNPLNhap["LUONGTONDK"] = 0;
                                drNPLNhap["TRIGIATONDK"] = 0;
                                drNPLNhap["LUONGNHAPTK"] = dtNPLNhap.Rows[i]["LUONGNHAPTK"].ToString();
                                drNPLNhap["TRIGIANHAPTK"] = dtNPLNhap.Rows[i]["TRIGIANHAPTK"].ToString();
                                drNPLNhap["LUONGXUATTK"] = 0;
                                drNPLNhap["TRIGIAXUATTK"] = 0;
                                drNPLNhap["LUONGTONCK"] = 0;
                                drNPLNhap["TRIGIATONCK"] = 0;
                                drNPLNhap["NAMQUYETTOAN"] = NamQuyetToan;
                                dtTotal.Rows.Add(drNPLNhap);
                                                        
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                    #endregion
                    #region Xử lý NPL xuất trong kỳ chi tiết và tổng hợp 
                    DataTable dtNPLXuat = BCXuatNhapTon.SelectNPLXuatDetailByYear(NamQuyetToan);
                    DataTable dtNPLXuatToTal = dtTotal.Clone();
                    DataTable dtNPLXuatDetail = dtNPLXuat.Clone();
                    //Biến đánh dấu NPL bị âm (1 : Âm , 0 : Dương )
                    for (int i = 0; i < dtNPLXuat.Rows.Count; i++)

                    {
                        try
                        {
                            MaNPL = dtNPLXuat.Rows[i]["MANPL"].ToString();
                            if (i + 1 < dtNPLXuat.Rows.Count)
                            {
                                MaNPLTemp = dtNPLXuat.Rows[i + 1]["MANPL"].ToString();
                            }
                            else
                            {
                                MaNPLTemp = dtNPLXuat.Rows[i - 1]["MANPL"].ToString();
                            }
                            LuongNhap = Convert.ToDecimal(dtNPLXuat.Rows[i]["LUONGNHAP"].ToString());
                            DonGiaNhap = Convert.ToDecimal(dtNPLXuat.Rows[i]["DONGIANHAP"].ToString());
                            TriGiaNhap = Convert.ToDecimal(dtNPLXuat.Rows[i]["DONGIANHAP"].ToString()) * LuongNhap;
                            LuongXuat = Convert.ToDecimal(dtNPLXuat.Rows[i]["LUONGXUAT"].ToString());
                            TriGiaXuat = Convert.ToDecimal(dtNPLXuat.Rows[i]["TRIGIAXUAT"].ToString());
                            LuongTon = Convert.ToDecimal(dtNPLXuat.Rows[i]["LUONGTONCUOI"].ToString());
                            TriGiaTon = Convert.ToDecimal(dtNPLXuat.Rows[i]["LUONGTONCUOI"].ToString()) * Convert.ToDecimal(dtNPLXuat.Rows[i]["DONGIANHAP"].ToString());
                            if (MaNPLTemp == MaNPL)
                            {
                                if (LuongTon >=0)
                                {
                                    DataRow dr = dtNPLXuatDetail.NewRow();
                                    dr["TOKHAIXUAT"] = dtNPLXuat.Rows[i]["TOKHAIXUAT"].ToString();
                                    dr["TOKHAINHAP"] = dtNPLXuat.Rows[i]["TOKHAINHAP"].ToString();
                                    dr["MANPL"] = MaNPL;
                                    dr["TENNPL"] = dtNPLXuat.Rows[i]["TENNPL"].ToString();
                                    dr["DVT_NPL"] = dtNPLXuat.Rows[i]["DVT_NPL"].ToString();
                                    dr["MASP"] = dtNPLXuat.Rows[i]["MASP"].ToString(); ;
                                    dr["TENSP"] = dtNPLXuat.Rows[i]["TENSP"].ToString();
                                    dr["DVT_SP"] = dtNPLXuat.Rows[i]["DVT_SP"].ToString();
                                    dr["LUONGNHAP"] = LuongNhap;
                                    dr["DONGIANHAP"] = dtNPLXuat.Rows[i]["DONGIANHAP"].ToString();
                                    dr["TRIGIANHAP"] = TriGiaNhap;
                                    if (k==0)
                                    {
                                        dr["LUONGXUAT"] = LuongXuat;
                                        dr["TRIGIAXUAT"] = TriGiaXuat;
                                    }
                                    else
                                    {
                                        dr["LUONGXUAT"] = LuongTonCuoiTemp;//LuongXuat - LuongXuatTemp;
                                        dr["TRIGIAXUAT"] = LuongTonCuoiTemp * DonGiaNhap ;//TriGiaXuat - TriGiaXuatTemp;
                                    }
                                    dr["LUONGTONCUOI"] = LuongTon;
                                    dr["TRIGIATON"] = TriGiaTon;
                                    dr["TYGIATINHTHUE"] = dtNPLXuat.Rows[i]["TYGIATINHTHUE"].ToString();
                                    dtNPLXuatDetail.Rows.Add(dr);
                                    LuongXuatTemp = LuongTon;
                                    TriGiaXuatTemp = TriGiaTon;
                                    k = 0;
                                }                                
                                else
                                {
                                    DataRow dr = dtNPLXuatDetail.NewRow();
                                    dr["TOKHAIXUAT"] = dtNPLXuat.Rows[i]["TOKHAIXUAT"].ToString();
                                    dr["TOKHAINHAP"] = dtNPLXuat.Rows[i]["TOKHAINHAP"].ToString();
                                    dr["MANPL"] = MaNPL;
                                    dr["TENNPL"] = dtNPLXuat.Rows[i]["TENNPL"].ToString();
                                    dr["DVT_NPL"] = dtNPLXuat.Rows[i]["DVT_NPL"].ToString();
                                    dr["MASP"] = dtNPLXuat.Rows[i]["MASP"].ToString(); ;
                                    dr["TENSP"] = dtNPLXuat.Rows[i]["TENSP"].ToString();
                                    dr["DVT_SP"] = dtNPLXuat.Rows[i]["DVT_SP"].ToString();
                                    dr["LUONGNHAP"] = LuongNhap;
                                    dr["DONGIANHAP"] = dtNPLXuat.Rows[i]["DONGIANHAP"].ToString();
                                    dr["TRIGIANHAP"] = TriGiaNhap;
                                    dr["LUONGXUAT"] = LuongXuatTemp;
                                    dr["TRIGIAXUAT"] = TriGiaXuatTemp;
                                    dr["LUONGTONCUOI"] = 0;
                                    dr["TRIGIATON"] = 0;
                                    dr["TYGIATINHTHUE"] = dtNPLXuat.Rows[i]["TYGIATINHTHUE"].ToString();
                                    k = 1;//Đánh dấu NPL bị âm 
                                    dtNPLXuatDetail.Rows.Add(dr);
                                    LuongTonCuoiTemp = LuongTon * -1;
                                }
                            }
                            else
                            {
                                if (LuongTon >=0)
                                {
                                    DataRow dr = dtNPLXuatDetail.NewRow();
                                    dr["TOKHAIXUAT"] = dtNPLXuat.Rows[i]["TOKHAIXUAT"].ToString();
                                    dr["TOKHAINHAP"] = dtNPLXuat.Rows[i]["TOKHAINHAP"].ToString();
                                    dr["MANPL"] = MaNPL;
                                    dr["TENNPL"] = dtNPLXuat.Rows[i]["TENNPL"].ToString();
                                    dr["DVT_NPL"] = dtNPLXuat.Rows[i]["DVT_NPL"].ToString();
                                    dr["MASP"] = dtNPLXuat.Rows[i]["MASP"].ToString(); ;
                                    dr["TENSP"] = dtNPLXuat.Rows[i]["TENSP"].ToString();
                                    dr["DVT_SP"] = dtNPLXuat.Rows[i]["DVT_SP"].ToString();
                                    dr["LUONGNHAP"] = LuongNhap;
                                    dr["DONGIANHAP"] = dtNPLXuat.Rows[i]["DONGIANHAP"].ToString();
                                    dr["TRIGIANHAP"] = TriGiaNhap;
                                    dr["LUONGXUAT"] = LuongXuat;
                                    dr["TRIGIAXUAT"] = TriGiaXuat;
                                    dr["LUONGTONCUOI"] = LuongTon;
                                    dr["TRIGIATON"] = TriGiaTon;
                                    dr["TYGIATINHTHUE"] = dtNPLXuat.Rows[i]["TYGIATINHTHUE"].ToString();
                                    dtNPLXuatDetail.Rows.Add(dr);
                                    k = 0;
                                }
                                else
                                {
                                    DataRow dr = dtNPLXuatDetail.NewRow();
                                    dr["TOKHAIXUAT"] = dtNPLXuat.Rows[i]["TOKHAIXUAT"].ToString();
                                    dr["TOKHAINHAP"] = dtNPLXuat.Rows[i]["TOKHAINHAP"].ToString();
                                    dr["MANPL"] = MaNPL;
                                    dr["TENNPL"] = dtNPLXuat.Rows[i]["TENNPL"].ToString();
                                    dr["DVT_NPL"] = dtNPLXuat.Rows[i]["DVT_NPL"].ToString();
                                    dr["MASP"] = dtNPLXuat.Rows[i]["MASP"].ToString(); ;
                                    dr["TENSP"] = dtNPLXuat.Rows[i]["TENSP"].ToString();
                                    dr["DVT_SP"] = dtNPLXuat.Rows[i]["DVT_SP"].ToString();
                                    dr["LUONGNHAP"] = LuongNhap;
                                    dr["DONGIANHAP"] = dtNPLXuat.Rows[i]["DONGIANHAP"].ToString();
                                    dr["TRIGIANHAP"] = TriGiaNhap;
                                    dr["LUONGXUAT"] = LuongTon;
                                    dr["TRIGIAXUAT"] = TriGiaTon;
                                    dr["LUONGTONCUOI"] = 0;
                                    dr["TRIGIATON"] = 0;
                                    dr["TYGIATINHTHUE"] = dtNPLXuat.Rows[i]["TYGIATINHTHUE"].ToString();
                                    k = 1; // Đánh dấu dòng NPL bị âm
                                    LuongXuatTemp = LuongTon;
                                    TriGiaXuatTemp = TriGiaTon;
                                    dtNPLXuatDetail.Rows.Add(dr);
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            StreamWriter write = File.AppendText("Error.txt");
                            write.WriteLine("--------------------------------");
                            write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                            write.WriteLine(ex.StackTrace);
                            write.WriteLine("Lỗi là : ");
                            write.WriteLine(ex.Message);
                            write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + MaNPL.ToString());
                            write.WriteLine("--------------------------------");
                            write.Flush();
                            write.Close();
                            Logger.LocalLogger.Instance().WriteMessage(ex);                            
                        }
                    }
                    dgListNPLXuatDetails.Refresh();
                    dgListNPLXuatDetails.DataSource = dtNPLXuatDetail;
                    dgListNPLXuatDetails.Refetch();
                    //for (int i = 0; i < dtNPLXuatDetail.Rows.Count; i++)
                    //{
                    //    MaNPL = dtNPLXuatDetail.Rows[i]["MANPL"].ToString();
                    //    MaNPLTemp = dtNPLXuatDetail.Rows[i + 1]["MANPL"].ToString();
                    //    LuongNhap = Convert.ToDecimal(dtNPLXuatDetail.Rows[i]["LUONGNHAP"].ToString());
                    //    TriGiaNhap = Convert.ToDecimal(dtNPLXuatDetail.Rows[i]["DONGIANHAP"].ToString()) * LuongNhap;
                    //    //LuongTon = Convert.ToDecimal(dtNPLXuatDetail.Rows[i]["LUONGTONDAU"].ToString());
                    //    LuongXuat = Convert.ToDecimal(dtNPLXuatDetail.Rows[i]["LUONGXUAT"].ToString());
                    //    LuongXuatTotal += LuongXuat;
                    //    TriGiaXuat = Convert.ToDecimal(dtNPLXuatDetail.Rows[i]["TRIGIAXUAT"].ToString());
                    //    TriGiaXuatToTal += TriGiaXuat;
                    //    if (MaNPL == MaNPLTemp)
                    //    {
                    //        // Dòng đầu tiên
                    //        if (dtNPLXuatToTal.Rows.Count == 0)
                    //        {
                    //            DataRow dr = dtNPLXuatToTal.NewRow();
                    //            dr["MANPL"] = MaNPL;
                    //            dr["TENNPL"] = dtNPLXuatDetail.Rows[i]["TENNPL"].ToString();
                    //            dr["DVT"] = dtNPLXuatDetail.Rows[i]["DVT_NPL"].ToString();
                    //            dr["LUONGTONDK"] = 0;
                    //            dr["TRIGIATONDK"] = 0;
                    //            dr["LUONGNHAPTK"] = 0;
                    //            dr["TRIGIANHAPTK"] = 0;
                    //            dr["LUONGXUATTK"] = LuongXuatTotal;
                    //            dr["TRIGIAXUATTK"] = TriGiaXuatToTal;
                    //            dr["LUONGTONCK"] = 0;
                    //            dr["TRIGIATONCK"] = 0;
                    //            //dr["TYGIATINHTHUE"] = dtNPLXuatDetail.Rows[i]["TYGIATINHTHUE"].ToString();
                    //            dr["NAMQUYETTOAN"] = NamQuyetToan;
                    //            dtNPLXuatToTal.Rows.Add(dr); 
                    //        }
                    //        else
                    //        {
                    //            for (int l = 0; l < dtNPLXuatToTal.Rows.Count; l++)
                    //            {
                    //                string MaNPLToTal = dtNPLXuatToTal.Rows[l]["MANPL"].ToString();
                    //                if (MaNPL == MaNPLToTal)
                    //                {
                    //                    //Gán lại lượng xuất 
                    //                    dtNPLXuatToTal.Rows[l]["LUONGXUATTK"] = LuongXuatTotal;
                    //                    dtNPLXuatToTal.Rows[l]["TRIGIAXUATTK"] = TriGiaXuatToTal;
                    //                }
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        DataRow dr = dtNPLXuatToTal.NewRow();
                    //        dr["MANPL"] = MaNPL;
                    //        dr["TENNPL"] = dtNPLXuatDetail.Rows[i]["TENNPL"].ToString();
                    //        dr["DVT"] = dtNPLXuatDetail.Rows[i]["DVT_NPL"].ToString();
                    //        dr["LUONGTONDK"] = 0;
                    //        dr["TRIGIATONDK"] = 0;
                    //        dr["LUONGNHAPTK"] = 0;
                    //        dr["TRIGIANHAPTK"] = 0;
                    //        dr["LUONGXUATTK"] = LuongXuat;
                    //        dr["TRIGIAXUATTK"] = TriGiaXuat;
                    //        dr["LUONGTONCK"] = 0;
                    //        dr["TRIGIATONCK"] = 0;
                    //        //dr["TYGIATINHTHUE"] = dtNPLXuatToTal.Rows[i]["TYGIATINHTHUE"].ToString();
                    //        dr["NAMQUYETTOAN"] = dtNPLXuatDetail.Rows[i]["NAMQUYETTOAN"].ToString();
                    //        LuongXuatTotal = LuongXuat;
                    //        TriGiaXuatToTal = TriGiaXuat;
                    //        dtNPLXuatToTal.Rows.Add(dr);  
                    //    }
                    //}
                    dtNPLXuatToTal = GetGroupedBy(dtNPLXuatDetail, "MANPL,LUONGXUAT,TRIGIAXUAT", "MANPL", "Sum");
                    // Xử lý lượng xuất trong kỳ
                    for (int i = 0; i < dtNPLXuatToTal.Rows.Count; i++)
                    {
                        MaNPL = dtNPLXuatToTal.Rows[i]["MANPL"].ToString();
                        for (int l = 0; l < dtTotal.Rows.Count; l++)
                        {
                            MaNPLTemp = dtTotal.Rows[l]["MANPL"].ToString();
                            if (MaNPL == MaNPLTemp)
                            {
                                j = 1;
                                dtTotal.Rows[l]["LUONGXUATTK"] = dtNPLXuatToTal.Rows[i]["LUONGXUAT"].ToString();
                                dtTotal.Rows[l]["TRIGIAXUATTK"] = dtNPLXuatToTal.Rows[i]["TRIGIAXUAT"].ToString();
                            }
                        }
                        if (j == 0)
                        {
                            try
                            {
                                DataRow drNPLXuat = dtTotal.NewRow();
                                drNPLXuat["MANPL"] = dtNPLXuatToTal.Rows[i]["MANPL"].ToString();
                                for (int m = 0; m < dtNPLXuatDetail.Rows.Count; m++)
                                {
                                    MaNPLTemp = dtNPLXuatDetail.Rows[m]["MANPL"].ToString();
                                    if (MaNPL == MaNPLTemp)
                                    {
                                        drNPLXuat["TENNPL"] = dtNPLXuatDetail.Rows[m]["TENNPL"].ToString();
                                        drNPLXuat["DVT"] = dtNPLXuatDetail.Rows[m]["DVT_NPL"].ToString();
                                    }
                                }
                                drNPLXuat["LUONGTONDK"] = 0;
                                drNPLXuat["TRIGIATONDK"] = 0;
                                drNPLXuat["LUONGNHAPTK"] = 0;
                                drNPLXuat["TRIGIANHAPTK"] = 0;
                                drNPLXuat["LUONGXUATTK"] = dtNPLXuatToTal.Rows[i]["LUONGXUAT"].ToString();
                                drNPLXuat["TRIGIAXUATTK"] = dtNPLXuatToTal.Rows[i]["TRIGIAXUAT"].ToString();
                                drNPLXuat["LUONGTONCK"] = 0;
                                drNPLXuat["TRIGIATONCK"] = 0;
                                drNPLXuat["NAMQUYETTOAN"] = NamQuyetToan;
                                dtTotal.Rows.Add(drNPLXuat);

                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
#endregion
                    this.Cursor = Cursors.Default;
                    transaction.Commit();
                    //Xử lý lượng tồn cuối kỳ
                    //DataTable dtNPLNhapTK = NPLNhapTonThucTe.SelectBy_Date(HD.ID, HD.NgayKy, dateFrom).Tables[0];
                    //DataTable dtNPLXuatTK = PhanBoToKhaiNhap.SelectBy_Date(HD.ID, HD.NgayKy, dateFrom).Tables[0];  
                    MLMessages("Xử lý dữ liệu thành công ", "MSG_WRN34", "", false);
                    //btnProcessDataNPL.Enabled = false;
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    transaction.Rollback();
                    MLMessages("Xử lý dữ liệu không thành công ", "MSG_WRN34", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        //public void Process()
        //{
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
        //        try
        //        {
        //            this.Cursor = Cursors.WaitCursor;
        //            List<T_GC_NPL_QUYETOAN> NPLQTCollection = new List<T_GC_NPL_QUYETOAN>();
        //            #region Xử lý lượng tồn cuối kỳ
        //            NPLQTCollection = T_GC_NPL_QUYETOAN.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID, "MANPL");
        //            foreach (T_GC_NPL_QUYETOAN NPLDataItem in NPLQTCollection)
        //            {
        //                NPLDataItem.LUONGTONCK = NPLDataItem.LUONGTONDK + NPLDataItem.LUONGNHAPTK - NPLDataItem.LUONGXUATTK;
        //                NPLDataItem.TRIGIATONCK = NPLDataItem.TRIGIATONDK + NPLDataItem.TRIGIANHAPTK - NPLDataItem.TRIGIAXUATTK;
        //                NPLDataItem.UPDATE_TONCK(transaction);
        //            }
        //            transaction.Commit();
        //            MLMessages("Xử lý dữ liệu thành công ", "MSG_WRN34", "", false);
        //            #endregion
        //        }
        //        catch (Exception ex)
        //        {
        //            this.Cursor = Cursors.Default;
        //            transaction.Rollback();
        //            MLMessages("Xử lý dữ liệu không thành công ", "MSG_WRN34", "", false);
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //        }
        //    }

        //}
        public void BindDataGoodItem()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = goodItem.GoodItemsCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private DataTable GetCungUngChiTiet()
        {
            string ngaychotton = dtpNgayChotTon.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string tuNgay = dtpTuNgay.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string denNgay = dtpDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
            try
            {
                #region sql
                string sql = @"SELECT  cudk.ID ,
        hs.LanThanhLy ,
        cu.SoToKhaiXuat,
        cu.MaSanPham,
        cu.TenSanPham,
        cud.MaNPL ,
        cud.TenNPL ,
		cud.LuongNPL AS LuongNPLCU ,
        CASE WHEN cud.DVT_ID <> ''
             THEN ( SELECT  dvt.Ten
                    FROM    dbo.t_HaiQuan_DonViTinh dvt
                    WHERE   dvt.ID = cud.DVT_ID
                  )
             ELSE cud.DVT_ID
        END AS TenDVT
FROM    dbo.t_KDT_SXXK_BKNPLCungUngDangKy cudk
        JOIN dbo.t_KDT_SXXK_BKNPLCungUng cu ON cu.Master_id = cudk.ID
        JOIN dbo.t_KDT_SXXK_BKNPLCungUng_Detail cud ON cud.Master_id = cu.ID
        JOIN dbo.t_KDT_SXXK_HoSoThanhLyDangKy hs ON hs.LanThanhLy = cu.LanThanhLy
        WHERE cudk.NgayTiepNhan>='" + tuNgay + @"' AND cudk.NgayTiepNhan <='" + denNgay + @"'
        ORDER BY MaNPL,cu.LanThanhLy";
                #endregion
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                //db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, Int32.Parse(txtNamTiepNhan.Text));
                dbCommand.CommandTimeout = 60000;
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private DataTable GetCungUngTong()
        {
            string ngaychotton = dtpNgayChotTon.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string tuNgay = dtpTuNgay.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string denNgay = dtpDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
            try
            {
                #region sql
                string sql = @"SELECT  
        cud.MaNPL ,
        cud.TenNPL ,
		SUM(cud.LuongNPL) AS LuongNPLCU ,
        CASE WHEN cud.DVT_ID <> ''
             THEN ( SELECT  dvt.Ten
                    FROM    dbo.t_HaiQuan_DonViTinh dvt
                    WHERE   dvt.ID = cud.DVT_ID
                  )
             ELSE cud.DVT_ID
        END AS TenDVT 
FROM    dbo.t_KDT_SXXK_BKNPLCungUngDangKy cudk
        JOIN dbo.t_KDT_SXXK_BKNPLCungUng cu ON cu.Master_id = cudk.ID
        JOIN dbo.t_KDT_SXXK_BKNPLCungUng_Detail cud ON cud.Master_id = cu.ID
        JOIN dbo.t_KDT_SXXK_HoSoThanhLyDangKy hs ON hs.LanThanhLy = cu.LanThanhLy
        WHERE cudk.NgayTiepNhan>='" + tuNgay + @"' AND cudk.NgayTiepNhan <='" + denNgay + @"'
		GROUP BY 
        cud.MaNPL ,
        cud.TenNPL ,
		cud.DVT_ID
		



		";
                #endregion
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                //db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, Int32.Parse(txtNamTiepNhan.Text));
                dbCommand.CommandTimeout = 60000;
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private DataTable GetChiTietNPLXuat_SP()
        {
            string tuNgay = dtpTuNgay.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string denNgay = dtpDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
            try
            {
                string sql = @"SELECT  tkmd.SoToKhai ,
        tkmd.NgayDangKy ,
        tkmd.MaLoaiHinh ,
        tkmd.NamDangKy,
        hmd.MaPhu,
        hmd.TenHang,
        hmd.SoLuong,
        dm.MaNguyenPhuLieu,
        dm.DinhMucChung,
        dm.DinhMucSuDung,
        hmd.SoLuong * dm.DinhMucChung AS LuongNPLChung,
        hmd.SoLuong * dm.DinhMucSuDung AS LuongNPLSuDungChung
        
FROM    dbo.t_SXXK_ToKhaiMauDich tkmd
        JOIN dbo.t_SXXK_HangMauDich hmd ON tkmd.SoToKhai = hmd.SoToKhai
                                           AND tkmd.NamDangKy = hmd.NamDangKy
                                           AND tkmd.MaLoaiHinh = hmd.MaLoaiHinh
                                           AND tkmd.MaHaiQuan = hmd.MaHaiQuan
         JOIN dbo.t_SXXK_DinhMuc dm ON hmd.MaPhu = dm.MaSanPham                                  
WHERE   tkmd.MaLoaiHinh LIKE 'X%'
        AND tkmd.SoToKhai <> 0
        AND tkmd.NgayDangKy >= '" + tuNgay + @"'
        AND NgayDangKy <= '" + denNgay + @"'
ORDER BY MaNguyenPhuLieu,NgayDangKy";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                //db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, Int32.Parse(txtNamTiepNhan.Text));
                dbCommand.CommandTimeout = 60000;
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private DataTable GetTongNPLXuat()
        {
            string ngaychotton = dtpNgayChotTon.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string tuNgay = dtpTuNgay.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string denNgay = dtpDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
            try
            {
                string sql = @"SELECT  t.MaNguyenPhuLieu ,
        SUM(t.LuongNPLSuDungChung) AS LuongNPLSuDungChung,MAX(dvt.Ten) AS DVT
FROM    ( SELECT    tkmd.SoToKhai ,
                    tkmd.NgayDangKy ,
                    tkmd.MaLoaiHinh ,
                    tkmd.NamDangKy ,
                    hmd.MaPhu ,
                    hmd.TenHang ,
                    hmd.SoLuong ,
                    dm.MaNguyenPhuLieu ,
                    dm.DinhMucChung ,
                    dm.DinhMucSuDung ,
                    hmd.SoLuong * dm.DinhMucChung AS LuongNPLChung ,
                    hmd.SoLuong * dm.DinhMucSuDung AS LuongNPLSuDungChung
          FROM      dbo.t_SXXK_ToKhaiMauDich tkmd
                    JOIN dbo.t_SXXK_HangMauDich hmd ON tkmd.SoToKhai = hmd.SoToKhai
                                                       AND tkmd.NamDangKy = hmd.NamDangKy
                                                       AND tkmd.MaLoaiHinh = hmd.MaLoaiHinh
                                                       AND tkmd.MaHaiQuan = hmd.MaHaiQuan
                    JOIN dbo.t_SXXK_DinhMuc dm ON hmd.MaPhu = dm.MaSanPham
          WHERE     tkmd.MaLoaiHinh LIKE 'X%'
                    AND tkmd.SoToKhai <> 0
                    AND tkmd.NgayDangKy >= '" + tuNgay + @"'
                    AND NgayDangKy <= '" + denNgay + @"'
        ) t JOIN dbo.t_SXXK_NguyenPhuLieu npl ON t.MaNguyenPhuLieu = npl.Ma JOIN dbo.t_HaiQuan_DonViTinh dvt ON npl.DVT_ID = dvt.ID
GROUP BY t.MaNguyenPhuLieu
ORDER BY MaNguyenPhuLieu";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                //db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, Int32.Parse(txtNamTiepNhan.Text));
                dbCommand.CommandTimeout = 60000;
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void XuatNhapTonForm_Load(object sender, EventArgs e)
        {
            dgList.HideColumnsWhenGrouped = InheritableBoolean.True;
            txtNamQT.Text = (DateTime.Now.Year - 1).ToString();
            cbbLoaiHinhBaoCao.SelectedIndex = 0;
            cbbDVT.SelectedIndex = 0;
            if (IsActive)
            {
                SetGoodItem();
                BindDataGoodItem();
            }
        }
        private void ExportExcel(string tenFile, GridEX dg)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = tenFile + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dg;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }
        private void btnXuatExcel_Click(object sender, EventArgs e)
        {

        }

        private void btnInBaoCao_Click(object sender, EventArgs e)
        {
            try
            {
                //this.Cursor = Cursors.WaitCursor;
                //NhapXuatTon_TT38 xnt = new NhapXuatTon_TT38();
                //xnt.HD = this.HD;


                Mau15_TT38_ChiTiet rp = new Mau15_TT38_ChiTiet();
                //rp.tb_Mau15 = tb;
                rp.BindReport(namDK.ToString());
                rp.ShowPreview();

                //xnt.tb = tb;
                //xnt.BindReport("");
                //xnt.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGetDuLieu_Click(object sender, EventArgs e)
        {
            try
            {
                txtTrangThaiXuLy.Text = "Đang lấy dữ liệu...";
                this.Cursor = Cursors.WaitCursor;
                //lấy chi tiết theo tờ khai theo ngày
                tb = GetDataSource();
                dgList.DataSource = tb;

                //lấy tổng theo NPL theo ngày
                tbTong = GetTongData();
                dgTongNPL.DataSource = tbTong;
                //lấy chi tiết npl xuất theo định mức theo ngày
                tbNPLChiTietXuat = GetChiTietNPLXuat_SP();
                dgChiTietNPLXuat.DataSource = GetChiTietNPLXuat_SP();
                //lấy tổng npl xuất theo ngày
                tbNPLTongXuat = GetTongNPLXuat();
                dgTongNPLXuat.DataSource = GetTongNPLXuat();
                //lấy chi tiết cung ứng theo ngày
                tbNPLCU_Details = GetCungUngChiTiet();
                dgNPLCU.DataSource = tbNPLCU_Details;
                //Lấy tổng cung ứng theo ngày
                tbNPLCU_Total = GetCungUngTong();
                dgNPLCU_Total.DataSource = tbNPLCU_Total;
                //Tổng hợp báo cáo quyết toán
                //ShowMessage("Xử lý dữ liệu thành công", false);
                //ProcessDataNPL();
                GetDataGoodItem();
                //GetDataGoodItemNew();
                BindDataGoodItem();

            }
            catch (Exception ex)
            {
                txtTrangThaiXuLy.Text = "Dữ liệu chưa được xử lý.";
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                txtTrangThaiXuLy.Text = "Lấy dữ liệu thành công.";
            }

        }

        private void btnMau15Luong_Click(object sender, EventArgs e)
        {
            try
            {

                Mau15_TT38 rp = new Mau15_TT38();
                DataTable dt = new DataTable();
                dt = tbTong.Clone();
                if (!dt.Columns.Contains("SoTaiKhoan"))
                {
                    DataTable dtNPL = new DataTable();
                    DataColumn[] dtColumnNPL = new DataColumn[15];
                    dtColumnNPL[1] = new DataColumn("SoTaiKhoan", typeof(string));
                    dtColumnNPL[2] = new DataColumn("MaNPL", typeof(string));
                    dtColumnNPL[3] = new DataColumn("TenNPL", typeof(string));
                    dtColumnNPL[4] = new DataColumn("TenDVT", typeof(string));
                    dtColumnNPL[5] = new DataColumn("TonDauKy", typeof(decimal));
                    dtColumnNPL[6] = new DataColumn("TriGiaTTTonDauKy", typeof(decimal));
                    dtColumnNPL[7] = new DataColumn("LuongNhapTrongKy", typeof(decimal));
                    dtColumnNPL[8] = new DataColumn("TriGiaTTNhapTrongKy", typeof(decimal));
                    dtColumnNPL[9] = new DataColumn("LuongXuatTrongKy", typeof(decimal));
                    dtColumnNPL[10] = new DataColumn("TriGiaTTXuatTrongKy", typeof(decimal));
                    dtColumnNPL[11] = new DataColumn("TonCuoiKy", typeof(decimal));
                    dtColumnNPL[12] = new DataColumn("TriGiaTTTonCuoiKy", typeof(decimal));
                    dtColumnNPL[13] = new DataColumn("GhiChu", typeof(string));

                    dtNPL.Columns.Add(dtColumnNPL[1]);
                    dtNPL.Columns.Add(dtColumnNPL[2]);
                    dtNPL.Columns.Add(dtColumnNPL[3]);
                    dtNPL.Columns.Add(dtColumnNPL[4]);
                    dtNPL.Columns.Add(dtColumnNPL[5]);
                    dtNPL.Columns.Add(dtColumnNPL[6]);
                    dtNPL.Columns.Add(dtColumnNPL[7]);
                    dtNPL.Columns.Add(dtColumnNPL[8]);
                    dtNPL.Columns.Add(dtColumnNPL[9]);
                    dtNPL.Columns.Add(dtColumnNPL[10]);
                    dtNPL.Columns.Add(dtColumnNPL[11]);
                    dtNPL.Columns.Add(dtColumnNPL[12]);
                    dtNPL.Columns.Add(dtColumnNPL[13]);
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in goodItem.GoodItemsCollection)
                    {
                        DataRow dr = dtNPL.NewRow();
                        if (item.LoaiHangHoa == 1)
                        {
                            dr["SoTaiKhoan"] = "152";
                        }
                        else
                        {
                            dr["SoTaiKhoan"] = "155";
                        }
                        dr["MaNPL"] = item.MaHangHoa;
                        dr["TenNPL"] = item.TenHangHoa;
                        dr["TenDVT"] = item.DVT;
                        dr["TonDauKy"] = item.TonDauKy;
                        dr["TriGiaTTTonDauKy"] = item.TonDauKy;
                        dr["LuongNhapTrongKy"] = item.NhapTrongKy;
                        dr["TriGiaTTNhapTrongKy"] = item.NhapTrongKy;
                        dr["LuongXuatTrongKy"] = item.XuatTrongKy;
                        dr["TriGiaTTXuatTrongKy"] = item.XuatTrongKy;
                        dr["TonCuoiKy"] = item.TonCuoiKy;
                        dr["TriGiaTTTonCuoiKy"] = item.TonCuoiKy;
                        dr["GhiChu"] = item.GhiChu;
                        dtNPL.Rows.Add(dr);
                    }
                    rp.dtTable = dtNPL;
                    rp.BindReport(true, dtpNgayChotTon.Value);
                    rp.ShowPreview();
                }
                else
                {
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in goodItem.GoodItemsCollection)
                    {
                        DataRow dr = dt.NewRow();
                        if (item.LoaiHangHoa == 1)
                        {
                            dr["SoTaiKhoan"] = "152";
                        }
                        else
                        {
                            dr["SoTaiKhoan"] = "155";
                        }
                        dr["MaNPL"] = item.MaHangHoa;
                        dr["TenNPL"] = item.TenHangHoa;
                        dr["TenDVT"] = item.DVT;
                        dr["TonDauKy"] = item.TonDauKy;
                        dr["TriGiaTTTonDauKy"] = item.TonDauKy;
                        dr["LuongNhapTrongKy"] = item.NhapTrongKy;
                        dr["TriGiaTTNhapTrongKy"] = item.NhapTrongKy;
                        dr["LuongXuatTrongKy"] = item.XuatTrongKy;
                        dr["TriGiaTTXuatTrongKy"] = item.XuatTrongKy;
                        dr["TonCuoiKy"] = item.TonCuoiKy;
                        dr["TriGiaTTTonCuoiKy"] = item.TonCuoiKy;
                        dt.Rows.Add(dr);
                    }
                    rp.dtTable = dt;
                    rp.BindReport(true, dtpNgayChotTon.Value);
                    rp.ShowPreview();
                }
                

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void btnMau15TriGia_Click(object sender, EventArgs e)
        {
            try
            {
                Mau15_TT38 rp = new Mau15_TT38();
                DataTable dt = new DataTable();
                dt = tbTong.Clone();
                if (!dt.Columns.Contains("SoTaiKhoan"))
                {
                    DataTable dtNPL = new DataTable();
                    DataColumn[] dtColumnNPL = new DataColumn[15];
                    dtColumnNPL[1] = new DataColumn("SoTaiKhoan", typeof(string));
                    dtColumnNPL[2] = new DataColumn("MaNPL", typeof(string));
                    dtColumnNPL[3] = new DataColumn("TenNPL", typeof(string));
                    dtColumnNPL[4] = new DataColumn("TenDVT", typeof(string));
                    dtColumnNPL[5] = new DataColumn("TonDauKy", typeof(decimal));
                    dtColumnNPL[6] = new DataColumn("TriGiaTTTonDauKy", typeof(decimal));
                    dtColumnNPL[7] = new DataColumn("LuongNhapTrongKy", typeof(decimal));
                    dtColumnNPL[8] = new DataColumn("TriGiaTTNhapTrongKy", typeof(decimal));
                    dtColumnNPL[9] = new DataColumn("LuongXuatTrongKy", typeof(decimal));
                    dtColumnNPL[10] = new DataColumn("TriGiaTTXuatTrongKy", typeof(decimal));
                    dtColumnNPL[11] = new DataColumn("TonCuoiKy", typeof(decimal));
                    dtColumnNPL[12] = new DataColumn("TriGiaTTTonCuoiKy", typeof(decimal));
                    dtColumnNPL[13] = new DataColumn("GhiChu", typeof(string));

                    dtNPL.Columns.Add(dtColumnNPL[1]);
                    dtNPL.Columns.Add(dtColumnNPL[2]);
                    dtNPL.Columns.Add(dtColumnNPL[3]);
                    dtNPL.Columns.Add(dtColumnNPL[4]);
                    dtNPL.Columns.Add(dtColumnNPL[5]);
                    dtNPL.Columns.Add(dtColumnNPL[6]);
                    dtNPL.Columns.Add(dtColumnNPL[7]);
                    dtNPL.Columns.Add(dtColumnNPL[8]);
                    dtNPL.Columns.Add(dtColumnNPL[9]);
                    dtNPL.Columns.Add(dtColumnNPL[10]);
                    dtNPL.Columns.Add(dtColumnNPL[11]);
                    dtNPL.Columns.Add(dtColumnNPL[12]);
                    dtNPL.Columns.Add(dtColumnNPL[13]);
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in goodItem.GoodItemsCollection)
                    {
                        DataRow dr = dtNPL.NewRow();
                        if (item.LoaiHangHoa == 1)
                        {
                            dr["SoTaiKhoan"] = "152";
                        }
                        else
                        {
                            dr["SoTaiKhoan"] = "155";
                        }
                        dr["MaNPL"] = item.MaHangHoa;
                        dr["TenNPL"] = item.TenHangHoa;
                        dr["TenDVT"] = item.DVT;
                        dr["TonDauKy"] = item.TonDauKy;
                        dr["TriGiaTTTonDauKy"] = item.TonDauKy;
                        dr["LuongNhapTrongKy"] = item.NhapTrongKy;
                        dr["TriGiaTTNhapTrongKy"] = item.NhapTrongKy;
                        dr["LuongXuatTrongKy"] = item.XuatTrongKy;
                        dr["TriGiaTTXuatTrongKy"] = item.XuatTrongKy;
                        dr["TonCuoiKy"] = item.TonCuoiKy;
                        dr["TriGiaTTTonCuoiKy"] = item.TonCuoiKy;
                        dr["GhiChu"] = item.GhiChu;
                        dtNPL.Rows.Add(dr);
                    }
                    rp.dtTable = dtNPL;
                    rp.BindReport(true, dtpNgayChotTon.Value);
                    rp.ShowPreview();
                }
                else
                {
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in goodItem.GoodItemsCollection)
                    {
                        DataRow dr = dt.NewRow();
                        if (item.LoaiHangHoa == 1)
                        {
                            dr["SoTaiKhoan"] = "152";
                        }
                        else
                        {
                            dr["SoTaiKhoan"] = "155";
                        }
                        dr["MaNPL"] = item.MaHangHoa;
                        dr["TenNPL"] = item.TenHangHoa;
                        dr["TenDVT"] = item.DVT;
                        dr["TonDauKy"] = item.TonDauKy;
                        dr["TriGiaTTTonDauKy"] = item.TonDauKy;
                        dr["LuongNhapTrongKy"] = item.NhapTrongKy;
                        dr["TriGiaTTNhapTrongKy"] = item.NhapTrongKy;
                        dr["LuongXuatTrongKy"] = item.XuatTrongKy;
                        dr["TriGiaTTXuatTrongKy"] = item.XuatTrongKy;
                        dr["TonCuoiKy"] = item.TonCuoiKy;
                        dr["TriGiaTTTonCuoiKy"] = item.TonCuoiKy;
                        dt.Rows.Add(dr);
                    }
                    rp.dtTable = dt;
                    rp.BindReport(false, dtpNgayChotTon.Value);
                    rp.ShowPreview();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            
        }

        private void toolStripChiTietNPL_Click(object sender, EventArgs e)
        {
            ExportExcel("ChiTietNPL_", dgList);
        }

        private void mnnTongNPL_Click(object sender, EventArgs e)
        {
            ExportExcel("TongNPL_", dgTongNPL);
        }

        private void mnuNPLTheoDM_Click(object sender, EventArgs e)
        {
            ExportExcel("ChiTietNPL_DM_", dgChiTietNPLXuat);
        }

        private void mnuTongNPLXuat_Click(object sender, EventArgs e)
        {
            ExportExcel("TongNPLXuat_", dgTongNPLXuat);
        }

        private void mnuNPL_CU_Click(object sender, EventArgs e)
        {
            ExportExcel("NPLCungUng_ChiTiet_", dgNPLCU);
        }

        private void mnuTongNPLCU_Click(object sender, EventArgs e)
        {
            ExportExcel("NPLCungUng_Tong_", dgNPLCU_Total);
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenFileDialog = new OpenFileDialog();

            OpenFileDialog.FileName = "";
            OpenFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                    + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                    + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

            OpenFileDialog.Multiselect = false;
            if (OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.FileInfo fin = new System.IO.FileInfo(OpenFileDialog.FileName);
                if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                  && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                  && fin.Extension.ToUpper() != ".gif".ToUpper()
                  && fin.Extension.ToUpper() != ".tiff".ToUpper()
                  && fin.Extension.ToUpper() != ".txt".ToUpper()
                  && fin.Extension.ToUpper() != ".xml".ToUpper()
                  && fin.Extension.ToUpper() != ".xsl".ToUpper()
                  && fin.Extension.ToUpper() != ".csv".ToUpper()
                  && fin.Extension.ToUpper() != ".doc".ToUpper()
                  && fin.Extension.ToUpper() != ".mdb".ToUpper()
                  && fin.Extension.ToUpper() != ".pdf".ToUpper()
                  && fin.Extension.ToUpper() != ".ppt".ToUpper()
                  && fin.Extension.ToUpper() != ".xls".ToUpper())
                {
                    ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                }
                else
                {
                    System.IO.FileStream fs = new System.IO.FileStream(OpenFileDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    size = 0;
                    size = fs.Length;
                    data = new byte[fs.Length];
                    fs.Read(data, 0, data.Length);
                    filebase64 = System.Convert.ToBase64String(data);
                    lblFile.Text = fin.Name;
                    goodItem.FileName = fin.Name;
                    goodItem.Content = filebase64;
                }

            }
        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send("Send");
                    break;
                case "cmdSendEdit":
                    Send("Edit");
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdImportExcel":
                    btnReadExcel_Click(null,null);
                    break;
                default:
                    break;
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtNamQT, errorProvider, "Năm quyết toán", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHinhBaoCao, errorProvider, "Loại hình báo cáo", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbDVT, errorProvider, "Đơn vị tính báo cáo", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void Save()
        {
            if (String.IsNullOrEmpty(lblFile.Text))
            {
                ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                return;
            }
            if (!ValidateForm(false))
                return;
            if (goodItem.GoodItemsCollection.Count < 0)
            {
                ShowMessage("Bạn chưa xử lý báo cáo ", false);
                return;
            }
            GetGoodItem();
            if (goodItem != null)
            {
                goodItemDetail.DeleteBy_GoodItem_ID(goodItem.ID);
            }
            goodItem.InsertUpdateFull();
            BindDataGoodItem();
            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            ShowMessage("Lưu thành công", false);
        }
        public void ProcessData()
        {
            DataTable dtBCXuatNhapTon = new DataTable();
            dtBCXuatNhapTon = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("NamThanhLy = " + txtNamQT.Text, "MaNPL").Tables[0];
            for (int i = 0; i < dtBCXuatNhapTon.Rows.Count; i++)
            {
                if (dtBCXuatNhapTon.Rows[i]["MaNPL"].ToString() == dtBCXuatNhapTon.Rows[i + 1]["MaNPL"].ToString())
                {
                    goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
                    goodItemDetail.STT = i;
                    goodItemDetail.LoaiHangHoa = 1;
                    goodItemDetail.TaiKhoan = 1;
                    goodItemDetail.MaHangHoa = dtBCXuatNhapTon.Rows[i]["MaNPL"].ToString();
                    goodItemDetail.TenHangHoa = dtBCXuatNhapTon.Rows[i]["TenNPL"].ToString();
                    goodItemDetail.DVT = dtBCXuatNhapTon.Rows[i]["TenDVT_NPL"].ToString();
                    goodItemDetail.TonDauKy = 0;
                    //goodItemDetail.NhapTrongKy = dtBCXuatNhapTon.Rows[i]["TenDVT_NPL"].ToString();
                }
            }
        }
        public void GetGoodItem()
        {
            goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            goodItem.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            goodItem.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
            goodItem.NamQuyetToan = Convert.ToDecimal(txtNamQT.Text);
            goodItem.LoaiHinhBaoCao = Convert.ToDecimal(cbbLoaiHinhBaoCao.SelectedValue.ToString());
            goodItem.DVTBaoCao = Convert.ToDecimal(cbbDVT.SelectedValue.ToString());
            goodItem.GhiChuKhac = txtGhiChuKhac.Text.ToString();
        }
        public void SetGoodItem()
        {
            string TrangThai = goodItem.TrangThaiXuLy.ToString();
            switch (TrangThai)
            {
                case "0":
                    lblTrangThaiXuLy.Text = "Chờ duyệt";
                    break;
                case "-1":
                    lblTrangThaiXuLy.Text = "Chưa khai báo";
                    break;
                case "1":
                    lblTrangThaiXuLy.Text = "Đã duyệt";
                    break;
                case "2":
                    lblTrangThaiXuLy.Text = "Không phê duyệt";
                    break;

            }
            txtSoTN.Text = goodItem.SoTN.ToString();
            dtpNgayTN.Value = goodItem.NgayTN;
            txtNamQT.Text = goodItem.NamQuyetToan.ToString();
            cbbDVT.SelectedValue = goodItem.DVTBaoCao.ToString();
            cbbLoaiHinhBaoCao.SelectedValue = goodItem.LoaiHinhBaoCao.ToString();
            txtGhiChuKhac.Text = goodItem.GhiChuKhac;
            lblFile.Text = goodItem.FileName;
            goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectCollectionDynamic("GoodItem_ID = " + goodItem.ID, " ");
        }
        public void GetDataGoodItemNew()
        {
            //for (int i = tbTong.Rows.Count - 1; i >= 0; i--)
            //{
            //    DataRow dr = tbTong.Rows[i];
            //    if (Convert.ToDecimal(dr["TonDauKy"].ToString()) == 0 && Convert.ToDecimal(dr["LuongNhapTrongKy"].ToString()) == 0 && Convert.ToDecimal(dr["LuongXuatTrongKy"].ToString()) == 0 && Convert.ToDecimal(dr["TonCuoiKy"]) == 0)
            //        dr.Delete();
            //}
            //goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP();
            goodItem.GoodItemsCollection.Clear();
            DataRow[] drTotal = new DataRow[tbTong.Rows.Count];
            tbTong.Rows.CopyTo(drTotal, 0);
            int k = 0;
            for (int i = 0; i < tbTong.Rows.Count; i++)
            {
                goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
                string SoTaiKhoan = drTotal[i]["SoTaiKhoan"].ToString();
                if (SoTaiKhoan == "152")
                {
                    goodItemDetail.LoaiHangHoa = 1;
                    goodItemDetail.TaiKhoan = 152;
                }
                else
                {
                    goodItemDetail.LoaiHangHoa = 2;
                    goodItemDetail.TaiKhoan = 155;
                }
                goodItemDetail.TenHangHoa = drTotal[i]["TenNPL"].ToString();
                goodItemDetail.MaHangHoa = drTotal[i]["MaNPL"].ToString();
                goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(drTotal[i]["TenDVT"].ToString()));
                decimal TonDauKy = 0;
                decimal NhapTrongKy = 0;
                decimal XuatTrongKy = 0;
                if (cbbDVT.SelectedValue.ToString() == "1")
                {
                    TonDauKy = Convert.ToDecimal(drTotal[i]["TriGiaTTTonDauKy"].ToString());
                    NhapTrongKy = Convert.ToDecimal(drTotal[i]["TriGiaTTNhapTrongKy"].ToString());
                    XuatTrongKy = Convert.ToDecimal(drTotal[i]["TriGiaTTXuatTrongKy"].ToString());
                    if (goodItemDetail.LoaiHangHoa == 1)
                    {
                        goodItemDetail.TonDauKy = 0;
                        goodItemDetail.NhapTrongKy = NhapTrongKy;
                        goodItemDetail.XuatTrongKy = 0;
                        goodItemDetail.TonCuoiKy = NhapTrongKy;
                    }
                    else
                    {
                        goodItemDetail.TonDauKy = TonDauKy;
                        goodItemDetail.NhapTrongKy = NhapTrongKy;
                        goodItemDetail.XuatTrongKy = XuatTrongKy;
                        goodItemDetail.TonCuoiKy = TonDauKy + NhapTrongKy - XuatTrongKy;
                    }

                }
                else
                {
                    TonDauKy = Convert.ToDecimal(drTotal[i]["TonDauKy"].ToString());
                    NhapTrongKy = Convert.ToDecimal(drTotal[i]["LuongNhapTrongKy"].ToString());
                    XuatTrongKy = Convert.ToDecimal(drTotal[i]["LuongXuatTrongKy"].ToString());
                    if (goodItemDetail.LoaiHangHoa == 1)
                    {
                        goodItemDetail.TonDauKy = 0;
                        goodItemDetail.NhapTrongKy = NhapTrongKy;
                        goodItemDetail.XuatTrongKy = 0;
                        goodItemDetail.TonCuoiKy = NhapTrongKy;
                    }
                    else
                    {
                        goodItemDetail.TonDauKy = TonDauKy;
                        goodItemDetail.NhapTrongKy = NhapTrongKy;
                        goodItemDetail.XuatTrongKy = XuatTrongKy;
                        goodItemDetail.TonCuoiKy = TonDauKy + NhapTrongKy - XuatTrongKy;
                    }
                }

                goodItemDetail.GhiChu = "";//dr["GHICHU"].ToString();
                if (TonDauKy == 0 && NhapTrongKy == 0 && XuatTrongKy == 0)
                {
                    k++;
                }
                else
                {
                    goodItemDetail.STT = i + 1 - k;
                    goodItem.GoodItemsCollection.Add(goodItemDetail);
                }
            }
            for (int i = 0; i < tbNPLTongXuat.Rows.Count; i++)
            {
                string MaNPL = tbNPLTongXuat.Rows[i]["MaNguyenPhuLieu"].ToString();
                decimal LuongXuat = Convert.ToDecimal(tbNPLTongXuat.Rows[i]["LuongNPLSuDungChung"].ToString());
                //decimal TriGiaXuat = Convert.ToDecimal(tbNPLTongXuat.Rows[i]["TriGiaXuat"].ToString());
                foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in goodItem.GoodItemsCollection)
                {
                    if (MaNPL.ToUpper() == item.MaHangHoa.ToUpper())
                    {
                        if (cbbDVT.SelectedValue.ToString() == "1")
                        {
                            //item.XuatTrongKy = TriGiaXuat;
                        }
                        else
                        {
                            item.XuatTrongKy = LuongXuat;
                            item.TonCuoiKy = item.NhapTrongKy - item.XuatTrongKy;
                        }
                        break;
                    }
                }
            }
        }
        public void GetDataGoodItem()
        {
            //for (int i = tbTong.Rows.Count - 1; i >= 0; i--)
            //{
            //    DataRow dr = tbTong.Rows[i];
            //    if (Convert.ToDecimal(dr["TonDauKy"].ToString()) == 0 && Convert.ToDecimal(dr["LuongNhapTrongKy"].ToString()) == 0 && Convert.ToDecimal(dr["LuongXuatTrongKy"].ToString()) == 0 && Convert.ToDecimal(dr["TonCuoiKy"]) == 0)
            //        dr.Delete();
            //}
            //goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP();
            goodItem.GoodItemsCollection.Clear();
            DataRow[] drTotal = new DataRow[tbTong.Rows.Count];
            tbTong.Rows.CopyTo(drTotal, 0);
            int k = 0;
            for (int i = 0; i < tbTong.Rows.Count; i++)
            {
                goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
                this.goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
                if (!(drTotal[i]["SoTaiKhoan"].ToString() == "152"))
                {
                    this.goodItemDetail.LoaiHangHoa = 2;
                    this.goodItemDetail.TaiKhoan = 155;
                }
                else
                {
                    this.goodItemDetail.LoaiHangHoa = 1;
                    this.goodItemDetail.TaiKhoan = 152;
                }
                //goodItemDetail.LoaiHangHoa = 1;
                //goodItemDetail.TaiKhoan = 152;
                goodItemDetail.TenHangHoa = drTotal[i]["TENNPL"].ToString();
                goodItemDetail.MaHangHoa = drTotal[i]["MANPL"].ToString();
                goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(drTotal[i]["TenDVT"].ToString()));
                decimal TonDauKy = 0;
                decimal NhapTrongKy = 0;
                decimal XuatTrongKy = 0;
                decimal TonCuoiKy = 0;
                if (cbbDVT.SelectedValue.ToString() == "2")
                {
                    //TonDauKy = Convert.ToDecimal(drTotal[i]["LUONGTONDK"].ToString());
                    //NhapTrongKy = Convert.ToDecimal(drTotal[i]["LUONGNHAPTK"].ToString());
                    //XuatTrongKy = Convert.ToDecimal(drTotal[i]["LUONGXUATTK"].ToString());
                    //TonCuoiKy = Convert.ToDecimal(drTotal[i]["LUONGTONCK"].ToString());

                    TonDauKy = Convert.ToDecimal(drTotal[i]["TonDauKy"].ToString());
                    NhapTrongKy = Convert.ToDecimal(drTotal[i]["LuongNhapTrongKy"].ToString());
                    XuatTrongKy = Convert.ToDecimal(drTotal[i]["LuongXuatTrongKy"].ToString());
                    //TonCuoiKy = Convert.ToDecimal(drTotal[i]["LUONGTONCK"].ToString());

                    goodItemDetail.TonDauKy = TonDauKy;
                    goodItemDetail.NhapTrongKy = NhapTrongKy;
                    goodItemDetail.XuatTrongKy = XuatTrongKy;
                    goodItemDetail.TonCuoiKy = TonDauKy + NhapTrongKy - XuatTrongKy;

                }
                else
                {
                    //TonDauKy = Convert.ToDecimal(drTotal[i]["TRIGIATONDK"].ToString());
                    //NhapTrongKy = Convert.ToDecimal(drTotal[i]["TRIGIANHAPTK"].ToString());
                    //XuatTrongKy = Convert.ToDecimal(drTotal[i]["TRIGIAXUATTK"].ToString());
                    //TonCuoiKy = Convert.ToDecimal(drTotal[i]["TRIGIATONCK"].ToString());

                    TonDauKy = Convert.ToDecimal(drTotal[i]["TriGiaTTTonDauKy"].ToString());
                    NhapTrongKy = Convert.ToDecimal(drTotal[i]["TriGiaTTNhapTrongKy"].ToString());
                    XuatTrongKy = Convert.ToDecimal(drTotal[i]["TriGiaTTXuatTrongKy"].ToString());
                    //TonCuoiKy = Convert.ToDecimal(drTotal[i]["TRIGIATONCK"].ToString());

                    goodItemDetail.TonDauKy = TonDauKy;
                    goodItemDetail.NhapTrongKy = NhapTrongKy;
                    goodItemDetail.XuatTrongKy = XuatTrongKy;
                    goodItemDetail.TonCuoiKy = TonDauKy + NhapTrongKy - XuatTrongKy;
                }

                goodItemDetail.GhiChu = "";//dr["GHICHU"].ToString();
                if (TonDauKy == 0 && NhapTrongKy == 0 && XuatTrongKy == 0)
                {
                    k++;
                }
                else
                {
                    goodItemDetail.STT = i + 1 - k ;
                    goodItem.GoodItemsCollection.Add(goodItemDetail);  
                }
            }
            for (int i = 0; i < tbNPLTongXuat.Rows.Count; i++)
            {
                string MaNPL = tbNPLTongXuat.Rows[i]["MaNguyenPhuLieu"].ToString();
                decimal LuongXuat = Convert.ToDecimal(tbNPLTongXuat.Rows[i]["LuongNPLSuDungChung"].ToString());
                //decimal TriGiaXuat = Convert.ToDecimal(tbNPLTongXuat.Rows[i]["TriGiaXuat"].ToString());
                foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in goodItem.GoodItemsCollection)
                {
                    if (MaNPL.ToUpper() == item.MaHangHoa.ToUpper())
                    {
                        if (cbbDVT.SelectedValue.ToString() == "1")
                        {
                           // item.XuatTrongKy = TriGiaXuat;
                        }
                        else
                        {
                            item.XuatTrongKy = LuongXuat;
                            item.TonCuoiKy = item.NhapTrongKy - item.XuatTrongKy;
                        }
                        break;
                    }
                }
            }
        }
        public void Send(string Status)
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (goodItem.ID == 0)
                {
                    ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    goodItem = KDT_VNACCS_BaoCaoQuyetToan_NPLSP.Load(goodItem.ID);
                    goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectCollectionDynamic("GoodItem_ID = " + goodItem.ID, " ");
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                sendXML.master_id = goodItem.ID;

                if (sendXML.Load())
                {
                    if (Status == "Send")
                    {
                        if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            return;
                        }
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    goodItem.GuidStr = Guid.NewGuid().ToString();
                    GoodsItems_VNACCS goodsItem_VNACCS = new GoodsItems_VNACCS();
                    goodsItem_VNACCS = Mapper_V4.ToDataTransferGoodsItem(goodItem, goodItemDetail, GlobalSettings.DIA_CHI, GlobalSettings.TEN_DON_VI, Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = goodItem.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ)),
                              Identity = goodItem.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = goodsItem_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = goodItem.GuidStr,
                          },
                          goodsItem_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                        sendXML.master_id = goodItem.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        goodItem.Update();
                        Feedback();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = goodItem.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.GoodsItem,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.GoodsItem,
                };
                subjectBase.Type = DeclarationIssuer.GoodsItem;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = goodItem.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ.Trim())),
                                                  Identity = goodItem.MaHQ
                                              }, subjectBase, null);
                if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
        }
        public void Result()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = goodItem.ID;
            form.DeclarationIssuer = DeclarationIssuer.GoodsItem;
            form.ShowDialog(this);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.GoodItemSendHandler(goodItem, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void btnReadExcel_Click(object sender, EventArgs e)
        {
            goodItem.GoodItemsCollection.Clear();
            IsReadExcel = true;
            //New Update
            //ReadExcelFormNPL f = new ReadExcelFormNPL();
            ReadExcelFormNPLSP f = new ReadExcelFormNPLSP();
            f.goodItem = goodItem;
            f.ShowDialog(this);
            BindDataGoodItem();
        }

        private void uiTab1_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {

        }

        private void mnuExportBCQT_Click(object sender, EventArgs e)
        {
            ExportExcel("TỔNG HỢP SỐ LIỆU BÁO CÁO QUYẾT TOÁN ", dgListTotal);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            ExportExcel("TỔNG HỢP SỐ LIỆU CHI TIẾT XUẤT NPL TRONG KỲ ", dgListNPLXuatDetails);
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
                    dtTotal = new DataTable();
                    DataColumn dtColMANPL = new DataColumn("MANPL");
                    dtColMANPL.DataType = typeof(System.String);
                    DataColumn dtColTENNPL = new DataColumn("TENNPL");
                    dtColTENNPL.DataType = typeof(System.String);
                    DataColumn dtColDVT = new DataColumn("DVT");
                    dtColDVT.DataType = typeof(System.String);
                    DataColumn dtColLUONGTONDK = new DataColumn("LUONGTONDK");
                    dtColLUONGTONDK.DataType = typeof(System.Decimal);
                    DataColumn dtColTRIGIATONDK = new DataColumn("TRIGIATONDK");
                    dtColTRIGIATONDK.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGNHAPTK = new DataColumn("LUONGNHAPTK");
                    dtColLUONGNHAPTK.DataType = typeof(System.Decimal);
                    DataColumn dtColTRIGIANHAPTK = new DataColumn("TRIGIANHAPTK");
                    dtColTRIGIANHAPTK.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGXUATTK = new DataColumn("LUONGXUATTK");
                    dtColLUONGXUATTK.DataType = typeof(System.Decimal);
                    DataColumn dtColTRIGIAXUATTK = new DataColumn("TRIGIAXUATTK");
                    dtColTRIGIAXUATTK.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGTONCK = new DataColumn("LUONGTONCK");
                    dtColLUONGTONCK.DataType = typeof(System.Decimal);
                    DataColumn dtColTRIGIATONCK = new DataColumn("TRIGIATONCK");
                    dtColTRIGIATONCK.DataType = typeof(System.Decimal);
                    DataColumn dtColNAMQUYETTOAN = new DataColumn("NAMQUYETTOAN");
                    dtColNAMQUYETTOAN.DataType = typeof(System.Decimal);

                    dtTotal.Columns.Add(dtColMANPL);
                    dtTotal.Columns.Add(dtColTENNPL);
                    dtTotal.Columns.Add(dtColDVT);
                    dtTotal.Columns.Add(dtColLUONGTONDK);
                    dtTotal.Columns.Add(dtColTRIGIATONDK);
                    dtTotal.Columns.Add(dtColLUONGNHAPTK);
                    dtTotal.Columns.Add(dtColTRIGIANHAPTK);
                    dtTotal.Columns.Add(dtColLUONGXUATTK);
                    dtTotal.Columns.Add(dtColTRIGIAXUATTK);
                    dtTotal.Columns.Add(dtColLUONGTONCK);
                    dtTotal.Columns.Add(dtColTRIGIATONCK);
                    dtTotal.Columns.Add(dtColNAMQUYETTOAN);

                    DataTable dtbNPLQTOld= KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectDynamic("","").Tables[0];
                    DataTable dtbNPLQuyeToan = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectDynamic("NAMQUYETTOAN =" + NamQuyetToan, "MANPL").Tables[0];
                    if (dtbNPLQTOld.Rows.Count == 0)
                    {
                        // Lần đầu tiên thì tính toán số liệu tồn đầu kỳ
                        DataTable dtNPLTon = NPLNhapTon.SelectTotalNPLTonByYear(NamQuyetToan).Tables[0];

                        foreach (DataRow dr in dtNPLTon.Rows)
                        {
                            try
                            {

                                DataRow drNPLTon = dtTotal.NewRow();
                                drNPLTon["MANPL"] = dr["MANPL"];
                                drNPLTon["TENNPL"] = dr["TENNPL"].ToString();
                                drNPLTon["DVT"] = dr["DVT"].ToString();
                                drNPLTon["LUONGTONDK"] = dr["LUONGTONDK"];
                                drNPLTon["TRIGIATONDK"] = dr["TRIGIATONDK"];
                                drNPLTon["LUONGNHAPTK"] = 0;
                                drNPLTon["TRIGIANHAPTK"] = 0;
                                drNPLTon["LUONGXUATTK"] = 0;
                                drNPLTon["TRIGIAXUATTK"] = 0;
                                drNPLTon["LUONGTONCK"] = 0;
                                drNPLTon["TRIGIATONCK"] = 0;
                                drNPLTon["NAMQUYETTOAN"] = NamQuyetToan;
                                dtTotal.Rows.Add(drNPLTon);
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                    else
                    {
                        foreach (DataRow dr in dtbNPLQuyeToan.Rows)
                        {
                            try
                            {

                                DataRow drNPLTon = dtTotal.NewRow();
                                drNPLTon["MANPL"] = dr["MANPL"];
                                drNPLTon["TENNPL"] = dr["TENNPL"].ToString();
                                drNPLTon["DVT"] = dr["DVT"].ToString();
                                //Gán lượng tồn cuối kỳ năm QT trước cho tồn đầu kỳ năm QT này
                                drNPLTon["LUONGTONDK"] = drNPLTon["LUONGTONCK"];
                                drNPLTon["TRIGIATONDK"] = drNPLTon["TRIGIATONCK"];
                                drNPLTon["LUONGNHAPTK"] = 0;
                                drNPLTon["TRIGIANHAPTK"] = 0;
                                drNPLTon["LUONGXUATTK"] = 0;
                                drNPLTon["TRIGIAXUATTK"] = 0;
                                drNPLTon["LUONGTONCK"] = 0;
                                drNPLTon["TRIGIATONCK"] = 0;
                                drNPLTon["NAMQUYETTOAN"] = NamQuyetToan;
                                dtTotal.Rows.Add(drNPLTon);
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
            //Lấy thông tin về tổng nhập và xuất của nam báo cáo
            List<SXXK_NguyenPhuLieu_Ton> NPLTonCollection = SXXK_NguyenPhuLieu_Ton.SelectCollectionAll();

            //Lấy lượng tồn cuối kỳ của năm trước 
            string MaNPLTemp = "";
            string MaNPL= "";

            List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail> NPLQTCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectCollectionAll();
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in NPLQTCollection)
            {
                NPLTemp = item.MaHangHoa;
                foreach (SXXK_NguyenPhuLieu_Ton NPL in NPLTonCollection)
                {
                    MaNPL = NPL.Ma;
                    if (NPLTemp==MaNPL)
                    {

                    }
                }
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string path = Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                string fileName = "";
                System.IO.FileStream fs;
                byte[] bytes;
                if (String.IsNullOrEmpty(goodItem.FileName))
                {
                    ShowMessage("Bạn chưa chọn File hoặc Không tồn tại File để xem.", false);
                    return;
                }
                fileName = path + "\\" + goodItem.FileName;
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                bytes = System.Convert.FromBase64String(goodItem.Content);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
                System.Diagnostics.Process.Start(fileName);
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}