﻿namespace Company.Interface.Feedback
{
    partial class LinhVucCauHoi
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.officeFormAdorner1 = new Janus.Windows.Ribbon.OfficeFormAdorner(this.components);
            this.trlLinhVuc = new DevExpress.XtraTreeList.TreeList();
            this.treeListColum = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.officeFormAdorner1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trlLinhVuc)).BeginInit();
            this.SuspendLayout();
            // 
            // officeFormAdorner1
            // 
            this.officeFormAdorner1.Office2007CustomColor = System.Drawing.Color.Empty;
            // 
            // trlLinhVuc
            // 
            this.trlLinhVuc.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColum});
            this.trlLinhVuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trlLinhVuc.Location = new System.Drawing.Point(0, 0);
            this.trlLinhVuc.Name = "trlLinhVuc";
            this.trlLinhVuc.OptionsBehavior.Editable = false;
            this.trlLinhVuc.OptionsView.ShowColumns = false;
            this.trlLinhVuc.OptionsView.ShowIndicator = false;
            this.trlLinhVuc.ParentFieldName = "";
            this.trlLinhVuc.ShowButtonMode = DevExpress.XtraTreeList.ShowButtonModeEnum.ShowForFocusedRow;
            this.trlLinhVuc.Size = new System.Drawing.Size(420, 356);
            this.trlLinhVuc.TabIndex = 0;
            this.trlLinhVuc.AfterFocusNode += new DevExpress.XtraTreeList.NodeEventHandler(this.trlLinhVuc_AfterFocusNode);
            // 
            // treeListColum
            // 
            this.treeListColum.Caption = "Lĩnh vực";
            this.treeListColum.FieldName = "Lĩnh vực";
            this.treeListColum.Name = "treeListColum";
            this.treeListColum.OptionsColumn.AllowEdit = false;
            this.treeListColum.OptionsColumn.AllowMove = false;
            this.treeListColum.Visible = true;
            this.treeListColum.VisibleIndex = 0;
            // 
            // LinhVucCauHoi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.trlLinhVuc);
            this.Name = "LinhVucCauHoi";
            this.Size = new System.Drawing.Size(420, 356);
            this.Load += new System.EventHandler(this.LinhVucCauHoi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.officeFormAdorner1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trlLinhVuc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.Ribbon.OfficeFormAdorner officeFormAdorner1;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private DevExpress.XtraTreeList.TreeList trlLinhVuc;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColum;

    }
}
