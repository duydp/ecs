﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Threading;
using IWshRuntimeLibrary;
namespace CreateUpdateFile
{
    public partial class FrmCreateUpdateFile : Form
    {
        XmlDocument doc;
        private string pathConfigXml = Application.StartupPath + "\\Config.xml";
        private string DateFormat = "yyyy-MM-dd hh:mm:ss tt";

        public FrmCreateUpdateFile()
        {
            InitializeComponent();
        }

        private void FrmCreateUpdateFile_Load(object sender, EventArgs e)
        {
            folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;
            LoadMakeConfig();
            btnClientSave.Enabled = false;
            CreateShorcut();
        }
        private void CreateShorcut()
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + Application.ProductName.Replace("v4", "V5") + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
        private DataSet LoadDataConfig(bool isLoadExists)
        {
            try
            {
                if (System.IO.File.Exists(pathConfigXml))
                {
                    dsConfig.Clear();
                    dsConfig.ReadXml(pathConfigXml);
                }
                else
                {
                    #region Create data

                    string uri = "http://ecs.softech.cloud/autoupdate/SOFTECH_ECS_TQDT_{0}";
                    string title = "Cập nhật phiên bản {0}.0 phần mềm Softech";
                    string header = "Phiên bản mới '[x]' bổ sung nâng cấp:";
                    string footer = "-------------------------------------------------------------------------------------------------------------------------\r\nCông ty CP Softech - 38 Yên Bái, Quận Hải Châu, Thành Phố Đà Nẵng (Tòa nhà VNPT)\r\nSố điện thoại: 0236.3.779.779\r\n";


                    //SOFTECH_ECS_TQDT_KD V4.0
                    DataRow rowNew = dtConfig.NewRow();
                    rowNew["Group"] = "ECS_TQDT_KD V4.0";
                    rowNew["Url"] = string.Format(uri, "KD_V4");
                    rowNew["Title"] = string.Format(title, 4);
                    rowNew["Version"] = 4;
                    rowNew["Header"] = header;
                    rowNew["Description"] = "";
                    rowNew["Footer"] = footer;
                    rowNew["Path"] = "?";
                    rowNew["Compare"] = rowNew["Url"] + "/ListUpdate.xml";
                    rowNew["DateRelease"] = new DateTime(1900, 01, 01);
                    dtConfig.Rows.Add(rowNew);

                    //SOFTECH_ECS_TQDT_GC V4.0
                    rowNew = dtConfig.NewRow();
                    rowNew["Group"] = "ECS_TQDT_GC V4.0";
                    rowNew["Url"] = string.Format(uri, "GC_V4");
                    rowNew["Title"] = string.Format(title, 4);
                    rowNew["Version"] = 4;
                    rowNew["Header"] = header;
                    rowNew["Description"] = "";
                    rowNew["Footer"] = footer;
                    rowNew["Path"] = "?";
                    rowNew["Compare"] = rowNew["Url"] + "/ListUpdate.xml";
                    rowNew["DateRelease"] = new DateTime(1900, 01, 01);
                    dtConfig.Rows.Add(rowNew);

                    //SOFTECH_ECS_TQDT_SXXK V4.0
                    rowNew = dtConfig.NewRow();
                    rowNew["Group"] = "ECS_TQDT_SXXK V4.0";
                    rowNew["Url"] = string.Format(uri, "SXXK_V4");
                    rowNew["Title"] = string.Format(title, 4);
                    rowNew["Version"] = 4;
                    rowNew["Header"] = header;
                    rowNew["Description"] = "";
                    rowNew["Footer"] = footer;
                    rowNew["Path"] = "?";
                    rowNew["Compare"] = rowNew["Url"] + "/ListUpdate.xml";
                    rowNew["DateRelease"] = new DateTime(1900, 01, 01);
                    dtConfig.Rows.Add(rowNew);

                    #endregion

                    dsConfig.WriteXml(pathConfigXml);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show(ex.Message);

                return null;
            }

            return dsConfig;
        }

        private void BindDataToCombo(DataSet ds)
        {
            try
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    cboGroup.DataSource = ds.Tables[0];
                    cboGroup.DisplayMember = "Group";
                }

                cboGroup_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadMakeConfig()
        {
            try
            {
                txtGroup.Text = MakeConfig.Install.Group;
                numVersion.Value = int.Parse(MakeConfig.Install.Version);
                txtUrl.Text = MakeConfig.Install.Url;
                txtTitle.Text = MakeConfig.Install.Title;

                txtHeader.Text = MakeConfig.Install.Header;
                txtDescription.Text = MakeConfig.Install.Description.Replace("\n", "\r\n");
                txtFooter.Text = MakeConfig.Install.Footer;

                txtFolderDatabase.Text = MakeConfig.Install.FolderDatabase;
                txtFileNeedRestart.Text = MakeConfig.Install.NeedRestartFile.ToString();
                txtClientFolderPath.Text = MakeConfig.Install.ClientPath;
                if (txtClientFolderPath.Text != "")
                    folderBrowserDialog1.SelectedPath = txtClientFolderPath.Text;

                LoadDataConfig(true);

                BindDataToCombo(dsConfig);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show(ex.Message);
            }
        }

        #region Server

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Client

        private void btnClientOpen_Click(object sender, EventArgs e)
        {
            if (txtClientFolderPath.Text != "")
                folderBrowserDialog1.SelectedPath = txtClientFolderPath.Text;
            if (DialogResult.OK == folderBrowserDialog1.ShowDialog())
            {
                txtClientFolderPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        int count = 0;
        private void DoWork(object obj)
        {
            try
            {
                isRunning = true;
                MakeFiles msk = new MakeFiles();
                msk.ZipHandler += new EventHandler<ZipEventArgs>(msk_ZipHandler);
                msk.GenerateFormatXml(txtClientFolderPath.Text, txtPathCompare.Text, chkClientZip.Checked, ref count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            isRunning = false;
        }

        void msk_ZipHandler(object sender, ZipEventArgs e)
        {
            this.Invoke(
                new EventHandler<ZipEventArgs>(ViewProcess),
                sender, e);
        }
        void ViewProcess(object sender, ZipEventArgs e)
        {
            try
            {
                label9.Text = e.FileName;
                progressBar1.Value = e.Percent;

                if (e.Percent == 100)
                {
                    doc = new XmlDocument();
                    doc.LoadXml(e.XmlContent);
                    AddColouredText(rtxClientListFile, e.XmlContent);
                    btnClientSave.Enabled = rtxClientListFile.Lines.Length > 0;
                    lblClientCount.Text = string.Format("Tổng số file(s): {0}", count);
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        bool isRunning = false;
        private void btnClientExecute_Click(object sender, EventArgs e)
        {
            if (!isRunning)
            {
                ThreadPool.QueueUserWorkItem(DoWork);
            }
            else MessageBox.Show("Tiến trình đang thực hiện", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void AddColouredText(RichTextBox richTextBox1, string strTextToAdd)
        {
            //Use the RichTextBox to create the initial RTF code
            richTextBox1.Clear();
            richTextBox1.AppendText(strTextToAdd);
            string strRTF = richTextBox1.Rtf;
            richTextBox1.Clear();

            /* 
             * ADD COLOUR TABLE TO THE HEADER FIRST 
             * */

            // Search for colour table info, if it exists (which it shouldn't)
            // remove it and replace with our one
            int iCTableStart = strRTF.IndexOf("colortbl;");

            if (iCTableStart != -1) //then colortbl exists
            {
                //find end of colortbl tab by searching
                //forward from the colortbl tab itself
                int iCTableEnd = strRTF.IndexOf('}', iCTableStart);
                strRTF = strRTF.Remove(iCTableStart, iCTableEnd - iCTableStart);

                //now insert new colour table at index of old colortbl tag
                strRTF = strRTF.Insert(iCTableStart,
                    // CHANGE THIS STRING TO ALTER COLOUR TABLE
                    "colortbl ;\\red255\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue255;}");
            }

            //colour table doesn't exist yet, so let's make one
            else
            {
                // find index of start of header
                int iRTFLoc = strRTF.IndexOf("\\rtf");
                // get index of where we'll insert the colour table
                // try finding opening bracket of first property of header first                
                int iInsertLoc = strRTF.IndexOf('{', iRTFLoc);

                // if there is no property, we'll insert colour table
                // just before the end bracket of the header
                if (iInsertLoc == -1) iInsertLoc = strRTF.IndexOf('}', iRTFLoc) - 1;

                // insert the colour table at our chosen location                
                strRTF = strRTF.Insert(iInsertLoc,
                    // CHANGE THIS STRING TO ALTER COLOUR TABLE
                    "{\\colortbl ;\\red128\\green0\\blue0;\\red0\\green128\\blue0;\\red0\\green0\\blue255;}");
            }

            /*
             * NOW PARSE THROUGH RTF DATA, ADDING RTF COLOUR TAGS WHERE WE WANT THEM
             * In our colour table we defined:
             * cf1 = red  
             * cf2 = green
             * cf3 = blue             
             * */

            for (int i = 0; i < strRTF.Length; i++)
            {
                if (strRTF[i] == '<')
                {
                    //add RTF tags after symbol 
                    //Check for comments tags 
                    if (strRTF[i + 1] == '!')
                        strRTF = strRTF.Insert(i + 4, "\\cf2 ");
                    else
                        strRTF = strRTF.Insert(i + 1, "\\cf1 ");
                    //add RTF before symbol
                    strRTF = strRTF.Insert(i, "\\cf3 ");

                    //skip forward past the characters we've just added
                    //to avoid getting trapped in the loop
                    i += 6;
                }
                else if (strRTF[i] == '>')
                {
                    //add RTF tags after character
                    strRTF = strRTF.Insert(i + 1, "\\cf0 ");
                    //Check for comments tags
                    if (strRTF[i - 1] == '-')
                    {
                        strRTF = strRTF.Insert(i - 2, "\\cf3 ");
                        //skip forward past the 6 characters we've just added
                        i += 8;
                    }
                    else
                    {
                        strRTF = strRTF.Insert(i, "\\cf3 ");
                        //skip forward past the 6 characters we've just added
                        i += 6;
                    }
                }
            }
            richTextBox1.Rtf = strRTF;
        }
        private void btnClientSave_Click(object sender, EventArgs e)
        {
            //AutoUpdate.AutoUpdate.DeleteFile(txtClientFolderPath.Text + "\\Zip", "*.*");

            //if (chkClientZip.Checked)
            //    AutoUpdate.AutoUpdate.CreateZipFile(txtClientFolderPath.Text, Application.StartupPath, rtxClientListFile.Lines);

            //rtxClientListFile.SaveFile(txtClientFolderPath.Text + "\\Zip\\Update.txt", RichTextBoxStreamType.PlainText);
            //doc = new XmlDocument();
            //doc.Load(rtxClientListFile.Text);
            doc.Save(AppDomain.CurrentDomain.BaseDirectory + "ZIP\\ListUpdate.xml");
            MakeConfig.Install.ClientPath = txtClientFolderPath.Text;
            MakeConfig.Install.DateRelease = DateTime.Now.ToString(DateFormat);
            MakeConfig.Save();

            DataRow[] results = dtConfig.Select("Group = '" + ConfigInfo.Group + "'");
            if (results.Length != 0)
            {
                cboGroup.SelectedIndexChanged -= new EventHandler(cboGroup_SelectedIndexChanged);

                results[0]["Group"] = txtGroup.Text;
                results[0]["Version"] = numVersion.Value.ToString();
                results[0]["Url"] = txtUrl.Text;
                results[0]["Title"] = txtTitle.Text;
                results[0]["Description"] = txtDescription.Text;
                results[0]["Footer"] = txtFooter.Text;
                results[0]["Path"] = txtClientFolderPath.Text;
                results[0]["Compare"] = txtPathCompare.Text;
                results[0]["DateRelease"] = DateTime.Now.ToString(DateFormat);

                cboGroup.SelectedIndexChanged += new EventHandler(cboGroup_SelectedIndexChanged);
            }

            dsConfig.WriteXml(pathConfigXml);

            MessageBox.Show("Lưu file thành công.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnClientClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        private void btnConfigSave_Click(object sender, EventArgs e)
        {
            MakeConfig.Install.Group = txtGroup.Text;
            MakeConfig.Install.Version = numVersion.Value.ToString();
            MakeConfig.Install.Url = txtUrl.Text;
            //MakeConfig.Install.Header = txtHeader.Text; //khong cap nhat noi dung nay. Thong tin nay se tu dong cap nhat lai sau khi gen file.
            MakeConfig.Install.Description = txtDescription.Text.Replace("&", "và");
            MakeConfig.Install.Footer = txtFooter.Text;
            MakeConfig.Install.Title = txtTitle.Text;
            MakeConfig.Install.FolderDatabase = txtFolderDatabase.Text;
            MakeConfig.Install.NeedRestartFile.Create(txtFileNeedRestart.Text);
            MakeConfig.Install.ClientPath = txtClientFolderPath.Text;
            MakeConfig.Install.DateRelease = DateTime.Now.ToString(DateFormat);

            DataRow[] results = dtConfig.Select("Group = '" + txtGroup.Text + "'");
            if (results.Length != 0)
            {
                cboGroup.SelectedIndexChanged -= new EventHandler(cboGroup_SelectedIndexChanged);

                results[0]["Group"] = txtGroup.Text;
                results[0]["Version"] = numVersion.Value.ToString();
                results[0]["Url"] = txtUrl.Text;
                results[0]["Title"] = txtTitle.Text;
                //results[0]["Header"] = txtHeader.Text;
                results[0]["Description"] = txtDescription.Text;
                results[0]["Footer"] = txtFooter.Text;
                results[0]["Path"] = txtClientFolderPath.Text;
                results[0]["Compare"] = txtPathCompare.Text;
                results[0]["DateRelease"] = DateTime.Now.ToString(DateFormat);

                cboGroup.SelectedIndexChanged += new EventHandler(cboGroup_SelectedIndexChanged);
            }
            else
            {
                #region Create data

                string title = "Cập nhật phiên bản ?";
                string header = "Phiên bản mới '[x]' bổ sung nâng cấp:";
                string footer = "-------------------------------------------------------------------------------------------------------------------------\r\nCông ty CP Softech - 15 Quang Trung - Tp. Đà Nẵng\r\nSố điện thoại: 0511 3 840888\r\n";

                //SOFTECH_ECS_TQDT_KD v2.0
                DataRow rowNew = dtConfig.NewRow();
                rowNew["Group"] = txtGroup.Text;
                rowNew["Version"] = numVersion.Value.ToString();
                rowNew["Url"] = txtUrl.Text;
                rowNew["Title"] = "?";
                rowNew["Header"] = header;
                rowNew["Description"] = txtDescription.Text;
                rowNew["Footer"] = footer;
                rowNew["Path"] = txtClientFolderPath.Text;
                rowNew["Compare"] = rowNew["Url"] + "/ListUpdate.xml";
                rowNew["DateRelease"] = new DateTime(1900, 01, 01);
                dtConfig.Rows.Add(rowNew);

                #endregion
            }

            dsConfig.WriteXml(pathConfigXml);

            if (MakeConfig.Save())
                MessageBox.Show("Lưu cấu hình thành công", "Thông báo", MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
            else
                MessageBox.Show("Lưu cấu hình không thành công", "Thông báo", MessageBoxButtons.OK,
                     MessageBoxIcon.Error);

        }

        private void btnOpenfolder_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();

            process.StartInfo.UseShellExecute = true;

            process.StartInfo.FileName = @"explorer";

            if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Zip"))
                process.StartInfo.Arguments = AppDomain.CurrentDomain.BaseDirectory + "Zip";
            else process.StartInfo.Arguments = AppDomain.CurrentDomain.BaseDirectory;

            process.Start();
        }

        private void btnCheckLink_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();

                process.StartInfo.UseShellExecute = true;

                process.StartInfo.FileName = @"explorer";

                if (!string.IsNullOrEmpty(txtPathCompare.Text))
                    process.StartInfo.Arguments = txtPathCompare.Text;

                process.Start();
            }
            catch { }
        }

        private void cboGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboGroup.SelectedValue == null)
                    return;

                string itemSelected = cboGroup.SelectedValue.ToString();

                System.Data.DataRowView drv = (System.Data.DataRowView)cboGroup.SelectedValue;

                ConfigInfo.Group = drv.Row["Group"].ToString();
                ConfigInfo.Version = drv.Row["Version"].ToString();
                ConfigInfo.Url = drv.Row["Url"].ToString();
                ConfigInfo.Title = drv.Row["Title"].ToString();
                ConfigInfo.Header = drv.Row["Header"].ToString();
                ConfigInfo.Description = drv.Row["Description"].ToString();
                ConfigInfo.Footer = drv.Row["Footer"].ToString();
                ConfigInfo.Path = drv.Row["Path"].ToString();
                ConfigInfo.Compare = drv.Row["Compare"].ToString();
                ConfigInfo.DateRelease = DateTime.Now.ToString(DateFormat);

                txtGroup.Text = ConfigInfo.Group;
                numVersion.Value = string.IsNullOrEmpty(ConfigInfo.Version) ? 1: int.Parse(ConfigInfo.Version);
                txtClientFolderPath.Text = ConfigInfo.Path;
                txtPathCompare.Text = ConfigInfo.Compare;

                txtUrl.Text = ConfigInfo.Url;
                txtTitle.Text = ConfigInfo.Title;
                txtHeader.Text = ConfigInfo.Header;
                txtDescription.Text = ConfigInfo.Description;
                txtFooter.Text = ConfigInfo.Footer;

                UpdateVersionInHeader();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show(ex.Message);
            }
        }

        public struct ConfigInfo
        {
            public static string Group;
            public static string Version;
            public static string Url;
            public static string Title;
            public static string Header;
            public static string Description;
            public static string Footer;
            public static string Path;
            public static string Compare;
            public static string DateRelease;
        }

        private void txtClientFolderPath_TextChanged(object sender, EventArgs e)
        {
            if (txtClientFolderPath.Text.Length > 0)
                MakeConfig.Install.ClientPath = txtClientFolderPath.Text;

            UpdateVersionInHeader();
        }

        private void UpdateVersionInHeader()
        {
            try
            {
                errorProvider1.SetError(txtHeader, "");
                if (MakeConfig.Install.ClientPath.Contains("?") || MakeConfig.Install.ClientPath.Length == 0)
                {
                    errorProvider1.SetError(txtHeader, "Thiếu thông tin đường dẫn trong Tab 'Tạo tệp tin update'.");
                    txtHeader.Focus();
                    return;
                }

                string fileVersion = GetVersionExecuteFile();

                if (fileVersion != "")
                {
                    ConfigInfo.Header = string.Format("Phiên bản mới '{0}' bổ sung nâng cấp:", fileVersion + string.Format(" ({0})", DateTime.Now.ToString("dd/MM/yyyy hh:mm tt")));

                    txtHeader.Text = ConfigInfo.Header;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private string GetVersionExecuteFile()
        {
            foreach (FileName item in MakeConfig.Install.NeedRestartFile)
            {
                string pathFile = MakeConfig.Install.ClientPath + "\\" + item.Name;

                if (System.IO.File.Exists(pathFile))
                {
                    System.Diagnostics.FileVersionInfo fv = System.Diagnostics.FileVersionInfo.GetVersionInfo(pathFile);

                    return fv != null ? fv.FileVersion : "";
                }
            }

            return "";
        }

        private void txtHeader_TextChanged(object sender, EventArgs e)
        {
            MakeConfig.Install.Header = txtHeader.Text;
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            MakeConfig.Install.Description = txtDescription.Text;
        }

        private void txtFooter_TextChanged(object sender, EventArgs e)
        {
            MakeConfig.Install.Footer = txtFooter.Text;
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            txtGroup.Text = "";
            numVersion.Value = 1;
            txtClientFolderPath.Text = "";
            txtPathCompare.Text = "";

            txtUrl.Text = "";
            txtTitle.Text = "";
            txtHeader.Text = "";
            txtDescription.Text = "";
            txtFooter.Text = "";
        }

        private void btnTaoDuLieuMacDinh_Click(object sender, EventArgs e)
        {
            try
            {
                LoadDataConfig(false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void linkGetContentFromServer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //Lay noi dung tu server
                string pathSourceCompare = txtPathCompare.Text;
                XmlDocument docSource = null;
                string contentServer = "";

                if (!string.IsNullOrEmpty(pathSourceCompare))
                {
                    docSource = new XmlDocument();
                    docSource.Load(pathSourceCompare);
                    XmlNode nodeDescription = docSource.DocumentElement.FirstChild;

                    contentServer = nodeDescription.InnerText;

                    //Loai bo phan chan noi dung
                    int lenFooter = txtFooter.Text.Trim().Length;
                    contentServer = contentServer.Trim().Substring(0, contentServer.Trim().Length - lenFooter);

                    //Bo sung them vao cuoi noi dung moi
                    //StringBuilder sb = new StringBuilder();
                    //sb.AppendLine(MakeConfig.Install.Description);
                    //sb.AppendLine();
                    //sb.AppendLine("-------------------------------------------------------------------------------------------------------------------------");
                    //sb.Append(contentServer);

                    txtDescription.Text = "";
                    txtDescription.Text = "[Nhập nội dung tại đây.]";
                    txtDescription.Text += "\r\n\n-------------------------------------------------------------------------------------------------------------------------";
                    txtDescription.Text += "\r\n" + contentServer; // sb.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show(ex.Message);
            }
            finally { Cursor = Cursors.Default; }
        }
    }
}
