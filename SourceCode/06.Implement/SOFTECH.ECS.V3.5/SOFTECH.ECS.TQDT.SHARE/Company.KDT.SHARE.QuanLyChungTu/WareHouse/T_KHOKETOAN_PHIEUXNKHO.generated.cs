﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.WareHouse
{
	public partial class T_KHOKETOAN_PHIEUXNKHO : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public int TRANGTHAI { set; get; }
		public string SOCT { set; get; }
		public DateTime NGAYCT { set; get; }
		public string LOAICHUNGTU { set; get; }
		public string SOCTGOC { set; get; }
		public DateTime NGAYCTGOC { set; get; }
		public decimal SOTK { set; get; }
		public DateTime NGAYTK { set; get; }
		public string MAHQ { set; get; }
		public string SOHOADON { set; get; }
		public DateTime NGAYHOADON { set; get; }
		public long HOPDONG_ID { set; get; }
		public string SOHOPDONG { set; get; }
		public DateTime NGAYHOPDONG { set; get; }
		public string MAKH { set; get; }
		public string TENKH { set; get; }
		public string DIACHIKH { set; get; }
		public string MADN { set; get; }
		public string TENDN { set; get; }
		public string DIACHIDN { set; get; }
		public string DIENGIAI { set; get; }
		public string NGUOIVANCHUYEN { set; get; }
		public string MAKHO { set; get; }
		public string TENKHO { set; get; }
		public string MAKHOCHUYEN { set; get; }
		public string TENKHOCHUYEN { set; get; }
		public string TKKHO { set; get; }
		public string LOAIHANGHOA { set; get; }
		public decimal TONGTIENHANG { set; get; }
		public string MANGUYENTE { set; get; }
		public decimal TYGIA { set; get; }
		public string GHICHU { set; get; }
		public decimal TONGLUONGHANG { set; get; }
		public decimal PHIVANCHUYEN { set; get; }
		public decimal CHIPHI { set; get; }
		public string GHICHUCHIPHI { set; get; }
		public string MAPO { set; get; }
		public bool PHANBO { set; get; }
        public List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> HangCollection = new List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KHOKETOAN_PHIEUXNKHO> ConvertToCollection(IDataReader reader)
		{
			List<T_KHOKETOAN_PHIEUXNKHO> collection = new List<T_KHOKETOAN_PHIEUXNKHO>();
			while (reader.Read())
			{
				T_KHOKETOAN_PHIEUXNKHO entity = new T_KHOKETOAN_PHIEUXNKHO();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRANGTHAI"))) entity.TRANGTHAI = reader.GetInt32(reader.GetOrdinal("TRANGTHAI"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOCT"))) entity.SOCT = reader.GetString(reader.GetOrdinal("SOCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYCT"))) entity.NGAYCT = reader.GetDateTime(reader.GetOrdinal("NGAYCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LOAICHUNGTU"))) entity.LOAICHUNGTU = reader.GetString(reader.GetOrdinal("LOAICHUNGTU"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOCTGOC"))) entity.SOCTGOC = reader.GetString(reader.GetOrdinal("SOCTGOC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYCTGOC"))) entity.NGAYCTGOC = reader.GetDateTime(reader.GetOrdinal("NGAYCTGOC"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOTK"))) entity.SOTK = reader.GetDecimal(reader.GetOrdinal("SOTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYTK"))) entity.NGAYTK = reader.GetDateTime(reader.GetOrdinal("NGAYTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAHQ"))) entity.MAHQ = reader.GetString(reader.GetOrdinal("MAHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOHOADON"))) entity.SOHOADON = reader.GetString(reader.GetOrdinal("SOHOADON"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYHOADON"))) entity.NGAYHOADON = reader.GetDateTime(reader.GetOrdinal("NGAYHOADON"));
				if (!reader.IsDBNull(reader.GetOrdinal("HOPDONG_ID"))) entity.HOPDONG_ID = reader.GetInt64(reader.GetOrdinal("HOPDONG_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOHOPDONG"))) entity.SOHOPDONG = reader.GetString(reader.GetOrdinal("SOHOPDONG"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYHOPDONG"))) entity.NGAYHOPDONG = reader.GetDateTime(reader.GetOrdinal("NGAYHOPDONG"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAKH"))) entity.MAKH = reader.GetString(reader.GetOrdinal("MAKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENKH"))) entity.TENKH = reader.GetString(reader.GetOrdinal("TENKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("DIACHIKH"))) entity.DIACHIKH = reader.GetString(reader.GetOrdinal("DIACHIKH"));
				if (!reader.IsDBNull(reader.GetOrdinal("MADN"))) entity.MADN = reader.GetString(reader.GetOrdinal("MADN"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENDN"))) entity.TENDN = reader.GetString(reader.GetOrdinal("TENDN"));
				if (!reader.IsDBNull(reader.GetOrdinal("DIACHIDN"))) entity.DIACHIDN = reader.GetString(reader.GetOrdinal("DIACHIDN"));
				if (!reader.IsDBNull(reader.GetOrdinal("DIENGIAI"))) entity.DIENGIAI = reader.GetString(reader.GetOrdinal("DIENGIAI"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGUOIVANCHUYEN"))) entity.NGUOIVANCHUYEN = reader.GetString(reader.GetOrdinal("NGUOIVANCHUYEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAKHO"))) entity.MAKHO = reader.GetString(reader.GetOrdinal("MAKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENKHO"))) entity.TENKHO = reader.GetString(reader.GetOrdinal("TENKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAKHOCHUYEN"))) entity.MAKHOCHUYEN = reader.GetString(reader.GetOrdinal("MAKHOCHUYEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENKHOCHUYEN"))) entity.TENKHOCHUYEN = reader.GetString(reader.GetOrdinal("TENKHOCHUYEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKKHO"))) entity.TKKHO = reader.GetString(reader.GetOrdinal("TKKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("LOAIHANGHOA"))) entity.LOAIHANGHOA = reader.GetString(reader.GetOrdinal("LOAIHANGHOA"));
				if (!reader.IsDBNull(reader.GetOrdinal("TONGTIENHANG"))) entity.TONGTIENHANG = reader.GetDecimal(reader.GetOrdinal("TONGTIENHANG"));
				if (!reader.IsDBNull(reader.GetOrdinal("MANGUYENTE"))) entity.MANGUYENTE = reader.GetString(reader.GetOrdinal("MANGUYENTE"));
				if (!reader.IsDBNull(reader.GetOrdinal("TYGIA"))) entity.TYGIA = reader.GetDecimal(reader.GetOrdinal("TYGIA"));
				if (!reader.IsDBNull(reader.GetOrdinal("GHICHU"))) entity.GHICHU = reader.GetString(reader.GetOrdinal("GHICHU"));
				if (!reader.IsDBNull(reader.GetOrdinal("TONGLUONGHANG"))) entity.TONGLUONGHANG = reader.GetDecimal(reader.GetOrdinal("TONGLUONGHANG"));
				if (!reader.IsDBNull(reader.GetOrdinal("PHIVANCHUYEN"))) entity.PHIVANCHUYEN = reader.GetDecimal(reader.GetOrdinal("PHIVANCHUYEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("CHIPHI"))) entity.CHIPHI = reader.GetDecimal(reader.GetOrdinal("CHIPHI"));
				if (!reader.IsDBNull(reader.GetOrdinal("GHICHUCHIPHI"))) entity.GHICHUCHIPHI = reader.GetString(reader.GetOrdinal("GHICHUCHIPHI"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAPO"))) entity.MAPO = reader.GetString(reader.GetOrdinal("MAPO"));
				if (!reader.IsDBNull(reader.GetOrdinal("PHANBO"))) entity.PHANBO = reader.GetBoolean(reader.GetOrdinal("PHANBO"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KHOKETOAN_PHIEUXNKHO> collection, long id)
        {
            foreach (T_KHOKETOAN_PHIEUXNKHO item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KHOKETOAN_PHIEUXNKHO VALUES(@TRANGTHAI, @SOCT, @NGAYCT, @LOAICHUNGTU, @SOCTGOC, @NGAYCTGOC, @SOTK, @NGAYTK, @MAHQ, @SOHOADON, @NGAYHOADON, @HOPDONG_ID, @SOHOPDONG, @NGAYHOPDONG, @MAKH, @TENKH, @DIACHIKH, @MADN, @TENDN, @DIACHIDN, @DIENGIAI, @NGUOIVANCHUYEN, @MAKHO, @TENKHO, @MAKHOCHUYEN, @TENKHOCHUYEN, @TKKHO, @LOAIHANGHOA, @TONGTIENHANG, @MANGUYENTE, @TYGIA, @GHICHU, @TONGLUONGHANG, @PHIVANCHUYEN, @CHIPHI, @GHICHUCHIPHI, @MAPO, @PHANBO)";
            string update = "UPDATE T_KHOKETOAN_PHIEUXNKHO SET TRANGTHAI = @TRANGTHAI, SOCT = @SOCT, NGAYCT = @NGAYCT, LOAICHUNGTU = @LOAICHUNGTU, SOCTGOC = @SOCTGOC, NGAYCTGOC = @NGAYCTGOC, SOTK = @SOTK, NGAYTK = @NGAYTK, MAHQ = @MAHQ, SOHOADON = @SOHOADON, NGAYHOADON = @NGAYHOADON, HOPDONG_ID = @HOPDONG_ID, SOHOPDONG = @SOHOPDONG, NGAYHOPDONG = @NGAYHOPDONG, MAKH = @MAKH, TENKH = @TENKH, DIACHIKH = @DIACHIKH, MADN = @MADN, TENDN = @TENDN, DIACHIDN = @DIACHIDN, DIENGIAI = @DIENGIAI, NGUOIVANCHUYEN = @NGUOIVANCHUYEN, MAKHO = @MAKHO, TENKHO = @TENKHO, MAKHOCHUYEN = @MAKHOCHUYEN, TENKHOCHUYEN = @TENKHOCHUYEN, TKKHO = @TKKHO, LOAIHANGHOA = @LOAIHANGHOA, TONGTIENHANG = @TONGTIENHANG, MANGUYENTE = @MANGUYENTE, TYGIA = @TYGIA, GHICHU = @GHICHU, TONGLUONGHANG = @TONGLUONGHANG, PHIVANCHUYEN = @PHIVANCHUYEN, CHIPHI = @CHIPHI, GHICHUCHIPHI = @GHICHUCHIPHI, MAPO = @MAPO, PHANBO = @PHANBO WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_PHIEUXNKHO WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRANGTHAI", SqlDbType.Int, "TRANGTHAI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOCT", SqlDbType.NVarChar, "SOCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYCT", SqlDbType.DateTime, "NGAYCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAICHUNGTU", SqlDbType.VarChar, "LOAICHUNGTU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOCTGOC", SqlDbType.NVarChar, "SOCTGOC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYCTGOC", SqlDbType.DateTime, "NGAYCTGOC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOTK", SqlDbType.Decimal, "SOTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYTK", SqlDbType.DateTime, "NGAYTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHQ", SqlDbType.VarChar, "MAHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOHOADON", SqlDbType.NVarChar, "SOHOADON", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYHOADON", SqlDbType.DateTime, "NGAYHOADON", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOHOPDONG", SqlDbType.NVarChar, "SOHOPDONG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYHOPDONG", SqlDbType.DateTime, "NGAYHOPDONG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKH", SqlDbType.NVarChar, "MAKH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKH", SqlDbType.NVarChar, "TENKH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DIACHIKH", SqlDbType.NVarChar, "DIACHIKH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MADN", SqlDbType.NVarChar, "MADN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENDN", SqlDbType.NVarChar, "TENDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DIACHIDN", SqlDbType.NVarChar, "DIACHIDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DIENGIAI", SqlDbType.NVarChar, "DIENGIAI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGUOIVANCHUYEN", SqlDbType.NVarChar, "NGUOIVANCHUYEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHOCHUYEN", SqlDbType.NVarChar, "MAKHOCHUYEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHOCHUYEN", SqlDbType.NVarChar, "TENKHOCHUYEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKKHO", SqlDbType.NVarChar, "TKKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAIHANGHOA", SqlDbType.NVarChar, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TONGTIENHANG", SqlDbType.Decimal, "TONGTIENHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANGUYENTE", SqlDbType.NVarChar, "MANGUYENTE", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYGIA", SqlDbType.Decimal, "TYGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TONGLUONGHANG", SqlDbType.Decimal, "TONGLUONGHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PHIVANCHUYEN", SqlDbType.Decimal, "PHIVANCHUYEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CHIPHI", SqlDbType.Decimal, "CHIPHI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHUCHIPHI", SqlDbType.NVarChar, "GHICHUCHIPHI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAPO", SqlDbType.NVarChar, "MAPO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PHANBO", SqlDbType.Bit, "PHANBO", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRANGTHAI", SqlDbType.Int, "TRANGTHAI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOCT", SqlDbType.NVarChar, "SOCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYCT", SqlDbType.DateTime, "NGAYCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAICHUNGTU", SqlDbType.VarChar, "LOAICHUNGTU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOCTGOC", SqlDbType.NVarChar, "SOCTGOC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYCTGOC", SqlDbType.DateTime, "NGAYCTGOC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOTK", SqlDbType.Decimal, "SOTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYTK", SqlDbType.DateTime, "NGAYTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHQ", SqlDbType.VarChar, "MAHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOHOADON", SqlDbType.NVarChar, "SOHOADON", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYHOADON", SqlDbType.DateTime, "NGAYHOADON", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOHOPDONG", SqlDbType.NVarChar, "SOHOPDONG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYHOPDONG", SqlDbType.DateTime, "NGAYHOPDONG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKH", SqlDbType.NVarChar, "MAKH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKH", SqlDbType.NVarChar, "TENKH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DIACHIKH", SqlDbType.NVarChar, "DIACHIKH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MADN", SqlDbType.NVarChar, "MADN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENDN", SqlDbType.NVarChar, "TENDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DIACHIDN", SqlDbType.NVarChar, "DIACHIDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DIENGIAI", SqlDbType.NVarChar, "DIENGIAI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGUOIVANCHUYEN", SqlDbType.NVarChar, "NGUOIVANCHUYEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHOCHUYEN", SqlDbType.NVarChar, "MAKHOCHUYEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHOCHUYEN", SqlDbType.NVarChar, "TENKHOCHUYEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKKHO", SqlDbType.NVarChar, "TKKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAIHANGHOA", SqlDbType.NVarChar, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TONGTIENHANG", SqlDbType.Decimal, "TONGTIENHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANGUYENTE", SqlDbType.NVarChar, "MANGUYENTE", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYGIA", SqlDbType.Decimal, "TYGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TONGLUONGHANG", SqlDbType.Decimal, "TONGLUONGHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PHIVANCHUYEN", SqlDbType.Decimal, "PHIVANCHUYEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CHIPHI", SqlDbType.Decimal, "CHIPHI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHUCHIPHI", SqlDbType.NVarChar, "GHICHUCHIPHI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAPO", SqlDbType.NVarChar, "MAPO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PHANBO", SqlDbType.Bit, "PHANBO", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KHOKETOAN_PHIEUXNKHO VALUES(@TRANGTHAI, @SOCT, @NGAYCT, @LOAICHUNGTU, @SOCTGOC, @NGAYCTGOC, @SOTK, @NGAYTK, @MAHQ, @SOHOADON, @NGAYHOADON, @HOPDONG_ID, @SOHOPDONG, @NGAYHOPDONG, @MAKH, @TENKH, @DIACHIKH, @MADN, @TENDN, @DIACHIDN, @DIENGIAI, @NGUOIVANCHUYEN, @MAKHO, @TENKHO, @MAKHOCHUYEN, @TENKHOCHUYEN, @TKKHO, @LOAIHANGHOA, @TONGTIENHANG, @MANGUYENTE, @TYGIA, @GHICHU, @TONGLUONGHANG, @PHIVANCHUYEN, @CHIPHI, @GHICHUCHIPHI, @MAPO, @PHANBO)";
            string update = "UPDATE T_KHOKETOAN_PHIEUXNKHO SET TRANGTHAI = @TRANGTHAI, SOCT = @SOCT, NGAYCT = @NGAYCT, LOAICHUNGTU = @LOAICHUNGTU, SOCTGOC = @SOCTGOC, NGAYCTGOC = @NGAYCTGOC, SOTK = @SOTK, NGAYTK = @NGAYTK, MAHQ = @MAHQ, SOHOADON = @SOHOADON, NGAYHOADON = @NGAYHOADON, HOPDONG_ID = @HOPDONG_ID, SOHOPDONG = @SOHOPDONG, NGAYHOPDONG = @NGAYHOPDONG, MAKH = @MAKH, TENKH = @TENKH, DIACHIKH = @DIACHIKH, MADN = @MADN, TENDN = @TENDN, DIACHIDN = @DIACHIDN, DIENGIAI = @DIENGIAI, NGUOIVANCHUYEN = @NGUOIVANCHUYEN, MAKHO = @MAKHO, TENKHO = @TENKHO, MAKHOCHUYEN = @MAKHOCHUYEN, TENKHOCHUYEN = @TENKHOCHUYEN, TKKHO = @TKKHO, LOAIHANGHOA = @LOAIHANGHOA, TONGTIENHANG = @TONGTIENHANG, MANGUYENTE = @MANGUYENTE, TYGIA = @TYGIA, GHICHU = @GHICHU, TONGLUONGHANG = @TONGLUONGHANG, PHIVANCHUYEN = @PHIVANCHUYEN, CHIPHI = @CHIPHI, GHICHUCHIPHI = @GHICHUCHIPHI, MAPO = @MAPO, PHANBO = @PHANBO WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_PHIEUXNKHO WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRANGTHAI", SqlDbType.Int, "TRANGTHAI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOCT", SqlDbType.NVarChar, "SOCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYCT", SqlDbType.DateTime, "NGAYCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAICHUNGTU", SqlDbType.VarChar, "LOAICHUNGTU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOCTGOC", SqlDbType.NVarChar, "SOCTGOC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYCTGOC", SqlDbType.DateTime, "NGAYCTGOC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOTK", SqlDbType.Decimal, "SOTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYTK", SqlDbType.DateTime, "NGAYTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHQ", SqlDbType.VarChar, "MAHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOHOADON", SqlDbType.NVarChar, "SOHOADON", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYHOADON", SqlDbType.DateTime, "NGAYHOADON", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOHOPDONG", SqlDbType.NVarChar, "SOHOPDONG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGAYHOPDONG", SqlDbType.DateTime, "NGAYHOPDONG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKH", SqlDbType.NVarChar, "MAKH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKH", SqlDbType.NVarChar, "TENKH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DIACHIKH", SqlDbType.NVarChar, "DIACHIKH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MADN", SqlDbType.NVarChar, "MADN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENDN", SqlDbType.NVarChar, "TENDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DIACHIDN", SqlDbType.NVarChar, "DIACHIDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DIENGIAI", SqlDbType.NVarChar, "DIENGIAI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NGUOIVANCHUYEN", SqlDbType.NVarChar, "NGUOIVANCHUYEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHOCHUYEN", SqlDbType.NVarChar, "MAKHOCHUYEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHOCHUYEN", SqlDbType.NVarChar, "TENKHOCHUYEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKKHO", SqlDbType.NVarChar, "TKKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAIHANGHOA", SqlDbType.NVarChar, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TONGTIENHANG", SqlDbType.Decimal, "TONGTIENHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANGUYENTE", SqlDbType.NVarChar, "MANGUYENTE", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYGIA", SqlDbType.Decimal, "TYGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TONGLUONGHANG", SqlDbType.Decimal, "TONGLUONGHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PHIVANCHUYEN", SqlDbType.Decimal, "PHIVANCHUYEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CHIPHI", SqlDbType.Decimal, "CHIPHI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHUCHIPHI", SqlDbType.NVarChar, "GHICHUCHIPHI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAPO", SqlDbType.NVarChar, "MAPO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PHANBO", SqlDbType.Bit, "PHANBO", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRANGTHAI", SqlDbType.Int, "TRANGTHAI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOCT", SqlDbType.NVarChar, "SOCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYCT", SqlDbType.DateTime, "NGAYCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAICHUNGTU", SqlDbType.VarChar, "LOAICHUNGTU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOCTGOC", SqlDbType.NVarChar, "SOCTGOC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYCTGOC", SqlDbType.DateTime, "NGAYCTGOC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOTK", SqlDbType.Decimal, "SOTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYTK", SqlDbType.DateTime, "NGAYTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHQ", SqlDbType.VarChar, "MAHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOHOADON", SqlDbType.NVarChar, "SOHOADON", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYHOADON", SqlDbType.DateTime, "NGAYHOADON", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOHOPDONG", SqlDbType.NVarChar, "SOHOPDONG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGAYHOPDONG", SqlDbType.DateTime, "NGAYHOPDONG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKH", SqlDbType.NVarChar, "MAKH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKH", SqlDbType.NVarChar, "TENKH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DIACHIKH", SqlDbType.NVarChar, "DIACHIKH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MADN", SqlDbType.NVarChar, "MADN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENDN", SqlDbType.NVarChar, "TENDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DIACHIDN", SqlDbType.NVarChar, "DIACHIDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DIENGIAI", SqlDbType.NVarChar, "DIENGIAI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NGUOIVANCHUYEN", SqlDbType.NVarChar, "NGUOIVANCHUYEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHOCHUYEN", SqlDbType.NVarChar, "MAKHOCHUYEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHOCHUYEN", SqlDbType.NVarChar, "TENKHOCHUYEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKKHO", SqlDbType.NVarChar, "TKKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAIHANGHOA", SqlDbType.NVarChar, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TONGTIENHANG", SqlDbType.Decimal, "TONGTIENHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANGUYENTE", SqlDbType.NVarChar, "MANGUYENTE", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYGIA", SqlDbType.Decimal, "TYGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TONGLUONGHANG", SqlDbType.Decimal, "TONGLUONGHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PHIVANCHUYEN", SqlDbType.Decimal, "PHIVANCHUYEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CHIPHI", SqlDbType.Decimal, "CHIPHI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHUCHIPHI", SqlDbType.NVarChar, "GHICHUCHIPHI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAPO", SqlDbType.NVarChar, "MAPO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PHANBO", SqlDbType.Bit, "PHANBO", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KHOKETOAN_PHIEUXNKHO Load(long id)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KHOKETOAN_PHIEUXNKHO> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KHOKETOAN_PHIEUXNKHO> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KHOKETOAN_PHIEUXNKHO> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public static DataSet SelectDynamicCartReport(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamicCartReport]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KHOKETOAN_PHIEUXNKHO(int tRANGTHAI, string sOCT, DateTime nGAYCT, string lOAICHUNGTU, string sOCTGOC, DateTime nGAYCTGOC, decimal sOTK, DateTime nGAYTK, string mAHQ, string sOHOADON, DateTime nGAYHOADON, long hOPDONG_ID, string sOHOPDONG, DateTime nGAYHOPDONG, string mAKH, string tENKH, string dIACHIKH, string mADN, string tENDN, string dIACHIDN, string dIENGIAI, string nGUOIVANCHUYEN, string mAKHO, string tENKHO, string mAKHOCHUYEN, string tENKHOCHUYEN, string tKKHO, string lOAIHANGHOA, decimal tONGTIENHANG, string mANGUYENTE, decimal tYGIA, string gHICHU, decimal tONGLUONGHANG, decimal pHIVANCHUYEN, decimal cHIPHI, string gHICHUCHIPHI, string mAPO, bool pHANBO)
		{
			T_KHOKETOAN_PHIEUXNKHO entity = new T_KHOKETOAN_PHIEUXNKHO();	
			entity.TRANGTHAI = tRANGTHAI;
			entity.SOCT = sOCT;
			entity.NGAYCT = nGAYCT;
			entity.LOAICHUNGTU = lOAICHUNGTU;
			entity.SOCTGOC = sOCTGOC;
			entity.NGAYCTGOC = nGAYCTGOC;
			entity.SOTK = sOTK;
			entity.NGAYTK = nGAYTK;
			entity.MAHQ = mAHQ;
			entity.SOHOADON = sOHOADON;
			entity.NGAYHOADON = nGAYHOADON;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.SOHOPDONG = sOHOPDONG;
			entity.NGAYHOPDONG = nGAYHOPDONG;
			entity.MAKH = mAKH;
			entity.TENKH = tENKH;
			entity.DIACHIKH = dIACHIKH;
			entity.MADN = mADN;
			entity.TENDN = tENDN;
			entity.DIACHIDN = dIACHIDN;
			entity.DIENGIAI = dIENGIAI;
			entity.NGUOIVANCHUYEN = nGUOIVANCHUYEN;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			entity.MAKHOCHUYEN = mAKHOCHUYEN;
			entity.TENKHOCHUYEN = tENKHOCHUYEN;
			entity.TKKHO = tKKHO;
			entity.LOAIHANGHOA = lOAIHANGHOA;
			entity.TONGTIENHANG = tONGTIENHANG;
			entity.MANGUYENTE = mANGUYENTE;
			entity.TYGIA = tYGIA;
			entity.GHICHU = gHICHU;
			entity.TONGLUONGHANG = tONGLUONGHANG;
			entity.PHIVANCHUYEN = pHIVANCHUYEN;
			entity.CHIPHI = cHIPHI;
			entity.GHICHUCHIPHI = gHICHUCHIPHI;
			entity.MAPO = mAPO;
			entity.PHANBO = pHANBO;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TRANGTHAI", SqlDbType.Int, TRANGTHAI);
			db.AddInParameter(dbCommand, "@SOCT", SqlDbType.NVarChar, SOCT);
			db.AddInParameter(dbCommand, "@NGAYCT", SqlDbType.DateTime, NGAYCT.Year <= 1753 ? DBNull.Value : (object) NGAYCT);
			db.AddInParameter(dbCommand, "@LOAICHUNGTU", SqlDbType.VarChar, LOAICHUNGTU);
			db.AddInParameter(dbCommand, "@SOCTGOC", SqlDbType.NVarChar, SOCTGOC);
			db.AddInParameter(dbCommand, "@NGAYCTGOC", SqlDbType.DateTime, NGAYCTGOC.Year <= 1753 ? DBNull.Value : (object) NGAYCTGOC);
			db.AddInParameter(dbCommand, "@SOTK", SqlDbType.Decimal, SOTK);
			db.AddInParameter(dbCommand, "@NGAYTK", SqlDbType.DateTime, NGAYTK.Year <= 1753 ? DBNull.Value : (object) NGAYTK);
			db.AddInParameter(dbCommand, "@MAHQ", SqlDbType.VarChar, MAHQ);
			db.AddInParameter(dbCommand, "@SOHOADON", SqlDbType.NVarChar, SOHOADON);
			db.AddInParameter(dbCommand, "@NGAYHOADON", SqlDbType.DateTime, NGAYHOADON.Year <= 1753 ? DBNull.Value : (object) NGAYHOADON);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@SOHOPDONG", SqlDbType.NVarChar, SOHOPDONG);
			db.AddInParameter(dbCommand, "@NGAYHOPDONG", SqlDbType.DateTime, NGAYHOPDONG.Year <= 1753 ? DBNull.Value : (object) NGAYHOPDONG);
			db.AddInParameter(dbCommand, "@MAKH", SqlDbType.NVarChar, MAKH);
			db.AddInParameter(dbCommand, "@TENKH", SqlDbType.NVarChar, TENKH);
			db.AddInParameter(dbCommand, "@DIACHIKH", SqlDbType.NVarChar, DIACHIKH);
			db.AddInParameter(dbCommand, "@MADN", SqlDbType.NVarChar, MADN);
			db.AddInParameter(dbCommand, "@TENDN", SqlDbType.NVarChar, TENDN);
			db.AddInParameter(dbCommand, "@DIACHIDN", SqlDbType.NVarChar, DIACHIDN);
			db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.NVarChar, DIENGIAI);
			db.AddInParameter(dbCommand, "@NGUOIVANCHUYEN", SqlDbType.NVarChar, NGUOIVANCHUYEN);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			db.AddInParameter(dbCommand, "@MAKHOCHUYEN", SqlDbType.NVarChar, MAKHOCHUYEN);
			db.AddInParameter(dbCommand, "@TENKHOCHUYEN", SqlDbType.NVarChar, TENKHOCHUYEN);
			db.AddInParameter(dbCommand, "@TKKHO", SqlDbType.NVarChar, TKKHO);
			db.AddInParameter(dbCommand, "@LOAIHANGHOA", SqlDbType.NVarChar, LOAIHANGHOA);
			db.AddInParameter(dbCommand, "@TONGTIENHANG", SqlDbType.Decimal, TONGTIENHANG);
			db.AddInParameter(dbCommand, "@MANGUYENTE", SqlDbType.NVarChar, MANGUYENTE);
			db.AddInParameter(dbCommand, "@TYGIA", SqlDbType.Decimal, TYGIA);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@TONGLUONGHANG", SqlDbType.Decimal, TONGLUONGHANG);
			db.AddInParameter(dbCommand, "@PHIVANCHUYEN", SqlDbType.Decimal, PHIVANCHUYEN);
			db.AddInParameter(dbCommand, "@CHIPHI", SqlDbType.Decimal, CHIPHI);
			db.AddInParameter(dbCommand, "@GHICHUCHIPHI", SqlDbType.NVarChar, GHICHUCHIPHI);
			db.AddInParameter(dbCommand, "@MAPO", SqlDbType.NVarChar, MAPO);
			db.AddInParameter(dbCommand, "@PHANBO", SqlDbType.Bit, PHANBO);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KHOKETOAN_PHIEUXNKHO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_PHIEUXNKHO item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KHOKETOAN_PHIEUXNKHO(long id, int tRANGTHAI, string sOCT, DateTime nGAYCT, string lOAICHUNGTU, string sOCTGOC, DateTime nGAYCTGOC, decimal sOTK, DateTime nGAYTK, string mAHQ, string sOHOADON, DateTime nGAYHOADON, long hOPDONG_ID, string sOHOPDONG, DateTime nGAYHOPDONG, string mAKH, string tENKH, string dIACHIKH, string mADN, string tENDN, string dIACHIDN, string dIENGIAI, string nGUOIVANCHUYEN, string mAKHO, string tENKHO, string mAKHOCHUYEN, string tENKHOCHUYEN, string tKKHO, string lOAIHANGHOA, decimal tONGTIENHANG, string mANGUYENTE, decimal tYGIA, string gHICHU, decimal tONGLUONGHANG, decimal pHIVANCHUYEN, decimal cHIPHI, string gHICHUCHIPHI, string mAPO, bool pHANBO)
		{
			T_KHOKETOAN_PHIEUXNKHO entity = new T_KHOKETOAN_PHIEUXNKHO();			
			entity.ID = id;
			entity.TRANGTHAI = tRANGTHAI;
			entity.SOCT = sOCT;
			entity.NGAYCT = nGAYCT;
			entity.LOAICHUNGTU = lOAICHUNGTU;
			entity.SOCTGOC = sOCTGOC;
			entity.NGAYCTGOC = nGAYCTGOC;
			entity.SOTK = sOTK;
			entity.NGAYTK = nGAYTK;
			entity.MAHQ = mAHQ;
			entity.SOHOADON = sOHOADON;
			entity.NGAYHOADON = nGAYHOADON;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.SOHOPDONG = sOHOPDONG;
			entity.NGAYHOPDONG = nGAYHOPDONG;
			entity.MAKH = mAKH;
			entity.TENKH = tENKH;
			entity.DIACHIKH = dIACHIKH;
			entity.MADN = mADN;
			entity.TENDN = tENDN;
			entity.DIACHIDN = dIACHIDN;
			entity.DIENGIAI = dIENGIAI;
			entity.NGUOIVANCHUYEN = nGUOIVANCHUYEN;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			entity.MAKHOCHUYEN = mAKHOCHUYEN;
			entity.TENKHOCHUYEN = tENKHOCHUYEN;
			entity.TKKHO = tKKHO;
			entity.LOAIHANGHOA = lOAIHANGHOA;
			entity.TONGTIENHANG = tONGTIENHANG;
			entity.MANGUYENTE = mANGUYENTE;
			entity.TYGIA = tYGIA;
			entity.GHICHU = gHICHU;
			entity.TONGLUONGHANG = tONGLUONGHANG;
			entity.PHIVANCHUYEN = pHIVANCHUYEN;
			entity.CHIPHI = cHIPHI;
			entity.GHICHUCHIPHI = gHICHUCHIPHI;
			entity.MAPO = mAPO;
			entity.PHANBO = pHANBO;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KHOKETOAN_PHIEUXNKHO_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TRANGTHAI", SqlDbType.Int, TRANGTHAI);
			db.AddInParameter(dbCommand, "@SOCT", SqlDbType.NVarChar, SOCT);
			db.AddInParameter(dbCommand, "@NGAYCT", SqlDbType.DateTime, NGAYCT.Year <= 1753 ? DBNull.Value : (object) NGAYCT);
			db.AddInParameter(dbCommand, "@LOAICHUNGTU", SqlDbType.VarChar, LOAICHUNGTU);
			db.AddInParameter(dbCommand, "@SOCTGOC", SqlDbType.NVarChar, SOCTGOC);
			db.AddInParameter(dbCommand, "@NGAYCTGOC", SqlDbType.DateTime, NGAYCTGOC.Year <= 1753 ? DBNull.Value : (object) NGAYCTGOC);
			db.AddInParameter(dbCommand, "@SOTK", SqlDbType.Decimal, SOTK);
			db.AddInParameter(dbCommand, "@NGAYTK", SqlDbType.DateTime, NGAYTK.Year <= 1753 ? DBNull.Value : (object) NGAYTK);
			db.AddInParameter(dbCommand, "@MAHQ", SqlDbType.VarChar, MAHQ);
			db.AddInParameter(dbCommand, "@SOHOADON", SqlDbType.NVarChar, SOHOADON);
			db.AddInParameter(dbCommand, "@NGAYHOADON", SqlDbType.DateTime, NGAYHOADON.Year <= 1753 ? DBNull.Value : (object) NGAYHOADON);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@SOHOPDONG", SqlDbType.NVarChar, SOHOPDONG);
			db.AddInParameter(dbCommand, "@NGAYHOPDONG", SqlDbType.DateTime, NGAYHOPDONG.Year <= 1753 ? DBNull.Value : (object) NGAYHOPDONG);
			db.AddInParameter(dbCommand, "@MAKH", SqlDbType.NVarChar, MAKH);
			db.AddInParameter(dbCommand, "@TENKH", SqlDbType.NVarChar, TENKH);
			db.AddInParameter(dbCommand, "@DIACHIKH", SqlDbType.NVarChar, DIACHIKH);
			db.AddInParameter(dbCommand, "@MADN", SqlDbType.NVarChar, MADN);
			db.AddInParameter(dbCommand, "@TENDN", SqlDbType.NVarChar, TENDN);
			db.AddInParameter(dbCommand, "@DIACHIDN", SqlDbType.NVarChar, DIACHIDN);
			db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.NVarChar, DIENGIAI);
			db.AddInParameter(dbCommand, "@NGUOIVANCHUYEN", SqlDbType.NVarChar, NGUOIVANCHUYEN);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			db.AddInParameter(dbCommand, "@MAKHOCHUYEN", SqlDbType.NVarChar, MAKHOCHUYEN);
			db.AddInParameter(dbCommand, "@TENKHOCHUYEN", SqlDbType.NVarChar, TENKHOCHUYEN);
			db.AddInParameter(dbCommand, "@TKKHO", SqlDbType.NVarChar, TKKHO);
			db.AddInParameter(dbCommand, "@LOAIHANGHOA", SqlDbType.NVarChar, LOAIHANGHOA);
			db.AddInParameter(dbCommand, "@TONGTIENHANG", SqlDbType.Decimal, TONGTIENHANG);
			db.AddInParameter(dbCommand, "@MANGUYENTE", SqlDbType.NVarChar, MANGUYENTE);
			db.AddInParameter(dbCommand, "@TYGIA", SqlDbType.Decimal, TYGIA);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@TONGLUONGHANG", SqlDbType.Decimal, TONGLUONGHANG);
			db.AddInParameter(dbCommand, "@PHIVANCHUYEN", SqlDbType.Decimal, PHIVANCHUYEN);
			db.AddInParameter(dbCommand, "@CHIPHI", SqlDbType.Decimal, CHIPHI);
			db.AddInParameter(dbCommand, "@GHICHUCHIPHI", SqlDbType.NVarChar, GHICHUCHIPHI);
			db.AddInParameter(dbCommand, "@MAPO", SqlDbType.NVarChar, MAPO);
			db.AddInParameter(dbCommand, "@PHANBO", SqlDbType.Bit, PHANBO);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KHOKETOAN_PHIEUXNKHO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_PHIEUXNKHO item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KHOKETOAN_PHIEUXNKHO(long id, int tRANGTHAI, string sOCT, DateTime nGAYCT, string lOAICHUNGTU, string sOCTGOC, DateTime nGAYCTGOC, decimal sOTK, DateTime nGAYTK, string mAHQ, string sOHOADON, DateTime nGAYHOADON, long hOPDONG_ID, string sOHOPDONG, DateTime nGAYHOPDONG, string mAKH, string tENKH, string dIACHIKH, string mADN, string tENDN, string dIACHIDN, string dIENGIAI, string nGUOIVANCHUYEN, string mAKHO, string tENKHO, string mAKHOCHUYEN, string tENKHOCHUYEN, string tKKHO, string lOAIHANGHOA, decimal tONGTIENHANG, string mANGUYENTE, decimal tYGIA, string gHICHU, decimal tONGLUONGHANG, decimal pHIVANCHUYEN, decimal cHIPHI, string gHICHUCHIPHI, string mAPO, bool pHANBO)
		{
			T_KHOKETOAN_PHIEUXNKHO entity = new T_KHOKETOAN_PHIEUXNKHO();			
			entity.ID = id;
			entity.TRANGTHAI = tRANGTHAI;
			entity.SOCT = sOCT;
			entity.NGAYCT = nGAYCT;
			entity.LOAICHUNGTU = lOAICHUNGTU;
			entity.SOCTGOC = sOCTGOC;
			entity.NGAYCTGOC = nGAYCTGOC;
			entity.SOTK = sOTK;
			entity.NGAYTK = nGAYTK;
			entity.MAHQ = mAHQ;
			entity.SOHOADON = sOHOADON;
			entity.NGAYHOADON = nGAYHOADON;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.SOHOPDONG = sOHOPDONG;
			entity.NGAYHOPDONG = nGAYHOPDONG;
			entity.MAKH = mAKH;
			entity.TENKH = tENKH;
			entity.DIACHIKH = dIACHIKH;
			entity.MADN = mADN;
			entity.TENDN = tENDN;
			entity.DIACHIDN = dIACHIDN;
			entity.DIENGIAI = dIENGIAI;
			entity.NGUOIVANCHUYEN = nGUOIVANCHUYEN;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			entity.MAKHOCHUYEN = mAKHOCHUYEN;
			entity.TENKHOCHUYEN = tENKHOCHUYEN;
			entity.TKKHO = tKKHO;
			entity.LOAIHANGHOA = lOAIHANGHOA;
			entity.TONGTIENHANG = tONGTIENHANG;
			entity.MANGUYENTE = mANGUYENTE;
			entity.TYGIA = tYGIA;
			entity.GHICHU = gHICHU;
			entity.TONGLUONGHANG = tONGLUONGHANG;
			entity.PHIVANCHUYEN = pHIVANCHUYEN;
			entity.CHIPHI = cHIPHI;
			entity.GHICHUCHIPHI = gHICHUCHIPHI;
			entity.MAPO = mAPO;
			entity.PHANBO = pHANBO;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TRANGTHAI", SqlDbType.Int, TRANGTHAI);
			db.AddInParameter(dbCommand, "@SOCT", SqlDbType.NVarChar, SOCT);
			db.AddInParameter(dbCommand, "@NGAYCT", SqlDbType.DateTime, NGAYCT.Year <= 1753 ? DBNull.Value : (object) NGAYCT);
			db.AddInParameter(dbCommand, "@LOAICHUNGTU", SqlDbType.VarChar, LOAICHUNGTU);
			db.AddInParameter(dbCommand, "@SOCTGOC", SqlDbType.NVarChar, SOCTGOC);
			db.AddInParameter(dbCommand, "@NGAYCTGOC", SqlDbType.DateTime, NGAYCTGOC.Year <= 1753 ? DBNull.Value : (object) NGAYCTGOC);
			db.AddInParameter(dbCommand, "@SOTK", SqlDbType.Decimal, SOTK);
			db.AddInParameter(dbCommand, "@NGAYTK", SqlDbType.DateTime, NGAYTK.Year <= 1753 ? DBNull.Value : (object) NGAYTK);
			db.AddInParameter(dbCommand, "@MAHQ", SqlDbType.VarChar, MAHQ);
			db.AddInParameter(dbCommand, "@SOHOADON", SqlDbType.NVarChar, SOHOADON);
			db.AddInParameter(dbCommand, "@NGAYHOADON", SqlDbType.DateTime, NGAYHOADON.Year <= 1753 ? DBNull.Value : (object) NGAYHOADON);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@SOHOPDONG", SqlDbType.NVarChar, SOHOPDONG);
			db.AddInParameter(dbCommand, "@NGAYHOPDONG", SqlDbType.DateTime, NGAYHOPDONG.Year <= 1753 ? DBNull.Value : (object) NGAYHOPDONG);
			db.AddInParameter(dbCommand, "@MAKH", SqlDbType.NVarChar, MAKH);
			db.AddInParameter(dbCommand, "@TENKH", SqlDbType.NVarChar, TENKH);
			db.AddInParameter(dbCommand, "@DIACHIKH", SqlDbType.NVarChar, DIACHIKH);
			db.AddInParameter(dbCommand, "@MADN", SqlDbType.NVarChar, MADN);
			db.AddInParameter(dbCommand, "@TENDN", SqlDbType.NVarChar, TENDN);
			db.AddInParameter(dbCommand, "@DIACHIDN", SqlDbType.NVarChar, DIACHIDN);
			db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.NVarChar, DIENGIAI);
			db.AddInParameter(dbCommand, "@NGUOIVANCHUYEN", SqlDbType.NVarChar, NGUOIVANCHUYEN);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			db.AddInParameter(dbCommand, "@MAKHOCHUYEN", SqlDbType.NVarChar, MAKHOCHUYEN);
			db.AddInParameter(dbCommand, "@TENKHOCHUYEN", SqlDbType.NVarChar, TENKHOCHUYEN);
			db.AddInParameter(dbCommand, "@TKKHO", SqlDbType.NVarChar, TKKHO);
			db.AddInParameter(dbCommand, "@LOAIHANGHOA", SqlDbType.NVarChar, LOAIHANGHOA);
			db.AddInParameter(dbCommand, "@TONGTIENHANG", SqlDbType.Decimal, TONGTIENHANG);
			db.AddInParameter(dbCommand, "@MANGUYENTE", SqlDbType.NVarChar, MANGUYENTE);
			db.AddInParameter(dbCommand, "@TYGIA", SqlDbType.Decimal, TYGIA);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@TONGLUONGHANG", SqlDbType.Decimal, TONGLUONGHANG);
			db.AddInParameter(dbCommand, "@PHIVANCHUYEN", SqlDbType.Decimal, PHIVANCHUYEN);
			db.AddInParameter(dbCommand, "@CHIPHI", SqlDbType.Decimal, CHIPHI);
			db.AddInParameter(dbCommand, "@GHICHUCHIPHI", SqlDbType.NVarChar, GHICHUCHIPHI);
			db.AddInParameter(dbCommand, "@MAPO", SqlDbType.NVarChar, MAPO);
			db.AddInParameter(dbCommand, "@PHANBO", SqlDbType.Bit, PHANBO);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KHOKETOAN_PHIEUXNKHO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_PHIEUXNKHO item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KHOKETOAN_PHIEUXNKHO(long id)
		{
			T_KHOKETOAN_PHIEUXNKHO entity = new T_KHOKETOAN_PHIEUXNKHO();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KHOKETOAN_PHIEUXNKHO> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_PHIEUXNKHO item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public static string GetChungTu(string LOAICT)
        {
            string spName = "SELECT TOP(1) SOCT FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE LOAICHUNGTU=N'"+ LOAICT + "' ORDER BY ID DESC";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return "";
            else
                return (String)obj;
        }
        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in HangCollection)
                    {
                        if (item.PHIEUXNKHO_ID == 0)
                        {
                            item.PHIEUXNKHO_ID = this.ID;
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
    }	
}