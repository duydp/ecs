﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Data;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class PhuKienGCManageForm : BaseForm
    {
        PhuKienDangKyCollection collection = new PhuKienDangKyCollection();
        PhuKienDangKy pkDangKy = new PhuKienDangKy();
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        //-----------------------------------------------------------------------------------------
        public PhuKienGCManageForm()
        {
            InitializeComponent();
        }
        private void BindHopDong()
        {
            DataTable dt;
            {
                string where = string.Format("madoanhnghiep='{0}'", GlobalSettings.MA_DON_VI);
                HopDong HD = new HopDong();
                dt = HopDong.SelectDynamic(where, "").Tables[0];
            }
            cbHopDong.DataSource = dt;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            BindHopDong();
        }

        //-----------------------------------------------------------------------------------------
        private void PhuKienGCManageForm_Load(object sender, EventArgs e)
        {
            //An nut Xac nhan
            XacNhanThongTin.Visible = Janus.Windows.UI.InheritableBoolean.False;

            this.khoitao_DuLieuChuan();
            cbStatus.SelectedIndex = 0;
            btnSearch_Click(null, null);
            //dgList.DataSource = this.dmDangKy.DMCollection;
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            if (!(dgPhuKien.GetRows().Length > 0 && dgPhuKien.GetRow().RowType == RowType.Record))
            {
                //                showMsg("MSG_240233");
                return;
            }
            pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "PK";
            sendXML.master_id = pkDangKy.ID;
            string xmlCurrent = "";
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //MLMessages("Phụ kiện đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.","MSG_STN03","", false);
                return;
            }
            pkDangKy.LoadCollection();
            if (pkDangKy.PKCollection.Count == 0)
            {
                showMsg("MSG_2702021");
                //MLMessages("Bạn chưa nhập thông tin cho loại phụ kiện nên không khai báo tới hải quan được.","MSG_SEN12","", false);
                return;
            }
            string password = "";
            try
            {


                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = pkDangKy.WSSend(password);
                this.Cursor = Cursors.Default;
                sendXML.func = 1;
                sendXML.msg = xmlCurrent;
                sendXML.master_id = pkDangKy.ID;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    this.Cursor = Cursors.Default;
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setcommandstatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Phụ kiện . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //e.Row.Cells[].Text = this.LoaiPhuKien_GetName(e.Row.Cells[1].Text);            
            string st = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
            if (st == "-1")
                e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
            else if (st == "0")
                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
            else if (st == "1")
                e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
            else if (st == "10")
                e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
            else if (st == "11")
                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
            else if (st == "2")
                e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
            if (e.Row.Cells["NgayTiepNhan"].Value != null)
            {
                DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value);
                if (dt.Year <= 1900)
                    e.Row.Cells["NgayTiepNhan"].Text = "";
            }
            if (e.Row.Cells["NgayTiepNhan"].Value != null)
            {
                DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayPhuKien"].Value);
                if (dt.Year <= 1900)
                    e.Row.Cells["NgayPhuKien"].Text = "";
            }
            HopDong hd = new HopDong();
            hd.ID = Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value);
            hd = HopDong.Load(hd.ID);
            e.Row.Cells["HopDong_ID"].Text = hd.SoHopDong;
            this.setcommandstatus();
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            Company.Interface.KDT.GC.PhuKienGCSendForm pk = new Company.Interface.KDT.GC.PhuKienGCSendForm();
            pk.HD.ID = Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value);
            pk.HD = HopDong.Load(pk.HD.ID);
            pk.pkdk.ID = Convert.ToInt64(e.Row.Cells["ID"].Value);
            pk.pkdk.Load();
            pk.pkdk.LoadCollection();
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien loaiPK in pk.pkdk.PKCollection)
            {
                loaiPK.Load();
                loaiPK.LoadCollection();
            }
            pk.OpenType = OpenFormType.Edit;
            pk.ShowDialog();
            btnSearch_Click(null, null);
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "KhaiBao": this.SendV3(); /* send()*/ ; break;
                case "cmdGet": this.FeedBackV3() /* nhandulieuHD()*/; break;
                case "Huy": this.CanceldV3() /* HuyNhieuHD()*/ ; break;
                //DATLMQ comment ngày 24/03/2011
                //case "XacNhanThongTin": LaySoTiepNhanDT(); break;
                case "XacNhanThongTin": this.FeedBackV3() /* nhandulieuHD()*/; break;
                case "cmdCSDaDuyet": ChuyenTrangThai(); break;
                case "cmdXuatPhuKien":
                    XuatDuLieuPhuKienChoPhongKhai();
                    break;
                case "InPhieuTN": this.inPhieuTN(); break;
            }
        }
        private void inPhieuTN()
        {
            if (!(dgPhuKien.GetRows().Length > 0 && dgPhuKien.GetRow().RowType == RowType.Record)) return;
            GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "PHỤ KIỆN";
            Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    PhuKienDangKy pkDangKySelected = (PhuKienDangKy)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = pkDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = pkDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();

        }
        private void ChuyenTrangThai()
        {
            if (dgPhuKien.SelectedItems.Count > 0)
            {
                PhuKienDangKyCollection pkdkColl = new PhuKienDangKyCollection();
                foreach (GridEXSelectedItem grItem in dgPhuKien.SelectedItems)
                {
                    pkdkColl.Add((PhuKienDangKy)grItem.GetRow().DataRow);
                }

                for (int i = 0; i < pkdkColl.Count; i++)
                {
                    pkdkColl[i].LoadCollection();

                    //string msg = "Bạn có muốn chuyển trạng thái của danh sách được chọn sang đã duyệt không?";
                    //msg += "\n\nSố thứ tự của danh sách: " + pkdkColl[i].ID.ToString();
                    //msg += "\nCó " + pkdkColl[i].PKCollection.Count.ToString() + " phụ kiện được đăng ký";

                    string[] args = new string[2];
                    args[0] = pkdkColl[i].ID.ToString();
                    args[1] = pkdkColl[i].PKCollection.Count.ToString();
                    if (showMsg("MSG_0203072", args, true) == "Yes")
                    {
                        if (pkdkColl[i].TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            pkdkColl[i].NgayTiepNhan = DateTime.Today;
                        }
                        //pkdkColl[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        pkdkColl[i].updateTrangThaiDuLieu();
                        Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty,
                            pkdkColl[i].ID, pkdkColl[i].GUIDSTR, Company.KDT.SHARE.Components.MessageTypes.PhuKien,
                             Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                             string.Format("Trước khi chuyển GUIDSTR={0},Trạng thái xử lý {1}", pkdkColl[i].GUIDSTR, pkdkColl[i].TrangThaiXuLy));

                    }
                }

                this.btnSearch_Click(null, null);
            }
            else
            {
                showMsg("MSG_240233");
                //MLMessages("Chưa có dữ liệu được chọn!","MSG_WRN11","", false);
            }
        }

        private void XuatDuLieuPhuKienChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
            if (items.Count == 0)
            {
                showMsg("MSG_2702064");
                //MLMessages("Chưa chọn danh sách phụ kiện cần xuất ra file.","MSG_WRN11","", false);
                return;
            }
            try
            {
                PhuKienDangKyCollection col = new PhuKienDangKyCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(PhuKienDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sophukien = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            PhuKienDangKy PK = (PhuKienDangKy)i.GetRow().DataRow;
                            PK.LoadCollection();
                            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK in PK.PKCollection)
                            {
                                LoaiPK.LoadCollection();
                            }
                            HopDong HD = new HopDong();
                            HD.ID = PK.HopDong_ID;
                            HD = HopDong.Load(HD.ID);
                            PK.SoHopDong = HD.SoHopDong;
                            col.Add(PK);
                            sophukien++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    showMsg("MSG_2702065", sophukien);
                    //MLMessages("Xuất ra file thành công " + sophukien + " phụ kiện.", "MSG_EXC05", sophukien.ToString(), false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }

        }


        private void nhandulieuHD()
        {
            NhanDuLieuCTMenu_Click(null, null);
        }
        private bool checkExistHD(string soHD)
        {
            HopDong hopdong = new HopDong();
            List<HopDong> hdcol = new List<HopDong>();
            hdcol = HopDong.SelectCollectionAll();
            //hdcol= hopdong.SelectCollectionDynamic("","") ;
            foreach (HopDong hd in hdcol)
            {
                if (soHD.Equals(hd.SoHopDong.Trim()))
                    return true;

            }
            return false;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {

            //FileStream fs = new FileStream("C:\\phu kien 04.xml", FileMode.Open);
            //XmlSerializer serializer = new XmlSerializer(typeof(PhuKienDangKyCollection));
            //PhuKienDangKyCollection pk = (PhuKienDangKyCollection)serializer.Deserialize(fs);
            //foreach (PhuKienDangKy p in pk)
            //    p.InsertPhuKienDongBoDuLieu();



            string where = "1=1  and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' ";
            if (txtSoTiepNhan.Text.Trim().Length > 0)
            {
                where += " and sotiepnhan=" + txtSoTiepNhan.Text;
            }
            if (cbHopDong.Text.Length > 0)
            {
                if (!checkExistHD(cbHopDong.Text.Trim().ToString()))
                {
                    showMsg("MSG_2702066");
                    //MLMessages("Không tồn tại số hợp đồng này","MSG_WRN10","", false);
                    return;
                }
                where += " and HopDong_ID=" + cbHopDong.Value.ToString();


            }

            if (txtNamTiepNhan.Text.Trim().Length > 0)
            {
                where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text;
            }
            if (cbStatus.Text.Length > 0)
                where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
            collection = pkDangKy.SelectCollectionDynamic(where, "");
            dgPhuKien.DataSource = collection;
            setcommandstatus();
        }

        private void khaibaoHD()
        {
            khaibaoCTMenu_Click(null, null);
        }
        private void HuyNhieuHD()
        {
            HuyCTMenu_Click(null, null);
        }
        private void khaibaoCTMenu_Click(object sender, EventArgs e)
        {
            string xmlCurrent = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            try
            {
                if (dgPhuKien.GetRow() != null)
                {
                    pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
                    {
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkDangKy.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_240203");
                            //MLMessages("Phụ kiện này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_STN03","", false);
                            return;
                        }
                    }

                    pkDangKy.Load();
                    pkDangKy.LoadCollection();
                    foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien loaiPK in pkDangKy.PKCollection)
                    {
                        loaiPK.Load();
                        loaiPK.LoadCollection();
                    }
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = pkDangKy.WSSend(wsForm.txtMatKhau.Text.Trim());
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "PK";
                    sendXML.master_id = pkDangKy.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 1;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(wsForm.txtMatKhau.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setcommandstatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Phụ kiện . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        private void NhanDuLieuCTMenu_Click(object sender, EventArgs e)
        {
            string xmlCurrent = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                if (dgPhuKien.GetRow() != null)
                {
                    pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
                    {
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkDangKy.ID;
                        if (sendXML.Load())
                        {
                            //showMsg("MSG_240203");
                            ////MLMessages("Phụ kiện này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_STN03","", false);
                            //return;
                        }

                        if (GlobalSettings.PassWordDT == "")
                        {
                            wsForm.ShowDialog(this);
                            if (!wsForm.IsReady) return;
                        }
                        if (GlobalSettings.PassWordDT != "")
                            password = GlobalSettings.PassWordDT;
                        else
                            password = wsForm.txtMatKhau.Text.Trim();
                        this.Cursor = Cursors.WaitCursor;
                        //DATLMQ comment ngày 24/03/2011
                        //xmlCurrent = pkDangKy.WSDownLoad(password);
                        this.Cursor = Cursors.Default;
                        sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkDangKy.ID;
                        sendXML.msg = xmlCurrent;
                        sendXML.func = 2;
                        xmlCurrent = "";
                        sendXML.InsertUpdate();
                        LayPhanHoi(password);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setcommandstatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void HuyCTMenu_Click(object sender, EventArgs e)
        {
            WSForm wsForm = new WSForm();
            string password = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {
                if (dgPhuKien.GetRow() != null)
                {
                    pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;

                    {
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkDangKy.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_240203");
                            //MLMessages("Phụ kiện này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_STN03","", false);
                            return;
                        }
                    }

                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    string xmlCurrent = pkDangKy.WSCancel(password);
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "PK";
                    sendXML.master_id = pkDangKy.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN12", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setcommandstatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }
        private void setcommandstatus()
        {
            if (cbStatus.SelectedValue.ToString() == "-1" || cbStatus.SelectedValue.ToString() == "2" || cbStatus.SelectedValue.ToString() == "10")
            {
                Huy.Enabled = Huy1.Enabled = Huy2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = cbStatus.SelectedValue.ToString() == "-1" ? Janus.Windows.UI.InheritableBoolean.True :
                     Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgPhuKien.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdXuatPhuKien.Enabled = cmdXuatPhuKien1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = true;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (cbStatus.SelectedValue.ToString() == "0" || cbStatus.SelectedValue.ToString() == "11")
            {
                Huy.Enabled = Huy1.Enabled = Huy2.Enabled = cbStatus.SelectedValue.ToString() == "0" ? Janus.Windows.UI.InheritableBoolean.True :
                     Janus.Windows.UI.InheritableBoolean.False;
                cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgPhuKien.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdXuatPhuKien.Enabled = cmdXuatPhuKien1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else
            {
                Huy.Enabled = Huy1.Enabled = Huy2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgPhuKien.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatPhuKien.Enabled = cmdXuatPhuKien1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;
                cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
        }

        private void LaySoTiepNhanDT()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            if (dgPhuKien.GetRow() != null)
            {
                pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                {
                    sendXML.LoaiHS = "PK";
                    sendXML.master_id = pkDangKy.ID;
                    if (!sendXML.Load())
                    {
                        showMsg("MSG_240203");
                        //MLMessages("Phụ kiện không có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN03","", false);
                        return;
                    }
                }
                string xmlCurrent = "";
                WSForm wsForm = new WSForm();
                string password = "";
                try
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    {
                        xmlCurrent = pkDangKy.LayPhanHoi(password, sendXML.msg);
                    }
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        doc.LoadXml(xmlCurrent);
                        node = doc.SelectSingleNode("Megs");
                        if (node == null)
                        {
                            if (showMsg("MSG_STN02", true) == "Yes")
                            //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                            {
                                this.Refresh();
                                LayPhanHoi(password);
                            }
                            return;
                        }
                    }

                    if (sendXML.func == 1)
                    {
                        showMsg("MSG_SEN02", pkDangKy.SoTiepNhan);
                        //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkDangKy.SoTiepNhan,"MSG_SEN02",pkDangKy.SoTiepNhan.ToString(), false);                        
                    }
                    else if (sendXML.func == 3)
                    {
                        showMsg("MSG_2702017");
                        //MLMessages("Đã hủy phụ kiện này","MSG_CAN01","", false);                      
                    }
                    else if (sendXML.func == 2)
                    {
                        if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_2702018", mess);
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.","MSG_SEN03","", false);                            
                        }
                        else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_SEN04", mess);
                            //MLMessages("Hải quan chưa xử lý !","MSG_SEN04","", false);
                        }
                        else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            showMsg("MSG_SEN05");
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu","MSG_SEN05","", false);                            
                        }
                    }
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                    this.btnSearch_Click(null, null);
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    HangDoi hd = new HangDoi();
                                //    hd.ID = TKMD.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //    hd.TrangThai = TKMD.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                //    hd.PassWord = pass;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //MLMessages("Có lỗi trong khai báo : " + msg[0],"MSG_SEN09",msg[0], false);
                                if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                                {
                                    sendXML.Delete();
                                    setcommandstatus();
                                }
                                else
                                {
                                    GlobalSettings.PassWordDT = "";
                                }
                            }
                        }
                        else
                        {
                            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                showMsg("MSG_WRN13", ex.Message.Trim());
                                //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                                sendXML.Delete();
                                setcommandstatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                                showMsg("MSG_2702004", ex.Message);
                                //ShowMessage(ex.Message, false);
                            }
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }
        private void LayPhanHoi(string pass)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            string xmlCurrent = "";
            try
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkDangKy.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                //DATLMQ bổ sung Nhận dữ liệu ngày 24/03/2011
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, pkDangKy.GUIDSTR.Trim()));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN.Trim();
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));
                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = pkDangKy.GUIDSTR.Trim().ToUpper();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                xmlCurrent = pkDangKy.TQDTLayPhanHoi(pass, xml.InnerXml);
                //xmlCurrent = pkDangKy.LayPhanHoi(pass, sendXML.msg);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", pkDangKy.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkDangKy.SoTiepNhan, "MSG_SEN02", pkDangKy.SoTiepNhan.ToString(), false);                    
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702019");
                    //MLMessages("Đã hủy danh sách phụ kiện này","MSG_CAN01","", false);                    
                }
                else if (sendXML.func == 2)
                {
                    if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";

                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        //showMsg("MSG_2702018", mess);
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.","MSG_SEN03","", false); 
                        try
                        {
                            if (pkDangKy.PhanLuong == "1")
                                ShowMessageTQDT("Phụ kiện đã được phân luồng XANH \r\n\r\n HƯỚNG DẪN CỦA HẢI QUAN : " + pkDangKy.HUONGDAN.ToString(), false);
                            else if (pkDangKy.PhanLuong == "2")
                                ShowMessageTQDT("Phụ kiện đã được phân luồng VÀNG \r\n\r\n HƯỚNG DẪN CỦA HẢI QUAN : " + pkDangKy.HUONGDAN.ToString(), false);
                            else
                                ShowMessageTQDT("Phụ kiện đã được phân luồng ĐỎ \r\n\r\n HƯỚNG DẪN CỦA HẢI QUAN : " + pkDangKy.HUONGDAN.ToString(), false);
                        }
                        catch (Exception ex1k) { ShowMessageTQDT("Lỗi :" + ex1k.Message, false); }
                    }
                    else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        //showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa xử lý danh sách sản phẩm này!","MSG_SEN04","", false);

                        if (mess != "")
                            ShowMessage(mess, false);
                        else
                            ShowMessage("Hải quan chưa xử lý danh sách phụ kiện này!", false);
                    }
                    else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        showMsg("MSG_2702068", string.Empty);
                    }

                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                this.btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setcommandstatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message.Trim());
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                            sendXML.Delete();
                            setcommandstatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgPhuKien_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        PhuKienDangKy pkdkDelete = (PhuKienDangKy)i.GetRow().DataRow;

                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkdkDelete.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn có muốn xóa không?", "MSG_STN09", j.ToString(), false);
                            //if (st == "Yes")
                            //{
                            //    if (pkdkDelete.ID > 0)
                            //    {
                            //        pkdkDelete.Delete();
                            //    }
                            //}
                        }
                        else
                        {
                            if (pkdkDelete.ID > 0)
                            {
                                pkdkDelete.Delete();
                            }
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (collection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
            int j = 0;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {

                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        PhuKienDangKy pkdkDelete = (PhuKienDangKy)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkdkDelete.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn có muốn xóa không?", "MSG_STN09",j.ToString(), false);
                            //if (st == "Yes")
                            //{
                            //    if (pkdkDelete.ID > 0)
                            //    {
                            //        pkdkDelete.Delete();
                            //    }
                            //}
                        }
                        else
                        {
                            if (pkdkDelete.ID > 0)
                            {
                                pkdkDelete.Delete();
                            }

                            //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                            try
                            {
                                string whereLog = "1 = 1";
                                whereLog += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", pkdkDelete.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.PhuKien);
                                List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(whereLog, "");
                                if (listLog.Count > 0)
                                {
                                    long idLog = listLog[0].IDLog;
                                    string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                    long idDK = listLog[0].ID_DK;
                                    string guidstr = listLog[0].GUIDSTR_DK;
                                    string userKhaiBao = listLog[0].UserNameKhaiBao;
                                    DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                    string userSuaDoi = GlobalSettings.UserLog;
                                    DateTime ngaySuaDoi = DateTime.Now;
                                    string ghiChu = listLog[0].GhiChu;
                                    bool isDelete = true;
                                    Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                                userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                                }
                            }
                            catch (Exception ex)
                            {
                                ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                                return;
                            }
                        }
                    }
                }

                string where = "1=1  and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' ";
                if (txtSoTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and sotiepnhan=" + txtSoTiepNhan.Text;
                }
                if (cbHopDong.Text.Length > 0)
                {
                    where += " and HopDong_ID=" + cbHopDong.Value.ToString();
                }

                if (txtNamTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text;
                }
                if (cbStatus.Text.Length > 0)
                    where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
                collection = pkDangKy.SelectCollectionDynamic(where, "");
                dgPhuKien.DataSource = collection;
                setcommandstatus();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtNamTiepNhan_Click(object sender, EventArgs e)
        {

        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            if (dgPhuKien.GetRow() != null)
            {
                PhuKienDangKy npl = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
                //Globals.ShowKetQuaXuLyBoSung(npl.GUIDSTR);
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = npl.ID;
                form.DeclarationIssuer = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG;
                form.ShowDialog(this);
            }
        }
        #region V3 - 14/03/2012
        private void SendV3()
        {
            if (dgPhuKien.GetRow() == null)
            {
                ShowMessage("Bạn chưa chọn danh sách phụ kiện.", false);
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            if (!(dgPhuKien.GetRows().Length > 0 && dgPhuKien.GetRow().RowType == RowType.Record))
            {
                //                showMsg("MSG_240233");
                return;
            }
            pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
            sendXML.master_id = pkDangKy.ID;
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //MLMessages("Phụ kiện đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.","MSG_STN03","", false);
                return;
            }
            pkDangKy.LoadCollection();
            if (pkDangKy.PKCollection.Count == 0)
            {
                showMsg("MSG_2702021");
                //MLMessages("Bạn chưa nhập thông tin cho loại phụ kiện nên không khai báo tới hải quan được.","MSG_SEN12","", false);
                return;
            }
            HopDong HD = new HopDong();
            HD.ID = pkDangKy.HopDong_ID;
            HD = HopDong.Load(HD.ID);
            HD.LoadCollection();

            try
            {
                pkDangKy.GUIDSTR = Guid.NewGuid().ToString();
                //pkDangKy.LoadCollection();
                //foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien loaiPK in pkDangKy.PKCollection)
                //{
                //    loaiPK.Load();
                //    loaiPK.LoadCollection();
                //}
                Company.KDT.SHARE.Components.GC_PhuKien pk = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferPhuKien(pkDangKy, HD);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = pkDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkDangKy.MaHaiQuan),
                                     Identity = pkDangKy.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                                    Function = pk.Function,
                                    Reference = pkDangKy.GUIDSTR,
                                }
                                ,
                                pk);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(pkDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoPhuKien);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
                    sendXML.master_id = pkDangKy.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.PhuKienSendHandler(pkDangKy, ref msgInfor, e);

        }
        private void FeedBackV3()
        {
            if (dgPhuKien.GetRow() == null)
            {
                ShowMessage("Bạn chưa chọn danh sách phụ kiện.", false);
                return;
            }
            pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                Reference = pkDangKy.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = pkDangKy.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkDangKy.MaHaiQuan.Trim()),
                                              Identity = pkDangKy.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        setcommandstatus();
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (pkDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || pkDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                }
            }
            setcommandstatus();
        }
        private void CanceldV3()
        {
            //Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
            //sendXML.master_id = pkDangKy.ID;
            //if (sendXML.Load())
            //{
            //    showMsg("MSG_WRN05");
            //    cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
            //    return;
            //}
            if (dgPhuKien.GetRow() == null)
            {
                ShowMessage("Bạn chưa chọn phụ kiện", false);
                return;
            }
            pkDangKy = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            Company.KDT.SHARE.Components.DeclarationBase phukien = Company.GC.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.GC_PHU_KIEN_HOP_DONG, pkDangKy.GUIDSTR, pkDangKy.SoTiepNhan, pkDangKy.MaHaiQuan, pkDangKy.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = pkDangKy.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkDangKy.MaHaiQuan),
                                 Identity = pkDangKy.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = pkDangKy.GUIDSTR,
                            }
                            ,
                            phukien);
            SendMessageForm sendForm = new SendMessageForm();
            pkDangKy.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO;
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && pkDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                sendForm.Message.XmlSaveMessage(pkDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.HQHuyKhaiBaoToKhai);
                cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //sendXML.func = 3;
                //sendXML.InsertUpdate();
                pkDangKy.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }

        #endregion
    }
}