﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Infragistics.Excel;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.SXXK
{
    public partial class AMA_To_VNACC_Form : BaseForm
    {
        public DataSet ds = new DataSet();
        public AMA_To_VNACC_Form()
        {
            InitializeComponent();
        }
        
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            //if(e.Row.RowType == RowType.Record)    
            //    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void XuatEX() 
        {
            //return;
            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets.Add("ToKhaiAMA");
            ws.Columns[0].Width = 5000;
            ws.Columns[1].Width = 2000;
            ws.Columns[2].Width = 5000;
            ws.Columns[3].Width = 5000;
            ws.Columns[4].Width = 5000;
            ws.Columns[5].Width = 3000;
            ws.Columns[6].Width = 2000;
            ws.Columns[7].Width = 10000;
            ws.Columns[8].Width = 5000;
            ws.Columns[9].Width = 5000;
            ws.Columns[10].Width = 10000;
            ws.Columns[11].Width = 5000;
            ws.Columns[12].Width = 5000;
            WorksheetRowCollection wsrc = ws.Rows;
            WorksheetRow wsr0 = ws.Rows[0];
            wsr0.Cells[0].Value = "Số TK AMA";
            wsr0.Cells[1].Value = "TKMD_ID";
            wsr0.Cells[2].Value = "Số Tờ Khai";
            wsr0.Cells[3].Value = "Ngày Đăng Ký";
            wsr0.Cells[4].Value = "Mã Loại Hình";
            wsr0.Cells[5].Value = "Trạng Thái";
            wsr0.Cells[6].Value = "STT";
            wsr0.Cells[7].Value = "Mô tả hàng trước";
            wsr0.Cells[8].Value = "Số lượng hàng trước";
            wsr0.Cells[9].Value = "Trị giá tính thuế trước";
            wsr0.Cells[10].Value = "Mô tả hàng sau";
            wsr0.Cells[11].Value = "Số lượng hàng sau";
            wsr0.Cells[12].Value = "Trị giá tính thuế sau";
            DataTable table = this.ds.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                WorksheetRow wsr = ws.Rows[i + 1];
                wsr.Cells[0].Value = table.Rows[i]["SoToKhaiBoSung"].ToString();
                wsr.Cells[1].Value = table.Rows[i]["TKMD_ID"].ToString();
                wsr.Cells[2].Value = table.Rows[i]["SoToKhai"].ToString();
                wsr.Cells[3].Value = table.Rows[i]["NgayKhaiBao"].ToString();
                wsr.Cells[4].Value = table.Rows[i]["MaLoaiHinh"].ToString();
                wsr.Cells[5].Value = table.Rows[i]["TrangThaiXuLy"].ToString();
                wsr.Cells[6].Value = table.Rows[i]["SoThuTuDongHangTrenToKhaiGoc"].ToString();
                wsr.Cells[7].Value = table.Rows[i]["MoTaHangHoaTruocKhiKhaiBoSung"].ToString();
                wsr.Cells[8].Value = table.Rows[i]["SoLuongTinhThueTruocKhiKhaiBoSung"].ToString();
                wsr.Cells[9].Value = table.Rows[i]["TriGiaTinhThueTruocKhiKhaiBoSung"].ToString();
                wsr.Cells[10].Value = table.Rows[i]["MoTaHangHoaSauKhiKhaiBoSung"].ToString();
                wsr.Cells[11].Value = table.Rows[i]["SoLuongTinhThueSauKhiKhaiBoSung"].ToString();
                wsr.Cells[12].Value = table.Rows[i]["TriGiaTinhThueSauKhiKhaiBoSung"].ToString();
            }
            DialogResult rs = new DialogResult();
            saveFileDialog1.FileName = "DanhSachHangTK_AMA_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            rs = saveFileDialog1.ShowDialog(this);
            if (rs == DialogResult.OK)
            {
                string fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
                
            }
        }
        private void CapNhat() 
        {
            DialogResult rs = new DialogResult();
            rs = MessageBox.Show("Bạn có muốn xuất excel trước khi cập nhật?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rs == DialogResult.Yes)
            {
                XuatEX();
            }
            rs = MessageBox.Show("Bạn có muốn cập nhật các mặt hàng đã khai báo sửa đổi bổ sung có thay đổi về số lượng và tên hàng?","Thông báo",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (rs == DialogResult.Yes) 
            {
                decimal SoToKhai = 0;
                int TKMD_ID = 0;
                int stt = 0;
                string maHangTruoc = "";
                string maHangSau = "";
                string tenHangTruoc = "";
                string tenHangSau = "";
                decimal soLuongTruoc = 0;
                decimal soLuongSau = 0;
                decimal triGiaTruoc = 0;
                decimal triGiaSau = 0;
                string motaTruoc = "";
                string motaSau = "";
                string sql = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    SoToKhai = Convert.ToDecimal(dr["SoToKhai"].ToString());
                    TKMD_ID = Convert.ToInt32(dr["TKMD_ID"].ToString());
                    stt = Convert.ToInt32(dr["SoThuTuDongHangTrenToKhaiGoc"].ToString());
                    motaTruoc = dr["MoTaHangHoaTruocKhiKhaiBoSung"].ToString();
                    motaSau = dr["MoTaHangHoaSauKhiKhaiBoSung"].ToString();
                    soLuongTruoc = Convert.ToDecimal(dr["SoLuongTinhThueTruocKhiKhaiBoSung"].ToString());
                    soLuongSau = Convert.ToDecimal(dr["SoLuongTinhThueSauKhiKhaiBoSung"].ToString());
                    triGiaTruoc = Convert.ToDecimal(dr["TriGiaTinhThueTruocKhiKhaiBoSung"].ToString());
                    triGiaSau = Convert.ToDecimal(dr["TriGiaTinhThueSauKhiKhaiBoSung"].ToString());
                    if (motaTruoc.Contains("#&"))
                    {
                        string[] temp = motaTruoc.Split(new string[] { "#&" }, StringSplitOptions.None);
                        if (temp.Length > 0)
                        {
                            maHangTruoc = temp[0];
                            tenHangTruoc = temp[1];
                        }

                    }
                    else 
                    {
                        maHangTruoc = "";
                        tenHangTruoc = motaTruoc;
                    }

                    if (motaSau.Contains("#&"))
                    {
                        string[] temp = motaTruoc.Split(new string[] { "#&" }, StringSplitOptions.None);
                        if (temp.Length > 0)
                        {
                            maHangSau = temp[0];
                            tenHangSau = temp[1];
                        }
                    }
                    else
                    {
                        maHangSau = "";
                        tenHangSau = motaSau;
                    }
                    
                    if (!string.IsNullOrEmpty(maHangTruoc) && !string.IsNullOrEmpty(tenHangTruoc))
                    {
                        KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                        sql = string.Format(" MaHangHoa = '{0}' AND TenHang = N'{1}' AND TKMD_ID = {2} AND SoLuong1 = {3} AND SoDong = '{4}'", maHangTruoc, tenHangTruoc,TKMD_ID, soLuongTruoc,stt);
                        sql = sql.Replace(',', '.');
                        try
                        {
                            hmd = KDT_VNACC_HangMauDich.SelectCollectionDynamic(sql, "")[0];
                            hmd.SoLuong1 = soLuongSau;
                            hmd.TenHang = tenHangSau;
                            hmd.MaHangHoa = maHangSau;
                            hmd.TriGiaTinhThueS = triGiaSau;
                            if (soLuongSau == 0)
                                hmd.Delete();
                            else
                                hmd.Update();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            //throw;
                        }
                        
                        
                    }
                    else 
                    {
                        KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                        sql = string.Format(" TKMD_ID = {0} AND SoLuong1 = {1} AND SoDong = '{2}'", TKMD_ID, soLuongTruoc,stt);
                        sql = sql.Replace(',','.');
                        try
                        {
                            hmd = KDT_VNACC_HangMauDich.SelectCollectionDynamic(sql, "")[0];
                            hmd.SoLuong1 = soLuongSau;
                            hmd.TenHang = tenHangSau;
                            hmd.TriGiaTinhThueS = triGiaSau;
                            if (soLuongSau == 0)
                                hmd.Delete();
                            else
                                hmd.Update();
                        }
                        catch (Exception ex)
                        {

                        }
                        
                    }
                    
                }

                MessageBox.Show("Cập nhật thành công! Lưu ý chức năng chỉ cập nhật cho các tờ khai VNACC. Cần thực hiên thêm chức năng CẬP NHẬT THANH KHOẢN đối với các tờ khai này","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            
        }
        
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key) 
            {
                case "XuatExcel":
                    XuatEX();
                    break;
                case "cmdCapNhatAMA":
                    CapNhat();
                    break;
            }
        }

        private void AMA_To_VNACC_Form_Load(object sender, EventArgs e)
        {
            this.dgList.DataSource = ds.Tables[0];
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
    }
}