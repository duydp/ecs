﻿using System;

using System.Data;
using System.Threading;
using System.Globalization;
using System.Resources;
using Company.Controls.CustomValidation;
using Company.KDT.SHARE.QuanLyChungTu;
/* LanNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.Utils;
using Company.KD.BLL.KDT.SXXK;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif
namespace Company.Interface
{
    public partial class AddContainerForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public Container container;
        public AddContainerForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (save())
            {
                txtSealNo.Clear();
                txtSoHieuContainer.Clear();
                container = null;
            }
        }

        private void AddContainerForm_Load(object sender, EventArgs e)
        {
            cbTrangThai.SelectedIndex = 0;
            cbLoaiContainer.SelectedIndex = 0;
            if (container != null)
            {
                txtSealNo.Text = container.Seal_No;
                txtSoHieuContainer.Text = container.SoHieu;
                cbLoaiContainer.SelectedValue = container.LoaiContainer;
                cbTrangThai.SelectedValue = container.Trang_thai;
            }

            bool isEnable = TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO
                            || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                            || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                            || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;

            btnSaveNew.Enabled = btnSave.Enabled = isEnable;
        }
        private bool checkSoHieu(string soHieu)
        {
            foreach (Container c in TKMD.VanTaiDon.ContainerCollection)
                if (c.SoHieu.Trim().ToUpper() == soHieu.ToUpper().Trim())
                {
                    return true;
                }
            return false;
        }
        private bool save()
        {
            cvError.Validate();
            if (!cvError.IsValid)
            {
                return false;
            }
            if (TKMD.VanTaiDon == null)
            {
                TKMD.VanTaiDon = new VanDon();
            }

            TKMD.VanTaiDon.ContainerCollection.Remove(container);

            if (checkSoHieu(txtSoHieuContainer.Text))
            {
                if (container != null)
                    TKMD.VanTaiDon.ContainerCollection.Add(container);
                TKMD.VanTaiDon.ContainerCollection.Remove(container);
                ShowMessage("Đã có số hiệu Container này.", false);
                return false;
            }
            if (container == null)
                container = new Container();
            container.SoHieu = txtSoHieuContainer.Text.Trim();
            container.LoaiContainer = cbLoaiContainer.SelectedValue.ToString();
            container.Seal_No = txtSealNo.Text.Trim();
            container.Trang_thai = Convert.ToInt32(cbTrangThai.SelectedValue.ToString());
            TKMD.VanTaiDon.ContainerCollection.Add(container);
            if (cbLoaiContainer.SelectedValue.ToString() == "2")
                TKMD.SoContainer20++;
            else
                TKMD.SoContainer40++;
            return true;

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (save())
                this.Close();
        }






    }
}