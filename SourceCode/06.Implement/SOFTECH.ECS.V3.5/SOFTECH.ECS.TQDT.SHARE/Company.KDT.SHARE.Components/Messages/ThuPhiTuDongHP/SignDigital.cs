﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class SignDigital
    {
        /// <summary>
        /// Số serial của chữ ký số
        /// </summary>
       [XmlElement("serial")]
       public string Serial { get; set; }

        /// <summary>
       /// Thông tin Cert của chữ ký số  
        /// </summary>
       [XmlElement("certString")]
       public string CertString { get; set; }

        /// <summary>
       /// Tên chủ thể của chữ ký số
        /// </summary>
       [XmlElement("subject")]
       public string Subject { get; set; }

        /// <summary>
        /// Đơn vị cấp chữ ký số
        /// </summary>
       [XmlElement("issuer")]
       public string Issuer { get; set; }

        /// <summary>
        /// Ngày bắt đầu của chữ ký số
        /// </summary>
       [XmlElement("validFrom")]
       public string ValidFrom { get; set; }

        /// <summary>
        /// Ngày hết hạn của chữ ký số
        /// </summary>
       [XmlElement("validTo")]
       public string ValidTo { get; set; }
    }
}
