using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.BLL.SXXK.ThanhKhoan
{
    public partial class NPLTaiXuat : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected int _LanThanhLy;
        protected string _MaDoanhNghiep = String.Empty;
        protected int _SoToKhai;
        protected string _MaLoaiHinh = String.Empty;
        protected short _NamDangKy;
        protected string _MaHaiQuan = String.Empty;
        protected string _MaNPL = String.Empty;
        protected decimal _SoLuongTaiXuat;
        protected int _SoToKhaiXuat;
        protected string _MaLoaiHinhXuat = String.Empty;
        protected short _NamDangKyXuat;
        protected string _MaHaiQuanXuat = String.Empty;
        protected bool _DaXuLy;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public int LanThanhLy
        {
            set { this._LanThanhLy = value; }
            get { return this._LanThanhLy; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public short NamDangKy
        {
            set { this._NamDangKy = value; }
            get { return this._NamDangKy; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
        public decimal SoLuongTaiXuat
        {
            set { this._SoLuongTaiXuat = value; }
            get { return this._SoLuongTaiXuat; }
        }
        public int SoToKhaiXuat
        {
            set { this._SoToKhaiXuat = value; }
            get { return this._SoToKhaiXuat; }
        }
        public string MaLoaiHinhXuat
        {
            set { this._MaLoaiHinhXuat = value; }
            get { return this._MaLoaiHinhXuat; }
        }
        public short NamDangKyXuat
        {
            set { this._NamDangKyXuat = value; }
            get { return this._NamDangKyXuat; }
        }
        public string MaHaiQuanXuat
        {
            set { this._MaHaiQuanXuat = value; }
            get { return this._MaHaiQuanXuat; }
        }
        public bool DaXuLy
        {
            set { this._DaXuLy = value; }
            get { return this._DaXuLy; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTaiXuat"))) this._SoLuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) this._SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) this._MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) this._NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) this._MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) this._DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                return true;
            }
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public NPLTaiXuatCollection SelectCollectionBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            NPLTaiXuatCollection collection = new NPLTaiXuatCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLTaiXuat entity = new NPLTaiXuat();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTaiXuat"))) entity.SoLuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------
        public NPLTaiXuatCollection SelectCollectionBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan()
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            NPLTaiXuatCollection collection = new NPLTaiXuatCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLTaiXuat entity = new NPLTaiXuat();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTaiXuat"))) entity.SoLuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------
        public NPLTaiXuatCollection SelectCollectionBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat_AND_MaHaiQuanXuat()
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat_AND_MaHaiQuanXuat";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);

            NPLTaiXuatCollection collection = new NPLTaiXuatCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLTaiXuat entity = new NPLTaiXuat();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTaiXuat"))) entity.SoLuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public DataSet SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan()
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public DataSet SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat_AND_MaHaiQuanXuat()
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat_AND_MaHaiQuanXuat";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public NPLTaiXuatCollection SelectCollectionAll()
        {
            NPLTaiXuatCollection collection = new NPLTaiXuatCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                NPLTaiXuat entity = new NPLTaiXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTaiXuat"))) entity.SoLuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public NPLTaiXuatCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            NPLTaiXuatCollection collection = new NPLTaiXuatCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                NPLTaiXuat entity = new NPLTaiXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTaiXuat"))) entity.SoLuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@SoLuongTaiXuat", SqlDbType.Decimal, this._SoLuongTaiXuat);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);
            this.db.AddInParameter(dbCommand, "@DaXuLy", SqlDbType.Bit, this._DaXuLy);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(NPLTaiXuatCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLTaiXuat item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, NPLTaiXuatCollection collection)
        {
            foreach (NPLTaiXuat item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@SoLuongTaiXuat", SqlDbType.Decimal, this._SoLuongTaiXuat);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);
            this.db.AddInParameter(dbCommand, "@DaXuLy", SqlDbType.Bit, this._DaXuLy);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(NPLTaiXuatCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLTaiXuat item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@SoLuongTaiXuat", SqlDbType.Decimal, this._SoLuongTaiXuat);
            this.db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            this.db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, this._NamDangKyXuat);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, this._MaHaiQuanXuat);
            this.db.AddInParameter(dbCommand, "@DaXuLy", SqlDbType.Bit, this._DaXuLy);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(NPLTaiXuatCollection collection, SqlTransaction transaction)
        {
            foreach (NPLTaiXuat item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(NPLTaiXuatCollection collection, SqlTransaction transaction)
        {
            foreach (NPLTaiXuat item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(NPLTaiXuatCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLTaiXuat item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion
        public NPLTaiXuat SelectEntityBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuanAndMaNPL(SqlTransaction transaction)
        {
            string spName = "p_SXXK_TL_NPLTaiXuat_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);

            IDataReader reader = null;
            if (transaction == null)
                reader = this.db.ExecuteReader(dbCommand);
            else
                reader = this.db.ExecuteReader(dbCommand, transaction);
            NPLTaiXuat entity = null;
            while (reader.Read())
            {
                entity = new NPLTaiXuat();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTaiXuat"))) entity.SoLuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaXuLy"))) entity.DaXuLy = reader.GetBoolean(reader.GetOrdinal("DaXuLy"));                
            }
            reader.Close();
            return entity;
        }
        public void InsertUpdateTransaction2(SqlTransaction transaction)
        {
            NPLTaiXuat tmp = this.SelectEntityBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuanAndMaNPL(transaction);
            if (tmp == null)
                this.InsertTransaction(transaction);
            else
            {
                this.ID = tmp.ID;
                this.UpdateTransaction(transaction);
            }
        }
        public static void updateDatabase(DataSet ds, SqlTransaction transaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NPLTaiXuat npl = new NPLTaiXuat();
                npl._DaXuLy = Convert.ToBoolean(row["DA_XL"].ToString());
                npl._LanThanhLy = Convert.ToInt32(row["LAN_TL"].ToString());
                npl._MaDoanhNghiep = (row["MA_DV"].ToString());
                npl._MaHaiQuan = (row["MA_HQ"].ToString());
                npl._MaHaiQuanXuat = (row["MA_HQTX"].ToString());
                npl._MaLoaiHinh = (row["MA_LH"].ToString());
                npl._MaLoaiHinhXuat = (row["MA_LHTX"].ToString());
                npl._MaNPL = (row["MA_NPL"].ToString());
                npl._NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                npl._NamDangKyXuat = Convert.ToInt16(row["NAMDKTX"].ToString());
                npl._SoLuongTaiXuat = Convert.ToDecimal(row["LUONG_TX"]);
                npl._SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                npl._SoToKhaiXuat = Convert.ToInt32(row["SOTKTX"].ToString());
                npl.InsertUpdateTransaction(transaction);
            }
        }

    }
}