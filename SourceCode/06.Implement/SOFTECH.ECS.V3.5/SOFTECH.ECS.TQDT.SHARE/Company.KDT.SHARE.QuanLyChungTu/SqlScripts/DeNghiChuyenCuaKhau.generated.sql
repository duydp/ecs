-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@SoVanDon varchar(500),
	@NgayVanDon datetime,
	@ThoiGianDen datetime,
	@DiaDiemKiemTra nvarchar(500),
	@TuyenDuong nvarchar(500),
	@PTVT_ID varchar(3),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_DeNghiChuyenCuaKhau]
(
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID]
)
VALUES 
(
	@ThongTinKhac,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan,
	@SoVanDon,
	@NgayVanDon,
	@ThoiGianDen,
	@DiaDiemKiemTra,
	@TuyenDuong,
	@PTVT_ID
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]
	@ID bigint,
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@SoVanDon varchar(500),
	@NgayVanDon datetime,
	@ThoiGianDen datetime,
	@DiaDiemKiemTra nvarchar(500),
	@TuyenDuong nvarchar(500),
	@PTVT_ID varchar(3)
AS

UPDATE
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
SET
	[ThongTinKhac] = @ThongTinKhac,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[ThoiGianDen] = @ThoiGianDen,
	[DiaDiemKiemTra] = @DiaDiemKiemTra,
	[TuyenDuong] = @TuyenDuong,
	[PTVT_ID] = @PTVT_ID
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]
	@ID bigint,
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@SoVanDon varchar(500),
	@NgayVanDon datetime,
	@ThoiGianDen datetime,
	@DiaDiemKiemTra nvarchar(500),
	@TuyenDuong nvarchar(500),
	@PTVT_ID varchar(3)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_DeNghiChuyenCuaKhau] 
		SET
			[ThongTinKhac] = @ThongTinKhac,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[ThoiGianDen] = @ThoiGianDen,
			[DiaDiemKiemTra] = @DiaDiemKiemTra,
			[TuyenDuong] = @TuyenDuong,
			[PTVT_ID] = @PTVT_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_DeNghiChuyenCuaKhau]
		(
			[ThongTinKhac],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan],
			[SoVanDon],
			[NgayVanDon],
			[ThoiGianDen],
			[DiaDiemKiemTra],
			[TuyenDuong],
			[PTVT_ID]
		)
		VALUES 
		(
			@ThongTinKhac,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan,
			@SoVanDon,
			@NgayVanDon,
			@ThoiGianDen,
			@DiaDiemKiemTra,
			@TuyenDuong,
			@PTVT_ID
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID]
FROM
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID]
FROM
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID]
FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Saturday, December 01, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong],
	[PTVT_ID]
FROM
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]	

GO

