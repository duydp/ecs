﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.Interface;
using System.Data;
using Logger;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class Mau15_TT38_ChiTiet : DevExpress.XtraReports.UI.XtraReport
    {
        public int NamDangKy;
        string MaHang = "";
        int STT = 0;
        decimal SoLuong_G_F = 0;
        decimal TriGia_G_F = 0;
        decimal TriGiaNhapTrongKy_G_F = 0;
        decimal TriGiaXuatTrongKy_G_F = 0;
        decimal TriGiaTonCuoiKy_G_F = 0;
        public DataTable tb_Mau15 = new DataTable();
        public Mau15_TT38_ChiTiet()
        {
            InitializeComponent();
        }
        public void BindReport(string where)
        {
            NamDangKy = Int32.Parse(where);
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontBCXNT);
            DataTable dtTable = new DataTable();
            dtTable = tb_Mau15;//GetDataSource();
            Int32 index = 0;
            string MaHang = "";
            dtTable.Columns.Add(new DataColumn("STT"));
            for (int i = 0; i < dtTable.Rows.Count; i++)
            {
                
                if (dtTable.Rows[i]["MaNPL"].ToString() != MaHang) 
                {
                    index++;
                }
                MaHang = dtTable.Rows[i]["MaNPL"].ToString();
                dtTable.Rows[i]["STT"] = index;
                
            }
            DetailReport.DataSource = dtTable;


            lbTenToChuc.Text = GlobalSettings.TEN_DON_VI;
            lbMaSo.Text = GlobalSettings.MA_DON_VI;
            lbDiaChi.Text = GlobalSettings.DIA_CHI;
            lbNam.Text = NamDangKy.ToString();

            this.GroupHeader2.Visible = true;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("SoTaiKhoan"),
                //new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                //new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                
            });
            this.GroupHeader1.Visible = false;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                //new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                //new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                
            });
            txtSoTaiKhoan_G_H_2.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            //lbNam.DataBindings.Add("Text",DetailReport.DataSource,"NamDangKy");
            txtSTT_G_F.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            //txtTaiKhoan.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            txtTenNPL.DataBindings.Add("Text",DetailReport.DataSource,"TenNPL");
            txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource,"MaNPL");
            lbl_MaHang_G_H.DataBindings.Add("Text", DetailReport.DataSource, "MaNPL");
            txt_MaNPL_G_F.DataBindings.Add("Text", DetailReport.DataSource, "MaNPL");
            txtDonGia.DataBindings.Add("Text", DetailReport.DataSource,"DonGiaKB");
            txtSoToKhai.DataBindings.Add("Text", DetailReport.DataSource,"SoToKhaiVNACCS");
            txtNgayDangKy.DataBindings.Add("Text", DetailReport.DataSource,"NgayDangKy");
            txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "TonDauKy", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            txtTriGia.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTTTonDauKy", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource,"TriGiaTTNhapTrongKy", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource,"TriGiaTTXuatTrongKy", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource,"TriGiaTTTonCuoiKy", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource,"GhiChu");
        }
        private DataTable GetDataSource()
        {
            try
            {
                string spName = "[p_sxxk_Mau15_Details]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void txt_SoTaiKhoan_G_H_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if(txt_SoTaiKhoan_G_H)
        }

        private void txtSTT_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            STT++;
            txtSTT_G_F.Text = STT.ToString();
        }

        private void txt_MaNPL_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void txt_MaNPL_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void lbl_STT_G_H_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtTenTaiKhoan_AfterPrint(object sender, EventArgs e)
        {
            if (txtSoTaiKhoan_G_H_2.Text == "152")
                txtTenTaiKhoan.Text = "Nguyên liệu , vật liệu nhập khẩu";
            else
            {
                txtTenTaiKhoan.Text = "Thanh Phẩm , vật liệu nhập khẩu";
                STT = 0;
            }
        }

        private void txtTenTaiKhoan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (txtSoTaiKhoan_G_H_2.Text == "152")
                txtTenTaiKhoan.Text = "Nguyên liệu , vật liệu nhập khẩu";
            else
            {
                txtTenTaiKhoan.Text = "Thành phẩm được sản xuất từ nguồn nhập khẩu";
                STT = 0;
            }
        }

        private void txtNhapTrongKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                decimal c = Convert.ToDecimal(txtNhapTrongKy.Text);
                txtNhapTrongKy.Text = c.ToString("N");
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            try
            {
                decimal c = Convert.ToDecimal(lbl.Text);
                if (c == 0)
                {
                    lbl.Text = c.ToString("N0");
                }
                else
                    lbl.Text = c.ToString("N4");
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            
        }

        private void txtNgayDangKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                DateTime d = Convert.ToDateTime(txtNgayDangKy.Text);
                txtNgayDangKy.Text = d.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            
        }

        private void txtSoLuong_G_F_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                

            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                
            }
            
        }

        private void txtSoLuong_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if(SoLuong_G_F==0)
                    txtSoLuong_G_F.Text = "0";
                else
                    txtSoLuong_G_F.Text = SoLuong_G_F.ToString("N4");
                SoLuong_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTonDauKy_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                SoLuong_G_F += Convert.ToDecimal(txtTonDauKy.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTriGia_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                TriGia_G_F += Convert.ToDecimal(txtTriGia.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void txtTriGiaTonDauKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (TriGia_G_F==0)
                    txtTriGiaTonDauKy.Text = "0";
                else
                    txtTriGiaTonDauKy.Text = TriGia_G_F.ToString("N4");
                TriGia_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void txtTriGiaTonDauKy_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtTriGiaNhapTrongKy_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    TriGiaNhapTrongKy_G_F += Convert.ToDecimal(txtTriGiaNhapTrongKy.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGiaNhapTrongKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (TriGiaNhapTrongKy_G_F==0)
                    txtTriGiaNhapTrongKy.Text = "0";
                else
                    txtTriGiaNhapTrongKy.Text = TriGiaNhapTrongKy_G_F.ToString("N4");
                TriGiaNhapTrongKy_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTriGiaXuatTrongKy_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (TriGiaXuatTrongKy_G_F==0)
                    txtTriGiaXuatTrongKy_G_F.Text = "0";
                else
                    txtTriGiaXuatTrongKy_G_F.Text = TriGiaXuatTrongKy_G_F.ToString("N4");
                TriGiaXuatTrongKy_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTriGiaXuatTrongKy_G_F_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    txtTriGiaXuatTrongKy_G_F.Text = TriGiaXuatTrongKy_G_F.ToString("N4");
            //    TriGiaXuatTrongKy_G_F = 0;
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGiaTonCuoiKy_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (TriGiaTonCuoiKy_G_F==0)
                    txtTriGiaTonCuoiKy_G_F.Text = "0";
                else
                    txtTriGiaTonCuoiKy_G_F.Text = TriGiaTonCuoiKy_G_F.ToString("N4");
                TriGiaTonCuoiKy_G_F = 0;
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTriGiaTonCuoiKy_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtTonCuoiKy_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                TriGiaTonCuoiKy_G_F += Convert.ToDecimal(txtTonCuoiKy.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtNhapTrongKy_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                TriGiaNhapTrongKy_G_F += Convert.ToDecimal(txtTriGiaNhapTrongKy.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtXuatTrongKy_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                TriGiaXuatTrongKy_G_F += Convert.ToDecimal(txtXuatTrongKy.Text);
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtDonGia_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtDonGia.Text)) 
                {
                    decimal c = Convert.ToDecimal(txtDonGia.Text);
                    if (c == 0)
                    {
                        txtDonGia.Text = "0";
                    }
                    else
                        txtDonGia.Text = c.ToString("N4");
                }
                
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            
        }

    }
}
