using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class HangVanDonDetail : ICloneable
    {
        #region Properties.

        public long ID { set; get; }
        public long VanDon_ID { set; get; }
        public string MaNguyenTe { set; get; }
        public string MaChuyenNganh { set; get; }
        public string GhiChu { set; get; }
        public long HMD_ID { set; get; }
        public int SoThuTuHang { set; get; }
        public string MaHS { set; get; }
        public string MaPhu { set; get; }
        public string TenHang { set; get; }
        public string NuocXX_ID { set; get; }
        public string DVT_ID { set; get; }
        public decimal SoLuong { set; get; }
        public double DonGiaKB { set; get; }
        public double TriGiaKB { set; get; }
        public string LoaiKien { set; get; }
        public string SoHieuContainer { set; get; }
        public string SoHieuKien { set; get; }
        public double TrongLuong { set; get; }
        public decimal SoKien { set; get; }
        public double TrongLuongTinh { set; get; }
        public string KichThuocHoacTheTich { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<HangVanDonDetail> ConvertToCollection(IDataReader reader)
        {
            IList<HangVanDonDetail> collection = new List<HangVanDonDetail>();
            while (reader.Read())
            {
                HangVanDonDetail entity = new HangVanDonDetail();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanDon_ID"))) entity.VanDon_ID = reader.GetInt64(reader.GetOrdinal("VanDon_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenTe"))) entity.MaNguyenTe = reader.GetString(reader.GetOrdinal("MaNguyenTe"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaChuyenNganh"))) entity.MaChuyenNganh = reader.GetString(reader.GetOrdinal("MaChuyenNganh"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("HMD_ID"))) entity.HMD_ID = reader.GetInt64(reader.GetOrdinal("HMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDouble(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDouble(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKien"))) entity.LoaiKien = reader.GetString(reader.GetOrdinal("LoaiKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuContainer"))) entity.SoHieuContainer = reader.GetString(reader.GetOrdinal("SoHieuContainer"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuKien"))) entity.SoHieuKien = reader.GetString(reader.GetOrdinal("SoHieuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDouble(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongTinh"))) entity.TrongLuongTinh = reader.GetDouble(reader.GetOrdinal("TrongLuongTinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("KichThuocHoacTheTich"))) entity.KichThuocHoacTheTich = reader.GetString(reader.GetOrdinal("KichThuocHoacTheTich"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static bool Find(IList<HangVanDonDetail> collection, long id)
        {
            foreach (HangVanDonDetail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }

        public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_HangVanDonDetail VALUES(@VanDon_ID, @MaNguyenTe, @MaChuyenNganh, @GhiChu, @HMD_ID, @SoThuTuHang, @MaHS, @MaPhu, @TenHang, @NuocXX_ID, @DVT_ID, @SoLuong, @DonGiaKB, @TriGiaKB, @LoaiKien, @SoHieuContainer, @SoHieuKien, @TrongLuong, @SoKien, @TrongLuongTinh, @KichThuocHoacTheTich)";
            string update = "UPDATE t_KDT_HangVanDonDetail SET VanDon_ID = @VanDon_ID, MaNguyenTe = @MaNguyenTe, MaChuyenNganh = @MaChuyenNganh, GhiChu = @GhiChu, HMD_ID = @HMD_ID, SoThuTuHang = @SoThuTuHang, MaHS = @MaHS, MaPhu = @MaPhu, TenHang = @TenHang, NuocXX_ID = @NuocXX_ID, DVT_ID = @DVT_ID, SoLuong = @SoLuong, DonGiaKB = @DonGiaKB, TriGiaKB = @TriGiaKB, LoaiKien = @LoaiKien, SoHieuContainer = @SoHieuContainer, SoHieuKien = @SoHieuKien, TrongLuong = @TrongLuong, SoKien = @SoKien, TrongLuongTinh = @TrongLuongTinh, KichThuocHoacTheTich = @KichThuocHoacTheTich WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_HangVanDonDetail WHERE ID = @ID";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@VanDon_ID", SqlDbType.BigInt, "VanDon_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaNguyenTe", SqlDbType.VarChar, "MaNguyenTe", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaChuyenNganh", SqlDbType.VarChar, "MaChuyenNganh", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DonGiaKB", SqlDbType.Float, "DonGiaKB", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TriGiaKB", SqlDbType.Float, "TriGiaKB", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@LoaiKien", SqlDbType.NVarChar, "LoaiKien", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoHieuContainer", SqlDbType.NVarChar, "SoHieuContainer", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoHieuKien", SqlDbType.NVarChar, "SoHieuKien", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TrongLuong", SqlDbType.Float, "TrongLuong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoKien", SqlDbType.Decimal, "SoKien", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TrongLuongTinh", SqlDbType.Float, "TrongLuongTinh", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@KichThuocHoacTheTich", SqlDbType.NVarChar, "KichThuocHoacTheTich", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@VanDon_ID", SqlDbType.BigInt, "VanDon_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaNguyenTe", SqlDbType.VarChar, "MaNguyenTe", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaChuyenNganh", SqlDbType.VarChar, "MaChuyenNganh", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DonGiaKB", SqlDbType.Float, "DonGiaKB", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TriGiaKB", SqlDbType.Float, "TriGiaKB", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@LoaiKien", SqlDbType.NVarChar, "LoaiKien", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoHieuContainer", SqlDbType.NVarChar, "SoHieuContainer", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoHieuKien", SqlDbType.NVarChar, "SoHieuKien", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TrongLuong", SqlDbType.Float, "TrongLuong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoKien", SqlDbType.Decimal, "SoKien", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TrongLuongTinh", SqlDbType.Float, "TrongLuongTinh", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@KichThuocHoacTheTich", SqlDbType.NVarChar, "KichThuocHoacTheTich", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_HangVanDonDetail VALUES(@VanDon_ID, @MaNguyenTe, @MaChuyenNganh, @GhiChu, @HMD_ID, @SoThuTuHang, @MaHS, @MaPhu, @TenHang, @NuocXX_ID, @DVT_ID, @SoLuong, @DonGiaKB, @TriGiaKB, @LoaiKien, @SoHieuContainer, @SoHieuKien, @TrongLuong, @SoKien, @TrongLuongTinh, @KichThuocHoacTheTich)";
            string update = "UPDATE t_KDT_HangVanDonDetail SET VanDon_ID = @VanDon_ID, MaNguyenTe = @MaNguyenTe, MaChuyenNganh = @MaChuyenNganh, GhiChu = @GhiChu, HMD_ID = @HMD_ID, SoThuTuHang = @SoThuTuHang, MaHS = @MaHS, MaPhu = @MaPhu, TenHang = @TenHang, NuocXX_ID = @NuocXX_ID, DVT_ID = @DVT_ID, SoLuong = @SoLuong, DonGiaKB = @DonGiaKB, TriGiaKB = @TriGiaKB, LoaiKien = @LoaiKien, SoHieuContainer = @SoHieuContainer, SoHieuKien = @SoHieuKien, TrongLuong = @TrongLuong, SoKien = @SoKien, TrongLuongTinh = @TrongLuongTinh, KichThuocHoacTheTich = @KichThuocHoacTheTich WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_HangVanDonDetail WHERE ID = @ID";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@VanDon_ID", SqlDbType.BigInt, "VanDon_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaNguyenTe", SqlDbType.VarChar, "MaNguyenTe", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaChuyenNganh", SqlDbType.VarChar, "MaChuyenNganh", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DonGiaKB", SqlDbType.Float, "DonGiaKB", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TriGiaKB", SqlDbType.Float, "TriGiaKB", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@LoaiKien", SqlDbType.NVarChar, "LoaiKien", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoHieuContainer", SqlDbType.NVarChar, "SoHieuContainer", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoHieuKien", SqlDbType.NVarChar, "SoHieuKien", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TrongLuong", SqlDbType.Float, "TrongLuong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoKien", SqlDbType.Decimal, "SoKien", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TrongLuongTinh", SqlDbType.Float, "TrongLuongTinh", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@KichThuocHoacTheTich", SqlDbType.NVarChar, "KichThuocHoacTheTich", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@VanDon_ID", SqlDbType.BigInt, "VanDon_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaNguyenTe", SqlDbType.VarChar, "MaNguyenTe", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaChuyenNganh", SqlDbType.VarChar, "MaChuyenNganh", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoThuTuHang", SqlDbType.Int, "SoThuTuHang", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaPhu", SqlDbType.VarChar, "MaPhu", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.Char, "NuocXX_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DonGiaKB", SqlDbType.Float, "DonGiaKB", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TriGiaKB", SqlDbType.Float, "TriGiaKB", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@LoaiKien", SqlDbType.NVarChar, "LoaiKien", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoHieuContainer", SqlDbType.NVarChar, "SoHieuContainer", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoHieuKien", SqlDbType.NVarChar, "SoHieuKien", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TrongLuong", SqlDbType.Float, "TrongLuong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoKien", SqlDbType.Decimal, "SoKien", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TrongLuongTinh", SqlDbType.Float, "TrongLuongTinh", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@KichThuocHoacTheTich", SqlDbType.NVarChar, "KichThuocHoacTheTich", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static HangVanDonDetail Load(long id)
        {
            const string spName = "[dbo].[p_KDT_HangVanDonDetail_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<HangVanDonDetail> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<HangVanDonDetail> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<HangVanDonDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HangVanDonDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HangVanDonDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HangVanDonDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HangVanDonDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertHangVanDonDetail(long vanDon_ID, string maNguyenTe, string maChuyenNganh, string ghiChu, long hMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, double donGiaKB, double triGiaKB, string loaiKien, string soHieuContainer, string soHieuKien, double trongLuong, decimal soKien, double trongLuongTinh, string kichThuocHoacTheTich)
        {
            HangVanDonDetail entity = new HangVanDonDetail();
            entity.VanDon_ID = vanDon_ID;
            entity.MaNguyenTe = maNguyenTe;
            entity.MaChuyenNganh = maChuyenNganh;
            entity.GhiChu = ghiChu;
            entity.HMD_ID = hMD_ID;
            entity.SoThuTuHang = soThuTuHang;
            entity.MaHS = maHS;
            entity.MaPhu = maPhu;
            entity.TenHang = tenHang;
            entity.NuocXX_ID = nuocXX_ID;
            entity.DVT_ID = dVT_ID;
            entity.SoLuong = soLuong;
            entity.DonGiaKB = donGiaKB;
            entity.TriGiaKB = triGiaKB;
            entity.LoaiKien = loaiKien;
            entity.SoHieuContainer = soHieuContainer;
            entity.SoHieuKien = soHieuKien;
            entity.TrongLuong = trongLuong;
            entity.SoKien = soKien;
            entity.TrongLuongTinh = trongLuongTinh;
            entity.KichThuocHoacTheTich = kichThuocHoacTheTich;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangVanDonDetail_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@VanDon_ID", SqlDbType.BigInt, VanDon_ID);
            db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
            db.AddInParameter(dbCommand, "@MaChuyenNganh", SqlDbType.VarChar, MaChuyenNganh);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@SoHieuContainer", SqlDbType.NVarChar, SoHieuContainer);
            db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuongTinh", SqlDbType.Float, TrongLuongTinh);
            db.AddInParameter(dbCommand, "@KichThuocHoacTheTich", SqlDbType.NVarChar, KichThuocHoacTheTich);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<HangVanDonDetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangVanDonDetail item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateHangVanDonDetail(long id, long vanDon_ID, string maNguyenTe, string maChuyenNganh, string ghiChu, long hMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, double donGiaKB, double triGiaKB, string loaiKien, string soHieuContainer, string soHieuKien, double trongLuong, decimal soKien, double trongLuongTinh, string kichThuocHoacTheTich)
        {
            HangVanDonDetail entity = new HangVanDonDetail();
            entity.ID = id;
            entity.VanDon_ID = vanDon_ID;
            entity.MaNguyenTe = maNguyenTe;
            entity.MaChuyenNganh = maChuyenNganh;
            entity.GhiChu = ghiChu;
            entity.HMD_ID = hMD_ID;
            entity.SoThuTuHang = soThuTuHang;
            entity.MaHS = maHS;
            entity.MaPhu = maPhu;
            entity.TenHang = tenHang;
            entity.NuocXX_ID = nuocXX_ID;
            entity.DVT_ID = dVT_ID;
            entity.SoLuong = soLuong;
            entity.DonGiaKB = donGiaKB;
            entity.TriGiaKB = triGiaKB;
            entity.LoaiKien = loaiKien;
            entity.SoHieuContainer = soHieuContainer;
            entity.SoHieuKien = soHieuKien;
            entity.TrongLuong = trongLuong;
            entity.SoKien = soKien;
            entity.TrongLuongTinh = trongLuongTinh;
            entity.KichThuocHoacTheTich = kichThuocHoacTheTich;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_HangVanDonDetail_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@VanDon_ID", SqlDbType.BigInt, VanDon_ID);
            db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
            db.AddInParameter(dbCommand, "@MaChuyenNganh", SqlDbType.VarChar, MaChuyenNganh);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@SoHieuContainer", SqlDbType.NVarChar, SoHieuContainer);
            db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuongTinh", SqlDbType.Float, TrongLuongTinh);
            db.AddInParameter(dbCommand, "@KichThuocHoacTheTich", SqlDbType.NVarChar, KichThuocHoacTheTich);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<HangVanDonDetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangVanDonDetail item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateHangVanDonDetail(long id, long vanDon_ID, string maNguyenTe, string maChuyenNganh, string ghiChu, long hMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, double donGiaKB, double triGiaKB, string loaiKien, string soHieuContainer, string soHieuKien, double trongLuong, decimal soKien, double trongLuongTinh, string kichThuocHoacTheTich)
        {
            HangVanDonDetail entity = new HangVanDonDetail();
            entity.ID = id;
            entity.VanDon_ID = vanDon_ID;
            entity.MaNguyenTe = maNguyenTe;
            entity.MaChuyenNganh = maChuyenNganh;
            entity.GhiChu = ghiChu;
            entity.HMD_ID = hMD_ID;
            entity.SoThuTuHang = soThuTuHang;
            entity.MaHS = maHS;
            entity.MaPhu = maPhu;
            entity.TenHang = tenHang;
            entity.NuocXX_ID = nuocXX_ID;
            entity.DVT_ID = dVT_ID;
            entity.SoLuong = soLuong;
            entity.DonGiaKB = donGiaKB;
            entity.TriGiaKB = triGiaKB;
            entity.LoaiKien = loaiKien;
            entity.SoHieuContainer = soHieuContainer;
            entity.SoHieuKien = soHieuKien;
            entity.TrongLuong = trongLuong;
            entity.SoKien = soKien;
            entity.TrongLuongTinh = trongLuongTinh;
            entity.KichThuocHoacTheTich = kichThuocHoacTheTich;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangVanDonDetail_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@VanDon_ID", SqlDbType.BigInt, VanDon_ID);
            db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
            db.AddInParameter(dbCommand, "@MaChuyenNganh", SqlDbType.VarChar, MaChuyenNganh);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@SoHieuContainer", SqlDbType.NVarChar, SoHieuContainer);
            db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
            db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, SoKien);
            db.AddInParameter(dbCommand, "@TrongLuongTinh", SqlDbType.Float, TrongLuongTinh);
            db.AddInParameter(dbCommand, "@KichThuocHoacTheTich", SqlDbType.NVarChar, KichThuocHoacTheTich);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<HangVanDonDetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangVanDonDetail item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteHangVanDonDetail(long id)
        {
            HangVanDonDetail entity = new HangVanDonDetail();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangVanDonDetail_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_HangVanDonDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<HangVanDonDetail> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangVanDonDetail item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion


        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
    }
}