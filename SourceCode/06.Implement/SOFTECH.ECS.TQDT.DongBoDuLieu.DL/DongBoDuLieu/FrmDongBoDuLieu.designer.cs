﻿namespace DongBoDuLieu
{
    partial class FrmDongBoDuLieu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDongBoDuLieu));
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel5 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel6 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.GridEX.GridEXLayout dgDaiLy_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.btnSynData = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label5 = new System.Windows.Forms.Label();
            this.timePanel1 = new DongBoDuLieu.Controls.TimePanel();
            this.uiStatusBar = new Janus.Windows.UI.StatusBar.UIStatusBar();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.bw = new System.ComponentModel.BackgroundWorker();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.btnResult = new Janus.Windows.EditControls.UIButton();
            this.chkOverwrite = new Janus.Windows.EditControls.UICheckBox();
            this.lbError = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.tabControl = new Janus.Windows.UI.Tab.UITab();
            this.tabDaiLy = new Janus.Windows.UI.Tab.UITabPage();
            this.dgDaiLy = new Janus.Windows.GridEX.GridEX();
            this.tabToKhai = new Janus.Windows.UI.Tab.UITabPage();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabDaiLy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDaiLy)).BeginInit();
            this.tabToKhai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.tabControl);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(943, 485);
            // 
            // btnSynData
            // 
            this.btnSynData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSynData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSynData.Image = ((System.Drawing.Image)(resources.GetObject("btnSynData.Image")));
            this.btnSynData.Location = new System.Drawing.Point(6, 78);
            this.btnSynData.Name = "btnSynData";
            this.btnSynData.Size = new System.Drawing.Size(90, 23);
            this.btnSynData.TabIndex = 2;
            this.btnSynData.Text = "Thực hiện";
            this.btnSynData.ToolTipText = "Lấy dữ liệu thông quan điện tử";
            this.btnSynData.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSynData.Click += new System.EventHandler(this.btnSynData_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(856, 80);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(81, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(6, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(910, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Lưu ý: Quá trình đồng bộ dữ liêu chỉ thực hiện chuyển (sao chép) các thông tin tờ" +
                " khai đã khai báo, đã duyệt và phân luồng.";
            // 
            // timePanel1
            // 
            this.timePanel1.BackColor = System.Drawing.Color.Transparent;
            this.timePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timePanel1.FromDate = new System.DateTime(2011, 1, 4, 0, 0, 0, 0);
            this.timePanel1.Location = new System.Drawing.Point(3, 18);
            this.timePanel1.Name = "timePanel1";
            this.timePanel1.Size = new System.Drawing.Size(937, 42);
            this.timePanel1.TabIndex = 6;
            this.timePanel1.TieuDe = "Thời gian Đồng bộ dữ liệu:";
            this.timePanel1.ToDate = new System.DateTime(2011, 1, 11, 0, 0, 0, 0);
            // 
            // uiStatusBar
            // 
            this.uiStatusBar.Location = new System.Drawing.Point(3, 107);
            this.uiStatusBar.Name = "uiStatusBar";
            uiStatusBarPanel5.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            uiStatusBarPanel5.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel5.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel5.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiStatusBarPanel5.Key = "barProgress";
            uiStatusBarPanel5.PanelType = Janus.Windows.UI.StatusBar.StatusBarPanelType.ProgressBar;
            uiStatusBarPanel5.ProgressBarValue = 0;
            uiStatusBarPanel5.Width = 848;
            uiStatusBarPanel6.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            uiStatusBarPanel6.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            uiStatusBarPanel6.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel6.Key = "barTime";
            uiStatusBarPanel6.ProgressBarValue = 0;
            uiStatusBarPanel6.Text = "00 : 00 : 00";
            uiStatusBarPanel6.ToolTipText = "Thời gian thực hiện";
            uiStatusBarPanel6.Width = 81;
            this.uiStatusBar.Panels.AddRange(new Janus.Windows.UI.StatusBar.UIStatusBarPanel[] {
            uiStatusBarPanel5,
            uiStatusBarPanel6});
            this.uiStatusBar.Size = new System.Drawing.Size(937, 23);
            this.uiStatusBar.TabIndex = 5;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this.btnSearch, "dinhmucmanageform.htm");
            this.helpProvider1.SetHelpNavigator(this.btnSearch, System.Windows.Forms.HelpNavigator.Topic);
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearch.Location = new System.Drawing.Point(636, 27);
            this.btnSearch.Name = "btnSearch";
            this.helpProvider1.SetShowHelp(this.btnSearch, true);
            this.btnSearch.Size = new System.Drawing.Size(89, 23);
            this.btnSearch.TabIndex = 17;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnResult
            // 
            this.btnResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnResult.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResult.Image = ((System.Drawing.Image)(resources.GetObject("btnResult.Image")));
            this.btnResult.Location = new System.Drawing.Point(102, 78);
            this.btnResult.Name = "btnResult";
            this.btnResult.Size = new System.Drawing.Size(116, 23);
            this.btnResult.TabIndex = 3;
            this.btnResult.Text = "Kết quả xử lý";
            this.btnResult.ToolTipText = "Lấy dữ liệu thông quan điện tử";
            this.btnResult.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnResult.Click += new System.EventHandler(this.btnResult_Click);
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkOverwrite.AutoSize = true;
            this.chkOverwrite.BackColor = System.Drawing.Color.Transparent;
            this.chkOverwrite.Location = new System.Drawing.Point(224, 80);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(131, 19);
            this.chkOverwrite.TabIndex = 18;
            this.chkOverwrite.Text = "Ghi đè dữ liệu đã có.";
            // 
            // lbError
            // 
            this.lbError.BackColor = System.Drawing.Color.Transparent;
            this.lbError.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbError.ForeColor = System.Drawing.Color.Red;
            this.lbError.Location = new System.Drawing.Point(6, 46);
            this.lbError.Name = "lbError";
            this.lbError.Size = new System.Drawing.Size(913, 18);
            this.lbError.TabIndex = 19;
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.timePanel1);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(943, 63);
            this.uiGroupBox4.TabIndex = 38;
            this.uiGroupBox4.Text = "Thông tin đồng bộ dữ liệu";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.lbError);
            this.uiGroupBox1.Controls.Add(this.chkOverwrite);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.uiStatusBar);
            this.uiGroupBox1.Controls.Add(this.btnSynData);
            this.uiGroupBox1.Controls.Add(this.btnResult);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 352);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(943, 133);
            this.uiGroupBox1.TabIndex = 39;
            this.uiGroupBox1.Text = "Qúa trình xử lý đồng bộ dữ liệu";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // tabControl
            // 
            this.tabControl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 63);
            this.tabControl.Name = "tabControl";
            this.tabControl.SettingsKey = "tabControl";
            this.tabControl.Size = new System.Drawing.Size(943, 289);
            this.tabControl.TabIndex = 40;
            this.tabControl.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tabDaiLy,
            this.tabToKhai});
            this.tabControl.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            this.tabControl.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.tabControl_SelectedTabChanged);
            // 
            // tabDaiLy
            // 
            this.tabDaiLy.Controls.Add(this.dgDaiLy);
            this.tabDaiLy.Location = new System.Drawing.Point(1, 21);
            this.tabDaiLy.Name = "tabDaiLy";
            this.tabDaiLy.Size = new System.Drawing.Size(941, 267);
            this.tabDaiLy.TabStop = true;
            this.tabDaiLy.Text = "Danh sách đại lý";
            // 
            // dgDaiLy
            // 
            this.dgDaiLy.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgDaiLy.AlternatingColors = true;
            this.dgDaiLy.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgDaiLy.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgDaiLy.ColumnAutoResize = true;
            this.dgDaiLy.ColumnAutoSizeMode = Janus.Windows.GridEX.ColumnAutoSizeMode.ColumnHeader;
            dgDaiLy_DesignTimeLayout.LayoutString = resources.GetString("dgDaiLy_DesignTimeLayout.LayoutString");
            this.dgDaiLy.DesignTimeLayout = dgDaiLy_DesignTimeLayout;
            this.dgDaiLy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgDaiLy.GroupByBoxVisible = false;
            this.dgDaiLy.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgDaiLy.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgDaiLy.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgDaiLy.Location = new System.Drawing.Point(0, 0);
            this.dgDaiLy.Name = "dgDaiLy";
            this.dgDaiLy.RecordNavigator = true;
            this.dgDaiLy.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgDaiLy.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgDaiLy.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgDaiLy.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgDaiLy.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgDaiLy.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgDaiLy.Size = new System.Drawing.Size(941, 267);
            this.dgDaiLy.TabIndex = 0;
            this.dgDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgDaiLy.VisualStyleManager = this.vsmMain;
            // 
            // tabToKhai
            // 
            this.tabToKhai.Controls.Add(this.dgList);
            this.tabToKhai.Location = new System.Drawing.Point(1, 21);
            this.tabToKhai.Name = "tabToKhai";
            this.tabToKhai.Size = new System.Drawing.Size(941, 267);
            this.tabToKhai.TabStop = true;
            this.tabToKhai.Text = "Danh sách tờ khai";
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.dgList.ColumnAutoSizeMode = Janus.Windows.GridEX.ColumnAutoSizeMode.ColumnHeader;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(941, 267);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgList_KeyDown);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // FrmDongBoDuLieu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(943, 485);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(336, 377);
            this.Name = "FrmDongBoDuLieu";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đồng bộ dữ liệu ";
            this.Load += new System.EventHandler(this.FrmDongBoDuLieu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabDaiLy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDaiLy)).EndInit();
            this.tabToKhai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnSynData;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label5;
        private DongBoDuLieu.Controls.TimePanel timePanel1;
        private Janus.Windows.UI.StatusBar.UIStatusBar uiStatusBar;
        private System.Windows.Forms.Timer timer;
        private System.ComponentModel.BackgroundWorker bw;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.EditControls.UIButton btnResult;
        private Janus.Windows.EditControls.UICheckBox chkOverwrite;
        private System.Windows.Forms.Label lbError;
        private System.Windows.Forms.Timer timer1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.UI.Tab.UITab tabControl;
        private Janus.Windows.UI.Tab.UITabPage tabDaiLy;
        private Janus.Windows.GridEX.GridEX dgDaiLy;
        private Janus.Windows.UI.Tab.UITabPage tabToKhai;
        private Janus.Windows.GridEX.GridEX dgList;
    }
}