﻿

using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.Interface.Report;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.SXXK
{
    public partial class ToKhaiMauDichRegisterForm : BaseForm
    {
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        private ToKhaiMauDich tkMD = new ToKhaiMauDich();
        ToKhaiMauDichCollection tkmdCol = new ToKhaiMauDichCollection();//Để cập nhật ngày hoàn thành
        
        public string NhomLoaiHinh = "";
        public ToKhaiMauDichRegisterForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        //private void SetCommandStatus()
        //{

        //}
        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            dgList.RootTable.RowHeaderWidth = 70;

            khoitao_DuLieuChuan();
            //uiButton2.Visible = uiButton2.Enabled = true;
            try
            {
                txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
                this.search();
                dgList.RootTable.Columns["NGAY_THN_THX"].DefaultValue = DateTime.Now.Date;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
            {
                //uiButton2.Visible = false;
            }
           
        }

        private void updateNgayHoanThanh()
        {
            try
            {
                //linhhtn them cap nhat ngay hoan thanh = ngay dangky
                this.loadTKMDCollection();
                foreach (ToKhaiMauDich TKMD in this.tkmdCol)
                {
                    if (TKMD.NgayHoanThanh.Year == 1900)
                    {
                        TKMD.NgayHoanThanh = TKMD.NgayDangKy;
                        TKMD.Update();
                        new Company.BLL.KDT.SXXK.BCXuatNhapTon().UpdateNgayThucNhapXuatTransaction(TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NgayDangKy, TKMD.NGAY_THN_THX, TKMD.NgayHoanThanh, null);
                        new Company.BLL.KDT.SXXK.BCThueXNK().UpdateNgayThucNhapXuatTransaction(TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NgayDangKy, TKMD.NGAY_THN_THX, null);
                    }
                }
                //end linhhtn                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {
            //short namdk = 0;
            //if (txtNamTiepNhan.Text.Trim().Length > 0)
            //    namdk = Convert.ToInt16(txtNamTiepNhan.Text.Trim());
            //try
            //{


            //    WSForm wsForm = new WSForm();
            //    wsForm.ShowDialog(this);
            //    if (!wsForm.IsReady) return;
            //    this.Cursor = Cursors.WaitCursor;
            //    string strSTN = "";
            //    long count=ToKhaiMauDich.DongBoDuLieuHaiQuanAll(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, namdk);
            //    if (count > 0)
            //    {
            //        this.Cursor = Cursors.Default;
            //        ShowMessage("Nhận dữ liệu thành công " + count + " tờ khai", false);
            //        this.search();
            //    }
            //    else
            //    {
            //        this.Cursor = Cursors.Default;
            //        ShowMessage("Không có tờ khai nào cả.", false);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    this.Cursor = Cursors.Default;
            //    string st=ShowMessage(ex.Message+"\n Có lỗi khi thực hiện. Bạn có muốn đưa vào hàng đợi không", true);
            //    if (st == "Yes")
            //    {
            //        Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();                   
            //        hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
            //        hd.TrangThai = TrangThaiXuLy.DA_DUYET;
            //        hd.ChucNang = ChucNang.DONG_BO_DU_LIEU;
            //        hd.SoTK = 0;
            //        hd.NamDangKy = namdk;
            //        hd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;                 
            //        MainForm.AddToQueueForm(hd);
            //        MainForm.ShowQueueForm();
            //    }
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
        }
        private ToKhaiMauDich getTKMDByPrimaryKey(int sotk, string mahaiquan, string maloaihinh, int namdk)
        {
            foreach (ToKhaiMauDich tk in this.tkmdCollection)
            {
                if (tk.SoToKhai == sotk && tk.MaHaiQuan.Trim() == mahaiquan && tk.NamDangKy == namdk && tk.MaLoaiHinh == maloaihinh)
                    return tk;
            }
            return null;
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string sotk1 = e.Row.Cells["SoToKhai"].Text;
                    Int16 soKT = Convert.ToInt16(e.Row.Cells["TrangThai"].Text);
                    //if(sotk1.Contains())
                    decimal sotkVNACCS = Convert.ToDecimal(sotk1.Substring(0, sotk1.IndexOf("/")));
                    if(sotkVNACCS.ToString().Length > 10)
                    {
                        
                        System.Collections.Generic.List<int> sotkmd = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKByVNACCS(sotkVNACCS);
                        if(sotkmd != null && sotkmd.Count > 0)
                        {
                            sotk1 = sotk1.Replace(sotkVNACCS.ToString(), sotkmd[0].ToString());
                        }
                    }
                    Form[] forms = this.ParentForm.MdiChildren;
                    for (int i = 0; i < forms.Length; i++)
                    {
                        if (forms[i].Name.ToString().Equals(sotk1))
                        {
                            forms[i].Activate();
                            return;
                        }
                    }
                    int sotk = Convert.ToInt32(sotk1.Substring(0, sotk1.IndexOf("/")));
                    int namdk = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year;
                    string mahq = GlobalSettings.MA_HAI_QUAN;
                    string maloaihinh = e.Row.Cells["MaLoaiHinh"].Value.ToString();
                    // tkMD.TrangThai = tk

                    ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                    f.TKMD = this.getTKMDByPrimaryKey(sotk, mahq, maloaihinh, namdk);
                    f.NhomLoaiHinh = maloaihinh.Substring(0, 3);
                    f.MdiParent = this.ParentForm;
                    //f.Name = sotk1;
                    f.Show();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            try
            {

                this.Cursor = Cursors.WaitCursor;

                //Comment by Hungtq 30/07/2012. Chuc nang nay lam cham chuong trinh rat nhieu. 
                //Chuyen vao chưc nang TransgferDataToSXXK()
                //updateNgayHoanThanh();

                // Xây dựng điều kiện tìm kiếm.
                string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", ctrCoQuanHQ.Ma);
                where += " AND MaLoaiHinh LIKE '" + cbbLoaiToKhai.SelectedValue.ToString() + "%'";
                if (txtSoTiepNhan.TextLength > 0)
                {
                     where += " AND (SoToKhai = " + txtSoTiepNhan.Value + " OR LoaiVanDon like '%" + txtSoTiepNhan.Value + "%')";
                }

                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND NamDangKy = " + txtNamTiepNhan.Value;
                }
                if (Convert.ToInt16(txtThangDK.Value) > 0)
                {
                    where += " AND month(NgayDangKy)=" + Convert.ToInt16(txtThangDK.Value);
                }

                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                        where += " AND TenChuHang LIKE '%" + txtTenChuHang.Text.Trim() + "%'";
                }
                else
                {
                    where += " AND TenChuHang LIKE ''";
                }

                if (txtTenKhachHang.Text.Trim().Length > 0)
                {
                    where += " AND TenDonViDoiTac LIKE '%" + txtTenKhachHang.Text.Trim() + "%'";
                }
                if (this.NhomLoaiHinh != "")
                {
                    where += " AND MaLoaiHinh LIKE '%" + cbbLoaiToKhai.SelectedValue.ToString() + "%'";
                }
               
                where += " AND TrangThai != 10";
                // Thực hiện tìm kiếm.            
                this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");
                dgList.DataSource = this.tkmdCollection;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void loadTKMDCollection()
        {
            // Xây dựng điều kiện tìm kiếm.

            string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "'";
            where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);
            where += " AND MaLoaiHinh LIKE '" + cbbLoaiToKhai.SelectedValue.ToString() + "%'";
            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoToKhai = " + txtSoTiepNhan.Value;
            }

            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND NamDangKy = " + txtNamTiepNhan.Value;
            }
            if (Convert.ToInt16(txtThangDK.Value) > 0)
            {
                where += " AND month(NgayDangKy)=" + Convert.ToInt16(txtThangDK.Value);
            }

            if (txtTenChuHang.Text.Trim().Length > 0)
            {
                if (txtTenChuHang.Text.Trim() != "*")
                    where += " AND TenChuHang LIKE '%" + txtTenChuHang.Text.Trim() + "%'";
            }
            else
            {
                where += " AND TenChuHang LIKE ''";
            }

            if (txtTenKhachHang.Text.Trim().Length > 0)
            {
                where += " AND TenDonViDoiTac LIKE '%" + txtTenKhachHang.Text.Trim() + "%'";
            }
            if (this.NhomLoaiHinh != "") where += " AND MaLoaiHinh LIKE '%" + cbbLoaiToKhai.SelectedValue.ToString() + "%'";

            where += " AND (year(NgayHoanThanh) is null or year(NgayHoanThanh)=1900)";
            // Thực hiện tìm kiếm.            
            this.tkmdCol = new ToKhaiMauDich().SelectCollectionDynamic(where, "");

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string loaiHinh = (e.Row.Cells["MaLoaiHinh"].Value.ToString());
                DateTime time = DateTime.Now;
                try
                {
                    time = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                }
                catch { }
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                if (e.Row.Cells["MaLoaiHinh"].Text.Length==3)
                {
                    string MaHaiQuan = GlobalSettings.MA_HAI_QUAN.Substring(1, 2).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(0, 1).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(3, 1).ToUpper();
                    e.Row.Cells["SoToKhai"].Text = e.Row.Cells["LoaiVanDon"].Text + "/" + MaHaiQuan + "/" + time.Year.ToString();
                }
                else
                    e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + GlobalSettings.MA_HAI_QUAN + "/" + time.Year.ToString();
                if (loaiHinh.StartsWith("N"))
                {
                    switch (e.Row.Cells["ThanhLy"].Value.ToString().Trim())
                    {
                        case "":
                            //e.Row.Cells["ThanhLy"].Text = "Chưa thanh khoản";
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["ThanhLy"].Text = "Chưa thanh khoản";
                            }
                            else
                            {
                                e.Row.Cells["ThanhLy"].Text = "Not yet liquidated";

                            }
                            break;
                        case "H":
                            //e.Row.Cells["ThanhLy"].Text = "Đã thanh khoản hết";
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["ThanhLy"].Text = "Đã thanh khoản hết";
                            }
                            else
                            {
                                e.Row.Cells["ThanhLy"].Text = "liquidation of all";

                            }
                            break;
                        case "L":
                            //e.Row.Cells["ThanhLy"].Text = "Thanh khoản một phần";
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["ThanhLy"].Text = "Thanh khoản một phần";
                            }
                            else
                            {
                                e.Row.Cells["ThanhLy"].Text = " Partial liquidation";

                            }
                            break;
                    }
                }
                else
                    e.Row.Cells["ThanhLy"].Text = "";
                //string phanluong = e.Row.Cells["PhanLuong"].Text;
                //if (phanluong == "1" || phanluong == "3" || phanluong == "5")
                //    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng xanh";
                //else if (phanluong == "2" || phanluong == "4" || phanluong == "6" || phanluong == "10")
                //{
                //    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng đỏ";
                //}
                //else if (phanluong == "8" || phanluong == "12")
                //    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng vàng";


                DateTime ngayTNX = Convert.ToDateTime(e.Row.Cells["NGAY_THN_THX"].Value.ToString());
                DateTime ngayHT = Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value.ToString());
                DateTime ngayDK = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value.ToString());
                DateTime ngayHopDong = Convert.ToDateTime(e.Row.Cells["NgayHopDong"].Value.ToString());
                DateTime today = DateTime.Today;

                if (loaiHinh.StartsWith("N"))
                {
                    string thanhly = e.Row.Cells["ThanhLy"].Value.ToString().Trim();
                    if (thanhly == "H")
                    {
                        //e.Row.Cells["ChuY"].Text = "Tờ khai này đã thanh khoản hết";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["ChuY"].Text = "Tờ khai này đã thanh khoản hết";
                        }
                        else
                        {
                            e.Row.Cells["ChuY"].Text = "This declaration has liquidated ";

                        }
                        return;
                    }
                    TimeSpan time1 = today.Subtract(ngayDK);
                    int ngay = 275 - time1.Days;
                    //if (ngay > 0)
                    //    e.Row.Cells["ChuY"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                    //else if (ngay == 0)
                    //    e.Row.Cells["ChuY"].Text = "Hết hạn thanh khoản trong ngày hôm nay";                        
                    //else
                    //    e.Row.Cells["ChuY"].Text = "Đã hết hạn thanh khoản";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        if (ngay > 0)
                            e.Row.Cells["ChuY"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                        else if (ngay == 0)
                            e.Row.Cells["ChuY"].Text = "Hết hạn thanh khoản trong ngày hôm nay";
                        else
                            e.Row.Cells["ChuY"].Text = "Đã hết hạn thanh khoản";
                    }
                    else
                    {
                        if (ngay > 0)
                            e.Row.Cells["ChuY"].Text = "Remaining " + ngay.ToString() + " days have to liquidate";
                        else if (ngay == 0)
                            e.Row.Cells["ChuY"].Text = "Expired liquidation at today";
                        else
                            e.Row.Cells["ChuY"].Text = "Expired liquidation";

                    }

                }
                if (loaiHinh.StartsWith("X"))
                {

                    TimeSpan time1 = today.Subtract(ngayDK);
                    int ngay = 45 - time1.Days;
                    //if (ngay > 0)
                    //    e.Row.Cells["ChuY"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                    //else if (ngay == 0)
                    //    e.Row.Cells["ChuY"].Text = "Hết hạn thanh khoản trong ngày hôm nay";
                    //else
                    //    e.Row.Cells["ChuY"].Text = "Đã hết hạn thanh khoản";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        if (ngay > 0)
                            e.Row.Cells["ChuY"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                        else if (ngay == 0)
                            e.Row.Cells["ChuY"].Text = "Hết hạn thanh khoản trong ngày hôm nay";
                        else
                            e.Row.Cells["ChuY"].Text = "Đã hết hạn thanh khoản";
                    }
                    else
                    {
                        if (ngay > 0)
                            e.Row.Cells["ChuY"].Text = "Remaining " + ngay.ToString() + " days have to liquidate";
                        else if (ngay == 0)
                            e.Row.Cells["ChuY"].Text = "Expired liquidation at today";
                        else
                            e.Row.Cells["ChuY"].Text = "Expired liquidation";

                    }

                }
                if (ngayTNX.Year <= 1900)
                {
                    e.Row.Cells["NGAY_THN_THX"].Text = "";
                }
                if (ngayHT.Year <= 1900)
                {
                    e.Row.Cells["NgayHoanThanh"].Text = "";
                }
                if (ngayHopDong.Year <= 1900)
                {
                    e.Row.Cells["NgayHopDong"].Text = "";
                }
                if (e.Row.Cells["NgayHoaDonThuongMai"].Value != null && Convert.ToDateTime(e.Row.Cells["NgayHoaDonThuongMai"].Value).Year <= 1900)
                {
                    e.Row.Cells["NgayHoaDonThuongMai"].Text = "";
                }
            }
        }

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {

        }


        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_DropDown(object sender, ColumnActionEventArgs e)
        {

        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                ImportNgayGioToKhaiForm tk = new ImportNgayGioToKhaiForm();
                tk.ShowDialog(this);

                this.search();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void dgList_EditingCell(object sender, EditingCellEventArgs e)
        {
            //GridEXRow row = dgList.GetRow();
            //if (row.Cells["TrangThaiThanhKhoan"].Value.ToString() == "S")
            //{
            //    e.Cancel = true;
            //}
        }


        private void uiButton3_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count > 0)
            {
                //if(ShowMessage("Bạn có muốn tính thuế sau kiểm hoá các tờ khai này không?",true) == "Yes")
                if (MLMessages("Bạn có muốn tính thuế sau kiểm hoá các tờ khai này không?", "MSG_THK74", "", true) == "Yes")
                    foreach (GridEXSelectedItem item in items)
                    {
                        if (item.RowType == RowType.Record)
                        {

                            ToKhaiMauDich tkmd = (ToKhaiMauDich)item.GetRow().DataRow;
                            if (tkmd.TrangThaiThanhKhoan == "D") ShowMessage("Tờ khai số '" + item.GetRow().Cells["SoToKhai"].Text + "' chưa được kiểm hoá.", false);
                            else
                                tkmd.TrangThaiThanhKhoan = "S";
                        }

                    }
                new ToKhaiMauDich().InsertUpdate(this.tkmdCollection);
                dgList.DataSource = this.tkmdCollection;
                dgList.Refetch();
            }

        }

        private void uiGroupBox2_Click(object sender, EventArgs e)
        {

        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
           
            bool ok = true;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            //if (ShowMessage("Bạn có muốn xóa các tờ khai này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa các tờ khai này không?", "MSG_DEL01", "", true) == "Yes")
            {
                this.Cursor = Cursors.WaitCursor;

                ToKhaiMauDichCollection TKMDColleciton = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiMauDich TKMD = (ToKhaiMauDich)i.GetRow().DataRow;
                        TKMD.LoadHMDCollection();
                        if (TKMD.IsToKhaiDaThanhKhoanVaDongHoSo())
                        {
                            ShowMessage(string.Format("Tờ khai đã tham gia thanh khoản và hồ sơ đã đóng, không thể sửa tờ khai này được."), false);
                            return;
                        }
                        if (TKMD.MaLoaiHinh.StartsWith("XSX") && TKMD.TrangThai == 0)
                        {
                            // ShowMessage("Tờ khai " + TKMD.SoToKhai + " đã được phân bổ nên không xóa được. Hãy xóa phân bổ trước khi xóa tờ khai này.", false);
                            MLMessages("Tờ khai " + TKMD.SoToKhai + " đã được phân bổ nên không xóa được. Hãy xóa phân bổ trước khi xóa tờ khai này.", "MSG_TK01", "" + TKMD.SoToKhai, false);
                            this.Cursor = Cursors.Default;
                            return;
                        }
                        else if (TKMD.MaLoaiHinh.StartsWith("NSX"))
                        {
                            Company.BLL.SXXK.PhanBoToKhaiXuat pbTKX = Company.BLL.SXXK.PhanBoToKhaiNhap.SelectToKhaiXuatDuocPhanBoDauTienChoToKhaiNhap(GlobalSettings.MA_HAI_QUAN, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NamDangKy);
                            if (pbTKX != null)
                            {
                                this.Cursor = Cursors.Default;
                                // ShowMessage("Tờ khai nhập " + TKMD.SoToKhai + " đã được phân bổ. Không thể xóa được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", false);
                                MLMessages("Tờ khai nhập " + TKMD.SoToKhai + " đã được phân bổ. Không thể xóa được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", "MSG_TK01", "" + TKMD.SoToKhai, false);
                                return;
                            }
                        }
                        TKMDColleciton.Add(TKMD);
                    }
                }
                try
                {
                    new ToKhaiMauDich().DeleteCollection(TKMDColleciton);
                    // ShowMessage("Xóa thành công!", false);
                    MLMessages("Xóa thành công!", "MSG_PUB14", "", false);
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                    this.Cursor = Cursors.Default;
                }
                this.Cursor = Cursors.Default;
                this.search();
            }
            else
                e.Cancel = true;
            }
            catch (System.Exception ex)
            {
                ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void HuyCTMenu_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            //  if (ShowMessage("Bạn có muốn xóa tờ khai này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa các tờ khai này không?", "MSG_DEL01", "", true) == "Yes")
            {
                ToKhaiMauDich tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;
                try
                {
                    tkmd.LoadHMDCollection();
                    if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                    {
                        tkmd.LoadPhanBo();
                        // if (ShowMessage("Nếu xóa tờ khai xuất này sẽ phải phân bổ lại cho các tờ khai xuất phía sau tờ khai xuất này. Bạn có muốn xóa không ?", true) != "Yes")
                        if (MLMessages("Nếu xóa tờ khai xuất này sẽ phải phân bổ lại cho các tờ khai xuất phía sau tờ khai xuất này. Bạn có muốn xóa không ?", "MSG_TK01", "", true) != "Yes")
                        {
                            return;
                        }
                    }
                    else
                    {
                        ;
                    }

                    tkmd.Delete();
                    //ShowMessage("Xóa thành công!", false);
                    MLMessages("Xóa thành công!", "MSG_PUB14", "", false);
                    this.search();
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                }
            }
        }

        private void print_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            if (row != null && row.RowType == RowType.Record)
            {
                ToKhaiMauDich tkmd = (ToKhaiMauDich)dgList.GetRow().DataRow;

                if (tkmd.MaLoaiHinh.Contains("N"))
                {
                    Report.ReportViewTKNSXXKForm f = new ReportViewTKNSXXKForm();
                    f.TKMD = tkmd;
                    f.Show();
                }
                else
                {
                    Report.ReportViewTKXSXXKForm f = new ReportViewTKXSXXKForm();
                    f.TKMD = tkmd;
                    f.Show();
                }
            }
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {
        }

        private void dgList_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            ToKhaiMauDich TKMD = (ToKhaiMauDich)row.DataRow;
            if (TKMD.NGAY_THN_THX.Year > 1900 || TKMD.NgayHoanThanh.Year > 1900)
            {
                TKMD.Update();
                new Company.BLL.KDT.SXXK.BCXuatNhapTon().UpdateNgayThucNhapXuatTransaction(TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NgayDangKy, TKMD.NGAY_THN_THX, TKMD.NgayHoanThanh, null);
                new Company.BLL.KDT.SXXK.BCThueXNK().UpdateNgayThucNhapXuatTransaction(TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NgayDangKy, TKMD.NGAY_THN_THX, null);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            //  if (ShowMessage("Bạn có muốn xóa các tờ khai này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa các tờ khai này không?", "MSG_DEL01", "", true) == "Yes")
            {
                this.Cursor = Cursors.WaitCursor;

                ToKhaiMauDichCollection TKMDColleciton = new ToKhaiMauDichCollection();
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiMauDich TKMD = (ToKhaiMauDich)i.GetRow().DataRow;
                        TKMD.LoadHMDCollection();
                        if (TKMD.MaLoaiHinh.StartsWith("X") && TKMD.TrangThai == 0)
                        {
                            // ShowMessage("Tờ khai " + TKMD.SoToKhai + " đã được phân bổ nên không xóa được. Hãy xóa phân bổ trước khi xóa tờ khai này.", false);
                            MLMessages("Tờ khai " + TKMD.SoToKhai + " đã được phân bổ nên không xóa được. Hãy xóa phân bổ trước khi xóa tờ khai này.", "MSG_TK01", "" + TKMD.SoToKhai, false);
                            this.Cursor = Cursors.Default;
                            return;
                        }
                        else if (TKMD.MaLoaiHinh.StartsWith("N"))
                        {
                            Company.BLL.SXXK.PhanBoToKhaiXuat pbTKX = Company.BLL.SXXK.PhanBoToKhaiNhap.SelectToKhaiXuatDuocPhanBoDauTienChoToKhaiNhap(GlobalSettings.MA_HAI_QUAN, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NamDangKy);
                            if (pbTKX != null)
                            {
                                this.Cursor = Cursors.Default;
                                //ShowMessage("Tờ khai nhập " + TKMD.SoToKhai + " đã được phân bổ. Không thể xóa được. Hãy xóa phân bổ trước khi xóa tờ khai này.", false);                                
                                MLMessages("Tờ khai nhập " + TKMD.SoToKhai + " đã được phân bổ. Không thể xóa được. Hãy xóa phân bổ trước khi xóa tờ khai này.", "MSG_TK01", "" + TKMD.SoToKhai, false);
                                return;
                            }
                        }
                        TKMDColleciton.Add(TKMD);
                    }
                }
                try
                {
                    new ToKhaiMauDich().DeleteCollection(TKMDColleciton);
                    //ShowMessage("Xóa thành công!", false);
                    MLMessages("Xóa thành công!", "MSG_PUB14", "", false);
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                    this.Cursor = Cursors.Default;
                }
                this.Cursor = Cursors.Default;
                this.search();
            }
        }

        private void uiButton3_Click_1(object sender, EventArgs e)
        {

        }

        private void mnKhongPhanBo_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                try
                {
                    ToKhaiMauDich TKMD = (ToKhaiMauDich)dgList.GetRow().DataRow;
                    if (TKMD.TrangThai == 0)
                    {
                        // ShowMessage("Tờ khai này đã được phân bổ. Bạn phải xóa phân bổ cho tờ khai này trước khi chuyển sang chế độ không phân bổ.", false);
                        MLMessages("Tờ khai này đã được phân bổ.", "MSG_TK03", "", false);
                        return;
                    }
                    TKMD.TrangThai = 2;
                    TKMD.Update();
                    //ShowMessage("Thực hiện thành công.", false);
                    MLMessages("Thực hiện thành công.", "MSG_TK02", "", false);
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                }
            }
        }

        private void mnDuaVaoPhanBo_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
            {
                try
                {
                    ToKhaiMauDich TKMD = (ToKhaiMauDich)dgList.GetRow().DataRow;
                    if (TKMD.TrangThai == 0)
                    {
                        // ShowMessage("Tờ khai này đã được phân bổ.", false);
                        MLMessages("Tờ khai này đã được phân bổ.", "MSG_TK03", "", false);
                        return;
                    }
                    TKMD.TrangThai = 1;
                    TKMD.Update();
                    //ShowMessage("Thực hiện thành công.", false);
                    MLMessages("Thực hiện thành công.", "MSG_TK02", "", false);
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                }
            }
        }

        private void btnGetTKXGC_Click(object sender, EventArgs e)
        {
            GetToKhaiMauDichGiaCongForm f = new GetToKhaiMauDichGiaCongForm();
            f.ShowDialog(this);
            search();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "BangKeToKhaiNhapDaDangKy_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.ShowDialog(this);

                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void uiButton3_Click_2(object sender, EventArgs e)
        {
            CapNhatChuHangForm f = new CapNhatChuHangForm();
            f.ShowDialog(this);
            this.search();
        }


        private void khoitao_DuLieuChuan()
        {

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            ctrCoQuanHQ.Ma = GlobalSettings.MA_HAI_QUAN;
        }
        private void cbbLoaiToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            search();
        }

        private void cmMain_CommandClick_1(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSync":
                    btnGetTKXGC_Click(null, null);
                    break;
                case "cmdExportExcel":
                    btnExportExcel_Click(null, null);
                    break;
                case "cmdUpdateChuHang":
                    uiButton3_Click_2(null, null);
                    break;
                case "cmdUpdateNgayGio":
                    uiButton2_Click(null, null);
                    break;
                case "cmdDelete":
                    btnDelete_Click(null, null);
                    break;
            }
        }

    }
}