﻿using System;
using System.Collections.Generic;
using System.Text;
namespace ActivationInfo
{
    public class ClientDetailInfo
    {

        public string ProductsID { get; set; }
        public string SerialCode { get; set; }
        private string _mahaiquan;
        public string MaHaiQuan
        {
            get { return _mahaiquan; }
            set { _mahaiquan = value; }
        }

        private string _serverdbName;

        public string ServerDBName
        {
            get { return _serverdbName; }
            set { _serverdbName = value; }
        }
        private string _userdbName;

        public string UserNameDB
        {
            get { return _userdbName; }
            set { _userdbName = value; }
        }
        private string _passWordDb;

        public string PasswordDB
        {
            get { return _passWordDb; }
            set { _passWordDb = value; }
        }
        private string _dbName;

        public string DBName
        {
            get { return _dbName; }
            set { _dbName = value; }
        }
        public DateTime NgayOnline { get; set; }
        private string _hostProxy;

        public string HostProxy
        {
            get { return _hostProxy; }
            set { _hostProxy = value; }
        }
        private string _portProxy;

        public string PortProxy
        {
            get { return _portProxy; }
            set { _portProxy = value; }
        }
        public DateTime NgayDangKy { get; set; }
        public DateTime NgayKichHoat { get; set; }
        public DateTime NgayHetHan { get; set; }
        public string DiaChiHaiQuan { get; set; }
        public string TenDichVu { get; set; }
      

    }
}