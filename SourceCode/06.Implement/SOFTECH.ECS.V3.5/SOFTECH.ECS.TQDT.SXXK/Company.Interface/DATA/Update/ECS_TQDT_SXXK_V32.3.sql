IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B01')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi' WHERE TableID='A404' AND TaxCode='B01'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B01','Biểu thuế nhập khẩu ưu đãi')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B02')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Chương 98 (1) - Biểu thuế nhập khẩu ưu đãi ' WHERE TableID='A404' AND TaxCode='B02'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B02','Chương 98 (1) - Biểu thuế nhập khẩu ưu đãi ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B03')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu thông thường (bằng 150% thuế suất MFN áp dụng cho các nước không có quan hệ tối huệ quốc đối với Việt Nam)' WHERE TableID='A404' AND TaxCode='B03'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B03','Biểu thuế nhập khẩu thông thường (bằng 150% thuế suất MFN áp dụng cho các nước không có quan hệ tối huệ quốc đối với Việt Nam)')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B04')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại hàng hóa ASEAN (ATIGA)' WHERE TableID='A404' AND TaxCode='B04'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B04','Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại hàng hóa ASEAN (ATIGA)')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B05')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Mậu dịch Tự do ASEAN - Trung Quốc' WHERE TableID='A404' AND TaxCode='B05'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B05','Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Mậu dịch Tự do ASEAN - Trung Quốc')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B06')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Mậu dịch Tự do ASEAN - Hàn Quốc' WHERE TableID='A404' AND TaxCode='B06'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B06','Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Mậu dịch Tự do ASEAN - Hàn Quốc')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B07')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Khu vực Thương mại tự do ASEAN - Úc - Niu Di lân' WHERE TableID='A404' AND TaxCode='B07'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B07','Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Khu vực Thương mại tự do ASEAN - Úc - Niu Di lân')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B08')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại Hàng hoá ASEAN - Ấn Độ' WHERE TableID='A404' AND TaxCode='B08'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B08','Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại Hàng hoá ASEAN - Ấn Độ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B09')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Đối tác kinh tế toàn diện ASEAN - Nhật Bản' WHERE TableID='A404' AND TaxCode='B09'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B09','Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Đối tác kinh tế toàn diện ASEAN - Nhật Bản')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B10')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định đối tác kinh tế Việt Nam - Nhật Bản' WHERE TableID='A404' AND TaxCode='B10'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B10','Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định đối tác kinh tế Việt Nam - Nhật Bản')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B11')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế thuế nhập khẩu đối với các mặt hàng được áp dụng ưu đãi thuế suất thuế nhập khẩu Việt - Lào' WHERE TableID='A404' AND TaxCode='B11'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B11','Biểu thuế thuế nhập khẩu đối với các mặt hàng được áp dụng ưu đãi thuế suất thuế nhập khẩu Việt - Lào')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B12')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế thuế nhập khẩu đối với hàng hoá có xuất xứ Campuchia' WHERE TableID='A404' AND TaxCode='B12'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B12','Biểu thuế thuế nhập khẩu đối với hàng hoá có xuất xứ Campuchia')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B13')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại tự do Việt Nam - Chi Lê' WHERE TableID='A404' AND TaxCode='B13'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B13','Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại tự do Việt Nam - Chi Lê')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B14')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế NK ngoài hạn ngạch ' WHERE TableID='A404' AND TaxCode='B14'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B14','Biểu thuế NK ngoài hạn ngạch ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B15')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu tuyệt đối' WHERE TableID='A404' AND TaxCode='B15'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B15','Biểu thuế nhập khẩu tuyệt đối')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B16')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu hỗn hợp' WHERE TableID='A404' AND TaxCode='B16'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B16','Biểu thuế nhập khẩu hỗn hợp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B17')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Chương 98 (2) - Biểu thuế nhập khẩu ưu đãi ' WHERE TableID='A404' AND TaxCode='B17'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B17','Chương 98 (2) - Biểu thuế nhập khẩu ưu đãi ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B30')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Mã biểu thuế áp dụng cho đối tượng không chịu thuế nhập khẩu' WHERE TableID='A404' AND TaxCode='B30'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B30','Mã biểu thuế áp dụng cho đối tượng không chịu thuế nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode='B18')
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại tự do Việt Nam - Hàn Quốc' WHERE TableID='A404' AND TaxCode='B18'
END
ELSE
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES ('A404','B18','Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại tự do Việt Nam - Hàn Quốc')
END

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '32.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('32.3',GETDATE(), N'Cập nhật mã biểu thuế nhập khẩu ')
END	