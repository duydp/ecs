﻿namespace Company.Interface
{
    partial class GiayNopTienChungTuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiayNopTienChungTuForm));
            this.grpThongTinChung = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayPhatHanh = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbLoaiChungTu = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinChung)).BeginInit();
            this.grpThongTinChung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.grpThongTinChung);
            this.grbMain.Size = new System.Drawing.Size(363, 193);
            // 
            // grpThongTinChung
            // 
            this.grpThongTinChung.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpThongTinChung.BackColor = System.Drawing.Color.Transparent;
            this.grpThongTinChung.Controls.Add(this.ccNgayPhatHanh);
            this.grpThongTinChung.Controls.Add(this.cbLoaiChungTu);
            this.grpThongTinChung.Controls.Add(this.txtSoChungTu);
            this.grpThongTinChung.Controls.Add(this.txtTenChungTu);
            this.grpThongTinChung.Controls.Add(this.label8);
            this.grpThongTinChung.Controls.Add(this.label5);
            this.grpThongTinChung.Controls.Add(this.label7);
            this.grpThongTinChung.Controls.Add(this.label4);
            this.grpThongTinChung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpThongTinChung.Location = new System.Drawing.Point(8, 3);
            this.grpThongTinChung.Name = "grpThongTinChung";
            this.grpThongTinChung.Size = new System.Drawing.Size(352, 156);
            this.grpThongTinChung.TabIndex = 0;
            this.grpThongTinChung.Text = "Thông tin chung";
            this.grpThongTinChung.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grpThongTinChung.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayPhatHanh
            // 
            // 
            // 
            // 
            this.ccNgayPhatHanh.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayPhatHanh.DropDownCalendar.Name = "";
            this.ccNgayPhatHanh.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayPhatHanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayPhatHanh.IsNullDate = true;
            this.ccNgayPhatHanh.Location = new System.Drawing.Point(89, 112);
            this.ccNgayPhatHanh.Name = "ccNgayPhatHanh";
            this.ccNgayPhatHanh.Nullable = true;
            this.ccNgayPhatHanh.NullButtonText = "Xóa";
            this.ccNgayPhatHanh.ShowNullButton = true;
            this.ccNgayPhatHanh.Size = new System.Drawing.Size(151, 21);
            this.ccNgayPhatHanh.TabIndex = 3;
            this.ccNgayPhatHanh.TodayButtonText = "Hôm nay";
            this.ccNgayPhatHanh.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayPhatHanh.VisualStyleManager = this.vsmMain;
            // 
            // cbLoaiChungTu
            // 
            this.cbLoaiChungTu.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbLoaiChungTu.DisplayMember = "Ten";
            this.cbLoaiChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Thuế XNK";
            uiComboBoxItem6.Value = 1;
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Thuế VAT";
            uiComboBoxItem7.Value = 2;
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Thuế TTĐB";
            uiComboBoxItem8.Value = 3;
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Thuế BVMT";
            uiComboBoxItem9.Value = 4;
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Thuế khác";
            uiComboBoxItem10.Value = 5;
            this.cbLoaiChungTu.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10});
            this.cbLoaiChungTu.Location = new System.Drawing.Point(89, 21);
            this.cbLoaiChungTu.Name = "cbLoaiChungTu";
            this.cbLoaiChungTu.Size = new System.Drawing.Size(153, 21);
            this.cbLoaiChungTu.TabIndex = 0;
            this.cbLoaiChungTu.ValueMember = "ID";
            this.cbLoaiChungTu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbLoaiChungTu.VisualStyleManager = this.vsmMain;
            // 
            // txtSoChungTu
            // 
            this.txtSoChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoChungTu.Location = new System.Drawing.Point(89, 52);
            this.txtSoChungTu.MaxLength = 255;
            this.txtSoChungTu.Name = "txtSoChungTu";
            this.txtSoChungTu.Size = new System.Drawing.Size(256, 21);
            this.txtSoChungTu.TabIndex = 1;
            this.txtSoChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenChungTu
            // 
            this.txtTenChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChungTu.Location = new System.Drawing.Point(89, 79);
            this.txtTenChungTu.MaxLength = 255;
            this.txtTenChungTu.Name = "txtTenChungTu";
            this.txtTenChungTu.Size = new System.Drawing.Size(256, 21);
            this.txtTenChungTu.TabIndex = 2;
            this.txtTenChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Ngày phát hành";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Số chứng từ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Tên chứng từ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Loại chứng từ";
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(123, 165);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 1;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(285, 165);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // btnGhi
            // 
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(204, 165);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 2;
            this.btnGhi.Text = "&Lưu";
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // GiayNopTienChungTuForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 193);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GiayNopTienChungTuForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chứng từ giấy nộp tiền";
            this.Load += new System.EventHandler(this.GiayNopTienChungTuForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinChung)).EndInit();
            this.grpThongTinChung.ResumeLayout(false);
            this.grpThongTinChung.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox grpThongTinChung;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTu;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChungTu;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIComboBox cbLoaiChungTu;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayPhatHanh;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.EditControls.UIButton btnGhi;
    }
}