using System.Data.SqlClient;

namespace Company.BLL.KDT.SXXK
{
    public interface IDangKy
    {
        bool InsertUpdateFull();
        void InsertUpdateFull(SqlTransaction transaction);
        void WSDownload();
    }
}