using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.GC
{
    public partial class NPLXuatForm : BaseForm
    {
        public DataSet ds = new DataSet();
        public NPLXuatForm()
        {
            InitializeComponent();
        }

        private void NPLXuatForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = ds.Tables[0];
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }
    }
}