﻿namespace Company.Interface
{
    partial class TKMD_ThongTinVanChuyen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDiaDiemDenCuaBaoThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDDTrungChuyen1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.NgayDi1 = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayDen1 = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayKhoiHanh = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayNhapKho = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayDi2 = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNGayDen2 = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDDTrungChuyen2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ccNgayDi3 = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNGayDen3 = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDDTrungChuyen3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ccNgayDenCuaBaoThue = new Janus.Windows.CalendarCombo.CalendarCombo();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(627, 220);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtDiaDiemDenCuaBaoThue);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.ccNgayNhapKho);
            this.uiGroupBox1.Controls.Add(this.ccNgayKhoiHanh);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.ccNgayDenCuaBaoThue);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(627, 91);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông tin chung";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(15, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(175, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "Ngày được phép nhập kho đầu tiên";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(356, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ngày khởi hành vận chuyển";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Địa điểm đến của VC bảo thuế";
            // 
            // txtDiaDiemDenCuaBaoThue
            // 
            this.txtDiaDiemDenCuaBaoThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemDenCuaBaoThue.Location = new System.Drawing.Point(196, 60);
            this.txtDiaDiemDenCuaBaoThue.MaxLength = 255;
            this.txtDiaDiemDenCuaBaoThue.Name = "txtDiaDiemDenCuaBaoThue";
            this.txtDiaDiemDenCuaBaoThue.Size = new System.Drawing.Size(135, 21);
            this.txtDiaDiemDenCuaBaoThue.TabIndex = 4;
            this.txtDiaDiemDenCuaBaoThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemDenCuaBaoThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaDiemDenCuaBaoThue.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(356, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Ngày đến của VC bảo thuế";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtDDTrungChuyen3);
            this.uiGroupBox2.Controls.Add(this.txtDDTrungChuyen2);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.txtDDTrungChuyen1);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.ccNGayDen3);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.ccNGayDen2);
            this.uiGroupBox2.Controls.Add(this.ccNgayDi3);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.ccNgayDi2);
            this.uiGroupBox2.Controls.Add(this.ccNgayDen1);
            this.uiGroupBox2.Controls.Add(this.NgayDi1);
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 97);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(624, 120);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin trung chuyển";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Điểm trung chuyển 1";
            // 
            // txtDDTrungChuyen1
            // 
            this.txtDDTrungChuyen1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDDTrungChuyen1.Location = new System.Drawing.Point(135, 27);
            this.txtDDTrungChuyen1.MaxLength = 255;
            this.txtDDTrungChuyen1.Name = "txtDDTrungChuyen1";
            this.txtDDTrungChuyen1.Size = new System.Drawing.Size(156, 21);
            this.txtDDTrungChuyen1.TabIndex = 4;
            this.txtDDTrungChuyen1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDDTrungChuyen1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(297, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Ngày đến";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(450, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Ngày đi";
            // 
            // NgayDi1
            // 
            // 
            // 
            // 
            this.NgayDi1.DropDownCalendar.Name = "";
            this.NgayDi1.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.NgayDi1.IsNullDate = true;
            this.NgayDi1.Location = new System.Drawing.Point(499, 27);
            this.NgayDi1.Name = "NgayDi1";
            this.NgayDi1.Nullable = true;
            this.NgayDi1.NullButtonText = "Xóa ngày";
            this.NgayDi1.ShowNullButton = true;
            this.NgayDi1.Size = new System.Drawing.Size(82, 21);
            this.NgayDi1.TabIndex = 3;
            this.NgayDi1.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ccNgayDen1
            // 
            // 
            // 
            // 
            this.ccNgayDen1.DropDownCalendar.Name = "";
            this.ccNgayDen1.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDen1.IsNullDate = true;
            this.ccNgayDen1.Location = new System.Drawing.Point(356, 27);
            this.ccNgayDen1.Name = "ccNgayDen1";
            this.ccNgayDen1.Nullable = true;
            this.ccNgayDen1.NullButtonText = "Xóa ngày";
            this.ccNgayDen1.ShowNullButton = true;
            this.ccNgayDen1.Size = new System.Drawing.Size(82, 21);
            this.ccNgayDen1.TabIndex = 3;
            this.ccNgayDen1.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ccNgayKhoiHanh
            // 
            // 
            // 
            // 
            this.ccNgayKhoiHanh.DropDownCalendar.Name = "";
            this.ccNgayKhoiHanh.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKhoiHanh.IsNullDate = true;
            this.ccNgayKhoiHanh.Location = new System.Drawing.Point(502, 20);
            this.ccNgayKhoiHanh.Name = "ccNgayKhoiHanh";
            this.ccNgayKhoiHanh.Nullable = true;
            this.ccNgayKhoiHanh.NullButtonText = "Xóa ngày";
            this.ccNgayKhoiHanh.ShowNullButton = true;
            this.ccNgayKhoiHanh.Size = new System.Drawing.Size(112, 21);
            this.ccNgayKhoiHanh.TabIndex = 3;
            this.ccNgayKhoiHanh.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ccNgayNhapKho
            // 
            // 
            // 
            // 
            this.ccNgayNhapKho.DropDownCalendar.Name = "";
            this.ccNgayNhapKho.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayNhapKho.IsNullDate = true;
            this.ccNgayNhapKho.Location = new System.Drawing.Point(196, 20);
            this.ccNgayNhapKho.Name = "ccNgayNhapKho";
            this.ccNgayNhapKho.Nullable = true;
            this.ccNgayNhapKho.NullButtonText = "Xóa ngày";
            this.ccNgayNhapKho.ShowNullButton = true;
            this.ccNgayNhapKho.Size = new System.Drawing.Size(135, 21);
            this.ccNgayNhapKho.TabIndex = 3;
            this.ccNgayNhapKho.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ccNgayDi2
            // 
            // 
            // 
            // 
            this.ccNgayDi2.DropDownCalendar.Name = "";
            this.ccNgayDi2.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDi2.IsNullDate = true;
            this.ccNgayDi2.Location = new System.Drawing.Point(499, 54);
            this.ccNgayDi2.Name = "ccNgayDi2";
            this.ccNgayDi2.Nullable = true;
            this.ccNgayDi2.NullButtonText = "Xóa ngày";
            this.ccNgayDi2.ShowNullButton = true;
            this.ccNgayDi2.Size = new System.Drawing.Size(82, 21);
            this.ccNgayDi2.TabIndex = 3;
            this.ccNgayDi2.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ccNGayDen2
            // 
            // 
            // 
            // 
            this.ccNGayDen2.DropDownCalendar.Name = "";
            this.ccNGayDen2.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNGayDen2.IsNullDate = true;
            this.ccNGayDen2.Location = new System.Drawing.Point(356, 54);
            this.ccNGayDen2.Name = "ccNGayDen2";
            this.ccNGayDen2.Nullable = true;
            this.ccNGayDen2.NullButtonText = "Xóa ngày";
            this.ccNGayDen2.ShowNullButton = true;
            this.ccNGayDen2.Size = new System.Drawing.Size(82, 21);
            this.ccNGayDen2.TabIndex = 3;
            this.ccNGayDen2.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(297, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Ngày đến";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(450, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Ngày đi";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Điểm trung chuyển 2";
            // 
            // txtDDTrungChuyen2
            // 
            this.txtDDTrungChuyen2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDDTrungChuyen2.Location = new System.Drawing.Point(135, 54);
            this.txtDDTrungChuyen2.MaxLength = 255;
            this.txtDDTrungChuyen2.Name = "txtDDTrungChuyen2";
            this.txtDDTrungChuyen2.Size = new System.Drawing.Size(156, 21);
            this.txtDDTrungChuyen2.TabIndex = 4;
            this.txtDDTrungChuyen2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDDTrungChuyen2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ccNgayDi3
            // 
            // 
            // 
            // 
            this.ccNgayDi3.DropDownCalendar.Name = "";
            this.ccNgayDi3.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDi3.IsNullDate = true;
            this.ccNgayDi3.Location = new System.Drawing.Point(499, 81);
            this.ccNgayDi3.Name = "ccNgayDi3";
            this.ccNgayDi3.Nullable = true;
            this.ccNgayDi3.NullButtonText = "Xóa ngày";
            this.ccNgayDi3.ShowNullButton = true;
            this.ccNgayDi3.Size = new System.Drawing.Size(82, 21);
            this.ccNgayDi3.TabIndex = 3;
            this.ccNgayDi3.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ccNGayDen3
            // 
            // 
            // 
            // 
            this.ccNGayDen3.DropDownCalendar.Name = "";
            this.ccNGayDen3.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNGayDen3.IsNullDate = true;
            this.ccNGayDen3.Location = new System.Drawing.Point(356, 81);
            this.ccNGayDen3.Name = "ccNGayDen3";
            this.ccNGayDen3.Nullable = true;
            this.ccNGayDen3.NullButtonText = "Xóa ngày";
            this.ccNGayDen3.ShowNullButton = true;
            this.ccNGayDen3.Size = new System.Drawing.Size(82, 21);
            this.ccNGayDen3.TabIndex = 3;
            this.ccNGayDen3.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(297, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Ngày đến";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(450, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Ngày đi";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Điểm trung chuyển 3";
            // 
            // txtDDTrungChuyen3
            // 
            this.txtDDTrungChuyen3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDDTrungChuyen3.Location = new System.Drawing.Point(135, 81);
            this.txtDDTrungChuyen3.MaxLength = 255;
            this.txtDDTrungChuyen3.Name = "txtDDTrungChuyen3";
            this.txtDDTrungChuyen3.Size = new System.Drawing.Size(156, 21);
            this.txtDDTrungChuyen3.TabIndex = 4;
            this.txtDDTrungChuyen3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDDTrungChuyen3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ccNgayDenCuaBaoThue
            // 
            // 
            // 
            // 
            this.ccNgayDenCuaBaoThue.DropDownCalendar.Name = "";
            this.ccNgayDenCuaBaoThue.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDenCuaBaoThue.IsNullDate = true;
            this.ccNgayDenCuaBaoThue.Location = new System.Drawing.Point(502, 60);
            this.ccNgayDenCuaBaoThue.Name = "ccNgayDenCuaBaoThue";
            this.ccNgayDenCuaBaoThue.Nullable = true;
            this.ccNgayDenCuaBaoThue.NullButtonText = "Xóa ngày";
            this.ccNgayDenCuaBaoThue.ShowNullButton = true;
            this.ccNgayDenCuaBaoThue.Size = new System.Drawing.Size(112, 21);
            this.ccNgayDenCuaBaoThue.TabIndex = 3;
            this.ccNgayDenCuaBaoThue.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // TKMD_ThongTinVanChuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 220);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "TKMD_ThongTinVanChuyen";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin vận chuyển";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemDenCuaBaoThue;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtDDTrungChuyen1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.CalendarCombo.CalendarCombo NgayDi1;
        private Janus.Windows.GridEX.EditControls.EditBox txtDDTrungChuyen3;
        private Janus.Windows.GridEX.EditControls.EditBox txtDDTrungChuyen2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNGayDen3;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNGayDen2;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDi3;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDi2;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDen1;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayNhapKho;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKhoiHanh;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDenCuaBaoThue;
    }
}