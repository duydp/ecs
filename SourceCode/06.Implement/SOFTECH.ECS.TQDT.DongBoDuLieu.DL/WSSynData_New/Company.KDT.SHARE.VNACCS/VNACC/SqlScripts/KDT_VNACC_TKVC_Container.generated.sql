-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Insert]
	@Master_ID bigint,
	@SotieuDe varchar(3),
	@SoHieuContainer varchar(17),
	@SoDongHangTrenTK varchar(5),
	@SoSeal1 varchar(15),
	@SoSeal2 varchar(15),
	@SoSeal3 varchar(15),
	@SoSeal4 varchar(15),
	@SoSeal5 varchar(15),
	@SoSeal6 varchar(15),
	@Note varchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TKVC_Container]
(
	[Master_ID],
	[SotieuDe],
	[SoHieuContainer],
	[SoDongHangTrenTK],
	[SoSeal1],
	[SoSeal2],
	[SoSeal3],
	[SoSeal4],
	[SoSeal5],
	[SoSeal6],
	[Note]
)
VALUES 
(
	@Master_ID,
	@SotieuDe,
	@SoHieuContainer,
	@SoDongHangTrenTK,
	@SoSeal1,
	@SoSeal2,
	@SoSeal3,
	@SoSeal4,
	@SoSeal5,
	@SoSeal6,
	@Note
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Update]
	@ID bigint,
	@Master_ID bigint,
	@SotieuDe varchar(3),
	@SoHieuContainer varchar(17),
	@SoDongHangTrenTK varchar(5),
	@SoSeal1 varchar(15),
	@SoSeal2 varchar(15),
	@SoSeal3 varchar(15),
	@SoSeal4 varchar(15),
	@SoSeal5 varchar(15),
	@SoSeal6 varchar(15),
	@Note varchar(50)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TKVC_Container]
SET
	[Master_ID] = @Master_ID,
	[SotieuDe] = @SotieuDe,
	[SoHieuContainer] = @SoHieuContainer,
	[SoDongHangTrenTK] = @SoDongHangTrenTK,
	[SoSeal1] = @SoSeal1,
	[SoSeal2] = @SoSeal2,
	[SoSeal3] = @SoSeal3,
	[SoSeal4] = @SoSeal4,
	[SoSeal5] = @SoSeal5,
	[SoSeal6] = @SoSeal6,
	[Note] = @Note
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SotieuDe varchar(3),
	@SoHieuContainer varchar(17),
	@SoDongHangTrenTK varchar(5),
	@SoSeal1 varchar(15),
	@SoSeal2 varchar(15),
	@SoSeal3 varchar(15),
	@SoSeal4 varchar(15),
	@SoSeal5 varchar(15),
	@SoSeal6 varchar(15),
	@Note varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TKVC_Container] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TKVC_Container] 
		SET
			[Master_ID] = @Master_ID,
			[SotieuDe] = @SotieuDe,
			[SoHieuContainer] = @SoHieuContainer,
			[SoDongHangTrenTK] = @SoDongHangTrenTK,
			[SoSeal1] = @SoSeal1,
			[SoSeal2] = @SoSeal2,
			[SoSeal3] = @SoSeal3,
			[SoSeal4] = @SoSeal4,
			[SoSeal5] = @SoSeal5,
			[SoSeal6] = @SoSeal6,
			[Note] = @Note
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TKVC_Container]
		(
			[Master_ID],
			[SotieuDe],
			[SoHieuContainer],
			[SoDongHangTrenTK],
			[SoSeal1],
			[SoSeal2],
			[SoSeal3],
			[SoSeal4],
			[SoSeal5],
			[SoSeal6],
			[Note]
		)
		VALUES 
		(
			@Master_ID,
			@SotieuDe,
			@SoHieuContainer,
			@SoDongHangTrenTK,
			@SoSeal1,
			@SoSeal2,
			@SoSeal3,
			@SoSeal4,
			@SoSeal5,
			@SoSeal6,
			@Note
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TKVC_Container]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TKVC_Container] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SotieuDe],
	[SoHieuContainer],
	[SoDongHangTrenTK],
	[SoSeal1],
	[SoSeal2],
	[SoSeal3],
	[SoSeal4],
	[SoSeal5],
	[SoSeal6],
	[Note]
FROM
	[dbo].[t_KDT_VNACC_TKVC_Container]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SotieuDe],
	[SoHieuContainer],
	[SoDongHangTrenTK],
	[SoSeal1],
	[SoSeal2],
	[SoSeal3],
	[SoSeal4],
	[SoSeal5],
	[SoSeal6],
	[Note]
FROM [dbo].[t_KDT_VNACC_TKVC_Container] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SotieuDe],
	[SoHieuContainer],
	[SoDongHangTrenTK],
	[SoSeal1],
	[SoSeal2],
	[SoSeal3],
	[SoSeal4],
	[SoSeal5],
	[SoSeal6],
	[Note]
FROM
	[dbo].[t_KDT_VNACC_TKVC_Container]	

GO

