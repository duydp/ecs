using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DongBo.Controls
{
    public partial class ucStart : ucDesign
    {
        frmMain frmParent = null;
        public ucStart()
        {
            InitializeComponent();
            frmParent = (frmMain)Application.OpenForms["frmMain"];
        }

        private void ucStart_Load(object sender, EventArgs e)
        {
            frmParent.btnPrev.Enabled = false;
            frmParent.btnNext.Enabled = true;
        }
    }
}
