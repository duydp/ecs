using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAL2700 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute JNO { get; set; }
public PropertiesAttribute TNM { get; set; }
public PropertiesAttribute AB { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute KIJ { get; set; }

public VAL2700()
        {
JNO = new PropertiesAttribute(5, typeof(string));
TNM = new PropertiesAttribute(210, typeof(string));
AB = new PropertiesAttribute(12, typeof(int));
CH = new PropertiesAttribute(16, typeof(string));
KIJ = new PropertiesAttribute(996, typeof(string));
TongSoByte = 1249;
}

}public partial class EnumGroupID
    {
}

}
