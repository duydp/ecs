﻿using System;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace CA_QuanLyGCN
{
    public class DoanhNghiepCollections : CollectionBase
    {
        #region Constructor

        public DoanhNghiepCollections() { }
        #endregion Constructor

        #region Indexer Methods

        public DoanhNghiep this[int index]
        {
            get { return (DoanhNghiep)List[index]; }
            set { List[index] = value; }
        }

        #endregion Indexer Methods

        #region Private Methods

        public int Add(DoanhNghiep value)
        {
            return List.Add(value);
        }

        public void Insert(int index, DoanhNghiep value)
        {
            List.Insert(index, value);
        }

        public void Remove(DoanhNghiep value)
        {
            List.Remove(value);
        }

        public bool Contains(string id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                DoanhNghiep DoanhNghiepEntity = (DoanhNghiep)List[i];
                if (DoanhNghiepEntity.Id.Equals(id) && (DoanhNghiepEntity.EntityState != EntityState.Deleted))
                {
                    return true;
                }
            }
            return false;
        }

        public DoanhNghiep SearchById(int id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                DoanhNghiep DoanhNghiepEntity = (DoanhNghiep)List[i];
                if ((DoanhNghiepEntity.Id.Equals(id)) && (DoanhNghiepEntity.EntityState != EntityState.Deleted))
                {
                    return DoanhNghiepEntity;
                }
            }
            return null;
        }
/*
        public DoanhNghiep SearchByTen(string name)
        {
            for (int i = 0; i < List.Count; i++)
            {
                DoanhNghiep DoanhNghiepEntity = (DoanhNghiep)List[i];
                if ((DoanhNghiepEntity.Name.ToLower().Trim().Equals(name.ToLower().Trim())) && (DoanhNghiepEntity.EntityState != EntityState.Deleted))
                {
                    return DoanhNghiepEntity;
                }
            }
            return null;
        }
*/
        public void Save()
        {
            for (int i = 0; i < List.Count; i++)
            {
                DoanhNghiep DoanhNghiepEntity = (DoanhNghiep)List[i];
                switch (DoanhNghiepEntity.EntityState)
                {
                    case EntityState.Deleted:
                        DoanhNghiepEntity.Delete();
                        break;
                    case EntityState.Added:
                        DoanhNghiepEntity.Insert();
                        break;
                    case EntityState.Updated:
                        DoanhNghiepEntity.Update();
                        break;
                }
                DoanhNghiepEntity.EntityState = EntityState.Unchanged;
            }

        }

        #endregion Private Methods
						
		#region Clone Method

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public DoanhNghiepCollections Clone()
        {
            DoanhNghiepCollections _tmpCollections = new DoanhNghiepCollections();

            foreach (DoanhNghiep item in this)
            {
                _tmpCollections.Add((DoanhNghiep)item.Clone());
            }

            return _tmpCollections;
        }

        #endregion

    }
}

