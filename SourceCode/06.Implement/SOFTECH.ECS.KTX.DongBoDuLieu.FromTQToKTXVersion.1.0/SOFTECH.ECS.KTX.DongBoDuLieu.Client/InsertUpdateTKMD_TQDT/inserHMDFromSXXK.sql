INSERT INTO [dbo].[t_KDT_HangMauDich]
           ([TKMD_ID]
           ,[SoThuTuHang]
           ,[MaHS]
           ,[MaPhu]
           ,[TenHang]
           ,[NuocXX_ID]
           ,[DVT_ID]
           ,[SoLuong]
           ,[DonGiaKB]
           ,[DonGiaTT]
           ,[TriGiaKB]
           ,[TriGiaTT]
           ,[TriGiaKB_VND]
           ,[ThueSuatXNK]
           ,[ThueSuatTTDB]
           ,[ThueSuatGTGT]
           ,[ThueXNK]
           ,[ThueTTDB]
           ,[ThueGTGT]
           ,[PhuThu]
           ,[TyLeThuKhac]
           ,[TriGiaThuKhac]
           ,[MienThue]
           )
SELECT     KDT_TKMD.ID, SXXK_HMD.SoThuTuHang, SXXK_HMD.MaHS, SXXK_HMD.MaPhu, SXXK_HMD.TenHang, SXXK_HMD.NuocXX_ID, SXXK_HMD.DVT_ID, 
                      SXXK_HMD.SoLuong, SXXK_HMD.DonGiaKB, SXXK_HMD.DonGiaTT, SXXK_HMD.TriGiaKB, SXXK_HMD.TriGiaTT, SXXK_HMD.TriGiaKB_VND, 
                      SXXK_HMD.ThueSuatXNK, SXXK_HMD.ThueSuatTTDB, SXXK_HMD.ThueSuatGTGT, SXXK_HMD.ThueXNK, SXXK_HMD.ThueTTDB, SXXK_HMD.ThueGTGT, 
                      SXXK_HMD.PhuThu, SXXK_HMD.TyLeThuKhac, SXXK_HMD.TriGiaThuKhac, SXXK_HMD.MienThue
FROM         t_SXXK_HangMauDich AS SXXK_HMD INNER JOIN
                      t_SXXK_ToKhaiMauDich AS SXXK_TKMD ON SXXK_HMD.MaHaiQuan = SXXK_TKMD.MaHaiQuan AND SXXK_HMD.SoToKhai = SXXK_TKMD.SoToKhai AND 
                      SXXK_HMD.MaLoaiHinh = SXXK_TKMD.MaLoaiHinh AND SXXK_HMD.NamDangKy = SXXK_TKMD.NamDangKy INNER JOIN
                      t_KDT_ToKhaiMauDich AS KDT_TKMD ON SXXK_HMD.MaHaiQuan = KDT_TKMD.MaHaiQuan AND SXXK_TKMD.SoToKhai = KDT_TKMD.SoToKhai AND 
                      SXXK_TKMD.MaLoaiHinh = KDT_TKMD.MaLoaiHinh AND SXXK_TKMD.MaHaiQuan = KDT_TKMD.MaHaiQuan AND SXXK_TKMD.NamDangKy = KDT_TKMD.NamDK AND
                       SXXK_TKMD.MaDoanhNghiep = KDT_TKMD.MaDoanhNghiep LEFT OUTER JOIN
                      t_KDT_HangMauDich AS KDT_HMD ON KDT_TKMD.ID = KDT_HMD.TKMD_ID AND SXXK_HMD.MaHS = KDT_HMD.MaHS AND 
                      SXXK_HMD.MaPhu = KDT_HMD.MaPhu AND SXXK_HMD.DVT_ID = KDT_HMD.DVT_ID
Where KDT_HMD.MaPhu is null



