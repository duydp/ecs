﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;

namespace Company.Interface.Report
{
    
    public partial class ReportViewDinhMucForm : BaseForm
    {
        public DinhMucDangKy DMDangKy;
        ReportDinhMuc reportDM = new ReportDinhMuc();
        public BLL.SXXK.SanPhamCollection spCollection = new Company.BLL.SXXK.SanPhamCollection();
        public ReportViewDinhMucForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            chkNKvaND.Checked = GlobalSettings.inNKvaND;
            cboSanPham.Items.Clear();
            if (spCollection.Count == 0)
            {
                DataSet ds = DMDangKy.getSanPhamOfDinhMuc(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    cboSanPham.Items.Add(row[2].ToString(), row[2].ToString());
                }
            }
            else
            {
                foreach (BLL.SXXK.SanPham sp in spCollection)
                {
                    cboSanPham.Items.Add(sp.Ma,sp.Ma);
                }
            }           
        }     

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            reportDM = new ReportDinhMuc();
            this.reportDM.DMDangKy = DMDangKy;
            BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
            sp.Ma = cboSanPham.SelectedItem.Value.ToString();
            sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            sp.Load();
            this.reportDM.MaSP = cboSanPham.SelectedItem.Text;
            this.reportDM.TenSP = sp.Ten;
            this.reportDM.DVT = DonViTinh_GetName(sp.DVT_ID);
            GlobalSettings.inNKvaND = chkNKvaND.Checked;
            if (DMDangKy != null)
            {
                this.reportDM.HopDong = txtHopDong.Text;
                SetText(txtHopDong.Text, txtToKhai.Text);
                this.reportDM.inNKvaND = chkNKvaND.Checked;
                this.reportDM.BindReport();
            }
            else
            {
                this.reportDM.HopDong = txtHopDong.Text;
                this.reportDM.inNKvaND = chkNKvaND.Checked;
                this.reportDM.BindReportDinhMucDaDangKy();
            }
            printControl1.PrintingSystem = reportDM.PrintingSystem;
            this.reportDM.CreateDocument();
            reportDM.GetText = new ReportDinhMuc.GetObjectInfo(SetText);
        }
        private void SetText(string hopdong, string tokhai)
        {
            txtHopDong.Text = hopdong;
            txtToKhai.Text = tokhai;
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            try
            {
                this.reportDM.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }                
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            foreach (UIComboBoxItem item in cboSanPham.Items)
            {
                reportDM = new ReportDinhMuc();
                this.reportDM.DMDangKy = DMDangKy;
                BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = item.Text;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                sp.Load();
                this.reportDM.MaSP = item.Text;
                this.reportDM.TenSP = sp.Ten;
                this.reportDM.DVT = DonViTinh_GetName(sp.DVT_ID);
                GlobalSettings.inNKvaND = chkNKvaND.Checked;
                if (DMDangKy != null)
                {
                    SetText(txtHopDong.Text, txtToKhai.Text);
                    this.reportDM.inNKvaND = chkNKvaND.Checked;
                    this.reportDM.BindReport();
                    this.reportDM.HopDong = txtHopDong.Text;
                }
                else
                {
                    this.reportDM.BindReportDinhMucDaDangKy();
                    this.reportDM.inNKvaND = chkNKvaND.Checked;
                }
                printControl1.PrintingSystem = reportDM.PrintingSystem;
                this.reportDM.CreateDocument();
                reportDM.Print();
            }

        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            try
            {
                this.reportDM.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }   
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            reportDM.SetText(txtHopDong.Text, txtToKhai.Text);             
        }

    

  

    
    }
}