using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Security.Principal;
using System.Security.AccessControl;
using System.Configuration;
using System.Windows.Forms;

namespace Company.GC.BLL.Utils
{
    #region
    //public class Activation
    //{
    //    public static string[] Activate(string Serial, string ProductID, string CodeActivate)
    //    {
    //        string[] rs = null;
    //        try
    //        {
    //            Company.GC.BLL.WS.AS.ActivateService obj = new Company.GC.BLL.WS.AS.ActivateService();
    //            Company.GC.BLL.WS.AS.ActivateResults asRs = obj.Activate(Serial, ProductID, CodeActivate);
    //            if (asRs.Value)
    //            {
    //                rs = new string[4];
    //                rs[0] = asRs.Value.ToString();
    //                rs[1] = asRs.Information.LicenseName;
    //                rs[2] = asRs.Information.NumberClient.ToString();
    //                rs[3] = asRs.Information.DateExpires;
    //            }
    //            else
    //            {
    //                rs = new string[2];
    //                rs[0] = asRs.Value.ToString();
    //                rs[1] = asRs.Message;
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            throw e;
    //        }
    //        return rs;
    //    }
    //}
    #endregion

    public class License
    {
        #region Private members.
        private string ecsSys = "ECS.TQDT.GC";
        private string passEncryptToFile = @"ECS.HQDT";
        private string passEncryptToString = @"Softech.ECS";

        private string CodeActivate;
        public string codeActivate
        {
            get { return CodeActivate; }
            set { CodeActivate = value; }
        }

        private string LicenseName;
        public string licenseName
        {
            get { return LicenseName; }
            set { LicenseName = value; }
        }

        private string NumberClient;
        public string numberClient
        {
            get { return NumberClient; }
            set { NumberClient = value; }
        }

        private string DayExpires;
        public string dayExpires
        {
            get { return DayExpires; }
            set { DayExpires = value; }
        }

        private string DateTrial;
        public string dateTrial
        {
            get { return DateTrial; }
            set { DateTrial = value; }
        }

        private string LastDayTrial;
        public string lastDayTrial
        {
            get { return LastDayTrial; }
            set { LastDayTrial = value; }
        }
        #endregion

        public License()
        {
            //this.setPermission();
        }

        public void Load()
        {
            try
            {
                string fileName = Application.StartupPath + "\\" + ecsSys + ".lic";
                XmlDocument doc = new XmlDocument();
                doc = this.DecryptToXml(fileName);
                this.codeActivate = doc.SelectNodes("License").Item(0).SelectSingleNode("CodeActivate").InnerText;
                this.licenseName = doc.SelectNodes("License").Item(0).SelectSingleNode("LicenseName").InnerText;
                this.numberClient = doc.SelectNodes("License").Item(0).SelectSingleNode("NumberClient").InnerText;
                this.dayExpires = doc.SelectNodes("License").Item(0).SelectSingleNode("DayExpires").InnerText;
                this.dateTrial = doc.SelectNodes("License").Item(0).SelectSingleNode("DateTrial").InnerText;
                this.lastDayTrial = doc.SelectNodes("License").Item(0).SelectSingleNode("LastDayTrial").InnerText;
                if (this.lastDayTrial != DateTime.Now.ToShortDateString())
                {
                    this.lastDayTrial = DateTime.Now.ToShortDateString();
                    if (int.Parse(this.dateTrial) > 0)
                    {
                        int i = int.Parse(this.dateTrial) - 1;
                        this.dateTrial = i.ToString();
                    }
                }
                this.Save();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Save()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                string xmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
                xmlStr += "<License>";
                xmlStr += "<CodeActivate>" + this.codeActivate + "</CodeActivate>";
                xmlStr += "<LicenseName>" + this.licenseName + "</LicenseName>";
                xmlStr += "<NumberClient>" + this.numberClient + "</NumberClient>";
                xmlStr += "<DayExpires>" + this.dayExpires + "</DayExpires>";
                xmlStr += "<DateTrial>" + this.dateTrial + "</DateTrial>";
                xmlStr += "<LastDayTrial>" + this.lastDayTrial + "</LastDayTrial>";
                xmlStr += "</License>";
                doc.LoadXml(xmlStr);
                string fileName = Application.StartupPath + "\\" + ecsSys + ".lic";
                this.EncryptToFile(doc, fileName);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void generTrial()
        {
            License lic = new License();
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            int numInConf = int.Parse(lic.DecryptString(config.AppSettings.Settings["DateTr"].Value));

            this.codeActivate = "Trial";
            this.licenseName = "Phiên bản chưa đăng ký";
            this.numberClient = "1";
            this.dayExpires = DateTime.Now.Date.AddDays((double)numInConf).ToShortDateString();
            this.dateTrial = "" + numInConf;
            this.lastDayTrial = DateTime.Now.ToShortDateString();
            this.Save();
        }

        public bool checkExistsLicense()
        {
            string fileName = Application.StartupPath + "\\" + ecsSys + ".lic";
            return File.Exists(fileName);
        }

        public string[] Activate(string Serial, string ProductID, string CodeActivate)
        {
            string[] rs = null;
            try
            {
                Company.GC.BLL.WS.AS.ActivateService obj = new Company.GC.BLL.WS.AS.ActivateService();
                Company.GC.BLL.WS.AS.ActivateResults asRs = obj.Activate(Serial, ProductID, CodeActivate);
                if (asRs.Value)
                {
                    rs = new string[4];
                    rs[0] = asRs.Value.ToString();
                    rs[1] = asRs.Information.LicenseName;
                    rs[2] = asRs.Information.NumberClient.ToString();
                    rs[3] = asRs.Information.DateExpires;
                }
                else
                {
                    rs = new string[2];
                    rs[0] = asRs.Value.ToString();
                    rs[1] = asRs.Message;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return rs;
        }

        public List<License> LoadListFromFile(string inputFile)
        {
            List<License> listLic = new List<License>();
            License lic = new License();
            XmlDocument doc = new XmlDocument();
            doc = this.DecryptToXml(inputFile);

            XmlNode nod = doc.SelectNodes("License").Item(0);
            XmlNodeList nodList = nod.SelectNodes("CodeActivates").Item(0).SelectNodes("CodeActivate");
            for (int i = 0; i < nodList.Count; i++)
            {
                lic = new License();
                lic.generTrial();
                lic.codeActivate = nodList[i].InnerText;
                lic.licenseName = nod.SelectSingleNode("LicenseName").InnerText;
                lic.numberClient = nod.SelectSingleNode("NumberClient").InnerText;
                lic.dayExpires = nod.SelectSingleNode("DayExpires").InnerText;

                listLic.Add(lic);
            }

            return listLic;
        }

        #region Encrypt/Decrypt File
        private XmlDocument DecryptToXml(string inputFile)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(this.passEncryptToFile);
                FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
                RijndaelManaged RMCrypto = new RijndaelManaged();
                Stream st = new CryptoStream(fsCrypt, RMCrypto.CreateDecryptor(key, key), CryptoStreamMode.Read);
                xmlDoc.Load(st);
                fsCrypt.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xmlDoc;
        }

        private void EncryptToFile(XmlDocument doc, string outputFile)
        {
            try
            {
                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(this.passEncryptToFile);

                string cryptFile = outputFile;
                FileStream fsCrypt = new FileStream(cryptFile, FileMode.Create);

                RijndaelManaged RMCrypto = new RijndaelManaged();

                CryptoStream cs = new CryptoStream(fsCrypt, RMCrypto.CreateEncryptor(key, key), CryptoStreamMode.Write);

                MemoryStream ms = new MemoryStream();
                doc.Save(ms);
                ms.Flush();
                ms.Position = 0;
                int data;
                while ((data = ms.ReadByte()) != -1)
                    cs.WriteByte((byte)data);


                ms.Close();
                cs.Close();
                fsCrypt.Close();
            }
            catch
            {

            }
        }
        #endregion

        #region Encrypt/Decrypt String
        public string EncryptString(string plainText)
        {
            byte[] text = Encoding.ASCII.GetBytes(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] salt = Encoding.ASCII.GetBytes(this.passEncryptToString.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(this.passEncryptToString, salt);

            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            CryptoStream encStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            encStream.Write(text, 0, text.Length);

            encStream.FlushFinalBlock();

            byte[] CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            encStream.Close();

            return Convert.ToBase64String(CipherBytes);
        }

        public string DecryptString(string plainText)
        {
            byte[] text = Convert.FromBase64String(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] salt = Encoding.ASCII.GetBytes(this.passEncryptToString.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(this.passEncryptToString, salt);

            ICryptoTransform decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(text);

            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            text = new byte[text.Length];
            int DecryptedCount = cryptoStream.Read(text, 0, text.Length);

            memoryStream.Close();
            cryptoStream.Close();


            return Encoding.ASCII.GetString(text);
        }
        #endregion

        #region Get MD5 of String
        /**
         * Get MD5 of String
         * return string with 25 char.
         * */
        public static string md5String(string strToEncrypt)
        {
            UTF8Encoding ue = new UTF8Encoding();
            byte[] bytes = ue.GetBytes(strToEncrypt);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] hashBytes = md5.ComputeHash(bytes);
            string hashString = "";
            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashString += Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
            }
            return hashString.PadLeft(32, '0').Substring(0, 25).ToUpper();
        }

        #endregion

        #region Set permission for system folder in windows
        private void setPermission()
        {
            try
            {
                DirectoryInfo myDirectoryInfo = new DirectoryInfo(Environment.SystemDirectory);
                DirectorySecurity myDirectorySecurity = myDirectoryInfo.GetAccessControl();
                AuthorizationRuleCollection acl = myDirectorySecurity.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier));
                bool val = false;
                for (int i = 0; i < acl.Count; i++)
                {
                    FileSystemAccessRule currentRule = (FileSystemAccessRule)acl[i];
                    SecurityIdentifier a = (SecurityIdentifier)currentRule.IdentityReference;
                    SecurityIdentifier b = WindowsIdentity.GetCurrent().User;
                    //if (WindowsIdentity.GetCurrent().User.Equals((SecurityIdentifier)currentRule.IdentityReference))
                    //{
                    if (currentRule.FileSystemRights != FileSystemRights.FullControl || currentRule.AccessControlType != AccessControlType.Deny)
                    {
                        val = true;
                    }
                    //}
                }

                if (val)
                {
                    myDirectorySecurity.AddAccessRule(new FileSystemAccessRule(WindowsIdentity.GetCurrent().Name,
                                                   FileSystemRights.FullControl, AccessControlType.Allow));
                    myDirectoryInfo.SetAccessControl(myDirectorySecurity);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

    }

}
