﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Janus.Windows.GridEX;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;


namespace Company.Interface
{
    public partial class CoForm_V4 : Company.Interface.BaseForm
    {
        public bool isKhaiBoSung = false;
        FeedBackContent feedbackContent = new FeedBackContent();
        private string msgInfor = string.Empty;
        public ToKhaiMauDich TKMD;
        public CO Co = new CO();
        string versionSend = Company.KDT.SHARE.Components.Globals.VersionSend;
        public CoForm_V4()
        {
            InitializeComponent();
            Co.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;

            epError.Clear();
            if (string.IsNullOrEmpty(txtToChucCap.Text))
            {
                epError.SetError(txtToChucCap, "Không được để trống Tổ chức cấp");
                return;
            }
            if (string.IsNullOrEmpty(txtMaNguoiNhap.Text))
            {
                epError.SetError(txtMaNguoiNhap, "Không được để trống Mã người nhập");
                return;
            }
            if (string.IsNullOrEmpty(txtMaNguoiXuat.Text))
            {
                epError.SetError(txtMaNguoiXuat, "Không được để trống Mã người xuất");
                return;
            }
            if (string.IsNullOrEmpty(txtDiaChiNguoiNK.Text))
            {
                epError.SetError(txtDiaChiNguoiNK, "Không được để trống Địa chỉ người nhập khẩu");
                return;
            }
            if (string.IsNullOrEmpty(txtDiaChiNguoiXK.Text))
            {
                epError.SetError(txtDiaChiNguoiXK, "Không được để trống Địa chỉ người xuất khẩu");
                return;
            }

            if (string.IsNullOrEmpty(txtMoTaHangHoa.Text))
            {
                epError.SetError(txtMoTaHangHoa, "Không được để trống Thông tin mô tả");
                return;
            }

            if (CO.checkSoCOExit(txtSoCO.Text.Trim(), GlobalSettings.MA_DON_VI, TKMD.ID, Co.ID))
            {
                ShowMessage("Số CO này đã tồn tại.", false);
                return;
            }
            if (chkNoCo.Checked)
            {
                if (ccNgayNopCO.Text == "" || ccNgayNopCO.Value.Year <= 1900)
                {
                    ShowMessage("Nhập thông tin thời hạn nộp CO.", false);
                    return;
                }
            }

            //epError.Clear();
            if (ccNgayHetHan.Text == "" || ccNgayHetHan.Value.Year <= 1900)
            {
                string msg = "Ngày hết hạn không được để trống";
                epError.SetError(ccNgayHetHan, msg);
                ShowMessage(msg, false);
                return;
            }

            if (ccNgayKhoiHanh.Text == "" || ccNgayKhoiHanh.Value.Year <= 1900)
            {
                string msg = "Ngày khởi hành không được để trống";
                epError.SetError(ccNgayKhoiHanh, msg);
                ShowMessage(msg, false);
                return;
            }

            if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
            {
                if (ctrCangXepHang.Ten == "")
                {
                    string msg = "Tên cảng xếp hàng không được để trống";
                    epError.SetError(ctrCangXepHang, msg);
                    ShowMessage(msg, false);
                    return;
                }

                if (ctrCangDoHang.Ten == "")
                {
                    string msg = "Tên cảng dỡ hàng không được để trống";
                    epError.SetError(ctrCangDoHang, msg);
                    ShowMessage(msg, false);
                    return;
                }
            }
            else if (TKMD.MaLoaiHinh.Substring(0, 1) == "X")
            {
                if (ctrCangDoHang.Ten == "")
                {
                    string msg = "Tên cảng dỡ hàng không được để trống";
                    epError.SetError(ctrCangDoHang, msg);
                    ShowMessage(msg, false);
                    return;
                }

                if (ctrCangXepHang.Ten == "")
                {
                    string msg = "Tên cảng xếp hàng không được để trống";
                    epError.SetError(ctrCangXepHang, msg);
                    ShowMessage(msg, false);
                    return;
                }
            }

            //if (cc.Text == "" || ccNgayKhoiHanh.Value.Year <= 1900)
            //{
            //    string msg = "Ngày khởi hành không được để trống";
            //    epError.SetError(ccNgayKhoiHanh, msg);
            //    ShowMessage(msg, false);
            //    return;
            //}
            //             if (Co.ListHMDofCo == null || Co.ListHMDofCo.Count == 0)
            //             {
            //                 ShowMessage("Bạn chưa chọn hàng cho Co\r\nVui lòng chọn hàng trước khi lưu", false);
            //                 return;
            //             }
            //             else
            //             {
            // 
            //                 GridEXRow[] rows = dgList.GetRows();
            //                 foreach (GridEXRow row in rows)
            //                 {
            //                     DataRowView rowview = (DataRowView)row.DataRow;
            //                     string msg = string.Empty;
            //                     double dTrongLuong = Convert.ToDouble(row.Cells["TrongLuong"].Value);
            //                     if (dTrongLuong <= 0)
            //                     {
            //                         msg = string.Format("Dòng hàng thứ {0} \r\nTrọng lượng phải lớn hơn không", row.Position + 1);
            // 
            //                     }
            //                     else if (string.IsNullOrEmpty(row.Cells["LoaiKien"].Text))
            //                     {
            // 
            //                         msg = string.Format("Dòng hàng thứ {0} \r\nLoại kiện chưa nhập đầy đủ", row.Position + 1);
            //                     }
            //                     else if (string.IsNullOrEmpty(row.Cells["SoHieuKien"].Text))
            //                     {
            //                         msg = string.Format("Dòng hàng thứ {0} \r\nSố hiệu kiện chưa nhập đầy đủ", row.Position + 1);
            // 
            //                     }
            //                     else if (string.IsNullOrEmpty(row.Cells["SoHoaDon"].Text))
            //                     {
            //                         msg = string.Format("Dòng hàng thứ {0} \r\nSố hóa đơn chưa nhập đầy đủ", row.Position + 1);
            //                     }
            //                     else if (string.IsNullOrEmpty(row.Cells["NgayHoaDon"].Text) || Convert.ToDateTime(row.Cells["NgayHoaDon"].Value).Year <= 1900)
            //                     {
            //                         msg = string.Format("Dòng hàng thứ {0} \r\nNgày hóa đơn không hợp lệ", row.Position + 1);
            //                     }
            //                     if (msg != string.Empty)
            //                     {
            //                         ShowMessage(msg, false);
            //                         return;
            //                     }
            //                     string idHang = rowview["HMD_ID"].ToString().Trim();
            //                     HangCoDetail hangCo = Co.ListHMDofCo.Find(hang => hang.HMD_ID.ToString() == idHang);
            //                     if (hangCo != null)
            //                     {
            //                         hangCo.LoaiKien = row.Cells["LoaiKien"].Text;
            //                         hangCo.SoHieuKien = row.Cells["SoHieuKien"].Text;
            //                         hangCo.SoHoaDon = row.Cells["SoHoaDon"].Text;
            //                         hangCo.GhiChu = row.Cells["GhiChu"].Text;
            //                         hangCo.TrongLuong = dTrongLuong;
            //                         hangCo.NgayHoaDon = Convert.ToDateTime(row.Cells["NgayHoaDon"].Value);
            //                     }
            // 
            //                 }
            //             }

            if (Co.NgayTiepNhan.Year < 1900)
                Co.NgayTiepNhan = new DateTime(1900, 1, 1);
            Co.LoaiCO = cbLoaiCO.SelectedValue.ToString();
            Co.MaNuocNKTrenCO = ctrMaNuocNK.Ma;
            Co.MaNuocXKTrenCO = ctrMaNuocXK.Ma;
            Co.NgayCO = ccNgayCO.Value;
            Co.NuocCapCO = ctrNuocCapCO.Ma;
            Co.SoCO = txtSoCO.Text.Trim();
            Co.TenDiaChiNguoiNK = txtDiaChiNguoiNK.Text.Trim();
            Co.TenDiaChiNguoiXK = txtDiaChiNguoiXK.Text.Trim();
            Co.ThongTinMoTaChiTiet = txtThongTinMoTa.Text.Trim();
            Co.ToChucCap = txtToChucCap.Text.Trim();
            Co.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            Co.TKMD_ID = TKMD.ID;
            Co.NguoiKy = txtNguoiKy.Text.Trim();
            Co.ThoiHanNop = ccNgayNopCO.Value;
            Co.MaNguoiNK = txtMaNguoiNhap.Text.Trim();
            Co.MaNguoiXK = txtMaNguoiXuat.Text.Trim();
            Co.HamLuongXuatXu = Convert.ToDouble(txtHamLuong.Value);
            Co.MoTaHangHoa = txtMoTaHangHoa.Text;
            #region V3
            Co.NgayHetHan = ccNgayHetHan.Value;
            Co.NgayKhoiHanh = ccNgayKhoiHanh.Value;
            Co.CangDoHang = ctrCangDoHang.Ma;
            Co.CangXepHang = ctrCangXepHang.Ma;
            #endregion

            //CO Bo sung
            Co.TenCangXepHang = ctrCangXepHang.Ten;
            Co.TenCangDoHang = ctrCangDoHang.Ten;

            if (string.IsNullOrEmpty(Co.GuidStr)) Co.GuidStr = Guid.NewGuid().ToString();
            if (chkNoCo.Checked)
                Co.NoCo = 1;
            else
                Co.NoCo = 0;
            if (isKhaiBoSung)
                Co.LoaiKB = 1;
            else
                Co.LoaiKB = 0;
            try
            {
                bool isAddNew = Co.ID == 0;
                Co.InsertUpdateFull();
                if (isAddNew)
                    TKMD.COCollection.Add(Co);
                ShowMessage("Lưu thành công.", false);
                if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET && TKMD.SoToKhai > 0)
                    btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }

        private void CoForm_Load(object sender, EventArgs e)
        {
            if (Co != null)
            {
                Company.KDT.SHARE.Components.Common.COBoSung coBS = CO.LoadCoBoSung(Co.ID);
                if (coBS != null)
                {
                    Co.TenCangDoHang = coBS.TenCangDoHang;
                    Co.TenCangXepHang = coBS.TenCangXepHang;
                }
            }

            if (TKMD.MaLoaiHinh.Substring(0, 1) == "N")
            {
                ctrCangXepHang.IsLoadData = false;
                ctrCangXepHang.loadData();

                ctrCangXepHang.Ten = Co.TenCangXepHang;

                if (Co != null && Co.ID > 0)
                    ctrCangDoHang.Ma = Co.CangDoHang;
            }
            else
            {
                ctrCangDoHang.IsLoadData = false;
                ctrCangDoHang.loadData();

                ctrCangDoHang.Ten = Co.TenCangDoHang;

                if (Co != null && Co.ID > 0)
                    ctrCangXepHang.Ma = Co.CangXepHang;
            }

            ccNgayNopCO.Enabled = false;
            cbLoaiCO.DataSource = LoaiCO.SelectAll().Tables[0];
            cbLoaiCO.ValueMember = "Ma";
            cbLoaiCO.DisplayMember = "Ten";
            cbLoaiCO.SelectedIndex = 0;

            //DATLMQ bổ sung tạo mới Co 17/01/2011
            if (Co == null || Co.ID == 0)
            {
                txtDiaChiNguoiXK.Text = TKMD.TenDonViDoiTac;
                txtDiaChiNguoiNK.Text = TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI;
                txtMaNguoiNhap.Text = TKMD.MaDoanhNghiep;

            }
            else if (Co != null && Co.ID > 0)
            {
                ctrMaNuocNK.Ma = Co.MaNuocNKTrenCO;
                ctrMaNuocXK.Ma = Co.MaNuocXKTrenCO;
                ccNgayCO.Text = Co.NgayCO.ToShortDateString();
                ctrNuocCapCO.Ma = Co.NuocCapCO;
                txtSoCO.Text = Co.SoCO;
                txtDiaChiNguoiNK.Text = Co.TenDiaChiNguoiNK;
                txtDiaChiNguoiXK.Text = Co.TenDiaChiNguoiXK;
                txtThongTinMoTa.Text = Co.ThongTinMoTaChiTiet;
                txtToChucCap.Text = Co.ToChucCap;
                cbLoaiCO.SelectedValue = Co.LoaiCO;
                txtNguoiKy.Text = Co.NguoiKy;
                #region V3
                ccNgayHetHan.Text = Co.NgayHetHan.ToShortDateString();
                ccNgayKhoiHanh.Text = Co.NgayKhoiHanh.ToShortDateString();
                //ctrCangDoHang.Ma = Co.CangDoHang;
                //ctrCangXepHang.Ma = Co.CangXepHang;
                txtMoTaHangHoa.Text = Co.MoTaHangHoa;
                txtMaNguoiNhap.Text = Co.MaNguoiNK;
                txtMaNguoiXuat.Text = Co.MaNguoiXK;
                txtHamLuong.Value = Co.HamLuongXuatXu;


                #endregion
                if (Co.NoCo == 0)
                    chkNoCo.Checked = false;
                else
                {
                    chkNoCo.Checked = true;
                    ccNgayNopCO.Text = Co.ThoiHanNop.ToShortDateString();
                }

                BindData();
            }
            if (TKMD.ID > 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;

            }
            if (Co.SoTiepNhan > 0)
            {
                ccNgayTiepNhan.Text = Co.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = Co.SoTiepNhan + "";
            }
            if (Co.TrangThai == TrangThaiXuLy.DA_DUYET)
            {
                lbTrangThai.Text = "Trạng thái: Đã duyệt";
            }
            SetButtonStateCO(TKMD, Co);
            cmdThuTucHQTruocDo.Visible = Company.KDT.SHARE.Components.Globals.IsTest;
        }

        private void btnChonCO_Click(object sender, EventArgs e)
        {
            ManageCOForm f = new ManageCOForm();
            f.isBrower = true;
            f.TKMD_ID = TKMD.ID;
            f.ShowDialog(this);
            if (f.Co != null)
            {
                Co = f.Co;
                ctrMaNuocNK.Ma = Co.MaNuocNKTrenCO;
                ctrMaNuocXK.Ma = Co.MaNuocXKTrenCO;
                ccNgayCO.Text = Co.NgayCO.ToShortDateString();
                ctrNuocCapCO.Ma = Co.NuocCapCO;
                txtSoCO.Text = Co.SoCO;
                txtDiaChiNguoiNK.Text = Co.TenDiaChiNguoiNK;
                txtDiaChiNguoiXK.Text = Co.TenDiaChiNguoiXK;
                txtThongTinMoTa.Text = Co.ThongTinMoTaChiTiet;
                txtToChucCap.Text = Co.ToChucCap;
                cbLoaiCO.SelectedValue = Co.LoaiCO;
                txtNguoiKy.Text = Co.NguoiKy;
                Co.ID = 0;
                Co.GuidStr = "";
                Co.SoTiepNhan = 0;
                Co.NgayTiepNhan = new DateTime(1900, 1, 1);
                Co.NamTiepNhan = 0;
                Co.TKMD_ID = TKMD.ID;
                if (Co.NoCo == 0)
                    chkNoCo.Checked = false;
                else
                {
                    chkNoCo.Checked = true;
                    ccNgayNopCO.Value = Co.ThoiHanNop;
                }

            }
        }

        private void BindData()
        {
            //             dgList.DataSource = Co.ConvertListToDataSet(HangMauDich.SelectBy_TKMD_ID(TKMD.ID).Tables[0]);
            //             try { dgList.Refetch(); }
            //             catch { dgList.Refresh(); }
        }


        private void chkNoCo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNoCo.Checked)
            {
                //lblNgayNopCo.Visible = true;
                ccNgayNopCO.Enabled = true;
            }
            else
            {
                // lblNgayNopCo.Visible = false;
                ccNgayNopCO.Enabled = false;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            if (Co != null && Co.ID > 0)
            {
                if (ShowMessage("Bạn có muốn xóa Co và hàng đi kèm này không?", true) == "Yes")
                    try
                    {
                        Co.DeleteCoFull();
                        TKMD.COCollection.Remove(Co);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi không thể lưu dữ liệu:\r\n" + ex.Message, false);
                    }


            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (Co.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }

            try
            {
                if (Co.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO || Co.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
                    Co.GuidStr = Guid.NewGuid().ToString();
                else if (Co.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    ShowMessage("Bạn đã gửi yêu cầu khai báo bổ sung Co đến hải quan\r\nVui lòng nhận phản hồi thông tin", false);
                    return;
                }
                #region V3
                ObjectSend msgSend = SingleMessage.BoSungCO(TKMD, Co);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;

                bool isSend = sendMessageForm.DoSend(msgSend);
                if ((Co.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                    Co.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET) && isSend)
                {
                    sendMessageForm.Message.XmlSaveMessage(Co.ID, MessageTitle.KhaiBaoBoSungCO);
                    Co.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    Co.Update();

                }
                SetButtonStateCO(TKMD, Co);
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }


        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBackBoSungCo(Co, TKMD);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                isFeedBack = sendMessageForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetButtonStateCO(TKMD, Co);
                }

                //             ObjectSend msgSend = SingleMessage.FeedBackBoSungCo(Co, TKMD);
                //             SendMessageForm sendMessageForm = new SendMessageForm();
                //             sendMessageForm.Send += SendMessage;
                //             sendMessageForm.DoSend(msgSend);
                //             SetButtonStateCO(TKMD, Co);
            }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (Co.GuidStr != null && Co.GuidStr != "")
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = Co.ID;
                form.DeclarationIssuer = AdditionalDocumentType.EXPORT_CO_FORM_D;
                form.ShowDialog(this);
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form CO.
        /// LanNT edit 03/01/2012
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private void SetButtonStateCO(ToKhaiMauDich tkmd, Company.KDT.SHARE.QuanLyChungTu.CO co)
        {
            if (co == null)
                return;
            //Khai bao moi
            if (!isKhaiBoSung)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                bool status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                      || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                      || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnChonGP.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else if (tkmd.SoToKhai > 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {
                btnKetQuaXuLy.Enabled = true;
                //Re set button declaration LanNT


                bool khaiBaoEnable = (Co.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     Co.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnXoa.Enabled = btnChonGP.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = Co.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   Co.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   Co.TrangThai == TrangThaiXuLy.CHO_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }
        }

        #endregion

        /// <summary>
        /// Lấy phản hồi sau khi khai báo. Mục đích là lấy số tiếp nhận điện tử.
        /// </summary>
        //         private void LayPhanHoiKhaiBao(string password)
        //         {
        //         StartInvoke:
        //             try
        //             {
        //                 bool thanhcong = Co.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
        // 
        //                 // Thực hiện kiểm tra.  
        //                 if (thanhcong == false)
        //                 {
        //                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
        //                     {
        //                         this.Refresh();
        //                         goto StartInvoke;
        //                     }
        // 
        //                     btnKhaiBao.Enabled = true;
        //                     btnLayPhanHoi.Enabled = true;
        //                 }
        //                 else
        //                 {
        //                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.Co.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungCOThanhCong);
        //                     this.ShowMessage(message, false);
        // 
        //                     btnKhaiBao.Enabled = false;
        //                     txtSoTiepNhan.Text = this.Co.SoTiepNhan.ToString("N0");
        //                     ccNgayTiepNhan.Value = this.Co.NgayTiepNhan;
        //                     ccNgayTiepNhan.Text = this.Co.NgayTiepNhan.ToShortDateString();
        //                 }
        //             }
        //             catch (Exception ex)
        //             {
        //                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
        //             }
        //         }

        /// <summary>
        /// Lấy thông tin phản hồi sau khi đã khai báo có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        //         private void LayPhanHoiDuyet(string password)
        //         {
        //         StartInvoke:
        //             try
        //             {
        //                 bool thanhcong = Co.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
        // 
        //                 // Thực hiện kiểm tra.  
        //                 if (thanhcong == false)
        //                 {
        //                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
        //                     {
        //                         this.Refresh();
        //                         goto StartInvoke;
        //                     }
        // 
        //                     btnLayPhanHoi.Enabled = true;
        //                 }
        //                 else
        //                 {
        //                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.Co.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungCODuocChapNhan);
        //                     if (message.Length == 0)
        //                     {
        //                         message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.Co.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
        //                         txtSoTiepNhan.Text = "";
        //                         ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
        //                         ccNgayTiepNhan.Text = "";
        //                     }
        //                     else
        //                         btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
        //                     this.ShowMessage(message, false);
        //                 }
        //             }
        //             catch (Exception ex)
        //             {
        //                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
        //             }
        //         }

        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            f.ShowDialog(this);

            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (HangMauDich HMD in f.HMDTMPCollection)
                {
                    //bool ok = false;
                    //foreach (HangCoDetail hangCo in Co.ListHMDofCo)
                    //{
                    //    if (hangCo.HMD_ID == HMD.ID)
                    //    {
                    //        ok = true;
                    //        break;
                    //    }
                    //}
                    HangCoDetail hangCo = Co.ListHMDofCo.Find(hang => hang.HMD_ID == HMD.ID);
                    if (hangCo == null)
                    {
                        hangCo = new HangCoDetail();
                        hangCo.HMD_ID = HMD.ID;
                        hangCo.Co_ID = Co.ID;
                        hangCo.MaPhu = HMD.MaPhu;
                        hangCo.MaHS = HMD.MaHS;
                        hangCo.TenHang = HMD.TenHang;
                        hangCo.DVT_ID = HMD.DVT_ID;
                        hangCo.SoThuTuHang = HMD.SoThuTuHang;
                        hangCo.SoLuong = HMD.SoLuong;
                        hangCo.NuocXX_ID = HMD.NuocXX_ID;
                        hangCo.MaNguyenTe = TKMD.NguyenTe_ID;
                        hangCo.DonGiaKB = Convert.ToDouble(HMD.DonGiaKB);
                        hangCo.TriGiaKB = Convert.ToDouble(HMD.TriGiaKB);
                        hangCo.TrongLuong = Convert.ToDouble(HMD.TrongLuong);
                        hangCo.NgayHoaDon = DateTime.Now;
                        Co.ListHMDofCo.Add(hangCo);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void btnXoaHang_Click(object sender, EventArgs e)
        {
            // 
            //             List<HangCoDetail> HangsInCo = new List<HangCoDetail>();
            //             GridEXSelectedItemCollection items = dgList.SelectedItems;
            //             if (dgList.GetRows().Length < 0) return;
            //             if (items.Count <= 0) return;
            //             if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            //             {
            //                 foreach (GridEXSelectedItem i in items)
            //                 {
            //                     if (i.RowType == RowType.Record)
            //                     {
            //                         HangCoDetail hangCo = new HangCoDetail();
            //                         DataRowView row = (DataRowView)i.GetRow().DataRow;
            //                         hangCo.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
            //                         hangCo.ID = Convert.ToInt64(row["ID"]);
            //                         HangsInCo.Add(hangCo);
            //                     }
            //                 }
            //                 foreach (HangCoDetail hangCo in HangsInCo)
            //                 {
            //                     try
            //                     {
            //                         if (hangCo.ID > 0)
            //                         {
            //                             hangCo.Delete();
            //                         }
            //                         foreach (HangCoDetail hgpDetail in Co.ListHMDofCo)
            //                         {
            //                             if (hgpDetail.HMD_ID == hangCo.HMD_ID)
            //                             {
            //                                 Co.ListHMDofCo.Remove(hgpDetail);
            //                                 break;
            //                             }
            //                         }
            //                     }
            //                     catch { }
            //                 }
            //             }
            //             BindData();
        }
        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(Co.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                Co.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                btnKhaiBao.Enabled = true;
                                btnLayPhanHoi.Enabled = false;
                                //this.ShowMessageTQDT("Hải quan từ chối tiếp nhận", noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            //noidung = noidung.Replace(string.Format("Message [{0}]", Co.GuidStr), string.Empty);
                            //this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                Co.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);

                                if (feedbackContent.Acceptance.Length == 10)
                                    Co.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd", null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    Co.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                else
                                    Co.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);

                                Co.NamTiepNhan = Co.NgayTiepNhan.Year;
                                e.FeedBackMessage.XmlSaveMessage(Co.ID, MessageTitle.KhaiBaoBoSungLaySoTiepNhanCO, noidung);
                                Co.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                //this.ShowMessageTQDT("Hải quan cấp số tiếp nhận\r\nSố tiếp nhận: " + Co.SoTiepNhan + "\r\nNgày tiếp nhận:" + Co.NgayTiepNhan.ToLongTimeString(), false);

                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(Co.ID, MessageTitle.KhaiBaoBoSungCOThanhCong, noidung);
                                Co.TrangThai = TrangThaiXuLy.DA_DUYET;
                                // ShowMessageTQDT("CO đã được duyệt.\n Thông tin trả về từ hải quan: " + noidung, false);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(Co.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }
                    if (isUpdate)
                        Co.Update();
                    msgInfor = noidung;
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion

        private void cmdThuTucHQTruocDo_Click(object sender, EventArgs e)
        {
            if (Co.ID == 0)
                ShowMessage("Vui lòng lưu thông tin giấy CO trước khi thêm chứng từ HQ trước đó", false);
            else
            {
                ListChungTuTruocDoForm f = new ListChungTuTruocDoForm();
                f.Master_ID = Co.ID;
                f.Type = "CO";
                f.ShowDialog(this);
            }
        }
    }
}

