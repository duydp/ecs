﻿namespace Company.KDT.SHARE.Components.Controls
{
    partial class TKMDVersionControlV
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblCKS = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblVersion.Location = new System.Drawing.Point(5, 2);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(87, 13);
            this.lblVersion.TabIndex = 0;
            this.lblVersion.Text = "Phiên bản: V?";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCKS
            // 
            this.lblCKS.AutoSize = true;
            this.lblCKS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCKS.ForeColor = System.Drawing.Color.Tomato;
            this.lblCKS.Location = new System.Drawing.Point(5, 21);
            this.lblCKS.Margin = new System.Windows.Forms.Padding(0);
            this.lblCKS.Name = "lblCKS";
            this.lblCKS.Size = new System.Drawing.Size(75, 13);
            this.lblCKS.TabIndex = 1;
            this.lblCKS.Text = "CKS: Không";
            this.lblCKS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TKMDVersionControlV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblCKS);
            this.Controls.Add(this.lblVersion);
            this.Name = "TKMDVersionControlV";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Size = new System.Drawing.Size(98, 41);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblCKS;
    }
}
