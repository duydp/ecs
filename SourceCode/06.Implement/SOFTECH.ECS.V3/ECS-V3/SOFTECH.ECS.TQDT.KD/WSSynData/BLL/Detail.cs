﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Company.KDT.SHARE.Components;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;

namespace WSSynData
{
    public partial class Detail
    {

        public void TaoDuLieu(SyncDaTaDetail thongtinmess)
        {
            
            this.GUIDSTR = thongtinmess.Reference;
            this.SoTiepNhan = (long)Convert.ToInt64(thongtinmess.CustomsReference);
            this.SoToKhai = (long)Convert.ToInt64(thongtinmess.Number);
            this.NgayDangKy = Convert.ToDateTime(thongtinmess.Acceptance);
            this.PhanHe = thongtinmess.Type;
            this.MaLoaiHinh = thongtinmess.NatureOfTransaction;
            this.PhanLuong = thongtinmess.StreamlineContent;

            this.MaDoanhNghiep = thongtinmess.Business;
            this.TenDoanhNghiep = "";
            
            this.MaDaiLy = thongtinmess.Issuer;
            this.TenDaiLy = "";

            this.MaHaiQuan = thongtinmess.DeclarationOffice;
            this.NgayGui = DateTime.Now;
        }
        public static IDataReader SelectCollection(string phanhe,string MaDoanhNghiep, DateTime TuNgay, DateTime DenNgay)
        {
            const string spName = "[dbo].[p_MSG_DBDL_Detail_LoadCollection]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.ToString());
            db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.ToString());
            db.AddInParameter(dbCommand, "@PhanHe", SqlDbType.Char, phanhe);
            return db.ExecuteReader(dbCommand);
        }
        public static List<Detail> LoadCollectionDetail(string PhanHe,string MaDoanhNghiep, DateTime TuNgay, DateTime DenNgay)
        {
            List<Detail> collection = new List<Detail>();

            SqlDataReader reader = (SqlDataReader)SelectCollection(PhanHe,MaDoanhNghiep, TuNgay, DenNgay);
            while (reader.Read())
            {
                Detail entity = new Detail();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanHe"))) entity.PhanHe = reader.GetString(reader.GetOrdinal("PhanHe"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLy"))) entity.TenDaiLy = reader.GetString(reader.GetOrdinal("TenDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGui"))) entity.NgayGui = reader.GetDateTime(reader.GetOrdinal("NgayGui"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public static List<Detail> LoadCollectionDetail(string phanhe, string madoanhnghiep, DateTime TuNgay, DateTime DenNgay, bool TimTatCa)
        {
            List<Detail> collection = new List<Detail>();
            string where = string.Format(@"(PhanHe= '{0}' AND MaDoanhNghiep = '{1}') AND (NgayGui BETWEEN '{2}' AND '{3}') AND TrangThai = {4}", phanhe, madoanhnghiep, TuNgay.ToShortDateString(), DenNgay.ToShortDateString(), 0);
            collection = Detail.SelectCollectionDynamic(where, null);
            return collection;
        }
        public static int DeleteByGuiStr(string guiStr,SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_MSG_DBDL_Detail_DeleteByGuiStr]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, guiStr);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public static Detail LoadByGuiStr(string GuiStr)
        {
            const string spName = "[dbo].[p_MSG_DBDL_Detail_LoadByGuiStr]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GuiStr);
            Detail entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new Detail();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanHe"))) entity.PhanHe = reader.GetString(reader.GetOrdinal("PhanHe"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLy"))) entity.TenDaiLy = reader.GetString(reader.GetOrdinal("TenDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGui"))) entity.NgayGui = reader.GetDateTime(reader.GetOrdinal("NgayGui"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
            }
            reader.Close();
            return entity;
        }
    }
}
