using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_TKVC_TrungChuyen : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string MaDiaDiemTrungChuyen { set; get; }
		public string TenDiaDiemTrungChuyen { set; get; }
		public DateTime NgayDenDiaDiem_TC { set; get; }
		public DateTime NgayDenThucTe { set; get; }
		public DateTime NgayDiDiaDiem_TC { set; get; }
		public DateTime NgayDiThucTe { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_TKVC_TrungChuyen> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_TKVC_TrungChuyen> collection = new List<KDT_VNACC_TKVC_TrungChuyen>();
			while (reader.Read())
			{
				KDT_VNACC_TKVC_TrungChuyen entity = new KDT_VNACC_TKVC_TrungChuyen();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemTrungChuyen"))) entity.MaDiaDiemTrungChuyen = reader.GetString(reader.GetOrdinal("MaDiaDiemTrungChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemTrungChuyen"))) entity.TenDiaDiemTrungChuyen = reader.GetString(reader.GetOrdinal("TenDiaDiemTrungChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenDiaDiem_TC"))) entity.NgayDenDiaDiem_TC = reader.GetDateTime(reader.GetOrdinal("NgayDenDiaDiem_TC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenThucTe"))) entity.NgayDenThucTe = reader.GetDateTime(reader.GetOrdinal("NgayDenThucTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDiDiaDiem_TC"))) entity.NgayDiDiaDiem_TC = reader.GetDateTime(reader.GetOrdinal("NgayDiDiaDiem_TC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDiThucTe"))) entity.NgayDiThucTe = reader.GetDateTime(reader.GetOrdinal("NgayDiThucTe"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_TKVC_TrungChuyen> collection, long id)
        {
            foreach (KDT_VNACC_TKVC_TrungChuyen item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_TKVC_TrungChuyen VALUES(@Master_ID, @MaDiaDiemTrungChuyen, @TenDiaDiemTrungChuyen, @NgayDenDiaDiem_TC, @NgayDenThucTe, @NgayDiDiaDiem_TC, @NgayDiThucTe)";
            string update = "UPDATE t_KDT_VNACC_TKVC_TrungChuyen SET Master_ID = @Master_ID, MaDiaDiemTrungChuyen = @MaDiaDiemTrungChuyen, TenDiaDiemTrungChuyen = @TenDiaDiemTrungChuyen, NgayDenDiaDiem_TC = @NgayDenDiaDiem_TC, NgayDenThucTe = @NgayDenThucTe, NgayDiDiaDiem_TC = @NgayDiDiaDiem_TC, NgayDiThucTe = @NgayDiThucTe WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TKVC_TrungChuyen WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemTrungChuyen", SqlDbType.VarChar, "MaDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemTrungChuyen", SqlDbType.VarChar, "TenDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenDiaDiem_TC", SqlDbType.DateTime, "NgayDenDiaDiem_TC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenThucTe", SqlDbType.DateTime, "NgayDenThucTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDiDiaDiem_TC", SqlDbType.DateTime, "NgayDiDiaDiem_TC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDiThucTe", SqlDbType.DateTime, "NgayDiThucTe", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemTrungChuyen", SqlDbType.VarChar, "MaDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemTrungChuyen", SqlDbType.VarChar, "TenDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenDiaDiem_TC", SqlDbType.DateTime, "NgayDenDiaDiem_TC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenThucTe", SqlDbType.DateTime, "NgayDenThucTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDiDiaDiem_TC", SqlDbType.DateTime, "NgayDiDiaDiem_TC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDiThucTe", SqlDbType.DateTime, "NgayDiThucTe", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_TKVC_TrungChuyen VALUES(@Master_ID, @MaDiaDiemTrungChuyen, @TenDiaDiemTrungChuyen, @NgayDenDiaDiem_TC, @NgayDenThucTe, @NgayDiDiaDiem_TC, @NgayDiThucTe)";
            string update = "UPDATE t_KDT_VNACC_TKVC_TrungChuyen SET Master_ID = @Master_ID, MaDiaDiemTrungChuyen = @MaDiaDiemTrungChuyen, TenDiaDiemTrungChuyen = @TenDiaDiemTrungChuyen, NgayDenDiaDiem_TC = @NgayDenDiaDiem_TC, NgayDenThucTe = @NgayDenThucTe, NgayDiDiaDiem_TC = @NgayDiDiaDiem_TC, NgayDiThucTe = @NgayDiThucTe WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TKVC_TrungChuyen WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemTrungChuyen", SqlDbType.VarChar, "MaDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemTrungChuyen", SqlDbType.VarChar, "TenDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenDiaDiem_TC", SqlDbType.DateTime, "NgayDenDiaDiem_TC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDenThucTe", SqlDbType.DateTime, "NgayDenThucTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDiDiaDiem_TC", SqlDbType.DateTime, "NgayDiDiaDiem_TC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDiThucTe", SqlDbType.DateTime, "NgayDiThucTe", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemTrungChuyen", SqlDbType.VarChar, "MaDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemTrungChuyen", SqlDbType.VarChar, "TenDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenDiaDiem_TC", SqlDbType.DateTime, "NgayDenDiaDiem_TC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDenThucTe", SqlDbType.DateTime, "NgayDenThucTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDiDiaDiem_TC", SqlDbType.DateTime, "NgayDiDiaDiem_TC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDiThucTe", SqlDbType.DateTime, "NgayDiThucTe", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_TKVC_TrungChuyen Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_TKVC_TrungChuyen> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_TKVC_TrungChuyen> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_TKVC_TrungChuyen> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_TKVC_TrungChuyen(long master_ID, string maDiaDiemTrungChuyen, string tenDiaDiemTrungChuyen, DateTime ngayDenDiaDiem_TC, DateTime ngayDenThucTe, DateTime ngayDiDiaDiem_TC, DateTime ngayDiThucTe)
		{
			KDT_VNACC_TKVC_TrungChuyen entity = new KDT_VNACC_TKVC_TrungChuyen();	
			entity.Master_ID = master_ID;
			entity.MaDiaDiemTrungChuyen = maDiaDiemTrungChuyen;
			entity.TenDiaDiemTrungChuyen = tenDiaDiemTrungChuyen;
			entity.NgayDenDiaDiem_TC = ngayDenDiaDiem_TC;
			entity.NgayDenThucTe = ngayDenThucTe;
			entity.NgayDiDiaDiem_TC = ngayDiDiaDiem_TC;
			entity.NgayDiThucTe = ngayDiThucTe;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaDiaDiemTrungChuyen", SqlDbType.VarChar, MaDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@TenDiaDiemTrungChuyen", SqlDbType.VarChar, TenDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@NgayDenDiaDiem_TC", SqlDbType.DateTime, NgayDenDiaDiem_TC.Year <= 1753 ? DBNull.Value : (object) NgayDenDiaDiem_TC);
			db.AddInParameter(dbCommand, "@NgayDenThucTe", SqlDbType.DateTime, NgayDenThucTe.Year <= 1753 ? DBNull.Value : (object) NgayDenThucTe);
			db.AddInParameter(dbCommand, "@NgayDiDiaDiem_TC", SqlDbType.DateTime, NgayDiDiaDiem_TC.Year <= 1753 ? DBNull.Value : (object) NgayDiDiaDiem_TC);
			db.AddInParameter(dbCommand, "@NgayDiThucTe", SqlDbType.DateTime, NgayDiThucTe.Year <= 1753 ? DBNull.Value : (object) NgayDiThucTe);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_TKVC_TrungChuyen> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_TrungChuyen item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_TKVC_TrungChuyen(long id, long master_ID, string maDiaDiemTrungChuyen, string tenDiaDiemTrungChuyen, DateTime ngayDenDiaDiem_TC, DateTime ngayDenThucTe, DateTime ngayDiDiaDiem_TC, DateTime ngayDiThucTe)
		{
			KDT_VNACC_TKVC_TrungChuyen entity = new KDT_VNACC_TKVC_TrungChuyen();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaDiaDiemTrungChuyen = maDiaDiemTrungChuyen;
			entity.TenDiaDiemTrungChuyen = tenDiaDiemTrungChuyen;
			entity.NgayDenDiaDiem_TC = ngayDenDiaDiem_TC;
			entity.NgayDenThucTe = ngayDenThucTe;
			entity.NgayDiDiaDiem_TC = ngayDiDiaDiem_TC;
			entity.NgayDiThucTe = ngayDiThucTe;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaDiaDiemTrungChuyen", SqlDbType.VarChar, MaDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@TenDiaDiemTrungChuyen", SqlDbType.VarChar, TenDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@NgayDenDiaDiem_TC", SqlDbType.DateTime, NgayDenDiaDiem_TC.Year <= 1753 ? DBNull.Value : (object) NgayDenDiaDiem_TC);
			db.AddInParameter(dbCommand, "@NgayDenThucTe", SqlDbType.DateTime, NgayDenThucTe.Year <= 1753 ? DBNull.Value : (object) NgayDenThucTe);
			db.AddInParameter(dbCommand, "@NgayDiDiaDiem_TC", SqlDbType.DateTime, NgayDiDiaDiem_TC.Year <= 1753 ? DBNull.Value : (object) NgayDiDiaDiem_TC);
			db.AddInParameter(dbCommand, "@NgayDiThucTe", SqlDbType.DateTime, NgayDiThucTe.Year <= 1753 ? DBNull.Value : (object) NgayDiThucTe);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_TKVC_TrungChuyen> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_TrungChuyen item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_TKVC_TrungChuyen(long id, long master_ID, string maDiaDiemTrungChuyen, string tenDiaDiemTrungChuyen, DateTime ngayDenDiaDiem_TC, DateTime ngayDenThucTe, DateTime ngayDiDiaDiem_TC, DateTime ngayDiThucTe)
		{
			KDT_VNACC_TKVC_TrungChuyen entity = new KDT_VNACC_TKVC_TrungChuyen();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaDiaDiemTrungChuyen = maDiaDiemTrungChuyen;
			entity.TenDiaDiemTrungChuyen = tenDiaDiemTrungChuyen;
			entity.NgayDenDiaDiem_TC = ngayDenDiaDiem_TC;
			entity.NgayDenThucTe = ngayDenThucTe;
			entity.NgayDiDiaDiem_TC = ngayDiDiaDiem_TC;
			entity.NgayDiThucTe = ngayDiThucTe;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaDiaDiemTrungChuyen", SqlDbType.VarChar, MaDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@TenDiaDiemTrungChuyen", SqlDbType.VarChar, TenDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@NgayDenDiaDiem_TC", SqlDbType.DateTime, NgayDenDiaDiem_TC.Year <= 1753 ? DBNull.Value : (object) NgayDenDiaDiem_TC);
			db.AddInParameter(dbCommand, "@NgayDenThucTe", SqlDbType.DateTime, NgayDenThucTe.Year <= 1753 ? DBNull.Value : (object) NgayDenThucTe);
			db.AddInParameter(dbCommand, "@NgayDiDiaDiem_TC", SqlDbType.DateTime, NgayDiDiaDiem_TC.Year <= 1753 ? DBNull.Value : (object) NgayDiDiaDiem_TC);
			db.AddInParameter(dbCommand, "@NgayDiThucTe", SqlDbType.DateTime, NgayDiThucTe.Year <= 1753 ? DBNull.Value : (object) NgayDiThucTe);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_TKVC_TrungChuyen> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_TrungChuyen item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_TKVC_TrungChuyen(long id)
		{
			KDT_VNACC_TKVC_TrungChuyen entity = new KDT_VNACC_TKVC_TrungChuyen();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_TKVC_TrungChuyen> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_TrungChuyen item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}