﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.Interface.VNACCS;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.Components;
namespace Company.Interface
{
    public partial class VNACC_TIAForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_TIA TIA = new KDT_VNACCS_TIA();
        DataSet dsHang = new DataSet();
        DataSet dsDVT = new DataSet();
        DataSet dsmaHS = new DataSet();
        DataTable dt = new DataTable();
        public VNACC_TIAForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.TIA.ToString());
        }

        private void VNACC_TIAForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                dsmaHS = VNACC_Category_HSCode.SelectAll();
                dsDVT = VNACC_Category_QuantityUnit.SelectAll();
                grListHang.DropDowns[0].DataSource = dsDVT.Tables[0];
                grListHang.DropDowns[1].DataSource = dsDVT.Tables[0];
                grListHang.DropDowns["drdMaSoHangHoa"].DataSource = dsmaHS.Tables[0];
                
                //dsHang = KDT_VNACCS_TIA_HangHoa.SelectDynamic("Master_ID=" + TIA.ID, "");
                //grListHang.DataSource = dsHang.Tables[0];
                SetTIA();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }


        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoToKhai, errorProvider, "Số tờ khai", isOnlyWarning);
                isValid &= ValidateControl.ValidateDate(clcThoiHanTaiXuatNhap, errorProvider, "Thời hạn Tái xuất nhập", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaNguoiKhaiDauTien, errorProvider, "Mã người khai đầu tiên", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNguoiKhaiDauTien, errorProvider, "Tên người khai đầu tiên", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaNguoiXuatNhapKhau, errorProvider, "Mã người xuất nhập khẩu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNguoiXuatNhapKhau, errorProvider, "Tên người xuất nhập khẩu", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void GetTIA()
        {
            try
            {
                TIA.SoToKhai = System.Convert.ToDecimal(txtSoToKhai.Text);
                TIA.CoQuanHaiQuan = GlobalSettings.MA_HAI_QUAN;
                TIA.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO.ToString();
                TIA.MaNguoiKhaiDauTien = txtMaNguoiKhaiDauTien.Text;
                TIA.TenNguoiKhaiDauTien = txtTenNguoiKhaiDauTien.Text;
                TIA.MaNguoiXuatNhapKhau = txtMaNguoiXuatNhapKhau.Text;
                TIA.TenNguoiXuatNhapKhau = txtTenNguoiXuatNhapKhau.Text;
                TIA.ThoiHanTaiXuatNhap = clcThoiHanTaiXuatNhap.Value;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void SetTIA()
        {
            try
            {
                if (TIA.ID >0)
                {
                    txtSoToKhai.Text = TIA.SoToKhai.ToString();
                    txtMaNguoiKhaiDauTien.Text = TIA.MaNguoiKhaiDauTien;
                    txtTenNguoiKhaiDauTien.Text = TIA.TenNguoiKhaiDauTien;
                    txtMaNguoiXuatNhapKhau.Text = TIA.MaNguoiXuatNhapKhau;
                    txtTenNguoiXuatNhapKhau.Text = TIA.TenNguoiXuatNhapKhau;
                    clcThoiHanTaiXuatNhap.Value = TIA.ThoiHanTaiXuatNhap;
                }
                else
                {
                    txtMaNguoiKhaiDauTien.Text = Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma;
                    txtTenNguoiKhaiDauTien.Text = GlobalSettings.TEN_DON_VI;
                    txtMaNguoiXuatNhapKhau.Text = GlobalSettings.MA_DON_VI;
                    txtTenNguoiXuatNhapKhau.Text = GlobalSettings.TEN_DON_VI;
                    clcThoiHanTaiXuatNhap.Value = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void Save()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (!ValidateForm(false))
                    return;
                GetTIA();
                //foreach (DataRow row in dsHang.Tables[0].Rows)
                //{
                //    KDT_VNACCS_TIA_HangHoa hang = new KDT_VNACCS_TIA_HangHoa();
                //    hang.Master_ID = TIA.ID;
                //    if (row["ID"].ToString().Length != 0)
                //    {
                //        hang.ID = Convert.ToInt32(row["ID"].ToString());
                //    }
                //    if (String.IsNullOrEmpty(row["MaSoHangHoa"].ToString()))
                //    {
                //        ShowMessage("Mã số hàng hóa chưa được nhập",false);
                //        return;
                //    }
                //    hang.MaSoHangHoa = row["MaSoHangHoa"].ToString();
                //    if (String.IsNullOrEmpty(row["SoLuongBanDau"].ToString()))
                //    {
                //        ShowMessage("Số lượng ban đầu chưa được nhập", false);
                //        return;
                //    }
                //    hang.SoLuongBanDau = Convert.ToDecimal(row["SoLuongBanDau"].ToString());
                //    if (String.IsNullOrEmpty(row["DVTSoLuongBanDau"].ToString()))
                //    {
                //        ShowMessage("ĐVT Số lượng ban đầu chưa được nhập", false);
                //        return;
                //    }
                //    hang.DVTSoLuongBanDau = row["DVTSoLuongBanDau"].ToString();
                //    if (String.IsNullOrEmpty(row["SoLuongDaTaiNhapTaiXuat"].ToString()))
                //    {
                //        ShowMessage("Số lượng đã tái xuất nhập chưa được nhập", false);
                //        return;
                //    }
                //    hang.SoLuongDaTaiNhapTaiXuat = Convert.ToDecimal(row["SoLuongDaTaiNhapTaiXuat"].ToString());
                //    if (String.IsNullOrEmpty(row["DVTSoLuongDaTaiNhapTaiXuat"].ToString()))
                //    {
                //        ShowMessage("ĐVT Số lượng tái xuất nhập chưa được nhập", false);
                //        return;
                //    }
                //    hang.DVTSoLuongDaTaiNhapTaiXuat = row["DVTSoLuongDaTaiNhapTaiXuat"].ToString();
                //    TIA.HangCollection.Add(hang);                  
                //}
                if (TIA.HangCollection.Count > 0)
                {
                    TIA.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;
                    TIA.InsertUpdateFul();
                    ShowMessage("Lưu thành công", false);
                }
                else
                    ShowMessage("Chưa nhập thông tin hàng", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    Add();
                    break;
                case "cmdLuu":
                    Save();
                    break;
                case "cmdKhaiBao":
                    sendvnaccs(false);
                    break;
                case "cmdKetQuaHQ":
                    ShowKetQuaTraVe();
                    break;
                case "cmdKetQuaXuLy":
                    ShowKetQuaXuLy();
                    break;
            }
        }
        private void Add()
        {
            VNACC_TIA_HangForm f = new VNACC_TIA_HangForm();
            f.TIA = TIA;
            f.ShowDialog(this);
            BindData();
        }
        private void BindData()
        {
            try
            {
                grListHang.Refetch();
                grListHang.DataSource = TIA.HangCollection;
                grListHang.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ShowKetQuaXuLy()
        {
            try
            {
                VNACC_KetQuaXuLy f = new VNACC_KetQuaXuLy(TIA.ID, "TIA");
                //f.master_id = TKMD.ID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void ShowKetQuaTraVe()
        {
            try
            {
                SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
                f.isSend = false;
                f.isRep = true;
                f.inputMSGID = TIA.InputMessageID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void grListHang_DeletingRecord(object sender, Janus.Windows.GridEX.RowActionCancelEventArgs e)
        {

        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                txtSoToKhai.Tag = "ECN"; //Số tờ khai
                txtMaNguoiKhaiDauTien.Tag = "CFD"; //Mã người khai đầu tiên
                txtTenNguoiKhaiDauTien.Tag = "NFD"; //Tên người khai đầu tiên
                txtMaNguoiXuatNhapKhau.Tag = "IEC"; //Mã người xuất nhập khẩu
                txtTenNguoiXuatNhapKhau.Tag = "IEN"; //Tên người xuất nhập khẩu
                clcThoiHanTaiXuatNhap.TagName = "TED"; //Thời hạn tái xuất nhập 


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        #region Cập nhật thông tin
        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            CapNhatThongTin(msgResult, string.Empty);
        }
        private void CapNhatThongTin(ReturnMessages msgResult, string MaNV)
        {
            try
            {
                //ProcessMessages.GetDataResult_TIA(msgResult, MaNV, TIA);
                //TIA.InsertUpdateFul();
                SetTIA();
                //setCommandStatus();
                //vad8010 = new VAD8010();
                //vad8010.LoadVAD8010(msgResult.Body.ToString());

                TIA.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                //TIA.SoDanhMucMienThue = System.Convert.ToDecimal(vad8010.A01.GetValue().ToString());

                TIA.InsertUpdateFul();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void TuDongCapNhatThongTin()
        {
            try
            {
                if (TIA != null && TIA.ID > 0)
                {
                    List<MsgLog> listLog = new List<MsgLog>();
                    IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("MessagesInputID = '{0}'", TIA.InputMessageID), null);

                    foreach (MsgPhanBo msgPb in listPB)
                    {
                        MsgLog log = MsgLog.Load(msgPb.Master_ID);
                        if (log == null)
                        {
                            msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                            msgPb.GhiChu = "Không tìm thấy log";
                            msgPb.InsertUpdate();
                        }
                        try
                        {
                            ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                            CapNhatThongTin(msgReturn);
                            //msgPb.Delete();
                            msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem;
                            msgPb.Update();//Đã cập nhật thông tin
                        }
                        catch (System.Exception ex)
                        {
                            msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                            msgPb.GhiChu = ex.Message;
                            msgPb.InsertUpdate();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        #endregion
        public void sendvnaccs(bool KhaiBaoSua)
        {
            try
            {
                if (TIA.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    TIA.LoadHang();
                    TIA tia = VNACCMaperFromObject.TIAMapper(TIA);
                    if (tia == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        TIA.InputMessageID = HelperVNACCS.NewInputMSGID();
                        TIA.InsertUpdateFul();
                        msg = MessagesSend.Load<TIA>(tia, TIA.InputMessageID);

                    }
                    else
                    {
                        msg = MessagesSend.Load<TIA>(tia, true, EnumNghiepVu.TIA, TIA.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TIA.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = true;
                    f.inputMSGID = TIA.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result)
                    {
                        TuDongCapNhatThongTin();
                        SetTIA();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                    

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        private void grListHang_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            VNACC_TIA_HangForm f = new VNACC_TIA_HangForm();
            f.TIA = TIA;
            f.ShowDialog(this);
            BindData();
        }

        private void txtSoToKhai_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
                    f.IsShowChonToKhai = true;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.OK)
                    {
                        foreach (KDT_VNACCS_TIA_HangHoa item in TIA.HangCollection)
                        {
                            if (item.ID > 0)
                                item.Delete();
                        }
                        TIA.HangCollection.Clear();
                        KDT_VNACC_ToKhaiMauDich TKMD = f.TKMDDuocChon;
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                        clcThoiHanTaiXuatNhap.Value = TKMD.NgayCapPhep;
                        TKMD.LoadFull();
                        foreach (KDT_VNACC_HangMauDich item in TKMD.HangCollection)
                        {
                            KDT_VNACCS_TIA_HangHoa Hang = new KDT_VNACCS_TIA_HangHoa();
                            Hang.MaSoHangHoa = item.MaSoHang;
                            Hang.SoLuongBanDau = item.SoLuong1;
                            Hang.DVTSoLuongBanDau = item.DVTLuong1;
                            Hang.SoLuongDaTaiNhapTaiXuat = item.SoLuong1;
                            Hang.DVTSoLuongDaTaiNhapTaiXuat = item.DVTLuong1;
                            TIA.HangCollection.Add(Hang);
                        }
                        BindData();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
      

    }
}
