﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAE3LE0 : BasicVNACC
    {
        public static int TongSoByte { get; set; }
        public PropertiesAttribute ECN { get; set; }
        public PropertiesAttribute JKN { get; set; }
        public PropertiesAttribute FIC { get; set; }
        public PropertiesAttribute BNO { get; set; }
        public PropertiesAttribute DNO { get; set; }
        public PropertiesAttribute TDN { get; set; }
        public PropertiesAttribute K07 { get; set; }
        public PropertiesAttribute ECB { get; set; }
        public PropertiesAttribute CCC { get; set; }
        public PropertiesAttribute MTC { get; set; }
        public PropertiesAttribute K01 { get; set; }
        public PropertiesAttribute K08 { get; set; }
        public PropertiesAttribute CHB { get; set; }
        public PropertiesAttribute K10 { get; set; }
        public PropertiesAttribute AD1 { get; set; }
        public PropertiesAttribute AD2 { get; set; }
        public PropertiesAttribute AD3 { get; set; }
        public PropertiesAttribute RID { get; set; }
        public PropertiesAttribute AAA { get; set; }
        public PropertiesAttribute EPC { get; set; }
        public PropertiesAttribute EPN { get; set; }
        public PropertiesAttribute EPP { get; set; }
        public PropertiesAttribute EPA { get; set; }
        public PropertiesAttribute EPT { get; set; }
        public PropertiesAttribute EXC { get; set; }
        public PropertiesAttribute EXN { get; set; }
        public PropertiesAttribute CGC { get; set; }
        public PropertiesAttribute CGN { get; set; }
        public PropertiesAttribute CGP { get; set; }
        public PropertiesAttribute CGA { get; set; }
        public PropertiesAttribute CAT { get; set; }
        public PropertiesAttribute CAC { get; set; }
        public PropertiesAttribute CAS { get; set; }
        public PropertiesAttribute CGK { get; set; }
        public PropertiesAttribute K32 { get; set; }
        public PropertiesAttribute K33 { get; set; }
        public PropertiesAttribute K34 { get; set; }
        public PropertiesAttribute EKN { get; set; }
        public PropertiesAttribute NO { get; set; }
        public PropertiesAttribute NOT { get; set; }
        public PropertiesAttribute GW { get; set; }
        public PropertiesAttribute GWT { get; set; }
        public PropertiesAttribute ST { get; set; }
        public PropertiesAttribute K42 { get; set; }
        public PropertiesAttribute DSC { get; set; }
        public PropertiesAttribute DSN { get; set; }
        public PropertiesAttribute PSC { get; set; }
        public PropertiesAttribute PSN { get; set; }
        public PropertiesAttribute VSC { get; set; }
        public PropertiesAttribute VSN { get; set; }
        public PropertiesAttribute SYM { get; set; }
        public PropertiesAttribute MRK { get; set; }
        public GroupAttribute SS_ { get; set; }
        public PropertiesAttribute IV1 { get; set; }
        public PropertiesAttribute IV3 { get; set; }
        public PropertiesAttribute IV2 { get; set; }
        public PropertiesAttribute IVD { get; set; }
        public PropertiesAttribute IVP { get; set; }
        public PropertiesAttribute IP2 { get; set; }
        public PropertiesAttribute IP3 { get; set; }
        public PropertiesAttribute IP4 { get; set; }
        public PropertiesAttribute IP1 { get; set; }
        public PropertiesAttribute FCD { get; set; }
        public PropertiesAttribute FKK { get; set; }
        public GroupAttribute RC_ { get; set; }
        public PropertiesAttribute TP { get; set; }
        public PropertiesAttribute K68 { get; set; }
        public PropertiesAttribute CNV { get; set; }
        public PropertiesAttribute TPM { get; set; }
        public PropertiesAttribute ENC { get; set; }
        public PropertiesAttribute PAY { get; set; }
        public PropertiesAttribute ETA { get; set; }
        public PropertiesAttribute AD7 { get; set; }
        public PropertiesAttribute TCO { get; set; }
        public PropertiesAttribute SAM { get; set; }
        public PropertiesAttribute AD8 { get; set; }
        public PropertiesAttribute K69 { get; set; }
        public PropertiesAttribute K70 { get; set; }
        public GroupAttribute EA_ { get; set; }
        public PropertiesAttribute NT2 { get; set; }
        public PropertiesAttribute REF { get; set; }
        public PropertiesAttribute K82 { get; set; }
        public PropertiesAttribute DPD { get; set; }
        public GroupAttribute ST_ { get; set; }
        public PropertiesAttribute ARP { get; set; }
        public PropertiesAttribute ADT { get; set; }
        public GroupAttribute VC_ { get; set; }
        public PropertiesAttribute VN { get; set; }
        public PropertiesAttribute K72 { get; set; }
        public GroupAttribute C__ { get; set; }
        public PropertiesAttribute CCM { get; set; }
        public GroupAttribute D__ { get; set; }

        public VAE3LE0()
        {
            ECN = new PropertiesAttribute(12, typeof(int));
            JKN = new PropertiesAttribute(1, typeof(string));
            FIC = new PropertiesAttribute(12, typeof(string));
            BNO = new PropertiesAttribute(2, typeof(int));
            DNO = new PropertiesAttribute(2, typeof(int));
            TDN = new PropertiesAttribute(12, typeof(int));
            K07 = new PropertiesAttribute(3, typeof(string));
            ECB = new PropertiesAttribute(3, typeof(string));
            CCC = new PropertiesAttribute(1, typeof(string));
            MTC = new PropertiesAttribute(1, typeof(string));
            K01 = new PropertiesAttribute(4, typeof(string));
            K08 = new PropertiesAttribute(10, typeof(string));
            CHB = new PropertiesAttribute(2, typeof(string));
            K10 = new PropertiesAttribute(8, typeof(DateTime));
            AD1 = new PropertiesAttribute(6, typeof(int));
            AD2 = new PropertiesAttribute(8, typeof(DateTime));
            AD3 = new PropertiesAttribute(6, typeof(int));
            RID = new PropertiesAttribute(8, typeof(DateTime));
            AAA = new PropertiesAttribute(1, typeof(string));
            EPC = new PropertiesAttribute(13, typeof(string));
            EPN = new PropertiesAttribute(300, typeof(string));
            EPP = new PropertiesAttribute(7, typeof(string));
            EPA = new PropertiesAttribute(300, typeof(string));
            EPT = new PropertiesAttribute(20, typeof(string));
            EXC = new PropertiesAttribute(13, typeof(string));
            EXN = new PropertiesAttribute(300, typeof(string));
            CGC = new PropertiesAttribute(13, typeof(string));
            CGN = new PropertiesAttribute(70, typeof(string));
            CGP = new PropertiesAttribute(9, typeof(string));
            CGA = new PropertiesAttribute(35, typeof(string));
            CAT = new PropertiesAttribute(35, typeof(string));
            CAC = new PropertiesAttribute(35, typeof(string));
            CAS = new PropertiesAttribute(35, typeof(string));
            CGK = new PropertiesAttribute(2, typeof(string));
            K32 = new PropertiesAttribute(5, typeof(string));
            K33 = new PropertiesAttribute(50, typeof(string));
            K34 = new PropertiesAttribute(5, typeof(string));
            EKN = new PropertiesAttribute(35, typeof(string));
            NO = new PropertiesAttribute(8, typeof(int));
            NOT = new PropertiesAttribute(3, typeof(string));
            GW = new PropertiesAttribute(10, typeof(int));
            GWT = new PropertiesAttribute(3, typeof(string));
            ST = new PropertiesAttribute(7, typeof(string));
            K42 = new PropertiesAttribute(20, typeof(string));
            DSC = new PropertiesAttribute(5, typeof(string));
            DSN = new PropertiesAttribute(35, typeof(string));
            PSC = new PropertiesAttribute(6, typeof(string));
            PSN = new PropertiesAttribute(35, typeof(string));
            VSC = new PropertiesAttribute(9, typeof(string));
            VSN = new PropertiesAttribute(35, typeof(string));
            SYM = new PropertiesAttribute(8, typeof(DateTime));
            MRK = new PropertiesAttribute(140, typeof(string));
            IV1 = new PropertiesAttribute(1, typeof(string));
            IV3 = new PropertiesAttribute(35, typeof(string));
            IV2 = new PropertiesAttribute(12, typeof(int));
            IVD = new PropertiesAttribute(8, typeof(DateTime));
            IVP = new PropertiesAttribute(7, typeof(string));
            IP2 = new PropertiesAttribute(3, typeof(string));
            IP3 = new PropertiesAttribute(3, typeof(string));
            IP4 = new PropertiesAttribute(20, typeof(int));
            IP1 = new PropertiesAttribute(1, typeof(string));
            FCD = new PropertiesAttribute(3, typeof(string));
            FKK = new PropertiesAttribute(20, typeof(int));
            TP = new PropertiesAttribute(20, typeof(int));
            K68 = new PropertiesAttribute(1, typeof(string));
            CNV = new PropertiesAttribute(1, typeof(string));
            TPM = new PropertiesAttribute(1, typeof(string));
            ENC = new PropertiesAttribute(1, typeof(string));
            PAY = new PropertiesAttribute(1, typeof(string));
            ETA = new PropertiesAttribute(11, typeof(int));
            AD7 = new PropertiesAttribute(3, typeof(string));
            TCO = new PropertiesAttribute(11, typeof(int));
            SAM = new PropertiesAttribute(11, typeof(int));
            AD8 = new PropertiesAttribute(3, typeof(string));
            K69 = new PropertiesAttribute(2, typeof(int));
            K70 = new PropertiesAttribute(2, typeof(int));
            NT2 = new PropertiesAttribute(300, typeof(string));
            REF = new PropertiesAttribute(20, typeof(string));
            K82 = new PropertiesAttribute(5, typeof(string));
            DPD = new PropertiesAttribute(8, typeof(DateTime));
            ARP = new PropertiesAttribute(7, typeof(string));
            ADT = new PropertiesAttribute(8, typeof(DateTime));
            VN = new PropertiesAttribute(70, typeof(string));
            K72 = new PropertiesAttribute(300, typeof(string));
            CCM = new PropertiesAttribute(1, typeof(string));
            #region SS_
            List<PropertiesAttribute> listSS_ = new List<PropertiesAttribute>();
            listSS_.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAE3LE0_SS_, typeof(string)));
            listSS_.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAE3LE0_SN_, typeof(string)));
            SS_ = new GroupAttribute("SS_", 5, listSS_);
            #endregion SS_
            #region RC_
            List<PropertiesAttribute> listRC_ = new List<PropertiesAttribute>();
            listRC_.Add(new PropertiesAttribute(3, 2, EnumGroupID.VAE3LE0_RC_, typeof(string)));
            listRC_.Add(new PropertiesAttribute(9, 2, EnumGroupID.VAE3LE0_RD_, typeof(int)));
            RC_ = new GroupAttribute("RC_", 2, listRC_);
            #endregion RC_
            #region EA_
            List<PropertiesAttribute> listEA_ = new List<PropertiesAttribute>();
            listEA_.Add(new PropertiesAttribute(3, 3, EnumGroupID.VAE3LE0_EA_, typeof(string)));
            listEA_.Add(new PropertiesAttribute(12, 3, EnumGroupID.VAE3LE0_EB_, typeof(int)));
            EA_ = new GroupAttribute("EA_", 3, listEA_);
            #endregion EA_
            #region ST_
            List<PropertiesAttribute> listST_ = new List<PropertiesAttribute>();
            listST_.Add(new PropertiesAttribute(7, 3, EnumGroupID.VAE3LE0_ST_, typeof(string)));
            listST_.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAE3LE0_AD_, typeof(DateTime)));
            listST_.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAE3LE0_SD_, typeof(DateTime)));
            ST_ = new GroupAttribute("ST_", 3, listST_);
            #endregion ST_
            #region VC_
            List<PropertiesAttribute> listVC_ = new List<PropertiesAttribute>();
            listVC_.Add(new PropertiesAttribute(7, 5, EnumGroupID.VAE3LE0_VC_, typeof(string)));
            VC_ = new GroupAttribute("VC_", 5, listVC_);
            #endregion VC_
            #region C__
            List<PropertiesAttribute> listC__ = new List<PropertiesAttribute>();
            listC__.Add(new PropertiesAttribute(12, 50, EnumGroupID.VAE3LE0_C__, typeof(string)));
            C__ = new GroupAttribute("C__", 50, listC__);
            #endregion C__
            #region D__
            List<PropertiesAttribute> listD__ = new List<PropertiesAttribute>();
            listD__.Add(new PropertiesAttribute(8, 10, EnumGroupID.VAE3LE0_D__, typeof(DateTime)));
            listD__.Add(new PropertiesAttribute(120, 10, EnumGroupID.VAE3LE0_T__, typeof(string)));
            listD__.Add(new PropertiesAttribute(420, 10, EnumGroupID.VAE3LE0_I__, typeof(string)));
            D__ = new GroupAttribute("D__", 10, listD__);
            #endregion D__
            TongSoByte = 9379;
        }

    }
    public partial class EnumGroupID
    {
        public static readonly string VAE3LE0_SS_ = "VAE3LE0_SS_";
        public static readonly string VAE3LE0_SN_ = "VAE3LE0_SN_";
        public static readonly string VAE3LE0_RC_ = "VAE3LE0_RC_";
        public static readonly string VAE3LE0_RD_ = "VAE3LE0_RD_";
        public static readonly string VAE3LE0_EA_ = "VAE3LE0_EA_";
        public static readonly string VAE3LE0_EB_ = "VAE3LE0_EB_";
        public static readonly string VAE3LE0_ST_ = "VAE3LE0_ST_";
        public static readonly string VAE3LE0_AD_ = "VAE3LE0_AD_";
        public static readonly string VAE3LE0_SD_ = "VAE3LE0_SD_";
        public static readonly string VAE3LE0_VC_ = "VAE3LE0_VC_";
        public static readonly string VAE3LE0_C__ = "VAE3LE0_C__";
        public static readonly string VAE3LE0_D__ = "VAE3LE0_D__";
        public static readonly string VAE3LE0_T__ = "VAE3LE0_T__";
        public static readonly string VAE3LE0_I__ = "VAE3LE0_I__";
    }

}
