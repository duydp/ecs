/*
Run this script on:

        ecsteam.ECS_TQDT_GC_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_GC

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 03/20/2012 1:34:06 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Insert]
-- Database: ECS_TQDT_GC
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 20, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Insert]
	@IDHopDong bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@MaHaiQuanTiepNhan char(6),
	@SoToKhai bigint,
	@CanBoDangKy nvarchar(50),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(20),
	@SoHopDongDV varchar(50),
	@NgayHDDV datetime,
	@NgayHetHanHDDV datetime,
	@NguoiChiDinhDV nvarchar(250),
	@MaKhachHang varchar(20),
	@TenKH nvarchar(250),
	@SoHDKH varchar(50),
	@NgayHDKH datetime,
	@NgayHetHanHDKH datetime,
	@NguoiChiDinhKH nvarchar(250),
	@MaDaiLy varchar(20),
	@MaLoaiHinh nchar(10),
	@DiaDiemXepHang nvarchar(250),
	@TyGiaVND money,
	@TyGiaUSD money,
	@LePhiHQ money,
	@SoBienLai nchar(10),
	@NgayBienLai datetime,
	@ChungTu nvarchar(250),
	@NguyenTe_ID char(3),
	@MaHaiQuanKH char(6),
	@ID_Relation bigint,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus int,
	@GuidReference nvarchar(500),
	@NamDK smallint,
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@Huongdan_PL nvarchar(500),
	@PTTT_ID varchar(10),
	@DKGH_ID varchar(7),
	@ThoiGianGiaoHang datetime,
	@SoKien decimal(10, 3),
	@TrongLuong decimal(10, 3),
	@TrongLuongTinh decimal(10, 3),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_GC_ToKhaiChuyenTiep]
(
	[IDHopDong],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[MaHaiQuanTiepNhan],
	[SoToKhai],
	[CanBoDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[SoHopDongDV],
	[NgayHDDV],
	[NgayHetHanHDDV],
	[NguoiChiDinhDV],
	[MaKhachHang],
	[TenKH],
	[SoHDKH],
	[NgayHDKH],
	[NgayHetHanHDKH],
	[NguoiChiDinhKH],
	[MaDaiLy],
	[MaLoaiHinh],
	[DiaDiemXepHang],
	[TyGiaVND],
	[TyGiaUSD],
	[LePhiHQ],
	[SoBienLai],
	[NgayBienLai],
	[ChungTu],
	[NguyenTe_ID],
	[MaHaiQuanKH],
	[ID_Relation],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[PTTT_ID],
	[DKGH_ID],
	[ThoiGianGiaoHang],
	[SoKien],
	[TrongLuong],
	[TrongLuongTinh]
)
VALUES 
(
	@IDHopDong,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@MaHaiQuanTiepNhan,
	@SoToKhai,
	@CanBoDangKy,
	@NgayDangKy,
	@MaDoanhNghiep,
	@SoHopDongDV,
	@NgayHDDV,
	@NgayHetHanHDDV,
	@NguoiChiDinhDV,
	@MaKhachHang,
	@TenKH,
	@SoHDKH,
	@NgayHDKH,
	@NgayHetHanHDKH,
	@NguoiChiDinhKH,
	@MaDaiLy,
	@MaLoaiHinh,
	@DiaDiemXepHang,
	@TyGiaVND,
	@TyGiaUSD,
	@LePhiHQ,
	@SoBienLai,
	@NgayBienLai,
	@ChungTu,
	@NguyenTe_ID,
	@MaHaiQuanKH,
	@ID_Relation,
	@GUIDSTR,
	@DeXuatKhac,
	@LyDoSua,
	@ActionStatus,
	@GuidReference,
	@NamDK,
	@HUONGDAN,
	@PhanLuong,
	@Huongdan_PL,
	@PTTT_ID,
	@DKGH_ID,
	@ThoiGianGiaoHang,
	@SoKien,
	@TrongLuong,
	@TrongLuongTinh
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Update]
-- Database: ECS_TQDT_GC
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 20, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Update]
	@ID bigint,
	@IDHopDong bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@MaHaiQuanTiepNhan char(6),
	@SoToKhai bigint,
	@CanBoDangKy nvarchar(50),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(20),
	@SoHopDongDV varchar(50),
	@NgayHDDV datetime,
	@NgayHetHanHDDV datetime,
	@NguoiChiDinhDV nvarchar(250),
	@MaKhachHang varchar(20),
	@TenKH nvarchar(250),
	@SoHDKH varchar(50),
	@NgayHDKH datetime,
	@NgayHetHanHDKH datetime,
	@NguoiChiDinhKH nvarchar(250),
	@MaDaiLy varchar(20),
	@MaLoaiHinh nchar(10),
	@DiaDiemXepHang nvarchar(250),
	@TyGiaVND money,
	@TyGiaUSD money,
	@LePhiHQ money,
	@SoBienLai nchar(10),
	@NgayBienLai datetime,
	@ChungTu nvarchar(250),
	@NguyenTe_ID char(3),
	@MaHaiQuanKH char(6),
	@ID_Relation bigint,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus int,
	@GuidReference nvarchar(500),
	@NamDK smallint,
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@Huongdan_PL nvarchar(500),
	@PTTT_ID varchar(10),
	@DKGH_ID varchar(7),
	@ThoiGianGiaoHang datetime,
	@SoKien decimal(10, 3),
	@TrongLuong decimal(10, 3),
	@TrongLuongTinh decimal(10, 3)
AS

UPDATE
	[dbo].[t_KDT_GC_ToKhaiChuyenTiep]
SET
	[IDHopDong] = @IDHopDong,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[MaHaiQuanTiepNhan] = @MaHaiQuanTiepNhan,
	[SoToKhai] = @SoToKhai,
	[CanBoDangKy] = @CanBoDangKy,
	[NgayDangKy] = @NgayDangKy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[SoHopDongDV] = @SoHopDongDV,
	[NgayHDDV] = @NgayHDDV,
	[NgayHetHanHDDV] = @NgayHetHanHDDV,
	[NguoiChiDinhDV] = @NguoiChiDinhDV,
	[MaKhachHang] = @MaKhachHang,
	[TenKH] = @TenKH,
	[SoHDKH] = @SoHDKH,
	[NgayHDKH] = @NgayHDKH,
	[NgayHetHanHDKH] = @NgayHetHanHDKH,
	[NguoiChiDinhKH] = @NguoiChiDinhKH,
	[MaDaiLy] = @MaDaiLy,
	[MaLoaiHinh] = @MaLoaiHinh,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[TyGiaVND] = @TyGiaVND,
	[TyGiaUSD] = @TyGiaUSD,
	[LePhiHQ] = @LePhiHQ,
	[SoBienLai] = @SoBienLai,
	[NgayBienLai] = @NgayBienLai,
	[ChungTu] = @ChungTu,
	[NguyenTe_ID] = @NguyenTe_ID,
	[MaHaiQuanKH] = @MaHaiQuanKH,
	[ID_Relation] = @ID_Relation,
	[GUIDSTR] = @GUIDSTR,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ActionStatus] = @ActionStatus,
	[GuidReference] = @GuidReference,
	[NamDK] = @NamDK,
	[HUONGDAN] = @HUONGDAN,
	[PhanLuong] = @PhanLuong,
	[Huongdan_PL] = @Huongdan_PL,
	[PTTT_ID] = @PTTT_ID,
	[DKGH_ID] = @DKGH_ID,
	[ThoiGianGiaoHang] = @ThoiGianGiaoHang,
	[SoKien] = @SoKien,
	[TrongLuong] = @TrongLuong,
	[TrongLuongTinh] = @TrongLuongTinh
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_InsertUpdate]
-- Database: ECS_TQDT_GC
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 20, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_InsertUpdate]
	@ID bigint,
	@IDHopDong bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@MaHaiQuanTiepNhan char(6),
	@SoToKhai bigint,
	@CanBoDangKy nvarchar(50),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(20),
	@SoHopDongDV varchar(50),
	@NgayHDDV datetime,
	@NgayHetHanHDDV datetime,
	@NguoiChiDinhDV nvarchar(250),
	@MaKhachHang varchar(20),
	@TenKH nvarchar(250),
	@SoHDKH varchar(50),
	@NgayHDKH datetime,
	@NgayHetHanHDKH datetime,
	@NguoiChiDinhKH nvarchar(250),
	@MaDaiLy varchar(20),
	@MaLoaiHinh nchar(10),
	@DiaDiemXepHang nvarchar(250),
	@TyGiaVND money,
	@TyGiaUSD money,
	@LePhiHQ money,
	@SoBienLai nchar(10),
	@NgayBienLai datetime,
	@ChungTu nvarchar(250),
	@NguyenTe_ID char(3),
	@MaHaiQuanKH char(6),
	@ID_Relation bigint,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus int,
	@GuidReference nvarchar(500),
	@NamDK smallint,
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@Huongdan_PL nvarchar(500),
	@PTTT_ID varchar(10),
	@DKGH_ID varchar(7),
	@ThoiGianGiaoHang datetime,
	@SoKien decimal(10, 3),
	@TrongLuong decimal(10, 3),
	@TrongLuongTinh decimal(10, 3)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_ToKhaiChuyenTiep] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_ToKhaiChuyenTiep] 
		SET
			[IDHopDong] = @IDHopDong,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[MaHaiQuanTiepNhan] = @MaHaiQuanTiepNhan,
			[SoToKhai] = @SoToKhai,
			[CanBoDangKy] = @CanBoDangKy,
			[NgayDangKy] = @NgayDangKy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[SoHopDongDV] = @SoHopDongDV,
			[NgayHDDV] = @NgayHDDV,
			[NgayHetHanHDDV] = @NgayHetHanHDDV,
			[NguoiChiDinhDV] = @NguoiChiDinhDV,
			[MaKhachHang] = @MaKhachHang,
			[TenKH] = @TenKH,
			[SoHDKH] = @SoHDKH,
			[NgayHDKH] = @NgayHDKH,
			[NgayHetHanHDKH] = @NgayHetHanHDKH,
			[NguoiChiDinhKH] = @NguoiChiDinhKH,
			[MaDaiLy] = @MaDaiLy,
			[MaLoaiHinh] = @MaLoaiHinh,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[TyGiaVND] = @TyGiaVND,
			[TyGiaUSD] = @TyGiaUSD,
			[LePhiHQ] = @LePhiHQ,
			[SoBienLai] = @SoBienLai,
			[NgayBienLai] = @NgayBienLai,
			[ChungTu] = @ChungTu,
			[NguyenTe_ID] = @NguyenTe_ID,
			[MaHaiQuanKH] = @MaHaiQuanKH,
			[ID_Relation] = @ID_Relation,
			[GUIDSTR] = @GUIDSTR,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ActionStatus] = @ActionStatus,
			[GuidReference] = @GuidReference,
			[NamDK] = @NamDK,
			[HUONGDAN] = @HUONGDAN,
			[PhanLuong] = @PhanLuong,
			[Huongdan_PL] = @Huongdan_PL,
			[PTTT_ID] = @PTTT_ID,
			[DKGH_ID] = @DKGH_ID,
			[ThoiGianGiaoHang] = @ThoiGianGiaoHang,
			[SoKien] = @SoKien,
			[TrongLuong] = @TrongLuong,
			[TrongLuongTinh] = @TrongLuongTinh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_ToKhaiChuyenTiep]
		(
			[IDHopDong],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[MaHaiQuanTiepNhan],
			[SoToKhai],
			[CanBoDangKy],
			[NgayDangKy],
			[MaDoanhNghiep],
			[SoHopDongDV],
			[NgayHDDV],
			[NgayHetHanHDDV],
			[NguoiChiDinhDV],
			[MaKhachHang],
			[TenKH],
			[SoHDKH],
			[NgayHDKH],
			[NgayHetHanHDKH],
			[NguoiChiDinhKH],
			[MaDaiLy],
			[MaLoaiHinh],
			[DiaDiemXepHang],
			[TyGiaVND],
			[TyGiaUSD],
			[LePhiHQ],
			[SoBienLai],
			[NgayBienLai],
			[ChungTu],
			[NguyenTe_ID],
			[MaHaiQuanKH],
			[ID_Relation],
			[GUIDSTR],
			[DeXuatKhac],
			[LyDoSua],
			[ActionStatus],
			[GuidReference],
			[NamDK],
			[HUONGDAN],
			[PhanLuong],
			[Huongdan_PL],
			[PTTT_ID],
			[DKGH_ID],
			[ThoiGianGiaoHang],
			[SoKien],
			[TrongLuong],
			[TrongLuongTinh]
		)
		VALUES 
		(
			@IDHopDong,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@MaHaiQuanTiepNhan,
			@SoToKhai,
			@CanBoDangKy,
			@NgayDangKy,
			@MaDoanhNghiep,
			@SoHopDongDV,
			@NgayHDDV,
			@NgayHetHanHDDV,
			@NguoiChiDinhDV,
			@MaKhachHang,
			@TenKH,
			@SoHDKH,
			@NgayHDKH,
			@NgayHetHanHDKH,
			@NguoiChiDinhKH,
			@MaDaiLy,
			@MaLoaiHinh,
			@DiaDiemXepHang,
			@TyGiaVND,
			@TyGiaUSD,
			@LePhiHQ,
			@SoBienLai,
			@NgayBienLai,
			@ChungTu,
			@NguyenTe_ID,
			@MaHaiQuanKH,
			@ID_Relation,
			@GUIDSTR,
			@DeXuatKhac,
			@LyDoSua,
			@ActionStatus,
			@GuidReference,
			@NamDK,
			@HUONGDAN,
			@PhanLuong,
			@Huongdan_PL,
			@PTTT_ID,
			@DKGH_ID,
			@ThoiGianGiaoHang,
			@SoKien,
			@TrongLuong,
			@TrongLuongTinh
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Load]
-- Database: ECS_TQDT_GC
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 20, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDHopDong],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[MaHaiQuanTiepNhan],
	[SoToKhai],
	[CanBoDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[SoHopDongDV],
	[NgayHDDV],
	[NgayHetHanHDDV],
	[NguoiChiDinhDV],
	[MaKhachHang],
	[TenKH],
	[SoHDKH],
	[NgayHDKH],
	[NgayHetHanHDKH],
	[NguoiChiDinhKH],
	[MaDaiLy],
	[MaLoaiHinh],
	[DiaDiemXepHang],
	[TyGiaVND],
	[TyGiaUSD],
	[LePhiHQ],
	[SoBienLai],
	[NgayBienLai],
	[ChungTu],
	[NguyenTe_ID],
	[MaHaiQuanKH],
	[ID_Relation],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[PTTT_ID],
	[DKGH_ID],
	[ThoiGianGiaoHang],
	[SoKien],
	[TrongLuong],
	[TrongLuongTinh]
FROM
	[dbo].[t_KDT_GC_ToKhaiChuyenTiep]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong]
-- Database: ECS_TQDT_GC
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 20, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong]
	@IDHopDong bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDHopDong],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[MaHaiQuanTiepNhan],
	[SoToKhai],
	[CanBoDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[SoHopDongDV],
	[NgayHDDV],
	[NgayHetHanHDDV],
	[NguoiChiDinhDV],
	[MaKhachHang],
	[TenKH],
	[SoHDKH],
	[NgayHDKH],
	[NgayHetHanHDKH],
	[NguoiChiDinhKH],
	[MaDaiLy],
	[MaLoaiHinh],
	[DiaDiemXepHang],
	[TyGiaVND],
	[TyGiaUSD],
	[LePhiHQ],
	[SoBienLai],
	[NgayBienLai],
	[ChungTu],
	[NguyenTe_ID],
	[MaHaiQuanKH],
	[ID_Relation],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[PTTT_ID],
	[DKGH_ID],
	[ThoiGianGiaoHang],
	[SoKien],
	[TrongLuong],
	[TrongLuongTinh]
FROM
	[dbo].[t_KDT_GC_ToKhaiChuyenTiep]
WHERE
	[IDHopDong] = @IDHopDong

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectAll]
-- Database: ECS_TQDT_GC
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 20, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDHopDong],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[MaHaiQuanTiepNhan],
	[SoToKhai],
	[CanBoDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[SoHopDongDV],
	[NgayHDDV],
	[NgayHetHanHDDV],
	[NguoiChiDinhDV],
	[MaKhachHang],
	[TenKH],
	[SoHDKH],
	[NgayHDKH],
	[NgayHetHanHDKH],
	[NguoiChiDinhKH],
	[MaDaiLy],
	[MaLoaiHinh],
	[DiaDiemXepHang],
	[TyGiaVND],
	[TyGiaUSD],
	[LePhiHQ],
	[SoBienLai],
	[NgayBienLai],
	[ChungTu],
	[NguyenTe_ID],
	[MaHaiQuanKH],
	[ID_Relation],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[PTTT_ID],
	[DKGH_ID],
	[ThoiGianGiaoHang],
	[SoKien],
	[TrongLuong],
	[TrongLuongTinh]
FROM
	[dbo].[t_KDT_GC_ToKhaiChuyenTiep]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic]
-- Database: ECS_TQDT_GC
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 20, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[IDHopDong],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[MaHaiQuanTiepNhan],
	[SoToKhai],
	[CanBoDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[SoHopDongDV],
	[NgayHDDV],
	[NgayHetHanHDDV],
	[NguoiChiDinhDV],
	[MaKhachHang],
	[TenKH],
	[SoHDKH],
	[NgayHDKH],
	[NgayHetHanHDKH],
	[NguoiChiDinhKH],
	[MaDaiLy],
	[MaLoaiHinh],
	[DiaDiemXepHang],
	[TyGiaVND],
	[TyGiaUSD],
	[LePhiHQ],
	[SoBienLai],
	[NgayBienLai],
	[ChungTu],
	[NguyenTe_ID],
	[MaHaiQuanKH],
	[ID_Relation],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[PTTT_ID],
	[DKGH_ID],
	[ThoiGianGiaoHang],
	[SoKien],
	[TrongLuong],
	[TrongLuongTinh]
FROM [dbo].[t_KDT_GC_ToKhaiChuyenTiep] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.9', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.9', getdate(), null)
	END
	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
