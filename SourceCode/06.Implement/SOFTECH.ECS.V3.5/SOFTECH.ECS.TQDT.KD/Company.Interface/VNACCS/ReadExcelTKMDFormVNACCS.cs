﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.KDT.SHARE.VNACCS;
using System.Data.OleDb;
using Janus.Windows.GridEX;
using Company.Interface.CauHinh;
using Company.KDT.SHARE.Components;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.Interface.CauHinh;
#endif
#if GC_V4
using Company.GC.BLL.GC;
using Company.Interface.VNACCS;
using Company.Interface.CauHinh;
#endif

namespace Company.Interface
{
    public partial class ReadExcelTKMDFormVNACCS : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public string LoaiHinh = "";
        public string TenHang;
        public string MaTienTen_HMD = "";
        private string ConnectionStringExcel = "Driver={Microsoft Excel Driver(*.xls)};DriverId=790;Dbq=";

        public int FormatSoLuong = 2;
        public int FormatSoLuong1 = 2;

        public int FormatDonGia = 6;
        public int FormatTriGia = 6;

        public ReadExcelTKMDFormVNACCS()
        {
            InitializeComponent();
        }

        private void editBox8_TextChanged(object sender, EventArgs e)
        {

        }
        //Hien thi Dialog box 
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog(this);
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetTK.DataSource = GetAllSheetName();
                cbbSheetTK.SelectedIndex = 0;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static int ExcelColumnNameToNumber(string columnName)
        {
            try
            {
                columnName = columnName.ToUpperInvariant();

                int sum = 0;

                for (int i = 0; i < columnName.Length; i++)
                {
                    sum *= 25;
                    sum += (columnName[i] - 'A' + 1);
                }
                if (columnName.Length >= 2)
                {
                    return sum;
                }
                else
                {
                    return sum - 1;
                }
            }
            catch (Exception ex)
            {
                return 'A';
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private int ConvertCharToInt(char ch)
        {
            try
            {
                    return ch - 'A';
            }
            catch (Exception ex)
            {
                return 'A';
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private int checkNPLExit(string maNPL)
        {
            for (int i = 0; i < this.TKMD.HangCollection.Count; i++)
            {
                if (this.TKMD.HangCollection[i].MaQuanLy.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
               // Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                //Dictionary<string, Worksheet> dict = new Dictionary<string, Worksheet>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                    //dict.Add(worksheet.Name, worksheet);
                }
                return Collection;
            }
            catch (Exception ex)
            {                
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                error.SetIconPadding(txtRow, 8);
                return;

            }

            Workbook wb = new Workbook();

            Worksheet ws = null;

            Worksheet wsTKMD = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
                wsTKMD = wb.Worksheets[cbbSheetTK.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            #region TỜ KHAI

            WorksheetRowCollection wsrcTKMD = wsTKMD.Rows;

            int SoToKhaiCol = ExcelColumnNameToNumber(txtSoTK.Text.Trim());

            int MaLoaiHinhCol = ExcelColumnNameToNumber(txtMaLH.Text.Trim());

            int CoQuanHaiQuanCol = ExcelColumnNameToNumber(txtMaHQ.Text.Trim());

            int NgayDangKyCol = ExcelColumnNameToNumber(txtNgayDK.Text.Trim());

            int MaDonViCol = ExcelColumnNameToNumber(txtMDonVi.Text.Trim());

            int MaUyThacCol = ExcelColumnNameToNumber(txtMaUyThac.Text.Trim());

            int TenDoiTacCol = ExcelColumnNameToNumber(txtTenDoiTac.Text.Trim());

            int MaPhuongThucVTCol = ExcelColumnNameToNumber(txtMaPhuongThucVT.Text.Trim());

            int TenPTVCCol = ExcelColumnNameToNumber(txtTenPTVC.Text.Trim());

            int NgayHangDenCol = ExcelColumnNameToNumber(txtNgayHangDen.Text.Trim());

            int MaDiaDiemXepHangCol = ExcelColumnNameToNumber(txtMaDiaDiemDoHang.Text.Trim());

            int MaDiaDiemDoHangCol = ExcelColumnNameToNumber(txtMaDiaDiemXepHang.Text.Trim());

            int HopDong_SoCol = ExcelColumnNameToNumber(txtHopDong_So.Text.Trim());

            int MaNuocDoiTacCol = ExcelColumnNameToNumber(txtMaNuocDoiTac.Text.Trim());

            int MaDieuKienGiaHDCol = ExcelColumnNameToNumber(txtMaDieuKienGiaHD.Text.Trim());

            int PhuongThucTTCol = ExcelColumnNameToNumber(txtPhuongThucTT.Text.Trim());

            int MaTTHoaDonCol = ExcelColumnNameToNumber(txtMaTTHoaDon.Text.Trim());

            int TongSoTienLePhiCol = ExcelColumnNameToNumber(txtTongSoTienLePhi.Text.Trim());

            int NgayKhaiBaoNopThueCol = ExcelColumnNameToNumber(txtNgayKhaiBaoNopThue.Text.Trim());

            int NguoiNopThueCol = ExcelColumnNameToNumber(txtNguoiNopThue.Text.Trim());

            int PhiBaoHiemCol = ExcelColumnNameToNumber(txtPhiBaoHiem.Text.Trim());

            int PhiVanChuyenCol = ExcelColumnNameToNumber(txtPhiVanChuyen.Text.Trim());

            int NgayCapPhepCol = ExcelColumnNameToNumber(txtNgayCapPhep.Text.Trim());

            int TongTriGiaHDCol = ExcelColumnNameToNumber(txtTongTriGiaHD.Text.Trim());

            int TriGiaTinhThueCol = ExcelColumnNameToNumber(txtTriGiaTinhThue.Text.Trim());

            int TrongLuongCol = ExcelColumnNameToNumber(txtTrongLuong.Text.Trim());

            int SoLuongCol = ExcelColumnNameToNumber(txtSoLuong.Text.Trim());

            int SoHoaDonCol = ExcelColumnNameToNumber(txtSoHoaDon.Text.Trim());

            int NgayPhatHanhHDCol = ExcelColumnNameToNumber(txtNgayPhatHanhHD.Text.Trim());

            int TenDiaDiemDohangCol = ExcelColumnNameToNumber(txtTenDiaDiemXepHang.Text.Trim());

            int TenDonViCol = ExcelColumnNameToNumber(txtTenDonVi.Text.Trim());

            int TenDiaDiemXepHangCol = ExcelColumnNameToNumber(txtTenDiaDiemDohang.Text.Trim());

            int NgayThayDoiDangKyCol = ExcelColumnNameToNumber(txtNgayThayDoiDangKy.Text.Trim());

            int NgayHoanThanhKiemTraBPCol = ExcelColumnNameToNumber(txtNgayHoanThanhKiemTraBP.Text.Trim());

            int MaDDLuuKhoCol = ExcelColumnNameToNumber(txtMaDDLuuKho.Text.Trim());

            int DiaDiemDichVCCol = ExcelColumnNameToNumber(txtDiaDiemDichVC.Text.Trim());

            List<KDT_VNACC_ToKhaiMauDich> TKMDCollection = new List<KDT_VNACC_ToKhaiMauDich>();
            List<Company.KDT.SHARE.Components.DuLieuChuan.DoiTac> DoiTacCollection = new List<Company.KDT.SHARE.Components.DuLieuChuan.DoiTac>();
            DoiTacCollection = Company.KDT.SHARE.Components.DuLieuChuan.DoiTac.SelectCollectionAll();
            foreach (WorksheetRow wsr in wsrcTKMD)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich(); ;
                        try
                        {
                            TKMD.SoToKhai = Convert.ToDecimal(wsr.Cells[SoToKhaiCol].Value.ToString().Trim());
                            if (TKMD.SoToKhai.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaLoaiHinh = Convert.ToString(wsr.Cells[MaLoaiHinhCol].Value.ToString().Trim());
                            if (TKMD.MaLoaiHinh.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.CoQuanHaiQuan = Convert.ToString(wsr.Cells[CoQuanHaiQuanCol].Value.ToString().Trim());
                            if (TKMD.CoQuanHaiQuan.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                               //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.NgayDangKy = Convert.ToDateTime(wsr.Cells[NgayDangKyCol].Value.ToString().Trim());
                            if (TKMD.NgayDangKy.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (TKMD.NgayDangKy.ToString() == "NULL")
                                {
                                    TKMD.NgayDangKy = new DateTime(1900, 01, 01);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaDonVi = Convert.ToString(wsr.Cells[MaDonViCol].Value.ToString().Trim());
                            TKMD.TenDonVi = GlobalSettings.TEN_DON_VI;
                            TKMD.DiaChiDonVi = GlobalSettings.DIA_CHI;
                            TKMD.SoDienThoaiDonVi = GlobalSettings.SoDienThoaiDN;
                            if (TKMD.MaDonVi.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaUyThac = Convert.ToString(wsr.Cells[MaUyThacCol].Value.ToString().Trim());
                            if (TKMD.MaUyThac.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.TenDoiTac = Convert.ToString(wsr.Cells[TenDoiTacCol].Value.ToString().Trim());
                            if (TKMD.TenDoiTac.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                foreach (Company.KDT.SHARE.Components.DuLieuChuan.DoiTac item in DoiTacCollection)
                                {
                                    if (item.TenCongTy.Contains(TKMD.TenDoiTac))
                                    {
                                        TKMD.MaDoiTac = item.MaCongTy;
                                        TKMD.DiaChiDoiTac1 = item.DiaChi;
                                        TKMD.DiaChiDoiTac2 = item.DiaChi2;
                                        TKMD.DiaChiDoiTac3 = item.QuocGia;
                                        TKMD.DiaChiDoiTac4 = item.QuocGia;
                                        TKMD.MaBuuChinhDoiTac = item.BuuChinh;
                                        TKMD.MaNuocDoiTac = item.MaNuoc;
                                        break;
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaPhuongThucVT = Convert.ToString(wsr.Cells[MaPhuongThucVTCol].Value.ToString().Trim());
                            if (TKMD.MaPhuongThucVT.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.TenPTVC = Convert.ToString(wsr.Cells[TenPTVCCol].Value.ToString().Trim());
                            if (TKMD.TenPTVC.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.NgayHangDen = Convert.ToDateTime(wsr.Cells[NgayHangDenCol].Value.ToString().Trim());
                            if (TKMD.NgayHangDen.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (TKMD.NgayHangDen.ToString()=="NULL")
                                {
                                    TKMD.NgayHangDen = new DateTime(1900,01,01);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaDiaDiemXepHang = Convert.ToString(wsr.Cells[MaDiaDiemXepHangCol].Value.ToString().Trim());
                            if (TKMD.MaDiaDiemXepHang.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaDiaDiemDoHang = Convert.ToString(wsr.Cells[MaDiaDiemDoHangCol].Value.ToString().Trim());
                            if (TKMD.MaDiaDiemDoHang.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.HopDong_So = Convert.ToString(wsr.Cells[HopDong_SoCol].Value.ToString().Trim());
                            if (TKMD.HopDong_So.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaNuocDoiTac = Convert.ToString(wsr.Cells[MaNuocDoiTacCol].Value.ToString().Trim());
                            if (TKMD.MaNuocDoiTac.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaDieuKienGiaHD = Convert.ToString(wsr.Cells[MaDieuKienGiaHDCol].Value.ToString().Trim());
                            if (TKMD.MaDieuKienGiaHD.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.PhuongThucTT = Convert.ToString(wsr.Cells[PhuongThucTTCol].Value.ToString().Trim());
                            if (TKMD.PhuongThucTT.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaTTHoaDon = Convert.ToString(wsr.Cells[MaTTHoaDonCol].Value.ToString().Trim());
                            if (TKMD.MaTTHoaDon.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            decimal TyGiaTinhThue  = 0;
                            TyGiaTinhThue = Convert.ToDecimal(wsr.Cells[TongSoTienLePhiCol].Value.ToString().Trim());                            
                            if (TyGiaTinhThue.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (TKMD.TyGiaCollection.Count==0)
                                {
                                    KDT_VNACC_TK_PhanHoi_TyGia tyGia = new KDT_VNACC_TK_PhanHoi_TyGia();
                                    tyGia.MaTTTyGiaTinhThue = TKMD.MaTTHoaDon;
                                    tyGia.TyGiaTinhThue = TyGiaTinhThue;
                                    TKMD.TyGiaCollection.Add(tyGia);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.NgayKhaiBaoNopThue = Convert.ToDateTime(wsr.Cells[NgayKhaiBaoNopThueCol].Value.ToString().Trim());
                            if (TKMD.NgayKhaiBaoNopThue.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (TKMD.NgayKhaiBaoNopThue.ToString() == "NULL")
                                {
                                    TKMD.NgayKhaiBaoNopThue = new DateTime(1900, 01, 01);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.NguoiNopThue = Convert.ToString(wsr.Cells[NguoiNopThueCol].Value.ToString().Trim());
                            if (TKMD.NguoiNopThue.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.PhiBaoHiem = Convert.ToDecimal(wsr.Cells[PhiBaoHiemCol].Value.ToString().Trim());
                            if (TKMD.PhiBaoHiem.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.PhiVanChuyen = Convert.ToDecimal(wsr.Cells[PhiVanChuyenCol].Value.ToString().Trim());
                            if (TKMD.PhiVanChuyen.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.NgayCapPhep = Convert.ToDateTime(wsr.Cells[NgayCapPhepCol].Value.ToString().Trim());
                            if (TKMD.NgayCapPhep.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (TKMD.NgayCapPhep.ToString() == "NULL")
                                {
                                    TKMD.NgayCapPhep = new DateTime(1900, 01, 01);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.NguoiNopThue = Convert.ToString(wsr.Cells[NguoiNopThueCol].Value.ToString().Trim());
                            if (TKMD.NguoiNopThue.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.TongTriGiaHD = Convert.ToDecimal(wsr.Cells[TongTriGiaHDCol].Value.ToString().Trim());
                            if (TKMD.TongTriGiaHD.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.TriGiaTinhThue = Convert.ToDecimal(wsr.Cells[TriGiaTinhThueCol].Value.ToString().Trim());
                            if (TKMD.TriGiaTinhThue.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.TrongLuong = Convert.ToDecimal(wsr.Cells[TrongLuongCol].Value.ToString().Trim());
                            if (TKMD.TrongLuong.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.SoLuong = Convert.ToDecimal(wsr.Cells[SoLuongCol].Value.ToString().Trim());
                            if (TKMD.SoLuong.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.SoHoaDon = Convert.ToString(wsr.Cells[SoHoaDonCol].Value.ToString().Trim());
                            if (TKMD.SoHoaDon.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.NgayPhatHanhHD = Convert.ToDateTime(wsr.Cells[NgayPhatHanhHDCol].Value.ToString().Trim());
                            if (TKMD.NgayPhatHanhHD.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (TKMD.NgayPhatHanhHD.ToString() == "NULL")
                                {
                                    TKMD.NgayPhatHanhHD = new DateTime(1900, 01, 01);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.TenDiaDiemDohang = Convert.ToString(wsr.Cells[TenDiaDiemDohangCol].Value.ToString().Trim());
                            if (TKMD.TenDiaDiemDohang.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.TenDonVi = Convert.ToString(wsr.Cells[TenDonViCol].Value.ToString().Trim());
                            if (TKMD.TenDonVi.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.TenDiaDiemXepHang = Convert.ToString(wsr.Cells[TenDiaDiemXepHangCol].Value.ToString().Trim());
                            if (TKMD.TenDiaDiemXepHang.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.NgayThayDoiDangKy = Convert.ToDateTime(wsr.Cells[NgayThayDoiDangKyCol].Value.ToString().Trim());
                            if (TKMD.NgayThayDoiDangKy.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (TKMD.NgayThayDoiDangKy.ToString() == "NULL")
                                {
                                    TKMD.NgayThayDoiDangKy = new DateTime(1900, 01, 01);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.NgayHoanThanhKiemTraBP = Convert.ToDateTime(wsr.Cells[NgayHoanThanhKiemTraBPCol].Value.ToString().Trim());
                            if (TKMD.NgayHoanThanhKiemTraBP.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (TKMD.NgayHoanThanhKiemTraBP.ToString() == "NULL")
                                {
                                    TKMD.NgayHoanThanhKiemTraBP = new DateTime(1900, 01, 01);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.MaDDLuuKho = Convert.ToString(wsr.Cells[MaDDLuuKhoCol].Value.ToString().Trim());
                            if (TKMD.MaDDLuuKho.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            TKMD.DiaDiemDichVC = Convert.ToString(wsr.Cells[DiaDiemDichVCCol].Value.ToString().Trim());
                            if (TKMD.DiaDiemDichVC.ToString().Length == 0)
                            {
                                //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            //errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        if (isAdd)
                            TKMDCollection.Add(TKMD);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            #endregion

            #region HÀNG MẬU DỊCH

            #endregion

            WorksheetRowCollection wsrc = ws.Rows;
            int maHangCol = ExcelColumnNameToNumber(txtMaHangColumn.Text.Trim());

            int tenHangCol = ExcelColumnNameToNumber(txtTenHangColumn.Text.Trim());

            int maHSCol = ExcelColumnNameToNumber(txtMaHSColumn.Text.Trim());

            int dVTCol = ExcelColumnNameToNumber(txtDVTColumn.Text.Trim());

            int soLuongCol = ExcelColumnNameToNumber(txtSoLuongColumn.Text.Trim());

            int donGiaCol = ExcelColumnNameToNumber(txtDonGiaColumn.Text.Trim());

            int donGiaTTCol = ExcelColumnNameToNumber(txtDonGiaTinhThue.Text.Trim());

            int xuatXuCol = ExcelColumnNameToNumber(txtXuatXuColumn.Text.Trim());

            int TriGiaHoaDonCol = ExcelColumnNameToNumber(txtTriGiaTT.Text);


            int TriGiaTTCol = ExcelColumnNameToNumber(txtTriGiaTinhThueS.Text);

            int tsXNKCol = ExcelColumnNameToNumber(txtThueSuat.Text.Trim());

            int tsTTDBCol = ExcelColumnNameToNumber(txtThueSuatTTDB.Text.Trim());

            int tsVATCol = ExcelColumnNameToNumber(txtThueSuatVAT.Text.Trim());

            int tsTVCol = ExcelColumnNameToNumber(txtThueSuatTV.Text.Trim());

            int tsBVMTCol = ExcelColumnNameToNumber(txtThueSuatBVMT.Text.Trim());

            int thueXNKCol = ExcelColumnNameToNumber(txtThueXNK.Text.Trim());

            int thueTTDBCol = ExcelColumnNameToNumber(txtThueTTDB.Text.Trim());

            int thueVATCol = ExcelColumnNameToNumber(txtThueVAT.Text.Trim());

            int thueTVCol = ExcelColumnNameToNumber(txtThueTV.Text.Trim());

            int MaPhanLoaiKiemTraCol = ExcelColumnNameToNumber(txtMaPhanLoaiKiemTra.Text.Trim());

            int GhiChuCol = ExcelColumnNameToNumber(txtGhiChu.Text.Trim());

            int SoVanDonCol = ExcelColumnNameToNumber(txtSoVanDon.Text.Trim());

            int SoGiayPhepCol = ExcelColumnNameToNumber(txtSoGP.Text.Trim());

            int SoLuongContCol = ExcelColumnNameToNumber(txtSoLuongCont.Text.Trim());

            int NgayTQCol = ExcelColumnNameToNumber(txtNgayThongQuan.Text.Trim());

            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorTenHangHoa = "";
            string errorMaHS = "";
            string errorMaHSExits = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorDVT2 = "";
            string errorDVTExits2 = "";
            string errorXuatXu = "";
            string errorXuatXuValid = "";
            string errorSoLuong = "";
            string errorSoLuongValid = "";
            string errorSoLuong2 = "";
            string errorSoLuongValid2 = "";
            string errorDonGia = "";
            string errorDonGiaValid = "";
            string errorTriGia = "";
            string errorTriGiaValid = "";
            string errorTongTriGiaValid = "";

            string errorNPL = "";
            string errorSP = "";
            string errorTB = "";
            string errorHM = "";
            List<KDT_VNACC_HangMauDich> HangCollection = new List<KDT_VNACC_HangMauDich>();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        bool isExits = false;
                        KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                        try
                        {
                            hmd.SoToKhai = Convert.ToDecimal(wsr.Cells[SoToKhaiCol].Value.ToString().Trim());
                            if (hmd.SoToKhai.ToString().Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }

                        try
                        {
                            hmd.MaSoHang = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                            if (hmd.MaSoHang.Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.TenHang = TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.UTF8Literal2Unicode(wsr.Cells[tenHangCol].Value.ToString());
                            if (hmd.TenHang.Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + hmd.TenHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            hmd.NuocXuatXu = Convert.ToString(wsr.Cells[xuatXuCol].Value).Trim().ToUpper();
                            if (hmd.NuocXuatXu.Length == 0)
                            {
                                errorXuatXu += "[" + (wsr.Index + 1) + "]-[" + hmd.NuocXuatXu + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (hmd.NuocXuatXu == "00")
                                {
                                    hmd.NuocXuatXu = "VN";
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorXuatXu += "[" + (wsr.Index + 1) + "]-[" + hmd.NuocXuatXu + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim().ToUpper();
                            if (hmd.DVTLuong1.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.SoLuong1 = Convert.ToDecimal(wsr.Cells[soLuongCol].Value);
                            if (hmd.SoLuong1.ToString().Length == 0)
                            {
                                errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + hmd.SoLuong1 + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + hmd.SoLuong1 + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.DonGiaHoaDon = Convert.ToDecimal(Convert.ToDecimal(wsr.Cells[donGiaCol].Value));
                            if (hmd.DonGiaHoaDon.ToString().Length == 0)
                            {
                                errorDonGia += "[" + (wsr.Index + 1) + "]-[" + hmd.DonGiaHoaDon + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDonGia += "[" + (wsr.Index + 1) + "]-[" + hmd.DonGiaHoaDon + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.DonGiaTinhThue = Convert.ToDecimal(Convert.ToDecimal(wsr.Cells[donGiaTTCol].Value));
                            hmd.MaTTDonGiaTinhThue = "VND";
                            if (hmd.DonGiaTinhThue.ToString().Length == 0)
                            {
                                errorDonGia += "[" + (wsr.Index + 1) + "]-[" + hmd.DonGiaHoaDon + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDonGia += "[" + (wsr.Index + 1) + "]-[" + hmd.DonGiaHoaDon + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.TriGiaHoaDon = Convert.ToDecimal(Convert.ToDecimal(wsr.Cells[TriGiaHoaDonCol].Value));
                            if (hmd.TriGiaHoaDon.ToString().Length == 0)
                            {
                                errorTriGia += "[" + (wsr.Index + 1) + "]-[" + hmd.TriGiaHoaDon + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errorTriGia += "[" + (wsr.Index + 1) + "]-[" + hmd.TriGiaHoaDon + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.TriGiaTinhThueS = Convert.ToDecimal(Convert.ToDecimal(wsr.Cells[TriGiaTTCol].Value));
                            hmd.MaTTTriGiaTinhThueS = "VND";
                            if (hmd.TriGiaTinhThueS.ToString().Length == 0)
                            {
                                errorTriGia += "[" + (wsr.Index + 1) + "]-[" + hmd.TriGiaHoaDon + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            errorTriGia += "[" + (wsr.Index + 1) + "]-[" + hmd.TriGiaHoaDon + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.ThueSuatThue = Convert.ToString(wsr.Cells[tsXNKCol].Value);
                        }
                        catch (Exception ex)
                        {
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.SoTienThue = Convert.ToDecimal(wsr.Cells[thueXNKCol].Value);
                        }
                        catch (Exception)
                        {
                            //isAdd = false;
                        }
                        hmd.DVTDonGia = hmd.DVTLuong1;
                        hmd.DV_SL_TrongDonGiaTinhThue = hmd.DVTLuong1;
                        try
                        {
                            hmd.MaPhanLoaiKiemTra = Convert.ToString(wsr.Cells[MaPhanLoaiKiemTraCol].Value.ToString().Trim());
                            if (hmd.MaPhanLoaiKiemTra.ToString().Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }

                        try
                        {
                            hmd.GhiChu = Convert.ToString(wsr.Cells[GhiChuCol].Value.ToString().Trim());
                            if (hmd.GhiChu.ToString().Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.SoLuongCont = Convert.ToDecimal(wsr.Cells[SoLuongContCol].Value.ToString().Trim());
                            if (hmd.SoLuongCont.ToString().Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.SoVanDon = Convert.ToString(wsr.Cells[SoVanDonCol].Value.ToString().Trim());
                            if (hmd.SoVanDon.ToString().Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                KDT_VNACC_TK_SoVanDon vd = new KDT_VNACC_TK_SoVanDon();
                                vd.SoVanDon = hmd.SoVanDon;
                                vd.SoDinhDanh = hmd.SoVanDon;
                                hmd.VanDonCollection.Add(vd);
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.SoGiayPhep = Convert.ToString(wsr.Cells[SoGiayPhepCol].Value.ToString().Trim());
                            if (hmd.SoGiayPhep.ToString().Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                KDT_VNACC_TK_GiayPhep gp = new KDT_VNACC_TK_GiayPhep();
                                gp.SoGiayPhep = hmd.SoGiayPhep;
                                hmd.GiayPhepCollection.Add(gp);
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            hmd.NgayCapPhep = Convert.ToDateTime(wsr.Cells[NgayTQCol].Value.ToString().Trim());
                            if (hmd.NgayCapPhep.ToString().Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }

                        // 
                        try
                        {
                            string ThueSuatXNK = Convert.ToString(wsr.Cells[tsXNKCol].Value);
                            string ThueXNK = Convert.ToString(wsr.Cells[thueXNKCol].Value);

                            if (String.IsNullOrEmpty(ThueSuatXNK))
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (ThueSuatXNK != "NULL")
                                {
                                    KDT_VNACC_TK_PhanHoi_SacThue sacthue = new KDT_VNACC_TK_PhanHoi_SacThue();
                                    sacthue.MaSacThue = "N";
                                    sacthue.TenSacThue = "Thuế NK";
                                    if (ThueXNK != "NULL" )
                                    {
                                        sacthue.TongTienThue = Convert.ToDecimal(ThueXNK);
                                        hmd.SacThueCollection.Add(sacthue);
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {
                            string ThueSuatVAT = Convert.ToString(wsr.Cells[tsVATCol].Value);
                            string ThueVAT = Convert.ToString(wsr.Cells[thueVATCol].Value);

                            if (String.IsNullOrEmpty(ThueSuatVAT))
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (ThueSuatVAT != "NULL")
                                {
                                    KDT_VNACC_TK_PhanHoi_SacThue sacthue = new KDT_VNACC_TK_PhanHoi_SacThue();
                                    sacthue.MaSacThue = "V";
                                    sacthue.TenSacThue = "Thuế GTGT";
                                    if (ThueVAT != "NULL")
                                    {
                                        sacthue.TongTienThue = Convert.ToDecimal(ThueVAT);
                  
                                        hmd.SacThueCollection.Add(sacthue);

                                        KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                                        thue.TriGiaTinhThueVaThuKhac = hmd.TriGiaTinhThueS;
                                        if (ThueSuatVAT == "0")
                                        {
                                            thue.MaTSThueThuKhac = "V";
                                        }
                                        else if (ThueSuatVAT == "10")
                                        {
                                            thue.MaTSThueThuKhac = "VB901";
                                        }
                                        thue.ThueSuatThueVaThuKhac = ThueSuatVAT + "%";
                                        thue.SoTienThueVaThuKhac = Convert.ToDecimal(ThueVAT);
                                        thue.TenKhoanMucThueVaThuKhac = "Thuế GTGT";
                                        hmd.ThueThuKhacCollection.Add(thue);
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }

                        try
                        {

                            string ThueSuatBVMT = Convert.ToString(wsr.Cells[tsBVMTCol].Value);

                            if (String.IsNullOrEmpty(ThueSuatBVMT))
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (ThueSuatBVMT != "NULL")
                                {
                                    KDT_VNACC_TK_PhanHoi_SacThue sacthue = new KDT_VNACC_TK_PhanHoi_SacThue();
                                    sacthue.MaSacThue = "M";
                                    sacthue.TenSacThue = "Thuế BVMT";
                                    sacthue.TongTienThue = 0;

                                    hmd.SacThueCollection.Add(sacthue);

                                    KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                                    thue.TriGiaTinhThueVaThuKhac = hmd.TriGiaTinhThueS;
                                    thue.MaTSThueThuKhac = "M";
                                    thue.ThueSuatThueVaThuKhac ="0%";
                                    thue.SoTienThueVaThuKhac = 0;
                                    thue.TenKhoanMucThueVaThuKhac = "Thuế BVMT";
                                    hmd.ThueThuKhacCollection.Add(thue);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {

                            string ThueSuatTTDB = Convert.ToString(wsr.Cells[tsTTDBCol].Value);
                            string ThueTTDB = Convert.ToString(wsr.Cells[thueTTDBCol].Value);


                            if (String.IsNullOrEmpty(ThueSuatTTDB))
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (ThueSuatTTDB != "NULL")
                                {
                                    KDT_VNACC_TK_PhanHoi_SacThue sacthue = new KDT_VNACC_TK_PhanHoi_SacThue();
                                    sacthue.MaSacThue = "T";
                                    sacthue.TenSacThue = "Thuế TTDB";
                                    if (ThueTTDB != "NULL")
                                    {
                                        sacthue.TongTienThue = Convert.ToDecimal(ThueTTDB);

                                        hmd.SacThueCollection.Add(sacthue);

                                        KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                                        thue.TriGiaTinhThueVaThuKhac = hmd.TriGiaTinhThueS;
                                        if (ThueSuatTTDB == "0")
                                        {
                                            thue.MaTSThueThuKhac = "T";
                                        }
                                        thue.ThueSuatThueVaThuKhac = ThueSuatTTDB + "%";
                                        thue.SoTienThueVaThuKhac = Convert.ToDecimal(ThueTTDB);
                                        thue.TenKhoanMucThueVaThuKhac = "Thuế TTDB";
                                        hmd.ThueThuKhacCollection.Add(thue);
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        try
                        {

                            string ThueSuatTSTV = Convert.ToString(wsr.Cells[tsTVCol].Value);
                            string ThueTSTV = Convert.ToString(wsr.Cells[thueTVCol].Value);

                            if (String.IsNullOrEmpty(ThueSuatTSTV))
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                //isAdd = false;
                            }
                            else
                            {
                                if (ThueSuatTSTV != "NULL")
                                {
                                    KDT_VNACC_TK_PhanHoi_SacThue sacthue = new KDT_VNACC_TK_PhanHoi_SacThue();
                                    sacthue.MaSacThue = "B";
                                    sacthue.TenSacThue = "Thuế TV";
                                    if (ThueTSTV != "NULL")
                                    {
                                        sacthue.TongTienThue = Convert.ToDecimal(ThueTSTV);
 
                                        hmd.SacThueCollection.Add(sacthue);

                                        KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                                        thue.TriGiaTinhThueVaThuKhac = hmd.TriGiaTinhThueS;
                                        if (ThueSuatTSTV == "0")
                                        {
                                            thue.MaTSThueThuKhac = "B";
                                        }
                                        thue.ThueSuatThueVaThuKhac = ThueSuatTSTV + "%";
                                        thue.SoTienThueVaThuKhac = Convert.ToDecimal(ThueTSTV);
                                        thue.TenKhoanMucThueVaThuKhac = "Thuế TV";
                                        hmd.ThueThuKhacCollection.Add(thue);
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            //isAdd = false;
                        }
                        //#region Thuế khác

                        //if (maThueThuKhacCol1 > 0)
                        //{

                        //    string MaMucThue = Convert.ToString(wsr.Cells[maThueThuKhacCol1].Value);
                        //    string MaMienGiam = Convert.ToString(wsr.Cells[maMienGiamCol1].Value);
                        //    string SoTien = Convert.ToString(wsr.Cells[soTienMGCol1].Value);
                        //    if (LoaiHinh != "X")
                        //    {
                        //        if (MaMucThue != "")
                        //        {
                        //            KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                        //            thue.MaTSThueThuKhac = Convert.ToString(wsr.Cells[maThueThuKhacCol1].Value);
                        //            thue.MaMGThueThuKhac = Convert.ToString(wsr.Cells[maMienGiamCol1].Value);
                        //            thue.SoTienGiamThueThuKhac = string.IsNullOrEmpty(SoTien) ? 0 : Convert.ToDecimal(wsr.Cells[soTienMGCol1].Value);
                        //            hmd.ThueThuKhacCollection.Add(thue);
                        //        }
                        //    }
                        //}
                        //if (maThueThuKhacCol2 > 0)
                        //{
                        //    string MaMucThue = Convert.ToString(wsr.Cells[maThueThuKhacCol2].Value);
                        //    string MaMienGiam = Convert.ToString(wsr.Cells[maMienGiamCol2].Value);
                        //    string SoTien = Convert.ToString(wsr.Cells[soTienMGCol2].Value);
                        //    if (LoaiHinh != "X")
                        //    {

                        //        if (MaMucThue != "")
                        //        {
                        //            KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                        //            thue.MaTSThueThuKhac = Convert.ToString(wsr.Cells[maThueThuKhacCol2].Value);
                        //            thue.MaMGThueThuKhac = Convert.ToString(wsr.Cells[maMienGiamCol2].Value);
                        //            thue.SoTienGiamThueThuKhac = string.IsNullOrEmpty(SoTien) ? 0 : Convert.ToDecimal(wsr.Cells[soTienMGCol2].Value);
                        //            hmd.ThueThuKhacCollection.Add(thue);
                        //        }
                        //    }
                        //}
                        //if (maThueThuKhacCol3 > 0)
                        //{
                        //    string MaMucThue = Convert.ToString(wsr.Cells[maThueThuKhacCol3].Value);
                        //    string MaMienGiam = Convert.ToString(wsr.Cells[maMienGiamCol3].Value);
                        //    string SoTien = Convert.ToString(wsr.Cells[soTienMGCol3].Value);
                        //    if (LoaiHinh != "X")
                        //    {
                        //        if (MaMucThue != "")
                        //        {
                        //            KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                        //            thue.MaTSThueThuKhac = Convert.ToString(wsr.Cells[maThueThuKhacCol3].Value);
                        //            thue.MaMGThueThuKhac = Convert.ToString(wsr.Cells[maMienGiamCol3].Value);
                        //            thue.SoTienGiamThueThuKhac = string.IsNullOrEmpty(SoTien) ? 0 : Convert.ToDecimal(wsr.Cells[soTienMGCol3].Value);
                        //            hmd.ThueThuKhacCollection.Add(thue);
                        //        }
                        //    }
                        //}
                        //if (maThueThuKhacCol4 > 0)
                        //{
                        //    string MaMucThue = Convert.ToString(wsr.Cells[maThueThuKhacCol4].Value);
                        //    string MaMienGiam = Convert.ToString(wsr.Cells[maMienGiamCol4].Value);
                        //    string SoTien = Convert.ToString(wsr.Cells[soTienMGCol4].Value);
                        //    if (LoaiHinh != "X")
                        //    {
                        //        if (MaMucThue != "")
                        //        {
                        //            KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                        //            thue.MaTSThueThuKhac = Convert.ToString(wsr.Cells[maThueThuKhacCol4].Value);
                        //            thue.MaMGThueThuKhac = Convert.ToString(wsr.Cells[maMienGiamCol4].Value);
                        //            thue.SoTienGiamThueThuKhac = string.IsNullOrEmpty(SoTien) ? 0 : Convert.ToDecimal(wsr.Cells[soTienMGCol4].Value);
                        //            hmd.ThueThuKhacCollection.Add(thue);
                        //        }
                        //    }
                        //}

                        //#endregion

                        if (isAdd)
                            HangCollection.Add(hmd);
                        }
                        catch (Exception ex)
                        {
                            //this.SaveDefault();
                            return;
                        }
                    }
            }
                //if (!String.IsNullOrEmpty(errorMaHangHoa))
                //    errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " không được để trống";
                //if (!String.IsNullOrEmpty(errorTenHangHoa))
                //    errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " không được để trống";
                //if (!String.IsNullOrEmpty(errorDVT))
                //    errorTotal += "\n - [STT] -[ĐVT 1] : \n" + errorDVT + " không được để trống";
                //if (!String.IsNullOrEmpty(errorDVTExits))
                //    errorTotal += "\n - [STT] -[ĐVT 1] : \n" + errorDVTExits + " không hợp lệ. ĐVT phải là ĐVT VNACCS (Ví dụ : Cái/Chiếc là : PCE)";
                //if (!String.IsNullOrEmpty(errorDVT2))
                //    errorTotal += "\n - [STT] -[ĐVT 2] : \n" + errorDVT2 + " không được để trống";
                //if (!String.IsNullOrEmpty(errorDVTExits2))
                //    errorTotal += "\n - [STT] -[ĐVT 2] : \n" + errorDVTExits2 + " không hợp lệ. ĐVT phải là ĐVT VNACCS (Ví dụ : Cái/Chiếc là : PCE)";
                //if (!String.IsNullOrEmpty(errorXuatXu))
                //    errorTotal += "\n - [STT] -[NƯỚC XUẤT XỨ] : \n" + errorXuatXu + " không được để trống";
                //if (!String.IsNullOrEmpty(errorXuatXuValid))
                //    errorTotal += "\n - [STT] -[NƯỚC XUẤT XỨ] : \n" + errorXuatXuValid + " không hợp lệ .Ví dụ : Việt Nam : VN";
                //if (!String.IsNullOrEmpty(errorMaHS))
                //    errorTotal += "\n - [STT] -[MÃ HS] : " + errorMaHS + " không được để trống";
                //if (!String.IsNullOrEmpty(errorMaHSExits))
                //    errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHSExits + "không hợp lê. Mã HS có độ dài là 8 số .";
                //if (!String.IsNullOrEmpty(errorNPL))
                //    errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorNPL + " chưa được đăng ký";
                //if (!String.IsNullOrEmpty(errorSP))
                //    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSP + " chưa được đăng ký";
                //if (!String.IsNullOrEmpty(errorTB))
                //    errorTotal += "\n - [STT] -[MÃ THIẾT BỊ] : \n" + errorTB + "chưa được đăng ký";
                //if (!String.IsNullOrEmpty(errorHM))
                //    errorTotal += "\n - [STT] -[MÃ HÀNG MẪU] : \n" + errorHM + "chưa được đăng ký";
                //if (!String.IsNullOrEmpty(errorSoLuong))
                //    errorTotal += "\n - [STT] -[SỐ LƯỢNG 1] : " + errorSoLuong + " không được để trống";
                //if (!String.IsNullOrEmpty(errorSoLuongValid))
                //    errorTotal += "\n - [STT] -[SỐ LƯỢNG 1] : \n" + errorSoLuongValid + " đã cấu hình chỉ được nhập tối đa " + FormatSoLuong + " số thập phân . Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel \n";
                //if (!String.IsNullOrEmpty(errorSoLuong2))
                //    errorTotal += "\n - [STT] -[SỐ LƯỢNG 2] : " + errorSoLuong2 + " không được để trống";
                //if (!String.IsNullOrEmpty(errorSoLuongValid2))
                //    errorTotal += "\n - [STT] -[SỐ LƯỢNG 2] : \n" + errorSoLuongValid2 + " đã cấu hình chỉ được nhập tối đa " + FormatSoLuong1 + " số thập phân .Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel\n";
                //if (!String.IsNullOrEmpty(errorDonGia))
                //    errorTotal += "\n - [STT] -[ĐƠN GIÁ] : " + errorDonGia + " không được để trống";
                //if (!String.IsNullOrEmpty(errorDonGiaValid))
                //    errorTotal += "\n - [STT] -[ĐƠN GIÁ] : \n" + errorDonGiaValid + " đã cấu hình chỉ được nhập tối đa " + FormatDonGia + " số thập phân . Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel\n";
                //if (!String.IsNullOrEmpty(errorTriGia))
                //    errorTotal += "\n - [STT] -[TRỊ GIÁ] : " + errorTriGia + " không được để trống";
                //if (!String.IsNullOrEmpty(errorTriGiaValid))
                //    errorTotal += "\n - [STT] -[TRỊ GIÁ] : \n" + errorTriGiaValid + " đã cấu hình chỉ được nhập tối đa " + FormatTriGia + " số thập phân  .Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel\n";
                //if (!String.IsNullOrEmpty(errorTongTriGiaValid))
                //    errorTotal += "\n - [STT] -[SỐ LƯỢNG]- [ĐƠN GIÁ] - [TRỊ GIÁ] : " + errorTongTriGiaValid + " Tổng trị giá không đúng bằng : Số lượng * Đơn giá";
                if (String.IsNullOrEmpty(errorTotal))
                {
                    foreach (KDT_VNACC_ToKhaiMauDich item in TKMDCollection)
                    {
                        foreach (KDT_VNACC_HangMauDich hmd in HangCollection)
                        {
                            if (item.SoToKhai == hmd.SoToKhai)
                            {
                                hmd.MaTTDonGia = item.MaTTHoaDon;
                                item.TrangThaiXuLy = TrangThaiXuLy.DA_PHAN_LUONG.ToString();
                                item.MaPhanLoaiKiemTra = hmd.MaPhanLoaiKiemTra;
                                item.GhiChu = hmd.GhiChu;
                                item.SoLuongCont = hmd.SoLuongCont;
                                item.NgayCapPhep = hmd.NgayCapPhep;
                                item.TongSoDongHangCuaToKhai = item.HangCollection.Count + 1;
                                item.TongSoTrangCuaToKhai = item.TongSoDongHangCuaToKhai + 2;
                                if (hmd.VanDonCollection.Count >0)
                                {
                                    item.VanDonCollection = hmd.VanDonCollection;
                                }
                                if (hmd.GiayPhepCollection.Count > 0)
                                {
                                    item.GiayPhepCollection = hmd.GiayPhepCollection;
                                }
                                decimal TongTienThuePhaiNop = 0;
                                if (hmd.SacThueCollection.Count > 0)
                                {
                                    foreach (KDT_VNACC_TK_PhanHoi_SacThue it in hmd.SacThueCollection)
                                    {
                                        it.SoDongTongTienThue = item.TongSoDongHangCuaToKhai;
                                        TongTienThuePhaiNop += it.TongTienThue;
                                    }
                                    item.SacThueCollection = hmd.SacThueCollection;
                                }
                                item.TongTienThuePhaiNop = TongTienThuePhaiNop;
                                item.HangCollection.Add(hmd);
                            }
                        }   
                    }
                    foreach (KDT_VNACC_ToKhaiMauDich TKMD in TKMDCollection)
                    {
                        if (TKMD.SoToKhai > 0)
                        {
                            TKMD.TongSoDongHangCuaToKhai = TKMD.HangCollection.Count;
                            TKMD.TongSoTrangCuaToKhai = TKMD.TongSoDongHangCuaToKhai + 2;
                            int i = 1;
                            foreach (KDT_VNACC_HangMauDich item in TKMD.HangCollection)
                            {
                                if (i < 10)
                                {
                                    item.SoDong = "0" +  i.ToString();
                                }
                                else
                                {
                                    item.SoDong = i.ToString();
                                }
                                i++;
                            }
                            TKMD.InsertUpdateFull();
                        }
                    }
                    ShowMessageTQDT("Thông báo từ hệ thống", "Nhập hàng từ Excel thành công ", false);
                }
                else
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "Nhập hàng từ Excel không thành công. \n" + errorTotal, false);
                }
                //this.SaveDefault();
                this.Close();
        }
        private List<KDT_VNACC_ToKhaiMauDich> CopyToKhai(KDT_VNACC_ToKhaiMauDich tkmd, bool isCopyHangHoa, int number)
        {
            List<KDT_VNACC_ToKhaiMauDich> ToKhaiMauDichCollection = new List<KDT_VNACC_ToKhaiMauDich>();
            int index = 0;
            int count = 49;
            for (int i = 0; i < number; i++)
            {
                KDT_VNACC_ToKhaiMauDich tkmdcopy = new KDT_VNACC_ToKhaiMauDich();
                HelperVNACCS.UpdateObject<KDT_VNACC_ToKhaiMauDich>(tkmdcopy, tkmd, false);
                tkmdcopy.NgayDangKy = new DateTime(1900, 1, 1);
                tkmdcopy.SoToKhai = 0;
                tkmdcopy.SoNhanhToKhai = 0;
                tkmdcopy.SoToKhaiDauTien = "";
                if (i==0)
                    tkmdcopy.SoToKhaiDauTien = "F";
                tkmdcopy.SoNhanhToKhai = i + 1;
                tkmdcopy.TongSoTKChiaNho = number;
                tkmdcopy.MaPhanLoaiKiemTra = string.Empty;
                tkmdcopy.AnHangCollection = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
                tkmdcopy.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                tkmdcopy.SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                tkmdcopy.TrangThaiXuLy = "0";
                tkmdcopy.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                tkmdcopy.InputMessageID = string.Empty;
                tkmdcopy.MessageTag = string.Empty;
                tkmdcopy.IndexTag = string.Empty;
                tkmdcopy.HangCollection = new List<KDT_VNACC_HangMauDich>();
                tkmdcopy.HangCollection = tkmd.HangCollection.GetRange(index, count);
                index += 50;
                count += 49;
                ToKhaiMauDichCollection.Add(tkmdcopy);
            }
            return ToKhaiMauDichCollection;
            //if (!isCopyHangHoa)
            //{
            //    tkmdcopy.HangCollection = new List<KDT_VNACC_HangMauDich>();
            //}
            //tkmdcopy.HangCollection = tkmd.HangCollection;

            //DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + tkmdcopy.MaLoaiHinh + "'", "ID");
            //DataRow dr = ds.Tables[0].Rows[0];
            //string loaiHinh = dr["ReferenceDB"].ToString();
            //if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
            //{
            //    VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
            //    //f.TKMD = new KDT_VNACC_ToKhaiMauDich();
            //    f.isCopy = true;
            //    f.TKMD = tkmdcopy;
            //    f.ShowDialog();
            //}
            //if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
            //{
            //    VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
            //    //f.TKMD = new KDT_VNACC_ToKhaiMauDich();
            //    f.IsCopy = true;
            //    f.TKMD = tkmdcopy;
            //    f.ShowDialog();
            //}
        }
        //private void SaveDefault()
        //{
        //    try
        //    {
        //        DocExcel_VNACCS docExcel = new DocExcel_VNACCS();
        //        docExcel.insertThongSoHangTK(cbbSheetName.Text.Trim(),
        //                                                              txtRow.Text.Trim(),
        //                                                              txtMaHangColumn.Text.Trim(),
        //                                                              txtTenHangColumn.Text.Trim(),
        //                                                              txtMaHSColumn.Text.Trim(),
        //                                                              txtXuatXuColumn.Text.Trim(),
        //                                                              txtSoLuongColumn.Text.Trim(),
        //                                                              txtSoLuongColumn2.Text.Trim(),
        //                                                              txtDVTColumn.Text.Trim(),
        //                                                              txtDVTColumn2.Text.Trim(),
        //            ////minhnd12/04/2015
        //            //txtSoLuongColumn2.Text.Trim(),
        //            //txtDVTColumn2.Text.Trim(),
        //            ////minhnd12/04/2015
        //                                                              txtDonGiaColumn.Text.Trim(),
        //                                                              txtTriGiaTT.Text.Trim(),
        //                                                              txtBieuThueXNK.Text.Trim(),
        //                                                              txtThueSuat.Text.Trim(),
        //                                                              txtMaMienGiamThueKhac1.Text.Trim(),
        //                                                              txtThueKhac1.Text.Trim(),
        //                                                              txtSoTienMienGiamThueKhac1.Text.Trim()

        //                                                              );
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);

        //    }
        //}
        //private void ReadDefault()
        //{
        //    try
        //    {
        //        //DocExcel_VNACCS docExcel = new DocExcel_VNACCS();
        //        FrmCauHinhThongSoReadExcel docExcel = new FrmCauHinhThongSoReadExcel();
        //        docExcel.ReadDefault(cbbSheetName,
        //                              txtRow,
        //                              txtMaHangColumn,
        //                              txtTenHangColumn,
        //                              txtMaHSColumn,
        //                              txtXuatXuColumn,
        //                              txtSoLuongColumn,
        //                              txtSoLuongColumn2,
        //                              txtDVTColumn,
        //                              txtDVTColumn2,
        //                              txtDonGiaColumn,
        //                              txtTriGiaTT,
        //                              txtBieuThueXNK,
        //                              txtThueSuat,
        //                              txtThueKhac1,
        //                              txtMaMienGiamThueKhac1,
        //                              txtSoTienMienGiamThueKhac1
        //                              );
        //        docExcel.FormatSoLuong = FormatSoLuong;
        //        docExcel.FormatSoLuong1 = FormatSoLuong1;
        //        docExcel.FormatDonGia = FormatDonGia;
        //        docExcel.FormatTriGia = FormatTriGia;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //    }
        //}

        private void ReadExcelForm_Load(object sender, EventArgs e)
        {
//            try
//            {
//                ReadDefault();
//                if (LoaiHinh == "X")
//                {
//                    lblXuatXu.Visible = true;
//                    txtXuatXuColumn.Visible = true;
//                    lblThue.Visible = false;
//                    txtBieuThueXNK.Visible = false;
//                    lblMucThue.Visible = false;
//                    txtThueKhac1.Visible = false;
//                    lblMaMienGiam.Visible = false;
//                    txtMaMienGiamThueKhac1.Visible = false;
//                    lblTienMG.Visible = false;
//                    txtSoTienMienGiamThueKhac1.Visible = false;
//                    grbThuKhac.Visible = false;
//                    uiGroupBox1.Height = uiGroupBox1.Height - grbThuKhac.Height;
//                    this.Height = this.Height - grbThuKhac.Height;
//                }
//#if GC_V4
//                if (LoaiHinh == "N")
//                {
//                    ctrMaMienGiam.ImportType = EImportExport.I;
//                    ctrMaMienGiam.ReLoadData();
//                    ctrMaMienGiam.Code = "XNG81";
//                }
//                else
//                {
//                    ctrMaMienGiam.ImportType = EImportExport.E;
//                    ctrMaMienGiam.Code = "XNG82";
//                    ctrMaMienGiam.ReLoadData();
//                }
//#elif SXXK_V4
//                if (LoaiHinh == "N")
//                {
//                    ctrMaMienGiam.ImportType = EImportExport.I;
//                    ctrMaMienGiam.ReLoadData();
//                    ctrMaMienGiam.Code = "XN210";
//                }
//#endif
//            }
//            catch (Exception ex)
//            {
//                Logger.LocalLogger.Instance().WriteMessage(ex);
//            }

        }


        private void lblLinkExcelTemplate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_HangHoa_VANCCS();
            }
            catch (Exception ex) 
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtXuatXuColumn_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHopDong_So_TextChanged(object sender, EventArgs e)
        {

        }


    }
}