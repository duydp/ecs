﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;

namespace Company.Interface
{
    public partial class Login : Form
    {
        protected Company.Controls.MessageBoxControl _MsgBox;
        public Login()
        {
            InitializeComponent();
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new Company.Controls.MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog();
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        ShowMessage("Đăng nhập không thành công.", false);

                    }
                    else
                    {
                        ShowMessage("Login unsuccessfully", false);
                    }
                    MainForm.isLoginSuccess = false;
                }
                else
                {
                    MainForm.isLoginSuccess = true;
                    GlobalSettings.UserLog = txtUser.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("UserLog", txtUser.Text.Trim());
                    GlobalSettings.PassLog = txtMatKhau.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PassLog", txtMatKhau.Text.Trim());
                    GlobalSettings.RefreshKey();
                    this.Close();
                }
            }
            catch
            {
                MessageBox.Show("Không kết nối tới cơ sở dữ liệu");
                linkLabel1_LinkClicked(null, null);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtUser.Text = string.Empty;
            txtMatKhau.Text = string.Empty;
            txtUser.Focus();
            if (GlobalSettings.NGON_NGU == "0")
            {

                linkLabel1.Text = "Cấu hình thông số kết nối ";
                uiButton3.Text = "Đăng nhập";
                uiButton2.Text = "Thoát";


                if (System.IO.File.Exists("loginv.png"))
                {
                    this.BackgroundImage = System.Drawing.Image.FromFile("loginv.png");
                    
                }
                
                //  this.BackgroundImageLayout = ImageLayout.Stretch;                
            }
            else
            {
                linkLabel1.Text = "Connected configuration";
                uiButton3.Text = "Login";
                uiButton2.Text = "Close";

                if (System.IO.File.Exists("logine.png"))
                    this.BackgroundImage = System.Drawing.Image.FromFile("logine.png");
                //this.BackgroundImageLayout = ImageLayout.Stretch;                
            }
        }




    }
}