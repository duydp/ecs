GO
IF OBJECT_ID(N'p_KDT_CX_HangDuaVaoDangKy_SelectDynamic_NPL') IS NOT NULL
	DROP PROCEDURE p_KDT_CX_HangDuaVaoDangKy_SelectDynamic_NPL
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_CX_HangDuaVaoDangKy_SelectDynamic_NPL]  
-- Database: ECS_TQDT_SXXK_V4  
-- Author: Ngo Thanh Tung  
-- Time created: Monday, January 21, 2013  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_CX_HangDuaVaoDangKy_SelectDynamic_NPL]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT   
 NPL_DK.[ID],  
 [SoTiepNhan],  
 [NgayTiepNhan],  
 [MaHaiQuan],  
 [MaDoanhNghiep],  
 [MaDaiLy],  
 [TrangThaiXuLy],  
 [GUIDSTR],  
 [DeXuatKhac],  
 [LyDoSua],  
 [ActionStatus],  
 [GuidReference],  
 [NamDK],  
 [HUONGDAN],  
 [PhanLuong],  
 [Huongdan_PL]  
FROM [dbo].[t_KDT_CX_HangDuaVaoDangKy]  NPL_DK INNER JOIN dbo.t_KDT_CX_HangDuaVao NPL ON NPL.Master_ID = NPL_DK.ID 
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO
------------------------------------------------------------------------------------------------------------------------          
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_DinhMucChung]          
-- Database: ECS_TQ_SXXK          
-- Author: Ngo Thanh Tung          
-- Time created: Monday, March 22, 2010          
------------------------------------------------------------------------------------------------------------------------          
          
ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_DinhMucChung]          
 @WhereCondition nvarchar(500),          
 @OrderByExpression nvarchar(250) = NULL          
           
AS          
          
SET NOCOUNT ON          
SET TRANSACTION ISOLATION LEVEL READ COMMITTED          
          
DECLARE @SQL nvarchar(3250)          
          
SET @SQL = ' SELECT  DMDK.ID ,      
  DMDK.MaHaiQuan,    
  DMDK.SoTiepNhan,  
        DMDK.NgayTiepNhan ,        
        DM.MaSanPham ,    
		DMDK.TrangThaiXuLy ,     
  CASE WHEN DM.MaSanPham IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_SXXK_SanPham WHERE DM.MaSanPham = Ma ) ELSE NULL END AS TenSanPham,        
    CASE WHEN DM.MaSanPham IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID= (SELECT TOP 1 DVT_ID FROM dbo.t_SXXK_SanPham WHERE DM.MaSanPham = Ma)) ELSE DM.DVT_ID END AS DVT_SP,        
        DM.MaNguyenPhuLieu AS MaNPL ,        
  CASE WHEN DM.MaNguyenPhuLieu IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_SXXK_NguyenPhuLieu WHERE DM.MaNguyenPhuLieu = Ma ) ELSE NULL END AS TenNPL,        
  CASE WHEN DM.MaNguyenPhuLieu IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID= (SELECT TOP 1 DVT_ID FROM dbo.t_SXXK_NguyenPhuLieu WHERE DM.MaNguyenPhuLieu = Ma)) ELSE DM.DVT_ID END AS DVT,        
        DM.DinhMucSuDung ,        
        DM.TyLeHaoHut ,        
  DM.DinhMucSuDung + DM.DinhMucSuDung*DM.TyLeHaoHut/100 AS DinhMucChung,        
               CASE WHEN IsFromVietNam = 1 THEN N''Có''        
      ELSE ''Không'' END AS MuaVN        
   FROM dbo.t_KDT_SXXK_DinhMucDangKy DMDK INNER JOIN t_kdt_SXXK_DinhMuc DM ON  DM.Master_ID = DMDK.ID WHERE          
'+ @WhereCondition          
          
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0          
BEGIN          
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression          
END          
          
EXEC sp_executesql @SQL          

GO
ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]  
 @Master_ID bigint  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
SELECT  
 [ID],  
 [MaSanPham],  
 [MaNguyenPhuLieu],
 CASE WHEN MaNguyenPhuLieu IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_SXXK_NguyenPhuLieu WHERE MaNguyenPhuLieu=Ma) ELSE NULL END AS TenNPL,
 [DVT_ID],  
 [DinhMucSuDung],  
 [TyLeHaoHut],  
 [GhiChu],  
 [STTHang],  
 [Master_ID],  
 [IsFromVietNam],  
 [MaDinhDanhLenhSX]  
FROM  
 [dbo].[t_KDT_SXXK_DinhMuc]  
WHERE  
 [Master_ID] = @Master_ID  
 GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.3',GETDATE(), N'CẬP NHẬT PROCEDURE ĐỊNH MỨC')
END