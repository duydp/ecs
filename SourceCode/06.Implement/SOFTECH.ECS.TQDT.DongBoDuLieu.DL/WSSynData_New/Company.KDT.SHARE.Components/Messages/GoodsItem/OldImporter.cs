﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
  public  class OldImporter
    {
     /// <summary>
        /// Tên doanh nghiệp
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

      /// <summary>
        /// Mã doanh nghiệp
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; }

      /// <summary>
        /// Lý do chuyển đổi
        /// </summary>
        [XmlElement("reason")]
        public string Reason { get; set; }
    }
}
