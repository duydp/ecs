/*
Run this script on:

        ecsteam.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 04/13/2012 9:58:41 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ALTER COLUMN [MaCuc] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ToKhaiMauDichCopy]'
GO

-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
ALTER PROCEDURE [dbo].[ToKhaiMauDichCopy]  
 -- Add the parameters for the stored procedure here  
 @ID BIGINT  
 AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
    --delete from dbo.t_KDT_ToKhaiMauDich_Delete  
    --delete  from  dbo.t_KDT_HangMauDich_Delete  
    --SELECT * FROM dbo.t_KDT_HangMauDich_Delete  
     --SELECT * from dbo.t_KDT_ToKhaiMauDich_Delete  
   INSERT INTO dbo.t_KDT_ToKhaiMauDich_Delete  
   ([SoTiepNhan]
      ,[NgayTiepNhan]
      ,[MaHaiQuan]
      ,[SoToKhai]
      ,[MaLoaiHinh]
      ,[NgayDangKy]
      ,[MaDoanhNghiep]
      ,[TenDoanhNghiep]
      ,[MaDaiLyTTHQ]
      ,[TenDaiLyTTHQ]
      ,[TenDonViDoiTac]
      ,[ChiTietDonViDoiTac]
      ,[SoGiayPhep]
      ,[NgayGiayPhep]
      ,[NgayHetHanGiayPhep]
      ,[SoHopDong]
      ,[NgayHopDong]
      ,[NgayHetHanHopDong]
      ,[SoHoaDonThuongMai]
      ,[NgayHoaDonThuongMai]
      ,[PTVT_ID]
      ,[SoHieuPTVT]
      ,[NgayDenPTVT]
      ,[QuocTichPTVT_ID]
      ,[LoaiVanDon]
      ,[SoVanDon]
      ,[NgayVanDon]
      ,[NuocXK_ID]
      ,[NuocNK_ID]
      ,[DiaDiemXepHang]
      ,[CuaKhau_ID]
      ,[DKGH_ID]
      ,[NguyenTe_ID]
      ,[TyGiaTinhThue]
      ,[TyGiaUSD]
      ,[PTTT_ID]
      ,[SoHang]
      ,[SoLuongPLTK]
      ,[TenChuHang]
      ,[SoContainer20]
      ,[SoContainer40]
      ,[SoKien]
      ,[TrongLuong]
      ,[TongTriGiaKhaiBao]
      ,[TongTriGiaTinhThue]
      ,[LoaiToKhaiGiaCong]
      ,[LePhiHaiQuan]
      ,[PhiBaoHiem]
      ,[PhiVanChuyen]
      ,[PhiXepDoHang]
      ,[PhiKhac]
      ,[CanBoDangKy]
      ,[QuanLyMay]
      ,[TrangThaiXuLy]
      ,[LoaiHangHoa]
      ,[GiayTo]
      ,[PhanLuong]
      ,[MaDonViUT]
      ,[TenDonViUT]
      ,[TrongLuongNet]
      ,[SoTienKhoan]
      ,[HeSoNhan]
      ,[GUIDSTR]
      ,[DeXuatKhac]
      ,[LyDoSua]
      ,[ActionStatus]
      ,[GuidReference]
      ,[NamDK]
      ,[HUONGDAN])
 SELECT 
      [SoTiepNhan]
      ,[NgayTiepNhan]
      ,[MaHaiQuan]
      ,[SoToKhai]
      ,[MaLoaiHinh]
      ,[NgayDangKy]
      ,[MaDoanhNghiep]
      ,[TenDoanhNghiep]
      ,[MaDaiLyTTHQ]
      ,[TenDaiLyTTHQ]
      ,[TenDonViDoiTac]
      ,[ChiTietDonViDoiTac]
      ,[SoGiayPhep]
      ,[NgayGiayPhep]
      ,[NgayHetHanGiayPhep]
      ,[SoHopDong]
      ,[NgayHopDong]
      ,[NgayHetHanHopDong]
      ,[SoHoaDonThuongMai]
      ,[NgayHoaDonThuongMai]
      ,[PTVT_ID]
      ,[SoHieuPTVT]
      ,[NgayDenPTVT]
      ,[QuocTichPTVT_ID]
      ,[LoaiVanDon]
      ,[SoVanDon]
      ,[NgayVanDon]
      ,[NuocXK_ID]
      ,[NuocNK_ID]
      ,[DiaDiemXepHang]
      ,[CuaKhau_ID]
      ,[DKGH_ID]
      ,[NguyenTe_ID]
      ,[TyGiaTinhThue]
      ,[TyGiaUSD]
      ,[PTTT_ID]
      ,[SoHang]
      ,[SoLuongPLTK]
      ,[TenChuHang]
      ,[SoContainer20]
      ,[SoContainer40]
      ,[SoKien]
      ,[TrongLuong]
      ,[TongTriGiaKhaiBao]
      ,[TongTriGiaTinhThue]
      ,[LoaiToKhaiGiaCong]
      ,[LePhiHaiQuan]
      ,[PhiBaoHiem]
      ,[PhiVanChuyen]
      ,[PhiXepDoHang]
      ,[PhiKhac]
      ,[CanBoDangKy]
      ,[QuanLyMay]
      ,[TrangThaiXuLy]
      ,[LoaiHangHoa]
      ,[GiayTo]
      ,[PhanLuong]
      ,[MaDonViUT]
      ,[TenDonViUT]
      ,[TrongLuongNet]
      ,[SoTienKhoan]
      ,[HeSoNhan]
      ,[GUIDSTR]
      ,[DeXuatKhac]
      ,[LyDoSua]
      ,[ActionStatus]
      ,[GuidReference]
      ,[NamDK]
      ,[HUONGDAN] 
 FROM dbo.t_KDT_ToKhaiMauDich WHERE ID=@ID  
   
 INSERT INTO  dbo.t_KDT_HangMauDich_Delete  
	([TKMD_ID]
	,[SoThuTuHang]
	,[MaHS]
	,[MaPhu]
	,[TenHang]
	,[NuocXX_ID]
	,[DVT_ID]
	,[SoLuong]
	,[TrongLuong]
	,[DonGiaKB]
	,[DonGiaTT]
	,[TriGiaKB]
	,[TriGiaTT]
	,[TriGiaKB_VND]
	,[ThueSuatXNK]
	,[ThueSuatTTDB]
	,[ThueSuatGTGT]
	,[ThueXNK]
	,[ThueTTDB]
	,[ThueGTGT]
	,[PhuThu]
	,[TyLeThuKhac]
	,[TriGiaThuKhac]
	,[MienThue]
	,[Ma_HTS]
	,[DVT_HTS]
	,[SoLuong_HTS]
	,[ThueSuatGiam])
 SELECT     
	t_KDT_HangMauDich.[TKMD_ID]
	, t_KDT_HangMauDich.[SoThuTuHang]
	, t_KDT_HangMauDich.[MaHS]
	, t_KDT_HangMauDich.[MaPhu]
	, t_KDT_HangMauDich.[TenHang]
	, t_KDT_HangMauDich.[NuocXX_ID]
	, t_KDT_HangMauDich.[DVT_ID]
	, t_KDT_HangMauDich.[SoLuong]
	, t_KDT_HangMauDich.[TrongLuong]
	, t_KDT_HangMauDich.[DonGiaKB]
	, t_KDT_HangMauDich.[DonGiaTT]
	, t_KDT_HangMauDich.[TriGiaKB]
	, t_KDT_HangMauDich.[TriGiaTT]
	, t_KDT_HangMauDich.[TriGiaKB_VND]
	, t_KDT_HangMauDich.[ThueSuatXNK]
	, t_KDT_HangMauDich.[ThueSuatTTDB]
	, t_KDT_HangMauDich.[ThueSuatGTGT]
	, t_KDT_HangMauDich.[ThueXNK]
	, t_KDT_HangMauDich.[ThueTTDB]
	, t_KDT_HangMauDich.[ThueGTGT]
	, t_KDT_HangMauDich.[PhuThu]
	, t_KDT_HangMauDich.[TyLeThuKhac]
	, t_KDT_HangMauDich.[TriGiaThuKhac]
	, t_KDT_HangMauDich.[MienThue]
	, t_KDT_HangMauDich.[Ma_HTS]
	, t_KDT_HangMauDich.[DVT_HTS]
	, t_KDT_HangMauDich.[SoLuong_HTS]
	, t_KDT_HangMauDich.[ThueSuatGiam]
 FROM         t_KDT_HangMauDich INNER JOIN  
        t_KDT_ToKhaiMauDich ON t_KDT_HangMauDich.TKMD_ID = t_KDT_ToKhaiMauDich.ID  
 WHERE     (t_KDT_ToKhaiMauDich.ID = @ID)  
   
   
  
   
   
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]'
GO
     
      
ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]      
@MaDoanhNghiep VARCHAR (14),      
@MaHaiQuan char(6),      
@TuNgay datetime,      
@DenNgay datetime      
AS    
SELECT       
  A.MaHaiQuan,      
     A.SoToKhai,      
  A.MaLoaiHinh,      
  A.NamDangKy,      
  A.NgayDangKy,      
  A.NGAY_THN_THX,     
  A.NgayHoanThanh      
  ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.           
FROM t_SXXK_ToKhaiMauDich A      
 LEFT JOIN (      
   SELECT DISTINCT      
    SoToKhai,      
    MaLoaiHinh,      
    NamDangKy,      
    MaHaiQuan      
   FROM t_KDT_SXXK_BKToKhaiXuat      
   WHERE      
    MaHaiQuan = @MaHaiQuan) D ON      
  A.SoToKhai=D.SoToKhai AND      
  A.MaLoaiHinh=D.MaLoaiHinh AND      
  A.NamDangKy=D.NamDangKy AND      
  A.MaHaiQuan=D.MaHaiQuan    
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.   
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)     
WHERE      
 A.MaDoanhNghiep = @MaDoanhNghiep AND      
 A.MaHaiQuan = @MaHaiQuan AND      
 (A.MaLoaiHinh LIKE 'XSX%' OR A.MaLoaiHinh LIKE 'XGC%')      
 AND A.NgayDangKy between @TuNgay and @DenNgay      
 AND (A.NGAY_THN_THX IS NOT NULL OR year(A.NGAY_THN_THX)!=1900 )      
 AND D.SoToKhai IS NULL     
 AND a.TrangThai != 10  --HungTQ Updated 22/06/2011: Bo sung loc du lieu theo TrangThai to khai != 10 (To khai Huy)    
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.     
 AND CAST(a.SoToKhai AS VARCHAR(50)) + CAST(a.MaLoaiHinh AS VARCHAR(50)) + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (SELECT  CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50)) + CAST(YEAR(NgayDangKy) AS VARCHAR(50)) FROM dbo.t_KDT_ToKhaiMauDich WHERE MaHaiQuan = 'C34C' AND MaDoanhNghiep = '0400101556' AND TrangThaiXuLy != 1 AND SoToKhai != 0)  
      
ORDER BY A.NgayDangKy , A.SoToKhai, A.MaLoaiHinh    
    
/* Comment Script    
    
SELECT       
 A.MaHaiQuan,      
 A.SoToKhai,      
 A.MaLoaiHinh,      
 A.NamDangKy,      
 A.NgayDangKy,      
 A.NGAY_THN_THX      
FROM     
(    
 --HungTQ Updated 22/06/2011: Bo sung loc du lieu theo TrangThaiXuLy = 1    
 SELECT   DISTINCT    
  A.MaHaiQuan,     
  A.MaDoanhNghiep,     
  A.SoToKhai,      
  A.MaLoaiHinh,      
  A.NamDangKy,      
  A.NgayDangKy,      
  A.NGAY_THN_THX, tkmd.TrangThaiXuLy     
 FROM t_SXXK_ToKhaiMauDich A  LEFT JOIN  dbo.t_KDT_ToKhaiMauDich tkmd    
 ON a.SoToKhai = tkmd.SoToKhai AND a.MaLoaiHinh = tkmd.MaLoaiHinh AND a.NamDangKy = YEAR(tkmd.NgayDangKy) AND A.MaHaiQuan = tkmd.MaHaiQuan AND A.MaDoanhNghiep = tkmd.MaDoanhNghiep    
) A      
 LEFT JOIN (      
   SELECT DISTINCT      
    SoToKhai,      
    MaLoaiHinh,      
    NamDangKy,      
    MaHaiQuan      
   FROM t_KDT_SXXK_BKToKhaiXuat      
   WHERE      
    MaHaiQuan = @MaHaiQuan) D ON      
  A.SoToKhai=D.SoToKhai AND      
  A.MaLoaiHinh=D.MaLoaiHinh AND      
  A.NamDangKy=D.NamDangKy AND      
  A.MaHaiQuan=D.MaHaiQuan      
WHERE      
 A.MaDoanhNghiep = @MaDoanhNghiep AND      
 A.MaHaiQuan = @MaHaiQuan AND      
 (A.MaLoaiHinh LIKE 'XSX%' OR A.MaLoaiHinh LIKE 'XGC%')      
 AND A.NgayDangKy between @TuNgay and @DenNgay      
 AND (A.NGAY_THN_THX IS NOT NULL OR year(A.NGAY_THN_THX)!=1900 )      
 AND D.SoToKhai IS NULL       
 AND A.TrangThaiXuLy = 1    
       
ORDER BY A.NgayDangKy , A.SoToKhai, A.MaLoaiHinh      
*/      
      
      
      
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='2.0', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('2.0', getdate(), null)
	end      
      
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
