﻿using System;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.VNACCS;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
using System.IO;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;
#elif GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
using System.IO;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;
#elif KD_V4
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
using System.IO;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.Interface.KDT.GC;
#endif
namespace Company.Interface.VNACCS
{
    public partial class ContainerBSForm : BaseFormHaveGuidPanel
    {
        public KDT_ContainerDangKy ContDK = new KDT_ContainerDangKy();
        private KDT_ContainerBS cont = new KDT_ContainerBS();
        private FeedBackContent feedbackContent = null;
        private Container_VNACCS feedbackContKVGS = null;
        private string msgInfor = string.Empty;
        bool isAdd = true;
        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public String Caption;
        public bool IsChange;

        public ContainerBSForm()
        {
            InitializeComponent();

        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (ContDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || ContDK.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || ContDK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    Save();
                    break;
                case "cmdSend":
                    SendV5(false);
                    //SendV5(true);
                    break;
                case "cmdFeedBack":
                    FeedBackV5(false);
                    //FeedBackV5(true);
                    break;
                case "cmdKetQuaXuLy":
                    KetQuaXuLyPK();
                    break;
                case "cmdKhaiKVGS":
                    //Không khai báo
                    //SendContKVGS();
                    //SendV5(false);
                    break;
                case "cmdXacNhanContQuaKVGS":
                    //Hỏi KVGS
                    Hoi_ContKVGS();
                    //SendContKVGS();
                    break;
                case "cmdNhanPhanHoiKVGS":
                    //SendContKVGS();
                    FeedBackV5(true);
                    break;
                case "cmdInBangKe":
                    Mau29();
                    break;
                //case "cmdMau30":
                //    Mau30();
                //    break;
                case "cmdMau31":
                    Mau31();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
            }

        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = ContDK.ID;
                f.loaiKhaiBao = LoaiKhaiBao.Container;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", ContDK.ID, LoaiKhaiBao.Container), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageBSContainer(ContDK);
                    ContDK.InsertUpdate();
                    ShowMessage("Cập nhật thông tin thành công .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_ContainerDangKy", "", Convert.ToInt32(ContDK.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void Save()
        {
            try
            {
                ContDK.MaHQ = ctrHaiQuan.Code;

                //if (TKMD.SoToKhai.ToString().Substring(1,1)=="3") 
                //{
                try
                {
                    ContDK.SoTiepNhan = long.Parse(txtSoTiepNhan.Text);
                }
                catch (Exception ex)
                {
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi lưu container liệu phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                }

                //}
                ContDK.InsertUpdateFull();
                ShowMessageTQDT(" Thông báo từ hệ thống ", "Lưu thành công.", false);
                this.SetChange(false);
            }
            catch (Exception ex)
            {
                //ShowMessage("Lỗi : " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void LoadData()
        {
            try
            {
                dgList.DataSource = ContDK.ListCont;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private bool ValidateFormContainer(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoVanDon, errorProvider, "SỐ VẬN ĐƠN", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoCont, errorProvider, "SỐ CONTAINER", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoSeal, errorProvider, "SỐ SEAL", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateFormContainer(false))
                    return;
                cont.SoContainer = txtSoCont.Text;
                cont.SoVanDon = txtSoVanDon.Text;
                cont.SoSeal = txtSoSeal.Text;
                cont.GhiChu = txtGhiChu.Text;
                if (isAdd)
                {
                    foreach (KDT_ContainerBS item in ContDK.ListCont)
                    {
                        if (item.SoContainer == txtSoCont.Text.ToString().Trim())
                        {
                            errorProvider.SetError(txtSoCont, "SỐ CONTAINER : ''" + txtSoCont.Text.ToString() + "'' NÀY ĐÃ CÓ TRONG DANH SÁCH PHÍA DƯỚI.");
                            return;
                        }
                    }
                }
                if (isAdd)
                    ContDK.ListCont.Add(cont);
                isAdd = true;
                cont = new KDT_ContainerBS();
                LoadData();

                this.SetChange(true);

                txtSoCont.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoCont.Text = String.Empty;
                txtSoCont.TextChanged += new EventHandler(txt_TextChanged);

                txtSoSeal.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoSeal.Text = String.Empty;
                txtSoSeal.TextChanged += new EventHandler(txt_TextChanged);

                txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChu.Text = String.Empty;
                txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<KDT_ContainerBS> ItemColl = new List<KDT_ContainerBS>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", "DOANH NGHIỆP CÓ MUỐN XÓA CONTAINER NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_ContainerBS)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_ContainerBS item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        ContDK.ListCont.Remove(item);
                    }
                    this.SetChange(true);
                }
                LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void SetCommandStatus()
        {
            if (ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || ContDK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;
                btnIn.Enabled = false;

                cmdKetQuaXuLy.Enabled = cmdKetQuaXuLy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdInBangKe1.Enabled = cmdInBangKe1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdMau31.Enabled = cmdMau311.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = ContDK.SoTiepNhan.ToString();
                lblTrangThai.Text = ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET ? setText("Đã duyệt", "Approved") : setText("Chờ duyệt", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (ContDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || ContDK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {

                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdInBangKe1.Enabled = cmdInBangKe1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdMau31.Enabled = cmdMau311.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKetQuaXuLy.Enabled = cmdKetQuaXuLy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = true;
                btnIn.Enabled = false;

                lblTrangThai.Text = ContDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (ContDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_CO_PHAN_HOI_QUA_KVGS)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdInBangKe1.Enabled = cmdInBangKe1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdMau31.Enabled = cmdMau311.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = false;
                btnIn.Enabled = false;

                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKetQuaXuLy.Enabled = cmdKetQuaXuLy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Chưa có phản hồi qua KVGS", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_CO_PHAN_HOI_QUA_KVGS)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdInBangKe1.Enabled = cmdInBangKe1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdMau31.Enabled = cmdMau311.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;
                btnIn.Enabled = false;

                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKetQuaXuLy.Enabled = cmdKetQuaXuLy.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã có phản hồi qua KVGS", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_CO_DANH_SACH_HANG_QUA_KVGS)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;

                btnIn.Enabled = true;
                cmdKetQuaXuLy.Enabled = cmdKetQuaXuLy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdInBangKe1.Enabled = cmdInBangKe1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdMau31.Enabled = cmdMau311.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                txtSoTiepNhan.Text = ContDK.SoTiepNhan.ToString();
                lblTrangThai.Text = setText("Đã có danh sách hàng qua KVGS", "Not declared");
                this.OpenType = OpenFormType.View;
            }

        }
        private void ContainerBSForm_Load(object sender, EventArgs e)
        {
            try
            {
                Caption = this.Text;
                txtSoTiepNhan.Text = ContDK.SoTiepNhan.ToString();
                clcNgayTN.Value = ContDK.NgayTiepNhan;
                //ctrHaiQuan.Code = ContDK.MaHQ;
                ctrHaiQuan.Code = TKMD.LoadToKhai(ContDK.TKMD_ID).CoQuanHaiQuan;
                if (ContDK.ID == 0)
                    ContDK.TrangThaiXuLy = -1;
                if (ContDK.TrangThaiXuLy == -1)
                    lblTrangThai.Text = "Chưa khai báo";
                else if (ContDK.TrangThaiXuLy == 0)
                    lblTrangThai.Text = "Chờ duyệt";
                else if (ContDK.TrangThaiXuLy == 1)
                    lblTrangThai.Text = "Đã duyệt";
                else if (ContDK.TrangThaiXuLy == 2)
                    lblTrangThai.Text = "Không phê duyệt";
                else if (ContDK.TrangThaiXuLy == 6)
                    lblTrangThai.Text = "Đã có danh sách hàng qua KVGS";
                else if (ContDK.TrangThaiXuLy == 7)
                    lblTrangThai.Text = "Chưa có phản hồi qua KVGS";
                else if (ContDK.TrangThaiXuLy == 8)
                    lblTrangThai.Text = "Đã có phản hồi qua KVGS";
                LoadData();
                TKMD.VanDonCollection = KDT_VNACC_TK_SoVanDon.SelectCollectionDynamic("TKMD_ID =" + ContDK.TKMD_ID, "");
                if (TKMD.VanDonCollection.Count > 0)
                {
                    txtSoVanDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoVanDon.Text = TKMD.VanDonCollection[0].SoDinhDanh;
                    txtSoVanDon.TextChanged += new EventHandler(txt_TextChanged);
                }
                if (TKMD.SoLuongCont > 0 || TKMD.MaPhuongThucVT == "2")
                    SetCommandStatus();
                if (TKMD.SoLuongCont == 0)
                {
                    this.Text = "Lấy phản hồi danh sách hàng hóa đủ diều kiện qua KVGS";
                    cmdSend.Visible = cmdSend1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Visible = cmdFeedBack1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanPhanHoiKVGS.Text = cmdNhanPhanHoiKVGS1.Text = "Nhận phản hồi";
                    cmdXacNhanContQuaKVGS.Text = cmdXacNhanContQuaKVGS1.Text = "Khai báo";
                    cmdMau29.Visible = cmdMau31.Visible = cmdMau311.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    txtSoVanDon.Enabled = txtSoCont.Enabled = txtSoSeal.Enabled = txtGhiChu.Enabled = false;
                    btnAddExcel.Enabled = btnLuu.Enabled = btnXoa.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.BSContainerSendHandler(ContDK, ref msgInfor, e);
        }
        void SendKVGSHandler(object sender, SendEventArgs e)
        {
            feedbackContKVGS = SingleMessage.ContainerVnaccsSendHandler(ContDK, ref msgInfor, e);
        }
        void SendMessageKVGS(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendKVGSHandler),
                sender, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendContKVGS()
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", "DOANH NGHIỆP CÓ MUỐN KHAI BÁO PHỤ KIỆN NÀY ĐẾN HẢI QUAN?", true) == "Yes")
            {
                if (ContDK.ID == 0)
                {
                    this.ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    ContDK = KDT_ContainerDangKy.LoadFull(ContDK.ID);
                }
                MsgSend sendXML = new MsgSend();

                sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;

                sendXML.master_id = ContDK.ID;
                if (sendXML.Load())
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    ContDK.GuidStr = Guid.NewGuid().ToString();
                    //Container_VNACCS contVNACCS = new Container_VNACCS();
                    //contVNACCS = Mapper_V4.ToDataTransferContainer(ContDK, TKMD, GlobalSettings.TEN_DON_VI, xacnhanKVGS);


                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = ContDK.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ)),
                              // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(ContDK.MaHQ).Trim() : ContDK.MaHQ
                              Identity = ContDK.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = DeclarationIssuer.Container_KVGS,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = ContDK.GuidStr,
                          },
                          null
                        );

                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(ContDK.MaHQ).Trim() : ContDK.MaHQ
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;

                    ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessageKVGS;
                    bool isSend = sendForm.DoSend(msgSend);
                    //if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    if (isSend)//&& feedbackContKVGS.Function == DeclarationFunction.THONG_QUAN)
                    {
                        sendForm.Message.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoBSContainer);
                        cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        btnLuu.Enabled = false;
                        btnXoa.Enabled = false;

                        sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;

                        sendXML.master_id = ContDK.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(msgSend.Subject.Function);
                        sendXML.InsertUpdate();

                        ContDK.Update();

                        Hoi_ContKVGS();

                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        private void SendV5(bool xacnhanKVGS)
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", "DOANH NGHIỆP CÓ MUỐN KHAI BÁO PHỤ KIỆN NÀY ĐẾN HẢI QUAN?", true) == "Yes")
            {
                if (ContDK.ID == 0)
                {
                    this.ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    ContDK = KDT_ContainerDangKy.LoadFull(ContDK.ID);
                }
                MsgSend sendXML = new MsgSend();

                sendXML.LoaiHS = LoaiKhaiBao.Container;

                sendXML.master_id = ContDK.ID;
                if (sendXML.Load())
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    if (this.ContDK.ListCont.Count > 0)
                    {

                        string returnMessage = string.Empty;
                        ContDK.GuidStr = Guid.NewGuid().ToString();
                        Container_VNACCS contVNACCS = new Container_VNACCS();
                        contVNACCS = Mapper_V4.ToDataTransferContainer(ContDK, TKMD, GlobalSettings.TEN_DON_VI, xacnhanKVGS);

                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = ContDK.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ)),
                                  // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(ContDK.MaHQ).Trim() : ContDK.MaHQ
                                  Identity = ContDK.MaHQ
                              },
                              new SubjectBase()
                              {

                                  Type = contVNACCS.Issuer,
                                  Function = DeclarationFunction.KHAI_BAO,
                                  Reference = ContDK.GuidStr,
                              },
                              contVNACCS
                            );
                        ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoBSContainer);
                            cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            btnLuu.Enabled = false;
                            btnXoa.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.Container;
                            sendXML.master_id = ContDK.ID;
                            sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                            sendXML.func = Convert.ToInt32(contVNACCS.Function);
                            sendXML.InsertUpdate();
                            if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                            {
                                txtSoTiepNhan.Text = ContDK.SoTiepNhan.ToString();
                                clcNgayTN.Value = ContDK.NgayTiepNhan;
                                lblTrangThai.Text = "Đã duyệt";
                                ContDK.Update();
                                SetCommandStatus();
                            }
                            else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                            {
                                FeedBackV5(false);
                                SetCommandStatus();
                            }
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                        {
                            sendForm.Message.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoBSContainer);
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatus();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public static Container_VNACCS ToDataTransferContainerKVGS(KDT_ContainerDangKy ContDK, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep)
        {
            string reference = string.Empty;
            if (string.IsNullOrEmpty(ContDK.GuidStr))
            {
                reference = Guid.NewGuid().ToString();
            }
            else
            {
                reference = ContDK.GuidStr;
            }
            Container_VNACCS Cont = new Container_VNACCS()
            {
                Issuer = DeclarationIssuer.Container_KVGS,
                Reference = reference,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = "",
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = TKMD.SoToKhai.ToString(),
                Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime),
                DeclarationOffice = TKMD.CoQuanHaiQuan,
                NatureOfTransaction = TKMD.MaLoaiHinh,
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Identity = ContDK.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                }

            };
            Cont.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = ContDK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            return Cont;
        }
        private void FeedBackV5(bool KVGS)
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = KVGS == true ? LoaiKhaiBao.ContainerKVGS : LoaiKhaiBao.Container;
            sendXML.master_id = ContDK.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = ContDK.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.Container_KVGS,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.Container_KVGS,
                };
                if (KVGS)
                    subjectBase.Type = DeclarationIssuer.Container_KVGS;
                else
                    subjectBase.Type = DeclarationIssuer.Container;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = ContDK.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ.Trim())),
                                                  Identity = ContDK.MaHQ
                                              }, subjectBase, null);
                if (ContDK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                if (KVGS)
                {
                    sendForm.Send += SendMessageKVGS;
                }
                else
                {
                    sendForm.Send += SendMessage;
                }
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (KVGS)
                    {
                        if (feedbackContKVGS.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContKVGS.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatus();
                        }
                        else if (feedbackContKVGS.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                    }
                    else
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            txtSoTiepNhan.Text = ContDK.SoTiepNhan.ToString();
                            clcNgayTN.Value = ContDK.NgayTiepNhan;
                            lblTrangThai.Text = "Đã duyệt";
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatus();
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                            SetCommandStatus();
                        }
                    }
                }
            }
        }
        private void Hoi_ContKVGS()
        {
            #region Hỏi phải hổi KVGS
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {
                if (ContDK.ID == 0)
                {
                    ContDK = new KDT_ContainerDangKy();
                    ContDK.MaHQ = TKMD.CoQuanHaiQuan;
                    ContDK.TKMD_ID = TKMD.ID;
                    ContDK.MaDoanhNghiep = TKMD.MaDonVi;
                    ContDK.GuidStr = Guid.NewGuid().ToString();
                    ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_CO_PHAN_HOI_QUA_KVGS;
                    foreach (KDT_VNACC_TK_SoContainer item in TKMD.ContainerTKMD.SoContainerCollection)
                    {
                        KDT_ContainerBS cont = new KDT_ContainerBS();
                        cont.SoContainer = item.SoContainer;
                        ContDK.ListCont.Add(cont);
                    }
                    ContDK.InsertUpdateFull();
                }
                else
                {
                    ContDK = KDT_ContainerDangKy.LoadFull(ContDK.ID);
                    if (ContDK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        ContDK.GuidStr = Guid.NewGuid().ToString();
                    }
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;
                sendXML.master_id = ContDK.ID;
                if (sendXML.Load())
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {

                    string returnMessage = string.Empty;

                    string reference = ContDK.GuidStr;

                    Container_VNACCS contVNACCS = new Container_VNACCS();
                    contVNACCS = ToDataTransferContainerKVGS(ContDK, TKMD, GlobalSettings.TEN_DON_VI);
                    SubjectBase subjectBase = new SubjectBase()
                    {
                        Issuer = DeclarationIssuer.Container_KVGS,
                        Reference = reference,
                        Function = DeclarationFunction.KHAI_BAO,
                        Type = DeclarationIssuer.Container_KVGS,
                    };
                    ObjectSend msgSend = new ObjectSend(
                                                new NameBase()
                                                {
                                                    Name = GlobalSettings.TEN_DON_VI,
                                                    Identity = ContDK.MaDoanhNghiep,
                                                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                                                },
                                                  new NameBase()
                                                  {
                                                      Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ.Trim())),
                                                      Identity = ContDK.MaHQ
                                                  }, subjectBase, contVNACCS);
                    ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_CO_PHAN_HOI_QUA_KVGS;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessageKVGS;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContKVGS.Function != DeclarationFunction.KHONG_CHAP_NHAN)// && feedbackContKVGS.Function == "32")
                    {
                        sendForm.Message.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoBSContainer);
                        cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        btnLuu.Enabled = false;
                        btnXoa.Enabled = false;
                        sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;
                        sendXML.master_id = ContDK.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        ContDK.Update();
                        if (feedbackContKVGS.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedBackV5(true);
                            SetCommandStatus();
                        }
                        else if (feedbackContKVGS.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            ContDK.Update();
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatus();
                        }
                        else if (feedbackContKVGS.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isSend = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
            #endregion
        }
        private void KetQuaXuLyPK()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = this.ContDK.ID;
                form.DeclarationIssuer = DeclarationIssuer.Container;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    cont = new KDT_ContainerBS();
                    cont = (KDT_ContainerBS)e.Row.DataRow;

                    txtSoCont.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoCont.Text = cont.SoContainer;
                    txtSoCont.TextChanged += new EventHandler(txt_TextChanged);

                    txtSoVanDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoVanDon.Text = cont.SoVanDon;
                    txtSoVanDon.TextChanged += new EventHandler(txt_TextChanged);

                    txtSoSeal.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoSeal.Text = cont.SoSeal;
                    txtSoSeal.TextChanged += new EventHandler(txt_TextChanged);

                    txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                    txtGhiChu.Text = cont.GhiChu;
                    txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void Mau31()
        {
            try
            {
                Company.Interface.Report.VNACCS.BangKeSoContainerXuatKhau_M31_TT38 bangkeCont = new Company.Interface.Report.VNACCS.BangKeSoContainerXuatKhau_M31_TT38();
                bangkeCont.BindReport(ContDK);
                bangkeCont.ShowRibbonPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void Mau29()
        {
            try
            {
                Company.Interface.Report.VNACCS.BangKeSoContainerQuaKVGS_M29_TT38 bangkeCont = new Company.Interface.Report.VNACCS.BangKeSoContainerQuaKVGS_M29_TT38();
                bangkeCont.BindReport(ContDK);
                bangkeCont.ShowRibbonPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        //private void Mau30()
        //{
        //    Company.Interface.Report.VNACCS.BangKeHangHoaQuaXNKVGS_M30_TT38 bangkeCont = new Company.Interface.Report.VNACCS.BangKeHangHoaQuaXNKVGS_M30_TT38();
        //    bangkeCont.BindReport(ContDK);
        //    bangkeCont.ShowRibbonPreview();
        //}
        private void InBangKeContainer()
        {
            //if (txtSoTiepNhan.Text == "0")
            //{
            //    Company.Interface.Report.VNACCS.BangKeSoContainerTKXuat bangkeCont = new Company.Interface.Report.VNACCS.BangKeSoContainerTKXuat();
            //    bangkeCont.BindReport(ContDK);
            //    bangkeCont.ShowRibbonPreview();
            //}
            //else 
            //{
            //    Company.Interface.Report.VNACCS.BangKeSoContainerQuaKVGS bangkeCont = new Company.Interface.Report.VNACCS.BangKeSoContainerQuaKVGS();
            //    bangkeCont.BindReport(ContDK);
            //    bangkeCont.ShowRibbonPreview();
            //}

        }



        private void linkQRCode_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://pus.customs.gov.vn/faces/ContainerBarcode");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnAddExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ReadExcelContainerForm f = new ReadExcelContainerForm();
                f.ContDK = ContDK;
                f.Type = "TKMD";
                f.ShowDialog(this);
                this.SetChange(f.ImportSuccess);
                LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnIn_Click(object sender, EventArgs e)
        {
            if (TKMD.MaPhuongThucVT == "2")
            {
                Company.Interface.Report.VNACCS.BangKeSoContainerQuaKVGS bangkeCont = new Company.Interface.Report.VNACCS.BangKeSoContainerQuaKVGS();
                bangkeCont.TKMD = TKMD;
                bangkeCont.BindReport(ContDK);
                bangkeCont.ShowRibbonPreview();
            }
            else
            {
                Company.Interface.Report.VNACCS.BangKeHangRoiQuaKVGS bangkeHangRoi = new Company.Interface.Report.VNACCS.BangKeHangRoiQuaKVGS();
                bangkeHangRoi.TKMD = TKMD;
                bangkeHangRoi.BindReport(ContDK);
                bangkeHangRoi.ShowRibbonPreview();
            }
        }

        private void ContainerBSForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Khai báo Bổ sung Container có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.Save();
                }
            }
        }
    }
}
