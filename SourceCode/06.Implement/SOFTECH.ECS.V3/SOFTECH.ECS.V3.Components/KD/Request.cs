﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using SOFTECH.ECS.V3.Components.KD;
using System.IO;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;
// SiHV
namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class Request
    {
        public static string ConfigRequest(int procedureType, string xmlString)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/KD/Request.xml");
                XElement Root = docXML.Root;
                if (Root != null && Root.Elements().Count() > 0)
                {
                    foreach (XElement element in Root.Elements())
                    {
                        //CurrencyExchange
                        if (element.Name.ToString().ToUpper().Equals(NodeRequest.procedureType.ToUpper()))
                            element.SetValue(procedureType);       // loại thủ tục áp dụng	1	n1	áp dụng cho thủ tục HQĐT, thủ công (1 - truyền thống, 2 - điện tử,...)
                        else if (element.Name.ToString().ToUpper().Equals(NodeRequest.procedureType.ToUpper()))
                        {
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.version.ToUpper()))
                                    child.SetValue("");            // phiên bản message	1	an..50	
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.messageId.ToUpper()))
                                    child.SetValue("");            // định danh message	1	an35
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeRequest.SendApplication.ToUpper()))
                        {
                            // Ứng dụng gửi phía doanh nghiệp
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.name.ToUpper()))
                                    child.SetValue("");            // Tên phần mềm		an..255	
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.version.ToUpper()))
                                    child.SetValue("");            // Phiên bản phần mềm		none
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.companyName.ToUpper()))
                                    child.SetValue("");            // Tên công ty	1	an..255	
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.companyIdentity.ToUpper()))
                                    child.SetValue("");            // Mã công ty	1	an50
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.createMessageIssue.ToUpper()))
                                    child.SetValue("");            // Ngày giờ biên soạn message		an19	YYYY-MM-DD HH:mm:ss
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.Signature.ToUpper()))
                                {
                                    foreach (XElement childSignature in child.Elements())
                                    {
                                        if (childSignature.Name.ToString().ToUpper().Equals(NodeRequest.data.ToUpper()))
                                            childSignature.SetValue(""); // Nội dung chữ ký số
                                        if (childSignature.Name.ToString().ToUpper().Equals(NodeRequest.fileCert.ToUpper()))
                                            childSignature.SetValue(""); // Nội dung file cert
                                    }
                                }
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeRequest.From.ToUpper()))
                        {
                            // Ứng dụng gửi phía doanh nghiệp
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.name.ToUpper()))
                                    child.SetValue("");            // Tên người gửi	1	an..255	Tên doanh nghiệp/hải quan gửi	
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.identity.ToUpper()))
                                    child.SetValue("");            // Mã người gửi	1	an50	mã doanh nghiệp/hải quan gửi
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeRequest.To.ToUpper()))
                        {
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.name.ToUpper()))
                                    child.SetValue("");            // Tên người nhận	1	an..255	Tên doanh nghiệp/hải quan nhận	
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.identity.ToUpper()))
                                    child.SetValue("");            // Mã người nhận	1	an50	mã doanh nghiệp/hải quan nhận
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeRequest.VAN.ToUpper()))
                        {
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.name.ToUpper()))
                                    child.SetValue("");            // Tên đơn vị VAN	1	an..255	Nếu có sử dụng dịch vụ VAN
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.identity.ToUpper()))
                                    child.SetValue("");            // Mã đơn vị VAN	1	an50	Nếu có sử dụng dịch vụ VAN
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeRequest.Subject.ToUpper()))
                        {
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.type.ToUpper()))
                                    child.SetValue("");            // Loại message	1	n3	vd: tờ khai, C/O…
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.function.ToUpper()))
                                    child.SetValue("");            // chức năng message	1	n..3	vd: khai mới, khai sửa…
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.reference.ToUpper()))
                                    child.SetValue("");            // Số tham chiếu	1	an35	Doanh nghiệp cấp và tự quản lý
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeRequest.Body.ToUpper()))
                        {
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.Content.ToUpper()))
                                {
                                    XElement xmlElement = XElement.Load(new StringReader(xmlString));
                                    if (xmlElement != null)
                                    {
                                        child.Add(xmlElement);
                                    }
                                }
                                if (child.Name.ToString().ToUpper().Equals(NodeRequest.Signature.ToUpper()))
                                {
                                    foreach (XElement childSignature in child.Elements())
                                    {
                                        if (childSignature.Name.ToString().ToUpper().Equals(NodeRequest.data.ToUpper()))
                                            childSignature.SetValue(""); // Nội dung chữ ký số
                                        if (childSignature.Name.ToString().ToUpper().Equals(NodeRequest.fileCert.ToUpper()))
                                            childSignature.SetValue(""); // Chứa nội dung chứng thư	
                                    }
                                }

                            }
                        }
                    }
                }
                return docXML.ToString();
            }
            catch { return ""; }
        }

    }
}
