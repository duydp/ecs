﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;

namespace Company.BLL.SXXK.ToKhai
{
    public partial class ThongTinDieuChinh
    {
        public static void updateDatabase(DataSet ds, SqlTransaction trangsaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ThongTinDieuChinh tt = new ThongTinDieuChinh();
                tt.LanDieuChinh = Convert.ToInt32(row["LAN_DC"].ToString());
                tt.MaHaiQuan = (row["MA_HQ"].ToString());
                tt.MaLoaiHinh = (row["MA_LH"].ToString());
                tt.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                if(row["NGAY_HL"].ToString()!="")
                    tt.NGAY_HL = Convert.ToDateTime(row["NGAY_HL"].ToString());
                if (row["SNAHAN"].ToString() != "")
                    tt.SNAHAN = Convert.ToInt32(row["SNAHAN"].ToString());
                if (row["SO_CT"].ToString() != "")
                    tt.SoChungTu = (row["SO_CT"].ToString());
                tt.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                tt.THANH_LY = Convert.ToInt32(row["THANH_LY"].ToString());
                tt.InsertUpdateTransaction(trangsaction);
            }
        }
        public static void updateDatabaseMoi(DataSet ds, SqlTransaction trangsaction,DataSet dsToKhaiCu)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();            
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ThongTinDieuChinh tt = new ThongTinDieuChinh();
                tt.LanDieuChinh = Convert.ToInt32(row["LAN_DC"].ToString());
                tt.MaHaiQuan = (row["MA_HQ"].ToString());
                tt.MaLoaiHinh = (row["MA_LH"].ToString());
                tt.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                if (row["NGAY_HL"].ToString() != "")
                    tt.NGAY_HL = Convert.ToDateTime(row["NGAY_HL"].ToString());
                if (row["SNAHAN"].ToString() != "")
                    tt.SNAHAN = Convert.ToInt32(row["SNAHAN"].ToString());
                if (row["SO_CT"].ToString() != "")
                    tt.SoChungTu = (row["SO_CT"].ToString());
                tt.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                tt.THANH_LY = Convert.ToInt32(row["THANH_LY"].ToString());               
                try
                {
                    DataRow[] rowTTDM = dsToKhaiCu.Tables[0].Select("MaLoaiHinh='" + tt.MaLoaiHinh + "' and NamDangKy=" + tt.NamDangKy + " and MaHaiQuan='" + tt.MaHaiQuan + "' and  SoToKhai=" + tt.SoToKhai);
                    if (rowTTDM == null || rowTTDM.Length == 0)
                    tt.InsertTransaction(trangsaction);
                }
                catch { }
            }
        }
 
        public static void WSGetDanhSachDieuChinh(string maHaiQuan,string maDoanhNghiep,DataSet ds,DataSet dsHangDC)
        {
           
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    ThongTinDieuChinh.updateDatabase(ds, transaction);
                    HangMauDichDieuChinh.updateDatabase(dsHangDC, transaction);
                    //HangMauDichDieuChinhCT.updateDatabase(dsHangDCCT, transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public static void WSGetDanhSachDieuChinhMoi(string maHaiQuan, string maDoanhNghiep, DataSet ds, DataSet dsHangDC)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            DataSet dsToKhaiCu = tkmd.SelectDynamic("madoanhnghiep='" + maDoanhNghiep + "' and mahaiquan='" + maHaiQuan + "'", "");
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    ThongTinDieuChinh.updateDatabaseMoi(ds, transaction,dsToKhaiCu);
                    HangMauDichDieuChinh.updateDatabaseMoi(dsHangDC, transaction,dsToKhaiCu);
                    //HangMauDichDieuChinhCT.updateDatabase(dsHangDCCT, transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        
    }
}
