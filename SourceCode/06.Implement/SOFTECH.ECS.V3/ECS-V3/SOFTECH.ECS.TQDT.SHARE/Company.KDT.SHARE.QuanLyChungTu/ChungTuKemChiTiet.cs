using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class ChungTuKemChiTiet
    {
        public static List<ChungTuKem> ListCTKem()
        {
            List<ChungTuKem> temp = new List<ChungTuKem>();

            return temp;
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_ChungTuKemID(long chungTuKemID, SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "[dbo].[p_ChungTuKemChiTiet_DeleteBy_ChungTuKemID]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, chungTuKemID);

            return db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_ChungTuKemChiTiet_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, ChungTuKemID);
            db.AddInParameter(dbCommand, "@FileName", SqlDbType.VarChar, FileName);
            db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

    }
}