﻿namespace Company.Interface
{
    partial class VNACC_ToKhaiMauDichXuatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grListTyGia_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ToKhaiMauDichXuatForm));
            this.grbDonVi = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoDienThoaiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNhomXuLyHS = new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy();
            this.lblPhanLuong = new System.Windows.Forms.Label();
            this.ctrCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.clcNgayDangKy = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcThoiHanTaiNhap = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label62 = new System.Windows.Forms.Label();
            this.ctrMaPhuongThucVT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiHH = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaLoaiHinh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoToKhaiDauTien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongSoTKChiaNho = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoNhanhToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoToKhaiTNTX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaSoThueDaiDien = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.grbDoiTac = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaNuocDoiTac = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtDiaChiDoiTac2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDiaChiDoiTac4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTenDaiLyHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDaiLyHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaXDThoiHanNopThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtKyHieuCTBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtSoCTBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamPhatHanhBL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtMaNHBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label38 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDDLuuKho = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDiaDiemXepHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ctrDiaDiemNhanHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.clcNgayHangDen = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.txtSoHieuKyHieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaPTVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenPTVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNguoiNopThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtKyHieuCTHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtSoCTHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNanPhatHanhHM = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtMaNHTraThueThay = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaTTTriGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtPhanLoaiKhongQDVND = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtTongHeSoPhanBoTG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.lblMaPhanLoaiTongGiaCoBan = new System.Windows.Forms.Label();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDieuKienGiaHD = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrPhuongThucTT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrPhanLoaiGiaHD = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.clcNgayPhatHanhHD = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ctrPhanLoaiHD = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTHoaDon = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtSoTiepNhanHD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtTongTriGiaHD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDVTTrongLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox14 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDiaDiemDichVC = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.clcNgayDen = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayKhoiHanhVC = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label59 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDVTSoLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoQuanLyNoiBoDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox15 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtPhanLoaiNopThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrMaTTCuaSoTienBaoLanh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTTongTienThueXuatKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.uiGroupBox17 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListTyGia = new Janus.Windows.GridEX.GridEX();
            this.txtTongSoDongHangCuaToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongSoTrangCuaToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtSoTienBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.txtTongSoTienLePhi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.txtTongSoTienThueXuatKhau = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdVanDon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmDinhKemDT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDinhKemDT");
            this.cmdTrungChuyen1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTrungChuyen");
            this.cmdContainer1 = new Janus.Windows.UI.CommandBars.UICommand("cmdContainer");
            this.cmdChiThiHQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHQ");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdIN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdIN");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKetQuaTraVe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaTraVe");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdChiThiHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHQ");
            this.cmdGiayPhep = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoi");
            this.cmdDinhKemDT = new Janus.Windows.UI.CommandBars.UICommand("cmdDinhKemDT");
            this.cmdTrungChuyen = new Janus.Windows.UI.CommandBars.UICommand("cmdTrungChuyen");
            this.cmdContainer = new Janus.Windows.UI.CommandBars.UICommand("cmdContainer");
            this.cmdIN = new Janus.Windows.UI.CommandBars.UICommand("cmdIN");
            this.cmdKetQuaTraVe2 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaTraVe");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.clcThoiHanTaiNhapTaiXuat = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).BeginInit();
            this.grbDonVi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).BeginInit();
            this.grbDoiTac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).BeginInit();
            this.uiGroupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).BeginInit();
            this.uiGroupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).BeginInit();
            this.uiGroupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListTyGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 709), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 709);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 685);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 685);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(910, 709);
            // 
            // grbDonVi
            // 
            this.grbDonVi.BackColor = System.Drawing.Color.Transparent;
            this.grbDonVi.Controls.Add(this.txtDiaChiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaDonVi);
            this.grbDonVi.Controls.Add(this.label3);
            this.grbDonVi.Controls.Add(this.txtSoDienThoaiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaBuuChinhDonVi);
            this.grbDonVi.Controls.Add(this.label5);
            this.grbDonVi.Controls.Add(this.txtTenDonVi);
            this.grbDonVi.Controls.Add(this.label4);
            this.grbDonVi.Controls.Add(this.label1);
            this.grbDonVi.Controls.Add(this.label2);
            this.grbDonVi.Location = new System.Drawing.Point(3, 126);
            this.grbDonVi.Name = "grbDonVi";
            this.grbDonVi.Size = new System.Drawing.Size(286, 156);
            this.grbDonVi.TabIndex = 1;
            this.grbDonVi.Text = "Người xuất khẩu";
            this.grbDonVi.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiDonVi
            // 
            this.txtDiaChiDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDonVi.Location = new System.Drawing.Point(57, 86);
            this.txtDiaChiDonVi.MaxLength = 100;
            this.txtDiaChiDonVi.Multiline = true;
            this.txtDiaChiDonVi.Name = "txtDiaChiDonVi";
            this.txtDiaChiDonVi.Size = new System.Drawing.Size(223, 40);
            this.txtDiaChiDonVi.TabIndex = 2;
            this.txtDiaChiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(57, 17);
            this.txtMaDonVi.MaxLength = 13;
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.Size = new System.Drawing.Size(223, 21);
            this.txtMaDonVi.TabIndex = 0;
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Địa chỉ";
            // 
            // txtSoDienThoaiDonVi
            // 
            this.txtSoDienThoaiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDienThoaiDonVi.Location = new System.Drawing.Point(185, 128);
            this.txtSoDienThoaiDonVi.MaxLength = 255;
            this.txtSoDienThoaiDonVi.Name = "txtSoDienThoaiDonVi";
            this.txtSoDienThoaiDonVi.Size = new System.Drawing.Size(95, 21);
            this.txtSoDienThoaiDonVi.TabIndex = 4;
            this.txtSoDienThoaiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDienThoaiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaBuuChinhDonVi
            // 
            this.txtMaBuuChinhDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDonVi.Location = new System.Drawing.Point(57, 129);
            this.txtMaBuuChinhDonVi.MaxLength = 255;
            this.txtMaBuuChinhDonVi.Name = "txtMaBuuChinhDonVi";
            this.txtMaBuuChinhDonVi.Size = new System.Drawing.Size(76, 21);
            this.txtMaBuuChinhDonVi.TabIndex = 3;
            this.txtMaBuuChinhDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(132, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Điện thoại";
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(57, 42);
            this.txtTenDonVi.MaxLength = 100;
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.Size = new System.Drawing.Size(223, 40);
            this.txtTenDonVi.TabIndex = 1;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDonVi.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Bưu chính";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Mã";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Tên";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtMaUyThac);
            this.uiGroupBox2.Controls.Add(this.txtTenUyThac);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 285);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(286, 87);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.Text = "Người ủy thác";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtMaUyThac
            // 
            this.txtMaUyThac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaUyThac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaUyThac.Location = new System.Drawing.Point(58, 15);
            this.txtMaUyThac.MaxLength = 13;
            this.txtMaUyThac.Name = "txtMaUyThac";
            this.txtMaUyThac.Size = new System.Drawing.Size(223, 21);
            this.txtMaUyThac.TabIndex = 0;
            this.txtMaUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenUyThac
            // 
            this.txtTenUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenUyThac.Location = new System.Drawing.Point(57, 41);
            this.txtTenUyThac.MaxLength = 100;
            this.txtTenUyThac.Multiline = true;
            this.txtTenUyThac.Name = "txtTenUyThac";
            this.txtTenUyThac.Size = new System.Drawing.Size(224, 40);
            this.txtTenUyThac.TabIndex = 1;
            this.txtTenUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 33;
            this.label12.Text = "Mã";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Tên";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.ctrNhomXuLyHS);
            this.uiGroupBox3.Controls.Add(this.lblPhanLuong);
            this.uiGroupBox3.Controls.Add(this.ctrCoQuanHaiQuan);
            this.uiGroupBox3.Controls.Add(this.clcNgayDangKy);
            this.uiGroupBox3.Controls.Add(this.clcThoiHanTaiNhap);
            this.uiGroupBox3.Controls.Add(this.label62);
            this.uiGroupBox3.Controls.Add(this.ctrMaPhuongThucVT);
            this.uiGroupBox3.Controls.Add(this.ctrMaPhanLoaiHH);
            this.uiGroupBox3.Controls.Add(this.ctrMaLoaiHinh);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhaiDauTien);
            this.uiGroupBox3.Controls.Add(this.txtTongSoTKChiaNho);
            this.uiGroupBox3.Controls.Add(this.txtSoNhanhToKhai);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhaiTNTX);
            this.uiGroupBox3.Controls.Add(this.txtMaSoThueDaiDien);
            this.uiGroupBox3.Controls.Add(this.label18);
            this.uiGroupBox3.Controls.Add(this.label28);
            this.uiGroupBox3.Controls.Add(this.label30);
            this.uiGroupBox3.Controls.Add(this.label29);
            this.uiGroupBox3.Controls.Add(this.label63);
            this.uiGroupBox3.Controls.Add(this.label27);
            this.uiGroupBox3.Controls.Add(this.label23);
            this.uiGroupBox3.Controls.Add(this.label26);
            this.uiGroupBox3.Controls.Add(this.label20);
            this.uiGroupBox3.Controls.Add(this.label25);
            this.uiGroupBox3.Controls.Add(this.label24);
            this.uiGroupBox3.Controls.Add(this.label19);
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(869, 117);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ctrNhomXuLyHS
            // 
            this.ctrNhomXuLyHS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A014;
            this.ctrNhomXuLyHS.Code = "";
            this.ctrNhomXuLyHS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNhomXuLyHS.CustomsCode = null;
            this.ctrNhomXuLyHS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrNhomXuLyHS.IsValidate = false;
            this.ctrNhomXuLyHS.Location = new System.Drawing.Point(735, 35);
            this.ctrNhomXuLyHS.Name = "ctrNhomXuLyHS";
            this.ctrNhomXuLyHS.Name_VN = "";
            this.ctrNhomXuLyHS.ShowColumnCode = true;
            this.ctrNhomXuLyHS.ShowColumnName = false;
            this.ctrNhomXuLyHS.Size = new System.Drawing.Size(128, 26);
            this.ctrNhomXuLyHS.TabIndex = 55;
            this.ctrNhomXuLyHS.TagName = "";
            this.ctrNhomXuLyHS.WhereCondition = "";
            // 
            // lblPhanLuong
            // 
            this.lblPhanLuong.AutoSize = true;
            this.lblPhanLuong.BackColor = System.Drawing.Color.Transparent;
            this.lblPhanLuong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLuong.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPhanLuong.Location = new System.Drawing.Point(100, 41);
            this.lblPhanLuong.Name = "lblPhanLuong";
            this.lblPhanLuong.Size = new System.Drawing.Size(15, 14);
            this.lblPhanLuong.TabIndex = 54;
            this.lblPhanLuong.Text = "  ";
            // 
            // ctrCoQuanHaiQuan
            // 
            this.ctrCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ctrCoQuanHaiQuan.Code = "";
            this.ctrCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHaiQuan.IsValidate = true;
            this.ctrCoQuanHaiQuan.Location = new System.Drawing.Point(310, 35);
            this.ctrCoQuanHaiQuan.Name = "ctrCoQuanHaiQuan";
            this.ctrCoQuanHaiQuan.Name_VN = "";
            this.ctrCoQuanHaiQuan.SetValidate = false;
            this.ctrCoQuanHaiQuan.ShowColumnCode = true;
            this.ctrCoQuanHaiQuan.ShowColumnName = true;
            this.ctrCoQuanHaiQuan.Size = new System.Drawing.Size(319, 26);
            this.ctrCoQuanHaiQuan.TabIndex = 53;
            this.ctrCoQuanHaiQuan.TagCode = "";
            this.ctrCoQuanHaiQuan.TagName = "";
            this.ctrCoQuanHaiQuan.WhereCondition = "";
            // 
            // clcNgayDangKy
            // 
            this.clcNgayDangKy.Location = new System.Drawing.Point(489, 12);
            this.clcNgayDangKy.Name = "clcNgayDangKy";
            this.clcNgayDangKy.ReadOnly = true;
            this.clcNgayDangKy.Size = new System.Drawing.Size(140, 21);
            this.clcNgayDangKy.TabIndex = 52;
            this.clcNgayDangKy.TagName = "";
            this.clcNgayDangKy.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // clcThoiHanTaiNhap
            // 
            this.clcThoiHanTaiNhap.Location = new System.Drawing.Point(330, 90);
            this.clcThoiHanTaiNhap.Name = "clcThoiHanTaiNhap";
            this.clcThoiHanTaiNhap.ReadOnly = false;
            this.clcThoiHanTaiNhap.Size = new System.Drawing.Size(89, 21);
            this.clcThoiHanTaiNhap.TabIndex = 52;
            this.clcThoiHanTaiNhap.TagName = "";
            this.clcThoiHanTaiNhap.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(3, 42);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(91, 13);
            this.label62.TabIndex = 42;
            this.label62.Text = "Phân loại kiểm tra";
            // 
            // ctrMaPhuongThucVT
            // 
            this.ctrMaPhuongThucVT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E005;
            this.ctrMaPhuongThucVT.Code = "";
            this.ctrMaPhuongThucVT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhuongThucVT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaPhuongThucVT.IsValidate = true;
            this.ctrMaPhuongThucVT.Location = new System.Drawing.Point(604, 87);
            this.ctrMaPhuongThucVT.Name = "ctrMaPhuongThucVT";
            this.ctrMaPhuongThucVT.Name_VN = "";
            this.ctrMaPhuongThucVT.SetValidate = false;
            this.ctrMaPhuongThucVT.ShowColumnCode = true;
            this.ctrMaPhuongThucVT.ShowColumnName = true;
            this.ctrMaPhuongThucVT.Size = new System.Drawing.Size(259, 26);
            this.ctrMaPhuongThucVT.TabIndex = 10;
            this.ctrMaPhuongThucVT.TagName = "";
            this.ctrMaPhuongThucVT.WhereCondition = "";
            this.ctrMaPhuongThucVT.Leave += new System.EventHandler(this.ctrMaPhuongThucVT_Leave);
            // 
            // ctrMaPhanLoaiHH
            // 
            this.ctrMaPhanLoaiHH.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E003;
            this.ctrMaPhanLoaiHH.Code = "";
            this.ctrMaPhanLoaiHH.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiHH.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaPhanLoaiHH.IsValidate = true;
            this.ctrMaPhanLoaiHH.Location = new System.Drawing.Point(538, 61);
            this.ctrMaPhanLoaiHH.Name = "ctrMaPhanLoaiHH";
            this.ctrMaPhanLoaiHH.Name_VN = "";
            this.ctrMaPhanLoaiHH.SetValidate = false;
            this.ctrMaPhanLoaiHH.ShowColumnCode = true;
            this.ctrMaPhanLoaiHH.ShowColumnName = true;
            this.ctrMaPhanLoaiHH.Size = new System.Drawing.Size(325, 26);
            this.ctrMaPhanLoaiHH.TabIndex = 6;
            this.ctrMaPhanLoaiHH.TagName = "";
            this.ctrMaPhanLoaiHH.WhereCondition = "";
            // 
            // ctrMaLoaiHinh
            // 
            this.ctrMaLoaiHinh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E002;
            this.ctrMaLoaiHinh.Code = "";
            this.ctrMaLoaiHinh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaLoaiHinh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaLoaiHinh.IsValidate = true;
            this.ctrMaLoaiHinh.Location = new System.Drawing.Point(105, 61);
            this.ctrMaLoaiHinh.Name = "ctrMaLoaiHinh";
            this.ctrMaLoaiHinh.Name_VN = "";
            this.ctrMaLoaiHinh.SetValidate = false;
            this.ctrMaLoaiHinh.ShowColumnCode = true;
            this.ctrMaLoaiHinh.ShowColumnName = true;
            this.ctrMaLoaiHinh.Size = new System.Drawing.Size(295, 26);
            this.ctrMaLoaiHinh.TabIndex = 5;
            this.ctrMaLoaiHinh.TagName = "";
            this.ctrMaLoaiHinh.WhereCondition = "";
            // 
            // txtSoToKhaiDauTien
            // 
            this.txtSoToKhaiDauTien.DecimalDigits = 12;
            this.txtSoToKhaiDauTien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhaiDauTien.Enabled = false;
            this.txtSoToKhaiDauTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiDauTien.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhaiDauTien.Location = new System.Drawing.Point(249, 12);
            this.txtSoToKhaiDauTien.MaxLength = 15;
            this.txtSoToKhaiDauTien.Name = "txtSoToKhaiDauTien";
            this.txtSoToKhaiDauTien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhaiDauTien.Size = new System.Drawing.Size(76, 21);
            this.txtSoToKhaiDauTien.TabIndex = 1;
            this.txtSoToKhaiDauTien.Text = "0";
            this.txtSoToKhaiDauTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhaiDauTien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhaiDauTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongSoTKChiaNho
            // 
            this.txtTongSoTKChiaNho.DecimalDigits = 20;
            this.txtTongSoTKChiaNho.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSoTKChiaNho.Enabled = false;
            this.txtTongSoTKChiaNho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTKChiaNho.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongSoTKChiaNho.Location = new System.Drawing.Point(364, 12);
            this.txtTongSoTKChiaNho.MaxLength = 15;
            this.txtTongSoTKChiaNho.Name = "txtTongSoTKChiaNho";
            this.txtTongSoTKChiaNho.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTKChiaNho.Size = new System.Drawing.Size(24, 21);
            this.txtTongSoTKChiaNho.TabIndex = 2;
            this.txtTongSoTKChiaNho.Text = "0";
            this.txtTongSoTKChiaNho.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTKChiaNho.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTKChiaNho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoNhanhToKhai
            // 
            this.txtSoNhanhToKhai.DecimalDigits = 20;
            this.txtSoNhanhToKhai.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoNhanhToKhai.Enabled = false;
            this.txtSoNhanhToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoNhanhToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoNhanhToKhai.Location = new System.Drawing.Point(330, 12);
            this.txtSoNhanhToKhai.MaxLength = 15;
            this.txtSoNhanhToKhai.Name = "txtSoNhanhToKhai";
            this.txtSoNhanhToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoNhanhToKhai.Size = new System.Drawing.Size(24, 21);
            this.txtSoNhanhToKhai.TabIndex = 2;
            this.txtSoNhanhToKhai.Text = "0";
            this.txtSoNhanhToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoNhanhToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoNhanhToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 12;
            this.txtSoToKhai.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhai.Enabled = false;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhai.Location = new System.Drawing.Point(63, 13);
            this.txtSoToKhai.MaxLength = 15;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhai.Size = new System.Drawing.Size(76, 21);
            this.txtSoToKhai.TabIndex = 0;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoToKhaiTNTX
            // 
            this.txtSoToKhaiTNTX.DecimalDigits = 12;
            this.txtSoToKhaiTNTX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhaiTNTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiTNTX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhaiTNTX.Location = new System.Drawing.Point(148, 90);
            this.txtSoToKhaiTNTX.MaxLength = 15;
            this.txtSoToKhaiTNTX.Name = "txtSoToKhaiTNTX";
            this.txtSoToKhaiTNTX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhaiTNTX.Size = new System.Drawing.Size(89, 21);
            this.txtSoToKhaiTNTX.TabIndex = 8;
            this.txtSoToKhaiTNTX.Text = "0";
            this.txtSoToKhaiTNTX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhaiTNTX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhaiTNTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaSoThueDaiDien
            // 
            this.txtMaSoThueDaiDien.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSoThueDaiDien.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSoThueDaiDien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSoThueDaiDien.Location = new System.Drawing.Point(808, 12);
            this.txtMaSoThueDaiDien.MaxLength = 12;
            this.txtMaSoThueDaiDien.Name = "txtMaSoThueDaiDien";
            this.txtMaSoThueDaiDien.Size = new System.Drawing.Size(50, 21);
            this.txtMaSoThueDaiDien.TabIndex = 7;
            this.txtMaSoThueDaiDien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSoThueDaiDien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Mã loại hình";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(429, 94);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(168, 13);
            this.label28.TabIndex = 33;
            this.label28.Text = "Mã hiệu phương thức vận chuyển";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(238, 94);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(90, 13);
            this.label30.TabIndex = 33;
            this.label30.Text = "Thời hạn tái nhập";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(410, 17);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(75, 13);
            this.label29.TabIndex = 33;
            this.label29.Text = "Ngày khai báo";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(212, 42);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(92, 13);
            this.label63.TabIndex = 33;
            this.label63.Text = "Cơ quan Hải quan";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(651, 16);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(158, 13);
            this.label27.TabIndex = 33;
            this.label27.Text = "Mã số hàng hóa đại diện tờ khai";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(353, 17);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 33;
            this.label23.Text = "/";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(642, 42);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(90, 13);
            this.label26.TabIndex = 33;
            this.label26.Text = "Mã bộ phận xữ lý";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(154, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "Số tờ khai đầu tiên";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(415, 68);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(115, 13);
            this.label25.TabIndex = 33;
            this.label25.Text = "Mã phân loại hàng hóa";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(5, 94);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(142, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "Số tờ khai tạm nhập tái xuất";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Số tờ khai";
            // 
            // grbDoiTac
            // 
            this.grbDoiTac.BackColor = System.Drawing.Color.Transparent;
            this.grbDoiTac.Controls.Add(this.ctrMaNuocDoiTac);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac2);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac1);
            this.grbDoiTac.Controls.Add(this.txtMaDoiTac);
            this.grbDoiTac.Controls.Add(this.label6);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac4);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac3);
            this.grbDoiTac.Controls.Add(this.label15);
            this.grbDoiTac.Controls.Add(this.label14);
            this.grbDoiTac.Controls.Add(this.label13);
            this.grbDoiTac.Controls.Add(this.txtTenDaiLyHaiQuan);
            this.grbDoiTac.Controls.Add(this.txtMaDaiLyHQ);
            this.grbDoiTac.Controls.Add(this.txtMaBuuChinhDoiTac);
            this.grbDoiTac.Controls.Add(this.label65);
            this.grbDoiTac.Controls.Add(this.label17);
            this.grbDoiTac.Controls.Add(this.label16);
            this.grbDoiTac.Controls.Add(this.txtTenDoiTac);
            this.grbDoiTac.Controls.Add(this.label8);
            this.grbDoiTac.Controls.Add(this.label9);
            this.grbDoiTac.Controls.Add(this.label10);
            this.grbDoiTac.Location = new System.Drawing.Point(3, 373);
            this.grbDoiTac.Name = "grbDoiTac";
            this.grbDoiTac.Size = new System.Drawing.Size(287, 303);
            this.grbDoiTac.TabIndex = 3;
            this.grbDoiTac.Text = "Người nhập khẩu";
            this.grbDoiTac.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ctrMaNuocDoiTac
            // 
            this.ctrMaNuocDoiTac.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrMaNuocDoiTac.Code = "";
            this.ctrMaNuocDoiTac.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaNuocDoiTac.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaNuocDoiTac.IsValidate = true;
            this.ctrMaNuocDoiTac.Location = new System.Drawing.Point(58, 222);
            this.ctrMaNuocDoiTac.Name = "ctrMaNuocDoiTac";
            this.ctrMaNuocDoiTac.Name_VN = "";
            this.ctrMaNuocDoiTac.SetValidate = false;
            this.ctrMaNuocDoiTac.ShowColumnCode = true;
            this.ctrMaNuocDoiTac.ShowColumnName = true;
            this.ctrMaNuocDoiTac.Size = new System.Drawing.Size(223, 26);
            this.ctrMaNuocDoiTac.TabIndex = 7;
            this.ctrMaNuocDoiTac.TagName = "";
            this.ctrMaNuocDoiTac.WhereCondition = "";
            // 
            // txtDiaChiDoiTac2
            // 
            this.txtDiaChiDoiTac2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac2.Location = new System.Drawing.Point(58, 131);
            this.txtDiaChiDoiTac2.MaxLength = 100;
            this.txtDiaChiDoiTac2.Multiline = true;
            this.txtDiaChiDoiTac2.Name = "txtDiaChiDoiTac2";
            this.txtDiaChiDoiTac2.Size = new System.Drawing.Size(223, 40);
            this.txtDiaChiDoiTac2.TabIndex = 3;
            this.txtDiaChiDoiTac2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac1
            // 
            this.txtDiaChiDoiTac1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac1.Location = new System.Drawing.Point(58, 86);
            this.txtDiaChiDoiTac1.MaxLength = 100;
            this.txtDiaChiDoiTac1.Multiline = true;
            this.txtDiaChiDoiTac1.Name = "txtDiaChiDoiTac1";
            this.txtDiaChiDoiTac1.Size = new System.Drawing.Size(223, 40);
            this.txtDiaChiDoiTac1.TabIndex = 2;
            this.txtDiaChiDoiTac1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoiTac
            // 
            this.txtMaDoiTac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDoiTac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoiTac.Location = new System.Drawing.Point(58, 17);
            this.txtMaDoiTac.MaxLength = 13;
            this.txtMaDoiTac.Name = "txtMaDoiTac";
            this.txtMaDoiTac.Size = new System.Drawing.Size(223, 21);
            this.txtMaDoiTac.TabIndex = 0;
            this.txtMaDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Địa chỉ (1)";
            // 
            // txtDiaChiDoiTac4
            // 
            this.txtDiaChiDoiTac4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac4.Location = new System.Drawing.Point(208, 201);
            this.txtDiaChiDoiTac4.MaxLength = 255;
            this.txtDiaChiDoiTac4.Name = "txtDiaChiDoiTac4";
            this.txtDiaChiDoiTac4.Size = new System.Drawing.Size(73, 21);
            this.txtDiaChiDoiTac4.TabIndex = 6;
            this.txtDiaChiDoiTac4.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac3
            // 
            this.txtDiaChiDoiTac3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac3.Location = new System.Drawing.Point(58, 201);
            this.txtDiaChiDoiTac3.MaxLength = 255;
            this.txtDiaChiDoiTac3.Name = "txtDiaChiDoiTac3";
            this.txtDiaChiDoiTac3.Size = new System.Drawing.Size(105, 21);
            this.txtDiaChiDoiTac3.TabIndex = 5;
            this.txtDiaChiDoiTac3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(162, 205);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "Quốc gia";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1, 204);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Tỉnh/Thành";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1, 134);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Địa chỉ (2)";
            // 
            // txtTenDaiLyHaiQuan
            // 
            this.txtTenDaiLyHaiQuan.Enabled = false;
            this.txtTenDaiLyHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDaiLyHaiQuan.Location = new System.Drawing.Point(70, 275);
            this.txtTenDaiLyHaiQuan.MaxLength = 255;
            this.txtTenDaiLyHaiQuan.Name = "txtTenDaiLyHaiQuan";
            this.txtTenDaiLyHaiQuan.ReadOnly = true;
            this.txtTenDaiLyHaiQuan.Size = new System.Drawing.Size(210, 21);
            this.txtTenDaiLyHaiQuan.TabIndex = 9;
            this.txtTenDaiLyHaiQuan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDaiLyHaiQuan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDaiLyHQ
            // 
            this.txtMaDaiLyHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLyHQ.Location = new System.Drawing.Point(71, 250);
            this.txtMaDaiLyHQ.MaxLength = 255;
            this.txtMaDaiLyHQ.Name = "txtMaDaiLyHQ";
            this.txtMaDaiLyHQ.Size = new System.Drawing.Size(92, 21);
            this.txtMaDaiLyHQ.TabIndex = 9;
            this.txtMaDaiLyHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDaiLyHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaBuuChinhDoiTac
            // 
            this.txtMaBuuChinhDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDoiTac.Location = new System.Drawing.Point(58, 176);
            this.txtMaBuuChinhDoiTac.MaxLength = 255;
            this.txtMaBuuChinhDoiTac.Name = "txtMaBuuChinhDoiTac";
            this.txtMaBuuChinhDoiTac.Size = new System.Drawing.Size(79, 21);
            this.txtMaBuuChinhDoiTac.TabIndex = 4;
            this.txtMaBuuChinhDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(0, 280);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(71, 13);
            this.label65.TabIndex = 8;
            this.label65.Text = "Tên đại lý HQ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(1, 254);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Mã đại lý HQ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1, 229);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Mã nước";
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoiTac.Location = new System.Drawing.Point(58, 42);
            this.txtTenDoiTac.MaxLength = 100;
            this.txtTenDoiTac.Multiline = true;
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(223, 40);
            this.txtTenDoiTac.TabIndex = 1;
            this.txtTenDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Bưu chính";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Mã";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Tên";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox1.Controls.Add(this.uiTab1);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(910, 709);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiTab1
            // 
            this.uiTab1.BackColor = System.Drawing.SystemColors.Control;
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(910, 709);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.uiGroupBox4);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(908, 687);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thông tin";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox4.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox4.Controls.Add(this.grbDonVi);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox12);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox13);
            this.uiGroupBox4.Controls.Add(this.grbDoiTac);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox14);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(908, 687);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.ctrMaXDThoiHanNopThue);
            this.uiGroupBox8.Controls.Add(this.txtKyHieuCTBaoLanh);
            this.uiGroupBox8.Controls.Add(this.label34);
            this.uiGroupBox8.Controls.Add(this.txtSoCTBaoLanh);
            this.uiGroupBox8.Controls.Add(this.txtNamPhatHanhBL);
            this.uiGroupBox8.Controls.Add(this.label35);
            this.uiGroupBox8.Controls.Add(this.label36);
            this.uiGroupBox8.Controls.Add(this.label37);
            this.uiGroupBox8.Controls.Add(this.txtMaNHBaoLanh);
            this.uiGroupBox8.Controls.Add(this.label38);
            this.uiGroupBox8.Location = new System.Drawing.Point(677, 397);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(196, 145);
            this.uiGroupBox8.TabIndex = 11;
            this.uiGroupBox8.Text = "Bảo lảnh thuế";
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaXDThoiHanNopThue
            // 
            this.ctrMaXDThoiHanNopThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E019;
            this.ctrMaXDThoiHanNopThue.Code = "";
            this.ctrMaXDThoiHanNopThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaXDThoiHanNopThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaXDThoiHanNopThue.IsValidate = true;
            this.ctrMaXDThoiHanNopThue.Location = new System.Drawing.Point(92, 14);
            this.ctrMaXDThoiHanNopThue.Name = "ctrMaXDThoiHanNopThue";
            this.ctrMaXDThoiHanNopThue.Name_VN = "";
            this.ctrMaXDThoiHanNopThue.SetValidate = false;
            this.ctrMaXDThoiHanNopThue.ShowColumnCode = true;
            this.ctrMaXDThoiHanNopThue.ShowColumnName = false;
            this.ctrMaXDThoiHanNopThue.Size = new System.Drawing.Size(100, 26);
            this.ctrMaXDThoiHanNopThue.TabIndex = 0;
            this.ctrMaXDThoiHanNopThue.TagName = "";
            this.ctrMaXDThoiHanNopThue.WhereCondition = "";
            // 
            // txtKyHieuCTBaoLanh
            // 
            this.txtKyHieuCTBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtKyHieuCTBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtKyHieuCTBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuCTBaoLanh.Location = new System.Drawing.Point(93, 88);
            this.txtKyHieuCTBaoLanh.MaxLength = 12;
            this.txtKyHieuCTBaoLanh.Name = "txtKyHieuCTBaoLanh";
            this.txtKyHieuCTBaoLanh.Size = new System.Drawing.Size(99, 21);
            this.txtKyHieuCTBaoLanh.TabIndex = 3;
            this.txtKyHieuCTBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKyHieuCTBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(6, 117);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 13);
            this.label34.TabIndex = 33;
            this.label34.Text = "Số chứng từ";
            // 
            // txtSoCTBaoLanh
            // 
            this.txtSoCTBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoCTBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoCTBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCTBaoLanh.Location = new System.Drawing.Point(93, 112);
            this.txtSoCTBaoLanh.MaxLength = 12;
            this.txtSoCTBaoLanh.Name = "txtSoCTBaoLanh";
            this.txtSoCTBaoLanh.Size = new System.Drawing.Size(99, 21);
            this.txtSoCTBaoLanh.TabIndex = 4;
            this.txtSoCTBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCTBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNamPhatHanhBL
            // 
            this.txtNamPhatHanhBL.DecimalDigits = 12;
            this.txtNamPhatHanhBL.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtNamPhatHanhBL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamPhatHanhBL.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNamPhatHanhBL.Location = new System.Drawing.Point(93, 64);
            this.txtNamPhatHanhBL.MaxLength = 15;
            this.txtNamPhatHanhBL.Name = "txtNamPhatHanhBL";
            this.txtNamPhatHanhBL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNamPhatHanhBL.Size = new System.Drawing.Size(100, 21);
            this.txtNamPhatHanhBL.TabIndex = 1;
            this.txtNamPhatHanhBL.Text = "0";
            this.txtNamPhatHanhBL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNamPhatHanhBL.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtNamPhatHanhBL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(6, 93);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(89, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "Ký hiệu chứng từ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(6, 69);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(80, 13);
            this.label36.TabIndex = 33;
            this.label36.Text = "Năm phát hành";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(6, 45);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(75, 13);
            this.label37.TabIndex = 33;
            this.label37.Text = "Mã ngân hàng";
            // 
            // txtMaNHBaoLanh
            // 
            this.txtMaNHBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNHBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNHBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNHBaoLanh.Location = new System.Drawing.Point(93, 40);
            this.txtMaNHBaoLanh.MaxLength = 12;
            this.txtMaNHBaoLanh.Name = "txtMaNHBaoLanh";
            this.txtMaNHBaoLanh.Size = new System.Drawing.Size(99, 21);
            this.txtMaNHBaoLanh.TabIndex = 1;
            this.txtMaNHBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNHBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(6, 21);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(83, 13);
            this.label38.TabIndex = 33;
            this.label38.Text = "Mã XD nộp thuế";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.ctrMaDDLuuKho);
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemXepHang);
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemNhanHang);
            this.uiGroupBox5.Controls.Add(this.clcNgayHangDen);
            this.uiGroupBox5.Controls.Add(this.label55);
            this.uiGroupBox5.Controls.Add(this.label54);
            this.uiGroupBox5.Controls.Add(this.label57);
            this.uiGroupBox5.Controls.Add(this.label56);
            this.uiGroupBox5.Controls.Add(this.label53);
            this.uiGroupBox5.Controls.Add(this.label52);
            this.uiGroupBox5.Controls.Add(this.label51);
            this.uiGroupBox5.Controls.Add(this.label50);
            this.uiGroupBox5.Controls.Add(this.txtSoHieuKyHieu);
            this.uiGroupBox5.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox5.Controls.Add(this.txtMaPTVC);
            this.uiGroupBox5.Controls.Add(this.txtTenPTVC);
            this.uiGroupBox5.Location = new System.Drawing.Point(296, 126);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(577, 119);
            this.uiGroupBox5.TabIndex = 4;
            this.uiGroupBox5.Text = "Vận đơn";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDDLuuKho
            // 
            this.ctrMaDDLuuKho.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDDLuuKho.Code = "";
            this.ctrMaDDLuuKho.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDDLuuKho.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDDLuuKho.IsValidate = true;
            this.ctrMaDDLuuKho.Location = new System.Drawing.Point(182, 37);
            this.ctrMaDDLuuKho.Name = "ctrMaDDLuuKho";
            this.ctrMaDDLuuKho.Name_VN = "";
            this.ctrMaDDLuuKho.SetValidate = false;
            this.ctrMaDDLuuKho.ShowColumnCode = true;
            this.ctrMaDDLuuKho.ShowColumnName = false;
            this.ctrMaDDLuuKho.Size = new System.Drawing.Size(73, 24);
            this.ctrMaDDLuuKho.TabIndex = 56;
            this.ctrMaDDLuuKho.TagName = "";
            this.ctrMaDDLuuKho.WhereCondition = "";
            // 
            // ctrDiaDiemXepHang
            // 
            this.ctrDiaDiemXepHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A620;
            this.ctrDiaDiemXepHang.Code = "";
            this.ctrDiaDiemXepHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemXepHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemXepHang.IsValidate = true;
            this.ctrDiaDiemXepHang.Location = new System.Drawing.Point(361, 35);
            this.ctrDiaDiemXepHang.Name = "ctrDiaDiemXepHang";
            this.ctrDiaDiemXepHang.Name_VN = "";
            this.ctrDiaDiemXepHang.SetValidate = false;
            this.ctrDiaDiemXepHang.ShowColumnCode = true;
            this.ctrDiaDiemXepHang.ShowColumnName = true;
            this.ctrDiaDiemXepHang.Size = new System.Drawing.Size(208, 26);
            this.ctrDiaDiemXepHang.TabIndex = 53;
            this.ctrDiaDiemXepHang.TagCode = "";
            this.ctrDiaDiemXepHang.TagName = "";
            this.ctrDiaDiemXepHang.WhereCondition = "";
            // 
            // ctrDiaDiemNhanHang
            // 
            this.ctrDiaDiemNhanHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ctrDiaDiemNhanHang.Code = "";
            this.ctrDiaDiemNhanHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemNhanHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemNhanHang.IsValidate = true;
            this.ctrDiaDiemNhanHang.Location = new System.Drawing.Point(361, 10);
            this.ctrDiaDiemNhanHang.Name = "ctrDiaDiemNhanHang";
            this.ctrDiaDiemNhanHang.Name_VN = "";
            this.ctrDiaDiemNhanHang.SetValidate = false;
            this.ctrDiaDiemNhanHang.ShowColumnCode = true;
            this.ctrDiaDiemNhanHang.ShowColumnName = true;
            this.ctrDiaDiemNhanHang.Size = new System.Drawing.Size(208, 26);
            this.ctrDiaDiemNhanHang.TabIndex = 53;
            this.ctrDiaDiemNhanHang.TagCode = "";
            this.ctrDiaDiemNhanHang.TagName = "";
            this.ctrDiaDiemNhanHang.WhereCondition = "";
            // 
            // clcNgayHangDen
            // 
            this.clcNgayHangDen.Location = new System.Drawing.Point(83, 66);
            this.clcNgayHangDen.Name = "clcNgayHangDen";
            this.clcNgayHangDen.ReadOnly = false;
            this.clcNgayHangDen.Size = new System.Drawing.Size(89, 21);
            this.clcNgayHangDen.TabIndex = 52;
            this.clcNgayHangDen.TagName = "";
            this.clcNgayHangDen.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(7, 97);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(94, 13);
            this.label55.TabIndex = 36;
            this.label55.Text = "Ký hiệu và số hiệu";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(7, 70);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(70, 13);
            this.label54.TabIndex = 36;
            this.label54.Text = "Ngày hàng đi";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(348, 70);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(53, 13);
            this.label57.TabIndex = 36;
            this.label57.Text = "Tên PTVC";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(206, 70);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(49, 13);
            this.label56.TabIndex = 36;
            this.label56.Text = "Mã PTVC";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(257, 41);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(96, 13);
            this.label53.TabIndex = 36;
            this.label53.Text = "Địa điểm xếp hàng";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(257, 15);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(102, 13);
            this.label52.TabIndex = 36;
            this.label52.Text = "Địa điểm nhận hàng";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(7, 43);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(179, 13);
            this.label51.TabIndex = 36;
            this.label51.Text = "Mã địa điểm lưu kho chờ thông quan";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(7, 20);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(19, 13);
            this.label50.TabIndex = 36;
            this.label50.Text = "Số";
            // 
            // txtSoHieuKyHieu
            // 
            this.txtSoHieuKyHieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuKyHieu.Location = new System.Drawing.Point(102, 93);
            this.txtSoHieuKyHieu.MaxLength = 255;
            this.txtSoHieuKyHieu.Name = "txtSoHieuKyHieu";
            this.txtSoHieuKyHieu.Size = new System.Drawing.Size(467, 21);
            this.txtSoHieuKyHieu.TabIndex = 7;
            this.txtSoHieuKyHieu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuKyHieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon.Location = new System.Drawing.Point(51, 15);
            this.txtSoVanDon.MaxLength = 255;
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(180, 21);
            this.txtSoVanDon.TabIndex = 0;
            this.txtSoVanDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaPTVC
            // 
            this.txtMaPTVC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaPTVC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaPTVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPTVC.Location = new System.Drawing.Point(258, 66);
            this.txtMaPTVC.MaxLength = 12;
            this.txtMaPTVC.Name = "txtMaPTVC";
            this.txtMaPTVC.Size = new System.Drawing.Size(89, 21);
            this.txtMaPTVC.TabIndex = 5;
            this.txtMaPTVC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaPTVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenPTVC
            // 
            this.txtTenPTVC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenPTVC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenPTVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPTVC.Location = new System.Drawing.Point(403, 65);
            this.txtTenPTVC.MaxLength = 12;
            this.txtTenPTVC.Name = "txtTenPTVC";
            this.txtTenPTVC.Size = new System.Drawing.Size(166, 21);
            this.txtTenPTVC.TabIndex = 6;
            this.txtTenPTVC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenPTVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.ctrNguoiNopThue);
            this.uiGroupBox7.Controls.Add(this.txtKyHieuCTHanMuc);
            this.uiGroupBox7.Controls.Add(this.label33);
            this.uiGroupBox7.Controls.Add(this.txtSoCTHanMuc);
            this.uiGroupBox7.Controls.Add(this.txtNanPhatHanhHM);
            this.uiGroupBox7.Controls.Add(this.label32);
            this.uiGroupBox7.Controls.Add(this.label31);
            this.uiGroupBox7.Controls.Add(this.label22);
            this.uiGroupBox7.Controls.Add(this.txtMaNHTraThueThay);
            this.uiGroupBox7.Controls.Add(this.label21);
            this.uiGroupBox7.Location = new System.Drawing.Point(677, 249);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(196, 147);
            this.uiGroupBox7.TabIndex = 10;
            this.uiGroupBox7.Text = "Hạn mức";
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // ctrNguoiNopThue
            // 
            this.ctrNguoiNopThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E015;
            this.ctrNguoiNopThue.Code = "";
            this.ctrNguoiNopThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNguoiNopThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrNguoiNopThue.IsValidate = true;
            this.ctrNguoiNopThue.Location = new System.Drawing.Point(92, 12);
            this.ctrNguoiNopThue.Name = "ctrNguoiNopThue";
            this.ctrNguoiNopThue.Name_VN = "";
            this.ctrNguoiNopThue.SetValidate = false;
            this.ctrNguoiNopThue.ShowColumnCode = true;
            this.ctrNguoiNopThue.ShowColumnName = false;
            this.ctrNguoiNopThue.Size = new System.Drawing.Size(100, 26);
            this.ctrNguoiNopThue.TabIndex = 0;
            this.ctrNguoiNopThue.TagName = "";
            this.ctrNguoiNopThue.WhereCondition = "";
            // 
            // txtKyHieuCTHanMuc
            // 
            this.txtKyHieuCTHanMuc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtKyHieuCTHanMuc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtKyHieuCTHanMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuCTHanMuc.Location = new System.Drawing.Point(92, 86);
            this.txtKyHieuCTHanMuc.MaxLength = 12;
            this.txtKyHieuCTHanMuc.Name = "txtKyHieuCTHanMuc";
            this.txtKyHieuCTHanMuc.Size = new System.Drawing.Size(100, 21);
            this.txtKyHieuCTHanMuc.TabIndex = 3;
            this.txtKyHieuCTHanMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKyHieuCTHanMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(5, 115);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(66, 13);
            this.label33.TabIndex = 33;
            this.label33.Text = "Số chứng từ";
            // 
            // txtSoCTHanMuc
            // 
            this.txtSoCTHanMuc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoCTHanMuc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoCTHanMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCTHanMuc.Location = new System.Drawing.Point(92, 110);
            this.txtSoCTHanMuc.MaxLength = 12;
            this.txtSoCTHanMuc.Name = "txtSoCTHanMuc";
            this.txtSoCTHanMuc.Size = new System.Drawing.Size(100, 21);
            this.txtSoCTHanMuc.TabIndex = 4;
            this.txtSoCTHanMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCTHanMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNanPhatHanhHM
            // 
            this.txtNanPhatHanhHM.DecimalDigits = 12;
            this.txtNanPhatHanhHM.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtNanPhatHanhHM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNanPhatHanhHM.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNanPhatHanhHM.Location = new System.Drawing.Point(92, 62);
            this.txtNanPhatHanhHM.MaxLength = 15;
            this.txtNanPhatHanhHM.Name = "txtNanPhatHanhHM";
            this.txtNanPhatHanhHM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNanPhatHanhHM.Size = new System.Drawing.Size(100, 21);
            this.txtNanPhatHanhHM.TabIndex = 1;
            this.txtNanPhatHanhHM.Text = "0";
            this.txtNanPhatHanhHM.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNanPhatHanhHM.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtNanPhatHanhHM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(5, 91);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(89, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "Ký hiệu chứng từ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(5, 67);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(80, 13);
            this.label31.TabIndex = 33;
            this.label31.Text = "Năm phát hành";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(5, 43);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 13);
            this.label22.TabIndex = 33;
            this.label22.Text = "Mã ngân hàng";
            // 
            // txtMaNHTraThueThay
            // 
            this.txtMaNHTraThueThay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNHTraThueThay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNHTraThueThay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNHTraThueThay.Location = new System.Drawing.Point(92, 38);
            this.txtMaNHTraThueThay.MaxLength = 12;
            this.txtMaNHTraThueThay.Name = "txtMaNHTraThueThay";
            this.txtMaNHTraThueThay.Size = new System.Drawing.Size(100, 21);
            this.txtMaNHTraThueThay.TabIndex = 1;
            this.txtMaNHTraThueThay.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNHTraThueThay.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(5, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 33;
            this.label21.Text = "Người nộp thuế";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.ctrMaTTTriGiaTinhThue);
            this.uiGroupBox9.Controls.Add(this.txtPhanLoaiKhongQDVND);
            this.uiGroupBox9.Controls.Add(this.label41);
            this.uiGroupBox9.Controls.Add(this.label40);
            this.uiGroupBox9.Controls.Add(this.label39);
            this.uiGroupBox9.Controls.Add(this.txtTongHeSoPhanBoTG);
            this.uiGroupBox9.Controls.Add(this.txtTriGiaTinhThue);
            this.uiGroupBox9.Controls.Add(this.lblMaPhanLoaiTongGiaCoBan);
            this.uiGroupBox9.Location = new System.Drawing.Point(295, 397);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(375, 85);
            this.uiGroupBox9.TabIndex = 6;
            this.uiGroupBox9.Text = "Tính thuế";
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaTTTriGiaTinhThue
            // 
            this.ctrMaTTTriGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTriGiaTinhThue.Code = "";
            this.ctrMaTTTriGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTriGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaTTTriGiaTinhThue.IsValidate = true;
            this.ctrMaTTTriGiaTinhThue.Location = new System.Drawing.Point(224, 9);
            this.ctrMaTTTriGiaTinhThue.Name = "ctrMaTTTriGiaTinhThue";
            this.ctrMaTTTriGiaTinhThue.Name_VN = "";
            this.ctrMaTTTriGiaTinhThue.SetValidate = false;
            this.ctrMaTTTriGiaTinhThue.ShowColumnCode = true;
            this.ctrMaTTTriGiaTinhThue.ShowColumnName = false;
            this.ctrMaTTTriGiaTinhThue.Size = new System.Drawing.Size(93, 26);
            this.ctrMaTTTriGiaTinhThue.TabIndex = 1;
            this.ctrMaTTTriGiaTinhThue.TagName = "";
            this.ctrMaTTTriGiaTinhThue.WhereCondition = "";
            // 
            // txtPhanLoaiKhongQDVND
            // 
            this.txtPhanLoaiKhongQDVND.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhanLoaiKhongQDVND.Location = new System.Drawing.Point(178, 61);
            this.txtPhanLoaiKhongQDVND.MaxLength = 255;
            this.txtPhanLoaiKhongQDVND.Name = "txtPhanLoaiKhongQDVND";
            this.txtPhanLoaiKhongQDVND.Size = new System.Drawing.Size(44, 21);
            this.txtPhanLoaiKhongQDVND.TabIndex = 3;
            this.txtPhanLoaiKhongQDVND.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhanLoaiKhongQDVND.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(8, 65);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(164, 13);
            this.label41.TabIndex = 3;
            this.label41.Text = "Phân loại không cần quy đổi VNĐ";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(6, 41);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(102, 13);
            this.label40.TabIndex = 33;
            this.label40.Text = "Tổng hệ số phân bổ";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(6, 17);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(82, 13);
            this.label39.TabIndex = 33;
            this.label39.Text = "Trị giá tính thuế";
            // 
            // txtTongHeSoPhanBoTG
            // 
            this.txtTongHeSoPhanBoTG.BackColor = System.Drawing.Color.White;
            this.txtTongHeSoPhanBoTG.DecimalDigits = 20;
            this.txtTongHeSoPhanBoTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongHeSoPhanBoTG.FormatString = "G20";
            this.txtTongHeSoPhanBoTG.Location = new System.Drawing.Point(110, 37);
            this.txtTongHeSoPhanBoTG.Name = "txtTongHeSoPhanBoTG";
            this.txtTongHeSoPhanBoTG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongHeSoPhanBoTG.Size = new System.Drawing.Size(112, 21);
            this.txtTongHeSoPhanBoTG.TabIndex = 7;
            this.txtTongHeSoPhanBoTG.Text = "0";
            this.txtTongHeSoPhanBoTG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongHeSoPhanBoTG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongHeSoPhanBoTG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThue
            // 
            this.txtTriGiaTinhThue.BackColor = System.Drawing.Color.White;
            this.txtTriGiaTinhThue.DecimalDigits = 20;
            this.txtTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThue.FormatString = "G20";
            this.txtTriGiaTinhThue.Location = new System.Drawing.Point(110, 12);
            this.txtTriGiaTinhThue.Name = "txtTriGiaTinhThue";
            this.txtTriGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThue.Size = new System.Drawing.Size(112, 21);
            this.txtTriGiaTinhThue.TabIndex = 7;
            this.txtTriGiaTinhThue.Text = "0";
            this.txtTriGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblMaPhanLoaiTongGiaCoBan
            // 
            this.lblMaPhanLoaiTongGiaCoBan.AutoSize = true;
            this.lblMaPhanLoaiTongGiaCoBan.BackColor = System.Drawing.Color.Transparent;
            this.lblMaPhanLoaiTongGiaCoBan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiTongGiaCoBan.Location = new System.Drawing.Point(228, 42);
            this.lblMaPhanLoaiTongGiaCoBan.Name = "lblMaPhanLoaiTongGiaCoBan";
            this.lblMaPhanLoaiTongGiaCoBan.Size = new System.Drawing.Size(0, 13);
            this.lblMaPhanLoaiTongGiaCoBan.TabIndex = 36;
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox12.Controls.Add(this.ctrMaDieuKienGiaHD);
            this.uiGroupBox12.Controls.Add(this.ctrPhuongThucTT);
            this.uiGroupBox12.Controls.Add(this.ctrPhanLoaiGiaHD);
            this.uiGroupBox12.Controls.Add(this.clcNgayPhatHanhHD);
            this.uiGroupBox12.Controls.Add(this.ctrPhanLoaiHD);
            this.uiGroupBox12.Controls.Add(this.ctrMaTTHoaDon);
            this.uiGroupBox12.Controls.Add(this.label42);
            this.uiGroupBox12.Controls.Add(this.label43);
            this.uiGroupBox12.Controls.Add(this.txtSoTiepNhanHD);
            this.uiGroupBox12.Controls.Add(this.label44);
            this.uiGroupBox12.Controls.Add(this.txtTongTriGiaHD);
            this.uiGroupBox12.Controls.Add(this.txtSoHoaDon);
            this.uiGroupBox12.Controls.Add(this.label45);
            this.uiGroupBox12.Controls.Add(this.label46);
            this.uiGroupBox12.Controls.Add(this.label47);
            this.uiGroupBox12.Controls.Add(this.label48);
            this.uiGroupBox12.Controls.Add(this.label49);
            this.uiGroupBox12.Location = new System.Drawing.Point(295, 249);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(375, 147);
            this.uiGroupBox12.TabIndex = 5;
            this.uiGroupBox12.Text = "Hóa đơn";
            this.uiGroupBox12.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDieuKienGiaHD
            // 
            this.ctrMaDieuKienGiaHD.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E010;
            this.ctrMaDieuKienGiaHD.Code = "";
            this.ctrMaDieuKienGiaHD.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDieuKienGiaHD.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDieuKienGiaHD.IsValidate = true;
            this.ctrMaDieuKienGiaHD.Location = new System.Drawing.Point(103, 92);
            this.ctrMaDieuKienGiaHD.Name = "ctrMaDieuKienGiaHD";
            this.ctrMaDieuKienGiaHD.Name_VN = "";
            this.ctrMaDieuKienGiaHD.SetValidate = false;
            this.ctrMaDieuKienGiaHD.ShowColumnCode = true;
            this.ctrMaDieuKienGiaHD.ShowColumnName = false;
            this.ctrMaDieuKienGiaHD.Size = new System.Drawing.Size(90, 24);
            this.ctrMaDieuKienGiaHD.TabIndex = 53;
            this.ctrMaDieuKienGiaHD.TagName = "";
            this.ctrMaDieuKienGiaHD.WhereCondition = "";
            // 
            // ctrPhuongThucTT
            // 
            this.ctrPhuongThucTT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E009;
            this.ctrPhuongThucTT.Code = "";
            this.ctrPhuongThucTT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrPhuongThucTT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrPhuongThucTT.IsValidate = true;
            this.ctrPhuongThucTT.Location = new System.Drawing.Point(261, 66);
            this.ctrPhuongThucTT.Name = "ctrPhuongThucTT";
            this.ctrPhuongThucTT.Name_VN = "";
            this.ctrPhuongThucTT.SetValidate = false;
            this.ctrPhuongThucTT.ShowColumnCode = true;
            this.ctrPhuongThucTT.ShowColumnName = false;
            this.ctrPhuongThucTT.Size = new System.Drawing.Size(106, 24);
            this.ctrPhuongThucTT.TabIndex = 53;
            this.ctrPhuongThucTT.TagName = "";
            this.ctrPhuongThucTT.WhereCondition = "";
            // 
            // ctrPhanLoaiGiaHD
            // 
            this.ctrPhanLoaiGiaHD.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E010;
            this.ctrPhanLoaiGiaHD.Code = "";
            this.ctrPhanLoaiGiaHD.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrPhanLoaiGiaHD.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrPhanLoaiGiaHD.IsValidate = true;
            this.ctrPhanLoaiGiaHD.Location = new System.Drawing.Point(261, 92);
            this.ctrPhanLoaiGiaHD.Name = "ctrPhanLoaiGiaHD";
            this.ctrPhanLoaiGiaHD.Name_VN = "";
            this.ctrPhanLoaiGiaHD.SetValidate = false;
            this.ctrPhanLoaiGiaHD.ShowColumnCode = true;
            this.ctrPhanLoaiGiaHD.ShowColumnName = false;
            this.ctrPhanLoaiGiaHD.Size = new System.Drawing.Size(106, 24);
            this.ctrPhanLoaiGiaHD.TabIndex = 53;
            this.ctrPhanLoaiGiaHD.TagName = "";
            this.ctrPhanLoaiGiaHD.WhereCondition = "";
            // 
            // clcNgayPhatHanhHD
            // 
            this.clcNgayPhatHanhHD.Location = new System.Drawing.Point(103, 67);
            this.clcNgayPhatHanhHD.Name = "clcNgayPhatHanhHD";
            this.clcNgayPhatHanhHD.ReadOnly = false;
            this.clcNgayPhatHanhHD.Size = new System.Drawing.Size(89, 21);
            this.clcNgayPhatHanhHD.TabIndex = 52;
            this.clcNgayPhatHanhHD.TagName = "";
            this.clcNgayPhatHanhHD.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // ctrPhanLoaiHD
            // 
            this.ctrPhanLoaiHD.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E008;
            this.ctrPhanLoaiHD.Code = "";
            this.ctrPhanLoaiHD.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrPhanLoaiHD.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrPhanLoaiHD.IsValidate = true;
            this.ctrPhanLoaiHD.Location = new System.Drawing.Point(103, 10);
            this.ctrPhanLoaiHD.Name = "ctrPhanLoaiHD";
            this.ctrPhanLoaiHD.Name_VN = "";
            this.ctrPhanLoaiHD.SetValidate = false;
            this.ctrPhanLoaiHD.ShowColumnCode = true;
            this.ctrPhanLoaiHD.ShowColumnName = true;
            this.ctrPhanLoaiHD.Size = new System.Drawing.Size(89, 26);
            this.ctrPhanLoaiHD.TabIndex = 0;
            this.ctrPhanLoaiHD.TagName = "";
            this.ctrPhanLoaiHD.WhereCondition = "";
            // 
            // ctrMaTTHoaDon
            // 
            this.ctrMaTTHoaDon.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTHoaDon.Code = "";
            this.ctrMaTTHoaDon.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTHoaDon.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaTTHoaDon.IsValidate = true;
            this.ctrMaTTHoaDon.Location = new System.Drawing.Point(287, 117);
            this.ctrMaTTHoaDon.Name = "ctrMaTTHoaDon";
            this.ctrMaTTHoaDon.Name_VN = "";
            this.ctrMaTTHoaDon.SetValidate = false;
            this.ctrMaTTHoaDon.ShowColumnCode = true;
            this.ctrMaTTHoaDon.ShowColumnName = false;
            this.ctrMaTTHoaDon.Size = new System.Drawing.Size(82, 26);
            this.ctrMaTTHoaDon.TabIndex = 36;
            this.ctrMaTTHoaDon.TagName = "";
            this.ctrMaTTHoaDon.WhereCondition = "";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(6, 17);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(98, 13);
            this.label42.TabIndex = 35;
            this.label42.Text = "Phân loại hình thức";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(191, 17);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(67, 13);
            this.label43.TabIndex = 35;
            this.label43.Text = "Số tiếp nhận";
            // 
            // txtSoTiepNhanHD
            // 
            this.txtSoTiepNhanHD.DecimalDigits = 12;
            this.txtSoTiepNhanHD.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTiepNhanHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhanHD.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTiepNhanHD.Location = new System.Drawing.Point(261, 13);
            this.txtSoTiepNhanHD.MaxLength = 15;
            this.txtSoTiepNhanHD.Name = "txtSoTiepNhanHD";
            this.txtSoTiepNhanHD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTiepNhanHD.Size = new System.Drawing.Size(106, 21);
            this.txtSoTiepNhanHD.TabIndex = 1;
            this.txtSoTiepNhanHD.Text = "0";
            this.txtSoTiepNhanHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhanHD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTiepNhanHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(6, 44);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(61, 13);
            this.label44.TabIndex = 35;
            this.label44.Text = "Số hóa đơn";
            // 
            // txtTongTriGiaHD
            // 
            this.txtTongTriGiaHD.BackColor = System.Drawing.Color.White;
            this.txtTongTriGiaHD.DecimalDigits = 20;
            this.txtTongTriGiaHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGiaHD.FormatString = "G20";
            this.txtTongTriGiaHD.Location = new System.Drawing.Point(103, 120);
            this.txtTongTriGiaHD.Name = "txtTongTriGiaHD";
            this.txtTongTriGiaHD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTriGiaHD.Size = new System.Drawing.Size(178, 21);
            this.txtTongTriGiaHD.TabIndex = 7;
            this.txtTongTriGiaHD.Text = "0";
            this.txtTongTriGiaHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTriGiaHD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTriGiaHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongTriGiaHD.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoHoaDon.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDon.Location = new System.Drawing.Point(103, 40);
            this.txtSoHoaDon.MaxLength = 12;
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(264, 21);
            this.txtSoHoaDon.TabIndex = 2;
            this.txtSoHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(194, 71);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(31, 13);
            this.label45.TabIndex = 35;
            this.label45.Text = "PTTT";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(7, 125);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(61, 13);
            this.label46.TabIndex = 35;
            this.label46.Text = "Tổng trị giá";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(6, 71);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(84, 13);
            this.label47.TabIndex = 35;
            this.label47.Text = "Ngày phát hành";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(194, 97);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(67, 13);
            this.label48.TabIndex = 35;
            this.label48.Text = "Phân loại giá";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(7, 98);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 13);
            this.label49.TabIndex = 35;
            this.label49.Text = "Điều kiện giá";
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.ctrMaDVTTrongLuong);
            this.uiGroupBox13.Controls.Add(this.txtTrongLuong);
            this.uiGroupBox13.Location = new System.Drawing.Point(483, 482);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(187, 45);
            this.uiGroupBox13.TabIndex = 8;
            this.uiGroupBox13.Text = "Tổng trọng lượng hàng";
            this.uiGroupBox13.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDVTTrongLuong
            // 
            this.ctrMaDVTTrongLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E006;
            this.ctrMaDVTTrongLuong.Code = "";
            this.ctrMaDVTTrongLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTTrongLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaDVTTrongLuong.IsValidate = true;
            this.ctrMaDVTTrongLuong.Location = new System.Drawing.Point(127, 13);
            this.ctrMaDVTTrongLuong.Name = "ctrMaDVTTrongLuong";
            this.ctrMaDVTTrongLuong.Name_VN = "";
            this.ctrMaDVTTrongLuong.SetValidate = false;
            this.ctrMaDVTTrongLuong.ShowColumnCode = true;
            this.ctrMaDVTTrongLuong.ShowColumnName = false;
            this.ctrMaDVTTrongLuong.Size = new System.Drawing.Size(56, 26);
            this.ctrMaDVTTrongLuong.TabIndex = 1;
            this.ctrMaDVTTrongLuong.TagName = "";
            this.ctrMaDVTTrongLuong.WhereCondition = "";
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 20;
            this.txtTrongLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTrongLuong.Location = new System.Drawing.Point(9, 17);
            this.txtTrongLuong.MaxLength = 15;
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTrongLuong.Size = new System.Drawing.Size(110, 21);
            this.txtTrongLuong.TabIndex = 0;
            this.txtTrongLuong.Text = "0";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.txtGhiChu);
            this.uiGroupBox10.Location = new System.Drawing.Point(294, 592);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(579, 84);
            this.uiGroupBox10.TabIndex = 13;
            this.uiGroupBox10.Text = "Ghi chú";
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(6, 16);
            this.txtGhiChu.MaxLength = 100;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(569, 62);
            this.txtGhiChu.TabIndex = 0;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox14
            // 
            this.uiGroupBox14.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox14.Controls.Add(this.ctrDiaDiemDichVC);
            this.uiGroupBox14.Controls.Add(this.clcNgayDen);
            this.uiGroupBox14.Controls.Add(this.clcNgayKhoiHanhVC);
            this.uiGroupBox14.Controls.Add(this.label59);
            this.uiGroupBox14.Controls.Add(this.label66);
            this.uiGroupBox14.Controls.Add(this.label58);
            this.uiGroupBox14.Location = new System.Drawing.Point(294, 524);
            this.uiGroupBox14.Name = "uiGroupBox14";
            this.uiGroupBox14.Size = new System.Drawing.Size(376, 68);
            this.uiGroupBox14.TabIndex = 9;
            this.uiGroupBox14.VisualStyleManager = this.vsmMain;
            // 
            // ctrDiaDiemDichVC
            // 
            this.ctrDiaDiemDichVC.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrDiaDiemDichVC.Code = "";
            this.ctrDiaDiemDichVC.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemDichVC.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemDichVC.IsValidate = true;
            this.ctrDiaDiemDichVC.Location = new System.Drawing.Point(207, 38);
            this.ctrDiaDiemDichVC.Name = "ctrDiaDiemDichVC";
            this.ctrDiaDiemDichVC.Name_VN = "";
            this.ctrDiaDiemDichVC.SetValidate = false;
            this.ctrDiaDiemDichVC.ShowColumnCode = true;
            this.ctrDiaDiemDichVC.ShowColumnName = false;
            this.ctrDiaDiemDichVC.Size = new System.Drawing.Size(160, 24);
            this.ctrDiaDiemDichVC.TabIndex = 56;
            this.ctrDiaDiemDichVC.TagName = "";
            this.ctrDiaDiemDichVC.WhereCondition = "";
            // 
            // clcNgayDen
            // 
            this.clcNgayDen.Location = new System.Drawing.Point(278, 14);
            this.clcNgayDen.Name = "clcNgayDen";
            this.clcNgayDen.ReadOnly = false;
            this.clcNgayDen.Size = new System.Drawing.Size(89, 21);
            this.clcNgayDen.TabIndex = 52;
            this.clcNgayDen.TagName = "";
            this.clcNgayDen.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // clcNgayKhoiHanhVC
            // 
            this.clcNgayKhoiHanhVC.Location = new System.Drawing.Point(89, 14);
            this.clcNgayKhoiHanhVC.Name = "clcNgayKhoiHanhVC";
            this.clcNgayKhoiHanhVC.ReadOnly = false;
            this.clcNgayKhoiHanhVC.Size = new System.Drawing.Size(89, 21);
            this.clcNgayKhoiHanhVC.TabIndex = 52;
            this.clcNgayKhoiHanhVC.TagName = "";
            this.clcNgayKhoiHanhVC.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(6, 42);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(195, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "Địa điểm đích cho vận chuyển bảo thuế";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(220, 19);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(53, 13);
            this.label66.TabIndex = 35;
            this.label66.Text = "Ngày đến";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(6, 18);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(81, 13);
            this.label58.TabIndex = 35;
            this.label58.Text = "Ngày khởi hành";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.ctrMaDVTSoLuong);
            this.uiGroupBox6.Controls.Add(this.txtSoLuong);
            this.uiGroupBox6.Location = new System.Drawing.Point(294, 482);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(186, 45);
            this.uiGroupBox6.TabIndex = 7;
            this.uiGroupBox6.Text = "Số lượng";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDVTSoLuong
            // 
            this.ctrMaDVTSoLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ctrMaDVTSoLuong.Code = "";
            this.ctrMaDVTSoLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTSoLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaDVTSoLuong.IsValidate = true;
            this.ctrMaDVTSoLuong.Location = new System.Drawing.Point(122, 14);
            this.ctrMaDVTSoLuong.Name = "ctrMaDVTSoLuong";
            this.ctrMaDVTSoLuong.Name_VN = "";
            this.ctrMaDVTSoLuong.SetValidate = false;
            this.ctrMaDVTSoLuong.ShowColumnCode = true;
            this.ctrMaDVTSoLuong.ShowColumnName = false;
            this.ctrMaDVTSoLuong.Size = new System.Drawing.Size(56, 26);
            this.ctrMaDVTSoLuong.TabIndex = 1;
            this.ctrMaDVTSoLuong.TagName = "";
            this.ctrMaDVTSoLuong.WhereCondition = "";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 20;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong.Location = new System.Drawing.Point(6, 17);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(110, 21);
            this.txtSoLuong.TabIndex = 0;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.txtSoQuanLyNoiBoDN);
            this.uiGroupBox11.Location = new System.Drawing.Point(677, 543);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(196, 43);
            this.uiGroupBox11.TabIndex = 12;
            this.uiGroupBox11.Text = "Số quản lý nội bộ doanh nghiệp";
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // txtSoQuanLyNoiBoDN
            // 
            this.txtSoQuanLyNoiBoDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoQuanLyNoiBoDN.Location = new System.Drawing.Point(8, 16);
            this.txtSoQuanLyNoiBoDN.MaxLength = 255;
            this.txtSoQuanLyNoiBoDN.Name = "txtSoQuanLyNoiBoDN";
            this.txtSoQuanLyNoiBoDN.Size = new System.Drawing.Size(184, 21);
            this.txtSoQuanLyNoiBoDN.TabIndex = 0;
            this.txtSoQuanLyNoiBoDN.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoQuanLyNoiBoDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.uiGroupBox15);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(908, 687);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thuế và lệ phí";
            // 
            // uiGroupBox15
            // 
            this.uiGroupBox15.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox15.Controls.Add(this.txtPhanLoaiNopThue);
            this.uiGroupBox15.Controls.Add(this.ctrMaTTCuaSoTienBaoLanh);
            this.uiGroupBox15.Controls.Add(this.ctrMaTTTongTienThueXuatKhau);
            this.uiGroupBox15.Controls.Add(this.uiGroupBox17);
            this.uiGroupBox15.Controls.Add(this.txtTongSoDongHangCuaToKhai);
            this.uiGroupBox15.Controls.Add(this.txtTongSoTrangCuaToKhai);
            this.uiGroupBox15.Controls.Add(this.label68);
            this.uiGroupBox15.Controls.Add(this.txtSoTienBaoLanh);
            this.uiGroupBox15.Controls.Add(this.label70);
            this.uiGroupBox15.Controls.Add(this.label67);
            this.uiGroupBox15.Controls.Add(this.txtTongSoTienLePhi);
            this.uiGroupBox15.Controls.Add(this.label69);
            this.uiGroupBox15.Controls.Add(this.label61);
            this.uiGroupBox15.Controls.Add(this.label60);
            this.uiGroupBox15.Controls.Add(this.txtTongSoTienThueXuatKhau);
            this.uiGroupBox15.Controls.Add(this.label7);
            this.uiGroupBox15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox15.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox15.Name = "uiGroupBox15";
            this.uiGroupBox15.Size = new System.Drawing.Size(908, 687);
            this.uiGroupBox15.TabIndex = 0;
            // 
            // txtPhanLoaiNopThue
            // 
            this.txtPhanLoaiNopThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhanLoaiNopThue.Location = new System.Drawing.Point(239, 23);
            this.txtPhanLoaiNopThue.MaxLength = 255;
            this.txtPhanLoaiNopThue.Name = "txtPhanLoaiNopThue";
            this.txtPhanLoaiNopThue.Size = new System.Drawing.Size(49, 21);
            this.txtPhanLoaiNopThue.TabIndex = 50;
            this.txtPhanLoaiNopThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhanLoaiNopThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrMaTTCuaSoTienBaoLanh
            // 
            this.ctrMaTTCuaSoTienBaoLanh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTCuaSoTienBaoLanh.Code = "";
            this.ctrMaTTCuaSoTienBaoLanh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTCuaSoTienBaoLanh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaTTCuaSoTienBaoLanh.IsValidate = true;
            this.ctrMaTTCuaSoTienBaoLanh.Location = new System.Drawing.Point(294, 117);
            this.ctrMaTTCuaSoTienBaoLanh.Name = "ctrMaTTCuaSoTienBaoLanh";
            this.ctrMaTTCuaSoTienBaoLanh.Name_VN = "";
            this.ctrMaTTCuaSoTienBaoLanh.SetValidate = false;
            this.ctrMaTTCuaSoTienBaoLanh.ShowColumnCode = true;
            this.ctrMaTTCuaSoTienBaoLanh.ShowColumnName = false;
            this.ctrMaTTCuaSoTienBaoLanh.Size = new System.Drawing.Size(66, 26);
            this.ctrMaTTCuaSoTienBaoLanh.TabIndex = 49;
            this.ctrMaTTCuaSoTienBaoLanh.TagName = "";
            this.ctrMaTTCuaSoTienBaoLanh.WhereCondition = "";
            // 
            // ctrMaTTTongTienThueXuatKhau
            // 
            this.ctrMaTTTongTienThueXuatKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTongTienThueXuatKhau.Code = "";
            this.ctrMaTTTongTienThueXuatKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTongTienThueXuatKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.E;
            this.ctrMaTTTongTienThueXuatKhau.IsValidate = true;
            this.ctrMaTTTongTienThueXuatKhau.Location = new System.Drawing.Point(294, 52);
            this.ctrMaTTTongTienThueXuatKhau.Name = "ctrMaTTTongTienThueXuatKhau";
            this.ctrMaTTTongTienThueXuatKhau.Name_VN = "";
            this.ctrMaTTTongTienThueXuatKhau.SetValidate = false;
            this.ctrMaTTTongTienThueXuatKhau.ShowColumnCode = true;
            this.ctrMaTTTongTienThueXuatKhau.ShowColumnName = false;
            this.ctrMaTTTongTienThueXuatKhau.Size = new System.Drawing.Size(66, 26);
            this.ctrMaTTTongTienThueXuatKhau.TabIndex = 49;
            this.ctrMaTTTongTienThueXuatKhau.TagName = "";
            this.ctrMaTTTongTienThueXuatKhau.WhereCondition = "";
            // 
            // uiGroupBox17
            // 
            this.uiGroupBox17.Controls.Add(this.grListTyGia);
            this.uiGroupBox17.Location = new System.Drawing.Point(378, 50);
            this.uiGroupBox17.Name = "uiGroupBox17";
            this.uiGroupBox17.Size = new System.Drawing.Size(393, 100);
            this.uiGroupBox17.TabIndex = 48;
            this.uiGroupBox17.Text = "Tỷ giá";
            this.uiGroupBox17.VisualStyleManager = this.vsmMain;
            // 
            // grListTyGia
            // 
            this.grListTyGia.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListTyGia.ColumnAutoResize = true;
            grListTyGia_DesignTimeLayout.LayoutString = resources.GetString("grListTyGia_DesignTimeLayout.LayoutString");
            this.grListTyGia.DesignTimeLayout = grListTyGia_DesignTimeLayout;
            this.grListTyGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListTyGia.GroupByBoxVisible = false;
            this.grListTyGia.Location = new System.Drawing.Point(3, 17);
            this.grListTyGia.Name = "grListTyGia";
            this.grListTyGia.Size = new System.Drawing.Size(387, 80);
            this.grListTyGia.TabIndex = 40;
            this.grListTyGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListTyGia.VisualStyleManager = this.vsmMain;
            // 
            // txtTongSoDongHangCuaToKhai
            // 
            this.txtTongSoDongHangCuaToKhai.BackColor = System.Drawing.Color.White;
            this.txtTongSoDongHangCuaToKhai.DecimalDigits = 20;
            this.txtTongSoDongHangCuaToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoDongHangCuaToKhai.FormatString = "G20";
            this.txtTongSoDongHangCuaToKhai.Location = new System.Drawing.Point(719, 21);
            this.txtTongSoDongHangCuaToKhai.Name = "txtTongSoDongHangCuaToKhai";
            this.txtTongSoDongHangCuaToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoDongHangCuaToKhai.Size = new System.Drawing.Size(52, 21);
            this.txtTongSoDongHangCuaToKhai.TabIndex = 41;
            this.txtTongSoDongHangCuaToKhai.Text = "0";
            this.txtTongSoDongHangCuaToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoDongHangCuaToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoDongHangCuaToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongSoTrangCuaToKhai
            // 
            this.txtTongSoTrangCuaToKhai.BackColor = System.Drawing.Color.White;
            this.txtTongSoTrangCuaToKhai.DecimalDigits = 20;
            this.txtTongSoTrangCuaToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTrangCuaToKhai.FormatString = "G20";
            this.txtTongSoTrangCuaToKhai.Location = new System.Drawing.Point(507, 20);
            this.txtTongSoTrangCuaToKhai.Name = "txtTongSoTrangCuaToKhai";
            this.txtTongSoTrangCuaToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTrangCuaToKhai.Size = new System.Drawing.Size(52, 21);
            this.txtTongSoTrangCuaToKhai.TabIndex = 42;
            this.txtTongSoTrangCuaToKhai.Text = "0";
            this.txtTongSoTrangCuaToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTrangCuaToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTrangCuaToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(566, 25);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(154, 13);
            this.label68.TabIndex = 45;
            this.label68.Text = "Tổng số dòng hàng của tờ khai";
            // 
            // txtSoTienBaoLanh
            // 
            this.txtSoTienBaoLanh.BackColor = System.Drawing.Color.White;
            this.txtSoTienBaoLanh.DecimalDigits = 20;
            this.txtSoTienBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienBaoLanh.FormatString = "G20";
            this.txtSoTienBaoLanh.Location = new System.Drawing.Point(157, 120);
            this.txtSoTienBaoLanh.Name = "txtSoTienBaoLanh";
            this.txtSoTienBaoLanh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienBaoLanh.Size = new System.Drawing.Size(131, 21);
            this.txtSoTienBaoLanh.TabIndex = 38;
            this.txtSoTienBaoLanh.Text = "0";
            this.txtSoTienBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienBaoLanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.BackColor = System.Drawing.Color.Transparent;
            this.label70.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(21, 25);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(212, 13);
            this.label70.TabIndex = 46;
            this.label70.Text = "Mã phân loại nhập liệu cho Tổng giá cơ bản";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(375, 25);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(132, 13);
            this.label67.TabIndex = 46;
            this.label67.Text = "Tổng số  trang của tờ khai";
            // 
            // txtTongSoTienLePhi
            // 
            this.txtTongSoTienLePhi.BackColor = System.Drawing.Color.White;
            this.txtTongSoTienLePhi.DecimalDigits = 20;
            this.txtTongSoTienLePhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTienLePhi.FormatString = "G20";
            this.txtTongSoTienLePhi.Location = new System.Drawing.Point(157, 87);
            this.txtTongSoTienLePhi.Name = "txtTongSoTienLePhi";
            this.txtTongSoTienLePhi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTienLePhi.Size = new System.Drawing.Size(131, 21);
            this.txtTongSoTienLePhi.TabIndex = 40;
            this.txtTongSoTienLePhi.Text = "0";
            this.txtTongSoTienLePhi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTienLePhi.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTienLePhi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.BackColor = System.Drawing.Color.Transparent;
            this.label69.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(294, 91);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(28, 13);
            this.label69.TabIndex = 47;
            this.label69.Text = "VNĐ";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(21, 124);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(84, 13);
            this.label61.TabIndex = 47;
            this.label61.Text = "Số tiền bảo lãnh";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(21, 91);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(94, 13);
            this.label60.TabIndex = 43;
            this.label60.Text = "Tổng số tiền lệ phí";
            // 
            // txtTongSoTienThueXuatKhau
            // 
            this.txtTongSoTienThueXuatKhau.BackColor = System.Drawing.Color.White;
            this.txtTongSoTienThueXuatKhau.DecimalDigits = 20;
            this.txtTongSoTienThueXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTienThueXuatKhau.FormatString = "G20";
            this.txtTongSoTienThueXuatKhau.Location = new System.Drawing.Point(157, 55);
            this.txtTongSoTienThueXuatKhau.Name = "txtTongSoTienThueXuatKhau";
            this.txtTongSoTienThueXuatKhau.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTienThueXuatKhau.Size = new System.Drawing.Size(131, 21);
            this.txtTongSoTienThueXuatKhau.TabIndex = 39;
            this.txtTongSoTienThueXuatKhau.Text = "0";
            this.txtTongSoTienThueXuatKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTienThueXuatKhau.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTienThueXuatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Tổng tiền thuế xuất khẩu";
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdChiThiHQ,
            this.cmdGiayPhep,
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdPhanHoi,
            this.cmdDinhKemDT,
            this.cmdTrungChuyen,
            this.cmdContainer,
            this.cmdIN,
            this.cmdKetQuaTraVe2});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.TopRebar1;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdVanDon1,
            this.cmDinhKemDT1,
            this.cmdTrungChuyen1,
            this.cmdContainer1,
            this.cmdChiThiHQ1,
            this.Separator1,
            this.cmdLuu1,
            this.cmdKhaiBao1,
            this.cmdIN1,
            this.Separator4,
            this.cmdKetQuaTraVe1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1116, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdVanDon1
            // 
            this.cmdVanDon1.Key = "cmdGiayPhep";
            this.cmdVanDon1.Name = "cmdVanDon1";
            // 
            // cmDinhKemDT1
            // 
            this.cmDinhKemDT1.Key = "cmdDinhKemDT";
            this.cmDinhKemDT1.Name = "cmDinhKemDT1";
            // 
            // cmdTrungChuyen1
            // 
            this.cmdTrungChuyen1.Key = "cmdTrungChuyen";
            this.cmdTrungChuyen1.Name = "cmdTrungChuyen1";
            // 
            // cmdContainer1
            // 
            this.cmdContainer1.Key = "cmdContainer";
            this.cmdContainer1.Name = "cmdContainer1";
            // 
            // cmdChiThiHQ1
            // 
            this.cmdChiThiHQ1.Key = "cmdChiThiHQ";
            this.cmdChiThiHQ1.Name = "cmdChiThiHQ1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdIN1
            // 
            this.cmdIN1.Key = "cmdIN";
            this.cmdIN1.Name = "cmdIN1";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdKetQuaTraVe1
            // 
            this.cmdKetQuaTraVe1.Key = "cmdKetQuaTraVe";
            this.cmdKetQuaTraVe1.Name = "cmdKetQuaTraVe1";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdChiThiHQ
            // 
            this.cmdChiThiHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChiThiHQ.Icon")));
            this.cmdChiThiHQ.Key = "cmdChiThiHQ";
            this.cmdChiThiHQ.Name = "cmdChiThiHQ";
            this.cmdChiThiHQ.Text = "Chỉ thị HQ";
            // 
            // cmdGiayPhep
            // 
            this.cmdGiayPhep.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdGiayPhep.Icon")));
            this.cmdGiayPhep.Key = "cmdGiayPhep";
            this.cmdGiayPhep.Name = "cmdGiayPhep";
            this.cmdGiayPhep.Text = "Giấy phép";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLuu.Icon")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai Báo";
            // 
            // cmdPhanHoi
            // 
            this.cmdPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPhanHoi.Icon")));
            this.cmdPhanHoi.Key = "cmdPhanHoi";
            this.cmdPhanHoi.Name = "cmdPhanHoi";
            this.cmdPhanHoi.Text = "Nhận phản hồi";
            // 
            // cmdDinhKemDT
            // 
            this.cmdDinhKemDT.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdDinhKemDT.Icon")));
            this.cmdDinhKemDT.Key = "cmdDinhKemDT";
            this.cmdDinhKemDT.Name = "cmdDinhKemDT";
            this.cmdDinhKemDT.Text = "Đính kèm ĐT";
            // 
            // cmdTrungChuyen
            // 
            this.cmdTrungChuyen.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTrungChuyen.Icon")));
            this.cmdTrungChuyen.Key = "cmdTrungChuyen";
            this.cmdTrungChuyen.Name = "cmdTrungChuyen";
            this.cmdTrungChuyen.Text = "Trung Chuyển";
            // 
            // cmdContainer
            // 
            this.cmdContainer.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdContainer.Icon")));
            this.cmdContainer.Key = "cmdContainer";
            this.cmdContainer.Name = "cmdContainer";
            this.cmdContainer.Text = "Container";
            // 
            // cmdIN
            // 
            this.cmdIN.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdIN.Icon")));
            this.cmdIN.Key = "cmdIN";
            this.cmdIN.Name = "cmdIN";
            this.cmdIN.Text = "In ấn";
            // 
            // cmdKetQuaTraVe2
            // 
            this.cmdKetQuaTraVe2.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaTraVe2.Image")));
            this.cmdKetQuaTraVe2.Key = "cmdKetQuaTraVe";
            this.cmdKetQuaTraVe2.Name = "cmdKetQuaTraVe2";
            this.cmdKetQuaTraVe2.Text = "Thông tin từ HQ";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1116, 28);
            // 
            // clcThoiHanTaiNhapTaiXuat
            // 
            this.clcThoiHanTaiNhapTaiXuat.Location = new System.Drawing.Point(323, 70);
            this.clcThoiHanTaiNhapTaiXuat.Name = "clcThoiHanTaiNhapTaiXuat";
            this.clcThoiHanTaiNhapTaiXuat.ReadOnly = false;
            this.clcThoiHanTaiNhapTaiXuat.Size = new System.Drawing.Size(89, 21);
            this.clcThoiHanTaiNhapTaiXuat.TabIndex = 52;
            this.clcThoiHanTaiNhapTaiXuat.TagName = "";
            this.clcThoiHanTaiNhapTaiXuat.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // VNACC_ToKhaiMauDichXuatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1116, 743);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ToKhaiMauDichXuatForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai xuất khẩu";
            this.Load += new System.EventHandler(this.VNACC_ToKhaiMauDichXuatForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).EndInit();
            this.grbDonVi.ResumeLayout(false);
            this.grbDonVi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).EndInit();
            this.grbDoiTac.ResumeLayout(false);
            this.grbDoiTac.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            this.uiGroupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            this.uiGroupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).EndInit();
            this.uiGroupBox14.ResumeLayout(false);
            this.uiGroupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).EndInit();
            this.uiGroupBox15.ResumeLayout(false);
            this.uiGroupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).EndInit();
            this.uiGroupBox17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListTyGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox grbDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonVi;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDonVi;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonVi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox grbDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoiTac;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoiTac;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac4;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDaiLyHQ;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDonVi;
        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieuCTHanMuc;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCTHanMuc;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNHTraThueThay;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieuCTBaoLanh;
        private System.Windows.Forms.Label label34;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCTBaoLanh;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNHBaoLanh;
        private System.Windows.Forms.Label label38;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhanLoaiKhongQDVND;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQuanLyNoiBoDN;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdVanDon1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHQ;
        private Janus.Windows.UI.CommandBars.UICommand cmdGiayPhep;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanHoi;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox12;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGiaHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label56;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuKyHieu;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenPTVC;
        private System.Windows.Forms.Label label57;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox14;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoTKChiaNho;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoNhanhToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiDauTien;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiTNTX;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhanHD;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHQ1;
        private Janus.Windows.UI.CommandBars.UICommand cmDinhKemDT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTrungChuyen1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDinhKemDT;
        private Janus.Windows.UI.CommandBars.UICommand cmdTrungChuyen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaLoaiHinh;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiHH;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrNguoiNopThue;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhuongThucVT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTHoaDon;
       
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPhanLoaiHD;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTrongLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTSoLuong;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPTVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaNuocDoiTac;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaXDThoiHanNopThue;
        private Janus.Windows.UI.CommandBars.UICommand cmdContainer1;
        private Janus.Windows.UI.CommandBars.UICommand cmdContainer;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanhBL;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNanPhatHanhHM;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThue;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox15;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoDongHangCuaToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoTrangCuaToKhai;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienBaoLanh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoTienLePhi;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoTienThueXuatKhau;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox17;
        private Janus.Windows.GridEX.GridEX grListTyGia;
        private System.Windows.Forms.Label label62;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSoThueDaiDien;
        private System.Windows.Forms.Label label63;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTCuaSoTienBaoLanh;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTongTienThueXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDaiLyHaiQuan;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label69;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhanLoaiNopThue;
        private System.Windows.Forms.Label label70;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongHeSoPhanBoTG;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDangKy;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcThoiHanTaiNhap;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHangDen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayPhatHanhHD;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayKhoiHanhVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcThoiHanTaiNhapTaiXuat;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTriGiaTinhThue;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDieuKienGiaHD;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPhanLoaiGiaHD;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPhuongThucTT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrDiaDiemXepHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrDiaDiemNhanHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDDLuuKho;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDiaDiemDichVC;
        private System.Windows.Forms.Label lblMaPhanLoaiTongGiaCoBan;
        private System.Windows.Forms.Label lblPhanLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy ctrNhomXuLyHS;
        private Janus.Windows.UI.CommandBars.UICommand cmdIN;
        private Janus.Windows.UI.CommandBars.UICommand cmdIN1;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand Separator4;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaTraVe1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaTraVe2;
    }
}