using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
using Company.BLL.SXXK;
using Company.Interface.Report.SXXK;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK01XGCForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKToKhaiNhapCollection bkTKNCollection = new BKToKhaiNhapCollection();

        public BK01XGCForm()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            int i = this.HSTL.getBKToKhaiNhap();
            if (i < 0)
            {
                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                bk.MaBangKe = "DTLTKN";
                bk.TenBangKe = "DTLTKN";
                bk.STTHang = this.HSTL.BKCollection.Count + 1;
                bk.bkTKNCollection = this.bkTKNCollection;
                this.HSTL.BKCollection.Add(bk);
            }
            else
            {
                this.HSTL.BKCollection[i].bkTKNCollection = this.bkTKNCollection;
            }
            BK02XGCForm bk02 = new BK02XGCForm();
            bk02.MdiParent = this.ParentForm;
            bk02.HSTL = this.HSTL;
            bk02.Show();
            this.Close();
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            if (this.bkTKNCollection.Count == 0)
            {
                //ShowMessage("Bạn hãy nhập danh sách tờ khai nhập để tạo mới hồ sơ.", false);
                MLMessages("Bạn hãy nhập danh sách tờ khai nhập để tạo mới hồ sơ.", "MSG_THK97", "", false);
            }
            else
            {
                try
                {
                    int i = this.HSTL.getBKToKhaiNhap();
                    if (i < 0)
                    {
                        BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                        bk.MaBangKe = "DTLTKN";
                        bk.TenBangKe = "DTLTKN";
                        bk.STTHang = this.HSTL.BKCollection.Count + 1;
                        bk.bkTKNCollection = this.bkTKNCollection;
                        this.HSTL.BKCollection.Add(bk);
                    }
                    else
                    {
                        this.HSTL.BKCollection[i].bkTKNCollection = this.bkTKNCollection;
                    }
                    this.HSTL.LanThanhLy = new HoSoThanhLyDangKy().GetLanThanhLyMoiNhat(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                    //TaoBangKeToKhaiXGC();
                    if (this.HSTL.InsertUpdateFull())
                    {
                        //ShowMessage("Hồ sơ thanh khoản đã tạo thành công.", false);
                        MLMessages("Hồ sơ thanh khoản đã tạo thành công.", "MSG_THK98","", false);
                        show_CapNhatHoSoThanhLyForm(this.HSTL);
                        this.Close();
                    }
                    else
                        //ShowMessage("Tạo mới hồ sơ không thành công.", false);
                        MLMessages("Tạo mới hồ sơ không thành công.", "MSG_THK83", "", false);
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                }
            }

        }

        private void TaoBangKeToKhaiXGC()
        {
            BKNPLXuatGiaCongCollection bktkXGCCollection = new BKNPLXuatGiaCongCollection();
            DataTable dtPhanBo = new ToKhaiMauDich().SelectPhanBoCuaToKhaiXuat(this.HSTL.BKCollection[0].bkTKXColletion);
            foreach (DataRow dr in dtPhanBo.Rows) 
            {
                BKNPLXuatGiaCong bk = new BKNPLXuatGiaCong();
                bk.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                bk.MaLoaiHinh = dr["MaLoaiHinhNhap"].ToString();
                bk.NamDangKy = Convert.ToInt16(dr["NamDangKyNhap"]);
                bk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                bk.NgayDangKy = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                bk.SoToKhaiXuat = tkmd.SoToKhai = Convert.ToInt32(dr["SoToKhaiXuat"]); ;
                bk.MaLoaiHinhXuat = tkmd.MaLoaiHinh = dr["MaLoaiHinhXuat"].ToString();
                bk.NamDangKyXuat = tkmd.NamDangKy = Convert.ToInt16(dr["NamDangKyXuat"]);
                bk.MaHaiQuanXuat = tkmd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                tkmd.Load();
                bk.NgayDangKyXuat = tkmd.NgayDangKy;
                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                npl.Ma = dr["MaNPL"].ToString();
                npl.Load();
                bk.MaNPL = npl.Ma;
                bk.TenNPL = npl.Ten;
                bk.DVT_ID = npl.DVT_ID;
                bk.TenDVT = DonViTinh_GetName(npl.DVT_ID);
                bk.LuongXuat = Convert.ToDecimal(dr["LuongPhanBo"]);
                bktkXGCCollection.Add(bk);
            }
            BangKeHoSoThanhLy bktl = new BangKeHoSoThanhLy();
            bktl.MaterID = this.HSTL.ID;
            bktl.STTHang = this.HSTL.BKCollection.Count + 1;
            bktl.MaBangKe = "DTLNPLXGC";
            bktl.TenBangKe = "DTLNPLXGC";
            bktl.bkNPLXGCCollection = bktkXGCCollection;
            this.HSTL.BKCollection.Add(bktl);
        }
        private void show_CapNhatHoSoThanhLyForm(HoSoThanhLyDangKy HSTL)
        {
            Form[] forms = this.ParentForm.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CapNhatHoSoThanhLyForm"))
                {
                    forms[i].Close();
                }
            }
            CapNhatHoSoThanhLyForm capNhatHSTLForm = new CapNhatHoSoThanhLyForm();
            capNhatHSTLForm.HSTL = HSTL;
            capNhatHSTLForm.MdiParent = this.ParentForm;
            capNhatHSTLForm.Show();
        }
        private DateTime getMaxDateBKTKX()
        {
            int i = this.HSTL.getBKToKhaiXuat();
            //this.HSTL.BKCollection[i].LoadChiTietBangKe();
            BKToKhaiXuatCollection bkTKXCollection = this.HSTL.BKCollection[i].bkTKXColletion;
            DateTime dt = bkTKXCollection[0].NgayDangKy;
            for (int j = 1; j < bkTKXCollection.Count; j++)
            {
                if (dt < bkTKXCollection[j].NgayDangKy) dt = bkTKXCollection[j].NgayDangKy;
            }
            return dt;
        }
        private void BK01WizardForm_Load(object sender, EventArgs e)
        {
            dgTKNTL.RootTable.Columns["NgayThucNhap"].DefaultValue = DateTime.Today;
            //ctrDonViHaiQuan.Ma = this.HSTL.MaHaiQuanTiepNhan;
            this.bkTKNCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKNCuaToKhaiXuatDaPhanBo(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, this.HSTL.BKCollection[0].bkTKXColletion);
            try
            {
                dgTKNTL.DataSource = this.bkTKNCollection;
            }
            catch { }
        }

        private void dgTKNTL_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }



        private void dgTKNTL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["NgayThucNhap"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayThucNhap"].Text = "";

        }



        private void dgTKNTL_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgTKNTL.GetRow();
            BKToKhaiNhap bk = (BKToKhaiNhap)row.DataRow;
            bk.Update();
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.SoToKhai = bk.SoToKhai;
            TKMD.MaLoaiHinh = bk.MaLoaiHinh;
            TKMD.MaHaiQuan = bk.MaHaiQuan;
            TKMD.NamDangKy = bk.NamDangKy;
            TKMD.Load();
            TKMD.NGAY_THN_THX = bk.NgayThucNhap;
            TKMD.Update();
        }


    }
}