﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{//AiNPV
    public partial class TuChoiHoacCapSoTiepNhan
    {
        public static string ConfigTChHoacCapSTN( ToKhaiMauDich tkmd, int issuer, int function, int declarationOffice)
        {
            string doc = "";
            string path = "../../../SOFTECH.ECS.V3.Components/XML/KD/TuChoiHoacCapSoTiepNhan.xml";
            XDocument docXML = XDocument.Load(path);
            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tkmd != null)
                {
                    
                    IList<GiayPhep> gp = GiayPhep.SelectCollectionBy_TKMD_ID(tkmd.ID);
                    if (gp != null && gp.Count > 0)
                    {
                        Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, 0, declarationOffice);
                        foreach (XElement element in Root.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodeTuChoiHoacCapSoTiepNhan.AdditionalInformation.ToUpper()))
                                foreach (XElement AdditionalInformationChild in element.Elements())
                                {
                                    if (AdditionalInformationChild.Name.ToString().ToUpper().Equals(NodeTuChoiHoacCapSoTiepNhan.content.ToUpper()))
                                        AdditionalInformationChild.SetValue(gp[0].SoGiayPhep);
                                    if (AdditionalInformationChild.Name.ToString().ToUpper().Equals(NodeTuChoiHoacCapSoTiepNhan.statement.ToUpper()))
                                        AdditionalInformationChild.SetValue(gp[0].ID);
                                }
                        }
                    }
                    else
                    {
                        return doc; // return "";
                    }
                }
                else
                {
                    return doc; // return "";
                }
            }
            //docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();
            return doc;
        }
    }
}
