﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS.Messages.Recived
{
   public partial class VAD2AC0: BasicVNACC
    {
        public List<VAD2AC0_HANG> HangMD { get; set; }
        public void LoadVAD2AC0(string strResult)
        {
            try
            {

                this.GetObject<VAD2AC0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD2AC0, false, VAD2AC0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD2AC0.TongSoByte);
                while (true)
                {
                    VAD2AC0_HANG vad1agHang = new VAD2AC0_HANG();
                    vad1agHang.GetObject<VAD2AC0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD2AC0_HANG", false, VAD2AC0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAD2AC0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD2AC0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD2AC0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAD2AC0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}
