﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Logger;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.GC;

namespace Company.Interface.KDT.SXXK
{
    public partial class AutoSendAndFeedbackXMLForm : Company.Interface.BaseForm
    {
        public AutoSendAndFeedbackXMLForm()
        {
            InitializeComponent();
        }
        public PhuKienDangKy pkdk = new PhuKienDangKy();
        public HopDong HD = new HopDong();
        public DinhMucDangKy dmDangKy = new DinhMucDangKy();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public string LoaiChungTu = "";
        public bool isFeedBack = true;
        private bool isStop;
        public DataTable dtFeedBack;
        public string Type = "";
        public int Counter;
        private void AutoSendAndFeedbackXMLForm_Load(object sender, EventArgs e)
        {
            switch (Type)
            {
                case "HD":
                    LoadDataHĐ();
                    break;
                case "PK":
                    LoadDataPK();
                    break;
                case "DM":
                    LoadDataDM();
                    break;
                default:
                    break;
            }
            dtFeedBack = new DataTable();

            DataColumn dtColSoToKhai = new DataColumn("ID");
            dtColSoToKhai.DataType = typeof(System.String);
            DataColumn dtColNgayDangKy = new DataColumn("SoTiepNhan");
            dtColNgayDangKy.DataType = typeof(System.DateTime);
            DataColumn dtColSoTN = new DataColumn("NgayTiepNhan");
            dtColSoTN.DataType = typeof(System.String);
            DataColumn dtColNgayTN = new DataColumn("MaHaiQuan");
            dtColNgayTN.DataType = typeof(System.DateTime);
            DataColumn dtColLoaiChungTu = new DataColumn("MaDoanhNghiep");
            dtColLoaiChungTu.DataType = typeof(System.String);

            dtFeedBack.Columns.Add(dtColSoToKhai);
            dtFeedBack.Columns.Add(dtColNgayDangKy);
            dtFeedBack.Columns.Add(dtColSoTN);
            dtFeedBack.Columns.Add(dtColNgayTN);
            dtFeedBack.Columns.Add(dtColLoaiChungTu);
        }
        private void LoadDataHĐ()
        {
            try
            {
                grList.Refresh();
                grList.DataSource = HopDong.SelectCollectionDynamic("TrangThaiXuLy IN (-1,-3)", "ID");
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadDataPK()
        {
            try
            {
                grList.Refresh();
                grList.DataSource = KDT_GC_PhuKienDangKy.SelectCollectionDynamic("TrangThaiXuLy IN (-1,-3)", "ID");
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadDataDM()
        {
            try
            {
                grList.Refresh();
                grList.DataSource = KDT_GC_DinhMucDangKy.SelectCollectionDynamic("TrangThaiXuLy IN (-1,-3)", "ID");
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void btnThucHien_Click(object sender, EventArgs e)
        {
            btnThucHien.Enabled = false;
            btnDong.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void DoWork(object obj)
        {
            try
            {

                SetError(string.Empty);
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    switch (Type)
                    {
                        case "HD":
                            HD = new HopDong();
                            HD = (HopDong)item.DataRow;
                            HD.LoadCollection();
                            SendV5HĐGC(false);
                            break;
                        case "PK":
                            pkdk = new PhuKienDangKy();
                            pkdk = (PhuKienDangKy)item.DataRow;
                            HD = new HopDong();
                            HD = HopDong.Load(pkdk.HopDong_ID);
                            pkdk.LoadCollection();
                            SendV5PK();
                            break;
                        case "DM":
                            dmDangKy = new DinhMucDangKy();
                            dmDangKy = (DinhMucDangKy)item.DataRow;
                            HD = new HopDong();
                            HD = HopDong.Load(dmDangKy.ID_HopDong);
                            dmDangKy.LoadCollection();
                            SendV5ĐM(false);
                            break;
                        default:
                            break;
                    }
                    SetProcessBar((i * 100 / listChecked.Length));
                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        switch (Type)
                        {
                            case "HD":
                                LoadDataHĐ();
                                break;
                            case "PK":
                                LoadDataPK();
                                break;
                            case "DM":
                                LoadDataDM();
                                break;
                            default:
                                break;
                        }
                        btnThucHien.Enabled = true; btnDong.Enabled = true;
                        btnStop.Enabled = true;

                    }));
                }
                else
                {
                    switch (Type)
                    {
                        case "HD":
                            LoadDataHĐ();
                            break;
                        case "PK":
                            LoadDataPK();
                            break;
                        case "DM":
                            LoadDataDM();
                            break;
                        default:
                            break;
                    }
                    btnThucHien.Enabled = true; btnDong.Enabled = true; btnStop.Enabled = true;
                }
                SetProcessBar(0);
                SetError("Hoàn thành");

            }

            catch (System.Exception ex)
            {
                SetError(string.Empty);
                LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    btnStop.Enabled = true;
                    btnThucHien.Enabled = true;
                    isFeedBack = false;
                    isStop = true;
                }));
            }
            else
            {
                btnStop.Enabled = true;
                btnThucHien.Enabled = true;
                isFeedBack = false;
                isStop = true;
            }
        }
        private void BinDataFeedBack()
        {
            try
            {
                grListResult.Refresh();
                grListResult.DataSource = dtFeedBack;
                grListResult.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        #region Phụ kiện hợp đồng
        private void SendV5PK()
        {

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                    {
                        FeedBackV5PK();
                    }
                }
            }
            try
            {
                pkdk.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.GC_PhuKien pk = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferPhuKien(pkdk, HD, GlobalSettings.TEN_DON_VI);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = pkdk.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkdk.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(pkdk.MaHaiQuan).Trim() : pkdk.MaHaiQuan.Trim()
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                                    Function = pk.Function,
                                    Reference = pkdk.GUIDSTR,
                                }
                                ,
                                pk);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessagePK;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoPhuKien);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
                    sendXML.master_id = pkdk.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        DataRow dtRow = dtFeedBack.NewRow();
                        dtRow["ID"] = pkdk.ID.ToString();
                        dtRow["SoTiepNhan"] = pkdk.SoTiepNhan.ToString();
                        dtRow["NgayTiepNhan"] = pkdk.NgayTiepNhan.ToString();
                        dtRow["MaHaiQuan"] = pkdk.MaHaiQuan.ToString();
                        dtRow["MaDoanhNghiep"] = pkdk.MaDoanhNghiep.ToString();
                        dtFeedBack.Rows.Add(dtRow);
                        BinDataFeedBack();
                        pkdk.updateTrangThaiDuLieu();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        FeedBackV5PK();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        void SendMessagePK(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandlerPK),
                sender, e);
        }
        void SendHandlerPK(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.PhuKienSendHandler(pkdk, ref msgInfor, e);

        }
        private void FeedBackV5PK()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
            sendXML.master_id = pkdk.ID;
            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                Reference = pkdk.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = pkdk.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkdk.MaHaiQuan.Trim()),
                                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(pkdk.MaHaiQuan).Trim() : pkdk.MaHaiQuan.Trim()
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessagePK;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY && count <= Counter)
                    {
                        isFeedBack = true;
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        DataRow dtRow = dtFeedBack.NewRow();
                        dtRow["ID"] = pkdk.ID.ToString();
                        dtRow["SoTiepNhan"] = pkdk.SoTiepNhan.ToString();
                        dtRow["NgayTiepNhan"] = pkdk.NgayTiepNhan.ToString();
                        dtRow["MaHaiQuan"] = HD.MaHaiQuan.ToString();
                        dtRow["MaDoanhNghiep"] = pkdk.MaDoanhNghiep.ToString();
                        dtFeedBack.Rows.Add(dtRow);
                        BinDataFeedBack();
                        pkdk.updateTrangThaiDuLieu();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        pkdk.Update();
                        isFeedBack = false;
                    }
                    else
                    {
                        isFeedBack = false;
                    }
                }
            }
        }
        #endregion

        #region Hợp đồng gia công
        private void SendV5HĐGC(bool isCancel)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.HopDong;
            sendXML.master_id = HD.ID;

            if (sendXML.Load())
            {
                if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    FeedBackV5HĐGC();
                }
            }
            try
            {

                HD.LoadHD(HD.ID);
                HD.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.GC_HopDong hd = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_GC_HopDong(HD, GlobalSettings.DIA_CHI, isCancel);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = HD.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(HD.MaHaiQuan).Trim() : HD.MaHaiQuan.Trim()
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG,
                                    Function = hd.Function,
                                    Reference = HD.GUIDSTR,
                                }
                                ,
                                hd);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessageHĐGC;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    if (feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(HD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHopDong);
                        sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.HopDong;
                        sendXML.master_id = HD.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();

                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            DataRow dtRow = dtFeedBack.NewRow();
                            dtRow["ID"] = HD.ID.ToString();
                            dtRow["SoTiepNhan"] = HD.SoTiepNhan.ToString();
                            dtRow["NgayTiepNhan"] = HD.NgayTiepNhan.ToString();
                            dtRow["MaHaiQuan"] = HD.MaHaiQuan.ToString();
                            dtRow["MaDoanhNghiep"] = HD.MaDoanhNghiep.ToString();
                            dtFeedBack.Rows.Add(dtRow);
                            BinDataFeedBack();
                            HD.Update();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedBackV5HĐGC();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(HD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQTuChoiHopDong, msgInfor);
                        ShowMessageTQDT(msgInfor, false);

                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(HD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        void SendMessageHĐGC(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandlerHĐGC),
                sender, e);
        }
        private void FeedBackV5HĐGC()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.HOP_DONG_GIA_CONG,
                Reference = HD.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.HOP_DONG_GIA_CONG,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = HD.TenDoanhNghiep,
                                            Identity = HD.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan.Trim()),
                                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(HD.MaHaiQuan).Trim() : HD.MaHaiQuan.Trim()
                                              //Identity = HD.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessageHĐGC;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY && count <= Counter)
                    {
                        isFeedBack = true;
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        DataRow dtRow = dtFeedBack.NewRow();
                        dtRow["ID"] = HD.ID.ToString();
                        dtRow["SoTiepNhan"] = HD.SoTiepNhan.ToString();
                        dtRow["NgayTiepNhan"] = HD.NgayTiepNhan.ToString();
                        dtRow["MaHaiQuan"] = HD.MaHaiQuan.ToString();
                        dtRow["MaDoanhNghiep"] = HD.MaDoanhNghiep.ToString();
                        dtFeedBack.Rows.Add(dtRow);
                        BinDataFeedBack();
                        HD.Update();
                        isFeedBack = false;
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        HD.Update();
                        isFeedBack = false;
                    }
                    else
                    {
                        isFeedBack = false;
                    }
                }
            }


        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandlerHĐGC(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.HopDongSendHandler(HD, ref msgInfor, e);

        }
        /// <summary>
        /// Hủy Thông Tin đến Hải Quan
        /// </summary>
        private void CancelV5()
        {
            Company.KDT.SHARE.Components.DeclarationBase npl = Company.GC.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG, HD.GUIDSTR, HD.SoTiepNhan, HD.MaHaiQuan, HD.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = HD.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan),
                                 Identity = HD.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = HD.GUIDSTR,
                            }
                            ,
                            npl);
            SendMessageForm sendForm = new SendMessageForm();
            HD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO;
            sendForm.Send += SendMessageHĐGC;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                sendForm.Message.XmlSaveMessage(HD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHuyHopDong);
                FeedBackV5HĐGC();
                HD.Update();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }
        #endregion
        #region Định mức gia công
        private void SendV5ĐM(bool isCancel)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (sendXML.Load())
            {
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    FeedBackV5ĐM();
                }
            }
            try
            {
                dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                Company.KDT.SHARE.Components.GC_DinhMuc dm = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferDinhMuc(dmDangKy, HD, GlobalSettings.TEN_DON_VI, isCancel);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dmDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan.Trim()

                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_DINH_MUC,
                                    Function = dm.Function,
                                    Reference = dmDangKy.GUIDSTR,
                                }
                                ,
                                dm);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessageĐM;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                    sendXML.master_id = dmDangKy.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        DataRow dtRow = dtFeedBack.NewRow();
                        dtRow["ID"] = dmDangKy.ID.ToString();
                        dtRow["SoTiepNhan"] = dmDangKy.SoTiepNhan.ToString();
                        dtRow["NgayTiepNhan"] = dmDangKy.NgayTiepNhan.ToString();
                        dtRow["MaHaiQuan"] = dmDangKy.MaHaiQuan.ToString();
                        dtRow["MaDoanhNghiep"] = dmDangKy.MaDoanhNghiep.ToString();
                        dtFeedBack.Rows.Add(dtRow);       
                        BinDataFeedBack();
                        dmDangKy.Update();
                        dmDangKy.TransferGC();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        FeedBackV5ĐM();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(dmDangKy.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
        }
        void SendMessageĐM(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandlerĐM),
                sender, e);
        }
        void SendHandlerĐM(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dmDangKy, ref msgInfor, e);

        }
        private void FeedBackV5ĐM()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_DINH_MUC,
                Reference = dmDangKy.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_DINH_MUC,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = dmDangKy.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan.Trim()),
                                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan.Trim()
                                          }, subjectBase, null);
            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessageĐM;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY && count <= Counter)
                    {
                        isFeedBack = true;
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        isFeedBack = false;
                        DataRow dtRow = dtFeedBack.NewRow();
                        dtRow["ID"] = dmDangKy.ID.ToString();
                        dtRow["SoTiepNhan"] = dmDangKy.SoTiepNhan.ToString();
                        dtRow["NgayTiepNhan"] = dmDangKy.NgayTiepNhan.ToString();
                        dtRow["MaHaiQuan"] = dmDangKy.MaHaiQuan.ToString();
                        dtRow["MaDoanhNghiep"] = dmDangKy.MaDoanhNghiep.ToString();
                        dtFeedBack.Rows.Add(dtRow);
                        BinDataFeedBack();
                        dmDangKy.Update();
                        dmDangKy.TransferGC();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        dmDangKy.Update();
                        isFeedBack = false;
                    }
                    else
                    {
                        isFeedBack = false;
                    }
                }
            }
        }
        #endregion
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Type = cbStatus.SelectedValue.ToString();
                switch (Type)
                {
                    case "HD":
                        LoadDataHĐ();
                        break;
                    case "PK":
                        LoadDataPK();
                        break;
                    case "DM":
                        LoadDataDM();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtNumber_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Counter = Convert.ToInt32(txtNumber.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (e.Row.Cells["TrangThaiXuLy"].Value != null)
                    {
                        string pl = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                        if (pl == "-1")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        else if (pl == "-3")
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                    }
                    HopDong hd = new HopDong();
                    switch (Type)
                    {
                        case "HD":
                            hd = new HopDong();
                            hd.ID = (long)Convert.ToInt64(e.Row.Cells["ID"].Value);
                            hd = HopDong.Load(hd.ID);
                            e.Row.Cells["HopDong_ID"].Text = hd.SoHopDong;
                            break;
                        case "PK":
                            hd = new HopDong();
                            hd.ID = (long)Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value);
                            hd = HopDong.Load(hd.ID);
                            e.Row.Cells["HopDong_ID"].Text = hd.SoHopDong;
                            break;
                        case "DM":
                            hd = new HopDong();
                            hd.ID = (long)Convert.ToInt64(e.Row.Cells["ID_HopDong"].Value);
                            hd = HopDong.Load(hd.ID);
                            e.Row.Cells["ID_HopDong"].Text = hd.SoHopDong;
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
