using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.BLL.VNACCS
{
	public partial class MsgLog : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaNghiepVu { set; get; }
		public string TieuDeThongBao { set; get; }
		public string NoiDungThongBao { set; get; }
		public string Log_Messages { set; get; }
		public long Item_id { set; get; }
		public DateTime CreatedTime { set; get; }
		public string MessagesTag { set; get; }
		public string InputMessagesID { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<MsgLog> ConvertToCollection(IDataReader reader)
		{
			IList<MsgLog> collection = new List<MsgLog>();
			while (reader.Read())
			{
				MsgLog entity = new MsgLog();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNghiepVu"))) entity.MaNghiepVu = reader.GetString(reader.GetOrdinal("MaNghiepVu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDeThongBao"))) entity.TieuDeThongBao = reader.GetString(reader.GetOrdinal("TieuDeThongBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungThongBao"))) entity.NoiDungThongBao = reader.GetString(reader.GetOrdinal("NoiDungThongBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("Log_Messages"))) entity.Log_Messages = reader.GetString(reader.GetOrdinal("Log_Messages"));
				if (!reader.IsDBNull(reader.GetOrdinal("Item_id"))) entity.Item_id = reader.GetInt64(reader.GetOrdinal("Item_id"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedTime"))) entity.CreatedTime = reader.GetDateTime(reader.GetOrdinal("CreatedTime"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessagesTag"))) entity.MessagesTag = reader.GetString(reader.GetOrdinal("MessagesTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessagesID"))) entity.InputMessagesID = reader.GetString(reader.GetOrdinal("InputMessagesID"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<MsgLog> collection, long id)
        {
            foreach (MsgLog item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_MsgLog VALUES(@MaNghiepVu, @TieuDeThongBao, @NoiDungThongBao, @Log_Messages, @Item_id, @CreatedTime, @MessagesTag, @InputMessagesID, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_MsgLog SET MaNghiepVu = @MaNghiepVu, TieuDeThongBao = @TieuDeThongBao, NoiDungThongBao = @NoiDungThongBao, Log_Messages = @Log_Messages, Item_id = @Item_id, CreatedTime = @CreatedTime, MessagesTag = @MessagesTag, InputMessagesID = @InputMessagesID, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_MsgLog WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNghiepVu", SqlDbType.VarChar, "MaNghiepVu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDeThongBao", SqlDbType.NVarChar, "TieuDeThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungThongBao", SqlDbType.NVarChar, "NoiDungThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Log_Messages", SqlDbType.NVarChar, "Log_Messages", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Item_id", SqlDbType.BigInt, "Item_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedTime", SqlDbType.DateTime, "CreatedTime", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessagesTag", SqlDbType.VarChar, "MessagesTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessagesID", SqlDbType.VarChar, "InputMessagesID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.NChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNghiepVu", SqlDbType.VarChar, "MaNghiepVu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDeThongBao", SqlDbType.NVarChar, "TieuDeThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungThongBao", SqlDbType.NVarChar, "NoiDungThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Log_Messages", SqlDbType.NVarChar, "Log_Messages", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Item_id", SqlDbType.BigInt, "Item_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedTime", SqlDbType.DateTime, "CreatedTime", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessagesTag", SqlDbType.VarChar, "MessagesTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessagesID", SqlDbType.VarChar, "InputMessagesID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.NChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_MsgLog VALUES(@MaNghiepVu, @TieuDeThongBao, @NoiDungThongBao, @Log_Messages, @Item_id, @CreatedTime, @MessagesTag, @InputMessagesID, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_MsgLog SET MaNghiepVu = @MaNghiepVu, TieuDeThongBao = @TieuDeThongBao, NoiDungThongBao = @NoiDungThongBao, Log_Messages = @Log_Messages, Item_id = @Item_id, CreatedTime = @CreatedTime, MessagesTag = @MessagesTag, InputMessagesID = @InputMessagesID, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_MsgLog WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNghiepVu", SqlDbType.VarChar, "MaNghiepVu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDeThongBao", SqlDbType.NVarChar, "TieuDeThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungThongBao", SqlDbType.NVarChar, "NoiDungThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Log_Messages", SqlDbType.NVarChar, "Log_Messages", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Item_id", SqlDbType.BigInt, "Item_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedTime", SqlDbType.DateTime, "CreatedTime", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessagesTag", SqlDbType.VarChar, "MessagesTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessagesID", SqlDbType.VarChar, "InputMessagesID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.NChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNghiepVu", SqlDbType.VarChar, "MaNghiepVu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDeThongBao", SqlDbType.NVarChar, "TieuDeThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungThongBao", SqlDbType.NVarChar, "NoiDungThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Log_Messages", SqlDbType.NVarChar, "Log_Messages", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Item_id", SqlDbType.BigInt, "Item_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedTime", SqlDbType.DateTime, "CreatedTime", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessagesTag", SqlDbType.VarChar, "MessagesTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessagesID", SqlDbType.VarChar, "InputMessagesID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.NChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static MsgLog Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_MsgLog_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<MsgLog> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<MsgLog> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<MsgLog> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_MsgLog_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_MsgLog_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertMsgLog(string maNghiepVu, string tieuDeThongBao, string noiDungThongBao, string log_Messages, long item_id, DateTime createdTime, string messagesTag, string inputMessagesID, string indexTag)
		{
			MsgLog entity = new MsgLog();	
			entity.MaNghiepVu = maNghiepVu;
			entity.TieuDeThongBao = tieuDeThongBao;
			entity.NoiDungThongBao = noiDungThongBao;
			entity.Log_Messages = log_Messages;
			entity.Item_id = item_id;
			entity.CreatedTime = createdTime;
			entity.MessagesTag = messagesTag;
			entity.InputMessagesID = inputMessagesID;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_MsgLog_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaNghiepVu", SqlDbType.VarChar, MaNghiepVu);
			db.AddInParameter(dbCommand, "@TieuDeThongBao", SqlDbType.NVarChar, TieuDeThongBao);
			db.AddInParameter(dbCommand, "@NoiDungThongBao", SqlDbType.NVarChar, NoiDungThongBao);
			db.AddInParameter(dbCommand, "@Log_Messages", SqlDbType.NVarChar, Log_Messages);
			db.AddInParameter(dbCommand, "@Item_id", SqlDbType.BigInt, Item_id);
			db.AddInParameter(dbCommand, "@CreatedTime", SqlDbType.DateTime, CreatedTime.Year <= 1753 ? DBNull.Value : (object) CreatedTime);
			db.AddInParameter(dbCommand, "@MessagesTag", SqlDbType.VarChar, MessagesTag);
			db.AddInParameter(dbCommand, "@InputMessagesID", SqlDbType.VarChar, InputMessagesID);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.NChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<MsgLog> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MsgLog item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateMsgLog(long id, string maNghiepVu, string tieuDeThongBao, string noiDungThongBao, string log_Messages, long item_id, DateTime createdTime, string messagesTag, string inputMessagesID, string indexTag)
		{
			MsgLog entity = new MsgLog();			
			entity.ID = id;
			entity.MaNghiepVu = maNghiepVu;
			entity.TieuDeThongBao = tieuDeThongBao;
			entity.NoiDungThongBao = noiDungThongBao;
			entity.Log_Messages = log_Messages;
			entity.Item_id = item_id;
			entity.CreatedTime = createdTime;
			entity.MessagesTag = messagesTag;
			entity.InputMessagesID = inputMessagesID;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_MsgLog_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNghiepVu", SqlDbType.VarChar, MaNghiepVu);
			db.AddInParameter(dbCommand, "@TieuDeThongBao", SqlDbType.NVarChar, TieuDeThongBao);
			db.AddInParameter(dbCommand, "@NoiDungThongBao", SqlDbType.NVarChar, NoiDungThongBao);
			db.AddInParameter(dbCommand, "@Log_Messages", SqlDbType.NVarChar, Log_Messages);
			db.AddInParameter(dbCommand, "@Item_id", SqlDbType.BigInt, Item_id);
			db.AddInParameter(dbCommand, "@CreatedTime", SqlDbType.DateTime, CreatedTime.Year <= 1753 ? DBNull.Value : (object) CreatedTime);
			db.AddInParameter(dbCommand, "@MessagesTag", SqlDbType.VarChar, MessagesTag);
			db.AddInParameter(dbCommand, "@InputMessagesID", SqlDbType.VarChar, InputMessagesID);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.NChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<MsgLog> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MsgLog item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateMsgLog(long id, string maNghiepVu, string tieuDeThongBao, string noiDungThongBao, string log_Messages, long item_id, DateTime createdTime, string messagesTag, string inputMessagesID, string indexTag)
		{
			MsgLog entity = new MsgLog();			
			entity.ID = id;
			entity.MaNghiepVu = maNghiepVu;
			entity.TieuDeThongBao = tieuDeThongBao;
			entity.NoiDungThongBao = noiDungThongBao;
			entity.Log_Messages = log_Messages;
			entity.Item_id = item_id;
			entity.CreatedTime = createdTime;
			entity.MessagesTag = messagesTag;
			entity.InputMessagesID = inputMessagesID;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_MsgLog_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNghiepVu", SqlDbType.VarChar, MaNghiepVu);
			db.AddInParameter(dbCommand, "@TieuDeThongBao", SqlDbType.NVarChar, TieuDeThongBao);
			db.AddInParameter(dbCommand, "@NoiDungThongBao", SqlDbType.NVarChar, NoiDungThongBao);
			db.AddInParameter(dbCommand, "@Log_Messages", SqlDbType.NVarChar, Log_Messages);
			db.AddInParameter(dbCommand, "@Item_id", SqlDbType.BigInt, Item_id);
			db.AddInParameter(dbCommand, "@CreatedTime", SqlDbType.DateTime, CreatedTime.Year <= 1753 ? DBNull.Value : (object) CreatedTime);
			db.AddInParameter(dbCommand, "@MessagesTag", SqlDbType.VarChar, MessagesTag);
			db.AddInParameter(dbCommand, "@InputMessagesID", SqlDbType.VarChar, InputMessagesID);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.NChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<MsgLog> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MsgLog item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteMsgLog(long id)
		{
			MsgLog entity = new MsgLog();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_MsgLog_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<MsgLog> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MsgLog item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}