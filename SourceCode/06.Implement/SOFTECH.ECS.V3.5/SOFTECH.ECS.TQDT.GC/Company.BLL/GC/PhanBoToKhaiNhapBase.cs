using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.GC.BLL.GC
{
    public partial class PhanBoToKhaiNhap
    {
        #region Private members.

        protected long _ID;
        protected int _SoToKhaiNhap;
        protected decimal _SoTKNVNACCS;
        protected string _MaLoaiHinhNhap = string.Empty;
        protected string _MaHaiQuanNhap = string.Empty;
        protected short _NamDangKyNhap;
        protected string _MaNPL = string.Empty;
        protected decimal _DinhMucChung;
        protected double _LuongTonDau;
        protected double _LuongPhanBo;
        protected double _LuongCungUng;
        protected double _LuongTonCuoi;
        protected long _TKXuat_ID;
        protected long _SoTKXVNACCS;
        protected string _MaDoanhNghiep = string.Empty;
        protected int _MuaVN;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public int SoToKhaiNhap
        {
            set { this._SoToKhaiNhap = value; }
            get { return this._SoToKhaiNhap; }
        }
        public decimal SoTKNVNACCS
        {
            set { this._SoTKNVNACCS = value; }
            get { return this._SoTKNVNACCS; }
        }
        public string MaLoaiHinhNhap
        {
            set { this._MaLoaiHinhNhap = value; }
            get { return this._MaLoaiHinhNhap; }
        }

        public string MaHaiQuanNhap
        {
            set { this._MaHaiQuanNhap = value; }
            get { return this._MaHaiQuanNhap; }
        }

        public short NamDangKyNhap
        {
            set { this._NamDangKyNhap = value; }
            get { return this._NamDangKyNhap; }
        }

        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }

        public decimal DinhMucChung
        {
            set { this._DinhMucChung = value; }
            get { return this._DinhMucChung; }
        }

        public double LuongTonDau
        {
            set { this._LuongTonDau = value; }
            get { return this._LuongTonDau; }
        }

        public double LuongPhanBo
        {
            set { this._LuongPhanBo = value; }
            get { return this._LuongPhanBo; }
        }

        public double LuongCungUng
        {
            set { this._LuongCungUng = value; }
            get { return this._LuongCungUng; }
        }

        public double LuongTonCuoi
        {
            set { this._LuongTonCuoi = value; }
            get { return this._LuongTonCuoi; }
        }

        public long TKXuat_ID
        {
            set { this._TKXuat_ID = value; }
            get { return this._TKXuat_ID; }
        }
        public long SoTKXVNACCS
        {
            set { this._SoTKXVNACCS = value; }
            get { return this._SoTKXVNACCS; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public int MuaVN
        {
            set { this._MuaVN = value; }
            get { return this._MuaVN; }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static PhanBoToKhaiNhap Load(long iD)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            PhanBoToKhaiNhap entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new PhanBoToKhaiNhap();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetInt32(reader.GetOrdinal("MuaVN"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<PhanBoToKhaiNhap> SelectCollectionAll()
        {
            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();
            IDataReader reader = SelectReaderAll();
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetInt32(reader.GetOrdinal("MuaVN"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
        public static IList<PhanBoToKhaiNhap> SelectCollectionBy_HopDong_ID(long HopDong_ID)
        {
            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();
            IDataReader reader = SelectReaderBy_HopDong_ID(HopDong_ID);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTKNVNACCS"))) entity.SoTKNVNACCS = reader.GetDecimal(reader.GetOrdinal("SoTKNVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTKXVNACCS"))) entity.SoTKXVNACCS = reader.GetInt64(reader.GetOrdinal("SoTKXVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetInt32(reader.GetOrdinal("MuaVN"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
        //---------------------------------------------------------------------------------------------

        public static IList<PhanBoToKhaiNhap> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetInt32(reader.GetOrdinal("MuaVN"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<PhanBoToKhaiNhap> SelectCollectionBy_TKXuat_ID(long tKXuat_ID)
        {
            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();
            IDataReader reader = SelectReaderBy_TKXuat_ID(tKXuat_ID);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetInt32(reader.GetOrdinal("MuaVN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public static IList<PhanBoToKhaiNhap> SelectCollectionBy_TKXuat_ID(long tKXuat_ID,SqlTransaction transaction)
        {
            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();
            IDataReader reader = SelectReaderBy_TKXuat_ID(tKXuat_ID,transaction);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetInt32(reader.GetOrdinal("MuaVN"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_TKXuat_ID(long tKXuat_ID)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_SelectBy_TKXuat_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKXuat_ID", SqlDbType.BigInt, tKXuat_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_Date(long IDHopDong ,DateTime dateFrom ,DateTime dateTo)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_GetByDate]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@DateFrom", SqlDbType.DateTime, dateFrom);
            db.AddInParameter(dbCommand, "@DateTo", SqlDbType.DateTime, dateTo);
            dbCommand.CommandTimeout = 6000;
            return db.ExecuteDataSet(dbCommand);
        }

                //---------------------------------------------------------------------------------------------
        public static DataSet SelectNPLXuatBy_Date(long IDHopDong ,DateTime dateFrom ,DateTime dateTo)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_SelectNPLXuat_ByDate]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@DateFrom", SqlDbType.DateTime, dateFrom);
            db.AddInParameter(dbCommand, "@DateTo", SqlDbType.DateTime, dateTo);
            dbCommand.CommandTimeout = 6000;
            return db.ExecuteDataSet(dbCommand);
        }
        //
        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }
        public static IDataReader SelectReaderBy_HopDong_ID(long HopDong_ID)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_SelectBy_HopDong_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.NVarChar, HopDong_ID);
            dbCommand.CommandTimeout = 6000;
            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_TKXuat_ID(long tKXuat_ID)
        {
            const string spName = "p_GC_PhanBoToKhaiNhap_SelectBy_TKXuat_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKXuat_ID", SqlDbType.BigInt, tKXuat_ID);

            return db.ExecuteReader(dbCommand);
        }
        public static IDataReader SelectReaderBy_TKXuat_ID(long tKXuat_ID, SqlTransaction transaction)
        {
            const string spName = "p_GC_PhanBoToKhaiNhap_SelectBy_TKXuat_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKXuat_ID", SqlDbType.BigInt, tKXuat_ID);

            return db.ExecuteReader(dbCommand,transaction);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertPhanBoToKhaiNhap(int soToKhaiNhap, string maLoaiHinhNhap, string maHaiQuanNhap, short namDangKyNhap, string maNPL, decimal dinhMucChung, double luongTonDau, double luongPhanBo, double luongCungUng, double luongTonCuoi, long tKXuat_ID, string maDoanhNghiep)
        {


            PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();
            entity.SoToKhaiNhap = soToKhaiNhap;
            entity.MaLoaiHinhNhap = maLoaiHinhNhap;
            entity.MaHaiQuanNhap = maHaiQuanNhap;
            entity.NamDangKyNhap = namDangKyNhap;
            entity.MaNPL = maNPL;
            entity.DinhMucChung = dinhMucChung;
            entity.LuongTonDau = luongTonDau;
            entity.LuongPhanBo = luongPhanBo;
            entity.LuongCungUng = luongCungUng;
            entity.LuongTonCuoi = luongTonCuoi;
            entity.TKXuat_ID = tKXuat_ID;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.MuaVN = 0;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@MaHaiQuanNhap", SqlDbType.Char, MaHaiQuanNhap);
            db.AddInParameter(dbCommand, "@NamDangKyNhap", SqlDbType.SmallInt, NamDangKyNhap);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
            db.AddInParameter(dbCommand, "@DinhMucChung", SqlDbType.Decimal, DinhMucChung);
            db.AddInParameter(dbCommand, "@LuongTonDau", SqlDbType.Float, LuongTonDau);
            db.AddInParameter(dbCommand, "@LuongPhanBo", SqlDbType.Float, LuongPhanBo);
            db.AddInParameter(dbCommand, "@LuongCungUng", SqlDbType.Float, LuongCungUng);
            db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Float, LuongTonCuoi);
            db.AddInParameter(dbCommand, "@TKXuat_ID", SqlDbType.BigInt, TKXuat_ID);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MuaVN", SqlDbType.Int, MuaVN);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<PhanBoToKhaiNhap> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiNhap item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdatePhanBoToKhaiNhap(long iD, int soToKhaiNhap, string maLoaiHinhNhap, string maHaiQuanNhap, short namDangKyNhap, string maNPL, decimal dinhMucChung, double luongTonDau, double luongPhanBo, double luongCungUng, double luongTonCuoi, long tKXuat_ID, string maDoanhNghiep)
        {
            PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();
            entity.SoToKhaiNhap = soToKhaiNhap;
            entity.MaLoaiHinhNhap = maLoaiHinhNhap;
            entity.MaHaiQuanNhap = maHaiQuanNhap;
            entity.NamDangKyNhap = namDangKyNhap;
            entity.MaNPL = maNPL;
            entity.DinhMucChung = dinhMucChung;
            entity.LuongTonDau = luongTonDau;
            entity.LuongPhanBo = luongPhanBo;
            entity.LuongCungUng = luongCungUng;
            entity.LuongTonCuoi = luongTonCuoi;
            entity.TKXuat_ID = tKXuat_ID;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.MuaVN = 0;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_GC_PhanBoToKhaiNhap_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@MaHaiQuanNhap", SqlDbType.Char, MaHaiQuanNhap);
            db.AddInParameter(dbCommand, "@NamDangKyNhap", SqlDbType.SmallInt, NamDangKyNhap);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
            db.AddInParameter(dbCommand, "@DinhMucChung", SqlDbType.Decimal, DinhMucChung);
            db.AddInParameter(dbCommand, "@LuongTonDau", SqlDbType.Float, LuongTonDau);
            db.AddInParameter(dbCommand, "@LuongPhanBo", SqlDbType.Float, LuongPhanBo);
            db.AddInParameter(dbCommand, "@LuongCungUng", SqlDbType.Float, LuongCungUng);
            db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Float, LuongTonCuoi);
            db.AddInParameter(dbCommand, "@TKXuat_ID", SqlDbType.BigInt, TKXuat_ID);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MuaVN", SqlDbType.Int, MuaVN);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<PhanBoToKhaiNhap> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiNhap item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdatePhanBoToKhaiNhap(long iD, int soToKhaiNhap, string maLoaiHinhNhap, string maHaiQuanNhap, short namDangKyNhap, string maNPL, decimal dinhMucChung, double luongTonDau, double luongPhanBo, double luongCungUng, double luongTonCuoi, long tKXuat_ID, string maDoanhNghiep)
        {
            PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();
            entity.ID = iD;
            entity.SoToKhaiNhap = soToKhaiNhap;
            entity.MaLoaiHinhNhap = maLoaiHinhNhap;
            entity.MaHaiQuanNhap = maHaiQuanNhap;
            entity.NamDangKyNhap = namDangKyNhap;
            entity.MaNPL = maNPL;
            entity.DinhMucChung = dinhMucChung;
            entity.LuongTonDau = luongTonDau;
            entity.LuongPhanBo = luongPhanBo;
            entity.LuongCungUng = luongCungUng;
            entity.LuongTonCuoi = luongTonCuoi;
            entity.TKXuat_ID = tKXuat_ID;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.MuaVN = 0;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@MaHaiQuanNhap", SqlDbType.Char, MaHaiQuanNhap);
            db.AddInParameter(dbCommand, "@NamDangKyNhap", SqlDbType.SmallInt, NamDangKyNhap);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
            db.AddInParameter(dbCommand, "@DinhMucChung", SqlDbType.Decimal, DinhMucChung);
            db.AddInParameter(dbCommand, "@LuongTonDau", SqlDbType.Float, LuongTonDau);
            db.AddInParameter(dbCommand, "@LuongPhanBo", SqlDbType.Float, LuongPhanBo);
            db.AddInParameter(dbCommand, "@LuongCungUng", SqlDbType.Float, LuongCungUng);
            db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Float, LuongTonCuoi);
            db.AddInParameter(dbCommand, "@TKXuat_ID", SqlDbType.BigInt, TKXuat_ID);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MuaVN", SqlDbType.Int, MuaVN);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static int Update_MuaVN(int SoToKhaiNhap, int MuaVN)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_Update_MuaVN]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@MuaVN", SqlDbType.Int, MuaVN);
            
            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<PhanBoToKhaiNhap> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiNhap item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeletePhanBoToKhaiNhap(long iD)
        {
            PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKXuat_ID(long tKXuat_ID)
        {
            string spName = "[dbo].[p_GC_PhanBoToKhaiNhap_DeleteBy_TKXuat_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKXuat_ID", SqlDbType.BigInt, tKXuat_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<PhanBoToKhaiNhap> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiNhap item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}