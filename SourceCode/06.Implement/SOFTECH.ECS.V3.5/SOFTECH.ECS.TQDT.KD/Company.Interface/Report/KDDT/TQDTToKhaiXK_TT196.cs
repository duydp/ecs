using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.Utils;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.Utils;
#endif
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace Company.Interface.Report
{
    public partial class TQDTToKhaiXK_TT196 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //public Company.Interface.Report.ReportViewTKXTQDTFormTT196 report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        int sTT_Container = 1;
        public bool isCuaKhau = true;
        private float fontReport;
        public bool inTriGiaTT = false;

#if KD_V3 || KD_V4

        double tongThueXNK = 0;
        double tongThueBVMT = 0;
        double tongThuKhac = 0;
        double _TongTriGiaNguyenTe = 0;
        double _TongThueXuatKhau = 0;
        double _TongThueThuKhac = 0;
        int formatSoLuong = GlobalSettings.SoThapPhan.SoLuongHMD;
        int formatDonGiaNT = GlobalSettings.SoThapPhan.DonGiaNT;
        int formatTriGiaNT = GlobalSettings.SoThapPhan.TriGiaNT;
        double tongtrigiaNT = 0;
#elif GC_V3 || GC_V4
        decimal tongThuKhac = 0;
        decimal tongThueXNK = 0;
        //double tongThueBVMT = 0;
        decimal _TongTriGiaNguyenTe = 0;
        decimal _TongThueXuatKhau = 0;
        decimal _TongThueThuKhac = 0;
        int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
        int formatDonGiaNT = GlobalSettings.DonGiaNT;
        int formatTriGiaNT = GlobalSettings.TriGiaNT;
        decimal tongtrigiaNT = 0;
#elif SXXK_V3 || SXXK_V4
        decimal tongThuKhac = 0;
        decimal tongThueXNK = 0;
        double tongThueBVMT = 0;
        decimal _TongTriGiaNguyenTe = 0;
        decimal _TongThueXuatKhau = 0;
        decimal _TongThueThuKhac = 0;
        int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
        int formatDonGiaNT = GlobalSettings.DonGiaNT;
        int formatTriGiaNT = GlobalSettings.TriGiaNT;
        decimal tongtrigiaNT = 0;
#endif
        double _TinhTongTrongLuong = 0;
        private DataTable Hang = new DataTable();

        public TQDTToKhaiXK_TT196()
        {
            InitializeComponent();
        }
        public string GetChiCucHQCK()
        {
            if (isCuaKhau)
            {
                string chicuc = NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                if (chicuc == string.Empty)
                    chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                return chicuc;
            }
            else
                return string.Empty;
        }
        public void BindReport()
        {

            try
            {
                fontReport = Convert.ToSingle(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8")) == 0 ? 8 : Convert.ToSingle(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8"));
                if (fontReport == 0)
                    fontReport = 8;

                TinhTong();
                setFont();

                string formatdateDetail = "dd-MM-yyyy HH:mm";
                string formatdate = "dd-MM-yyyy";

                //Set ngay mac dinh
                DateTime minDate = new DateTime(1900, 1, 1);

                #region Thông tin chính
                this.PrintingSystem.ShowMarginsWarning = false;
                // Cục Hải Quan
                string maCuc = TKMD.MaHaiQuan.Substring(1, 2);
                xlbCucHaiQuan.Text = DonViHaiQuan.GetName("Z" + maCuc + "Z");
                // Góc trên bên trái
                xtcChiCucHQDK.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                xtcChiCucHQCuaKhauXuat.Text = GetChiCucHQCK(); /*CuaKhau.GetName(TKMD.CuaKhau_ID);*/
                // Góc trên ở giữa
                //So luong phu luc to khai
                if (this.TKMD.HMDCollection.Count >= 7)
                    xtcSoLuongPhuLuc.Text = this.TKMD.HMDCollection != null ? ((this.TKMD.HMDCollection.Count - 1) / 7 + 1).ToString("00") : "0";
                else if (this.TKMD.HMDCollection.Count > 3)
                    xtcSoLuongPhuLuc.Text = "01";
                else
                    xtcSoLuongPhuLuc.Text = "00";

                xtcSoTN.Text = TKMD.SoTiepNhan.ToString();
                xtcNgayDK.Text = TKMD.NgayTiepNhan.ToString(formatdateDetail);
                xtcSoTK.Text = TKMD.SoToKhai.ToString();
                xtcNgayCapSoTK.Text = TKMD.NgayDangKy.ToString(formatdateDetail);
                // Ô 1
                xtcNguoiXuatKhau.Text = TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI;
                xlbMSNguoiXuatKhau.Text = TKMD.MaDoanhNghiep;
                //Ô 2
                xtcNguoiNhapKhau.Text = TKMD.TenDonViDoiTac;

                // Ô 3
                xtcNguoiUT.Text = TKMD.TenDonViUT;
                xlbMSNguoiUT.Text = TKMD.MaDonViUT;
                //Ô 4
                xtcDaiLyHQ.Text = TKMD.TenDaiLyTTHQ;
                xlbMSDaiLyHQ.Text = TKMD.MaDaiLyTTHQ;
                // Ô 5
                xtcLoaiHinh.Text = TKMD.MaLoaiHinh;
                // Ô 6
                if (TKMD.SoGiayPhep != "")
                    lbSoGP.Text = "" + this.TKMD.SoGiayPhep;
                else
                    lbSoGP.Text = "";
                lbSoGP.Font = new Font("Times New Roman", fontReport);
                if (this.TKMD.NgayGiayPhep > minDate)
                    lbNgayGP.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                else
                    lbNgayGP.Text = "";
                lbNgayGP.Font = new Font("Times New Roman", fontReport);
                if (this.TKMD.NgayHetHanGiayPhep > minDate)
                    lbNgayHHGP.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                else
                    lbNgayHHGP.Text = "";
                lbNgayHHGP.Font = new Font("Times New Roman", fontReport);

                // Ô 7
                if (!string.IsNullOrEmpty(TKMD.SoHopDong))
                {
                    xtcSoHopDong.Text = TKMD.SoHopDong;
                    if (TKMD.NgayHopDong > minDate)
                        xtcNgayHopDong.Text = TKMD.NgayHopDong.ToString(formatdate);
                    else
                        xtcNgayHopDong.Text = "";
                    if (TKMD.NgayHetHanHopDong > minDate)
                        xtcNgayHHHopDong.Text = TKMD.NgayHetHanHopDong.ToString(formatdate);
                    else
                        xtcNgayHHHopDong.Text = "";
                }
                else
                {
                    xtcSoHopDong.Text = "";
                    xtcNgayHopDong.Text = "";
                    xtcNgayHHHopDong.Text = "";
                }
                // Ô 8                
                xtcHoaDonTM.Text = "" + this.TKMD.SoHoaDonThuongMai;
                xtcHoaDonTM.Font = new Font("Times New Roman", fontReport);
                if (this.TKMD.NgayHoaDonThuongMai > minDate)
                    lblNgayHoaDon.Text = this.TKMD.NgayHoaDonThuongMai.ToString(formatdate);
                else
                    lblNgayHoaDon.Text = "";
                lblNgayHoaDon.Font = new Font("Times New Roman", fontReport);

                // Ô 9
                xtcMaCuaKhauXuat.Text = TKMD.CuaKhau_ID;
                xtcCuaKhauXuat.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);
                // Ô 10
                xtcMaNuocNhap.Text = TKMD.NuocNK_ID;
                xtcNuocNhapKhau.Text = "  " + Nuoc.GetName(TKMD.NuocNK_ID);
                //Ô 11
                xtcDieuKienGiaoHang.Text = TKMD.DKGH_ID;
                // Ô 12
                xtcPhuongThucThanhToan.Text = TKMD.PTTT_ID;
                // Ô 13
                xtcDongTienTT.Text = TKMD.NguyenTe_ID;
                //xtcDongTienTT.Text = NguyenTe.SelectName(TKMD.NguyenTe_ID);
                // Ô 14
                xtcTiGiaTinhThue.Text = TKMD.TyGiaTinhThue.ToString();
                #endregion

                #region chi tiết Hàng Hóa 15 - 24
                List<HangMauDich> DatasourceHangMauDich = new List<HangMauDich>();
                if (TKMD.HMDCollection != null && TKMD.HMDCollection.Count > 3)
                {
                    MoTaHang1.Text = "Theo phụ lục tờ khai";
                    for (int i = 1; i < 4; i++)
                        DatasourceHangMauDich.Add(new HangMauDich { SoThuTuHang = i });
                    DatasourceHangMauDich[0].TenHang = "Theo phụ lục tờ khai";
                    // in tong tri gia nguyen te
                    foreach (HangMauDich hmd in TKMD.HMDCollection)
                    {
                        tongtrigiaNT = tongtrigiaNT + hmd.TriGiaTT;
                        tongThueXNK = tongThueXNK + hmd.ThueSuatXNK;
                        tongThuKhac = tongThuKhac + hmd.TriGiaThuKhac;
                    }
                    lblTongTGNT.Text = tongtrigiaNT.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatDonGiaNT, true));
                    xtcThueXuat_Cong.Text = tongThueXNK.ToString("N0");
                    xtcThuKhac_Cong.Text = tongThuKhac.ToString("N0");

                }
                else
                {
                    DatasourceHangMauDich = GetDataSource();

                    while (DatasourceHangMauDich.Count < 3)
                    {
                        DatasourceHangMauDich.Add(new HangMauDich { SoThuTuHang = DatasourceHangMauDich[DatasourceHangMauDich.Count - 1].SoThuTuHang + 1 });
                    }
                }
                //                 while (DatasourceHangMauDich.Count < 10000)
                //                 {
                //                     DatasourceHangMauDich.Add(new HangMauDich { SoThuTuHang = DatasourceHangMauDich[DatasourceHangMauDich.Count - 1].SoThuTuHang + 1 });
                //                 }
                #region Bảng mô tả hàng - Ô 15 - Ô 21
                DetailHangHoa.DataSource = DatasourceHangMauDich;
                STTHang1.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoThuTuHang");
                MoTaHang1.DataBindings.Add("Text", DetailHangHoa.DataSource, "TenHang");

                MaSoHang1.DataBindings.Add("Text", DetailHangHoa.DataSource, "MaHS");
                XuatXuHang1.DataBindings.Add("Text", DetailHangHoa.DataSource, "NuocXX_ID");
                LuongHang1.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoLuong", Company.KDT.SHARE.Components.Globals.FormatNumber(formatSoLuong, true));
                DVTHang1.DataBindings.Add("Text", DetailHangHoa.DataSource, "DVT_ID");
                DonGiaHang1.DataBindings.Add("Text", DetailHangHoa.DataSource, "DonGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(formatDonGiaNT, true));
                TGNTHang1.DataBindings.Add("Text", DetailHangHoa.DataSource, "TriGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(formatTriGiaNT, true));
                lblTongTGNT.Text = _TongTriGiaNguyenTe.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(formatTriGiaNT, true));
                #endregion

                #region Bảng Thuế xuất - Ô 22 + 23
                DetailThue.DataSource = DatasourceHangMauDich;
                xtcThueXuat_TriGiaTT.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(0, true));
                xtcThueXuat_TiLe.DataBindings.Add("Text", DetailThue.DataSource, "ThueSuatXNK");
                xtcThueXuat_TienThue.DataBindings.Add("Text", DetailThue.DataSource, "ThueXNK", Company.KDT.SHARE.Components.Globals.FormatNumber(0, true));
                xtcThueXuat_SoTT.DataBindings.Add("Text", DetailThue.DataSource, "SoThuTuHang");
                //xtcThuKhac_TriGia.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaThuKhac", Company.KDT.SHARE.Components.Globals.FormatNumber(0, true));
                xtcThuKhac_TyLe.DataBindings.Add("Text", DetailThue.DataSource, "TyLeThuKhac");
                xtcThuKhac_SoTien.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaThuKhac", Company.KDT.SHARE.Components.Globals.FormatNumber(0, true));
                xtcThueXuat_Cong.Text = _TongThueXuatKhau.ToString("N0");
                xtcThuKhac_Cong.Text = _TongThueThuKhac.ToString("N0");
                #endregion

                #region Ô 24
                xtcTongTienBangSo.Text = (_TongThueXuatKhau + _TongThueThuKhac).ToString("N0") + " VNĐ";
                string s = VNCurrency.ToString((decimal)(_TongThueXuatKhau + _TongThueThuKhac)).Trim();
                s = s[0].ToString().ToUpper() + s.Substring(1);
                xtcTongTienBangChu.Text = s.Replace("  ", " ");
                #endregion

                #endregion

                #region Bảng Container - Ô 25
                // IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangVD = TKMD.VanTaiDon.ListHangOfVanDon.ToArray().Distinct(new DistinctSHContainer());
                List<Company.KDT.SHARE.QuanLyChungTu.Container> dataSourceContainer = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
                if (TKMD.VanTaiDon == null || TKMD.VanTaiDon.ContainerCollection == null || TKMD.VanTaiDon.ContainerCollection.Count == 0)
                {
                    if (TKMD.TrongLuongNet > 0)
                        lblTongTrongLuong.Text = TKMD.TrongLuong.ToString("###,###,##0.####") + " (kg)/ " + TKMD.TrongLuongNet.ToString("###,###,##0.####") + "(kg)";
                    else
                        lblTongTrongLuong.Text = TKMD.TrongLuong.ToString("###,###,##0.####") + " (kg)";
                    if (TKMD.SoKien > 0)
                        lblTongKien.Text = TKMD.SoKien.ToString() + " (kiện)";

                }
                else if (TKMD.VanTaiDon.ContainerCollection.Count <= 4)
                {
                    foreach (Company.KDT.SHARE.QuanLyChungTu.Container item in TKMD.VanTaiDon.ContainerCollection)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.Container cont = new Company.KDT.SHARE.QuanLyChungTu.Container()
                        {
                            SoHieu = item.SoHieu + "/ " + item.Seal_No,
                            Seal_No = item.Seal_No,
                            LoaiContainer = item.LoaiContainer,
                            Trang_thai = item.Trang_thai,
#if KD_V4 || SXXK_V4 || GC_V4
                            SoKien = item.SoKien,
                            TrongLuong = item.TrongLuong,
                            DiaDiemDongHang = item.DiaDiemDongHang
#endif
                        };
                        dataSourceContainer.Add(cont);
                    }
                }
                else if (TKMD.VanTaiDon.ContainerCollection.Count > 4)
                {
                    if (GlobalSettings.IsKhongDungBangKeCont == false)
                        dataSourceContainer.Add(new Company.KDT.SHARE.QuanLyChungTu.Container { SoHieu = "Chi tiết phụ lục đính kèm" });
                    else
                        for (int i = 0; i < 4; i++)
                        {
                            Company.KDT.SHARE.QuanLyChungTu.Container cont = new Company.KDT.SHARE.QuanLyChungTu.Container()
                            {
                                SoHieu = TKMD.VanTaiDon.ContainerCollection[i].SoHieu + "/ " + TKMD.VanTaiDon.ContainerCollection[i].Seal_No,
                                Seal_No = TKMD.VanTaiDon.ContainerCollection[i].Seal_No,
                                LoaiContainer = TKMD.VanTaiDon.ContainerCollection[i].LoaiContainer,
                                Trang_thai = TKMD.VanTaiDon.ContainerCollection[i].Trang_thai,
#if KD_V4 || SXXK_V4 || GC_V4
                                SoKien = TKMD.VanTaiDon.ContainerCollection[i].SoKien,
                                TrongLuong = TKMD.VanTaiDon.ContainerCollection[i].TrongLuong,
                                DiaDiemDongHang = TKMD.VanTaiDon.ContainerCollection[i].DiaDiemDongHang
#endif
                            };
                            dataSourceContainer.Add(cont);
                        }
                }
                while (dataSourceContainer.Count < 4)
                    dataSourceContainer.Add(new Company.KDT.SHARE.QuanLyChungTu.Container());

                DetailLuongHang.DataSource = dataSourceContainer;
                xtcLuongHang_SoHieuContainer.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoHieu");

#if KD_V4 || SXXK_V4 || GC_V4
                xtcLuongHang_SoLuongKien.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoKien");
                xtcLuongHang_TrongLuongHang.DataBindings.Add("Text", DetailLuongHang.DataSource, "TrongLuong");
                xtcLuongHang_DDDongHang.DataBindings.Add("Text", DetailLuongHang.DataSource, "DiaDiemDongHang");
                //Tong trọng luong container
                double sumTrongLuong = dataSourceContainer.Sum(x => x.TrongLuong);
                if (TKMD.TrongLuongNet > 0)
                    lblTongTrongLuong.Text = TKMD.TrongLuong.ToString("###,###,##0.####") + " (kg)/ " + TKMD.TrongLuongNet.ToString("###,###,##0.####") + " (kg)";
                else
                    lblTongTrongLuong.Text = TKMD.TrongLuong.ToString("###,###,##0.####") + " (kg)";
                if (TKMD.SoKien > 0)
                    lblTongKien.Text = TKMD.SoKien.ToString() + " (Kiện)";
                //if (GlobalSettings.IsKhongDungBangKeCont)
                //{
                //    double sumSoKien = dataSourceContainer.Sum(x => x.SoKien);
                //    lblTongKien.Text = Convert.ToString(sumSoKien) + " (Kiện)";
                //}
                //else
                //    lblTongKien.Text = TKMD.SoKien.ToString() + " (Kiện)";

#endif
#if KD_V3 || SXXK_V3 || GC_V3
                //tong container
               // xtcLuongHang_CongTL.Text = TKMD.TrongLuong.ToString("###,###,##0.####") + " (kg)";
               // lblTongKien.Text = TKMD.SoKien.ToString() + " (kiện)";
#endif
                #endregion

                ReportContainer(TKMD);

                //Sau o 25. Thong tin Le phi 
                ReportLePhi(TKMD);

                #region Ô 26. Chứng từ đi kèm
                xlbChungTuDinhKem.Text = "";
                //                 if (TKMD.GiayPhepCollection != null && TKMD.GiayPhepCollection.Count > 0)
                //                 {
                //                     xlbChungTuDinhKem.Text += "- Giấy phép: ";
                //                     foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep item in TKMD.GiayPhepCollection)
                //                         xlbChungTuDinhKem.Text += item.SoGiayPhep + ", ";
                //                     xlbChungTuDinhKem.Text += "\r\n";
                //                 }
                //                 if (TKMD.HoaDonThuongMaiCollection != null && TKMD.HoaDonThuongMaiCollection.Count > 0)
                //                 {
                //                     xlbChungTuDinhKem.Text += "- Hóa đơn: ";
                //                     foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai item in TKMD.HoaDonThuongMaiCollection)
                //                         xlbChungTuDinhKem.Text += item.SoHoaDon + ", ";
                //                     xlbChungTuDinhKem.Text += "\r\n";
                //                 }
                //                 if (TKMD.HopDongThuongMaiCollection != null && TKMD.HopDongThuongMaiCollection.Count > 0)
                //                 {
                //                     xlbChungTuDinhKem.Text += "- Hợp đồng: ";
                //                     foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai item in TKMD.HopDongThuongMaiCollection)
                //                         xlbChungTuDinhKem.Text += item.SoHopDongTM + ", ";
                //                     xlbChungTuDinhKem.Text += "\r\n";
                //                 }
                //                 if (TKMD.COCollection != null && TKMD.COCollection.Count > 0)
                //                 {
                //                     xlbChungTuDinhKem.Text += "- CO : ";
                //                     foreach (Company.KDT.SHARE.QuanLyChungTu.CO item in TKMD.COCollection)
                //                         xlbChungTuDinhKem.Text += item.SoCO;
                //                     xlbChungTuDinhKem.Text += "\r\n";
                //                 }
                //                 if (TKMD.listChuyenCuaKhau != null && TKMD.listChuyenCuaKhau.Count > 0)
                //                 {
                //                     xlbChungTuDinhKem.Text += "- Đề nghị chuyển cửa khẩu : ";
                //                     foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau item in TKMD.listChuyenCuaKhau)
                //                         xlbChungTuDinhKem.Text += item.SoVanDon;
                //                     xlbChungTuDinhKem.Text += "\r\n";
                //                 }
                //                 if (TKMD.VanTaiDon != null && !string.IsNullOrEmpty(TKMD.VanTaiDon.SoVanDon))
                //                 {
                //                     xlbChungTuDinhKem.Text += "- Vận đơn : " + TKMD.VanTaiDon.SoVanDon;
                //                     xlbChungTuDinhKem.Text += "\r\n";
                //                 }
                //                 if (TKMD.listCTDK != null && TKMD.listCTDK.Count > 0)
                //                 {
                //                     xlbChungTuDinhKem.Text += "- Chứng từ dạng ảnh: ";
                //                     foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem item in TKMD.listCTDK)
                //                         xlbChungTuDinhKem.Text += item.SO_CT;
                //                     xlbChungTuDinhKem.Text += "\r\n";
                // 
                //                 }
                string ctk = "";
                ctk += TKMD.VanTaiDon != null ? "Vận đơn" : "";
                ctk += (TKMD.GiayPhepCollection.Count != 0 ? "; " + string.Format("Giấy phép ({0})", TKMD.GiayPhepCollection.Count) : "");
                ctk += (TKMD.HoaDonThuongMaiCollection.Count != 0 ? "; " + string.Format("Hóa đơn ({0})", TKMD.HoaDonThuongMaiCollection.Count) : "");
                ctk += (TKMD.HopDongThuongMaiCollection.Count != 0 ? "; " + string.Format("Hợp đồng ({0})", TKMD.HopDongThuongMaiCollection.Count) : "");
                ctk += (TKMD.COCollection.Count != 0 ? "; " + string.Format("CO ({0})", TKMD.COCollection.Count) : "");
                ctk += (TKMD.listChuyenCuaKhau.Count != 0 ? "; " + string.Format("Đề nghị chuyển cửa khẩu ({0})", TKMD.listChuyenCuaKhau.Count) : "");
                ctk += (TKMD.listCTDK.Count != 0 ? "; " + string.Format("Chứng từ kèm dạng ảnh ({0})", TKMD.listCTDK.Count) : "");
                ctk += (TKMD.ChungTuNoCollection.Count != 0 ? "; " + string.Format("Chứng từ nợ ({0})", TKMD.ChungTuNoCollection.Count) : "");
                if (ctk != "")
                {
                    ctk = ctk.Substring(0, 1).Equals(";") ? ctk.Remove(0, 1) : ctk;
                    ctk = ctk.Substring(ctk.Length - 1, 1).Equals(";") ? ctk.Remove(ctk.Length - 1, 1) : ctk;
                }

                xlbChungTuDinhKem.Font = new Font("Times New Roman", fontReport);
                xlbChungTuDinhKem.Text = ctk;
                #endregion

                #region Ô 28
                lbKetQuaPL.Text = "";
                lbKetQuaPL.Text += "Hướng dẫn: " + (Company.KDT.SHARE.Components.Globals.FontName == "TCVN3" ? Company.KDT.SHARE.Components.ConvertFont.TCVN3ToUnicode(TKMD.HUONGDAN) : TKMD.HUONGDAN);
                #endregion

                //Ngay thang nam in to khai
#if SXXK_V4
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1) || GlobalSettings.NgayHeThong)
                    lblNgayThangNam.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                else
                    lblNgayThangNam.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;
#else
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1)||GlobalSettings.NgayHeThong)
                    lblNgayThangNam.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                else
                    lblNgayThangNam.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;
#endif
                //Ô 29. Ghi chep khac: O nay danh cho HQ ghi
                xtcGhiChuKhac.Font = new Font("Times New Roman", fontReport);
                xtcGhiChuKhac.Text = TKMD.DeXuatKhac;


            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private decimal TinhSoLuongHangContainer(string soHieuContainer)
        {
            //IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangCollection = from h in TKMD.VanTaiDon.ListHangOfVanDon
            //                                                                               where h.SoHieuContainer == soHieuContainer
            //                                                                               select h;
            decimal soLuongHang = 0;
            //foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in HangCollection)
            //    soLuongHang += item.SoLuong;
            return soLuongHang;
        }
        private double TinhTrongLuongHangContainer(string soHieuContainer)
        {
            IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangCollection = from h in TKMD.VanTaiDon.ListHangOfVanDon
                                                                                           where h.SoHieuContainer == soHieuContainer
                                                                                           select h;
            double trongLuong = 0;
            foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in HangCollection)
                trongLuong += item.TrongLuong;
            return trongLuong;
        }

        private void xtcDVT_Hang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dvt = (XRTableCell)sender;

            if (!string.IsNullOrEmpty(dvt.Text))
                dvt.Text = DonViTinh.GetName(dvt.Text.Trim());
        }

        private void xtcLuongHang_SoLuongKien_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
        private void xtcLuongHang_TrongLuongHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTableCell TrongLuongHang = (XRTableCell)sender;
            //if (!string.IsNullOrEmpty(TrongLuongHang.Text))
            //    TrongLuongHang.Text = TinhTrongLuongHangContainer(TrongLuongHang.Text).ToString(); 
        }

        private void xtcLuongHang_SoTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (sTT_Container == 5)
                sTT_Container = 1;
            ((XRTableCell)sender).Text = sTT_Container.ToString();

            sTT_Container++;
        }
        private void TinhTong()
        {
            _TongThueThuKhac = _TongThueXuatKhau = _TongTriGiaNguyenTe = 0;
            _TinhTongTrongLuong = 0;
            foreach (HangMauDich item in TKMD.HMDCollection)
            {
                _TongTriGiaNguyenTe += item.TriGiaKB;
                _TongThueXuatKhau += item.ThueXNK;
                _TongThueThuKhac += item.TriGiaThuKhac;
            }
            if (TKMD.VanTaiDon != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in TKMD.VanTaiDon.ListHangOfVanDon)
                    _TinhTongTrongLuong += item.TrongLuong;
        }
        private void setFont()
        {
            xrTable3.Font = new Font("Times New Roman", fontReport);
            DetailThue.Font = DetailHangHoa.Font = DetailLuongHang.Font = GroupFooter1.Font = new Font("Times New Roman", fontReport);
        }

        private void xtcThuKhac_TriGia_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            decimal TriGiaTinhThueXNK = 0;
            decimal TienThueXNK = 0;
            XRTableCell TriGiaTinhThueThuKhac = (XRTableCell)sender;
            // if (inTriGiaTT)
            // {
            double temp2 = 1;
            if (decimal.TryParse(xtcThueXuat_TriGiaTT.Text, out TriGiaTinhThueXNK) && decimal.TryParse(string.IsNullOrEmpty(xtcThueXuat_TienThue.Text) ? "0" : xtcThueXuat_TienThue.Text, out TienThueXNK))
            {
                TriGiaTinhThueThuKhac.Text = (TriGiaTinhThueXNK + TienThueXNK).ToString("N0");
            }
            //else if (string.IsNullOrEmpty(xtcThuKhac_SoTien.Text) || (double.TryParse(xtcThuKhac_SoTien.Text, out temp2) && temp2 == 0))
            //    TriGiaTinhThueThuKhac.Text = string.Empty;
            if (!inTriGiaTT && (string.IsNullOrEmpty(xtcThuKhac_SoTien.Text) || (double.TryParse(xtcThuKhac_SoTien.Text, out temp2) && temp2 == 0)))
                TriGiaTinhThueThuKhac.Text = string.Empty;

            double temp = 1;
            if (TriGiaTinhThueThuKhac.Text == "0" || (double.TryParse(TriGiaTinhThueThuKhac.Text, out temp) && temp == 0))
            {
                TriGiaTinhThueThuKhac.Text = string.Empty;

            }
            if (inTriGiaTT && string.IsNullOrEmpty(xtcThueXuat_TriGiaTT.Text))
                TriGiaTinhThueThuKhac.Text = string.Empty;
        }

        private void xtcLoaiHinh_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell LoaiHinh = (XRTableCell)sender;
            if (!string.IsNullOrEmpty(LoaiHinh.Text))
            {
                LoaiHinh.Text = LoaiHinh.Text + " - " + LoaiHinhMauDich.GetName(LoaiHinh.Text.Trim());
            }


        }

        private void xtcThueXuat_TiLe_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            double number = 0;
            if (double.TryParse(cell.Text, out number))
            {
                if (number == 0)
                {
                    cell.Text = string.Empty;
                    if (!inTriGiaTT)
                        xtcThueXuat_TriGiaTT.Text = string.Empty;
                }
            }
        }

        private void xtcThueXuat_TienThue_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            XRTableCell cell = (XRTableCell)sender;
            Decimal number = 0;
            if (Decimal.TryParse(cell.Text, out number))
            {
                if (number == 0)
                {
                    cell.Text = string.Empty;
                    if (!inTriGiaTT)
                        xtcThueXuat_TriGiaTT.Text = string.Empty;

                }
            }

        }

        private void xtcThuKhac_SoTien_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            Decimal number = 0;
            if (Decimal.TryParse(cell.Text, out number))
            {
                if (number == 0)
                {
                    cell.Text = string.Empty;
                    if (!inTriGiaTT)
                        xtcThuKhac_TriGia.Text = cell.Text;

                }
            }
        }

        private void xtcThueXuat_TriGiaTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell TriGiaTT = (XRTableCell)sender;
            if (!inTriGiaTT && (string.IsNullOrEmpty(xtcThueXuat_TienThue.Text) || xtcThueXuat_TienThue.Text == "0"))
            {
                TriGiaTT.Text = string.Empty;
            }
            double temp = 1;
            if (TriGiaTT.Text == "0" || (double.TryParse(TriGiaTT.Text, out temp) && temp == 0))
            {
                TriGiaTT.Text = string.Empty;

            }
        }

        private List<HangMauDich> GetDataSource()
        {
            List<HangMauDich> datasource = new List<HangMauDich>();
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                HangMauDich hangMD = new HangMauDich();
                hangMD.MaPhu = hmd.MaPhu;
                hangMD.SoThuTuHang = hmd.SoThuTuHang;
                hangMD.TenHang = inMaHang ? hmd.TenHang + "/" + hmd.MaPhu : hmd.TenHang;
                hangMD.MaHS = hmd.MaHS;
                hangMD.NuocXX_ID = hmd.NuocXX_ID;
                hangMD.SoLuong = hmd.SoLuong;
                hangMD.DVT_ID = hmd.DVT_ID;
                hangMD.DonGiaKB = hmd.DonGiaKB;
                hangMD.TriGiaKB = hmd.TriGiaKB;
                hangMD.TriGiaTT = hmd.TriGiaTT;
                hangMD.ThueSuatXNK = hmd.ThueSuatXNK;
                hangMD.TriGiaThuKhac = hmd.TriGiaThuKhac;
                hangMD.TyLeThuKhac = hmd.TyLeThuKhac;
                hangMD.ThueXNK = hmd.ThueXNK;
                datasource.Add(hangMD);
            }
            return datasource;
        }

        private void xtcLuongHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            double temp = 1;
            XRTableCell cell = (XRTableCell)sender;
            if (cell.Text == "0" || (double.TryParse(cell.Text, out temp) && temp == 0))
                cell.Text = string.Empty;
        }

        private string ReportContainer(ToKhaiMauDich objTKMD)
        {

            string tongCont = "";

            try
            {
                if (TKMD.VanTaiDon != null)
                {
                    //Tinh tong cont
                    if (objTKMD.VanTaiDon.ContainerCollection != null && objTKMD.VanTaiDon.ContainerCollection.Count != 0)
                    {
#if KD_V3 || SXXK_V3 || GC_V3 
                        tongCont = "";

                        double sumCont20 = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.Container20).ToString()));
                        if (sumCont20 > 0)
                            tongCont += string.Format("Cont20: {0}", sumCont20.ToString("N0"));

                        double sumCont40 = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.Container40).ToString()));
                        if (sumCont40 > 0)
                            tongCont += "; " + string.Format("Cont40: {0}", sumCont40.ToString("N0"));

#elif KD_V4 || SXXK_V4 || GC_V4
                        tongCont = "";

                        double sumCont20 = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.Container20).ToString()));
                        if (sumCont20 > 0)
                            tongCont += string.Format("Cont20: {0}", sumCont20.ToString("N0"));

                        double sumCont40 = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.Container40).ToString()));
                        if (sumCont40 > 0)
                            tongCont += "; " + string.Format("Cont40: {0}", sumCont40.ToString("N0"));
                        double sumCont45 = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.Container45).ToString()));
                        if (sumCont45 > 0)
                            tongCont += "; " + string.Format("Cont45: {0}", sumCont45);

                        double sumContKhac = objTKMD.VanTaiDon.ContainerCollection.Count(x => x.LoaiContainer.Equals(((int)Company.KDT.SHARE.Components.ELoaiContainer.ContainerKhac).ToString()));
                        if (sumContKhac > 0)
                            tongCont += "; " + string.Format("Cont khác: {0}", sumContKhac);
#endif
                    }
                    else
                    {
                        tongCont = "";
#if KD_V3 || SXXK_V3 || GC_V3 
                        if (TKMD.SoContainer20 != 0)
                            tongCont += string.Format("Cont20: {0}", TKMD.SoContainer20.ToString("N0"));

                        if (TKMD.SoContainer40 != 0)
                            tongCont += "; " + string.Format("Cont40: {0}", TKMD.SoContainer40.ToString("N0"));
#elif KD_V4 || SXXK_V4 || GC_V4
                        if (TKMD.SoLuongContainer.Cont20 != 0)
                            tongCont += string.Format("Cont20: {0}", TKMD.SoLuongContainer.Cont20.ToString("N0"));
                        if (TKMD.SoLuongContainer.Cont40 != 0)
                            tongCont += string.Format("Cont40: {0}", TKMD.SoLuongContainer.Cont40.ToString("N0"));
                        if (TKMD.SoLuongContainer.Cont45 != 0)
                            tongCont += string.Format("Cont45: {0}", TKMD.SoLuongContainer.Cont45.ToString("N0"));
                        if (TKMD.SoLuongContainer.ContKhac != 0)
                            tongCont += string.Format("Cont khác: {0}", TKMD.SoLuongContainer.ContKhac.ToString("N0"));
#endif
                    }
                }
                //Khong co Van don, lay thong tin cont ngoai to khai chinh
                else
                {
                    tongCont = "";
#if KD_V3 || SXXK_V3 || GC_V3 
                    if (TKMD.SoContainer20 != 0)
                        tongCont += string.Format("Cont20: {0}", TKMD.SoContainer20.ToString("N0"));

                    if (TKMD.SoContainer40 != 0)
                        tongCont += "; " + string.Format("Cont40: {0}", TKMD.SoContainer40.ToString("N0"));

#elif KD_V4 || SXXK_V4 || GC_V4
                    if (TKMD.SoLuongContainer.Cont20 != 0)
                        tongCont += string.Format("Cont20: {0}", TKMD.SoLuongContainer.Cont20.ToString("N0"));
                    if (TKMD.SoLuongContainer.Cont40 != 0)
                        tongCont += string.Format("Cont40: {0}", TKMD.SoLuongContainer.Cont40.ToString("N0"));
                    if (TKMD.SoLuongContainer.Cont45 != 0)
                        tongCont += string.Format("Cont45: {0}", TKMD.SoLuongContainer.Cont45.ToString("N0"));
                    if (TKMD.SoLuongContainer.ContKhac != 0)
                        tongCont += string.Format("Cont khác: {0}", TKMD.SoLuongContainer.ContKhac.ToString("N0"));
#endif
                }

                //tong container

                lblTongCont.Text = RemoveCharacterString(tongCont, ";");
                lblTongCont.Font = new Font("Times New Roman", fontReport);
                if (!GlobalSettings.IsKhongDungBangKeCont)
                {
                    if (TKMD.TrongLuongNet > 0)
                        lblTongTrongLuong.Text = TKMD.TrongLuong.ToString("###,###,##0.####") + "(kg)/ " + TKMD.TrongLuongNet.ToString("###,###,##0.####") + "(kg)";
                    else
                        lblTongTrongLuong.Text = TKMD.TrongLuong.ToString("###,###,##0.####") + "(kg)";
                    if (TKMD.SoKien > 0)
                        lblTongKien.Text = objTKMD.SoKien.ToString("N0") + "(kiện)";

                }



            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            return string.Empty;
        }

        private void ReportLePhi(ToKhaiMauDich TKMD)
        {
            try
            {
                string st = "";

                if (this.TKMD.PhiBaoHiem > 0)
                    st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
                if (this.TKMD.PhiVanChuyen > 0)
                    st += "; F = " + this.TKMD.PhiVanChuyen.ToString("N2");
                if (this.TKMD.PhiKhac > 0)
                    st += "; Phí khác = " + this.TKMD.PhiKhac.ToString("N2");

                lblLePhi.Text = RemoveCharacterString(st, ";");
                lblLePhi.Font = new Font("Times New Roman", fontReport);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private string RemoveCharacterString(string value, string character)
        {
            try
            {
                //Tra ve chuoi rong neu gia tri null
                if (value == null || value == "")
                    return string.Empty;

                //loai bo dau ';' o dau chuoi (Neu co)
                if (value.Substring(0, 1) == character)
                {
                    value = value.Remove(0, 1);
                }

                //loai bo dau ';' o cuoi chuoi (Neu co)
                if (value.Substring(value.Length - 1, 1) == character)
                {
                    value = value.Remove(value.Length - 1, 1);
                }

                //Tra lai gia tri chuoi sau khi loai bo dau ';' o dau va cuoi chuoi (Neu co)
                return value;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            //Neu loi xay ra thi tra lai gia tri chinh no
            return value;
        }



    }
}
