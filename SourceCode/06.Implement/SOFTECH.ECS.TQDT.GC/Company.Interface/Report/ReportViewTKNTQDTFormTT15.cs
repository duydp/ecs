﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
#if KD_V2
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V2
using Company.GC.BLL.KDT;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.Utils;
#elif SXXK_V2
using Company.BLL.KDT;
using Company.BLL.DuLieuChuan;
using Company.BLL.Utils;
#endif

using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System.Diagnostics;

namespace Company.Interface.Report
{

    public partial class ReportViewTKNTQDTFormTT15 : BaseForm
    {
        public Company.Interface.Report.TQDTToKhaiNK_TT15 ToKhaiChinhReport = new Company.Interface.Report.TQDTToKhaiNK_TT15();
        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT15 PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT15();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public XRControl Cell = new XRControl();
        public int index = 0;



        /// <summary>
        /// Mac dinh = 9 dong hang tren phu luc to khai.
        /// </summary>
        private int soDongHang = 3;

        public ReportViewTKNTQDTFormTT15()
        {
            InitializeComponent();
        }

        private void ReportViewTKXTQDTFormTT15_Load(object sender, EventArgs e)
        {
            //DATLMQ bổ sung thiết lập margin: 25/02/2011
            this.ToKhaiChinhReport.Margins.Top = 0;
            this.ToKhaiChinhReport.Margins.Bottom = 0;
            this.PhuLucReport.Margins.Top = 0;
            this.PhuLucReport.Margins.Bottom = 0;
            // edit by KhanhHn
            // fix bug tờ khai không có vận đơn
            int soContainer;
            if (TKMD.VanTaiDon != null)
                soContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
            else
                soContainer = 0;
            if (this.TKMD.HMDCollection.Count > 1 || soContainer > 3)
            {
                int countHang = (this.TKMD.HMDCollection.Count - 1) / 3 + 1;
                int countContainer = (soContainer - 1) / 4 + 1;
                int count = countHang >= countContainer ? countHang : countContainer;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            cboToKhai.SelectedIndex = 0;
        }
        
        /// <summary>
        /// Thêm số phục lục vào comboBox Tờ khai
        /// </summary>
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboToKhai.SelectedIndex == 0)
                {
                    this.ToKhaiChinhReport.TKMD = this.TKMD;
                    this.ToKhaiChinhReport.report = this;

                    this.ToKhaiChinhReport.BindReport();
                    printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                    this.ToKhaiChinhReport.CreateDocument();
                }
                else
                {
                    try
                    {
                        index = cboToKhai.SelectedIndex;
                    }
                    catch { index = 1; }

                    txtDeXuatKhac.Text = "";
                    txtTenNhomHang.Text = "";
                    List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
                   // chia hang theo phu luc
                    int begin = (cboToKhai.SelectedIndex - 1) * 3; //Comment by Hungtq
                    int end = cboToKhai.SelectedIndex * 3; //Comment by Hungtq

                    if (begin <= this.TKMD.HMDCollection.Count - 1)
                    {
                        if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                        for (int i = begin; i < end; i++)
                            HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                    }
                    //chia container theo phu luc
                    List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
                    int beginContainer = (cboToKhai.SelectedIndex - 1) * 4;
                    int endContainer = cboToKhai.SelectedIndex * 4;
                    //edit by KhanhHn - fix lỗi vận đơn null
                    if (TKMD.VanTaiDon != null)
                    {
                        if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count - 1)
                        {
                            if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
                                endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
                            for (int j = beginContainer; j < endContainer; j++)
                                ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
                        }
                    }
                    this.PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT15();
                    this.PhuLucReport.report = this;
                    this.PhuLucReport.TKMD = this.TKMD;
                    if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                        this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                    this.PhuLucReport.HMDCollection = HMDReportCollection;
                    this.PhuLucReport.ContCollection = ContainerReportCo;
                    this.PhuLucReport.SoDongHang = soDongHang;
                    this.PhuLucReport.BindReport(index.ToString());
                    printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                    
                    this.PhuLucReport.CreateDocument();

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboToKhai.SelectedIndex == 0)
                {

                    this.ToKhaiChinhReport.CreateDocument();
                    try
                    {
                        printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi in báo cáo tờ khai: " + ex.Message, false);
                    }
                    //this.ToKhaiChinhReport.CreateDocument();
                }
                else
                {
                    this.PhuLucReport.CreateDocument();
                    try
                    {
                        printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi in báo cáo tờ khai: " + ex.Message, false);
                    }
                    //this.PhuLucReport.CreateDocument();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnApDung_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboToKhai.SelectedIndex == 0)
                {
                    //datlmq update 29072010
                    //if (txtTenNhomHang.Text != "")
                    //    this.ToKhaiChinhReport.setThongTin(this.Cell, txtTenNhomHang.Text);
                    this.ToKhaiChinhReport.CreateDocument();
                }
                else
                {
                    //this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                    this.PhuLucReport.CreateDocument();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void chkInMaHang_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboToKhai.SelectedIndex == 0)
                {
                    if (chkInMaHang.Checked)
                        this.ToKhaiChinhReport.InMaHang = true;
                    else
                        this.ToKhaiChinhReport.InMaHang = false;

                    this.ToKhaiChinhReport.BindReport();
                    printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                    this.ToKhaiChinhReport.CreateDocument();

                }
                else
                {
                    if (chkInMaHang.Checked)
                        this.PhuLucReport.InMaHang = true;
                    else
                        this.PhuLucReport.InMaHang = false;

                    this.PhuLucReport.BindReport(index.ToString());
                    printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                    this.PhuLucReport.CreateDocument();

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboToKhai.SelectedIndex == 0)
                {
                    this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                    this.ToKhaiChinhReport.BindReport();
                    printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                    this.ToKhaiChinhReport.CreateDocument();

                }
                else
                {
                    this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                    this.PhuLucReport.BindReport(index.ToString());
                    printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                    this.PhuLucReport.CreateDocument();

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void chkMienThueNK_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboToKhai.SelectedIndex == 0)
                {
                    this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                    this.ToKhaiChinhReport.MienThueNK = chkMienThueNK.Checked;
                    this.ToKhaiChinhReport.MienThueGTGT = chkMienThueGTGT.Checked;

                    this.ToKhaiChinhReport.BindReport();
                    printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                    this.ToKhaiChinhReport.CreateDocument();
                }
                else
                {
                    this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                    this.PhuLucReport.MienThueNK = chkMienThueNK.Checked;
                    this.PhuLucReport.MienThueGTGT = chkMienThueGTGT.Checked;

                    this.PhuLucReport.BindReport(cboToKhai.Items.Count.ToString());
                    printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                    this.PhuLucReport.CreateDocument();

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// In pdf Button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrintPDF_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboToKhai.SelectedIndex == 0)
                {

                    this.ToKhaiChinhReport.CreateDocument();
                    this.ToKhaiChinhReport.Margins.Top = 0;
                    this.ToKhaiChinhReport.Margins.Bottom = 0;

                    // BEGIN: SAVE FILE TO PDF.
                    SaveFileDialog dlg = new SaveFileDialog();
                    dlg.InitialDirectory = "C:\\";
                    dlg.RestoreDirectory = true;
                    dlg.Filter = "pdf files (*.pdf)|*.pdf";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        if (string.IsNullOrEmpty(dlg.FileName) == false)
                        {
                            string filePath = dlg.FileName;
                            this.ToKhaiChinhReport.PrintingSystem.ExportToPdf(filePath);
                            try
                            {
                                // Open file pdf.
                                Process.Start(filePath);
                            }
                            catch (Exception ex) { throw ex; }
                        }
                    }
                    // END: SAVE FILE TO PDF.
                    this.ToKhaiChinhReport.CreateDocument();
                }
                else
                {
                    this.PhuLucReport.CreateDocument();
                    this.PhuLucReport.Margins.Top = 0;
                    this.PhuLucReport.Margins.Bottom = 0;

                    // BEGIN: SAVE FILE TO PDF.
                    SaveFileDialog dlg = new SaveFileDialog();
                    dlg.InitialDirectory = "C:\\";
                    dlg.RestoreDirectory = true;
                    dlg.Filter = "pdf files (*.pdf)|*.pdf";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        if (string.IsNullOrEmpty(dlg.FileName) == false)
                        {
                            string filePath = dlg.FileName;
                            this.PhuLucReport.PrintingSystem.ExportToPdf(filePath);
                            try
                            {
                                // Open file pdf.
                                Process.Start(filePath);
                            }
                            catch (Exception ex) { throw ex; }
                        }
                    }
                    // END: SAVE FILE TO PDF.
                    this.PhuLucReport.CreateDocument();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            //if (this.cboToKhai.SelectedIndex == 0)
            //{
            //    Globals.ExportExcel(Globals.ToKhaiType.Nhap, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThueNK.Checked, chkMienThueGTGT.Checked, "");
            //}
            //else
            //{
            //    Globals.ExportExcel(Globals.ToKhaiType.PhucLucTKN, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThueNK.Checked, chkMienThueGTGT.Checked, cboToKhai.SelectedIndex.ToString());
            //}
        }
    }
}