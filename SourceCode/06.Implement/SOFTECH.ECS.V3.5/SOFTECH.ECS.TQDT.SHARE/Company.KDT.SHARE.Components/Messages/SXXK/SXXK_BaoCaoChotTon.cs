﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.SXXK
{
    [XmlRoot("Declaration")]
    public class SXXK_BaoCaoChotTon : DeclarationBase
    {
        ///<summary>
        ///Số lượng chứng từ, phụ lục đính kèm(n..5)
        ///</summary>
        [XmlElement("GoodsItems")]
        public List<CustomsGoodsItem> GoodsItems { set; get; }
    }
}
