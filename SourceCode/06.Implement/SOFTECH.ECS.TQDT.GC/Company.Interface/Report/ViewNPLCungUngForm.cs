﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.GC;
using Company.GC.BLL.KDT.SXXK;
using DevExpress.XtraPrinting;
using Company.GC.BLL.SXXK;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using Infragistics.Excel;
using System.Threading;
using System.Diagnostics;

namespace Company.Interface.Report
{
    public partial class ViewNPLCungUngForm : BaseForm
    {
        BangNPLCungUngReport Report = new BangNPLCungUngReport();
        BangNPLCungUngReport ReportAll = new BangNPLCungUngReport();
        public Company.GC.BLL.KDT.ToKhaiMauDich TKMD;
        public HopDong HD;
        public ViewNPLCungUngForm()
        {
            InitializeComponent();
        }

        private void explorerBar1_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            this.Text += " của tờ khai xuất số " + this.TKMD.SoToKhai + "-" + this.TKMD.NgayDangKy.ToShortDateString();
            this.TKMD.LoadHMDCollection();
            for (int i = 0; i < this.TKMD.HMDCollection.Count; i++)
            {
                if(PhanBoToKhaiNhap.CheckSanPhamCungUng(TKMD.ID,TKMD.IDHopDong,TKMD.HMDCollection[i].MaPhu))
                    cbPage.Items.Add(this.TKMD.HMDCollection[i].TenHang + "/" + this.TKMD.HMDCollection[i].MaPhu, this.TKMD.HMDCollection[i].MaPhu);
            }
            if (cbPage.Items.Count > 0)
            {
                cbPage.SelectedIndex = 0;
                this.ViewPhanBo();
            }
            else
            {
                ShowMessage("Tờ khai này không có nguyên phụ liệu cung ứng", false);
                this.Close();
            }
            
        }
        private void ViewPhanBo()
        {
            this.Report.HD = this.HD;
            this.Report.TKMD = this.TKMD;
            this.Report.BindReport(cbPage.SelectedValue.ToString());
            printControl1.PrintingSystem = this.Report.PrintingSystem;
            this.Report.CreateDocument();
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            } 
        }

       
      

        

        private void cbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewPhanBo();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            } 

        }

        private void txtTo_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtTo.Value) > cbPage.Items.Count) txtTo.Value = cbPage.Items.Count;
            if (Convert.ToInt32(txtTo.Value) < 1) txtTo.Value = 1;
        }

        private void txtFrom_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtFrom.Value) < 1) txtFrom.Value = 1;
        }

        private void btnPrintAll_Click(object sender, EventArgs e)
        {
            int to = Convert.ToInt32(txtTo.Value);
            int from = Convert.ToInt32(txtFrom.Value);
            this.ReportAll.CreateDocument();
            this.ReportAll.HD = this.HD;
            this.ReportAll.TKMD = this.TKMD;
            this.ReportAll.BindReport(cbPage.Items[0].Value.ToString());
            this.ReportAll.CreateDocument();
            for (int i = from; i < to; i++)
            {
                BangNPLCungUngReport ReportTemp = new BangNPLCungUngReport();
                ReportTemp.HD = this.HD;
                ReportTemp.TKMD = this.TKMD;
                ReportTemp.BindReport(cbPage.Items[i].Value.ToString());
                ReportTemp.CreateDocument();
                this.ReportAll.Pages.AddRange(ReportTemp.Pages);
            }
            this.ReportAll.PrintingSystem.ShowMarginsWarning = false;
            this.ReportAll.PrintDialog();
        }

        

    }
}