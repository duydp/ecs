﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe04_HQGC : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public DataSet dsBK = new DataSet();
        public BangKe04_HQGC()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
             //this.PrintingSystem.ShowMarginsWarning = false;
           // DataTable dt = new Company.GC.BLL.GC.BKNguyenPhuLieu().getNPL(this.HD.ID).Tables[0];            
            DataTable dt = dsBK.Tables[0];
            dt.TableName = "t_GC_BKNguyenPhuLieu";
            this.DataSource = dt;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                }
            }
            lblMathang.Text = loaiSP;  

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong; 
         
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblDonGia.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGia", "{0:N5}");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblHTCU.DataBindings.Add("Text", this.DataSource, dt.TableName + ".HinhThucCU");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL");
            lblTriGia.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGia", "{0:N2}");
            lblLuongCU.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TongNPLCU", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".GhiChu");
            xrLabel19.Text = GlobalSettings.TieudeNgay;
        }
    }
}
