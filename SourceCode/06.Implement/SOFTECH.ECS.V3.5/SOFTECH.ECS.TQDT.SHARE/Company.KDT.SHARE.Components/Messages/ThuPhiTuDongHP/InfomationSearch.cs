﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
  public  class InfomationSearch
    {
        /// <summary>
        /// Loại hàng hóa:
        ///100 - Hàng container
        ///101 - Hàng lỏng, hàng rời
        /// </summary>
      [XmlElement("goodItemType")]
      public string GoodItemType { get; set; }

        /// <summary>
        /// Số biên lai thu tiền
        /// </summary>
      [XmlElement("receiptNo")]
      public string ReceiptNo { get; set; }

        /// <summary>
        /// Số tờ khai cần tra cứu
        /// </summary>
      [XmlElement("customsReference")]
      public string CustomsReference { get; set; }

        /// <summary>
        /// Số vận đơn cần tra cứu
        /// </summary>
      [XmlElement("billOfLading")]
      public string BillOfLading { get; set; }

        /// <summary>
        /// Số container
        /// </summary>
      [XmlElement("containerNo")]
      public string ContainerNo { get; set; }
    }
}
