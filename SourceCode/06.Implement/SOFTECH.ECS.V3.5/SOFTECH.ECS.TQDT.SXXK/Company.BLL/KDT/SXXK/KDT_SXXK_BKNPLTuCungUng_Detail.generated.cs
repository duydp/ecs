using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_SXXK_BKNPLTuCungUng_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long BangKeHoSoThanhLy_ID { set; get; }
		public string MaNPL { set; get; }
		public string TenNPL { set; get; }
		public string DVT_ID { set; get; }
		public decimal DonGiaTT { set; get; }
		public decimal ThueSuat { set; get; }
		public decimal LuongTuCungUng { set; get; }
		public string SoChungTu { set; get; }
		public DateTime NgayChungTu { set; get; }
		public int LoaiNPL { set; get; }
		public string GiaiTrinh { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_SXXK_BKNPLTuCungUng_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_SXXK_BKNPLTuCungUng_Detail> collection = new List<KDT_SXXK_BKNPLTuCungUng_Detail>();
			while (reader.Read())
			{
				KDT_SXXK_BKNPLTuCungUng_Detail entity = new KDT_SXXK_BKNPLTuCungUng_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) entity.ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTuCungUng"))) entity.LuongTuCungUng = reader.GetDecimal(reader.GetOrdinal("LuongTuCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiNPL"))) entity.LoaiNPL = reader.GetInt32(reader.GetOrdinal("LoaiNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiaiTrinh"))) entity.GiaiTrinh = reader.GetString(reader.GetOrdinal("GiaiTrinh"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_SXXK_BKNPLTuCungUng_Detail> collection, long id)
        {
            foreach (KDT_SXXK_BKNPLTuCungUng_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_SXXK_BKNPLTuCungUng_Detail VALUES(@BangKeHoSoThanhLy_ID, @MaNPL, @TenNPL, @DVT_ID, @DonGiaTT, @ThueSuat, @LuongTuCungUng, @SoChungTu, @NgayChungTu, @LoaiNPL, @GiaiTrinh)";
            string update = "UPDATE t_KDT_SXXK_BKNPLTuCungUng_Detail SET BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, DVT_ID = @DVT_ID, DonGiaTT = @DonGiaTT, ThueSuat = @ThueSuat, LuongTuCungUng = @LuongTuCungUng, SoChungTu = @SoChungTu, NgayChungTu = @NgayChungTu, LoaiNPL = @LoaiNPL, GiaiTrinh = @GiaiTrinh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_BKNPLTuCungUng_Detail WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, "BangKeHoSoThanhLy_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTuCungUng", SqlDbType.Decimal, "LuongTuCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.VarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiNPL", SqlDbType.Int, "LoaiNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaiTrinh", SqlDbType.NVarChar, "GiaiTrinh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, "BangKeHoSoThanhLy_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTuCungUng", SqlDbType.Decimal, "LuongTuCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.VarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiNPL", SqlDbType.Int, "LoaiNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaiTrinh", SqlDbType.NVarChar, "GiaiTrinh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_SXXK_BKNPLTuCungUng_Detail VALUES(@BangKeHoSoThanhLy_ID, @MaNPL, @TenNPL, @DVT_ID, @DonGiaTT, @ThueSuat, @LuongTuCungUng, @SoChungTu, @NgayChungTu, @LoaiNPL, @GiaiTrinh)";
            string update = "UPDATE t_KDT_SXXK_BKNPLTuCungUng_Detail SET BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, DVT_ID = @DVT_ID, DonGiaTT = @DonGiaTT, ThueSuat = @ThueSuat, LuongTuCungUng = @LuongTuCungUng, SoChungTu = @SoChungTu, NgayChungTu = @NgayChungTu, LoaiNPL = @LoaiNPL, GiaiTrinh = @GiaiTrinh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_BKNPLTuCungUng_Detail WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, "BangKeHoSoThanhLy_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTuCungUng", SqlDbType.Decimal, "LuongTuCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.VarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiNPL", SqlDbType.Int, "LoaiNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaiTrinh", SqlDbType.NVarChar, "GiaiTrinh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, "BangKeHoSoThanhLy_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuat", SqlDbType.Decimal, "ThueSuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTuCungUng", SqlDbType.Decimal, "LuongTuCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.VarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiNPL", SqlDbType.Int, "LoaiNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaiTrinh", SqlDbType.NVarChar, "GiaiTrinh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_SXXK_BKNPLTuCungUng_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_SXXK_BKNPLTuCungUng_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_SXXK_BKNPLTuCungUng_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_SXXK_BKNPLTuCungUng_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_SXXK_BKNPLTuCungUng_Detail(long bangKeHoSoThanhLy_ID, string maNPL, string tenNPL, string dVT_ID, decimal donGiaTT, decimal thueSuat, decimal luongTuCungUng, string soChungTu, DateTime ngayChungTu, int loaiNPL, string giaiTrinh)
		{
			KDT_SXXK_BKNPLTuCungUng_Detail entity = new KDT_SXXK_BKNPLTuCungUng_Detail();	
			entity.BangKeHoSoThanhLy_ID = bangKeHoSoThanhLy_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.DVT_ID = dVT_ID;
			entity.DonGiaTT = donGiaTT;
			entity.ThueSuat = thueSuat;
			entity.LuongTuCungUng = luongTuCungUng;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.LoaiNPL = loaiNPL;
			entity.GiaiTrinh = giaiTrinh;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
			db.AddInParameter(dbCommand, "@LuongTuCungUng", SqlDbType.Decimal, LuongTuCungUng);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.VarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@LoaiNPL", SqlDbType.Int, LoaiNPL);
			db.AddInParameter(dbCommand, "@GiaiTrinh", SqlDbType.NVarChar, GiaiTrinh);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_SXXK_BKNPLTuCungUng_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_BKNPLTuCungUng_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_SXXK_BKNPLTuCungUng_Detail(long id, long bangKeHoSoThanhLy_ID, string maNPL, string tenNPL, string dVT_ID, decimal donGiaTT, decimal thueSuat, decimal luongTuCungUng, string soChungTu, DateTime ngayChungTu, int loaiNPL, string giaiTrinh)
		{
			KDT_SXXK_BKNPLTuCungUng_Detail entity = new KDT_SXXK_BKNPLTuCungUng_Detail();			
			entity.ID = id;
			entity.BangKeHoSoThanhLy_ID = bangKeHoSoThanhLy_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.DVT_ID = dVT_ID;
			entity.DonGiaTT = donGiaTT;
			entity.ThueSuat = thueSuat;
			entity.LuongTuCungUng = luongTuCungUng;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.LoaiNPL = loaiNPL;
			entity.GiaiTrinh = giaiTrinh;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_BKNPLTuCungUng_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
			db.AddInParameter(dbCommand, "@LuongTuCungUng", SqlDbType.Decimal, LuongTuCungUng);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.VarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@LoaiNPL", SqlDbType.Int, LoaiNPL);
			db.AddInParameter(dbCommand, "@GiaiTrinh", SqlDbType.NVarChar, GiaiTrinh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_SXXK_BKNPLTuCungUng_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_BKNPLTuCungUng_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_SXXK_BKNPLTuCungUng_Detail(long id, long bangKeHoSoThanhLy_ID, string maNPL, string tenNPL, string dVT_ID, decimal donGiaTT, decimal thueSuat, decimal luongTuCungUng, string soChungTu, DateTime ngayChungTu, int loaiNPL, string giaiTrinh)
		{
			KDT_SXXK_BKNPLTuCungUng_Detail entity = new KDT_SXXK_BKNPLTuCungUng_Detail();			
			entity.ID = id;
			entity.BangKeHoSoThanhLy_ID = bangKeHoSoThanhLy_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.DVT_ID = dVT_ID;
			entity.DonGiaTT = donGiaTT;
			entity.ThueSuat = thueSuat;
			entity.LuongTuCungUng = luongTuCungUng;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.LoaiNPL = loaiNPL;
			entity.GiaiTrinh = giaiTrinh;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
			db.AddInParameter(dbCommand, "@LuongTuCungUng", SqlDbType.Decimal, LuongTuCungUng);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.VarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@LoaiNPL", SqlDbType.Int, LoaiNPL);
			db.AddInParameter(dbCommand, "@GiaiTrinh", SqlDbType.NVarChar, GiaiTrinh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_SXXK_BKNPLTuCungUng_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_BKNPLTuCungUng_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_SXXK_BKNPLTuCungUng_Detail(long id)
		{
			KDT_SXXK_BKNPLTuCungUng_Detail entity = new KDT_SXXK_BKNPLTuCungUng_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_SXXK_BKNPLTuCungUng_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_SXXK_BKNPLTuCungUng_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}