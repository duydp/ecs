﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
  public  class AffiliatedMemberCompanies
    {
        /// <summary>
        /// Tên công ty mẹ
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Mã công ty mẹ
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; }

       /// <summary>
        /// Số lượng thành viên
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }

        /// <summary>
        /// Công ty thành viên trực thuộc Công ty mẹ nhập khẩu, cung ứng nguyên liệu, vật tư để sản xuất xuất khẩu cho các đơn vị thành viên khác trực thuộc Công ty mẹ 
        /// </summary>
        [XmlElement("AffiliatedMemberCompany")]
        public List<AffiliatedMemberCompany> AffiliatedMemberCompany { get; set; }
    }
}
