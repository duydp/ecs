GO
IF NOT EXISTS (SELECT 1 FROM sys.columns 
          WHERE Name = N'SoTiepNhan'
          AND Object_ID = Object_ID(N't_SXXK_DinhMuc_Log'))
BEGIN
	ALTER TABLE dbo.t_SXXK_DinhMuc_Log
	ADD 
		MaDinhDanh NVARCHAR(50) NULL,
		SoTiepNhan NUMERIC(18,0) NULL,
		NgayTiepNhan DATETIME NULL,
		TrangThaiXuLy INT NULL,
		LenhSanXuat_ID BIGINT NULL DEFAULT(0),
		GuidString NVARCHAR(50) NULL
END
GO
UPDATE dbo.t_SXXK_DinhMuc_Log SET LenhSanXuat_ID = 0 WHERE LenhSanXuat_ID IS NULL
GO
ALTER TABLE dbo.t_SXXK_DinhMuc_Log ALTER COLUMN LenhSanXuat_ID BIGINT NOT NULL

GO
IF NOT EXISTS (SELECT 1 FROM sys.columns 
          WHERE Name = N'SoTiepNhan'
          AND Object_ID = Object_ID(N't_SXXK_DinhMuc'))
BEGIN
	ALTER TABLE dbo.t_SXXK_DinhMuc
	ADD 
		MaDinhDanh NVARCHAR(50) NULL,
		SoTiepNhan NUMERIC(18,0) NULL,
		NgayTiepNhan DATETIME NULL,
		TrangThaiXuLy INT NULL,
		LenhSanXuat_ID BIGINT NULL DEFAULT(0),
		GuidString NVARCHAR(50) NULL
END
GO


ALTER TRIGGER trg_t_SXXK_DinhMuc_Log   
ON dbo.t_SXXK_DinhMuc  
AFTER INSERT,UPDATE,DELETE AS  
BEGIN  
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)  
 BEGIN  
        INSERT INTO t_SXXK_DinhMuc_Log  
  SELECT Inserted.MaHaiQuan,
         Inserted.MaDoanhNghiep,
         Inserted.MaSanPham,
         Inserted.MaNguyenPhuLieu,
         Inserted.DinhMucSuDung,
         Inserted.TyLeHaoHut,
         Inserted.DinhMucChung,
         Inserted.GhiChu,
         Inserted.IsFromVietNam,
		 GETDATE(),'Inserted',
		 Inserted.MaDinhDanh,
         Inserted.SoTiepNhan,
         Inserted.NgayTiepNhan,
         Inserted.TrangThaiXuLy,
         Inserted.LenhSanXuat_ID,
         Inserted.GuidString FROM Inserted  
 END  
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)   
 BEGIN  
        INSERT INTO t_SXXK_DinhMuc_Log  
  SELECT Inserted.MaHaiQuan,
         Inserted.MaDoanhNghiep,
         Inserted.MaSanPham,
         Inserted.MaNguyenPhuLieu,
         Inserted.DinhMucSuDung,
         Inserted.TyLeHaoHut,
         Inserted.DinhMucChung,
         Inserted.GhiChu,
         Inserted.IsFromVietNam,
		 GETDATE(),'Updated',
		 Inserted.MaDinhDanh,
         Inserted.SoTiepNhan,
         Inserted.NgayTiepNhan,
         Inserted.TrangThaiXuLy,
         Inserted.LenhSanXuat_ID,
         Inserted.GuidString FROM Inserted  
 END  
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)  
 BEGIN  
        INSERT INTO t_SXXK_DinhMuc_Log  
  SELECT Deleted.MaHaiQuan,
         Deleted.MaDoanhNghiep,
         Deleted.MaSanPham,
         Deleted.MaNguyenPhuLieu,
         Deleted.DinhMucSuDung,
         Deleted.TyLeHaoHut,
         Deleted.DinhMucChung,
         Deleted.GhiChu,
         Deleted.IsFromVietNam,
		 GETDATE(),'Deleted',
		 Deleted.MaDinhDanh,
         Deleted.SoTiepNhan,
         Deleted.NgayTiepNhan,
         Deleted.TrangThaiXuLy,
         Deleted.LenhSanXuat_ID,
         Deleted.GuidString FROM Deleted  
 END  
END  

GO
UPDATE dbo.t_SXXK_DinhMuc SET LenhSanXuat_ID = 0 WHERE LenhSanXuat_ID IS NULL

GO
ALTER TABLE dbo.t_SXXK_DinhMuc ALTER COLUMN LenhSanXuat_ID BIGINT NOT NULL

GO
IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE TYPE_DESC =  'PRIMARY_KEY_CONSTRAINT' AND NAME = 'PK_t_SXXK_DinhMuc')
	ALTER TABLE [dbo].[t_SXXK_DinhMuc] DROP CONSTRAINT [PK_t_SXXK_DinhMuc]
GO
ALTER TABLE [dbo].[t_SXXK_DinhMuc] ADD CONSTRAINT [PK_t_SXXK_DinhMuc] PRIMARY KEY CLUSTERED ([MaHaiQuan], [MaDoanhNghiep], [MaSanPham], [MaNguyenPhuLieu],LenhSanXuat_ID) ON [PRIMARY]
GO

IF OBJECT_ID('t_KDT_SXXK_DinhMucThucTeDangKy') IS NULL
CREATE TABLE t_KDT_SXXK_DinhMucThucTeDangKy
(
	ID BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	SoTiepNhan NUMERIC(18,0) ,
	NgayTiepNhan DATETIME,
	TrangThaiXuLy INT NOT NULL,
	GuidString NVARCHAR(50) NULL,
	MaDoanhNghiep VARCHAR(20) NOT NULL,
	TenDoanhNghiep NVARCHAR(255) NOT NULL,
	MaHaiQuan VARCHAR(6) NOT NULL,
	LenhSanXuat_ID BIGINT FOREIGN KEY REFERENCES t_KDT_LenhSanXuat(ID),
	GhiChu NVARCHAR(255),
)
GO

IF OBJECT_ID('t_KDT_SXXK_DinhMucThucTe_SP') IS NULL
CREATE TABLE t_KDT_SXXK_DinhMucThucTe_SP
(
	ID BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	DinhMucThucTe_ID BIGINT NOT NULL FOREIGN KEY REFERENCES t_KDT_SXXK_DinhMucThucTeDangKy(ID),
	[MaSanPham] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	GhiChu NVARCHAR(255),
	TrangThai INT NOT NULL DEFAULT (0)
)
GO

IF OBJECT_ID('t_KDT_SXXK_DinhMucThucTe_DinhMuc') IS NULL
CREATE TABLE t_KDT_SXXK_DinhMucThucTe_DinhMuc
(
	ID BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	DinhMucThucTeSP_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.t_KDT_SXXK_DinhMucThucTe_SP(ID),
	[STT] [int] NOT NULL,
	[MaSanPham] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DVT_SP] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MaNPL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TenNPL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DVT_NPL] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DinhMucSuDung] [numeric] (18, 8) NOT NULL,
	[TyLeHaoHut] [numeric] (18, 8) DEFAULT(0),
	[NPL_TuCungUng] [numeric] (18, 6) NULL,
	GhiChu NVARCHAR(255),
)
GO

IF OBJECT_ID('p_KDT_HangMauDich_SelectDistinctDynamicHMDHSTL') IS NOT NULL
DROP PROCEDURE p_KDT_HangMauDich_SelectDistinctDynamicHMDHSTL
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectDistinctHMDHSTLDynamic]  
-- Database: ECS_TQDT_SXXK_V4  
-- Author: Ngo Thanh Tung  
-- Time created: Wednesday, April 17, 2013  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_SelectDistinctDynamicHMDHSTL]  
 @BangKeHoSoThanhLy_ID BIGINT
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT 
DISTINCT MaPhu 
FROM dbo.t_KDT_ToKhaiMauDich 
INNER JOIN dbo.t_KDT_HangMauDich 
ON 
t_KDT_HangMauDich.TKMD_ID = t_KDT_ToKhaiMauDich.ID 
WHERE SoToKhai IN (SELECT SoToKhai FROM dbo.t_KDT_SXXK_BKToKhaiXuat WHERE BangKeHoSoThanhLy_ID ='+ @BangKeHoSoThanhLy_ID +')'  
 
EXEC sp_executesql @SQL
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_DeleteBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Insert]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam bit,
	@MaDinhDanh nvarchar(50),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@LenhSanXuat_ID bigint,
	@GuidString nvarchar(50)
AS
INSERT INTO [dbo].[t_SXXK_DinhMuc]
(
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam],
	[MaDinhDanh],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[LenhSanXuat_ID],
	[GuidString]
)
VALUES
(
	@MaHaiQuan,
	@MaDoanhNghiep,
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@DinhMucChung,
	@GhiChu,
	@IsFromVietNam,
	@MaDinhDanh,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@LenhSanXuat_ID,
	@GuidString
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Update]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam bit,
	@MaDinhDanh nvarchar(50),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@LenhSanXuat_ID bigint,
	@GuidString nvarchar(50)
AS

UPDATE
	[dbo].[t_SXXK_DinhMuc]
SET
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[DinhMucChung] = @DinhMucChung,
	[GhiChu] = @GhiChu,
	[IsFromVietNam] = @IsFromVietNam,
	[MaDinhDanh] = @MaDinhDanh,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_InsertUpdate]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam bit,
	@MaDinhDanh nvarchar(50),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@LenhSanXuat_ID bigint,
	@GuidString nvarchar(50)
AS
IF EXISTS(SELECT [MaHaiQuan], [MaDoanhNghiep], [MaSanPham], [MaNguyenPhuLieu], [LenhSanXuat_ID] FROM [dbo].[t_SXXK_DinhMuc] WHERE [MaHaiQuan] = @MaHaiQuan AND [MaDoanhNghiep] = @MaDoanhNghiep AND [MaSanPham] = @MaSanPham AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu AND [LenhSanXuat_ID] = @LenhSanXuat_ID)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_DinhMuc] 
		SET
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[DinhMucChung] = @DinhMucChung,
			[GhiChu] = @GhiChu,
			[IsFromVietNam] = @IsFromVietNam,
			[MaDinhDanh] = @MaDinhDanh,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString
		WHERE
			[MaHaiQuan] = @MaHaiQuan
			AND [MaDoanhNghiep] = @MaDoanhNghiep
			AND [MaSanPham] = @MaSanPham
			AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
			AND [LenhSanXuat_ID] = @LenhSanXuat_ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_DinhMuc]
	(
			[MaHaiQuan],
			[MaDoanhNghiep],
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[DinhMucChung],
			[GhiChu],
			[IsFromVietNam],
			[MaDinhDanh],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[LenhSanXuat_ID],
			[GuidString]
	)
	VALUES
	(
			@MaHaiQuan,
			@MaDoanhNghiep,
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@DinhMucChung,
			@GhiChu,
			@IsFromVietNam,
			@MaDinhDanh,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@LenhSanXuat_ID,
			@GuidString
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Delete]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@LenhSanXuat_ID bigint
AS

DELETE FROM 
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_DeleteBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6)
AS

DELETE FROM [dbo].[t_SXXK_DinhMuc]
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Load]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@LenhSanXuat_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam],
	[MaDinhDanh],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[LenhSanXuat_ID],
	[GuidString]
FROM
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam],
	[MaDinhDanh],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[LenhSanXuat_ID],
	[GuidString]
FROM
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam],
	[MaDinhDanh],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[LenhSanXuat_ID],
	[GuidString]
FROM [dbo].[t_SXXK_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectAll]















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam],
	[MaDinhDanh],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[LenhSanXuat_ID],
	[GuidString]
FROM
	[dbo].[t_SXXK_DinhMuc]	

GO
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Update]  
-- Database: ECS_TQDT_SXXK_V4  
-----------------------------------------------  
GO  
ALTER PROCEDURE [dbo].[p_SXXK_DinhMuc_Update]  
 @MaHaiQuan char(6),  
 @MaDoanhNghiep varchar(14),  
 @MaSanPham nvarchar(50),  
 @MaNguyenPhuLieu nvarchar(50),  
 @DinhMucSuDung numeric(18, 8),  
 @TyLeHaoHut numeric(18, 5),  
 @DinhMucChung numeric(18, 8),  
 @GhiChu varchar(250),  
 @IsFromVietNam bit,  
 @MaDinhDanh nvarchar(50),  
 @SoTiepNhan numeric(18, 0),  
 @NgayTiepNhan datetime,  
 @TrangThaiXuLy int,  
 @LenhSanXuat_ID bigint,  
 @GuidString nvarchar(50)  
AS  
  
UPDATE  
 [dbo].[t_SXXK_DinhMuc]  
SET  
 [DinhMucSuDung] = @DinhMucSuDung,  
 [TyLeHaoHut] = @TyLeHaoHut,  
 [DinhMucChung] = @DinhMucChung,  
 [GhiChu] = @GhiChu,  
 [IsFromVietNam] = @IsFromVietNam,  
 [MaDinhDanh] = @MaDinhDanh,  
 [SoTiepNhan] = @SoTiepNhan,  
 [NgayTiepNhan] = @NgayTiepNhan,  
 [TrangThaiXuLy] = @TrangThaiXuLy,  
 [GuidString] = @GuidString  ,
 [LenhSanXuat_ID] = @LenhSanXuat_ID 
WHERE  
 [MaHaiQuan] = @MaHaiQuan  
 AND [MaDoanhNghiep] = @MaDoanhNghiep  
 AND [MaSanPham] = @MaSanPham  
 AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu  
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Insert]
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@LenhSanXuat_ID bigint,
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@GuidString,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaHaiQuan,
	@LenhSanXuat_ID,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Update]
	@ID bigint,
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@LenhSanXuat_ID bigint,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaHaiQuan] = @MaHaiQuan,
	[LenhSanXuat_ID] = @LenhSanXuat_ID,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@LenhSanXuat_ID bigint,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaHaiQuan] = @MaHaiQuan,
			[LenhSanXuat_ID] = @LenhSanXuat_ID,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[GuidString],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaHaiQuan],
			[LenhSanXuat_ID],
			[GhiChu]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@GuidString,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaHaiQuan,
			@LenhSanXuat_ID,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]
	@LenhSanXuat_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
WHERE
	[LenhSanXuat_ID] = @LenhSanXuat_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]
	@LenhSanXuat_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
WHERE
	[LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectBy_DinhMucThucTe_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectBy_DinhMucThucTe_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_SP_DeleteBy_DinhMucThucTe_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_DeleteBy_DinhMucThucTe_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Insert]
	@DinhMucThucTe_ID bigint,
	@MaSanPham nvarchar(50),
	@TenSanPham nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@GhiChu nvarchar(255),
	@TrangThai int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_DinhMucThucTe_SP]
(
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
)
VALUES 
(
	@DinhMucThucTe_ID,
	@MaSanPham,
	@TenSanPham,
	@MaHS,
	@DVT_ID,
	@GhiChu,
	@TrangThai
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Update]
	@ID bigint,
	@DinhMucThucTe_ID bigint,
	@MaSanPham nvarchar(50),
	@TenSanPham nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@GhiChu nvarchar(255),
	@TrangThai int
AS

UPDATE
	[dbo].[t_KDT_SXXK_DinhMucThucTe_SP]
SET
	[DinhMucThucTe_ID] = @DinhMucThucTe_ID,
	[MaSanPham] = @MaSanPham,
	[TenSanPham] = @TenSanPham,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[GhiChu] = @GhiChu,
	[TrangThai] = @TrangThai
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_InsertUpdate]
	@ID bigint,
	@DinhMucThucTe_ID bigint,
	@MaSanPham nvarchar(50),
	@TenSanPham nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@GhiChu nvarchar(255),
	@TrangThai int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_DinhMucThucTe_SP] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_DinhMucThucTe_SP] 
		SET
			[DinhMucThucTe_ID] = @DinhMucThucTe_ID,
			[MaSanPham] = @MaSanPham,
			[TenSanPham] = @TenSanPham,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[GhiChu] = @GhiChu,
			[TrangThai] = @TrangThai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_DinhMucThucTe_SP]
		(
			[DinhMucThucTe_ID],
			[MaSanPham],
			[TenSanPham],
			[MaHS],
			[DVT_ID],
			[GhiChu],
			[TrangThai]
		)
		VALUES 
		(
			@DinhMucThucTe_ID,
			@MaSanPham,
			@TenSanPham,
			@MaHS,
			@DVT_ID,
			@GhiChu,
			@TrangThai
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_DinhMucThucTe_SP]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_DeleteBy_DinhMucThucTe_ID]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_DeleteBy_DinhMucThucTe_ID]
	@DinhMucThucTe_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_DinhMucThucTe_SP]
WHERE
	[DinhMucThucTe_ID] = @DinhMucThucTe_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_DinhMucThucTe_SP] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTe_SP]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectBy_DinhMucThucTe_ID]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectBy_DinhMucThucTe_ID]
	@DinhMucThucTe_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTe_SP]
WHERE
	[DinhMucThucTe_ID] = @DinhMucThucTe_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM [dbo].[t_KDT_SXXK_DinhMucThucTe_SP] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_SP_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTe_SP]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Insert]
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@LenhSanXuat_ID bigint,
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@GuidString,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaHaiQuan,
	@LenhSanXuat_ID,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Update]
	@ID bigint,
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@LenhSanXuat_ID bigint,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaHaiQuan] = @MaHaiQuan,
	[LenhSanXuat_ID] = @LenhSanXuat_ID,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@LenhSanXuat_ID bigint,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaHaiQuan] = @MaHaiQuan,
			[LenhSanXuat_ID] = @LenhSanXuat_ID,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[GuidString],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaHaiQuan],
			[LenhSanXuat_ID],
			[GhiChu]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@GuidString,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaHaiQuan,
			@LenhSanXuat_ID,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]
	@LenhSanXuat_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
WHERE
	[LenhSanXuat_ID] = @LenhSanXuat_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]
	@LenhSanXuat_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]
WHERE
	[LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM [dbo].[t_KDT_SXXK_DinhMucThucTeDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTeDangKy_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTeDangKy]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('41.2',GETDATE(), N'CẬP NHẬT ĐỊNH MỨC THỰC TẾ ')
END
