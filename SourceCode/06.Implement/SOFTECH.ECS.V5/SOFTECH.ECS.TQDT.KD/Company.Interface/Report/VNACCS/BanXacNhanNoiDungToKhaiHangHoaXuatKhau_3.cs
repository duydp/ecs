using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;



namespace Company.Interface.Report.VNACCS
{
    public partial class BanXacNhanNoiDungToKhaiHangHoaXuatKhau_3 : DevExpress.XtraReports.UI.XtraReport
    {
        public BanXacNhanNoiDungToKhaiHangHoaXuatKhau_3()
        {
            InitializeComponent();
        }
        public void BindReport(VAE1LD0 Vae)
        {

            lblSotokhai.Text = Vae.ECN.GetValue().ToString();
            lblSotokhaidautien.Text = Vae.FIC.GetValue().ToString();
            lblSonhanhtokhaichianho.Text = Vae.BNO.GetValue().ToString();
            lblTongsotokhaichianho.Text = Vae.DNO.GetValue().ToString();
            lblSotokhaitamnhaptaixuat.Text = Vae.TDN.GetValue().ToString();
            lblMaphanloaikiemtra.Text = Vae.K07.GetValue().ToString();
            lblMaloaihinh.Text = Vae.ECB.GetValue().ToString();
            lblMaphanloaihanghoa.Text = Vae.CCC.GetValue().ToString();
            lblMahieuphuongthucvanchuyen.Text = Vae.MTC.GetValue().ToString();
            lblMasothuedaidien.Text = Vae.K01.GetValue().ToString();
            lblTencoquanhaiquantiepnhantokhai.Text = Vae.K08.GetValue().ToString();
            lblNhomxulyhoso.Text = Vae.CHB.GetValue().ToString();
            if
            (Convert.ToDateTime(Vae.K10.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydangky.Text = Convert.ToDateTime(Vae.K10.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydangky.Text = "";
            }

            if (Vae.AD1.GetValue().ToString() != "" && Vae.AD1.GetValue().ToString() != "0")
            {
                lblGiodangky.Text = Vae.AD1.GetValue().ToString().Substring(0, 2) + ":" + Vae.AD1.GetValue().ToString().Substring(2, 2) + ":" + Vae.AD1.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiodangky.Text = "";
            }

            if
            (Convert.ToDateTime(Vae.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaythaydoidangky.Text = Convert.ToDateTime(Vae.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaythaydoidangky.Text = "";
            }
            if (Vae.AD3.GetValue().ToString() != "" && Vae.AD3.GetValue().ToString() != "0")
            {
                lblGiothaydoidangky.Text = Vae.AD3.GetValue().ToString().Substring(0, 2) + ":" + Vae.AD3.GetValue().ToString().Substring(2, 2) + ":" + Vae.AD3.GetValue().ToString().Substring(4, 2);
            }
               
                else
                {
                    lblGiothaydoidangky.Text = "";
                }
                
             

            if
            (Convert.ToDateTime(Vae.RID.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoihantainhapxuat.Text = Convert.ToDateTime(Vae.RID.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoihantainhapxuat.Text = "";
            }
                
            BindingReport(Vae.HangMD[0]);
        }




        public void BindingReport(VAE1LD0_HANG hmd) 
        {


            lblMasohanghoa.Text = hmd.CMD.GetValue().ToString();
            lblSodong1.Text = hmd.R01.GetValue().ToString();
            lblMasohanghoa.Text = hmd.CMD.GetValue().ToString();
            lblMaquanlyrieng.Text = hmd.COC.GetValue().ToString();
            lblMataixacnhangia.Text = hmd.R03.GetValue().ToString();
            lblMotahanghoa.Text = hmd.CMN.GetValue().ToString();
            lblSoluong1.Text = hmd.QN1.GetValue().ToString();
            lblMadvtinh1.Text = hmd.QT1.GetValue().ToString();
            lblSoluong2.Text = hmd.QN2.GetValue().ToString();
            lblMadvtinh2.Text = hmd.QT2.GetValue().ToString();
            lblTrigia.Text = hmd.BPR.GetValue().ToString();
            lblDongia.Text = hmd.UPR.GetValue().ToString();
            lblMadongtien.Text = hmd.UPC.GetValue().ToString();
            lblDonvidongia.Text = hmd.TSC.GetValue().ToString();
            lblTrigiathueS.Text = hmd.R07.GetValue().ToString();
            lblMadongtienS.Text = hmd.AD9.GetValue().ToString();
            lblMadongtienM.Text = hmd.R14.GetValue().ToString();
            lblTrigiathueM.Text = hmd.R15.GetValue().ToString();
            lblSoluongthue.Text = hmd.TSQ.GetValue().ToString();
            lblMadvtinhchuanthue.Text = hmd.TSU.GetValue().ToString();
            lblDongiathue.Text = hmd.CVU.GetValue().ToString();
            lblMatientedongiathue.Text = hmd.ADA.GetValue().ToString();
            lblDonvisoluongthue.Text = hmd.QCV.GetValue().ToString();
            lblThuesuat.Text = hmd.TRA.GetValue().ToString();
            lblPhanloainhapthue.Text = hmd.TRM.GetValue().ToString();
            lblSotienthue.Text = hmd.TAX.GetValue().ToString();
            lblMatientesotienthue.Text = hmd.ADB.GetValue().ToString();
            lblSotienmiengiam.Text = hmd.REG.GetValue().ToString();
            lblMatientesotiengiamthue.Text = hmd.ADC.GetValue().ToString();
            lblSodonghangtaixuat.Text = hmd.TDL.GetValue().ToString();
            lblDanhmucmienthue.Text = hmd.TXN.GetValue().ToString();
            lblSodongmienthue.Text = hmd.TXR.GetValue().ToString();
            lblTienlephi.Text = hmd.CUP.GetValue().ToString();
            lblTienbaohiem.Text = hmd.IUP.GetValue().ToString();
            lblSoluongtienlephi.Text = hmd.CQU.GetValue().ToString();
            lblMadvtienlephi.Text = hmd.CQC.GetValue().ToString();
            lblSoluongtienbaohiem.Text = hmd.IQU.GetValue().ToString();
            lblMadvtienbaohiem.Text = hmd.IQC.GetValue().ToString();
            lblKhoantienlephi.Text = hmd.CPR.GetValue().ToString();
            lblKhoantienbaohiem.Text = hmd.IPR.GetValue().ToString();

            lblMamiengiam.Text = hmd.RE.GetValue().ToString();
            lblDieukhoanmien.Text = hmd.TRL.GetValue().ToString();

            for (int i = 0; i < hmd.OL_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMavanvanphapquy1.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();
                        break;
                    case 1:
                        lblMavanvanphapquy2.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();
                        break;
                    case 2:
                        lblMavanvanphapquy3.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();
                        break;
                    case 3:
                        lblMavanvanphapquy4.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();

                        break;
                    case 4:
                        lblMavanvanphapquy5.Text = hmd.OL_.listAttribute[0].GetValueCollection(i).ToString();
                        break;

                }

            }


        }
    }
}
