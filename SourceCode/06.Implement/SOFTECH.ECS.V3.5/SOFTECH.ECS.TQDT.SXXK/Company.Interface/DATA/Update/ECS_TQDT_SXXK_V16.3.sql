

/****** Object:  Table [dbo].[t_VNACCS_CapSoToKhai]    Script Date: 05/09/2014 08:23:56 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACCS_CapSoToKhai]') AND type in (N'U'))
BEGIN 


/****** Object:  Table [dbo].[t_VNACCS_CapSoToKhai]    Script Date: 05/09/2014 08:23:56 ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[t_VNACCS_CapSoToKhai](
	[SoTK] [int] NOT NULL,
	[MaLoaiHinh] [varchar](50) NOT NULL,
	[NamDangKy] [int] NOT NULL,
	[SoTKVNACCS] [decimal](15, 0) NOT NULL,
	[SoTKVNACCSFull] [decimal](12, 0) NULL,
	[SoTKDauTien] [decimal](15, 0) NULL,
	[SoNhanhTK] [int] NULL,
	[TongSoTKChiaNho] [int] NULL,
	[SoTKTNTX] [decimal](15, 0) NULL,
	[Temp1] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_VNACCS_CapSoToKhai] PRIMARY KEY CLUSTERED 
(
	[SoTK] ASC,
	[MaLoaiHinh] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_t_VNACCS_CapSoToKhai] UNIQUE NONCLUSTERED 
(
	[SoTKVNACCS] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF
END 


GO 
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Update]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Load]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_CapSoToKhai_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Insert]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Insert]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(15, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255)
AS
INSERT INTO [dbo].[t_VNACCS_CapSoToKhai]
(
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
)
VALUES
(
	@SoTK,
	@MaLoaiHinh,
	@NamDangKy,
	@SoTKVNACCS,
	@SoTKVNACCSFull,
	@SoTKDauTien,
	@SoNhanhTK,
	@TongSoTKChiaNho,
	@SoTKTNTX,
	@Temp1
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Update]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Update]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(15, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255)
AS

UPDATE
	[dbo].[t_VNACCS_CapSoToKhai]
SET
	[NamDangKy] = @NamDangKy,
	[SoTKVNACCS] = @SoTKVNACCS,
	[SoTKVNACCSFull] = @SoTKVNACCSFull,
	[SoTKDauTien] = @SoTKDauTien,
	[SoNhanhTK] = @SoNhanhTK,
	[TongSoTKChiaNho] = @TongSoTKChiaNho,
	[SoTKTNTX] = @SoTKTNTX,
	[Temp1] = @Temp1
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_InsertUpdate]
	@SoTK int,
	@MaLoaiHinh varchar(50),
	@NamDangKy int,
	@SoTKVNACCS decimal(15, 0),
	@SoTKVNACCSFull decimal(12, 0),
	@SoTKDauTien decimal(15, 0),
	@SoNhanhTK int,
	@TongSoTKChiaNho int,
	@SoTKTNTX decimal(15, 0),
	@Temp1 nvarchar(255)
AS
IF EXISTS(SELECT [SoTK], [MaLoaiHinh] FROM [dbo].[t_VNACCS_CapSoToKhai] WHERE [SoTK] = @SoTK AND [MaLoaiHinh] = @MaLoaiHinh)
	BEGIN
		UPDATE
			[dbo].[t_VNACCS_CapSoToKhai] 
		SET
			[NamDangKy] = @NamDangKy,
			[SoTKVNACCS] = @SoTKVNACCS,
			[SoTKVNACCSFull] = @SoTKVNACCSFull,
			[SoTKDauTien] = @SoTKDauTien,
			[SoNhanhTK] = @SoNhanhTK,
			[TongSoTKChiaNho] = @TongSoTKChiaNho,
			[SoTKTNTX] = @SoTKTNTX,
			[Temp1] = @Temp1
		WHERE
			[SoTK] = @SoTK
			AND [MaLoaiHinh] = @MaLoaiHinh
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACCS_CapSoToKhai]
	(
			[SoTK],
			[MaLoaiHinh],
			[NamDangKy],
			[SoTKVNACCS],
			[SoTKVNACCSFull],
			[SoTKDauTien],
			[SoNhanhTK],
			[TongSoTKChiaNho],
			[SoTKTNTX],
			[Temp1]
	)
	VALUES
	(
			@SoTK,
			@MaLoaiHinh,
			@NamDangKy,
			@SoTKVNACCS,
			@SoTKVNACCSFull,
			@SoTKDauTien,
			@SoNhanhTK,
			@TongSoTKChiaNho,
			@SoTKTNTX,
			@Temp1
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Delete]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Delete]
	@SoTK int,
	@MaLoaiHinh varchar(50)
AS

DELETE FROM 
	[dbo].[t_VNACCS_CapSoToKhai]
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACCS_CapSoToKhai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Load]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Load]
	@SoTK int,
	@MaLoaiHinh varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
FROM
	[dbo].[t_VNACCS_CapSoToKhai]
WHERE
	[SoTK] = @SoTK
	AND [MaLoaiHinh] = @MaLoaiHinh
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
FROM [dbo].[t_VNACCS_CapSoToKhai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoTK],
	[MaLoaiHinh],
	[NamDangKy],
	[SoTKVNACCS],
	[SoTKVNACCSFull],
	[SoTKDauTien],
	[SoNhanhTK],
	[TongSoTKChiaNho],
	[SoTKTNTX],
	[Temp1]
FROM
	[dbo].[t_VNACCS_CapSoToKhai]	

GO




DELETE FROM t_HaiQuan_LoaiHinhMauDich WHERE id LIKE '%V%'

Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVB11',N'Xuất khẩu thương mại','B11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVB12',N'Xuất khẩu hàng hoá tạm xuất khẩu','B12',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVB13',N'Xuất khẩu hàng hoá nhập khẩu (reshipment)','B13',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE42',N'Xuất khẩu các sản phẩm từ nhà máy CX','E42',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE44',N'Xuất khẩu các sản phẩm từ CX nhà máy được ủy quyền','E44',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE46',N'Mang hàng hóa từ nhà máy CX vào nội địa','E46',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE52',N'Xuất khẩu các sản phẩm từ nhà máy GC','E52',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE54',N'Xuất khẩu các sản phẩm từ GC nhà máy được ủy quyền','E54',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE56',N'Mang hàng hóa từ nhà máy GC vào nội địa','E56',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE62',N'Xuất khẩu các sản phẩm từ nhà máy sản xuất SXXK','E62',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE64',N'Xuất khẩu SP từ nhà máy SXXK có thẩm quyền','E64',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVE82',N'Xuất khẩu nguyên liệu để chế biến nước ngoài','E82',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG21',N'Tái xuất hàng hoá thương mại ','G21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG22',N'Tái xuất máy móc, TB cho  các dự án giới hạn','G22',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG23',N'Tái xuất khẩu hàng hóa không chịu thuế tạm nhập','G23',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG24',N'Khác tái xuất','G24',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVG61',N'Xuất khẩu tạm thời hàng hóa','G61',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVC12',N'Mang hàng ra khỏi kho ngoại quan để xuất khẩu','C12',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVC22',N'Mang hàng ra khỏi khu miễn thuế khác để xuất khẩu','C22',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVAEO',N'Xuất khẩu AEO','AEO',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('XVH21',N'Xuất khẩu khác','H21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA11',N'Nhập kinh doanh tiêu dùng','A11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA12',N'Nhập kinh doanh sản xuất','A12',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA21',N'Chuyển tiêu thụ nội địa từ nguồn tạm nhập','A21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA31',N'Nhập hàng XK bị trả lại','A31',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA41',N'Nhập kinh doanh của doanh nghiệp đầu tư','A41',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA42',N'Chuyển tiêu thụ nội địa khác','A42',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA43',N'Không dùng/dự phòng','A43',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVA44',N'Nhập vào khu phi thuế quan từ nội địa','A44',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE11',N'Nhập nguyên liệu của doanh nghiệp chế xuất','E11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE13',N'Nhập tạo tài sản cố định của doanh nghiệp chế xuất','E13',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE15',N'Nhập NL của DNCX từ nội địa','E15',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE21',N'Nhập NL để GC cho  nước ngoài','E21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE23',N'Nhập NL GC từ hợp đồng khác chuyển sang','E23',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE25',N'Không dùng/dự phòng','E25',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE31',N'Nhập nguyên liệu sản xuất xuất khẩu','E31',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE33',N'Không dùng/dự phòng','E33',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVE41',N'Nhập sản phẩm thuê gia công ở nước ngoài','E41',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG11',N'Tạm nhập hàng kinh doanh tạm nhập tái xuất','G11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG12',N'Tạm nhập máy móc, thiết bị cho dự án có thời hạn','G12',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG13',N'Tạm nhập miễn thuế','G13',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG14',N'Tạm nhập khác','G14',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVG51',N'Tái nhập hàng đã tạm xuất','G51',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVC11',N'Hàng gửi kho ngoại quan','C11',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVC21',N'Hàng đưa vào khu phi thuế quan','C21',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVAEO',N'Loại hình dành cho doanh nghiệp ưu tiên','AEO',GetDate(),GetDate())
Insert into [t_HaiQuan_LoaiHinhMauDich] (ID,Ten,Ten_VT,DateCreated,DateModified) values ('NVH11',N'Các loại nhập khẩu khác','H11',GetDate(),GetDate())


GO 

  UPDATE t_KDT_VNACC_ToKhaiMauDich
  SET LoaiHang = (SELECT TOP 1 
							CASE WHEN  hmd.MaQuanLy LIKE '1%' THEN 'N'
							 WHEN hmd.MaQuanLy  LIKE '2%' THEN 'S'
							 WHEN hmd.MaQuanLy LIKE '3%' THEN 'T'
							 WHEN hmd.MaQuanLy LIKE '4%' THEN 'H' END AS LoaiHangT FROM t_kdt_VNACC_HangMauDich hmd WHERE hmd.TKMD_ID = tkmd.ID )
 From  t_KDT_VNACC_ToKhaiMauDich tkmd  
							
 GO
 
 
 

/****** Object:  View [dbo].[v_HangTon]    Script Date: 05/12/2014 20:10:59 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_HangTon]'))
DROP VIEW [dbo].[v_HangTon]
GO


/****** Object:  View [dbo].[v_HangTon]    Script Date: 05/12/2014 20:10:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_HangTon]
AS
SELECT     CASE WHEN t .MaLoaiHinh LIKE '%V%' THEN
                          (SELECT     TOP 1 SoTKVNACCS
                            FROM          t_VNACCS_CapSoToKhai
                            WHERE      SoTK = t .SoToKhai) ELSE t .SoToKhai END AS SoToKhaiVNACCS, t.MaDoanhNghiep, t.MaHaiQuan, t.SoToKhai, t.MaLoaiHinh, t.NamDangKy, t.MaNPL, 
                      hmd.TenHang AS TenNPL, hmd.DVT_ID, dvt.Ten AS TenDVT, hmd.NuocXX_ID, hmd.TriGiaKB, t.Luong, t.Ton, t.ThueXNK, t.ThueXNKTon, t.ThueTTDB, t.ThueVAT, 
                      t.ThueCLGia, t.PhuThu,
                          (SELECT     NgayDangKy
                            FROM          dbo.t_SXXK_ToKhaiMauDich
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 
                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS NgayDangKy,
                          (SELECT     COUNT(*) AS Expr1
                            FROM          dbo.t_KDT_SXXK_NPLNhapTon
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND (TonCuoi < TonDau) AND 
                                                   (LanThanhLy IN
                                                       (SELECT     LanThanhLy
                                                         FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy)) AND (MaDoanhNghiep = t.MaDoanhNghiep)) AS SoLanThanhLy,
                          (SELECT     MAX(LanThanhLy) AS Expr1
                            FROM          dbo.t_KDT_SXXK_NPLNhapTon AS t_KDT_SXXK_NPLNhapTon_2
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND (TonCuoi < TonDau) AND 
                                                   (LanThanhLy IN
                                                       (SELECT     LanThanhLy
                                                         FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_3)) AND (MaDoanhNghiep = t.MaDoanhNghiep)) 
                      AS LanThanhLy,
                          (SELECT     TrangThaiThanhKhoan
                            FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_2
                            WHERE      (LanThanhLy =
                                                       (SELECT     MAX(LanThanhLy) AS Expr1
                                                         FROM          dbo.t_KDT_SXXK_NPLNhapTon AS t_KDT_SXXK_NPLNhapTon_1
                                                         WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND 
                                                                                (TonCuoi < TonDau) AND (LanThanhLy IN
                                                                                    (SELECT     LanThanhLy
                                                                                      FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_1)) AND 
                                                                                (MaDoanhNghiep = t.MaDoanhNghiep)))) AS TrangThaiThanhKhoan,
                          (SELECT     COUNT(*) AS Expr1
                            FROM          dbo.t_SXXK_ThanhLy_NPLNhapTon AS nt INNER JOIN
                                                   dbo.t_SXXK_HangMauDich AS h ON nt.MaNPL = h.MaPhu AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 
                                                   nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan AND nt.Luong <> h.SoLuong
                            WHERE      (nt.SoToKhai = t.SoToKhai) AND (nt.MaLoaiHinh = t.MaLoaiHinh) AND (nt.NamDangKy = t.NamDangKy) AND (nt.MaNPL = t.MaNPL) AND 
                                                   (nt.MaHaiQuan = t.MaHaiQuan) AND (nt.MaDoanhNghiep = t.MaDoanhNghiep)) AS SaiSoLuong,
                          (SELECT     h.SoLuong
                            FROM          dbo.t_SXXK_ThanhLy_NPLNhapTon AS nt INNER JOIN
                                                   dbo.t_SXXK_HangMauDich AS h ON nt.MaNPL = h.MaPhu AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 
                                                   nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan AND nt.Luong <> h.SoLuong
                            WHERE      (nt.SoToKhai = t.SoToKhai) AND (nt.MaLoaiHinh = t.MaLoaiHinh) AND (nt.NamDangKy = t.NamDangKy) AND (nt.MaNPL = t.MaNPL) AND 
                                                   (nt.MaHaiQuan = t.MaHaiQuan) AND (nt.MaDoanhNghiep = t.MaDoanhNghiep)) AS SoLuongDangKy, dbo.f_KiemTra_TienTrinh_ThanhLy(t.MaHaiQuan, 
                      t.MaDoanhNghiep, t.SoToKhai, t.MaLoaiHinh, t.NamDangKy, t.MaNPL, t.Ton) AS TienTrinhChayThanhLy,
                          (SELECT     ThanhLy
                            FROM          dbo.t_SXXK_ToKhaiMauDich AS tk
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 
                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS ThanhLy, CASE WHEN
                          ((SELECT     COUNT(*)
                              FROM         [dbo].t_SXXK_ThanhLy_NPLNhapTon nt INNER JOIN
                                                    dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 
                                                    nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan
                              WHERE     nt.SoToKhai = t .SoToKhai AND nt.MaLoaiHinh = t .MaLoaiHinh AND nt.NamDangKy = t .NamDangKy AND nt.MaNPL = t .MaNPL AND 
                                                    nt.MaHaiQuan = t .MaHaiQuan AND nt.MaDoanhNghiep = t .MaDoanhNghiep AND nt.Luong <> h.SoLuong) = 0 AND
                          (SELECT     COUNT(*)
                            FROM          [dbo].[t_KDT_SXXK_NPLNhapTon]
                            WHERE      SoToKhai = t .SoToKhai AND MaLoaiHinh = t .MaLoaiHinh AND NamDangKy = t .NamDangKy AND MaNPL = t .MaNPL AND TonCuoi < TonDau AND 
                                                   LanThanhLy IN
                                                       (SELECT     LanThanhLy
                                                         FROM          t_KDT_SXXK_HoSoThanhLyDangKy) AND MaDoanhNghiep = t .MaDoanhNghiep) = 0 AND (t .Luong <> t .Ton)) 
                      THEN 1 ELSE 0 END AS LechTon,
                          (SELECT     TenChuHang
                            FROM          dbo.t_SXXK_ToKhaiMauDich AS t_SXXK_ToKhaiMauDich_2
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 
                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS TenChuHang
FROM         dbo.t_SXXK_ThanhLy_NPLNhapTon AS t LEFT OUTER JOIN
                      dbo.t_SXXK_HangMauDich AS hmd ON t.MaHaiQuan = hmd.MaHaiQuan AND t.MaNPL = hmd.MaPhu AND t.SoToKhai = hmd.SoToKhai AND 
                      t.MaLoaiHinh = hmd.MaLoaiHinh AND t.NamDangKy = hmd.NamDangKy LEFT OUTER JOIN
                      dbo.t_HaiQuan_DonViTinh AS dvt ON hmd.DVT_ID = dvt.ID
WHERE     ((CAST(t.SoToKhai AS VARCHAR(10)) + t.MaLoaiHinh + CAST(t.NamDangKy AS VARCHAR(4)) + t.MaHaiQuan) IN
                          (SELECT     CAST(SoToKhai AS VARCHAR(10)) + MaLoaiHinh + CAST(NamDangKy AS VARCHAR(4)) + MaHaiQuan AS Expr1
                            FROM          dbo.t_SXXK_ToKhaiMauDich AS t_SXXK_ToKhaiMauDich_1
                            WHERE      (MaLoaiHinh LIKE 'N%')))

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t"
            Begin Extent = 
               Top = 4
               Left = 0
               Bottom = 281
               Right = 164
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hmd"
            Begin Extent = 
               Top = 0
               Left = 616
               Bottom = 317
               Right = 780
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dvt"
            Begin Extent = 
               Top = 134
               Left = 198
               Bottom = 317
               Right = 350
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_HangTon'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_HangTon'
GO



/****** Object:  View [dbo].[v_KDT_SXXK_NPLXuatTon]    Script Date: 05/12/2014 20:09:38 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_KDT_SXXK_NPLXuatTon]'))
DROP VIEW [dbo].[v_KDT_SXXK_NPLXuatTon]
GO



/****** Object:  View [dbo].[v_KDT_SXXK_NPLXuatTon]    Script Date: 05/12/2014 20:09:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------*/
CREATE VIEW [dbo].[v_KDT_SXXK_NPLXuatTon]
AS
SELECT     TOP (100) PERCENT a.SoToKhai, a.MaLoaiHinh, a.NamDangKy, a.MaHaiQuan, c.MaSanPham AS MaSP, b.TenHang AS TenSP, e.Ten AS TenDVT_SP, 
                      b.SoLuong AS LuongSP, c.MaNguyenPhuLieu AS MaNPL, c.DinhMucChung AS DinhMuc, b.SoLuong * c.DinhMucChung AS LuongNPL, 
                      b.SoLuong * c.DinhMucChung AS TonNPL, a.BangKeHoSoThanhLy_ID, CASE WHEN year(d .NGAY_THN_THX) 
                      > 1900 THEN d .Ngay_THN_THX ELSE d .NgayDangKy END AS NgayThucXuat, CASE WHEN year(d .NgayHoanThanh) 
                      > 1900 THEN d .NgayHoanThanh ELSE d .NgayDangKy END AS NgayHoanThanhXuat, d.NgayHoanThanh, a.NgayDangKy, b.MaHaiQuan AS Expr1
FROM         dbo.t_KDT_SXXK_BKToKhaiXuat AS a INNER JOIN
                      dbo.t_SXXK_ToKhaiMauDich AS d ON a.SoToKhai = d.SoToKhai AND a.MaLoaiHinh = d.MaLoaiHinh AND a.NamDangKy = d.NamDangKy AND 
                      a.MaHaiQuan = d.MaHaiQuan INNER JOIN
                      dbo.t_SXXK_HangMauDich AS b ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh AND a.NamDangKy = b.NamDangKy AND 
                      a.MaHaiQuan = b.MaHaiQuan INNER JOIN
                      dbo.t_SXXK_DinhMuc AS c ON b.MaPhu = c.MaSanPham AND b.MaHaiQuan = c.MaHaiQuan AND d.MaDoanhNghiep = c.MaDoanhNghiep INNER JOIN
                      dbo.t_HaiQuan_DonViTinh AS e ON b.DVT_ID = e.ID
WHERE     (a.MaLoaiHinh LIKE 'XSX%' OR
                      a.MaLoaiHinh LIKE 'XV%' OR
                      a.MaLoaiHinh LIKE 'XCX%') AND (d.Xuat_NPL_SP = 'S')
ORDER BY d.NgayHoanThanh, a.SoToKhai

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 0
               Left = 8
               Bottom = 214
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 47
               Left = 253
               Bottom = 274
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 486
               Bottom = 304
               Right = 646
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 43
               Left = 679
               Bottom = 300
               Right = 855
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 230
               Left = 0
               Bottom = 346
               Right = 159
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_KDT_SXXK_NPLXuatTon'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_KDT_SXXK_NPLXuatTon'
GO



/****** Object:  UserDefinedFunction [dbo].[f_KiemTra_TienTrinh_ThanhLy]    Script Date: 05/10/2014 11:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
(
	@MaHaiQuan VARCHAR(10),
	@MaDoanhNghiep VARCHAR(50),
	@SoToKhai INT,
	@MaLoaihinh VARCHAR(10),
	@NamDangKy INT,
	@MaNPL NVARCHAR(255)
)
RETURNS NVARCHAR(500)
AS 
    
/*
@SOFTECH
HUNGTQ, CREATED 04/08/2011.
Kiem tra tien trinh thuc hien chay thanh ly: . 
Gia tri dung phai la so ton cuoi cua lan thanh ly dau = so ton dau cua lan thanh ly tiep theo. 
*/
	
BEGIN

--SET @MaHaiQuan = 'C34C'
--SET @MaDoanhNghiep = '0400101556'
--SET @SoToKhai = 152
--SET @MaLoaihinh = 'NSX01'
--SET @NamDangKy = 2010
--SET @MaNPL = 'NEPLUNG'
			
DECLARE
	@LanThanhLy INT,
	@Luong NUMERIC(18,8),
	@Luong_BanLuu NUMERIC(18,8),
	@Luong_NhapTon NUMERIC(18,8),
	@TonDau NUMERIC(18,8),
	@TonCuoi NUMERIC(18,8),
	@TonCuoi_BanLuu NUMERIC(18,8),
	@TonDauThueXNK NUMERIC(18,8),
	@TonCuoiThueXNK NUMERIC(18,8),
	@cnt1 INT,--Bien dem
	@str NVARCHAR(500),
	@ketqua NVARCHAR(500), --hien thi ket qua
	@SaiSoLuong INT,
	@Valid BIT
	
/*<I> Cap nhat lech luong nhap do bi lam tron so thap phan*/	
DECLARE curThanhLy CURSOR FOR
	SELECT  LanThanhLy, Luong, TonDau, TonCuoi, TonDauThueXNK, TonCuoiThueXNK
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon]
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
	ORDER BY TonCuoi DESC
	
--BEGIN TRANSACTION T1 ;

	SET @Valid = 1
	SET @SaiSoLuong = 0	
    SET @cnt1 = 0;
    
	SET @str = '....................................................................' + char(10)
	SET @str = @str + '		STT' + ' - ' + + 'SoToKhai' + ' - ' + 'MaLoaiHinh' + ' - ' + 'NamDangKy' + ' - ' + 'MaNPL' + char(10)
	SET @str = @str + STR(@cnt1) + ' - ' + STR(@SoToKhai) + ' - ' + @MaLoaiHinh + ' - ' + str(@NamDangKy)+ ' - ' + @MaNPL + char(10)
	--PRINT @str								
	
    OPEN curThanhLy 
    FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK
	--Fetch next record
    WHILE @@FETCH_STATUS = 0 
		BEGIN
							
			SET @cnt1 = @cnt1 + 1
						
			--PRINT STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau: ' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi: ' + cast(@TonCuoi AS VARCHAR(50))
										
			IF (@cnt1 = 1)
				BEGIN
					
					--Kiem tra so luong nhap dang ky ban dau so voi luong ton dau.
					IF (@Luong <> @TonDau)
						BEGIN
							SET @ketqua = N'Sai lượng nhập và lượng tồn đầu tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
							SET @Valid = 0
						END				
					
						  
					IF(@Valid = 1)
						BEGIN
							--Kiem tra so luong nhap dang ky ban dau so voi luong nhap luu trong ton.
							SELECT  @SaiSoLuong =  COUNT(*), @Luong_NhapTon = nt.Luong
								  FROM      [dbo].t_SXXK_ThanhLy_NPLNhapTon nt
											INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu
																				   AND nt.SoToKhai = h.SoToKhai
																				   AND nt.MaLoaiHinh = h.MaLoaiHinh
																				   AND nt.NamDangKy = h.NamDangKy
																				   AND nt.MaHaiQuan = h.MaHaiQuan
								  WHERE     nt.SoToKhai = @SoToKhai
											AND nt.MaLoaiHinh = @MaLoaiHinh
											AND nt.NamDangKy = @NamDangKy
											AND nt.MaNPL = @MaNPL
											AND nt.MaHaiQuan = @MaHaiQuan
											AND nt.MaDoanhNghiep = @MaDoanhNghiep
											AND nt.Luong <> h.SoLuong
								  GROUP BY nt.Luong
						  
							IF (@SaiSoLuong > 0 AND @Luong_NhapTon <> @Luong)
								BEGIN
									SET @ketqua = N'Sai lượng nhập tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
									SET @Valid = 0
								END
							ELSE
								BEGIN	  
									SET @ketqua = N'Thanh lý đúng'
									SET @Valid = 1
									SET @TonCuoi_BanLuu = @TonCuoi
								END
						END
				END

			--Kiem tra so ton
			IF (@cnt1 > 1 AND @Valid = 1) --kiem tra tu dong thu 2 tro di
				BEGIN

					SET @str = STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi' + cast(@TonCuoi AS VARCHAR(50))
					
					IF (@TonDau <> @TonCuoi_BanLuu AND ((@TonDau < @TonCuoi_BanLuu AND  (@TonCuoi_BanLuu - @TonDau) > 0.00001) OR (@TonDau > @TonCuoi_BanLuu) AND  (@TonDau - @TonCuoi_BanLuu) > 0.00001))
						BEGIN
							
							SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@TonCuoi_BanLuu AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
						END
					ELSE
						SET @ketqua = N'Thanh lý đúng'
				END						
			
			--Luu lai ton cuoi cua row truoc do
			SET @TonCuoi_BanLuu = @TonCuoi
				
			FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK
        END
        
    CLOSE curThanhLy --Close cursor
    DEALLOCATE curThanhLy --Deallocate cursor
	
	--PRINT @ketqua
	
    --IF @@ERROR != 0 
    --    BEGIN
            --PRINT 'Qua trinh thuc hien xay ra loi.'
            --PRINT STR(@@ERROR)
            ----ROLLBACK TRANSACTION T1 ;
            --PRINT 'ROLLBACK TRANSACTION T1'
  --      END
  --  ELSE 
		--BEGIN
			----COMMIT TRANSACTION T1 ;	
			--PRINT ''
			----PRINT 'COMMIT TRANSACTION T1'
		--END
	
	RETURN @ketqua
	
END	

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]    Script Date: 05/12/2014 21:26:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]    Script Date: 05/12/2014 21:26:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, July 01, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]
	@WhereCondition nvarchar(max),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(max)

SET @SQL = 'SELECT a.ID,
	a.STT,
	a.LanThanhLy,
	a.NamThanhLy,
	a.MaDoanhNghiep,
	Case When MaLoaiHinhNhap like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiNhap]) else [SoToKhaiNhap] end as SoToKhaiNhap ,
	a.NgayDangKyNhap,
	Case When MaLoaiHinhNhap like ''%V%'' Then Substring(MaLoaiHinhNhap,3,3) else MaLoaiHinhNhap end as MaLoaiHinhNhap,
	a.NgayThucNhap,
	UPPER(a.MaNPL) AS MaNPL,
	case WHEN a.TenNPL is NULL OR a.TenNPL ='''' then b.Ten else a.TenNPL end as TenNPL,
	a.LuongNhap,
	a.TenDVT_NPL,
	a.DonGiaTT,
	a.TyGiaTT,
	a.ThueSuat,
	a.ThueNKNop,
	Case When MaLoaiHinhXuat like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiXuat]) else [SoToKhaiXuat] end as SoToKhaiXuat,
	a.NgayDangKyXuat,
	case WHEN year(a.NgayThucXuat)<=1900 then null else a.NgayThucXuat end as NgayThucXuat,
	Case When MaLoaiHinhXuat like ''%V%'' Then Substring(MaLoaiHinhXuat,3,3) else MaLoaiHinhXuat end as MaLoaiHinhXuat,
	a.LuongNPLSuDung,
	a.LuongNPLTon,
	a.TienThueHoan,
	a.TienThueTKTiep,
	a.GhiChu,
	a.ThueXNK,
	a.Luong
 FROM t_KDT_SXXK_BCThueXNK a
 INNER JOIN t_SXXK_NguyenPhuLieu b
 ON a.MaNPL  = b.Ma AND a.MaDoanhNghiep = b.MaDoanhNghiep WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]    Script Date: 05/12/2014 21:05:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 28, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]
	@WhereCondition nvarchar(max),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(max)

SET @SQL = 'SELECT 	[ID],
	[STT],
	[LanThanhLy],
	[NamThanhLy],
	[MaDoanhNghiep],
	UPPER([MaNPL]) AS MaNPL,
	[TenNPL],
	Case When MaLoaiHinhNhap like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiNhap]) else [SoToKhaiNhap] end as SoToKhaiNhap ,
	[NgayDangKyNhap],
	[NgayHoanThanhNhap],
	Case When MaLoaiHinhNhap like ''%V%'' Then Substring(MaLoaiHinhNhap,3,3) else MaLoaiHinhNhap end as MaLoaiHinhNhap,
	[LuongNhap],
	[LuongTonDau],
	[TenDVT_NPL],
	[MaSP],
	[TenSP],
	Case When MaLoaiHinhXuat like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiXuat]) else [SoToKhaiXuat] end as SoToKhaiXuat,
	[NgayDangKyXuat],
	case WHEN year(NgayHoanThanhXuat)<=1900 then null else NgayHoanThanhXuat end as NgayHoanThanhXuat,
	Case When MaLoaiHinhXuat like ''%V%'' Then Substring(MaLoaiHinhXuat,3,3) else MaLoaiHinhXuat end as MaLoaiHinhXuat,
	[LuongSPXuat],
	[TenDVT_SP],
	[DinhMuc],
	[LuongNPLSuDung],
	Case when [SoToKhaiTaiXuat]  > 0 Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiTaiXuat]) else 0 end as SoToKhaiTaiXuat,
	[NgayTaiXuat],
	[LuongNPLTaiXuat],
	[LuongTonCuoi],
	[ThanhKhoanTiep],
	[ChuyenMucDichKhac],
	[DonGiaTT],
	[TyGiaTT],
	[ThueSuat],
	[ThueXNK],
	[ThueXNKTon],
[NgayThucXuat]
 FROM [dbo].[t_KDT_SXXK_BCXuatNhapTon] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKToKhaiNhap_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]    Script Date: 05/12/2014 14:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiNhap_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]  
 @BangKeHoSoThanhLy_ID bigint  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
  
SELECT  
 B.ID,  
 B.BangKeHoSoThanhLy_ID,  
  CASE WHEN B.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=B.SoToKhai)
		ELSE B.SoToKhai END AS SoToKhaiVNACCS,
            B.SoToKhai ,
    --    Case When  B.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = B.MaLoaiHinh)
				--else B.MaLoaiHinh end as MaLoaiHinh
				B.MaLoaiHinh,  
 B.NamDangKy,  
 B.MaHaiQuan,  
 B.NgayDangKy,  
 B.NgayThucNhap,  
 B.STTHang,  
 B.GhiChu,  
 B.UserName,  
 A.NgayHoanThanh
 ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.           
FROM  
 [dbo].[t_KDT_SXXK_BKToKhaiNhap] B  
 inner join t_SXXK_ToKhaiMauDich A  
 on B.SoToKhai=A.SoToKhai AND B.MaLoaiHinh=A.MaLoaiHinh AND B.NamDangKy=A.NamDangKy and B.MaHaiQuan=A.MaHaiQuan  
 --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
 LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)
WHERE  
 B.BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID  
  
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKToKhaiXuat_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]    Script Date: 05/12/2014 17:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BKToKhaiXuat_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh]  
 @BangKeHoSoThanhLy_ID bigint  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
SELECT  
 B.ID,  
 B.BangKeHoSoThanhLy_ID,  
 CASE WHEN B.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=B.SoToKhai)
		ELSE B.SoToKhai END AS SoToKhaiVNACCS,
            B.SoToKhai ,
 B.MaLoaiHinh,  
 B.NamDangKy,  
 B.MaHaiQuan,  
 B.NgayDangKy,  
 B.NgayThucXuat,  
 B.STTHang,  
 B.GhiChu,  
 A.NgayHoanThanh  
 ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.           
FROM  
 [dbo].[t_KDT_SXXK_BKToKhaiXuat] B  
 INNER JOIN t_SXXK_ToKhaiMauDich A  
 on B.SoToKhai=A.SoToKhai AND B.MaLoaiHinh=A.MaLoaiHinh AND B.NamDangKy=A.NamDangKy and B.MaHaiQuan=A.MaHaiQuan  
 --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
 LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)
WHERE  
 B.BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID  

GO


/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]    Script Date: 05/12/2014 14:46:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]    
@MaDoanhNghiep VARCHAR (14),              
@MaHaiQuan char(6),              
@TuNgay datetime,              
@DenNgay datetime,              
@UserName varchar(50),              
@TenChuHang nvarchar(200)              
              
AS
IF (@MaHaiQuan IS NOT NULL AND @MaHaiQuan <> '')
BEGIN
SELECT               
  A.MaHaiQuan,              
     CASE WHEN A.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh 
				A.MaLoaiHinh,           
  A.NamDangKy,              
  A.NgayDangKy,              
  A.NGAY_THN_THX,     
   A.NgayHoanThanh 
   ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.      
FROM t_SXXK_ToKhaiMauDich A              
 LEFT JOIN (SELECT DISTINCT              
    SoToKhai,              
    MaLoaiHinh,              
    NamDangKy,              
    MaHaiQuan              
   FROM t_KDT_SXXK_BKToKhaiNhap              
   WHERE              
    UserName != @UserName) D ON              
  A.SoToKhai=D.SoToKhai AND              
  A.MaLoaiHinh=D.MaLoaiHinh AND              
  A.NamDangKy=D.NamDangKy AND              
  A.MaHaiQuan=D.MaHaiQuan
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)
              
WHERE              
 A.MaDoanhNghiep = @MaDoanhNghiep AND              
 A.MaHaiQuan = @MaHaiQuan AND              
-- D.SoToKhai IS NULL AND              
 A.MaLoaiHinh LIKE 'N%'AND              
 A.TenChuHang LIKE '%' + @TenChuHang + '%' AND              
 A.NgayDangKy between @TuNgay and @DenNgay AND               
 A.THANHLY <> 'H'     
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.   
 AND CAST(a.SoToKhai AS VARCHAR(50)) + CAST(a.MaLoaiHinh AS VARCHAR(50)) + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (SELECT  CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50)) + CAST(YEAR(NgayDangKy) AS VARCHAR(50)) FROM dbo.t_KDT_ToKhaiMauDich WHERE MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep AND TrangThaiXuLy != 1 AND SoToKhai != 0)
ORDER BY A.NgayDangKy  DESC, A.SoToKhai, A.MaLoaiHinh
END
ELSE
	BEGIN
		SELECT               
  A.MaHaiQuan,              
     CASE WHEN A.MaLoaiHinh like'%V%' THEN (select SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
        Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				else A.MaLoaiHinh end as MaLoaiHinh,  
  A.NamDangKy,              
  A.NgayDangKy,              
  A.NGAY_THN_THX,     
   A.NgayHoanThanh 
   ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.      
FROM t_SXXK_ToKhaiMauDich A              
 LEFT JOIN (SELECT DISTINCT              
    SoToKhai,              
    MaLoaiHinh,              
    NamDangKy,              
    MaHaiQuan              
   FROM t_KDT_SXXK_BKToKhaiNhap              
   WHERE              
    UserName != @UserName) D ON              
  A.SoToKhai=D.SoToKhai AND              
  A.MaLoaiHinh=D.MaLoaiHinh AND              
  A.NamDangKy=D.NamDangKy              
  AND A.MaHaiQuan=D.MaHaiQuan
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy) AND a.MaHaiQuan = e.MaHaiQuan
              
WHERE              
 A.MaDoanhNghiep = @MaDoanhNghiep AND              
 --A.MaHaiQuan = @MaHaiQuan AND              
-- D.SoToKhai IS NULL AND              
 A.MaLoaiHinh LIKE 'N%'AND              
 A.TenChuHang LIKE '%' + @TenChuHang + '%' AND              
 A.NgayDangKy between @TuNgay and @DenNgay AND               
 A.THANHLY <> 'H'     
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.   
 AND CAST(a.SoToKhai AS VARCHAR(50)) + CAST(a.MaLoaiHinh AS VARCHAR(50)) + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (SELECT  CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50)) + CAST(YEAR(NgayDangKy) AS VARCHAR(50)) FROM dbo.t_KDT_ToKhaiMauDich WHERE /*MaHaiQuan = @MaHaiQuan AND*/ MaDoanhNghiep = @MaDoanhNghiep AND TrangThaiXuLy != 1 AND SoToKhai != 0)
ORDER BY A.NgayDangKy  DESC, A.SoToKhai, A.MaLoaiHinh
	END
	
GO

---------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKNKhongTheThanhKhoanDate]    Script Date: 05/11/2014 11:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKNKhongTheThanhKhoanDate]
    @MaDoanhNghiep VARCHAR(14) ,
    @MaHaiQuan CHAR(6) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME ,
    @UserName VARCHAR(50) ,
    @TenChuHang NVARCHAR(200)
AS 
    SELECT  A.MaHaiQuan ,
             CASE WHEN A.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh
				A.MaLoaiHinh,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.        
            ,
            ISNULL(e.TrangThaiXuLy, 404) AS TrangThaiXuLy
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiNhap
                        WHERE   UserName != @UserName
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan  
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.   
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            AND A.MaHaiQuan = @MaHaiQuan
            AND                
-- D.SoToKhai IS NULL AND                
            A.MaLoaiHinh LIKE 'N%'
            AND A.TenChuHang LIKE '%' + @TenChuHang + '%'
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND A.THANHLY <> 'H'       
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.     
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   MaHaiQuan = @MaHaiQuan
                    AND MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy = 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy DESC ,
            A.SoToKhai ,
            A.MaLoaiHinh  
GO



/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    Script Date: 05/10/2014 08:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]
    @MaDoanhNghiep VARCHAR(14) ,
    @MaHaiQuan CHAR(6) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
IF(@MaHaiQuan IS not NULL AND @MaHaiQuan <> '')
BEGIN
    SELECT  A.MaHaiQuan ,
            CASE WHEN A.MaLoaiHinh like'%V%' THEN (select Top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh 
		    A.MaLoaiHinh,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan        
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'X%'
                  --OR A.MaLoaiHinh LIKE 'XGC%'
                  --OR A.MaLoaiHinh LIKE 'XCX%'
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL         
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   MaHaiQuan = @MaHaiQuan
                    AND MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy != 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh       
            
END
ELSE
	BEGIN
		  SELECT  A.MaHaiQuan ,
            CASE WHEN A.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
        Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				else A.MaLoaiHinh end as MaLoaiHinh,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        --WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan        
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            --AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'X%'
                  --OR A.MaLoaiHinh LIKE 'XGC%'
                  --OR A.MaLoaiHinh LIKE 'XCX%'
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL         
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   --MaHaiQuan = @MaHaiQuan AND
                     MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy != 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh       
	END 
	
	
	GO
        
/* Comment Script        
        
SELECT           
 A.MaHaiQuan,          
 A.SoToKhai,          
 A.MaLoaiHinh,          
 A.NamDangKy,          
 A.NgayDangKy,          
 A.NGAY_THN_THX          
FROM         
(        
 --HungTQ Updated 22/06/2011: Bo sung loc du lieu theo TrangThaiXuLy = 1        
 SELECT   DISTINCT        
  A.MaHaiQuan,         
  A.MaDoanhNghiep,         
  A.SoToKhai,          
  A.MaLoaiHinh,          
  A.NamDangKy,          
  A.NgayDangKy,          
  A.NGAY_THN_THX, tkmd.TrangThaiXuLy         
 FROM t_SXXK_ToKhaiMauDich A  LEFT JOIN  dbo.t_KDT_ToKhaiMauDich tkmd        
 ON a.SoToKhai = tkmd.SoToKhai AND a.MaLoaiHinh = tkmd.MaLoaiHinh AND a.NamDangKy = YEAR(tkmd.NgayDangKy) AND A.MaHaiQuan = tkmd.MaHaiQuan AND A.MaDoanhNghiep = tkmd.MaDoanhNghiep        
) A          
 LEFT JOIN (          
   SELECT DISTINCT          
    SoToKhai,          
    MaLoaiHinh,          
    NamDangKy,          
    MaHaiQuan          
   FROM t_KDT_SXXK_BKToKhaiXuat          
   WHERE          
    MaHaiQuan = @MaHaiQuan) D ON          
  A.SoToKhai=D.SoToKhai AND          
  A.MaLoaiHinh=D.MaLoaiHinh AND          
  A.NamDangKy=D.NamDangKy AND          
  A.MaHaiQuan=D.MaHaiQuan          
WHERE          
 A.MaDoanhNghiep = @MaDoanhNghiep AND          
 A.MaHaiQuan = @MaHaiQuan AND          
 (A.MaLoaiHinh LIKE 'XSX%' OR A.MaLoaiHinh LIKE 'XGC%')          
 AND A.NgayDangKy between @TuNgay and @DenNgay          
 AND (A.NGAY_THN_THX IS NOT NULL OR year(A.NGAY_THN_THX)!=1900 )          
 AND D.SoToKhai IS NULL           
 AND A.TrangThaiXuLy = 1        
           
ORDER BY A.NgayDangKy , A.SoToKhai, A.MaLoaiHinh          
*/          
     
     
/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXKhongTheThanhKhoanDate]    Script Date: 05/10/2014 10:32:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       
ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKXKhongTheThanhKhoanDate]
    @MaDoanhNghiep VARCHAR(14) ,
    @MaHaiQuan CHAR(6) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
    SELECT  A.MaHaiQuan ,
           CASE WHEN A.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS sotokhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh
				A.MaLoaiHinh,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong , --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.             
            ISNULL(e.TrangThaiXuLy, 404) AS TrangThaiXuLy
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan      
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.     
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'X%'
                  --OR A.MaLoaiHinh LIKE 'XGC%'
                )
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho huy, sua to khai, to khai sua cho duyet, khong phe duyet.       
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   MaHaiQuan = @MaHaiQuan
                    AND MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy = 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh      
GO     
          
          /****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKNPLChuaThanhLy_SelectBy_BangKeHoSoThanhLy_ID]    Script Date: 05/12/2014 18:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLChuaThanhLy_SelectBy_BangKeHoSoThanhLy_ID]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 17, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_BKNPLChuaThanhLy_SelectBy_BangKeHoSoThanhLy_ID]
	@BangKeHoSoThanhLy_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BangKeHoSoThanhLy_ID],
	CASE WHEN MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=SoToKhai)ELSE SoToKhai END AS SoToKhaiVNACCS,
	[SoToKhai],
	[MaLoaiHinh],
	[NamDangKy],
	[MaHaiQuan],
	[NgayDangKy],
	[MaNPL],
	[TenNPL],
	[Luong],
	[DVT_ID],
	[TenDVT],
	[STTHang]
FROM
	[dbo].[t_KDT_SXXK_BKNPLChuaThanhLy]
WHERE
	[BangKeHoSoThanhLy_ID] = @BangKeHoSoThanhLy_ID
	
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BKNPLXinHuy_SelectBy_BangKeHoSoThanhLy_ID]    Script Date: 05/12/2014 18:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLXinHuy_SelectBy_BangKeHoSoThanhLy_ID]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 17, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_BKNPLXinHuy_SelectBy_BangKeHoSoThanhLy_ID]
	@BangKeHoSoThanhLy_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BangKeHoSoThanhLy_ID],
	[SoToKhai],
	CASE WHEN MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=SoToKhai)ELSE SoToKhai END AS SoToKhaiVNACCS,
	[MaLoaiHinh],
	[NamDangKy],
	[MaHaiQuan],
	[NgayDangKy],
	[MaNPL],
	[TenNPL],
	[BienBanHuy],
	[NgayHuy],
	[LuongHuy],
	[DVT_ID],
	[TenDVT],
	[STTHang]
FROM
	[dbo].[t_KDT_SXXK_BKNPLXinHuy]
WHERE
	[BangKeHoSoThanhLy_ID] = @BangKeHoSoThanhLy_ID


GO

 
  IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '16.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('16.3',GETDATE(), N'Tạo table t_VNACCS_CapSoTK,Cập nhật store,View,report')
END	

