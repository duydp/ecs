﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KeyServer;
using KeySecurity;
using License = KeySecurity.License;
namespace LicenseCode
{
    public partial class frmDemo : Form
    {
        const string productID = "ECS_TQDT_SXXK";
        const string svActiveName = "ActivatarKey";
        public frmDemo()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {

            License license = Active.Install.License;
            string sfmtLinces = string.Empty;
            if (license.Status != LicenseStatus.Licensed || license.Status != LicenseStatus.TrialVersion)
            {
                sfmtLinces = license.Message;
            }
            else
            {
                KeyInfo key = license.KeyInfo;
                sfmtLinces = string.Format("License={0}\r\nProductId={1}\r\nTrialDays={2}\r\nLastOnline={3}\r\nMachineCode={4}",
              new object[]{ key.Key, key.ProductId, key.TrialDays,key.ExpiredDate,
                      key.MachineCode}
             );
            }
            MessageBoxControl.ShowMessage(sfmtLinces);
        }

        private void btnGen_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbProduct.Text))
            {
                errorProvider1.SetError(cbProduct, "Mã sản phẩm không được để trống");
                return;
            }
            if (txtPrefix.Text == "")
            {
                errorProvider1.SetError(cbProduct, "Tiền tố không được để trống");
                return;
            }
            int soNgay = 0;
            int soLuong = 0;
            try
            {
                if (string.IsNullOrEmpty(txtSoNgay.Text))
                {
                    errorProvider1.SetError(txtSoNgay, "Số ngày không được để trống");
                    return;
                }
                else
                    soNgay = Convert.ToInt32(txtSoNgay.Text);
            }
            catch
            {
                errorProvider1.SetError(txtSoNgay, "Số ngày không hợp lệ");
                return;

            }
            try
            {
                if (string.IsNullOrEmpty(txtSoLuong.Text))
                {
                    errorProvider1.SetError(txtSoLuong, "Số lượng không được để trống");
                    return;
                }
                else
                    soLuong = Convert.ToInt32(txtSoLuong.Text);
            }
            catch
            {
                errorProvider1.SetError(txtSoLuong, "Số lượng không hợp lệ");
                return;
            }
            try
            {
                string productID = cbProduct.Text.Trim();
                string sfmtKeyNumber = txtPrefix.Text.Trim();
                int startID = 0;
                List<Key> keyItems = Key.SelectCollectionDynamic("ProductId='" + productID + "'", "SerialNumber Desc");
                if (keyItems.Count > 0)
                {
                    string maxKey = keyItems[0].SerialNumber;
                    startID = int.Parse(maxKey.Substring(7, maxKey.Length - 7));
                }
                DateTime dateTrials = new DateTime(1900, 1, 1); //DateTime.Now.Date.AddDays(soNgay);
                List<Key> keys = new List<Key>();
                for (int i = 0; i < soLuong; i++)
                {

                    Key k = new Key()
                    {
                        SerialNumber = string.Format("{0}{1}{2:00000}", sfmtKeyNumber, DateTime.Now.Year, ++startID),
                        ProductId = productID,
                        ActivedKey = Guid.NewGuid().ToString().ToUpper(),
                        Days = soNgay,
                        Status = "0",
                        ActivedDate = new DateTime(1900, 1, 1),
                        CreatedDate = DateTime.Now,
                        ExpiredDate = dateTrials,
                        LastCheckStatus = new DateTime(1900, 1, 1)

                    };
                    keys.Add(k);
                }
                Key.InsertCollection(keys);
                MessageBoxControl.ShowMessage("Tạo thành công");
            }
            catch (Exception ex)
            {

                MessageBoxControl.ShowMessage(ex.Message);
            }
        }
        private void LoadLicese()
        {

        }
        private void btnActive_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            try
            {
                Guid g = new Guid(txtSerialCode.Text.Trim());
            }
            catch
            {
                errorProvider1.SetError(txtSerialCode, "Số serial không hợp lệ");
                return;
            }

            License lic = Active.Install.ActiveKey(txtSerialCode.Text.Trim());
            lbStatus.ForeColor = Color.Black;
            lbStatus.Text = lic.Message;
            switch (lic.Status)
            {
                case LicenseStatus.Expired:
                    lbStatus.ForeColor = Color.Red;
                    break;
                case LicenseStatus.MachineHashMismatch:
                    break;
                case LicenseStatus.NotFound:
                    lbStatus.ForeColor = Color.Red;
                    break;
                case LicenseStatus.Invalid:
                    lbStatus.ForeColor = Color.Red;
                    break;
                case LicenseStatus.InternalError:
                    lbStatus.ForeColor = Color.Red;
                    break;
                default:
                    break;
            }


        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(cbProduct.Text))
            {
                 lbStatus.Text = "Mã sản phẩm không được để trống";
                return;
            }
            if (txtPrefix.Text == "")
            {
                 lbStatus.Text = "Tiền tố không được để trống";
                return;
            }
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string text = KeyCode.ReadFile(openFileDialog1.FileName);

                //Key key = Key.Load(new Guid("a5dd729c-8d76-4c22-b8be-04463f9bb259"));
                List<Key> keys = Key.SelectCollectionDynamic("SerialNumber like '" + txtPrefix.Text.Trim() + "%' and ProductId='" + cbProduct.Text.Trim() + "'", "");
                foreach (Key key in keys)
                {
                    key.Notice = text;
                    key.CustomerField10 = null;
                    if (key.LastCheckStatus.Year < 1900)
                        key.LastCheckStatus = new DateTime(1900, 1, 1);
                    key.Update();
                }
            }
        }

        private void btnCheckInfo_Click(object sender, EventArgs e)
        {
            string sfmtMsg = "{0}\nVui lòng liên hệ với Công ty cổ phẩn Softech để tiếp tục sử dụng";
            bool isShowActive = false;
            lbStatus.Text = string.Empty;
            if (KeySecurity.Active.Install.License.KeyInfo == null)
            {
                sfmtMsg = string.Format(sfmtMsg, "Không có file lic");
                isShowActive = true;
            }
            else if (KeySecurity.Active.Install.License.KeyInfo.TrialDays == 0)
            {
                sfmtMsg = string.Format(sfmtMsg, "hết hạn sử dụng");
                
                isShowActive = true;
            }
            lbStatus.Text += "\r\nNgày hết hạn: " + KeySecurity.Active.Install.License.KeyInfo.ExpiredDate.ToString();
        }
    }
}
