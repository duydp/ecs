﻿namespace LicenseCode
{
    partial class frmKeyManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout gridList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmKeyManager));
            Janus.Windows.GridEX.GridEXLayout cbbSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout cbbDoanhNghiep_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.Lock = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.gridList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.cbbSanPham = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.clcDateTo = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbbDoanhNghiep = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.clcDateFrom = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbbStatus = new Janus.Windows.EditControls.UIComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbbCDKeyType = new Janus.Windows.EditControls.UIComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDoanhNghiep)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Copy,
            this.Lock});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(149, 48);
            // 
            // Copy
            // 
            this.Copy.Name = "Copy";
            this.Copy.Size = new System.Drawing.Size(148, 22);
            this.Copy.Text = "Copy";
            // 
            // Lock
            // 
            this.Lock.Name = "Lock";
            this.Lock.Size = new System.Drawing.Size(148, 22);
            this.Lock.Text = "Thông tin Key";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox4.Controls.Add(this.gridList);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox4.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1172, 714);
            this.uiGroupBox4.TabIndex = 9;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // gridList
            // 
            this.gridList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            gridList_DesignTimeLayout.LayoutString = resources.GetString("gridList_DesignTimeLayout.LayoutString");
            this.gridList.DesignTimeLayout = gridList_DesignTimeLayout;
            this.gridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridList.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridList.FrozenColumns = 5;
            this.gridList.GridLineColor = System.Drawing.SystemColors.Control;
            this.gridList.GroupByBoxVisible = false;
            this.gridList.HeaderFormatStyle.BackColorGradient = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(238)))), ((int)(((byte)(134)))));
            this.gridList.Location = new System.Drawing.Point(0, 106);
            this.gridList.Name = "gridList";
            this.gridList.RecordNavigator = true;
            this.gridList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridList.ScrollBars = Janus.Windows.GridEX.ScrollBars.Both;
            this.gridList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.gridList.Size = new System.Drawing.Size(1172, 564);
            this.gridList.TabIndex = 12;
            this.gridList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridList_RowDoubleClick);
            this.gridList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.gridList_LoadingRow);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox7.Controls.Add(this.btnClose);
            this.uiGroupBox7.Controls.Add(this.btnDelete);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 670);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1172, 44);
            this.uiGroupBox7.TabIndex = 11;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageIndex = 0;
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1049, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(84, 24);
            this.btnClose.TabIndex = 63;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageIndex = 0;
            this.btnDelete.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDelete.Location = new System.Drawing.Point(960, 14);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(84, 24);
            this.btnDelete.TabIndex = 63;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox6.Controls.Add(this.btnExportExcel);
            this.uiGroupBox6.Controls.Add(this.btnSearch);
            this.uiGroupBox6.Controls.Add(this.cbbSanPham);
            this.uiGroupBox6.Controls.Add(this.clcDateTo);
            this.uiGroupBox6.Controls.Add(this.cbbDoanhNghiep);
            this.uiGroupBox6.Controls.Add(this.label5);
            this.uiGroupBox6.Controls.Add(this.clcDateFrom);
            this.uiGroupBox6.Controls.Add(this.label4);
            this.uiGroupBox6.Controls.Add(this.label3);
            this.uiGroupBox6.Controls.Add(this.label2);
            this.uiGroupBox6.Controls.Add(this.cbbStatus);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.cbbCDKeyType);
            this.uiGroupBox6.Controls.Add(this.label8);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1172, 106);
            this.uiGroupBox6.TabIndex = 10;
            this.uiGroupBox6.Text = "Tìm kiếm";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageIndex = 0;
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(999, 27);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(106, 24);
            this.btnExportExcel.TabIndex = 63;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageIndex = 0;
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(887, 27);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(106, 24);
            this.btnSearch.TabIndex = 63;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cbbSanPham
            // 
            cbbSanPham_DesignTimeLayout.LayoutString = resources.GetString("cbbSanPham_DesignTimeLayout.LayoutString");
            this.cbbSanPham.DesignTimeLayout = cbbSanPham_DesignTimeLayout;
            this.cbbSanPham.Location = new System.Drawing.Point(624, 28);
            this.cbbSanPham.Name = "cbbSanPham";
            this.cbbSanPham.SelectedIndex = -1;
            this.cbbSanPham.SelectedItem = null;
            this.cbbSanPham.Size = new System.Drawing.Size(257, 23);
            this.cbbSanPham.TabIndex = 14;
            this.cbbSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbSanPham.ValueChanged += new System.EventHandler(this.cbbSanPham_ValueChanged);
            // 
            // clcDateTo
            // 
            this.clcDateTo.CustomFormat = "dd/MM/yyyy";
            this.clcDateTo.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDateTo.DropDownCalendar.Name = "";
            this.clcDateTo.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDateTo.Location = new System.Drawing.Point(361, 67);
            this.clcDateTo.Name = "clcDateTo";
            this.clcDateTo.Size = new System.Drawing.Size(131, 23);
            this.clcDateTo.TabIndex = 15;
            this.clcDateTo.Value = new System.DateTime(2018, 12, 19, 0, 0, 0, 0);
            this.clcDateTo.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDateTo.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // cbbDoanhNghiep
            // 
            cbbDoanhNghiep_DesignTimeLayout.LayoutString = resources.GetString("cbbDoanhNghiep_DesignTimeLayout.LayoutString");
            this.cbbDoanhNghiep.DesignTimeLayout = cbbDoanhNghiep_DesignTimeLayout;
            this.cbbDoanhNghiep.Location = new System.Drawing.Point(624, 67);
            this.cbbDoanhNghiep.Name = "cbbDoanhNghiep";
            this.cbbDoanhNghiep.SelectedIndex = -1;
            this.cbbDoanhNghiep.SelectedItem = null;
            this.cbbDoanhNghiep.Size = new System.Drawing.Size(481, 23);
            this.cbbDoanhNghiep.TabIndex = 14;
            this.cbbDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbDoanhNghiep.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(508, 70);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Mã doanh nghiệp";
            // 
            // clcDateFrom
            // 
            this.clcDateFrom.CustomFormat = "dd/MM/yyyy";
            this.clcDateFrom.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDateFrom.DropDownCalendar.Name = "";
            this.clcDateFrom.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDateFrom.Location = new System.Drawing.Point(359, 28);
            this.clcDateFrom.Name = "clcDateFrom";
            this.clcDateFrom.Size = new System.Drawing.Size(131, 23);
            this.clcDateFrom.TabIndex = 15;
            this.clcDateFrom.Value = new System.DateTime(2018, 12, 19, 0, 0, 0, 0);
            this.clcDateFrom.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDateFrom.ValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(508, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Mã sản phẩm";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(292, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Đến ngày";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(292, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Từ ngày :";
            // 
            // cbbStatus
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Đã kích hoạt";
            uiComboBoxItem1.Value = "0";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Chưa kích hoạt";
            uiComboBoxItem2.Value = "1";
            this.cbbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbStatus.Location = new System.Drawing.Point(93, 67);
            this.cbbStatus.Name = "cbbStatus";
            this.cbbStatus.Size = new System.Drawing.Size(190, 23);
            this.cbbStatus.TabIndex = 13;
            this.cbbStatus.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbStatus.SelectedValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(11, 70);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Trạng thái";
            // 
            // cbbCDKeyType
            // 
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Trial";
            uiComboBoxItem3.Value = "0";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "License";
            uiComboBoxItem4.Value = "1";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Lock";
            uiComboBoxItem5.Value = "LOCK";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Wait For Approve";
            uiComboBoxItem6.Value = "-1";
            this.cbbCDKeyType.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbbCDKeyType.Location = new System.Drawing.Point(93, 28);
            this.cbbCDKeyType.Name = "cbbCDKeyType";
            this.cbbCDKeyType.Size = new System.Drawing.Size(190, 23);
            this.cbbCDKeyType.TabIndex = 13;
            this.cbbCDKeyType.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbCDKeyType.SelectedValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(11, 31);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Loại CD Key";
            // 
            // frmKeyManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1172, 714);
            this.Controls.Add(this.uiGroupBox4);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmKeyManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý CD Key";
            this.Load += new System.EventHandler(this.frmKeyTrial_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDoanhNghiep)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Copy;
        private System.Windows.Forms.ToolStripMenuItem Lock;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIComboBox cbbCDKeyType;
        private Janus.Windows.EditControls.UIComboBox cbbStatus;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbSanPham;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDateFrom;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDateTo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.GridEX.GridEX gridList;
    }
}