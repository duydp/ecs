﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
#endif
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Janus.Windows.GridEX;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
#endif


namespace Company.Interface.VNACCS.Vouchers
{
    public partial class VNACC_TransportEquipmentForm : BaseFormHaveGuidPanel
    {
        public long TKMD_ID { get; set; }
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public KDT_VNACC_ToKhaiVanChuyen TKVC;
        public List<KDT_ContainerDangKy> ContDK = new List<KDT_ContainerDangKy>();
        public List<KDT_ContainerBS> ContainerCollection = new List<KDT_ContainerBS>();
        public KDT_VNACCS_TransportEquipment transportEquipment = new KDT_VNACCS_TransportEquipment();
        public KDT_VNACCS_TransportEquipment_Detail transportEquipmentDetail = new KDT_VNACCS_TransportEquipment_Detail();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public VNACC_TransportEquipmentForm()
        {
            InitializeComponent();
        }
        public void SetFromToKhai(KDT_VNACC_ToKhaiVanChuyen TKVC)
        {
            try
            {
                ctrCoQuanHaiQuan.Code = TKVC.CoQuanHaiQuan;
                txtSoTK.Text = TKVC.SoToKhaiVC.ToString();
                txtTenNhaVC.Text = TKVC.TenNguoiVC;
                txtMaNhaVC.Text = TKVC.MaNguoiVC;
                txtDiaChiNhaVC.Text = TKVC.DiaChiNguoiVC;
                cbbCoBao.SelectedValue = TKVC.CoBaoXuatNhapKhau;
                dateNgayTK.Value = TKVC.NgayDangKy;
                dateNgayCP.Value = TKVC.NgayDangKy;
                cbbMaLHVC.SelectedValue = TKVC.LoaiHinhVanTai;
                ctrCoQuanHQGS.Code = TKVC.CoQuanHaiQuan;
                ctrCoQuanHQToKhai.Code = TKVC.CoQuanHaiQuan;
                cbbMaPTVC.SelectedValue = TKVC.MaPhuongTienVC;
                cbbMaMDVC.SelectedValue = TKVC.MaMucDichVC;
                dateNgayBatDauVC.Value = TKVC.NgayDuKienBatDauVC;
                dateNgayKetThucVC.Value = TKVC.NgayDuKienKetThucVC;

                dateNgayHopDongVC.Value = TKVC.NgayHopDongVC;
                txtSoHopDongVC.Text = TKVC.SoHopDongVC;
                dateNgayHetHanHDVC.Value = TKVC.NgayHetHanHopDongVC;

                ctrDiaDiemXepHang.Code = TKVC.MaDiaDiemXepHang;
                txtTenDiaDiemXepHang.Text = TKVC.DiaDiemXepHang;
                ctrMaViTriXepHang.Code = TKVC.MaViTriXepHang;
                ctrMaCangGaCKXepHang.Code = TKVC.MaCangCuaKhauGaXepHang;

                ctrMaDiaDiemDoHang.Code = TKVC.MaDiaDiemDoHang;
                txtDiaDiemDoHang.Text = TKVC.DiaDiemDoHang;
                ctrMaViTriDoHang.Code = TKVC.MaViTriDoHang;
                ctrMaCangCKGaDoHang.Code = TKVC.MaCangCuaKhauGaDoHang;

                txtGhiChu.Text = TKVC.GhiChu;
                txtTuyenDuong.Text = TKVC.TuyenDuongVC;
                //ContDK = KDT_ContainerDangKy.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID + " AND TrangThaiXuLy = 0 ", "");
                //if (ContDK.Count > 0)
                //{
                //    ContainerCollection = KDT_ContainerBS.SelectCollectionDynamic("Master_id = " + ContDK[0].ID, "");
                //    foreach (KDT_ContainerBS item in ContainerCollection)
                //    {
                //        transportEquipmentDetail = new KDT_VNACCS_TransportEquipment_Detail();
                //        transportEquipmentDetail.SoContainer = item.SoContainer;
                //        transportEquipmentDetail.SoVanDon = item.SoVanDon;
                //        transportEquipmentDetail.SoSeal = item.SoSeal;
                //        transportEquipmentDetail.SoSealHQ = item.CustomsSeal;
                //        transportEquipment.TransportEquipmentCollection.Add(transportEquipmentDetail);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void BindData()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = transportEquipment.TransportEquipmentCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
                //throw;
            }
        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send();
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                default:
                    break;
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_TransportEquipment", "", Convert.ToInt32(transportEquipment.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtTenNhaVC, errorProvider, "TÊN NHÀ VẬN CHUYỂN ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaNhaVC, errorProvider, "MÃ NHÀ VẬN CHUYỂN ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoTK, errorProvider, "SỐ TK", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbCoBao, errorProvider, "CỜ BÁO NHẬP KHẨU/XUẤT KHẨU ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbMaLHVC, errorProvider, "MÃ LH VẬN CHUYỂN", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTuyenDuong, errorProvider, "TUYẾN ĐƯỜNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDaiDienDN, errorProvider, "TÊN ĐẠI DIỆN DOANH NGHIỆP", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoKMVC, errorProvider, "KILOMET VẬN CHUYỂN", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtThoiGianVC, errorProvider, "THỜI GIAN VẬN CHUYỂN", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void GetTransportEquipment()
        {
            try
            {
                transportEquipment.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                transportEquipment.MaHQ = ctrCoQuanHaiQuan.Code;
                transportEquipment.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                transportEquipment.TenNhaVanChuyen = txtTenNhaVC.Text.ToString();
                transportEquipment.MaNhaVanChuyen = txtMaNhaVC.Text.ToString();
                transportEquipment.DiaChiNhaVanChuyen = txtDiaChiNhaVC.Text.ToString();
                if (TKVC != null)
                {
                    transportEquipment.TKMD_ID = TKVC.ID;
                }
                transportEquipment.CoBaoXNK = cbbCoBao.SelectedValue.ToString();
                transportEquipment.MaLoaiHinhVanChuyen = cbbMaLHVC.SelectedValue.ToString();
                transportEquipment.MaHQ = ctrCoQuanHQToKhai.Code;
                transportEquipment.MaHQGiamSat = ctrCoQuanHQGS.Code;
                transportEquipment.MaPTVC = cbbMaPTVC.SelectedValue.ToString();
                transportEquipment.MaMucDichVC = cbbMaMDVC.SelectedValue.ToString();
                transportEquipment.NgayBatDauVC = dateNgayBatDauVC.Value;
                transportEquipment.NgayKetThucVC = dateNgayKetThucVC.Value;

                transportEquipment.NgayHopDongVC = dateNgayHopDongVC.Value;
                transportEquipment.SoHopDongVC = txtSoHopDongVC.Text.ToString();
                transportEquipment.NgayHetHanHDVC = dateNgayHetHanHDVC.Value;

                transportEquipment.MaDiaDiemXepHang = ctrDiaDiemXepHang.Code;
                transportEquipment.TenDiaDiemXepHang = txtTenDiaDiemXepHang.Text;
                transportEquipment.MaViTriXepHang = ctrMaViTriXepHang.Code;
                transportEquipment.MaCangCuaKhauGaXepHang = ctrMaCangGaCKXepHang.Code;

                transportEquipment.MaDiaDiemDoHang = ctrMaDiaDiemDoHang.Code;
                transportEquipment.TenDiaDiemDoHang = txtDiaDiemDoHang.Text;
                transportEquipment.MaViTriDoHang = ctrMaViTriDoHang.Code;
                transportEquipment.MaCangCuaKhauGaDoHang = ctrMaCangCKGaDoHang.Code;

                transportEquipment.GhiChuKhac = txtGhiChu.Text.ToString();
                transportEquipment.TuyenDuong = txtTuyenDuong.Text.ToString();
                transportEquipment.SoDienThoaiHQ = txtSoDienThoaiHQ.Text.ToString();
                transportEquipment.SoFaxHQ = txtSoFaxHQ.Text.ToString();
                transportEquipment.TenDaiDienDN = txtTenDaiDienDN.Text.ToString();
                transportEquipment.ThoiGianVC = txtThoiGianVC.Text.ToString();
                transportEquipment.SoKMVC = Convert.ToDecimal(txtSoKMVC.Text.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void SetTransportEquipment()
        {
            try
            {
                txtSoTiepNhan.Text = transportEquipment.SoTN.ToString();
                dateNgayTN.Value = transportEquipment.NgayTN;
                string TrangThai = transportEquipment.TrangThaiXuLy.ToString();
                switch (TrangThai)
                {
                    case "0":
                        lblTrangThai.Text = "Chờ duyệt";
                        break;
                    case "-1":
                        lblTrangThai.Text = "Chưa khai báo";
                        break;
                    case "1":
                        lblTrangThai.Text = "Đã duyệt";
                        break;
                    case "2":
                        lblTrangThai.Text = "Không phê duyệt";
                        break;

                }
                try
                {
                    TKMD = KDT_VNACC_ToKhaiMauDich.Load(transportEquipment.TKMD_ID);
                    txtSoTK.Text = TKMD.SoToKhai.ToString();
                    dateNgayTK.Value = TKMD.NgayDangKy;
                    dateNgayCP.Value = TKMD.NgayCapPhep;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

                txtTenNhaVC.Text = transportEquipment.TenNhaVanChuyen;
                txtMaNhaVC.Text = transportEquipment.MaNhaVanChuyen;
                txtDiaChiNhaVC.Text = transportEquipment.DiaChiNhaVanChuyen;
                cbbCoBao.SelectedValue = transportEquipment.CoBaoXNK;
                cbbMaLHVC.SelectedValue = transportEquipment.MaLoaiHinhVanChuyen;
                ctrCoQuanHQToKhai.Code = transportEquipment.MaHQ;
                ctrCoQuanHQGS.Code = transportEquipment.MaHQGiamSat;
                cbbMaPTVC.SelectedValue = transportEquipment.MaPTVC;
                cbbMaMDVC.SelectedValue = transportEquipment.MaMucDichVC;

                dateNgayBatDauVC.Value = transportEquipment.NgayBatDauVC;
                txtSoHopDongVC.Text = transportEquipment.SoHopDongVC;
                dateNgayKetThucVC.Value = transportEquipment.NgayKetThucVC;

                ctrDiaDiemXepHang.Code = transportEquipment.MaDiaDiemXepHang;
                txtTenDiaDiemXepHang.Text = transportEquipment.TenDiaDiemXepHang;
                ctrMaViTriXepHang.Code = transportEquipment.MaViTriXepHang;
                ctrMaCangGaCKXepHang.Code = transportEquipment.MaCangCuaKhauGaXepHang;

                ctrMaDiaDiemDoHang.Code = transportEquipment.MaDiaDiemDoHang;
                txtDiaDiemDoHang.Text = transportEquipment.TenDiaDiemDoHang;
                ctrMaViTriDoHang.Code = transportEquipment.MaViTriDoHang;
                ctrMaCangCKGaDoHang.Code = transportEquipment.MaCangCuaKhauGaDoHang;

                txtGhiChu.Text = transportEquipment.GhiChuKhac;
                txtTuyenDuong.Text = transportEquipment.TuyenDuong;
                txtSoDienThoaiHQ.Text = transportEquipment.SoDienThoaiHQ;
                txtSoFaxHQ.Text = transportEquipment.SoFaxHQ;
                txtTenDaiDienDN.Text = transportEquipment.TenDaiDienDN;
                txtThoiGianVC.Text = transportEquipment.ThoiGianVC;
                txtSoKMVC.Text = transportEquipment.SoKMVC.ToString();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        public void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (transportEquipment.TransportEquipmentCollection.Count == 0)
                {
                    ShowMessage("DOANH NGHIỆP CHƯA THÊM DANH SÁCH CONTAINER ", false);
                    return;
                }
                GetTransportEquipment();
                transportEquipment.InsertUpdateFull();
                ShowMessage("Lưu thành công .", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Thông báo lỗi\r\n" + ex, false);
            }

        }
        public void Send()
        {
            if (ShowMessage("Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (transportEquipment.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    transportEquipment = KDT_VNACCS_TransportEquipment.Load(transportEquipment.ID);
                    transportEquipment.TransportEquipmentCollection = KDT_VNACCS_TransportEquipment_Detail.SelectCollectionBy_TransportEquipment_ID(transportEquipment.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.TransportEquipment;
                sendXML.master_id = transportEquipment.ID;

                if (sendXML.Load())
                {
                        ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    transportEquipment.GuidStr = Guid.NewGuid().ToString();
                    TransportEquipment_VNACCS transportEquipment_VNACCS = new TransportEquipment_VNACCS();
                    transportEquipment_VNACCS = Mapper_V4.ToDataTransferTransportEquipment(transportEquipment,transportEquipmentDetail,TKMD,GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = transportEquipment.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(transportEquipment.MaHQ)),
                              Identity = transportEquipment.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = transportEquipment_VNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = transportEquipment.GuidStr,
                          },
                          transportEquipment_VNACCS
                        );
                       // msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        transportEquipment.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(transportEquipment.ID, MessageTitle.RegisterTransportEquipment);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.TransportEquipment;
                        sendXML.master_id = transportEquipment.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        transportEquipment.Update();
                        Feedback();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = transportEquipment.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.TransportEquipment,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.TransportEquipment,
                };
                subjectBase.Type = DeclarationIssuer.GoodsItem;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = transportEquipment.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(transportEquipment.MaHQ.Trim())),
                                                  Identity = transportEquipment.MaHQ
                                              }, subjectBase, null);
                if (transportEquipment.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(transportEquipment.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
        }
        public void Result()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = transportEquipment.ID;
            form.DeclarationIssuer = DeclarationIssuer.TransportEquipment;
            form.ShowDialog(this);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.TransportEquipmentSendHandler(transportEquipment, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void VNACC_TransportEquipmentForm_Load(object sender, EventArgs e)
        {
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            if (transportEquipment!=null)
            {
                transportEquipment.TransportEquipmentCollection = KDT_VNACCS_TransportEquipment_Detail.SelectCollectionBy_TransportEquipment_ID(transportEquipment.ID);
                SetTransportEquipment();
                
            }
        }

        private bool ValidateFormContainer(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtSoContainer, errorProvider, "SỐ CONTAINER ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoVanDon, errorProvider, "SỐ VẬN ĐƠN ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoSeal, errorProvider, "SỐ SEAL", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiContainer, errorProvider, "LOẠI CONTAINER", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateFormContainer(false))
                    return;
                transportEquipmentDetail.SoContainer = txtSoContainer.Text.ToString();
                transportEquipmentDetail.SoVanDon = txtSoVanDon.Text.ToString();
                transportEquipmentDetail.SoSeal = txtSoSeal.Text.ToString();
                transportEquipmentDetail.SoSealHQ = txtSoSealHQ.Text.ToString();
                transportEquipmentDetail.LoaiContainer = cbbLoaiContainer.SelectedValue.ToString();
                transportEquipment.TransportEquipmentCollection.Add(transportEquipmentDetail);
                transportEquipmentDetail = new KDT_VNACCS_TransportEquipment_Detail();
                txtSoContainer.Text = String.Empty;
                txtSoVanDon.Text = String.Empty;
                txtSoSeal.Text = String.Empty;
                txtSoSealHQ.Text = String.Empty;
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListTotal.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            transportEquipmentDetail = (KDT_VNACCS_TransportEquipment_Detail)i.GetRow().DataRow;
                            transportEquipment.TransportEquipmentCollection.Remove(transportEquipmentDetail);
                            if (transportEquipmentDetail.ID > 0)
                            {
                                transportEquipmentDetail.Delete();
                            }
                        }
                    }
                    transportEquipmentDetail = new KDT_VNACCS_TransportEquipment_Detail();
                    ShowMessage("XÓA THÀNH CÔNG", false);
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListTotal_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                transportEquipmentDetail = (KDT_VNACCS_TransportEquipment_Detail)dgListTotal.GetRow().DataRow;
                txtSoContainer.Text = transportEquipmentDetail.SoContainer;
                txtSoVanDon.Text = transportEquipmentDetail.SoVanDon;
                txtSoSeal.Text = transportEquipmentDetail.SoSeal;
                txtSoSealHQ.Text = transportEquipmentDetail.SoSealHQ;
                cbbLoaiContainer.SelectedValue = transportEquipmentDetail.LoaiContainer;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                VNACC_ChuyenCuaKhau_ManagerForm f = new VNACC_ChuyenCuaKhau_ManagerForm();
                f.IsSelect = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    TKVC = f.TKVCSelect;
                    SetFromToKhai(TKVC);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
    }
}
