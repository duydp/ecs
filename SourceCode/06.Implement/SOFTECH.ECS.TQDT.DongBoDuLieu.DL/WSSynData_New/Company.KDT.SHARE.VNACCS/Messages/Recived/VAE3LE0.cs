﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAE3LE0 : BasicVNACC
    {
        public List<VAE3LE0_HANG> HangMD { get; set; }
        public void LoadVAE3LE0(string strResult)
        {
            try
            {
                this.GetObject<VAE3LE0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAE3LE0, false, VAE3LE0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAE3LE0.TongSoByte);
                while (true)
                {
                    VAE3LE0_HANG vad1agHang = new VAE3LE0_HANG();
                    vad1agHang.GetObject<VAE3LE0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAE3LE0_HANG", false, VAE3LE0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAE3LE0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAE3LE0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAE3LE0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAE3LE0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

    }
}
