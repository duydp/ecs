﻿namespace Company.Interface.Report.SXXK
{
    partial class PhuLucToKhaiNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhuLucToKhaiNhap));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMienThue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMienThue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTriGiaThuKhac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThueGTGT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThueXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.lblMienThue1,
            this.lblMienThue2,
            this.lblTongTriGiaThuKhac,
            this.lblTongTienThueGTGT,
            this.lblTongTriGiaNT,
            this.lblTongTienThueXNK,
            this.lblNgayDangKy,
            this.lblMaHaiQuan,
            this.xrLabel3,
            this.lblSoToKhai,
            this.xrLabel1,
            this.xrTable2,
            this.xrTable1,
            this.xrPictureBox1});
            this.Detail.Height = 1155;
            this.Detail.Name = "Detail";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Location = new System.Drawing.Point(333, 1133);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.Size = new System.Drawing.Size(100, 17);
            this.xrLabel2.Text = "Trang 1";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel2_BeforePrint);
            // 
            // lblMienThue1
            // 
            this.lblMienThue1.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.lblMienThue1.Location = new System.Drawing.Point(58, 692);
            this.lblMienThue1.Multiline = true;
            this.lblMienThue1.Name = "lblMienThue1";
            this.lblMienThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMienThue1.ParentStyleUsing.UseFont = false;
            this.lblMienThue1.Size = new System.Drawing.Size(267, 92);
            this.lblMienThue1.Text = "ad";
            this.lblMienThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMienThue1.Visible = false;
            // 
            // lblMienThue2
            // 
            this.lblMienThue2.Font = new System.Drawing.Font("Times New Roman", 15F);
            this.lblMienThue2.Location = new System.Drawing.Point(342, 692);
            this.lblMienThue2.Multiline = true;
            this.lblMienThue2.Name = "lblMienThue2";
            this.lblMienThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMienThue2.ParentStyleUsing.UseFont = false;
            this.lblMienThue2.Size = new System.Drawing.Size(283, 92);
            this.lblMienThue2.Text = "ad";
            this.lblMienThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMienThue2.Visible = false;
            // 
            // lblTongTriGiaThuKhac
            // 
            this.lblTongTriGiaThuKhac.Location = new System.Drawing.Point(667, 983);
            this.lblTongTriGiaThuKhac.Name = "lblTongTriGiaThuKhac";
            this.lblTongTriGiaThuKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaThuKhac.Size = new System.Drawing.Size(116, 34);
            this.lblTongTriGiaThuKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblTongTienThueGTGT
            // 
            this.lblTongTienThueGTGT.Location = new System.Drawing.Point(500, 983);
            this.lblTongTienThueGTGT.Name = "lblTongTienThueGTGT";
            this.lblTongTienThueGTGT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThueGTGT.Size = new System.Drawing.Size(116, 34);
            this.lblTongTienThueGTGT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.Location = new System.Drawing.Point(692, 542);
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.Size = new System.Drawing.Size(91, 34);
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblTongTienThueXNK
            // 
            this.lblTongTienThueXNK.Location = new System.Drawing.Point(217, 983);
            this.lblTongTienThueXNK.Name = "lblTongTienThueXNK";
            this.lblTongTienThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThueXNK.Size = new System.Drawing.Size(116, 34);
            this.lblTongTienThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Location = new System.Drawing.Point(325, 97);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.Size = new System.Drawing.Size(83, 17);
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaHaiQuan
            // 
            this.lblMaHaiQuan.Location = new System.Drawing.Point(508, 75);
            this.lblMaHaiQuan.Name = "lblMaHaiQuan";
            this.lblMaHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHaiQuan.Size = new System.Drawing.Size(50, 17);
            this.lblMaHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Location = new System.Drawing.Point(442, 75);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Size = new System.Drawing.Size(50, 17);
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Location = new System.Drawing.Point(325, 75);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.Size = new System.Drawing.Size(83, 17);
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Location = new System.Drawing.Point(325, 58);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(100, 17);
            // 
            // xrTable2
            // 
            this.xrTable2.Location = new System.Drawing.Point(58, 645);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTable2.Size = new System.Drawing.Size(725, 333);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT1,
            this.ThueSuatXNK1,
            this.TienThueXNK1,
            this.TriGiaTTGTGT1,
            this.ThueSuatGTGT1,
            this.TienThueGTGT1,
            this.TyLeThuKhac1,
            this.TriGiaThuKhac1});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Size = new System.Drawing.Size(725, 37);
            // 
            // TriGiaTT1
            // 
            this.TriGiaTT1.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT1.Name = "TriGiaTT1";
            this.TriGiaTT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT1.Size = new System.Drawing.Size(117, 37);
            this.TriGiaTT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK1
            // 
            this.ThueSuatXNK1.Location = new System.Drawing.Point(117, 0);
            this.ThueSuatXNK1.Name = "ThueSuatXNK1";
            this.ThueSuatXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK1.Size = new System.Drawing.Size(42, 37);
            this.ThueSuatXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK1
            // 
            this.TienThueXNK1.Location = new System.Drawing.Point(159, 0);
            this.TienThueXNK1.Name = "TienThueXNK1";
            this.TienThueXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK1.Size = new System.Drawing.Size(116, 37);
            this.TienThueXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT1
            // 
            this.TriGiaTTGTGT1.Location = new System.Drawing.Point(275, 0);
            this.TriGiaTTGTGT1.Name = "TriGiaTTGTGT1";
            this.TriGiaTTGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT1.Size = new System.Drawing.Size(125, 37);
            this.TriGiaTTGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT1
            // 
            this.ThueSuatGTGT1.Location = new System.Drawing.Point(400, 0);
            this.ThueSuatGTGT1.Name = "ThueSuatGTGT1";
            this.ThueSuatGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT1.Size = new System.Drawing.Size(44, 37);
            this.ThueSuatGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT1
            // 
            this.TienThueGTGT1.Location = new System.Drawing.Point(444, 0);
            this.TienThueGTGT1.Name = "TienThueGTGT1";
            this.TienThueGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT1.Size = new System.Drawing.Size(123, 37);
            this.TienThueGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac1
            // 
            this.TyLeThuKhac1.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac1.Name = "TyLeThuKhac1";
            this.TyLeThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac1.Size = new System.Drawing.Size(46, 37);
            this.TyLeThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac1
            // 
            this.TriGiaThuKhac1.Location = new System.Drawing.Point(613, 0);
            this.TriGiaThuKhac1.Name = "TriGiaThuKhac1";
            this.TriGiaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac1.Size = new System.Drawing.Size(112, 37);
            this.TriGiaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT2,
            this.ThueSuatXNK2,
            this.TienThueXNK2,
            this.TriGiaTTGTGT2,
            this.ThueSuatGTGT2,
            this.TienThueGTGT2,
            this.TyLeThuKhac2,
            this.TriGiaThuKhac2});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Size = new System.Drawing.Size(725, 37);
            // 
            // TriGiaTT2
            // 
            this.TriGiaTT2.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT2.Name = "TriGiaTT2";
            this.TriGiaTT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT2.Size = new System.Drawing.Size(117, 37);
            this.TriGiaTT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK2
            // 
            this.ThueSuatXNK2.Location = new System.Drawing.Point(117, 0);
            this.ThueSuatXNK2.Name = "ThueSuatXNK2";
            this.ThueSuatXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK2.Size = new System.Drawing.Size(42, 37);
            this.ThueSuatXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK2
            // 
            this.TienThueXNK2.Location = new System.Drawing.Point(159, 0);
            this.TienThueXNK2.Name = "TienThueXNK2";
            this.TienThueXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK2.Size = new System.Drawing.Size(116, 37);
            this.TienThueXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT2
            // 
            this.TriGiaTTGTGT2.Location = new System.Drawing.Point(275, 0);
            this.TriGiaTTGTGT2.Name = "TriGiaTTGTGT2";
            this.TriGiaTTGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT2.Size = new System.Drawing.Size(125, 37);
            this.TriGiaTTGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT2
            // 
            this.ThueSuatGTGT2.Location = new System.Drawing.Point(400, 0);
            this.ThueSuatGTGT2.Name = "ThueSuatGTGT2";
            this.ThueSuatGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT2.Size = new System.Drawing.Size(44, 37);
            this.ThueSuatGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT2
            // 
            this.TienThueGTGT2.Location = new System.Drawing.Point(444, 0);
            this.TienThueGTGT2.Name = "TienThueGTGT2";
            this.TienThueGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT2.Size = new System.Drawing.Size(123, 37);
            this.TienThueGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac2
            // 
            this.TyLeThuKhac2.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac2.Name = "TyLeThuKhac2";
            this.TyLeThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac2.Size = new System.Drawing.Size(46, 37);
            this.TyLeThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac2
            // 
            this.TriGiaThuKhac2.Location = new System.Drawing.Point(613, 0);
            this.TriGiaThuKhac2.Name = "TriGiaThuKhac2";
            this.TriGiaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac2.Size = new System.Drawing.Size(112, 37);
            this.TriGiaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT3,
            this.ThueSuatXNK3,
            this.TienThueXNK3,
            this.TriGiaTTGTGT3,
            this.ThueSuatGTGT3,
            this.TienThueGTGT3,
            this.TyLeThuKhac3,
            this.TriGiaThuKhac3});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Size = new System.Drawing.Size(725, 37);
            // 
            // TriGiaTT3
            // 
            this.TriGiaTT3.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT3.Name = "TriGiaTT3";
            this.TriGiaTT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT3.Size = new System.Drawing.Size(117, 37);
            this.TriGiaTT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK3
            // 
            this.ThueSuatXNK3.Location = new System.Drawing.Point(117, 0);
            this.ThueSuatXNK3.Name = "ThueSuatXNK3";
            this.ThueSuatXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK3.Size = new System.Drawing.Size(42, 37);
            this.ThueSuatXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK3
            // 
            this.TienThueXNK3.Location = new System.Drawing.Point(159, 0);
            this.TienThueXNK3.Name = "TienThueXNK3";
            this.TienThueXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK3.Size = new System.Drawing.Size(116, 37);
            this.TienThueXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT3
            // 
            this.TriGiaTTGTGT3.Location = new System.Drawing.Point(275, 0);
            this.TriGiaTTGTGT3.Name = "TriGiaTTGTGT3";
            this.TriGiaTTGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT3.Size = new System.Drawing.Size(125, 37);
            this.TriGiaTTGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT3
            // 
            this.ThueSuatGTGT3.Location = new System.Drawing.Point(400, 0);
            this.ThueSuatGTGT3.Name = "ThueSuatGTGT3";
            this.ThueSuatGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT3.Size = new System.Drawing.Size(44, 37);
            this.ThueSuatGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT3
            // 
            this.TienThueGTGT3.Location = new System.Drawing.Point(444, 0);
            this.TienThueGTGT3.Name = "TienThueGTGT3";
            this.TienThueGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT3.Size = new System.Drawing.Size(123, 37);
            this.TienThueGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac3
            // 
            this.TyLeThuKhac3.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac3.Name = "TyLeThuKhac3";
            this.TyLeThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac3.Size = new System.Drawing.Size(46, 37);
            this.TyLeThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac3
            // 
            this.TriGiaThuKhac3.Location = new System.Drawing.Point(613, 0);
            this.TriGiaThuKhac3.Name = "TriGiaThuKhac3";
            this.TriGiaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac3.Size = new System.Drawing.Size(112, 37);
            this.TriGiaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT4,
            this.ThueSuatXNK4,
            this.TienThueXNK4,
            this.TriGiaTTGTGT4,
            this.ThueSuatGTGT4,
            this.TienThueGTGT4,
            this.TyLeThuKhac4,
            this.TriGiaThuKhac4});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Size = new System.Drawing.Size(725, 37);
            // 
            // TriGiaTT4
            // 
            this.TriGiaTT4.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT4.Name = "TriGiaTT4";
            this.TriGiaTT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT4.Size = new System.Drawing.Size(117, 37);
            this.TriGiaTT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK4
            // 
            this.ThueSuatXNK4.Location = new System.Drawing.Point(117, 0);
            this.ThueSuatXNK4.Name = "ThueSuatXNK4";
            this.ThueSuatXNK4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK4.Size = new System.Drawing.Size(42, 37);
            this.ThueSuatXNK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK4
            // 
            this.TienThueXNK4.Location = new System.Drawing.Point(159, 0);
            this.TienThueXNK4.Name = "TienThueXNK4";
            this.TienThueXNK4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK4.Size = new System.Drawing.Size(116, 37);
            this.TienThueXNK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT4
            // 
            this.TriGiaTTGTGT4.Location = new System.Drawing.Point(275, 0);
            this.TriGiaTTGTGT4.Name = "TriGiaTTGTGT4";
            this.TriGiaTTGTGT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT4.Size = new System.Drawing.Size(125, 37);
            this.TriGiaTTGTGT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT4
            // 
            this.ThueSuatGTGT4.Location = new System.Drawing.Point(400, 0);
            this.ThueSuatGTGT4.Name = "ThueSuatGTGT4";
            this.ThueSuatGTGT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT4.Size = new System.Drawing.Size(44, 37);
            this.ThueSuatGTGT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT4
            // 
            this.TienThueGTGT4.Location = new System.Drawing.Point(444, 0);
            this.TienThueGTGT4.Name = "TienThueGTGT4";
            this.TienThueGTGT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT4.Size = new System.Drawing.Size(123, 37);
            this.TienThueGTGT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac4
            // 
            this.TyLeThuKhac4.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac4.Name = "TyLeThuKhac4";
            this.TyLeThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac4.Size = new System.Drawing.Size(46, 37);
            this.TyLeThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac4
            // 
            this.TriGiaThuKhac4.Location = new System.Drawing.Point(613, 0);
            this.TriGiaThuKhac4.Name = "TriGiaThuKhac4";
            this.TriGiaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac4.Size = new System.Drawing.Size(112, 37);
            this.TriGiaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT5,
            this.ThueSuatXNK5,
            this.TienThueXNK5,
            this.TriGiaTTGTGT5,
            this.ThueSuatGTGT5,
            this.TienThueGTGT5,
            this.TyLeThuKhac5,
            this.TriGiaThuKhac5});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Size = new System.Drawing.Size(725, 37);
            // 
            // TriGiaTT5
            // 
            this.TriGiaTT5.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT5.Name = "TriGiaTT5";
            this.TriGiaTT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT5.Size = new System.Drawing.Size(117, 37);
            this.TriGiaTT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK5
            // 
            this.ThueSuatXNK5.Location = new System.Drawing.Point(117, 0);
            this.ThueSuatXNK5.Name = "ThueSuatXNK5";
            this.ThueSuatXNK5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK5.Size = new System.Drawing.Size(42, 37);
            this.ThueSuatXNK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK5
            // 
            this.TienThueXNK5.Location = new System.Drawing.Point(159, 0);
            this.TienThueXNK5.Name = "TienThueXNK5";
            this.TienThueXNK5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK5.Size = new System.Drawing.Size(116, 37);
            this.TienThueXNK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT5
            // 
            this.TriGiaTTGTGT5.Location = new System.Drawing.Point(275, 0);
            this.TriGiaTTGTGT5.Name = "TriGiaTTGTGT5";
            this.TriGiaTTGTGT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT5.Size = new System.Drawing.Size(125, 37);
            this.TriGiaTTGTGT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT5
            // 
            this.ThueSuatGTGT5.Location = new System.Drawing.Point(400, 0);
            this.ThueSuatGTGT5.Name = "ThueSuatGTGT5";
            this.ThueSuatGTGT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT5.Size = new System.Drawing.Size(44, 37);
            this.ThueSuatGTGT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT5
            // 
            this.TienThueGTGT5.Location = new System.Drawing.Point(444, 0);
            this.TienThueGTGT5.Name = "TienThueGTGT5";
            this.TienThueGTGT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT5.Size = new System.Drawing.Size(123, 37);
            this.TienThueGTGT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac5
            // 
            this.TyLeThuKhac5.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac5.Name = "TyLeThuKhac5";
            this.TyLeThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac5.Size = new System.Drawing.Size(46, 37);
            this.TyLeThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac5
            // 
            this.TriGiaThuKhac5.Location = new System.Drawing.Point(613, 0);
            this.TriGiaThuKhac5.Name = "TriGiaThuKhac5";
            this.TriGiaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac5.Size = new System.Drawing.Size(112, 37);
            this.TriGiaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT6,
            this.ThueSuatXNK6,
            this.TienThueXNK6,
            this.TriGiaTTGTGT6,
            this.ThueSuatGTGT6,
            this.TienThueGTGT6,
            this.TyLeThuKhac6,
            this.TriGiaThuKhac6});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Size = new System.Drawing.Size(725, 37);
            // 
            // TriGiaTT6
            // 
            this.TriGiaTT6.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT6.Name = "TriGiaTT6";
            this.TriGiaTT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT6.Size = new System.Drawing.Size(117, 37);
            this.TriGiaTT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK6
            // 
            this.ThueSuatXNK6.Location = new System.Drawing.Point(117, 0);
            this.ThueSuatXNK6.Name = "ThueSuatXNK6";
            this.ThueSuatXNK6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK6.Size = new System.Drawing.Size(42, 37);
            this.ThueSuatXNK6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK6
            // 
            this.TienThueXNK6.Location = new System.Drawing.Point(159, 0);
            this.TienThueXNK6.Name = "TienThueXNK6";
            this.TienThueXNK6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK6.Size = new System.Drawing.Size(116, 37);
            this.TienThueXNK6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT6
            // 
            this.TriGiaTTGTGT6.Location = new System.Drawing.Point(275, 0);
            this.TriGiaTTGTGT6.Name = "TriGiaTTGTGT6";
            this.TriGiaTTGTGT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT6.Size = new System.Drawing.Size(125, 37);
            this.TriGiaTTGTGT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT6
            // 
            this.ThueSuatGTGT6.Location = new System.Drawing.Point(400, 0);
            this.ThueSuatGTGT6.Name = "ThueSuatGTGT6";
            this.ThueSuatGTGT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT6.Size = new System.Drawing.Size(44, 37);
            this.ThueSuatGTGT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT6
            // 
            this.TienThueGTGT6.Location = new System.Drawing.Point(444, 0);
            this.TienThueGTGT6.Name = "TienThueGTGT6";
            this.TienThueGTGT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT6.Size = new System.Drawing.Size(123, 37);
            this.TienThueGTGT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac6
            // 
            this.TyLeThuKhac6.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac6.Name = "TyLeThuKhac6";
            this.TyLeThuKhac6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac6.Size = new System.Drawing.Size(46, 37);
            this.TyLeThuKhac6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac6
            // 
            this.TriGiaThuKhac6.Location = new System.Drawing.Point(613, 0);
            this.TriGiaThuKhac6.Name = "TriGiaThuKhac6";
            this.TriGiaThuKhac6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac6.Size = new System.Drawing.Size(112, 37);
            this.TriGiaThuKhac6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT7,
            this.ThueSuatXNK7,
            this.TienThueXNK7,
            this.TriGiaTTGTGT7,
            this.ThueSuatGTGT7,
            this.TienThueGTGT7,
            this.TyLeThuKhac7,
            this.TriGiaThuKhac7});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Size = new System.Drawing.Size(725, 37);
            // 
            // TriGiaTT7
            // 
            this.TriGiaTT7.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT7.Name = "TriGiaTT7";
            this.TriGiaTT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT7.Size = new System.Drawing.Size(117, 37);
            this.TriGiaTT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK7
            // 
            this.ThueSuatXNK7.Location = new System.Drawing.Point(117, 0);
            this.ThueSuatXNK7.Name = "ThueSuatXNK7";
            this.ThueSuatXNK7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK7.Size = new System.Drawing.Size(42, 37);
            this.ThueSuatXNK7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK7
            // 
            this.TienThueXNK7.Location = new System.Drawing.Point(159, 0);
            this.TienThueXNK7.Name = "TienThueXNK7";
            this.TienThueXNK7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK7.Size = new System.Drawing.Size(116, 37);
            this.TienThueXNK7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT7
            // 
            this.TriGiaTTGTGT7.Location = new System.Drawing.Point(275, 0);
            this.TriGiaTTGTGT7.Name = "TriGiaTTGTGT7";
            this.TriGiaTTGTGT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT7.Size = new System.Drawing.Size(125, 37);
            this.TriGiaTTGTGT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT7
            // 
            this.ThueSuatGTGT7.Location = new System.Drawing.Point(400, 0);
            this.ThueSuatGTGT7.Name = "ThueSuatGTGT7";
            this.ThueSuatGTGT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT7.Size = new System.Drawing.Size(44, 37);
            this.ThueSuatGTGT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT7
            // 
            this.TienThueGTGT7.Location = new System.Drawing.Point(444, 0);
            this.TienThueGTGT7.Name = "TienThueGTGT7";
            this.TienThueGTGT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT7.Size = new System.Drawing.Size(123, 37);
            this.TienThueGTGT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac7
            // 
            this.TyLeThuKhac7.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac7.Name = "TyLeThuKhac7";
            this.TyLeThuKhac7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac7.Size = new System.Drawing.Size(46, 37);
            this.TyLeThuKhac7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac7
            // 
            this.TriGiaThuKhac7.Location = new System.Drawing.Point(613, 0);
            this.TriGiaThuKhac7.Name = "TriGiaThuKhac7";
            this.TriGiaThuKhac7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac7.Size = new System.Drawing.Size(112, 37);
            this.TriGiaThuKhac7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT8,
            this.ThueSuatXNK8,
            this.TienThueXNK8,
            this.TriGiaTTGTGT8,
            this.ThueSuatGTGT8,
            this.TienThueGTGT8,
            this.TyLeThuKhac8,
            this.TriGiaThuKhac8});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Size = new System.Drawing.Size(725, 37);
            // 
            // TriGiaTT8
            // 
            this.TriGiaTT8.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT8.Name = "TriGiaTT8";
            this.TriGiaTT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT8.Size = new System.Drawing.Size(117, 37);
            this.TriGiaTT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK8
            // 
            this.ThueSuatXNK8.Location = new System.Drawing.Point(117, 0);
            this.ThueSuatXNK8.Name = "ThueSuatXNK8";
            this.ThueSuatXNK8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK8.Size = new System.Drawing.Size(42, 37);
            this.ThueSuatXNK8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK8
            // 
            this.TienThueXNK8.Location = new System.Drawing.Point(159, 0);
            this.TienThueXNK8.Name = "TienThueXNK8";
            this.TienThueXNK8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK8.Size = new System.Drawing.Size(116, 37);
            this.TienThueXNK8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT8
            // 
            this.TriGiaTTGTGT8.Location = new System.Drawing.Point(275, 0);
            this.TriGiaTTGTGT8.Name = "TriGiaTTGTGT8";
            this.TriGiaTTGTGT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT8.Size = new System.Drawing.Size(125, 37);
            this.TriGiaTTGTGT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT8
            // 
            this.ThueSuatGTGT8.Location = new System.Drawing.Point(400, 0);
            this.ThueSuatGTGT8.Name = "ThueSuatGTGT8";
            this.ThueSuatGTGT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT8.Size = new System.Drawing.Size(44, 37);
            this.ThueSuatGTGT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT8
            // 
            this.TienThueGTGT8.Location = new System.Drawing.Point(444, 0);
            this.TienThueGTGT8.Name = "TienThueGTGT8";
            this.TienThueGTGT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT8.Size = new System.Drawing.Size(123, 37);
            this.TienThueGTGT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac8
            // 
            this.TyLeThuKhac8.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac8.Name = "TyLeThuKhac8";
            this.TyLeThuKhac8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac8.Size = new System.Drawing.Size(46, 37);
            this.TyLeThuKhac8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac8
            // 
            this.TriGiaThuKhac8.Location = new System.Drawing.Point(613, 0);
            this.TriGiaThuKhac8.Name = "TriGiaThuKhac8";
            this.TriGiaThuKhac8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac8.Size = new System.Drawing.Size(112, 37);
            this.TriGiaThuKhac8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT9,
            this.ThueSuatXNK9,
            this.TienThueXNK9,
            this.TriGiaTTGTGT9,
            this.ThueSuatGTGT9,
            this.TienThueGTGT9,
            this.TyLeThuKhac9,
            this.TriGiaThuKhac9});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Size = new System.Drawing.Size(725, 37);
            // 
            // TriGiaTT9
            // 
            this.TriGiaTT9.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT9.Name = "TriGiaTT9";
            this.TriGiaTT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT9.Size = new System.Drawing.Size(117, 37);
            this.TriGiaTT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK9
            // 
            this.ThueSuatXNK9.Location = new System.Drawing.Point(117, 0);
            this.ThueSuatXNK9.Name = "ThueSuatXNK9";
            this.ThueSuatXNK9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK9.Size = new System.Drawing.Size(42, 37);
            this.ThueSuatXNK9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK9
            // 
            this.TienThueXNK9.Location = new System.Drawing.Point(159, 0);
            this.TienThueXNK9.Name = "TienThueXNK9";
            this.TienThueXNK9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK9.Size = new System.Drawing.Size(116, 37);
            this.TienThueXNK9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT9
            // 
            this.TriGiaTTGTGT9.Location = new System.Drawing.Point(275, 0);
            this.TriGiaTTGTGT9.Name = "TriGiaTTGTGT9";
            this.TriGiaTTGTGT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT9.Size = new System.Drawing.Size(125, 37);
            this.TriGiaTTGTGT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT9
            // 
            this.ThueSuatGTGT9.Location = new System.Drawing.Point(400, 0);
            this.ThueSuatGTGT9.Name = "ThueSuatGTGT9";
            this.ThueSuatGTGT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT9.Size = new System.Drawing.Size(44, 37);
            this.ThueSuatGTGT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT9
            // 
            this.TienThueGTGT9.Location = new System.Drawing.Point(444, 0);
            this.TienThueGTGT9.Name = "TienThueGTGT9";
            this.TienThueGTGT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT9.Size = new System.Drawing.Size(123, 37);
            this.TienThueGTGT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac9
            // 
            this.TyLeThuKhac9.Location = new System.Drawing.Point(567, 0);
            this.TyLeThuKhac9.Name = "TyLeThuKhac9";
            this.TyLeThuKhac9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac9.Size = new System.Drawing.Size(46, 37);
            this.TyLeThuKhac9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac9
            // 
            this.TriGiaThuKhac9.Location = new System.Drawing.Point(613, 0);
            this.TriGiaThuKhac9.Name = "TriGiaThuKhac9";
            this.TriGiaThuKhac9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac9.Size = new System.Drawing.Size(112, 37);
            this.TriGiaThuKhac9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(58, 200);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrTable1.Size = new System.Drawing.Size(722, 333);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang1,
            this.MaHS1,
            this.XuatXu1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang1
            // 
            this.TenHang1.Location = new System.Drawing.Point(0, 0);
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang1.Size = new System.Drawing.Size(274, 37);
            // 
            // MaHS1
            // 
            this.MaHS1.Location = new System.Drawing.Point(274, 0);
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.Size = new System.Drawing.Size(92, 37);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu1
            // 
            this.XuatXu1.Location = new System.Drawing.Point(366, 0);
            this.XuatXu1.Name = "XuatXu1";
            this.XuatXu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu1.Size = new System.Drawing.Size(58, 37);
            this.XuatXu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong1
            // 
            this.Luong1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong1.Location = new System.Drawing.Point(424, 0);
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.ParentStyleUsing.UseFont = false;
            this.Luong1.Size = new System.Drawing.Size(75, 37);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT1
            // 
            this.DVT1.Location = new System.Drawing.Point(499, 0);
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.Size = new System.Drawing.Size(67, 37);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang2,
            this.MaHS2,
            this.XuatXu2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang2
            // 
            this.TenHang2.Location = new System.Drawing.Point(0, 0);
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang2.Size = new System.Drawing.Size(274, 37);
            // 
            // MaHS2
            // 
            this.MaHS2.Location = new System.Drawing.Point(274, 0);
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.Size = new System.Drawing.Size(92, 37);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu2
            // 
            this.XuatXu2.Location = new System.Drawing.Point(366, 0);
            this.XuatXu2.Name = "XuatXu2";
            this.XuatXu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu2.Size = new System.Drawing.Size(58, 37);
            this.XuatXu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong2
            // 
            this.Luong2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong2.Location = new System.Drawing.Point(424, 0);
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.ParentStyleUsing.UseFont = false;
            this.Luong2.Size = new System.Drawing.Size(75, 37);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT2
            // 
            this.DVT2.Location = new System.Drawing.Point(499, 0);
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.Size = new System.Drawing.Size(67, 37);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang3,
            this.MaHS3,
            this.XuatXu3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang3
            // 
            this.TenHang3.Location = new System.Drawing.Point(0, 0);
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang3.Size = new System.Drawing.Size(274, 37);
            // 
            // MaHS3
            // 
            this.MaHS3.Location = new System.Drawing.Point(274, 0);
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.Size = new System.Drawing.Size(92, 37);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu3
            // 
            this.XuatXu3.Location = new System.Drawing.Point(366, 0);
            this.XuatXu3.Name = "XuatXu3";
            this.XuatXu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu3.Size = new System.Drawing.Size(58, 37);
            this.XuatXu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong3
            // 
            this.Luong3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong3.Location = new System.Drawing.Point(424, 0);
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.ParentStyleUsing.UseFont = false;
            this.Luong3.Size = new System.Drawing.Size(75, 37);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT3
            // 
            this.DVT3.Location = new System.Drawing.Point(499, 0);
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.Size = new System.Drawing.Size(67, 37);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang4,
            this.MaHS4,
            this.XuatXu4,
            this.Luong4,
            this.DVT4,
            this.DonGiaNT4,
            this.TriGiaNT4});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang4
            // 
            this.TenHang4.Location = new System.Drawing.Point(0, 0);
            this.TenHang4.Name = "TenHang4";
            this.TenHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang4.Size = new System.Drawing.Size(274, 37);
            // 
            // MaHS4
            // 
            this.MaHS4.Location = new System.Drawing.Point(274, 0);
            this.MaHS4.Name = "MaHS4";
            this.MaHS4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS4.Size = new System.Drawing.Size(92, 37);
            this.MaHS4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu4
            // 
            this.XuatXu4.Location = new System.Drawing.Point(366, 0);
            this.XuatXu4.Name = "XuatXu4";
            this.XuatXu4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu4.Size = new System.Drawing.Size(58, 37);
            this.XuatXu4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong4
            // 
            this.Luong4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong4.Location = new System.Drawing.Point(424, 0);
            this.Luong4.Name = "Luong4";
            this.Luong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong4.ParentStyleUsing.UseFont = false;
            this.Luong4.Size = new System.Drawing.Size(75, 37);
            this.Luong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT4
            // 
            this.DVT4.Location = new System.Drawing.Point(499, 0);
            this.DVT4.Name = "DVT4";
            this.DVT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT4.Size = new System.Drawing.Size(67, 37);
            this.DVT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT4
            // 
            this.DonGiaNT4.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT4.Name = "DonGiaNT4";
            this.DonGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT4.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT4
            // 
            this.TriGiaNT4.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT4.Name = "TriGiaNT4";
            this.TriGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT4.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang5,
            this.MaHS5,
            this.XuatXu5,
            this.Luong5,
            this.DVT5,
            this.DonGiaNT5,
            this.TriGiaNT5});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang5
            // 
            this.TenHang5.Location = new System.Drawing.Point(0, 0);
            this.TenHang5.Name = "TenHang5";
            this.TenHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang5.Size = new System.Drawing.Size(274, 37);
            // 
            // MaHS5
            // 
            this.MaHS5.Location = new System.Drawing.Point(274, 0);
            this.MaHS5.Name = "MaHS5";
            this.MaHS5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS5.Size = new System.Drawing.Size(92, 37);
            this.MaHS5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu5
            // 
            this.XuatXu5.Location = new System.Drawing.Point(366, 0);
            this.XuatXu5.Name = "XuatXu5";
            this.XuatXu5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu5.Size = new System.Drawing.Size(58, 37);
            this.XuatXu5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong5
            // 
            this.Luong5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong5.Location = new System.Drawing.Point(424, 0);
            this.Luong5.Name = "Luong5";
            this.Luong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong5.ParentStyleUsing.UseFont = false;
            this.Luong5.Size = new System.Drawing.Size(75, 37);
            this.Luong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT5
            // 
            this.DVT5.Location = new System.Drawing.Point(499, 0);
            this.DVT5.Name = "DVT5";
            this.DVT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT5.Size = new System.Drawing.Size(67, 37);
            this.DVT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT5
            // 
            this.DonGiaNT5.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT5.Name = "DonGiaNT5";
            this.DonGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT5.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT5
            // 
            this.TriGiaNT5.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT5.Name = "TriGiaNT5";
            this.TriGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT5.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang6,
            this.MaHS6,
            this.XuatXu6,
            this.Luong6,
            this.DVT6,
            this.DonGiaNT6,
            this.TriGiaNT6});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang6
            // 
            this.TenHang6.Location = new System.Drawing.Point(0, 0);
            this.TenHang6.Name = "TenHang6";
            this.TenHang6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang6.Size = new System.Drawing.Size(274, 37);
            // 
            // MaHS6
            // 
            this.MaHS6.Location = new System.Drawing.Point(274, 0);
            this.MaHS6.Name = "MaHS6";
            this.MaHS6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS6.Size = new System.Drawing.Size(92, 37);
            this.MaHS6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu6
            // 
            this.XuatXu6.Location = new System.Drawing.Point(366, 0);
            this.XuatXu6.Name = "XuatXu6";
            this.XuatXu6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu6.Size = new System.Drawing.Size(58, 37);
            this.XuatXu6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong6
            // 
            this.Luong6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong6.Location = new System.Drawing.Point(424, 0);
            this.Luong6.Name = "Luong6";
            this.Luong6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong6.ParentStyleUsing.UseFont = false;
            this.Luong6.Size = new System.Drawing.Size(75, 37);
            this.Luong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT6
            // 
            this.DVT6.Location = new System.Drawing.Point(499, 0);
            this.DVT6.Name = "DVT6";
            this.DVT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT6.Size = new System.Drawing.Size(67, 37);
            this.DVT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT6
            // 
            this.DonGiaNT6.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT6.Name = "DonGiaNT6";
            this.DonGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT6.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT6
            // 
            this.TriGiaNT6.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT6.Name = "TriGiaNT6";
            this.TriGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT6.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang7,
            this.MaHS7,
            this.XuatXu7,
            this.Luong7,
            this.DVT7,
            this.DonGiaNT7,
            this.TriGiaNT7});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang7
            // 
            this.TenHang7.Location = new System.Drawing.Point(0, 0);
            this.TenHang7.Name = "TenHang7";
            this.TenHang7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang7.Size = new System.Drawing.Size(274, 37);
            // 
            // MaHS7
            // 
            this.MaHS7.Location = new System.Drawing.Point(274, 0);
            this.MaHS7.Name = "MaHS7";
            this.MaHS7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS7.Size = new System.Drawing.Size(92, 37);
            this.MaHS7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu7
            // 
            this.XuatXu7.Location = new System.Drawing.Point(366, 0);
            this.XuatXu7.Name = "XuatXu7";
            this.XuatXu7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu7.Size = new System.Drawing.Size(58, 37);
            this.XuatXu7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong7
            // 
            this.Luong7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong7.Location = new System.Drawing.Point(424, 0);
            this.Luong7.Name = "Luong7";
            this.Luong7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong7.ParentStyleUsing.UseFont = false;
            this.Luong7.Size = new System.Drawing.Size(75, 37);
            this.Luong7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT7
            // 
            this.DVT7.Location = new System.Drawing.Point(499, 0);
            this.DVT7.Name = "DVT7";
            this.DVT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT7.Size = new System.Drawing.Size(67, 37);
            this.DVT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT7
            // 
            this.DonGiaNT7.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT7.Name = "DonGiaNT7";
            this.DonGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT7.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT7
            // 
            this.TriGiaNT7.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT7.Name = "TriGiaNT7";
            this.TriGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT7.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang8,
            this.MaHS8,
            this.XuatXu8,
            this.Luong8,
            this.DVT8,
            this.DonGiaNT8,
            this.TriGiaNT8});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang8
            // 
            this.TenHang8.Location = new System.Drawing.Point(0, 0);
            this.TenHang8.Name = "TenHang8";
            this.TenHang8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang8.Size = new System.Drawing.Size(274, 37);
            // 
            // MaHS8
            // 
            this.MaHS8.Location = new System.Drawing.Point(274, 0);
            this.MaHS8.Name = "MaHS8";
            this.MaHS8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS8.Size = new System.Drawing.Size(92, 37);
            this.MaHS8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu8
            // 
            this.XuatXu8.Location = new System.Drawing.Point(366, 0);
            this.XuatXu8.Name = "XuatXu8";
            this.XuatXu8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu8.Size = new System.Drawing.Size(58, 37);
            this.XuatXu8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong8
            // 
            this.Luong8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong8.Location = new System.Drawing.Point(424, 0);
            this.Luong8.Name = "Luong8";
            this.Luong8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong8.ParentStyleUsing.UseFont = false;
            this.Luong8.Size = new System.Drawing.Size(75, 37);
            this.Luong8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT8
            // 
            this.DVT8.Location = new System.Drawing.Point(499, 0);
            this.DVT8.Name = "DVT8";
            this.DVT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT8.Size = new System.Drawing.Size(67, 37);
            this.DVT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT8
            // 
            this.DonGiaNT8.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT8.Name = "DonGiaNT8";
            this.DonGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT8.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT8
            // 
            this.TriGiaNT8.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT8.Name = "TriGiaNT8";
            this.TriGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT8.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang9,
            this.MaHS9,
            this.XuatXu9,
            this.Luong9,
            this.DVT9,
            this.DonGiaNT9,
            this.TriGiaNT9});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang9
            // 
            this.TenHang9.Location = new System.Drawing.Point(0, 0);
            this.TenHang9.Name = "TenHang9";
            this.TenHang9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang9.Size = new System.Drawing.Size(274, 37);
            // 
            // MaHS9
            // 
            this.MaHS9.Location = new System.Drawing.Point(274, 0);
            this.MaHS9.Name = "MaHS9";
            this.MaHS9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS9.Size = new System.Drawing.Size(92, 37);
            this.MaHS9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu9
            // 
            this.XuatXu9.Location = new System.Drawing.Point(366, 0);
            this.XuatXu9.Name = "XuatXu9";
            this.XuatXu9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu9.Size = new System.Drawing.Size(58, 37);
            this.XuatXu9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong9
            // 
            this.Luong9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong9.Location = new System.Drawing.Point(424, 0);
            this.Luong9.Name = "Luong9";
            this.Luong9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong9.ParentStyleUsing.UseFont = false;
            this.Luong9.Size = new System.Drawing.Size(75, 37);
            this.Luong9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT9
            // 
            this.DVT9.Location = new System.Drawing.Point(499, 0);
            this.DVT9.Name = "DVT9";
            this.DVT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT9.Size = new System.Drawing.Size(67, 37);
            this.DVT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT9
            // 
            this.DonGiaNT9.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT9.Name = "DonGiaNT9";
            this.DonGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT9.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT9
            // 
            this.TriGiaNT9.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT9.Name = "TriGiaNT9";
            this.TriGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT9.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.Location = new System.Drawing.Point(0, 0);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.Size = new System.Drawing.Size(814, 1155);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize;
            // 
            // PhuLucToKhaiNhap
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(7, 5, 6, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell TenHang4;
        private DevExpress.XtraReports.UI.XRTableCell MaHS4;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu4;
        private DevExpress.XtraReports.UI.XRTableCell Luong4;
        private DevExpress.XtraReports.UI.XRTableCell DVT4;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell TenHang5;
        private DevExpress.XtraReports.UI.XRTableCell MaHS5;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu5;
        private DevExpress.XtraReports.UI.XRTableCell Luong5;
        private DevExpress.XtraReports.UI.XRTableCell DVT5;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell TenHang6;
        private DevExpress.XtraReports.UI.XRTableCell MaHS6;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu6;
        private DevExpress.XtraReports.UI.XRTableCell Luong6;
        private DevExpress.XtraReports.UI.XRTableCell DVT6;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell TenHang7;
        private DevExpress.XtraReports.UI.XRTableCell MaHS7;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu7;
        private DevExpress.XtraReports.UI.XRTableCell Luong7;
        private DevExpress.XtraReports.UI.XRTableCell DVT7;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell TenHang8;
        private DevExpress.XtraReports.UI.XRTableCell MaHS8;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu8;
        private DevExpress.XtraReports.UI.XRTableCell Luong8;
        private DevExpress.XtraReports.UI.XRTableCell DVT8;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell TenHang9;
        private DevExpress.XtraReports.UI.XRTableCell MaHS9;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu9;
        private DevExpress.XtraReports.UI.XRTableCell Luong9;
        private DevExpress.XtraReports.UI.XRTableCell DVT9;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT9;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT4;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK4;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT5;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK5;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT6;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK6;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT7;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK7;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT8;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK8;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT9;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK9;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK9;
        private DevExpress.XtraReports.UI.XRLabel lblMaHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThueXNK;
        private DevExpress.XtraReports.UI.XRLabel lblTongTriGiaNT;
        public DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT9;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThueGTGT;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT4;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT4;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT5;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT5;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT6;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT6;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac6;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT7;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT7;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac7;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT8;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT8;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac8;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT9;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT9;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac9;
        private DevExpress.XtraReports.UI.XRLabel lblTongTriGiaThuKhac;
        private DevExpress.XtraReports.UI.XRLabel lblMienThue2;
        private DevExpress.XtraReports.UI.XRLabel lblMienThue1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
    }
}
