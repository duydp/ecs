﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.GC
{
    public partial class FormChonHangMau : BaseForm
    {
        public HopDong HD;
        public HangPhuKienCollection HPKCollection = new HangPhuKienCollection();
        private List<HangMau> hangMaus;

        public FormChonHangMau()
        {
            InitializeComponent();
        }
        private bool checkHangExit(string MaHang)
        {
            foreach (HangPhuKien hPK in HPKCollection)
            {
                if (hPK.MaHang == MaHang) return true;
            }
            return false;
        }
        private void btnChonAll_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            if (items.Length < 0) return;
            if (items.Length <= 0) return;
            {
                foreach (GridEXRow i in items)
                {

                    if (i.IsChecked)
                    {
                        HangMau hangmau = (HangMau)i.DataRow;
                        HangPhuKien hangPK = new HangPhuKien()
                        {
                            MaHang = hangmau.Ma,
                            TenHang = hangmau.Ten,
                            MaHS = hangmau.MaHS,
                            DVT_ID = hangmau.DVT_ID,
                            SoLuong = hangmau.SoLuongDangKy,
                            ThongTinCu = hangmau.Ma
                        };
                        HPKCollection.Add(hangPK);
                    }
                }
            }
            DialogResult = DialogResult.OK;

        }

        private void FormChonHangMau_Load(object sender, EventArgs e)
        {
            hangMaus = HangMau.SelectCollectionDynamic("HopDong_ID="+ HD.ID, "");
            dgList.DataSource = hangMaus;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaHM.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
