﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
#endif
using Company.KDT.SHARE.Components;



namespace Company.Interface.Controls
{
    public partial class ToKhaiInfo : UserControl
    {
        private ToKhaiMauDich _tkmd;
        private void Reset()
        {
            ReSetText(lblVanDon, "     Vận đơn");
            ReSetText(lbHopDong, "     Hợp đồng");
            ReSetText(lbGiayPhep, "     Giấy phép");
            ReSetText(lbHoaDonTM, "     Hóa đơn TM");
            ReSetText(lbCo, "     CO");
            ReSetText(lbDeNghiChuyenCK, "     Đề nghị chuyển CK");
            ReSetText(lbChungTuAnh, "     Chứng từ dạng ảnh");
            ReSetText(lbChungTuNo, "     Chứng từ nợ");
            ReSetText(lbSoDongHang, "0");
            ReSetText(lbSoHang, "0");
            ReSetText(lbTriGia, "0");
        }
        private void ReSetText(Label lb, string text)
        {
            lb.Text = text;
            lb.ImageIndex = -1;
        }
        public ToKhaiMauDich ToKhai
        {
            set
            {
                try
                {
                    _tkmd = value;
                    Reset();
                    string title = string.Format("Tờ khai {0}/{1}/{2}", new object[] { _tkmd.SoToKhai, _tkmd.MaLoaiHinh.Trim(), _tkmd.MaHaiQuan.Trim() });
                    if (_tkmd.NgayDangKy.Year > 1900) title = title + "/" + _tkmd.NgayDangKy.Year.ToString();
                    lbTitle.Text = title;
                    if (_tkmd.MaLoaiHinh.Substring(0, 1) == "X")
                    {
                        this.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                    }
                    else
                    {
                        this.BackColor = System.Drawing.Color.FromArgb(217, 232, 226);
                    }
                    if (_tkmd.VanTaiDon != null)
                        lblVanDon.ImageIndex = 0;
                    if (_tkmd.HoaDonThuongMaiCollection.Count > 0)
                    {
                        lbHoaDonTM.ImageIndex = 0;
                        lbHoaDonTM.Text += string.Format("({0})", _tkmd.HoaDonThuongMaiCollection.Count);
                    }
                    if (_tkmd.HopDongThuongMaiCollection.Count > 0)
                    {

                        lbHopDong.ImageIndex = 0;
                        lbHopDong.Text += string.Format("({0})", _tkmd.HopDongThuongMaiCollection.Count);
                    }
                    if (_tkmd.GiayPhepCollection.Count > 0)
                    {
                        lbGiayPhep.ImageIndex = 0;
                        lbGiayPhep.Text += string.Format("({0})", _tkmd.GiayPhepCollection.Count);
                    }
                    if (_tkmd.COCollection.Count > 0)
                    {

                        lbCo.ImageIndex = 0;
                        lbCo.Text += string.Format("({0})", _tkmd.COCollection.Count);
                    }
                    if (_tkmd.ChungTuKemCollection.Count > 0)
                    {

                        lbChungTuAnh.ImageIndex = 0;
                        lbChungTuAnh.Text += string.Format("({0})", _tkmd.ChungTuKemCollection.Count);
                    }
                    if (_tkmd.ChungTuNoCollection.Count > 0)
                    {

                        lbChungTuNo.ImageIndex = 0;
                        lbChungTuNo.Text += string.Format("({0})", _tkmd.ChungTuNoCollection.Count);
                    }
                    if (_tkmd.listChuyenCuaKhau.Count > 0)
                    {
                        lbDeNghiChuyenCK.ImageIndex = 0;
                        lbDeNghiChuyenCK.Text += string.Format("({0})", _tkmd.listChuyenCuaKhau.Count);
                    }
                    lbSoDongHang.Text = _tkmd.HMDCollection.Count.ToString();
                    double tongtrigia = 0;
                    decimal tongSoLuong = 0;
                    foreach (HangMauDich item in _tkmd.HMDCollection)
                    {
                        tongtrigia += Convert.ToDouble(item.TriGiaKB);
                        tongSoLuong += item.SoLuong;
                    }
                    lbSoHang.Text = Helpers.Format(tongSoLuong, Company.KDT.SHARE.Components.Globals.TriGiaNT);
                    lbTriGia.Text = Helpers.Format(tongtrigia, Company.KDT.SHARE.Components.Globals.TriGiaNT);
                    //lbTriGia.Text = tongtrigia.ToString();
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }

        public ToKhaiInfo()
        {
            InitializeComponent();
        }

    }
}
