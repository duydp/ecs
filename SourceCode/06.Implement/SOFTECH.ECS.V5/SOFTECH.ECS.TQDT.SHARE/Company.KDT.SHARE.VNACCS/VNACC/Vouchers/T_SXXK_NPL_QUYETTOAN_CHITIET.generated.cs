﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class T_SXXK_NPL_QUYETTOAN_CHITIET : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public decimal TOKHAIXUAT { set; get; }
		public decimal TOKHAINHAP { set; get; }
		public string MANPL { set; get; }
		public string TENNPL { set; get; }
		public string DVT_NPL { set; get; }
		public string MASP { set; get; }
		public string TENSP { set; get; }
		public string DVT_SP { set; get; }
		public decimal LUONGXUAT { set; get; }
		public decimal TRIGIAXUAT { set; get; }
		public decimal LUONGNHAP { set; get; }
		public decimal LUONGTONDAU { set; get; }
		public decimal DONGIANHAP { set; get; }
		public decimal TRIGIANHAP { set; get; }
		public decimal DONGIANHAPTT { set; get; }
		public decimal TRIGIANHAPTT { set; get; }
		public int TYGIATINHTHUE { set; get; }
		public int NAMQUYETTOAN { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_SXXK_NPL_QUYETTOAN_CHITIET> ConvertToCollection(IDataReader reader)
		{
			List<T_SXXK_NPL_QUYETTOAN_CHITIET> collection = new List<T_SXXK_NPL_QUYETTOAN_CHITIET>();
			while (reader.Read())
			{
				T_SXXK_NPL_QUYETTOAN_CHITIET entity = new T_SXXK_NPL_QUYETTOAN_CHITIET();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TOKHAIXUAT"))) entity.TOKHAIXUAT = reader.GetDecimal(reader.GetOrdinal("TOKHAIXUAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TOKHAINHAP"))) entity.TOKHAINHAP = reader.GetDecimal(reader.GetOrdinal("TOKHAINHAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("MANPL"))) entity.MANPL = reader.GetString(reader.GetOrdinal("MANPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENNPL"))) entity.TENNPL = reader.GetString(reader.GetOrdinal("TENNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_NPL"))) entity.DVT_NPL = reader.GetString(reader.GetOrdinal("DVT_NPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MASP"))) entity.MASP = reader.GetString(reader.GetOrdinal("MASP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENSP"))) entity.TENSP = reader.GetString(reader.GetOrdinal("TENSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_SP"))) entity.DVT_SP = reader.GetString(reader.GetOrdinal("DVT_SP"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGXUAT"))) entity.LUONGXUAT = reader.GetDecimal(reader.GetOrdinal("LUONGXUAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIAXUAT"))) entity.TRIGIAXUAT = reader.GetDecimal(reader.GetOrdinal("TRIGIAXUAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGNHAP"))) entity.LUONGNHAP = reader.GetDecimal(reader.GetOrdinal("LUONGNHAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGTONDAU"))) entity.LUONGTONDAU = reader.GetDecimal(reader.GetOrdinal("LUONGTONDAU"));
				if (!reader.IsDBNull(reader.GetOrdinal("DONGIANHAP"))) entity.DONGIANHAP = reader.GetDecimal(reader.GetOrdinal("DONGIANHAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIANHAP"))) entity.TRIGIANHAP = reader.GetDecimal(reader.GetOrdinal("TRIGIANHAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DONGIANHAPTT"))) entity.DONGIANHAPTT = reader.GetDecimal(reader.GetOrdinal("DONGIANHAPTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIANHAPTT"))) entity.TRIGIANHAPTT = reader.GetDecimal(reader.GetOrdinal("TRIGIANHAPTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TYGIATINHTHUE"))) entity.TYGIATINHTHUE = reader.GetInt32(reader.GetOrdinal("TYGIATINHTHUE"));
				if (!reader.IsDBNull(reader.GetOrdinal("NAMQUYETTOAN"))) entity.NAMQUYETTOAN = reader.GetInt32(reader.GetOrdinal("NAMQUYETTOAN"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_SXXK_NPL_QUYETTOAN_CHITIET> collection, long id)
        {
            foreach (T_SXXK_NPL_QUYETTOAN_CHITIET item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_SXXK_NPL_QUYETTOAN_CHITIET VALUES(@TOKHAIXUAT, @TOKHAINHAP, @MANPL, @TENNPL, @DVT_NPL, @MASP, @TENSP, @DVT_SP, @LUONGXUAT, @TRIGIAXUAT, @LUONGNHAP, @LUONGTONDAU, @DONGIANHAP, @TRIGIANHAP, @DONGIANHAPTT, @TRIGIANHAPTT, @TYGIATINHTHUE, @NAMQUYETTOAN)";
            string update = "UPDATE T_SXXK_NPL_QUYETTOAN_CHITIET SET TOKHAIXUAT = @TOKHAIXUAT, TOKHAINHAP = @TOKHAINHAP, MANPL = @MANPL, TENNPL = @TENNPL, DVT_NPL = @DVT_NPL, MASP = @MASP, TENSP = @TENSP, DVT_SP = @DVT_SP, LUONGXUAT = @LUONGXUAT, TRIGIAXUAT = @TRIGIAXUAT, LUONGNHAP = @LUONGNHAP, LUONGTONDAU = @LUONGTONDAU, DONGIANHAP = @DONGIANHAP, TRIGIANHAP = @TRIGIANHAP, DONGIANHAPTT = @DONGIANHAPTT, TRIGIANHAPTT = @TRIGIANHAPTT, TYGIATINHTHUE = @TYGIATINHTHUE, NAMQUYETTOAN = @NAMQUYETTOAN WHERE ID = @ID";
            string delete = "DELETE FROM T_SXXK_NPL_QUYETTOAN_CHITIET WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TOKHAINHAP", SqlDbType.Decimal, "TOKHAINHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.VarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_NPL", SqlDbType.NVarChar, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_SP", SqlDbType.NVarChar, "DVT_SP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGNHAP", SqlDbType.Decimal, "LUONGNHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONDAU", SqlDbType.Decimal, "LUONGTONDAU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIANHAP", SqlDbType.Decimal, "DONGIANHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAP", SqlDbType.Decimal, "TRIGIANHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIANHAPTT", SqlDbType.Decimal, "DONGIANHAPTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAPTT", SqlDbType.Decimal, "TRIGIANHAPTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYGIATINHTHUE", SqlDbType.Int, "TYGIATINHTHUE", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TOKHAINHAP", SqlDbType.Decimal, "TOKHAINHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.VarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_NPL", SqlDbType.NVarChar, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_SP", SqlDbType.NVarChar, "DVT_SP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGNHAP", SqlDbType.Decimal, "LUONGNHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONDAU", SqlDbType.Decimal, "LUONGTONDAU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIANHAP", SqlDbType.Decimal, "DONGIANHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAP", SqlDbType.Decimal, "TRIGIANHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIANHAPTT", SqlDbType.Decimal, "DONGIANHAPTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAPTT", SqlDbType.Decimal, "TRIGIANHAPTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYGIATINHTHUE", SqlDbType.Int, "TYGIATINHTHUE", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_SXXK_NPL_QUYETTOAN_CHITIET VALUES(@TOKHAIXUAT, @TOKHAINHAP, @MANPL, @TENNPL, @DVT_NPL, @MASP, @TENSP, @DVT_SP, @LUONGXUAT, @TRIGIAXUAT, @LUONGNHAP, @LUONGTONDAU, @DONGIANHAP, @TRIGIANHAP, @DONGIANHAPTT, @TRIGIANHAPTT, @TYGIATINHTHUE, @NAMQUYETTOAN)";
            string update = "UPDATE T_SXXK_NPL_QUYETTOAN_CHITIET SET TOKHAIXUAT = @TOKHAIXUAT, TOKHAINHAP = @TOKHAINHAP, MANPL = @MANPL, TENNPL = @TENNPL, DVT_NPL = @DVT_NPL, MASP = @MASP, TENSP = @TENSP, DVT_SP = @DVT_SP, LUONGXUAT = @LUONGXUAT, TRIGIAXUAT = @TRIGIAXUAT, LUONGNHAP = @LUONGNHAP, LUONGTONDAU = @LUONGTONDAU, DONGIANHAP = @DONGIANHAP, TRIGIANHAP = @TRIGIANHAP, DONGIANHAPTT = @DONGIANHAPTT, TRIGIANHAPTT = @TRIGIANHAPTT, TYGIATINHTHUE = @TYGIATINHTHUE, NAMQUYETTOAN = @NAMQUYETTOAN WHERE ID = @ID";
            string delete = "DELETE FROM T_SXXK_NPL_QUYETTOAN_CHITIET WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TOKHAINHAP", SqlDbType.Decimal, "TOKHAINHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MANPL", SqlDbType.VarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_NPL", SqlDbType.NVarChar, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_SP", SqlDbType.NVarChar, "DVT_SP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGNHAP", SqlDbType.Decimal, "LUONGNHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONDAU", SqlDbType.Decimal, "LUONGTONDAU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIANHAP", SqlDbType.Decimal, "DONGIANHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAP", SqlDbType.Decimal, "TRIGIANHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIANHAPTT", SqlDbType.Decimal, "DONGIANHAPTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAPTT", SqlDbType.Decimal, "TRIGIANHAPTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYGIATINHTHUE", SqlDbType.Int, "TYGIATINHTHUE", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TOKHAINHAP", SqlDbType.Decimal, "TOKHAINHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MANPL", SqlDbType.VarChar, "MANPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENNPL", SqlDbType.NVarChar, "TENNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_NPL", SqlDbType.NVarChar, "DVT_NPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_SP", SqlDbType.NVarChar, "DVT_SP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGNHAP", SqlDbType.Decimal, "LUONGNHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONDAU", SqlDbType.Decimal, "LUONGTONDAU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIANHAP", SqlDbType.Decimal, "DONGIANHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAP", SqlDbType.Decimal, "TRIGIANHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIANHAPTT", SqlDbType.Decimal, "DONGIANHAPTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAPTT", SqlDbType.Decimal, "TRIGIANHAPTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYGIATINHTHUE", SqlDbType.Int, "TYGIATINHTHUE", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_SXXK_NPL_QUYETTOAN_CHITIET Load(long id)
		{
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_SXXK_NPL_QUYETTOAN_CHITIET> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_SXXK_NPL_QUYETTOAN_CHITIET> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_SXXK_NPL_QUYETTOAN_CHITIET> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_SXXK_NPL_QUYETTOAN_CHITIET(decimal tOKHAIXUAT, decimal tOKHAINHAP, string mANPL, string tENNPL, string dVT_NPL, string mASP, string tENSP, string dVT_SP, decimal lUONGXUAT, decimal tRIGIAXUAT, decimal lUONGNHAP, decimal lUONGTONDAU, decimal dONGIANHAP, decimal tRIGIANHAP, decimal dONGIANHAPTT, decimal tRIGIANHAPTT, int tYGIATINHTHUE, int nAMQUYETTOAN)
		{
			T_SXXK_NPL_QUYETTOAN_CHITIET entity = new T_SXXK_NPL_QUYETTOAN_CHITIET();	
			entity.TOKHAIXUAT = tOKHAIXUAT;
			entity.TOKHAINHAP = tOKHAINHAP;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVT_NPL = dVT_NPL;
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVT_SP = dVT_SP;
			entity.LUONGXUAT = lUONGXUAT;
			entity.TRIGIAXUAT = tRIGIAXUAT;
			entity.LUONGNHAP = lUONGNHAP;
			entity.LUONGTONDAU = lUONGTONDAU;
			entity.DONGIANHAP = dONGIANHAP;
			entity.TRIGIANHAP = tRIGIANHAP;
			entity.DONGIANHAPTT = dONGIANHAPTT;
			entity.TRIGIANHAPTT = tRIGIANHAPTT;
			entity.TYGIATINHTHUE = tYGIATINHTHUE;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TOKHAIXUAT", SqlDbType.Decimal, TOKHAIXUAT);
			db.AddInParameter(dbCommand, "@TOKHAINHAP", SqlDbType.Decimal, TOKHAINHAP);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.VarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVT_NPL", SqlDbType.NVarChar, DVT_NPL);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.VarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVT_SP", SqlDbType.NVarChar, DVT_SP);
			db.AddInParameter(dbCommand, "@LUONGXUAT", SqlDbType.Decimal, LUONGXUAT);
			db.AddInParameter(dbCommand, "@TRIGIAXUAT", SqlDbType.Decimal, TRIGIAXUAT);
			db.AddInParameter(dbCommand, "@LUONGNHAP", SqlDbType.Decimal, LUONGNHAP);
			db.AddInParameter(dbCommand, "@LUONGTONDAU", SqlDbType.Decimal, LUONGTONDAU);
			db.AddInParameter(dbCommand, "@DONGIANHAP", SqlDbType.Decimal, DONGIANHAP);
			db.AddInParameter(dbCommand, "@TRIGIANHAP", SqlDbType.Decimal, TRIGIANHAP);
			db.AddInParameter(dbCommand, "@DONGIANHAPTT", SqlDbType.Decimal, DONGIANHAPTT);
			db.AddInParameter(dbCommand, "@TRIGIANHAPTT", SqlDbType.Decimal, TRIGIANHAPTT);
			db.AddInParameter(dbCommand, "@TYGIATINHTHUE", SqlDbType.Int, TYGIATINHTHUE);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_SXXK_NPL_QUYETTOAN_CHITIET> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_SXXK_NPL_QUYETTOAN_CHITIET item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_SXXK_NPL_QUYETTOAN_CHITIET(long id, decimal tOKHAIXUAT, decimal tOKHAINHAP, string mANPL, string tENNPL, string dVT_NPL, string mASP, string tENSP, string dVT_SP, decimal lUONGXUAT, decimal tRIGIAXUAT, decimal lUONGNHAP, decimal lUONGTONDAU, decimal dONGIANHAP, decimal tRIGIANHAP, decimal dONGIANHAPTT, decimal tRIGIANHAPTT, int tYGIATINHTHUE, int nAMQUYETTOAN)
		{
			T_SXXK_NPL_QUYETTOAN_CHITIET entity = new T_SXXK_NPL_QUYETTOAN_CHITIET();			
			entity.ID = id;
			entity.TOKHAIXUAT = tOKHAIXUAT;
			entity.TOKHAINHAP = tOKHAINHAP;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVT_NPL = dVT_NPL;
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVT_SP = dVT_SP;
			entity.LUONGXUAT = lUONGXUAT;
			entity.TRIGIAXUAT = tRIGIAXUAT;
			entity.LUONGNHAP = lUONGNHAP;
			entity.LUONGTONDAU = lUONGTONDAU;
			entity.DONGIANHAP = dONGIANHAP;
			entity.TRIGIANHAP = tRIGIANHAP;
			entity.DONGIANHAPTT = dONGIANHAPTT;
			entity.TRIGIANHAPTT = tRIGIANHAPTT;
			entity.TYGIATINHTHUE = tYGIATINHTHUE;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_SXXK_NPL_QUYETTOAN_CHITIET_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TOKHAIXUAT", SqlDbType.Decimal, TOKHAIXUAT);
			db.AddInParameter(dbCommand, "@TOKHAINHAP", SqlDbType.Decimal, TOKHAINHAP);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.VarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVT_NPL", SqlDbType.NVarChar, DVT_NPL);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.VarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVT_SP", SqlDbType.NVarChar, DVT_SP);
			db.AddInParameter(dbCommand, "@LUONGXUAT", SqlDbType.Decimal, LUONGXUAT);
			db.AddInParameter(dbCommand, "@TRIGIAXUAT", SqlDbType.Decimal, TRIGIAXUAT);
			db.AddInParameter(dbCommand, "@LUONGNHAP", SqlDbType.Decimal, LUONGNHAP);
			db.AddInParameter(dbCommand, "@LUONGTONDAU", SqlDbType.Decimal, LUONGTONDAU);
			db.AddInParameter(dbCommand, "@DONGIANHAP", SqlDbType.Decimal, DONGIANHAP);
			db.AddInParameter(dbCommand, "@TRIGIANHAP", SqlDbType.Decimal, TRIGIANHAP);
			db.AddInParameter(dbCommand, "@DONGIANHAPTT", SqlDbType.Decimal, DONGIANHAPTT);
			db.AddInParameter(dbCommand, "@TRIGIANHAPTT", SqlDbType.Decimal, TRIGIANHAPTT);
			db.AddInParameter(dbCommand, "@TYGIATINHTHUE", SqlDbType.Int, TYGIATINHTHUE);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_SXXK_NPL_QUYETTOAN_CHITIET> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_SXXK_NPL_QUYETTOAN_CHITIET item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_SXXK_NPL_QUYETTOAN_CHITIET(long id, decimal tOKHAIXUAT, decimal tOKHAINHAP, string mANPL, string tENNPL, string dVT_NPL, string mASP, string tENSP, string dVT_SP, decimal lUONGXUAT, decimal tRIGIAXUAT, decimal lUONGNHAP, decimal lUONGTONDAU, decimal dONGIANHAP, decimal tRIGIANHAP, decimal dONGIANHAPTT, decimal tRIGIANHAPTT, int tYGIATINHTHUE, int nAMQUYETTOAN)
		{
			T_SXXK_NPL_QUYETTOAN_CHITIET entity = new T_SXXK_NPL_QUYETTOAN_CHITIET();			
			entity.ID = id;
			entity.TOKHAIXUAT = tOKHAIXUAT;
			entity.TOKHAINHAP = tOKHAINHAP;
			entity.MANPL = mANPL;
			entity.TENNPL = tENNPL;
			entity.DVT_NPL = dVT_NPL;
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVT_SP = dVT_SP;
			entity.LUONGXUAT = lUONGXUAT;
			entity.TRIGIAXUAT = tRIGIAXUAT;
			entity.LUONGNHAP = lUONGNHAP;
			entity.LUONGTONDAU = lUONGTONDAU;
			entity.DONGIANHAP = dONGIANHAP;
			entity.TRIGIANHAP = tRIGIANHAP;
			entity.DONGIANHAPTT = dONGIANHAPTT;
			entity.TRIGIANHAPTT = tRIGIANHAPTT;
			entity.TYGIATINHTHUE = tYGIATINHTHUE;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TOKHAIXUAT", SqlDbType.Decimal, TOKHAIXUAT);
			db.AddInParameter(dbCommand, "@TOKHAINHAP", SqlDbType.Decimal, TOKHAINHAP);
			db.AddInParameter(dbCommand, "@MANPL", SqlDbType.VarChar, MANPL);
			db.AddInParameter(dbCommand, "@TENNPL", SqlDbType.NVarChar, TENNPL);
			db.AddInParameter(dbCommand, "@DVT_NPL", SqlDbType.NVarChar, DVT_NPL);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.VarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVT_SP", SqlDbType.NVarChar, DVT_SP);
			db.AddInParameter(dbCommand, "@LUONGXUAT", SqlDbType.Decimal, LUONGXUAT);
			db.AddInParameter(dbCommand, "@TRIGIAXUAT", SqlDbType.Decimal, TRIGIAXUAT);
			db.AddInParameter(dbCommand, "@LUONGNHAP", SqlDbType.Decimal, LUONGNHAP);
			db.AddInParameter(dbCommand, "@LUONGTONDAU", SqlDbType.Decimal, LUONGTONDAU);
			db.AddInParameter(dbCommand, "@DONGIANHAP", SqlDbType.Decimal, DONGIANHAP);
			db.AddInParameter(dbCommand, "@TRIGIANHAP", SqlDbType.Decimal, TRIGIANHAP);
			db.AddInParameter(dbCommand, "@DONGIANHAPTT", SqlDbType.Decimal, DONGIANHAPTT);
			db.AddInParameter(dbCommand, "@TRIGIANHAPTT", SqlDbType.Decimal, TRIGIANHAPTT);
			db.AddInParameter(dbCommand, "@TYGIATINHTHUE", SqlDbType.Int, TYGIATINHTHUE);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_SXXK_NPL_QUYETTOAN_CHITIET> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_SXXK_NPL_QUYETTOAN_CHITIET item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_SXXK_NPL_QUYETTOAN_CHITIET(long id)
		{
			T_SXXK_NPL_QUYETTOAN_CHITIET entity = new T_SXXK_NPL_QUYETTOAN_CHITIET();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_SXXK_NPL_QUYETTOAN_CHITIET_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_SXXK_NPL_QUYETTOAN_CHITIET> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_SXXK_NPL_QUYETTOAN_CHITIET item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}