﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Helper.Controls
{
    public partial class MessageBoxControlH : Form
    {
        public MessageBoxControlH()
        {
            InitializeComponent();
        }

        public DialogResult ReturnValue = DialogResult.Cancel;
        public string exceptionString = "";
        public bool ShowYesNoButton
        {
            set
            {
                this.btnNo.Visible = this.btnYes.Visible = value;
                this.btnClose.Visible = !value;
                if (!value) this.CancelButton = btnClose;
                else this.CancelButton = btnNo;
            }
        }

        public bool ShowErrorButton
        {
            set
            {
                //this.btnSend.Visible = value;
                this.btnClose.Visible = !this.btnNo.Visible;
            }
        }

        public string MessageString
        {
            set { this.txtMessage.Text = value; }
            get { return this.txtMessage.Text; }
        }

        public void dispEnglish()
        {
            this.Text = "Announcement";
            this.btnYes.Text = "&Yes";
            this.btnNo.Text = "&No";
            this.btnClose.Text = "&Close";
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.ReturnValue = DialogResult.Yes;
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.ReturnValue = DialogResult.No;
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Thong bao

        protected static MessageBoxControlH _MsgBoxH;

        public static DialogResult ShowMessage(string message, bool showYesNoButton)
        {
            _MsgBoxH = new MessageBoxControlH();
            _MsgBoxH.TopMost = false;
            _MsgBoxH.ShowYesNoButton = showYesNoButton;
            _MsgBoxH.MessageString = message;
            _MsgBoxH.ShowDialog();
            DialogResult st = _MsgBoxH.ReturnValue;
            _MsgBoxH.Dispose();
            return st;
        }

        public static DialogResult ShowMessage(string message, bool showYesNoButton, bool isTopMost)
        {
            _MsgBoxH = new MessageBoxControlH();
            _MsgBoxH.TopMost = isTopMost;
            _MsgBoxH.ShowYesNoButton = showYesNoButton;
            _MsgBoxH.MessageString = message;
            _MsgBoxH.ShowDialog();
            DialogResult st = _MsgBoxH.ReturnValue;
            _MsgBoxH.Dispose();
            return st;
        }

        #endregion

    }
}
