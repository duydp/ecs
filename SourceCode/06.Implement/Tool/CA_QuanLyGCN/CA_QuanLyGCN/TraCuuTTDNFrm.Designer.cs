﻿namespace CA_QuanLyGCN
{
    partial class TraCuuTTDNFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TraCuuTTDNFrm));
            Janus.Windows.GridEX.GridEXLayout gdList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNamCap = new Janus.Windows.GridEX.EditControls.IntegerUpDown();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.txtThangCap = new Janus.Windows.GridEX.EditControls.IntegerUpDown();
            this.txtNgayCap = new Janus.Windows.GridEX.EditControls.IntegerUpDown();
            this.btnThem = new Janus.Windows.EditControls.UIButton();
            this.btnTim = new Janus.Windows.EditControls.UIButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDiaChiCoSo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenCoSo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenGiamDoc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.gdList = new Janus.Windows.GridEX.GridEX();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox1.Controls.Add(this.txtNamCap);
            this.uiGroupBox1.Controls.Add(this.txtThangCap);
            this.uiGroupBox1.Controls.Add(this.txtNgayCap);
            this.uiGroupBox1.Controls.Add(this.btnThem);
            this.uiGroupBox1.Controls.Add(this.btnTim);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtDiaChiCoSo);
            this.uiGroupBox1.Controls.Add(this.txtDiaChi);
            this.uiGroupBox1.Controls.Add(this.editBox1);
            this.uiGroupBox1.Controls.Add(this.txtTenCoSo);
            this.uiGroupBox1.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.txtTenGiamDoc);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(684, 162);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtNamCap
            // 
            this.txtNamCap.Location = new System.Drawing.Point(287, 129);
            this.txtNamCap.Maximum = 2500;
            this.txtNamCap.Name = "txtNamCap";
            this.txtNamCap.Size = new System.Drawing.Size(57, 20);
            this.txtNamCap.TabIndex = 8;
            this.txtNamCap.VisualStyleManager = this.visualStyleManager1;
            // 
            // visualStyleManager1
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office2007";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme1.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme1);
            // 
            // txtThangCap
            // 
            this.txtThangCap.Location = new System.Drawing.Point(228, 129);
            this.txtThangCap.Maximum = 12;
            this.txtThangCap.Name = "txtThangCap";
            this.txtThangCap.Size = new System.Drawing.Size(35, 20);
            this.txtThangCap.TabIndex = 7;
            this.txtThangCap.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtNgayCap
            // 
            this.txtNgayCap.Location = new System.Drawing.Point(174, 129);
            this.txtNgayCap.Maximum = 31;
            this.txtNgayCap.Name = "txtNgayCap";
            this.txtNgayCap.Size = new System.Drawing.Size(35, 20);
            this.txtNgayCap.TabIndex = 6;
            this.txtNgayCap.VisualStyleManager = this.visualStyleManager1;
            // 
            // btnThem
            // 
            this.btnThem.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThem.Icon")));
            this.btnThem.Location = new System.Drawing.Point(516, 128);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(75, 23);
            this.btnThem.TabIndex = 11;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.VisualStyleManager = this.visualStyleManager1;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnTim
            // 
            this.btnTim.Icon = ((System.Drawing.Icon)(resources.GetObject("btnTim.Icon")));
            this.btnTim.Location = new System.Drawing.Point(597, 128);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(75, 23);
            this.btnTim.TabIndex = 10;
            this.btnTim.Text = "Tìm kiếm";
            this.btnTim.VisualStyleManager = this.visualStyleManager1;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(269, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 18);
            this.label9.TabIndex = 1;
            this.label9.Text = "/";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(213, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 18);
            this.label8.TabIndex = 1;
            this.label8.Text = "/";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Ngày/Tháng/Năm cấp giấy CN";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Tên cơ sở KD";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(371, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Địa chỉ cơ sở";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Địa chỉ trụ sở";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã số thuế";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên danh nghiệp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(367, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Tên giám đốc";
            // 
            // txtDiaChiCoSo
            // 
            this.txtDiaChiCoSo.Location = new System.Drawing.Point(446, 74);
            this.txtDiaChiCoSo.Name = "txtDiaChiCoSo";
            this.txtDiaChiCoSo.Size = new System.Drawing.Size(226, 20);
            this.txtDiaChiCoSo.TabIndex = 4;
            this.txtDiaChiCoSo.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(118, 74);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(243, 20);
            this.txtDiaChi.TabIndex = 3;
            this.txtDiaChi.VisualStyleManager = this.visualStyleManager1;
            // 
            // editBox1
            // 
            this.editBox1.Location = new System.Drawing.Point(118, 22);
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(243, 20);
            this.editBox1.TabIndex = 0;
            this.editBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtTenCoSo
            // 
            this.txtTenCoSo.Location = new System.Drawing.Point(118, 102);
            this.txtTenCoSo.Name = "txtTenCoSo";
            this.txtTenCoSo.Size = new System.Drawing.Size(554, 20);
            this.txtTenCoSo.TabIndex = 5;
            this.txtTenCoSo.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(118, 48);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(243, 20);
            this.txtTenDoanhNghiep.TabIndex = 1;
            this.txtTenDoanhNghiep.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtTenGiamDoc
            // 
            this.txtTenGiamDoc.Location = new System.Drawing.Point(446, 48);
            this.txtTenGiamDoc.Name = "txtTenGiamDoc";
            this.txtTenGiamDoc.Size = new System.Drawing.Size(226, 20);
            this.txtTenGiamDoc.TabIndex = 2;
            this.txtTenGiamDoc.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox2.Controls.Add(this.gdList);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 162);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(684, 472);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyleManager = this.visualStyleManager1;
            // 
            // gdList
            // 
            gdList_DesignTimeLayout.LayoutString = resources.GetString("gdList_DesignTimeLayout.LayoutString");
            this.gdList.DesignTimeLayout = gdList_DesignTimeLayout;
            this.gdList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gdList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gdList.GroupByBoxVisible = false;
            this.gdList.Location = new System.Drawing.Point(3, 8);
            this.gdList.Name = "gdList";
            this.gdList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gdList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gdList.Size = new System.Drawing.Size(678, 461);
            this.gdList.TabIndex = 0;
            this.gdList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gdList.VisualStyleManager = this.visualStyleManager1;
            this.gdList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gdList_RowDoubleClick);
            this.gdList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.gdList_DeletingRecords);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // TraCuuTTDNFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 634);
            this.Controls.Add(this.uiGroupBox2);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "TraCuuTTDNFrm";
            this.Text = "Tra cứu thông tin doanh nghiệp";
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private Janus.Windows.GridEX.GridEX gdList;
        private Janus.Windows.EditControls.UIButton btnTim;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenGiamDoc;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Janus.Windows.EditControls.UIButton btnThem;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCoSo;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiCoSo;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.IntegerUpDown txtNamCap;
        private Janus.Windows.GridEX.EditControls.IntegerUpDown txtThangCap;
        private Janus.Windows.GridEX.EditControls.IntegerUpDown txtNgayCap;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}