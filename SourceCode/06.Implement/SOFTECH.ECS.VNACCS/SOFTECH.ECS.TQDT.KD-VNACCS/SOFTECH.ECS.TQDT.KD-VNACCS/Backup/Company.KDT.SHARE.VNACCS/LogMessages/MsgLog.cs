﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.VNACCS.LogMessages
{
    public partial class MsgLog
    {


        public static IList<MsgLog> SelectCollectionDynamicByItemID(int itemID)
        {
            return SelectCollectionDynamic("Item_id = " + itemID, "CreatedTime");
        }
        
        public static bool SaveMessages(string Msg,string MaNghiepVu, long itemid, string TieuDeThongBao, string NoiDungThongBao, string messtag, string inputMsgID, string indexTag)
        {
            try
            {
                MsgLog mess = new MsgLog
                    {
                        Log_Messages = Msg,
                        MaNghiepVu = MaNghiepVu,
                        Item_id = itemid,
                        TieuDeThongBao = TieuDeThongBao,
                        NoiDungThongBao = NoiDungThongBao,
                        MessagesTag = messtag,
                        InputMessagesID = inputMsgID,
                        IndexTag = indexTag,
                        CreatedTime = DateTime.Now,
                    };  
                return mess.Insert() == 1;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(new Exception("Msg"));
                return false;
            }
         
          
        }
        public static bool SaveMessages(MessagesSend msgSend, long itemid, string TieuDe, string NoiDungTB)
        {
            try
            {
                MsgLog mess = new MsgLog
                    {
                        Log_Messages = HelperVNACCS.BuildEdiMessages(msgSend).ToString(),
                        MaNghiepVu = msgSend.Header.MaNghiepVu.GetValue().ToString(),
                        Item_id = itemid,
                        TieuDeThongBao = TieuDe,
                        NoiDungThongBao = NoiDungTB,
                        MessagesTag = msgSend.Header.MessagesTag.GetValue().ToString(),
                        InputMessagesID = msgSend.Header.InputMessagesID.GetValue().ToString(),
                        IndexTag = msgSend.Header.IndexTag.GetValue().ToString(),
                        CreatedTime = DateTime.Now,
                    };
                return mess.Insert() == 1;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            
        }
       

        public static DataTable GetKetQuaXuLy(long itemID,string typeMsg)
        {
            string sql = string.Format(@"SELECT ID,TieuDeThongBao,CreatedTime,
	                                       InputMessagesID
	                                       ,MaNghiepVu
                                    FROM t_KDT_VNACCS_MsgLog 
                                    WHERE InputMessagesID IN (SELECT DISTINCT(InputMessagesID) FROM t_KDT_VNACCS_MsgLog
                                    WHERE Item_id = {0} AND IndexTag IN ('','{1}'))
                                    UNION
                                    SELECT Master_ID AS ID,'' AS TieuDeThongBao,CreatedTime ,
		                                    MessagesInputID AS InputMessagesID,
	                                       MaNghiepVu
                                      FROM t_KDT_VNACCS_MsgPhanBo
                                        WHERE t_KDT_VNACCS_MsgPhanBo.MessagesInputID IN (SELECT DISTINCT(InputMessagesID) FROM t_KDT_VNACCS_MsgLog
                                    WHERE Item_id = {0} AND IndexTag IN ('','{1}')) 

                                    ORDER BY CreatedTime ",itemID,typeMsg);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@itemID", SqlDbType.BigInt, itemID);
            //db.AddInParameter(dbCommand, "@type", SqlDbType.VarChar, typeMsg);

            return db.ExecuteDataSet(dbCommand).Tables[0];

        }
        //minhnd
        public static DataTable GetKetQuaXuLy(long itemID)
        {
            string sql = string.Format(@"SELECT ID,TieuDeThongBao,CreatedTime,
	                                       InputMessagesID
	                                       ,MaNghiepVu
                                    FROM t_KDT_VNACCS_MsgLog 
                                    WHERE InputMessagesID IN (SELECT DISTINCT(InputMessagesID) FROM t_KDT_VNACCS_MsgLog
                                    WHERE Item_id = {0} and MaNghiepVu like '%HYS%')
                                    UNION
                                    SELECT Master_ID AS ID,'' AS TieuDeThongBao,CreatedTime ,
		                                    MessagesInputID AS InputMessagesID,
	                                       MaNghiepVu
                                      FROM t_KDT_VNACCS_MsgPhanBo t
                                        WHERE t.MessagesInputID IN (SELECT DISTINCT(InputMessagesID) FROM t_KDT_VNACCS_MsgLog
                                    WHERE Item_id = {0}) and t.MaNghiepVu LIKE 'VAL0020'

                                    ORDER BY CreatedTime ", itemID);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@itemID", SqlDbType.BigInt, itemID);
            //db.AddInParameter(dbCommand, "@type", SqlDbType.VarChar, typeMsg);

            return db.ExecuteDataSet(dbCommand).Tables[0];

        }
        public static MsgLog GetThongTinToKhaiTam(string inputMSg, string MaNghiepVu)
        {
            string sql = string.Format(@"SELECT TOP 1 * FROM [t_KDT_VNACCS_MsgLog]
WHERE InputMessagesID = '{0}' AND MaNghiepVu LIKE '%{1}%'
ORDER BY id desc ", inputMSg, MaNghiepVu);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<MsgLog> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }


        public bool ProcessMSG(string MsgFeedBack,bool isThemMoi, SqlTransaction transaction)
        {
            //KDT_VNACC_ToKhaiMauDich tkmd = null;
            //bool isCapNhatTK = false;
            MsgLog log = new MsgLog();
            try
            {

                if (!string.IsNullOrEmpty(MsgFeedBack) && !MsgFeedBack.Contains(EnumThongBao.KetThucPhanHoi))
                {
                    #region log

                    log.Log_Messages = MsgFeedBack;
                    log.MaNghiepVu = "Return";
                    log.MessagesTag = "";
                    log.NoiDungThongBao = "";
                    log.TieuDeThongBao = "Thông tin phản hồi từ HQ";
                    log.InputMessagesID = "";
                    log.Item_id = 0;
                    log.IndexTag = "";
                    log.CreatedTime = DateTime.Now;
                    #endregion
                    //if (MsgFeedBack.Contains("\n\n")) MsgFeedBack = MsgFeedBack.Replace("\n\n", "\r\n");
                    //string[] lines = MsgFeedBack.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                    ReturnMessages msgReturn = new ReturnMessages(MsgFeedBack);
                    if (msgReturn == null)
                    {
                        long idLog = log.Insert(transaction);
                        if (idLog == 0)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(new Exception("Không lưu được messages phản hồi"));
                            return false;
                        }
                        Logger.LocalLogger.Instance().WriteMessage(new Exception("Không xử lý được messages có ID = " + idLog));
                        return false;
                    }
                    #region Lưu msg chờ xử lý

                    MsgPhanBo msgPB = new MsgPhanBo();
                    IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic("RTPTag = '" + msgReturn.GetRTPtag().Trim() + "'", null);
                    if (listPB != null && listPB.Count > 0)
                    {
                        msgPB = listPB[0];
                        msgPB.SoTiepNhan = msgReturn.GetSoTNHeader();
                        msgPB.MessagesInputID = msgReturn.header.InputMessagesID.GetValue().ToString();
                        msgPB.messagesTag = msgReturn.header.MessagesTag.GetValue().ToString();
                        msgPB.TerminalID = msgReturn.header.TerminalID.GetValue().ToString();
                        msgPB.MaNghiepVu = msgReturn.getMaNVPhanHoi();
                        msgPB.Update(transaction);
                    }
                    else
                    {
                        long idLog = log.Insert(transaction);
                        if (idLog == 0)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(new Exception("Không lưu được messages phản hồi"));
                            return false;
                        }
                        msgPB.Master_ID = idLog;
                        msgPB.CreatedTime = DateTime.Now;
                        msgPB.SoTiepNhan = msgReturn.GetSoTNHeader();
                        msgPB.RTPTag = msgReturn.GetRTPtag();
                        msgPB.MessagesInputID = msgReturn.header.InputMessagesID.GetValue().ToString();
                        msgPB.messagesTag = msgReturn.header.MessagesTag.GetValue().ToString();
                        msgPB.TerminalID = msgReturn.header.TerminalID.GetValue().ToString();
                        msgPB.IndexTag = msgReturn.header.IndexTag.GetValue().ToString();
                        msgPB.TrangThai = EnumTrangThaiXuLyMessage.ChuaXuLy;
                        msgPB.GhiChu = "Nhập thủ công";
                        KDT_VNACC_ToKhaiMauDich TKMD = ProcessMessages.XuLyMsg(msgReturn, isThemMoi);
                        if (TKMD != null)
                        {
                            msgPB.TrangThai = EnumTrangThaiXuLyMessage.DaXem;
                            //if (TKMD.TrangThaiXuLy.Contains("3")) //Thong quan
                            ////{
                            ////    tkmd = TKMD;
                            ////    isCapNhatTK = true;
                            //}
                            if (isThemMoi)
                                TKMD.InsertUpdateFull(transaction);
                        }
                        msgPB.MaNghiepVu = msgReturn.getMaNVPhanHoi();
                        msgPB.Insert(transaction);
                    }
                    #endregion
                    
                    return true;
                }
                else
                    return false;
            }
            catch (System.Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                if (log.ID > 0) Logger.LocalLogger.Instance().WriteMessage(new Exception("Không xử lý được messages có ID = " + log.ID));
                throw ex;
            }
        }


    }
}
