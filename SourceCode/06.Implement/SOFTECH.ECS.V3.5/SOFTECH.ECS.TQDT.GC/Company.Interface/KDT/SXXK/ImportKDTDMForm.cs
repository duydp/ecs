using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
using Company.Interface.CauHinh;
using Company.QuanTri;
namespace Company.Interface.KDT.SXXK
{
    public partial class ImportKDTDMForm : BaseForm
    {
        public DinhMucDangKy dmDangKy;// = new DinhMucDangKy();                                                   
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        public int FormatDMSD = GlobalSettings.SoThapPhan.DinhMuc;
        public int FormatTLHH = GlobalSettings.SoThapPhan.TLHH;
        public bool ImportExcelSucces = false;
        public ImportKDTDMForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        //private int checkDMExit1(string maSP, string maNPL)
        //{
        //    for (int i = 0; i < this.DMCollection.Count; i++)
        //    {
        //        if (this.DMCollection[i].MaSanPHam.ToUpper() == maSP.ToUpper() && this.DMCollection[i].MaNguyenPhuLieu.ToUpper() == maNPL.ToUpper()) return i;
        //    }
        //    return -1;

        //}

        private void ImportDMForm_Load(object sender, EventArgs e)
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.ReadDefaultDM(cbbSheetName, txtRow, txtMaSPColumn, txtMaNPLColumn, txtDMSDColumn, txtTLHHColumn, txtNPLCungUng, txtMaDD, chkOverwrite);
                FormatDMSD = f.FormatDMSD;
                FormatTLHH = f.FormatTLHH;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            // this.dgList.DataSource = this.DMReadCollection;
        }
        private void SaveDefault()
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.SaveConfigDM(cbbSheetName.Text, txtRow.Text, txtMaSPColumn.Text, txtMaNPLColumn.Text, txtDMSDColumn.Text, txtTLHHColumn.Text, txtNPLCungUng.Text, txtMaDD.Text, chkOverwrite.Checked);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool EmptyCheck(string st)
        {
            if (st == "") return true;
            return false;
        }
        private bool CheckNumber(decimal so)
        {
            if (so < 0 || so > 1000000000000) return true;
            return false;
        }

        private bool CheckTLHH(decimal so)
        {
            if (so < 0 || so > 100) return true;
            return false;
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                //Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("Lỗi khi chọn file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                //Dictionary<string, Worksheet> dict = new Dictionary<string, Worksheet>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                    //dict.Add(worksheet.Name, worksheet);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc sheet. Doanh nghiệp hãy kiểm tra lại tên sheet trước khi chọn file.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtFilePath, errorProvider, "ĐƯỜNG DẪN FILE EXCEL", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbSheetName, errorProvider, "TÊN SHEET", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaNPLColumn, errorProvider, "MÃ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaSPColumn, errorProvider, "MÃ SẢN PHẨM", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDMSDColumn, errorProvider, "ĐỊNH MỨC SỬ DỤNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTLHHColumn, errorProvider, "TỶ LỆ HAO HỤT", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNPLCungUng, errorProvider, "NPL CUNG ỨNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaDD, errorProvider, "MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtRow, errorProvider, "DÒNG BẮT ĐẦU");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                int beginRow = Convert.ToInt32(txtRow.Value) - 1;
                if (beginRow < 0)
                {
                    error.SetError(txtRow, "DÒNG BẮT ĐẦU PHẢI LỚN HƠN 0");
                    error.SetIconPadding(txtRow, 8);
                    return;

                }
                this.Cursor = Cursors.WaitCursor;
                Workbook wb = new Workbook();
                Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text, true);
                }
                catch (Exception ex)
                {
                    MLMessages("Lỗi khi đọc file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    return;
                }
                try
                {
                    ws = wb.Worksheets[cbbSheetName.Text];
                }
                catch
                {
                    MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"", "MSG_EXC01", cbbSheetName.Text, false);
                    return;
                }

                WorksheetRowCollection wsrc = ws.Rows;
                char maSPColumn = Convert.ToChar(txtMaSPColumn.Text);
                int maSPCol = ConvertCharToInt(maSPColumn);

                char maNPLColumn = Convert.ToChar(txtMaNPLColumn.Text);
                int maNPLCol = ConvertCharToInt(maNPLColumn);

                char DMSDColumn = Convert.ToChar(txtDMSDColumn.Text);
                int DMSDCol = ConvertCharToInt(DMSDColumn);

                char TLHHColumn = Convert.ToChar(txtTLHHColumn.Text);
                int TLHHCol = ConvertCharToInt(TLHHColumn);

                char MaDinhDanhColumn = Convert.ToChar(txtMaDD.Text);
                int MaDinhDanhCol = ConvertCharToInt(MaDinhDanhColumn);

                string errorTotal = "";
                string errorMaSanPham = "";
                string errorMaSanPhamExits = "";
                string errorMaNguyenPhuLieu = "";
                string errorMaNguyenPhuLieuExits = "";
                string errorDMSD = "";
                string errorDMSDExits = "";
                string errorTLHH = "";
                string errorTLHHExits = "";
                string errorMaDinhDanh = "";
                string errorDinhMuc = "";
                string errorDinhMucExits = "";
                string errorMaDinhDanhExits = "";

                string errorNPL = "";
                string errorSP = "";
                string errorSPExits = "";
                bool isCheck = false;
                string MaNPLCheck = String.Empty;
                string MaSPCheck = String.Empty;
                List<KDT_LenhSanXuat> LenhSXCollection = KDT_LenhSanXuat.SelectCollectionDynamic("HopDong_ID = " + dmDangKy.ID_HopDong, "");

                foreach (KDT_LenhSanXuat item in LenhSXCollection)
                {
                    item.SPCollection =  KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
                }
                List<GC_NguyenPhuLieu> AllNPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(dmDangKy.ID_HopDong);
                List<GC_SanPham> AllSPCollection = GC_SanPham.SelectCollectionBy_HopDong_ID(dmDangKy.ID_HopDong);
                List<KDT_GC_DinhMucDangKy> AllDMDKCollection = KDT_GC_DinhMucDangKy.SelectCollectionBy_ID_HopDongAndMaSP(dmDangKy.ID_HopDong);
                List<GC_DinhMuc> AllDMCollection = GC_DinhMuc.SelectCollectionBy_HopDong_ID(dmDangKy.ID_HopDong);
                Company.GC.BLL.KDT.GC.DinhMucCollection DMCollectionAdd = new Company.GC.BLL.KDT.GC.DinhMucCollection(); 
                List<Company.GC.BLL.GC.SanPham> SPCollectionCheck = new List<Company.GC.BLL.GC.SanPham>();
                List<Company.GC.BLL.GC.NguyenPhuLieu> NPLCollectionCheck = new List<Company.GC.BLL.GC.NguyenPhuLieu>();
                bool isAdd = true;
                bool isExits = false;
                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            Company.GC.BLL.KDT.GC.DinhMuc dinhMuc = new Company.GC.BLL.KDT.GC.DinhMuc();
                            if (wsr.Cells[maNPLCol].Value == null || Convert.ToString(wsr.Cells[maNPLCol].Value) == "") continue;
                            try
                            {
                                dinhMuc.MaSanPham = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                                if (dinhMuc.MaSanPham.Length == 0)
                                {
                                    errorMaSanPham += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaSanPham + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    //TẠO DANH SÁCH SẢN PHẨM ĐỂ KIỂM TRA ĐÃ ĐĂNG KÝ HAY CHƯA
                                    if (String.IsNullOrEmpty(MaSPCheck))
                                    {
                                        Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                                        SP.Ma = dinhMuc.MaSanPham;
                                        SP.STT = (wsr.Index + 1);
                                        SPCollectionCheck.Add(SP);
                                        MaSPCheck = dinhMuc.MaSanPham;
                                    }
                                    else if (MaSPCheck != dinhMuc.MaSanPham)
                                    {
                                        Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                                        SP.Ma = dinhMuc.MaSanPham;
                                        SP.STT = (wsr.Index + 1);
                                        SPCollectionCheck.Add(SP);
                                        MaSPCheck = dinhMuc.MaSanPham;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorMaSanPham += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaSanPham + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                dinhMuc.MaNguyenPhuLieu = Convert.ToString(wsr.Cells[maNPLCol].Value).Trim();
                                isExits = false;
                                if (dinhMuc.MaNguyenPhuLieu.Length == 0)
                                {
                                    errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNguyenPhuLieu + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(MaNPLCheck))
                                    {
                                        Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                                        NPL.Ma = dinhMuc.MaNguyenPhuLieu;
                                        NPL.STT = (wsr.Index + 1);
                                        NPLCollectionCheck.Add(NPL);
                                        MaNPLCheck = dinhMuc.MaNguyenPhuLieu;
                                    }
                                    else if (MaNPLCheck != dinhMuc.MaNguyenPhuLieu)
                                    {
                                        Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                                        NPL.Ma = dinhMuc.MaNguyenPhuLieu;
                                        NPL.STT = (wsr.Index + 1);
                                        NPLCollectionCheck.Add(NPL);
                                        MaNPLCheck = dinhMuc.MaNguyenPhuLieu;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNguyenPhuLieu + "]\n";
                                isAdd = false;
                            }
                            string DinhMucSuDung = String.Empty;
                            try
                            {
                                DinhMucSuDung = wsr.Cells[DMSDCol].Value.ToString();
                                if (DinhMucSuDung.Length == 0)
                                {
                                    errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    dinhMuc.DinhMucSuDung = Convert.ToDecimal(wsr.Cells[DMSDCol].Value);
                                    if (Decimal.Round(dinhMuc.DinhMucSuDung, FormatDMSD) != dinhMuc.DinhMucSuDung)
                                    {
                                        errorDMSDExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.DinhMucSuDung + "]\n";
                                        isAdd = false;
                                    }

                                }
                            }
                            catch (Exception)
                            {
                                errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                isAdd = false;
                            }
                            String TLHH = String.Empty;
                            try
                            {
                                TLHH = wsr.Cells[TLHHCol].Value.ToString();
                                if (TLHH.Length == 0)
                                {
                                    errorTLHH += "[" + (wsr.Index + 1) + "]-[" + TLHH + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    dinhMuc.TyLeHaoHut = Convert.ToDecimal(wsr.Cells[TLHHCol].Value);
                                    if (Decimal.Round(dinhMuc.TyLeHaoHut, FormatTLHH) != dinhMuc.TyLeHaoHut)
                                    {
                                        errorTLHHExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.TyLeHaoHut + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorTLHH += "[" + (wsr.Index + 1) + "]-[" + TLHH + "]\n";
                                isAdd = false;
                            }
                            String MaDinhDanh = String.Empty;
                            try
                            {
                                MaDinhDanh = wsr.Cells[MaDinhDanhCol].Value.ToString();
                                if (MaDinhDanh.Length == 0)
                                {
                                    errorMaDinhDanh += "[" + (wsr.Index + 1) + "]-[" + MaDinhDanh + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    dinhMuc.MaDDSX = Convert.ToString(MaDinhDanh);
                                }
                            }
                            catch (Exception)
                            {
                                errorMaDinhDanh += "[" + (wsr.Index + 1) + "]-[" + MaDinhDanh + "]\n";
                                isAdd = false;
                            }
                            if (isAdd)
                                DMCollectionAdd.Add(dinhMuc);
                        }
                        catch (Exception ex)
                        {
                            this.SaveDefault();
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            return;
                        }
                    }
                }
                //KIỂM TRA ĐỊNH MỨC ĐÃ ĐĂNG KÝ
                //isExits = false;
                //foreach (Company.GC.BLL.GC.SanPham item in SPCollectionCheck)
                //{
                //    foreach (GC_DinhMuc items in AllDMCollection)
                //    {
                //        if (item.Ma == items.MaSanPham)
                //        {
                //            if (LenhSXCollection.Count >0)
                //            {
                //                foreach (KDT_LenhSanXuat LSX in LenhSXCollection)
                //                {
                //                    foreach (KDT_LenhSanXuat_SP SP in LSX.SPCollection)
                //                    {
                //                        if (item.Ma == SP.MaSanPham)
                //                        {
                //                            isExits = true;
                //                            break;
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //    if (isExits)
                //    {
                //        errorMaDinhDanhExits += "[" + item.STT + "]-[" + item.MaDDSX + "]\n";
                //    }

                //}
                isExits = false;
                String STT = String.Empty;
                String MaSPTemp = String.Empty;
                //foreach (Company.GC.BLL.GC.SanPham items in SPCollectionCheck)
                //{
                //    foreach (KDT_GC_DinhMucDangKy item in AllDMDKCollection)
                //    {
                //        if (chkOverwrite.Checked)
                //        {
                //            if (item.MaSanPham == items.Ma && item.ID != dmDangKy.ID)
                //            {

                //                STT = items.STT.ToString();
                //                if (MaSPTemp != items.Ma)
                //                {
                //                    errorDinhMuc += "[" + STT + "]-[" + item.MaSanPham + "]-[" + item.SoTiepNhan + "]-[" + item.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(item.TrangThaiXuLy).ToUpper() + "]\n";
                //                }
                //                MaSPTemp = items.Ma;
                //                break;
                //            }
                //        }
                //        else
                //        {
                //            if (item.MaSanPham == items.Ma)
                //            {
                //                STT = items.STT.ToString();
                //                if (MaSPTemp != items.Ma)
                //                {
                //                    errorDinhMuc += "[" + STT + "]-[" + item.MaSanPham + "]-[" + item.SoTiepNhan + "]-[" + item.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(item.TrangThaiXuLy).ToUpper() + "]\n";
                //                }
                //                MaSPTemp = items.Ma;
                //                break;
                //            }
                //        }
                //    }
                //}
                //NẾU CHƯA ĐĂNG KÝ THÌ KIỂM TRA ĐỊNH MỨC TRÊN LƯỚI

                if (String.IsNullOrEmpty(errorDinhMucExits) && String.IsNullOrEmpty(errorDinhMuc))
                {
                    String MaSP = String.Empty;
                    String MaNPL = String.Empty;
                    foreach (Company.GC.BLL.KDT.GC.DinhMuc dinhMuc in DMCollectionAdd)
                    {
                        //NẾU KHÔNG GHI ĐÈ
                        isExits = false;
                        if (!chkOverwrite.Checked)
                        {
                            foreach (Company.GC.BLL.KDT.GC.DinhMuc item in dmDangKy.DMCollection)
                            {
                                if (dinhMuc.MaSanPham.Trim().ToUpper() == item.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == item.MaNguyenPhuLieu.Trim().ToUpper())
                                {
                                    isExits = true;
                                    break;
                                }
                            }
                            if (isExits)
                            {
                                if (MaSP != dinhMuc.MaSanPham && MaNPL != dinhMuc.MaNguyenPhuLieu)
                                {
                                    errorSPExits += "[" + dinhMuc.STT + "]-[" + dinhMuc.MaSanPham + "]\n";
                                }
                                MaSP = dinhMuc.MaSanPham;
                                MaNPL = dinhMuc.MaNguyenPhuLieu;
                                isAdd = false;
                            }
                        }
                        else
                        {
                            foreach (Company.GC.BLL.KDT.GC.DinhMuc item in dmDangKy.DMCollection)
                            {
                                if (dinhMuc.MaSanPham.Trim().ToUpper() == item.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == item.MaNguyenPhuLieu.Trim().ToUpper())
                                {
                                    dmDangKy.DMCollection.Remove(item);
                                    item.Delete(dmDangKy.ID_HopDong);
                                    dmDangKy.DMCollection.Add(dinhMuc);
                                    break;
                                }
                            }
                        }
                    }
                    if (String.IsNullOrEmpty(errorSP))
                    {
                        //KIỂM TRA MÃ SP ĐÃ ĐƯỢC ĐĂNG KÝ HAY CHƯA
                        MaSPTemp = String.Empty;
                        String MaNPLTemp = String.Empty;
                        foreach (Company.GC.BLL.GC.SanPham item in SPCollectionCheck)
                        {
                            isExits = false;
                            foreach (GC_SanPham items in AllSPCollection)
                            {
                                if (item.Ma == items.Ma)
                                {                                    
                                    isExits = true;
                                    break;
                                }
                            }
                            if (!isExits)
                            {
                                if (MaSPTemp != item.Ma)
                                {
                                    errorSP += "[" + item.STT + "]-[" + item.Ma + "]\n";
                                }
                                MaSPTemp = item.Ma;
                            }
                        }
                        //KIỂM TRA MÃ NPL ĐÃ ĐƯỢC ĐĂNG KÝ HAY CHƯA
                        foreach (Company.GC.BLL.GC.NguyenPhuLieu item in NPLCollectionCheck)
                        {
                            isExits = false;
                            foreach (GC_NguyenPhuLieu items in AllNPLCollection)
                            {
                                if (item.Ma == items.Ma)
                                {
                                    item.Ten = items.Ten;
                                    isExits = true;
                                    break;
                                }
                            }
                            if (!isExits)
                            {
                                if (MaNPLTemp != item.Ma)
                                {
                                    errorNPL += "[" + item.STT + "]-[" + item.Ma + "]\n";
                                }
                                MaNPLTemp = item.Ma;
                            }
                        }
                    }
                }
                else
                {
                    //LẤY THÔNG TIN ĐỊNH MỨC ĐÃ NHẬP LIỆU TRƯỚC ĐÓ
                    errorDinhMucExits = String.Empty;
                    isExits = false;
                    STT = String.Empty;
                    MaSPTemp = String.Empty;
                    foreach (Company.GC.BLL.GC.SanPham items in SPCollectionCheck)
                    {
                        foreach (KDT_GC_DinhMucDangKy item in AllDMDKCollection)
                        {
                            if (chkOverwrite.Checked)
                            {
                                if (item.MaSanPham == items.Ma && item.ID != dmDangKy.ID)
                                {

                                    STT = items.STT.ToString();
                                    if (MaSPTemp != items.Ma)
                                    {
                                        errorDinhMuc += "[" + STT + "]-[" + item.MaSanPham + "]-[" + item.SoTiepNhan + "]-[" + item.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(item.TrangThaiXuLy).ToUpper() + "]\n";
                                    }
                                    MaSPTemp = items.Ma;
                                    break;
                                }
                            }
                            else
                            {
                                if (item.MaSanPham == items.Ma)
                                {
                                    STT = items.STT.ToString();
                                    if (MaSPTemp != items.Ma)
                                    {
                                        errorDinhMuc += "[" + STT + "]-[" + item.MaSanPham + "]-[" + item.SoTiepNhan + "]-[" + item.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(item.TrangThaiXuLy).ToUpper() + "]\n";
                                    }
                                    MaSPTemp = items.Ma;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (!String.IsNullOrEmpty(errorMaSanPham))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorMaSanPham + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorMaSanPhamExits))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorMaSanPhamExits + " CHƯA ĐƯỢC ĐĂNG KÝ\n";
                if (!String.IsNullOrEmpty(errorSPExits))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSPExits + " ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC TRÊN LƯỚI\n";
                if (!String.IsNullOrEmpty(errorMaNguyenPhuLieu))
                    errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorMaNguyenPhuLieu + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorMaNguyenPhuLieuExits))
                    errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorMaNguyenPhuLieuExits + " CHƯA ĐƯỢC ĐĂNG KÝ\n";
                if (!String.IsNullOrEmpty(errorDMSD))
                    errorTotal += "\n - [STT] -[ĐỊNH MỨC SỬ DỤNG] : \n" + errorDMSD + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorDMSDExits))
                    errorTotal += "\n - [STT] -[ĐỊNH MỨC SỬ DỤNG] : \n" + errorDMSDExits + " DOANH NGHIỆP CẤU HÌNH CHỈ ĐƯỢC NHẬP TỐI ĐA " + FormatDMSD + " SỐ THẬP PHÂN . ĐỂ CẤU HÌNH : VÀO MENU HỆ THỐNG - CẤU HÌNH THÔNG SỐ ĐỌC EXCEL\n";
                if (!String.IsNullOrEmpty(errorTLHH))
                    errorTotal += "\n - [STT] -[TỶ LỆ HAO HỤT] : \n" + errorTLHH + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorTLHHExits))
                    errorTotal += "\n - [STT] -[TỶ LỆ HAO HỤT] : \n" + errorTLHHExits + " DOANH NGHIỆP CẤU HÌNH CHỈ ĐƯỢC NHẬP TỐI ĐA " + FormatTLHH + " SỐ THẬP PHÂN .ĐỂ CẤU HÌNH : VÀO MENU HỆ THỐNG - CẤU HÌNH THÔNG SỐ ĐỌC EXCEL\n";
                if (!String.IsNullOrEmpty(errorMaDinhDanh))
                    errorTotal += "\n - [STT] -[MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT] : \n" + errorMaDinhDanh + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorDinhMuc))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + errorDinhMuc + " ĐÃ ĐƯỢC NHẬP LIỆU ĐỊNH MỨC TRƯỚC ĐÓ .\n";
                if (!String.IsNullOrEmpty(errorDinhMucExits))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorDinhMucExits + " ĐÃ ĐƯỢC KHAI BÁO ĐỊNH MỨC\n";
                if (!String.IsNullOrEmpty(errorNPL))
                    errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorNPL + " CHƯA ĐƯỢC ĐĂNG KÝ\n";
                if (!String.IsNullOrEmpty(errorSP))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSP + " CHƯA ĐƯỢC ĐĂNG KÝ\n";

                if (String.IsNullOrEmpty(errorTotal))
                {
                    foreach (Company.GC.BLL.KDT.GC.DinhMuc DM in DMCollectionAdd)
                    {
                        foreach (Company.GC.BLL.GC.NguyenPhuLieu item in NPLCollectionCheck)
                        {
                            if (DM.MaNguyenPhuLieu==item.Ma)
                            {
                                DM.TenNPL = item.Ten;
                            }
                        }
                        //LOẠI BỎ NHỮNG ĐỊNH MỨC ĐÃ NHẬP VỚI ĐỊNH MỨC SỬ DỤNG BẰNG 0
                        if (DM.DinhMucSuDung !=0)
                        {
                            dmDangKy.DMCollection.Add(DM);   
                        }
                    }
                    ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
                    LogHistory Log = new LogHistory();
                    Log.LogDinhMuc(dmDangKy, Company.KDT.SHARE.Components.MessageTitle.NhapExcel, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    ImportExcelSucces = true;
                }
                else
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG .\r\n" + errorTotal, false);
                    return;
                }
                this.SaveDefault();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {

            //cvError.Validate();
            //if (!cvError.IsValid) return;
            if (!ValidateForm(false))
                return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;

            this.Cursor = Cursors.WaitCursor;
            uiButton1.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                //showMsg("MSG_0203008");
                ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại.\r\n\nLỖI CHI TIẾT: " + ex.Message, false);
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", cbbSheetName.Text);
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }

            WorksheetRowCollection wsrc = ws.Rows;
            char maSPColumn = Convert.ToChar(txtMaSPColumn.Text);
            int maSPCol = ConvertCharToInt(maSPColumn);

            char maNPLColumn = Convert.ToChar(txtMaNPLColumn.Text);
            int maNPLCol = ConvertCharToInt(maNPLColumn);

            char DMSDColumn = Convert.ToChar(txtDMSDColumn.Text);
            int DMSDCol = ConvertCharToInt(DMSDColumn);

            char TLHHColumn = Convert.ToChar(txtTLHHColumn.Text);
            int TLHHCol = ConvertCharToInt(TLHHColumn);


            char NPLCungUngColumn = Convert.ToChar(txtNPLCungUng.Text);
            int NPLCungUngCol = ConvertCharToInt(NPLCungUngColumn);

            char MaDinhDanhColumn = Convert.ToChar(txtMaDD.Text);
            int MaDinhDanhCol = ConvertCharToInt(MaDinhDanhColumn);


            string errorTotal = "";
            string errorMaSanPham = "";
            string errorMaSanPhamExits = "";
            string errorMaNguyenPhuLieu = "";
            string errorMaNguyenPhuLieuExits = "";
            string errorDMSD = "";
            string errorDMSDExits = "";
            string errorTLHH = "";
            string errorTLHHExits = "";
            string errorMaDinhDanh = "";
            string errorDinhMuc = "";
            string errorDinhMucExits = "";

            string errorNPL = "";
            string errorSP = "";
            string errorTB = "";
            string errorHM = "";

            List<GC_NguyenPhuLieu> NPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(dmDangKy.ID_HopDong);
            List<GC_SanPham> SPCollection = GC_SanPham.SelectCollectionBy_HopDong_ID(dmDangKy.ID_HopDong);
            List<GC_ThietBi> TBCollection = GC_ThietBi.SelectCollectionBy_HopDong_ID(dmDangKy.ID_HopDong);
            List<GC_HangMau> HMCollection = GC_HangMau.SelectCollectionDynamic("HopDong_ID = " + dmDangKy.ID_HopDong, "");
            List<KDT_GC_DinhMucDangKy> DMDKCollection = KDT_GC_DinhMucDangKy.SelectCollectionBy_ID_HopDong(dmDangKy.ID_HopDong);
            List<GC_DinhMuc> DMCollection = GC_DinhMuc.SelectCollectionBy_HopDong_ID(dmDangKy.ID_HopDong);
            Company.GC.BLL.KDT.GC.DinhMucCollection DMCollectionAdd = new Company.GC.BLL.KDT.GC.DinhMucCollection(); 
            int j = 0;
            bool isCheck = false;
            string MaNPLCheck = String.Empty;
            string MaSPCheck = String.Empty;
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        Company.GC.BLL.KDT.GC.DinhMuc dinhMuc = new Company.GC.BLL.KDT.GC.DinhMuc();
                        bool isAdd = true;
                        bool isExits = false;
                        if (wsr.Cells[maNPLCol].Value == null || Convert.ToString(wsr.Cells[maNPLCol].Value) == "") continue;                                              
                        dinhMuc.MaSanPham = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                        if (dinhMuc.MaSanPham.Length == 0)
                        {
                            errorMaSanPham += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaSanPham + "]\n";
                            isAdd = false;
                        }
                        else
                        {
                            //LOẠI BỎ CHECK MÃ SP NHIỀU LẦN CHỈ CHECK LẦN ĐẦU NẾU TỒN TẠI THÌ CÁC LẦN SAU SẼ BỎ QUA
                            //if (String.IsNullOrEmpty(MaSPCheck))
                            //{
                            //    MaSPCheck = dinhMuc.MaSanPham;
                            //}
                            //else if (MaSPCheck == dinhMuc.MaSanPham && isCheck)
                            //{
                            //    MaSPCheck = dinhMuc.MaSanPham;
                            //    break;
                            //}
                            //else
                            //{
                            //    isCheck = false;
                            //}
                            foreach (GC_SanPham item in SPCollection)
                            {
                                if (item.Ma == dinhMuc.MaSanPham)
                                {
                                    dinhMuc.TenSanPham = item.Ten;
                                    isExits = true;
                                    break;
                                }
                            }
                            if (!isExits)
                            {
                                errorSP += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaSanPham + "]\n";
                                isAdd = false;
                                isCheck = true;
                                break;
                            }
                        }
                        dinhMuc.MaNguyenPhuLieu = Convert.ToString(wsr.Cells[maNPLCol].Value).Trim();
                        isExits = false;
                        if (dinhMuc.MaNguyenPhuLieu.Length == 0)
                        {
                            errorMaNguyenPhuLieu+= "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNguyenPhuLieu + "]\n";
                            isAdd = false;
                        }
                        else
                        {
                            //if (String.IsNullOrEmpty(MaNPLCheck))
                            //{
                            //    MaNPLCheck = dinhMuc.MaNguyenPhuLieu;
                            //}
                            //else if (MaNPLCheck == dinhMuc.MaNguyenPhuLieu && isCheck)
                            //{
                            //    MaNPLCheck = dinhMuc.MaNguyenPhuLieu;
                            //    break;
                            //}
                            //else
                            //{
                            //    isCheck = false;
                            //}
                            foreach (GC_NguyenPhuLieu item in NPLCollection)
                            {
                                if (item.Ma == dinhMuc.MaNguyenPhuLieu)
                                {
                                    dinhMuc.TenNPL = item.Ten;
                                    dinhMuc.DVT_ID = item.DVT_ID;
                                    isExits = true;
                                    break;
                                }
                            }
                            if (!isExits)
                            {
                                errorNPL += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNguyenPhuLieu + "]\n";
                                isAdd = false;
                                isCheck = true;
                                break;
                            }
                        }
                        string DinhMucSuDung = wsr.Cells[DMSDCol].Value.ToString();
                        if (DinhMucSuDung.Length==0)
                        {
                                errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                isAdd = false;
                        }
                        else
                        {
                            dinhMuc.DinhMucSuDung = Convert.ToDecimal(wsr.Cells[DMSDCol].Value);
                            if (Decimal.Round(dinhMuc.DinhMucSuDung, 8) != dinhMuc.DinhMucSuDung)
                            {
                                errorDMSDExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.DinhMucSuDung + "]\n";
                                isAdd = false;
                            }

                        }
                        string TLHH = wsr.Cells[TLHHCol].Value.ToString();
                        if (TLHH.Length == 0)
                        {
                            errorTLHH += "[" + (wsr.Index + 1) + "]-[" + TLHH + "]\n";
                            isAdd = false;
                        }
                        else
                        {
                            dinhMuc.TyLeHaoHut = Convert.ToDecimal(wsr.Cells[TLHHCol].Value);
                            if (Decimal.Round(dinhMuc.TyLeHaoHut, 4) != dinhMuc.TyLeHaoHut)
                            {
                                errorTLHHExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.TyLeHaoHut + "]\n";
                                isAdd = false;
                            }

                        }                        
                        if (wsr.Cells[MaDinhDanhCol].Value.ToString().Length == 0)
                        {
                            errorMaDinhDanh += "[" + (wsr.Index + 1) + "]-[" + wsr.Cells[MaDinhDanhCol].Value.ToString() + "]\n";
                            isAdd = false;
                        }
                        else
                        {
                            dinhMuc.MaDDSX = Convert.ToString(wsr.Cells[MaDinhDanhCol].Value);
                        }
                        // KIỂM TRA ĐỊNH MỨC ĐÃ ĐĂNG KÝ
                        //isExits = false;
                        //foreach (GC_DinhMuc item in DMCollection)
                        //{
                        //    if (!isCheck)
                        //    {
                        //        if (item.MaSanPham == dinhMuc.MaSanPham)
                        //        {
                        //            isExits = true;
                        //            break;
                        //        }   
                        //    }
                        //}
                        //if (!isExits)
                        //{
                        //    errorDinhMucExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaSanPham + "]\n";
                        //    isAdd = false;
                        //    isCheck = true;
                        //    break;
                        //}
                        if (!isExits)
                        {
                            // KIỂM TRA ĐỊNH MỨC ĐÃ ĐƯỢC NHẬP LIỆU TRƯỚC ĐÓ
                            //foreach (KDT_GC_DinhMucDangKy item in DMDKCollection)
                            //{
                            //    if (!isCheck)
                            //    {
                            //        item.DinhMucCollection = KDT_GC_DinhMuc.SelectCollectionBy_Master_ID(item.ID);
                            //        foreach (KDT_GC_DinhMuc items in item.DinhMucCollection)
                            //        {
                            //            if (chkOverwrite.Checked)
                            //            {
                            //                if (items.MaSanPham == dinhMuc.MaSanPham)
                            //                {
                            //                    isExits = true;
                            //                    break;
                            //                }
                            //            }
                            //            else
                            //            {
                            //                if (items.MaSanPham == dinhMuc.MaSanPham && items.Master_ID != dmDangKy.ID)
                            //                {
                            //                    isExits = true;
                            //                    break;
                            //                }
                            //            }
                            //        }
                            //        if (!isExits)
                            //        {
                            //            errorDinhMuc += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaSanPham + "]-[" + item.SoTiepNhan + "]-[" + item.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + item.TrangThaiXuLy.ToString() + "]\n";
                            //            isAdd = false;
                            //            isCheck = true;
                            //            break;
                            //        }
                            //    }

                            //}
                        }
                        if (chkOverwrite.Checked)
                        {
                            isExits = false;
                            foreach (Company.GC.BLL.KDT.GC.DinhMuc item in dmDangKy.DMCollection)
                            {
                                if (dinhMuc.MaSanPham.Trim().ToUpper() == item.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == item.MaNguyenPhuLieu.Trim().ToUpper())
                                {
                                    if (isAdd)
                                    {
                                        dmDangKy.DMCollection.Remove(item);
                                        item.Delete(HD.ID);
                                        dmDangKy.DMCollection.Add(dinhMuc);
                                        isExits = true;
                                        break;   
                                    }
                                }
                            }
                            if (isAdd && !isExits)
                                DMCollectionAdd.Add(dinhMuc);

                        }
                        else
                        {
                            isExits = false;
                            foreach (Company.GC.BLL.KDT.GC.DinhMuc item in dmDangKy.DMCollection)
                            {
                                if (dinhMuc.MaSanPham.Trim().ToUpper() == item.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == item.MaNguyenPhuLieu.Trim().ToUpper())
                                {
                                    isExits = true;
                                    break;
                                }
                            }
                            if (!isExits && isAdd)
                                DMCollectionAdd.Add(dinhMuc);
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaSanPham))
                errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorMaSanPham + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorMaSanPhamExits))
                errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorMaSanPhamExits + " CHƯA ĐƯỢC ĐĂNG KÝ";
            if (!String.IsNullOrEmpty(errorMaNguyenPhuLieu))
                errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorMaNguyenPhuLieu + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorMaNguyenPhuLieuExits))
                errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorMaNguyenPhuLieuExits + " CHƯA ĐƯỢC ĐĂNG KÝ";
            if (!String.IsNullOrEmpty(errorDMSD))
                errorTotal += "\n - [STT] -[ĐỊNH MỨC SỬ DỤNG] : \n" + errorDMSD + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorDMSDExits))
                errorTotal += "\n - [STT] -[ĐỊNH MỨC SỬ DỤNG] : \n" + errorDMSDExits + " CHỈ ĐƯỢC NHẬP TỐI ĐA 8 SỐ THẬP PHÂN (VÍ DỤ : ****,12345678)";
            if (!String.IsNullOrEmpty(errorTLHH))
                errorTotal += "\n - [STT] -[TỶ LỆ HAO HỤT] : " + errorTLHH + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorTLHHExits))
                errorTotal += "\n - [STT] -[TỶ LỆ HAO HỤT] : \n" + errorTLHHExits + " CHỈ ĐƯỢC NHẬP TỐI ĐA 1 SỐ THẬP PHÂN (VÍ DỤ : ****,1)";
            if (!String.IsNullOrEmpty(errorMaDinhDanh))
                errorTotal += "\n - [STT] -[MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT] : \n" + errorMaDinhDanh + " KHÔNG ĐƯỢC ĐỂ TRỐNG ";
            if (!String.IsNullOrEmpty(errorDinhMuc))
                errorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + errorDinhMuc + " ĐÃ ĐƯỢC NHẬP LIỆU ĐỊNH MỨC TRƯỚC ĐÓ .";
            if (!String.IsNullOrEmpty(errorDinhMucExits))
                errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorDinhMucExits + " ĐÃ ĐƯỢC KHAI BÁO ĐỊNH MỨC";
            if (!String.IsNullOrEmpty(errorNPL))
                errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorNPL + " CHƯA ĐƯỢC ĐĂNG KÝ";
            if (!String.IsNullOrEmpty(errorSP))
                errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSP + " CHƯA ĐƯỢC ĐĂNG KÝ";

            if (String.IsNullOrEmpty(errorTotal))
            {
                foreach (Company.GC.BLL.KDT.GC.DinhMuc DM in DMCollectionAdd)
                {
                    dmDangKy.DMCollection.Add(DM);
                }
                ShowMessageTQDT("NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
            }
            else
            {
                ShowMessageTQDT("NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                return;
            }

            this.Close();
            this.Cursor = Cursors.Default;
            this.Close();
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {

        }

        private void lblLinkExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GC("DM");
            }
            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}