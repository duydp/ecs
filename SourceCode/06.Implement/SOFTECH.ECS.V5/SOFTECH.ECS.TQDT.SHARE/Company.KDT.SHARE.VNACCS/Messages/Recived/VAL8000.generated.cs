using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAL8000 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ADD { get; set; }
public PropertiesAttribute AD2 { get; set; }
public PropertiesAttribute AD3 { get; set; }
public PropertiesAttribute A03 { get; set; }
public PropertiesAttribute A02 { get; set; }
public PropertiesAttribute AD1 { get; set; }
public PropertiesAttribute A00 { get; set; }
public PropertiesAttribute A01 { get; set; }
public PropertiesAttribute ADG { get; set; }
public PropertiesAttribute A27 { get; set; }
public PropertiesAttribute ADQ { get; set; }
public PropertiesAttribute A29 { get; set; }
public PropertiesAttribute ADE { get; set; }
public PropertiesAttribute CTO { get; set; }
public PropertiesAttribute A30 { get; set; }
public PropertiesAttribute ADR { get; set; }
public PropertiesAttribute A04 { get; set; }
public PropertiesAttribute A05 { get; set; }
public PropertiesAttribute A06 { get; set; }
public PropertiesAttribute A07 { get; set; }
public PropertiesAttribute A11 { get; set; }
public PropertiesAttribute A12 { get; set; }
public PropertiesAttribute A13 { get; set; }
public PropertiesAttribute A14 { get; set; }
public PropertiesAttribute A15 { get; set; }
public PropertiesAttribute A16 { get; set; }
public PropertiesAttribute ADL { get; set; }
public PropertiesAttribute ADS { get; set; }
public PropertiesAttribute ADT { get; set; }
public PropertiesAttribute ADU { get; set; }
public PropertiesAttribute A20 { get; set; }
public PropertiesAttribute ADH { get; set; }
public PropertiesAttribute ADI { get; set; }
public PropertiesAttribute ADJ { get; set; }
public PropertiesAttribute ADK { get; set; }
public PropertiesAttribute A21 { get; set; }
public PropertiesAttribute A22 { get; set; }
public PropertiesAttribute ADB { get; set; }
public PropertiesAttribute A23 { get; set; }
public PropertiesAttribute ADN { get; set; }
public GroupAttribute KA1 { get; set; }
public PropertiesAttribute A24 { get; set; }
public PropertiesAttribute A25 { get; set; }
public PropertiesAttribute ADO { get; set; }
public PropertiesAttribute ADF { get; set; }
public PropertiesAttribute ADP { get; set; }

public VAL8000()
        {
ADD = new PropertiesAttribute(12, typeof(int));
AD2 = new PropertiesAttribute(8, typeof(DateTime));
AD3 = new PropertiesAttribute(6, typeof(int));
A03 = new PropertiesAttribute(12, typeof(int));
A02 = new PropertiesAttribute(8, typeof(DateTime));
AD1 = new PropertiesAttribute(6, typeof(int));
A00 = new PropertiesAttribute(10, typeof(string));
A01 = new PropertiesAttribute(2, typeof(string));
ADG = new PropertiesAttribute(1, typeof(string));
A27 = new PropertiesAttribute(12, typeof(int));
ADQ = new PropertiesAttribute(3, typeof(string));
A29 = new PropertiesAttribute(8, typeof(DateTime));
ADE = new PropertiesAttribute(1, typeof(string));
CTO = new PropertiesAttribute(1, typeof(string));
A30 = new PropertiesAttribute(8, typeof(DateTime));
ADR = new PropertiesAttribute(8, typeof(DateTime));
A04 = new PropertiesAttribute(13, typeof(string));
A05 = new PropertiesAttribute(300, typeof(string));
A06 = new PropertiesAttribute(7, typeof(string));
A07 = new PropertiesAttribute(300, typeof(string));
A11 = new PropertiesAttribute(20, typeof(string));
A12 = new PropertiesAttribute(5, typeof(string));
A13 = new PropertiesAttribute(50, typeof(string));
A14 = new PropertiesAttribute(5, typeof(string));
A15 = new PropertiesAttribute(1, typeof(string));
A16 = new PropertiesAttribute(1, typeof(string));
ADL = new PropertiesAttribute(1, typeof(string));
ADS = new PropertiesAttribute(8, typeof(DateTime));
ADT = new PropertiesAttribute(3, typeof(int));
ADU = new PropertiesAttribute(3, typeof(int));
A20 = new PropertiesAttribute(20, typeof(string));
ADH = new PropertiesAttribute(3, typeof(string));
ADI = new PropertiesAttribute(9, typeof(int));
ADJ = new PropertiesAttribute(300, typeof(string));
ADK = new PropertiesAttribute(3, typeof(string));
A21 = new PropertiesAttribute(9, typeof(int));
A22 = new PropertiesAttribute(300, typeof(string));
ADB = new PropertiesAttribute(1, typeof(string));
A23 = new PropertiesAttribute(11, typeof(int));
ADN = new PropertiesAttribute(3, typeof(string));
A24 = new PropertiesAttribute(2, typeof(int));
A25 = new PropertiesAttribute(2, typeof(int));
ADO = new PropertiesAttribute(600, typeof(string));
ADF = new PropertiesAttribute(108, typeof(string));
ADP = new PropertiesAttribute(108, typeof(string));
#region KA1
List<PropertiesAttribute> listKA1 = new List<PropertiesAttribute>();
listKA1.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAL8000_KA1, typeof(string)));
listKA1.Add(new PropertiesAttribute(27, 5, EnumGroupID.VAL8000_KB1, typeof(string)));
listKA1.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAL8000_KU1, typeof(string)));
listKA1.Add(new PropertiesAttribute(11, 5, EnumGroupID.VAL8000_KC1, typeof(int)));
listKA1.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAL8000_KW1, typeof(string)));
KA1 = new GroupAttribute("KA1", 5, listKA1);
#endregion KA1
TongSoByte = 2657;
}

}public partial class EnumGroupID
    {
public static readonly string VAL8000_KA1 = "VAL8000_KA1";
public static readonly string VAL8000_KB1 = "VAL8000_KB1";
public static readonly string VAL8000_KU1 = "VAL8000_KU1";
public static readonly string VAL8000_KC1 = "VAL8000_KC1";
public static readonly string VAL8000_KW1 = "VAL8000_KW1";
}

}
