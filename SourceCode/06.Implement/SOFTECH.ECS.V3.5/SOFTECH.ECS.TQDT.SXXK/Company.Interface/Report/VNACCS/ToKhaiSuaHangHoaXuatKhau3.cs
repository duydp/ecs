using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
namespace Company.Interface.Report.VNACCS
{
    public partial class ToKhaiSuaHangHoaXuatKhau3 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiSuaHangHoaXuatKhau3()
        {
            InitializeComponent();
        }
        public void BindReport(VAE2LE0 Vae)
        {
            //lblSoTrang.Text = Vae.K69.GetValue().ToString();
            int HangHoa = Vae.HangMD.Count;
            if (HangHoa <= 2)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblSotokhai.Text = Vae.ECN.GetValue().ToString();
            lblSotokhaidautien.Text = Vae.FIC.GetValue().ToString();
            lblSonhanhtokhaichianho.Text = Vae.BNO.GetValue().ToString();
            lblTongsotokhaichianho.Text = Vae.DNO.GetValue().ToString();
            lblSotokhaitamnhaptaixuat.Text = Vae.TDN.GetValue().ToString();
            lblMaphanloaikiemtra.Text = Vae.K07.GetValue().ToString();
            lblMaloaihinh.Text = Vae.ECB.GetValue().ToString();
            lblMaphanloaihanghoa.Text = Vae.CCC.GetValue().ToString();
            lblMahieuphuongthucvanchuyen.Text = Vae.MTC.GetValue().ToString();
            lblMasothuedaidien.Text = Vae.K01.GetValue().ToString();
            lblTencoquanhaiquantiepnhantokhai.Text = Vae.K08.GetValue().ToString();
            lblNhomxulyhoso.Text = Vae.CHB.GetValue().ToString();
            if
            (Convert.ToDateTime(Vae.K10.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydangky.Text = Convert.ToDateTime(Vae.K10.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydangky.Text = "";
            }

            if (Vae.AD1.GetValue().ToString() != "" && Vae.AD1.GetValue().ToString() != "0")
            {
                lblGiodangky.Text = Vae.AD1.GetValue().ToString().Substring(0, 2) + ":" + Vae.AD1.GetValue().ToString().Substring(2, 2) + ":" + Vae.AD1.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiodangky.Text = "";
            }

            if
            (Convert.ToDateTime(Vae.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaythaydoidangky.Text = Convert.ToDateTime(Vae.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaythaydoidangky.Text = "";
            }
            if (Vae.AD3.GetValue().ToString() != "" && Vae.AD3.GetValue().ToString() != "0")
            {
                lblGiothaydoidangky.Text = Vae.AD3.GetValue().ToString().Substring(0, 2) + ":" + Vae.AD3.GetValue().ToString().Substring(2, 2) + ":" + Vae.AD3.GetValue().ToString().Substring(4, 2);
            }

            else
            {
                lblGiothaydoidangky.Text = "";
            }



            if
            (Convert.ToDateTime(Vae.RID.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoihantainhapxuat.Text = Convert.ToDateTime(Vae.RID.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoihantainhapxuat.Text = "";
            }

            lblBieuthitruonghophethan.Text = Vae.AAA.GetValue().ToString();

            #region bind hang
            DataTable dt = new DataTable();
            dt.Columns.Add("Sodong1", typeof(string));
            dt.Columns.Add("Masohanghoa", typeof(string));
            dt.Columns.Add("Maquanlyrieng", typeof(string));
            dt.Columns.Add("Mataixacnhangia", typeof(string));
            dt.Columns.Add("Motahanghoa", typeof(string));
            dt.Columns.Add("Soluong1", typeof(decimal));
            dt.Columns.Add("Madvtinh1", typeof(string));
            dt.Columns.Add("Soluong2", typeof(decimal));
            dt.Columns.Add("Madvtinh2", typeof(string));
            dt.Columns.Add("Trigia", typeof(decimal));
            dt.Columns.Add("Dongia", typeof(decimal));
            dt.Columns.Add("Madongtien", typeof(string));
            dt.Columns.Add("Donvidongia", typeof(string));
            dt.Columns.Add("TrigiathueS", typeof(decimal));
            dt.Columns.Add("MadongtienS", typeof(string));
            dt.Columns.Add("MadongtienM", typeof(string));
            dt.Columns.Add("TrigiathueM", typeof(decimal));
            dt.Columns.Add("Soluongthue", typeof(decimal));
            dt.Columns.Add("Madvtinhchuanthue", typeof(string));
            dt.Columns.Add("Dongiathue", typeof(decimal));
            dt.Columns.Add("Matientedongiathue", typeof(string));
            dt.Columns.Add("Donvisoluongthue", typeof(string));
            dt.Columns.Add("Thuesuat", typeof(string));
            dt.Columns.Add("Phanloainhapthue", typeof(string));
            dt.Columns.Add("Sotienthue", typeof(decimal));
            dt.Columns.Add("Matientesotienthue", typeof(string));
            dt.Columns.Add("Sotienmiengiam", typeof(decimal));
            dt.Columns.Add("Matientesotiengiamthue", typeof(string));
            dt.Columns.Add("Sodonghangtaixuat", typeof(string));
            dt.Columns.Add("Danhmucmienthue", typeof(string));
            dt.Columns.Add("Sodongmienthue", typeof(string));
            dt.Columns.Add("Tienlephi", typeof(string));
            dt.Columns.Add("Tienbaohiem", typeof(string));
            dt.Columns.Add("Soluongtienlephi", typeof(decimal));
            dt.Columns.Add("Madvtienlephi", typeof(string));
            dt.Columns.Add("Soluongtienbaohiem", typeof(decimal));
            dt.Columns.Add("Madvtienbaohiem", typeof(string));
            dt.Columns.Add("Khoantienlephi", typeof(decimal));
            dt.Columns.Add("Khoantienbaohiem", typeof(decimal));
            dt.Columns.Add("Mavanvanphapquy1", typeof(string));
            dt.Columns.Add("Mavanvanphapquy2", typeof(string));
            dt.Columns.Add("Mavanvanphapquy3", typeof(string));
            dt.Columns.Add("Mavanvanphapquy4", typeof(string));
            dt.Columns.Add("Mavanvanphapquy5", typeof(string));
            dt.Columns.Add("Mamiengiam", typeof(string));
            dt.Columns.Add("Dieukhoanmien", typeof(string));

            //int begin = spl * 2;
            //int end = spl * 2 + 2;
            //for (int i = begin; i < end; i++)
            for (int i = 0; i < Vae.HangMD.Count; i++)
            {
                //if (i < Vae.HangMD.Count)
                //{
                DataRow dr = dt.NewRow();

                dr["Sodong1"] = Vae.HangMD[i].R01.GetValue().ToString();
                dr["Masohanghoa"] = Vae.HangMD[i].CMD.GetValue().ToString();
                dr["Maquanlyrieng"] = Vae.HangMD[i].COC.GetValue().ToString();
                dr["Mataixacnhangia"] = Vae.HangMD[i].R03.GetValue().ToString();
                dr["Motahanghoa"] = Vae.HangMD[i].CMN.GetValue().ToString();
                dr["Soluong1"] = Vae.HangMD[i].QN1.GetValue().ToString().Replace(".", ",");
                dr["Madvtinh1"] = Vae.HangMD[i].QT1.GetValue().ToString();
                dr["Soluong2"] = Vae.HangMD[i].QN2.GetValue().ToString().Replace(".", ",");
                dr["Madvtinh2"] = Vae.HangMD[i].QT2.GetValue().ToString();
                dr["Trigia"] = Vae.HangMD[i].BPR.GetValue().ToString().Replace(".", ",");
                dr["Dongia"] = Vae.HangMD[i].UPR.GetValue().ToString().Replace(".", ",");
                dr["Madongtien"] = Vae.HangMD[i].UPC.GetValue().ToString();
                dr["Donvidongia"] = Vae.HangMD[i].TSC.GetValue().ToString();
                dr["TrigiathueS"] = Vae.HangMD[i].R07.GetValue().ToString().Replace(".", ",");
                dr["MadongtienS"] = Vae.HangMD[i].AD9.GetValue().ToString();
                dr["MadongtienM"] = Vae.HangMD[i].R14.GetValue().ToString();
                dr["TrigiathueM"] = Vae.HangMD[i].R15.GetValue().ToString().Replace(".", ",");
                dr["Soluongthue"] = Vae.HangMD[i].TSQ.GetValue().ToString().Replace(".", ",");
                dr["Madvtinhchuanthue"] = Vae.HangMD[i].TSU.GetValue().ToString();
                dr["Dongiathue"] = Vae.HangMD[i].CVU.GetValue().ToString().Replace(".", ",");
                dr["Matientedongiathue"] = Vae.HangMD[i].ADA.GetValue().ToString();
                dr["Donvisoluongthue"] = Vae.HangMD[i].QCV.GetValue().ToString();
                dr["Thuesuat"] = Vae.HangMD[i].TRA.GetValue().ToString();
                dr["Phanloainhapthue"] = Vae.HangMD[i].TRM.GetValue().ToString();
                dr["Sotienthue"] = Vae.HangMD[i].TAX.GetValue().ToString().Replace(".", ",");
                dr["Matientesotienthue"] = Vae.HangMD[i].ADB.GetValue().ToString();
                dr["Sotienmiengiam"] = Vae.HangMD[i].REG.GetValue().ToString().Replace(".", ",");
                dr["Matientesotiengiamthue"] = Vae.HangMD[i].ADC.GetValue().ToString();
                dr["Sodonghangtaixuat"] = Vae.HangMD[i].TDL.GetValue().ToString();
                dr["Danhmucmienthue"] = Vae.HangMD[i].TXN.GetValue().ToString();
                dr["Sodongmienthue"] = Vae.HangMD[i].TXR.GetValue().ToString();
                dr["Tienlephi"] = Vae.HangMD[i].CUP.GetValue().ToString();
                dr["Tienbaohiem"] = Vae.HangMD[i].IUP.GetValue().ToString();
                dr["Soluongtienlephi"] = Vae.HangMD[i].CQU.GetValue().ToString().Replace(".", ",");
                dr["Madvtienlephi"] = Vae.HangMD[i].CQC.GetValue().ToString();
                dr["Soluongtienbaohiem"] = Vae.HangMD[i].IQU.GetValue().ToString().Replace(".", ",");
                dr["Madvtienbaohiem"] = Vae.HangMD[i].IQC.GetValue().ToString();
                dr["Khoantienlephi"] = Vae.HangMD[i].CPR.GetValue().ToString().Replace(".", ",");
                dr["Khoantienbaohiem"] = Vae.HangMD[i].IPR.GetValue().ToString().Replace(".", ",");
                dr["Mavanvanphapquy1"] = Vae.HangMD[i].OL_.listAttribute[0].GetValueCollection(0).ToString();
                dr["Mavanvanphapquy2"] = Vae.HangMD[i].OL_.listAttribute[0].GetValueCollection(1).ToString();
                dr["Mavanvanphapquy3"] = Vae.HangMD[i].OL_.listAttribute[0].GetValueCollection(2).ToString();
                dr["Mavanvanphapquy4"] = Vae.HangMD[i].OL_.listAttribute[0].GetValueCollection(3).ToString();
                dr["Mavanvanphapquy5"] = Vae.HangMD[i].OL_.listAttribute[0].GetValueCollection(4).ToString();
                dr["Mamiengiam"] = Vae.HangMD[i].RE.GetValue().ToString();
                dr["Dieukhoanmien"] = Vae.HangMD[i].TRL.GetValue().ToString();
                dt.Rows.Add(dr);
                //}


            }
            BindReportHang(dt);
            #endregion
        }
        public void BindReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSodong1.DataBindings.Add("Text", DetailReport.DataSource, "Sodong1");
            lblMasohanghoa.DataBindings.Add("Text", DetailReport.DataSource, "Masohanghoa");
            lblMaquanlyrieng.DataBindings.Add("Text", DetailReport.DataSource, "Maquanlyrieng");
            lblMataixacnhangia.DataBindings.Add("Text", DetailReport.DataSource, "Mataixacnhangia");
            lblMotahanghoa.DataBindings.Add("Text", DetailReport.DataSource, "Motahanghoa");
            lblSoluong1.DataBindings.Add("Text", DetailReport.DataSource, "Soluong1", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            lblMadvtinh1.DataBindings.Add("Text", DetailReport.DataSource, "Madvtinh1");
            lblSoluong2.DataBindings.Add("Text", DetailReport.DataSource, "Soluong2", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            lblMadvtinh2.DataBindings.Add("Text", DetailReport.DataSource, "Madvtinh2");
            lblTrigia.DataBindings.Add("Text", DetailReport.DataSource, "Trigia", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblDongia.DataBindings.Add("Text", DetailReport.DataSource, "Dongia", Company.KDT.SHARE.Components.Globals.FormatNumber(6, false));
            lblMadongtien.DataBindings.Add("Text", DetailReport.DataSource, "Madongtien");
            lblDonvidongia.DataBindings.Add("Text", DetailReport.DataSource, "Donvidongia");
            lblTrigiathueS.DataBindings.Add("Text", DetailReport.DataSource, "TrigiathueS", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMadongtienS.DataBindings.Add("Text", DetailReport.DataSource, "MadongtienS");
            lblMadongtienM.DataBindings.Add("Text", DetailReport.DataSource, "MadongtienM");
            lblTrigiathueM.DataBindings.Add("Text", DetailReport.DataSource, "TrigiathueM", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoluongthue.DataBindings.Add("Text", DetailReport.DataSource, "Soluongthue", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMadvtinhchuanthue.DataBindings.Add("Text", DetailReport.DataSource, "Madvtinhchuanthue");
            lblDongiathue.DataBindings.Add("Text", DetailReport.DataSource, "Dongiathue", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMatientedongiathue.DataBindings.Add("Text", DetailReport.DataSource, "Matientedongiathue");
            lblDonvisoluongthue.DataBindings.Add("Text", DetailReport.DataSource, "Donvisoluongthue");
            lblThuesuat.DataBindings.Add("Text", DetailReport.DataSource, "Thuesuat");
            lblPhanloainhapthue.DataBindings.Add("Text", DetailReport.DataSource, "Phanloainhapthue");
            lblSotienthue.DataBindings.Add("Text", DetailReport.DataSource, "Sotienthue", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMatientesotienthue.DataBindings.Add("Text", DetailReport.DataSource, "Matientesotienthue");
            lblSotienmiengiam.DataBindings.Add("Text", DetailReport.DataSource, "Sotienmiengiam", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMatientesotiengiamthue.DataBindings.Add("Text", DetailReport.DataSource, "Matientesotiengiamthue");
            lblSodonghangtaixuat.DataBindings.Add("Text", DetailReport.DataSource, "Sodonghangtaixuat");
            lblDanhmucmienthue.DataBindings.Add("Text", DetailReport.DataSource, "Danhmucmienthue");
            lblSodongmienthue.DataBindings.Add("Text", DetailReport.DataSource, "Sodongmienthue");
            lblTienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Tienlephi");
            lblTienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Tienbaohiem");
            lblSoluongtienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Soluongtienlephi", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMadvtienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Madvtienlephi");
            lblSoluongtienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Soluongtienbaohiem", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMadvtienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Madvtienbaohiem", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblKhoantienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Khoantienlephi", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblKhoantienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Khoantienbaohiem");
            lblMavanvanphapquy1.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy1");
            lblMavanvanphapquy2.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy2");
            lblMavanvanphapquy3.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy3");
            lblMavanvanphapquy4.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy4");
            lblMavanvanphapquy5.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy5");
            lblMamiengiam.DataBindings.Add("Text", DetailReport.DataSource, "Mamiengiam");
            lblDieukhoanmien.DataBindings.Add("Text", DetailReport.DataSource, "Dieukhoanmien");

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }

        private void lblTongSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 3).ToString();
        }
    }
}
