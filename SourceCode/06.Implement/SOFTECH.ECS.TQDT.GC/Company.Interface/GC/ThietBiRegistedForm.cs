using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.GC
{
    public partial class ThietBiRegistedForm : BaseForm
    {
        public ThietBi ThietBiSelected = new ThietBi();        
        public bool isBrower = true;
        //2 hien da duyet nhung chua dieu chinh
        public int isDisplayAll = 0;//0 hien tat ca . 1 hien cac npl da duyet nhhung chua dieu chinh va bo sung . 

        public ThietBiRegistedForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            if (isDisplayAll == 0)
                dgList.DataSource = ThietBiSelected.SelectCollectionBy_HopDong_ID();
            else if (isDisplayAll == 1)
                dgList.DataSource = ThietBiSelected.SelectCollectionDaDuyetAndBoSungBy_HopDong_ID();
            else if (isDisplayAll == 2)
                dgList.DataSource = ThietBiSelected.SelectCollectionDaDuyetBy_HopDong_ID();
        }

        //-----------------------------------------------------------------------------------------
        
        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll();
            
          
            
            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void ThietBiRegistedForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            BindData(); 
        }

        //-----------------------------------------------------------------------------------------
        
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------
        
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    this.ThietBiSelected = (ThietBi)e.Row.DataRow;
                    this.Close();
                }
            }
            else
            {

            }
        }

      

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //-----------------------------------------------------------------------------------------
    }
}