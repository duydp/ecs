﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class Information
    {
        /// <summary>
        /// Ghi chú
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }
    }
}
