﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.GC
{
    public partial class FormChonSanPham : BaseForm
    {
        public HopDong HD;
        public HangPhuKienCollection HPKCollection = new HangPhuKienCollection();
        private SanPhamCollection sps;

        public FormChonSanPham()
        {
            InitializeComponent();
        }
        private bool checkHangExit(string MaHang)
        {
            foreach (HangPhuKien hPK in HPKCollection)
            {
                if (hPK.MaHang == MaHang) return true;
            }
            return false;
        }
        private void btnChonAll_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            if (items.Length < 0) return;
            if (items.Length <= 0) return;
            {
                foreach (GridEXRow i in items)
                {
                   
                    if (i.IsChecked)
                    {
                        SanPham sp = (SanPham)i.DataRow;
                        HangPhuKien hangPK = new HangPhuKien()
                        {
                            MaHang = sp.Ma,
                            TenHang = sp.Ten,
                            MaHS = sp.MaHS,
                            DVT_ID = sp.DVT_ID,
                            NhomSP = sp.NhomSanPham_ID,
                            ThongTinCu = sp.Ma
                        };
                        HPKCollection.Add(hangPK);
                    }
                }
            }
            DialogResult = DialogResult.OK;

        }

        private void FormChonSanPham_Load(object sender, EventArgs e)
        {
            SanPham sp = new SanPham();
            sp.HopDong_ID = HD.ID;
            sps = sp.SelectCollectionBy_HopDong_ID();
            dgList.DataSource = sps;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }
    }
}
