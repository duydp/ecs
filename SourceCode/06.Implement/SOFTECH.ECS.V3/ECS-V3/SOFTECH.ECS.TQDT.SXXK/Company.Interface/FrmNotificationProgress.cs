﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Net;
using System.Xml;

namespace Company.Interface
{
    public partial class FrmNotificationProgress : Form
    {
        // The progress of the download in percentage
        private static int PercentProgress;
        // The delegate which we will call from the thread to update the form
        private delegate void UpdateProgessCallback(Int32 counts, Int32 totals);
        private delegate void UpdateProgess2Callback(Int32 percents);
        private delegate void HideFormCallback(System.Windows.Forms.Form frm);
        private delegate void CloseFormCallback(System.Windows.Forms.Form frm);

        public FrmNotificationProgress()
        {
            InitializeComponent();
        }

        private void FrmUpdate1_Load(object sender, EventArgs e)
        {
            PercentProgress = 0;
            progressBar.Value = PercentProgress;
        }

        public void UpdateProgress(Int32 counts, Int32 totals)
        {
            try
            {
                if (progressBar.InvokeRequired)
                {
                    UpdateProgessCallback d = new UpdateProgessCallback(UpdateProgress);
                    this.Invoke(d, new object[] { counts, totals });
                }
                else
                {
                    // Calculate the download progress in percentages

                    PercentProgress = Convert.ToInt32((counts * 100) / totals);

                    // Make progress on the progress bar

                    progressBar.Value = PercentProgress;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public void UpdateProgress(Int32 percents)
        {
            try
            {
                if (progressBar.InvokeRequired)
                {
                    UpdateProgess2Callback d = new UpdateProgess2Callback(UpdateProgress);
                    this.Invoke(d, new object[] { percents });
                }
                else
                {
                    // Calculate the download progress in percentages

                    PercentProgress = percents;

                    // Make progress on the progress bar

                    progressBar.Value = PercentProgress;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }


        public void HideForm(System.Windows.Forms.Form frm)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    CloseFormCallback d = new CloseFormCallback(CloseForm);
                    this.Invoke(d, new object[] { frm });
                }
                else
                {
                    frm.Opacity = 0;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public void CloseForm(System.Windows.Forms.Form frm)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    CloseFormCallback d = new CloseFormCallback(CloseForm);
                    this.Invoke(d, new object[] { frm });
                }
                else
                {
                    frm.Close();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
