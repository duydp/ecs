﻿IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WA02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép XNK, giấy phép quá cảnh'  WHERE ReferenceDB='A528' AND Code='WA02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WA02',N'Giấy phép XNK, giấy phép quá cảnh')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WY02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu (nhập khẩu) hoá chất Bảng 1  của Thủ tướng Chính phủ'  WHERE ReferenceDB='A528' AND Code='WY02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WY02',N'Giấy phép xuất khẩu (nhập khẩu) hoá chất Bảng 1  của Thủ tướng Chính phủ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WY03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu (nhập khẩu) hóa chất Bảng 2, hóa chất Bảng 3 của Bộ Công thương'  WHERE ReferenceDB='A528' AND Code='WY03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WY03',N'Giấy phép xuất khẩu (nhập khẩu) hóa chất Bảng 2, hóa chất Bảng 3 của Bộ Công thương')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WF02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy chứng nhận đủ điều kiện xk gạo; Hợp đồng được Hiệp hội lương thực Việt Nam xác nhận'  WHERE ReferenceDB='A528' AND Code='WF02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WF02',N'Giấy chứng nhận đủ điều kiện xk gạo; Hợp đồng được Hiệp hội lương thực Việt Nam xác nhận')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WH02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy khai báo hóa chất'  WHERE ReferenceDB='A528' AND Code='WH02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WH02',N'Giấy khai báo hóa chất')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WH03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Phiếu kiểm soát mua, bán hóa chất độc'  WHERE ReferenceDB='A528' AND Code='WH03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WH03',N'Phiếu kiểm soát mua, bán hóa chất độc')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WL02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy xác nhận của Kiểm Lâm hoặc của UBND nơi có cây trồng; Bảng kê lâm sản
Hàng nhập khẩu có xác nhận của Cites
Kiểm dịch thực vật'  WHERE ReferenceDB='A528' AND Code='WL02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WL02',N'Giấy xác nhận của Kiểm Lâm hoặc của UBND nơi có cây trồng; Bảng kê lâm sản
Hàng nhập khẩu có xác nhận của Cites
Kiểm dịch thực vật')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WM02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy chứng nhận nhà nhập khẩu, ủy quyền, được chỉ định nhập khẩu
Giấy đăng ký kiểm tra VSATTP
Thông báo kêt quả chất lượng'  WHERE ReferenceDB='A528' AND Code='WM02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WM02',N'Giấy chứng nhận nhà nhập khẩu, ủy quyền, được chỉ định nhập khẩu
Giấy đăng ký kiểm tra VSATTP
Thông báo kêt quả chất lượng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WQ02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy xác nhận được phép NK của Bộ Công an; Bộ Y tế'  WHERE ReferenceDB='A528' AND Code='WQ02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WQ02',N'Giấy xác nhận được phép NK của Bộ Công an; Bộ Y tế')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WQ03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy xác nhận được phép NK của Bộ Công an; Bộ Y tế'  WHERE ReferenceDB='A528' AND Code='WQ03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WQ03',N'Giấy xác nhận được phép NK của Bộ Công an; Bộ Y tế')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='         WQ04')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép của Bộ Công thương'  WHERE ReferenceDB='A528' AND Code='         WQ04'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WQ04',N'Giấy phép của Bộ Công thương')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WR02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy xác nhận của Bộ KHCN, Bộ Kế hoạch đầu tư'  WHERE ReferenceDB='A528' AND Code='WR02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WR02',N'Giấy xác nhận của Bộ KHCN, Bộ Kế hoạch đầu tư')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='WS02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy chứng nhận hợp quy 
Giấy đăng ký đúng ngành nghề
Thông báo kết quả kiểm tra đạt chất lượng
'  WHERE ReferenceDB='A528' AND Code='WS02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','WS02',N'Giấy chứng nhận hợp quy 
Giấy đăng ký đúng ngành nghề
Thông báo kết quả kiểm tra đạt chất lượng
')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AA02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'
Trường hợp xuất khẩu, nhập khẩu hoá chất thuộc Danh mục hoá chất cấm xuất khẩu, cấm nhập khẩu trong những trường hợp đặc biệt cho mục đích nghiên cứu, y tế, dược phẩm hoặc bảo vệ thực hiện theo quy định tại Điều 5 Nghị định số 100/2005/NĐ-CP, phải có các giấy tờ:
- Giấy chứng nhận đăng ký kinh doanh hành nghề hoá chất do cơ quan nhà nước có thẩm quyền cấp và chứng nhận đăng ký mã số xuất, nhập khẩu ghi trên Giấy chứng nhận đăng ký thuế do cơ quan thuế cấp;
-Giấy chứng nhận đủ điều kiện kinh doanh hàng hoá là hoá chất độc hại và sản phẩm có hoá chất độc hại do Sở Khoa học và Công nghệ tỉnh, thành phố trực thuộc Trung ương cấp theo quy định của Bộ Khoa học và Công nghệ;
-  Giấy phép của Bộ Công nghiệp cấp cho doanh nghiệp đối với từng lần xuất khẩu hoặc nhập khẩu.

'  WHERE ReferenceDB='A528' AND Code='AA02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AA02',N'
Trường hợp xuất khẩu, nhập khẩu hoá chất thuộc Danh mục hoá chất cấm xuất khẩu, cấm nhập khẩu trong những trường hợp đặc biệt cho mục đích nghiên cứu, y tế, dược phẩm hoặc bảo vệ thực hiện theo quy định tại Điều 5 Nghị định số 100/2005/NĐ-CP, phải có các giấy tờ:
- Giấy chứng nhận đăng ký kinh doanh hành nghề hoá chất do cơ quan nhà nước có thẩm quyền cấp và chứng nhận đăng ký mã số xuất, nhập khẩu ghi trên Giấy chứng nhận đăng ký thuế do cơ quan thuế cấp;
-Giấy chứng nhận đủ điều kiện kinh doanh hàng hoá là hoá chất độc hại và sản phẩm có hoá chất độc hại do Sở Khoa học và Công nghệ tỉnh, thành phố trực thuộc Trung ương cấp theo quy định của Bộ Khoa học và Công nghệ;
-  Giấy phép của Bộ Công nghiệp cấp cho doanh nghiệp đối với từng lần xuất khẩu hoặc nhập khẩu.

')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AC02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'TNTX tinh dầu xá xị: Giấy phép của Bộ Công Thương '  WHERE ReferenceDB='A528' AND Code='AC02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AC02',N'TNTX tinh dầu xá xị: Giấy phép của Bộ Công Thương ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AD02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu của Bộ Công nghiệp (Bộ Công Thương)'  WHERE ReferenceDB='A528' AND Code='AD02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AD02',N'Giấy phép nhập khẩu của Bộ Công nghiệp (Bộ Công Thương)')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AD03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Bản kê khai tiêu chuẩn chất lượng và tiêu chuẩn kỹ thuật tương ứng để cơ quan HQ kiểm tra đối chiếu. Việc NK hoá chất có tiêu chuẩn thấp hơn phải có ý kiến đồng ý bằng văn bản của Bộ CT'  WHERE ReferenceDB='A528' AND Code='AD03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AD03',N'Bản kê khai tiêu chuẩn chất lượng và tiêu chuẩn kỹ thuật tương ứng để cơ quan HQ kiểm tra đối chiếu. Việc NK hoá chất có tiêu chuẩn thấp hơn phải có ý kiến đồng ý bằng văn bản của Bộ CT')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AF02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu tự động'  WHERE ReferenceDB='A528' AND Code='AF02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AF02',N'Giấy phép nhập khẩu tự động')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code=N'AG01AG02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu, giấy phép nhập khẩu'  WHERE ReferenceDB='A528' AND Code='AG01 hoặc AG02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528',N'AG01AG02',N'Giấy phép xuất khẩu, giấy phép nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AJ02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu tự động của Bộ Công Thương'  WHERE ReferenceDB='A528' AND Code='AJ02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AJ02',N'Giấy phép nhập khẩu tự động của Bộ Công Thương')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code=N'AG01AG02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu, giấy phép nhập khẩu'  WHERE ReferenceDB='A528' AND Code=N'AG01 hoặc AG02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528',N'AG01AG02',N'Giấy phép xuất khẩu, giấy phép nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AQ02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Phiếu phân tích mẫu để xác nhận sự phù hợp về tiêu chuẩn, chất lượng của lô than xuất khẩu, do một phòng thử nghiệm đạt tiêu chuẩn VILAS cấp.Hồ sơ chứng minh nguồn gốc hợp pháp của than xuất khẩu'  WHERE ReferenceDB='A528' AND Code='AQ02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AQ02',N'Phiếu phân tích mẫu để xác nhận sự phù hợp về tiêu chuẩn, chất lượng của lô than xuất khẩu, do một phòng thử nghiệm đạt tiêu chuẩn VILAS cấp.
Hồ sơ chứng minh nguồn gốc hợp pháp của than xuất khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AR02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu tự động'  WHERE ReferenceDB='A528' AND Code='AR02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AR02',N'Giấy phép nhập khẩu tự động')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AS02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'TNTX tinh dầu xá xị: Giấy phép của Bộ Công Thương '  WHERE ReferenceDB='A528' AND Code='AS02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AS02',N'TNTX tinh dầu xá xị: Giấy phép của Bộ Công Thương ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AS03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu của Bộ Công thương'  WHERE ReferenceDB='A528' AND Code='AS03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AS03',N'Giấy phép nhập khẩu của Bộ Công thương')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AS04')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Trường hợp nhập khẩu phục vụ nghiên cứu khoa học, viện trợ nhân đạo: phải có văn bản của Bộ Công Thương'  WHERE ReferenceDB='A528' AND Code='AS04'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AS04',N'Trường hợp nhập khẩu phục vụ nghiên cứu khoa học, viện trợ nhân đạo: phải có văn bản của Bộ Công Thương')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AS05')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép của Bộ Công Thương
Đối với trường hợp nhập khẩu hàng hóa để phục vụ cho mục đích an ninh, quốc phòng, việc nhập khẩu thực hiện theo quy định của Bộ Công an, Bộ Quốc phòng.'  WHERE ReferenceDB='A528' AND Code='AS05'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AS05',N'Giấy phép của Bộ Công Thương
Đối với trường hợp nhập khẩu hàng hóa để phục vụ cho mục đích an ninh, quốc phòng, việc nhập khẩu thực hiện theo quy định của Bộ Công an, Bộ Quốc phòng.')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AT02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép tạm nhập tái xuất do Bộ Công Thương cấp'  WHERE ReferenceDB='A528' AND Code='AT02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AT02',N'Giấy phép tạm nhập tái xuất do Bộ Công Thương cấp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AT03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép tạm nhập tái xuất do Bộ Công Thương cấp'  WHERE ReferenceDB='A528' AND Code='AT03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AT03',N'Giấy phép tạm nhập tái xuất do Bộ Công Thương cấp')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='AV02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'-Hàng hóa nhập khẩu phải có Giấy chứng nhận xuất xứ hàng hóa mẫu S (C/O form S) do Bộ Thương mại Vương quốc Campuchia hoặc cơ quan được ủy quyền cấp theo quy định và được thông quan qua các cặp cửa khẩu nêu tại Phụ lục số 02 kèm theo Thông tư này
-Thương nhân Việt Nam được nhập khẩu mặt hàng thóc, gạo các loại theo hạn ngạch thuế quan.
- Đối với lá thuốc lá khô, chỉ những thương nhân có giấy phép nhập khẩu thuốc lá nguyên liệu theo hạn ngạch thuế quan do Bộ Công Thương cấp theo quy định tại Thông tư số 04/2006/TT-BTM ngày 06 tháng 4 năm 2006 của Bộ Thương mại (nay là Bộ Công Thương) .'  WHERE ReferenceDB='A528' AND Code='AV02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','AV02',N'-Hàng hóa nhập khẩu phải có Giấy chứng nhận xuất xứ hàng hóa mẫu S (C/O form S) do Bộ Thương mại Vương quốc Campuchia hoặc cơ quan được ủy quyền cấp theo quy định và được thông quan qua các cặp cửa khẩu nêu tại Phụ lục số 02 kèm theo Thông tư này
-Thương nhân Việt Nam được nhập khẩu mặt hàng thóc, gạo các loại theo hạn ngạch thuế quan.
- Đối với lá thuốc lá khô, chỉ những thương nhân có giấy phép nhập khẩu thuốc lá nguyên liệu theo hạn ngạch thuế quan do Bộ Công Thương cấp theo quy định tại Thông tư số 04/2006/TT-BTM ngày 06 tháng 4 năm 2006 của Bộ Thương mại (nay là Bộ Công Thương) .')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='BC02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'-Thông báo thực phẩm đạt yêu cầu nhập khẩu
hoặc Thông báo thực phẩm chỉ kiểm tra hồ sơ
'  WHERE ReferenceDB='A528' AND Code='BC02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','BC02',N'-Thông báo thực phẩm đạt yêu cầu nhập khẩu
hoặc Thông báo thực phẩm chỉ kiểm tra hồ sơ
')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='BD02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu/Giấy phép nhập khẩu của Bộ Công Thương'  WHERE ReferenceDB='A528' AND Code='BD02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','BD02',N'Giấy phép xuất khẩu/Giấy phép nhập khẩu của Bộ Công Thương')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='BF02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu tự động đối với một số mặt hàng phân bón thuộc Danh mục quy định tại Phụ lục I '  WHERE ReferenceDB='A528' AND Code='BF02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','BF02',N'Giấy phép nhập khẩu tự động đối với một số mặt hàng phân bón thuộc Danh mục quy định tại Phụ lục I ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='BN02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Kết quả giám định hàm lượng Formaldehyt'  WHERE ReferenceDB='A528' AND Code='BN02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','BN02',N'Kết quả giám định hàm lượng Formaldehyt')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='DS02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập khẩu các mặt hàng nuôi trồng thủy sản ngoài Danh mục thức ăn chăn nuôi thủy sản được phép lưu hành tại Việt Nam và Danh mục sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam do Bộ Nông nghiệp và Phát triển nông thôn ban hành phải có giấy phép của Tổng cục Thủy sản'  WHERE ReferenceDB='A528' AND Code='DS02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','DS02',N'Nhập khẩu các mặt hàng nuôi trồng thủy sản ngoài Danh mục thức ăn chăn nuôi thủy sản được phép lưu hành tại Việt Nam và Danh mục sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam do Bộ Nông nghiệp và Phát triển nông thôn ban hành phải có giấy phép của Tổng cục Thủy sản')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='DS03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Các loài thủy sản có tên trong Danh mục các loài thuỷ sản xuất khẩu có điều kiện chỉ được xuất khẩu khi đáp ứng đủ các điều kiện nêu tại Phụ lục 02 Thông tư 88/2011/TT-BNNPTNT ngày 28/12/2011, không phải xin phép.'  WHERE ReferenceDB='A528' AND Code='DS03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','DS03',N'Các loài thủy sản có tên trong Danh mục các loài thuỷ sản xuất khẩu có điều kiện chỉ được xuất khẩu khi đáp ứng đủ các điều kiện nêu tại Phụ lục 02 Thông tư 88/2011/TT-BNNPTNT ngày 28/12/2011, không phải xin phép.')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='DX02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Phụ lục 3. Danh mục hoá chất, kháng sinh hạn chế sử dụng trong sản xuất, kinh doanh thuỷ sản
Nhập khẩu thức ăn chăn nuôi ngoài Danh mục thức ăn chăn nuôi thủy sản phải có giấy phép của Tổng cục Thủy sản (đối với thức ăn thủy sản); Nhập khẩu sản phẩm xử lý, cải tạo môi trường nuôi trồng thủy sản ngoài Danh mục sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam phải xin phép Tổng cục Thủy sản'  WHERE ReferenceDB='A528' AND Code='DX02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','DX02',N'Phụ lục 3. Danh mục hoá chất, kháng sinh hạn chế sử dụng trong sản xuất, kinh doanh thuỷ sản
Nhập khẩu thức ăn chăn nuôi ngoài Danh mục thức ăn chăn nuôi thủy sản phải có giấy phép của Tổng cục Thủy sản (đối với thức ăn thủy sản); Nhập khẩu sản phẩm xử lý, cải tạo môi trường nuôi trồng thủy sản ngoài Danh mục sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam phải xin phép Tổng cục Thủy sản')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='DX03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Phụ lục 4. Danh mục thuốc, hoá chất, kháng sinh hạn chế sử dụng trong thú y . Thuốc thú y, nguyên liệu làm thuốc thú y chưa có Giấy chứng nhận lưu hành tại Việt Nam khi nhập khẩu phải được Cục Thú y cấp phép.
Thuốc thú y, nguyên liệu làm thuốc thú y chưa có Giấy chứng nhận lưu hành tại Việt Nam khi nhập khẩu phải được Cục Thú y cấp phép'  WHERE ReferenceDB='A528' AND Code='DX03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','DX03',N'Phụ lục 4. Danh mục thuốc, hoá chất, kháng sinh hạn chế sử dụng trong thú y . Thuốc thú y, nguyên liệu làm thuốc thú y chưa có Giấy chứng nhận lưu hành tại Việt Nam khi nhập khẩu phải được Cục Thú y cấp phép.
Thuốc thú y, nguyên liệu làm thuốc thú y chưa có Giấy chứng nhận lưu hành tại Việt Nam khi nhập khẩu phải được Cục Thú y cấp phép')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='EC02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Thương nhân xuất khẩu giống cây trồng có trong Danh mục Nguồn gen cây trồng quý hiếm trao đổi quốc tế trong trường hợp đặc biệt theo quy định của Bộ Nông nghiệp và Phát triển nông thôn, phải được sự đồng ý bằng văn bản của Bộ trưởng Bộ Nông nghiệp và Phát triển nông thôn'  WHERE ReferenceDB='A528' AND Code='EC02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','EC02',N'Thương nhân xuất khẩu giống cây trồng có trong Danh mục Nguồn gen cây trồng quý hiếm trao đổi quốc tế trong trường hợp đặc biệt theo quy định của Bộ Nông nghiệp và Phát triển nông thôn, phải được sự đồng ý bằng văn bản của Bộ trưởng Bộ Nông nghiệp và Phát triển nông thôn')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='EC03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Thương nhân xuất khẩu giống cây trồng có trong Danh mục Nguồn gen cây trồng quý hiếm hạn chế trao đổi quốc tế theo quy định của Bộ Nông nghiệp và Phát triển nông thôn, phải được sự đồng ý bằng văn bản của Bộ trưởng Bộ Nông nghiệp và Phát triển nông thôn'  WHERE ReferenceDB='A528' AND Code='EC03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','EC03',N'Thương nhân xuất khẩu giống cây trồng có trong Danh mục Nguồn gen cây trồng quý hiếm hạn chế trao đổi quốc tế theo quy định của Bộ Nông nghiệp và Phát triển nông thôn, phải được sự đồng ý bằng văn bản của Bộ trưởng Bộ Nông nghiệp và Phát triển nông thôn')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='EH02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Thương nhân trao đổi với nước ngoài những giống vật nuôi quý hiếm có trong Danh mục giống vật nuôi quý hiếm cấm xuất khẩu và Danh mục nguồn gen vật nuôi quý hiếm cần bảo tồn để phục vụ nghiên cứu khoa học hoặc các mục đích đặc biệt khác do Bộ trưởng Bộ Nông nghiệp và Phát triển nông thôn quyết định'  WHERE ReferenceDB='A528' AND Code='EH02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','EH02',N'Thương nhân trao đổi với nước ngoài những giống vật nuôi quý hiếm có trong Danh mục giống vật nuôi quý hiếm cấm xuất khẩu và Danh mục nguồn gen vật nuôi quý hiếm cần bảo tồn để phục vụ nghiên cứu khoa học hoặc các mục đích đặc biệt khác do Bộ trưởng Bộ Nông nghiệp và Phát triển nông thôn quyết định')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='EK02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục những loài động vật, thực vật hoang dã mà một nước thành viên CITES yêu cầu nước thành viên khác của CITES hợp tác để kiểm soát việc xuất khẩu, nhập khẩu, tái xuất khẩu vì mục đích thương mại.'  WHERE ReferenceDB='A528' AND Code='EK02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','EK02',N'Danh mục những loài động vật, thực vật hoang dã mà một nước thành viên CITES yêu cầu nước thành viên khác của CITES hợp tác để kiểm soát việc xuất khẩu, nhập khẩu, tái xuất khẩu vì mục đích thương mại.')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='EL02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nộp/xuất trình giấy phép của Cục Thú y theo TT 88/2011/TT-BNNPTNT'  WHERE ReferenceDB='A528' AND Code='EL02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','EL02',N'Nộp/xuất trình giấy phép của Cục Thú y theo TT 88/2011/TT-BNNPTNT')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='EN02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy thông báo kết quả kiểm dịch hoặc giấy miễn kiểm dịch theo Thông tư 01/2012/TT-BTC ngày 03/01/2012; Giấy thông báo kết quả kiểm tra VSATTP đối với hàng hóa có nguồn gốc động vật nhập khẩu theo Thông tư 25/2010/TT-BNNPNT ngày 08/4/2010;'  WHERE ReferenceDB='A528' AND Code='EN02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','EN02',N'Giấy thông báo kết quả kiểm dịch hoặc giấy miễn kiểm dịch theo Thông tư 01/2012/TT-BTC ngày 03/01/2012; Giấy thông báo kết quả kiểm tra VSATTP đối với hàng hóa có nguồn gốc động vật nhập khẩu theo Thông tư 25/2010/TT-BNNPNT ngày 08/4/2010;')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='EP02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy thông báo kết quả kiểm tra hoặc giấy miễn kiểm tra theo Thông tư 01/2012/TT-BTC ngày 03/01/2012'  WHERE ReferenceDB='A528' AND Code='EP02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','EP02',N'Giấy thông báo kết quả kiểm tra hoặc giấy miễn kiểm tra theo Thông tư 01/2012/TT-BTC ngày 03/01/2012')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='EQ02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy thông báo kết quả kiểm tra nhà nước về chất lượng hoặc giấy miễn kiểm tra nhà nước về chất lượng theo Thông tư 50/2009/TT-BNNTPTN ngày 18/8/2009'  WHERE ReferenceDB='A528' AND Code='EQ02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','EQ02',N'Giấy thông báo kết quả kiểm tra nhà nước về chất lượng hoặc giấy miễn kiểm tra nhà nước về chất lượng theo Thông tư 50/2009/TT-BNNTPTN ngày 18/8/2009')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='ER02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Hàng phải kiểm tra an toàn thực phẩm'  WHERE ReferenceDB='A528' AND Code='ER02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','ER02',N'Hàng phải kiểm tra an toàn thực phẩm')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='EX02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Hàng thuộc diện phải kiểm dịch theo TT 32/2012/TT-BNNPTNT'  WHERE ReferenceDB='A528' AND Code='EX02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','EX02',N'Hàng thuộc diện phải kiểm dịch theo TT 32/2012/TT-BNNPTNT')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FA02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy xác nhận chất lượng'  WHERE ReferenceDB='A528' AND Code='FA02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FA02',N'Giấy xác nhận chất lượng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FE02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Thông báo kết quả kiểm tra chất lượng muối nhập khẩu'  WHERE ReferenceDB='A528' AND Code='FE02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FE02',N'Thông báo kết quả kiểm tra chất lượng muối nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FF02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Thông báo kết quả kiểm dịch đạt yêu cầu'  WHERE ReferenceDB='A528' AND Code='FF02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FF02',N'Thông báo kết quả kiểm dịch đạt yêu cầu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép CITES khi XK gỗ và sản phẩm làm từ gỗ thuộc các Phụ lục của CITES'  WHERE ReferenceDB='A528' AND Code='FH02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH02',N'Giấy phép CITES khi XK gỗ và sản phẩm làm từ gỗ thuộc các Phụ lục của CITES')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu với hàng hóa xuất khẩu quy định tại Điều 10 Thông tư này'  WHERE ReferenceDB='A528' AND Code='FH03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH03',N'Giấy phép xuất khẩu với hàng hóa xuất khẩu quy định tại Điều 10 Thông tư này')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH04')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu với hàng hóa nhập khẩu quy định tại Điều 11 Thông tư này'  WHERE ReferenceDB='A528' AND Code='FH04'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH04',N'Giấy phép nhập khẩu với hàng hóa nhập khẩu quy định tại Điều 11 Thông tư này')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH05')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu, nhập khẩu giống cây trồng quy định tại Mục 3 Thông tư'  WHERE ReferenceDB='A528' AND Code='FH05'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH05',N'Giấy phép xuất khẩu, nhập khẩu giống cây trồng quy định tại Mục 3 Thông tư')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH06')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu thuốc thú y quy định tại Mục 5 Thông tư'  WHERE ReferenceDB='A528' AND Code='FH06'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH06',N'Giấy phép nhập khẩu thuốc thú y quy định tại Mục 5 Thông tư')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH07')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu thuốc bảo vệ thực vật và vật thể phải có giấy phép kiểm dịch thực vật nhập khẩu  quy định tại Mục 6 Thông tư'  WHERE ReferenceDB='A528' AND Code='FH07'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH07',N'Giấy phép nhập khẩu thuốc bảo vệ thực vật và vật thể phải có giấy phép kiểm dịch thực vật nhập khẩu  quy định tại Mục 6 Thông tư')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH08')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu thức ăn gia súc, gia cầm quy định tại Mục 7 Thông tư'  WHERE ReferenceDB='A528' AND Code='FH08'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH08',N'Giấy phép nhập khẩu thức ăn gia súc, gia cầm quy định tại Mục 7 Thông tư')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH09')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu phân bón quy định tại Mục 8 Thông tư'  WHERE ReferenceDB='A528' AND Code='FH09'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH09',N'Giấy phép nhập khẩu phân bón quy định tại Mục 8 Thông tư')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH10')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu, nhập khẩu nguồn gen cây trồng phục vụ nghiên cứu, trao đổi khoa học kỹ thuật quy định tại Mục 9 Thông tư'  WHERE ReferenceDB='A528' AND Code='FH10'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH10',N'Giấy phép xuất khẩu, nhập khẩu nguồn gen cây trồng phục vụ nghiên cứu, trao đổi khoa học kỹ thuật quy định tại Mục 9 Thông tư')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH11')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu, nhập khẩu hàng hóa chuyên ngành thủy sản theo quy định tại Mục 10 Thông tư'  WHERE ReferenceDB='A528' AND Code='FH11'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH11',N'Giấy phép xuất khẩu, nhập khẩu hàng hóa chuyên ngành thủy sản theo quy định tại Mục 10 Thông tư')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FH12')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu thuốc bảo vệ thực vật nhằm mục đích xuất khẩu'  WHERE ReferenceDB='A528' AND Code='FH12'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FH12',N'Giấy phép nhập khẩu thuốc bảo vệ thực vật nhằm mục đích xuất khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FN02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy chứng nhận đủ điều kiện kiểm tra chất lượng'  WHERE ReferenceDB='A528' AND Code='FN02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FN02',N'Giấy chứng nhận đủ điều kiện kiểm tra chất lượng')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='FN03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục hàng hóa phải có giấy phép NK '  WHERE ReferenceDB='A528' AND Code='FN03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','FN03',N'Danh mục hàng hóa phải có giấy phép NK ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='TB02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép xuất khẩu, nhập khẩu vàng nguyên liệu'  WHERE ReferenceDB='A528' AND Code='TB02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','TB02',N'Giấy phép xuất khẩu, nhập khẩu vàng nguyên liệu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='TC02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy đăng ký kiểm tra chất lượng  (được đưa hàng về bảo quản trước khi có kết quả kiểm tra) hoặc giấy chứng nhận kiểm tra chất lượng '  WHERE ReferenceDB='A528' AND Code='TC02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','TC02',N'Giấy đăng ký kiểm tra chất lượng  (được đưa hàng về bảo quản trước khi có kết quả kiểm tra) hoặc giấy chứng nhận kiểm tra chất lượng ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='VA02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục tem bưu chính cần GPNK'  WHERE ReferenceDB='A528' AND Code='VA02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','VA02',N'Danh mục tem bưu chính cần GPNK')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='LA02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'DM thiết bị thu phát sóng vô tuyến điện cần GPNK'  WHERE ReferenceDB='A528' AND Code='LA02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','LA02',N'DM thiết bị thu phát sóng vô tuyến điện cần GPNK')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='      LA03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Văn bản cho phép nhập khẩu sản phẩm thuộc Danh mục cấm nhập khẩu để nghiên cứu khoa học của Bộ Thông tin và truyền thông'  WHERE ReferenceDB='A528' AND Code='      LA03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','      LA03',N'Văn bản cho phép nhập khẩu sản phẩm thuộc Danh mục cấm nhập khẩu để nghiên cứu khoa học của Bộ Thông tin và truyền thông')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='LJ03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Văn bản chấp thuận cho phép thực hiện hoạt động gia công của Bộ Thông tin và truyền thông khi thực hiện hoạt động gia công tái chế, sửa chữa các sản phẩm công nghệ thông tin đã qua sử dụng thuộc danh mục cấm nhập khẩu cho thương nhân nước ngoài'  WHERE ReferenceDB='A528' AND Code='LJ03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','LJ03',N'Văn bản chấp thuận cho phép thực hiện hoạt động gia công của Bộ Thông tin và truyền thông khi thực hiện hoạt động gia công tái chế, sửa chữa các sản phẩm công nghệ thông tin đã qua sử dụng thuộc danh mục cấm nhập khẩu cho thương nhân nước ngoài')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='LE02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục thiết bị phát, thu-phát sóng vô tuyến điện theo Phụ lục I'  WHERE ReferenceDB='A528' AND Code='LE02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','LE02',N'Danh mục thiết bị phát, thu-phát sóng vô tuyến điện theo Phụ lục I')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='LF02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mịc tem bưu chính cần giấy phép nhập khẩu theo quy định tại Phụ lục I'  WHERE ReferenceDB='A528' AND Code='LF02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','LF02',N'Danh mịc tem bưu chính cần giấy phép nhập khẩu theo quy định tại Phụ lục I')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='LG02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không có'  WHERE ReferenceDB='A528' AND Code='LG02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','LG02',N'Không có')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='LH02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục chủng loại thiết bị in phải có giấy phép nhập khẩu theo quy định tại Điều 9 Thông tư'  WHERE ReferenceDB='A528' AND Code='LH02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','LH02',N'Danh mục chủng loại thiết bị in phải có giấy phép nhập khẩu theo quy định tại Điều 9 Thông tư')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='MB02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'DM phế liệu được phép NK từ nước ngoài để làm nguyên liệu sản xuất'  WHERE ReferenceDB='A528' AND Code='MB02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','MB02',N'DM phế liệu được phép NK từ nước ngoài để làm nguyên liệu sản xuất')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='NB02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục sản phẩm, hàng hoá có khả năng gây mất an toàn'  WHERE ReferenceDB='A528' AND Code='NB02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','NB02',N'Danh mục sản phẩm, hàng hoá có khả năng gây mất an toàn')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='NC02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'không'  WHERE ReferenceDB='A528' AND Code='NC02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','NC02',N'không')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='ND02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục hàng hoá vật liệu xây dựng gạch ốp lát'  WHERE ReferenceDB='A528' AND Code='ND02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','ND02',N'Danh mục hàng hoá vật liệu xây dựng gạch ốp lát')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='PC02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục hàng hóa được phép xuất khẩu'  WHERE ReferenceDB='A528' AND Code='PC02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','PC02',N'Danh mục hàng hóa được phép xuất khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='PC03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục hàng hóa được phép nhập khẩu'  WHERE ReferenceDB='A528' AND Code='PC03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','PC03',N'Danh mục hàng hóa được phép nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='PC04')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N''  WHERE ReferenceDB='A528' AND Code='PC04'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','PC04',N'')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='QB02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'24 hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế được cấp giấy chứng nhận đăng ký lưu hành tại VN'  WHERE ReferenceDB='A528' AND Code='QB02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','QB02',N'24 hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế được cấp giấy chứng nhận đăng ký lưu hành tại VN')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HA02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục vắc xin, sinh phẩm y tế dùng trong gia dụng và y tế được nhập khẩu theo nhu cầu '  WHERE ReferenceDB='A528' AND Code='HA02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HA02',N'Danh mục vắc xin, sinh phẩm y tế dùng trong gia dụng và y tế được nhập khẩu theo nhu cầu ')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HB02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục hoá chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong gia dụng và y tế được nhập khẩu theo nhu cầu'  WHERE ReferenceDB='A528' AND Code='HB02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HB02',N'Danh mục hoá chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong gia dụng và y tế được nhập khẩu theo nhu cầu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HB03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'1.Danh mục mã số hàng hóa nguyên liệu làm thuốc dùng cho người nhập khẩu vào VN'  WHERE ReferenceDB='A528' AND Code='HB03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HB03',N'1.Danh mục mã số hàng hóa nguyên liệu làm thuốc dùng cho người nhập khẩu vào VN')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HC02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'2.Danh mục mã số hàng hóa thuốc bán thành phẩm nhập khẩu vào VN'  WHERE ReferenceDB='A528' AND Code='HC02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HC02',N'2.Danh mục mã số hàng hóa thuốc bán thành phẩm nhập khẩu vào VN')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HC03')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'3.Danh mục mã số hàng hóa thuốc thành phẩm dạng đơn chất nhập khẩu vào VN'  WHERE ReferenceDB='A528' AND Code='HC03'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HC03',N'3.Danh mục mã số hàng hóa thuốc thành phẩm dạng đơn chất nhập khẩu vào VN')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HC04')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'4.Danh mục Mã số hàng hóa của thuốc thành phẩm đa thành phần nhập khẩu vào VN'  WHERE ReferenceDB='A528' AND Code='HC04'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HC04',N'4.Danh mục Mã số hàng hóa của thuốc thành phẩm đa thành phần nhập khẩu vào VN')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HC05')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'5.Danh mục mã số hàng hóa Dược liệu nhập khẩu vào VN'  WHERE ReferenceDB='A528' AND Code='HC05'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HC05',N'5.Danh mục mã số hàng hóa Dược liệu nhập khẩu vào VN')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HC06')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'6.Danh mục hàng hóa mỹ phẩm nhập khẩu vào VN'  WHERE ReferenceDB='A528' AND Code='HC06'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HC06',N'6.Danh mục hàng hóa mỹ phẩm nhập khẩu vào VN')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HC07')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục nguyên liệu và thuốc thành phẩm cấm nhập khẩu để làm thuốc dùng cho người'  WHERE ReferenceDB='A528' AND Code='HC07'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HC07',N'Danh mục nguyên liệu và thuốc thành phẩm cấm nhập khẩu để làm thuốc dùng cho người')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HQ01HQ02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục hàng hoá nhập khẩu phải kiểm tra về vệ sinh an toàn thực phẩm theo mã số HS'  WHERE ReferenceDB='A528' AND Code='HQ01HQ02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528',N'HQ01HQ02',N'Danh mục hàng hoá nhập khẩu phải kiểm tra về vệ sinh an toàn thực phẩm theo mã số HS')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HK01HK02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'không'  WHERE ReferenceDB='A528' AND Code='HK01HK02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528',N'HK01HK02',N'không')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HL02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không'  WHERE ReferenceDB='A528' AND Code='HL02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HL02',N'Không')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HM02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc phạm vi được phân công quản lý của Bộ Y tế'  WHERE ReferenceDB='A528' AND Code='HM02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HM02',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc phạm vi được phân công quản lý của Bộ Y tế')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HN02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không'  WHERE ReferenceDB='A528' AND Code='HN02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HN02',N'Không')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HP02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không có danh mục, chỉ sửa đổi phần nguyên tắc quản lý, không sửa đổi danh mục'  WHERE ReferenceDB='A528' AND Code='HP02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HP02',N'Không có danh mục, chỉ sửa đổi phần nguyên tắc quản lý, không sửa đổi danh mục')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HQ01 hoặc HQ02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N''  WHERE ReferenceDB='A528' AND Code='HQ01 hoặc HQ02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528',N'HQ01HQ02',N'')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HR02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không có'  WHERE ReferenceDB='A528' AND Code='HR02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HR02',N'Không có')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HS02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không'  WHERE ReferenceDB='A528' AND Code='HS02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HS02',N'Không')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='HT02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục trang thiết bị y tế phải cấp giấy phép nhập khẩu'  WHERE ReferenceDB='A528' AND Code='HT02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','HT02',N'Danh mục trang thiết bị y tế phải cấp giấy phép nhập khẩu')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='KB02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục xe máy chuyên dùng phải kiểm định'  WHERE ReferenceDB='A528' AND Code='KB02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','KB02',N'Danh mục xe máy chuyên dùng phải kiểm định')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='KD02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không có'  WHERE ReferenceDB='A528' AND Code='KD02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','KD02',N'Không có')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='KE02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý nhà nước của Bộ Giao thông vận tải'  WHERE ReferenceDB='A528' AND Code='KE02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','KE02',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý nhà nước của Bộ Giao thông vận tải')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='KF02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không'  WHERE ReferenceDB='A528' AND Code='KF02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','KF02',N'Không')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='KG02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không'  WHERE ReferenceDB='A528' AND Code='KG02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','KG02',N'Không')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='KH02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Giấy phép nhập khẩu pháp hiệu cho an toàn hàng hải'  WHERE ReferenceDB='A528' AND Code='KH02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','KH02',N'Giấy phép nhập khẩu pháp hiệu cho an toàn hàng hải')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='RA02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục hàng hóa nhóm 2 thuộc trách nhiệm quản lý của Bộ KH&CN.'  WHERE ReferenceDB='A528' AND Code='RA02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','RA02',N'Danh mục hàng hóa nhóm 2 thuộc trách nhiệm quản lý của Bộ KH&CN.')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='UB02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không có'  WHERE ReferenceDB='A528' AND Code='UB02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','UB02',N'Không có')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='XA02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không có danh mục'  WHERE ReferenceDB='A528' AND Code='XA02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','XA02',N'Không có danh mục')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='KD02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không có'  WHERE ReferenceDB='A528' AND Code='KD02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','KD02',N'Không có')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='KD02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Không có'  WHERE ReferenceDB='A528' AND Code='KD02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','KD02',N'Không có')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='XF02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Có danh mục'  WHERE ReferenceDB='A528' AND Code='XF02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','XF02',N'Có danh mục')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='XG02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N''  WHERE ReferenceDB='A528' AND Code='XG02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','XG02',N'')
END
IF EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A528' AND Code='XH02')
BEGIN
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh mục các loại thép và tiêu chuẩn, quy chuẩn quốc gia về thép'  WHERE ReferenceDB='A528' AND Code='XH02'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_Common(ReferenceDB,Code,Name_VN) VALUES('A528','XH02',N'Danh mục các loại thép và tiêu chuẩn, quy chuẩn quốc gia về thép')
END

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '23.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('23.4',GETDATE(), N'Cập nhật giấy phép ')
END	