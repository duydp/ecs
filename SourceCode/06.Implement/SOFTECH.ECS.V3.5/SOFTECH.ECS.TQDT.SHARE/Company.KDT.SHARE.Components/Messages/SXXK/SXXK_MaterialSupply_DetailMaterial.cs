﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class SXXK_MaterialSupply_DetailMaterial
    {

        /// <summary>
        // Số chứng từ/hóa đơn/văn bản
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }

        /// <summary>
        /// Ngày chứng từ
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }
       

       
    }
}
