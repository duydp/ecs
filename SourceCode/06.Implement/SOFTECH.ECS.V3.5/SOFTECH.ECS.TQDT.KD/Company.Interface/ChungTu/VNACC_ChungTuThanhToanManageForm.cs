﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{
    public partial class VNACC_ChungTuThanhToanManageForm : BaseForm
    {
        private DataSet dsChungTuThanhToan = new DataSet();
        private List<KDT_VNACC_ChungTuThanhToan> ChungTuThanhToanColecctions = new List<KDT_VNACC_ChungTuThanhToan>();
        public ELoaiThongTin LoaiChungTu = ELoaiThongTin.IAS;
        public string whereIAS = "";
        public string whereIBA = "";
        public VNACC_ChungTuThanhToanManageForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void VNACC_ChungTuThanhToanManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                BindData();

                //Fill ValueList
                //Loai chung tu
                this.dgList.RootTable.Columns["LoaiChungTu"].HasValueList = true;
                Janus.Windows.GridEX.GridEXValueListItemCollection vlLoaiChungTu = this.dgList.RootTable.Columns["LoaiChungTu"].ValueList;
                vlLoaiChungTu.Add(new Janus.Windows.GridEX.GridEXValueListItem(ELoaiThongTin.IAS.ToString(), "Chứng từ bảo lãnh"));
                vlLoaiChungTu.Add(new Janus.Windows.GridEX.GridEXValueListItem(ELoaiThongTin.IBA.ToString(), "Hạn mức ngân hàng"));
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void BindData()
        {
            dsChungTuThanhToan = KDT_VNACC_ChungTuThanhToan.SelectAllManagement(GetSearchWhere());

            dgList.DataSource = dsChungTuThanhToan.Tables[0];
            try { dgList.Refetch(); }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private List<String> GetSearchWhere()
        {
            whereIAS += " 1 = 1";
            whereIBA += " 1 = 1";
            if (!String.IsNullOrEmpty(cbbLoaiCT.SelectedValue.ToString()))
            {
                if (cbbLoaiCT.SelectedValue.ToString() == "IAS")
                {
                    whereIAS += " AND LoaiChungTu = '" + cbbLoaiCT.SelectedValue.ToString() + "'";
                }
                else if (cbbLoaiCT.SelectedValue.ToString() == "IBA")
                {
                    whereIBA += " AND LoaiChungTu = '" + cbbLoaiCT.SelectedValue.ToString() + "'";
                }
                else
                {
                    whereIAS += " AND LoaiChungTu ='IAS'";
                    whereIBA += " AND LoaiChungTu ='IBA'";
                }
            }
                //where += " AND LoaiChungTu = '" + cbbLoaiCT.SelectedValue.ToString() + "'";
            if (!String.IsNullOrEmpty(txtKiHieuCT.Text))
            {
                if (cbbLoaiCT.SelectedValue.ToString() == "IAS")
                {
                    whereIAS += " AND KiHieuChungTuPhatHanhBaoLanh = '" + txtKiHieuCT.Text.Trim() + "'";
                }
                else if (cbbLoaiCT.SelectedValue.ToString() == "IBA")
                {
                    whereIBA += " AND KiHieuChungTuPhatHanhHanMuc = '" + txtKiHieuCT.Text.Trim()+ "'";
                }
                else
                {
                    whereIAS += " AND KiHieuChungTuPhatHanhBaoLanh = '" + txtKiHieuCT.Text.Trim() + "'";
                    whereIBA += " AND KiHieuChungTuPhatHanhHanMuc = '" + txtKiHieuCT.Text.Trim() + "'";
                }
            }
                //where += " AND KiHieuChungTuPhatHanhBaoLanh = '" + txtKiHieuCT.Text.Trim() + "' OR KiHieuChungTuPhatHanhHanMuc = '" + txtKiHieuCT.Text.Trim() + "'";
            if (!String.IsNullOrEmpty(txtMaNH.Text))
            {
                if (cbbLoaiCT.SelectedValue.ToString() == "IAS")
                {
                    whereIAS += " AND MaNganHangCungCapBaoLanh = '" + txtMaNH.Text.Trim() + "'";
                }
                else if (cbbLoaiCT.SelectedValue.ToString() == "IBA")
                {
                    whereIBA += " AND MaNganHangTraThay = '" + txtMaNH.Text.Trim() + "'";
                }else
                {
                    whereIAS += " AND MaNganHangCungCapBaoLanh = '" + txtMaNH.Text.Trim() + "'";
                    whereIBA += " AND MaNganHangTraThay = '" + txtMaNH.Text.Trim() + "'";
                }
            }
                //where += " AND MaNganHangCungCapBaoLanh = '" + txtMaNH.Text.Trim() + "' OR MaNganHangTraThay = '" + txtMaNH.Text.Trim() + "'";
            if (!String.IsNullOrEmpty(txtSoHieuCT.Text))
            {
                if (cbbLoaiCT.SelectedValue.ToString() == "IAS")
                {
                    whereIAS += " AND SoChungTuPhatHanhBaoLanh = '" + txtSoHieuCT.Text.Trim() + "'";
                }
                else if (cbbLoaiCT.SelectedValue.ToString() == "IBA")
                {
                    whereIBA += " AND SoHieuPhatHanhHanMuc = '" + txtSoHieuCT.Text.Trim() + "'";
                }
                else
                {
                    whereIAS += " AND SoChungTuPhatHanhBaoLanh = '" + txtSoHieuCT.Text.Trim() + "'";
                    whereIBA += " AND SoHieuPhatHanhHanMuc = '" + txtSoHieuCT.Text.Trim() + "'";
                }
            }
            List<String> List = new List<string>();
            List.Add(whereIAS);
            List.Add(whereIBA);
                //where += " AND SoChungTuPhatHanhBaoLanh = '" + txtSoHieuCT.Text.Trim() + "' OR SoHieuPhatHanhHanMuc = '" + txtSoHieuCT.Text.Trim() + "'";
            return List;
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                List<KDT_VNACC_ChungTuThanhToan> listChungTu = new List<KDT_VNACC_ChungTuThanhToan>();
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                {
                    if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                    {
                        return;
                    }
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            KDT_VNACC_ChungTuThanhToan gp = KDT_VNACC_ChungTuThanhToan.Load(Convert.ToInt64(((System.Data.DataRowView)i.GetRow().DataRow)["ID"]));
                            listChungTu.Add(gp);
                        }
                    }
                }

                foreach (KDT_VNACC_ChungTuThanhToan item in listChungTu)
                {
                    if (item.ID > 0)
                    {
                        //Load chung tu kem chi tiet (Neu co)
                        item.LoadChungTuThanhToan_ChiTiet();

                        //Xoa chung tu dinh kem chi tiet
                        for (int k = 0; k < item.ChungTuChiTietCollection.Count; k++)
                        {
                            item.ChungTuChiTietCollection[k].Delete();
                        }

                        //Xoa chung tu dinh kem trong DB
                        item.Delete();
                    }
                }

                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                KDT_VNACC_ChungTuThanhToan chungTu = KDT_VNACC_ChungTuThanhToan.Load(Convert.ToInt64(((System.Data.DataRowView)e.Row.DataRow)["ID"]));

                if (chungTu.LoaiChungTu == ELoaiThongTin.IAS.ToString())
                {
                    VNACC_ChungTuThanhToanForm_IAS f = new VNACC_ChungTuThanhToanForm_IAS(chungTu.ID);
                    f.ShowDialog(this);
                }
                else if (chungTu.LoaiChungTu == ELoaiThongTin.IBA.ToString())
                {
                    VNACC_ChungTuThanhToanForm_IBA f = new VNACC_ChungTuThanhToanForm_IBA(chungTu.ID);
                    f.ShowDialog(this);
                }

                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            else
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        KDT_VNACC_ChungTuThanhToan gp = KDT_VNACC_ChungTuThanhToan.Load(Convert.ToInt64(((System.Data.DataRowView)i.GetRow().DataRow)["ID"]));
                        if ((gp.SoChungTuPhatHanhBaoLanh != null && gp.SoChungTuPhatHanhBaoLanh.Length > 0)
                            || (gp.SoHieuPhatHanhHanMuc != null && gp.SoHieuPhatHanhHanMuc.Length > 0))
                            btnXoa.Enabled = false;
                        else
                            btnXoa.Enabled = true;
                    }
                }
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            BindData();
        }

    }
}
