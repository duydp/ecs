using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Management;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;
using Company.KD.BLL.Utils;
using System.Data;
using System.Xml;
using Company.KDT.SHARE.Components.Utils;

namespace Company.Interface
{
    internal static class Program
    {
        public static bool isActivated = false;
        //public static License lic = null;

        [STAThread]
        private static void Main()
        {
            try
            {

                //try
                //{                  
                //   // Company.KDT.SHARE.Components.Helpers.DemoSend();
                //    lic = new Company.KDT.SHARE.Components.Utils.License();
                //    loadLicense();

                //}
                //catch (Exception e)
                //{
                //    MessageBox.Show(e.Message + "\nChương trình có lỗi. Vui lòng cài đặt lại.");
                //    Application.Exit();
                //    return;
                //}

                //isActivated = checkRegistered();
                //isActivated = true;

                string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
                ApplicationSettings settings = new ApplicationSettings();
                settings.ReadXml(settingFileName);

                Company.KDT.SHARE.Components.Globals.AupdateConfigXMLToExe("KD");

                if (String.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.GetConfig("APP_ID")))
                {
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("APP_ID", "4094-637750986327728798.apps.smartcaapi.com");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("APP_SECRET", "ZDJkZmZkZWU-NDY0YS00YmRm");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("SERVICE_GET_TOKENURL", "https://rmgateway.vnptit.vn/auth/token");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("URL_CREDENTIALSLIST", "https://rmgateway.vnptit.vn/csc/credentials/list");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("URL_CREDENTIALSINFO", "https://rmgateway.vnptit.vn/csc/credentials/info");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("URL_SIGNHASH", "https://rmgateway.vnptit.vn/csc/signature/signhash");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("URL_SIGN", "https://rmgateway.vnptit.vn/csc/signature/sign");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("URL_GETTRANINFO", "https://rmgateway.vnptit.vn/csc/credentials/gettraninfo");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("IsSignSmartCA", "false");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("UserNameSmartCA", String.Empty);
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("PassWordSmartCA", String.Empty);
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("ACCESS_TOKEN", String.Empty);
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("REFRESH_TOKEN", String.Empty);
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("EXPIRES_TOKEN", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                Company.KDT.SHARE.Components.Globals.APP_SECRET = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("APP_SECRET");
                Company.KDT.SHARE.Components.Globals.APP_ID = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("APP_ID");
                Company.KDT.SHARE.Components.Globals.SERVICE_GET_TOKENURL = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SERVICE_GET_TOKENURL");
                Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSLIST = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_CREDENTIALSLIST");
                Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSINFO = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_CREDENTIALSINFO");
                Company.KDT.SHARE.Components.Globals.URL_SIGNHASH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_SIGNHASH");
                Company.KDT.SHARE.Components.Globals.URL_SIGN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_SIGN");
                Company.KDT.SHARE.Components.Globals.URL_GETTRANINFO = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_GETTRANINFO");
                Company.KDT.SHARE.Components.Globals.IsSignSmartCA = System.Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsSignSmartCA", "false"));
                Company.KDT.SHARE.Components.Globals.UserNameSmartCA = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("UserNameSmartCA");
                Company.KDT.SHARE.Components.Globals.PaswordSmartCA = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PassWordSmartCA");
                Company.KDT.SHARE.Components.Globals.ACCESS_TOKEN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ACCESS_TOKEN");
                Company.KDT.SHARE.Components.Globals.REFRESH_TOKEN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("REFRESH_TOKEN");
                Company.KDT.SHARE.Components.Globals.EXPIRES_TOKEN = Convert.ToDateTime(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("EXPIRES_TOKEN"));
                GlobalSettings.RefreshKey();

                //TODO: VNACCS--------------------------------------
                ThreadPool.QueueUserWorkItem(Company.KDT.SHARE.VNACCS.Controls.ucBase.InitialData, false);

                CultureInfo culture = null;
                DevExpress.UserSkins.OfficeSkins.Register();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                if (Properties.Settings.Default.NgonNgu == "0")
                    culture = new CultureInfo("vi-VN");
                else
                    culture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;
                DevExpress.UserSkins.BonusSkins.Register();

                DevExpress.LookAndFeel.DefaultLookAndFeel defaultSkin = new DevExpress.LookAndFeel.DefaultLookAndFeel();

                defaultSkin.LookAndFeel.SetSkinStyle("Office 2007 Blue");


                //#if DEBUG
                //Application.Run(new Feedback.QuanLyForm());
                //#else
                Application.Run(new MainForm());

                //ShowFormTrangThai();
                //#endif
            }
            catch (Exception exx)
            {
                Logger.LocalLogger.Instance().WriteMessage(exx);
                MessageBox.Show(" Lỗi : " + exx.Message);
            }
        }

        //-----------------------------------------------------------------------------------------------
        #region Activate Software
        private static void loadLicense()
        {
            //try
            //{
            //    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //    int numInConf = int.Parse(lic.DecryptString(config.AppSettings.Settings["DateTr"].Value));
            //    if (!lic.checkExistsLicense())
            //    {
            //        lic.generTrial();
            //        lic.Load();
            //        if (numInConf < int.Parse(lic.dateTrial))
            //        {
            //            lic.dateTrial = numInConf.ToString();
            //            lic.Save();
            //        }
            //    }
            //    else lic.Load();
            //    isActivated = !string.IsNullOrEmpty(lic.codeActivate);
            //    //if (numInConf > int.Parse(lic.dateTrial))
            //    //{
            //    //    config.AppSettings.Settings.Remove("DateTr");
            //    //    config.AppSettings.Settings.Add("DateTr", lic.EncryptString(lic.dateTrial));
            //    //    config.Save(ConfigurationSaveMode.Full);
            //    //    ConfigurationManager.RefreshSection("appSettings");
            //    //}
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
        }

        //public static string getProcessorID()
        //{
        //    string CPUID = "BoardID:";
        //    ManagementObjectSearcher srch = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
        //    foreach (ManagementObject obj in srch.Get())
        //    {
        //        CPUID = obj.Properties["SerialNumber"].Value.ToString();
        //    }
        //    return License.md5String(CPUID);
        //}

        //public static string getCodeToActivate()
        //{
        //    return License.md5String(getProcessorID() + "SOFTECH.ECS.KD");
        //}

        //private static bool checkRegistered()
        //{
        //    bool value = false;
        //    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        //    if (lic.codeActivate == getCodeToActivate())
        //    {
        //        CultureInfo infoVN = new CultureInfo("vi-VN");
        //        if (DateTime.Parse(lic.dayExpires, infoVN.DateTimeFormat).Date > DateTime.Now.Date)
        //        {
        //            value = true;
        //        }
        //    }
        //    return value;
        //}
        #endregion
    }
}
