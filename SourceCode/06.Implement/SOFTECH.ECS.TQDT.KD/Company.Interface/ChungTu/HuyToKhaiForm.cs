﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.Messages.Send;


namespace Company.Interface
{
    public partial class HuyToKhaiForm : BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiMauDich TKMD;
        public Company.KDT.SHARE.QuanLyChungTu.HuyToKhai huyTK = null;

        public HuyToKhaiForm()
        {
            InitializeComponent();
        }

        private void HuyToKhaiForm_Load(object sender, EventArgs e)
        {
            List<Company.KDT.SHARE.QuanLyChungTu.HuyToKhai> listHuyTK = Company.KDT.SHARE.QuanLyChungTu.HuyToKhai.SelectCollectionBy_TKMD_ID(TKMD.ID);
            if (listHuyTK.Count > 0)
                huyTK = listHuyTK[0];
            //huyTK.TrangThai = 0;
            //huyTK.Update();
            //TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
            //TKMD.ActionStatus = (int)1;
            //TKMD.Update();
            if (huyTK != null)
            {
                txtLyDoHuy.Text = huyTK.LyDoHuy;
                if (huyTK.SoTiepNhan > 0)
                {
                    txtSoTiepNhan.Text = huyTK.SoTiepNhan.ToString();
                    ccNgayTiepNhan.Text = huyTK.NgayTiepNhan.ToShortDateString();
                }
                if (huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                {
                    btnKhaiBao.Enabled = false;
                    btnGhi.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                }
                else if (huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                {
                    lblTrangThai.Text = TrangThaiXuLy.strDAHUY;
                }
                else if (huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                }
            }
            else
            {
                lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                huyTK = new Company.KDT.SHARE.QuanLyChungTu.HuyToKhai();
                btnKhaiBao.Enabled = false;
                btnGhi.Enabled = true;
                btnLayPhanHoi.Enabled = false;
            }
            lblSoTK.Text = TKMD.SoToKhai.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (txtLyDoHuy.Text.Trim() == "")
            {
                ShowMessage("Phải nhập lý do hủy!", false);
                return;
            }
            try
            {
                huyTK.LyDoHuy = txtLyDoHuy.Text.Trim();
                huyTK.TKMD_ID = TKMD.ID;
                huyTK.Guid = TKMD.GUIDSTR;
                if (huyTK.ID == 0)
                    huyTK.Insert();
                else
                    huyTK.Update();
                ShowMessage("Lưu thành công.", false);
                btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Xảy ra lỗi: " + ex.ToString(), false);
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
            {
                SendV3();
            }
            else
                HuyToKhaiV2(this.TKMD);
        }

        private void HuyToKhaiV2(ToKhaiMauDich tkmd)
        {
            WSForm wsForm = new WSForm();
            string password = string.Empty;
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.Default;
                //KHAI BÁO HỦY TỜ KHAI
                bool thanhcong = false;
                thanhcong = tkmd.WSHuyToKhai(password);

                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    btnGhi.Enabled = false;
                    lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                    btnGhi.Enabled = true;
                    lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessageTQDT("Thông báo từ hệ thống ECS", "Chưa có thông tin phản hồi từ hệ thống thông quan điện tử.", false);
            }

        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
            {
                FeedBackV3();
            }
            else
            {
                LayPhanHoiV2();
            }
        }

        private void LayPhanHoiV2()
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                if (this.huyTK.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.huyTK.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                if (huyTK.Guid == "")
                {
                    huyTK.Guid = TKMD.GUIDSTR;
                    huyTK.Update();
                }
                bool thanhcong = huyTK.WSLaySoTiepNhan(password, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep);



                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;

                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHuyTKDuocChapNhan);
                    this.ShowMessage(message, false);

                    TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                    TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                    TKMD.Update();
                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.huyTK.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.huyTK.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.huyTK.NgayTiepNhan.ToShortDateString();
                }
                lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;

            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = huyTK.WSLayPhanHoi(password, TKMD.SoToKhai, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                    lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                }
                else// CÓ PHẢN HỒI
                {
                    string message = "";

                    if (this.huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                        lblTrangThai.Text = TrangThaiXuLy.strTUCHOI;

                        TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                        TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                        TKMD.Update();

                        btnKhaiBao.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                        lblTrangThai.Text = TrangThaiXuLy.strTUCHOI;
                    }
                    //TỜ KHAI HỦY ĐƯỢC CHẤP NHẬN
                    else if (this.huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.huyTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHuyTKThanhCong);

                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                        TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY;
                        TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiDaHuy;
                        TKMD.Update();
                        lblTrangThai.Text = TrangThaiXuLy.strDAHUY;
                    }
                    this.ShowMessage(message, false);

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = this.TKMD.ID;
                form.ShowDialog(this);
            }
            else
            {
                if (huyTK.Guid != null && huyTK.Guid != "")
                    Globals.ShowKetQuaXuLyBoSung(huyTK.Guid);
                else
                    Globals.ShowMessageTQDT("Không có thông tin", false);
            }
        }
        #region Khai báo hủy V3
        private void SendV3()
        {
            string msgSend = SingleMessage.CancelMessageV3(TKMD);
            msgSend.XmlSaveMessage(TKMD.ID, MessageTitle.HuyToKhai);
            SendMessageForm dlgSend = new SendMessageForm();
            TKMD.ActionStatus = (int)ActionStatus.ToKhaiXinHuy;
            TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
            dlgSend.Send += SendMessage;
            dlgSend.DoSend(msgSend);
        }
        private void FeedBackV3()
        {
            string msgSend = SingleMessage.FeedBackMessageV3(TKMD);
            SendMessageForm dlgSendForm = new SendMessageForm();
            dlgSendForm.Send += SendMessage;
            dlgSendForm.DoSend(msgSend);

        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    FeedBackContent feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformation.Content.Text;
                    switch (feedbackContent.AdditionalInformation.Statement.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {
                                noidung = "Từ chối tiếp nhận khai báo hủy tờ khai\r\n" + noidung;
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                TKMD.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                btnKhaiBao.Enabled = true;
                                btnLayPhanHoi.Enabled = false;
                                lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                                this.ShowMessageTQDT(noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            btnKhaiBao.Enabled = false;
                            btnLayPhanHoi.Enabled = true;
                            btnGhi.Enabled = false;
                            lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                            this.ShowMessage(noidung, false);
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {

                                string[] vals = noidung.Split('/');

                                this.huyTK.SoTiepNhan = long.Parse(vals[0].Trim());
                                this.huyTK.NamTiepNhan = int.Parse(vals[1].Trim());
                                this.huyTK.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);

                                noidung = string.Format("Khai báo hủy tờ khai\r\nSố tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nNăm tiếp nhận: {2}\r\nLoại hình: {3}\r\nHải quan: {4}", new object[] {
                                    huyTK.SoTiepNhan, huyTK.NgayTiepNhan.ToString("dd-MM-yyyy HH:mm:ss"), TKMD.NamDK, TKMD.MaLoaiHinh, TKMD.MaHaiQuan });
                                e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocCapSoTiepNhan, noidung);
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    noidung = "Tờ khai được cấp số tiếp nhận\r\n" + noidung;
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;

                                }
                                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
                                {
                                    noidung = "Tờ khai được cấp số chờ hủy khao báo\r\n" + noidung;
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
                                }
                                txtSoTiepNhan.Text = this.huyTK.SoTiepNhan.ToString("N0");
                                ccNgayTiepNhan.Value = this.huyTK.NgayTiepNhan;
                                ccNgayTiepNhan.Text = this.huyTK.NgayTiepNhan.ToShortDateString();
                                lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;

                                this.ShowMessageTQDT(noidung, false);
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
                                {
                                    if (TKMD.SoTiepNhan.ToString() == feedbackContent.CustomsReference)
                                    {
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                                        noidung = "Tờ khai được chấp nhận hủy\r\n" + noidung;
                                        e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.HuyToKhaiThanhCong, noidung);
                                        this.ShowMessage(noidung, false);
                                    }
                                }
                                //  e.FeedBackMessage.XmlSaveMessage(TKMD.ID, MessageTitle.ToKhaiDuocPhanLuong, noidung);
                            }
                            break;
                        default:
                            break;
                    }
                    TKMD.Update();
                }
                else
                {
                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }
        #endregion
    }
}

