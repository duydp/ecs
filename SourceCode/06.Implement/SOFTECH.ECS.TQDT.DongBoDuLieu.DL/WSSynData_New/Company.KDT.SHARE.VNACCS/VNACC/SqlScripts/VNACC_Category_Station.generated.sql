-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@StationCode varchar(5),
	@StationName nvarchar(250),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@CustomsOfficeCode varchar(5),
	@CustomsOfficeCodeRM varchar(5),
	@UserCodeOfImmigrationBureau varchar(5),
	@ManifestForImmigration int,
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@CrewForImmigration int,
	@UserCodeOfQuarantineStation varchar(5),
	@ManifestForQuarantine int,
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@CrewForQuarantine int,
	@UserCodeOfRailwayStationAuthority varchar(5),
	@ManifestForRailwayStationAuthority int,
	@ArrivalDepartureForRailwayStationAuthority int,
	@PassengerForRailwayStationAuthority int,
	@CrewForRailwayStationAuthority int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_Stations]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@StationCode,
	@StationName,
	@CountryCode,
	@NecessityIndicationOfInputName,
	@CustomsOfficeCode,
	@CustomsOfficeCodeRM,
	@UserCodeOfImmigrationBureau,
	@ManifestForImmigration,
	@ArrivalDepartureForImmigration,
	@PassengerForImmigration,
	@CrewForImmigration,
	@UserCodeOfQuarantineStation,
	@ManifestForQuarantine,
	@ArrivalDepartureForQuarantine,
	@PassengerForQuarantine,
	@CrewForQuarantine,
	@UserCodeOfRailwayStationAuthority,
	@ManifestForRailwayStationAuthority,
	@ArrivalDepartureForRailwayStationAuthority,
	@PassengerForRailwayStationAuthority,
	@CrewForRailwayStationAuthority,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@StationCode varchar(5),
	@StationName nvarchar(250),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@CustomsOfficeCode varchar(5),
	@CustomsOfficeCodeRM varchar(5),
	@UserCodeOfImmigrationBureau varchar(5),
	@ManifestForImmigration int,
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@CrewForImmigration int,
	@UserCodeOfQuarantineStation varchar(5),
	@ManifestForQuarantine int,
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@CrewForQuarantine int,
	@UserCodeOfRailwayStationAuthority varchar(5),
	@ManifestForRailwayStationAuthority int,
	@ArrivalDepartureForRailwayStationAuthority int,
	@PassengerForRailwayStationAuthority int,
	@CrewForRailwayStationAuthority int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Stations]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[StationName] = @StationName,
	[CountryCode] = @CountryCode,
	[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
	[CustomsOfficeCode] = @CustomsOfficeCode,
	[CustomsOfficeCodeRM] = @CustomsOfficeCodeRM,
	[UserCodeOfImmigrationBureau] = @UserCodeOfImmigrationBureau,
	[ManifestForImmigration] = @ManifestForImmigration,
	[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
	[PassengerForImmigration] = @PassengerForImmigration,
	[CrewForImmigration] = @CrewForImmigration,
	[UserCodeOfQuarantineStation] = @UserCodeOfQuarantineStation,
	[ManifestForQuarantine] = @ManifestForQuarantine,
	[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
	[PassengerForQuarantine] = @PassengerForQuarantine,
	[CrewForQuarantine] = @CrewForQuarantine,
	[UserCodeOfRailwayStationAuthority] = @UserCodeOfRailwayStationAuthority,
	[ManifestForRailwayStationAuthority] = @ManifestForRailwayStationAuthority,
	[ArrivalDepartureForRailwayStationAuthority] = @ArrivalDepartureForRailwayStationAuthority,
	[PassengerForRailwayStationAuthority] = @PassengerForRailwayStationAuthority,
	[CrewForRailwayStationAuthority] = @CrewForRailwayStationAuthority,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[StationCode] = @StationCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@StationCode varchar(5),
	@StationName nvarchar(250),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@CustomsOfficeCode varchar(5),
	@CustomsOfficeCodeRM varchar(5),
	@UserCodeOfImmigrationBureau varchar(5),
	@ManifestForImmigration int,
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@CrewForImmigration int,
	@UserCodeOfQuarantineStation varchar(5),
	@ManifestForQuarantine int,
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@CrewForQuarantine int,
	@UserCodeOfRailwayStationAuthority varchar(5),
	@ManifestForRailwayStationAuthority int,
	@ArrivalDepartureForRailwayStationAuthority int,
	@PassengerForRailwayStationAuthority int,
	@CrewForRailwayStationAuthority int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [StationCode] FROM [dbo].[t_VNACC_Category_Stations] WHERE [StationCode] = @StationCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Stations] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[StationName] = @StationName,
			[CountryCode] = @CountryCode,
			[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
			[CustomsOfficeCode] = @CustomsOfficeCode,
			[CustomsOfficeCodeRM] = @CustomsOfficeCodeRM,
			[UserCodeOfImmigrationBureau] = @UserCodeOfImmigrationBureau,
			[ManifestForImmigration] = @ManifestForImmigration,
			[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
			[PassengerForImmigration] = @PassengerForImmigration,
			[CrewForImmigration] = @CrewForImmigration,
			[UserCodeOfQuarantineStation] = @UserCodeOfQuarantineStation,
			[ManifestForQuarantine] = @ManifestForQuarantine,
			[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
			[PassengerForQuarantine] = @PassengerForQuarantine,
			[CrewForQuarantine] = @CrewForQuarantine,
			[UserCodeOfRailwayStationAuthority] = @UserCodeOfRailwayStationAuthority,
			[ManifestForRailwayStationAuthority] = @ManifestForRailwayStationAuthority,
			[ArrivalDepartureForRailwayStationAuthority] = @ArrivalDepartureForRailwayStationAuthority,
			[PassengerForRailwayStationAuthority] = @PassengerForRailwayStationAuthority,
			[CrewForRailwayStationAuthority] = @CrewForRailwayStationAuthority,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[StationCode] = @StationCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_Stations]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[StationCode],
			[StationName],
			[CountryCode],
			[NecessityIndicationOfInputName],
			[CustomsOfficeCode],
			[CustomsOfficeCodeRM],
			[UserCodeOfImmigrationBureau],
			[ManifestForImmigration],
			[ArrivalDepartureForImmigration],
			[PassengerForImmigration],
			[CrewForImmigration],
			[UserCodeOfQuarantineStation],
			[ManifestForQuarantine],
			[ArrivalDepartureForQuarantine],
			[PassengerForQuarantine],
			[CrewForQuarantine],
			[UserCodeOfRailwayStationAuthority],
			[ManifestForRailwayStationAuthority],
			[ArrivalDepartureForRailwayStationAuthority],
			[PassengerForRailwayStationAuthority],
			[CrewForRailwayStationAuthority],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@StationCode,
			@StationName,
			@CountryCode,
			@NecessityIndicationOfInputName,
			@CustomsOfficeCode,
			@CustomsOfficeCodeRM,
			@UserCodeOfImmigrationBureau,
			@ManifestForImmigration,
			@ArrivalDepartureForImmigration,
			@PassengerForImmigration,
			@CrewForImmigration,
			@UserCodeOfQuarantineStation,
			@ManifestForQuarantine,
			@ArrivalDepartureForQuarantine,
			@PassengerForQuarantine,
			@CrewForQuarantine,
			@UserCodeOfRailwayStationAuthority,
			@ManifestForRailwayStationAuthority,
			@ArrivalDepartureForRailwayStationAuthority,
			@PassengerForRailwayStationAuthority,
			@CrewForRailwayStationAuthority,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Delete]
	@StationCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Stations]
WHERE
	[StationCode] = @StationCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Stations] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Load]
	@StationCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Stations]
WHERE
	[StationCode] = @StationCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Stations] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Stations]	

GO

