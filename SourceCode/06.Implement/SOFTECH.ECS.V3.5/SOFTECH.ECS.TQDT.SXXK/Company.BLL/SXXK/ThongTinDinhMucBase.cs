using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.BLL.SXXK
{
    public partial class ThongTinDinhMuc : EntityBase
    {
        #region Private members.

        protected string _MaSanPham = String.Empty;
        protected string _MaDoanhNghiep = String.Empty;
        protected string _MaHaiQuan = String.Empty;
        protected int _SoDinhMuc;
        protected DateTime _NgayDangKy = new DateTime(1900, 01, 01);
        protected DateTime _NgayApDung = new DateTime(1900, 01, 01);
        protected DateTime _NgayHetHan = new DateTime(1900, 01, 01);
        protected int _ThanhLy;
        protected long _LenhSanXuat_ID;
        protected int _IsExist = 0;
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string MaSanPham
        {
            set { this._MaSanPham = value.Trim(); }
            get { return this._MaSanPham.Trim(); }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value.Trim(); }
            get { return this._MaDoanhNghiep.Trim(); }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }
        public int SoDinhMuc
        {
            set { this._SoDinhMuc = value; }
            get { return this._SoDinhMuc; }
        }
        public DateTime NgayDangKy
        {
            set { this._NgayDangKy = value; }
            get { return this._NgayDangKy; }
        }
        public DateTime NgayApDung
        {
            set { this._NgayApDung = value; }
            get { return this._NgayApDung; }
        }
        public DateTime NgayHetHan
        {
            set { this._NgayHetHan = value; }
            get { return this._NgayHetHan; }
        }
        public int ThanhLy
        {
            set { this._ThanhLy = value; }
            get { return this._ThanhLy; }
        }

        public long LenhSanXuat_ID
        {
            set { this._LenhSanXuat_ID = value; }
            get { return this._LenhSanXuat_ID; }
        }
        //---------------------------------------------------------------------------------------------

        public int IsExist
        {
            set { this._IsExist = value; }
            get { return this._IsExist; }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_ThongTinDinhMuc_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.Int, this._LenhSanXuat_ID);
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) this._MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) this._SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) this._NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) this._NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetInt32(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) this._LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_SXXK_ThongTinDinhMuc_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ThongTinDinhMuc_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
        public static IDataReader SelectReader()
        {
            const string spName = "[dbo].[p_SXXK_ThongTinDinhMuc_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public ThongTinDinhMucCollection SelectCollectionAll()
        {
            ThongTinDinhMucCollection collection = new ThongTinDinhMucCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                ThongTinDinhMuc entity = new ThongTinDinhMuc();

                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) entity.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) entity.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetInt32(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) entity.LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
                collection.Add(entity);
            }
            return collection;
        }
        public static ThongTinDinhMucCollection SelectCollection()
        {
            ThongTinDinhMucCollection collection = new ThongTinDinhMucCollection();

            IDataReader reader = SelectReader();
            while (reader.Read())
            {
                ThongTinDinhMuc entity = new ThongTinDinhMuc();

                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) entity.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) entity.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetInt32(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) entity.LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
                collection.Add(entity);
            }
            return collection;
        }
        //---------------------------------------------------------------------------------------------

        public ThongTinDinhMucCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            ThongTinDinhMucCollection collection = new ThongTinDinhMucCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ThongTinDinhMuc entity = new ThongTinDinhMuc();

                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) entity.SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) entity.NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetInt32(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) entity.LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
                collection.Add(entity);
            }
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoDinhMuc", SqlDbType.Int, this._SoDinhMuc);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayApDung", SqlDbType.DateTime, this._NgayApDung);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, this._NgayHetHan);
            this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Int, this._ThanhLy);
            this.db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, this.LenhSanXuat_ID);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        public long InsertTransaction(SqlTransaction transaction, string connectionString)
        {
            SqlDatabase db = new SqlDatabase(connectionString);
            string spName = "p_SXXK_ThongTinDinhMuc_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoDinhMuc", SqlDbType.Int, this._SoDinhMuc);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayApDung", SqlDbType.DateTime, this._NgayApDung);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, this._NgayHetHan);
            this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Int, this._ThanhLy);
            this.db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, this.LenhSanXuat_ID);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(ThongTinDinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ThongTinDinhMuc item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, ThongTinDinhMucCollection collection)
        {
            foreach (ThongTinDinhMuc item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoDinhMuc", SqlDbType.Int, this._SoDinhMuc);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayApDung", SqlDbType.DateTime, this._NgayApDung);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, this._NgayHetHan);
            this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Int, this._ThanhLy);
            this.db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.Int, this._LenhSanXuat_ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(ThongTinDinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ThongTinDinhMuc item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoDinhMuc", SqlDbType.Int, this._SoDinhMuc);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayApDung", SqlDbType.DateTime, this._NgayApDung);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, this._NgayHetHan);
            this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Int, this._ThanhLy);
            this.db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, this.LenhSanXuat_ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(ThongTinDinhMucCollection collection, SqlTransaction transaction)
        {
            foreach (ThongTinDinhMuc item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, this.LenhSanXuat_ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(ThongTinDinhMucCollection collection, SqlTransaction transaction)
        {
            foreach (ThongTinDinhMuc item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(ThongTinDinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ThongTinDinhMuc item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion

        public static void Delete(string maHQ, string maDN)
        {
            string sql = "delete from t_SXXK_ThongTinDinhMuc where MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan and " +
                        " MaSanPham not in (select distinct MaSanPham from t_SXXK_DinhMuc where MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHQ);
            db.ExecuteNonQuery(dbCommand);
        }

        public bool Load(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, this.LenhSanXuat_ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) this._MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) this._SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) this._NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) this._NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetInt32(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) this._LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool Load(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_Load";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            IDataReader reader = db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) this._MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDinhMuc"))) this._SoDinhMuc = reader.GetInt32(reader.GetOrdinal("SoDinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayApDung"))) this._NgayApDung = reader.GetDateTime(reader.GetOrdinal("NgayApDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) this._NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetInt32(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LenhSanXuat_ID"))) this._LenhSanXuat_ID = reader.GetInt64(reader.GetOrdinal("LenhSanXuat_ID"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_ThongTinDinhMuc_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@SoDinhMuc", SqlDbType.Int, this._SoDinhMuc);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            db.AddInParameter(dbCommand, "@NgayApDung", SqlDbType.DateTime, this._NgayApDung);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, this._NgayHetHan);
            db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Int, this._ThanhLy);
            db.AddInParameter(dbCommand, "@LenhSanXuat_ID", SqlDbType.BigInt, this.LenhSanXuat_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

    }
}