﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class CauHinhToKhaiForm : BaseForm
    {
        public CauHinhToKhaiForm()
        {
            InitializeComponent();
        }

        public Label lblPhuongThucTT { get { return _lblPhuongThucTT; } }
        public List<Label> Listlable = new List<Label>();

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (Label lable in Listlable)
            {
                lable.ForeColor = lable.ForeColor == Color.Black ? Color.Red : Color.Black;
            }
        }

        private void khoitao_DuLieuChuan()
        {
            //Hungtq 22/12/2010.

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll();
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            txtTenDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            //txtTyGiaUSD.Value =  GlobalSettings.TY_GIA_USD;
            cbMaHTS.SelectedIndex = GlobalSettings.MaHTS;

            ctrCuaKhauXuatHang.Ma = GlobalSettings.CUA_KHAU;
            ctrDiaDiemDoHang.Ma = GlobalSettings.DIA_DIEM_DO_HANG;
            cbTinhThue.SelectedValue = GlobalSettings.TuDongTinhThue;
            //phih 23/10/2012
            txtTieuDeDM.Text = GlobalSettings.TieuDeInDinhMuc;
            txtMienThueXNK.Text = GlobalSettings.MienThueXNK;
            //Khanhhn 23/06/2012
            cbPTTinhThue.SelectedValue = GlobalSettings.TinhThueTGNT;

            chkTuDongTinhThue.Checked = Company.KDT.SHARE.Components.Globals.IsTinhThue;
            
        }
        private void CauHinhToKhaiForm_Load(object sender, EventArgs e)
        {
            khoitao_DuLieuChuan();
            if (Listlable.Count > 0)
            {
                timer1.Interval = 500;
                timer1.Start();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //Hungtq 22/12/2010. Luu cau hinh
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DOI_TAC", txtTenDoiTac.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PTTT_MAC_DINH", cbPTTT.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PTVT_MAC_DINH", cbPTVT.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DKGH_MAC_DINH", cbDKGH.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CUA_KHAU", ctrCuaKhauXuatHang.Ma);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DIA_DIEM_DO_HANG", ctrDiaDiemDoHang.Ma);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NGUYEN_TE_MAC_DINH", ctrNguyenTe.Ma);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NUOC", ctrNuocXK.Ma);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_HTS", cbMaHTS.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TuDongTinhThue", cbTinhThue.SelectedValue);
            //phiph 23/10/2012 (Cập nhật thông tu miễn thuế)
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieuDeInDinhMuc", txtTieuDeDM.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MienThueXNK", txtMienThueXNK.Text.Trim());
            //KhanhHN 23/06/2012
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TinhThueTGNT", cbPTTinhThue.SelectedValue);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsTinhThue", chkTuDongTinhThue.Checked.ToString());
            Company.KDT.SHARE.Components.Globals.IsTinhThue = chkTuDongTinhThue.Checked;
            

            GlobalSettings.RefreshKey();

            showMsg("MSG_0203043");
            //ShowMessage("Lưu cấu hình thành công.", false);
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkTuDongTinhThue_CheckedChanged(object sender, EventArgs e)
        {
            cbPTTinhThue.Enabled = cbTinhThue.Enabled = chkTuDongTinhThue.Checked;
        }

        private void chkDelete_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDelete.CheckState == CheckState.Checked)
            {
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsDelete","True");
            }
            else
            {
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsDelete","False");
            }
        }


    }
}

