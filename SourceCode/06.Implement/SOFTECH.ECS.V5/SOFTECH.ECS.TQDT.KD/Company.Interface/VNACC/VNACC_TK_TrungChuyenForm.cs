﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TK_TrungChuyenForm : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        DataTable dt = new DataTable();
        public VNACC_TK_TrungChuyenForm()
        {
            InitializeComponent();
        }

        private void VNACC_TK_TrungChuyenForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.TrungChuyenCollection);
            grList.DataSource = dt;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                int sott = 1;
                TKMD.TrungChuyenCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow item in dt.Rows)
                {
                    KDT_VNACC_TK_TrungChuyen tc = new KDT_VNACC_TK_TrungChuyen();
                    if (item["ID"].ToString().Length != 0)
                    {
                        tc.ID = Convert.ToInt32(item["ID"].ToString());
                        tc.TKMD_ID = Convert.ToInt32(item["TKMD_ID"].ToString());
                    }
                    tc.SoTT = sott;
                    tc.MaDiaDiem = item["MaDiaDiem"].ToString();
                    tc.NgayDen = Convert.ToDateTime(item["NgayDen"].ToString());
                    tc.NgayKhoiHanh = tc.NgayDen = Convert.ToDateTime(item["NgayKhoiHanh"].ToString());
                    TKMD.TrungChuyenCollection.Add(tc);
                    sott++;
                }
                ShowMessage("Lưu thành công ", false);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa thông tin trung chuyển này không?", true) == "Yes")
                    {
                        KDT_VNACC_TK_TrungChuyen.DeleteDynamic("ID=" + ID);
                        grList.CurrentRow.Delete();
                    }
                }
                else
                    grList.CurrentRow.Delete();
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }


    }
}
