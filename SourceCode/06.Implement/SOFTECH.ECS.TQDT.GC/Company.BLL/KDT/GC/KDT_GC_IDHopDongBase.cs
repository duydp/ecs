using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_IDHopDong : EntityBase
	{
		#region Private members.
		
		protected string _SoHopDong = String.Empty;
		protected long _ID_CU;
		protected long _ID_MOI;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public string SoHopDong
		{
			set {this._SoHopDong = value;}
			get {return this._SoHopDong;}
		}
		public long ID_CU
		{
			set {this._ID_CU = value;}
			get {return this._ID_CU;}
		}
		public long ID_MOI
		{
			set {this._ID_MOI = value;}
			get {return this._ID_MOI;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_KDT_GC_IDHopDong_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, this._SoHopDong);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_CU"))) this._ID_CU = reader.GetInt64(reader.GetOrdinal("ID_CU"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_MOI"))) this._ID_MOI = reader.GetInt64(reader.GetOrdinal("ID_MOI"));
				reader.Close();
				return true;
			}
			reader.Close();
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		


		public DataSet SelectAll()
        {
            string spName = "p_KDT_GC_IDHopDong_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_GC_IDHopDong_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_GC_IDHopDong_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_GC_IDHopDong_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public KDT_GC_IDHopDongCollection SelectCollectionAll()
		{
			KDT_GC_IDHopDongCollection collection = new KDT_GC_IDHopDongCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				KDT_GC_IDHopDong entity = new KDT_GC_IDHopDong();
				
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_CU"))) entity.ID_CU = reader.GetInt64(reader.GetOrdinal("ID_CU"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_MOI"))) entity.ID_MOI = reader.GetInt64(reader.GetOrdinal("ID_MOI"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public KDT_GC_IDHopDongCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			KDT_GC_IDHopDongCollection collection = new KDT_GC_IDHopDongCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				KDT_GC_IDHopDong entity = new KDT_GC_IDHopDong();
				
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_CU"))) entity.ID_CU = reader.GetInt64(reader.GetOrdinal("ID_CU"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_MOI"))) entity.ID_MOI = reader.GetInt64(reader.GetOrdinal("ID_MOI"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_GC_IDHopDong_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, this._SoHopDong);
			this.db.AddInParameter(dbCommand, "@ID_CU", SqlDbType.BigInt, this._ID_CU);
			this.db.AddInParameter(dbCommand, "@ID_MOI", SqlDbType.BigInt, this._ID_MOI);
			
			if (transaction != null)
			{
				return this.db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return this.db.ExecuteNonQuery(dbCommand);
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(KDT_GC_IDHopDongCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (KDT_GC_IDHopDong item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, KDT_GC_IDHopDongCollection collection)
        {
            foreach (KDT_GC_IDHopDong item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_GC_IDHopDong_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, this._SoHopDong);
			this.db.AddInParameter(dbCommand, "@ID_CU", SqlDbType.BigInt, this._ID_CU);
			this.db.AddInParameter(dbCommand, "@ID_MOI", SqlDbType.BigInt, this._ID_MOI);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(KDT_GC_IDHopDongCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (KDT_GC_IDHopDong item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_GC_IDHopDong_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, this._SoHopDong);
			this.db.AddInParameter(dbCommand, "@ID_CU", SqlDbType.BigInt, this._ID_CU);
			this.db.AddInParameter(dbCommand, "@ID_MOI", SqlDbType.BigInt, this._ID_MOI);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(KDT_GC_IDHopDongCollection collection, SqlTransaction transaction)
        {
            foreach (KDT_GC_IDHopDong item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_GC_IDHopDong_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, this._SoHopDong);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(KDT_GC_IDHopDongCollection collection, SqlTransaction transaction)
        {
            foreach (KDT_GC_IDHopDong item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(KDT_GC_IDHopDongCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (KDT_GC_IDHopDong item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		#endregion
	}	
}