-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Insert]
	@ReferenceDB varchar(5),
	@CodeHQ varchar(3),
	@Code varchar(6),
	@MoTa_HeThong varchar(255),
	@TenCang nvarchar(255),
	@DiaChi nvarchar(255),
	@Note varchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_Berth]
(
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
)
VALUES 
(
	@ReferenceDB,
	@CodeHQ,
	@Code,
	@MoTa_HeThong,
	@TenCang,
	@DiaChi,
	@Note
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Update]
	@ID bigint,
	@ReferenceDB varchar(5),
	@CodeHQ varchar(3),
	@Code varchar(6),
	@MoTa_HeThong varchar(255),
	@TenCang nvarchar(255),
	@DiaChi nvarchar(255),
	@Note varchar(255)
AS

UPDATE
	[dbo].[t_VNACC_Category_Berth]
SET
	[ReferenceDB] = @ReferenceDB,
	[CodeHQ] = @CodeHQ,
	[Code] = @Code,
	[MoTa_HeThong] = @MoTa_HeThong,
	[TenCang] = @TenCang,
	[DiaChi] = @DiaChi,
	[Note] = @Note
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_InsertUpdate]
	@ID bigint,
	@ReferenceDB varchar(5),
	@CodeHQ varchar(3),
	@Code varchar(6),
	@MoTa_HeThong varchar(255),
	@TenCang nvarchar(255),
	@DiaChi nvarchar(255),
	@Note varchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Berth] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Berth] 
		SET
			[ReferenceDB] = @ReferenceDB,
			[CodeHQ] = @CodeHQ,
			[Code] = @Code,
			[MoTa_HeThong] = @MoTa_HeThong,
			[TenCang] = @TenCang,
			[DiaChi] = @DiaChi,
			[Note] = @Note
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_Berth]
		(
			[ReferenceDB],
			[CodeHQ],
			[Code],
			[MoTa_HeThong],
			[TenCang],
			[DiaChi],
			[Note]
		)
		VALUES 
		(
			@ReferenceDB,
			@CodeHQ,
			@Code,
			@MoTa_HeThong,
			@TenCang,
			@DiaChi,
			@Note
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Berth]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Berth] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
FROM
	[dbo].[t_VNACC_Category_Berth]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
FROM [dbo].[t_VNACC_Category_Berth] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
FROM
	[dbo].[t_VNACC_Category_Berth]	

GO

