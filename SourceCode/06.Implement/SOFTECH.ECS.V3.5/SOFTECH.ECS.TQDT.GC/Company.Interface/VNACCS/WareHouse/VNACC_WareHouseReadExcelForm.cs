﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Infragistics.Excel;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.VNACCS.WareHouse
{
    public partial class VNACC_WareHouseReadExcelForm : Company.Interface.BaseForm
    {
        public T_KDT_VNACCS_WarehouseImport_Detail warehouseImport_Detail;
        public T_KDT_VNACCS_WarehouseExport_Detail warehouseExport_Detail;
        public String Type = "";
        public VNACC_WareHouseReadExcelForm()
        {
            InitializeComponent();
        }

        private void VNACC_WareHouseReadExcelForm_Load(object sender, EventArgs e)
        {

        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                // Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //showMsg("MSG_0203008");
                    ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                //Dictionary<string, Worksheet> dict = new Dictionary<string, Worksheet>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                    //dict.Add(worksheet.Name, worksheet);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                return;
            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi chọn file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;

            char STTColumn = Convert.ToChar(txtSTTColumn.Text.Trim());
            int STTCol = ConvertCharToInt(STTColumn);

            char MaHHColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int MaHHCol = ConvertCharToInt(MaHHColumn);

            char TenHHColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int TenHHCol = ConvertCharToInt(TenHHColumn);

            char DVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int DVTCol = ConvertCharToInt(DVTColumn);

            char LoaiHangColumn = Convert.ToChar(txtLoaiHHColumn.Text.Trim());
            int LoaiHangCol = ConvertCharToInt(LoaiHangColumn);

            char MaDinhDanhColumn = Convert.ToChar(txtMaDDColumn.Text.Trim());
            int MaDinhDanhCol = ConvertCharToInt(MaDinhDanhColumn);

            char NguonColumn = Convert.ToChar(txtNguonColumn.Text.Trim());
            int NguonCol = ConvertCharToInt(NguonColumn);

            char SoLuongDKColumn = Convert.ToChar(txtSoLuongDKColumn.Text.Trim());
            int SoLuongDKCol = ConvertCharToInt(SoLuongDKColumn);

            char SoLuongThucColumn = Convert.ToChar(txtSoLuongThucColumn.Text.Trim());
            int SoLuongThucCol = ConvertCharToInt(SoLuongThucColumn);

            string errorTotal = "";
            string errorSTT = "";
            string errorMaHangHoa = "";
            string errorTenHangHoa = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorNguonNhap = "";
            string errorLoaiHangHoa = "";
            string errorMaDinhDanh = "";
            string errorSoLuongDK = "";
            string errorSoLuongDKValid = "";
            string errorSoLuongThuc = "";
            string errorSoLuongThucValid = "";

            List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> ImportCollection = new List<T_KDT_VNACCS_WarehouseImport_GoodsDetail>();
            List<T_KDT_VNACCS_WarehouseExport_GoodsDetail> ExportCollection = new List<T_KDT_VNACCS_WarehouseExport_GoodsDetail>();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        bool isExits = false;
                        if (Type=="Import")
                        {
                            T_KDT_VNACCS_WarehouseImport_GoodsDetail importGoodsDetail = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();
                            try
                            {
                                importGoodsDetail.STT = Convert.ToInt32(wsr.Cells[STTCol].Value);
                                if (importGoodsDetail.STT.ToString().Length == 0)
                                {
                                    errorSTT += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.STT + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorSTT += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.STT + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                importGoodsDetail.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (importGoodsDetail.MaHangHoa.Trim().Length == 0)
                                {
                                    errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.MaHangHoa + "]\n";
                                }
                                else
                                {

                                }
                            }
                            catch (Exception)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                importGoodsDetail.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (importGoodsDetail.TenHangHoa.Trim().Length == 0)
                                {
                                    errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.TenHangHoa + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.TenHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                importGoodsDetail.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                                if (importGoodsDetail.DVT.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.DVT + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
                                    foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                    {
                                        if (item.Code == importGoodsDetail.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.DVT + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.DVT + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                importGoodsDetail.LoaiHangHoa = Convert.ToDecimal(wsr.Cells[LoaiHangCol].Value);
                                if (importGoodsDetail.LoaiHangHoa.ToString().Trim().Length == 0)
                                {
                                    errorLoaiHangHoa += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.LoaiHangHoa + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorLoaiHangHoa += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.LoaiHangHoa + "]\n";
                                isAdd = false;
                            }

                            try
                            {
                                isExits = false;
                                importGoodsDetail.NguonNhap = Convert.ToDecimal(wsr.Cells[NguonCol].Value);
                                if (importGoodsDetail.NguonNhap.ToString().Trim().Length == 0)
                                {
                                    errorNguonNhap += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.NguonNhap + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorNguonNhap += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.NguonNhap + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                importGoodsDetail.SoLuongDuKienNhap = Convert.ToDecimal(wsr.Cells[SoLuongDKCol].Value);
                                if (importGoodsDetail.SoLuongDuKienNhap.ToString().Trim().Length == 0)
                                {
                                    errorSoLuongDK += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.SoLuongDuKienNhap + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(importGoodsDetail.SoLuongDuKienNhap, 4) != importGoodsDetail.SoLuongDuKienNhap)
                                    {
                                        errorSoLuongDKValid += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.SoLuongDuKienNhap + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorSoLuongDK += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.SoLuongDuKienNhap + "]\n";
                                isAdd = false;
                            }

                            try
                            {
                                importGoodsDetail.SoLuongThucNhap = Convert.ToDecimal(wsr.Cells[SoLuongThucCol].Value);
                                if (importGoodsDetail.SoLuongThucNhap.ToString().Trim().Length == 0)
                                {
                                    errorSoLuongThuc += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.SoLuongThucNhap + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(importGoodsDetail.SoLuongThucNhap, 4) != importGoodsDetail.SoLuongThucNhap)
                                    {
                                        errorSoLuongThucValid += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.SoLuongThucNhap + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorSoLuongThuc += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.SoLuongThucNhap + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                importGoodsDetail.MaDinhDanhSX = Convert.ToString(wsr.Cells[MaDinhDanhCol].Value);
                                if (importGoodsDetail.MaDinhDanhSX.ToString().Length == 0)
                                {
                                    errorMaDinhDanh += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.MaDinhDanhSX + "]\n";
                                    isAdd = false;                                    
                                }
                            }
                            catch (Exception)
                            {
                                errorMaDinhDanh += "[" + (wsr.Index + 1) + "]-[" + importGoodsDetail.MaDinhDanhSX + "]\n";
                                isAdd = false;
                            }
                            if (isAdd)
                                ImportCollection.Add(importGoodsDetail);
                        }
                        else
                        {
                            T_KDT_VNACCS_WarehouseExport_GoodsDetail exportGoodsDetail = new T_KDT_VNACCS_WarehouseExport_GoodsDetail();
                            try
                            {
                                exportGoodsDetail.STT = Convert.ToInt32(wsr.Cells[STTCol].Value);
                                if (exportGoodsDetail.STT.ToString().Length == 0)
                                {
                                    errorSTT += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.STT + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorSTT += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.STT + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                exportGoodsDetail.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (exportGoodsDetail.MaHangHoa.Trim().Length == 0)
                                {
                                    errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.MaHangHoa + "]\n";
                                }
                                else
                                {

                                }
                            }
                            catch (Exception)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                exportGoodsDetail.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (exportGoodsDetail.TenHangHoa.Trim().Length == 0)
                                {
                                    errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.TenHangHoa + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.TenHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                exportGoodsDetail.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                                if (exportGoodsDetail.DVT.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.DVT + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
                                    foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                    {
                                        if (item.Code == exportGoodsDetail.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.DVT + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.DVT + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                exportGoodsDetail.LoaiHangHoa = Convert.ToDecimal(wsr.Cells[LoaiHangCol].Value);
                                if (exportGoodsDetail.LoaiHangHoa.ToString().Trim().Length == 0)
                                {
                                    errorLoaiHangHoa += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.LoaiHangHoa + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorLoaiHangHoa += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.LoaiHangHoa + "]\n";
                                isAdd = false;
                            }

                            try
                            {
                                isExits = false;
                                exportGoodsDetail.MucDichSuDung = Convert.ToDecimal(wsr.Cells[NguonCol].Value);
                                if (exportGoodsDetail.MucDichSuDung.ToString().Trim().Length == 0)
                                {
                                    errorNguonNhap += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.MucDichSuDung + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorNguonNhap += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.MucDichSuDung + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                exportGoodsDetail.SoLuongDuKienXuat = Convert.ToDecimal(wsr.Cells[SoLuongDKCol].Value);
                                if (exportGoodsDetail.SoLuongDuKienXuat.ToString().Trim().Length == 0)
                                {
                                    errorSoLuongDK += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.SoLuongDuKienXuat + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(exportGoodsDetail.SoLuongDuKienXuat, 4) != exportGoodsDetail.SoLuongDuKienXuat)
                                    {
                                        errorSoLuongDKValid += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.SoLuongDuKienXuat + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorSoLuongDK += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.SoLuongDuKienXuat + "]\n";
                                isAdd = false;
                            }

                            try
                            {
                                exportGoodsDetail.SoLuongThucXuat = Convert.ToDecimal(wsr.Cells[SoLuongThucCol].Value);
                                if (exportGoodsDetail.SoLuongThucXuat.ToString().Trim().Length == 0)
                                {
                                    errorSoLuongThuc += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.SoLuongThucXuat + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(exportGoodsDetail.SoLuongThucXuat, 4) != exportGoodsDetail.SoLuongThucXuat)
                                    {
                                        errorSoLuongThucValid += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.SoLuongThucXuat + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorSoLuongThuc += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.SoLuongThucXuat + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                exportGoodsDetail.MaDinhDanhSX = Convert.ToString(wsr.Cells[MaDinhDanhCol].Value);
                                if (exportGoodsDetail.MaDinhDanhSX.ToString().Length == 0)
                                {
                                    errorMaDinhDanh += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.MaDinhDanhSX + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorMaDinhDanh += "[" + (wsr.Index + 1) + "]-[" + exportGoodsDetail.MaDinhDanhSX + "]\n";
                                isAdd = false;
                            }
                            if (isAdd)
                                ExportCollection.Add(exportGoodsDetail);
                        }
                    }
                    catch (Exception ex)
                    {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorSTT))
                errorTotal += "\n - [STT] -[STT] : \n" + errorSTT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ. ĐVT PHẢI LÀ ĐVT VNACCS (VÍ DỤ : CÁI/CHIẾC LÀ : PCE)\n";
            if (!String.IsNullOrEmpty(errorLoaiHangHoa))
                errorTotal += "\n - [STT] -[LOẠI HÀNG HÓA] : \n" + errorLoaiHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG . NGUYÊN PHỤ LIỆU NHẬP : 1 , SẢN PHẨM NHẬP : 2\n";
            if (!String.IsNullOrEmpty(errorNguonNhap))
                errorTotal += "\n - [STT] -[NGUỒN NHẬP HOẶC MỤC ĐÍCH SỬ DỤNG] : \n" + errorNguonNhap + " KHÔNG ĐƯỢC ĐỂ TRỐNG .";
            if (!String.IsNullOrEmpty(errorSoLuongDK))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG DỰ KIẾN] : \n" + errorSoLuongDK + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoLuongDKValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG DỰ KIẾN] : \n" + errorSoLuongDKValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorSoLuongThuc))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG THỰC] : \n" + errorSoLuongThuc + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoLuongThucValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG THỰC] : \n" + errorSoLuongThucValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorMaDinhDanh))
                errorTotal += "\n - [STT] -[MÃ ĐỊNH DANH] : \n" + errorMaDinhDanh + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (String.IsNullOrEmpty(errorTotal))
            {
                if (Type == "Import")
                {
                    foreach (T_KDT_VNACCS_WarehouseImport_GoodsDetail goodItem in ImportCollection)
                    {
                        warehouseImport_Detail.Collection.Add(goodItem);
                    }
                }
                else
                {
                    foreach (T_KDT_VNACCS_WarehouseExport_GoodsDetail goodItem in ExportCollection)
                    {
                        warehouseExport_Detail.Collection.Add(goodItem);
                    }
                }
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
            }
            else
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                return;
            }
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkExcelMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_WareHouse();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
