﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC08Form : BaseForm
    {
        public DataSet ds = new DataSet();
        private BangKe08_HQGC BC08 = new BangKe08_HQGC();
        public HopDong HD = new HopDong();
        public DataSet dsBK = new DataSet();
        public ReportViewBC08Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            
                this.BC08.HD = this.HD;
                this.BC08.dsBK = this.dsBK;
                this.BC08.BindReport( );
                printControl1.PrintingSystem = this.BC08.PrintingSystem;
                this.BC08.CreateDocument();
            
        }

        

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }   
        }

   
    }
}