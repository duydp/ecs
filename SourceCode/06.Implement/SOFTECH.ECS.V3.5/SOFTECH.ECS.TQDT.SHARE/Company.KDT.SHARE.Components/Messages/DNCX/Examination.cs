﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class Examination
    {
       /// <summary>
        /// Địa diểm kiểm tra,bắt buộc,an..256
       /// </summary>
        [XmlElement("place")]
       public string Place { get; set; }
       /// <summary>
        /// Ngày dự kiến kiểm tra, bắt buộc, an..10
       /// </summary>
       [XmlElement("time")]
        public string Time { get; set; }
    }
}
