﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using DevExpress.XtraEditors;

namespace Helper.Controls
{
    public enum MessageType
    {
        Error,
        Warning,
        Question,
        Exclamation,
        Infomation
    }

    public partial class ErrorMessageBoxForm_VNACC : DevExpress.XtraEditors.XtraForm
    {
        public static XmlDocument docGuide;
        public static XmlDocument docGuideError;
        public static XmlDocument docGuideErrorSYS;
        private static string formatString = "";

        public DialogResult ReturnValue = DialogResult.Cancel;

        private MessageType _messageBoxType = MessageType.Error;
        public MessageType MessageBoxType
        {
            get { return _messageBoxType; }
            set { _messageBoxType = value; }
        }

        public ErrorMessageBoxForm_VNACC()
        {
            InitializeComponent();
        }

        private void ErrorMessageBoxForm_VNACC_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                switch (MessageBoxType)
                {
                    case MessageType.Error:
                        pic.Image = Company.KDT.SHARE.VNACCS.Controls.Properties.Resources.error1;
                        break;
                    case MessageType.Warning:
                        pic.Image = Company.KDT.SHARE.VNACCS.Controls.Properties.Resources.Warning1;
                        break;
                    case MessageType.Question:
                        pic.Image = Company.KDT.SHARE.VNACCS.Controls.Properties.Resources.Help_32x32;
                        break;
                    case MessageType.Exclamation:
                        pic.Image = Company.KDT.SHARE.VNACCS.Controls.Properties.Resources.Exclamation;
                        break;
                    case MessageType.Infomation:
                        pic.Image = Company.KDT.SHARE.VNACCS.Controls.Properties.Resources.info_32x32;
                        break;
                }

                btnCopy.Visible = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { this.Cursor = Cursors.Default; }

        }

        public bool ShowSendEmailButton
        {
            set { this.btnSendError.Visible = value; }
        }

        public string ErrorCode
        {
            set { this.txtCode.Text = value; }
        }
        public string ErrorString
        {
            set
            {
                this.txtError.Text = value;
                FormatFontRichTextBox(txtError, formatString, 0, FontStyle.Bold, Color.Maroon);
            }
        }
        public string ContentString
        {
            set
            {
                this.txtContent.Text = value;
                FormatFontRichTextBox(txtContent, formatString, 0, FontStyle.Bold, Color.Maroon);
                FormatFontRichTextBox(txtContent, "Giải pháp", 0, FontStyle.Underline, Color.Blue);
            }
        }

        #region Thong bao

        protected static ErrorMessageBoxForm_VNACC _MsgBoxV;

        public static DialogResult ShowMessage(string guideCode, List<ErrorVNACCS> errorLists, bool ShowSendEmailButton, MessageType messageBoxType)
        {
            return ShowMessage(guideCode, errorLists, ShowSendEmailButton, messageBoxType, false);
        }

        public static DialogResult ShowMessage(string guideCode, List<ErrorVNACCS> errorLists, bool ShowSendEmailButton, MessageType messageBoxType, bool isTopMost)
        {
            //Neu khong co danh sach ma loi nao -> khong lam gi ca
            if (errorLists.Count == 0)
                return DialogResult.None;

            docGuideError = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideErrorFile(guideCode);
            docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(guideCode);

            _MsgBoxV = new ErrorMessageBoxForm_VNACC();
            _MsgBoxV.TopMost = isTopMost;
            _MsgBoxV.MessageBoxType = messageBoxType;
            _MsgBoxV.ShowSendEmailButton = ShowSendEmailButton;

            string errorCodes = "";
            string errorStrings = "";
            string noiDungLoiStrings = "";
            string id = "";
            int lanLap = 0;
            bool isSysError = false;
            bool isHaveID = true;
            bool isHaveMultiID = false;
            formatString = "";

            //Danh sach loi
            for (int i = 0; i < errorLists.Count; i++)
            {
                isSysError = false; isHaveID = true; isHaveMultiID = false;

                lanLap = Convert.ToInt32(errorLists[i].Loop); //So lan lap loi

                //Code
                errorCodes += errorLists[i].ErrorCode;
                if (errorLists[i].ErrorCode.Substring(0, 1) == "A") //Ma Code bat dau ky tu = A -> loi SYSTEM
                {
                    isSysError = true;
                    if (docGuideErrorSYS == null)
                        docGuideErrorSYS = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideErrorFile("SYS");
                }

                //ID
                id = errorLists[i].ColumnError.Trim();
                if (id == "0000") //Khong co ID
                {
                    isHaveID = false;
                }
                else if (id.Contains("_")) //Co nhieu ID
                {
                    isHaveMultiID = true;
                }
                else if (lanLap > 0)
                {
                    int idLanLap = 0;
                    
                    if (string.IsNullOrEmpty(id) && id.Length > 2 && int.TryParse(id.Substring(2, 1), out idLanLap))
                        id = id.Substring(0, id.Length - 1) + "_";
                }

                formatString += string.Format("{0}. {1}:", i + 1, errorLists[i].ErrorCode);

                //Noi dung loi theo Code
                if (!isSysError)
                    errorStrings += string.Format("{0}. {1}:\r\n- {2}\r\n-> {3}\r\n\n", i + 1, errorLists[i].ErrorCode,
                        Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuideErrorDescription(docGuideError, errorLists[i].ErrorCode),
                        Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuideErrorDisposition(docGuideError, errorLists[i].ErrorCode));
                else
                    errorStrings += string.Format("{0}. {1}:\r\n- {2}\r\n-> {3}\r\n\n", i + 1, errorLists[i].ErrorCode,
                        Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuideErrorDescription(docGuideErrorSYS, errorLists[i].ErrorCode),
                        Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuideErrorDisposition(docGuideErrorSYS, errorLists[i].ErrorCode));

                //Noi dung mo ta loi theo ID
                if (!isSysError)
                {
                    if (lanLap > 0)
                        noiDungLoiStrings += string.Format("{0}. {1}:\r\n- {2} ({3}):\r\nGiải pháp:\r\n{4}\r\n\n- Lỗi tại dòng/ hàng {5}\r\n\n", i + 1,
                            errorLists[i].ErrorCode,
                            Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuideName(docGuide, id), id,
                            Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuide(docGuide, id),
                            errorLists[i].Loop);
                    else
                        noiDungLoiStrings += string.Format("{0}. {1}:\r\n- {2} ({3}):\r\nGiải pháp:\r\n{4}\r\n\n", i + 1, errorLists[i].ErrorCode,
                            Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuideName(docGuide, id), id,
                            Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuide(docGuide, id));
                }

                if (i < errorLists.Count - 1)
                {
                    errorCodes += ", ";
                    formatString += ", ";
                }
            }

            _MsgBoxV.ErrorCode = errorCodes;
            _MsgBoxV.ErrorString = errorStrings;
            _MsgBoxV.ContentString = noiDungLoiStrings;
            _MsgBoxV.ShowDialog();
            DialogResult st = _MsgBoxV.ReturnValue;
            _MsgBoxV.Dispose();
            return st;
        }

        private static void FormatFontRichTextBox(RichTextBox rtf, string vals, int indexStart, System.Drawing.FontStyle fontStyle, System.Drawing.Color fontColor)
        {
            try
            {
                bool isExists = false;
                if (indexStart < rtf.Text.Length)
                {
                    string[] arr = vals.Split(new char[] { ',' });

                    foreach (string item in arr)
                    {
                        System.Drawing.Font fntFont = new Font("Microsoft Sans Serif", 8f, fontStyle);
                        int i = rtf.Find(item.Trim(), indexStart, RichTextBoxFinds.WholeWord);
                        int j = item.Length;
                        isExists = i != -1; //i != -1

                        if (isExists)
                        {
                            rtf.Select(i, j);
                            rtf.SelectionFont = fntFont;
                            rtf.SelectionColor = fontColor;

                            if (i + j < rtf.Text.Length)
                            {
                                FormatFontRichTextBox(rtf, item.Trim(), i + j, fontStyle, fontColor);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        #endregion

        private void btnSendError_Click(object sender, EventArgs e)
        {
            //Chua lam
        }

        delegate string GetCopyErrorToClipboardCallback();
        private string GetCopyErrorToClipboard()
        {
            string r = "";
            if (txtError.InvokeRequired)
            {
                GetCopyErrorToClipboardCallback d = new GetCopyErrorToClipboardCallback(GetCopyErrorToClipboard);
                Invoke(d, new object[] { txtError.Text });
            }
            else
                r = txtError.Text;

            return r;
        }

        delegate string GetCopytxtContentToClipboardCallback();
        private string GetCopyContentToClipboard()
        {
            string r = "";
            if (txtError.InvokeRequired)
            {
                GetCopytxtContentToClipboardCallback d = new GetCopytxtContentToClipboardCallback(GetCopyContentToClipboard);
                Invoke(d, new object[] { txtContent.Text });
            }
            else
                r = txtContent.Text;

            return r;
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            try
            {
                System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(CopyToClipboard));
                t.SetApartmentState(System.Threading.ApartmentState.STA);
                t.Start();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false, true, ex);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void CopyToClipboard()
        {
            if (tabControl1.SelectedTabPageIndex == 0)
                Clipboard.SetText(GetCopyErrorToClipboard());
            else if (tabControl1.SelectedTabPageIndex == 1)
                Clipboard.SetText(GetCopyContentToClipboard());
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
