﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class CommodityBase
    {
        /// <summary>
        /// Kiểu
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
        /// <summary>
        /// tên hàng
        /// </summary>
        [XmlElement("description")]
        public string Description { set; get; }
        /// <summary>
        /// Mã hàng do nhà sản xuất hoặc doanh nghiệp quy định
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }
        /// <summary>
        /// Mã HS
        /// </summary>
        [XmlElement("tariffClassification")]
        public string TariffClassification { set; get; }
        
    }
}
