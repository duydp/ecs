﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECS.AutoUpdate
{
    /// <summary>
    /// Thực hiện file
    /// </summary>
    internal interface IUpdate
    {
        bool BeforeUpdate();
        bool Update(string fileName);
        bool AfterUpdate();
        string VersionDatabase {get; }
    }
}
