using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V2
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V2
using Company.GC.BLL.KDT;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.Utils;
#elif SXXK_V2
using Company.BLL.KDT;
using Company.BLL.DuLieuChuan;
using Company.BLL.Utils;
#endif

using System.Data;
using System.Collections.Generic;

namespace Company.Interface.Report
{
    public partial class TQDTToKhaiXK_TT15 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKXTQDTFormTT15 report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        int sTT_Container = 0;
#if KD_V2
        double _TongTriGiaNguyenTe = 0;
        double _TongThueXuatKhau = 0;
        double _TongThueThuKhac = 0;
#elif GC_V2
        decimal _TongTriGiaNguyenTe = 0;
        decimal _TongThueXuatKhau = 0;
        decimal _TongThueThuKhac = 0;
#elif SXXK_V2
        decimal _TongTriGiaNguyenTe = 0;
        decimal _TongThueXuatKhau = 0;
        decimal _TongThueThuKhac = 0;
#endif
        private DataTable Hang = new DataTable();

        public TQDTToKhaiXK_TT15()
        {
            InitializeComponent();
        }

        public void BindReport()
        {
            try
            {
                TinhTong();
                setFont();
                sTT_Container = 0;
                string formatdateDetail = "dd-MM-yyyy HH:mm";
                string formatdate = "dd-MM-yyyy";
                #region Thông tin chính
                this.PrintingSystem.ShowMarginsWarning = false;
                // Cục Hải Quan
                xlbCucHaiQuan.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                // Góc trên bên trái
                xtcChiCucHQDK.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                xtcChiCucHQCuaKhauXuat.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);
                // Góc trên ở giữa
                //So luong phu luc to khai
                if (this.TKMD.SoLuongPLTK > 0)
                    xtcSoLuongPhuLuc.Text = this.TKMD.SoLuongPLTK.ToString("00");
                else
                {
                    xtcSoLuongPhuLuc.Text = this.TKMD.HMDCollection!=null ? (this.TKMD.HMDCollection.Count / 3).ToString("00") : "0";
                }
                xtcSoTN.Text = TKMD.SoTiepNhan.ToString();
                xtcNgayDK.Text = TKMD.NgayTiepNhan.ToString(formatdateDetail);
                xtcSoTK.Text = TKMD.SoToKhai.ToString();
                xtcNgayCapSoTK.Text = TKMD.NgayDangKy.ToString(formatdateDetail);
                // Ô 1
                xtcNguoiXuatKhau.Text = TKMD.TenDoanhNghiep;
                xlbMSNguoiXuatKhau.Text = TKMD.MaDoanhNghiep;
                //Ô 2
                xtcNguoiNhapKhau.Text = TKMD.TenDonViDoiTac;
                // Ô 3
                xtcNguoiUT.Text = TKMD.TenDonViUT;
                xlbMSNguoiUT.Text = TKMD.MaDonViUT;
                //Ô 4
                xtcDaiLyHQ.Text = TKMD.TenDaiLyTTHQ;
                xlbMSDaiLyHQ.Text = TKMD.MaDaiLyTTHQ;
                // Ô 5
                xtcLoaiHinh.Text = TKMD.MaLoaiHinh;
                // Ô 6
                if (!string.IsNullOrEmpty(TKMD.SoGiayPhep))
                {
                    lbSoGP.Text = TKMD.SoGiayPhep;
                    lbNgayGP.Text = TKMD.NgayGiayPhep.ToString(formatdate);
                    lbNgayHHGP.Text = TKMD.NgayHetHanGiayPhep.ToString(formatdate);
                }
                else
                {
                    lbSoGP.Text = "";
                    lbNgayGP.Text = "";
                    lbNgayHHGP.Text = "";
                }
                // Ô 7
                if (!string.IsNullOrEmpty(TKMD.SoHopDong))
                {
                    xtcSoHopDong.Text = TKMD.SoHopDong;
                    xtcNgayHopDong.Text = TKMD.NgayHopDong.ToString(formatdate);
                    xtcNgayHHHopDong.Text = TKMD.NgayHetHanHopDong.ToString(formatdate);
                }
                else
                {
                    xtcSoHopDong.Text = "";
                    xtcNgayHopDong.Text = "";
                    xtcNgayHHHopDong.Text = "";
                }
                // Ô 8
                xtcHoaDonTM.Text = TKMD.SoHoaDonThuongMai;
                // Ô 9
                xtcCuaKhauXuat.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);
                // Ô 10
                xtcNuocNhapKhau.Text = "  " + Nuoc.GetName(TKMD.NuocNK_ID);
                //Ô 11
                xtcDieuKienGiaoHang.Text = TKMD.DKGH_ID;
                // Ô 12
                xtcPhuongThucThanhToan.Text = TKMD.PTTT_ID;
                // Ô 13
                xtcDongTienTT.Text = NguyenTe.SelectName(TKMD.NguyenTe_ID);
                // Ô 14
                xtcTiGiaTinhThue.Text = TKMD.TyGiaTinhThue.ToString();
                #endregion
                #region chi tiết Hàng Hóa 15 - 24
                if (TKMD.HMDCollection.Count > 3)
                {
                    xtcMoTaHang.Text = "Theo phụ lục tờ khai";
                    xtcThueXuat_Cong.Text = _TongThueXuatKhau.ToString();
                    xrTableRow7.Size = new Size { Height = 60 };
                    xrTableRow15.Size = new Size { Height = 60 };
                    xtcThueXuat_Cong.Text = _TongThueXuatKhau.ToString("N0");
                    xtcThuKhac_Cong.Text = _TongThueThuKhac.ToString("N0");
                    #region Ô 24
                    xtcTongTienBangSo.Text = (_TongThueXuatKhau + _TongThueThuKhac).ToString("N0");
                    string s = VNCurrency.ToString((decimal)(_TongThueXuatKhau + _TongThueThuKhac)).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    xtcTongTienBangChu.Text = s.Replace("  ", " ");
                    #endregion
                }
                else
                {
                    #region Bảng mô tả hàng - Ô 15 - Ô 21
                    DetailHangHoa.DataSource = TKMD.HMDCollection;
                    xtcSoTTHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoThuTuHang");
                    xtcMoTaHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "TenHang");
                    xtcMaSoHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "MaHS");
                    xtcXuatXuHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "NuocXX_ID");
                    xtcLuongHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoLuong");
                    xtcDVT_Hang.DataBindings.Add("Text", DetailHangHoa.DataSource, "DVT_ID");
                    xtcDonGia_Hang.DataBindings.Add("Text", DetailHangHoa.DataSource, "DonGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                    xtcTriGiaNguyenTe_Hang.DataBindings.Add("Text", DetailHangHoa.DataSource, "TriGiaKB",Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                    xtcCongHang.Text = _TongTriGiaNguyenTe.ToString("N2");
                    #endregion
                    #region Bảng Thuế xuất - Ô 22 + 23
                    DetailThue.DataSource = TKMD.HMDCollection;
                    xtcThueXuat_TriGiaTT.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                    xtcThueXuat_TiLe.DataBindings.Add("Text", DetailThue.DataSource, "ThueSuatXNK");
                    xtcThueXuat_TienThue.DataBindings.Add("Text", DetailThue.DataSource, "ThueXNK", Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                    xtcThueXuat_SoTT.DataBindings.Add("Text", DetailThue.DataSource, "SoThuTuHang");
                    xtcThuKhac_TriGia.DataBindings.Add("Text", DetailThue.DataSource, "TriGiaThuKhac");
                    xtcThuKhac_TyLe.DataBindings.Add("Text", DetailThue.DataSource, "TyLeThuKhac");
                    xtcThuKhac_SoTien.DataBindings.Add("Text", DetailThue.DataSource, "PhuThu", Company.KDT.SHARE.Components.Globals.FormatNumber(2));
                    xtcThueXuat_Cong.Text = _TongThueXuatKhau.ToString("N0");
                    xtcThuKhac_Cong.Text = _TongThueThuKhac.ToString("N0");
                    #endregion
                    #region Ô 24
                    xtcTongTienBangSo.Text = (_TongThueXuatKhau + _TongThueThuKhac).ToString("N0");
                    string s = VNCurrency.ToString((decimal)(_TongThueXuatKhau + _TongThueThuKhac)).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    xtcTongTienBangChu.Text = s.Replace("  ", " ");
                    #endregion
                }
                #endregion

                #region Bảng Container - Ô 25
                // IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangVD = TKMD.VanTaiDon.ListHangOfVanDon.ToArray().Distinct(new DistinctSHContainer());
                if (TKMD.VanTaiDon.ContainerCollection.Count <= 3)
                {
                    DetailLuongHang.DataSource = TKMD.VanTaiDon.ContainerCollection;
                    xtcLuongHang_SoHieuContainer.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoHieu");
                    xtcLuongHang_SoLuongKien.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoHieu");
                    xtcLuongHang_TrongLuongHang.DataBindings.Add("Text", DetailLuongHang.DataSource, "SoHieu");
                    xtcLuongHang_DDDongHang.DataBindings.Add("Text", TKMD.VanTaiDon, "TenCangXepHang");
                }
                else
                    xrTableRow4.Size = new Size { Height = 60 };
                #endregion

                #region Ô 26. Chứng từ đi kèm
                xlbChungTuDinhKem.Text = "";
                if (TKMD.GiayPhepCollection.Count >0 )
                {
                    xlbChungTuDinhKem.Text += "- Giấy phép: ";
                    foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep item in TKMD.GiayPhepCollection)
                        xlbChungTuDinhKem.Text += item.SoGiayPhep + ", ";
                    xlbChungTuDinhKem.Text += "\r\n";
                }
                if (TKMD.HoaDonThuongMaiCollection.Count > 0)
                {
                    xlbChungTuDinhKem.Text += "- Hóa đơn: ";
                    foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai item in TKMD.HoaDonThuongMaiCollection)
                        xlbChungTuDinhKem.Text += item.SoHoaDon + ", ";
                    xlbChungTuDinhKem.Text += "\r\n";
                }
                if (TKMD.HopDongThuongMaiCollection.Count > 0)
                {
                    xlbChungTuDinhKem.Text += "- Hợp đồng: ";
                    foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai item in TKMD.HopDongThuongMaiCollection)
                        xlbChungTuDinhKem.Text += item.SoHopDongTM + ", ";
                    xlbChungTuDinhKem.Text += "\r\n";
                }
                if (TKMD.COCollection.Count > 0)
                {
                    xlbChungTuDinhKem.Text += "- CO : ";
                    foreach (Company.KDT.SHARE.QuanLyChungTu.CO item in TKMD.COCollection)
                        xlbChungTuDinhKem.Text += item.SoCO;
                    xlbChungTuDinhKem.Text += "\r\n";
                }
                if (TKMD.listChuyenCuaKhau.Count > 0)
                {
                    xlbChungTuDinhKem.Text += "- Đề nghị chuyển cửa khẩu : ";
                    foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau item in TKMD.listChuyenCuaKhau)
                        xlbChungTuDinhKem.Text += item.SoVanDon;
                    xlbChungTuDinhKem.Text += "\r\n";
                }
                if (!string.IsNullOrEmpty(TKMD.VanTaiDon.SoVanDon))
                {
                    xlbChungTuDinhKem.Text += "- Vận đơn : " + TKMD.VanTaiDon.SoVanDon;
                    xlbChungTuDinhKem.Text += "\r\n";
                }
                if (TKMD.listCTDK.Count > 0)
                {
                    xlbChungTuDinhKem.Text += "- Chứng từ dạng ảnh: ";
                    foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem item in TKMD.listCTDK)
                        xlbChungTuDinhKem.Text += item.SO_CT;
                    xlbChungTuDinhKem.Text += "\r\n";

                }

                #endregion
                #region Ô 28
                lbKetQuaPL.Text = "";
                lbKetQuaPL.Text += "Luồng: " + TKMD.PhanLuong + "\r\nHướng dẫn: " + TKMD.HUONGDAN;
                #endregion
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        //private decimal TinhSoLuongHangContainer(string soHieuContainer)
        //{
        ////    IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangCollection = from h in TKMD.VanTaiDon.ListHangOfVanDon
        ////                                                                                   where h.SoHieuContainer == soHieuContainer
        ////                                                                                   select h;
        ////    decimal soLuongHang = 0;
        ////    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in HangCollection)
        ////        soLuongHang += item.SoLuong;
        ////    return soLuongHang;
        //}
        //private double TinhTrongLuongHangContainer(string soHieuContainer)
        //{
        ////    IEnumerable<Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail> HangCollection = from h in TKMD.VanTaiDon.ListHangOfVanDon
        ////                                                                                   where h.SoHieuContainer == soHieuContainer
        ////                                                                                   select h;
        ////    double trongLuong = 0;
        ////    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in HangCollection)
        ////        trongLuong += item.TrongLuong;
        ////    return trongLuong;
        //}

        private void xtcDVT_Hang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dvt = (XRTableCell)sender;

            if (!string.IsNullOrEmpty(dvt.Text))
                dvt.Text = DonViTinh.GetName(dvt.Text.Trim());
        }

        //private void xtcLuongHang_SoLuongKien_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    XRTableCell slkien = (XRTableCell)sender;
        //    if (!string.IsNullOrEmpty(slkien.Text))
        //        slkien.Text = TinhSoLuongHangContainer(slkien.Text).ToString();
        //}
        //private void xtcLuongHang_TrongLuongHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    XRTableCell TrongLuongHang = (XRTableCell)sender;
        //    if (!string.IsNullOrEmpty(TrongLuongHang.Text))
        //        TrongLuongHang.Text = TinhTrongLuongHangContainer(TrongLuongHang.Text).ToString(); 
        //}
        
        private void xtcLuongHang_SoTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell stt = (XRTableCell)sender;
            if (!string.IsNullOrEmpty(stt.Text))
            {
                sTT_Container++;
                stt.Text = sTT_Container.ToString();
            }
        }
        private void TinhTong()
        {
             _TongThueThuKhac = _TongThueXuatKhau = _TongTriGiaNguyenTe = 0;
             foreach (HangMauDich item in TKMD.HMDCollection)
            {
                _TongTriGiaNguyenTe += item.TriGiaKB;
                _TongThueXuatKhau += item.ThueXNK;
                _TongThueThuKhac += item.PhuThu;
            }
            //if (TKMD.VanTaiDon != null)
            //    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail item in TKMD.VanTaiDon.ListHangOfVanDon)
            //        _TinhTongTrongLuong += item.TrongLuong;
        }
        private void setFont()
        {
            xrTable3.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            DetailThue.Font = DetailHangHoa.Font = DetailLuongHang.Font = GroupFooter1.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
        }

    }
}
                