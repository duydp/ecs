﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    public class IVA_HangHoa : BasicVNACC
    {

        [AttributeIndex(STT = 1)]
        public PropertiesAttribute SoDong { set; get; }
        [AttributeIndex(STT = 2)]
        public PropertiesAttribute MaHangHoa { set; get; }
        [AttributeIndex(STT = 3)]
        public PropertiesAttribute MaSoHangHoa { set; get; }
        [AttributeIndex(STT = 4)]
        public PropertiesAttribute MoTaHangHoa { set; get; }
        [AttributeIndex(STT = 5)]
        public PropertiesAttribute MaNuocXX { set; get; }
        [AttributeIndex(STT = 6)]
        public PropertiesAttribute TenNuocXX { set; get; }
        [AttributeIndex(STT = 7)]
        public PropertiesAttribute SoKienHang { set; get; }
        [AttributeIndex(STT = 8)]
        public PropertiesAttribute SoLuong1 { set; get; }
        [AttributeIndex(STT = 9)]
        public PropertiesAttribute SoLuong1_DVT { set; get; }
        [AttributeIndex(STT = 10)]
        public PropertiesAttribute SoLuong2 { set; get; }
        [AttributeIndex(STT = 11)]
        public PropertiesAttribute SoLuong2_DVT { set; get; }
        [AttributeIndex(STT = 12)]
        public PropertiesAttribute DonGia { set; get; }
        [AttributeIndex(STT = 13)]
        public PropertiesAttribute DonGia_MaNT { set; get; }
        [AttributeIndex(STT = 14)]
        public PropertiesAttribute DonGia_DVT { set; get; }
        [AttributeIndex(STT = 15)]
        public PropertiesAttribute TriGiaHD { set; get; }
        [AttributeIndex(STT = 16)]
        public PropertiesAttribute TriGiaHD_NguyenTe { set; get; }
        [AttributeIndex(STT = 17)]
        public PropertiesAttribute TienKhauTru_LoaiKhauTru { set; get; }
        [AttributeIndex(STT = 18)]
        public PropertiesAttribute TienKhauTru { set; get; }
        [AttributeIndex(STT = 19)]
        public PropertiesAttribute TienKhauTru_NguyenTe { set; get; }
        

        public IVA_HangHoa()
        {
            SoDong = new PropertiesAttribute(3,typeof(int));
            MaHangHoa = new PropertiesAttribute(40);
            MaSoHangHoa = new PropertiesAttribute(12);
            MoTaHangHoa = new PropertiesAttribute(200);
            MaNuocXX = new PropertiesAttribute(2);
            TenNuocXX = new PropertiesAttribute(30);
            SoKienHang = new PropertiesAttribute(35);
            SoLuong1 = new PropertiesAttribute(12, typeof(double));
            SoLuong1_DVT = new PropertiesAttribute(4);
            SoLuong2 = new PropertiesAttribute(12, typeof(double));
            SoLuong2_DVT = new PropertiesAttribute(4);
            DonGia = new PropertiesAttribute(9, typeof(decimal));
            DonGia_MaNT = new PropertiesAttribute(3);
            DonGia_DVT = new PropertiesAttribute(3);

            TriGiaHD = new PropertiesAttribute(20, typeof(decimal));
            TriGiaHD_NguyenTe = new PropertiesAttribute(3);

            TienKhauTru_LoaiKhauTru = new PropertiesAttribute(20);
            TienKhauTru = new PropertiesAttribute(9, typeof(decimal));
            TienKhauTru_NguyenTe = new PropertiesAttribute(3);
        }



        
    }
}
