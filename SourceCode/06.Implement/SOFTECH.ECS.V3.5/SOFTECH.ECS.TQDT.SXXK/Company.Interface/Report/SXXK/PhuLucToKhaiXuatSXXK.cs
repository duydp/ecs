using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ToKhai;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Collections.Generic;

namespace Company.Interface.Report.SXXK
{
    public partial class PhuLucToKhaiXuatSXXK : DevExpress.XtraReports.UI.XtraReport
    {
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public int SoToKhai = 0;
        public DateTime NgayDangKy = new DateTime(1900, 1, 1);
        private int STT = 0;
        private int STTNhom = 0;
        public PhuLucToKhaiXuatSXXK()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            ArrayList arr = this.GetNhomHang();
            if (arr.Count == 1 && arr[0].ToString() == "")
            {
                GroupHeader1.Visible = false;
                GroupFooter1.Visible = false;
            }
            xrLabel12.Text = GlobalSettings.MA_HAI_QUAN;
            if (this.SoToKhai > 0)
                lblSoToKhai.Text = this.SoToKhai + "";
            if (this.NgayDangKy > new DateTime(1900, 1, 1))
                lblNgayDangKy.Text = this.NgayDangKy.ToString("dd/MM/yyyy");
            this.DataSource = this.HMDCollection;
            //lblSTT.DataBindings.Add("Text", this.DataSource, "SoThuTuHang");
            lblTenHang.DataBindings.Add("Text", this.DataSource, "TenHang");
            lblMaHang.DataBindings.Add("Text", this.DataSource, "MaHS");
            lblLuong.DataBindings.Add("Text", this.DataSource, "SoLuong","{0:g20}");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT_ID");
            lblDonGiaNT.DataBindings.Add("Text", this.DataSource, "DonGiaKB", "{0:g20}");
            lblTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGiaKB", "{0:n2}");
            lblTongTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGiaKB", "{0:n2}");
            lblNhomHang.DataBindings.Add("Text", this.DataSource, "NhomHang");
            lblTongCong.DataBindings.Add("Text", this.DataSource, "TriGiaKB", "{0:n2}");
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            this.STT = 0;
        }
        private string ConvertToSoLaMa(int i)
        {
            switch (i)
            {
                case 1: return "I";
                case 2: return "II";
                case 3: return "III";
                case 4: return "IV";
                case 5: return "V";
                case 6: return "VI";
                case 7: return "VII";
                case 8: return "VIII";
                case 9: return "IX";
                case 10: return "X";
                case 11: return "XI";
                case 12: return "XII";
                case 13: return "XIII";
                case 14: return "XIV";
                case 15: return "XV"; 
                default: return "";
            }
        }

        private void lblSTTNhomHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STTNhom ++;
            lblSTTNhomHang.Text = ConvertToSoLaMa(this.STTNhom);
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDVT.Text = DonViTinh.GetName(lblDVT.Text);
            if (GlobalSettings.MaHTS == 1) lblDVT.Text += "\n\r(" + DonViTinh.GetName(GetCurrentColumnValue("DVT_HTS")) + ")"; 
        }
        private ArrayList GetNhomHang()
        {
            ArrayList arr = new ArrayList();
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                if (!CheckExitNhomHang(hmd.NhomHang, arr)) arr.Add(hmd.NhomHang);
            }
            return arr;
        }
        private bool CheckExitNhomHang(string nhomHang, ArrayList arr)
        {
            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i].ToString() == nhomHang) return true;
            }
            return false;
        }

        private void lblTenHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GlobalSettings.MaHTS == 1) lblTenHang.Text = GetCurrentColumnValue("Ma_HTS").ToString() + ";\n\r" + GetCurrentColumnValue("TenHang").ToString();
        }

        private void lblLuong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GlobalSettings.MaHTS == 1) lblLuong.Text = Convert.ToDecimal(GetCurrentColumnValue("SoLuong")).ToString("G15") + "\n\r(" + Convert.ToDecimal(GetCurrentColumnValue("SoLuong_HTS")).ToString("G15") + ")";
        }
    }
}
