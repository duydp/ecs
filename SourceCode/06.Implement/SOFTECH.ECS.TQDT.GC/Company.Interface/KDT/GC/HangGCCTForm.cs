﻿using System;
using System.Drawing;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL;
using Company.GC.BLL.Utils;

using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using Company.GC.BLL.GC;

namespace Company.Interface.KDT.GC
{
    public partial class HangGCCTForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        private decimal clg;
        private decimal dgnt;
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        public ToKhaiChuyenTiep TKCT;
        private bool IsExit = true;

        private decimal luong;
        private decimal st_clg;
        private bool edit = false;
        //-----------------------------------------------------------------------------------------				
        private decimal tgnt;
        private decimal tgtt_gtgt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tl_clg;
        private decimal tongTGKB;
        private decimal ts_gtgt;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal tt_gtgt;

        private decimal tt_nk;
        private decimal tt_ttdb;
        public decimal TyGiaTT;
        //      NguyenPhuLieuRegistedForm NPLRegistedForm;
        //        SanPhamRegistedForm SPRegistedForm;
        private decimal LuongCon = 0;
        //-----------------------------------------------------------------------------------------

        public HangGCCTForm()
        {
            InitializeComponent();
        }
        private void SetVisibleHTS(bool b)
        {
            txtMa_HTS.Visible = lblMa_HTS.Visible = txtSoLuong_HTS.Visible = lblSoLuong_HTS.Visible = cbbDVT_HTS.Visible = lblDVT_HTS.Visible = b;
            if (!b)
            {
                txtMaHS.Width = txtTenHang.Width = cbDonViTinh.Width = txtLuong.Width = txtMaHang.Width;
            }
        }
        private void khoitao_GiaoDien()
        {
            if (this.TKCT.MaLoaiHinh.Substring(0, 3).Equals("NGC")) { SetVisibleHTS(false); }
            else
                lblLuongCon.Text = "Lượng còn được xuất";
            //if (this.TKCT.LoaiHangHoa != "S" || TKCT.MaMid == "") SetVisibleHTS(false);
            if (this.TKCT.MaLoaiHinh.Substring(0, 1) != "S" || GlobalSettings.MaMID.Trim() == "") SetVisibleHTS(false);
            if (this.TKCT.MaLoaiHinh.Substring(0, 1) == "N")
            {
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            }
            else
            {
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                txtSoLuong_HTS.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
            }

            txtTriGiaKB.DecimalDigits = GlobalSettings.TriGiaNT;
            txtTGNT.DecimalDigits = GlobalSettings.TriGiaNT;
            txtTGTT_NK.DecimalDigits = GlobalSettings.TriGiaNT;
            txtTGTT_TTDB.DecimalDigits = GlobalSettings.TriGiaNT;
            txtTGTT_GTGT.DecimalDigits = GlobalSettings.TriGiaNT;
            txtCLG.DecimalDigits = GlobalSettings.TriGiaNT;
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll();

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll();
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);

            cbbDVT_HTS.DataSource = this._DonViTinh;
            cbbDVT_HTS.SelectedValue = "15 ";
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;

            switch (TKCT.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "SP":
                    if (TKCT.MaLoaiHinh.EndsWith("X"))
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                        txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                    }
                    else
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                        txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    }
                    break;
                case "TB":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
            }

            khoitao_DuLieuChuanUpdate();
        }
        private void khoitao_DuLieuChuanUpdate()
        {
            //TODO: Hungtq update 22/02/2011
            switch (TKCT.MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "19"://"SP":
                    if (TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                    }
                    else
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    }
                    break;
                case "20"://"TB":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    break;
            }
        }

        private decimal tinhthue2()
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Text);
            this.luong = Convert.ToDecimal(txtLuong.Text);
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;

            this.tgnt = this.dgnt * Convert.ToDecimal(this.luong);
            this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            if (tgtt_nk == 0)
            {
                tgtt_nk = tgnt * TyGiaTT;
            }

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private decimal tinhthue()
        {
            this.tgnt = this.dgnt * Convert.ToDecimal(this.luong);
            this.tgtt_nk = this.tgnt * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
            {
                if (hmd.MaHang.Trim().ToUpper() == maHang.Trim().ToUpper()) return true;
            }
            return false;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("XGC"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKCT.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                        return;
                    }
                }
            }
            KiemTraTonTaiHang();
            txtMaHang.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                return;
            }

            if (checkMaHangExit(txtMaHang.Text))
            {
                showMsg("MSG_2702007");
                //ShowMessage("Mặt hàng này đã tồn tại trên lưới, vui lòng chọn lại.", false);
                return;
            }

            HangChuyenTiep hmd = new HangChuyenTiep();
            hmd.SoThuTuHang = 0;
            hmd.MaHS = txtMaHS.Text;
            hmd.MaHang = txtMaHang.Text;
            hmd.TenHang = txtTenHang.Text;
            hmd.ID_NuocXX = ctrNuocXX.Ma;
            hmd.ID_DVT = cbDonViTinh.SelectedValue.ToString();
            hmd.SoLuong = Convert.ToDecimal(txtLuong.Value);
            hmd.DonGia = Convert.ToDecimal(txtDGNT.Value);
            hmd.Ma_HTS = txtMa_HTS.Text;
            hmd.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
            hmd.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
            hmd.TriGia = Convert.ToDecimal(txtTGNT.Value);
            hmd.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
            hmd.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
            hmd.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
            hmd.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
            hmd.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
            hmd.TyLeThuKhac = Convert.ToDecimal(txtTL_CLG.Value);
            hmd.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Value), 0);
            hmd.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Value), 0);
            hmd.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Value), 0);
            hmd.DonGiaTT = hmd.TriGiaTT / Convert.ToDecimal(hmd.SoLuong);
            hmd.TriGiaThuKhac = Convert.ToDecimal(txtTien_CLG.Value);
            hmd.ThueSuatXNKGiam = txtTSXNKGiam.Text.Trim();
            hmd.ThueSuatTTDBGiam = txtTSTTDBGiam.Text.Trim();
            hmd.ThueSuatVATGiam = txtTSVatGiam.Text.Trim();
            if (chkMienThue.Checked)
                hmd.MienThue = 1;
            else
                hmd.MienThue = 0;
            epError.Clear();

            this.TKCT.HCTCollection.Add(hmd);
            TinhTongTriGiaNT();

            reset();
        }
        private void reset()
        {
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
            txtMaHang.Text = "";
            txtTenHang.Text = "";
            txtMaHS.Text = "";
            txtMa_HTS.Text = "";
            txtLuong.Value = 0;
            txtDGNT.Value = 0;
            txtSoLuong_HTS.Value = 0;
            txtTGNT.Value = 0;
            txtTriGiaKB.Value = 0;
            txtSoLuongConDcNhap.Value = 0;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            ctrNuocXX.Ma = GlobalSettings.NUOC;
            txtTSVatGiam.Text = "";
            txtTSXNKGiam.Text = "";
            txtTSTTDBGiam.Text = "";
        }
        //-----------------------------------------------------------------------------------------
        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            reSetError();

            switch (this.TKCT.MaLoaiHinh.Substring(2, 2))
            {
                #region NPL
                case "PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text.Trim();
                    if (npl.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = npl.Ma;
                        if (txtMaHS.Text.Trim().Length == 0)
                            txtMaHS.Text = npl.MaHS;
                        txtTenHang.Text = npl.Ten;
                        cbDonViTinh.SelectedValue = npl.DVT_ID;
                        if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                        {
                            LuongCon = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                            txtSoLuongConDcNhap.Text = LuongCon.ToString();
                        }
                        else
                        {
                            LuongCon = npl.SoLuongDaNhap - npl.SoLuongDaDung;//+ npl.SoLuongCungUng
                            txtSoLuongConDcNhap.Text = LuongCon.ToString();
                        }
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);

                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                    break;
                #endregion

                #region SP
                case "SP":
                    if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text.Trim();
                        if (npl1.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = npl1.Ma;
                            txtTenHang.Text = npl1.Ten;
                            txtMaHS.Text = npl1.MaHS;
                            cbDonViTinh.SelectedValue = npl1.DVT_ID;
                            if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                            {
                                decimal SoLuongConDuocNhap = npl1.SoLuongDangKy - npl1.SoLuongDaNhap - npl1.SoLuongCungUng;
                                txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                            }
                            epError.SetError(txtMaHang, null);
                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại mã hàng này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = sp.Ma;
                            txtTenHang.Text = sp.Ten;
                            txtMaHS.Text = sp.MaHS;
                            cbDonViTinh.SelectedValue = sp.DVT_ID;
                            txtLuong.Focus();
                            if (this.TKCT.MaLoaiHinh.EndsWith("X"))
                            {
                                decimal SoLuongConDuocNhap = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                                txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                            }
                            epError.SetError(txtMaHang, null);
                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                #endregion

                #region TB
                case "TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = tb.Ma;
                        txtTenHang.Text = tb.Ten;
                        txtMaHS.Text = tb.MaHS;
                        cbDonViTinh.SelectedValue = tb.DVT_ID;
                        epError.SetError(txtMaHang, string.Empty);
                        if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = tb.SoLuongDaNhap;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocGiao.ToString();
                        }
                        epError.SetError(txtMaHang, null);
                        txtLuong.Focus();
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                    break;
                #endregion
            }
        }
        private void txtMaHang_LeaveUpdate()
        {
            //TODO: Update by Hungtq 22/02/2011
            switch (this.TKCT.MaLoaiHinh.Substring(3, 2))
            {
                #region NPL
                case "18"://"PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text.Trim();
                    if (npl.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = npl.Ma;
                        if (txtMaHS.Text.Trim().Length == 0)
                            txtMaHS.Text = npl.MaHS;
                        txtTenHang.Text = npl.Ten;
                        cbDonViTinh.SelectedValue = npl.DVT_ID;
                        if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                        {
                            LuongCon = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                            txtSoLuongConDcNhap.Text = LuongCon.ToString();
                        }
                        else
                        {
                            LuongCon = npl.SoLuongDaNhap - npl.SoLuongDaDung;//+ npl.SoLuongCungUng
                            txtSoLuongConDcNhap.Text = LuongCon.ToString();
                        }
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);

                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                    break;
                #endregion

                #region SP
                case "19"://"SP":
                    if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text.Trim();
                        if (npl1.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = npl1.Ma;
                            txtTenHang.Text = npl1.Ten;
                            txtMaHS.Text = npl1.MaHS;
                            cbDonViTinh.SelectedValue = npl1.DVT_ID;
                            if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                            {
                                decimal SoLuongConDuocNhap = npl1.SoLuongDangKy - npl1.SoLuongDaNhap - npl1.SoLuongCungUng;
                                txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                            }
                            epError.SetError(txtMaHang, null);
                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại mã hàng này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = sp.Ma;
                            txtTenHang.Text = sp.Ten;
                            txtMaHS.Text = sp.MaHS;
                            cbDonViTinh.SelectedValue = sp.DVT_ID;
                            txtLuong.Focus();
                            if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("X"))
                            {
                                decimal SoLuongConDuocNhap = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                                txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                            }
                            epError.SetError(txtMaHang, null);
                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                #endregion

                #region TB
                case "20"://"TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = tb.Ma;
                        txtTenHang.Text = tb.Ten;
                        txtMaHS.Text = tb.MaHS;
                        cbDonViTinh.SelectedValue = tb.DVT_ID;
                        epError.SetError(txtMaHang, string.Empty);
                        if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                        {
                            decimal SoLuongConDuocNhap = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = tb.SoLuongDaNhap;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocGiao.ToString();
                        }
                        epError.SetError(txtMaHang, null);
                        txtLuong.Focus();
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                    break;
                #endregion
            }
        }

        private void KiemTraTonTaiHang()
        {
            switch (this.TKCT.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "SP":
                    if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text;
                        if (npl1.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }

            KiemTraTonTaiHangUpdate();
        }
        private void KiemTraTonTaiHangUpdate()
        {
            switch (this.TKCT.MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "19"://"SP":
                    if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text;
                        if (npl1.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "20"://"TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }
        }

        private void TinhTongTriGiaNT()
        {
            this.tongTGKB = 0;
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
            {
                this.tongTGKB += hmd.TriGia;
            }


            lblTongTGKB.Text = string.Format("Tổng trị giá khai khai báo : {0} ({1})", this.tongTGKB.ToString("N"), this.TKCT.NguyenTe_ID);
        }
        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            revMaHS.ValidationExpression = @"\d{8}";

            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();

            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.TKCT.NguyenTe_ID + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.TKCT.NguyenTe_ID);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.TKCT.NguyenTe_ID);

            TinhTongTriGiaNT();
            lblTyGiaTT.Text += " : " + this.TyGiaTT.ToString("N");
            if (this.OpenType == OpenFormType.View)
                btnAddNew.Visible = false;
            dgList.DataSource = this.TKCT.HCTCollection;
        }

        private void reSetError()
        {
            epError.SetError(txtMaHang, null);
            epError.SetError(txtTenHang, null);
            epError.SetError(txtMaHS, null);
        }
        private void txtLuong_Leave(object sender, EventArgs e)
        {
            try
            {
                //reSetError();
                this.luong = Convert.ToDecimal(txtLuong.Value);
                LuongCon = Convert.ToDecimal(txtSoLuongConDcNhap.Text);
                txtSoLuong_HTS.Value = Math.Round(this.luong / 12);
                if (this.luong > LuongCon)
                {
                    if (TKCT.MaLoaiHinh.Substring(0, 3).Equals("NGC"))
                    {
                        //if (!(MLMessages("Bạn đã nhập quá số lượng có thể nhập.Bạn có muốn tiếp tục không?","MSG_WRN16","", true) == "Yes"))
                        if (!(showMsg("MSG_WRN07", true) == "Yes"))
                        {
                            luong = 0;
                            txtLuong.Value = "0";
                        }
                    }
                    else
                    {
                        //if (!(MLMessages("Bạn đã xuất quá số lượng có thể xuất.Bạn có muốn tiếp tục không?", "MSG_WRN16", "", true) == "Yes"))
                        if (!(showMsg("MSG_WRN08", true) == "Yes"))
                        {
                            luong = 0;
                            txtLuong.Value = "0";
                        }
                    }
                }
                LuongCon = 0;
                this.tinhthue();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                return;
            }
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            try
            {
                this.dgnt = Convert.ToDecimal(txtDGNT.Value);
                this.tinhthue();
            }
            catch (Exception ex)
            {
                // showMsg("MSG_2702004", ex.Message);
                return;
            }
        }

        //-----------------------------------------------------------------------------------------------       

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["ID_NuocXX"].Value != null)
                e.Row.Cells["ID_NuocXX"].Text = this.Nuoc_GetName(e.Row.Cells["ID_NuocXX"].Value.ToString());
            e.Row.Cells["ID_DVT"].Text = this.DonViTinh_GetName(e.Row.Cells["ID_DVT"].Text);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //    if (txtMaHang.Text != "")
            //    {
            //        //if (ShowMessage("Bạn có muốn lưu lại thông tin đã nhập không?", true) == "Yes")
            //        if (Message("MSG_WRN12", "", false) == "Yes")
            //        {
            //            return;
            //        }
            //        else
            //        {
            //            this.Close();

            //        }
            //    }
            this.Close();


        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.TKCT.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    Company.Interface.GC.NguyenPhuLieuRegistedGCForm fNPL = new Company.Interface.GC.NguyenPhuLieuRegistedGCForm();
                    fNPL.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                    fNPL.isBrower = true;
                    fNPL.ShowDialog();
                    if (fNPL.NguyenPhuLieuSelected.Ten != "" && fNPL.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = fNPL.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = fNPL.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = fNPL.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = fNPL.NguyenPhuLieuSelected.DVT_ID;
                        if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = fNPL.NguyenPhuLieuSelected.SoLuongDangKy - fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongDaDung; //+ fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuong.Focus();
                    }
                    break;

                case "SP":
                    if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.Interface.GC.NguyenPhuLieuRegistedGCForm fNPL1 = new Company.Interface.GC.NguyenPhuLieuRegistedGCForm();
                        fNPL1.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                        fNPL1.isBrower = true;
                        fNPL1.ShowDialog();
                        if (fNPL1.NguyenPhuLieuSelected.Ten != "" && fNPL1.NguyenPhuLieuSelected.Ma != "")
                        {
                            txtMaHang.Text = fNPL1.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = fNPL1.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = fNPL1.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = fNPL1.NguyenPhuLieuSelected.DVT_ID;
                            if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                            {
                                decimal SoLuongConDuocNhap = fNPL1.NguyenPhuLieuSelected.SoLuongDangKy - fNPL1.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL1.NguyenPhuLieuSelected.SoLuongCungUng;
                                txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                            }
                            txtLuong.Focus();
                        }
                    }
                    else
                    {
                        Company.Interface.GC.SanPhamRegistedGCForm fsp = new Company.Interface.GC.SanPhamRegistedGCForm();
                        fsp.SanPhamSelected.HopDong_ID = HD.ID;
                        fsp.isBrower = true;
                        fsp.ShowDialog();
                        if (fsp.SanPhamSelected.Ma != "")
                        {
                            txtMaHang.Text = fsp.SanPhamSelected.Ma;
                            txtTenHang.Text = fsp.SanPhamSelected.Ten;
                            txtMaHS.Text = fsp.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = fsp.SanPhamSelected.DVT_ID;
                            txtLuong.Focus();
                        }
                    }
                    break;

                case "TB":
                    Company.Interface.GC.ThietBiRegistedForm ftb = new Company.Interface.GC.ThietBiRegistedForm();
                    ftb.ThietBiSelected.HopDong_ID = HD.ID;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (ftb.ThietBiSelected.Ma != "")
                    {
                        txtMaHang.Text = ftb.ThietBiSelected.Ma;
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                        cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                        if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = ftb.ThietBiSelected.SoLuongDangKy - ftb.ThietBiSelected.SoLuongDaNhap;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = ftb.ThietBiSelected.SoLuongDaNhap;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuong.Focus();
                    }
                    break;
            }

            txtMaHang_ButtonClickUpdate();
        }
        private void txtMaHang_ButtonClickUpdate()
        {
            //TODO: Update by Hungtq 22/02/2011
            switch (this.TKCT.MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":
                    Company.Interface.GC.NguyenPhuLieuRegistedGCForm fNPL = new Company.Interface.GC.NguyenPhuLieuRegistedGCForm();
                    fNPL.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                    fNPL.isBrower = true;
                    fNPL.ShowDialog();
                    if (fNPL.NguyenPhuLieuSelected.Ten != "" && fNPL.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = fNPL.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = fNPL.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = fNPL.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = fNPL.NguyenPhuLieuSelected.DVT_ID;
                        if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                        {
                            decimal SoLuongConDuocNhap = fNPL.NguyenPhuLieuSelected.SoLuongDangKy - fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongDaDung; //+ fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuong.Focus();
                    }
                    break;

                case "19"://"SP":
                    if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        Company.Interface.GC.NguyenPhuLieuRegistedGCForm fNPL1 = new Company.Interface.GC.NguyenPhuLieuRegistedGCForm();
                        fNPL1.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                        fNPL1.isBrower = true;
                        fNPL1.ShowDialog();
                        if (fNPL1.NguyenPhuLieuSelected.Ten != "" && fNPL1.NguyenPhuLieuSelected.Ma != "")
                        {
                            txtMaHang.Text = fNPL1.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = fNPL1.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = fNPL1.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = fNPL1.NguyenPhuLieuSelected.DVT_ID;
                            if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                            {
                                decimal SoLuongConDuocNhap = fNPL1.NguyenPhuLieuSelected.SoLuongDangKy - fNPL1.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL1.NguyenPhuLieuSelected.SoLuongCungUng;
                                txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                            }
                            txtLuong.Focus();
                        }
                    }
                    else
                    {
                        Company.Interface.GC.SanPhamRegistedGCForm fsp = new Company.Interface.GC.SanPhamRegistedGCForm();
                        fsp.SanPhamSelected.HopDong_ID = HD.ID;
                        fsp.isBrower = true;
                        fsp.ShowDialog();
                        if (fsp.SanPhamSelected.Ma != "")
                        {
                            txtMaHang.Text = fsp.SanPhamSelected.Ma;
                            txtTenHang.Text = fsp.SanPhamSelected.Ten;
                            txtMaHS.Text = fsp.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = fsp.SanPhamSelected.DVT_ID;
                            txtLuong.Focus();
                        }
                    }
                    break;

                case "20"://"TB":
                    Company.Interface.GC.ThietBiRegistedForm ftb = new Company.Interface.GC.ThietBiRegistedForm();
                    ftb.ThietBiSelected.HopDong_ID = HD.ID;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (ftb.ThietBiSelected.Ma != "")
                    {
                        txtMaHang.Text = ftb.ThietBiSelected.Ma;
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                        cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                        if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                        {
                            decimal SoLuongConDuocNhap = ftb.ThietBiSelected.SoLuongDangKy - ftb.ThietBiSelected.SoLuongDaNhap;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = ftb.ThietBiSelected.SoLuongDaNhap;
                            txtSoLuongConDcNhap.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuong.Focus();
                    }
                    break;
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangGCCTEditForm f = new HangGCCTEditForm();
                    f.HCT = (Company.GC.BLL.KDT.GC.HangChuyenTiep)e.Row.DataRow;
                    f.HD.ID = this.TKCT.IDHopDong;
                    f.TKCT = this.TKCT;
                    f.TyGiaTT = this.TyGiaTT;
                    if (this.TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        f.OpenType = OpenFormType.Edit;
                    else
                        f.OpenType = OpenFormType.View;
                    f.ShowDialog();
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { dgList.Refresh(); }

                    this.tongTGKB = 0;
                    foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
                    {
                        // Tính lại thuế.
                        //hmd.TinhThue(this.TyGiaTT);
                        // Tổng trị giá khai báo.
                        this.tongTGKB += hmd.TriGia;
                    }
                    lblTongTGKB.Text += string.Format(" : {0} ({1})", this.tongTGKB.ToString("N"), this.TKCT.NguyenTe_ID);
                }
                break;
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("XGC"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKCT.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                        e.Cancel = true;
                        return;
                    }
                }
            }
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangChuyenTiep hct = (HangChuyenTiep)i.GetRow().DataRow;
                        if (hct.ID > 0)
                        {
                            string LoaiHangHoa = "N";
                            if (TKCT.MaLoaiHinh.IndexOf("SP") > 0 || TKCT.MaLoaiHinh.IndexOf("19") > 0)
                                LoaiHangHoa = "S";
                            else if (TKCT.MaLoaiHinh.IndexOf("TB") > 0 || TKCT.MaLoaiHinh.IndexOf("20") > 0)
                                LoaiHangHoa = "T";
                            if (hct.ID > 0)
                            {
                                hct.Delete(TKCT.IDHopDong, LoaiHangHoa, TKCT.MaLoaiHinh);
                            }
                        }
                    }
                }
                TinhTongTriGiaNT();

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void SaoChepCha_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            HangChuyenTiep hmd = (HangChuyenTiep)dgList.GetRow().DataRow;
            txtMaHang.Text = hmd.MaHang;
            txtTenHang.Text = hmd.TenHang;
            txtMaHS.Text = hmd.MaHS;
            cbDonViTinh.SelectedValue = hmd.ID_DVT;
            ctrNuocXX.Ma = hmd.ID_NuocXX;
            txtDGNT.Value = this.dgnt = hmd.DonGia;
            txtLuong.Value = this.luong = hmd.SoLuong;
            txtTGNT.Value = hmd.TriGia;
            txtTriGiaKB.Value = hmd.TriGiaKB_VND;
            txtLuong.Focus();
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã HS không hợp lệ.", "This value is invalid"));
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
            string MoTa = MaHS.CheckExist(txtMaHS.Text);
            if (MoTa == "")
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã HS không có trong danh mục mã HS.", "This value is not exist"));
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
        }

        private void lblMa_HTS_Click(object sender, EventArgs e)
        {

        }

        private void txtMa_HTS_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblDVT_HTS_Click(object sender, EventArgs e)
        {

        }

        private void cbbDVT_HTS_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void cbDonViTinh_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void toolTip1_Popup(object sender, System.Windows.Forms.PopupEventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("XGC"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKCT.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                        return;
                    }
                }
                //TKCT.Delete();

            }
            HangChuyenTiepCollection hmdcoll = new HangChuyenTiepCollection();
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangChuyenTiep hct = (HangChuyenTiep)i.GetRow().DataRow;
                        if (hct.ID > 0)
                        {
                            string LoaiHangHoa = "N";
                            if (TKCT.MaLoaiHinh.IndexOf("SP") > 0 || TKCT.MaLoaiHinh.IndexOf("19") > 0)
                                LoaiHangHoa = "S";
                            else if (TKCT.MaLoaiHinh.IndexOf("TB") > 0 || TKCT.MaLoaiHinh.IndexOf("20") > 0)
                                LoaiHangHoa = "T";
                            if (hct.ID > 0)
                            {
                                hct.Delete(TKCT.IDHopDong, LoaiHangHoa, TKCT.MaLoaiHinh);
                            }
                        }
                        hmdcoll.Add(hct);
                        //hmdcoll.Remove(hct);
                    }
                }
                foreach (HangChuyenTiep hmdDelete in hmdcoll)
                {
                    this.TKCT.HCTCollection.Remove(hmdDelete);
                }
                //this.TKCT.HCTCollection = hmdcoll;
                //dgList.DataSource = hmdcoll;   
                dgList.DataSource = this.TKCT.HCTCollection;
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
                TinhTongTriGiaNT();
            }
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            tinhthue2();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            tinhthue2();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            tinhthue2();
        }

        private void txtTL_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
            tinhthue2();
        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue2();
        }

        private void txtTGNT_Leave(object sender, EventArgs e)
        {
            decimal trigia = Convert.ToDecimal(txtTGNT.Text);
            decimal luong = Convert.ToDecimal(txtLuong.Text);
            if (luong == 0)
                luong = 1;
            decimal dongia = trigia / luong;
            txtDGNT.Text = dongia.ToString();
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            this.tinhthue();
        }
    }
}
