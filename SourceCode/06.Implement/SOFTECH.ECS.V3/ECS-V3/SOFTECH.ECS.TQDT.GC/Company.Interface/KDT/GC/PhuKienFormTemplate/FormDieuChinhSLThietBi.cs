﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;

using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;


namespace Company.Interface.KDT.GC.PCTFormTemplate
{
    public partial class FormDieuChinhSLThietBi : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        public bool boolFlag;
        private string dvt = string.Empty;
        public string MaLoaiPK = string.Empty;

        public FormDieuChinhSLThietBi()
        {
            InitializeComponent();
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (txtMaNPL.Text.Trim() == "")
                return;
            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
            tb.HopDong_ID = this.HD.ID;
            tb.Ma = txtMaNPL.Text.Trim();
            if (tb.Load())
            {
                if (tb.TrangThai == 0)
                {
                    txtMaNPL.Text = tb.Ma.Trim();
                    txtTenNPL.Text = tb.Ten.Trim();
                    txtMaHSNPL.Text = tb.MaHS.Trim();
                    dvt = tb.DVT_ID;
                    cbDonViTinh.SelectedValue = (tb.DVT_ID);
                    txtSoLuongCu.Text = tb.SoLuongDangKy.ToString();
                    decimal LuongConDCNhap = tb.SoLuongDaNhap;
                    txtLuongDaNhap.Text = LuongConDCNhap.ToString();                
                    txtSoLuongMoi.Focus();
                }
                else if (tb.TrangThai != 0 && HangPK.ID > 0)
                {
                    txtMaNPL.Text = tb.Ma.Trim();
                    txtTenNPL.Text = tb.Ten.Trim();
                    txtMaHSNPL.Text = tb.MaHS.Trim();
                    dvt = tb.DVT_ID;
                    cbDonViTinh.SelectedValue = (tb.DVT_ID);
                    decimal LuongConDCNhap = tb.SoLuongDaNhap;
                    txtLuongDaNhap.Text = LuongConDCNhap.ToString();                
                    txtSoLuongMoi.Focus();
                }
                else
                {
                    error.SetError(txtMaNPL, setText("Thiết bị này đang được điều chỉnh nhưng chưa được hải quan duyệt nên không điều chỉnh số lượng được.", "This equipment have been updated and is waiting approval so can not update quanlity now"));
                    txtMaNPL.Focus();
                    return;
                }
            }
            else
            {
                error.SetError(txtMaNPL, setText("Không tồn tại thiết bị này.","This value is not exist"));
                txtMaNPL.Focus();
                return;
            }
        }


        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            Company.Interface.GC.ThietBiRegistedForm f = new Company.Interface.GC.ThietBiRegistedForm();
            f.ThietBiSelected.HopDong_ID = this.HD.ID;
            f.isDisplayAll = 2;
            //f.ThietBiCollection = ThietBiCollection;
            f.isBrower = true;
            f.ShowDialog(this);
            if (f.ThietBiSelected != null && f.ThietBiSelected.Ma.Length > 0)
            {
                txtMaNPL.Text = f.ThietBiSelected.Ma.Trim();
                txtTenNPL.Text = f.ThietBiSelected.Ten.Trim();
                txtMaHSNPL.Text = f.ThietBiSelected.MaHS.Trim();
                dvt = f.ThietBiSelected.DVT_ID;
                cbDonViTinh.SelectedValue = (f.ThietBiSelected.DVT_ID);
                txtSoLuongCu.Text = f.ThietBiSelected.SoLuongDangKy.ToString();
                decimal LuongConDCNhap = f.ThietBiSelected.SoLuongDaNhap;
                txtLuongDaNhap.Text = LuongConDCNhap.ToString();
                txtSoLuongMoi.Focus();
            }
            else
            {
                error.SetError(txtMaNPL, setText("Không tồn tại thiết bị này.","This value is not exist"));
                txtMaNPL.Focus();
                return;
            }
        }
        private bool CheckNPLExit(string maNPL)
        {
            foreach (HangPhuKien pk1 in LoaiPK.HPKCollection)
            {
                if (pk1.MaHang == maNPL)
                {
                    return true;
                }
            }
            return false; ;
        }
        private void reset()
        {
            txtMaHSNPL.Text = "";
            txtMaNPL.Text = "";
            txtSoLuongCu.Text = "0";
            txtSoLuongMoi.Text = "0";
            txtTenNPL.Text = "";
            txtMaNPL.Focus();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            HangPK = new HangPhuKien();


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                txtMaNPL.Focus();
                cvError.Validate();
                if (!cvError.IsValid) return;
                {
                    if (Convert.ToDecimal(txtSoLuongMoi.Text) == 0)
                    {
                        error.SetError(txtSoLuongMoi, setText("Số lượng phải lớn hơn 0.","This value must be greater than 0"));
                        return;
                    }
                }
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                tb.HopDong_ID = this.HD.ID;
                tb.Ma = txtMaNPL.Text.Trim();
                if (!tb.Load())
                {
                    showMsg("MSG_240217");
                    //ShowMessage("Thiết bị này không có trong hệ thống.", false);
                    return;
                }
                LoaiPK.HPKCollection.Remove(HangPK);
                if (CheckNPLExit(txtMaNPL.Text.Trim()))
                {
                    //ShowMessage("Đã điều chỉnh thiết bị này rồi", false);
                    showMsg("MSG_240218");
                    if (HangPK.MaHang.Trim() != "")
                        LoaiPK.HPKCollection.Add(HangPK);
                    return;
                }

                HangPK.MaHang = txtMaNPL.Text.Trim();
                HangPK.TenHang = txtTenNPL.Text.Trim();
                HangPK.SoLuong = Convert.ToDecimal(txtSoLuongMoi.Text);
                HangPK.MaHS = txtMaHSNPL.Text.Trim();
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                HangPK.ThongTinCu = txtSoLuongCu.Text.Trim();
                HangPK.NuocXX_ID = tb.NuocXX_ID;
                HangPK.NguyenTe_ID = tb.NguyenTe_ID;
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                if (boolFlag == true)
                {
                    LoaiPK.InsertUpdateDieuChinhSoLuongThietBiDaDangKy(pkdk.HopDong_ID);
                    boolFlag = false;
                }
                else 
                {
                    LoaiPK.InsertUpdateDieuChinhSoLuongThietBi(pkdk.HopDong_ID);
                }
                
                //PhuKienGCSendForm.isEdit = true;
                reset();
            }
            catch (Exception ex)
            {
                //showMsg("MSG_2702004", ex.Message);
                ShowMessage(setText(" Dữ liệu không hợp lệ. Lỗi  : ","Invalid value. Error ") + ex.Message, false);
            }
        }


        private void DieuChinhSoLuongNPLForm_Load(object sender, EventArgs e)
        {
            txtLuongDaNhap.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtSoLuongCu.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtSoLuongMoi.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["SoLuongDK"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["ThongTinCu"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";

            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "T01")
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                btnXoa.Enabled = false;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                btnXoa.Enabled = true ;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {

                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                btnXoa.Enabled = false;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            //Registered
            if (boolFlag == true)
            {
                btnUpdate.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            LoaiPK.MaPhuKien = "T01";
            dgList.DataSource = LoaiPK.HPKCollection;
        }


        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            HangPK = (Company.GC.BLL.KDT.GC.HangPhuKien)e.Row.DataRow;
            txtMaNPL.Text = HangPK.MaHang.Trim();
            txtMaNPL_Leave(null, null);
            txtSoLuongMoi.Text = HangPK.SoLuong.ToString();
            txtSoLuongCu.Text = HangPK.ThongTinCu;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa thiết bị này không?", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                            //pkDelete.DeleteTransaction(null);
                        }
                    }
                }

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void FormDieuChinhSLThietBi_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (LoaiPK.HPKCollection.Count > 0)
            {
                LoaiPK.MaPhuKien = "T01";
                LoaiPK.NoiDung = LoaiPhuKien_GetName("T01");
                pkdk.PKCollection.Remove(LoaiPK);
                pkdk.PKCollection.Add(LoaiPK);
            }
            else
            {
                pkdk.PKCollection.Remove(LoaiPK);
                LoaiPK.Delete(pkdk.HopDong_ID); ;
                //LoaiPK.DeleteTransaction(null);
            }
        }

        private void txtSoLuongMoi_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtLuongDaNhap.Text) > Convert.ToDecimal(txtSoLuongMoi.Text))
            {
                if (showMsg("MSG_240222", true) == "Yes")
                //if (ShowMessage("Lượng điều chỉnh không được nhỏ hơn lượng đã nhập.Bạn có muốn tiếp tục không?", true) == "Yes")
                {
                    ;
                }
                else
                {
                    txtSoLuongMoi.Text = "0";
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            HangPhuKienCollection hpkColl = new HangPhuKienCollection();
            if (LoaiPK.HPKCollection.Count <= 0) return ;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;

            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa thiết bị này không?", true) == "Yes")
            {
                
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        try 
                        { 
                            if (pkDelete.ID > 0)
                            {
                                pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                                //pkDelete.DeleteTransaction(null);
                            }
                            hpkColl.Add(pkDelete);  
                            
                        }
                        catch { }
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.HangPhuKien hpkk in hpkColl)
                {
                    try
                    {
                        LoaiPK.HPKCollection.Remove(hpkk);
                    }
                    catch { }

                }
                dgList.DataSource = LoaiPK.HPKCollection;
                reset();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }

            }
        }

        private void txtLuongDaNhap_Click(object sender, EventArgs e)
        {

        }

    }
}