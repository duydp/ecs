﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface.GC
{
    public partial class UpdateDataNumber : BaseForm
    {
        public T_GC_NPL_QUYETOAN NPLQuyetToan = new T_GC_NPL_QUYETOAN();
        public UpdateDataNumber()
        {
            InitializeComponent();
            this._DonViTinh = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
            txtDonViTinhNPL.ReadOnly = false;
            txtDonViTinhNPL.DataSource = this._DonViTinh;
            txtDonViTinhNPL.DisplayMember = "Ten";
            txtDonViTinhNPL.ValueMember = "ID";
            txtDonViTinhNPL.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
               SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
               using (SqlConnection connection = (SqlConnection)db.CreateConnection())
               {
                   connection.Open();
                   SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                   try
                   {
                       NPLQuyetToan.MANPL = txtMaNPL.Text.ToString();
                       NPLQuyetToan.TENNPL = txtTenNPL.Text.ToString();
                       NPLQuyetToan.DVT = txtDonViTinhNPL.Text.ToString();
                       NPLQuyetToan.LUONGTONDK = Convert.ToDecimal(txtSoLuong.Text.ToString());
                       NPLQuyetToan.TRIGIATONDK = Convert.ToDecimal(txtTriGia.Text.ToString());
                       NPLQuyetToan.UPDATE_TONDK(transaction);
                       transaction.Commit();
                   }
                   catch (Exception ex)
                   {
                       transaction.Rollback();
                       Logger.LocalLogger.Instance().WriteMessage(ex);
                   }
               }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void UpdateDataNumber_Load(object sender, EventArgs e)
        {
            try
            {
                txtMaNPL.Text = NPLQuyetToan.MANPL;
                txtTenNPL.Text = NPLQuyetToan.TENNPL;
                txtDonViTinhNPL.SelectedValue = NPLQuyetToan.DVT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
