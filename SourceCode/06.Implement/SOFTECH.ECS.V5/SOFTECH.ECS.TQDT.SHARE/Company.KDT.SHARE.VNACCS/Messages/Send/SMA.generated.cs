using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class SMA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute SMC { get; set; }
public PropertiesAttribute APN { get; set; }
public PropertiesAttribute FNC { get; set; }
public PropertiesAttribute APT { get; set; }
public PropertiesAttribute APP { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute IPC { get; set; }
public PropertiesAttribute IMA { get; set; }
public PropertiesAttribute ICC { get; set; }
public PropertiesAttribute IPN { get; set; }
public PropertiesAttribute IMF { get; set; }
public PropertiesAttribute IME { get; set; }
public PropertiesAttribute FSC { get; set; }
public PropertiesAttribute GMP { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute AD { get; set; }
public PropertiesAttribute RMK { get; set; }
public PropertiesAttribute SUD { get; set; }
public PropertiesAttribute PIC { get; set; }

public SMA()
        {
SMC = new PropertiesAttribute(13, typeof(string));
APN = new PropertiesAttribute(12, typeof(int));
FNC = new PropertiesAttribute(1, typeof(int));
APT = new PropertiesAttribute(4, typeof(string));
APP = new PropertiesAttribute(6, typeof(string));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
IPC = new PropertiesAttribute(7, typeof(string));
IMA = new PropertiesAttribute(300, typeof(string));
ICC = new PropertiesAttribute(2, typeof(string));
IPN = new PropertiesAttribute(20, typeof(string));
IMF = new PropertiesAttribute(20, typeof(string));
IME = new PropertiesAttribute(210, typeof(string));
FSC = new PropertiesAttribute(17, typeof(string));
GMP = new PropertiesAttribute(17, typeof(string));
EPC = new PropertiesAttribute(6, typeof(string));
EPN = new PropertiesAttribute(35, typeof(string));
AD = new PropertiesAttribute(750, typeof(string));
RMK = new PropertiesAttribute(996, typeof(string));
SUD = new PropertiesAttribute(8, typeof(DateTime));
PIC = new PropertiesAttribute(300, typeof(string));
TongSoByte = 3079;
}

}public partial class EnumGroupID
    {
}

}
