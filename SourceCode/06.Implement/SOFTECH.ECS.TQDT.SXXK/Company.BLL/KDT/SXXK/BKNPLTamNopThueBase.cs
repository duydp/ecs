using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Company.BLL.KDT.SXXK
{
	public partial class BKNPLTamNopThue 
	{
		#region Private members.
		
		protected long _ID;
		protected long _BangKeHoSoThanhLy_ID;
		protected int _SoToKhai;
		protected string _MaLoaiHinh = String.Empty;
		protected short _NamDangKy;
		protected string _MaHaiQuan = String.Empty;
		protected DateTime _NgayDangKy = new DateTime(1900, 01, 01);
		protected string _MaNPL = String.Empty;
		protected string _TenNPL = String.Empty;
		protected DateTime _NgayNopThue = new DateTime(1900, 01, 01);
		protected decimal _Nop_NK;
		protected decimal _Nop_VAT;
		protected decimal _Nop_TTDB;
		protected decimal _Nop_CLGia;
		protected string _So_TBT_QDDC = String.Empty;
		protected int _STTHang;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		public long BangKeHoSoThanhLy_ID
		{
			set {this._BangKeHoSoThanhLy_ID = value;}
			get {return this._BangKeHoSoThanhLy_ID;}
		}
		public int SoToKhai
		{
			set {this._SoToKhai = value;}
			get {return this._SoToKhai;}
		}
		public string MaLoaiHinh
		{
			set {this._MaLoaiHinh = value;}
			get {return this._MaLoaiHinh;}
		}
		public short NamDangKy
		{
			set {this._NamDangKy = value;}
			get {return this._NamDangKy;}
		}
		public string MaHaiQuan
		{
			set {this._MaHaiQuan = value;}
			get {return this._MaHaiQuan;}
		}
		public DateTime NgayDangKy
		{
			set {this._NgayDangKy = value;}
			get {return this._NgayDangKy;}
		}
		public string MaNPL
		{
			set {this._MaNPL = value;}
			get {return this._MaNPL;}
		}
		public string TenNPL
		{
			set {this._TenNPL = value;}
			get {return this._TenNPL;}
		}
		public DateTime NgayNopThue
		{
			set {this._NgayNopThue = value;}
			get {return this._NgayNopThue;}
		}
		public decimal Nop_NK
		{
			set {this._Nop_NK = value;}
			get {return this._Nop_NK;}
		}
		public decimal Nop_VAT
		{
			set {this._Nop_VAT = value;}
			get {return this._Nop_VAT;}
		}
		public decimal Nop_TTDB
		{
			set {this._Nop_TTDB = value;}
			get {return this._Nop_TTDB;}
		}
		public decimal Nop_CLGia
		{
			set {this._Nop_CLGia = value;}
			get {return this._Nop_CLGia;}
		}
		public string So_TBT_QDDC
		{
			set {this._So_TBT_QDDC = value;}
			get {return this._So_TBT_QDDC;}
		}
		public int STTHang
		{
			set {this._STTHang = value;}
			get {return this._STTHang;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_KDT_SXXK_BKNPLTamNopThue_Load";
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) this._BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) this._NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_NK"))) this._Nop_NK = reader.GetDecimal(reader.GetOrdinal("Nop_NK"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_VAT"))) this._Nop_VAT = reader.GetDecimal(reader.GetOrdinal("Nop_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_TTDB"))) this._Nop_TTDB = reader.GetDecimal(reader.GetOrdinal("Nop_TTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_CLGia"))) this._Nop_CLGia = reader.GetDecimal(reader.GetOrdinal("Nop_CLGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("So_TBT_QDDC"))) this._So_TBT_QDDC = reader.GetString(reader.GetOrdinal("So_TBT_QDDC"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static BKNPLTamNopThueCollection SelectCollectionBy_BangKeHoSoThanhLy_ID(long bangKeHoSoThanhLy_ID)
		{
			string spName = "p_KDT_SXXK_BKNPLTamNopThue_SelectBy_BangKeHoSoThanhLy_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, bangKeHoSoThanhLy_ID);
			
			BKNPLTamNopThueCollection collection = new BKNPLTamNopThueCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				BKNPLTamNopThue entity = new BKNPLTamNopThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) entity.NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_NK"))) entity.Nop_NK = reader.GetDecimal(reader.GetOrdinal("Nop_NK"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_VAT"))) entity.Nop_VAT = reader.GetDecimal(reader.GetOrdinal("Nop_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_TTDB"))) entity.Nop_TTDB = reader.GetDecimal(reader.GetOrdinal("Nop_TTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_CLGia"))) entity.Nop_CLGia = reader.GetDecimal(reader.GetOrdinal("Nop_CLGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("So_TBT_QDDC"))) entity.So_TBT_QDDC = reader.GetString(reader.GetOrdinal("So_TBT_QDDC"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				collection.Add(entity);
			}
            reader.Close();
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_BangKeHoSoThanhLy_ID()
		{
			string spName = "p_KDT_SXXK_BKNPLTamNopThue_SelectBy_BangKeHoSoThanhLy_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_BKNPLTamNopThue_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_BKNPLTamNopThue_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_BKNPLTamNopThue_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_BKNPLTamNopThue_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public BKNPLTamNopThueCollection SelectCollectionAll()
		{
			BKNPLTamNopThueCollection collection = new BKNPLTamNopThueCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				BKNPLTamNopThue entity = new BKNPLTamNopThue();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) entity.NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_NK"))) entity.Nop_NK = reader.GetDecimal(reader.GetOrdinal("Nop_NK"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_VAT"))) entity.Nop_VAT = reader.GetDecimal(reader.GetOrdinal("Nop_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_TTDB"))) entity.Nop_TTDB = reader.GetDecimal(reader.GetOrdinal("Nop_TTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_CLGia"))) entity.Nop_CLGia = reader.GetDecimal(reader.GetOrdinal("Nop_CLGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("So_TBT_QDDC"))) entity.So_TBT_QDDC = reader.GetString(reader.GetOrdinal("So_TBT_QDDC"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public BKNPLTamNopThueCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			BKNPLTamNopThueCollection collection = new BKNPLTamNopThueCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				BKNPLTamNopThue entity = new BKNPLTamNopThue();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) entity.NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_NK"))) entity.Nop_NK = reader.GetDecimal(reader.GetOrdinal("Nop_NK"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_VAT"))) entity.Nop_VAT = reader.GetDecimal(reader.GetOrdinal("Nop_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_TTDB"))) entity.Nop_TTDB = reader.GetDecimal(reader.GetOrdinal("Nop_TTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nop_CLGia"))) entity.Nop_CLGia = reader.GetDecimal(reader.GetOrdinal("Nop_CLGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("So_TBT_QDDC"))) entity.So_TBT_QDDC = reader.GetString(reader.GetOrdinal("So_TBT_QDDC"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_BKNPLTamNopThue_Insert";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
			db.AddInParameter(dbCommand, "@NgayNopThue", SqlDbType.DateTime, this._NgayNopThue);
			db.AddInParameter(dbCommand, "@Nop_NK", SqlDbType.Money, this._Nop_NK);
			db.AddInParameter(dbCommand, "@Nop_VAT", SqlDbType.Money, this._Nop_VAT);
			db.AddInParameter(dbCommand, "@Nop_TTDB", SqlDbType.Money, this._Nop_TTDB);
			db.AddInParameter(dbCommand, "@Nop_CLGia", SqlDbType.Money, this._Nop_CLGia);
			db.AddInParameter(dbCommand, "@So_TBT_QDDC", SqlDbType.VarChar, this._So_TBT_QDDC);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(BKNPLTamNopThueCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BKNPLTamNopThue item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, BKNPLTamNopThueCollection collection)
        {
            foreach (BKNPLTamNopThue item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_BKNPLTamNopThue_InsertUpdate";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
			db.AddInParameter(dbCommand, "@NgayNopThue", SqlDbType.DateTime, this._NgayNopThue);
			db.AddInParameter(dbCommand, "@Nop_NK", SqlDbType.Money, this._Nop_NK);
			db.AddInParameter(dbCommand, "@Nop_VAT", SqlDbType.Money, this._Nop_VAT);
			db.AddInParameter(dbCommand, "@Nop_TTDB", SqlDbType.Money, this._Nop_TTDB);
			db.AddInParameter(dbCommand, "@Nop_CLGia", SqlDbType.Money, this._Nop_CLGia);
			db.AddInParameter(dbCommand, "@So_TBT_QDDC", SqlDbType.VarChar, this._So_TBT_QDDC);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(BKNPLTamNopThueCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BKNPLTamNopThue item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_BKNPLTamNopThue_Update";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, this._BangKeHoSoThanhLy_ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
			db.AddInParameter(dbCommand, "@NgayNopThue", SqlDbType.DateTime, this._NgayNopThue);
			db.AddInParameter(dbCommand, "@Nop_NK", SqlDbType.Money, this._Nop_NK);
			db.AddInParameter(dbCommand, "@Nop_VAT", SqlDbType.Money, this._Nop_VAT);
			db.AddInParameter(dbCommand, "@Nop_TTDB", SqlDbType.Money, this._Nop_TTDB);
			db.AddInParameter(dbCommand, "@Nop_CLGia", SqlDbType.Money, this._Nop_CLGia);
			db.AddInParameter(dbCommand, "@So_TBT_QDDC", SqlDbType.VarChar, this._So_TBT_QDDC);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(BKNPLTamNopThueCollection collection, SqlTransaction transaction)
        {
            foreach (BKNPLTamNopThue item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
        public int DeleteTransactionBy_BangKeHoSoThanhLy_ID(SqlTransaction transaction, long id)
        {
            string spName = "p_KDT_SXXK_BKNPLTamNopThue_DeleteBy_BangKeHoSoThanhLy_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, id);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------
        public int DeleteTransactionByBKTKN(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, SqlTransaction transaction)
        {
            string query = "DELETE FROM t_KDT_SXXK_BKNPLTamNopThue WHERE ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string where = "SoToKhai=" + soToKhai + " AND MaLoaiHinh='" + maLoaiHinh + "' AND NamDangKy=" + namDangKy + " AND MaHaiQuan ='" + maHaiQuan + "'";
            DbCommand dbCommand = db.GetSqlStringCommand(query + where);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_BKNPLTamNopThue_Delete";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(BKNPLTamNopThueCollection collection, SqlTransaction transaction)
        {
            foreach (BKNPLTamNopThue item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(BKNPLTamNopThueCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BKNPLTamNopThue item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion
	}	
}