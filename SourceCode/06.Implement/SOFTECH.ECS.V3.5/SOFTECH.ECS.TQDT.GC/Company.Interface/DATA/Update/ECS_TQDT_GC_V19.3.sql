
/****** Object:  StoredProcedure [dbo].[p_KDT_GC_CanDoiNPLCungUng]    Script Date: 08/27/2014 16:33:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_CanDoiNPLCungUng]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_CanDoiNPLCungUng]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_GC_CanDoiNPLCungUng]    Script Date: 08/27/2014 16:33:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_GC_DinhMucDangKy_SelectAll]    
-- Database: Haiquan    
-- Author: Ngo Thanh Tung    
-- Time created: Monday, March 31, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_GC_CanDoiNPLCungUng]    
 @HopDong_ID bigint  

AS    
--SELECT * FROM t_gc_sanpham WHERE HopDong_ID = @HopDong_ID
BEGIN TRANSACTION 
BEGIN TRY

--DECLARE @HopDong_ID BIGINT
--SET @HopDong_ID = 425
DECLARE @ChenhLechNgay INT	
SET @ChenhLechNgay = 1
-- Tạo bảng hàng xuất khẩu

CREATE TABLE #HangXuat
(
	STT BIGINT,
	STTHang int,
	MaSP VARCHAR(50),
	SoToKhai BIGINT,
	NgayXuat DATETIME,
	LuongXuat NUMERIC(18,4),
	SoTKVNACCS VARCHAR(12)
)

-- Liệt kê các sản phẩm xuất khẩu

INSERT INTO #HangXuat
(STT,STTHang,MaSP,SoToKhai,NgayXuat,LuongXuat,SoTKVNACCS
	)
SELECT ROW_NUMBER() OVER (ORDER BY a.NgayDangKy,a.SoToKhai,a.STTHang) AS STT,a.STTHang AS STTHang, a.MaPhu AS MaPhu,  a.SoToKhai AS SoToKhai,a.NgayDangKy AS NgayXuat ,a.SoLuong AS LuongXuat, a.SoTKVNACCS AS SoTKVNACCS
                           FROM 
	(select MaPhu,SoLuong AS SoLuong, tkmd.SoToKhai AS SoToKhai, tkmd.NgayDangKy AS NgayDangKy,hmd.SoThuTuHang AS STTHang, SoTKVNACCS AS SoTKVNACCS
	   from t_KDT_HangMauDich hmd INNER JOIN (SELECT ID,t_KDT_ToKhaiMauDich.SoToKhai,t_KDT_ToKhaiMauDich.NgayDangKy AS NgayDangKy, CASE WHEN MaLoaiHinh LIKE '%V%' THEN LoaiVanDon ELSE CONVERT(VARCHAR(12),t_KDT_ToKhaiMauDich.SoToKhai) END AS SoTKVNACCS
FROM t_KDT_ToKhaiMauDich WHERE IDHopDong = @HopDong_ID AND  t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' AND Maloaihinh LIKE 'X%' AND  TrangThaiXuLy IN (1,0,5) ) as tkmd
	ON   hmd.TKMD_ID = tkmd.ID 
	 UNION
	 SELECT MaHang AS MaPhu,SoLuong AS SoLuong,tkct.SoToKhai AS SoToKhai, tkct.NgayDangKy AS NgayDangKy,hct.SoThuTuHang AS STTHang, SoTKVNACCS AS SoTKVNACCS
	   FROM t_KDT_GC_HangChuyenTiep hct INNER JOIN
	 (SELECT ID,t_kdt_gc_tokhaichuyentiep.SoToKhai,t_kdt_gc_tokhaichuyentiep.NgayDangKy AS NgayDangKy, CASE WHEN MaLoaiHinh LIKE '%V%' THEN HuongDan_PL ELSE CONVERT(VARCHAR(12),t_kdt_gc_tokhaichuyentiep.SoToKhai) END AS SoTKVNACCS
	    FROM t_kdt_gc_tokhaichuyentiep WHERE IDHopDong = @HopDong_ID AND t_kdt_gc_tokhaichuyentiep.LoaiHangHoa = 'S' AND t_kdt_gc_tokhaichuyentiep.MaLoaiHinh LIKE 'X%' AND  TrangThaiXuLy IN (1,0,5) ) as tkct 
	 ON hct.Master_ID = tkct.ID ) as a



-- Tạo bảng NPL 
CREATE TABLE #HangNhap
(
	STT BIGINT,
	MaNPL VARCHAR(50),
	LuongTon NUMERIC(23,8),
	LuongNhap NUMERIC(23,8),
	LuongNPLSuDung NUMERIC(23,8) 
)
-- Liệt kê các NPL của hợp đồng
INSERT INTO #HangNhap
(STT,MaNPL,LuongTon,LuongNhap,LuongNPLSuDung)
SELECT ROW_NUMBER() OVER (ORDER BY Ma ) AS STT,Ma,0 AS LuongTon,0 AS LuongNhap,0 AS LuongNPLSuDung
FROM t_GC_NguyenPhuLieu WHERE HopDong_ID = @HopDong_ID

-- Tạo indext cho vòng lặp while
DECLARE @ind BIGINT, @NgayXuatCu DATETIME
SET @ind = 1
SET @NgayXuatCu = CONVERT(DATETIME,'1900-01-01')

-- Tạo bảng kết quả tự cung ứng 
CREATE TABLE #CungUng
(
	MaNPL VARCHAR(100),
	MaSP VARCHAR(100),
	STTHang INT,
	SoToKhai BIGINT,
	NgayXuat DATETIME,
	LuongXuat NUMERIC(23,8),
	DinhMuc NUMERIC(23,8),
	TyLeHaoHut FLOAT,
	LuongTonDau NUMERIC(23,8),
	LuongNPLSuDung NUMERIC(23,8),
	LuongTonCuoi NUMERIC(23,8),
	LuongCungUng NUMERIC(23,8),
	SoTKVNACCS VARCHAR(12)
	)


-- Bắt đầu vòng lặp
WHILE ( @ind < (SELECT MAX(STT) FROM #HangXuat ) OR @ind = (SELECT MAX(STT) FROM #HangXuat ))
BEGIN
	-- Biến debug
		DECLARE @ketqua1 NVARCHAR(1000)
		DECLARE @ketqua2 NVARCHAR(1000)
	--SELECT @ketqua = LuongTon FROM #HangNhap WHERE STT = 2
	--PRINT @ketqua
	
	-- Lấy ngày đăng ký của tờ khai đang xét
	DECLARE @NgayXuatMoi DATETIME
	SELECT @NgayXuatMoi = DATEADD(DAY,@ChenhLechNgay * -1,hx.NgayXuat) FROM #HangXuat hx WHERE STT = @ind
	
	-- Kiểm tra lượng tồn, lượng nhập
	DECLARE @LuongNhap NUMERIC(23,8),@LuongTon NUMERIC(23,8)
	
	
	-- Kiểm tra với ngày đăng ký trước đó, nếu khác nhau sẽ cập nhật lại lượng tồn của NPL trong khoảng thời gian từ tờ khai trước đến tờ khai này
	-- Lượng tồn = lượng tồn trước đó +  lượng nhập trong khoản thời gian - lượng tái xuất trong khoản thời gian này
	PRINT N'Ngày xuất cũ ' + Convert(nvarchar(20),@NgayXuatCu,103)
	PRINT N'Ngày xuất Mới ' + Convert(nvarchar(20),@NgayXuatMoi,103)
	IF(@NgayXuatCu <> @NgayXuatMoi)
	BEGIN 
	UPDATE hangnhap SET LuongNhap = hangnhap.LuongNhap + CASE WHEN  hang.SoLuong IS NOT NULL THEN hang.soluong ELSE 0 END  - case when hangTaiXuat.SoLuong IS NOT NULL THEN hangTaiXuat.SoLuong ELSE 0 END ,
						 LuongTon = hangnhap.LuongTon + CASE WHEN  hang.SoLuong IS NOT NULL THEN hang.soluong ELSE 0 END  - case when hangTaiXuat.SoLuong IS NOT NULL THEN hangTaiXuat.SoLuong ELSE 0 END 
	FROM #HangNhap hangnhap LEFT JOIN 
	(
		SELECT a.MaPhu AS MaPhu, CASE WHEN  SUM(a.SoLuong) IS NOT NULL THEN SUM(a.SoLuong) ELSE 0 end AS SoLuong  FROM 
	(select MaPhu,Sum(SoLuong) AS SoLuong from t_KDT_HangMauDich hmd INNER JOIN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE IDHopDong = @HopDong_ID AND NgayDangKy BETWEEN  @NgayXuatCu AND @NgayXuatMoi AND t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N' AND Maloaihinh LIKE 'N%' AND  TrangThaiXuLy IN (1,0,5)) as tkmd
	ON   hmd.TKMD_ID = tkmd.ID GROUP BY hmd.MaPhu
	 UNION
	 SELECT MaHang AS MaPhu, SUM(SoLuong) AS SoLuong FROM t_KDT_GC_HangChuyenTiep hct INNER JOIN
	 (SELECT ID FROM t_kdt_gc_tokhaichuyentiep WHERE IDHopDong = @HopDong_ID AND NgayDangKy BETWEEN  @NgayXuatCu AND @NgayXuatMoi AND t_kdt_gc_tokhaichuyentiep.LoaiHangHoa = 'N' AND t_kdt_gc_tokhaichuyentiep.MaLoaiHinh LIKE 'N%'AND  TrangThaiXuLy IN (1,0,5)) as tkct 
	 ON hct.Master_ID = tkct.ID GROUP BY hct.MaHang ) as a GROUP BY a.MaPhu )AS hang ON hangnhap.MaNPL = hang.MaPhu LEFT JOIN
	 (
		SELECT a.MaPhu AS MaPhu, CASE WHEN  SUM(a.SoLuong) IS NOT NULL THEN SUM(a.SoLuong) ELSE 0 end AS SoLuong  FROM 
	(select MaPhu,Sum(SoLuong) AS SoLuong from t_KDT_HangMauDich hmd INNER JOIN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE IDHopDong = @HopDong_ID AND NgayDangKy BETWEEN  @NgayXuatCu AND @NgayXuatMoi AND t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N' AND Maloaihinh LIKE 'X%' AND  TrangThaiXuLy IN (1,0,5)) as tkmd
	ON   hmd.TKMD_ID = tkmd.ID GROUP BY hmd.MaPhu
	 UNION
	 SELECT MaHang AS MaPhu,SUM(SoLuong) AS SoLuong FROM t_KDT_GC_HangChuyenTiep hct INNER JOIN
	 (SELECT ID FROM t_kdt_gc_tokhaichuyentiep WHERE IDHopDong = @HopDong_ID AND NgayDangKy BETWEEN  @NgayXuatCu AND @NgayXuatMoi AND t_kdt_gc_tokhaichuyentiep.LoaiHangHoa = 'N' AND t_kdt_gc_tokhaichuyentiep.MaLoaiHinh LIKE 'X%'AND  TrangThaiXuLy IN (1,0,5)) as tkct 
	 ON hct.Master_ID = tkct.ID GROUP BY hct.MaHang ) as a GROUP BY a.MaPhu )AS hangTaiXuat ON hangnhap.MaNPL = hangTaiXuat.MaPhu
	
	
	END
	
	--SELECT @LuongTon = hn.LuongTon FROM #HangNhap hn WHERE hn.MaNPL = '13HTMC13/01'
	--PRINT @LuongTon 
	
	SELECT @ketqua1 = CONVERT(NVARCHAR(1000), N'Mã SP - Số TK - Ngày - Lượng xuất: ' + hx.MaSP +'-' + convert(nvarchar(20),hx.SoToKhai) +'-' + Convert(nvarchar(20),hx.NgayXuat,103) + ' - ' + convert(nvarchar(20),hx.LuongXuat))  FROM #HangXuat hx WHERE hx.STT = @ind
	PRINT @ketqua1
	
	
	-- Trừ lượng tồn của NPL cho hàng xuất 
	UPDATE #HangNhap SET LuongTon = nhap.LuongTon - xuat.LuongNPLSuDung,LuongNPLSuDung = xuat.LuongNPLSuDung
	FROM #HangNhap nhap INNER JOIN
	(SELECT dm.MaNguyenPhuLieu AS MaNPL,(dm.DinhMucSuDung + dm.DinhMucSuDung*dm.TyLeHaoHut/100)*hx.LuongXuat AS LuongNPLSuDung FROM 
	(SELECT
		STT,
		STTHang,
		MaSP,
		SoToKhai,
		NgayXuat,
		LuongXuat
	FROM
		#HangXuat WHERE STT = @ind) AS hx 
		INNER JOIN (SELECT * FROM t_GC_DinhMuc dm WHERE dm.HopDong_ID = @HopDong_ID) AS dm 
		ON dm.MaSanPham = hx.MaSP) AS xuat ON nhap.MaNPL = xuat.MaNPL
	
	SELECT @ketqua2 = CONVERT(NVARCHAR(1000), N'Ma NPL - Tồn đầu - sử dụng - tồn cuối : ' + hn.MaNPL + ' - ' + convert(nvarchar(20),hn.LuongNhap)+ ' - '  + convert(nvarchar(20),hn.LuongNPLSuDung)+ ' - '  + convert(nvarchar(20),hn.LuongTon))
	  FROM #HangNhap hn WHERE hn.MaNPL = '13HTMC13/01'
	  PRINT @ketqua2
		
	-- Kiểm tra mặt hàng này đã được khai cung ứng chưa trên V5

	IF EXISTS (SELECT ID,dt.Master_ID FROM [t_KDT_GC_CungUng_Detail] dt INNER JOIN (SELECT STTHang,MaSP,SoToKhai,NgayXuat
	                                                                    FROM #HangXuat WHERE #HangXuat.STT = @ind) as hangxuat
							ON dt.SoToKhaiXuat = hangxuat.SoToKhai AND dt.MaSanPham = hangxuat.MaSP AND YEAR(dt.NgayDangKy) = YEAR(hangxuat.NgayXuat) 
							AND MONTH(dt.NgayDangKy) = MONTH(hangxuat.NgayXuat) AND Day(dt.NgayDangKy) = Day(hangxuat.NgayXuat))
							BEGIN 
								
								UPDATE #HangNhap
								SET
									LuongTon = CASE WHEN NPLcungUng.LuongCungUng > nhap.LuongNPLSuDung THEN LuongTon + LuongNPLSuDung ELSE LuongTon + NPLcungUng.LuongCungUng END 
									
								FROM #HangNhap nhap INNER JOIN
														
								(SELECT npl.MaNPL, SUM(chungtu.SoLuong) AS LuongCungUng FROM t_KDT_GC_CungUng npl INNER JOIN 
								 (SELECT ID,dt.Master_ID FROM [t_KDT_GC_CungUng_Detail] dt INNER JOIN 
										(SELECT STTHang,MaSP,SoToKhai,NgayXuat FROM #HangXuat WHERE #HangXuat.STT = @ind) as hangxuat
												ON dt.SoToKhaiXuat = hangxuat.SoToKhai AND dt.MaSanPham = hangxuat.MaSP AND YEAR(dt.NgayDangKy) = YEAR(hangxuat.NgayXuat) 
												AND MONTH(dt.NgayDangKy) = MONTH(hangxuat.NgayXuat) AND Day(dt.NgayDangKy) = Day(hangxuat.NgayXuat)) AS sanpham
								ON npl.ID = sanpham.Master_id INNER JOIN t_KDT_GC_CungUng_ChungTu chungtu ON chungtu.Master_ID = sanpham.ID
								INNER JOIN t_KDT_GC_CungUngDangKy dk ON dk.ID = npl.Master_ID AND dk.HopDong_ID = @HopDong_ID
								GROUP BY npl.MaNPL) AS NPLcungUng
								ON NPLcungUng.MaNPL = nhap.MaNPL 
								
								END
	
	
	-- Kiểm tra đã khai cung ứng trên V4 
	ELSE IF EXISTS (SELECT * FROM  (SELECT dk.TKMDID,dk.MaHang AS MaNPL,hmd.MaPhu AS MaSP,dt.DongHangTrenCT AS STTHang,tkmd.SoToKhai,tkmd.NgayDangKy,hmd.SoLuong AS LuongXuat,dt.SoLuong AS LuongCungUng,hmd.SoThuTuHang
  FROM [t_KDT_GC_NPLCungUngDetail] dt
INNER JOIN (SELECT * FROM  [t_KDT_GC_NPLCungUng] WHERE TKMDID IN (SELECT id FROM t_KDT_ToKhaiMauDich WHERE t_KDT_ToKhaiMauDich.IDHopDong = @HopDong_ID )) AS dk 
ON dk.ID = dt.Master_ID INNER JOIN 
(SELECT ID,SoToKhai,MaLoaiHinh,NgayDangKy  FROM t_KDT_ToKhaiMauDich WHERE t_KDT_ToKhaiMauDich.IDHopDong = @HopDong_ID) AS tkmd
ON tkmd.id = dk.TKMDID  INNER JOIN 
(SELECT MaPhu,SoThuTuHang,TKMD_ID,SoLuong  FROM t_KDT_HangMauDich WHERE TKMD_ID IN (SELECT id FROM t_KDT_ToKhaiMauDich WHERE t_KDT_ToKhaiMauDich.IDHopDong = @HopDong_ID)) AS hmd
ON hmd.SoThuTuHang = dt.DongHangTrenCT AND hmd.TKMD_ID = dk.TKMDID) AS CungUng
INNER JOIN #HangXuat hx ON CungUng.MaSP = hx.MaSP AND CungUng.SoToKhai = hx.SoToKhai AND Year(CungUng.NgayDangKy) = YEAR(hx.NgayXuat)
AND Year(CungUng.NgayDangKy) = YEAR(hx.NgayXuat) AND Year(CungUng.NgayDangKy) = YEAR(hx.NgayXuat) AND hx.STT = @ind AND hx.STTHang = CungUng.STTHang )

BEGIN 
	UPDATE #HangNhap
	SET
		LuongTon = CASE WHEN NPLcungUng.LuongCungUng > nhap.LuongNPLSuDung THEN LuongTon + LuongNPLSuDung ELSE LuongTon + NPLcungUng.LuongCungUng END
	FROM #HangNhap nhap INNER JOIN
	 (SELECT CungUng.MaNPL,CungUng.LuongCungUng
	   FROM  (SELECT dk.TKMDID,dk.MaHang AS MaNPL,hmd.MaPhu AS MaSP,dt.DongHangTrenCT AS STTHang,tkmd.SoToKhai,tkmd.NgayDangKy,hmd.SoLuong AS LuongXuat,dt.SoLuong AS LuongCungUng,hmd.SoThuTuHang
  FROM [t_KDT_GC_NPLCungUngDetail] dt
INNER JOIN (SELECT * FROM  [t_KDT_GC_NPLCungUng] WHERE TKMDID IN (SELECT id FROM t_KDT_ToKhaiMauDich WHERE t_KDT_ToKhaiMauDich.IDHopDong = @HopDong_ID )) AS dk 
ON dk.ID = dt.Master_ID INNER JOIN 
(SELECT ID,SoToKhai,MaLoaiHinh,NgayDangKy  FROM t_KDT_ToKhaiMauDich WHERE t_KDT_ToKhaiMauDich.IDHopDong = @HopDong_ID) AS tkmd
ON tkmd.id = dk.TKMDID  INNER JOIN 
(SELECT MaPhu,SoThuTuHang,TKMD_ID,SoLuong  FROM t_KDT_HangMauDich WHERE TKMD_ID IN (SELECT id FROM t_KDT_ToKhaiMauDich WHERE t_KDT_ToKhaiMauDich.IDHopDong = @HopDong_ID)) AS hmd
ON hmd.SoThuTuHang = dt.DongHangTrenCT AND hmd.TKMD_ID = dk.TKMDID) AS CungUng
INNER JOIN #HangXuat hx ON CungUng.MaSP = hx.MaSP AND CungUng.SoToKhai = hx.SoToKhai AND Year(CungUng.NgayDangKy) = YEAR(hx.NgayXuat)
AND Year(CungUng.NgayDangKy) = YEAR(hx.NgayXuat) AND Year(CungUng.NgayDangKy) = YEAR(hx.NgayXuat) AND hx.STT = @ind AND hx.STTHang = CungUng.STTHang ) AS NPLcungUng
ON NPLcungUng.MaNPL = nhap.MaNPL
	
	--DECLARE @Temp NVARCHAR(1000)
	--SELECT TOP 1 @Temp = 
	--      	'STT : ' +  CONVERT(VARCHAR(10),hx.STT) + 
	--      	' MA SP: ' + hx.MaSP + 
	--      	' So tk: ' + CONVERT(VARCHAR(10),hx.SoToKhai) + 
	--      	' Ngay : ' + convert(varchar(10),hx.NgayXuat,103) +
	--      	' Luong Xuat: ' + CONVERT(VARCHAR(20),hx.LuongXuat) 
	--      FROM
	--      	#HangXuat hx WHERE hx.STT = @ind AND hx.MaSP = '1796' AND hx.SoToKhai = '496'
	--PRINT @Temp
	
	END
	
	
	
	
	
	
	-- Insert vào bảng cung ứng những NPL bị âm lượng tồn
	INSERT INTO #CungUng
	(
		MaNPL,
		MaSP,
		STTHang,
		SoToKhai,
		NgayXuat,
		DinhMuc,
		TyLeHaoHut,
		LuongTonDau,
		LuongNPLSuDung,
		LuongTonCuoi,
		LuongXuat,
		LuongCungUng,
		SoTKVNACCS
	)
	SELECT 
		 hn.MaNPL	,
		 (SELECT MaSP FROM #HangXuat WHERE STT = @ind) AS MaSP	,
		 (SELECT STTHang FROM #HangXuat WHERE STT = @ind) AS STTHang,
		 (SELECT SoToKhai FROM #HangXuat WHERE STT = @ind) AS SoToKhai,
		 (SELECT NgayXuat FROM #HangXuat WHERE STT = @ind) AS NgayXuat,
		 dm.DinhMucSuDung AS DinhMuc,
		 dm.TyLeHaoHut AS TyLeHaoHut,
		 hn.LuongNhap AS LuongTonDau,
		 hn.LuongNPLSuDung AS LuongNPLSuDung,
		 hn.LuongTon AS LuongTonCuoi,
		 (SELECT LuongXuat FROM #HangXuat WHERE STT = @ind) AS LuongXuat,
		 (hn.LuongTon * -1 ) AS LuongCungUng,
		 (SELECT SoTKVNACCS FROM #HangXuat WHERE STT = @ind) AS SoTKVNACCS
	FROM #HangNhap hn  INNER JOIN (SELECT * FROM t_GC_DinhMuc dm WHERE dm.HopDong_ID = @HopDong_ID AND dm.MaSanPham IN (SELECT MaSP FROM #HangXuat WHERE STT = @ind) ) AS dm
	ON hn.MaNPL = dm.MaNguyenPhuLieu
	WHERE hn.LuongTon < 0
	
	-- Cập nhật lại lượng tồn = 0 đối với những NPL âm - để sau này tính lượng cung ứng cho hàng tiếp theo
	UPDATE #HangNhap SET LuongTon = CASE WHEN LuongTon < 0 THEN 0 ELSE LuongTon END , LuongNhap = CASE WHEN LuongTon < 0 THEN 0 ELSE LuongTon END
	
	
	--SELECT
	--	@ketqua = hx.LuongNhap
	--FROM
	--	#hangNhap hx WHERE hx.MaNPL = '17HT-KR2013-01'
		
	--PRINT @ketqua
	
	SET @ind = @ind + 1

	--SELECT @NgayXuatCu = hx.NgayXuat FROM #HangXuat hx WHERE STT = @ind
	SET @NgayXuatCu = DATEADD(DAY,@ChenhLechNgay ,@NgayXuatMoi)
END




SELECT *  FROM #CungUng 
--WHERE MaNPL =  '13HTMC13/01'
ORDER BY MaNPL
--WHERE MaNPL = '17HT-KR2013-02'
DROP TABLE #HangXuat
DROP TABLE #HangNhap    
DROP TABLE #CungUng
ROLLBACK
END TRY
BEGIN CATCH
ROLLBACK
	
		SELECT
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage
	
END CATCH
GO




/****** Object:  StoredProcedure [dbo].[p_KDT_GC_CungUng_DanhSachCUbyHopDong]    Script Date: 08/27/2014 16:34:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_GC_CungUng_DanhSachCUbyHopDong]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_GC_CungUng_DanhSachCUbyHopDong]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_GC_CungUng_DanhSachCUbyHopDong]    Script Date: 08/27/2014 16:34:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_GC_DinhMucDangKy_SelectAll]    
-- Database: Haiquan    
-- Author: Ngo Thanh Tung    
-- Time created: Monday, March 31, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_GC_CungUng_DanhSachCUbyHopDong]    
 @HopDong_ID bigint    
AS    
  			SELECT npl.MaNPL, SUM(chungtu.SoLuong) AS LuongCungUng FROM t_KDT_GC_CungUng npl INNER JOIN 
								 (SELECT ID,dt.Master_ID FROM [t_KDT_GC_CungUng_Detail] dt ) AS sanpham
								ON npl.ID = sanpham.Master_id INNER JOIN t_KDT_GC_CungUng_ChungTu chungtu ON chungtu.Master_ID = sanpham.ID
								INNER JOIN t_KDT_GC_CungUngDangKy dk ON dk.ID = npl.Master_ID AND dk.HopDong_ID = @HopDong_ID
								GROUP BY npl.MaNPL
GO






-------------------------Tao Bang TaiXuat -------------------------------------------------------------------------------------

/****** Object:  Table [dbo].[t_KDT_GC_TaiXuatDangKy]    Script Date: 08/28/2014 09:48:50 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_GC_TaiXuatDangKy]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[t_KDT_GC_TaiXuatDangKy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [bigint] NULL,
	[NgayTiepNhan] [datetime] NULL,
	[HopDong_ID] [bigint] NULL,
	[SoHopDong] [varchar](100) NULL,
	[NgayHopDong] [datetime] NULL,
	[NgayHetHan] [datetime] NULL,
	[TrangThaiXuLy] [int] NULL,
	[MaHQ] [varchar](6) NULL,
	[MaDoanhNghiep] [varchar](50) NULL,
	[GuidStr] [varchar](100) NULL,
	[DeXuatKhac] [nvarchar](250) NULL,
	[LyDoSua] [nvarchar](250) NULL,
 CONSTRAINT [PK_t_KDT_GC_TaiXuatDangKy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END

GO

/****** Object:  Table [dbo].[t_KDT_GC_TaiXuat]    Script Date: 08/28/2014 09:47:02 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_GC_TaiXuat]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[t_KDT_GC_TaiXuat](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[SoToKhai] [varchar](50) NULL,
	[SoToKhaiVNACCS] [varchar](50) NULL,
	[MaLoaiHinh] [varchar](6) NULL,
	[NgayDangKy] [datetime] NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaNPL] [nvarchar](100) NULL,
	[TenNPL] [nvarchar](255) NULL,
	[LoaiHang] [int] NULL,
	[LuongTaiXuat] [numeric](22, 4) NULL,
	[DVT_ID] [varchar](6) NULL,
	[GhiChu] [nvarchar](300) NULL,
 CONSTRAINT [PK_t_KDT_GC_TaiXuat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuatDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuatDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuatDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuatDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuatDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuatDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuatDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuatDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuatDangKy_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@HopDong_ID bigint,
	@SoHopDong varchar(100),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_TaiXuatDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@HopDong_ID,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHan,
	@TrangThaiXuLy,
	@MaHQ,
	@MaDoanhNghiep,
	@GuidStr,
	@DeXuatKhac,
	@LyDoSua
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuatDangKy_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@HopDong_ID bigint,
	@SoHopDong varchar(100),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_GC_TaiXuatDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[HopDong_ID] = @HopDong_ID,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHan] = @NgayHetHan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[GuidStr] = @GuidStr,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuatDangKy_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@HopDong_ID bigint,
	@SoHopDong varchar(100),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_TaiXuatDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_TaiXuatDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[HopDong_ID] = @HopDong_ID,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHan] = @NgayHetHan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[GuidStr] = @GuidStr,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_TaiXuatDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[HopDong_ID],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHan],
			[TrangThaiXuLy],
			[MaHQ],
			[MaDoanhNghiep],
			[GuidStr],
			[DeXuatKhac],
			[LyDoSua]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@HopDong_ID,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHan,
			@TrangThaiXuLy,
			@MaHQ,
			@MaDoanhNghiep,
			@GuidStr,
			@DeXuatKhac,
			@LyDoSua
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuatDangKy_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_TaiXuatDangKy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuatDangKy_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_TaiXuatDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuatDangKy_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua]
FROM
	[dbo].[t_KDT_GC_TaiXuatDangKy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuatDangKy_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua]
FROM [dbo].[t_KDT_GC_TaiXuatDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuatDangKy_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuatDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua]
FROM
	[dbo].[t_KDT_GC_TaiXuatDangKy]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuat_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuat_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuat_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuat_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuat_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuat_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuat_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuat_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuat_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuat_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuat_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuat_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuat_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuat_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_TaiXuat_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_TaiXuat_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuat_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuat_Insert]
	@Master_ID bigint,
	@SoToKhai varchar(50),
	@SoToKhaiVNACCS varchar(50),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@LoaiHang int,
	@LuongTaiXuat numeric(22, 4),
	@DVT_ID varchar(6),
	@GhiChu nvarchar(300),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_TaiXuat]
(
	[Master_ID],
	[SoToKhai],
	[SoToKhaiVNACCS],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaNPL],
	[TenNPL],
	[LoaiHang],
	[LuongTaiXuat],
	[DVT_ID],
	[GhiChu]
)
VALUES 
(
	@Master_ID,
	@SoToKhai,
	@SoToKhaiVNACCS,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaHaiQuan,
	@MaNPL,
	@TenNPL,
	@LoaiHang,
	@LuongTaiXuat,
	@DVT_ID,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuat_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuat_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoToKhai varchar(50),
	@SoToKhaiVNACCS varchar(50),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@LoaiHang int,
	@LuongTaiXuat numeric(22, 4),
	@DVT_ID varchar(6),
	@GhiChu nvarchar(300)
AS

UPDATE
	[dbo].[t_KDT_GC_TaiXuat]
SET
	[Master_ID] = @Master_ID,
	[SoToKhai] = @SoToKhai,
	[SoToKhaiVNACCS] = @SoToKhaiVNACCS,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaHaiQuan] = @MaHaiQuan,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[LoaiHang] = @LoaiHang,
	[LuongTaiXuat] = @LuongTaiXuat,
	[DVT_ID] = @DVT_ID,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuat_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuat_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoToKhai varchar(50),
	@SoToKhaiVNACCS varchar(50),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@LoaiHang int,
	@LuongTaiXuat numeric(22, 4),
	@DVT_ID varchar(6),
	@GhiChu nvarchar(300)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_TaiXuat] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_TaiXuat] 
		SET
			[Master_ID] = @Master_ID,
			[SoToKhai] = @SoToKhai,
			[SoToKhaiVNACCS] = @SoToKhaiVNACCS,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaHaiQuan] = @MaHaiQuan,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[LoaiHang] = @LoaiHang,
			[LuongTaiXuat] = @LuongTaiXuat,
			[DVT_ID] = @DVT_ID,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_TaiXuat]
		(
			[Master_ID],
			[SoToKhai],
			[SoToKhaiVNACCS],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaHaiQuan],
			[MaNPL],
			[TenNPL],
			[LoaiHang],
			[LuongTaiXuat],
			[DVT_ID],
			[GhiChu]
		)
		VALUES 
		(
			@Master_ID,
			@SoToKhai,
			@SoToKhaiVNACCS,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaHaiQuan,
			@MaNPL,
			@TenNPL,
			@LoaiHang,
			@LuongTaiXuat,
			@DVT_ID,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuat_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuat_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_TaiXuat]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuat_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuat_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_TaiXuat] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuat_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuat_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoToKhai],
	[SoToKhaiVNACCS],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaNPL],
	[TenNPL],
	[LoaiHang],
	[LuongTaiXuat],
	[DVT_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_TaiXuat]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuat_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuat_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoToKhai],
	[SoToKhaiVNACCS],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaNPL],
	[TenNPL],
	[LoaiHang],
	[LuongTaiXuat],
	[DVT_ID],
	[GhiChu]
FROM [dbo].[t_KDT_GC_TaiXuat] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_TaiXuat_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_TaiXuat_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoToKhai],
	[SoToKhaiVNACCS],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaNPL],
	[TenNPL],
	[LoaiHang],
	[LuongTaiXuat],
	[DVT_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_TaiXuat]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.3',GETDATE(), N' Cập nhật store cân đối NPL cung ung, table Tái xuất ')
END	


