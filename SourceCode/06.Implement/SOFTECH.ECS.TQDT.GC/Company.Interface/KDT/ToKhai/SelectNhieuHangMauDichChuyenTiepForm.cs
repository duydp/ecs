﻿using System;
using System.Drawing;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
//using Company.GC.BLL.KDT;

using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using System.Data;
using Company.Interface.GC;
using Company.GC.BLL.KDT.GC ;

namespace Company.Interface.KDT.ToKhai
{
    public partial class SelectNhieuHangMauDichChuyenTiepForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------       
        public Company.GC.BLL.KDT.GC.HopDong HD = new  Company.GC.BLL.KDT.GC.HopDong();
        //public ToKhaiMauDich TKMD; 
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT;
        //public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public HangChuyenTiepCollection HCTCollection = new HangChuyenTiepCollection();
        public decimal TyGiaTT;
        public string NhomLH = "";
        public string LoaiHangHoa = "";
        public bool ismoreGoods ;
        //-----------------------------------------------------------------------------------------

        public SelectNhieuHangMauDichChuyenTiepForm()
        {
            InitializeComponent();           
        }
        
        private void khoitao_GiaoDien()
        {
            //if (this.TKCT.MaLoaiHinh.StartsWith("S"))
            //{
            //    dgList.RootTable.Columns["Ma_HTS"].Visible = false;
            //    dgList.RootTable.Columns["DVT_HTS"].Visible = false;
            //    dgList.RootTable.Columns["SoLuong_HTS"].Visible = false;
            //}
            //else
            //{
            //    if (this.TKCT.MaLoaiHinh.Contains("S") )
            //    {
            //        dgList.RootTable.Columns["Ma_HTS"].Visible = false;
            //        dgList.RootTable.Columns["DVT_HTS"].Visible = false;
            //        dgList.RootTable.Columns["SoLuong_HTS"].Visible = false;
            //    }
            //}
            //dgList.RootTable.Columns["ID_NuocXX"].HasValueList = true;
            foreach (DataRow rowNuoc in this._Nuoc.Rows)
            {
               dgList.RootTable.Columns["ID_NuocXX"].ValueList.Add(rowNuoc["ID"].ToString(), rowNuoc["Ten"].ToString());
            }
            SetGridSettings();
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll();

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll();
          
        }

        
        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangChuyenTiep  hmd in this.HCTCollection )
            {
                if (hmd.MaHang.Trim().ToUpper() == maHang.Trim().ToUpper()) return true;
            }
            return false;
        }
        
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetDataRows();
            foreach (GridEXRow i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    Company.GC.BLL.KDT.GC.HangChuyenTiep hmd = (Company.GC.BLL.KDT.GC.HangChuyenTiep)i.DataRow;
                    //if (hmd.nu.NuocXX_ID == "")
                    //{
                    //    ShowMessage("Nước xuất xứ của hàng thứ : "+(i.RowIndex+1)+" không hợp lệ.", false);
                    //    return;
                    //}
                    if (hmd.SoLuong <= 0)
                    {
                        showMsg("MSG_0203025", i.RowIndex + 1);
                        //MLMessages("Số lượng của hàng thứ : " + (i.RowIndex + 1) + " không hợp lệ.","MSG_EXC09",Convert.ToString(i.RowIndex + 1), false);
                        return;
                    }
                    if (hmd.DonGia  <= 0)
                    {
                        showMsg("MSG_0203026", i.RowIndex + 1);
                        //MLMessages("Đơn giá nguyên tệ của hàng thứ : " + (i.RowIndex + 1) + " không hợp lệ.", "MSG_EXC09", Convert.ToString(i.RowIndex + 1), false);
                        return;
                    }
                    if (!MaHS.Validate(hmd.MaHS, 10))
                    {
                        showMsg("MSG_0203027", i.RowIndex + 1);
                        //MLMessages("Mã HS của hàng thứ : " + (i.RowIndex + 1) + " không hợp lệ.", "MSG_EXC09", Convert.ToString(i.RowIndex + 1), false);                        
                        return;
                    }
                }
            }
            this.TKCT.HCTCollection .Clear();
            foreach (HangChuyenTiep HCT in this.HCTCollection )
            {
                this.TKCT.HCTCollection.Add(HCT.SaoChepHCT ());
            }            
            this.Close();
        }
        private void reset()
        {
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
            
        }
        private void SetGridSettings()
        {
            this.dgList.View = Janus.Windows.GridEX.View.CardView;
            this.dgList.GridLineStyle = GridLineStyle.Solid;
            this.dgList.CardInnerSpacing = 2;
            this.dgList.CardViewGridlines = CardViewGridlines.Vertical;
            //this.dgList.RootTable.Columns["Icon"].CardIcon = true;
            //this.dgList.RootTable.Columns["Icon"].Visible = false;
            //this.dgList.RootTable.Columns["Icon"].ImageIndex = 0;
            //this.dgList.RootTable.Columns["CompanyName"].CardCaption = true;

            //Set the BackColor of card (RowFormatStyle) equal to the backcolor or a header
            //And set the backcolor of each cell equal to SystemColors.Window

            this.dgList.RowFormatStyle.BackColor = this.dgList.HeaderFormatStyle.BackColor;
            foreach (GridEXColumn col in this.dgList.RootTable.Columns)
            {
                col.CellStyle.BackColor = SystemColors.Window;
            }

        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();

          
            decimal tongTGKB = 0;
            //TH
            foreach (HangChuyenTiep  hct in this.HCTCollection )
            {
               tongTGKB += hct.TriGia ;
            }
            dgList.DataSource = this.HCTCollection;

            lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", tongTGKB.ToString("N"), this.TKCT.NguyenTe_ID);
            //lblTyGiaTT.Text = "Tỷ giá tính thuế: " + this.TyGiaTT.ToString("N");
            if (this.TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                btnAddNew.Visible = false;
        }

      //-----------------------------------------------------------------------------------------------       

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }
     
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

 
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (ismoreGoods)
            {
                ismoreGoods = false;
                return;
            }
            else
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        //HangMauDichEditForm f = new HangMauDichEditForm();
                        Company.Interface.KDT.GC.HangChuyenTiepEditForm f = new Company.Interface.KDT.GC.HangChuyenTiepEditForm();
                        f.HCT = (HangChuyenTiep)e.Row.DataRow;
                        f.HD.ID = this.TKCT.IDHopDong;
                        f.TKCT = this.TKCT;
                        // f.TyGiaTT = this.TyGiaTT;
                        if (this.TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                            f.OpenType = OpenFormType.Edit;
                        else
                            f.OpenType = OpenFormType.View;
                        f.ShowDialog();
                        try
                        {
                            dgList.Refetch();
                        }
                        catch { dgList.Refresh(); }
                        decimal tongTGKB = 0;
                        foreach (HangChuyenTiep hct in this.HCTCollection)
                        {
                            // Tính lại thuế.
                            // hct..TinhThue(this.TyGiaTT);
                            // Tổng trị giá khai báo.
                            tongTGKB += hct.TriGia;
                        }
                        lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", tongTGKB.ToString("N"), this.TKCT.NguyenTe_ID);
                    }
                    break;
                }
            }
        }

    
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangChuyenTiep  hmd = (HangChuyenTiep )i.GetRow().DataRow;
                        if (hmd.ID > 0)
                        {
                            hmd.Delete( TKCT.IDHopDong,LoaiHangHoa, TKCT.MaLoaiHinh );
                        }
                    }
                }               
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
           
                if (LoaiHangHoa.Substring(2,2).Equals("PL"))
                {
                    NguyenPhuLieuChuyenTiepRegistedForm f = new NguyenPhuLieuChuyenTiepRegistedForm();
                    f.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                    f.TKCT = TKCT;
                    f.HCTCollection = HCTCollection;
                    f.ShowDialog();
                }

                else if (LoaiHangHoa.Substring(2, 2).Equals("SP"))
                 {
                      SanPhamChuyenTiepRegistedForm  f = new SanPhamChuyenTiepRegistedForm();
                       f.SanPhamSelected.HopDong_ID  = HD.ID;
                       f.TKCT  = TKCT;
                       f.HCTCollection  = HCTCollection;
                       f.ShowDialog();
                }
                else 
                {
                    ThietBiChuyenTiepRegistedForm f = new ThietBiChuyenTiepRegistedForm();
                    f.ThietBiSelected.HopDong_ID = HD.ID ;
                    f.TKCT = TKCT;
                    f.HCTCollection = HCTCollection ;
                    f.ShowDialog();
                }
           
            dgList.DataSource = HCTCollection ;
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }
        private bool KiemTraLuongHang(string MaHang,decimal SoLuong)
        {
            decimal LuongCon = 0;
            //if (this.TKCT.MaLoaiHinh.Substring(2,2).Equals("PL"))
            if (LoaiHangHoa.Substring(2, 2).Equals("PL"))
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = MaHang;
                if (npl.Load())
                {
                    if (this.TKCT.MaLoaiHinh.EndsWith("X"))
                    {
                        LuongCon = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203028", LuongCon, true) != "Yes")
                            //if (MLMessages("Không được nhập quá số lượng còn được nhập là : " + LuongCon + " .Bạn có muốn tiếp tục không ?","MSG_WRN32",LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }
                    else
                    {
                        LuongCon = npl.SoLuongDaNhap + npl.SoLuongCungUng - npl.SoLuongDaDung;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203029", LuongCon, true) != "Yes")
                            //if (MLMessages("Không được xuất quá số lượng còn được xuất là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }                   

                }
                else
                {
                }
            }
            else if (LoaiHangHoa.Substring(2, 2).Equals("SP"))
            {
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = MaHang;
                if (sp.Load())
                {                    
                    LuongCon = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                    if (LuongCon < SoLuong)
                    {
                        if (showMsg("MSG_0203029", LuongCon, true) != "Yes")
                        //if (MLMessages("Không được xuất quá số lượng còn được xuất là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                            return false;
                    }
                }
                else
                {
                    ;
                }
            }
            else if (LoaiHangHoa.Substring(2, 2).Equals("TB"))
            {
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                tb.HopDong_ID = HD.ID;
                tb.Ma = MaHang;
                if (tb.Load())
                {
                    if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                    {
                        LuongCon = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203028", LuongCon, true) != "Yes")
                            //if (MLMessages("Không được nhập quá số lượng còn được nhập là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }
                    else
                    {
                        LuongCon = tb.SoLuongDaNhap;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203029", LuongCon, true) != "Yes")
                            //if (MLMessages("Không được xuất quá số lượng còn được xuất là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }
                }
                else
                {
                    ;
                }
            }
            return true;
        }
        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "DonGiaKB" || e.Column.Key == "SoLuong")
            {
                decimal SoLuong = Convert.ToDecimal(e.Value);
                decimal DonGia = 0;
                if (SoLuong <= 0)
                {
                    showMsg("MSG_WRN33");
                    //MLMessages("Dữ liệu không hợp lệ","MSG_WRN33","", false);
                    e.Cancel = true;
                }
                if (e.Column.Key == "SoLuong")
                {
                 HangChuyenTiep HCT = (HangChuyenTiep )dgList.GetRow().DataRow;
                    if (!KiemTraLuongHang(HCT.MaHang.Trim(), SoLuong))
                        e.Cancel = true;
                }
            }
        }

        private void dgList_RecordUpdated(object sender, EventArgs e)
        {
            HangChuyenTiep HCT = (HangChuyenTiep)dgList.GetRow().DataRow;
            HCT.TriGia = HCT.DonGia * HCT.SoLuong;
            //HMD.TriGiaKB_VND = HMD.TriGiaKB * TKCT.TyGiaTinhThue;
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }

        private void dgList_EditingCell(object sender, EditingCellEventArgs e)
        {
            
        }

        

      
    }
}
