using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class AMA_HANGHOA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute RAN { get; set; }
public PropertiesAttribute CMB { get; set; }
public PropertiesAttribute CMA { get; set; }
public PropertiesAttribute ORB { get; set; }
public PropertiesAttribute ORA { get; set; }
public PropertiesAttribute MKB { get; set; }
public PropertiesAttribute MKQ { get; set; }
public PropertiesAttribute MKC { get; set; }
public PropertiesAttribute MKT { get; set; }
public PropertiesAttribute MKR { get; set; }
public PropertiesAttribute MKA { get; set; }
public PropertiesAttribute AKB { get; set; }
public PropertiesAttribute AKQ { get; set; }
public PropertiesAttribute AKC { get; set; }
public PropertiesAttribute AKT { get; set; }
public PropertiesAttribute AKR { get; set; }
public PropertiesAttribute AKA { get; set; }
    //minhnd them ghi chu fix loi HOATHO
public PropertiesAttribute NTB { get; set; }
public PropertiesAttribute NTA { get; set; }
//minhnd them ghi chu fix loi HOATHO
public GroupAttribute MB_ { get; set; }

public AMA_HANGHOA()
        {
RAN = new PropertiesAttribute(2, typeof(string));
CMB = new PropertiesAttribute(600, typeof(string));
CMA = new PropertiesAttribute(600, typeof(string));
ORB = new PropertiesAttribute(2, typeof(string));
ORA = new PropertiesAttribute(2, typeof(string));
MKB = new PropertiesAttribute(17, typeof(int));
MKQ = new PropertiesAttribute(12, typeof(int));
MKC = new PropertiesAttribute(4, typeof(string));
MKT = new PropertiesAttribute(12, typeof(string));
MKR = new PropertiesAttribute(30, typeof(string));
MKA = new PropertiesAttribute(16, typeof(string));
AKB = new PropertiesAttribute(17, typeof(int));
AKQ = new PropertiesAttribute(12, typeof(int));
AKC = new PropertiesAttribute(4, typeof(string));
AKT = new PropertiesAttribute(12, typeof(string));
AKR = new PropertiesAttribute(30, typeof(string));
AKA = new PropertiesAttribute(16, typeof(string));

//minhnd them ghi chu fix loi HOATHO
NTB = new PropertiesAttribute(100, typeof(string));
NTA = new PropertiesAttribute(100, typeof(string));
//minhnd them ghi chu fix loi HOATHO

#region MB_
List<PropertiesAttribute> listMB_ = new List<PropertiesAttribute>();
listMB_.Add(new PropertiesAttribute(17, 5, EnumGroupID.AMA_HANGHOA_MB_, typeof(int)));
listMB_.Add(new PropertiesAttribute(12, 5, EnumGroupID.AMA_HANGHOA_MQ_, typeof(int)));
listMB_.Add(new PropertiesAttribute(4, 5, EnumGroupID.AMA_HANGHOA_MC_, typeof(string)));
listMB_.Add(new PropertiesAttribute(10, 5, EnumGroupID.AMA_HANGHOA_MK_, typeof(string)));
listMB_.Add(new PropertiesAttribute(25, 5, EnumGroupID.AMA_HANGHOA_MR_, typeof(string)));
listMB_.Add(new PropertiesAttribute(16, 5, EnumGroupID.AMA_HANGHOA_MA_, typeof(string)));
listMB_.Add(new PropertiesAttribute(17, 5, EnumGroupID.AMA_HANGHOA_AB_, typeof(int)));
listMB_.Add(new PropertiesAttribute(12, 5, EnumGroupID.AMA_HANGHOA_AQ_, typeof(int)));
listMB_.Add(new PropertiesAttribute(4, 5, EnumGroupID.AMA_HANGHOA_AC_, typeof(string)));
listMB_.Add(new PropertiesAttribute(10, 5, EnumGroupID.AMA_HANGHOA_AK_, typeof(string)));
listMB_.Add(new PropertiesAttribute(25, 5, EnumGroupID.AMA_HANGHOA_AR_, typeof(string)));
listMB_.Add(new PropertiesAttribute(16, 5, EnumGroupID.AMA_HANGHOA_AA_, typeof(string)));
MB_ = new GroupAttribute("MB_", 5, listMB_);
#endregion MB_
TongSoByte = 2382;
}

}public partial class EnumGroupID
    {
public static readonly string AMA_HANGHOA_MB_ = "AMA_HANGHOA_MB_";
public static readonly string AMA_HANGHOA_MQ_ = "AMA_HANGHOA_MQ_";
public static readonly string AMA_HANGHOA_MC_ = "AMA_HANGHOA_MC_";
public static readonly string AMA_HANGHOA_MK_ = "AMA_HANGHOA_MK_";
public static readonly string AMA_HANGHOA_MR_ = "AMA_HANGHOA_MR_";
public static readonly string AMA_HANGHOA_MA_ = "AMA_HANGHOA_MA_";
public static readonly string AMA_HANGHOA_AB_ = "AMA_HANGHOA_AB_";
public static readonly string AMA_HANGHOA_AQ_ = "AMA_HANGHOA_AQ_";
public static readonly string AMA_HANGHOA_AC_ = "AMA_HANGHOA_AC_";
public static readonly string AMA_HANGHOA_AK_ = "AMA_HANGHOA_AK_";
public static readonly string AMA_HANGHOA_AR_ = "AMA_HANGHOA_AR_";
public static readonly string AMA_HANGHOA_AA_ = "AMA_HANGHOA_AA_";
}

}
