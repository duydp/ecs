﻿namespace Company.Interface.Report
{
    partial class CauHinhInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CauHinhInForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rvTLHH = new Company.Controls.CustomValidation.RangeValidator();
            this.txtDinhMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rangeValidator1 = new Company.Controls.CustomValidation.RangeValidator();
            this.txtLuongNPL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.rangeValidator2 = new Company.Controls.CustomValidation.RangeValidator();
            this.txtLuongSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTriGiaNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTLHH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rdNgayTuNhap = new Janus.Windows.EditControls.UIRadioButton();
            this.rdNgayTuDong = new Janus.Windows.EditControls.UIRadioButton();
            this.txtDiaPhuong = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.numTimeDelay = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.numFontDongHang = new System.Windows.Forms.NumericUpDown();
            this.cboXuongDongThongTinDiaChi = new Janus.Windows.EditControls.UIComboBox();
            this.cboChiPhi = new Janus.Windows.EditControls.UIComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rvTLHH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFontDongHang)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Size = new System.Drawing.Size(472, 346);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Định mức";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lượng NPL";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rvTLHH
            // 
            this.rvTLHH.ControlToValidate = this.txtDinhMuc;
            this.rvTLHH.ErrorMessage = "\"Định mức không hợp lệ\"";
            this.rvTLHH.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTLHH.Icon")));
            this.rvTLHH.MaximumValue = "8";
            this.rvTLHH.MinimumValue = "0";
            this.rvTLHH.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // txtDinhMuc
            // 
            this.txtDinhMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDinhMuc.Location = new System.Drawing.Point(107, 19);
            this.txtDinhMuc.Name = "txtDinhMuc";
            this.txtDinhMuc.Size = new System.Drawing.Size(121, 21);
            this.txtDinhMuc.TabIndex = 0;
            this.txtDinhMuc.Text = "0";
            this.txtDinhMuc.Value = 0;
            this.txtDinhMuc.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtDinhMuc.VisualStyleManager = this.vsmMain;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rangeValidator1
            // 
            this.rangeValidator1.ControlToValidate = this.txtLuongNPL;
            this.rangeValidator1.ErrorMessage = "\"Lượng NPL không hợp lệ\"";
            this.rangeValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator1.Icon")));
            this.rangeValidator1.MaximumValue = "100";
            this.rangeValidator1.MinimumValue = "0";
            this.rangeValidator1.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // txtLuongNPL
            // 
            this.txtLuongNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongNPL.Location = new System.Drawing.Point(107, 46);
            this.txtLuongNPL.Name = "txtLuongNPL";
            this.txtLuongNPL.Size = new System.Drawing.Size(121, 21);
            this.txtLuongNPL.TabIndex = 1;
            this.txtLuongNPL.Text = "0";
            this.txtLuongNPL.Value = 0;
            this.txtLuongNPL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtLuongNPL.VisualStyleManager = this.vsmMain;
            // 
            // rangeValidator2
            // 
            this.rangeValidator2.ControlToValidate = this.txtLuongSP;
            this.rangeValidator2.ErrorMessage = "\"Lượng SP không hợp lệ\"";
            this.rangeValidator2.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator2.Icon")));
            this.rangeValidator2.MaximumValue = "6";
            this.rangeValidator2.MinimumValue = "0";
            this.rangeValidator2.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // txtLuongSP
            // 
            this.txtLuongSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongSP.Location = new System.Drawing.Point(107, 73);
            this.txtLuongSP.Name = "txtLuongSP";
            this.txtLuongSP.Size = new System.Drawing.Size(121, 21);
            this.txtLuongSP.TabIndex = 2;
            this.txtLuongSP.Text = "0";
            this.txtLuongSP.Value = 0;
            this.txtLuongSP.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtLuongSP.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(152, 314);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Lượng SP";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaNT);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.txtTLHH);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.txtDinhMuc);
            this.uiGroupBox1.Controls.Add(this.txtLuongSP);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.txtLuongNPL);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 71);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(448, 129);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.Text = "Báo cáo thông tư 174";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(413, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "0 - 6";
            // 
            // txtTriGiaNT
            // 
            this.txtTriGiaNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaNT.Location = new System.Drawing.Point(321, 19);
            this.txtTriGiaNT.Name = "txtTriGiaNT";
            this.txtTriGiaNT.Size = new System.Drawing.Size(86, 21);
            this.txtTriGiaNT.TabIndex = 4;
            this.txtTriGiaNT.Text = "0";
            this.txtTriGiaNT.Value = 0;
            this.txtTriGiaNT.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTriGiaNT.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(263, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Trị giá NT";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(234, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "*";
            // 
            // txtTLHH
            // 
            this.txtTLHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTLHH.Location = new System.Drawing.Point(107, 100);
            this.txtTLHH.Name = "txtTLHH";
            this.txtTLHH.Size = new System.Drawing.Size(121, 21);
            this.txtTLHH.TabIndex = 3;
            this.txtTLHH.Text = "0";
            this.txtTLHH.Value = 0;
            this.txtTLHH.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTLHH.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Tỷ lệ hao hụt";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(234, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(234, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(234, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "*";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.rdNgayTuNhap);
            this.uiGroupBox2.Controls.Add(this.rdNgayTuDong);
            this.uiGroupBox2.Controls.Add(this.txtDiaPhuong);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 2);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(448, 63);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Báo cáo chung";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(-6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Ngày..tháng..năm..";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rdNgayTuNhap
            // 
            this.rdNgayTuNhap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdNgayTuNhap.Location = new System.Drawing.Point(268, 37);
            this.rdNgayTuNhap.Name = "rdNgayTuNhap";
            this.rdNgayTuNhap.Size = new System.Drawing.Size(104, 23);
            this.rdNgayTuNhap.TabIndex = 2;
            this.rdNgayTuNhap.Text = "Để trống";
            // 
            // rdNgayTuDong
            // 
            this.rdNgayTuDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdNgayTuDong.Location = new System.Drawing.Point(138, 37);
            this.rdNgayTuDong.Name = "rdNgayTuDong";
            this.rdNgayTuDong.Size = new System.Drawing.Size(124, 23);
            this.rdNgayTuDong.TabIndex = 1;
            this.rdNgayTuDong.Text = "Ngày hệ thống";
            // 
            // txtDiaPhuong
            // 
            this.txtDiaPhuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaPhuong.Location = new System.Drawing.Point(138, 16);
            this.txtDiaPhuong.Name = "txtDiaPhuong";
            this.txtDiaPhuong.Size = new System.Drawing.Size(195, 21);
            this.txtDiaPhuong.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tỉnh/Thành phố";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(335, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "*";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label26);
            this.uiGroupBox3.Controls.Add(this.numTimeDelay);
            this.uiGroupBox3.Controls.Add(this.label35);
            this.uiGroupBox3.Controls.Add(this.label34);
            this.uiGroupBox3.Controls.Add(this.label29);
            this.uiGroupBox3.Controls.Add(this.numFontDongHang);
            this.uiGroupBox3.Controls.Add(this.cboXuongDongThongTinDiaChi);
            this.uiGroupBox3.Controls.Add(this.cboChiPhi);
            this.uiGroupBox3.Controls.Add(this.label25);
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 206);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(448, 102);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.Text = "Thông tin tờ khai";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(231, 36);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 13);
            this.label26.TabIndex = 40;
            this.label26.Text = "Font dòng hàng";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numTimeDelay
            // 
            this.numTimeDelay.Location = new System.Drawing.Point(328, 58);
            this.numTimeDelay.Name = "numTimeDelay";
            this.numTimeDelay.Size = new System.Drawing.Size(54, 21);
            this.numTimeDelay.TabIndex = 3;
            this.numTimeDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(385, 63);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 13);
            this.label35.TabIndex = 44;
            this.label35.Text = "(giây)";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(234, 48);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(99, 41);
            this.label34.TabIndex = 42;
            this.label34.Text = "Thời gian chờ gửi và nhận thông tin";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(10, 58);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(96, 27);
            this.label29.TabIndex = 41;
            this.label29.Text = "Xuống dòng thông tin địa chỉ";
            // 
            // numFontDongHang
            // 
            this.numFontDongHang.Location = new System.Drawing.Point(328, 29);
            this.numFontDongHang.Name = "numFontDongHang";
            this.numFontDongHang.Size = new System.Drawing.Size(54, 21);
            this.numFontDongHang.TabIndex = 2;
            this.numFontDongHang.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboXuongDongThongTinDiaChi
            // 
            this.cboXuongDongThongTinDiaChi.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboXuongDongThongTinDiaChi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Không";
            uiComboBoxItem1.Value = false;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Có";
            uiComboBoxItem2.Value = true;
            this.cboXuongDongThongTinDiaChi.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cboXuongDongThongTinDiaChi.Location = new System.Drawing.Point(106, 58);
            this.cboXuongDongThongTinDiaChi.Name = "cboXuongDongThongTinDiaChi";
            this.cboXuongDongThongTinDiaChi.Office2007ColorScheme = Janus.Windows.UI.Office2007ColorScheme.Blue;
            this.cboXuongDongThongTinDiaChi.Size = new System.Drawing.Size(121, 21);
            this.cboXuongDongThongTinDiaChi.TabIndex = 1;
            // 
            // cboChiPhi
            // 
            this.cboChiPhi.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboChiPhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Không";
            uiComboBoxItem3.Value = 0;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Có";
            uiComboBoxItem4.Value = 1;
            this.cboChiPhi.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cboChiPhi.Location = new System.Drawing.Point(106, 31);
            this.cboChiPhi.Name = "cboChiPhi";
            this.cboChiPhi.Size = new System.Drawing.Size(121, 21);
            this.cboChiPhi.TabIndex = 0;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(10, 26);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 32);
            this.label25.TabIndex = 39;
            this.label25.Text = "Hiển thị chi phí trên tờ khai";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(246, 314);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // CauHinhInForm
            // 
            this.AcceptButton = this.btnSave;
            this.ClientSize = new System.Drawing.Size(472, 346);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CauHinhInForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cấu hình báo cáo";
            this.Load += new System.EventHandler(this.CauHinhInForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rvTLHH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFontDongHang)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Company.Controls.CustomValidation.RangeValidator rvTLHH;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RangeValidator rangeValidator1;
        private Company.Controls.CustomValidation.RangeValidator rangeValidator2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDinhMuc;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongSP;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongNPL;
        private Janus.Windows.EditControls.UIButton btnSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.EditControls.UIRadioButton rdNgayTuNhap;
        private Janus.Windows.EditControls.UIRadioButton rdNgayTuDong;
        private System.Windows.Forms.TextBox txtDiaPhuong;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTLHH;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.NumericUpDown numFontDongHang;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.EditControls.UIComboBox cboXuongDongThongTinDiaChi;
        private Janus.Windows.EditControls.UIComboBox cboChiPhi;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaNT;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown numTimeDelay;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private Janus.Windows.EditControls.UIButton btnClose;
    }
}
