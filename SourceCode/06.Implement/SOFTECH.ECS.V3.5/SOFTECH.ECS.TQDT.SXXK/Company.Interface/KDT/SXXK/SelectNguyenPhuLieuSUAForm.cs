﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.KDT.SXXK
{
    public partial class SelectNguyenPhuLieuSUAForm : BaseForm
    {
        public NguyenPhuLieuCollection nplCollection = new NguyenPhuLieuCollection();
        public NguyenPhuLieuDangKy NPLDangKy = new NguyenPhuLieuDangKy();
        public List<NguyenPhuLieuSUA> listNplSUA = new List<NguyenPhuLieuSUA>();
        public long masterId;

        public SelectNguyenPhuLieuSUAForm()
        {
            InitializeComponent();
        }

        private void btnChonNhieuHang_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieu npl = (NguyenPhuLieu)i.GetRow().DataRow;
                        NguyenPhuLieuSUA nplSua = new NguyenPhuLieuSUA();
                        nplSua.Ma = npl.Ma;
                        nplSua.Ten = npl.Ten;
                        nplSua.MaHS = npl.MaHS;
                        nplSua.STTHang = npl.STTHang;
                        nplSua.DVT_ID = npl.DVT_ID;
                        nplSua.IDNPL = npl.ID;
                        listNplSUA.Add(nplSua);
                    }
                }
            }
            this.Close();
        }

        private void btnChonAll_Click(object sender, EventArgs e)
        {
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectSanPhamForm_Load(object sender, EventArgs e)
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            BindData();
        }
        public void BindData()
        {
            //DataSet ds = Company.BLL.KDT.SXXK.SanPham.SelectCollectionBy_Master_ID(masterId);
            NguyenPhuLieuCollection ds = Company.BLL.KDT.SXXK.NguyenPhuLieu.SelectCollectionBy_Master_ID(masterId);//Company.BLL.KDT.SXXK.SanPham.SelectCollectionBy_Master_ID(masterId);
            dgList.DataSource = ds;

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }
    }
}
