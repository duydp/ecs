﻿using System.ComponentModel;
using System.Windows.Forms;
using Company.Interface.Controls;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface
{
    partial class HangMauDichForm
    {
        private UIGroupBox uiGroupBox1;
        private Label label16;
        private Label label17;
        private Label label19;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label27;
        private UIGroupBox uiGroupBox2;
        private UIGroupBox grbThue;
        private Label label1;
        private Label label5;
        private Label label13;
        private Label label14;
        private UIGroupBox uiGroupBox4;
        private UIButton btnGhi;
        private NumericEditBox txtDGNT;
        private EditBox txtMaHang;
        private EditBox txtTenHang;
        private EditBox txtMaHS;
        private NumericEditBox txtTienThue_GTGT;
        private NumericEditBox txtTienThue_NK;
        private NumericEditBox txtTGTT_TTDB;
        private NumericEditBox txtTGTT_GTGT;
        private NumericEditBox txtTGTT_NK;
        private NumericEditBox txtTS_GTGT;
        private NumericEditBox txtTS_NK;
        private NumericEditBox txtTienThue_TTDB;
        private NumericEditBox txtTS_TTDB;
        private Label label18;
        private ToolTip toolTip1;
        private Label lblTongTGKB;
        private Label lblTyGiaTT;
        private Label lblNguyenTe_DGNT;
        private Label lblNguyenTe_TGNT;
        private ErrorProvider epError;
        private NuocHControl ctrNuocXX;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HangMauDichForm));
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dglContainer_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.DVT_QuyDoiCtr = new Company.Interface.Controls.DonViTinhQuyDoiControl();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtNhanHieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnChungTuTruoc = new Janus.Windows.EditControls.UIButton();
            this.txtQuyCach = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtThanhPhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtModel = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtTenHangSX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSMoRong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtMaHangSX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkHangDongBo = new Janus.Windows.EditControls.UICheckBox();
            this.chkIsOld = new Janus.Windows.EditControls.UICheckBox();
            this.chkThueTuyetDoi = new Janus.Windows.EditControls.UICheckBox();
            this.cbDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.chkHangFOC = new Janus.Windows.EditControls.UICheckBox();
            this.txtMaHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTongTienThue = new System.Windows.Forms.Label();
            this.lblTongTGKB = new System.Windows.Forms.Label();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SaoChepCha = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cmbCO = new Janus.Windows.EditControls.UIComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDonGiaTuyetDoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTriGiaKB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrNuocXX = new Company.Interface.Controls.NuocHControl();
            this.lblNguyenTe_TGNT = new System.Windows.Forms.Label();
            this.lblNguyenTe_DGNT = new System.Windows.Forms.Label();
            this.txtDGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.grbThue = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTabMienGiamThue = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.lblCopyBieuThue = new System.Windows.Forms.LinkLabel();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTTBVMT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTSVatGiam = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtTGTT_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtTSBVMT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTriGiaTinhThueBVMT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cbBieuThueGTGT = new Janus.Windows.EditControls.UIComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtTienThue_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.cbBieuThueBVMT = new Janus.Windows.EditControls.UIComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTSTTDBGiam = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTGTT_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.txtTienThue_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTSXNKGiam = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.cbBieuThueXNK = new Janus.Windows.EditControls.UIComboBox();
            this.cbBieuThueTTDB = new Janus.Windows.EditControls.UIComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.txtTien_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTL_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThueCPG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtTSCPG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.txtTriGiaTTCPG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.cbBieuThueCBPG = new Janus.Windows.EditControls.UIComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtTyLeThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numericEditBox1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtCLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtGiamThue_SoVanBan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGiamThue_ThueSuatGoc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtGiamThue_TyLeGiam = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label47 = new System.Windows.Forms.Label();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.dglContainer = new Janus.Windows.GridEX.GridEX();
            this.label73 = new System.Windows.Forms.Label();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.lbxChungTuKem = new System.Windows.Forms.ListBox();
            this.label72 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.numericEditBox3 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.chkMienThue = new Janus.Windows.EditControls.UICheckBox();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.lblTyGiaTT = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.revMaHS = new Company.Controls.CustomValidation.RegularExpressionValidator();
            this.rvLuong = new Company.Controls.CustomValidation.RangeValidator();
            this.rvDGNT = new Company.Controls.CustomValidation.RangeValidator();
            this.cvSoLuong = new Company.Controls.CustomValidation.CompareValidator();
            this.rfvBieuThueXNK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvBieuThueGTGT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.linkSaoChep_CheDoUuDai = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThue)).BeginInit();
            this.grbThue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTabMienGiamThue)).BeginInit();
            this.uiTabMienGiamThue.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dglContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvDGNT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvBieuThueXNK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvBieuThueGTGT)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(1284, 757);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1284, 757);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox3.Controls.Add(this.lblTongTienThue);
            this.uiGroupBox3.Controls.Add(this.lblTongTGKB);
            this.uiGroupBox3.Controls.Add(this.btnAddNew);
            this.uiGroupBox3.Controls.Add(this.dgList);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox3.Controls.Add(this.grbThue);
            this.uiGroupBox3.Controls.Add(this.btnXoa);
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnGhi);
            this.uiGroupBox3.Controls.Add(this.lblTyGiaTT);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1284, 757);
            this.uiGroupBox3.TabIndex = 11;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.DVT_QuyDoiCtr);
            this.uiGroupBox2.Controls.Add(this.label36);
            this.uiGroupBox2.Controls.Add(this.label37);
            this.uiGroupBox2.Controls.Add(this.txtNhanHieu);
            this.uiGroupBox2.Controls.Add(this.btnChungTuTruoc);
            this.uiGroupBox2.Controls.Add(this.txtQuyCach);
            this.uiGroupBox2.Controls.Add(this.label34);
            this.uiGroupBox2.Controls.Add(this.label35);
            this.uiGroupBox2.Controls.Add(this.txtThanhPhan);
            this.uiGroupBox2.Controls.Add(this.txtModel);
            this.uiGroupBox2.Controls.Add(this.label33);
            this.uiGroupBox2.Controls.Add(this.label32);
            this.uiGroupBox2.Controls.Add(this.txtTenHangSX);
            this.uiGroupBox2.Controls.Add(this.txtMaHSMoRong);
            this.uiGroupBox2.Controls.Add(this.label31);
            this.uiGroupBox2.Controls.Add(this.txtMaHangSX);
            this.uiGroupBox2.Controls.Add(this.chkHangDongBo);
            this.uiGroupBox2.Controls.Add(this.chkIsOld);
            this.uiGroupBox2.Controls.Add(this.chkThueTuyetDoi);
            this.uiGroupBox2.Controls.Add(this.cbDonViTinh);
            this.uiGroupBox2.Controls.Add(this.chkHangFOC);
            this.uiGroupBox2.Controls.Add(this.txtMaHang);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.txtTenHang);
            this.uiGroupBox2.Controls.Add(this.txtMaHS);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(9, 9);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1269, 157);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Phần tên hàng";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // DVT_QuyDoiCtr
            // 
            this.DVT_QuyDoiCtr.BackColor = System.Drawing.Color.Transparent;
            this.DVT_QuyDoiCtr.Location = new System.Drawing.Point(668, 15);
            this.DVT_QuyDoiCtr.Name = "DVT_QuyDoiCtr";
            this.DVT_QuyDoiCtr.Size = new System.Drawing.Size(245, 96);
            this.DVT_QuyDoiCtr.TabIndex = 41;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(231, 103);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(52, 13);
            this.label36.TabIndex = 40;
            this.label36.Text = "Quy cách";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(12, 103);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(55, 13);
            this.label37.TabIndex = 38;
            this.label37.Text = "Nhãn hiệu";
            // 
            // txtNhanHieu
            // 
            this.txtNhanHieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNhanHieu.Location = new System.Drawing.Point(80, 98);
            this.txtNhanHieu.MaxLength = 256;
            this.txtNhanHieu.Name = "txtNhanHieu";
            this.txtNhanHieu.Size = new System.Drawing.Size(139, 21);
            this.txtNhanHieu.TabIndex = 7;
            this.txtNhanHieu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNhanHieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNhanHieu.VisualStyleManager = this.vsmMain;
            // 
            // btnChungTuTruoc
            // 
            this.btnChungTuTruoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChungTuTruoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChungTuTruoc.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChungTuTruoc.Icon")));
            this.btnChungTuTruoc.Location = new System.Drawing.Point(767, 117);
            this.btnChungTuTruoc.Name = "btnChungTuTruoc";
            this.btnChungTuTruoc.Size = new System.Drawing.Size(144, 31);
            this.btnChungTuTruoc.TabIndex = 1;
            this.btnChungTuTruoc.Text = "Thủ tục HQ trước đó";
            this.btnChungTuTruoc.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnChungTuTruoc.Click += new System.EventHandler(this.btnChungTuTruoc_Click);
            // 
            // txtQuyCach
            // 
            this.txtQuyCach.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtQuyCach.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtQuyCach.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuyCach.Location = new System.Drawing.Point(299, 98);
            this.txtQuyCach.MaxLength = 1000;
            this.txtQuyCach.Name = "txtQuyCach";
            this.txtQuyCach.Size = new System.Drawing.Size(197, 21);
            this.txtQuyCach.TabIndex = 8;
            this.txtQuyCach.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtQuyCach.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtQuyCach.VisualStyleManager = this.vsmMain;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(231, 130);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(35, 13);
            this.label34.TabIndex = 36;
            this.label34.Text = "Model";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(12, 130);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(64, 13);
            this.label35.TabIndex = 34;
            this.label35.Text = "Thành phần";
            // 
            // txtThanhPhan
            // 
            this.txtThanhPhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhPhan.Location = new System.Drawing.Point(80, 125);
            this.txtThanhPhan.MaxLength = 100;
            this.txtThanhPhan.Name = "txtThanhPhan";
            this.txtThanhPhan.Size = new System.Drawing.Size(139, 21);
            this.txtThanhPhan.TabIndex = 9;
            this.txtThanhPhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThanhPhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtThanhPhan.VisualStyleManager = this.vsmMain;
            // 
            // txtModel
            // 
            this.txtModel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtModel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtModel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModel.Location = new System.Drawing.Point(299, 125);
            this.txtModel.MaxLength = 35;
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(197, 21);
            this.txtModel.TabIndex = 10;
            this.txtModel.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtModel.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtModel.VisualStyleManager = this.vsmMain;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(445, 48);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(79, 13);
            this.label33.TabIndex = 32;
            this.label33.Text = "Mã HS mở rộng";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(231, 76);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(67, 13);
            this.label32.TabIndex = 30;
            this.label32.Text = "Tên hãng SX";
            // 
            // txtTenHangSX
            // 
            this.txtTenHangSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangSX.Location = new System.Drawing.Point(299, 71);
            this.txtTenHangSX.MaxLength = 256;
            this.txtTenHangSX.Name = "txtTenHangSX";
            this.txtTenHangSX.Size = new System.Drawing.Size(197, 21);
            this.txtTenHangSX.TabIndex = 6;
            this.txtTenHangSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHangSX.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHSMoRong
            // 
            this.txtMaHSMoRong.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHSMoRong.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHSMoRong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSMoRong.Location = new System.Drawing.Point(529, 43);
            this.txtMaHSMoRong.MaxLength = 12;
            this.txtMaHSMoRong.Name = "txtMaHSMoRong";
            this.txtMaHSMoRong.Size = new System.Drawing.Size(132, 21);
            this.txtMaHSMoRong.TabIndex = 4;
            this.txtMaHSMoRong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHSMoRong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHSMoRong.VisualStyleManager = this.vsmMain;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(12, 76);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(63, 13);
            this.label31.TabIndex = 28;
            this.label31.Text = "Mã hãng SX";
            // 
            // txtMaHangSX
            // 
            this.txtMaHangSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangSX.Location = new System.Drawing.Point(80, 71);
            this.txtMaHangSX.MaxLength = 30;
            this.txtMaHangSX.Name = "txtMaHangSX";
            this.txtMaHangSX.Size = new System.Drawing.Size(139, 21);
            this.txtMaHangSX.TabIndex = 5;
            this.txtMaHangSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHangSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHangSX.VisualStyleManager = this.vsmMain;
            // 
            // chkHangDongBo
            // 
            this.chkHangDongBo.BackColor = System.Drawing.Color.Transparent;
            this.chkHangDongBo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHangDongBo.ForeColor = System.Drawing.Color.Blue;
            this.chkHangDongBo.Location = new System.Drawing.Point(664, 128);
            this.chkHangDongBo.Name = "chkHangDongBo";
            this.chkHangDongBo.Size = new System.Drawing.Size(129, 17);
            this.chkHangDongBo.TabIndex = 12;
            this.chkHangDongBo.Text = "Hàng đồng bộ";
            this.chkHangDongBo.VisualStyleManager = this.vsmMain;
            // 
            // chkIsOld
            // 
            this.chkIsOld.BackColor = System.Drawing.Color.Transparent;
            this.chkIsOld.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsOld.ForeColor = System.Drawing.Color.Blue;
            this.chkIsOld.Location = new System.Drawing.Point(529, 128);
            this.chkIsOld.Name = "chkIsOld";
            this.chkIsOld.Size = new System.Drawing.Size(129, 17);
            this.chkIsOld.TabIndex = 12;
            this.chkIsOld.Text = "Hàng cũ";
            this.chkIsOld.VisualStyleManager = this.vsmMain;
            this.chkIsOld.CheckedChanged += new System.EventHandler(this.uiCheckBox1_CheckedChanged);
            // 
            // chkThueTuyetDoi
            // 
            this.chkThueTuyetDoi.BackColor = System.Drawing.Color.Transparent;
            this.chkThueTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkThueTuyetDoi.ForeColor = System.Drawing.Color.Blue;
            this.chkThueTuyetDoi.Location = new System.Drawing.Point(529, 102);
            this.chkThueTuyetDoi.Name = "chkThueTuyetDoi";
            this.chkThueTuyetDoi.Size = new System.Drawing.Size(129, 14);
            this.chkThueTuyetDoi.TabIndex = 12;
            this.chkThueTuyetDoi.Text = "Thuế tuyệt đối";
            this.chkThueTuyetDoi.VisualStyleManager = this.vsmMain;
            this.chkThueTuyetDoi.CheckedChanged += new System.EventHandler(this.uiCheckBox1_CheckedChanged);
            // 
            // cbDonViTinh
            // 
            this.cbDonViTinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDonViTinh.DisplayMember = "Ten";
            this.cbDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDonViTinh.Location = new System.Drawing.Point(299, 43);
            this.cbDonViTinh.Name = "cbDonViTinh";
            this.cbDonViTinh.Size = new System.Drawing.Size(140, 21);
            this.cbDonViTinh.TabIndex = 3;
            this.cbDonViTinh.ValueMember = "ID";
            this.cbDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbDonViTinh.VisualStyleManager = this.vsmMain;
            // 
            // chkHangFOC
            // 
            this.chkHangFOC.BackColor = System.Drawing.Color.Transparent;
            this.chkHangFOC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHangFOC.ForeColor = System.Drawing.Color.Blue;
            this.chkHangFOC.Location = new System.Drawing.Point(529, 75);
            this.chkHangFOC.Name = "chkHangFOC";
            this.chkHangFOC.Size = new System.Drawing.Size(130, 14);
            this.chkHangFOC.TabIndex = 11;
            this.chkHangFOC.Text = "Hàng FOC";
            this.chkHangFOC.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHang
            // 
            this.txtMaHang.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHang.Location = new System.Drawing.Point(80, 15);
            this.txtMaHang.MaxLength = 30;
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(139, 21);
            this.txtMaHang.TabIndex = 0;
            this.txtMaHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHang.VisualStyleManager = this.vsmMain;
            this.txtMaHang.ButtonClick += new System.EventHandler(this.txtMaHang_ButtonClick);
            this.txtMaHang.Leave += new System.EventHandler(this.txtMaNPL_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã HS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(231, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên hàng";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(12, 20);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(48, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã hàng";
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(300, 15);
            this.txtTenHang.MaxLength = 255;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(362, 21);
            this.txtTenHang.TabIndex = 1;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHS
            // 
            this.txtMaHS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHS.Location = new System.Drawing.Point(80, 42);
            this.txtMaHS.MaxLength = 12;
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(139, 21);
            this.txtMaHS.TabIndex = 2;
            this.txtMaHS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHS.VisualStyleManager = this.vsmMain;
            this.txtMaHS.TextChanged += new System.EventHandler(this.txtMaHS_TextChanged);
            this.txtMaHS.Leave += new System.EventHandler(this.txtMaHS_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(231, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Đơn vị tính";
            // 
            // lblTongTienThue
            // 
            this.lblTongTienThue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTongTienThue.AutoSize = true;
            this.lblTongTienThue.BackColor = System.Drawing.Color.Transparent;
            this.lblTongTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTienThue.Location = new System.Drawing.Point(306, 736);
            this.lblTongTienThue.Name = "lblTongTienThue";
            this.lblTongTienThue.Size = new System.Drawing.Size(95, 13);
            this.lblTongTienThue.TabIndex = 10;
            this.lblTongTienThue.Text = "Tổng tiền thuế :";
            // 
            // lblTongTGKB
            // 
            this.lblTongTGKB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTongTGKB.AutoSize = true;
            this.lblTongTGKB.BackColor = System.Drawing.Color.Transparent;
            this.lblTongTGKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTGKB.Location = new System.Drawing.Point(7, 736);
            this.lblTongTGKB.Name = "lblTongTGKB";
            this.lblTongTGKB.Size = new System.Drawing.Size(134, 13);
            this.lblTongTGKB.TabIndex = 5;
            this.lblTongTGKB.Text = "Tổng trị giá nguyên tệ:";
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddNew.Icon")));
            this.btnAddNew.Location = new System.Drawing.Point(951, 506);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnAddNew.TabIndex = 1;
            this.btnAddNew.Text = "Tạo mới";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddNew.VisualStyleManager = this.vsmMain;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(9, 538);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1269, 194);
            this.dgList.TabIndex = 4;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaoChepCha});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(165, 26);
            // 
            // SaoChepCha
            // 
            this.SaoChepCha.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepCha.Image")));
            this.SaoChepCha.Name = "SaoChepCha";
            this.SaoChepCha.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.SaoChepCha.Size = new System.Drawing.Size(164, 22);
            this.SaoChepCha.Text = "Sao chép";
            this.SaoChepCha.Click += new System.EventHandler(this.SaoChepCha_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "filesaveas.png");
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.linkSaoChep_CheDoUuDai);
            this.uiGroupBox4.Controls.Add(this.cmbCO);
            this.uiGroupBox4.Controls.Add(this.label30);
            this.uiGroupBox4.Controls.Add(this.linkLabel2);
            this.uiGroupBox4.Controls.Add(this.linkLabel1);
            this.uiGroupBox4.Controls.Add(this.label39);
            this.uiGroupBox4.Controls.Add(this.label38);
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.txtDonGiaTuyetDoi);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaKB);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Controls.Add(this.txtLuong);
            this.uiGroupBox4.Controls.Add(this.ctrNuocXX);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_TGNT);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_DGNT);
            this.uiGroupBox4.Controls.Add(this.txtDGNT);
            this.uiGroupBox4.Controls.Add(this.txtTGNT);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(9, 172);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1269, 108);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Phần số lượng";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // cmbCO
            // 
            this.cmbCO.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbCO.DisplayMember = "Ten";
            this.cmbCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCO.Location = new System.Drawing.Point(709, 75);
            this.cmbCO.Name = "cmbCO";
            this.cmbCO.Size = new System.Drawing.Size(140, 21);
            this.cmbCO.TabIndex = 24;
            this.cmbCO.ValueMember = "ID";
            this.cmbCO.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmbCO.VisualStyleManager = this.vsmMain;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(629, 80);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(74, 13);
            this.label30.TabIndex = 25;
            this.label30.Text = "Chế độ ưu đãi";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.Location = new System.Drawing.Point(777, 49);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(72, 12);
            this.linkLabel2.TabIndex = 21;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Nhấn vào đây";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(765, 21);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(72, 12);
            this.linkLabel1.TabIndex = 20;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Nhấn vào đây";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label39.Location = new System.Drawing.Point(605, 48);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(169, 12);
            this.label39.TabIndex = 23;
            this.label39.Text = "- Cấu hình phương pháp tính thuế";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label38.Location = new System.Drawing.Point(605, 21);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(163, 12);
            this.label38.TabIndex = 22;
            this.label38.Text = "- Cấu hình số thập phân vui lòng";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(224, 50);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "(USD)";
            // 
            // txtDonGiaTuyetDoi
            // 
            this.txtDonGiaTuyetDoi.DecimalDigits = 20;
            this.txtDonGiaTuyetDoi.Enabled = false;
            this.txtDonGiaTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTuyetDoi.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaTuyetDoi.Location = new System.Drawing.Point(99, 46);
            this.txtDonGiaTuyetDoi.Name = "txtDonGiaTuyetDoi";
            this.txtDonGiaTuyetDoi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaTuyetDoi.Size = new System.Drawing.Size(119, 21);
            this.txtDonGiaTuyetDoi.TabIndex = 2;
            this.txtDonGiaTuyetDoi.Text = "0";
            this.txtDonGiaTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtDonGiaTuyetDoi, "Đơn giá khai báo");
            this.txtDonGiaTuyetDoi.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaTuyetDoi.VisualStyleManager = this.vsmMain;
            this.txtDonGiaTuyetDoi.Leave += new System.EventHandler(this.txtDonGiaTuyetDoi_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 50);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(90, 13);
            this.label28.TabIndex = 4;
            this.label28.Text = "Đơn giá tuyệt đối";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(224, 80);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(36, 13);
            this.label23.TabIndex = 12;
            this.label23.Text = "(VNĐ)";
            // 
            // txtTriGiaKB
            // 
            this.txtTriGiaKB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTriGiaKB.DecimalDigits = 0;
            this.txtTriGiaKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaKB.Location = new System.Drawing.Point(99, 75);
            this.txtTriGiaKB.Name = "txtTriGiaKB";
            this.txtTriGiaKB.ReadOnly = true;
            this.txtTriGiaKB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaKB.Size = new System.Drawing.Size(119, 21);
            this.txtTriGiaKB.TabIndex = 4;
            this.txtTriGiaKB.TabStop = false;
            this.txtTriGiaKB.Text = "0";
            this.txtTriGiaKB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTriGiaKB, "Trị giá khai báo");
            this.txtTriGiaKB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaKB.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(6, 80);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Trị giá khai báo";
            // 
            // txtLuong
            // 
            this.txtLuong.DecimalDigits = 20;
            this.txtLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtLuong.Location = new System.Drawing.Point(100, 15);
            this.txtLuong.MaxLength = 15;
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLuong.Size = new System.Drawing.Size(119, 21);
            this.txtLuong.TabIndex = 0;
            this.txtLuong.Text = "0";
            this.txtLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLuong.VisualStyleManager = this.vsmMain;
            this.txtLuong.Leave += new System.EventHandler(this.txtLuong_Leave);
            // 
            // ctrNuocXX
            // 
            this.ctrNuocXX.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXX.ErrorMessage = "\"Nước xuất xứ\" không được bỏ trống.";
            this.ctrNuocXX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXX.Location = new System.Drawing.Point(401, 75);
            this.ctrNuocXX.Ma = "";
            this.ctrNuocXX.Name = "ctrNuocXX";
            this.ctrNuocXX.ReadOnly = false;
            this.ctrNuocXX.Size = new System.Drawing.Size(199, 22);
            this.ctrNuocXX.TabIndex = 5;
            this.ctrNuocXX.VisualStyleManager = this.vsmMain;
            // 
            // lblNguyenTe_TGNT
            // 
            this.lblNguyenTe_TGNT.AutoSize = true;
            this.lblNguyenTe_TGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_TGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_TGNT.Location = new System.Drawing.Point(527, 48);
            this.lblNguyenTe_TGNT.Name = "lblNguyenTe_TGNT";
            this.lblNguyenTe_TGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_TGNT.TabIndex = 9;
            this.lblNguyenTe_TGNT.Text = "Mã nguyên tệ";
            // 
            // lblNguyenTe_DGNT
            // 
            this.lblNguyenTe_DGNT.AutoSize = true;
            this.lblNguyenTe_DGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_DGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_DGNT.Location = new System.Drawing.Point(526, 20);
            this.lblNguyenTe_DGNT.Name = "lblNguyenTe_DGNT";
            this.lblNguyenTe_DGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_DGNT.TabIndex = 4;
            this.lblNguyenTe_DGNT.Text = "Mã nguyên tệ";
            // 
            // txtDGNT
            // 
            this.txtDGNT.DecimalDigits = 20;
            this.txtDGNT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDGNT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDGNT.Location = new System.Drawing.Point(401, 15);
            this.txtDGNT.MaxLength = 15;
            this.txtDGNT.Name = "txtDGNT";
            this.txtDGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDGNT.Size = new System.Drawing.Size(119, 21);
            this.txtDGNT.TabIndex = 1;
            this.txtDGNT.Text = "0";
            this.txtDGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtDGNT, "Đơn giá khai báo");
            this.txtDGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDGNT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDGNT.VisualStyleManager = this.vsmMain;
            this.txtDGNT.Leave += new System.EventHandler(this.txtDGNT_Leave);
            // 
            // txtTGNT
            // 
            this.txtTGNT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGNT.DecimalDigits = 20;
            this.txtTGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGNT.FormatString = "G20";
            this.txtTGNT.Location = new System.Drawing.Point(401, 42);
            this.txtTGNT.Name = "txtTGNT";
            this.txtTGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGNT.Size = new System.Drawing.Size(119, 21);
            this.txtTGNT.TabIndex = 3;
            this.txtTGNT.Text = "0";
            this.txtTGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTGNT, "Trị giá khai báo");
            this.txtTGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGNT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGNT.VisualStyleManager = this.vsmMain;
            this.txtTGNT.Leave += new System.EventHandler(this.txtTGNT_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(296, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Đơn giá nguyên tệ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(296, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Trị giá nguyên tệ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số lượng";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(297, 80);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Nước xuất xứ";
            // 
            // grbThue
            // 
            this.grbThue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbThue.BackColor = System.Drawing.Color.Transparent;
            this.grbThue.Controls.Add(this.uiTabMienGiamThue);
            this.grbThue.Controls.Add(this.chkMienThue);
            this.grbThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbThue.Location = new System.Drawing.Point(9, 286);
            this.grbThue.Name = "grbThue";
            this.grbThue.Size = new System.Drawing.Size(1269, 214);
            this.grbThue.TabIndex = 3;
            this.grbThue.Text = "Phần thuế (VND)";
            this.grbThue.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbThue.VisualStyleManager = this.vsmMain;
            // 
            // uiTabMienGiamThue
            // 
            this.uiTabMienGiamThue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTabMienGiamThue.Location = new System.Drawing.Point(3, 17);
            this.uiTabMienGiamThue.Name = "uiTabMienGiamThue";
            this.uiTabMienGiamThue.Size = new System.Drawing.Size(1263, 194);
            this.uiTabMienGiamThue.TabIndex = 0;
            this.uiTabMienGiamThue.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage4});
            this.uiTabMienGiamThue.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.AutoScroll = true;
            this.uiTabPage1.Controls.Add(this.lblCopyBieuThue);
            this.uiTabPage1.Controls.Add(this.label59);
            this.uiTabPage1.Controls.Add(this.label58);
            this.uiTabPage1.Controls.Add(this.label66);
            this.uiTabPage1.Controls.Add(this.label14);
            this.uiTabPage1.Controls.Add(this.txtTTBVMT);
            this.uiTabPage1.Controls.Add(this.txtTongSoTienThue);
            this.uiTabPage1.Controls.Add(this.txtTienThue_GTGT);
            this.uiTabPage1.Controls.Add(this.label11);
            this.uiTabPage1.Controls.Add(this.txtTSVatGiam);
            this.uiTabPage1.Controls.Add(this.txtTS_GTGT);
            this.uiTabPage1.Controls.Add(this.label5);
            this.uiTabPage1.Controls.Add(this.label25);
            this.uiTabPage1.Controls.Add(this.label61);
            this.uiTabPage1.Controls.Add(this.txtTGTT_GTGT);
            this.uiTabPage1.Controls.Add(this.label43);
            this.uiTabPage1.Controls.Add(this.label63);
            this.uiTabPage1.Controls.Add(this.label57);
            this.uiTabPage1.Controls.Add(this.label55);
            this.uiTabPage1.Controls.Add(this.label60);
            this.uiTabPage1.Controls.Add(this.label42);
            this.uiTabPage1.Controls.Add(this.label40);
            this.uiTabPage1.Controls.Add(this.txtTSBVMT);
            this.uiTabPage1.Controls.Add(this.label54);
            this.uiTabPage1.Controls.Add(this.label13);
            this.uiTabPage1.Controls.Add(this.txtTriGiaTinhThueBVMT);
            this.uiTabPage1.Controls.Add(this.cbBieuThueGTGT);
            this.uiTabPage1.Controls.Add(this.label64);
            this.uiTabPage1.Controls.Add(this.label62);
            this.uiTabPage1.Controls.Add(this.txtTienThue_TTDB);
            this.uiTabPage1.Controls.Add(this.label51);
            this.uiTabPage1.Controls.Add(this.label10);
            this.uiTabPage1.Controls.Add(this.label52);
            this.uiTabPage1.Controls.Add(this.cbBieuThueBVMT);
            this.uiTabPage1.Controls.Add(this.label24);
            this.uiTabPage1.Controls.Add(this.txtTSTTDBGiam);
            this.uiTabPage1.Controls.Add(this.txtTS_TTDB);
            this.uiTabPage1.Controls.Add(this.label1);
            this.uiTabPage1.Controls.Add(this.txtTGTT_TTDB);
            this.uiTabPage1.Controls.Add(this.label17);
            this.uiTabPage1.Controls.Add(this.label53);
            this.uiTabPage1.Controls.Add(this.txtTienThue_NK);
            this.uiTabPage1.Controls.Add(this.label56);
            this.uiTabPage1.Controls.Add(this.label16);
            this.uiTabPage1.Controls.Add(this.txtTSXNKGiam);
            this.uiTabPage1.Controls.Add(this.txtTS_NK);
            this.uiTabPage1.Controls.Add(this.txtTGTT_NK);
            this.uiTabPage1.Controls.Add(this.label19);
            this.uiTabPage1.Controls.Add(this.label21);
            this.uiTabPage1.Controls.Add(this.label46);
            this.uiTabPage1.Controls.Add(this.cbBieuThueXNK);
            this.uiTabPage1.Controls.Add(this.cbBieuThueTTDB);
            this.uiTabPage1.Controls.Add(this.label50);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1261, 172);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thuế xuất nhập khẩu";
            // 
            // lblCopyBieuThue
            // 
            this.lblCopyBieuThue.BackColor = System.Drawing.Color.Transparent;
            this.lblCopyBieuThue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyBieuThue.Location = new System.Drawing.Point(738, 25);
            this.lblCopyBieuThue.Name = "lblCopyBieuThue";
            this.lblCopyBieuThue.Size = new System.Drawing.Size(169, 31);
            this.lblCopyBieuThue.TabIndex = 21;
            this.lblCopyBieuThue.TabStop = true;
            this.lblCopyBieuThue.Text = "Sao chép biểu thuế và thuế suất cho tất cả hàng";
            this.lblCopyBieuThue.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblCopyBieuThue_LinkClicked);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(686, 151);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(36, 13);
            this.label59.TabIndex = 12;
            this.label59.Text = "(VNĐ)";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(429, 151);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(18, 13);
            this.label58.TabIndex = 13;
            this.label58.Text = "%";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(736, 130);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(91, 13);
            this.label66.TabIndex = 22;
            this.label66.Text = "Tổng số tiền thuế";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(537, 130);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Tiền thuế GTGT";
            // 
            // txtTTBVMT
            // 
            this.txtTTBVMT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTTBVMT.DecimalDigits = 0;
            this.txtTTBVMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTTBVMT.Location = new System.Drawing.Point(540, 106);
            this.txtTTBVMT.Name = "txtTTBVMT";
            this.txtTTBVMT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTTBVMT.Size = new System.Drawing.Size(140, 21);
            this.txtTTBVMT.TabIndex = 13;
            this.txtTTBVMT.TabStop = false;
            this.txtTTBVMT.Text = "0";
            this.txtTTBVMT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTTBVMT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTTBVMT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTTBVMT.VisualStyleManager = this.vsmMain;
            this.txtTTBVMT.Leave += new System.EventHandler(this.txtTSBVMT_Leave);
            // 
            // txtTongSoTienThue
            // 
            this.txtTongSoTienThue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTongSoTienThue.DecimalDigits = 0;
            this.txtTongSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTienThue.Location = new System.Drawing.Point(739, 146);
            this.txtTongSoTienThue.Name = "txtTongSoTienThue";
            this.txtTongSoTienThue.ReadOnly = true;
            this.txtTongSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTienThue.Size = new System.Drawing.Size(140, 21);
            this.txtTongSoTienThue.TabIndex = 19;
            this.txtTongSoTienThue.TabStop = false;
            this.txtTongSoTienThue.Text = "0";
            this.txtTongSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTienThue_GTGT
            // 
            this.txtTienThue_GTGT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_GTGT.DecimalDigits = 0;
            this.txtTienThue_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_GTGT.Location = new System.Drawing.Point(540, 146);
            this.txtTienThue_GTGT.Name = "txtTienThue_GTGT";
            this.txtTienThue_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_GTGT.Size = new System.Drawing.Size(140, 21);
            this.txtTienThue_GTGT.TabIndex = 18;
            this.txtTienThue_GTGT.TabStop = false;
            this.txtTienThue_GTGT.Text = "0";
            this.txtTienThue_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThue_GTGT.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(365, 130);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Thuế suất";
            // 
            // txtTSVatGiam
            // 
            this.txtTSVatGiam.DecimalDigits = 3;
            this.txtTSVatGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSVatGiam.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTSVatGiam.Location = new System.Drawing.Point(459, 146);
            this.txtTSVatGiam.MaxLength = 10;
            this.txtTSVatGiam.Name = "txtTSVatGiam";
            this.txtTSVatGiam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTSVatGiam.Size = new System.Drawing.Size(52, 21);
            this.txtTSVatGiam.TabIndex = 17;
            this.txtTSVatGiam.Text = "0";
            this.txtTSVatGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSVatGiam.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTSVatGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSVatGiam.Leave += new System.EventHandler(this.txtTS_GTGT_Leave);
            // 
            // txtTS_GTGT
            // 
            this.txtTS_GTGT.DecimalDigits = 3;
            this.txtTS_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_GTGT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_GTGT.Location = new System.Drawing.Point(368, 146);
            this.txtTS_GTGT.MaxLength = 10;
            this.txtTS_GTGT.Name = "txtTS_GTGT";
            this.txtTS_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_GTGT.Size = new System.Drawing.Size(52, 21);
            this.txtTS_GTGT.TabIndex = 16;
            this.txtTS_GTGT.Text = "0";
            this.txtTS_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTS_GTGT.VisualStyleManager = this.vsmMain;
            this.txtTS_GTGT.Leave += new System.EventHandler(this.txtTS_GTGT_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(163, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Trị giá tính thuế GTGT";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(456, 130);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(45, 13);
            this.label25.TabIndex = 20;
            this.label25.Text = "TS Giảm";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(426, 111);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(18, 13);
            this.label61.TabIndex = 13;
            this.label61.Text = "%";
            // 
            // txtTGTT_GTGT
            // 
            this.txtTGTT_GTGT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGTT_GTGT.DecimalDigits = 0;
            this.txtTGTT_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_GTGT.Location = new System.Drawing.Point(166, 146);
            this.txtTGTT_GTGT.Name = "txtTGTT_GTGT";
            this.txtTGTT_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_GTGT.Size = new System.Drawing.Size(140, 21);
            this.txtTGTT_GTGT.TabIndex = 15;
            this.txtTGTT_GTGT.TabStop = false;
            this.txtTGTT_GTGT.Text = "0";
            this.txtTGTT_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGTT_GTGT.VisualStyleManager = this.vsmMain;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(537, 90);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(143, 13);
            this.label43.TabIndex = 20;
            this.label43.Text = "Tiền thuế bảo vệ môi trường";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(686, 111);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(36, 13);
            this.label63.TabIndex = 12;
            this.label63.Text = "(VNĐ)";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(7, 130);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(124, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "Biểu thuế giá trị gia tăng";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(686, 70);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(36, 13);
            this.label55.TabIndex = 12;
            this.label55.Text = "(VNĐ)";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(312, 151);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(36, 13);
            this.label60.TabIndex = 12;
            this.label60.Text = "(VNĐ)";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(365, 90);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(55, 13);
            this.label42.TabIndex = 15;
            this.label42.Text = "Thuế suất";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(163, 90);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(111, 13);
            this.label40.TabIndex = 16;
            this.label40.Text = "Trị giá tính thuế BVMT";
            // 
            // txtTSBVMT
            // 
            this.txtTSBVMT.DecimalDigits = 3;
            this.txtTSBVMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSBVMT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTSBVMT.Location = new System.Drawing.Point(368, 106);
            this.txtTSBVMT.MaxLength = 10;
            this.txtTSBVMT.Name = "txtTSBVMT";
            this.txtTSBVMT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTSBVMT.Size = new System.Drawing.Size(52, 21);
            this.txtTSBVMT.TabIndex = 12;
            this.txtTSBVMT.Text = "0";
            this.txtTSBVMT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSBVMT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTSBVMT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSBVMT.VisualStyleManager = this.vsmMain;
            this.txtTSBVMT.Leave += new System.EventHandler(this.txtTSBVMT_Leave);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(429, 69);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(18, 13);
            this.label54.TabIndex = 13;
            this.label54.Text = "%";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(537, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Tiền thuế TTĐB";
            // 
            // txtTriGiaTinhThueBVMT
            // 
            this.txtTriGiaTinhThueBVMT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTriGiaTinhThueBVMT.DecimalDigits = 0;
            this.txtTriGiaTinhThueBVMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueBVMT.Location = new System.Drawing.Point(166, 106);
            this.txtTriGiaTinhThueBVMT.Name = "txtTriGiaTinhThueBVMT";
            this.txtTriGiaTinhThueBVMT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueBVMT.Size = new System.Drawing.Size(140, 21);
            this.txtTriGiaTinhThueBVMT.TabIndex = 11;
            this.txtTriGiaTinhThueBVMT.TabStop = false;
            this.txtTriGiaTinhThueBVMT.Text = "0";
            this.txtTriGiaTinhThueBVMT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueBVMT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThueBVMT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGiaTinhThueBVMT.VisualStyleManager = this.vsmMain;
            // 
            // cbBieuThueGTGT
            // 
            this.cbBieuThueGTGT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbBieuThueGTGT.DisplayMember = "Ten";
            this.cbBieuThueGTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBieuThueGTGT.Location = new System.Drawing.Point(10, 146);
            this.cbBieuThueGTGT.Name = "cbBieuThueGTGT";
            this.cbBieuThueGTGT.Size = new System.Drawing.Size(140, 21);
            this.cbBieuThueGTGT.TabIndex = 14;
            this.cbBieuThueGTGT.ValueMember = "ID";
            this.cbBieuThueGTGT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(312, 111);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(36, 13);
            this.label64.TabIndex = 12;
            this.label64.Text = "(VNĐ)";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(7, 90);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(143, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "Biểu thuế bảo vệ môi trường";
            // 
            // txtTienThue_TTDB
            // 
            this.txtTienThue_TTDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_TTDB.DecimalDigits = 0;
            this.txtTienThue_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_TTDB.Location = new System.Drawing.Point(540, 65);
            this.txtTienThue_TTDB.Name = "txtTienThue_TTDB";
            this.txtTienThue_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_TTDB.Size = new System.Drawing.Size(140, 21);
            this.txtTienThue_TTDB.TabIndex = 9;
            this.txtTienThue_TTDB.TabStop = false;
            this.txtTienThue_TTDB.Text = "0";
            this.txtTienThue_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThue_TTDB.VisualStyleManager = this.vsmMain;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(686, 30);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(36, 13);
            this.label51.TabIndex = 12;
            this.label51.Text = "(VNĐ)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(365, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Thuế suất";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(426, 31);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(18, 13);
            this.label52.TabIndex = 13;
            this.label52.Text = "%";
            // 
            // cbBieuThueBVMT
            // 
            this.cbBieuThueBVMT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbBieuThueBVMT.DisplayMember = "Ten";
            this.cbBieuThueBVMT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBieuThueBVMT.Location = new System.Drawing.Point(10, 106);
            this.cbBieuThueBVMT.Name = "cbBieuThueBVMT";
            this.cbBieuThueBVMT.Size = new System.Drawing.Size(140, 21);
            this.cbBieuThueBVMT.TabIndex = 10;
            this.cbBieuThueBVMT.ValueMember = "ID";
            this.cbBieuThueBVMT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(456, 50);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 13);
            this.label24.TabIndex = 12;
            this.label24.Text = "TS Giảm";
            // 
            // txtTSTTDBGiam
            // 
            this.txtTSTTDBGiam.DecimalDigits = 3;
            this.txtTSTTDBGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSTTDBGiam.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTSTTDBGiam.Location = new System.Drawing.Point(459, 66);
            this.txtTSTTDBGiam.MaxLength = 10;
            this.txtTSTTDBGiam.Name = "txtTSTTDBGiam";
            this.txtTSTTDBGiam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTSTTDBGiam.Size = new System.Drawing.Size(52, 21);
            this.txtTSTTDBGiam.TabIndex = 8;
            this.txtTSTTDBGiam.Text = "0";
            this.txtTSTTDBGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSTTDBGiam.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTSTTDBGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSTTDBGiam.Leave += new System.EventHandler(this.txtTS_TTDB_Leave);
            // 
            // txtTS_TTDB
            // 
            this.txtTS_TTDB.DecimalDigits = 3;
            this.txtTS_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_TTDB.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_TTDB.Location = new System.Drawing.Point(368, 66);
            this.txtTS_TTDB.MaxLength = 10;
            this.txtTS_TTDB.Name = "txtTS_TTDB";
            this.txtTS_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_TTDB.Size = new System.Drawing.Size(52, 21);
            this.txtTS_TTDB.TabIndex = 7;
            this.txtTS_TTDB.Text = "0";
            this.txtTS_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTS_TTDB.VisualStyleManager = this.vsmMain;
            this.txtTS_TTDB.Leave += new System.EventHandler(this.txtTS_TTDB_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(163, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Trị giá tính thuế TTĐB";
            // 
            // txtTGTT_TTDB
            // 
            this.txtTGTT_TTDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGTT_TTDB.DecimalDigits = 0;
            this.txtTGTT_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_TTDB.Location = new System.Drawing.Point(166, 66);
            this.txtTGTT_TTDB.Name = "txtTGTT_TTDB";
            this.txtTGTT_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_TTDB.Size = new System.Drawing.Size(140, 21);
            this.txtTGTT_TTDB.TabIndex = 6;
            this.txtTGTT_TTDB.TabStop = false;
            this.txtTGTT_TTDB.Text = "0";
            this.txtTGTT_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGTT_TTDB.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(537, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Tiền thuế NK";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(7, 50);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(133, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "Biểu thuế tiêu thụ đặc biệt";
            // 
            // txtTienThue_NK
            // 
            this.txtTienThue_NK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_NK.DecimalDigits = 0;
            this.txtTienThue_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_NK.Location = new System.Drawing.Point(540, 25);
            this.txtTienThue_NK.Name = "txtTienThue_NK";
            this.txtTienThue_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_NK.Size = new System.Drawing.Size(140, 21);
            this.txtTienThue_NK.TabIndex = 4;
            this.txtTienThue_NK.TabStop = false;
            this.txtTienThue_NK.Text = "0";
            this.txtTienThue_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTienThue_NK.VisualStyleManager = this.vsmMain;
            this.txtTienThue_NK.Leave += new System.EventHandler(this.txtTienThue_NK_Leave);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(312, 71);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(36, 13);
            this.label56.TabIndex = 12;
            this.label56.Text = "(VNĐ)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(163, 10);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Trị giá tính thuế XNK";
            // 
            // txtTSXNKGiam
            // 
            this.txtTSXNKGiam.DecimalDigits = 3;
            this.txtTSXNKGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSXNKGiam.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTSXNKGiam.Location = new System.Drawing.Point(459, 26);
            this.txtTSXNKGiam.MaxLength = 10;
            this.txtTSXNKGiam.Name = "txtTSXNKGiam";
            this.txtTSXNKGiam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTSXNKGiam.Size = new System.Drawing.Size(52, 21);
            this.txtTSXNKGiam.TabIndex = 3;
            this.txtTSXNKGiam.Text = "0";
            this.txtTSXNKGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSXNKGiam.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTSXNKGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSXNKGiam.Leave += new System.EventHandler(this.txtTS_NK_Leave);
            // 
            // txtTS_NK
            // 
            this.txtTS_NK.DecimalDigits = 3;
            this.txtTS_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_NK.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_NK.Location = new System.Drawing.Point(368, 26);
            this.txtTS_NK.MaxLength = 10;
            this.txtTS_NK.Name = "txtTS_NK";
            this.txtTS_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_NK.Size = new System.Drawing.Size(52, 21);
            this.txtTS_NK.TabIndex = 2;
            this.txtTS_NK.Text = "0";
            this.txtTS_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTS_NK.VisualStyleManager = this.vsmMain;
            this.txtTS_NK.Leave += new System.EventHandler(this.txtTS_NK_Leave);
            // 
            // txtTGTT_NK
            // 
            this.txtTGTT_NK.BackColor = System.Drawing.Color.White;
            this.txtTGTT_NK.DecimalDigits = 0;
            this.txtTGTT_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_NK.Location = new System.Drawing.Point(166, 26);
            this.txtTGTT_NK.Name = "txtTGTT_NK";
            this.txtTGTT_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_NK.Size = new System.Drawing.Size(140, 21);
            this.txtTGTT_NK.TabIndex = 1;
            this.txtTGTT_NK.TabStop = false;
            this.txtTGTT_NK.Text = "0";
            this.txtTGTT_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTGTT_NK.VisualStyleManager = this.vsmMain;
            this.txtTGTT_NK.TextChanged += new System.EventHandler(this.txtTGTT_NK_TextChanged);
            this.txtTGTT_NK.Leave += new System.EventHandler(this.txtTGTT_NK_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(365, 10);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Thuế suất";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(459, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "TS Giảm";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(7, 10);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(130, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "Biểu thuế xuất nhập khẩu";
            // 
            // cbBieuThueXNK
            // 
            this.cbBieuThueXNK.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbBieuThueXNK.DisplayMember = "Ten";
            this.cbBieuThueXNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBieuThueXNK.Location = new System.Drawing.Point(10, 26);
            this.cbBieuThueXNK.Name = "cbBieuThueXNK";
            this.cbBieuThueXNK.Size = new System.Drawing.Size(140, 21);
            this.cbBieuThueXNK.TabIndex = 0;
            this.cbBieuThueXNK.ValueMember = "ID";
            this.cbBieuThueXNK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbBieuThueTTDB
            // 
            this.cbBieuThueTTDB.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbBieuThueTTDB.DisplayMember = "Ten";
            this.cbBieuThueTTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBieuThueTTDB.Location = new System.Drawing.Point(10, 66);
            this.cbBieuThueTTDB.Name = "cbBieuThueTTDB";
            this.cbBieuThueTTDB.Size = new System.Drawing.Size(140, 21);
            this.cbBieuThueTTDB.TabIndex = 5;
            this.cbBieuThueTTDB.ValueMember = "ID";
            this.cbBieuThueTTDB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(312, 31);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(36, 13);
            this.label50.TabIndex = 12;
            this.label50.Text = "(VNĐ)";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.AutoScroll = true;
            this.uiTabPage2.Controls.Add(this.txtTien_CLG);
            this.uiTabPage2.Controls.Add(this.txtTL_CLG);
            this.uiTabPage2.Controls.Add(this.txtTienThueCPG);
            this.uiTabPage2.Controls.Add(this.label44);
            this.uiTabPage2.Controls.Add(this.label41);
            this.uiTabPage2.Controls.Add(this.label45);
            this.uiTabPage2.Controls.Add(this.txtTSCPG);
            this.uiTabPage2.Controls.Add(this.txtTriGiaThuKhac);
            this.uiTabPage2.Controls.Add(this.label15);
            this.uiTabPage2.Controls.Add(this.label65);
            this.uiTabPage2.Controls.Add(this.txtTriGiaTTCPG);
            this.uiTabPage2.Controls.Add(this.cbBieuThueCBPG);
            this.uiTabPage2.Controls.Add(this.label29);
            this.uiTabPage2.Controls.Add(this.txtTyLeThuKhac);
            this.uiTabPage2.Controls.Add(this.label67);
            this.uiTabPage2.Controls.Add(this.label12);
            this.uiTabPage2.Controls.Add(this.label20);
            this.uiTabPage2.Controls.Add(this.label9);
            this.uiTabPage2.Controls.Add(this.numericEditBox1);
            this.uiTabPage2.Controls.Add(this.txtCLG);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1229, 172);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Các loại thuế khác";
            // 
            // txtTien_CLG
            // 
            this.txtTien_CLG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTien_CLG.DecimalDigits = 0;
            this.txtTien_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTien_CLG.Location = new System.Drawing.Point(487, 86);
            this.txtTien_CLG.Name = "txtTien_CLG";
            this.txtTien_CLG.ReadOnly = true;
            this.txtTien_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTien_CLG.Size = new System.Drawing.Size(136, 21);
            this.txtTien_CLG.TabIndex = 37;
            this.txtTien_CLG.TabStop = false;
            this.txtTien_CLG.Text = "0";
            this.txtTien_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTien_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTien_CLG.Visible = false;
            this.txtTien_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTien_CLG.VisualStyleManager = this.vsmMain;
            // 
            // txtTL_CLG
            // 
            this.txtTL_CLG.DecimalDigits = 3;
            this.txtTL_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTL_CLG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTL_CLG.Location = new System.Drawing.Point(395, 86);
            this.txtTL_CLG.MaxLength = 10;
            this.txtTL_CLG.Name = "txtTL_CLG";
            this.txtTL_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTL_CLG.Size = new System.Drawing.Size(53, 21);
            this.txtTL_CLG.TabIndex = 3;
            this.txtTL_CLG.Text = "0";
            this.txtTL_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTL_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTL_CLG.Visible = false;
            this.txtTL_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTL_CLG.VisualStyleManager = this.vsmMain;
            // 
            // txtTienThueCPG
            // 
            this.txtTienThueCPG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThueCPG.DecimalDigits = 0;
            this.txtTienThueCPG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThueCPG.Location = new System.Drawing.Point(485, 37);
            this.txtTienThueCPG.Name = "txtTienThueCPG";
            this.txtTienThueCPG.ReadOnly = true;
            this.txtTienThueCPG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThueCPG.Size = new System.Drawing.Size(138, 21);
            this.txtTienThueCPG.TabIndex = 12;
            this.txtTienThueCPG.TabStop = false;
            this.txtTienThueCPG.Text = "0";
            this.txtTienThueCPG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThueCPG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThueCPG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(393, 20);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(55, 13);
            this.label44.TabIndex = 10;
            this.label44.Text = "Thuế suất";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(482, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(143, 13);
            this.label41.TabIndex = 14;
            this.label41.Text = "Tiền thuế chống bán phá giá";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(174, 20);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(173, 13);
            this.label45.TabIndex = 8;
            this.label45.Text = "Trị giá tính thuế chống bán phá giá";
            // 
            // txtTSCPG
            // 
            this.txtTSCPG.DecimalDigits = 3;
            this.txtTSCPG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSCPG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTSCPG.Location = new System.Drawing.Point(396, 36);
            this.txtTSCPG.MaxLength = 10;
            this.txtTSCPG.Name = "txtTSCPG";
            this.txtTSCPG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTSCPG.Size = new System.Drawing.Size(52, 21);
            this.txtTSCPG.TabIndex = 2;
            this.txtTSCPG.Text = "0";
            this.txtTSCPG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSCPG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTSCPG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTSCPG.Leave += new System.EventHandler(this.txtTSCPG_Leave);
            // 
            // txtTriGiaThuKhac
            // 
            this.txtTriGiaThuKhac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTriGiaThuKhac.DecimalDigits = 3;
            this.txtTriGiaThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaThuKhac.Location = new System.Drawing.Point(484, 136);
            this.txtTriGiaThuKhac.Name = "txtTriGiaThuKhac";
            this.txtTriGiaThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaThuKhac.Size = new System.Drawing.Size(139, 21);
            this.txtTriGiaThuKhac.TabIndex = 39;
            this.txtTriGiaThuKhac.TabStop = false;
            this.txtTriGiaThuKhac.Text = "0.000";
            this.txtTriGiaThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTriGiaThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGiaThuKhac.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(484, 70);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 13);
            this.label15.TabIndex = 43;
            this.label15.Text = "Tiền chênh lệch giá";
            this.label15.Visible = false;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(7, 20);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(143, 13);
            this.label65.TabIndex = 15;
            this.label65.Text = "Biểu thuế chống bán phá giá";
            // 
            // txtTriGiaTTCPG
            // 
            this.txtTriGiaTTCPG.BackColor = System.Drawing.Color.White;
            this.txtTriGiaTTCPG.DecimalDigits = 0;
            this.txtTriGiaTTCPG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTTCPG.Location = new System.Drawing.Point(177, 36);
            this.txtTriGiaTTCPG.Name = "txtTriGiaTTCPG";
            this.txtTriGiaTTCPG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTTCPG.Size = new System.Drawing.Size(170, 21);
            this.txtTriGiaTTCPG.TabIndex = 1;
            this.txtTriGiaTTCPG.TabStop = false;
            this.txtTriGiaTTCPG.Text = "0";
            this.txtTriGiaTTCPG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTTCPG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTTCPG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cbBieuThueCBPG
            // 
            this.cbBieuThueCBPG.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbBieuThueCBPG.DisplayMember = "Ten";
            this.cbBieuThueCBPG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBieuThueCBPG.Location = new System.Drawing.Point(10, 36);
            this.cbBieuThueCBPG.Name = "cbBieuThueCBPG";
            this.cbBieuThueCBPG.Size = new System.Drawing.Size(140, 21);
            this.cbBieuThueCBPG.TabIndex = 0;
            this.cbBieuThueCBPG.ValueMember = "ID";
            this.cbBieuThueCBPG.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(484, 120);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 13);
            this.label29.TabIndex = 46;
            this.label29.Text = "Tiền thu khác";
            // 
            // txtTyLeThuKhac
            // 
            this.txtTyLeThuKhac.DecimalDigits = 4;
            this.txtTyLeThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyLeThuKhac.Location = new System.Drawing.Point(396, 136);
            this.txtTyLeThuKhac.Name = "txtTyLeThuKhac";
            this.txtTyLeThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyLeThuKhac.Size = new System.Drawing.Size(52, 21);
            this.txtTyLeThuKhac.TabIndex = 4;
            this.txtTyLeThuKhac.TabStop = false;
            this.txtTyLeThuKhac.Text = "0.0000";
            this.txtTyLeThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyLeThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTyLeThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyLeThuKhac.VisualStyleManager = this.vsmMain;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(393, 120);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(52, 13);
            this.label67.TabIndex = 42;
            this.label67.Text = "Tỷ lệ (%)";
            this.label67.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(395, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 42;
            this.label12.Text = "Tỷ lệ (%)";
            this.label12.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(177, 120);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(101, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "Trị giá tiền thu khác";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(177, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 41;
            this.label9.Text = "Chênh lệch giá";
            // 
            // numericEditBox1
            // 
            this.numericEditBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.numericEditBox1.DecimalDigits = 0;
            this.numericEditBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox1.Location = new System.Drawing.Point(180, 136);
            this.numericEditBox1.Name = "numericEditBox1";
            this.numericEditBox1.ReadOnly = true;
            this.numericEditBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox1.Size = new System.Drawing.Size(169, 21);
            this.numericEditBox1.TabIndex = 35;
            this.numericEditBox1.TabStop = false;
            this.numericEditBox1.Text = "0";
            this.numericEditBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtCLG
            // 
            this.txtCLG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCLG.DecimalDigits = 0;
            this.txtCLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCLG.Location = new System.Drawing.Point(180, 86);
            this.txtCLG.Name = "txtCLG";
            this.txtCLG.ReadOnly = true;
            this.txtCLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCLG.Size = new System.Drawing.Size(169, 21);
            this.txtCLG.TabIndex = 35;
            this.txtCLG.TabStop = false;
            this.txtCLG.Text = "0";
            this.txtCLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtCLG.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.uiGroupBox5);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1229, 172);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Miễn/Giảm thuế";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.label48);
            this.uiGroupBox5.Controls.Add(this.txtGiamThue_SoVanBan);
            this.uiGroupBox5.Controls.Add(this.txtGiamThue_ThueSuatGoc);
            this.uiGroupBox5.Controls.Add(this.label49);
            this.uiGroupBox5.Controls.Add(this.txtGiamThue_TyLeGiam);
            this.uiGroupBox5.Controls.Add(this.label47);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1229, 172);
            this.uiGroupBox5.TabIndex = 22;
            this.uiGroupBox5.Text = "Thông tin miễn thuế";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(13, 30);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(166, 13);
            this.label48.TabIndex = 17;
            this.label48.Text = "Số văn bản được miễn/Giảm";
            // 
            // txtGiamThue_SoVanBan
            // 
            this.txtGiamThue_SoVanBan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiamThue_SoVanBan.Location = new System.Drawing.Point(16, 46);
            this.txtGiamThue_SoVanBan.MaxLength = 255;
            this.txtGiamThue_SoVanBan.Name = "txtGiamThue_SoVanBan";
            this.txtGiamThue_SoVanBan.Size = new System.Drawing.Size(172, 21);
            this.txtGiamThue_SoVanBan.TabIndex = 0;
            this.txtGiamThue_SoVanBan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGiamThue_SoVanBan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGiamThue_ThueSuatGoc
            // 
            this.txtGiamThue_ThueSuatGoc.DecimalDigits = 4;
            this.txtGiamThue_ThueSuatGoc.Location = new System.Drawing.Point(212, 46);
            this.txtGiamThue_ThueSuatGoc.MaxLength = 20;
            this.txtGiamThue_ThueSuatGoc.Name = "txtGiamThue_ThueSuatGoc";
            this.txtGiamThue_ThueSuatGoc.Size = new System.Drawing.Size(113, 21);
            this.txtGiamThue_ThueSuatGoc.TabIndex = 1;
            this.txtGiamThue_ThueSuatGoc.Tag = "SoContainer20";
            this.txtGiamThue_ThueSuatGoc.Text = "0.0000";
            this.txtGiamThue_ThueSuatGoc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtGiamThue_ThueSuatGoc.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtGiamThue_ThueSuatGoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(345, 30);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(91, 13);
            this.label49.TabIndex = 17;
            this.label49.Text = "Tỷ lệ giảm (%)";
            // 
            // txtGiamThue_TyLeGiam
            // 
            this.txtGiamThue_TyLeGiam.DecimalDigits = 4;
            this.txtGiamThue_TyLeGiam.Location = new System.Drawing.Point(348, 46);
            this.txtGiamThue_TyLeGiam.MaxLength = 20;
            this.txtGiamThue_TyLeGiam.Name = "txtGiamThue_TyLeGiam";
            this.txtGiamThue_TyLeGiam.Size = new System.Drawing.Size(113, 21);
            this.txtGiamThue_TyLeGiam.TabIndex = 2;
            this.txtGiamThue_TyLeGiam.Tag = "SoContainer20";
            this.txtGiamThue_TyLeGiam.Text = "0.0000";
            this.txtGiamThue_TyLeGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtGiamThue_TyLeGiam.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtGiamThue_TyLeGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(209, 30);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(112, 13);
            this.label47.TabIndex = 17;
            this.label47.Text = "Thuế suất gốc (%)";
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.AutoScroll = true;
            this.uiTabPage4.Controls.Add(this.uiGroupBox8);
            this.uiTabPage4.Controls.Add(this.uiGroupBox7);
            this.uiTabPage4.Controls.Add(this.uiGroupBox6);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1245, 172);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Thông tin bổ sung theo TT115";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.dglContainer);
            this.uiGroupBox8.Controls.Add(this.label73);
            this.uiGroupBox8.Location = new System.Drawing.Point(610, 6);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(358, 166);
            this.uiGroupBox8.TabIndex = 0;
            this.uiGroupBox8.Text = "Container của hàng (*)";
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // dglContainer
            // 
            dglContainer_DesignTimeLayout.LayoutString = resources.GetString("dglContainer_DesignTimeLayout.LayoutString");
            this.dglContainer.DesignTimeLayout = dglContainer_DesignTimeLayout;
            this.dglContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dglContainer.Location = new System.Drawing.Point(8, 17);
            this.dglContainer.Name = "dglContainer";
            this.dglContainer.Size = new System.Drawing.Size(344, 121);
            this.dglContainer.TabIndex = 11;
            this.dglContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dglContainer.VisualStyleManager = this.vsmMain;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.BackColor = System.Drawing.Color.Transparent;
            this.label73.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label73.Location = new System.Drawing.Point(5, 152);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(214, 13);
            this.label73.TabIndex = 10;
            this.label73.Text = "(*) Vui lòng vào vận đơn để thêm container";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.lbxChungTuKem);
            this.uiGroupBox7.Controls.Add(this.label72);
            this.uiGroupBox7.Location = new System.Drawing.Point(296, 3);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(308, 166);
            this.uiGroupBox7.TabIndex = 0;
            this.uiGroupBox7.Text = "Chứng từ đính kèm theo hàng hóa (*)";
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // lbxChungTuKem
            // 
            this.lbxChungTuKem.FormattingEnabled = true;
            this.lbxChungTuKem.Items.AddRange(new object[] {
            "Không có chứng từ kèm theo cho hàng này.",
            "Vui lòng vào mục chứng từ kèm để thêm chứng từ"});
            this.lbxChungTuKem.Location = new System.Drawing.Point(8, 20);
            this.lbxChungTuKem.Name = "lbxChungTuKem";
            this.lbxChungTuKem.Size = new System.Drawing.Size(292, 121);
            this.lbxChungTuKem.TabIndex = 12;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.BackColor = System.Drawing.Color.Transparent;
            this.label72.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label72.Location = new System.Drawing.Point(5, 152);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(223, 13);
            this.label72.TabIndex = 10;
            this.label72.Text = "(*) Các chứng từ đính kèm có chứa hàng này";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.label71);
            this.uiGroupBox6.Controls.Add(this.label70);
            this.uiGroupBox6.Controls.Add(this.label69);
            this.uiGroupBox6.Controls.Add(this.label68);
            this.uiGroupBox6.Controls.Add(this.uiButton1);
            this.uiGroupBox6.Controls.Add(this.numericEditBox3);
            this.uiGroupBox6.Controls.Add(this.numericEditBox2);
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 3);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(283, 166);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.Text = "Phương pháp xác định trị giá";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.BackColor = System.Drawing.Color.Transparent;
            this.label71.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label71.Location = new System.Drawing.Point(5, 152);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(257, 13);
            this.label71.TabIndex = 10;
            this.label71.Text = "(*) lưu thông tin tờ khai trước khi thêm tờ khai trị giá";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.BackColor = System.Drawing.Color.Transparent;
            this.label70.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(5, 85);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(129, 13);
            this.label70.TabIndex = 10;
            this.label70.Text = "Tổng các khoảng phải trừ";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.BackColor = System.Drawing.Color.Transparent;
            this.label69.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(5, 58);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(137, 13);
            this.label69.TabIndex = 10;
            this.label69.Text = "Tổng các khoảng phải cộng";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(5, 33);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(162, 13);
            this.label68.TabIndex = 10;
            this.label68.Text = "Phương pháp xác định trị giá HQ";
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(159, 107);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(118, 33);
            this.uiButton1.TabIndex = 1;
            this.uiButton1.Text = "BS hàng vào tờ khai trị giá (*)";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton1.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // numericEditBox3
            // 
            this.numericEditBox3.DecimalDigits = 20;
            this.numericEditBox3.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox3.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox3.Location = new System.Drawing.Point(158, 80);
            this.numericEditBox3.MaxLength = 15;
            this.numericEditBox3.Name = "numericEditBox3";
            this.numericEditBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox3.Size = new System.Drawing.Size(119, 21);
            this.numericEditBox3.TabIndex = 0;
            this.numericEditBox3.Text = "0";
            this.numericEditBox3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox3.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.numericEditBox3.Leave += new System.EventHandler(this.txtLuong_Leave);
            // 
            // numericEditBox2
            // 
            this.numericEditBox2.DecimalDigits = 20;
            this.numericEditBox2.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox2.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox2.Location = new System.Drawing.Point(158, 53);
            this.numericEditBox2.MaxLength = 15;
            this.numericEditBox2.Name = "numericEditBox2";
            this.numericEditBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox2.Size = new System.Drawing.Size(119, 21);
            this.numericEditBox2.TabIndex = 0;
            this.numericEditBox2.Text = "0";
            this.numericEditBox2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.numericEditBox2.Leave += new System.EventHandler(this.txtLuong_Leave);
            // 
            // chkMienThue
            // 
            this.chkMienThue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMienThue.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.chkMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMienThue.ForeColor = System.Drawing.Color.Red;
            this.chkMienThue.Location = new System.Drawing.Point(1185, -4);
            this.chkMienThue.Name = "chkMienThue";
            this.chkMienThue.Size = new System.Drawing.Size(80, 23);
            this.chkMienThue.TabIndex = 2;
            this.chkMienThue.Text = "Miễn thuế";
            this.chkMienThue.VisualStyleManager = this.vsmMain;
            this.chkMienThue.CheckedChanged += new System.EventHandler(this.chkMienThue_CheckedChanged);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(1120, 506);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 2;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(1203, 506);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.ImageList = this.ImageList1;
            this.btnGhi.Location = new System.Drawing.Point(1036, 506);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // lblTyGiaTT
            // 
            this.lblTyGiaTT.AutoSize = true;
            this.lblTyGiaTT.BackColor = System.Drawing.Color.Transparent;
            this.lblTyGiaTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTyGiaTT.Location = new System.Drawing.Point(7, 511);
            this.lblTyGiaTT.Name = "lblTyGiaTT";
            this.lblTyGiaTT.Size = new System.Drawing.Size(98, 13);
            this.lblTyGiaTT.TabIndex = 8;
            this.lblTyGiaTT.Text = "Tỷ giá tính thuế:";
            this.lblTyGiaTT.Click += new System.EventHandler(this.lblTyGiaTT_Click);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTenHang;
            this.rfvTen.ErrorMessage = "\"Tên hàng\" không được để trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            this.rfvTen.Tag = "rfvTen";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHS;
            this.rfvMaHS.ErrorMessage = "\"Mã HS\" không được để trống.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // revMaHS
            // 
            this.revMaHS.ControlToValidate = this.txtMaHS;
            this.revMaHS.ErrorMessage = "\"Mã HS\" không hợp lệ.";
            this.revMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("revMaHS.Icon")));
            this.revMaHS.Tag = "revMaHS";
            this.revMaHS.ValidationExpression = "\\d{1,12}";
            // 
            // rvLuong
            // 
            this.rvLuong.ControlToValidate = this.txtLuong;
            this.rvLuong.ErrorMessage = "Giá trị không hợp lệ";
            this.rvLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rvLuong.Icon")));
            this.rvLuong.MaximumValue = "1000000000000";
            this.rvLuong.MinimumValue = "0";
            this.rvLuong.Tag = "rvLuong";
            this.rvLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvDGNT
            // 
            this.rvDGNT.ControlToValidate = this.txtDGNT;
            this.rvDGNT.ErrorMessage = "Giá trị không hợp lệ";
            this.rvDGNT.Icon = ((System.Drawing.Icon)(resources.GetObject("rvDGNT.Icon")));
            this.rvDGNT.MaximumValue = "1000000000000";
            this.rvDGNT.MinimumValue = "0";
            this.rvDGNT.Tag = "rvLuong";
            this.rvDGNT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cvSoLuong
            // 
            this.cvSoLuong.ControlToValidate = this.txtLuong;
            this.cvSoLuong.ErrorMessage = "Giá trị không hợp lệ";
            this.cvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuong.Icon")));
            this.cvSoLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoLuong.Tag = "cvSoLuong";
            this.cvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuong.ValueToCompare = "0";
            // 
            // rfvBieuThueXNK
            // 
            this.rfvBieuThueXNK.ControlToValidate = this.cbBieuThueXNK;
            this.rfvBieuThueXNK.ErrorMessage = "\"Biểu thuế xuất nhập khẩu\" không được để trống.";
            this.rfvBieuThueXNK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvBieuThueXNK.Icon")));
            this.rfvBieuThueXNK.Tag = "rfvBieuThueXNK";
            // 
            // rfvBieuThueGTGT
            // 
            this.rfvBieuThueGTGT.ControlToValidate = this.cbBieuThueGTGT;
            this.rfvBieuThueGTGT.ErrorMessage = "\"Biểu thuế giá trị gia tăng\" không được để trống.";
            this.rfvBieuThueGTGT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvBieuThueGTGT.Icon")));
            this.rfvBieuThueGTGT.Tag = "rfvBieuThueGTGT";
            // 
            // linkSaoChep_CheDoUuDai
            // 
            this.linkSaoChep_CheDoUuDai.BackColor = System.Drawing.Color.Transparent;
            this.linkSaoChep_CheDoUuDai.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkSaoChep_CheDoUuDai.Location = new System.Drawing.Point(855, 80);
            this.linkSaoChep_CheDoUuDai.Name = "linkSaoChep_CheDoUuDai";
            this.linkSaoChep_CheDoUuDai.Size = new System.Drawing.Size(146, 16);
            this.linkSaoChep_CheDoUuDai.TabIndex = 23;
            this.linkSaoChep_CheDoUuDai.TabStop = true;
            this.linkSaoChep_CheDoUuDai.Text = "Sao chép chế độ ưu đãi";
            this.linkSaoChep_CheDoUuDai.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkSaoChep_CheDoUuDai_LinkClicked);
            // 
            // HangMauDichForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1284, 757);
            this.Controls.Add(this.uiGroupBox1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HangMauDichForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin hàng hóa";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.HangMauDichForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThue)).EndInit();
            this.grbThue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTabMienGiamThue)).EndInit();
            this.uiTabMienGiamThue.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            this.uiTabPage2.PerformLayout();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dglContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvDGNT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvBieuThueXNK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvBieuThueGTGT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion        

        private UIComboBox cbDonViTinh;
        private GridEX dgList;
        private ImageList ImageList1;
        private UIButton btnClose;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private Company.Controls.CustomValidation.RegularExpressionValidator revMaHS;
        private NumericEditBox txtLuong;
        private Label label23;
        private NumericEditBox txtTriGiaKB;
        private Label label22;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem SaoChepCha;
        private UIButton btnXoa;
        private Company.Controls.CustomValidation.RangeValidator rvLuong;
        private Company.Controls.CustomValidation.RangeValidator rvDGNT;
        private Company.Controls.CustomValidation.CompareValidator cvSoLuong;
        private UICheckBox chkHangFOC;
        private UICheckBox chkThueTuyetDoi;
        private Label label21;
        private Label label24;
        private Label label25;
        protected UICheckBox chkMienThue;
        private Label label26;
        private NumericEditBox txtDonGiaTuyetDoi;
        private Label label28;
        private Label lblTongTienThue;
        private Label label32;
        private EditBox txtTenHangSX;
        private Label label31;
        private EditBox txtMaHangSX;
        private Label label33;
        private EditBox txtMaHSMoRong;
        private Label label36;
        private Label label37;
        private EditBox txtNhanHieu;
        private EditBox txtQuyCach;
        private Label label34;
        private Label label35;
        private EditBox txtThanhPhan;
        private EditBox txtModel;
        private UIButton btnAddNew;
        private NumericEditBox txtTGNT;
        private LinkLabel linkLabel2;
        private LinkLabel linkLabel1;
        private Label label39;
        private Label label38;
        private Janus.Windows.UI.Tab.UITab uiTabMienGiamThue;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Label label45;
        private Label label44;
        private NumericEditBox txtTienThueCPG;
        private Label label41;
        private NumericEditBox txtTriGiaTTCPG;
        private NumericEditBox txtTSCPG;
        private UICheckBox chkIsOld;
        private UIComboBox cbBieuThueXNK;
        private Label label52;
        private Label label46;
        private Label label51;
        private Label label50;
        private Label label54;
        private Label label10;
        private Label label53;
        private Label label56;
        private Label label55;
        private UIComboBox cbBieuThueTTDB;
        private Label label57;
        private UIComboBox cbBieuThueGTGT;
        private Label label58;
        private Label label11;
        private Label label60;
        private Label label59;
        private NumericEditBox txtTriGiaThuKhac;
        private Label label15;
        private Label label29;
        private NumericEditBox txtTyLeThuKhac;
        private Label label12;
        private Label label9;
        private NumericEditBox txtTL_CLG;
        private NumericEditBox txtTien_CLG;
        private NumericEditBox txtCLG;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private UIGroupBox uiGroupBox5;
        private Label label48;
        private EditBox txtGiamThue_SoVanBan;
        private NumericEditBox txtGiamThue_ThueSuatGoc;
        private Label label49;
        private NumericEditBox txtGiamThue_TyLeGiam;
        private Label label47;
        private Label label61;
        private Label label40;
        private Label label42;
        private NumericEditBox txtTTBVMT;
        private NumericEditBox txtTriGiaTinhThueBVMT;
        private NumericEditBox txtTSBVMT;
        private Label label43;
        private Label label62;
        private UIComboBox cbBieuThueBVMT;
        private Label label64;
        private Label label63;
        private Label label65;
        private UIComboBox cbBieuThueCBPG;
        private Label label20;
        private NumericEditBox numericEditBox1;
        private Label label66;
        private NumericEditBox txtTongSoTienThue;
        private NumericEditBox txtTSVatGiam;
        private NumericEditBox txtTSTTDBGiam;
        private NumericEditBox txtTSXNKGiam;
        private Label label67;
        private UIGroupBox uiGroupBox3;
        private LinkLabel lblCopyBieuThue;
        private UICheckBox chkHangDongBo;
        private UIComboBox cmbCO;
        private Label label30;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvBieuThueXNK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvBieuThueGTGT;
        private UIButton btnChungTuTruoc;
        private DonViTinhQuyDoiControl DVT_QuyDoiCtr;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private UIGroupBox uiGroupBox6;
        private Label label70;
        private Label label69;
        private Label label68;
        private Label label71;
        private UIButton uiButton1;
        private NumericEditBox numericEditBox3;
        private NumericEditBox numericEditBox2;
        private UIGroupBox uiGroupBox7;
        private Label label72;
        private ListBox lbxChungTuKem;
        private UIGroupBox uiGroupBox8;
        private Label label73;
        private GridEX dglContainer;
        private LinkLabel linkSaoChep_CheDoUuDai;
    }
}
