﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class HangMauForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public HangMau hmDetail = new HangMau();
        public bool isBrower = false;
        public HangMauForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void HangMauForm_Load(object sender, EventArgs e)
        {
            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;


            if (!isBrower)
            {
                txtMa.Focus();
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                cbDonViTinh.DataSource = this._DonViTinh;
                cbDonViTinh.DisplayMember = "Ten";
                cbDonViTinh.ValueMember = "ID";
                if (!string.IsNullOrEmpty(hmDetail.Ma))
                {
                    txtMa.Text = hmDetail.Ma.Trim();
                    txtTen.Text = hmDetail.Ten.Trim();
                    txtGhiChu.Text = hmDetail.GhiChu;
                    txtMaHS.Text = hmDetail.MaHS.Trim();
                    cbDonViTinh.SelectedValue = hmDetail.DVT_ID;
                    txtSoLuong.Text = (hmDetail.SoLuongDangKy.ToString());
                }
                else
                {
                    cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
                }


                if (this.OpenType == OpenFormType.View)
                {
                    btnAdd.Enabled = false;
                    btnXoa.Enabled = false;
                    dgHangMau.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;;
                }
                
            }
            else
            {
                uiGroupBox2.Visible = false;
                HD.LoadCollection();
                dgHangMau.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                this.Width = dgHangMau.Width;
                this.Height = dgHangMau.Height;
            }
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            dgHangMau.DataSource = HD.HangMauCollection;

        }
        private void reset()
        {
            txtMa.Text = "";
            txtTen.Text = "";
            txtMaHS.Text = "";
            txtSoLuong.Text = "";
            txtGhiChu.Text = string.Empty;
            error.SetError(txtMaHS, null);
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);


            try
            {
                dgHangMau.Refetch();
            }
            catch
            {
                dgHangMau.Refresh();
            }
            hmDetail = new HangMau();


        }
        private void setError()
        {
            error.Clear();
            error.SetError(txtTen, null);
            error.SetError(txtMa, null);
            error.SetError(txtMaHS, null);

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {

            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {
                checkExitsAndSTTHang();
            }
        }
        private void checkExitsAndSTTHang()
        {
            HD.HangMauCollection.Remove(hmDetail);

            foreach (HangMau hm in HD.HangMauCollection)
            {
                if (hm.Ma.Trim() == txtMa.Text.Trim())
                {
                    showMsg("Hàng mẫu này đã được thêm vào trước đây", false);
                    if (hmDetail.Ma != "")
                        HD.HangMauCollection.Add(hmDetail);
                    return;
                }
            }
            hmDetail.Ma = txtMa.Text.Trim();
            hmDetail.Ten = txtTen.Text.Trim();
            hmDetail.MaHS = txtMaHS.Text.Trim();
            hmDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            hmDetail.HopDong_ID = HD.ID;
            hmDetail.GhiChu = txtGhiChu.Text;
            hmDetail.SoLuongDangKy = Convert.ToDecimal(txtSoLuong.Value);
            HD.HangMauCollection.Add(hmDetail);
            reset();
            this.setError();
        }
        
        private void dgHangMau_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {

            hmDetail = (HangMau)e.Row.DataRow;
            hmDetail.HopDong_ID = HD.ID;
            if (!isBrower)
            {
                txtMa.Text = hmDetail.Ma;
                txtTen.Text = hmDetail.Ten;
                txtMaHS.Text = hmDetail.MaHS;
                txtSoLuong.Value = hmDetail.SoLuongDangKy;
                cbDonViTinh.SelectedValue = hmDetail.DVT_ID;
                txtGhiChu.Text = hmDetail.GhiChu;
            }
            else
            {
                this.Close();
            }
        }



        private void dgHangMau_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
        }

        private void ctrNguyenTe_Load(object sender, EventArgs e)
        {

        }

        private void dgHangMau_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {

            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgHangMau.SelectedItems;
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.HangMau tbDelete = (Company.GC.BLL.KDT.GC.HangMau)row.GetRow().DataRow;
                        tbDelete.Delete();

                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HangMau> tbColl = new List<HangMau>();
            if (HD.HangMauCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgHangMau.SelectedItems;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                foreach (GridEXSelectedItem row in items)
                {
                    HangMau tbDelete = (HangMau)row.GetRow().DataRow;
                    if (HD.ID > 0)
                    {
                        try { tbDelete.Delete(); }
                        catch { }
                    }

                    tbColl.Add(tbDelete);
                }
                foreach (HangMau tbt in tbColl)
                {
                    HD.HangMauCollection.Remove(tbt);
                }
                dgHangMau.DataSource = HD.HangMauCollection;
                this.setError();
                try { dgHangMau.Refetch(); }
                catch { dgHangMau.Refresh(); }

            }

        }
    }
}