using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using BarcodeLib;
using ZXing.QrCode;
using ZXing;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class ReportViewDinhDanhHangHoaToKhai : DevExpress.XtraReports.UI.XtraReport
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public ReportViewDinhDanhHangHoaToKhai()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = true,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 55,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        private void GeneralQrCodeNew()
        {
            try
            {
                QrCodeEncodingOptions options = new QrCodeEncodingOptions();
                options = new QrCodeEncodingOptions
                {
                    DisableECI = true,
                    CharacterSet = "UTF-8",
                    Width = 200,
                    Height = 200,
                    Margin = 0,
                };
                var writer = new BarcodeWriter();
                writer.Format = BarcodeFormat.QR_CODE;
                writer.Options = options;

                var qr = new ZXing.BarcodeWriter();
                qr.Options = options;
                qr.Format = ZXing.BarcodeFormat.QR_CODE;
                var result = new Bitmap(qr.Write(TKMD.VanDonCollection[0].SoDinhDanh.ToString()));
                picQrCode.Image = result;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public void BindReport()
        {
            try
            {
                GeneralQrCodeNew();
                lblSoDinhDanh.Text = TKMD.VanDonCollection[0].SoDinhDanh.ToString();
                lblMaDoanhNghiep.Text = TKMD.MaDonVi;
                lblTenDoanhNghiep.Text = TKMD.TenDonVi;
                lblSoTKXK.Text = TKMD.SoToKhai.ToString();
                picBarCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
                picBarCode.Image = GenerateBarCode(TKMD.SoToKhai.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
