using System;
using System.Data;
using System.Data.SqlClient;
using Company.BLL.KDT.SXXK;

namespace Company.BLL.KDT.SXXK
{
    public partial class MsgSend : EntityBase
    {
        #region Private members.

        protected long _master_id;
        protected int _func;
        protected string _LoaiHS = String.Empty;
        protected string _msg = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long master_id
        {
            set { this._master_id = value; }
            get { return this._master_id; }
        }
        public int func
        {
            set { this._func = value; }
            get { return this._func; }
        }
        public string LoaiHS
        {
            set { this._LoaiHS = value; }
            get { return this._LoaiHS; }
        }
        public string msg
        {
            set { this._msg = value; }
            get { return this._msg; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_SXXK_MsgSend_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, this._master_id);
            this.db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, this._LoaiHS);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("master_id"))) this._master_id = reader.GetInt64(reader.GetOrdinal("master_id"));
                if (!reader.IsDBNull(reader.GetOrdinal("func"))) this._func = reader.GetInt32(reader.GetOrdinal("func"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHS"))) this._LoaiHS = reader.GetString(reader.GetOrdinal("LoaiHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("msg"))) this._msg = reader.GetString(reader.GetOrdinal("msg"));
                return true;
            }
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_MsgSend_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_MsgSend_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_MsgSend_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_MsgSend_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public MsgSendCollection SelectCollectionAll()
        {
            MsgSendCollection collection = new MsgSendCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                MsgSend entity = new MsgSend();

                if (!reader.IsDBNull(reader.GetOrdinal("master_id"))) entity.master_id = reader.GetInt64(reader.GetOrdinal("master_id"));
                if (!reader.IsDBNull(reader.GetOrdinal("func"))) entity.func = reader.GetInt32(reader.GetOrdinal("func"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHS"))) entity.LoaiHS = reader.GetString(reader.GetOrdinal("LoaiHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("msg"))) entity.msg = reader.GetString(reader.GetOrdinal("msg"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public MsgSendCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            MsgSendCollection collection = new MsgSendCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                MsgSend entity = new MsgSend();

                if (!reader.IsDBNull(reader.GetOrdinal("master_id"))) entity.master_id = reader.GetInt64(reader.GetOrdinal("master_id"));
                if (!reader.IsDBNull(reader.GetOrdinal("func"))) entity.func = reader.GetInt32(reader.GetOrdinal("func"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHS"))) entity.LoaiHS = reader.GetString(reader.GetOrdinal("LoaiHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("msg"))) entity.msg = reader.GetString(reader.GetOrdinal("msg"));
                collection.Add(entity);
            }
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_MsgSend_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, this._master_id);
            this.db.AddInParameter(dbCommand, "@func", SqlDbType.Int, this._func);
            this.db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, this._LoaiHS);
            this.db.AddInParameter(dbCommand, "@msg", SqlDbType.NText, this._msg);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(MsgSendCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (MsgSend item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, MsgSendCollection collection)
        {
            foreach (MsgSend item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_MsgSend_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, this._master_id);
            this.db.AddInParameter(dbCommand, "@func", SqlDbType.Int, this._func);
            this.db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, this._LoaiHS);
            this.db.AddInParameter(dbCommand, "@msg", SqlDbType.NText, this._msg);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(MsgSendCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (MsgSend item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_MsgSend_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, this._master_id);
            this.db.AddInParameter(dbCommand, "@func", SqlDbType.Int, this._func);
            this.db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, this._LoaiHS);
            this.db.AddInParameter(dbCommand, "@msg", SqlDbType.NText, this._msg);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(MsgSendCollection collection, SqlTransaction transaction)
        {
            foreach (MsgSend item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_MsgSend_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, this._master_id);
            this.db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, this._LoaiHS);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(MsgSendCollection collection, SqlTransaction transaction)
        {
            foreach (MsgSend item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(MsgSendCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (MsgSend item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion
    }
}