﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;
namespace Company.Interface
{
    public partial class VNACC_TK_DinhKiemDTForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        private static List<VNACC_Category_Common> datas = null;
        DataTable dt = new DataTable();
        public String Caption;
        public bool IsChange;
        public VNACC_TK_DinhKiemDTForm()
        {
            InitializeComponent();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void VNACC_TK_DinhKiemDTForm_Load(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                LoadData();
                Caption = this.Text;
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
                {
                    btnSave.Enabled = false;
                    btnXoa.Enabled = false;
                    btnChonChungTu.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
            
        }
        private void LoadData()
        {
            try
            {
                //Load dropdown
                datas = VNACC_Category_Common.SelectCollectionDynamic("ReferenceDB = 'E020'", "Code asc");
                grList.DropDowns["drdPhanLoai"].DataSource = datas;
                //Load grid
                dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.DinhKemCollection);
                grList.DataSource = dt;
                grList.RecordAdded += new EventHandler(txt_TextChanged);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (TKMD.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy Lưu tờ khai trước khi Chọn chứng từ đính kèm .",false);
                    return;
                }
                int sott = 1;
                TKMD.DinhKemCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow item in dt.Rows)
                {
                    KDT_VNACC_TK_DinhKemDienTu dk = new KDT_VNACC_TK_DinhKemDienTu();
                    if (item["ID"].ToString().Length != 0)
                    {
                        dk.ID = Convert.ToInt32(item["ID"].ToString());
                        dk.TKMD_ID = Convert.ToInt32(item["TKMD_ID"].ToString());
                    }
                    dk.SoTT = sott;
                    dk.PhanLoai = item["PhanLoai"].ToString();
                    dk.SoDinhKemKhaiBaoDT = Convert.ToDecimal(item["SoDinhKemKhaiBaoDT"].ToString());
                    sott++;
                    TKMD.DinhKemCollection.Add(dk);
                    List<KDT_VNACC_ChungTuDinhKem> chungTuDinhKemCollection = KDT_VNACC_ChungTuDinhKem.SelectCollectionDynamic("PhanLoaiThuTucKhaiBao ='A02' AND SoTiepNhan=" + dk.SoDinhKemKhaiBaoDT, "");
                    foreach (KDT_VNACC_ChungTuDinhKem chungtu in chungTuDinhKemCollection)
                    {
                        List<KDT_VNACC_ContainerDinhKem> containerCollection = KDT_VNACC_ContainerDinhKem.SelectCollectionBy_ChungTuDinhKem_ID(chungtu.ID);
                        KDT_ContainerDangKy containerDK = new KDT_ContainerDangKy();
                        containerDK.MaHQ = TKMD.CoQuanHaiQuan;
                        containerDK.MaDoanhNghiep = TKMD.MaDonVi;
                        containerDK.TKMD_ID = TKMD.ID;
                        containerDK.NgayTiepNhan = DateTime.Now;
                        containerDK.SoTiepNhan = 0;
                        containerDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        foreach (KDT_VNACC_ContainerDinhKem container in containerCollection)
                        {
                            KDT_ContainerBS cont = new KDT_ContainerBS();
                            cont.SoVanDon = container.SoVanDon;
                            cont.SoContainer = container.SoContainer;
                            cont.SoSeal = container.SoSeal;
                            cont.GhiChu = container.GhiChu;
                            containerDK.ListCont.Add(cont);
                        }
                        containerDK.InsertUpdateFull();
                    }
                }
                ShowMessage("Lưu thành công", false);
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                this.Cursor = Cursors.Default;
            }

        }

        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa đính kèm điện tử này không?", true) == "Yes")
                    {
                        KDT_VNACC_TK_DinhKemDienTu.DeleteDynamic("ID=" + ID);
                        grList.CurrentRow.Delete();
                        this.SetChange(true);
                    }
                }
                else
                    grList.CurrentRow.Delete();
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                this.Cursor = Cursors.Default;
            }
        }
        private long IDSoDinhKemKhaiBaoDT = 0;
        private void btnChonChungTu_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                VNACC_ChungTuKemManageForm f = new VNACC_ChungTuKemManageForm();
                f.IsShowChonToKhai = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    //IDSoDinhKemKhaiBaoDT = f.TKMDDuocChon.ID;
                    //KDT_VNACC_TK_DinhKemDienTu dk = new KDT_VNACC_TK_DinhKemDienTu();
                    //GridEXRow newRow = grList.CurrentRow;
                    //grList.CurrentRow.Cells["SoDinhKemKhaiBaoDT"].Value = f.TKMDDuocChon.SoTiepNhan;
                    //grList.CurrentRow.Cells["PhanLoai"].Value = "Other";
                    foreach (KDT_VNACC_ChungTuDinhKem item in f.ListCTK)
                    {
                        DataRow dr = dt.NewRow();
                        dr["SoDinhKemKhaiBaoDT"] = item.SoTiepNhan;
                        dr["PhanLoai"] = "ETC";
                        dt.Rows.Add(dr);

                    }

                    grList.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_TK_DinhKiemDTForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Tờ khai Đính kèm điện tử có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.btnSave_Click(null, null);
                }
            }
        }

        //private void SoDinhKemKhaiBaoDT_ValueChanged(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(SoDinhKemKhaiBaoDT.Text) && SoDinhKemKhaiBaoDT.Text != "0")
        //    {
        //        List<KDT_VNACC_TK_DinhKemDienTu> listtkmd = KDT_VNACC_TK_DinhKemDienTu.SelectCollectionDynamic("SoToKhai = " + SoDinhKemKhaiBaoDT.Text, null);
        //        if (listtkmd.Count == 1)
        //        {
        //            IDSoDinhKemKhaiBaoDT = listtkmd[0].ID;
        //        }

        //    }
        //}
    }
}
