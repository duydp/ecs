using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Janus.Windows.EditControls;
using Company.GC.BLL;
using Company.Interface.Report.SXXK;
using System.IO;
using Company.Interface.Report.GC; 
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.KDT;
using Company.Interface.KDT.GC;
namespace Company.Interface.GC
{
    public partial class PhieuXuatNhapKhoManager : BaseForm
    {
        public List<KDT_VNACC_ToKhaiMauDich> ListTKMD = new List<KDT_VNACC_ToKhaiMauDich>();
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        //public ToKhaiMauDich tkmd = new ToKhaiMauDich();
        public KDT_GC_PhieuXuatKho PXK = new KDT_GC_PhieuXuatKho();
        public KDT_GC_PhieuNhapKho PNK = new KDT_GC_PhieuNhapKho();
        public List<KDT_VNACC_HangMauDich> ListHang = new List<KDT_VNACC_HangMauDich>();
        public List<KDT_GC_PhieuNhapKho> ListTKN = new List<KDT_GC_PhieuNhapKho>();
        public List<KDT_GC_PhieuXuatKho> ListTKX = new List<KDT_GC_PhieuXuatKho>();
        DataSet ds = new DataSet();
        public bool IsShowChonToKhai = false;
        public long TKMD_ID { get; set; }
        public KDT_VNACC_ToKhaiMauDich TKMDDuocChon = null;
        public HopDong HopDong = null;
        public PhieuXuatNhapKhoManager()
        {
            InitializeComponent();
            btnXNT.Click += new EventHandler(btnXNT_Click);
        }

        void btnXNT_Click(object sender, EventArgs e)
        {
            PhieuXuatKho_ThongKeTon f = new PhieuXuatKho_ThongKeTon();
            if(HopDong.ID!=0)
                f.hd = HopDong;
            f.ShowDialog(this);
        }

        private void PhieuXuatNhapKhoManager_Load(object sender, EventArgs e)
        {
            this.AcceptButton = this.btnTimKiem;
            clcTuNgay.Value = DateTime.Now.AddDays(-30);
            clcDenNgay.Value = DateTime.Now;
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
            rdPhieuNhapKho.Checked = true;
            dgList.Tables[0].Columns["NgayXuatKho"].Visible = false;
            if (HopDong != null)
                txtSoHopDong.Text = HopDong.SoHopDong;
            btnTimKiem_Click(null, null);
        }
        private void LoadDataPhieuNhap(string where)
        {
            ListTKN = KDT_GC_PhieuNhapKho.SelectCollectionDynamic(where, "");

        }
        private void LoadDataPhieuXuat(string where)
        {
            ListTKX = KDT_GC_PhieuXuatKho.SelectCollectionDynamic(where, "");
        }
        private KDT_GC_PhieuNhapKho getTKN(long id)
        {
            string where = "id = " + id;
            ListTKN = KDT_GC_PhieuNhapKho.SelectCollectionDynamic(where, null);          
            foreach (KDT_GC_PhieuNhapKho PN in ListTKN)
            {
                if (PN.ID == id)
                {
                    string query = "TKMD_ID =" + PN.ID;
                    List<KDT_GC_PhieuNhapKho_Hang> listHang = new List<KDT_GC_PhieuNhapKho_Hang>();
                    listHang = KDT_GC_PhieuNhapKho_Hang.SelectCollectionDynamic(query, "ID");
                    foreach (KDT_GC_PhieuNhapKho_Hang hang in listHang)
                    {
                        PN.HangNhapCollection.Add(hang);
                    }

                    return PN;
                }
            }

            return null;
        }
        private KDT_GC_PhieuXuatKho getTKX(long id)
        {
            string where = "id = " + id;
            ListTKX = KDT_GC_PhieuXuatKho.SelectCollectionDynamic(where, null);
            foreach (KDT_GC_PhieuXuatKho PX in ListTKX)
            {
                if (PX.ID == id)
                {
                    string query = "TKMD_ID =" + PX.ID;
                    List<KDT_GC_PhieuXuatKho_Hang> listHang = new List<KDT_GC_PhieuXuatKho_Hang>();
                    listHang = KDT_GC_PhieuXuatKho_Hang.SelectCollectionDynamic(query, "ID");
                    foreach (KDT_GC_PhieuXuatKho_Hang hang in listHang)
                    {
                        PX.HangXuatCollection.Add(hang);
                    }

                    return PX;
                }
            }

            return null;
        }
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soHD = txtSoHopDong.Text.Trim();
                //string tungay = Convert.ToDateTime(clcTuNgay.Value).ToString("dd/MM/yyyy hh:mm:ss");
                //string denngay = Convert.ToDateTime(clcDenNgay.Value).ToString("dd/MM/yyyy hh:mm:ss");
                string where = "(1=1)";
                
                if (rdPhieuNhapKho.Checked)
                {
                    if (ckbTimKiem.Checked)
                    {
                        DateTime fromDate = clcTuNgay.Value;
                        DateTime toDate = clcDenNgay.Value;
                        if (toDate.Year <= 1900) toDate = DateTime.Now;
                        toDate = toDate.AddDays(1);
                        where = where + " AND (NgayNhapKho Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";                       
                    }
                }
                else
                {
                    if (ckbTimKiem.Checked)
                    {
                        DateTime fromDate = clcTuNgay.Value;
                        DateTime toDate = clcDenNgay.Value;
                        if (toDate.Year <= 1900) toDate = DateTime.Now;
                        toDate = toDate.AddDays(1);
                        where = where + " AND (NgayXuatKho Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                        //if (tungay != null && denngay != null)
                            //where = where + " and NgayXuatKho between '" + Convert.ToDateTime(clcTuNgay.Value).ToString("MM/dd/yyyy 00:00:00") + "'" + " and " + "'" + Convert.ToDateTime(clcDenNgay.Value).ToString("MM/dd/yyyy 23:59:59") + "'";
                    }
                }
                if (soHD != "")
                    where = where + " and SoHopDong like '%" + soHD + "%' ";
                if (rdPhieuNhapKho.Checked)
                {
                    ListTKN.Clear();
                    LoadDataPhieuNhap(where);
                    dgList.DataSource = ListTKN;
                    dgList.Refresh();
                }
                else if (rdPhieuXuatKho.Checked)
                {
                    ListTKX.Clear();
                    LoadDataPhieuXuat(where);
                    dgList.DataSource = ListTKX;
                    dgList.Refresh();
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
            {
                grbtimkiem.Enabled = true;
            }
            else
            {
                grbtimkiem.Enabled = false;
            }
        } 
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {


            if (e.Row.RowType == RowType.Record)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                    if (id != 0 && rdPhieuXuatKho.Checked)
                    {
                        PhieuXuatKhoForm f = new PhieuXuatKhoForm();
                        KDT_GC_PhieuXuatKho PXK = getTKX(id);
                        f.PXK = PXK;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }
                    if (id != 0 && rdPhieuNhapKho.Checked)
                    {
                        PhieuNhapKhoForm f = new PhieuNhapKhoForm();
                        KDT_GC_PhieuNhapKho PNK = getTKN(id);
                        f.PNK = PNK;
                        f.ShowDialog();
                        btnTimKiem_Click(null, null);
                    }




                    //int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                    //KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(id);
                    //TKMD.LoadFull();
                    //if (IsShowChonToKhai)
                    //{
                    //    TKMDDuocChon = TKMD;
                    //    this.DialogResult = DialogResult.OK;
                    //    this.Close();
                    //    return;
                    //}
                    //PXK = getTKX(id);
                    //PNK = getTKN(id);
                    //string loaiHinh = "";
                    //DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                    //DataRow dr = ds.Tables[0].Rows[0];
                    //loaiHinh = dr["ReferenceDB"].ToString();

                    //if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
                    //{
                    //    PhieuXuatKhoForm f = new PhieuXuatKhoForm();
                    //    if (PXK == null || PXK.TKMD_ID != id)
                    //    {
                    //        f.PXK = new KDT_GC_PhieuXuatKho();
                    //        f.PXK = PXK;
                    //    }
                    //    else
                    //    {
                    //        f.PXK = PXK;
                    //        ////PXK = KDT_GC_PhieuXuatKho.LoadPXK(id);
                    //        //KDT_GC_PhieuXuatKho PhieuXuatKho = getTKX(id);
                    //        //f.PXK = PhieuXuatKho;                           
                    //    }
                    //    f.TKMD = TKMD;
                    //    f.ListHang = ListHang;
                    //    f.ShowDialog();
                    //    //btnTimKiem_Click(null, null);
                    //}
                    //if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
                    //{
                    //    PhieuNhapKhoForm f = new PhieuNhapKhoForm();
                    //    if (PNK == null || PNK.TKMD_ID == 0)
                    //    {
                    //        f.PNK = new KDT_GC_PhieuNhapKho();
                    //        f.PNK = PNK;
                    //    }
                    //    else
                    //    {
                    //        //KDT_GC_PhieuNhapKho PhieuNhapKho = getTKN(id);
                    //        f.PNK = PNK;
                    //    }
                    //    f.TKMD = TKMD;
                    //    f.ShowDialog();
                    //    //btnTimKiem_Click(null, null);
                    //}
                    //btnTimKiem_Click(null, null);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                }
                finally { Cursor = Cursors.Default; }
            }
        }

        private void cbbTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
            f.IsShowChonToKhai = true;
            if (this.HopDong.ID != 0) 
            {
                f.hd = this.HopDong;
                if (rdPhieuXuatKho.Checked)
                    f.isPhieuXuat = true;
            }
                
            f.ShowDialog(this);
            if (f.DialogResult == DialogResult.OK)
            {
                KDT_VNACC_ToKhaiMauDich TKMD = f.TKMDDuocChon;
                string loaiHinh = "";
                DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                DataRow dr = ds.Tables[0].Rows[0];
                loaiHinh = dr["ReferenceDB"].ToString();
                if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
                {
                    PhieuNhapKhoForm pn = new PhieuNhapKhoForm();
                    pn.TKMD = TKMD;
                    pn.HopDong = this.HopDong;
                    pn.ShowDialog();
                }
                if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
                {
                    PhieuXuatKhoForm px = new PhieuXuatKhoForm();
                    px.TKMD = TKMD;
                    px.HopDong = this.HopDong;
                    px.ShowDialog();
                }
            }
            btnTimKiem.PerformClick();
        }

        private void rdPhieuNhapKho_CheckedChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
            dgList.Tables[0].Columns["NgayXuatKho"].Visible = false;
        }

        private void rdPhieuXuatKho_CheckedChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
            dgList.Tables[0].Columns["NgayNhapKho"].Visible = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dgList.CurrentRow.RowType == RowType.Record)
            {
                int id = System.Convert.ToInt32(dgList.CurrentRow.Cells["ID"].Value.ToString());
                if (rdPhieuNhapKho.Checked == true)
                {

                    if (ShowMessage("Bạn có muốn xóa Phiếu nhập kho này không?", true) == "Yes")
                    {

                        KDT_GC_PhieuNhapKho PNK = new KDT_GC_PhieuNhapKho();
                        PNK.DeleteFull(id);
                        ShowMessage("Xóa phiếu nhập kho thành công.", false);
                    }
                }
                else if (ShowMessage("Bạn có muốn xóa Phiếu xuất kho này không?", true) == "Yes")
                {
                        KDT_GC_PhieuXuatKho PXK = new KDT_GC_PhieuXuatKho();
                        PXK.DeleteFull(id);
                        ShowMessage("Xóa phiếu xuất kho thành công.", false);
                }

            }
            btnTimKiem_Click(null, null);
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            
                if (rdPhieuNhapKho.Checked)
                {
                    PhieuNhapKhoForm pn = new PhieuNhapKhoForm();
                    pn.TKMD = this.TKMD;
                    pn.HopDong = this.HopDong;
                    pn.ShowDialog();
                }
                else
                {
                    PhieuXuatKhoForm px = new PhieuXuatKhoForm();
                    px.TKMD = this.TKMD;
                    px.HopDong = this.HopDong;
                    px.ShowDialog();
                }
        }

        private void btnXNT_Details_Click(object sender, EventArgs e)
        {
            PhieuXuatKho_ThongKeTon_ChiTiet f = new PhieuXuatKho_ThongKeTon_ChiTiet();
            if (HopDong.ID != 0)
                f.hd = HopDong;
            f.ShowDialog(this);
        }

        private void btnTaoMoi_V4_Click(object sender, EventArgs e)
        {
            //VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
            //f.IsShowChonToKhai = true;
            //if (this.HopDong.ID != 0)
            //{
            //    f.hd = this.HopDong;
            //    if (rdPhieuXuatKho.Checked)
            //        f.isPhieuXuat = true;
            //}
            ToKhaiMauDichManageForm f = new ToKhaiMauDichManageForm();
            f.isChonTK_V4 = true;
            f.HD = this.HopDong;
            f.ShowDialog(this);
            if (f.DialogResult == DialogResult.OK)
            {
                ToKhaiMauDich TKMD = f.TKMD_DcChon;
                string loaiHinh = "";
                //DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                //DataRow dr = ds.Tables[0].Rows[0];
                loaiHinh = TKMD.MaLoaiHinh;
                if (loaiHinh.Contains("N"))
                {
                    PhieuNhapKhoForm pn = new PhieuNhapKhoForm();
                    pn.TKMD_V4 = TKMD;
                    pn.isV4 = true;
                    pn.HopDong = this.HopDong;
                    pn.ShowDialog();
                }
                if (loaiHinh.Contains("X"))
                {
                    PhieuXuatKhoForm px = new PhieuXuatKhoForm();
                    px.TKMD_V4 = TKMD;
                    px.isV4 = true;
                    px.HopDong = this.HopDong;
                    px.ShowDialog();
                }
            }
            btnTimKiem.PerformClick();
        }

        private void btnTaoMoi_V4_GCCT_Click(object sender, EventArgs e)
        {
            ToKhaiGCCTManagerForm f = new ToKhaiGCCTManagerForm();
            f.isChonTK_V4 = true;
            f.HD = this.HopDong;
            f.ShowDialog(this);
            if (f.DialogResult == DialogResult.OK)
            {
                ToKhaiChuyenTiep TKCT = f.TKCT_Select;
                string loaiHinh = "";
                //DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                //DataRow dr = ds.Tables[0].Rows[0];
                loaiHinh = TKCT.MaLoaiHinh;
                if (loaiHinh.Contains("N"))
                {
                    PhieuNhapKhoForm pn = new PhieuNhapKhoForm();
                    pn.TKCT_V4 = TKCT;
                    pn.isV4_TKCT = true;
                    pn.HopDong = this.HopDong;
                    pn.ShowDialog();
                }
                if (loaiHinh.Contains("X"))
                {
                    PhieuXuatKhoForm px = new PhieuXuatKhoForm();
                    px.TKCT_V4 = TKCT;
                    px.isV4_TKCT = true;
                    px.HopDong = this.HopDong;
                    px.ShowDialog();
                }
            }
            btnTimKiem.PerformClick();
        }


       
    }
}