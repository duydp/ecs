﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class ConsignmentItem
    {
       /// <summary>
       /// Số thứ tự
       /// </summary>
        [XmlElement("sequence")]
       public string Sequence { get; set; }   
       /// <summary>
       /// 
       /// </summary>
        [XmlElement("ConsignmentItemPackaging")]
        public Packaging ConsignmentItemPackaging { get; set; }
       /// <summary>
       /// 
       /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
       /// <summary>
       /// 
       /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }
       /// <summary>
       /// 
       /// </summary>
        [XmlElement("EquipmentIdentification")]
        public EquipmentIdentification EquipmentIdentification { get; set; }

    }
}
