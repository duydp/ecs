﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{
    public partial class VNACC_TEA_ManagerForm : BaseForm
    {
        private List<KDT_VNACCS_TEA> listTEA = new List<KDT_VNACCS_TEA>();
        private DateTime dayMin = new DateTime(1900, 1, 1);
        private DataSet ds = new DataSet();
        private string where = "";
        public VNACC_TEA_ManagerForm()
        {
            InitializeComponent();

            this.Text = "Theo dõi hàng miễn thuế";
        }

        private void VNACC_TEA_ManagerForm_Load(object sender, EventArgs e)
        {
            where = "TrangThaiXuLy='-1'";
            LoadData(where);
        }
        private void LoadData(string where )
        {
            listTEA = KDT_VNACCS_TEA.SelectCollectionDynamic(where,"");
            grList.DataSource = listTEA;
        }
        private KDT_VNACCS_TEA getTEA(long id)
        {

            foreach (KDT_VNACCS_TEA TEA in listTEA)
            {
                if (TEA.ID == id)
                {
                    string query = "ID=" + id;
                    List<KDT_VNACCS_TEA_HangHoa> listHang = new List<KDT_VNACCS_TEA_HangHoa>();
                    listHang = KDT_VNACCS_TEA_HangHoa.SelectCollectionDynamic(query, "ID");
                    foreach (KDT_VNACCS_TEA_HangHoa hang in listHang)
                    {
                        TEA.HangCollection.Add(hang);
                    }
                    List<KDT_VNACCS_TEADieuChinh> listDieuChinh = new List<KDT_VNACCS_TEADieuChinh>();
                    listDieuChinh = KDT_VNACCS_TEADieuChinh.SelectCollectionDynamic(query, "ID");
                    foreach (KDT_VNACCS_TEADieuChinh dc in listDieuChinh)
                    {
                        TEA.DieuChinhCollection.Add(dc);
                    }
                    List<KDT_VNACCS_TEANguoiXNK> listNguoiXNK = new List<KDT_VNACCS_TEANguoiXNK>();
                    listNguoiXNK = KDT_VNACCS_TEANguoiXNK.SelectCollectionDynamic(query, "ID");
                    foreach (KDT_VNACCS_TEANguoiXNK nguoiXNK in listNguoiXNK)
                    {
                        TEA.NguoiXNKCollection.Add(nguoiXNK);
                    }
                    return TEA;
                }
            }

            return null;
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                if (id != 0)
                {
                    VNACC_TEAForm f = new VNACC_TEAForm();
                    KDT_VNACCS_TEA TEA =getTEA(id);
                    f.TEA = TEA;
                    f.ShowDialog();
                    LoadData(where);
                }
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soTN = txtSoTiepNhan.Text.Trim();
                string soHD = txtSoHoaDon.Text.Trim();
                string loaiHinh = (cbbPhanLoaiXuatNhap.SelectedValue.ToString() == "0") ? "" : cbbPhanLoaiXuatNhap.SelectedValue.ToString();
                string trangThai = (cbbTrangThaiXuLy.SelectedValue == null) ? "0" : cbbTrangThaiXuLy.SelectedValue.ToString();
                string where = String.Empty;
                if (soTN != "")
                    where = "SoTiepNhan='" + soTN + "' and ";
                if (clcNgayTiepNhan.Text != "" && clcNgayTiepNhan.Value > dayMin)
                    where = where + "NgayTiepNhan='" + String.Format("{0:yyyy/MM/dd}", clcNgayTiepNhan.Value) + "' and ";
                if (soHD != "")
                    where = where + "SoHoaDon='" + soHD + "' and ";
                if (loaiHinh != "")
                    where = where + "PhanLoaiXuatNhap='" + loaiHinh + "' and ";
                if (trangThai != "")
                    where = where + "TrangThaiXuLy='" + trangThai + "'";
                listTEA.Clear();
                listTEA = KDT_VNACCS_TEA.SelectCollectionDynamic(where, "ID");

                grList.DataSource = listTEA;
                grList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        
    }
}
