SET QUOTED_IDENTIFIER OFF;
SET ANSI_NULLS ON;
GO
ALTER VIEW [dbo].[t_View_LuongNPLTrongTKXuat]
AS
SELECT CASE
           WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN
               tkmd.LoaiVanDon
           ELSE
               CONVERT(VARCHAR(12), tkmd.SoToKhai)
       END AS SoToKhaiVNACCS,
       tkmd.TrangThaiXuLy,
       tkmd.MaHaiQuan,
       tkmd.SoToKhai,
       tkmd.MaLoaiHinh,
       tkmd.NgayDangKy,
       hmd.MaPhu AS MaSP,
       hmd.TenHang,
       hmd.DVT_ID,
       hmd.SoLuong,
       dm.MaNguyenPhuLieu,
       npl.Ten,
       hmd.SoLuong * (dm.DinhMucSuDung / 100 * dm.TyLeHaoHut + dm.DinhMucSuDung) AS LuongNPL,
       hmd.TriGiaTT,
       hmd.DonGiaTT,
       tkmd.MaDoanhNghiep,
       tkmd.IDHopDong,
       tkmd.ID,
       dm.DinhMucSuDung,
       dm.TyLeHaoHut,
       YEAR(tkmd.NgayDangKy) AS NamDK
FROM dbo.t_KDT_ToKhaiMauDich tkmd
    INNER JOIN dbo.t_KDT_HangMauDich hmd
        ON tkmd.ID = hmd.TKMD_ID
    INNER JOIN dbo.t_GC_DinhMuc dm
        ON dm.MaSanPham = hmd.MaPhu
           AND dm.HopDong_ID = tkmd.IDHopDong
    INNER JOIN dbo.t_GC_NguyenPhuLieu npl
        ON dm.HopDong_ID = npl.HopDong_ID
           AND dm.MaNguyenPhuLieu = npl.Ma
WHERE (
          tkmd.LoaiHangHoa = 'S'
          AND tkmd.TrangThaiXuLy NOT IN ( 10, 11 )
          AND tkmd.MaLoaiHinh LIKE 'XV%'
      );
	  
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.5') = 0
	BEGIN
		INSERT INTO dbo.t_HaiQuan_Version VALUES('41.5',GETDATE(), N'CẬP NHẬT VIEW')
	END	

