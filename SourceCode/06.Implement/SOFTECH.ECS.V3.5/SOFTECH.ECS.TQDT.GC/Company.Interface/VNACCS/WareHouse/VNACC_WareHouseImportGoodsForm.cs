﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
#if GC_V4
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
#endif
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.DanhMucChuan;
#if SXXK_V4
using Company.Interface.SXXK;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using Company.Interface.DanhMucChuan;
#endif

namespace Company.Interface.VNACCS.WareHouse
{
    public partial class VNACC_WareHouseImportGoodsForm : BaseFormHaveGuidPanel
    {
        public string LoaiHangHoa = "N";
#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#elif GC_V4
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
        public HopDong HD;
#endif
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public T_KDT_VNACCS_WarehouseImport wareHouseImport = null;
        public T_KDT_VNACCS_WarehouseImport_Detail wareHouseDetail = null;
        public T_KDT_VNACCS_WarehouseImport_GoodsDetail wareHouseGoodDetail = null;
        public List<T_KDT_VNACCS_WarehouseImport_Detail> wareHouseDetailCollection = new List<T_KDT_VNACCS_WarehouseImport_Detail>();
        public List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> wareHouseGoodDetaiCollection = new List<T_KDT_VNACCS_WarehouseImport_GoodsDetail>();
        public VNACC_WareHouseImportGoodsForm()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinh.DataSource = this._DonViTinh;
            txtDonViTinh.DisplayMember = "Ten";
            txtDonViTinh.ValueMember = "ID";
            txtDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
        }

        private void VNACC_WareHouseImportGoodsForm_Load(object sender, EventArgs e)
        {
            try
            {
                cbbLoaiHang.SelectedIndex = 0;
                if (wareHouseDetail!=null)
                {
                    if (wareHouseDetail.ID > 0)
                    {
                        wareHouseDetail.Collection = T_KDT_VNACCS_WarehouseImport_GoodsDetail.SelectCollectionBy_WarehouseImport_Details_ID(wareHouseDetail.ID);
                        SetWareHouseDetail();
                        BindData();
                    }
                    else
                    {
                        BindData();
                        SetWareHouseDetail();
                    }
                }
                else
                {
                    wareHouseDetail = new T_KDT_VNACCS_WarehouseImport_Detail();
                }
                if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
                {
                    btnAdd.Enabled = false;
                    btnDelete.Enabled = false;
                    btnReadExcel.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtSoPhieuNhap, errorProvider, "SỐ PHIẾU NHẬP ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaNguoiGiao, errorProvider, "MÃ NGƯỜI GIAO HÀNG ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNguoiGiao, errorProvider, "TÊN NGƯỜI GIAO HÀNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHangHoa, errorProvider, "MÃ HÀNG HÓA", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHangHoa, errorProvider, "TÊN HÀNG HÓA", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHang, errorProvider, "LOẠI HÀNG HÓA", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtMaDinhDanhSX, errorProvider, "Mã định danh của lệnh sản xuất", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbNguonNhap, errorProvider, "NGUỒN NHẬP", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongTN, errorProvider, "SỐ LƯỢNG THỰC NHẬP", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoLuongDK, errorProvider, "SỐ LƯỢNG NHẬP DỰ KIẾN", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> ItemColl = new List<T_KDT_VNACCS_WarehouseImport_GoodsDetail>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XÓA HÀNG NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KDT_VNACCS_WarehouseImport_GoodsDetail)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KDT_VNACCS_WarehouseImport_GoodsDetail item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        wareHouseDetail.Collection.Remove(item);
                    }

                    dgList.DataSource = wareHouseDetail.Collection;
                    try { dgList.Refetch(); }
                    catch { dgList.Refresh(); }

                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
                //
            }
        }
        public bool isAddNew = false;
        private void btnAdd_Click(object sender, EventArgs e)
        {
                try
                {
                    if (!ValidateForm(false))
                        return;

                    if (wareHouseGoodDetail == null)
                    {
                        wareHouseGoodDetail = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();
                        isAddNew = true;
                    }
                    GetWareHouseDetail();
                    GetWareHouseGoodItem();
                    if (isAddNew)
                    {
                        wareHouseDetail.Collection.Add(wareHouseGoodDetail);
                        int STT = 1;
                        foreach (T_KDT_VNACCS_WarehouseImport_GoodsDetail item in wareHouseDetail.Collection)
                        {
                            item.STT = STT;
                            STT++;
                        }
                        wareHouseImport.WarehouseImportDetailsCollection.Add(wareHouseDetail);
                    }
                    BindData();
                    //dgList.DataSource = wareHouseDetail.Collection;
                    //dgList.Refetch();
                    wareHouseGoodDetail = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();
                    wareHouseGoodDetail = null;
                    txtMaHangHoa.Text = String.Empty;
                    txtTenHangHoa.Text = String.Empty;
                    txtMaDinhDanhSX.Text = String.Empty;
                    txtSoLuongDK.Text = "0";
                    txtSoLuongTN.Text = "0";

                }
                catch (Exception ex)
                {
                    //ShowMessage("Lỗi khi lưu hàng: " + ex, false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    this.Cursor = Cursors.Default;
                }
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = wareHouseDetail.Collection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
                //
            }
        }
        private void GetWareHouseDetail()
        {
            try
            {
                wareHouseDetail.SoPhieuNhap = txtSoPhieuNhap.Text;
                wareHouseDetail.NgayPhieuNhap = dtpNgayNhap.Value;
                wareHouseDetail.MaNguoiGiaoHang = txtMaNguoiGiao.Text;
                wareHouseDetail.TenNguoiGiaoHang = txtTenNguoiGiao.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void SetWareHouseDetail()
        {
            try
            {
                txtSoPhieuNhap.Text = wareHouseDetail.SoPhieuNhap;
                dtpNgayNhap.Value = wareHouseDetail.NgayPhieuNhap.Year ==1 ? DateTime.Now : wareHouseDetail.NgayPhieuNhap;
                txtMaNguoiGiao.Text = wareHouseDetail.MaNguoiGiaoHang;
                txtTenNguoiGiao.Text = wareHouseDetail.TenNguoiGiaoHang;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void GetWareHouseGoodItem()
        {
            try
            {
                wareHouseGoodDetail.MaHangHoa = txtMaHangHoa.Text;
                wareHouseGoodDetail.TenHangHoa = txtTenHangHoa.Text;
                wareHouseGoodDetail.DVT = txtDonViTinh.SelectedValue.ToString();
                wareHouseGoodDetail.MaDinhDanhSX = txtMaDinhDanhSX.Text;
                wareHouseGoodDetail.LoaiHangHoa = Convert.ToDecimal(cbbLoaiHang.SelectedValue.ToString());
                wareHouseGoodDetail.NguonNhap = Convert.ToDecimal(cbbNguonNhap.SelectedValue.ToString());
                wareHouseGoodDetail.SoLuongDuKienNhap = Convert.ToDecimal(txtSoLuongDK.Text);
                wareHouseGoodDetail.SoLuongThucNhap = Convert.ToDecimal(txtSoLuongTN.Text);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        private void SetWareHouseGoodItem()
        {
            try
            {
                txtMaHangHoa.Text = wareHouseGoodDetail.MaHangHoa;
                txtTenHangHoa.Text = wareHouseGoodDetail.TenHangHoa;
                txtDonViTinh.Text = DonViTinh_GetName(wareHouseGoodDetail.DVT);
                txtMaDinhDanhSX.Text = wareHouseGoodDetail.MaDinhDanhSX;
                cbbLoaiHang.SelectedValue = wareHouseGoodDetail.LoaiHangHoa.ToString();
                cbbNguonNhap.SelectedValue = wareHouseGoodDetail.NguonNhap.ToString();
                txtSoLuongDK.Text = wareHouseGoodDetail.SoLuongDuKienNhap.ToString();
                txtSoLuongTN.Text = wareHouseGoodDetail.SoLuongThucNhap.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try            
            {
                this.Cursor = Cursors.WaitCursor;

                if (dgList.GetRows().Length < 0) return;

                wareHouseGoodDetail = (T_KDT_VNACCS_WarehouseImport_GoodsDetail)dgList.CurrentRow.DataRow;
                SetWareHouseGoodItem();
                //SetWareHouseDetail();
                isAddNew = false;
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
                //
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                try
                {
                    string NguonNhap = e.Row.Cells["NguonNhap"].Value.ToString();
                    switch (NguonNhap)
                    {
                        case "1":
                            e.Row.Cells["NguonNhap"].Text = "Nhập từ nước ngoài để gia công";
                            break;
                        case "2":
                            e.Row.Cells["NguonNhap"].Text = "Nhập từ nước ngoài để sxxk";
                            break;
                        case "3":
                            e.Row.Cells["NguonNhap"].Text = "Nhập từ nội địa";
                            break;
                        case "4":
                            e.Row.Cells["NguonNhap"].Text = "Nhập SP gia công từ dây truyền sản xuất";
                            break;
                        case "5":
                            e.Row.Cells["NguonNhap"].Text = "Nhập SP SXXK từ dây truyền sản xuất";
                            break;
                        case "6":
                            e.Row.Cells["NguonNhap"].Text = "Nhập thu hồi NPL từ sản xuất";
                            break;
                        case "7":
                            e.Row.Cells["NguonNhap"].Text = "Nhập chuyển kho nội bộ";
                            break;
                        default:
                            break;
                    }
                    string LoaiHangHoa = e.Row.Cells["LoaiHangHoa"].Value.ToString();
                    switch (LoaiHangHoa)
                    {
                        case "1":
                            e.Row.Cells["LoaiHangHoa"].Text = "Nguyên liệu, vật liệu nhập khẩu ";
                            break;
                        case "2":
                            e.Row.Cells["LoaiHangHoa"].Text = "Thành phẩm được sản xuất từ nguồn nhập khẩu";
                            break;
                        default:
                            break;
                    }
                    e.Row.Cells["DVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT"].Value.ToString());
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }

        private void txtSoPhieuNhap_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
                f.IsShowChonToKhai = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    TKMD = f.TKMDDuocChon;
                    txtSoPhieuNhap.Text = TKMD.SoToKhai.ToString();
                    wareHouseDetail = new T_KDT_VNACCS_WarehouseImport_Detail();
                    foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                    {
                        T_KDT_VNACCS_WarehouseImport_GoodsDetail goodDetail = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();
                        goodDetail.STT = Convert.ToDecimal(hmd.SoDong);
                        goodDetail.MaHangHoa = hmd.MaHangHoa;
                        goodDetail.TenHangHoa = hmd.TenHang;
                        goodDetail.DVT = VNACCS_Mapper.GetCodeV4DVT(hmd.DVTDonGia);
                        goodDetail.NguonNhap = hmd.NuocXuatXu == "VN" ? 1 : 2;
                        goodDetail.LoaiHangHoa = TKMD.LoaiHang == "N" ? 1 : 2;
                        //if (TKMD.LoaiHang=="N")
                        //    goodDetail.LoaiHangHoa = 1;
                        //else
                        //    goodDetail.LoaiHangHoa = 2;     
                        goodDetail.SoLuongDuKienNhap = hmd.SoLuong1;
                        goodDetail.SoLuongThucNhap = hmd.SoLuong1;
                        wareHouseDetail.Collection.Add(goodDetail);
                    }
                    wareHouseImport.WarehouseImportDetailsCollection.Add(wareHouseDetail);
                    LoaiHangHoa = f.TKMDDuocChon.LoaiHang;
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void txtMaNguoiGiao_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    txtMaNguoiGiao.Text = string.IsNullOrEmpty(f.doiTac.MaCongTy) ? "" : f.doiTac.MaCongTy.Trim().ToUpper();
                    txtTenNguoiGiao.Text = string.IsNullOrEmpty(f.doiTac.TenCongTy) ? "" : f.doiTac.TenCongTy.Trim().ToUpper();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHangHoa_ButtonClick(object sender, EventArgs e)
        {
            this.LoaiHangHoa = cbbLoaiHang.SelectedValue.ToString();
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;//VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4
            if (this.LoaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    txtTenHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                    txtDonViTinh.Text = DonViTinh_GetName(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID);
                   
                }

            }
            else if (this.LoaiHangHoa == "1")
            {
                if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm.isBrowser = true;
                this.NPLRegistedForm.ShowDialog(this);
                if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    txtTenHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                    txtDonViTinh.Text = DonViTinh_GetName(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID);

                }                
            }
            else 
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    txtTenHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                    txtDonViTinh.Text = DonViTinh_GetName(this.SPRegistedForm.SanPhamSelected.DVT_ID);
                }
            }
#elif GC_V4
            try
            {
                switch (this.LoaiHangHoa)
                {
                    case "1":
                        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                        this.NPLRegistedForm.isBrower = true;
                        this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        if (TKMD != null)
                        {
                            this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = TKMD.HopDong_ID;
                        }
                        else if (HD!=null)
                        {
                            this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                        }
                        else
                        {
                            this.NPLRegistedForm.isDisplayAll = 3;
                        }
                        this.NPLRegistedForm.ShowDialog();
                        if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
                        {
                            txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                            txtTenHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                            txtDonViTinh.Text = DonViTinh_GetName(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID);
                            //cbbDVT.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        }
                        break;
                    case "2":
                        this.SPRegistedForm = new SanPhamRegistedForm();
                        this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        if (TKMD != null)
                        {
                            this.SPRegistedForm.SanPhamSelected.HopDong_ID = TKMD.HopDong_ID;
                        }
                        else if (HD != null)
                        {
                            this.SPRegistedForm.SanPhamSelected.HopDong_ID = HD.ID;
                        }
                        else
                        {
                            this.SPRegistedForm.isDisplayAll = 3;
                        }
                        this.SPRegistedForm.ShowDialog();
                        if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                        {
                            txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                            txtTenHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                            txtDonViTinh.Text = DonViTinh_GetName(this.SPRegistedForm.SanPhamSelected.DVT_ID);
                            //cbbDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                        }
                        break;

                    case "3":
                        ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                        if (TKMD != null)
                        {
                            ftb.ThietBiSelected.HopDong_ID = TKMD.HopDong_ID;
                        }
                        else if (HD != null)
                        {
                            ftb.ThietBiSelected.HopDong_ID = HD.ID;
                        }
                        else
                        {
                            ftb.isDisplayAll = 3;
                        }
                        ftb.isBrower = true;
                        ftb.ShowDialog();
                        if (!string.IsNullOrEmpty(ftb.ThietBiSelected.Ma))
                        {
                            txtMaHangHoa.Text = ftb.ThietBiSelected.Ma;
                            txtTenHangHoa.Text = ftb.ThietBiSelected.Ten;
                            txtDonViTinh.Text = DonViTinh_GetName(ftb.ThietBiSelected.DVT_ID);
                            //cbbDVT.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
                        }
                        break;
                    case "4":
                        HangMauRegistedForm hangmauForm = new HangMauRegistedForm();
                        hangmauForm.isBrower = true;
                        hangmauForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        if (TKMD != null)
                        {
                            hangmauForm.HangMauSelected.HopDong_ID = TKMD.HopDong_ID;
                        }
                        else if (HD != null)
                        {
                            hangmauForm.HangMauSelected.HopDong_ID = HD.ID;
                        }
                        else
                        {
                            hangmauForm.isDisplayAll = 3;
                        }
                        hangmauForm.ShowDialog();
                        if (!string.IsNullOrEmpty(hangmauForm.HangMauSelected.Ma))
                        {
                            txtMaHangHoa.Text = hangmauForm.HangMauSelected.Ma;
                            txtTenHangHoa.Text = hangmauForm.HangMauSelected.Ten;
                            txtDonViTinh.Text = DonViTinh_GetName(hangmauForm.HangMauSelected.DVT_ID);
                            //cbbDVT.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
            
#endif            
        }

        private void txtMaHangHoa_Leave(object sender, EventArgs e)
        {
            this.LoaiHangHoa = cbbLoaiHang.SelectedValue.ToString();
#if GC_V4       
            try
            {
                bool isNotFound = false;
                //if (TKMD != null)
                //{
                //    LoaiHangHoa = TKMD.LoaiHang;
                //}
                //else
                //{
                //    LoaiHangHoa = "S";
                //}
                if (this.LoaiHangHoa == "1")
                {

                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    if (TKMD!=null)
                    {
                        npl.HopDong_ID = TKMD.HopDong_ID;
                    }
                    else if (HD!=null)
                    {
                        npl.HopDong_ID = HD.ID;
                    }
                    else
                    {

                    }
                    npl.Ma = txtMaHangHoa.Text.Trim();
                    if (npl.Load())
                    {
                        txtMaHangHoa.Text = npl.Ma;
                        txtTenHangHoa.Text = npl.Ten;
                        txtDonViTinh.Text = DonViTinh_GetName(npl.DVT_ID);
                        //cbbDVT.Code = VNACCS_Mapper.GetCodeVNACC(npl.DVT_ID.Trim());
                    }
                    else
                    {
                        isNotFound = true;
                    }
                }
                else if (this.LoaiHangHoa == "2")
                {
                    SanPham sp = new SanPham();
                    if (TKMD != null)
                    {
                        sp.HopDong_ID = TKMD.HopDong_ID;
                    }
                    else if (HD != null)
                    {
                        sp.HopDong_ID = HD.ID;
                    }
                    else
                    {

                    }
                    
                    sp.Ma = txtMaHangHoa.Text.Trim();
                    if (sp.Load())
                    {
                        txtMaHangHoa.Text = sp.Ma;
                        txtTenHangHoa.Text = sp.Ten;
                        txtDonViTinh.Text = DonViTinh_GetName(sp.DVT_ID);
                        //cbbDVT.Code = VNACCS_Mapper.GetCodeVNACC(sp.DVT_ID.Trim());
                    }
                    else
                    {
                        isNotFound = true;
                    }
                }
                else if (this.LoaiHangHoa == "3")
                {
                    ThietBi tb = new ThietBi();
                    if (TKMD != null)
                    {
                        tb.HopDong_ID = TKMD.HopDong_ID;
                    }
                    else if (HD != null)
                    {
                        tb.HopDong_ID = HD.ID;
                    }
                    else
                    {

                    }
                    tb.Ma = txtMaHangHoa.Text.Trim();
                    if (tb.Load())
                    {
                        txtMaHangHoa.Text = tb.Ma;
                        txtTenHangHoa.Text = tb.Ten;
                        txtDonViTinh.Text = DonViTinh_GetName(tb.DVT_ID);
                        //cbbDVT.Code = VNACCS_Mapper.GetCodeVNACC(tb.DVT_ID.Trim());
                    }
                    else
                    {
                        isNotFound = true;
                    }
                }
                else if (this.LoaiHangHoa == "4")
                {
                    HangMau hangmau = new HangMau();
                    if (TKMD != null)
                    {
                         hangmau = HangMau.Load(TKMD.HopDong_ID, txtMaHangHoa.Text.Trim());
                    }
                    else if (HD != null)
                    {
                         hangmau = HangMau.Load(HD.ID, txtMaHangHoa.Text.Trim());
                    }
                    else
                    {

                    }

                    if (hangmau != null)
                    {
                        txtMaHangHoa.Text = hangmau.Ma;
                        txtTenHangHoa.Text = hangmau.Ten;
                        txtDonViTinh.Text = DonViTinh_GetName(hangmau.DVT_ID);
                        //cbbDVT.Code = VNACCS_Mapper.GetCodeVNACC(hangmau.DVT_ID.Trim());
                    }
                    else isNotFound = true;

                }
                if (isNotFound)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }            
#elif SXXK_V4

            if (LoaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                IList<HangDuaVao> listTB = HangDuaVao.SelectCollectionDynamic(string.Format("Ma = '{0}' and LoaiNPL = '3'", txtMaHangHoa.Text.Trim()), null);
                if (listTB != null && listTB.Count > 0)
                {
                    HangDuaVao tb = listTB[0];
                    txtMaHangHoa.Text = tb.Ma;
                    txtTenHangHoa.Text = tb.Ten;
                    txtDonViTinh.Text = DonViTinh_GetName(tb.DVT_ID);
                }
                else
                {
                }
            }
            else if (LoaiHangHoa == "1")
            {
                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.Ma = txtMaHangHoa.Text;
                if (npl.Load())
                {
                    txtMaHangHoa.Text = npl.Ma;
                    txtDonViTinh.Text = DonViTinh_GetName(npl.DVT_ID);
                    txtTenHangHoa.Text = npl.Ten;


                }


            }
            else if (LoaiHangHoa == "2")
            {
                Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = txtMaHangHoa.Text;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (sp.Load())
                {
                    txtMaHangHoa.Text = sp.Ma;
                    txtDonViTinh.Text = DonViTinh_GetName(sp.DVT_ID);
                    txtTenHangHoa.Text = sp.Ten;

                }
            }
#endif
        }

        private void txtMaHangHoa_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnReadExcel_Click(object sender, EventArgs e)
        {
            try
            {
                VNACC_WareHouseReadExcelForm f = new VNACC_WareHouseReadExcelForm();
                f.warehouseImport_Detail = wareHouseDetail;
                f.ShowDialog(this);
                f.Type = "Import";
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType==Company.KDT.SHARE.Components.OpenFormType.View)
                {
                    if (dgList.GetRows().Length < 0) return;
                    wareHouseGoodDetail = (T_KDT_VNACCS_WarehouseImport_GoodsDetail)dgList.CurrentRow.DataRow;
                    SetWareHouseGoodItem();
                    isAddNew = false;   
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
