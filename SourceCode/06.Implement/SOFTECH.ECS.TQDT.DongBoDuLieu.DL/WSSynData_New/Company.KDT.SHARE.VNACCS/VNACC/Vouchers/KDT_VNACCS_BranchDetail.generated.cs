﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.KDT.SHARE.Components.Messages.Vouchers;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_BranchDetail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long BillOfLadings_Details_ID { set; get; }
		public decimal STT { set; get; }
		public string SoVanDon { set; get; }
		public string TenNguoiGuiHang { set; get; }
		public string DiaChiNguoiGuiHang { set; get; }
		public string TenNguoiNhanHang { set; get; }
		public string DiaChiNguoiNhanHang { set; get; }
		public decimal TongSoLuongContainer { set; get; }
		public decimal SoLuongHang { set; get; }
		public string DVTSoLuong { set; get; }
		public decimal TongTrongLuongHang { set; get; }
		public string DVTTrongLuong { set; get; }

        public List<KDT_VNACCS_BranchDetail_TransportEquipment> Collection = new List<KDT_VNACCS_BranchDetail_TransportEquipment>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_BranchDetail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_BranchDetail> collection = new List<KDT_VNACCS_BranchDetail>();
			while (reader.Read())
			{
				KDT_VNACCS_BranchDetail entity = new KDT_VNACCS_BranchDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BillOfLadings_Details_ID"))) entity.BillOfLadings_Details_ID = reader.GetInt64(reader.GetOrdinal("BillOfLadings_Details_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetDecimal(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGuiHang"))) entity.TenNguoiGuiHang = reader.GetString(reader.GetOrdinal("TenNguoiGuiHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiGuiHang"))) entity.DiaChiNguoiGuiHang = reader.GetString(reader.GetOrdinal("DiaChiNguoiGuiHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiNhanHang"))) entity.DiaChiNguoiNhanHang = reader.GetString(reader.GetOrdinal("DiaChiNguoiNhanHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoLuongContainer"))) entity.TongSoLuongContainer = reader.GetDecimal(reader.GetOrdinal("TongSoLuongContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHang"))) entity.SoLuongHang = reader.GetDecimal(reader.GetOrdinal("SoLuongHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTSoLuong"))) entity.DVTSoLuong = reader.GetString(reader.GetOrdinal("DVTSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTrongLuongHang"))) entity.TongTrongLuongHang = reader.GetDecimal(reader.GetOrdinal("TongTrongLuongHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTTrongLuong"))) entity.DVTTrongLuong = reader.GetString(reader.GetOrdinal("DVTTrongLuong"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        //protected static List<BranchDetails> ConvertToCollectionNew(IDataReader reader)
        //{
        //    List<BranchDetails> collection = new List<BranchDetails>();
        //    while (reader.Read())
        //    {
        //        long BranchDetail_ID = reader.GetInt64(reader.GetOrdinal("ID"));
        //        BranchDetails entity = new BranchDetails()
        //        {
        //            TransportEquipments = KDT_VNACCS_BranchDetail_TransportEquipment.SelectCollectionBy_BranchDetail_ID_New(BranchDetail_ID),
        //        };
        //        if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.Sequence = reader.GetDecimal(reader.GetOrdinal("STT")).ToString();
        //        if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.Reference = reader.GetString(reader.GetOrdinal("SoVanDon"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGuiHang"))) entity.ShipperName = reader.GetString(reader.GetOrdinal("TenNguoiGuiHang"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiGuiHang"))) entity.ShipperAddress = reader.GetString(reader.GetOrdinal("DiaChiNguoiGuiHang"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.ConsigneeName = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiNhanHang"))) entity.ConsigneeAddress = reader.GetString(reader.GetOrdinal("DiaChiNguoiNhanHang"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("TongSoLuongContainer"))) entity.NumberOfCont = reader.GetDecimal(reader.GetOrdinal("TongSoLuongContainer")).ToString();
        //        if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHang"))) entity.CargoPiece = reader.GetDecimal(reader.GetOrdinal("SoLuongHang")).ToString();
        //        if (!reader.IsDBNull(reader.GetOrdinal("DVTSoLuong"))) entity.PieceUnitCode = reader.GetString(reader.GetOrdinal("DVTSoLuong")).ToString();
        //        if (!reader.IsDBNull(reader.GetOrdinal("TongTrongLuongHang"))) entity.CargoWeight = reader.GetDecimal(reader.GetOrdinal("TongTrongLuongHang")).ToString();
        //        if (!reader.IsDBNull(reader.GetOrdinal("DVTTrongLuong"))) entity.WeightUnitCode = reader.GetString(reader.GetOrdinal("DVTTrongLuong"));
        //        collection.Add(entity);
        //    }
        //    reader.Close();
        //    return collection;
        //}
		public static bool Find(List<KDT_VNACCS_BranchDetail> collection, long id)
        {
            foreach (KDT_VNACCS_BranchDetail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BranchDetail VALUES(@BillOfLadings_Details_ID, @STT, @SoVanDon, @TenNguoiGuiHang, @DiaChiNguoiGuiHang, @TenNguoiNhanHang, @DiaChiNguoiNhanHang, @TongSoLuongContainer, @SoLuongHang, @DVTSoLuong, @TongTrongLuongHang, @DVTTrongLuong)";
            string update = "UPDATE t_KDT_VNACCS_BranchDetail SET BillOfLadings_Details_ID = @BillOfLadings_Details_ID, STT = @STT, SoVanDon = @SoVanDon, TenNguoiGuiHang = @TenNguoiGuiHang, DiaChiNguoiGuiHang = @DiaChiNguoiGuiHang, TenNguoiNhanHang = @TenNguoiNhanHang, DiaChiNguoiNhanHang = @DiaChiNguoiNhanHang, TongSoLuongContainer = @TongSoLuongContainer, SoLuongHang = @SoLuongHang, DVTSoLuong = @DVTSoLuong, TongTrongLuongHang = @TongTrongLuongHang, DVTTrongLuong = @DVTTrongLuong WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BranchDetail WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, "BillOfLadings_Details_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiGuiHang", SqlDbType.NVarChar, "TenNguoiGuiHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiGuiHang", SqlDbType.NVarChar, "DiaChiNguoiGuiHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanHang", SqlDbType.NVarChar, "DiaChiNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoLuongContainer", SqlDbType.Decimal, "TongSoLuongContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongHang", SqlDbType.Decimal, "SoLuongHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSoLuong", SqlDbType.NVarChar, "DVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTrongLuongHang", SqlDbType.Decimal, "TongTrongLuongHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTTrongLuong", SqlDbType.NVarChar, "DVTTrongLuong", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, "BillOfLadings_Details_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiGuiHang", SqlDbType.NVarChar, "TenNguoiGuiHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiGuiHang", SqlDbType.NVarChar, "DiaChiNguoiGuiHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanHang", SqlDbType.NVarChar, "DiaChiNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoLuongContainer", SqlDbType.Decimal, "TongSoLuongContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongHang", SqlDbType.Decimal, "SoLuongHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSoLuong", SqlDbType.NVarChar, "DVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTrongLuongHang", SqlDbType.Decimal, "TongTrongLuongHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTTrongLuong", SqlDbType.NVarChar, "DVTTrongLuong", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BranchDetail VALUES(@BillOfLadings_Details_ID, @STT, @SoVanDon, @TenNguoiGuiHang, @DiaChiNguoiGuiHang, @TenNguoiNhanHang, @DiaChiNguoiNhanHang, @TongSoLuongContainer, @SoLuongHang, @DVTSoLuong, @TongTrongLuongHang, @DVTTrongLuong)";
            string update = "UPDATE t_KDT_VNACCS_BranchDetail SET BillOfLadings_Details_ID = @BillOfLadings_Details_ID, STT = @STT, SoVanDon = @SoVanDon, TenNguoiGuiHang = @TenNguoiGuiHang, DiaChiNguoiGuiHang = @DiaChiNguoiGuiHang, TenNguoiNhanHang = @TenNguoiNhanHang, DiaChiNguoiNhanHang = @DiaChiNguoiNhanHang, TongSoLuongContainer = @TongSoLuongContainer, SoLuongHang = @SoLuongHang, DVTSoLuong = @DVTSoLuong, TongTrongLuongHang = @TongTrongLuongHang, DVTTrongLuong = @DVTTrongLuong WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BranchDetail WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, "BillOfLadings_Details_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiGuiHang", SqlDbType.NVarChar, "TenNguoiGuiHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiGuiHang", SqlDbType.NVarChar, "DiaChiNguoiGuiHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanHang", SqlDbType.NVarChar, "DiaChiNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoLuongContainer", SqlDbType.Decimal, "TongSoLuongContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongHang", SqlDbType.Decimal, "SoLuongHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSoLuong", SqlDbType.NVarChar, "DVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTrongLuongHang", SqlDbType.Decimal, "TongTrongLuongHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTTrongLuong", SqlDbType.NVarChar, "DVTTrongLuong", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, "BillOfLadings_Details_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.NVarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiGuiHang", SqlDbType.NVarChar, "TenNguoiGuiHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiGuiHang", SqlDbType.NVarChar, "DiaChiNguoiGuiHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanHang", SqlDbType.NVarChar, "DiaChiNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoLuongContainer", SqlDbType.Decimal, "TongSoLuongContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongHang", SqlDbType.Decimal, "SoLuongHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSoLuong", SqlDbType.NVarChar, "DVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTrongLuongHang", SqlDbType.Decimal, "TongTrongLuongHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTTrongLuong", SqlDbType.NVarChar, "DVTTrongLuong", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_BranchDetail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_BranchDetail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_BranchDetail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_BranchDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_BranchDetail> SelectCollectionBy_BillOfLadings_Details_ID(long billOfLadings_Details_ID)
		{
            IDataReader reader = SelectReaderBy_BillOfLadings_Details_ID(billOfLadings_Details_ID);
			return ConvertToCollection(reader);	
		}
        //public static List<BranchDetails> SelectCollectionBy_BillOfLadings_Details_ID_New(long billOfLadings_Details_ID)
        //{
        //    IDataReader reader = SelectReaderBy_BillOfLadings_Details_ID(billOfLadings_Details_ID);
        //    return ConvertToCollectionNew(reader);
        //}	
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_BillOfLadings_Details_ID(long billOfLadings_Details_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_SelectBy_BillOfLadings_Details_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, billOfLadings_Details_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_BillOfLadings_Details_ID(long billOfLadings_Details_ID)
		{
			const string spName = "p_KDT_VNACCS_BranchDetail_SelectBy_BillOfLadings_Details_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, billOfLadings_Details_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_BranchDetail(long billOfLadings_Details_ID, decimal sTT, string soVanDon, string tenNguoiGuiHang, string diaChiNguoiGuiHang, string tenNguoiNhanHang, string diaChiNguoiNhanHang, decimal tongSoLuongContainer, decimal soLuongHang, string dVTSoLuong, decimal tongTrongLuongHang, string dVTTrongLuong)
		{
			KDT_VNACCS_BranchDetail entity = new KDT_VNACCS_BranchDetail();	
			entity.BillOfLadings_Details_ID = billOfLadings_Details_ID;
			entity.STT = sTT;
			entity.SoVanDon = soVanDon;
			entity.TenNguoiGuiHang = tenNguoiGuiHang;
			entity.DiaChiNguoiGuiHang = diaChiNguoiGuiHang;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.DiaChiNguoiNhanHang = diaChiNguoiNhanHang;
			entity.TongSoLuongContainer = tongSoLuongContainer;
			entity.SoLuongHang = soLuongHang;
			entity.DVTSoLuong = dVTSoLuong;
			entity.TongTrongLuongHang = tongTrongLuongHang;
			entity.DVTTrongLuong = dVTTrongLuong;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, BillOfLadings_Details_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@TenNguoiGuiHang", SqlDbType.NVarChar, TenNguoiGuiHang);
			db.AddInParameter(dbCommand, "@DiaChiNguoiGuiHang", SqlDbType.NVarChar, DiaChiNguoiGuiHang);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanHang", SqlDbType.NVarChar, DiaChiNguoiNhanHang);
			db.AddInParameter(dbCommand, "@TongSoLuongContainer", SqlDbType.Decimal, TongSoLuongContainer);
			db.AddInParameter(dbCommand, "@SoLuongHang", SqlDbType.Decimal, SoLuongHang);
			db.AddInParameter(dbCommand, "@DVTSoLuong", SqlDbType.NVarChar, DVTSoLuong);
			db.AddInParameter(dbCommand, "@TongTrongLuongHang", SqlDbType.Decimal, TongTrongLuongHang);
			db.AddInParameter(dbCommand, "@DVTTrongLuong", SqlDbType.NVarChar, DVTTrongLuong);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_BranchDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BranchDetail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_BranchDetail(long id, long billOfLadings_Details_ID, decimal sTT, string soVanDon, string tenNguoiGuiHang, string diaChiNguoiGuiHang, string tenNguoiNhanHang, string diaChiNguoiNhanHang, decimal tongSoLuongContainer, decimal soLuongHang, string dVTSoLuong, decimal tongTrongLuongHang, string dVTTrongLuong)
		{
			KDT_VNACCS_BranchDetail entity = new KDT_VNACCS_BranchDetail();			
			entity.ID = id;
			entity.BillOfLadings_Details_ID = billOfLadings_Details_ID;
			entity.STT = sTT;
			entity.SoVanDon = soVanDon;
			entity.TenNguoiGuiHang = tenNguoiGuiHang;
			entity.DiaChiNguoiGuiHang = diaChiNguoiGuiHang;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.DiaChiNguoiNhanHang = diaChiNguoiNhanHang;
			entity.TongSoLuongContainer = tongSoLuongContainer;
			entity.SoLuongHang = soLuongHang;
			entity.DVTSoLuong = dVTSoLuong;
			entity.TongTrongLuongHang = tongTrongLuongHang;
			entity.DVTTrongLuong = dVTTrongLuong;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_BranchDetail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, BillOfLadings_Details_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@TenNguoiGuiHang", SqlDbType.NVarChar, TenNguoiGuiHang);
			db.AddInParameter(dbCommand, "@DiaChiNguoiGuiHang", SqlDbType.NVarChar, DiaChiNguoiGuiHang);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanHang", SqlDbType.NVarChar, DiaChiNguoiNhanHang);
			db.AddInParameter(dbCommand, "@TongSoLuongContainer", SqlDbType.Decimal, TongSoLuongContainer);
			db.AddInParameter(dbCommand, "@SoLuongHang", SqlDbType.Decimal, SoLuongHang);
			db.AddInParameter(dbCommand, "@DVTSoLuong", SqlDbType.NVarChar, DVTSoLuong);
			db.AddInParameter(dbCommand, "@TongTrongLuongHang", SqlDbType.Decimal, TongTrongLuongHang);
			db.AddInParameter(dbCommand, "@DVTTrongLuong", SqlDbType.NVarChar, DVTTrongLuong);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_BranchDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BranchDetail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_BranchDetail(long id, long billOfLadings_Details_ID, decimal sTT, string soVanDon, string tenNguoiGuiHang, string diaChiNguoiGuiHang, string tenNguoiNhanHang, string diaChiNguoiNhanHang, decimal tongSoLuongContainer, decimal soLuongHang, string dVTSoLuong, decimal tongTrongLuongHang, string dVTTrongLuong)
		{
			KDT_VNACCS_BranchDetail entity = new KDT_VNACCS_BranchDetail();			
			entity.ID = id;
			entity.BillOfLadings_Details_ID = billOfLadings_Details_ID;
			entity.STT = sTT;
			entity.SoVanDon = soVanDon;
			entity.TenNguoiGuiHang = tenNguoiGuiHang;
			entity.DiaChiNguoiGuiHang = diaChiNguoiGuiHang;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.DiaChiNguoiNhanHang = diaChiNguoiNhanHang;
			entity.TongSoLuongContainer = tongSoLuongContainer;
			entity.SoLuongHang = soLuongHang;
			entity.DVTSoLuong = dVTSoLuong;
			entity.TongTrongLuongHang = tongTrongLuongHang;
			entity.DVTTrongLuong = dVTTrongLuong;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, BillOfLadings_Details_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@TenNguoiGuiHang", SqlDbType.NVarChar, TenNguoiGuiHang);
			db.AddInParameter(dbCommand, "@DiaChiNguoiGuiHang", SqlDbType.NVarChar, DiaChiNguoiGuiHang);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanHang", SqlDbType.NVarChar, DiaChiNguoiNhanHang);
			db.AddInParameter(dbCommand, "@TongSoLuongContainer", SqlDbType.Decimal, TongSoLuongContainer);
			db.AddInParameter(dbCommand, "@SoLuongHang", SqlDbType.Decimal, SoLuongHang);
			db.AddInParameter(dbCommand, "@DVTSoLuong", SqlDbType.NVarChar, DVTSoLuong);
			db.AddInParameter(dbCommand, "@TongTrongLuongHang", SqlDbType.Decimal, TongTrongLuongHang);
			db.AddInParameter(dbCommand, "@DVTTrongLuong", SqlDbType.NVarChar, DVTTrongLuong);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_BranchDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BranchDetail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_BranchDetail(long id)
		{
			KDT_VNACCS_BranchDetail entity = new KDT_VNACCS_BranchDetail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int DeleteBy_BillOfLadings_Details_ID(long billOfLadings_Details_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_DeleteBy_BillOfLadings_Details_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BillOfLadings_Details_ID", SqlDbType.BigInt, billOfLadings_Details_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_BranchDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BranchDetail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert();
                    }
                    else
                    {
                        this.Update();
                    }

                    foreach (KDT_VNACCS_BranchDetail_TransportEquipment item in Collection)
                    {
                        //item.BranchDetail_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.BranchDetail_ID = this.ID;
                            item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {

                }

        }
        public void DeleteFull()
        {
            try
            {
                foreach (KDT_VNACCS_BranchDetail_TransportEquipment item in Collection)
                {
                    item.Delete();
                    this.DeleteBy_BillOfLadings_Details_ID(this.ID);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }
	}	
}