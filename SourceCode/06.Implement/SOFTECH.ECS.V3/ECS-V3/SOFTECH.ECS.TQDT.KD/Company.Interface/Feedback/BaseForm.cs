﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
using System.Threading;
using System.Resources;
using System;
using Janus.Windows.FilterEditor;
using Janus.Windows.EditControls;
using Janus.Windows.UI.StatusBar;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Tab;
using Janus.Windows.UI.Dock;
using Janus.Windows.GridEX;
using Janus.Windows.ExplorerBar;

namespace Company.Interface.Feedback
{
    public partial class BaseForm : Form
    {
        public enum OpenFormType
        {
            View,
            Insert,
            Edit,
            EditAll
        }

        public OpenFormType OpenType;
        public string CalledForm = string.Empty;
        private IContainer components = null;
        private bool _isChanged = false;
        private string _title = "";

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public bool IsChanged
        {
            get { return _isChanged; }
            set { _isChanged = value; }
        }

        protected void SetChanged(bool change)
        {
            if (change)
            {
                Text = Title + " (*)";
                IsChanged = true; ;
            }
            else
            {
                Text = Title;
                IsChanged = false;
            }
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
        }

        public BaseForm()
        {
            InitializeComponent();

            Title = this.Text;
        }

    }
}