﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class BillOfLading : IssueBase
    {

        /// <summary>
        /// Phương tiện vận tải
        /// </summary>
        [XmlElement("BorderTransportMeans")]
        public BorderTransportMeans BorderTransportMeans { get; set; }
        /// <summary>
        /// Tên vận tải
        /// </summary>
        [XmlElement("Carrier")]
        public NameBase Carrier { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("Consignment")]
        public Consignment Consignment { get; set; }


    }
}
