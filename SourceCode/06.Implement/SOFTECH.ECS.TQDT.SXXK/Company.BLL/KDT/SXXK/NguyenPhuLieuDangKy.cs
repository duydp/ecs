﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.BLL.Utils;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;

namespace Company.BLL.KDT.SXXK
{
    public delegate void EventWait(bool isExit, string title);
    public partial class NguyenPhuLieuDangKy
    {
        private NguyenPhuLieuCollection _NPLCollection = new NguyenPhuLieuCollection();
        public event EventWait Wait;
        public NguyenPhuLieuCollection NPLCollection
        {
            set { this._NPLCollection = value; }
            get { return this._NPLCollection; }
        }

        //-----------------------------------------------------------------------------------------
        public void LoadNPLCollection()
        {
            this._NPLCollection = NguyenPhuLieu.SelectCollectionBy_Master_ID(this.ID);
        }

        public void LoadNPLCollection(SqlTransaction transaction)
        {
            this._NPLCollection = NguyenPhuLieu.SelectCollectionBy_Master_ID(this.ID, transaction);
        }
        //-----------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_SXXK_NguyenPhuLieuDangKy_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_SXXK_NguyenPhuLieuDangKy_Update";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, this._MaDaiLy);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdateFull(string connectionString)
        {
            bool ret;
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    //Kiem tra ton tai NPL DK tren Database Target
                    NguyenPhuLieuDangKy nplTemp = NguyenPhuLieuDangKy.Load(this._SoTiepNhan, this._NgayTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, this._TrangThaiXuLy, connectionString);
                    //KDT Nguyen phu lieu dang ky
                    if (nplTemp == null || nplTemp._ID == 0)
                        this._ID = this.InsertTransaction(transaction, db);
                    else
                    {
                        this._ID = nplTemp.ID;
                        this.UpdateTransaction(transaction, db);
                    }
                    //KDT Nguyen phu lieu
                    foreach (NguyenPhuLieu nplDetail in this._NPLCollection)
                    {
                        nplDetail.Master_ID = this._ID;

                        nplDetail.InsertUpdateTransactionBy(transaction, db);
                    }
                    //SXXK NPL
                    TransgferDataToSXXK(transaction, db);

                    transaction.Commit();
                    ret = true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFull(string connectionString, int i)
        {
            bool ret;
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    //Kiem tra ton tai NPL DK tren Database Target
                    NguyenPhuLieuDangKy nplTemp = NguyenPhuLieuDangKy.Load(this._SoTiepNhan, this._NgayTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, this._TrangThaiXuLy, connectionString);
                    //KDT Nguyen phu lieu dang ky
                    if (nplTemp == null || nplTemp._ID == 0)
                        this._ID = this.InsertTransaction(transaction, db);
                    else
                    {
                        this._ID = nplTemp.ID;
                        this.UpdateTransaction(transaction, db);
                    }
                    //KDT Nguyen phu lieu
                    foreach (NguyenPhuLieu nplDetail in this._NPLCollection)
                    {
                        nplDetail.Master_ID = this._ID;

                        nplDetail.InsertUpdateTransactionBy(transaction, db);
                    }
                    //SXXK NPL
                    TransgferDataToSXXK(transaction, db, i);

                    transaction.Commit();
                    ret = true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        //-----------------------------------------------------------------------------------------

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);

                    foreach (NguyenPhuLieu nplDetail in this._NPLCollection)
                    {
                        if (nplDetail.ID == 0)
                        {
                            nplDetail.Master_ID = this._ID;
                            nplDetail.ID = nplDetail.InsertTransaction(transaction);
                        }
                        else
                        {
                            NguyenPhuLieu NPL = NguyenPhuLieu.Load(nplDetail.ID);
                            BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                            nplSXXK.Ma = NPL.Ma;
                            nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplSXXK.MaHaiQuan = this.MaHaiQuan;
                            nplSXXK.DeleteTransaction(transaction);


                            nplDetail.UpdateTransaction(transaction);
                        }
                    }
                    TransgferDataToSXXK(transaction);
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int Delete1()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DeleteNPLCollection(transaction);
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }

            }
            return 1;
        }

        public void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this._ID == 0)
                this._ID = this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);

            foreach (NguyenPhuLieu nplDetail in this._NPLCollection)
            {
                if (nplDetail.ID == 0)
                {
                    nplDetail.Master_ID = this._ID;
                    nplDetail.ID = nplDetail.InsertTransaction(transaction);
                }
                else
                {
                    NguyenPhuLieu NPL = NguyenPhuLieu.Load(nplDetail.ID);
                    BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                    nplSXXK.Ma = NPL.Ma;
                    nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                    nplSXXK.MaHaiQuan = this.MaHaiQuan;
                    nplSXXK.DeleteTransaction(transaction);
                    nplDetail.UpdateTransaction(transaction);
                }
            }
            TransgferDataToSXXK(transaction);
        }

        public void DeleteNPLCollection(SqlTransaction transaction)
        {
            string sql = "delete from t_KDT_SXXK_NguyenPhuLieu where master_id=@ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                db.ExecuteNonQuery(dbCommand, transaction);
            else
                db.ExecuteNonQuery(dbCommand);

            foreach (NguyenPhuLieu npl in this.NPLCollection)
            {
                BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                nplSXXK.Ma = npl.Ma;
                nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                nplSXXK.MaHaiQuan = this.MaHaiQuan;
                nplSXXK.DeleteTransaction(transaction);
            }
        }
        public bool CloneToDB(SqlTransaction transaction)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand("p_KDT_SXXK_NguyenPhuLieuDangKy_Copy");
                db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
                if (transaction != null)
                    db.ExecuteNonQuery(dbCommand, transaction);
                else
                    db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch
            {
                return false;
            }
        }
        //public DataTable selectNPLDangKyDynamic(string where)
        //{
        //    DataSet ds = new DataSet();
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    string sql = "SELECT a.ID,SoTiepNhan,NgayTiepNhan,MaHaiQuan,MaDoanhNghiep,TrangThaiXuLy,b.Ten as TenHaiQuan "+
        //                 "FROM t_KDT_SXXK_NguyenPhuLieuDangKy a INNER JOIN t_HaiQuan_DonViHaiQuan b on a.MaHaiQuan = b.ID WHERE ";
        //    if (where != string.Empty) sql = sql + where;
        //    DbCommand dbCommand = db.GetSqlStringCommand(sql);
        //    db.LoadDataSet(dbCommand, ds, "NPLDangKy");
        //    return ds.Tables[0];
        //}
        //-----------------------------------------------------------------------------------------

        public static void DongBoDuLieuPhongKhai(NguyenPhuLieuDangKyCollection nplDKCollection)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (NguyenPhuLieuDangKy nplDangKy in nplDKCollection)
                    {
                        foreach (NguyenPhuLieu npl in nplDangKy.NPLCollection)
                        {
                            npl.ID = 0;
                        }
                        MsgSend msg = new MsgSend();
                        msg.master_id = nplDangKy.ID;
                        msg.LoaiHS = "NPL";
                        msg.DeleteTransaction(transaction);
                        nplDangKy.DeleteNPLCollection(transaction);
                        nplDangKy.InsertUpdateFull(transaction);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }


        }

        public void TransgferDataToSXXK()
        {
            if (this.NPLCollection.Count == 0)
            {
                this.LoadNPLCollection();
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);

                    foreach (NguyenPhuLieu npl in this.NPLCollection)
                    {
                        BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                        nplSXXK.DVT_ID = npl.DVT_ID;
                        nplSXXK.Ma = npl.Ma;
                        nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                        nplSXXK.MaHaiQuan = this.MaHaiQuan;
                        nplSXXK.MaHS = npl.MaHS;
                        nplSXXK.Ten = npl.Ten;
                        nplSXXK.InsertUpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void TransgferDataToSXXK(SqlTransaction transaction)
        {
            if (this.NPLCollection.Count == 0)
            {
                this.LoadNPLCollection(transaction);
            }
            foreach (NguyenPhuLieu npl in this.NPLCollection)
            {
                BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                nplSXXK.DVT_ID = npl.DVT_ID;
                nplSXXK.Ma = npl.Ma;
                nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                nplSXXK.MaHaiQuan = this.MaHaiQuan;
                nplSXXK.MaHS = npl.MaHS;
                nplSXXK.Ten = npl.Ten;
                nplSXXK.InsertUpdateTransaction(transaction);
            }
        }

        public void TransgferDataToSXXK(SqlTransaction transaction, SqlDatabase db)
        {
            if (this.NPLCollection.Count == 0)
            {
                this.LoadNPLCollection(transaction);
            }
            foreach (NguyenPhuLieu npl in this.NPLCollection)
            {
                BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                nplSXXK.DVT_ID = npl.DVT_ID;
                nplSXXK.Ma = npl.Ma;
                nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                nplSXXK.MaHaiQuan = this.MaHaiQuan;
                nplSXXK.MaHS = npl.MaHS;
                nplSXXK.Ten = npl.Ten;
                nplSXXK.InsertUpdateTransaction(transaction, db);
            }
        }

        public void TransgferDataToSXXK(SqlTransaction transaction, SqlDatabase db, int i)
        {
            if (this.NPLCollection.Count == 0)
            {
                this.LoadNPLCollection(transaction);
            }
            foreach (NguyenPhuLieu npl in this.NPLCollection)
            {
                BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                nplSXXK.DVT_ID = npl.DVT_ID;
                nplSXXK.Ma = npl.Ma;
                nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                nplSXXK.MaHaiQuan = this.MaHaiQuan;
                nplSXXK.MaHS = npl.MaHS;
                nplSXXK.Ten = npl.Ten;
                nplSXXK.InsertUpdateTransaction(transaction, db);

                //TODO: HungTQ, Update 27/04/2010.
                Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("{6}.KDT Nguyen phu lieu: MaHaiQuan={4}, MaDoanhNghiep={5}, Ma={0}, Ten={1}, DVT={2}, MaHS={3} ", nplSXXK.Ma, nplSXXK.Ten, nplSXXK.DVT_ID, nplSXXK.MaHS, nplSXXK.MaHaiQuan, nplSXXK.MaDoanhNghiep, i)));

            }
        }

        #region WebService Methods của Softech

        ////-----------------------------------------------------------------------------------------
        ///// <summary>
        ///// Kiểm tra sự tồn tại của các mã nguyên phụ liệu. 
        ///// </summary>
        ///// <returns>Danh sách nguyên phụ liệu đã tồn tại.</returns>
        //public NguyenPhuLieuCollection WSCheckExist()
        //{
        //    // Chuẩn bị dữ liệu để gửi qua WEB SERVICE.
        //    SXXKService sxxkService = new SXXKService();
        //    string[] danhsachNPL = new string[this.NPLCollection.Count];
        //    for (int i = 0; i < this.NPLCollection.Count; i++)
        //    {
        //        danhsachNPL[i] = this.NPLCollection[i].Ma;
        //    }
        //    string[] danhsachNPLDaDangKy = sxxkService.NguyenPhuLieu_CheckExist(this.MaHaiQuan, this.MaDoanhNghiep, danhsachNPL);

        //    // Dữ liệu trả về.
        //    NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
        //    foreach (string s in danhsachNPLDaDangKy)
        //    {
        //        foreach (NguyenPhuLieu npl in this.NPLCollection)
        //        {
        //            if (npl.Ma == s)
        //            {
        //                collection.Add(npl);
        //                break;
        //            }
        //        }
        //    }
        //    return collection;
        //}

        ////-----------------------------------------------------------------------------------------
        //public long WSSend(ref string[] DanhSachDaDangKy)
        //{
        //    // Chuẩn bị dữ liệu để gửi qua WEB SERVICE.
        //    // Lấy thông tin đăng ký.
        //    KDT_SXXK_Service kdtService = new KDT_SXXK_Service();
        //    NguyenPhuLieuDangKyInfo webNPLDangKyInfo = new NguyenPhuLieuDangKyInfo();
        //    webNPLDangKyInfo.MaHaiQuan = this._MaHaiQuan;
        //    webNPLDangKyInfo.MaDoanhNghiep = this._MaDoanhNghiep;
        //    webNPLDangKyInfo.MaDaiLy = this.MaDaiLy;
        //    webNPLDangKyInfo.SoTiepNhan = this.SoTiepNhan;
        //    webNPLDangKyInfo.NamTiepNhan = (short)this.NgayTiepNhan.Year;
        //    webNPLDangKyInfo.NgayTiepNhan = this.NgayTiepNhan;
        //    // Lấy danh sách nguyên phụ liệu.
        //    int count = this._NPLCollection.Count;
        //    webNPLDangKyInfo.NPLInfoCollection = new NguyenPhuLieuInfo[count];
        //    int i = 0;
        //    foreach (NguyenPhuLieu nplDetail in this._NPLCollection)
        //    {
        //        NguyenPhuLieuInfo webNPLInfo = new NguyenPhuLieuInfo();
        //        webNPLInfo.MaHaiQuan = this._MaHaiQuan;
        //        webNPLInfo.MaDoanhNghiep = this._MaDoanhNghiep;
        //        webNPLInfo.Ma = nplDetail.Ma;
        //        webNPLInfo.Ten = nplDetail.Ten;
        //        webNPLInfo.MaHS = nplDetail.MaHS;
        //        webNPLInfo.DVT_ID = nplDetail.DVT_ID;
        //        webNPLDangKyInfo.NPLInfoCollection[i] = webNPLInfo;
        //        i++;
        //    }

        //    // Thực hiện gửi dữ liệu.
        //    this._SoTiepNhan = kdtService.NguyenPhuLieu_Send(webNPLDangKyInfo, ref DanhSachDaDangKy);
        //    if (this._SoTiepNhan > 0)
        //    {
        //        this._TrangThaiXuLy = BLL.TrangThaiXuLy.CHO_DUYET;
        //        this._NgayTiepNhan = DateTime.Today;
        //        this.InsertUpdateFull();
        //    }
        //    return this._SoTiepNhan;
        //}

        ////-----------------------------------------------------------------------------------------
        ///// <summary>
        ///// Xóa thông tin đăng ký tại Hải quan.
        ///// </summary>
        //public bool WSCancel()
        //{
        //    KDT_SXXK_Service kdtService = new KDT_SXXK_Service();
        //    bool result = kdtService.NguyenPhuLieu_Cancel(this._SoTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, this._NgayTiepNhan.Year);
        //    if (result)
        //    {
        //        // Trả lại trạng thái chưa khai báo.
        //        this.SoTiepNhan = 0;
        //        this.NgayTiepNhan = new DateTime(1900, 1, 1);
        //        this.TrangThaiXuLy = BLL.TrangThaiXuLy.CHUA_KHAI_BAO;
        //        this.Update();
        //    }
        //    return result;
        //}

        ///// <summary>
        ///// Cập nhật dữ liệu từ client -> server.
        ///// </summary>
        ///// <param name="DanhSachDaDangKy">Danh sách nguyên phụ liệu đã đăng ký.</param>
        ///// <returns>0: Không thành công; 1: Thành công; 2: Đã duyệt, không thể cập nhật; 3: Có một số nguyên phụ liệu đã đăng ký.</returns>
        //public int WSUpload(ref string[] DanhSachDaDangKy)
        //{
        //    // Chuẩn bị dữ liệu để gửi qua WEB SERVICE.
        //    // Lấy thông tin đăng ký.
        //    //KDT_SXXK_Service kdtService = new KDT_SXXK_Service();
        //    //NguyenPhuLieuDangKyInfo webNPLDangKyInfo = new NguyenPhuLieuDangKyInfo();
        //    //webNPLDangKyInfo.MaHaiQuan = this._MaHaiQuan;
        //    //webNPLDangKyInfo.MaDoanhNghiep = this._MaDoanhNghiep;
        //    //webNPLDangKyInfo.MaDaiLy = this.MaDaiLy;

        //    //// Lấy danh sách nguyên phụ liệu.
        //    //int count = this._NPLCollection.Count;
        //    //webNPLDangKyInfo.NPLInfoCollection = new NguyenPhuLieuInfo[count];
        //    //int i = 0;
        //    //foreach (NguyenPhuLieu nplDetail in this._NPLCollection)
        //    //{
        //    //    NguyenPhuLieuInfo webNPLInfo = new NguyenPhuLieuInfo();
        //    //    webNPLInfo.MaHaiQuan = this._MaHaiQuan;
        //    //    webNPLInfo.MaDoanhNghiep = this._MaDoanhNghiep;
        //    //    webNPLInfo.Ma = nplDetail.Ma;
        //    //    webNPLInfo.Ten = nplDetail.Ten;
        //    //    webNPLInfo.MaHS = nplDetail.MaHS;
        //    //    webNPLInfo.DVT_ID = nplDetail.DVT_ID;
        //    //    webNPLDangKyInfo.NPLInfoCollection[i] = webNPLInfo;
        //    //    i++;
        //    //}

        //    //// Thực hiện.
        //    //return kdtService.SXXK_NguyenPhuLieuDangKy_Upload(this._SoTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, this._NgayTiepNhan.Year, webNPLDangKyInfo.NPLInfoCollection, ref DanhSachDaDangKy);
        //    return 0;
        //}
        ////-----------------------------------------------------------------------------------------
        //public void WSDownload()
        //{
        //     //Các bước thực hiện:
        //     //1. Lấy thông tin từ Web Service.
        //     //2. Cập nhật thông tin đăng ký.
        //     //3. Xóa danh sách nguyên phụ liệu cũ.
        //     //4. Cập nhật lại danh sách nguyên phụ liệu mới.
        //     //5. Nếu trạng thái: Đã duyệt chính thức thì chuyển danh sách nguyên phụ liệu đã duyệt sang phân hệ SXXK.

        //     //1. Lấy danh sách từ Web Service.
        //    KDT_SXXK_Service kdtService = new KDT_SXXK_Service();
        //    DataSet ds = kdtService.NguyenPhuLieu_RequestStatus(this._SoTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, Convert.ToInt16(this._NgayTiepNhan.Year));
        //    DataTable dt = ds.Tables[0];
        //    if (dt.Rows.Count > 0)
        //    {
        //        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //        using (SqlConnection connection = (SqlConnection) db.CreateConnection())
        //        {
        //            connection.Open();
        //            SqlTransaction transaction = connection.BeginTransaction();
        //            try
        //            {
        //                // 2. Cập nhật thông tin đăng ký.
        //                this._TrangThaiXuLy = Convert.ToInt32(dt.Rows[0]["TrangThaiXL"]);

        //                // 3. Xóa danh sách nguyên phụ liệu cũ.
        //                this.LoadNPLCollection();
        //                NguyenPhuLieu.DeleteCollection(this._NPLCollection, transaction);
        //                this.NPLCollection.Clear();

        //                // 4. Cập nhật lại danh sách nguyên phụ liệu mới.
        //                int stt = 1;
        //                foreach (DataRow row in dt.Rows)
        //                {
        //                    NguyenPhuLieu npl = new NguyenPhuLieu();
        //                    npl.Master_ID = this._ID;
        //                    npl.Ma = row["Ma_NPL"].ToString();
        //                    npl.Ten = row["Ten_NPL"].ToString();
        //                    npl.MaHS = row["Ma_HS"].ToString();
        //                    npl.DVT_ID = row["Ma_DVT"].ToString();
        //                    npl.STTHang = stt++;
        //                    this._NPLCollection.Add(npl);
        //                }

        //                // Cập nhật vào CSDL.
        //                this.InsertUpdateFull(transaction);

        //                // 5. Nếu trạng thái là đã duyệt chính thức thì chuyển danh sách nguyên phụ liệu đã duyệt sang phân hệ SXXK.
        //                if (this._TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                {
        //                    foreach (DataRow row in dt.Rows)
        //                    {
        //                        BLL.SXXK.NguyenPhuLieu npl = new BLL.SXXK.NguyenPhuLieu();
        //                        npl.MaHaiQuan = this._MaHaiQuan;
        //                        npl.MaDoanhNghiep = this._MaDoanhNghiep;
        //                        npl.Ma = row["Ma_NPL"].ToString();
        //                        npl.Ten = row["Ten_NPL"].ToString();
        //                        npl.MaHS = row["Ma_HS"].ToString();
        //                        npl.DVT_ID = row["Ma_DVT"].ToString();

        //                        npl.InsertUpdateTransaction(transaction);
        //                    }
        //                }

        //                transaction.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw new Exception(ex.Message);
        //            }
        //            finally
        //            {
        //                connection.Close();
        //            }
        //        }
        //    }
        //    else
        //    {
        //        this.Update();
        //    }
        //}

        //public void WSRequestStatus()
        //{
        //    //Các bước thực hiện:
        //    //1. Lấy thông tin từ Web Service.
        //    //2. Cập nhật thông tin đăng ký.
        //    //2. Nếu trạng thái: Đã duyệt chính thức thì chuyển danh sách nguyên phụ liệu đã duyệt sang phân hệ SXXK.

        //    //1. Lấy danh sách từ Web Service.
        //    KDT_SXXK_Service kdtService = new KDT_SXXK_Service();
        //    DataSet ds = kdtService.NguyenPhuLieu_RequestStatus(this._SoTiepNhan, this._MaHaiQuan, this._MaDoanhNghiep, Convert.ToInt16(this._NgayTiepNhan.Year));
        //    DataTable dt = ds.Tables[0];
        //    if (dt.Rows.Count > 0)
        //    {
        //        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //        using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //        {
        //            connection.Open();
        //            SqlTransaction transaction = connection.BeginTransaction();
        //            try
        //            {
        //                // 2. Cập nhật thông tin đăng ký.
        //                this._TrangThaiXuLy = Convert.ToInt32(dt.Rows[0]["TrangThaiXL"]);

        //                // Cập nhật vào CSDL.
        //                this.UpdateTransaction(transaction);

        //                // 5. Nếu trạng thái là đã duyệt chính thức thì chuyển danh sách nguyên phụ liệu đã duyệt sang phân hệ SXXK.
        //                if (this._TrangThaiXuLy == BLL.TrangThaiXuLy.DA_DUYET)
        //                {
        //                    foreach (DataRow row in dt.Rows)
        //                    {
        //                        BLL.SXXK.NguyenPhuLieu npl = new BLL.SXXK.NguyenPhuLieu();
        //                        npl.MaHaiQuan = this._MaHaiQuan;
        //                        npl.MaDoanhNghiep = this._MaDoanhNghiep;
        //                        npl.Ma = row["Ma_NPL"].ToString();
        //                        npl.Ten = row["Ten_NPL"].ToString();
        //                        npl.MaHS = row["Ma_HS"].ToString();
        //                        npl.DVT_ID = row["Ma_DVT"].ToString();
        //                        npl.InsertUpdateTransaction(transaction);
        //                    }
        //                }

        //                transaction.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw new Exception(ex.Message);
        //            }
        //            finally
        //            {
        //                connection.Close();
        //            }
        //        }
        //    }
        //    else
        //    {
        //        this.Update();
        //    }
        //}
        //#endregion WebService Methods của Softech

        #region WebService Methods của Hải quan
        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            //lanNT khong cho phep tu dong sinh ma GuidStr vi da sinh ma khi luu thong tin
            //this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            this.Update();
            return doc.InnerXml;
        }

        private string ConfigPhongBiPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProram();
            doc.Load(path + "\\TemplateXML\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            //this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            // this.Update();

            return doc.InnerXml;
        }

        #region Khai báo danh sách nguyên phụ liệu
        public string LoadXML()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXML());


            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            return doc.InnerXml;
        }
        public string WSSendXML(string pass, string xml)
        {

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                Globals.SaveMessage(xml, this.ID, MessageTitle.KhaiBaoNguyenPhuLieu);
                kq = kdt.Send(xml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return xml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }
        public string WSSendXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.KhaiBao));
            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXML());


            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);


            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();


            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                Globals.SaveMessage(doc.InnerXml, this.ID, MessageTitle.KhaiBaoNguyenPhuLieu);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage("Không thể gửi: Message", new Exception(doc.InnerXml));
                Logger.LocalLogger.Instance().WriteMessage(ex);

                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    //XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    //XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";
        }

        private string ConvertCollectionToXML()
        {
            //load du lieu
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + "\\TemplateXML\\KhaiBaoNguyenPhuLieu.xml");
            XmlNode nplNode = docNPL.GetElementsByTagName("NPL")[0];

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            if (this.NPLCollection == null || this.NPLCollection.Count == 0)
            {
                this.NPLCollection = NguyenPhuLieu.SelectCollectionBy_Master_ID(this.ID);
            }
            foreach (NguyenPhuLieu npl in this._NPLCollection)
            {
                XmlNode node = docNPL.CreateElement("NPL.ITEM");
                XmlAttribute sttAtt = docNPL.CreateAttribute("STT");
                sttAtt.Value = npl.STTHang.ToString();
                node.Attributes.Append(sttAtt);

                XmlAttribute maAtt = docNPL.CreateAttribute("MA_NPL");
                maAtt.Value = npl.Ma;
                node.Attributes.Append(maAtt);

                XmlAttribute MaHSAtt = docNPL.CreateAttribute("MA_HS");
                MaHSAtt.Value = npl.MaHS;
                node.Attributes.Append(MaHSAtt);

                XmlAttribute TenAtt = docNPL.CreateAttribute("TEN_NPL");
                //Hungtq updated 18/01/2012. Chuyển mã font từ Unicde -> TCVN3.
                // KhanhHN upadte 22/06/2012. Config mã font theo hải quan
                if (Globals.FontName == "TCVN3")
                    TenAtt.Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(npl.Ten);
                else
                    TenAtt.Value = npl.Ten.Trim();
                //Hungtq updated 13/03/2012. Sử dụng Unicode.
                //TenAtt.Value = npl.Ten;
                node.Attributes.Append(TenAtt);

                XmlAttribute dvtAtt = docNPL.CreateAttribute("MA_DVT");
                dvtAtt.Value = npl.DVT_ID.ToString();
                node.Attributes.Append(dvtAtt);

                nplNode.AppendChild(node);
            }
            return docNPL.InnerXml;
        }
        #endregion Khai báo danh sách nguyên phụ liệu

        #region Huy khai báo danh mục nguyên phụ liệu

        public string WSCancelXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.HuyKhaiBao));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + "\\TemplateXML\\HuyKhaiBaoNguyenPhuLieu.xml");

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);


            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];

            Content.AppendChild(root);
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                Globals.SaveMessage(doc.InnerXml, this.ID, MessageTitle.KhaiBaoHuyNguyenPhuLieu);
                kq = kdt.Send(doc.InnerXml, pass);
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";

        }

        #endregion Huy khai báo danh mục nguyên phụ liệu

        #region Lấy phản hồi

        public string WSRequestXML(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.HoiTrangThai));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + "\\TemplateXML\\LayPhanHoiNguyenPhuLieu.xml");

            XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeDN.Attributes["TEN_DV"].Value = "";

            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU");
            nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {

                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";

        }

        #endregion Lấy phản hồi

        public string LayPhanHoi(string pass, string xml)
        {

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.GetElementsByTagName("function")[0].InnerText = "5";
            //HungTQ, Update 21/04/2010.
            doc.GetElementsByTagName("declarationType")[0].InnerText = "1";

            string kq = "";
            int k = 0;
            int i = 0;

            for (i = 1; i <= 3; ++i)
            {
                k = i;
                try
                {


                    if (Wait != null)
                        Wait(false, string.Format("Nhận phản hồi lần {0}", i));
                    else
                        System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
                    }
                }
                catch
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }
            if (Wait != null)
                Wait(true, string.Empty);
            if (i > 1)
                return doc.InnerXml;

            try
            {
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
                {
                    XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                    if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.NPL)
                    {
                        #region Lấy số tiếp nhận của danh sách NPL

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NamDK = Convert.ToInt16(nodeDuLieu.Attributes["NAM_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.TrangThaiXuLy = 0;
                            this.Update();
                        }
                        #endregion Lấy số tiếp nhận của danh sách NPL
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoLayPhanHoiNguyenPhuLieu, string.Format("Số tiếp nhận {0}, năm đăng ký {1},ngày tiếp nhận {2}, trạng thái xử lý {3}", this.SoTiepNhan, this.NamDK, this.NgayTiepNhan, this.TrangThaiXuLy));

                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.NPL)
                    {
                        #region Hủy khai báo NPL

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            this.SoTiepNhan = 0;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                            this.Update();
                        }

                        #endregion Hủy khai báo NPL
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHuyNguyenPhuLieu, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == LAY_THONG_TIN.NPL)
                    {
                        #region Nhận trạng thái hồ sơ NPL
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "DA_XU_LY")
                        {
                            this.TrangThaiXuLy = BLL.TrangThaiXuLy.DA_DUYET;
                            TransgferDataToSXXK();
                            Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQDaDuyetNguyenPhuLieu, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                        }
                        else if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "TU_CHOI")
                        {
                            this.TrangThaiXuLy = BLL.TrangThaiXuLy.KHONG_PHE_DUYET;
                            this.Update();
                            Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQKhongDuyetNguyenPhuLieu, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                        }
                        #endregion Nhận trạng thái hồ sơ NPL
                    }
                }
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
                {
                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                    string errorSt = "";
                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";

                    Globals.SaveMessage(kq, this.ID, errorSt, nodeMota.InnerText);
                    throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
                }
            }
            catch (Exception ex)
            {
                if (Wait != null)
                    Wait(true, string.Empty);
                Logger.LocalLogger.Instance().WriteMessage("Không thể xử lý message kết quả", new Exception(kq));
                throw ex;
            }
            return "";

        }

        #endregion WebService Methods của Hải quan

        //#region Webservice của FPT

        //#region Khai báo danh sách nguyên phụ liệu
        ////cap nhat
        //public string WSUpdateXMLFPT(string pass)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_NPL_CAPNHAT.xml");

        //    //luu thong tin gui
        //    XmlNode hq = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hq.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    hq.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode data = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    data.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
        //    data.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

        //    XmlNode nplNode = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL");
        //    foreach (NguyenPhuLieu npl in this.NPLCollection)
        //    {
        //        XmlNode node = doc.CreateElement("NPL.ITEM");

        //        XmlAttribute maAtt = doc.CreateAttribute("MA_NPL");
        //        maAtt.Value = npl.Ma;
        //        node.Attributes.Append(maAtt);

        //        XmlAttribute MaHSAtt = doc.CreateAttribute("MA_HS");
        //        MaHSAtt.Value = npl.MaHS;
        //        node.Attributes.Append(MaHSAtt);

        //        XmlAttribute TenAtt = doc.CreateAttribute("TEN_NPL");
        //        TenAtt.Value = npl.Ten;
        //        node.Attributes.Append(TenAtt);


        //        XmlAttribute DVTAtt = doc.CreateAttribute("MA_DVT");
        //        DVTAtt.Value = npl.DVT_ID;
        //        node.Attributes.Append(DVTAtt);
        //        nplNode.AppendChild(node);
        //    }
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    return kq;
        //}

        ////dang ky
        //public bool WSSendXMLFPT(string pass)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_NPL_DK.xml");

        //    //luu thong tin gui
        //    XmlNode hq = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hq.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    hq.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";


        //    XmlNode nplNode = doc.SelectSingleNode("Root/SXXK/DU_LIEU/NPL");
        //   if (_NPLCollection == null || _NPLCollection.Count == 0)
        //   {
        //       _NPLCollection = NguyenPhuLieu.SelectCollectionBy_Master_ID(this.ID);
        //   }
        //   foreach (NguyenPhuLieu npl in _NPLCollection)
        //    {
        //        XmlNode node = doc.CreateElement("NPL.ITEM");

        //        XmlAttribute maAtt = doc.CreateAttribute("MA_NPL");
        //        maAtt.Value = npl.Ma;
        //        node.Attributes.Append(maAtt);

        //        XmlAttribute MaHSAtt = doc.CreateAttribute("MA_HS");
        //        MaHSAtt.Value = npl.MaHS;
        //        node.Attributes.Append(MaHSAtt);

        //        XmlAttribute TenAtt = doc.CreateAttribute("TEN_NPL");
        //        TenAtt.Value = npl.Ten;
        //        node.Attributes.Append(TenAtt);

        //        XmlAttribute DVTAtt = doc.CreateAttribute("MA_DVT");
        //        DVTAtt.Value = npl.DVT_ID;
        //        node.Attributes.Append(DVTAtt);
        //        nplNode.AppendChild(node);
        //    }
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    string kq = service.DataProcess(doc.InnerXml, @"ecusxgeiyklgA\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        XmlNode nodeData = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        this.SoTiepNhan = Convert.ToInt64(nodeData.Attributes["SO_TN"].Value);
        //        this.NgayTiepNhan = DateTime.Today;
        //        this.TrangThaiXuLy = BLL.TrangThaiXuLy.CHO_DUYET;
        //        this.InsertUpdate();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi=nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText+"|"+stMucLoi);
        //    }                
        //    return true;

        //}

        //#endregion Khai báo danh sách nguyên phụ liệu

        //#region Hủy khai báo danh sách nguyên phụ liệu

        ////huy dang ky
        //public bool WSHuyXMLFPT(string pass)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_NPL_HUY.xml");

        //    //luu thong tin gui
        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode data = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    data.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
        //    data.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();
        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);

        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;         
        //    if (stKQ == "THANH CONG")
        //    {
        //        XmlNode nodeData = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        this.SoTiepNhan = 0;
        //        this.NgayTiepNhan = new DateTime(1900, 1, 1);
        //        this.TrangThaiXuLy = BLL.TrangThaiXuLy.CHUA_KHAI_BAO;
        //        this.Update();
        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //    return true;
        //}

        //#endregion Hủy khai báo danh sách nguyên phụ liệu


        //#region Lấy thông tin trả lời từ hải quan

        ////lây thong tin dang ky
        //public bool WSRequestXMLFPT(string pass)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load("FPTService\\SXXK_LAY_TT.xml");

        //    XmlNode hqNhan = doc.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
        //    hqNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
        //    hqNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan);

        //    XmlNode dv = doc.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
        //    dv.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
        //    dv.Attributes["TEN_DV"].Value = "";

        //    XmlNode data = doc.SelectSingleNode("Root/SXXK/DU_LIEU");
        //    data.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
        //    data.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

        //    XmlNode nodeYeuCau = doc.SelectSingleNode("Root/SXXK");
        //    nodeYeuCau.Attributes["YEU_CAU"].Value = LAY_THONG_TIN.NPL;

        //    WS.FPT.SXXK.SXXKService service = new Company.BLL.WS.FPT.SXXK.SXXKService();
        //    string kq = service.DataProcess(doc.InnerXml, "ecusxgeiyklgA\\SmqUK@rbiqDyAgbXBIlBTUTL;", pass);
        //    XmlDocument docResult = new XmlDocument();
        //    docResult.LoadXml(kq);
        //    XmlNode nodeRS = docResult.SelectSingleNode("Root/SXXK");
        //    string stKQ = nodeRS.Attributes["TRANG_THAI"].Value;
        //    if (stKQ == "THANH CONG")
        //    {
        //        XmlNode nodeData = docResult.SelectSingleNode("Root/SXXK/DU_LIEU");
        //        string stTT = nodeData.Attributes["TRANG_THAI"].Value;
        //        if (stTT == "DA_XU_LY")
        //        {
        //            this.TrangThaiXuLy = BLL.TrangThaiXuLy.DA_DUYET;
        //            TransgferDataToSXXK();
        //        }
        //        else if (stTT == "TU_CHOI")
        //        {
        //            this.TrangThaiXuLy = BLL.TrangThaiXuLy.KHONG_PHE_DUYET;
        //            this.Update();                   
        //        }

        //    }
        //    else
        //    {
        //        XmlNode nodeMota = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
        //        XmlNode nodeMucLoi = docResult.SelectSingleNode("Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
        //        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
        //        string errorSt = "";
        //        if (stMucLoi == "XML_LEVEL")
        //            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
        //        else if (stMucLoi == "DATA_LEVEL")
        //            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
        //        else if (stMucLoi == "SERVICE_LEVEL")
        //            errorSt = "Lỗi do Web service trả về ";
        //        else if (stMucLoi == "DOTNET_LEVEL")
        //            errorSt = "Lỗi do hệ thống của hải quan ";
        //        throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
        //    }
        //    return true;
        //}

        //public void TransgferDataToSXXKNguyenPhuLieuTon()
        //{
        //    if (this.NPLCollection.Count == 0)
        //    {
        //        this.LoadNPLCollection();
        //    }
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        try
        //        {
        //            this.UpdateTransaction(transaction);

        //            foreach (NguyenPhuLieu npl in this.NPLCollection)
        //            {
        //                BLL.SXXK.NguyenPhuLieuTon nplSXXK = new Company.BLL.SXXK.NguyenPhuLieuTon();                        
        //                nplSXXK.Ma = npl.Ma;
        //                nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
        //                nplSXXK.MaHaiQuan = this.MaHaiQuan;                        
        //                nplSXXK.Ten = npl.Ten;
        //                nplSXXK.LuongTon = 0;
        //                nplSXXK.InsertUpdateTransaction(transaction);
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw new Exception(ex.Message);
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}
        //#endregion Lấy thông tin trả lời từ hải quan


        #endregion Webservice của FPT

        #region TQDT :
        public string TQDTWSLayPhanHoi(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiPhanHoi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.HoiTrangThai));
            //doc.LoadXml(ConfigPhongBiPhanHoi(MessgaseType.DanhMucNguyenPhuLieu, MessgaseFunction.LayPhanHoi));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + "\\TemplateXML\\LayPhanHoiDaDuyet.xml");

            //XmlNode nodeHQNhan = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/HQ_NHAN");
            //nodeHQNhan.Attributes["MA_HQ"].Value = this.MaHaiQuan;
            //nodeHQNhan.Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);

            //XmlNode nodeDN = docNPL.SelectSingleNode("Root/SXXK/THONG_TIN/DON_VI_GUI");
            //nodeDN.Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            //nodeDN.Attributes["TEN_DV"].Value = "";

            //XmlNode nodeDuLieu = docNPL.SelectSingleNode("Root/SXXK/DU_LIEU");
            //nodeDuLieu.Attributes["SO_TN"].Value = this.SoTiepNhan.ToString();
            //nodeDuLieu.Attributes["NAM_TN"].Value = this.NgayTiepNhan.Year.ToString();

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = "";

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan);
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
            }
            return "";

        }
        public string TQDTLayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            //doc.GetElementsByTagName("function")[0].InnerText = "5";
            doc.GetElementsByTagName("function")[0].InnerText = MessgaseFunction.LayPhanHoi.ToString();
            doc.GetElementsByTagName("type")[0].InnerText = MessgaseType.ThongTin.ToString();

            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);

            string kq = "";
            int k = 0;
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                k = i;
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);

                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "Web Service");
                    }
                }
                catch
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;

            /*XU LY THONG TIN MESSAGE PHAN HOI TU HAI QUAN*/

            try
            {
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "THANH CONG")
                {

                    XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK");
                    if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_DANG_KY.NPL)
                    {
                        #region Lấy số tiếp nhận của danh sách NPL

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SO_TN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.NamDK = short.Parse(nodeDuLieu.Attributes["NAM_TN"].Value);
                            this.TrangThaiXuLy = 0;
                            this.Update();
                        }
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoLayPhanHoiNguyenPhuLieu, string.Format("Số tiếp nhận {0},năm đăng ký {1},ngày tiếp nhận {2}", this.SoTiepNhan, this.NamDK, this.NgayTiepNhan));
                        #endregion Lấy số tiếp nhận của danh sách NPL
                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == THONG_TIN_HUY.NPL)
                    {
                        #region Hủy khai báo NPL

                        if (nodeTrangthai.Attributes["TRANG_THAI"].Value == "THANH CONG")
                        {
                            this.SoTiepNhan = 0;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                            this.Update();
                        }

                        #endregion Hủy khai báo NPL
                        Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoChapNhanHuyNguyenPhuLieu);


                    }
                    else if (nodeTrangthai.Attributes["TRA_LOI"].Value == LAY_THONG_TIN.NPL)
                    {
                        #region Nhận trạng thái hồ sơ NPL
                        XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU");
                        if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "DA_XU_LY")
                        {
                            string phanHoi = TQDTWSLayPhanHoi(pass);

                            if (phanHoi.Length != 0)
                            {
                                XmlDocument docPH = new XmlDocument();
                                docPH.LoadXml(phanHoi);

                                XmlNode nodeTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                                if (nodeTuChoi.Attributes.GetNamedItem("TuChoi") != null
                                    && nodeTuChoi.Attributes["TuChoi"].Value == "yes")
                                {
                                    //nodeTuChoi.InnerText;
                                    this.TrangThaiXuLy = BLL.TrangThaiXuLy.KHONG_PHE_DUYET;
                                    this.Update();
                                    Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQKhongDuyetNguyenPhuLieu);

                                }
                            }
                            else
                            {
                                this.TrangThaiXuLy = BLL.TrangThaiXuLy.DA_DUYET;
                                TransgferDataToSXXK();
                                this.Update();
                                Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQDaDuyetNguyenPhuLieu);
                            }
                        }
                        else if (nodeDuLieu.Attributes["TRANG_THAI"].Value == "TU_CHOI")
                        {
                            this.TrangThaiXuLy = BLL.TrangThaiXuLy.KHONG_PHE_DUYET;
                            this.Update();
                            Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQKhongDuyetNguyenPhuLieu);

                        }
                        #endregion Nhận trạng thái hồ sơ NPL
                    }

                }
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "LOI")
                {
                    XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MO_TA");
                    XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK/DU_LIEU/TT_LOI/MA_LOI");
                    string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;
                    string errorSt = "";
                    if (stMucLoi == "XML_LEVEL")
                        errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                    else if (stMucLoi == "DATA_LEVEL")
                        errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                    else if (stMucLoi == "SERVICE_LEVEL")
                        errorSt = "Lỗi do Web service trả về ";
                    else if (stMucLoi == "DOTNET_LEVEL")
                        errorSt = "Lỗi do hệ thống của hải quan ";
                    throw new Exception(errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi);
                }

                //DATLMQ update kiem tra thong tin Trang thai da duyet 25/11/2010
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK") != null
                    && docNPL.SelectSingleNode("Envelope/Body/Content/Root/SXXK").Attributes["TRANG_THAI"].Value == "yes")
                {
                    XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SOTN"].Value);
                    this.NamDK = Convert.ToSByte(nodeDuLieu.Attributes["NAMTN"].Value);
                    //this.NgayTiepNhan = Convert.ToDateTime(nodeDuLieu.Attributes["NGAYTN"].Value);
                    this.ActionStatus = 1;
                    this.TrangThaiXuLy = Company.BLL.TrangThaiXuLy.DA_DUYET;
                    this.Update();

                    Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_NguyenPhuLieu;
                    kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_HopDongDuocDuyet;
                    //kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNăm tiếp nhận: {1}\r\nNgày tiếp nhận: {2}", this.SoTiepNhan, this.NamTN, this.NgayTiepNhan.ToShortDateString());
                    kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNăm tiếp nhận: {1}", this.SoTiepNhan, this.NamDK);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                }

                /*Kiem tra PHAN LUONG*/
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTN") != null)
                {
                    #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                    XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                    this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                    this.NgayTiepNhan = DateTime.Today;
                    this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);
                    this.TrangThaiXuLy = BLL.TrangThaiXuLy.DA_DUYET;

                    /*Lay thong tin phan luong*/
                    if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                    {
                        XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                        this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                        this.Huongdan_PL = nodePhanLuong.Attributes["HUONGDAN"].Value;
                    }

                    this.Update();

                    #endregion Lấy số tiếp nhận của danh sách NPL
                    Globals.SaveMessage(docNPL.InnerXml, this.ID, MessageTitle.KhaiBaoHQDuyetNguyennPhuLieu);
                }
                else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("TuChoi") != null)
                {
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 01, 01);
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.HUONGDAN = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("TuChoi").InnerText.ToString();
                    this.Update();

                    Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    kqxl.ItemID = this.ID;
                    kqxl.ReferenceID = new Guid(this.GUIDSTR);
                    kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_NguyenPhuLieu;
                    kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                    kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNăm tiếp nhận: {1} bị Hải quan từ chối", this.SoTiepNhan, this.NamDK);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                    Globals.SaveMessage(kq, this.ID, MessageTitle.KhaiBaoHQKhongDuyetNguyenPhuLieu, docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }

            return "";

        }

        #endregion

        public static NguyenPhuLieuDangKy Load(long iD)
        {
            string spName = "[dbo].p_KDT_SXXK_NguyenPhuLieuDangKy_Load";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            NguyenPhuLieuDangKy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new NguyenPhuLieuDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
            }
            reader.Close();
            dbCommand.Connection.Close();
            return entity;
        }

        public static NguyenPhuLieuDangKy Load(long SoTiepNhan, DateTime NgayTiepNhan, string MaHaiQuan, string MaDoanhNghiep, int TrangThaiXuLy, string connectionString)
        {
            SqlDatabase db = new SqlDatabase(connectionString);

            string spName = "[dbo].p_KDT_SXXK_NguyenPhuLieuDangKy_LoadBy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);

            NguyenPhuLieuDangKy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new NguyenPhuLieuDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
            }
            reader.Close();
            dbCommand.Connection.Close();
            return entity;
        }

        public bool Load(long SoTiepNhan, DateTime NgayTiepNhan, string MaHaiQuan, string MaDoanhNghiep, int TrangThaiXuLy)
        {
            string spName = "[dbo].p_KDT_SXXK_NguyenPhuLieuDangKy_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NVarChar, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) this._MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;

        }

        public int DeleteDynamicTransaction(string where, SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_NguyenPhuLieuDangKy_DeleteDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, where);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public bool InsertUpdateFullDaiLy()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    NguyenPhuLieuDangKy objTemp = new NguyenPhuLieuDangKy();
                    objTemp.Load(SoTiepNhan, NgayTiepNhan, MaHaiQuan, MaDoanhNghiep, TrangThaiXuLy);

                    if (objTemp.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.ID = objTemp.ID;
                        this.UpdateTransaction(transaction);
                    }

                    foreach (NguyenPhuLieu nplDetail in this.NPLCollection)
                    {
                        nplDetail.Master_ID = this.ID;
                        NguyenPhuLieu tmp = NguyenPhuLieu.Load(nplDetail.Ma);
                        if (tmp == null)
                        {
                            nplDetail.Master_ID = this.ID;
                            nplDetail.ID = nplDetail.InsertTransaction(transaction);
                        }
                        else
                        {
                            //NguyenPhuLieu NPL = NguyenPhuLieu.Load(nplDetail.ID);
                            BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                            //nplSXXK.Ma = NPL.Ma;
                            nplSXXK.Ma = tmp.Ma;
                            nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplSXXK.MaHaiQuan = this.MaHaiQuan;
                            nplSXXK.DeleteTransaction(transaction);


                            nplDetail.UpdateTransaction(transaction);
                        }
                    }
                    TransgferDataToSXXK(transaction);
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }
}