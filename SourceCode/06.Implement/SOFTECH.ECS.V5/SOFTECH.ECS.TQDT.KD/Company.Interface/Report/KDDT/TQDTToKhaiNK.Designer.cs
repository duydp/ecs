﻿namespace Company.Interface.Report.KDDT
{
    partial class TQDTToKhaiNK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.Shape.ShapeRectangle shapeRectangle1 = new DevExpress.XtraPrinting.Shape.ShapeRectangle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TQDTToKhaiNK));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.lblChungTu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHuongDan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaTinhThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDongTienThanhToan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhuongThucThanhToan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDieuKienGiaoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNuocXK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhuongTienVanTai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCangDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCangXepHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayVanTaiDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblVanTaiDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHetHanHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHetHanGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHoaDonThuongMai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLoaiHinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiXK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGui = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThamChieu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHQCK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrShape1 = new DevExpress.XtraReports.UI.XRShape();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine19 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhiBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongThueXNKSo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongThueXNKChu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDeXuatKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbltongtrongluong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChitietCon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCon20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCon40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongsoKien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayIn = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaCangdoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThueGTGT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaThuKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenPTVT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLyDoTK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMienThueNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMienThueGTGT = new DevExpress.XtraReports.UI.XRLabel();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1,
            this.xrLabel2,
            this.xrLabel3,
            this.xrLabel1,
            this.winControlContainer1,
            this.lblMienThueNK,
            this.lblMienThueGTGT});
            this.PageHeader.Height = 1140;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblChungTu,
            this.lblHuongDan,
            this.lblTyGiaTinhThue,
            this.lblDongTienThanhToan,
            this.lblPhuongThucThanhToan,
            this.lblDieuKienGiaoHang,
            this.lblNuocXK,
            this.lblPhuongTienVanTai,
            this.lblCangDoHang,
            this.lblCangXepHang,
            this.lblNgayVanTaiDon,
            this.lblVanTaiDon,
            this.lblNgayHetHanHopDong,
            this.lblNgayHopDong,
            this.lblHopDong,
            this.lblNgayHetHanGiayPhep,
            this.lblNgayGiayPhep,
            this.lblGiayPhep,
            this.lblNgayHoaDon,
            this.lblHoaDonThuongMai,
            this.lblLoaiHinh,
            this.lblNguoiUyThac,
            this.lblNguoiNK,
            this.lblNguoiXK,
            this.lblToKhai,
            this.lblNgayDangKy,
            this.lblNgayGui,
            this.lblThamChieu,
            this.lblChiCucHQCK,
            this.lblChiCucHQ,
            this.xrShape1,
            this.xrLine17,
            this.xrLabel36,
            this.xrLabel35,
            this.xrLine19,
            this.xrLabel34,
            this.xrLabel32,
            this.xrLine18,
            this.xrLine16,
            this.xrLabel33,
            this.xrLabel31,
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel13,
            this.xrLine10,
            this.xrLabel12,
            this.xrLine5,
            this.xrLabel30,
            this.xrLabel29,
            this.xrLine15,
            this.xrLabel26,
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel25,
            this.xrLine14,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLine13,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel24,
            this.xrLabel21,
            this.xrLabel20,
            this.xrLine11,
            this.xrLabel11,
            this.xrLine4,
            this.xrLabel17,
            this.xrLine12,
            this.xrLabel16,
            this.xrLabel10,
            this.xrLine3,
            this.xrLabel8,
            this.xrLabel9,
            this.xrLine2,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLine1,
            this.xrLabel5,
            this.xrLabel4,
            this.xrTable16,
            this.xrTable1,
            this.xrTable3,
            this.xrTable19,
            this.xrTable2,
            this.xrTable23,
            this.xrTable24,
            this.xrTable5,
            this.xrTable6,
            this.xrTable7,
            this.lblMaCangdoHang,
            this.lblMaDoanhNghiep,
            this.xrTable20,
            this.xrLabel43,
            this.xrTable22,
            this.lblTenPTVT,
            this.lblTenNuoc,
            this.lblLyDoTK,
            this.xrLabel38});
            this.xrPanel1.Location = new System.Drawing.Point(18, 75);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrPanel1.Size = new System.Drawing.Size(775, 1000);
            this.xrPanel1.StylePriority.UseBorders = false;
            // 
            // lblChungTu
            // 
            this.lblChungTu.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblChungTu.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblChungTu.Location = new System.Drawing.Point(532, 307);
            this.lblChungTu.Name = "lblChungTu";
            this.lblChungTu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChungTu.Size = new System.Drawing.Size(244, 25);
            this.lblChungTu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHuongDan
            // 
            this.lblHuongDan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblHuongDan.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblHuongDan.Location = new System.Drawing.Point(16, 307);
            this.lblHuongDan.Name = "lblHuongDan";
            this.lblHuongDan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHuongDan.Size = new System.Drawing.Size(491, 42);
            this.lblHuongDan.Text = " ";
            this.lblHuongDan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTyGiaTinhThue
            // 
            this.lblTyGiaTinhThue.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTyGiaTinhThue.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTyGiaTinhThue.Location = new System.Drawing.Point(630, 255);
            this.lblTyGiaTinhThue.Name = "lblTyGiaTinhThue";
            this.lblTyGiaTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaTinhThue.Size = new System.Drawing.Size(100, 15);
            this.lblTyGiaTinhThue.Text = " ";
            this.lblTyGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDongTienThanhToan
            // 
            this.lblDongTienThanhToan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblDongTienThanhToan.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblDongTienThanhToan.Location = new System.Drawing.Point(423, 255);
            this.lblDongTienThanhToan.Name = "lblDongTienThanhToan";
            this.lblDongTienThanhToan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDongTienThanhToan.Size = new System.Drawing.Size(66, 15);
            this.lblDongTienThanhToan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblPhuongThucThanhToan
            // 
            this.lblPhuongThucThanhToan.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblPhuongThucThanhToan.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblPhuongThucThanhToan.Location = new System.Drawing.Point(670, 230);
            this.lblPhuongThucThanhToan.Name = "lblPhuongThucThanhToan";
            this.lblPhuongThucThanhToan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhuongThucThanhToan.Size = new System.Drawing.Size(95, 15);
            this.lblPhuongThucThanhToan.Text = " ";
            this.lblPhuongThucThanhToan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDieuKienGiaoHang
            // 
            this.lblDieuKienGiaoHang.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblDieuKienGiaoHang.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblDieuKienGiaoHang.Location = new System.Drawing.Point(420, 230);
            this.lblDieuKienGiaoHang.Name = "lblDieuKienGiaoHang";
            this.lblDieuKienGiaoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDieuKienGiaoHang.Size = new System.Drawing.Size(66, 15);
            this.lblDieuKienGiaoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNuocXK
            // 
            this.lblNuocXK.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNuocXK.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNuocXK.Location = new System.Drawing.Point(714, 190);
            this.lblNuocXK.Name = "lblNuocXK";
            this.lblNuocXK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNuocXK.Size = new System.Drawing.Size(42, 15);
            this.lblNuocXK.StylePriority.UseTextAlignment = false;
            this.lblNuocXK.Text = " ";
            this.lblNuocXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblPhuongTienVanTai
            // 
            this.lblPhuongTienVanTai.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblPhuongTienVanTai.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblPhuongTienVanTai.Location = new System.Drawing.Point(308, 207);
            this.lblPhuongTienVanTai.Name = "lblPhuongTienVanTai";
            this.lblPhuongTienVanTai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhuongTienVanTai.Size = new System.Drawing.Size(275, 15);
            this.lblPhuongTienVanTai.StylePriority.UseTextAlignment = false;
            this.lblPhuongTienVanTai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblCangDoHang
            // 
            this.lblCangDoHang.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblCangDoHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblCangDoHang.Location = new System.Drawing.Point(600, 152);
            this.lblCangDoHang.Name = "lblCangDoHang";
            this.lblCangDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCangDoHang.Size = new System.Drawing.Size(186, 32);
            this.lblCangDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblCangXepHang
            // 
            this.lblCangXepHang.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblCangXepHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblCangXepHang.Location = new System.Drawing.Point(441, 152);
            this.lblCangXepHang.Name = "lblCangXepHang";
            this.lblCangXepHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCangXepHang.Size = new System.Drawing.Size(153, 32);
            this.lblCangXepHang.Text = " ";
            this.lblCangXepHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayVanTaiDon
            // 
            this.lblNgayVanTaiDon.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayVanTaiDon.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayVanTaiDon.Location = new System.Drawing.Point(325, 170);
            this.lblNgayVanTaiDon.Name = "lblNgayVanTaiDon";
            this.lblNgayVanTaiDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayVanTaiDon.Size = new System.Drawing.Size(107, 15);
            this.lblNgayVanTaiDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblVanTaiDon
            // 
            this.lblVanTaiDon.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblVanTaiDon.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblVanTaiDon.Location = new System.Drawing.Point(291, 152);
            this.lblVanTaiDon.Name = "lblVanTaiDon";
            this.lblVanTaiDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblVanTaiDon.Size = new System.Drawing.Size(142, 17);
            this.lblVanTaiDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayHetHanHopDong
            // 
            this.lblNgayHetHanHopDong.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayHetHanHopDong.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayHetHanHopDong.Location = new System.Drawing.Point(678, 115);
            this.lblNgayHetHanHopDong.Name = "lblNgayHetHanHopDong";
            this.lblNgayHetHanHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHetHanHopDong.Size = new System.Drawing.Size(104, 15);
            this.lblNgayHetHanHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayHopDong.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayHopDong.Location = new System.Drawing.Point(637, 96);
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHopDong.Size = new System.Drawing.Size(131, 17);
            this.lblNgayHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHopDong
            // 
            this.lblHopDong.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblHopDong.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblHopDong.Location = new System.Drawing.Point(673, 80);
            this.lblHopDong.Name = "lblHopDong";
            this.lblHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHopDong.Size = new System.Drawing.Size(112, 15);
            this.lblHopDong.Text = "MN-FZ20091206";
            this.lblHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayHetHanGiayPhep
            // 
            this.lblNgayHetHanGiayPhep.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayHetHanGiayPhep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayHetHanGiayPhep.Location = new System.Drawing.Point(516, 115);
            this.lblNgayHetHanGiayPhep.Name = "lblNgayHetHanGiayPhep";
            this.lblNgayHetHanGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHetHanGiayPhep.Size = new System.Drawing.Size(75, 17);
            this.lblNgayHetHanGiayPhep.Text = " ";
            this.lblNgayHetHanGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayGiayPhep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayGiayPhep.Location = new System.Drawing.Point(475, 96);
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayGiayPhep.Size = new System.Drawing.Size(83, 17);
            this.lblNgayGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGiayPhep
            // 
            this.lblGiayPhep.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblGiayPhep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblGiayPhep.Location = new System.Drawing.Point(517, 80);
            this.lblGiayPhep.Name = "lblGiayPhep";
            this.lblGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiayPhep.Size = new System.Drawing.Size(72, 15);
            this.lblGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayHoaDon
            // 
            this.lblNgayHoaDon.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayHoaDon.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayHoaDon.Location = new System.Drawing.Point(325, 115);
            this.lblNgayHoaDon.Name = "lblNgayHoaDon";
            this.lblNgayHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHoaDon.Size = new System.Drawing.Size(75, 17);
            this.lblNgayHoaDon.Text = " ";
            this.lblNgayHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblHoaDonThuongMai
            // 
            this.lblHoaDonThuongMai.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblHoaDonThuongMai.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblHoaDonThuongMai.Location = new System.Drawing.Point(290, 100);
            this.lblHoaDonThuongMai.Name = "lblHoaDonThuongMai";
            this.lblHoaDonThuongMai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHoaDonThuongMai.Size = new System.Drawing.Size(142, 15);
            this.lblHoaDonThuongMai.StylePriority.UseTextAlignment = false;
            this.lblHoaDonThuongMai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblLoaiHinh
            // 
            this.lblLoaiHinh.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.lblLoaiHinh.Location = new System.Drawing.Point(370, 55);
            this.lblLoaiHinh.Name = "lblLoaiHinh";
            this.lblLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLoaiHinh.Size = new System.Drawing.Size(383, 18);
            this.lblLoaiHinh.StylePriority.UseFont = false;
            this.lblLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblLoaiHinh.Text = "NKD01 Nhập Kinh doanh";
            this.lblLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNguoiUyThac
            // 
            this.lblNguoiUyThac.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNguoiUyThac.Location = new System.Drawing.Point(99, 163);
            this.lblNguoiUyThac.Name = "lblNguoiUyThac";
            this.lblNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiUyThac.Size = new System.Drawing.Size(184, 35);
            this.lblNguoiUyThac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNguoiNK
            // 
            this.lblNguoiNK.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNguoiNK.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNguoiNK.Location = new System.Drawing.Point(0, 127);
            this.lblNguoiNK.Multiline = true;
            this.lblNguoiNK.Name = "lblNguoiNK";
            this.lblNguoiNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiNK.Size = new System.Drawing.Size(281, 25);
            this.lblNguoiNK.StylePriority.UseTextAlignment = false;
            this.lblNguoiNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNguoiXK
            // 
            this.lblNguoiXK.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNguoiXK.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNguoiXK.Location = new System.Drawing.Point(0, 75);
            this.lblNguoiXK.Name = "lblNguoiXK";
            this.lblNguoiXK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiXK.Size = new System.Drawing.Size(281, 25);
            this.lblNguoiXK.StylePriority.UseTextAlignment = false;
            this.lblNguoiXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblToKhai
            // 
            this.lblToKhai.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblToKhai.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblToKhai.Location = new System.Drawing.Point(623, 10);
            this.lblToKhai.Name = "lblToKhai";
            this.lblToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblToKhai.Size = new System.Drawing.Size(73, 17);
            this.lblToKhai.StylePriority.UseFont = false;
            this.lblToKhai.Text = " ";
            this.lblToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.lblNgayDangKy.Location = new System.Drawing.Point(660, 27);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.Size = new System.Drawing.Size(123, 18);
            this.lblNgayDangKy.StylePriority.UseFont = false;
            this.lblNgayDangKy.Text = " ";
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayGui
            // 
            this.lblNgayGui.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayGui.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.lblNgayGui.Location = new System.Drawing.Point(392, 27);
            this.lblNgayGui.Name = "lblNgayGui";
            this.lblNgayGui.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayGui.Size = new System.Drawing.Size(130, 18);
            this.lblNgayGui.Text = " ";
            this.lblNgayGui.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThamChieu
            // 
            this.lblThamChieu.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblThamChieu.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblThamChieu.Location = new System.Drawing.Point(392, 8);
            this.lblThamChieu.Name = "lblThamChieu";
            this.lblThamChieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThamChieu.Size = new System.Drawing.Size(80, 17);
            this.lblThamChieu.Tag = "Số tham chiếu:";
            this.lblThamChieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblThamChieu.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblThamChieu_PreviewClick);
            // 
            // lblChiCucHQCK
            // 
            this.lblChiCucHQCK.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblChiCucHQCK.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblChiCucHQCK.Location = new System.Drawing.Point(155, 25);
            this.lblChiCucHQCK.Name = "lblChiCucHQCK";
            this.lblChiCucHQCK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQCK.Size = new System.Drawing.Size(125, 15);
            this.lblChiCucHQCK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblChiCucHQ
            // 
            this.lblChiCucHQ.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblChiCucHQ.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblChiCucHQ.Location = new System.Drawing.Point(117, 4);
            this.lblChiCucHQ.Name = "lblChiCucHQ";
            this.lblChiCucHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQ.Size = new System.Drawing.Size(165, 17);
            this.lblChiCucHQ.StylePriority.UseFont = false;
            this.lblChiCucHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrShape1
            // 
            this.xrShape1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrShape1.Location = new System.Drawing.Point(23, 245);
            this.xrShape1.Name = "xrShape1";
            this.xrShape1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrShape1.Shape = shapeRectangle1;
            this.xrShape1.Size = new System.Drawing.Size(26, 17);
            this.xrShape1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine17
            // 
            this.xrLine17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine17.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine17.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine17.Location = new System.Drawing.Point(506, 223);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine17.Size = new System.Drawing.Size(10, 130);
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel36.Location = new System.Drawing.Point(531, 290);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.Size = new System.Drawing.Size(177, 15);
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.Text = "19. Chứng từ Hải quan trước đó";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel35.Location = new System.Drawing.Point(0, 290);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.Size = new System.Drawing.Size(327, 15);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.Text = "18. Kết quả phân luồng và hướng dẫn làm thủ tục Hải quan:";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine19
            // 
            this.xrLine19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine19.Location = new System.Drawing.Point(0, 280);
            this.xrLine19.Name = "xrLine19";
            this.xrLine19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine19.Size = new System.Drawing.Size(799, 10);
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel34.Location = new System.Drawing.Point(523, 255);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.Size = new System.Drawing.Size(107, 15);
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.Text = "17. Tỷ giá tính thuế:";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel32.Location = new System.Drawing.Point(523, 230);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.Size = new System.Drawing.Size(147, 15);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.Text = "15. Phương thức thanh toán:";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine18
            // 
            this.xrLine18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine18.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine18.Location = new System.Drawing.Point(290, 248);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine18.Size = new System.Drawing.Size(500, 7);
            // 
            // xrLine16
            // 
            this.xrLine16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine16.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine16.Location = new System.Drawing.Point(291, 220);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine16.Size = new System.Drawing.Size(500, 9);
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel33.Location = new System.Drawing.Point(291, 255);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.Size = new System.Drawing.Size(132, 15);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.Text = "16. Đồng tiền thanh toán:";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel31.Location = new System.Drawing.Point(291, 230);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.Size = new System.Drawing.Size(128, 15);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.Text = "14. Điều kiện giao hàng:";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel15.Location = new System.Drawing.Point(57, 245);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.Size = new System.Drawing.Size(60, 15);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "Nộp thuế";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel14.Location = new System.Drawing.Point(8, 228);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.Size = new System.Drawing.Size(104, 15);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "Nội dung ủy quyền";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.Location = new System.Drawing.Point(0, 210);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.Size = new System.Drawing.Size(170, 15);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "4. Đại lý làm thủ tục Hải quan:";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine10
            // 
            this.xrLine10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine10.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine10.Location = new System.Drawing.Point(0, 200);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine10.Size = new System.Drawing.Size(288, 12);
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.Location = new System.Drawing.Point(0, 163);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.Size = new System.Drawing.Size(98, 15);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.Text = "3. Người ủy thác:";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine5
            // 
            this.xrLine5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine5.Location = new System.Drawing.Point(0, 155);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine5.Size = new System.Drawing.Size(288, 12);
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel30.Location = new System.Drawing.Point(607, 190);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.Size = new System.Drawing.Size(107, 15);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.Text = "13. Nước xuất khẩu:";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel29.Location = new System.Drawing.Point(291, 190);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.Size = new System.Drawing.Size(125, 15);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.Text = "12. Phương tiện vận tải:";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine15
            // 
            this.xrLine15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine15.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine15.Location = new System.Drawing.Point(291, 185);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine15.Size = new System.Drawing.Size(508, 8);
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel26.Location = new System.Drawing.Point(291, 170);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.Size = new System.Drawing.Size(32, 15);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.Text = "Ngày";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel28.Location = new System.Drawing.Point(600, 137);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.Size = new System.Drawing.Size(113, 15);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.Text = "11. Cảng dỡ hàng:";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel27.Location = new System.Drawing.Point(441, 137);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.Size = new System.Drawing.Size(102, 15);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.Text = "10. Cảng xếp hàng:";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel25.Location = new System.Drawing.Point(291, 137);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.Size = new System.Drawing.Size(80, 15);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.Text = "9. Vận tải đơn:";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine14
            // 
            this.xrLine14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine14.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine14.Location = new System.Drawing.Point(291, 130);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine14.Size = new System.Drawing.Size(508, 8);
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel18.Location = new System.Drawing.Point(290, 115);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.Size = new System.Drawing.Size(32, 17);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.Text = "Ngày";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel19.Location = new System.Drawing.Point(441, 80);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.Size = new System.Drawing.Size(72, 15);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.Text = "7. Giấy phép:";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine13
            // 
            this.xrLine13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine13.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine13.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine13.Location = new System.Drawing.Point(593, 80);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine13.Size = new System.Drawing.Size(8, 150);
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel22.Location = new System.Drawing.Point(605, 115);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.Size = new System.Drawing.Size(72, 15);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.Text = "Ngày hết hạn";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel23.Location = new System.Drawing.Point(604, 96);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.Size = new System.Drawing.Size(32, 17);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.Text = "Ngày";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel24.Location = new System.Drawing.Point(602, 80);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.Size = new System.Drawing.Size(70, 15);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.Text = "8. Hợp đồng:";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel21.Location = new System.Drawing.Point(441, 115);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.Size = new System.Drawing.Size(72, 17);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.Text = "Ngày hết hạn";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel20.Location = new System.Drawing.Point(441, 96);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.Size = new System.Drawing.Size(32, 17);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.Text = "Ngày";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine11
            // 
            this.xrLine11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine11.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine11.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine11.Location = new System.Drawing.Point(432, 78);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine11.Size = new System.Drawing.Size(8, 117);
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.Location = new System.Drawing.Point(0, 111);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.Size = new System.Drawing.Size(117, 15);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.Text = "2. Người nhập khẩu:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine4.Location = new System.Drawing.Point(0, 104);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine4.Size = new System.Drawing.Size(288, 12);
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel17.Location = new System.Drawing.Point(290, 80);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.Size = new System.Drawing.Size(125, 17);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.Text = "6. Hóa đơn thương mại:";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine12
            // 
            this.xrLine12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine12.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine12.Location = new System.Drawing.Point(294, 75);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine12.Size = new System.Drawing.Size(506, 8);
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel16.Location = new System.Drawing.Point(292, 55);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.Size = new System.Drawing.Size(77, 18);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "5. Loại hình:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.Location = new System.Drawing.Point(0, 55);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.Size = new System.Drawing.Size(123, 18);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.Text = "1. Người xuất khẩu:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine3.Location = new System.Drawing.Point(0, 46);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine3.Size = new System.Drawing.Size(800, 6);
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.Location = new System.Drawing.Point(541, 27);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.Size = new System.Drawing.Size(117, 18);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = "Ngày, giờ đăng ký:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.Location = new System.Drawing.Point(541, 10);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.Size = new System.Drawing.Size(75, 17);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Số tờ khai:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine2
            // 
            this.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine2.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine2.Location = new System.Drawing.Point(525, 1);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine2.Size = new System.Drawing.Size(9, 54);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.Location = new System.Drawing.Point(292, 27);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.Size = new System.Drawing.Size(91, 18);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Ngày, giờ gửi:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.Location = new System.Drawing.Point(292, 8);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.Size = new System.Drawing.Size(100, 17);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "Số tham chiếu:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine1.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine1.Location = new System.Drawing.Point(282, 2);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLine1.Size = new System.Drawing.Size(10, 295);
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.Location = new System.Drawing.Point(0, 25);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.Size = new System.Drawing.Size(155, 17);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "Chi cục Hải quan cửa khẩu:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.Location = new System.Drawing.Point(0, 4);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.Size = new System.Drawing.Size(117, 17);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Chi cục Hải quan:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable16.Location = new System.Drawing.Point(0, 350);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable16.Size = new System.Drawing.Size(773, 34);
            this.xrTable16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell88,
            this.xrTableCell80,
            this.xrTableCell89,
            this.xrTableCell81,
            this.xrTableCell83,
            this.xrTableCell91,
            this.xrTableCell90,
            this.xrTableCell92});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow20.Weight = 0.82926829268292679;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell88.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Text = "SỐ TT";
            this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell88.Weight = 0.03259452411994785;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell80.Multiline = true;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Text = "20. TÊN HÀNG \r\n QUY CÁCH PHẨM CHẤT";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell80.Weight = 0.36395119246064528;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell89.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Text = "21. MÃ SỐ HÀNG HÓA";
            this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell89.Weight = 0.10700819075444258;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Text = "22. XUẤT XỨ";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell81.Weight = 0.087304248820953417;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell83.Multiline = true;
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.Text = "23.LƯỢNG\r\n      ";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell83.Weight = 0.087353324641460228;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell91.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Text = "24. ĐƠN VỊ TÍNH";
            this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell91.Weight = 0.087353324641460228;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell90.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.Text = "25. ĐƠN GIÁ NGUYÊN TỆ";
            this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell90.Weight = 0.093010130885213288;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell92.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Text = "26. TRỊ GIÁ NGUYÊN TỆ ";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell92.Weight = 0.11131214021289092;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.Transparent;
            this.xrTable1.BorderColor = System.Drawing.Color.Black;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable1.Location = new System.Drawing.Point(0, 385);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3});
            this.xrTable1.Size = new System.Drawing.Size(773, 90);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.TenHang1,
            this.MaHS1,
            this.XuatXu1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 0.33333333333333331;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.BorderColor = System.Drawing.Color.Gray;
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell93.StylePriority.UseBorderColor = false;
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Text = "1";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell93.Weight = 0.03259452411994785;
            // 
            // TenHang1
            // 
            this.TenHang1.BackColor = System.Drawing.Color.Ivory;
            this.TenHang1.BorderColor = System.Drawing.Color.Gray;
            this.TenHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang1.CanGrow = false;
            this.TenHang1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang1.StylePriority.UseBackColor = false;
            this.TenHang1.StylePriority.UseBorderColor = false;
            this.TenHang1.StylePriority.UseBorders = false;
            this.TenHang1.Tag = "Tên hàng 1";
            this.TenHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang1.Weight = 0.36395119246064528;
            this.TenHang1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS1
            // 
            this.MaHS1.BorderColor = System.Drawing.Color.Gray;
            this.MaHS1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.StylePriority.UseBorderColor = false;
            this.MaHS1.StylePriority.UseBorders = false;
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS1.Weight = 0.10700819075444258;
            // 
            // XuatXu1
            // 
            this.XuatXu1.BorderColor = System.Drawing.Color.Gray;
            this.XuatXu1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.XuatXu1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.XuatXu1.Name = "XuatXu1";
            this.XuatXu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu1.StylePriority.UseBorderColor = false;
            this.XuatXu1.StylePriority.UseBorders = false;
            this.XuatXu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu1.Weight = 0.087304248820953417;
            // 
            // Luong1
            // 
            this.Luong1.BorderColor = System.Drawing.Color.Gray;
            this.Luong1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.StylePriority.UseBorderColor = false;
            this.Luong1.StylePriority.UseBorders = false;
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong1.Weight = 0.087353324641460228;
            // 
            // DVT1
            // 
            this.DVT1.BorderColor = System.Drawing.Color.Gray;
            this.DVT1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.StylePriority.UseBorderColor = false;
            this.DVT1.StylePriority.UseBorders = false;
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT1.Weight = 0.087353324641460228;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.BorderColor = System.Drawing.Color.Gray;
            this.DonGiaNT1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.StylePriority.UseBorderColor = false;
            this.DonGiaNT1.StylePriority.UseBorders = false;
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT1.Weight = 0.093010130885213288;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.BackColor = System.Drawing.Color.Ivory;
            this.TriGiaNT1.BorderColor = System.Drawing.Color.Gray;
            this.TriGiaNT1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.TriGiaNT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.StylePriority.UseBorderColor = false;
            this.TriGiaNT1.StylePriority.UseBorders = false;
            this.TriGiaNT1.Tag = "Trị giá NT 1";
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT1.Weight = 0.11131214021289092;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.TenHang2,
            this.MaHS2,
            this.XuatXu2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 0.33333333333333331;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.BorderColor = System.Drawing.Color.Gray;
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell94.StylePriority.UseBorderColor = false;
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.Text = "2";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell94.Weight = 0.03259452411994785;
            // 
            // TenHang2
            // 
            this.TenHang2.BackColor = System.Drawing.Color.Ivory;
            this.TenHang2.BorderColor = System.Drawing.Color.Gray;
            this.TenHang2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang2.CanGrow = false;
            this.TenHang2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang2.StylePriority.UseBorderColor = false;
            this.TenHang2.StylePriority.UseBorders = false;
            this.TenHang2.Tag = "Tên hàng 2";
            this.TenHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang2.Weight = 0.36395119246064528;
            this.TenHang2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang2_PreviewClick);
            // 
            // MaHS2
            // 
            this.MaHS2.BorderColor = System.Drawing.Color.Gray;
            this.MaHS2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.StylePriority.UseBorderColor = false;
            this.MaHS2.StylePriority.UseBorders = false;
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS2.Weight = 0.10700819075444258;
            // 
            // XuatXu2
            // 
            this.XuatXu2.BorderColor = System.Drawing.Color.Gray;
            this.XuatXu2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.XuatXu2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.XuatXu2.Name = "XuatXu2";
            this.XuatXu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu2.StylePriority.UseBorderColor = false;
            this.XuatXu2.StylePriority.UseBorders = false;
            this.XuatXu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu2.Weight = 0.087304248820953417;
            // 
            // Luong2
            // 
            this.Luong2.BorderColor = System.Drawing.Color.Gray;
            this.Luong2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.StylePriority.UseBorderColor = false;
            this.Luong2.StylePriority.UseBorders = false;
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong2.Weight = 0.087353324641460228;
            // 
            // DVT2
            // 
            this.DVT2.BorderColor = System.Drawing.Color.Gray;
            this.DVT2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.StylePriority.UseBorderColor = false;
            this.DVT2.StylePriority.UseBorders = false;
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT2.Weight = 0.087353324641460228;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.BorderColor = System.Drawing.Color.Gray;
            this.DonGiaNT2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.StylePriority.UseBorderColor = false;
            this.DonGiaNT2.StylePriority.UseBorders = false;
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT2.Weight = 0.093010130885213288;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.BackColor = System.Drawing.Color.Ivory;
            this.TriGiaNT2.BorderColor = System.Drawing.Color.Gray;
            this.TriGiaNT2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.TriGiaNT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.StylePriority.UseBorderColor = false;
            this.TriGiaNT2.StylePriority.UseBorders = false;
            this.TriGiaNT2.Tag = "Trị giá NT 2";
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT2.Weight = 0.11131214021289092;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell95,
            this.TenHang3,
            this.MaHS3,
            this.XuatXu3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 0.33333333333333331;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.BorderColor = System.Drawing.Color.Gray;
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell95.StylePriority.UseBorderColor = false;
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.Text = "3";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell95.Weight = 0.03259452411994785;
            // 
            // TenHang3
            // 
            this.TenHang3.BackColor = System.Drawing.Color.Ivory;
            this.TenHang3.BorderColor = System.Drawing.Color.Gray;
            this.TenHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang3.CanGrow = false;
            this.TenHang3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang3.StylePriority.UseBorderColor = false;
            this.TenHang3.StylePriority.UseBorders = false;
            this.TenHang3.Tag = "Tên hàng 3";
            this.TenHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang3.Weight = 0.36395119246064528;
            this.TenHang3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang3_PreviewClick);
            // 
            // MaHS3
            // 
            this.MaHS3.BorderColor = System.Drawing.Color.Gray;
            this.MaHS3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.StylePriority.UseBorderColor = false;
            this.MaHS3.StylePriority.UseBorders = false;
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.MaHS3.Weight = 0.10700819075444258;
            // 
            // XuatXu3
            // 
            this.XuatXu3.BorderColor = System.Drawing.Color.Gray;
            this.XuatXu3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.XuatXu3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.XuatXu3.Name = "XuatXu3";
            this.XuatXu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu3.StylePriority.UseBorderColor = false;
            this.XuatXu3.StylePriority.UseBorders = false;
            this.XuatXu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XuatXu3.Weight = 0.087304248820953417;
            // 
            // Luong3
            // 
            this.Luong3.BorderColor = System.Drawing.Color.Gray;
            this.Luong3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.StylePriority.UseBorderColor = false;
            this.Luong3.StylePriority.UseBorders = false;
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.Luong3.Weight = 0.087353324641460228;
            // 
            // DVT3
            // 
            this.DVT3.BorderColor = System.Drawing.Color.Gray;
            this.DVT3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.StylePriority.UseBorderColor = false;
            this.DVT3.StylePriority.UseBorders = false;
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DVT3.Weight = 0.087353324641460228;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.BorderColor = System.Drawing.Color.Gray;
            this.DonGiaNT3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.StylePriority.UseBorderColor = false;
            this.DonGiaNT3.StylePriority.UseBorders = false;
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.DonGiaNT3.Weight = 0.093010130885213288;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.BackColor = System.Drawing.Color.Ivory;
            this.TriGiaNT3.BorderColor = System.Drawing.Color.Gray;
            this.TriGiaNT3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.TriGiaNT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.StylePriority.UseBorderColor = false;
            this.TriGiaNT3.StylePriority.UseBorders = false;
            this.TriGiaNT3.Tag = "Trị giá NT 3";
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT3.Weight = 0.11131214021289092;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable3.Location = new System.Drawing.Point(0, 475);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.Size = new System.Drawing.Size(773, 31);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTrongLuong,
            this.lblPhiBaoHiem,
            this.xrTableCell3,
            this.lblTongTriGiaNT});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // lblTrongLuong
            // 
            this.lblTrongLuong.BorderColor = System.Drawing.Color.Gray;
            this.lblTrongLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTrongLuong.CanGrow = false;
            this.lblTrongLuong.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTrongLuong.Name = "lblTrongLuong";
            this.lblTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.lblTrongLuong.StylePriority.UseBorderColor = false;
            this.lblTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTrongLuong.Weight = 0.37434708710467379;
            // 
            // lblPhiBaoHiem
            // 
            this.lblPhiBaoHiem.BorderColor = System.Drawing.Color.Gray;
            this.lblPhiBaoHiem.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblPhiBaoHiem.Name = "lblPhiBaoHiem";
            this.lblPhiBaoHiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.lblPhiBaoHiem.StylePriority.UseBorderColor = false;
            this.lblPhiBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPhiBaoHiem.Weight = 0.3912177183342358;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BorderColor = System.Drawing.Color.Gray;
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100F);
            this.xrTableCell3.StylePriority.UseBorderColor = false;
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.Text = "Cộng:     ";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell3.Weight = 0.094212603718068513;
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.BackColor = System.Drawing.Color.Ivory;
            this.lblTongTriGiaNT.BorderColor = System.Drawing.Color.Gray;
            this.lblTongTriGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTriGiaNT.CanGrow = false;
            this.lblTongTriGiaNT.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.StylePriority.UseBorderColor = false;
            this.lblTongTriGiaNT.StylePriority.UseBorders = false;
            this.lblTongTriGiaNT.Tag = "Tổng trị giá NT";
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTriGiaNT.Weight = 0.11010966738003572;
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable19.Location = new System.Drawing.Point(28, 505);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable19.Size = new System.Drawing.Size(758, 25);
            this.xrTable19.StylePriority.UseBorders = false;
            this.xrTable19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell107,
            this.xrTableCell110});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow23.Weight = 1.1;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell105.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell105.Multiline = true;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.Text = "27.THUẾ NHẬP KHẨU";
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell105.Weight = 0.37881429512906967;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.Text = "28. THUẾ GTGT(HOẶC TTĐB)";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell107.Weight = 0.37498735978074021;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.Text = "29.THU KHÁC";
            this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell110.Weight = 0.22679342917815909;
            // 
            // xrTable2
            // 
            this.xrTable2.BorderColor = System.Drawing.Color.DarkGray;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable2.Location = new System.Drawing.Point(1, 570);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12});
            this.xrTable2.Size = new System.Drawing.Size(773, 90);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.TriGiaTT1,
            this.ThueSuatXNK1,
            this.TienThueXNK1,
            this.TriGiaTTGTGT1,
            this.ThueSuatGTGT1,
            this.TienThueGTGT1,
            this.TyLeThuKhac1,
            this.TriGiaThuKhac1});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 0.33333333333333331;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.Text = "1";
            this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell123.Weight = 0.03259452411994785;
            // 
            // TriGiaTT1
            // 
            this.TriGiaTT1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaTT1.CanGrow = false;
            this.TriGiaTT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTT1.Name = "TriGiaTT1";
            this.TriGiaTT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT1.StylePriority.UseBorders = false;
            this.TriGiaTT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT1.Weight = 0.15645371577574968;
            this.TriGiaTT1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TriGiaTT1_PreviewClick);
            // 
            // ThueSuatXNK1
            // 
            this.ThueSuatXNK1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ThueSuatXNK1.CanGrow = false;
            this.ThueSuatXNK1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatXNK1.Name = "ThueSuatXNK1";
            this.ThueSuatXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK1.StylePriority.UseBorders = false;
            this.ThueSuatXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK1.Weight = 0.0560625814863103;
            // 
            // TienThueXNK1
            // 
            this.TienThueXNK1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TienThueXNK1.CanGrow = false;
            this.TienThueXNK1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueXNK1.Name = "TienThueXNK1";
            this.TienThueXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK1.StylePriority.UseBorders = false;
            this.TienThueXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK1.Weight = 0.15645371577574968;
            // 
            // TriGiaTTGTGT1
            // 
            this.TriGiaTTGTGT1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaTTGTGT1.CanGrow = false;
            this.TriGiaTTGTGT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTTGTGT1.Name = "TriGiaTTGTGT1";
            this.TriGiaTTGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT1.StylePriority.UseBorders = false;
            this.TriGiaTTGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT1.Weight = 0.15645371577574968;
            // 
            // ThueSuatGTGT1
            // 
            this.ThueSuatGTGT1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ThueSuatGTGT1.CanGrow = false;
            this.ThueSuatGTGT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatGTGT1.Name = "ThueSuatGTGT1";
            this.ThueSuatGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT1.StylePriority.UseBorders = false;
            this.ThueSuatGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT1.Weight = 0.054758800521512385;
            // 
            // TienThueGTGT1
            // 
            this.TienThueGTGT1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TienThueGTGT1.CanGrow = false;
            this.TienThueGTGT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueGTGT1.Name = "TienThueGTGT1";
            this.TienThueGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT1.StylePriority.UseBorders = false;
            this.TienThueGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT1.Weight = 0.15148397101909214;
            // 
            // TyLeThuKhac1
            // 
            this.TyLeThuKhac1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TyLeThuKhac1.CanGrow = false;
            this.TyLeThuKhac1.Name = "TyLeThuKhac1";
            this.TyLeThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac1.StylePriority.UseBorders = false;
            this.TyLeThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac1.Weight = 0.05867014341590613;
            // 
            // TriGiaThuKhac1
            // 
            this.TriGiaThuKhac1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaThuKhac1.CanGrow = false;
            this.TriGiaThuKhac1.Name = "TriGiaThuKhac1";
            this.TriGiaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac1.StylePriority.UseBorders = false;
            this.TriGiaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac1.Weight = 0.14695590864699598;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell124,
            this.TriGiaTT2,
            this.ThueSuatXNK2,
            this.TienThueXNK2,
            this.TriGiaTTGTGT2,
            this.ThueSuatGTGT2,
            this.TienThueGTGT2,
            this.TyLeThuKhac2,
            this.TriGiaThuKhac2});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow11.Weight = 0.33333333333333331;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Text = "2";
            this.xrTableCell124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell124.Weight = 0.03259452411994785;
            // 
            // TriGiaTT2
            // 
            this.TriGiaTT2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaTT2.CanGrow = false;
            this.TriGiaTT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTT2.Name = "TriGiaTT2";
            this.TriGiaTT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT2.StylePriority.UseBorders = false;
            this.TriGiaTT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT2.Weight = 0.15645371577574968;
            // 
            // ThueSuatXNK2
            // 
            this.ThueSuatXNK2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ThueSuatXNK2.CanGrow = false;
            this.ThueSuatXNK2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatXNK2.Name = "ThueSuatXNK2";
            this.ThueSuatXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK2.StylePriority.UseBorders = false;
            this.ThueSuatXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK2.Weight = 0.0560625814863103;
            // 
            // TienThueXNK2
            // 
            this.TienThueXNK2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TienThueXNK2.CanGrow = false;
            this.TienThueXNK2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueXNK2.Name = "TienThueXNK2";
            this.TienThueXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK2.StylePriority.UseBorders = false;
            this.TienThueXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK2.Weight = 0.15645371577574968;
            // 
            // TriGiaTTGTGT2
            // 
            this.TriGiaTTGTGT2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaTTGTGT2.CanGrow = false;
            this.TriGiaTTGTGT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTTGTGT2.Name = "TriGiaTTGTGT2";
            this.TriGiaTTGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT2.StylePriority.UseBorders = false;
            this.TriGiaTTGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT2.Weight = 0.15645371577574968;
            // 
            // ThueSuatGTGT2
            // 
            this.ThueSuatGTGT2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ThueSuatGTGT2.CanGrow = false;
            this.ThueSuatGTGT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatGTGT2.Name = "ThueSuatGTGT2";
            this.ThueSuatGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT2.StylePriority.UseBorders = false;
            this.ThueSuatGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT2.Weight = 0.054758800521512385;
            // 
            // TienThueGTGT2
            // 
            this.TienThueGTGT2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TienThueGTGT2.CanGrow = false;
            this.TienThueGTGT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueGTGT2.Name = "TienThueGTGT2";
            this.TienThueGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT2.StylePriority.UseBorders = false;
            this.TienThueGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT2.Weight = 0.15148397101909214;
            // 
            // TyLeThuKhac2
            // 
            this.TyLeThuKhac2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TyLeThuKhac2.CanGrow = false;
            this.TyLeThuKhac2.Name = "TyLeThuKhac2";
            this.TyLeThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac2.StylePriority.UseBorders = false;
            this.TyLeThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac2.Weight = 0.05867014341590613;
            // 
            // TriGiaThuKhac2
            // 
            this.TriGiaThuKhac2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaThuKhac2.CanGrow = false;
            this.TriGiaThuKhac2.Name = "TriGiaThuKhac2";
            this.TriGiaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac2.StylePriority.UseBorders = false;
            this.TriGiaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac2.Weight = 0.14695590864699598;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.TriGiaTT3,
            this.ThueSuatXNK3,
            this.TienThueXNK3,
            this.TriGiaTTGTGT3,
            this.ThueSuatGTGT3,
            this.TienThueGTGT3,
            this.TyLeThuKhac3,
            this.TriGiaThuKhac3});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow12.Weight = 0.33333333333333331;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell125.StylePriority.UseBorders = false;
            this.xrTableCell125.Text = "3";
            this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell125.Weight = 0.03259452411994785;
            // 
            // TriGiaTT3
            // 
            this.TriGiaTT3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaTT3.CanGrow = false;
            this.TriGiaTT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTT3.Name = "TriGiaTT3";
            this.TriGiaTT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT3.StylePriority.UseBorders = false;
            this.TriGiaTT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTT3.Weight = 0.15645371577574968;
            // 
            // ThueSuatXNK3
            // 
            this.ThueSuatXNK3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ThueSuatXNK3.CanGrow = false;
            this.ThueSuatXNK3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatXNK3.Name = "ThueSuatXNK3";
            this.ThueSuatXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK3.StylePriority.UseBorders = false;
            this.ThueSuatXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatXNK3.Weight = 0.0560625814863103;
            // 
            // TienThueXNK3
            // 
            this.TienThueXNK3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TienThueXNK3.CanGrow = false;
            this.TienThueXNK3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueXNK3.Name = "TienThueXNK3";
            this.TienThueXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK3.StylePriority.UseBorders = false;
            this.TienThueXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueXNK3.Weight = 0.15645371577574968;
            // 
            // TriGiaTTGTGT3
            // 
            this.TriGiaTTGTGT3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaTTGTGT3.CanGrow = false;
            this.TriGiaTTGTGT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTTGTGT3.Name = "TriGiaTTGTGT3";
            this.TriGiaTTGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT3.StylePriority.UseBorders = false;
            this.TriGiaTTGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaTTGTGT3.Weight = 0.15645371577574968;
            // 
            // ThueSuatGTGT3
            // 
            this.ThueSuatGTGT3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ThueSuatGTGT3.CanGrow = false;
            this.ThueSuatGTGT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatGTGT3.Name = "ThueSuatGTGT3";
            this.ThueSuatGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT3.StylePriority.UseBorders = false;
            this.ThueSuatGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.ThueSuatGTGT3.Weight = 0.054758800521512385;
            // 
            // TienThueGTGT3
            // 
            this.TienThueGTGT3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TienThueGTGT3.CanGrow = false;
            this.TienThueGTGT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueGTGT3.Name = "TienThueGTGT3";
            this.TienThueGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT3.StylePriority.UseBorders = false;
            this.TienThueGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TienThueGTGT3.Weight = 0.15148397101909214;
            // 
            // TyLeThuKhac3
            // 
            this.TyLeThuKhac3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TyLeThuKhac3.CanGrow = false;
            this.TyLeThuKhac3.Name = "TyLeThuKhac3";
            this.TyLeThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac3.StylePriority.UseBorders = false;
            this.TyLeThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TyLeThuKhac3.Weight = 0.05867014341590613;
            // 
            // TriGiaThuKhac3
            // 
            this.TriGiaThuKhac3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaThuKhac3.CanGrow = false;
            this.TriGiaThuKhac3.Name = "TriGiaThuKhac3";
            this.TriGiaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac3.StylePriority.UseBorders = false;
            this.TriGiaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaThuKhac3.Weight = 0.14695590864699598;
            // 
            // xrTable23
            // 
            this.xrTable23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable23.Location = new System.Drawing.Point(0, 690);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable23.Size = new System.Drawing.Size(799, 30);
            this.xrTable23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow27.Weight = 1.3199999999999998;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell133.CanGrow = false;
            this.xrTableCell133.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel56,
            this.lblTongThueXNKSo});
            this.xrTableCell133.Multiline = true;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell133.Text = "27. Tổng số tiền thuế và thu khác (ô 24+25+26) : Bằng số:........................" +
                "................................................................................" +
                ".........................\r\n";
            this.xrTableCell133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell133.Weight = 1;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel56.Location = new System.Drawing.Point(4, 6);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.Size = new System.Drawing.Size(638, 17);
            this.xrLabel56.StylePriority.UseBorders = false;
            this.xrLabel56.StylePriority.UseTextAlignment = false;
            this.xrLabel56.Text = "30. Tổng số tiền thuế và thu khác (ô 27+28+29) : Bằng số: ";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongThueXNKSo
            // 
            this.lblTongThueXNKSo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTongThueXNKSo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblTongThueXNKSo.Location = new System.Drawing.Point(349, 5);
            this.lblTongThueXNKSo.Name = "lblTongThueXNKSo";
            this.lblTongThueXNKSo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNKSo.Size = new System.Drawing.Size(309, 17);
            this.lblTongThueXNKSo.StylePriority.UseBorders = false;
            this.lblTongThueXNKSo.StylePriority.UseFont = false;
            this.lblTongThueXNKSo.StylePriority.UseTextAlignment = false;
            this.lblTongThueXNKSo.Text = " ";
            this.lblTongThueXNKSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable24
            // 
            this.xrTable24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable24.Location = new System.Drawing.Point(0, 715);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable24.Size = new System.Drawing.Size(797, 23);
            this.xrTable24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow28.Weight = 1;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell129.CanGrow = false;
            this.xrTableCell129.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel44,
            this.lblTongThueXNKChu});
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.Text = " ";
            this.xrTableCell129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell129.Weight = 1.0365058670143417;
            // 
            // xrLabel44
            // 
            this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel44.Location = new System.Drawing.Point(16, 1);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.Size = new System.Drawing.Size(743, 25);
            this.xrLabel44.StylePriority.UseBorders = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "Bằng chữ : ";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongThueXNKChu
            // 
            this.lblTongThueXNKChu.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTongThueXNKChu.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.lblTongThueXNKChu.Location = new System.Drawing.Point(91, 1);
            this.lblTongThueXNKChu.Name = "lblTongThueXNKChu";
            this.lblTongThueXNKChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNKChu.Size = new System.Drawing.Size(692, 17);
            this.lblTongThueXNKChu.StylePriority.UseBorders = false;
            this.lblTongThueXNKChu.StylePriority.UseFont = false;
            this.lblTongThueXNKChu.StylePriority.UseTextAlignment = false;
            this.lblTongThueXNKChu.Text = " ";
            this.lblTongThueXNKChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable5.Location = new System.Drawing.Point(0, 787);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable5.Size = new System.Drawing.Size(785, 50);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDeXuatKhac});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // lblDeXuatKhac
            // 
            this.lblDeXuatKhac.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDeXuatKhac.CanGrow = false;
            this.lblDeXuatKhac.Name = "lblDeXuatKhac";
            this.lblDeXuatKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.lblDeXuatKhac.StylePriority.UseBorders = false;
            this.lblDeXuatKhac.StylePriority.UseTextAlignment = false;
            this.lblDeXuatKhac.Text = "32. Ghi chép khác";
            this.lblDeXuatKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblDeXuatKhac.Weight = 1.0234680573663624;
            this.lblDeXuatKhac.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblDeXuatKhac_PreviewClick);
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable6.Location = new System.Drawing.Point(0, 740);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable6.Size = new System.Drawing.Size(797, 43);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1.2142857142857142;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.CanGrow = false;
            this.xrTableCell4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel45,
            this.lbltongtrongluong,
            this.xrLabel47,
            this.lblChitietCon,
            this.xrLabel51,
            this.xrLabel52,
            this.lblCon20,
            this.lblCon40,
            this.xrLabel41,
            this.xrLabel40,
            this.lblTongsoKien,
            this.xrLabel55});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Text = " ";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell4.Weight = 1.0391134289439374;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel45.Location = new System.Drawing.Point(16, 2);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.Size = new System.Drawing.Size(144, 17);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "31. Tổng trọng lượng :";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbltongtrongluong
            // 
            this.lbltongtrongluong.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lbltongtrongluong.Location = new System.Drawing.Point(160, 2);
            this.lbltongtrongluong.Name = "lbltongtrongluong";
            this.lbltongtrongluong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbltongtrongluong.Size = new System.Drawing.Size(130, 17);
            this.lbltongtrongluong.StylePriority.UseBorders = false;
            this.lbltongtrongluong.StylePriority.UseTextAlignment = false;
            this.lbltongtrongluong.Text = " ";
            this.lbltongtrongluong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.Location = new System.Drawing.Point(142, 21);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.Size = new System.Drawing.Size(70, 17);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "Container :";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblChitietCon
            // 
            this.lblChitietCon.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblChitietCon.Location = new System.Drawing.Point(214, 22);
            this.lblChitietCon.Name = "lblChitietCon";
            this.lblChitietCon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChitietCon.Size = new System.Drawing.Size(559, 17);
            this.lblChitietCon.StylePriority.UseBorders = false;
            this.lblChitietCon.StylePriority.UseTextAlignment = false;
            this.lblChitietCon.Text = " ";
            this.lblChitietCon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.Location = new System.Drawing.Point(424, 2);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.Size = new System.Drawing.Size(52, 17);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "Cont20 :";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel52.Location = new System.Drawing.Point(292, 3);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.Size = new System.Drawing.Size(125, 17);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "Tổng số container :";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblCon20
            // 
            this.lblCon20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblCon20.Location = new System.Drawing.Point(477, 2);
            this.lblCon20.Name = "lblCon20";
            this.lblCon20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCon20.Size = new System.Drawing.Size(40, 17);
            this.lblCon20.StylePriority.UseBorders = false;
            this.lblCon20.StylePriority.UseTextAlignment = false;
            this.lblCon20.Text = " ";
            this.lblCon20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblCon40
            // 
            this.lblCon40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblCon40.Location = new System.Drawing.Point(573, 1);
            this.lblCon40.Name = "lblCon40";
            this.lblCon40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCon40.Size = new System.Drawing.Size(40, 17);
            this.lblCon40.StylePriority.UseBorders = false;
            this.lblCon40.StylePriority.UseTextAlignment = false;
            this.lblCon40.Text = " ";
            this.lblCon40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.Location = new System.Drawing.Point(520, 2);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.Size = new System.Drawing.Size(52, 17);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.StylePriority.UseTextAlignment = false;
            this.xrLabel41.Text = "Cont40 :";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel40.Location = new System.Drawing.Point(16, 22);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.Size = new System.Drawing.Size(118, 17);
            this.xrLabel40.StylePriority.UseBorders = false;
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "Số hiệu kiện,cont :";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongsoKien
            // 
            this.lblTongsoKien.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTongsoKien.Location = new System.Drawing.Point(709, 0);
            this.lblTongsoKien.Name = "lblTongsoKien";
            this.lblTongsoKien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongsoKien.Size = new System.Drawing.Size(78, 17);
            this.lblTongsoKien.StylePriority.UseBorders = false;
            this.lblTongsoKien.StylePriority.UseTextAlignment = false;
            this.lblTongsoKien.Text = " ";
            this.lblTongsoKien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel55.Location = new System.Drawing.Point(616, 1);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.Size = new System.Drawing.Size(92, 17);
            this.xrLabel55.StylePriority.UseBorders = false;
            this.xrLabel55.StylePriority.UseFont = false;
            this.xrLabel55.StylePriority.UseTextAlignment = false;
            this.xrLabel55.Text = "Tổng số kiện :";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable7.BorderWidth = 0;
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable7.Location = new System.Drawing.Point(0, 836);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable7.Size = new System.Drawing.Size(776, 165);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseBorderWidth = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell6,
            this.xrTableCell8,
            this.xrTableCell5});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1.6708860759493671;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell7.BorderWidth = 1;
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel37,
            this.lblNgayIn,
            this.xrLabel39});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseBorderWidth = false;
            this.xrTableCell7.Text = "Tôi xin cam đoan , chịu trách nhiệm trước pháp luật về những nội dung khai báo tr" +
                "ên tờ khai này.";
            this.xrTableCell7.Weight = 0.426422453766843;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel37.Location = new System.Drawing.Point(24, 0);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.Size = new System.Drawing.Size(308, 34);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "33. Tôi xin cam đoan , chịu trách nhiệm trước pháp luật về những nội dung khai bá" +
                "o trên tờ khai này.";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayIn
            // 
            this.lblNgayIn.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayIn.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayIn.Location = new System.Drawing.Point(14, 34);
            this.lblNgayIn.Name = "lblNgayIn";
            this.lblNgayIn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayIn.Size = new System.Drawing.Size(308, 15);
            this.lblNgayIn.StylePriority.UseFont = false;
            this.lblNgayIn.StylePriority.UseTextAlignment = false;
            this.lblNgayIn.Text = "Ngày 3 tháng 3 năm 2010";
            this.lblNgayIn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel39.Location = new System.Drawing.Point(8, 140);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.Size = new System.Drawing.Size(325, 15);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "Người khai báo ghi rõ họ tên, chức danh,ký tên, đóng dấu";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell6.BorderWidth = 1;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseBorderWidth = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "34. Xác nhận hàng đã qua khu vực giám sát";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell6.Weight = 0.17633661396477196;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell8.BorderWidth = 1;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseBorderWidth = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "35. Xác nhận giải phóng hàng/đưa hàng về bảo quản/chuyển cửa khẩu";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell8.Weight = 0.18130730007348528;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell5.BorderWidth = 1;
            this.xrTableCell5.CanGrow = false;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseBorderWidth = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "36. Xác nhận thông quan";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell5.Weight = 0.18958482416478678;
            // 
            // lblMaCangdoHang
            // 
            this.lblMaCangdoHang.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaCangdoHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblMaCangdoHang.Location = new System.Drawing.Point(712, 137);
            this.lblMaCangdoHang.Name = "lblMaCangdoHang";
            this.lblMaCangdoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaCangdoHang.Size = new System.Drawing.Size(75, 15);
            this.lblMaCangdoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(120, 111);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(160, 15);
            this.lblMaDoanhNghiep.Text = "lai ";
            this.lblMaDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable20
            // 
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable20.Location = new System.Drawing.Point(28, 530);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable20.Size = new System.Drawing.Size(746, 40);
            this.xrTable20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.xrTableCell111,
            this.xrTableCell109,
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell106,
            this.xrTableCell108,
            this.xrTableCell114});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow24.Weight = 1;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell104.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell104.Multiline = true;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell104.StylePriority.UseBorders = false;
            this.xrTableCell104.Text = "Trị giá tính thuế";
            this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell104.Weight = 0.16031982369945219;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell111.Text = "Thuế suất (%)";
            this.xrTableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell111.Weight = 0.059241656583549226;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell109.Text = "Tiền thuế";
            this.xrTableCell109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell109.Weight = 0.16043489328167987;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell112.Text = "Trị giá tính thuế";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell112.Weight = 0.1617826021765586;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell113.Text = "Thuế suất (%)";
            this.xrTableCell113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell113.Weight = 0.056603773584905662;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell106.Text = "Tiền thuế";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell106.Weight = 0.15662190576149912;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell108.Text = "Tỷ lệ(%)";
            this.xrTableCell108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell108.Weight = 0.060646900269541788;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Text = "Số tiền";
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell114.Weight = 0.14681832605140471;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel43.BorderWidth = 1;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.Location = new System.Drawing.Point(0, 505);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.Size = new System.Drawing.Size(28, 65);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.Text = "SỐ TT";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable22
            // 
            this.xrTable22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable22.Location = new System.Drawing.Point(0, 660);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable22.Size = new System.Drawing.Size(780, 28);
            this.xrTable22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.lblTongThueXNK,
            this.xrTableCell128,
            this.lblTongTienThueGTGT,
            this.lblTongTriGiaThuKhac});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow26.Weight = 0.93333333333333335;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.BorderColor = System.Drawing.SystemColors.GrayText;
            this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell127.CanGrow = false;
            this.xrTableCell127.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100F);
            this.xrTableCell127.StylePriority.UseBorderColor = false;
            this.xrTableCell127.StylePriority.UsePadding = false;
            this.xrTableCell127.Text = "Cộng :";
            this.xrTableCell127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell127.Weight = 0.24762023167059002;
            // 
            // lblTongThueXNK
            // 
            this.lblTongThueXNK.BorderColor = System.Drawing.SystemColors.GrayText;
            this.lblTongThueXNK.Name = "lblTongThueXNK";
            this.lblTongThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNK.StylePriority.UseBorderColor = false;
            this.lblTongThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongThueXNK.Weight = 0.15514993481095177;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.BorderColor = System.Drawing.SystemColors.GrayText;
            this.xrTableCell128.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell128.CanGrow = false;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell128.StylePriority.UseBorderColor = false;
            this.xrTableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell128.Weight = 0.21382007822685789;
            // 
            // lblTongTienThueGTGT
            // 
            this.lblTongTienThueGTGT.BorderColor = System.Drawing.SystemColors.GrayText;
            this.lblTongTienThueGTGT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTienThueGTGT.CanGrow = false;
            this.lblTongTienThueGTGT.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTienThueGTGT.Name = "lblTongTienThueGTGT";
            this.lblTongTienThueGTGT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100F);
            this.lblTongTienThueGTGT.StylePriority.UseBorderColor = false;
            this.lblTongTienThueGTGT.StylePriority.UseBorders = false;
            this.lblTongTienThueGTGT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTienThueGTGT.Weight = 0.15143489519858533;
            // 
            // lblTongTriGiaThuKhac
            // 
            this.lblTongTriGiaThuKhac.BorderColor = System.Drawing.SystemColors.GrayText;
            this.lblTongTriGiaThuKhac.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTriGiaThuKhac.CanGrow = false;
            this.lblTongTriGiaThuKhac.Name = "lblTongTriGiaThuKhac";
            this.lblTongTriGiaThuKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaThuKhac.StylePriority.UseBorderColor = false;
            this.lblTongTriGiaThuKhac.StylePriority.UseBorders = false;
            this.lblTongTriGiaThuKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTriGiaThuKhac.Weight = 0.21064487264006646;
            // 
            // lblTenPTVT
            // 
            this.lblTenPTVT.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTenPTVT.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTenPTVT.Location = new System.Drawing.Point(415, 190);
            this.lblTenPTVT.Name = "lblTenPTVT";
            this.lblTenPTVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenPTVT.Size = new System.Drawing.Size(133, 15);
            this.lblTenPTVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenNuoc
            // 
            this.lblTenNuoc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTenNuoc.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTenNuoc.Location = new System.Drawing.Point(608, 207);
            this.lblTenNuoc.Name = "lblTenNuoc";
            this.lblTenNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNuoc.Size = new System.Drawing.Size(167, 15);
            this.lblTenNuoc.StylePriority.UseTextAlignment = false;
            this.lblTenNuoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblLyDoTK
            // 
            this.lblLyDoTK.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblLyDoTK.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblLyDoTK.Location = new System.Drawing.Point(327, 290);
            this.lblLyDoTK.Name = "lblLyDoTK";
            this.lblLyDoTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLyDoTK.Size = new System.Drawing.Size(133, 15);
            this.lblLyDoTK.Text = " ";
            this.lblLyDoTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.Location = new System.Drawing.Point(282, 483);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.Size = new System.Drawing.Size(231, 17);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = " ";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.Location = new System.Drawing.Point(8, 55);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.Size = new System.Drawing.Size(442, 17);
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Nhập khẩu";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel3.Location = new System.Drawing.Point(583, 58);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Size = new System.Drawing.Size(200, 17);
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "HQ/2009-TKĐTNK";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.Location = new System.Drawing.Point(8, 27);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(442, 27);
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "TỜ KHAI HẢI QUAN ĐIỆN TỬ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.Location = new System.Drawing.Point(583, 33);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.Size = new System.Drawing.Size(208, 25);
            this.winControlContainer1.WinControl = this.label1;
            // 
            // label1
            // 
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = " ";
            // 
            // lblMienThueNK
            // 
            this.lblMienThueNK.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.lblMienThueNK.Location = new System.Drawing.Point(42, 695);
            this.lblMienThueNK.Name = "lblMienThueNK";
            this.lblMienThueNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMienThueNK.Size = new System.Drawing.Size(283, 83);
            this.lblMienThueNK.StylePriority.UseFont = false;
            this.lblMienThueNK.StylePriority.UseTextAlignment = false;
            this.lblMienThueNK.Text = "Hàng nhập khẩu thuộc đối tượng không chịu thuế theo quy định tại điều 2.3 nghị đị" +
                "nh 149/2005/NN-CP của Chính phủ, ngày 05/12/2005";
            this.lblMienThueNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMienThueGTGT
            // 
            this.lblMienThueGTGT.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.lblMienThueGTGT.Location = new System.Drawing.Point(342, 695);
            this.lblMienThueGTGT.Name = "lblMienThueGTGT";
            this.lblMienThueGTGT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMienThueGTGT.Size = new System.Drawing.Size(275, 83);
            this.lblMienThueGTGT.StylePriority.UseFont = false;
            this.lblMienThueGTGT.StylePriority.UseTextAlignment = false;
            this.lblMienThueGTGT.Text = "Hàng không chịu thuế GTGT theo điểm 20 mục II phần A thông tư 29/2008/TT-BTC ngày" +
                " 26/12/2008";
            this.lblMienThueGTGT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // TQDTToKhaiNK
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(7, 7, 4, 8);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRLine xrLine13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLine xrLine14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLine xrLine16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLine xrLine15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLine xrLine18;
        private DevExpress.XtraReports.UI.XRLine xrLine17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLine xrLine19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRShape xrShape1;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQCK;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQ;
        private DevExpress.XtraReports.UI.XRLabel lblThamChieu;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGui;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiNK;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiXK;
        private DevExpress.XtraReports.UI.XRLabel lblToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHetHanHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHetHanGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHoaDon;
        private DevExpress.XtraReports.UI.XRLabel lblHoaDonThuongMai;
        private DevExpress.XtraReports.UI.XRLabel lblLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel lblCangDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblCangXepHang;
        private DevExpress.XtraReports.UI.XRLabel lblNgayVanTaiDon;
        private DevExpress.XtraReports.UI.XRLabel lblVanTaiDon;
        private DevExpress.XtraReports.UI.XRLabel lblPhuongThucThanhToan;
        private DevExpress.XtraReports.UI.XRLabel lblDieuKienGiaoHang;
        private DevExpress.XtraReports.UI.XRLabel lblNuocXK;
        private DevExpress.XtraReports.UI.XRLabel lblPhuongTienVanTai;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaTinhThue;
        private DevExpress.XtraReports.UI.XRLabel lblDongTienThanhToan;
        private DevExpress.XtraReports.UI.XRLabel lblHuongDan;
        private DevExpress.XtraReports.UI.XRLabel lblChungTu;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell lblPhiBaoHiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaNT;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac3;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblDeXuatKhac;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel lblNgayIn;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel lblMaCangdoHang;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraReports.UI.XRLabel lblTongThueXNKChu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel lbltongtrongluong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel lblChitietCon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel lblCon20;
        private DevExpress.XtraReports.UI.XRLabel lblTongsoKien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel lblTongThueXNKSo;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThueGTGT;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaThuKhac;
        private DevExpress.XtraReports.UI.XRLabel lblCon40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel lblTenPTVT;
        private DevExpress.XtraReports.UI.XRLabel lblTenNuoc;
        private DevExpress.XtraReports.UI.XRLabel lblLyDoTK;
        private DevExpress.XtraReports.UI.XRLabel lblMienThueNK;
        private DevExpress.XtraReports.UI.XRLabel lblMienThueGTGT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
    }
}
