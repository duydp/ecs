﻿namespace Company.KDT.SHARE.VNACCS.Controls
{
    partial class ucCalendar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clcCombobox = new Janus.Windows.CalendarCombo.CalendarCombo();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // clcCombobox
            // 
            this.clcCombobox.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // 
            // 
            this.clcCombobox.DropDownCalendar.Name = "";
            this.clcCombobox.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcCombobox.IsNullDate = true;
            this.clcCombobox.Location = new System.Drawing.Point(0, 0);
            this.clcCombobox.Name = "clcCombobox";
            this.clcCombobox.Nullable = true;
            this.clcCombobox.NullButtonText = "Xóa";
            this.clcCombobox.ShowNullButton = true;
            this.clcCombobox.Size = new System.Drawing.Size(130, 21);
            this.clcCombobox.TabIndex = 0;
            this.clcCombobox.TodayButtonText = "Hôm nay";
            this.clcCombobox.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcCombobox.TextChanged += new System.EventHandler(this.cboCategory_EditValueChanged);
            // 
            // ucCalendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.clcCombobox);
            this.Name = "ucCalendar";
            this.Size = new System.Drawing.Size(130, 21);
            this.Load += new System.EventHandler(this.ucCalendar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Janus.Windows.CalendarCombo.CalendarCombo clcCombobox;


    }
}
