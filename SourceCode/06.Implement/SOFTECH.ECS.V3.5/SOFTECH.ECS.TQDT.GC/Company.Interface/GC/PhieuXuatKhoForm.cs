using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Xml.Serialization;
using Company.KDT.SHARE.VNACCS;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using Company.Interface.VNACCS;
//using Company.GC.BLL.GC;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;

namespace Company.Interface.GC
{
    public partial class PhieuXuatKhoForm : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public KDT_GC_PhieuXuatKho PXK = new KDT_GC_PhieuXuatKho();
        public List<KDT_VNACC_HangMauDich> ListHang = new List<KDT_VNACC_HangMauDich>();
        public ToKhaiMauDich TKMD_V4 = new ToKhaiMauDich();
        public ToKhaiChuyenTiep TKCT_V4 = new ToKhaiChuyenTiep();
        public List<HangMauDich> ListHang_V4 = new List<HangMauDich>();
        public List<KDT_GC_PhieuXuatKho_Hang> ListHangX = new List<KDT_GC_PhieuXuatKho_Hang>();
        public KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
        DataTable dt = new DataTable();
        DataSet dsHang = new DataSet();
        DataSet dsDVT = new DataSet();
        DataSet dsmaHS = new DataSet();
        public HopDong HopDong = new HopDong();
        public bool isV4 = false;
        public bool isV4_TKCT = false;
        public PhieuXuatKhoForm()
        {
            InitializeComponent();
        }
        private void GetPhieuXuatKho()
        {
            PXK.NgayXuatKho = Convert.ToDateTime(clcNgayXuatKho.Value);
            PXK.SoPhieu = Convert.ToDecimal(txtSoPhieu.Text);
            PXK.SoHopDong = txtSoHopDong.Text;
            PXK.SoInvoice = txtSoHoaDon.Text;
            PXK.NgayInvoice = Convert.ToDateTime(clcNgayHoaDon.Value);
            PXK.KhachHang = txtKhachHang.Text;
            PXK.Style = txtStyle.Text;
            PXK.PO = txtPO.Text;
            PXK.DonViNhan = txtDonViNhan.Text;
            PXK.XuatTaiKho = txtXuatTaiKho.Text;
            PXK.SoLuong = Convert.ToDecimal(txtSoLuong.Text);
            PXK.DVTSoLuong = ctrDVT.Code;
            //GetHang();
        }
        private void SetPhieuXuatKho()
        {
            errorProvider.Clear();
            clcNgayXuatKho.Value = Convert.ToDateTime(PXK.NgayXuatKho);
            txtSoPhieu.Text = Convert.ToString(PXK.SoPhieu);
            txtSoHopDong.Text = PXK.SoHopDong;
            txtSoHoaDon.Text = PXK.SoInvoice;
            clcNgayHoaDon.Value = Convert.ToDateTime(PXK.NgayInvoice);
            txtKhachHang.Text = PXK.KhachHang;
            txtStyle.Text = PXK.Style;
            txtPO.Text = PXK.PO;
            txtDonViNhan.Text = PXK.DonViNhan;
            txtXuatTaiKho.Text = PXK.XuatTaiKho;
            txtSoLuong.Text = Convert.ToString(PXK.SoLuong);
            ctrDVT.Code = PXK.DVTSoLuong;
        }
        //private void GetHang()
        //{
        //    //PXK.HangXuatCollection.Clear();
        //    //DataTable dtPX = new DataTable();
        //    //dtPX = dt;
        //    //if (dtPX.Rows.Count == 0) return;
        //    if (PXK.HangXuatCollection.Count == 0)
        //        PXK.HangXuatCollection.Add(hang);
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        hang = new KDT_GC_PhieuXuatKho_Hang();
        //        hang.TKMD_ID = PXK.ID;
        //        if (row["ID"].ToString().Length != 0)
        //        {
        //            hang.ID = Convert.ToInt32(row["ID"].ToString());
        //        }
        //        hang.MaSoHang = row["MaNPL1"].ToString();
        //        hang.MaNPL2 = row["MaNPL2"].ToString();
        //        hang.TenHang = row["MoTaNPL"].ToString();
        //        hang.Art = row["Art"].ToString();
        //        hang.NCC = row["NCC"].ToString();
        //        hang.Size = row["Size"].ToString();
        //        hang.Color = row["Color"].ToString();
        //        hang.SoLuong1 = Convert.ToDecimal(row["SoLuongSP"].ToString());
        //        hang.DVTLuong1 = row["DVT"].ToString();
        //        hang.DM = Convert.ToDecimal(row["DM"].ToString());
        //        hang.PhanTram = Convert.ToDecimal(row["PhanTram"].ToString());
        //        hang.Nhucau = Convert.ToDecimal(row["Nhucau"].ToString());
        //        hang.ThucNhan = Convert.ToDecimal(row["ThucNhan"].ToString());
        //        hang.GhiChu = row["GhiChu"].ToString();
        //        PXK.HangXuatCollection.Add(hang);
        //    }

        //}
        private void PhieuXuatKhoForm_Load(object sender, EventArgs e)
        {
            cbDonViTinh.DataSource = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            if (isV4)
            {
                #region Xuất tờ khai V4
                if (TKMD_V4.SoToKhai != 0)
                {
                    if (PXK == null || PXK.ID == 0)
                    {
                        PXK = new KDT_GC_PhieuXuatKho();
                        txtKhachHang.Text = TKMD_V4.TenDonViDoiTac;
                        //Company.GC.BLL.KDT.GC.HopDong hd = Company.GC.BLL.KDT.GC.HopDong.Load(TKMD.HopDong_ID);
                        txtSoHopDong.Text = HopDong != null ? HopDong.SoHopDong : string.Empty;
                        txtSoPhieu.Text = (KDT_GC_PhieuXuatKho.GetSoPhieuMax(TKMD_V4.IDHopDong) + 1).ToString();

                        clcNgayXuatKho.Value = TKMD_V4.NgayDangKy.AddDays(+1);

                        txtSoHoaDon.Text = TKMD_V4.SoHoaDonThuongMai.ToString();
                        clcNgayHoaDon.Value = TKMD_V4.NgayHoaDonThuongMai;

                        //txtSoHopDong.Text = TKMD.HopDong_So;
                        txtSoLuong.Text = Convert.ToString(TKMD_V4.SoLuong);
                        ctrDVT.Code = TKMD_V4.MaDonViUT;
                        Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
                        Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                        string whereCondition = " MaSanPham in (";
                        int index = 1;
                        TKMD_V4.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(TKMD_V4.ID);
                        foreach (HangMauDich HMD in TKMD_V4.HMDCollection)
                        {
                            if (TKMD_V4.HMDCollection.Count == index)
                                whereCondition += "'" + HMD.MaPhu + "'";
                            else
                                whereCondition += "'" + HMD.MaPhu + "',";
                            index++;
                        }
                        whereCondition += ") AND HopDong_ID=" + HopDong.ID + " ";
                        DataTable tbDM = new DataTable();
                        try
                        {
                            tbDM = dm.SelectDynamic(whereCondition, "MaSanPham").Tables[0];
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            //throw;
                        }


                        foreach (HangMauDich HMD in TKMD_V4.HMDCollection)
                        {
                            foreach (DataRow dr in tbDM.Select("MaSanPham ='" + HMD.MaPhu + "'", "MaNguyenPhuLieu"))
                            {
                                KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                                hang.MaNPL2 = dr["MaNguyenPhuLieu"].ToString();
                                hang.TenHang = dr["TenNPL"].ToString();
                                hang.MaSoHang = HMD.MaPhu;
                                hang.SoLuong1 = HMD.SoLuong;
                                hang.DVTLuong1 = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(HMD.DVT_ID);
                                hang.DVTLuong1 = DonViTinh.GetName(HMD.DVT_ID);
                                hang.DM = Convert.ToDecimal(dr["DinhMucSuDung"].ToString());
                                hang.PhanTram = Convert.ToDecimal(dr["TyLeHaoHut"].ToString());
                                decimal NPLXuat = (hang.SoLuong1 * hang.DM) + (hang.SoLuong1 * hang.DM) * (hang.PhanTram / 100);
                                hang.Nhucau = NPLXuat;
                                hang.TKMD_ID = TKMD_V4.ID;
                                PXK.HangXuatCollection.Add(hang);
                            }

                        }

                        dgList.DataSource = PXK.HangXuatCollection;
                        dgList.Refetch();
                    }
                    else
                    {
                        SetPhieuXuatKho();
                        dgList.DataSource = PXK.HangXuatCollection;
                    }
                    if (PXK.HangXuatCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                } else{
                    if (PXK == null || PXK.ID == 0)
                    {
                        PXK = new KDT_GC_PhieuXuatKho();
                        txtKhachHang.Text = "";//TKMD.TenDoiTac;
                        //Company.GC.BLL.KDT.GC.HopDong hd = Company.GC.BLL.KDT.GC.HopDong.Load(TKMD.HopDong_ID);
                        txtSoHopDong.Text = HopDong != null ? HopDong.SoHopDong : string.Empty;
                        txtSoPhieu.Text = "0";//(KDT_GC_PhieuXuatKho.GetSoPhieuMax(TKMD.HopDong_ID) + 1).ToString();

                        clcNgayXuatKho.Value = DateTime.Now;
                    }
                    else
                    {
                        SetPhieuXuatKho();
                        dgList.DataSource = PXK.HangXuatCollection;
                    }
                    if (PXK.HangXuatCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }
                #endregion Xuất tờ khai V4
            }
            else if (isV4_TKCT)
            {
                if (TKCT_V4.SoToKhai != 0)
                {
                    if (PXK == null || PXK.ID == 0)
                    {
                        PXK = new KDT_GC_PhieuXuatKho();
                        txtKhachHang.Text = TKCT_V4.TenDonViDoiTac;
                        //Company.GC.BLL.KDT.GC.HopDong hd = Company.GC.BLL.KDT.GC.HopDong.Load(TKMD.HopDong_ID);
                        txtSoHopDong.Text = HopDong != null ? HopDong.SoHopDong : string.Empty;
                        txtSoPhieu.Text = (KDT_GC_PhieuXuatKho.GetSoPhieuMax(TKCT_V4.IDHopDong) + 1).ToString();

                        clcNgayXuatKho.Value = TKCT_V4.NgayDangKy.AddDays(+1);

                        txtSoHoaDon.Text = TKCT_V4.SoHDKH.ToString();
                        clcNgayHoaDon.Value = TKCT_V4.NgayHDKH;
                        //txtSoHopDong.Text = TKMD.HopDong_So;
                        txtSoLuong.Text = Convert.ToString(TKCT_V4.SoKien);
                        ctrDVT.Code = TKCT_V4.MaDonViUT;
                        Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
                        Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                        string whereCondition = " MaSanPham in (";
                        int index = 1;
                        TKCT_V4.HCTCollection = (List<HangChuyenTiep>)HangChuyenTiep.SelectCollectionBy_Master_ID(TKCT_V4.ID);
                        foreach (HangChuyenTiep HCT in TKCT_V4.HCTCollection)
                        {
                            if (TKCT_V4.HCTCollection.Count == index)
                                whereCondition += "'" + HCT.MaHang + "'";
                            else
                                whereCondition += "'" + HCT.MaHang + "',";
                            index++;
                        }
                        whereCondition += ") AND HopDong_ID=" + HopDong.ID + " ";
                        DataTable tbDM = new DataTable();
                        try
                        {
                            tbDM = dm.SelectDynamic(whereCondition, "MaSanPham").Tables[0];
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            //throw;
                        }


                        foreach (HangChuyenTiep HCT in TKCT_V4.HCTCollection)
                        {
                            foreach (DataRow dr in tbDM.Select("MaSanPham ='" + HCT.MaHang + "'", "MaNguyenPhuLieu"))
                            {
                                KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                                hang.MaNPL2 = dr["MaNguyenPhuLieu"].ToString();
                                hang.TenHang = dr["TenNPL"].ToString();
                                hang.MaSoHang = HCT.MaHS;
                                hang.SoLuong1 = HCT.SoLuong;
                                hang.DVTLuong1 = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(HCT.ID_DVT);
                                hang.DM = Convert.ToDecimal(dr["DinhMucSuDung"].ToString());
                                hang.PhanTram = Convert.ToDecimal(dr["TyLeHaoHut"].ToString());
                                decimal NPLXuat = (hang.SoLuong1 * hang.DM) + (hang.SoLuong1 * hang.DM) * (hang.PhanTram / 100);
                                hang.Nhucau = NPLXuat;
                                hang.TKMD_ID = TKCT_V4.ID;
                                PXK.HangXuatCollection.Add(hang);
                            }
                        }
                        if (PXK.HangXuatCollection.Count==0)
                        {
                            foreach (HangChuyenTiep HCT  in TKCT_V4.HCTCollection)
                            {
                                KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                                hang.MaNPL2 = HCT.MaHang.ToString();
                                hang.TenHang = HCT.TenHang.ToString();
                                //hang.MaSoHang = HCT.MaHS;
                                hang.SoLuong1 = HCT.SoLuong;
                                String ID_DVT = HCT.ID_DVT;
                                if (Char.IsNumber(ID_DVT, 0))
                                {
                                    hang.DVTLuong1 = DonViTinh.GetName(ID_DVT);
                                }
                                else
                                {
                                    hang.DVTLuong1 = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(HCT.ID_DVT);
                                }
                                hang.TKMD_ID = TKCT_V4.ID;
                                PXK.HangXuatCollection.Add(hang);
                            }
                        }
                        dgList.DataSource = PXK.HangXuatCollection;
                        dgList.Refetch();
                    }
                    else
                    {
                        SetPhieuXuatKho();
                        dgList.DataSource = PXK.HangXuatCollection;
                    }
                    if (PXK.HangXuatCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }
                else
                {
                    if (PXK == null || PXK.ID == 0)
                    {
                        PXK = new KDT_GC_PhieuXuatKho();
                        txtKhachHang.Text = "";//TKMD.TenDoiTac;
                        //Company.GC.BLL.KDT.GC.HopDong hd = Company.GC.BLL.KDT.GC.HopDong.Load(TKMD.HopDong_ID);
                        txtSoHopDong.Text = HopDong != null ? HopDong.SoHopDong : string.Empty;
                        txtSoPhieu.Text = "0";//(KDT_GC_PhieuXuatKho.GetSoPhieuMax(TKMD.HopDong_ID) + 1).ToString();
                        clcNgayXuatKho.Value = DateTime.Now;
                    }
                    else
                    {
                        SetPhieuXuatKho();
                        dgList.DataSource = PXK.HangXuatCollection;
                    }
                    if (PXK.HangXuatCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }                
            }
            else
            {
                #region Xuất tờ khai VNACC
                if (TKMD.SoToKhai != 0)
                {
                    if (PXK == null || PXK.ID == 0)
                    {
                        PXK = new KDT_GC_PhieuXuatKho();
                        txtKhachHang.Text = TKMD.TenDoiTac;
                        //Company.GC.BLL.KDT.GC.HopDong hd = Company.GC.BLL.KDT.GC.HopDong.Load(TKMD.HopDong_ID);
                        txtSoHopDong.Text = HopDong != null ? HopDong.SoHopDong : string.Empty;
                        txtSoPhieu.Text = (KDT_GC_PhieuXuatKho.GetSoPhieuMax(TKMD.HopDong_ID) + 1).ToString();

                        clcNgayXuatKho.Value = TKMD.NgayDangKy.AddDays(+1);

                        txtSoHoaDon.Text = TKMD.SoHoaDon.ToString();
                        clcNgayHoaDon.Value = TKMD.NgayPhatHanhHD;
                        //txtSoHopDong.Text = TKMD.HopDong_So;
                        txtSoLuong.Text = Convert.ToString(TKMD.SoLuong);
                        ctrDVT.Code = TKMD.MaDVTSoLuong;
                        Company.GC.BLL.GC.DinhMucCollection dmCollection = new Company.GC.BLL.GC.DinhMucCollection();
                        Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                        string whereCondition = " WHERE MaSanPham in (";
                        int index = 1;
                        foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                        {
                            if (TKMD.HangCollection.Count == index)
                                whereCondition += "'" + HMD.MaHangHoa + "'";
                            else
                                whereCondition += "'" + HMD.MaHangHoa + "',";
                            index++;

                            //KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                            //hang.MaNPL2 = HMD.MaHangHoa;
                            //hang.TenHang = HMD.TenHang;
                            //hang.MaSoHang = HMD.MaSoHang;
                            //hang.SoLuong1 = HMD.SoLuong1;
                            //hang.DVTLuong1 = HMD.DVTLuong1;

                            //PXK.HangXuatCollection.Add(hang);
                        }
                        whereCondition += ") AND HopDong_ID=" + HopDong.ID + " ";
                        //DataTable tbDM = new DataTable();
                        DataTable tbDMDangKy = new DataTable();
                        try
                        {
                            Company.GC.BLL.KDT.GC.DinhMuc dmDangKy = new Company.GC.BLL.KDT.GC.DinhMuc();
                            tbDMDangKy = dmDangKy.SelectDinhMuc(whereCondition).Tables[0];
                            //tbDM = dm.SelectDynamic(whereCondition, "MaSanPham").Tables[0];
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            //throw;
                        }


                        foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                        {
                            if (TKMD.MaLoaiHinh != "E54")
                            {
                                foreach (DataRow dr in tbDMDangKy.Select("MaSanPham ='" + HMD.MaHangHoa + "'", "MaNguyenPhuLieu"))
                                {
                                    KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                                    hang.MaNPL2 = dr["MaNguyenPhuLieu"].ToString();
                                    hang.TenHang = dr["TenNPL"].ToString();
                                    hang.MaSoHang = HMD.MaHangHoa;
                                    hang.SoLuong1 = HMD.SoLuong1;
                                    //hang.DVTLuong1 = HMD.DVTLuong1;                            
                                    //string ID_DVTV4 = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1);
                                    string ID_DVTV4 = dr["DVT_ID"].ToString();
                                    if (Char.IsNumber(ID_DVTV4, 0) && ID_DVTV4 != String.Empty)
                                    {
                                        hang.DVTLuong1 = DonViTinh.GetName(ID_DVTV4);
                                    }
                                    else
                                    {
                                        hang.DVTLuong1 = dr["DVT_ID"].ToString();
                                    }
                                    hang.DM = Convert.ToDecimal(dr["DinhMucSuDung"].ToString());
                                    hang.PhanTram = Convert.ToDecimal(dr["TyLeHaoHut"].ToString());
                                    decimal NPLXuat = (hang.SoLuong1 * hang.DM) + (hang.SoLuong1 * hang.DM) * (hang.PhanTram / 100);
                                    hang.Nhucau = NPLXuat;
                                    hang.TKMD_ID = TKMD.ID;
                                    PXK.HangXuatCollection.Add(hang);
                                }
                            }
                            else
                            {
                                KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                                hang.MaNPL2 = HMD.MaHangHoa.ToString();
                                hang.TenHang = HMD.TenHang.ToString();
                                //hang.MaSoHang = HMD.MaSoHang;
                                hang.SoLuong1 = HMD.SoLuong1;
                                //hang.DVTLuong1 = HMD.DVTLuong1;                            
                                string ID_DVTV4 = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1);
                                //string ID_DVTV4 = dr["DVT_ID"].ToString();
                                if (Char.IsNumber(ID_DVTV4, 0) && ID_DVTV4 != String.Empty)
                                {
                                    hang.DVTLuong1 = DonViTinh.GetName(ID_DVTV4);
                                }
                                else
                                {
                                    hang.DVTLuong1 = HMD.DVTLuong1.ToString();
                                }
                                hang.DM = 0;
                                hang.PhanTram = 0;
                                hang.Nhucau = 0;
                                hang.TKMD_ID = TKMD.ID;
                                PXK.HangXuatCollection.Add(hang);
                            }

                        }

                        dgList.DataSource = PXK.HangXuatCollection;
                        dgList.Refetch();
                    }
                    else
                    {
                        SetPhieuXuatKho();
                        dgList.DataSource = PXK.HangXuatCollection;
                    }
                    if (PXK.HangXuatCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }
                else
                {
                    if (PXK == null || PXK.ID == 0)
                    {
                        PXK = new KDT_GC_PhieuXuatKho();
                        txtKhachHang.Text = "";//TKMD.TenDoiTac;
                        //Company.GC.BLL.KDT.GC.HopDong hd = Company.GC.BLL.KDT.GC.HopDong.Load(TKMD.HopDong_ID);
                        txtSoHopDong.Text = HopDong != null ? HopDong.SoHopDong : string.Empty;
                        txtSoPhieu.Text = "0";//(KDT_GC_PhieuXuatKho.GetSoPhieuMax(TKMD.HopDong_ID) + 1).ToString();

                        clcNgayXuatKho.Value = DateTime.Now;

                        //txtSoHopDong.Text = TKMD.HopDong_So;
                        //txtSoLuong.Text = Convert.ToString(TKMD.SoLuong);
                        //ctrDVT.Code = TKMD.MaDVTSoLuong;
                        //foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                        //{
                        //    KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                        //    hang.MaNPL2 = HMD.MaHangHoa;
                        //    hang.TenHang = HMD.TenHang;
                        //    //hang.MaSoHang = HMD.MaSoHang;
                        //    //hang.SoLuong1 = HMD.SoLuong1;
                        //    //hang.DVTLuong1 = HMD.DVTLuong1;
                        //    PXK.HangXuatCollection.Add(hang);
                        //}
                        //dgList.DataSource = PXK.HangXuatCollection;
                        //dgList.Refetch();
                    }
                    else
                    {
                        SetPhieuXuatKho();
                        dgList.DataSource = PXK.HangXuatCollection;
                    }
                    if (PXK.HangXuatCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }
                #endregion Xuất tờ khai VNACC
            }
            

        }
        private void LoadGrid()
        {
            //dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.HangCollection);
            //dgList.DataSource = TKMD.HangCollection;
            //dsHang = KDT_GC_PhieuXuatKho_Hang.SelectDynamic("select * from t_kdt_vnacc_hangmaudich where tkmd_id =" + TKMD.ID,"");
            //dgList.DataSource = dsHang.Tables[0];
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                GetPhieuXuatKho();
                if (PXK.HangXuatCollection.Count > 0)
                {
                    if (!KiemTraLuongTon())
                    {
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    if (isV4)
                    {
                        if (PXK.TKMD_ID == 0)
                            PXK.TKMD_ID = TKMD_V4.ID;
                    }
                    else 
                    {
                        if (PXK.TKMD_ID == 0)
                            PXK.TKMD_ID = TKMD.ID;
                    }
                    
                    PXK.InsertUpdateFul();
                    ShowMessage("Lưu thành công", false);
                    dgList.DataSource = PXK.HangXuatCollection;
                    SetPhieuXuatKho();
                    dgList.Refetch();
                    btnIn.Enabled = true;
                }
                else
                {
                    ShowMessage("Chưa nhập thông tin hàng", false);
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            this.Cursor = Cursors.Default;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            Company.Interface.Report.GC.PhieuXuatKho pxkrp = new Company.Interface.Report.GC.PhieuXuatKho();
            pxkrp.BindReport(PXK);
            pxkrp.ShowRibbonPreview();

        }
        public long IDTKMD;
        private void txtSoThuTuDongHangTrenToKhaiGoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (isV4)
                {
                    VNACC_ListHangHMD listForm = new VNACC_ListHangHMD(VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { this.TKMD_V4.ID });
                    listForm.isV4 = true;
                    listForm.ShowDialog();
                    if (listForm.DialogResult == DialogResult.OK)
                    {
                        if (listForm.HangMD != null)
                        {
                            HangMauDich hmdTNTX = listForm.HangMD_V4;
                            //ctrMaSoHang.Code = hmdTNTX.MaSoHang;
                            txtMoTaHH.Text = hmdTNTX.TenHang;
                            txtLuong1.Text = hmdTNTX.SoLuong.ToString();
                            //ctrDVTLuong1.Code = hmdTNTX.DVTLuong1;
                            //ctrNuocXuatXu.Code = hmdTNTX.NuocXuatXu;
                            //txtSoThuTuDongHangTrenToKhaiGoc.Text = hmdTNTX.SoDong;
                            txtMaNPL2.Text = hmdTNTX.Ma;
                        }
                    }
                }
                else if (isV4_TKCT)
                {
                    VNACC_ListHangHMD listForm = new VNACC_ListHangHMD(VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { this.TKCT_V4.ID });
                    listForm.isV4_TKCT = true;
                    listForm.ShowDialog();
                    if (listForm.DialogResult == DialogResult.OK)
                    {
                        if (listForm.HangMD != null)
                        {
                            HangChuyenTiep hmdTNTX = listForm.HangCT_V4;
                            //ctrMaSoHang.Code = hmdTNTX.MaSoHang;
                            txtMoTaHH.Text = hmdTNTX.TenHang;
                            txtLuong1.Text = hmdTNTX.SoLuong.ToString();
                            //ctrDVTLuong1.Code = hmdTNTX.DVTLuong1;
                            //ctrNuocXuatXu.Code = hmdTNTX.NuocXuatXu;
                            //txtSoThuTuDongHangTrenToKhaiGoc.Text = hmdTNTX.SoDong;
                            txtMaNPL2.Text = hmdTNTX.MaHang;
                        }
                    }                    
                }
                else
                {
                    VNACC_ListHangHMD listForm = new VNACC_ListHangHMD(VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { this.TKMD.ID });
                    listForm.isV4 = false;
                    listForm.ShowDialog();
                    if (listForm.DialogResult == DialogResult.OK)
                    {
                        if (listForm.HangMD != null)
                        {
                            KDT_VNACC_HangMauDich hmdTNTX = listForm.HangMD;
                            //ctrMaSoHang.Code = hmdTNTX.MaSoHang;
                            txtMoTaHH.Text = hmdTNTX.TenHang;
                            txtLuong1.Text = hmdTNTX.SoLuong1.ToString();
                            //ctrDVTLuong1.Code = hmdTNTX.DVTLuong1;
                            //ctrNuocXuatXu.Code = hmdTNTX.NuocXuatXu;
                            //txtSoThuTuDongHangTrenToKhaiGoc.Text = hmdTNTX.SoDong;
                            txtMaNPL2.Text = hmdTNTX.MaHangHoa;
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        private void GetHang()
        {
            hang.MaSoHang = txtMa1.Text;
            hang.MaNPL2 = txtMaNPL2.Text;
            hang.TenHang = txtMoTaHH.Text;
            hang.Art = txtArt.Text;
            hang.Size = txtSize.Text;
            hang.NCC = txtNhaCC.Text;
            hang.Color = txtMauSac.Text;
            hang.SoLuong1 = Convert.ToDecimal(txtLuong1.Text);
            //hang.DVTLuong1 = ctrDVTLuong1.Code;
            hang.DVTLuong1 = cbDonViTinh.Text;
            hang.DM = Convert.ToDecimal(txtDinhMuc.Text);
            hang.PhanTram = Convert.ToDecimal(txtPhanTram.Text);
            hang.Nhucau = Convert.ToDecimal(txtNhuCau.Text);
            hang.ThucNhan = Convert.ToDecimal(txtThucNhan.Text);
            hang.GhiChu = txtGhiChu.Text;
        }
        private void SetHang()
        {
            errorProvider.Clear();
            txtMa1.Text = hang.MaSoHang;
            txtMaNPL2.Text = hang.MaNPL2;
            txtMoTaHH.Text = hang.TenHang;
            txtArt.Text = hang.Art;
            txtSize.Text = hang.Size;
            txtNhaCC.Text = hang.NCC;
            txtMauSac.Text = hang.Color;
            txtLuong1.Text = Convert.ToString(hang.SoLuong1);
            cbDonViTinh.Text = hang.DVTLuong1;
            //txtDVTLuong1.Text = hang.DVTLuong1;//DonViTinh_GetName(hang.DVTLuong1);
            txtDinhMuc.Text = Convert.ToString(hang.DM);
            txtPhanTram.Text = Convert.ToString(hang.PhanTram);
            txtNhuCau.Text = Convert.ToString(hang.Nhucau);
            txtThucNhan.Text = Convert.ToString(hang.ThucNhan);
            txtGhiChu.Text = hang.GhiChu;
        }
        bool isAddNew = true;
        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                GetHang();
                if (isAddNew)
                    PXK.HangXuatCollection.Add(hang);
                dgList.DataSource = PXK.HangXuatCollection;
                dgList.Refetch();
                hang = new KDT_GC_PhieuXuatKho_Hang();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi khi lưu hàng: " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                hang = (KDT_GC_PhieuXuatKho_Hang)dgList.CurrentRow.DataRow;
                SetHang();
                isAddNew = false;

                dgList.DataSource = PXK.HangXuatCollection;
                try
                {
                    dgList.Refetch();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<KDT_GC_PhieuXuatKho_Hang> ItemColl = new List<KDT_GC_PhieuXuatKho_Hang>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_GC_PhieuXuatKho_Hang)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_GC_PhieuXuatKho_Hang item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        PXK.HangXuatCollection.Remove(item);
                    }

                    dgList.DataSource = PXK.HangXuatCollection;
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { }

                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void txtPO_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                VNACC_ListHangHMD listForm = new VNACC_ListHangHMD(VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { this.TKMD.ID });
                if (isV4)
                {
                    listForm.isV4 = true;
                    listForm.TKMD_ID = (int)TKMD_V4.ID;
                }else if(isV4_TKCT)
                {
                    listForm.isV4_TKCT = true;
                    listForm.TKMD_ID = (int)TKCT_V4.ID;
                }
                listForm.ShowDialog();
                if (listForm.DialogResult == DialogResult.OK)
                {
                    foreach (GridEXRow row in dgList.GetRows())
                    {
                        row.Delete();
                    }
                    if (listForm.HangMD != null)
                    {
                        KDT_VNACC_HangMauDich hmdTNTX = listForm.HangMD;
                        txtPO.Text = hmdTNTX.MaHangHoa;
                        txtSoLuong.Value = hmdTNTX.SoLuong1;
                        ctrDVT.Code = hmdTNTX.DVTLuong1;

                        Company.GC.BLL.GC.DinhMuc dinhmuc = new Company.GC.BLL.GC.DinhMuc();
                        Company.GC.BLL.GC.DinhMucCollection DMCollection = new Company.GC.BLL.GC.DinhMucCollection();
                        Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                        NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
                        //List<NguyenPhuLieu> ListNPL = new List<NguyenPhuLieu>();


                        string query = "HopDong_ID =" + TKMD.HopDong_ID + " and MaSanPham = '" + hmdTNTX.MaHangHoa + "'";
                        DMCollection = dinhmuc.SelectCollectionDynamic(query, "HopDong_ID");
                        foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
                        {
                            KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                            hang.MaNPL2 = DM.MaNguyenPhuLieu;
                            hang.TenHang = DM.TenNPL;
                            hang.SoLuong1 = hmdTNTX.SoLuong1;
                            string qr = "HopDong_ID =" + TKMD.HopDong_ID + " and Ma = '" + DM.MaNguyenPhuLieu + "'";
                            NPLCollection = NPL.SelectCollectionDynamic(qr, "HopDong_ID");
                            foreach (Company.GC.BLL.GC.NguyenPhuLieu N in NPLCollection)
                            {
                                if (Char.IsNumber(N.DVT_ID, 0))
                                {
                                    hang.DVTLuong1 = DonViTinh.GetName(N.DVT_ID);
                                }
                                else
                                {
                                    hang.DVTLuong1 = N.DVT_ID;
                                }
                            }
                            hang.DM = DM.DinhMucSuDung;
                            hang.PhanTram = DM.TyLeHaoHut;
                            decimal dmsp = hang.DM;
                            decimal tyleHH = hang.PhanTram;
                            decimal soluong = hang.SoLuong1;
                            decimal nhucauchuaHH = soluong * dmsp;
                            decimal nhucau = nhucauchuaHH + nhucauchuaHH * tyleHH / 100;
                            hang.Nhucau = nhucau;
                            PXK.HangXuatCollection.Add(hang);
                        }
                        if (DMCollection.Count == 0)
                        {
                            KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                            hang.MaNPL2 = hmdTNTX.MaHangHoa;
                            hang.TenHang = hmdTNTX.TenHang;
                            hang.SoLuong1 = hmdTNTX.SoLuong1;
                            hang.DVTLuong1 = hmdTNTX.DVTLuong1;
                            hang.DM = 0;
                            hang.PhanTram = 0;
                            hang.Nhucau = 0;
                            PXK.HangXuatCollection.Add(hang);
                        }
                        dgList.DataSource = PXK.HangXuatCollection;
                        dgList.Refetch();
                    }
                    else if (listForm.HangMD_V4 != null)
                    {
                        HangMauDich hmdTNTX = listForm.HangMD_V4;
                        txtPO.Text = hmdTNTX.MaPhu;
                        txtSoLuong.Value = hmdTNTX.SoLuong;
                        ctrDVT.Code = hmdTNTX.DVT_ID;
                        if (Char.IsNumber(hmdTNTX.DVT_ID,0))
                        {
                            ctrDVT.Code = DonViTinh.GetName(hmdTNTX.DVT_ID);  
                        }
                        Company.GC.BLL.GC.DinhMuc dinhmuc = new Company.GC.BLL.GC.DinhMuc();
                        Company.GC.BLL.GC.DinhMucCollection DMCollection = new Company.GC.BLL.GC.DinhMucCollection();
                        Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                        NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
                        //List<NguyenPhuLieu> ListNPL = new List<NguyenPhuLieu>();


                        string query = "HopDong_ID =" + TKMD_V4.IDHopDong + " and MaSanPham = '" + hmdTNTX.MaPhu + "'";
                        DMCollection = dinhmuc.SelectCollectionDynamic(query, "HopDong_ID");
                        foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
                        {
                            KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                            hang.MaNPL2 = DM.MaNguyenPhuLieu;
                            hang.TenHang = DM.TenNPL;
                            hang.SoLuong1 = hmdTNTX.SoLuong;
                            hang.MaSoHang = hmdTNTX.MaPhu;
                            string qr = "HopDong_ID =" + TKMD_V4.IDHopDong + " and Ma = '" + DM.MaNguyenPhuLieu + "'";
                            NPLCollection = NPL.SelectCollectionDynamic(qr, "HopDong_ID");
                            foreach (Company.GC.BLL.GC.NguyenPhuLieu N in NPLCollection)
                            {
                                if (Char.IsNumber(N.DVT_ID, 0))
                                {
                                    hang.DVTLuong1 = DonViTinh.GetName(N.DVT_ID);
                                }
                                else
                                {
                                    hang.DVTLuong1 = N.DVT_ID;
                                }

                            }
                            hang.DM = DM.DinhMucSuDung;
                            hang.PhanTram = DM.TyLeHaoHut;
                            decimal dmsp = hang.DM;
                            decimal tyleHH = hang.PhanTram;
                            decimal soluong = hang.SoLuong1;
                            decimal nhucauchuaHH = soluong * dmsp;
                            decimal nhucau = nhucauchuaHH + nhucauchuaHH * tyleHH / 100;
                            hang.Nhucau = nhucau;
                            PXK.HangXuatCollection.Add(hang);
                        }

                        //foreach (GridEXRow row in dgList.GetRows())
                        //{
                        //    row.Delete();
                        //}
                        dgList.DataSource = PXK.HangXuatCollection;
                        dgList.Refetch();

                    }
                    else if (listForm.HangCT_V4 != null)
                    {
                        HangChuyenTiep hmdTNTX = listForm.HangCT_V4;
                        txtPO.Text = hmdTNTX.MaHang;
                        txtSoLuong.Value = hmdTNTX.SoLuong;
                        ctrDVT.Code = hmdTNTX.ID_DVT;

                        Company.GC.BLL.GC.DinhMuc dinhmuc = new Company.GC.BLL.GC.DinhMuc();
                        Company.GC.BLL.GC.DinhMucCollection DMCollection = new Company.GC.BLL.GC.DinhMucCollection();
                        Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                        NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
                        //List<NguyenPhuLieu> ListNPL = new List<NguyenPhuLieu>();


                        string query = "HopDong_ID =" + TKCT_V4.IDHopDong + " and MaSanPham = '" + hmdTNTX.MaHang + "'";
                        DMCollection = dinhmuc.SelectCollectionDynamic(query, "HopDong_ID");
                        foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
                        {
                            KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                            hang.MaNPL2 = DM.MaNguyenPhuLieu;
                            hang.TenHang = DM.TenNPL;
                            hang.SoLuong1 = hmdTNTX.SoLuong;
                            string qr = "HopDong_ID =" + TKCT_V4.IDHopDong + " and Ma = '" + DM.MaNguyenPhuLieu + "'";
                            NPLCollection = NPL.SelectCollectionDynamic(qr, "HopDong_ID");
                            foreach (Company.GC.BLL.GC.NguyenPhuLieu N in NPLCollection)
                            {
                                if (Char.IsNumber(N.DVT_ID, 0))
                                {
                                    hang.DVTLuong1 = DonViTinh.GetName(N.DVT_ID);
                                }
                                else
                                {
                                    hang.DVTLuong1 = N.DVT_ID;
                                }
                            }
                            hang.DM = DM.DinhMucSuDung;
                            hang.PhanTram = DM.TyLeHaoHut;
                            decimal dmsp = hang.DM;
                            decimal tyleHH = hang.PhanTram;
                            decimal soluong = hang.SoLuong1;
                            decimal nhucauchuaHH = soluong * dmsp;
                            decimal nhucau = nhucauchuaHH + nhucauchuaHH * tyleHH / 100;
                            hang.Nhucau = nhucau;
                            PXK.HangXuatCollection.Add(hang);
                        }
                        if (DMCollection.Count==0)
                        {
                            KDT_GC_PhieuXuatKho_Hang hang = new KDT_GC_PhieuXuatKho_Hang();
                            hang.MaNPL2 = hmdTNTX.MaHang;
                            hang.TenHang = hmdTNTX.TenHang;
                            hang.SoLuong1 = hmdTNTX.SoLuong;
                            hang.DVTLuong1 = DonViTinh.GetName(hmdTNTX.ID_DVT);
                            hang.DM = 0;
                            hang.PhanTram = 0;
                            hang.Nhucau = 0;
                            PXK.HangXuatCollection.Add(hang);
                        }
                        dgList.DataSource = PXK.HangXuatCollection;
                        dgList.Refetch();                        
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        private Company.GC.BLL.GC.DinhMuc getDinhMuc(long idHD)
        {
            string where = "id = " + idHD;
            Company.GC.BLL.GC.DinhMucCollection DMCollection = new Company.GC.BLL.GC.DinhMucCollection();

            //DMCollection = DinhMuc.GetTLHHOfHopDong(idHD);
            foreach (Company.GC.BLL.GC.DinhMuc DM in DMCollection)
            {
                if (TKMD.HopDong_ID == idHD)
                {
                    string query = "HopDong_ID =" + TKMD.HopDong_ID;
                    DMCollection = DM.SelectCollectionDynamic(query, "HopDong_ID");


                }
            }

            return null;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record && e.Row.Cells["DVTLuong1"].Value != null)
                    e.Row.Cells["TenDVT"].Text = e.Row.Cells["DVTLuong1"].Value.ToString();//this.DonViTinh_GetName(e.Row.Cells["DVTLuong1"].Value.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }

        }

        private void txtPhanTram_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSoLuong.Text))
            {
                decimal LuongSP = System.Convert.ToDecimal(txtLuong1.Value);
                decimal DinhMuc = System.Convert.ToDecimal(txtDinhMuc.Value);
                decimal HaoHut = System.Convert.ToDecimal(txtPhanTram.Value);
                decimal LuongNPL = LuongSP * DinhMuc * (1 + HaoHut / 100);
                txtNhuCau.Value = LuongNPL;
            }
        }
        private bool KiemTraLuongTon()
        {
            long IDHD = TKMD.HopDong_ID;
            if (TKMD.HopDong_ID == 0)
            {
                KDT_VNACC_ToKhaiMauDich tkmd = KDT_VNACC_ToKhaiMauDich.Load(PXK.TKMD_ID);
                IDHD = tkmd != null ? tkmd.HopDong_ID : TKMD.HopDong_ID;
            }
            DataTable dt = KDT_GC_PhieuXuatKho.LoadXuatNhapTon(IDHD, PXK.ID, PXK.PO);
            List<KDT_GC_PhieuXuatKho_Hang> hangAm = new List<KDT_GC_PhieuXuatKho_Hang>();
            List<KDT_GC_PhieuXuatKho_Hang> hangKhongNhap = new List<KDT_GC_PhieuXuatKho_Hang>();
            foreach (KDT_GC_PhieuXuatKho_Hang item in PXK.HangXuatCollection)
            {
                DataRow[] dr = dt.Select("MaNPL = '" + item.MaNPL2 + "'");
                if (dr.Length > 0)
                {
                    if (System.Convert.ToDecimal(dr[0]["LuongTon"]) < System.Convert.ToDecimal(item.ThucNhan))
                    {
                        hangAm.Add(item);
                    }
                }
                else
                {
                    hangKhongNhap.Add(item);
                }
            }
            if (hangAm.Count == 0 && hangKhongNhap.Count == 0)
                return true;
            else
            {
                string _loiAm = "";
                string _LoiKhongNhap = "";
                foreach (KDT_GC_PhieuXuatKho_Hang item in hangKhongNhap)
                {
                    _LoiKhongNhap += item.MaNPL2 + " - ";
                }
                foreach (KDT_GC_PhieuXuatKho_Hang item in hangAm)
                {
                    _loiAm += item.MaNPL2 + " - ";
                }
                string loi = string.Empty;
                if (!string.IsNullOrEmpty(_loiAm))
                    loi += "Danh sách NPL không đủ lượng tồn để xuất kho: " + _loiAm;
                if (!string.IsNullOrEmpty(_LoiKhongNhap))
                    loi += "\r\n" + "Danh sách NPL không nhập khẩu: " + _LoiKhongNhap;
                loi += "\r\n" + "Bạn có muốn tiếp tục lưu thông tin";
                if (this.ShowMessage(loi, true) == "Yes")
                    return true;
                else
                    return false;


            }
        }

        private void btnXuatNhapTon_Click(object sender, EventArgs e)
        {
            PhieuXuatKho_ThongKeTon f = new PhieuXuatKho_ThongKeTon();
            f.IDPhieuXuat = PXK.ID;
            f.MaPO = PXK.PO;
            KDT_VNACC_ToKhaiMauDich tkmd = KDT_VNACC_ToKhaiMauDich.Load(PXK.TKMD_ID);
            f.IDHopDong = tkmd != null ? tkmd.HopDong_ID : TKMD.HopDong_ID;
            f.ShowDialog(this);
        }
    }
}
