﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using DevExpress.XtraCharts;
using Company.Interface.Report;
using Company.Interface.Report.SXXK;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#endif

namespace Company.Interface
{
    public partial class HoaDonThuongMaiForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        public HoaDonThuongMai HDonTM;
        public List<HoaDonThuongMaiDetail> listHDTMDetail = new List<HoaDonThuongMaiDetail>();
        public bool isKhaiBoSung = false;
        private FeedBackContent feedbackContent;
        private string msgInfor = string.Empty;
        public HoaDonThuongMaiForm()
        {
            InitializeComponent();
        }

        private void HoaDonThuongMaiForm_Load(object sender, EventArgs e)
        {
            //Update by KHANHHN - 03/03/2012
            // Tạo mới HDonTM với trạng thái chưa khao báo
            if (HDonTM == null || HDonTM.ID == 0)
                HDonTM = new HoaDonThuongMai { TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO };
            btnKhaiBao.Enabled = false;
            btnLayPhanHoi.Enabled = false;

            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
            //cbPTTT.SelectedValue = TKMD.PTTT_ID;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.DisplayMember = cbDKGH.ValueMember = "ID";
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            //cbDKGH.SelectedValue = TKMD.DKGH_ID;

            //Start Ngonnt 25/01
            if (TKMD.DKGH_ID != "")
            {
                cbDKGH.SelectedValue = TKMD.DKGH_ID;
            }
            if (TKMD.PTTT_ID != "")
            {
                cbPTTT.SelectedValue = TKMD.PTTT_ID;
            }
            if (TKMD.NguyenTe_ID != "")
            {
                nguyenTeControl2.Ma = TKMD.NguyenTe_ID;
            }
            //End

            if (HDonTM != null && HDonTM.ID <= 0)
            {
                //DATLMQ bổ sung lấy HĐơn từ TKMD 13/12/2010
                HDonTM.NgayHoaDon = TKMD.NgayHoaDonThuongMai;
                ccNgayVanDon.Value = HDonTM.NgayHoaDon;
                HDonTM.SoHoaDon = TKMD.SoHoaDonThuongMai;
                txtSoVanDon.Text = HDonTM.SoHoaDon;
                if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    txtMaDVMua.Text = TKMD.MaDoanhNghiep;
                    txtTenDVMua.Text = TKMD.TenDoanhNghiep;
                    txtTenDVBan.Text = TKMD.TenDonViDoiTac;
                }
                else
                {
                    txtTenDVMua.Text = TKMD.TenDonViDoiTac;
                    txtMaDVBan.Text = TKMD.MaDoanhNghiep;
                    txtTenDVBan.Text = TKMD.TenDoanhNghiep;
                }
            }
            else if (HDonTM != null && HDonTM.ID > 0)
            {
                cbDKGH.SelectedValue = HDonTM.DKGH_ID;
                txtMaDVBan.Text = HDonTM.MaDonViBan;
                txtMaDVMua.Text = HDonTM.MaDonViMua;
                ccNgayVanDon.Text = HDonTM.NgayHoaDon.ToLongDateString();
                nguyenTeControl2.Ma = HDonTM.NguyenTe_ID;
                cbPTTT.SelectedValue = HDonTM.PTTT_ID;
                txtSoVanDon.Text = HDonTM.SoHoaDon;
                txtTenDVBan.Text = HDonTM.TenDonViBan;
                txtTenDVMua.Text = HDonTM.TenDonViMua;
                txtThongTinKhac.Text = HDonTM.ThongTinKhac;
                BindData();
            }
            if (HDonTM.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                ccNgayTiepNhan.Text = HDonTM.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = HDonTM.SoTiepNhan + "";
            }
            SetButtonStateHOADON(TKMD, isKhaiBoSung, HDonTM);
        }
        private void BindData()
        {
            //Company.KD.BLL.KDT.HangMauDich hmd = new Company.KD.BLL.KDT.HangMauDich();
            //hmd.TKMD_ID = TKMD.ID;
            DataTable dt = HangMauDich.SelectBy_TKMD_ID(TKMD.ID).Tables[0];
            dgList.DataSource = HDonTM.ConvertListToDataSet(dt);
            //dgList.DataSource = HDonTM.ListHangMDOfHoaDon;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private bool checkSoHoaDon(string soHoaDon)
        {
            foreach (HoaDonThuongMai HDTM in TKMD.HoaDonThuongMaiCollection)
            {
                if (HDTM.SoHoaDon.Trim().ToUpper() == soHoaDon.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (HDonTM.ListHangMDOfHoaDon.Count == 0)
            {
                ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                return;
            }
            bool isValid = ValidateControl.ValidateDate(ccNgayVanDon, epError, "Ngày hóa đơn");
            if (!isValid) return;

            TKMD.HoaDonThuongMaiCollection.Remove(HDonTM);
            if (checkSoHoaDon(txtSoVanDon.Text.Trim()))
            {
                if (HDonTM.ID > 0)
                    TKMD.HoaDonThuongMaiCollection.Add(HDonTM);
                ShowMessage("Số hóa đơn này đã tồn tại.", false);
                return;
            }
            HDonTM.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
            HDonTM.MaDonViBan = txtMaDVBan.Text.Trim();
            HDonTM.MaDonViMua = txtMaDVMua.Text.Trim();
            HDonTM.NgayHoaDon = ccNgayVanDon.Value;
            HDonTM.NguyenTe_ID = nguyenTeControl2.Ma;
            HDonTM.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
            HDonTM.SoHoaDon = txtSoVanDon.Text.Trim();
            HDonTM.TenDonViBan = txtTenDVBan.Text.Trim();
            HDonTM.TenDonViMua = txtTenDVMua.Text.Trim();
            HDonTM.ThongTinKhac = txtThongTinKhac.Text.Trim();
            HDonTM.TKMD_ID = TKMD.ID;
            HDonTM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            if (isKhaiBoSung)
                HDonTM.LoaiKB = 1;
            else
                HDonTM.LoaiKB = 0;

            GridEXRow[] rowCollection = dgList.GetRows();
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (HoaDonThuongMaiDetail item in HDonTM.ListHangMDOfHoaDon)
                {
                    if (rowview["HMD_ID"].ToString().Trim() == item.HMD_ID.ToString().Trim())
                    {
                        item.GhiChu = row.Cells["GhiChu"].Text;
                        item.GiaiTriDieuChinhGiam = Convert.ToDouble(row.Cells["GiaiTriDieuChinhGiam"].Value);
                        item.GiaTriDieuChinhTang = Convert.ToDouble(row.Cells["GiaTriDieuChinhTang"].Value);
                        item.DVT_ID = row.Cells["DVT_ID"].Value.ToString();
                        item.SoLuong = Convert.ToDecimal(row.Cells["SoLuong"].Value);
                        item.DonGiaKB = Convert.ToDouble(row.Cells["DonGiaKB"].Value);
                        item.TriGiaKB = Convert.ToDouble(row.Cells["TriGiaKB"].Value);
                        item.NuocXX_ID = row.Cells["NuocXX_ID"].Value.ToString();
                        break;
                    }
                }
            }
            try
            {
                if (string.IsNullOrEmpty(HDonTM.GuidStr)) HDonTM.GuidStr = Guid.NewGuid().ToString();
                if (HDonTM.NgayTiepNhan.Year < 1900) HDonTM.NgayTiepNhan = new DateTime(1900, 1, 1);
                HDonTM.InsertUpdateFull();
                TKMD.HoaDonThuongMaiCollection.Add(HDonTM);
                BindData();
                ShowMessage("Lưu thành công.", false);
                if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET && TKMD.SoToKhai > 0)
                    btnKhaiBao.Enabled = true;
                TKMD.SoHoaDonThuongMai = HDonTM.SoHoaDon;
                TKMD.NgayHoaDonThuongMai = HDonTM.NgayHoaDon;
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HoaDonThuongMaiDetail> HoaDonThuongMaiDetailCollection = new List<HoaDonThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonThuongMaiDetail hdtmdtmp = new HoaDonThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HoaDonThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HoaDonThuongMaiDetail hdtmtmp in HoaDonThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HoaDonThuongMaiDetail hdtmd in HDonTM.ListHangMDOfHoaDon)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HDonTM.ListHangMDOfHoaDon.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public List<HangMauDich> ConvertListToHangMauDichCollection(List<HoaDonThuongMaiDetail> listHangHoaDon, List<HangMauDich> hangCollection)
        {

            List<HangMauDich> tmp = new List<HangMauDich>();

            foreach (HoaDonThuongMaiDetail item in listHangHoaDon)
            {
                foreach (HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            /*
            if (HDonTM.ListHangMDOfHoaDon.Count > 0)
            {
                f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(HDonTM.ListHangMDOfHoaDon, TKMD.HMDCollection);
            }*/
            f.ShowDialog(this);
            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (HangMauDich HMD in f.HMDTMPCollection)
                {
                    bool ok = false;
                    foreach (HoaDonThuongMaiDetail hangHoaDonDetail in HDonTM.ListHangMDOfHoaDon)
                    {
                        if (hangHoaDonDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HoaDonThuongMaiDetail hoaDonDetail = new HoaDonThuongMaiDetail();
                        hoaDonDetail.HMD_ID = HMD.ID;
                        hoaDonDetail.HoaDonTM_ID = HDonTM.ID;
                        hoaDonDetail.HMD_ID = HMD.ID;
                        hoaDonDetail.HoaDonTM_ID = HDonTM.ID;
                        hoaDonDetail.MaPhu = HMD.MaPhu;
                        hoaDonDetail.MaHS = HMD.MaHS;
                        hoaDonDetail.TenHang = HMD.TenHang;
                        hoaDonDetail.DVT_ID = HMD.DVT_ID;
                        hoaDonDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hoaDonDetail.SoLuong = HMD.SoLuong;
                        hoaDonDetail.NuocXX_ID = HMD.NuocXX_ID;
                        hoaDonDetail.DonGiaKB = Convert.ToDouble(HMD.DonGiaKB);
                        hoaDonDetail.TriGiaKB = Convert.ToDouble(HMD.TriGiaKB);

                        HDonTM.ListHangMDOfHoaDon.Add(hoaDonDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (HDonTM.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            #region V3

            try
            {
                if ((HDonTM.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO) || (HDonTM.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET))
                    HDonTM.GuidStr = Guid.NewGuid().ToString();
                else if (HDonTM.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    return;
                }

                ObjectSend msgSend = SingleMessage.BoSungHoaDonTM(TKMD, HDonTM);

                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    HDonTM.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    SetButtonStateHOADON(TKMD, isKhaiBoSung, HDonTM);
                    sendMessageForm.Message.XmlSaveMessage(HDonTM.ID, MessageTitle.KhaiBaoBoSungHoaDonTM);
                    HDonTM.Update();
                    btnLayPhanHoi_Click(null, null);
                }
                else if (isSend && feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    ShowMessageTQDT(msgInfor, false);
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));

            }


            #endregion
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBack(TKMD, HDonTM.GuidStr);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                isFeedBack = sendMessageForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetButtonStateHOADON(TKMD, isKhaiBoSung, HDonTM);
                }


            }
//             ObjectSend msgSend = SingleMessage.FeedBack(TKMD, HDonTM.GuidStr);
//             SendMessageForm sendMessageForm = new SendMessageForm();
//             sendMessageForm.Send += SendMessage;
//             sendMessageForm.DoSend(msgSend);


        }

        private void LayPhanHoiKhaiBao(string password)
        {
//         StartInvoke:
//             try
//             {
//                 bool thanhcong = HDonTM.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
// 
//                 // Thực hiện kiểm tra.  
//                 if (thanhcong == false)
//                 {
//                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
//                     {
//                         this.Refresh();
//                         goto StartInvoke;
//                     }
// 
//                     btnKhaiBao.Enabled = true;
//                     btnLayPhanHoi.Enabled = true;
//                 }
//                 else
//                 {
//                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HDonTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong);
//                     this.ShowMessage(message, false);
// 
//                     btnKhaiBao.Enabled = false;
//                     txtSoTiepNhan.Text = this.HDonTM.SoTiepNhan.ToString("N0");
//                     ccNgayTiepNhan.Value = this.HDonTM.NgayTiepNhan;
//                     ccNgayTiepNhan.Text = this.HDonTM.NgayTiepNhan.ToShortDateString();
//                 }
//             }
//             catch (Exception ex)
//             {
//                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
//             }
        }

        private void LayPhanHoiDuyet(string password)
        {
//         StartInvoke:
//             try
//             {
//                 bool thanhcong = HDonTM.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
// 
//                 // Thực hiện kiểm tra.  
//                 if (thanhcong == false)
//                 {
//                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
//                     {
//                         this.Refresh();
//                         goto StartInvoke;
//                     }
// 
//                     btnLayPhanHoi.Enabled = true;
//                 }
//                 else
//                 {
//                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HDonTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHoaDonTMDuocChapNhan);
//                     if (message.Length == 0)
//                     {
//                         message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HDonTM.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
//                         txtSoTiepNhan.Text = "";
//                         ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
//                         ccNgayTiepNhan.Text = "";
//                     }
//                     else
//                         btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
//                     this.ShowMessage(message, false);
// 
//                 }
//             }
//             catch (Exception ex)
//             {
//                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
//             }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOA DON.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOADON(ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon)
        {
            if (hoadon == null)
                return false;

            if (hoadon != null)
            {
                txtSoVanDon.Text = hoadon.SoHoaDon;
            }
            if (HDonTM.LoaiKB == 1)
            {
                switch (hoadon.TrangThai)
                {
                    case -1:
                        lbTrangThai.Text = "Chưa Khai Báo";
                        break;
                    case -3:
                        lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                        break;
                    case 0:
                        lbTrangThai.Text = "Chờ Duyệt";
                        break;
                    case 1:
                        lbTrangThai.Text = "Đã Duyệt";
                        break;
                    case 2:
                        lbTrangThai.Text = "Hải quan không phê duyệt";
                        break;

                }
            }
            else
                lbTrangThai.Text = string.Empty;
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                     || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                      || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;

                btnChonHang.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            else if (tkmd.SoToKhai > 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {
                btnKetQuaXuLy.Enabled = true;
                //Re set button declaration LanNT


                bool khaiBaoEnable = (hoadon.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     hoadon.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnChonHang.Enabled = btnXoa.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = hoadon.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   hoadon.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   hoadon.TrangThai == TrangThaiXuLy.CHO_DUYET || hoadon.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }

            txtSoTiepNhan.Text = hoadon.SoTiepNhan > 0 ? hoadon.SoTiepNhan.ToString() : string.Empty;
            if (hoadon.NgayTiepNhan.Year > 1900) ccNgayTiepNhan.Value = hoadon.NgayTiepNhan;
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (HDonTM.GuidStr != null && HDonTM.GuidStr != "")
            {
                ThongDiepForm f = new ThongDiepForm();
                f.DeclarationIssuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                f.ItemID = HDonTM.ID;
                f.ShowDialog(this);
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            List<HoaDonThuongMaiDetail> HoaDonThuongMaiDetailCollection = new List<HoaDonThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonThuongMaiDetail hdtmdtmp = new HoaDonThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HoaDonThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HoaDonThuongMaiDetail hdtmtmp in HoaDonThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HoaDonThuongMaiDetail hdtmd in HDonTM.ListHangMDOfHoaDon)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HDonTM.ListHangMDOfHoaDon.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            List<HoaDonThuongMaiDetail> HoaDonThuongMaiDetailCollection = new List<HoaDonThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonThuongMaiDetail hdtmdtmp = new HoaDonThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HoaDonThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HoaDonThuongMaiDetail hdtmtmp in HoaDonThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HoaDonThuongMaiDetail hdtmd in HDonTM.ListHangMDOfHoaDon)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HDonTM.ListHangMDOfHoaDon.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
        #region  V3

        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(HDonTM.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                HDonTM.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                noidung =  "Hải quan từ chối tiếp nhận.\r\n"+ noidung;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                           // noidung = noidung.Replace(string.Format("Message [{0}]", HDonTM.GuidStr), string.Empty);
                            //this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                HDonTM.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);

                                if (feedbackContent.Acceptance.Length == 10)
                                    HDonTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd", null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    HDonTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                else
                                    HDonTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);

                                HDonTM.NamTiepNhan = HDonTM.NgayTiepNhan.Year;
                                // HDonTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(HDonTM.ID, MessageTitle.KhaiBaoBoSungHoaDonTMDuocChapNhan, noidung);
                                HDonTM.TrangThai = TrangThaiXuLy.CHO_DUYET;
                               noidung = "Hải quan cấp số tiếp nhận\r\nSố tiếp nhận: " + HDonTM.SoTiepNhan.ToString() + "\r\nNgày tiếp nhận: " + HDonTM.NgayTiepNhan.ToString();
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HDonTM.ID, MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong, noidung);
                                HDonTM.TrangThai = TrangThaiXuLy.DA_DUYET;
                                noidung = "Hóa đơn đã được duyệt.\n Thông tin trả về từ hải quan: " + noidung;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HDonTM.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }

                    if (isUpdate)
                    {
                        HDonTM.Update();
                        SetButtonStateHOADON(TKMD, isKhaiBoSung, HDonTM);
                    }
                    msgInfor = noidung;
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion

        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "DVT_ID")
            {
                int val = DonViTinhID(e.Value.ToString());
                if (val != -1)
                    e.Value = Convert.ToInt32(val);
            }
            //else { }

        }
    }
}
