﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    public class ErrorVNACCS
    {
        public string ErrorCode { get; set; }
        public string ColumnError { get; set; }
        public string Loop { get; set; }
        public bool isError { get; set; }
        public ErrorVNACCS(string StrError)
        {
            if (StrError.Length == 15)
            {
                ErrorCode = StrError.Substring(0, 5);
                ColumnError = StrError.Substring(6, 4);
                Loop = StrError.Substring(11, 4);
                if (ErrorCode != "00000" || ColumnError != "0000" || Loop != "0000")
                    isError = true;
                else
                    isError = false;
                if (string.IsNullOrEmpty(ErrorCode) || string.IsNullOrEmpty(ErrorCode.Trim()))
                {
                    isError = false;
                }
            }
        }

        public string loadError()
        {
            XmlDocument docGuideErrorSYS = null;
            string errorStrings = string.Empty;
            if (this.ErrorCode.Substring(0, 1) == "A") //Ma Code bat dau ky tu = A -> loi SYSTEM
            {
                if (docGuideErrorSYS == null)
                    docGuideErrorSYS = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideErrorFile("SYS");

                errorStrings += string.Format("{0}:\r\n- {1}\r\n-> {2}\r\n\n", this.ErrorCode,
                        Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuideErrorDescription(docGuideErrorSYS, this.ErrorCode),
                        Company.KDT.SHARE.VNACCS.HelperVNACCS.GetGuideErrorDisposition(docGuideErrorSYS, this.ErrorCode));
            }

            return !string.IsNullOrEmpty(errorStrings) ? errorStrings : "ErrorCode: " + ErrorCode + "  ErrorColumn: " + ColumnError + " L: " + Loop;
        }
    }
}
