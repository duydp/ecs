﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.DuLieuChuan;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;

namespace Company.Interface
{
    public partial class HopDongFormGCCT : BaseForm
    {

        public bool isKhaiBoSung = false;
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT;
        public Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong HopDongTM = new Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong();
        public HopDongFormGCCT()
        {
            InitializeComponent();

            SetEvent_TextBox_DoiTac();
        }

        private bool checkSoHopDong(string soHopDong)
        {
            foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong HDTM in TKCT.CTKHopDongThuongMaiCollection)
            {
                if (HDTM.SoHopDongTM.Trim().ToUpper() == soHopDong.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HopDongChiTiet> HopDongThuongMaiDetailCollection = new List<HopDongChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongChiTiet hdtmdtmp = new HopDongChiTiet();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HopDongThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HopDongChiTiet hdtmtmp in HopDongThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HopDongChiTiet hdtmd in HopDongTM.ListHangMDOfHopDong)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HopDongTM.ListHangMDOfHopDong.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }

        }
        private void BindData()
        {
            //Company.GC.BLL.KDT.GC.HangChuyenTiep hmd = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
            //hmd.ID_TK = TKCT.ID;

            //dgList.DataSource = HopDongTM.ConvertListToDataSet(hmd.SelectBy_TKMD_ID().Tables[0]);

            dgList.DataSource = HopDongTM.ListHangMDOfHopDong;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (HopDongTM.ListHangMDOfHopDong.Count == 0)
            {
                ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                return;
            }


            TKCT.CTKHopDongThuongMaiCollection.Remove(HopDongTM);

            if (checkSoHopDong(txtSoHopDong.Text))
            {
                if (HopDongTM.ID > 0)
                    TKCT.CTKHopDongThuongMaiCollection.Add(HopDongTM);
                ShowMessage("Số hợp đồng này đã tồn tại.", false);
                return;
            }

            if (!ValidateHopDong())
                return;

            HopDongTM.DiaDiemGiaoHang = txtDiaDiemGiaoHang.Text.Trim();
            HopDongTM.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
            HopDongTM.MaDonViBan = txtMaDVBan.Text.Trim();
            HopDongTM.MaDonViMua = txtMaDVMua.Text.Trim();
            HopDongTM.NgayHopDongTM = ccNgayHopDong.Value;
            HopDongTM.NguyenTe_ID = nguyenTeControl1.Ma;
            HopDongTM.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
            HopDongTM.SoHopDongTM = txtSoHopDong.Text.Trim();
            HopDongTM.TenDonViBan = txtTenDVBan.Text.Trim();
            HopDongTM.TenDonViMua = txtTenDVMua.Text.Trim();
            HopDongTM.ThoiHanThanhToan = ccThoiHanTT.Value;
            HopDongTM.TongTriGia = Convert.ToDecimal(txtTongTriGia.Text);
            HopDongTM.ThongTinKhac = txtThongTinKhac.Text.Trim();
            HopDongTM.ID_TK = TKCT.ID;
            if (isKhaiBoSung)
                HopDongTM.LoaiKB = 1;
            else
                HopDongTM.LoaiKB = 0;
            HopDongTM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            GridEXRow[] rowCollection = dgList.GetRows();
            /*
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (HopDongChiTiet item in HopDongTM.ListHangMDOfHopDong)
                {
                    if (item.HMD_ID.ToString().Trim() == rowview["HMD_ID"].ToString().Trim())
                    {
                        item.GhiChu = rowview["GhiChu"].ToString();

                        item.MaHS = rowview["MaHS"].ToString();
                        item.MaPhu = rowview["MaPhu"].ToString();
                        item.TenHang = rowview["TenHang"].ToString();
                        item.NuocXX_ID = rowview["NuocXX_ID"].ToString();
                        item.DVT_ID = rowview["DVT_ID"].ToString();
                        item.SoLuong = Convert.ToDecimal(rowview["SoLuong"]);
                        item.DonGiaKB = Convert.ToDouble(rowview["DonGiaKB"]);
                        item.TriGiaKB = Convert.ToDouble(rowview["TriGiaKB"]);

                        break;
                    }
                }
            }
            */

            try
            {
                HopDongTM.LoaiTK = "TKCT";
                HopDongTM.InsertUpdateFull();
                TKCT.CTKHopDongThuongMaiCollection.Add(HopDongTM);
                BindData();
                ShowMessage("Lưu thành công.", false);
                //if (TKCT.ActionStatus != TrangThaiXuLy.SUATKDADUYET)
                //    btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }


        }
        public HangChuyenTiepCollection ConvertListToHangMauDichCollection(List<HopDongChiTiet> listHangHopDong, HangChuyenTiepCollection hangCollection)
        {

            HangChuyenTiepCollection tmp = new HangChuyenTiepCollection();

            foreach (HopDongChiTiet item in listHangHopDong)
            {
                foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichFormGCCT f = new SelectHangMauDichFormGCCT();
            f.TKCT = TKCT;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất HMD
            //if (HopDongTM.ListHangMDOfHopDong.Count > 0)
            //{
            //    f.TKCT.HMDCollection = ConvertListToHangMauDichCollection(HopDongTM.ListHangMDOfHopDong, TKCT.HMDCollection);
            //}
            f.ShowDialog();
            if (f.HCTCollection.Count > 0)
            {
                foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HMD in f.HCTCollection)
                {
                    bool ok = false;
                    foreach (HopDongChiTiet hangHopDongDetail in HopDongTM.ListHangMDOfHopDong)
                    {
                        if (hangHopDongDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HopDongChiTiet hopDongDetail = new HopDongChiTiet();
                        hopDongDetail.HMD_ID = HMD.ID;
                        hopDongDetail.HopDongTM_ID = HopDongTM.ID;
                        hopDongDetail.MaPhu = HMD.MaHang;
                        hopDongDetail.MaHS = HMD.MaHS;
                        hopDongDetail.TenHang = HMD.TenHang;
                        hopDongDetail.DVT_ID = HMD.ID_DVT;
                        hopDongDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hopDongDetail.SoLuong = HMD.SoLuong;
                        hopDongDetail.NuocXX_ID = HMD.ID_NuocXX;
                        hopDongDetail.DonGiaKB = Convert.ToDouble(HMD.DonGia);
                        hopDongDetail.TriGiaKB = Convert.ToDouble(HMD.TriGia);
                        HopDongTM.ListHangMDOfHopDong.Add(hopDongDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void HopDongForm_Load(object sender, EventArgs e)
        {
            dgList.RootTable.Columns["MaPhu"].Selectable = false;

            // an cac button truoc khi load
            btnKhaiBao.Enabled = false;
            btnLayPhanHoi.Enabled = false;

            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll();
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll();
            cbDKGH.DisplayMember = cbDKGH.ValueMember = "ID";
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            if (HopDongTM != null && HopDongTM.ID > 0)
            {
                txtDiaDiemGiaoHang.Text = HopDongTM.DiaDiemGiaoHang;
                cbDKGH.SelectedValue = HopDongTM.DKGH_ID;
                txtMaDVBan.Text = HopDongTM.MaDonViBan;
                txtMaDVMua.Text = HopDongTM.MaDonViMua;
                ccNgayHopDong.Value = HopDongTM.NgayHopDongTM;
                nguyenTeControl1.Ma = HopDongTM.NguyenTe_ID;
                cbPTTT.SelectedValue = HopDongTM.PTTT_ID;
                txtSoHopDong.Text = HopDongTM.SoHopDongTM;
                txtTenDVBan.Text = HopDongTM.TenDonViBan;
                txtTenDVMua.Text = HopDongTM.TenDonViMua;
                ccThoiHanTT.Value = HopDongTM.ThoiHanThanhToan;
                txtTongTriGia.Text = HopDongTM.TongTriGia.ToString();
                txtThongTinKhac.Text = HopDongTM.ThongTinKhac;
                ccNgayHopDong.Text = HopDongTM.NgayHopDongTM.ToShortDateString();
                ccThoiHanTT.Text = HopDongTM.ThoiHanThanhToan.ToShortDateString();
                BindData();
            }
            if (HopDongTM.SoTiepNhan > 0)
            {
                txtSoTiepNhan.Text = HopDongTM.SoTiepNhan + "";
                ccNgayTiepNhan.Text = HopDongTM.NgayTiepNhan.ToShortDateString();
            }
            if (TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                //btnChonHang.Visible = false;
                //btnGhi.Visible = false;
                //btnXoa.Visible = false;
            }

            //Set ValueList
            Globals.FillHSValueList(this.dgList.RootTable.Columns["MaHS"]);
            Globals.FillDonViTinhValueList(this.dgList.RootTable.Columns["DVT_ID"]);
            Globals.FillNuocXXValueList(this.dgList.RootTable.Columns["NuocXX_ID"]);

            //HUNGTQ, Update 07/06/2010. 
            //Thiết lập trạng thái các nút trên form
            SetButtonStateHOPDONG(TKCT, isKhaiBoSung, HopDongTM);

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (HopDongTM.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                bool thanhcong = HopDongTM.WSKhaiBaoBoSungHopDong(password, TKCT.MaHaiQuanTiepNhan, (long)TKCT.SoToKhai, TKCT.MaLoaiHinh, TKCT.NamDK, TKCT.ID, TKCT.MaDoanhNghiep, null, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao, TKCT.ConvertHMDKDToHangMauDich());
                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                if (this.HopDongTM.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.HopDongTM.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }

            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = HopDongTM.WSLaySoTiepNhan(password);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HopDongTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHopDongThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.HopDongTM.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.HopDongTM.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.HopDongTM.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = HopDongTM.WSLayPhanHoi(password);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HopDongTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHopDongDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HopDongTM.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    this.ShowMessage(message, false);
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        #region Begin Doi tac TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick, Leave cho các TextBox Mã đơn vị mua, bán.
        /// </summary>
        /// Hungtq, Update 30052010
        private void SetEvent_TextBox_DoiTac()
        {
            txtMaDVMua.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtMaDVBan.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;

            txtMaDVMua.ButtonClick += new EventHandler(txtMaDVMua_ButtonClick);

            txtMaDVBan.ButtonClick += new EventHandler(txtMaDVBan_ButtonClick);

            txtMaDVMua.Leave += new EventHandler(txtMaDVMua_Leave);

            txtMaDVBan.Leave += new EventHandler(txtMaDVBan_Leave);
        }

        private void txtMaDVMua_ButtonClick(object sender, EventArgs e)
        {
            Company.GC.BLL.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaDVMua.Text = objDoiTac.MaCongTy;
                txtTenDVMua.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVBan_ButtonClick(object sender, EventArgs e)
        {
            Company.GC.BLL.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaDVBan.Text = objDoiTac.MaCongTy;
                txtTenDVBan.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVMua_Leave(object sender, EventArgs e)
        {
            if (txtMaDVMua.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVMua.Text.Trim()) != "")
                txtTenDVMua.Text = DoiTac.GetName(txtMaDVMua.Text.Trim());
        }

        private void txtMaDVBan_Leave(object sender, EventArgs e)
        {
            if (txtMaDVBan.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVBan.Text.Trim()) != "")
                txtTenDVBan.Text = DoiTac.GetName(txtMaDVBan.Text.Trim());
        }

        #endregion End Doi tac TextBox

        #region Begin VALIDATE HOP DONG

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateHopDong()
        {
            bool isValid = true;

            //So_CT	varchar(50)
            isValid = Globals.ValidateLength(txtSoHopDong, 50, err, "Số hợp đồng");

            //Ma_PTTT	varchar(10)
            isValid &= Globals.ValidateLength(cbPTTT, 10, err, "Phương thức thanh toán");

            //Ma_GH	varchar(7)
            isValid &= Globals.ValidateLength(cbDKGH, 7, err, "Điều kiện giao hàng");

            //Ma_NT	char(3)

            //Ma_DV	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVMua, 14, err, "Mã đơn vị mua");

            //Ma_DV_DT	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVBan, 14, err, "Mã đơn vị bán");

            return isValid;
        }

        #endregion End VALIDATE HOP DONG

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOP DONG.
        /// </summary>
        /// <param name="tkct"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOPDONG(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hopdong)
        {
            if (hopdong == null)
                return false;

            bool status = false;
            if (TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {
                //Khai bao moi
                if (isKhaiBoSung == false)
                {
                    //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                    status = (tkct.SoTiepNhan == 0);

                    btnXoa.Enabled = status;
                    btnChonHang.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXuLy.Enabled = false;
                    btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                }
                //Khai bao bo sung
                else
                {
                    //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                    if (tkct.SoToKhai == 0)
                    {
                        //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                        //Globals.ShowMessageTQDT(msg, false);

                        //return false;
                    }
                    else
                    {
                        status = true;

                        btnXoa.Enabled = status;
                        btnChonHang.Enabled = status;
                        btnGhi.Enabled = status;
                        btnKetQuaXuLy.Enabled = true;
                        //Neu hop dong chua co so tiep nhan -> phai khai bao
                        if (hopdong.SoTiepNhan == 0)
                        {
                            btnKhaiBao.Enabled = true;
                            btnLayPhanHoi.Enabled = false;
                        }
                        //Neu hop dong da co so tiep nhan -> co the lay phan hoi
                        else
                        {
                            btnKhaiBao.Enabled = false;
                            btnLayPhanHoi.Enabled = true;
                        }


                    }
                }
            }
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (HopDongTM.GuidStr != null && HopDongTM.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(HopDongTM.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }
    }
}

