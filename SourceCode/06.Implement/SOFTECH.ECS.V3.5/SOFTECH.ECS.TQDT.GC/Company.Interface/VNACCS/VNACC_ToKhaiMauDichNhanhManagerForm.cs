﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.VNACCS
{
    public partial class VNACC_ToKhaiMauDichNhanhManagerForm : Company.Interface.BaseForm
    {
        public List<KDT_VNACC_ToKhaiMauDich> TKMDCollection = new List<KDT_VNACC_ToKhaiMauDich>();
        public VNACC_ToKhaiMauDichNhanhManagerForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = TKMDCollection;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }

        private void VNACC_ToKhaiMauDichNhanhManagerForm_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                    TKMD = (KDT_VNACC_ToKhaiMauDich)grList.GetRow().DataRow;
                    TKMD.LoadFull();
                    string loaiHinh = "";
                    DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                    DataRow dr = ds.Tables[0].Rows[0];
                    loaiHinh = dr["ReferenceDB"].ToString();

                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
                    {
                        VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                    }
                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
                    {
                        VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
                {
                    string pl = e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString();
                    if (pl == "1")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Xanh";
                    else if (pl == "2")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Vàng";
                    else if (pl == "3")
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Đỏ";
                    else
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Chưa phân luồng";
                }
                if (e.Row.Cells["NgayPhatHanhHD"].Value != null)
                {
                    DateTime ngayHD = System.Convert.ToDateTime(e.Row.Cells["NgayPhatHanhHD"].Value.ToString());
                    if (ngayHD.Year == 1900)
                        e.Row.Cells["NgayPhatHanhHD"].Text = "";
                }
                if (e.Row.Cells["NgayDangKy"].Value != null)
                {
                    DateTime ngaydk = System.Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value.ToString());
                    if (ngaydk.Year == 1900)
                        e.Row.Cells["NgayDangKy"].Text = "";
                }
                if (grList.RootTable.Columns.Contains("MaDiaDiemDoHang"))
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells["MaDiaDiemDoHang"].Text))
                    {
                        if (e.Row.Cells["MaDiaDiemDoHang"].Text.Trim().Length == 5)
                            e.Row.Cells["MaDiaDiemDoHang"].Text = e.Row.Cells["MaDiaDiemDoHang"].Text.Substring(0, 2);
                        else
                            e.Row.Cells["MaDiaDiemDoHang"].Text = "VN";
                    }
                    else
                        e.Row.Cells["MaDiaDiemDoHang"].Text = "VN";
                }
                if (grList.RootTable.Columns.Contains("MaDiaDiemXepHang"))
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells["MaDiaDiemXepHang"].Text))
                    {
                        if (e.Row.Cells["MaDiaDiemXepHang"].Text.Trim().Length == 5)
                            e.Row.Cells["MaDiaDiemXepHang"].Text = e.Row.Cells["MaDiaDiemXepHang"].Text.Substring(0, 2);
                        else
                            e.Row.Cells["MaDiaDiemXepHang"].Text = "VN";
                    }
                    else
                        e.Row.Cells["MaDiaDiemXepHang"].Text = "VN";
                }
#if GC_V4
                if (e.Row.Cells["HopDong_So"].Value != null)
                {
                    //HopDong hd = HopDong.Load(TKMD.HopDong_ID);
                    try
                    {
                        int HD = 0;
                        HD = Convert.ToInt32(e.Row.Cells["HopDong_So"].Value.ToString());
                        HopDong hd = HopDong.Load(HD);
                        e.Row.Cells["HopDong_So"].Text = hd.SoHopDong;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
#endif
            }
        }
    }
}
