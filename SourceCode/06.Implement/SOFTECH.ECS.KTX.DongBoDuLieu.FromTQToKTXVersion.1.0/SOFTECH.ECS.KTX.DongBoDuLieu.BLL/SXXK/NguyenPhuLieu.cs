﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Company.BLL.SXXK;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace DongBoDuLieu.BLL.SXXK
{
    public class NPL
    {
        public static DataSet GetALLDanhSachNPL(string MaHaiQuan, string MaDoanhNghiep, string matKhau, bool isPhongKhai)
        {
            string query = "SELECT NPL.Ma_HQ AS MaHaiQuan, " +
                         "NPL.Ma_NPL AS Ma, " +
                         "NPL.Ten_NPL AS Ten, " +
                         "NPL.Ma_HS AS MaHS, " +
                         "NPL.MA_DVT AS DVT_ID " +
                         "FROM SNPL NPL " +
                         "WHERE NPL.MA_HQ = '" + MaHaiQuan + "' AND MA_DV = '" + MaDoanhNghiep + "'";
            DataSet ds = null;
            if (!isPhongKhai)
                ds = WebServiceDB.getDataBySQL(query, MaHaiQuan, "SXXK", MaDoanhNghiep, matKhau);
            else
                ds = WebServiceDB.getDataBySQLDatabase(query, MaHaiQuan, "SXXK");
            return ds;
        }

        public static DataSet GetALLDanhSachNPLLocal(string MaHaiQuan, string MaDoanhNghiep, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = "SELECT NPL.MaHaiQuan AS MaHaiQuan, " +
                            "NPL.Ma AS Ma, " +
                            "NPL.Ten AS Ten, " +
                            "NPL.MaHS AS MaHS, " +
                            "NPL.DVT_ID AS DVT_ID  " +
                            "FROM dbo.t_SXXK_NguyenPhuLieu NPL  " +
                            "WHERE NPL.MaHaiQuan = '" + MaHaiQuan + "' AND npl.MaDoanhNghiep = '" + MaDoanhNghiep + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.Ma FROM [" + databaseNameTarget + "].dbo.t_SXXK_NguyenPhuLieu temp WHERE temp.Ma = NPL.Ma )";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);

            return ds;
        }

        public static DataSet GetALLDanhSachNPLLocalTQDT(string MaHaiQuan, string MaDoanhNghiep, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay, bool soSanhDuLieu, string databaseNameTarget)
        {
            string query = "	SELECT  NPL.Master_ID,  " +
                            "			NPLDK.SoTiepNhan, NPLDK.NgayTiepNhan, NPLDK.TrangThaiXuLy,  " +
                            "			NPL.Ma AS Ma,  " +
                            "			NPL.Ten AS Ten,  " +
                            "			NPL.MaHS AS MaHS,  " +
                            "			NPL.DVT_ID AS DVT_ID  " +
                            "	FROM    dbo.t_KDT_SXXK_NguyenPhuLieu NPL " +
                            "			INNER JOIN dbo.t_KDT_SXXK_NguyenPhuLieuDangKy NPLDK ON NPL.Master_ID = NPLDK.ID " +
                            "	WHERE   NPLDK.MaHaiQuan = '" + MaHaiQuan + "' " +
                            "			AND NPLDK.MaDoanhNghiep = '" + MaDoanhNghiep + "' ";
            if (trangThaiDaDuyet)
                query += "			AND npldk.TrangThaiXuLy = 1";
            if (tuNgay > DateTime.MinValue)
                query += " and npldk.NgayTiepNhan >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND npldk.NgayTiepNhan <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            if (soSanhDuLieu)
                query += " AND NOT EXISTS ( SELECT temp.Ma FROM [" + databaseNameTarget + "].dbo.t_KDT_SXXK_NguyenPhuLieu temp WHERE temp.Ma = NPL.Ma )";
            query += " ORDER BY npldk.NgayTiepNhan";

            DataSet ds = WebServiceDB.getDataBySQLDatabaseLocal(query, MaHaiQuan);

            return ds;
        }

        /// <summary>
        /// Cập nhập thông tin NPL nếu có thì ghi đè, không có thì insert
        /// </summary>
        /// <param name="MaHaiQuan"></param>
        /// <param name="MaDoanhNghiep"></param>
        /// <param name="ds"></param>
        public static void InsertDuLieu(string MaHaiQuan, string MaDoanhNghiep, DataSet ds)
        {
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.MaHaiQuan = MaHaiQuan;
                npl.MaDoanhNghiep = MaDoanhNghiep;
                npl.Ma = row["Ma"].ToString().Trim();
                npl.Ten = row["Ten"].ToString().Trim();
                npl.MaHS = row["MaHS"].ToString().Trim();
                int k = npl.MaHS.Length;
                ////if (npl.MaHS.Length < 10)
                ////{
                ////    for (int i = 1; i <= 10 - k; ++i)
                ////        npl.MaHS += "0";
                ////}
                npl.DVT_ID = row["DVT_ID"].ToString();
                collection.Add(npl);
            }
            NguyenPhuLieu NPL = new NguyenPhuLieu();
            NPL.InsertUpdate(collection);
        }

        public static void InsertDuLieu(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.MaHaiQuan = MaHaiQuan;
                npl.MaDoanhNghiep = MaDoanhNghiep;
                npl.Ma = row["Ma"].ToString().Trim();
                npl.Ten = row["Ten"].ToString().Trim();
                npl.MaHS = row["MaHS"].ToString().Trim();
                int k = npl.MaHS.Length;
                //if (npl.MaHS.Length < 10)
                //{
                //    for (int i = 1; i <= 10 - k; ++i)
                //        npl.MaHS += "0";
                //}
                npl.DVT_ID = row["DVT_ID"].ToString();
                collection.Add(npl);
            }

            InsertUpdate(collection, connectionString);
        }

        //---------------------------------------------------------------------------------------------

        public static void InsertDuLieuTQDT(string MaHaiQuan, string MaDoanhNghiep, DataSet ds, string connectionString)
        {
            try
            {
                Company.BLL.KDT.SXXK.NguyenPhuLieuDangKyCollection kdtNPLDKCollection = new Company.BLL.KDT.SXXK.NguyenPhuLieuDangKyCollection();

                //Create collection NguyenPhuLieuDangKy
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy nplDK = Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy.Load(long.Parse(row["Master_ID"].ToString()));

                    if (nplDK != null)
                    {
                        //Load NPL cua NPL DK.
                        nplDK.LoadNPLCollection();

                        kdtNPLDKCollection.Add(nplDK);
                    }
                }

                //Insert data NguyenPhuLieuDangKy
                int i = 0;
                foreach (Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy item in kdtNPLDKCollection)
                {
                    i += 1;
                    item.InsertUpdateFull(connectionString, i);
                }

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //---------------------------------------------------------------------------------------------

        public static bool InsertUpdate(NguyenPhuLieuCollection collection, string connectionString)
        {
            bool ret;

            SqlDatabase db = new SqlDatabase(connectionString);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    int i = 0;
                    bool ret01 = true;
                    foreach (NguyenPhuLieu item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction, db) <= 0)
                        {
                            ret01 = false;
                            break;
                        }

                        //TODO: HungTQ, Update 27/04/2010.
                        i += 1;
                        Logger.LocalLogger.Instance().WriteMessage(new Exception(string.Format("{6}.SXXK Nguyen phu lieu: MaHaiQuan={4}, MaDoanhNghiep={5}, Ma={0}, Ten={1}, DVT={2}, MaHS={3} ", item.Ma, item.Ten, item.DVT_ID, item.MaHS, item.MaHaiQuan, item.MaDoanhNghiep, i)));
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

    }
}
