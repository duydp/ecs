﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{

    public partial class FrmCauHinhChuKySo : BaseForm
    {
        //public event EventHandler<EventArgs> HandlerConfig;
        public string madoanhnghiep = GlobalSettings.MA_DON_VI;
        public static int versionHD = 0;
        public FrmCauHinhChuKySo()
        {
            InitializeComponent();
        }

        private void FrmCauHinhChuKySo_Load(object sender, EventArgs e)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(Application.StartupPath + "\\Config.xml");
            XmlNode node = doc.SelectSingleNode("Root/Type");
            versionHD = Convert.ToInt32(node.InnerText);
            try
            {
                if (versionHD == 0)
                {
                    this.Cursor = Cursors.WaitCursor;
                    btnRefersh_Click(null, null);

                    cbSigns.SelectedValue = Company.KDT.SHARE.Components.Globals.GetX509CertificatedName;
                    chkIsUseSign.Checked = Company.KDT.SHARE.Components.Globals.IsSignature;
                    chIsRemember.Checked = Company.KDT.SHARE.Components.Globals.IsRememberSign;
                    cbSigns.SelectedValue = Company.KDT.SHARE.Components.Globals.GetX509CertificatedName;
                    txtPassword.Text = Company.KDT.SHARE.Components.Globals.PasswordSign;


                    txtPasswordSign.Text = Company.KDT.SHARE.Components.Globals.PasswordSignRemote;
                    txtUserNameSign.Text = Company.KDT.SHARE.Components.Globals.UserNameSignRemote;
                    chIsUseSignRemote.Checked = Company.KDT.SHARE.Components.Globals.IsSignRemote;

                    txtUserNameSmartCA.Text = Company.KDT.SHARE.Components.Globals.UserNameSmartCA;
                    txtPasswordSmartCA.Text = Company.KDT.SHARE.Components.Globals.PaswordSmartCA;
                    chkSmartCA.Checked = Company.KDT.SHARE.Components.Globals.IsSignSmartCA;

                    this.chkIsUseSign.CheckedChanged += new System.EventHandler(this.chkIsUseSign_CheckedChanged);
                    this.chIsUseSignRemote.CheckedChanged += new System.EventHandler(this.chIsUseSignRemote_CheckedChanged);
                    chIsSignOnLan.CheckedChanged += new EventHandler(chIsSignOnLan_CheckedChanged);
                    chkSmartCA.CheckedChanged += new EventHandler(chkSmartCA_CheckedChanged);

                    string strConnString = "server=" + GlobalSettings.SERVER_NAME;
                    strConnString += ";User Id=" + GlobalSettings.USER + ";Password=" + GlobalSettings.PASS;

                    Company.KDT.SHARE.Components.SQL.InitializeServer(strConnString);
                    foreach (string dbName in Company.KDT.SHARE.Components.SQL.ListDatabases())
                    {
                        cbDataSignName.Items.Add(dbName);
                    }
                    cbDataSignName.SelectedValue = Company.KDT.SHARE.Components.Globals.DataSignLan;
                    chIsSignOnLan.Checked = Company.KDT.SHARE.Components.Globals.IsSignOnLan;
                    chkSmartCA.Checked = Company.KDT.SHARE.Components.Globals.IsSignSmartCA;

                }
                else
                {

                    this.Cursor = Cursors.WaitCursor;
                    btnRefersh_Click(null, null);

                    cbSigns.SelectedValue = Company.KDT.SHARE.Components.Globals.GetX509CertificatedName;
                    chkIsUseSign.Checked = Company.KDT.SHARE.Components.Globals.IsSignature;
                    chIsRemember.Checked = Company.KDT.SHARE.Components.Globals.IsRememberSign;
                    cbSigns.SelectedValue = Company.KDT.SHARE.Components.Globals.GetX509CertificatedName;
                    txtPassword.Text = Company.KDT.SHARE.Components.Globals.PasswordSign;


                    txtPasswordSign.Text = Company.KDT.SHARE.Components.Globals.PasswordSignRemote;
                    txtUserNameSign.Text = Company.KDT.SHARE.Components.Globals.UserNameSignRemote;
                    chIsUseSignRemote.Checked = Company.KDT.SHARE.Components.Globals.IsSignRemote;

                    txtUserNameSmartCA.Text = Company.KDT.SHARE.Components.Globals.UserNameSmartCA;
                    txtPasswordSmartCA.Text = Company.KDT.SHARE.Components.Globals.PaswordSmartCA;
                    chkSmartCA.Checked = Company.KDT.SHARE.Components.Globals.IsSignSmartCA;

                    this.chkIsUseSign.CheckedChanged += new System.EventHandler(this.chkIsUseSign_CheckedChanged);
                    this.chIsUseSignRemote.CheckedChanged += new System.EventHandler(this.chIsUseSignRemote_CheckedChanged);
                    chIsSignOnLan.CheckedChanged += new EventHandler(chIsSignOnLan_CheckedChanged);
                    chkSmartCA.CheckedChanged += new EventHandler(chkSmartCA_CheckedChanged);

                    string strConnString = "server=" + GlobalSettings.SERVER_NAME;
                    strConnString += ";User Id=" + GlobalSettings.USER + ";Password=" + GlobalSettings.PASS;

                    Company.KDT.SHARE.Components.SQL.InitializeServer(strConnString);
                    foreach (string dbName in Company.KDT.SHARE.Components.SQL.ListDatabases())
                    {
                        cbDataSignName.Items.Add(dbName);
                    }
                    cbDataSignName.SelectedValue = Company.KDT.SHARE.Components.Globals.DataSignLan;
                    chIsSignOnLan.Checked = Company.KDT.SHARE.Components.Globals.IsSignOnLan;

                    // uiTabPage1.Enabled = false;
                    //uiGroupBox1.Enabled = false;
                    uiTabPage3.Selected = true;
                    txtUserNameSign.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(madoanhnghiep, "UserNameSignRemote").Value_Config.ToString();
                    txtPasswordSign.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(madoanhnghiep, "PasswordSignRemote").Value_Config.ToString();
                    chIsUseSignRemote.Checked = Convert.ToBoolean(Company.KDT.SHARE.Components.HeThongPhongKhai.Load(madoanhnghiep, "IsSignRemote").Value_Config.ToString());


                    chIsUseSignRemote.CheckedValue = GlobalSettings.iSignRemote;
                    chkSmartCA.Checked = Company.KDT.SHARE.Components.Globals.IsSignSmartCA;

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }
        //protected override void OnClosing(CancelEventArgs e)
        //{
        //    if (HandlerConfig != null)
        //    {

        //            e.Cancel = true;
        //         bool KyTrucTiep = Company.KDT.SHARE.Components.Globals.IsSignature;
        //        bool KyTuXa = Company.KDT.SHARE.Components.Globals.IsSignRemote;
        //        bool KyMangLan = Company.KDT.SHARE.Components.Globals.IsSignOnLan;
        //        if (KyTrucTiep == false && KyTuXa == false && KyMangLan == false)
        //        {
        //            ShowMessageTQDT("Chưa cchonj", false);
        //            return;
        //        }
        //    }

        //    base.OnClosing(e);
        //}
        void chIsSignOnLan_CheckedChanged(object sender, EventArgs e)
        {
            if (chIsSignOnLan.CheckState == CheckState.Checked)
            {
                if (chIsUseSignRemote.Checked || chkIsUseSign.Checked || chkSmartCA.Checked)
                {
                    chIsUseSignRemote.CheckState = CheckState.Unchecked;
                    chkIsUseSign.CheckState = CheckState.Unchecked;
                    chkSmartCA.CheckState = CheckState.Unchecked;
                }
            }
            //if (chIsUseSignRemote.Checked || chkIsUseSign.Checked)
            //{
            //    ShowMessage("Hệ thống chỉ cho phép bạn chọn một trong các cách ký số\r\nHệ thống sẽ bỏ kích hoạt các chức năng còn lại", false);
            //    chIsUseSignRemote.CheckedChanged -= new EventHandler(chIsUseSignRemote_CheckedChanged);
            //    chkIsUseSign.CheckedChanged -= new EventHandler(chkIsUseSign_CheckedChanged);
            //    chIsUseSignRemote.Checked = false;
            //    chkIsUseSign.Checked = false;
            //    chIsUseSignRemote.CheckedChanged += new EventHandler(chIsUseSignRemote_CheckedChanged);
            //    chkIsUseSign.CheckedChanged += new EventHandler(chkIsUseSign_CheckedChanged);
            //}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (versionHD == 1)
                {
                    Company.KDT.SHARE.Components.HeThongPhongKhai htpk = new Company.KDT.SHARE.Components.HeThongPhongKhai();
                    htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                    htpk.PassWord = "0";

                    htpk.Key_Config = "IsSignRemote";
                    htpk.Value_Config = chIsUseSignRemote.Checked == true ? "true" : "false";
                    htpk.InsertUpdate();
                    htpk.Key_Config = "PasswordSignRemote";
                    htpk.Value_Config = txtPasswordSign.Text;
                    htpk.InsertUpdate();
                    htpk.Key_Config = "UserNameSignRemote";
                    htpk.Value_Config = txtUserNameSign.Text;
                    htpk.InsertUpdate();

                }

                if (chkIsUseSign.Checked && string.IsNullOrEmpty(cbSigns.Text))
                {
                    if (ShowMessage("Chưa có chữ ký số được chọn\r\nBs muốn lưu không?", true) != "Yes")
                        return;
                }
                if (chkIsUseSign.CheckState == CheckState.Checked)
                {
                    if (!CheckUSB())
                    {
                        ShowMessage("Mã PIN của chữ ký số không đúng Hoặc\nThiết bị USB Token đã bị tháo ra vui lòng gắn thiết bị USB Token vào .", false);
                        return;
                    }
                }
                #region Set Value
                //Ky truc tiep

                Company.KDT.SHARE.Components.Globals.IsSignature = chkIsUseSign.Checked;
                Company.KDT.SHARE.Components.Globals.IsRememberSign = chIsRemember.Checked;
                Company.KDT.SHARE.Components.Globals.GetX509CertificatedName = (cbSigns.SelectedValue == null) ? "" : cbSigns.SelectedValue.ToString();
                Company.KDT.SHARE.Components.Globals.PasswordSign = txtPassword.Text;

                //Ky tu xa
                Company.KDT.SHARE.Components.Globals.IsSignRemote = chIsUseSignRemote.Checked;
                if (!Company.KDT.SHARE.Components.Globals.PasswordSignRemote.Equals(txtPasswordSign.Text.Trim()))
                    Company.KDT.SHARE.Components.Globals.PasswordSignRemote = Company.KDT.SHARE.Components.Helpers.GetMD5Value(txtPasswordSign.Text.Trim());
                Company.KDT.SHARE.Components.Globals.UserNameSignRemote = txtUserNameSign.Text;
                //Ky noi bo
                Company.KDT.SHARE.Components.Globals.DataSignLan = (cbDataSignName.SelectedValue == null) ? "" : cbDataSignName.SelectedValue.ToString();
                Company.KDT.SHARE.Components.Globals.IsSignOnLan = chIsSignOnLan.Checked;

                //Ký SmartCA
                Company.KDT.SHARE.Components.Globals.IsSignSmartCA = chkSmartCA.Checked;
                if (Company.KDT.SHARE.Components.Globals.PaswordSmartCA != txtPasswordSmartCA.Text.Trim())
                    Company.KDT.SHARE.Components.Globals.PaswordSmartCA = txtPasswordSmartCA.Text.Trim();
                Company.KDT.SHARE.Components.Globals.UserNameSmartCA = txtUserNameSmartCA.Text;
                #endregion

                #region Save Value
                //Ký trực tiếp

                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsRememberSign", chIsRemember.Checked == true ? "true" : "false");
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SuDungChuKySo", chkIsUseSign.Checked == true ? "true" : "false");
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ChuKySo", (cbSigns.SelectedValue == null) ? "" : cbSigns.SelectedValue.ToString());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PasswordSign", Helpers.EncryptString(txtPassword.Text, "KEYWORD"));

                // Ký tư xa
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsSignRemote", chIsUseSignRemote.Checked == true ? "true" : "false");
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("UserNameSignRemote", txtUserNameSign.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PasswordSignRemote", Helpers.EncryptString(Company.KDT.SHARE.Components.Globals.PasswordSignRemote, "KEYWORD"));
                //Ky nội bộ
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsSignOnLan", Company.KDT.SHARE.Components.Globals.IsSignOnLan);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DataSignLan", Company.KDT.SHARE.Components.Globals.DataSignLan);

                // Ký SmartCA
                if (String.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.GetConfig("IsSignSmartCA")))
                {
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("IsSignSmartCA", chkSmartCA.Checked == true ? "true" : "false");
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("UserNameSmartCA", txtUserNameSmartCA.Text);
                    Company.KDT.SHARE.Components.Globals.AddNodeConfig("PassWordSmartCA", txtPasswordSmartCA.Text);
                }
                else
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsSignSmartCA", chkSmartCA.Checked == true ? "true" : "false");
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("UserNameSmartCA", txtUserNameSmartCA.Text);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PassWordSmartCA", txtPasswordSmartCA.Text);
                }

                #endregion

                //bool KyTrucTiep = Company.KDT.SHARE.Components.Globals.IsSignature;
                //bool KyTuXa = Company.KDT.SHARE.Components.Globals.IsSignRemote;
                //bool KyMangLan = Company.KDT.SHARE.Components.Globals.IsSignOnLan;
                //if (Company.KDT.SHARE.Components.Globals.SuDungHttps == false)
                //{
                //    if (KyTrucTiep == true || KyTuXa == true || KyMangLan == true)
                //    {
                //        string URI = "https://tqdttntt.customs.gov.vn/KDTService/Service.asmx";
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaChiWS", URI);
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("Company_BLL_WS_KhaiDienTu_KDTService", URI);
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("QuanLyChungTu_WS_KDTService", URI);
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WS_URL", URI);
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SuDungHttps", "true");
                //        Company.KDT.SHARE.Components.Globals.SuDungHttps = true;


                //    }

                //}
                //else
                //{
                //    if (KyTrucTiep == false && KyTuXa == false && KyMangLan == false)
                //    {
                //        string maCuc = GlobalSettings.MA_CUC_HAI_QUAN;

                //        Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc objCucHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.Load(maCuc);
                //        string URI = (objCucHQ.IPService != "" ? objCucHQ.IPService : "?");
                //        if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                //        {
                //            URI = URI + "/" + objCucHQ.ServicePathV3;
                //        }
                //        else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                //        {
                //            URI = URI + "/" + objCucHQ.ServicePathV2;
                //        }
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaChiWS", URI);
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("Company_BLL_WS_KhaiDienTu_KDTService", URI);
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("QuanLyChungTu_WS_KDTService", URI);
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WS_URL", URI);
                //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SuDungHttps", "false");
                //        Company.KDT.SHARE.Components.Globals.SuDungHttps = false;

                //    }
                //}

                ShowMessage("Lưu cấu hình chữ ký số thành công.", false);
                GlobalSettings.RefreshKey();
                this.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }
        private string GetName(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            if (!string.IsNullOrEmpty(mst)) return string.Format("{0} - [{1}]", name, mst);
            else return string.Format("{0}", name);
        }
        private void btnRefersh_Click(object sender, EventArgs e)
        {
            List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = Cryptography.GetX509CertificatedNames();
            cbSigns.Items.Clear();
            foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
            {
                string signName = item.SubjectName.Name.ToString();
                string name = "";
                name = GetName(signName);
                cbSigns.Items.Add(new Janus.Windows.EditControls.UIComboBoxItem() { Text = name, Value = signName });
            }

        }

        private void cbChuKySo_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                string signName = cbSigns.SelectedValue.ToString();
                X509Certificate2 x059 = Cryptography.GetStoreX509Certificate2(signName);

                if (x059 != null)
                {
                    lblNhaCungCap.Text = GetName(x059.IssuerName.Name);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string Check()
        {
            string msg = string.Empty;
            try
            {
                Company.KDT.SHARE.Components.Globals.GetX509CertificatedName = cbSigns.SelectedValue.ToString();
                Company.KDT.SHARE.Components.Globals.PasswordSign = txtPassword.Text;
                Company.KDT.SHARE.Components.Globals.IsSignature = true;
                Helpers.GetSignature("Test");
                btnCheckConnect.ImageIndex = 0;
                return "Mật khẩu được xác nhận là hợp lệ";
            }
            catch (Exception ex)
            {
                btnCheckConnect.ImageIndex = 1;
                return ex.Message;
            }
        }
        private bool CheckUSB()
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.GetX509CertificatedName = cbSigns.SelectedValue.ToString();
                Company.KDT.SHARE.Components.Globals.PasswordSign = txtPassword.Text;
                Company.KDT.SHARE.Components.Globals.IsSignature = true;
                Helpers.GetSignature("Test");
                btnCheckConnect.ImageIndex = 0;
                return true;
            }
            catch (Exception ex)
            {
                btnCheckConnect.ImageIndex = 1;
                return false;
            }
        }
        private void btnCheckConnect_Click(object sender, EventArgs e)
        {
            ShowMessage(Check(), false);
        }
        private void btnSignRemote_Click(object sender, EventArgs e)
        {
            try
            {
                ISignMessage service = WebService.SignMessage();
                if (!Company.KDT.SHARE.Components.Globals.PasswordSignRemote.Equals(txtPasswordSign.Text.Trim()))
                    Company.KDT.SHARE.Components.Globals.PasswordSignRemote = Company.KDT.SHARE.Components.Helpers.GetMD5Value(txtPasswordSign.Text.Trim());
                bool valid = service.IsExist(txtUserNameSign.Text.Trim(), Company.KDT.SHARE.Components.Globals.PasswordSignRemote);
                if (!valid)
                {
                    ShowMessage("Tên đăng nhập hoặc mật khẩu không chính xác", false);
                    return;
                }
                else
                    btnSignRemote.ImageIndex = 0;
                ShowMessage("Đăng nhập thành công", false);
            }
            catch (Exception ex)
            {
                btnSignRemote.ImageIndex = 1;
                ShowMessage(ex.Message, false);
            }
        }
        private void chkIsUseSign_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsUseSign.CheckState == CheckState.Checked)
            {
                if (chIsUseSignRemote.Checked || chIsSignOnLan.Checked || chkSmartCA.Checked)
                {
                    chIsUseSignRemote.CheckState = CheckState.Unchecked;
                    chIsSignOnLan.CheckState = CheckState.Unchecked;
                    chkSmartCA.CheckState = CheckState.Unchecked;

                }
            }
            //if (chIsUseSignRemote.Checked || chIsSignOnLan.Checked)
            //{
            //    ShowMessage("Hệ thống chỉ cho phép bạn chọn một trong các cách ký số\r\nHệ thống sẽ bỏ kích hoạt các chức năng còn lại", false);
            //    chIsUseSignRemote.CheckedChanged -= new EventHandler(chIsUseSignRemote_CheckedChanged);
            //    chIsSignOnLan.CheckedChanged -= new EventHandler(chIsSignOnLan_CheckedChanged);
            //    chIsUseSignRemote.Checked = false;
            //    chIsSignOnLan.Checked = false;
            //    chkIsUseSign.Checked = true;
            //    chIsUseSignRemote.CheckedChanged += new EventHandler(chIsUseSignRemote_CheckedChanged);
            //    chIsSignOnLan.CheckedChanged += new EventHandler(chIsSignOnLan_CheckedChanged);

            //}
        }

        private void chIsUseSignRemote_CheckedChanged(object sender, EventArgs e)
        {
            if (chIsUseSignRemote.CheckState == CheckState.Checked)
            {
                if (chkIsUseSign.Checked || chIsSignOnLan.Checked || chkSmartCA.Checked)
                {
                    chkIsUseSign.CheckState = CheckState.Unchecked;
                    chIsSignOnLan.CheckState = CheckState.Unchecked;
                    chkSmartCA.CheckState = CheckState.Unchecked;
                }
            }
            //if (chkIsUseSign.Checked || chIsSignOnLan.Checked)
            //{
            //    ShowMessage("Hệ thống chỉ cho phép bạn chọn một trong các cách ký số\r\nHệ thống sẽ bỏ kích hoạt các chức năng còn lại", false);
            //    chkIsUseSign.CheckedChanged -= new EventHandler(chkIsUseSign_CheckedChanged);
            //    chIsSignOnLan.CheckedChanged -= new EventHandler(chIsSignOnLan_CheckedChanged);
            //    chkIsUseSign.Checked = false;
            //    chIsSignOnLan.Checked = false;
            //    chkIsUseSign.CheckedChanged += new EventHandler(chkIsUseSign_CheckedChanged);
            //    chIsSignOnLan.CheckedChanged += new EventHandler(chIsSignOnLan_CheckedChanged);

            //}
        }

        private void btnExportInfo_Click(object sender, EventArgs e)
        {
            string signName = cbSigns.SelectedValue.ToString();
            X509Certificate2 x059 = Cryptography.GetStoreX509Certificate2(signName);

            if (x059 != null)
            {

                byte[] certData = x059.Export(X509ContentType.Cert);
                //using (FileStream fs = new FileStream("Cer.pfx", FileMode.OpenOrCreate))
                //{
                //    byte[] blob = x059.Export(X509ContentType.Pfx, "MKkb1980");
                //    fs.Write(certData, 0, certData.Length);
                //    fs.Close();
                //}
                string filename = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Certificate.cer";
                if (File.Exists(filename)) File.Delete(filename);
                File.WriteAllText(filename, Convert.ToBase64String(x059.Export(X509ContentType.Cert)));
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.EnableRaisingEvents = false;
                proc.StartInfo.FileName = filename;
                proc.Start();


                //}

            }
        }

        private void btnCheckHSD_CKS_Click(object sender, EventArgs e)
        {
            if (!GlobalSettings.iSignRemote)
                MessageBox.Show("Chữ ký số còn: " + GlobalSettings.CheckChuKySo() + "ngày sử dụng.", "Hạn sử dụng CKS", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Chức năng chỉ sử dụng cho chức năng ký trực tiếp.", "Hạn sử dụng CKS", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=o1d9U8RkOyw&list=PLFhBUYQraey6Lbi8czV7vHQ32GSnfT9wN");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void chkSmartCA_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSmartCA.CheckState == CheckState.Checked)
            {
                if (chIsUseSignRemote.Checked || chkIsUseSign.Checked || chIsSignOnLan.Checked)
                {
                    chIsUseSignRemote.CheckState = CheckState.Unchecked;
                    chkIsUseSign.CheckState = CheckState.Unchecked;
                    chIsSignOnLan.CheckState = CheckState.Unchecked;
                }
            }
        }

        private void txtCheckSmartCA_Click(object sender, EventArgs e)
        {
            try
            {
                bool isValid = HelperVNACCS.CheckSmartCA(txtUserNameSmartCA.Text.ToString().Trim(), txtPasswordSmartCA.Text.ToString().Trim(), null);
                if (!isValid)
                {
                    ShowMessage("Tên đăng nhập hoặc mật khẩu không chính xác", false);
                    return;
                }
                else
                    ShowMessage("Đăng nhập thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
