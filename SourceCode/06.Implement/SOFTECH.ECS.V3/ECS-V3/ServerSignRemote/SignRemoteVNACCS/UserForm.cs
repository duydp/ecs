﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using ServerRemote;

namespace SignRemoteVNACCS
{
    public partial class UserForm : Form
    {
        public User_DaiLy user;
        public UserForm()
        {
            InitializeComponent();
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (user != null)
                {
                    txtUser.Text = user.USER_NAME;
                    //txtPass.Text = EncryptString(user.PASSWORD, "KEYWORD");
                    txtMaDN.Text = user.MaDoanhNghiep;
                    txtTenDN.Text = user.HO_TEN;
                    txtGhiChu.Text = user.MO_TA;
                    chkAdmin.Checked = Convert.ToBoolean(user.isAdmin);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (user == null)
                {
                    user = new User_DaiLy();
                }
                user.USER_NAME = txtUser.Text;
                user.PASSWORD = GetMD5Value(txtPass.Text);
                user.MaDoanhNghiep = txtMaDN.Text;
                user.HO_TEN = txtTenDN.Text;
                user.MO_TA = txtGhiChu.Text;
                user.isAdmin = Convert.ToBoolean(chkAdmin.Checked);
                user.InsertUpdate();
                MessageBoxControl.ShowMessage("Cập nhật thành công ");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }
        public static string EncryptString(string plainText, string keyword)
        {
            byte[] text = Encoding.ASCII.GetBytes(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] salt = Encoding.ASCII.GetBytes(keyword);

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(keyword, salt);

            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            CryptoStream encStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            encStream.Write(text, 0, text.Length);

            encStream.FlushFinalBlock();

            byte[] CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            encStream.Close();

            return Convert.ToBase64String(CipherBytes);
        }
        public static string DecryptString(string plainText, string keyWord)
        {
            byte[] text = Convert.FromBase64String(plainText);

            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] salt = Encoding.ASCII.GetBytes(keyWord);

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(keyWord, salt);

            ICryptoTransform decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(text);

            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            text = new byte[text.Length];
            int DecryptedCount = cryptoStream.Read(text, 0, text.Length);

            memoryStream.Close();
            cryptoStream.Close();


            return Encoding.ASCII.GetString(text);
        }
    }
}
