using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Company.BLL.KDT.SXXK
{
    public partial class BKToKhaiXuat
    {
        protected DateTime _NgayHoanThanh = new DateTime(1900, 01, 01);
        public DateTime NgayHoanThanh
        {
            set { this._NgayHoanThanh = value; }
            get { return this._NgayHoanThanh; }
        }

        public string PhanLuong { get; set; }

        protected decimal _trigia = 0;
        public decimal TriGia { get { return this._trigia; } set { this._trigia = value; } }
        public DataSet getBKToKhaiXuatNPL(long BangKeHoSoThanhLy_ID)
        {
            try
            {
                string sql = "SELECT a.SoToKhai,a.MaLoaiHinh,a.NamDangKy,a.NgayDangKy,a.MaHaiQuan,b.MaPhu,b.MaHS,b.TenHang, b.SoLuong, b.DVT_ID " +
                            "FROM t_KDT_SXXK_BKToKhaiXuat a INNER JOIN t_SXXK_HangMauDich b " +
                            "ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                            "AND a.NamDangKy = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                            "INNER JOIN t_SXXK_ToKhaiMauDich c " +
                            "ON a.SoToKhai = c.SoToKhai AND a.MaLoaiHinh = c.MaLoaiHinh " +
                            "AND a.NamDangKy = c.NamDangKy AND a.MaHaiQuan = c.MaHaiQuan " +
                            "WHERE BangKeHoSoThanhLy_ID =" + BangKeHoSoThanhLy_ID +
                            " AND c.Xuat_NPL_SP = 'N'";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch
            {
                return null;
            }

        }
        public DataSet getBKToKhaiXuatSP(long BangKeHoSoThanhLy_ID)
        {
            try
            {
                string sql = "SELECT a.SoToKhai,a.MaLoaiHinh,a.NamDangKy,a.NgayDangKy,a.MaHaiQuan,b.MaPhu,b.MaHS,b.TenHang, b.SoLuong, b.DVT_ID " +
                            "FROM t_KDT_SXXK_BKToKhaiXuat a INNER JOIN t_SXXK_HangMauDich b " +
                            "ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                            "AND a.NamDangKy = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                            "INNER JOIN t_SXXK_ToKhaiMauDich c " +
                            "ON a.SoToKhai = c.SoToKhai AND a.MaLoaiHinh = c.MaLoaiHinh " +
                            "AND a.NamDangKy = c.NamDangKy AND a.MaHaiQuan = c.MaHaiQuan " +
                            "WHERE BangKeHoSoThanhLy_ID =" + BangKeHoSoThanhLy_ID +
                            " AND c.Xuat_NPL_SP = 'S'";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch
            {
                return null;
            }

        }

        public DataSet getBKToKhaiXuatGC(long BangKeHoSoThanhLy_ID)
        {
            try
            {
                string sql = "SELECT a.SoToKhai,a.MaLoaiHinh,a.NamDangKy,a.NgayDangKy,a.MaHaiQuan " +
                            "FROM t_KDT_SXXK_BKToKhaiXuat a " +
                            "WHERE BangKeHoSoThanhLy_ID =" + BangKeHoSoThanhLy_ID +
                            " AND a.MaLoaiHinh LIKE 'XGC%'";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch
            {
                return null;
            }

        }
        public DataSet getBKToKhaiXuatForReport(int LanThanhLy, string MaDoanhNghiep)
        {
            try
            {
                string sql = "SELECT row_number() OVER (ORDER BY a.NgayDangKy) as STT, Cast(a.SoToKhai as varchar(100)) +'/' + substring(a.MaLoaiHinh,0,4) as SoToKhai, a.NgayDangKy, CASE WHEN year (a.NGAY_THN_THX) = 1900 THEN a.NgayHoanThanh ELSE a.NGAY_THN_THX END AS NgayThucXuat, " +
                                "a.SoHopDong, a.NgayHopDong, c.Ten " +
                                "FROM t_SXXK_ToKhaiMauDich a " +
                                "INNER JOIN (SELECT DISTINCT SoToKhaiXuat, MaLoaiHinhXuat, year(NgayDangKyXuat) as NamDangKyXuat FROM t_KDT_SXXK_BCThueXNK Where LanThanhLy = " + LanThanhLy + " AND MaDoanhNghiep ='" + MaDoanhNghiep + "')b " +
                                "ON a.SoToKhai = b.SoToKhaiXuat AND a.MaLoaiHinh = b.MaLoaiHinhXuat AND a.NamDangKy = b.NamDangKyXuat " +
                                "INNER JOIN dbo.t_HaiQuan_DonViHaiQuan c " +
                                "ON a.MaHaiQuan = c.ID ";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch
            {
                return null;
            }

        }

        public static BKToKhaiXuatCollection SelectCollectionBy_BangKeHoSoThanhLy_ID_NgayHoanThanh(long bangKeHoSoThanhLy_ID)
        {
            string spName = "p_KDT_SXXK_BKToKhaiXuat_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, bangKeHoSoThanhLy_ID);

            BKToKhaiXuatCollection collection = new BKToKhaiXuatCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                BKToKhaiXuat entity = new BKToKhaiXuat();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucXuat"))) entity.NgayThucXuat = reader.GetDateTime(reader.GetOrdinal("NgayThucXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
    }
}