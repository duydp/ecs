﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_TotalInventoryReport_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TotalInventory_ID { set; get; }
		public string TenHangHoa { set; get; }
		public string MaHangHoa { set; get; }
		public decimal SoLuongTonKhoSoSach { set; get; }
		public decimal SoLuongTonKhoThucTe { set; get; }
		public string DVT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_TotalInventoryReport_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_TotalInventoryReport_Detail> collection = new List<KDT_VNACCS_TotalInventoryReport_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_TotalInventoryReport_Detail entity = new KDT_VNACCS_TotalInventoryReport_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TotalInventory_ID"))) entity.TotalInventory_ID = reader.GetInt64(reader.GetOrdinal("TotalInventory_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangHoa"))) entity.TenHangHoa = reader.GetString(reader.GetOrdinal("TenHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangHoa"))) entity.MaHangHoa = reader.GetString(reader.GetOrdinal("MaHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTonKhoSoSach"))) entity.SoLuongTonKhoSoSach = reader.GetDecimal(reader.GetOrdinal("SoLuongTonKhoSoSach"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTonKhoThucTe"))) entity.SoLuongTonKhoThucTe = reader.GetDecimal(reader.GetOrdinal("SoLuongTonKhoThucTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_TotalInventoryReport_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_TotalInventoryReport_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TotalInventoryReport_Details VALUES(@TotalInventory_ID, @TenHangHoa, @MaHangHoa, @SoLuongTonKhoSoSach, @SoLuongTonKhoThucTe, @DVT)";
            string update = "UPDATE t_KDT_VNACCS_TotalInventoryReport_Details SET TotalInventory_ID = @TotalInventory_ID, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, SoLuongTonKhoSoSach = @SoLuongTonKhoSoSach, SoLuongTonKhoThucTe = @SoLuongTonKhoThucTe, DVT = @DVT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TotalInventoryReport_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TotalInventory_ID", SqlDbType.BigInt, "TotalInventory_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTonKhoSoSach", SqlDbType.Decimal, "SoLuongTonKhoSoSach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTonKhoThucTe", SqlDbType.Decimal, "SoLuongTonKhoThucTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TotalInventory_ID", SqlDbType.BigInt, "TotalInventory_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTonKhoSoSach", SqlDbType.Decimal, "SoLuongTonKhoSoSach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTonKhoThucTe", SqlDbType.Decimal, "SoLuongTonKhoThucTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TotalInventoryReport_Details VALUES(@TotalInventory_ID, @TenHangHoa, @MaHangHoa, @SoLuongTonKhoSoSach, @SoLuongTonKhoThucTe, @DVT)";
            string update = "UPDATE t_KDT_VNACCS_TotalInventoryReport_Details SET TotalInventory_ID = @TotalInventory_ID, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, SoLuongTonKhoSoSach = @SoLuongTonKhoSoSach, SoLuongTonKhoThucTe = @SoLuongTonKhoThucTe, DVT = @DVT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TotalInventoryReport_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TotalInventory_ID", SqlDbType.BigInt, "TotalInventory_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTonKhoSoSach", SqlDbType.Decimal, "SoLuongTonKhoSoSach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTonKhoThucTe", SqlDbType.Decimal, "SoLuongTonKhoThucTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TotalInventory_ID", SqlDbType.BigInt, "TotalInventory_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTonKhoSoSach", SqlDbType.Decimal, "SoLuongTonKhoSoSach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTonKhoThucTe", SqlDbType.Decimal, "SoLuongTonKhoThucTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_TotalInventoryReport_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_TotalInventoryReport_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_TotalInventoryReport_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_TotalInventoryReport_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_TotalInventoryReport_Detail> SelectCollectionBy_TotalInventory_ID(long totalInventory_ID)
		{
            IDataReader reader = SelectReaderBy_TotalInventory_ID(totalInventory_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TotalInventory_ID(long totalInventory_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_TotalInventory_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, totalInventory_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TotalInventory_ID(long totalInventory_ID)
		{
			const string spName = "p_KDT_VNACCS_TotalInventoryReport_Detail_SelectBy_TotalInventory_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, totalInventory_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_TotalInventoryReport_Detail(long totalInventory_ID, string tenHangHoa, string maHangHoa, decimal soLuongTonKhoSoSach, decimal soLuongTonKhoThucTe, string dVT)
		{
			KDT_VNACCS_TotalInventoryReport_Detail entity = new KDT_VNACCS_TotalInventoryReport_Detail();	
			entity.TotalInventory_ID = totalInventory_ID;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.SoLuongTonKhoSoSach = soLuongTonKhoSoSach;
			entity.SoLuongTonKhoThucTe = soLuongTonKhoThucTe;
			entity.DVT = dVT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, TotalInventory_ID);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@SoLuongTonKhoSoSach", SqlDbType.Decimal, SoLuongTonKhoSoSach);
			db.AddInParameter(dbCommand, "@SoLuongTonKhoThucTe", SqlDbType.Decimal, SoLuongTonKhoThucTe);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_TotalInventoryReport_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TotalInventoryReport_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_TotalInventoryReport_Detail(long id, long totalInventory_ID, string tenHangHoa, string maHangHoa, decimal soLuongTonKhoSoSach, decimal soLuongTonKhoThucTe, string dVT)
		{
			KDT_VNACCS_TotalInventoryReport_Detail entity = new KDT_VNACCS_TotalInventoryReport_Detail();			
			entity.ID = id;
			entity.TotalInventory_ID = totalInventory_ID;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.SoLuongTonKhoSoSach = soLuongTonKhoSoSach;
			entity.SoLuongTonKhoThucTe = soLuongTonKhoThucTe;
			entity.DVT = dVT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_TotalInventoryReport_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, TotalInventory_ID);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@SoLuongTonKhoSoSach", SqlDbType.Decimal, SoLuongTonKhoSoSach);
			db.AddInParameter(dbCommand, "@SoLuongTonKhoThucTe", SqlDbType.Decimal, SoLuongTonKhoThucTe);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_TotalInventoryReport_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TotalInventoryReport_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_TotalInventoryReport_Detail(long id, long totalInventory_ID, string tenHangHoa, string maHangHoa, decimal soLuongTonKhoSoSach, decimal soLuongTonKhoThucTe, string dVT)
		{
			KDT_VNACCS_TotalInventoryReport_Detail entity = new KDT_VNACCS_TotalInventoryReport_Detail();			
			entity.ID = id;
			entity.TotalInventory_ID = totalInventory_ID;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.SoLuongTonKhoSoSach = soLuongTonKhoSoSach;
			entity.SoLuongTonKhoThucTe = soLuongTonKhoThucTe;
			entity.DVT = dVT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, TotalInventory_ID);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@SoLuongTonKhoSoSach", SqlDbType.Decimal, SoLuongTonKhoSoSach);
			db.AddInParameter(dbCommand, "@SoLuongTonKhoThucTe", SqlDbType.Decimal, SoLuongTonKhoThucTe);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_TotalInventoryReport_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TotalInventoryReport_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_TotalInventoryReport_Detail(long id)
		{
			KDT_VNACCS_TotalInventoryReport_Detail entity = new KDT_VNACCS_TotalInventoryReport_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TotalInventory_ID(long totalInventory_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteBy_TotalInventory_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TotalInventory_ID", SqlDbType.BigInt, totalInventory_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TotalInventoryReport_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_TotalInventoryReport_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TotalInventoryReport_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}