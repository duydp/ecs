-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]
	@GiayPhep_ID bigint,
	@TenThuocQuyCachDongGoi nvarchar(768),
	@MaSoHangHoa varchar(12),
	@TenHoatChatGayNghien nvarchar(70),
	@HoatChat varchar(35),
	@TieuChuanChatLuong varchar(35),
	@SoDangKy varchar(17),
	@HanDung datetime,
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@CongDung varchar(50),
	@TongSoKhoiLuongHoatChatGayNghien numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@GiaNhapKhauVND numeric(19, 3),
	@GiaBanBuonVND numeric(19, 3),
	@GiaBanLeVND numeric(19, 3),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(300),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@MaCongTySanXuat varchar(13),
	@TenCongTySanXuat nvarchar(300),
	@MaBuuChinhCongTySanXuat varchar(9),
	@SoNhaTenDuongCongTySanXuat varchar(35),
	@PhuongXaCongTySanXuat nvarchar(35),
	@QuanHuyenCongTySanXuat nvarchar(35),
	@TinhThanhPhoCongTySanXuat nvarchar(35),
	@MaQuocGiaCongTySanXuat varchar(2),
	@MaCongTyCungCap varchar(13),
	@TenCongTyCungCap nvarchar(300),
	@MaBuuChinhCongTyCungCap varchar(9),
	@SoNhaTenDuongCongTyCungCap varchar(35),
	@PhuongXaCongTyCungCap nvarchar(35),
	@QuanHuyenCongTyCungCap nvarchar(35),
	@TinhThanhPhoCongTyCungCap nvarchar(35),
	@MaQuocGiaCongTyCungCap varchar(2),
	@MaDonViUyThac varchar(13),
	@TenDonViUyThac nvarchar(300),
	@MaBuuChinhDonViUyThac varchar(9),
	@SoNhaTenDuongDonViUyThac varchar(35),
	@PhuongXaDonViUyThac nvarchar(35),
	@QuanHuyenDonViUyThac nvarchar(35),
	@TinhThanhPhoDonViUyThac nvarchar(35),
	@MaQuocGiaDonViUyThac varchar(2),
	@LuongTonKhoKyTruoc numeric(8, 0),
	@DonViTinhLuongTonKyTruoc varchar(3),
	@LuongNhapTrongKy numeric(8, 0),
	@TongSo numeric(9, 0),
	@TongSoXuatTrongKy numeric(9, 0),
	@SoLuongTonKhoDenNgay numeric(8, 0),
	@HuHao numeric(8, 0),
	@GhiChuChiTiet nvarchar(105),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
(
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@GiayPhep_ID,
	@TenThuocQuyCachDongGoi,
	@MaSoHangHoa,
	@TenHoatChatGayNghien,
	@HoatChat,
	@TieuChuanChatLuong,
	@SoDangKy,
	@HanDung,
	@SoLuong,
	@DonVitinhSoLuong,
	@CongDung,
	@TongSoKhoiLuongHoatChatGayNghien,
	@DonVitinhKhoiLuong,
	@GiaNhapKhauVND,
	@GiaBanBuonVND,
	@GiaBanLeVND,
	@MaThuongNhanXuatKhau,
	@TenThuongNhanXuatKhau,
	@MaBuuChinhXuatKhau,
	@SoNhaTenDuongXuatKhau,
	@PhuongXaXuatKhau,
	@QuanHuyenXuatKhau,
	@TinhThanhPhoXuatKhau,
	@MaQuocGiaXuatKhau,
	@MaCongTySanXuat,
	@TenCongTySanXuat,
	@MaBuuChinhCongTySanXuat,
	@SoNhaTenDuongCongTySanXuat,
	@PhuongXaCongTySanXuat,
	@QuanHuyenCongTySanXuat,
	@TinhThanhPhoCongTySanXuat,
	@MaQuocGiaCongTySanXuat,
	@MaCongTyCungCap,
	@TenCongTyCungCap,
	@MaBuuChinhCongTyCungCap,
	@SoNhaTenDuongCongTyCungCap,
	@PhuongXaCongTyCungCap,
	@QuanHuyenCongTyCungCap,
	@TinhThanhPhoCongTyCungCap,
	@MaQuocGiaCongTyCungCap,
	@MaDonViUyThac,
	@TenDonViUyThac,
	@MaBuuChinhDonViUyThac,
	@SoNhaTenDuongDonViUyThac,
	@PhuongXaDonViUyThac,
	@QuanHuyenDonViUyThac,
	@TinhThanhPhoDonViUyThac,
	@MaQuocGiaDonViUyThac,
	@LuongTonKhoKyTruoc,
	@DonViTinhLuongTonKyTruoc,
	@LuongNhapTrongKy,
	@TongSo,
	@TongSoXuatTrongKy,
	@SoLuongTonKhoDenNgay,
	@HuHao,
	@GhiChuChiTiet,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TenThuocQuyCachDongGoi nvarchar(768),
	@MaSoHangHoa varchar(12),
	@TenHoatChatGayNghien nvarchar(70),
	@HoatChat varchar(35),
	@TieuChuanChatLuong varchar(35),
	@SoDangKy varchar(17),
	@HanDung datetime,
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@CongDung varchar(50),
	@TongSoKhoiLuongHoatChatGayNghien numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@GiaNhapKhauVND numeric(19, 3),
	@GiaBanBuonVND numeric(19, 3),
	@GiaBanLeVND numeric(19, 3),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(300),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@MaCongTySanXuat varchar(13),
	@TenCongTySanXuat nvarchar(300),
	@MaBuuChinhCongTySanXuat varchar(9),
	@SoNhaTenDuongCongTySanXuat varchar(35),
	@PhuongXaCongTySanXuat nvarchar(35),
	@QuanHuyenCongTySanXuat nvarchar(35),
	@TinhThanhPhoCongTySanXuat nvarchar(35),
	@MaQuocGiaCongTySanXuat varchar(2),
	@MaCongTyCungCap varchar(13),
	@TenCongTyCungCap nvarchar(300),
	@MaBuuChinhCongTyCungCap varchar(9),
	@SoNhaTenDuongCongTyCungCap varchar(35),
	@PhuongXaCongTyCungCap nvarchar(35),
	@QuanHuyenCongTyCungCap nvarchar(35),
	@TinhThanhPhoCongTyCungCap nvarchar(35),
	@MaQuocGiaCongTyCungCap varchar(2),
	@MaDonViUyThac varchar(13),
	@TenDonViUyThac nvarchar(300),
	@MaBuuChinhDonViUyThac varchar(9),
	@SoNhaTenDuongDonViUyThac varchar(35),
	@PhuongXaDonViUyThac nvarchar(35),
	@QuanHuyenDonViUyThac nvarchar(35),
	@TinhThanhPhoDonViUyThac nvarchar(35),
	@MaQuocGiaDonViUyThac varchar(2),
	@LuongTonKhoKyTruoc numeric(8, 0),
	@DonViTinhLuongTonKyTruoc varchar(3),
	@LuongNhapTrongKy numeric(8, 0),
	@TongSo numeric(9, 0),
	@TongSoXuatTrongKy numeric(9, 0),
	@SoLuongTonKhoDenNgay numeric(8, 0),
	@HuHao numeric(8, 0),
	@GhiChuChiTiet nvarchar(105),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
SET
	[GiayPhep_ID] = @GiayPhep_ID,
	[TenThuocQuyCachDongGoi] = @TenThuocQuyCachDongGoi,
	[MaSoHangHoa] = @MaSoHangHoa,
	[TenHoatChatGayNghien] = @TenHoatChatGayNghien,
	[HoatChat] = @HoatChat,
	[TieuChuanChatLuong] = @TieuChuanChatLuong,
	[SoDangKy] = @SoDangKy,
	[HanDung] = @HanDung,
	[SoLuong] = @SoLuong,
	[DonVitinhSoLuong] = @DonVitinhSoLuong,
	[CongDung] = @CongDung,
	[TongSoKhoiLuongHoatChatGayNghien] = @TongSoKhoiLuongHoatChatGayNghien,
	[DonVitinhKhoiLuong] = @DonVitinhKhoiLuong,
	[GiaNhapKhauVND] = @GiaNhapKhauVND,
	[GiaBanBuonVND] = @GiaBanBuonVND,
	[GiaBanLeVND] = @GiaBanLeVND,
	[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
	[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
	[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
	[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
	[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
	[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
	[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
	[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
	[MaCongTySanXuat] = @MaCongTySanXuat,
	[TenCongTySanXuat] = @TenCongTySanXuat,
	[MaBuuChinhCongTySanXuat] = @MaBuuChinhCongTySanXuat,
	[SoNhaTenDuongCongTySanXuat] = @SoNhaTenDuongCongTySanXuat,
	[PhuongXaCongTySanXuat] = @PhuongXaCongTySanXuat,
	[QuanHuyenCongTySanXuat] = @QuanHuyenCongTySanXuat,
	[TinhThanhPhoCongTySanXuat] = @TinhThanhPhoCongTySanXuat,
	[MaQuocGiaCongTySanXuat] = @MaQuocGiaCongTySanXuat,
	[MaCongTyCungCap] = @MaCongTyCungCap,
	[TenCongTyCungCap] = @TenCongTyCungCap,
	[MaBuuChinhCongTyCungCap] = @MaBuuChinhCongTyCungCap,
	[SoNhaTenDuongCongTyCungCap] = @SoNhaTenDuongCongTyCungCap,
	[PhuongXaCongTyCungCap] = @PhuongXaCongTyCungCap,
	[QuanHuyenCongTyCungCap] = @QuanHuyenCongTyCungCap,
	[TinhThanhPhoCongTyCungCap] = @TinhThanhPhoCongTyCungCap,
	[MaQuocGiaCongTyCungCap] = @MaQuocGiaCongTyCungCap,
	[MaDonViUyThac] = @MaDonViUyThac,
	[TenDonViUyThac] = @TenDonViUyThac,
	[MaBuuChinhDonViUyThac] = @MaBuuChinhDonViUyThac,
	[SoNhaTenDuongDonViUyThac] = @SoNhaTenDuongDonViUyThac,
	[PhuongXaDonViUyThac] = @PhuongXaDonViUyThac,
	[QuanHuyenDonViUyThac] = @QuanHuyenDonViUyThac,
	[TinhThanhPhoDonViUyThac] = @TinhThanhPhoDonViUyThac,
	[MaQuocGiaDonViUyThac] = @MaQuocGiaDonViUyThac,
	[LuongTonKhoKyTruoc] = @LuongTonKhoKyTruoc,
	[DonViTinhLuongTonKyTruoc] = @DonViTinhLuongTonKyTruoc,
	[LuongNhapTrongKy] = @LuongNhapTrongKy,
	[TongSo] = @TongSo,
	[TongSoXuatTrongKy] = @TongSoXuatTrongKy,
	[SoLuongTonKhoDenNgay] = @SoLuongTonKhoDenNgay,
	[HuHao] = @HuHao,
	[GhiChuChiTiet] = @GhiChuChiTiet,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TenThuocQuyCachDongGoi nvarchar(768),
	@MaSoHangHoa varchar(12),
	@TenHoatChatGayNghien nvarchar(70),
	@HoatChat varchar(35),
	@TieuChuanChatLuong varchar(35),
	@SoDangKy varchar(17),
	@HanDung datetime,
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@CongDung varchar(50),
	@TongSoKhoiLuongHoatChatGayNghien numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@GiaNhapKhauVND numeric(19, 3),
	@GiaBanBuonVND numeric(19, 3),
	@GiaBanLeVND numeric(19, 3),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(300),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@MaCongTySanXuat varchar(13),
	@TenCongTySanXuat nvarchar(300),
	@MaBuuChinhCongTySanXuat varchar(9),
	@SoNhaTenDuongCongTySanXuat varchar(35),
	@PhuongXaCongTySanXuat nvarchar(35),
	@QuanHuyenCongTySanXuat nvarchar(35),
	@TinhThanhPhoCongTySanXuat nvarchar(35),
	@MaQuocGiaCongTySanXuat varchar(2),
	@MaCongTyCungCap varchar(13),
	@TenCongTyCungCap nvarchar(300),
	@MaBuuChinhCongTyCungCap varchar(9),
	@SoNhaTenDuongCongTyCungCap varchar(35),
	@PhuongXaCongTyCungCap nvarchar(35),
	@QuanHuyenCongTyCungCap nvarchar(35),
	@TinhThanhPhoCongTyCungCap nvarchar(35),
	@MaQuocGiaCongTyCungCap varchar(2),
	@MaDonViUyThac varchar(13),
	@TenDonViUyThac nvarchar(300),
	@MaBuuChinhDonViUyThac varchar(9),
	@SoNhaTenDuongDonViUyThac varchar(35),
	@PhuongXaDonViUyThac nvarchar(35),
	@QuanHuyenDonViUyThac nvarchar(35),
	@TinhThanhPhoDonViUyThac nvarchar(35),
	@MaQuocGiaDonViUyThac varchar(2),
	@LuongTonKhoKyTruoc numeric(8, 0),
	@DonViTinhLuongTonKyTruoc varchar(3),
	@LuongNhapTrongKy numeric(8, 0),
	@TongSo numeric(9, 0),
	@TongSoXuatTrongKy numeric(9, 0),
	@SoLuongTonKhoDenNgay numeric(8, 0),
	@HuHao numeric(8, 0),
	@GhiChuChiTiet nvarchar(105),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangGiayPhep_SMA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangGiayPhep_SMA] 
		SET
			[GiayPhep_ID] = @GiayPhep_ID,
			[TenThuocQuyCachDongGoi] = @TenThuocQuyCachDongGoi,
			[MaSoHangHoa] = @MaSoHangHoa,
			[TenHoatChatGayNghien] = @TenHoatChatGayNghien,
			[HoatChat] = @HoatChat,
			[TieuChuanChatLuong] = @TieuChuanChatLuong,
			[SoDangKy] = @SoDangKy,
			[HanDung] = @HanDung,
			[SoLuong] = @SoLuong,
			[DonVitinhSoLuong] = @DonVitinhSoLuong,
			[CongDung] = @CongDung,
			[TongSoKhoiLuongHoatChatGayNghien] = @TongSoKhoiLuongHoatChatGayNghien,
			[DonVitinhKhoiLuong] = @DonVitinhKhoiLuong,
			[GiaNhapKhauVND] = @GiaNhapKhauVND,
			[GiaBanBuonVND] = @GiaBanBuonVND,
			[GiaBanLeVND] = @GiaBanLeVND,
			[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
			[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
			[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
			[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
			[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
			[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
			[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
			[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
			[MaCongTySanXuat] = @MaCongTySanXuat,
			[TenCongTySanXuat] = @TenCongTySanXuat,
			[MaBuuChinhCongTySanXuat] = @MaBuuChinhCongTySanXuat,
			[SoNhaTenDuongCongTySanXuat] = @SoNhaTenDuongCongTySanXuat,
			[PhuongXaCongTySanXuat] = @PhuongXaCongTySanXuat,
			[QuanHuyenCongTySanXuat] = @QuanHuyenCongTySanXuat,
			[TinhThanhPhoCongTySanXuat] = @TinhThanhPhoCongTySanXuat,
			[MaQuocGiaCongTySanXuat] = @MaQuocGiaCongTySanXuat,
			[MaCongTyCungCap] = @MaCongTyCungCap,
			[TenCongTyCungCap] = @TenCongTyCungCap,
			[MaBuuChinhCongTyCungCap] = @MaBuuChinhCongTyCungCap,
			[SoNhaTenDuongCongTyCungCap] = @SoNhaTenDuongCongTyCungCap,
			[PhuongXaCongTyCungCap] = @PhuongXaCongTyCungCap,
			[QuanHuyenCongTyCungCap] = @QuanHuyenCongTyCungCap,
			[TinhThanhPhoCongTyCungCap] = @TinhThanhPhoCongTyCungCap,
			[MaQuocGiaCongTyCungCap] = @MaQuocGiaCongTyCungCap,
			[MaDonViUyThac] = @MaDonViUyThac,
			[TenDonViUyThac] = @TenDonViUyThac,
			[MaBuuChinhDonViUyThac] = @MaBuuChinhDonViUyThac,
			[SoNhaTenDuongDonViUyThac] = @SoNhaTenDuongDonViUyThac,
			[PhuongXaDonViUyThac] = @PhuongXaDonViUyThac,
			[QuanHuyenDonViUyThac] = @QuanHuyenDonViUyThac,
			[TinhThanhPhoDonViUyThac] = @TinhThanhPhoDonViUyThac,
			[MaQuocGiaDonViUyThac] = @MaQuocGiaDonViUyThac,
			[LuongTonKhoKyTruoc] = @LuongTonKhoKyTruoc,
			[DonViTinhLuongTonKyTruoc] = @DonViTinhLuongTonKyTruoc,
			[LuongNhapTrongKy] = @LuongNhapTrongKy,
			[TongSo] = @TongSo,
			[TongSoXuatTrongKy] = @TongSoXuatTrongKy,
			[SoLuongTonKhoDenNgay] = @SoLuongTonKhoDenNgay,
			[HuHao] = @HuHao,
			[GhiChuChiTiet] = @GhiChuChiTiet,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
		(
			[GiayPhep_ID],
			[TenThuocQuyCachDongGoi],
			[MaSoHangHoa],
			[TenHoatChatGayNghien],
			[HoatChat],
			[TieuChuanChatLuong],
			[SoDangKy],
			[HanDung],
			[SoLuong],
			[DonVitinhSoLuong],
			[CongDung],
			[TongSoKhoiLuongHoatChatGayNghien],
			[DonVitinhKhoiLuong],
			[GiaNhapKhauVND],
			[GiaBanBuonVND],
			[GiaBanLeVND],
			[MaThuongNhanXuatKhau],
			[TenThuongNhanXuatKhau],
			[MaBuuChinhXuatKhau],
			[SoNhaTenDuongXuatKhau],
			[PhuongXaXuatKhau],
			[QuanHuyenXuatKhau],
			[TinhThanhPhoXuatKhau],
			[MaQuocGiaXuatKhau],
			[MaCongTySanXuat],
			[TenCongTySanXuat],
			[MaBuuChinhCongTySanXuat],
			[SoNhaTenDuongCongTySanXuat],
			[PhuongXaCongTySanXuat],
			[QuanHuyenCongTySanXuat],
			[TinhThanhPhoCongTySanXuat],
			[MaQuocGiaCongTySanXuat],
			[MaCongTyCungCap],
			[TenCongTyCungCap],
			[MaBuuChinhCongTyCungCap],
			[SoNhaTenDuongCongTyCungCap],
			[PhuongXaCongTyCungCap],
			[QuanHuyenCongTyCungCap],
			[TinhThanhPhoCongTyCungCap],
			[MaQuocGiaCongTyCungCap],
			[MaDonViUyThac],
			[TenDonViUyThac],
			[MaBuuChinhDonViUyThac],
			[SoNhaTenDuongDonViUyThac],
			[PhuongXaDonViUyThac],
			[QuanHuyenDonViUyThac],
			[TinhThanhPhoDonViUyThac],
			[MaQuocGiaDonViUyThac],
			[LuongTonKhoKyTruoc],
			[DonViTinhLuongTonKyTruoc],
			[LuongNhapTrongKy],
			[TongSo],
			[TongSoXuatTrongKy],
			[SoLuongTonKhoDenNgay],
			[HuHao],
			[GhiChuChiTiet],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@GiayPhep_ID,
			@TenThuocQuyCachDongGoi,
			@MaSoHangHoa,
			@TenHoatChatGayNghien,
			@HoatChat,
			@TieuChuanChatLuong,
			@SoDangKy,
			@HanDung,
			@SoLuong,
			@DonVitinhSoLuong,
			@CongDung,
			@TongSoKhoiLuongHoatChatGayNghien,
			@DonVitinhKhoiLuong,
			@GiaNhapKhauVND,
			@GiaBanBuonVND,
			@GiaBanLeVND,
			@MaThuongNhanXuatKhau,
			@TenThuongNhanXuatKhau,
			@MaBuuChinhXuatKhau,
			@SoNhaTenDuongXuatKhau,
			@PhuongXaXuatKhau,
			@QuanHuyenXuatKhau,
			@TinhThanhPhoXuatKhau,
			@MaQuocGiaXuatKhau,
			@MaCongTySanXuat,
			@TenCongTySanXuat,
			@MaBuuChinhCongTySanXuat,
			@SoNhaTenDuongCongTySanXuat,
			@PhuongXaCongTySanXuat,
			@QuanHuyenCongTySanXuat,
			@TinhThanhPhoCongTySanXuat,
			@MaQuocGiaCongTySanXuat,
			@MaCongTyCungCap,
			@TenCongTyCungCap,
			@MaBuuChinhCongTyCungCap,
			@SoNhaTenDuongCongTyCungCap,
			@PhuongXaCongTyCungCap,
			@QuanHuyenCongTyCungCap,
			@TinhThanhPhoCongTyCungCap,
			@MaQuocGiaCongTyCungCap,
			@MaDonViUyThac,
			@TenDonViUyThac,
			@MaBuuChinhDonViUyThac,
			@SoNhaTenDuongDonViUyThac,
			@PhuongXaDonViUyThac,
			@QuanHuyenDonViUyThac,
			@TinhThanhPhoDonViUyThac,
			@MaQuocGiaDonViUyThac,
			@LuongTonKhoKyTruoc,
			@DonViTinhLuongTonKyTruoc,
			@LuongNhapTrongKy,
			@TongSo,
			@TongSoXuatTrongKy,
			@SoLuongTonKhoDenNgay,
			@HuHao,
			@GhiChuChiTiet,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep_SMA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_HangGiayPhep_SMA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]	

GO

