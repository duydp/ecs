﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.GC
{
    public partial class SaoChepDinhMucGCForm : Company.Interface.BaseForm
    {
        public DinhMucCollection DMCollectionTransfer = new DinhMucCollection();
        public DinhMucCollection DMCollectionCopy = new DinhMucCollection();
        public DinhMucCollection DMCollectionRemove = new DinhMucCollection();
        public DinhMucDangKy DMDangKy;
        public string MaSanPham;
        public SaoChepDinhMucGCForm()
        {
            InitializeComponent();
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = DMCollectionTransfer;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAdd = true;
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    isAdd = true;
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    Company.GC.BLL.KDT.GC.DinhMuc dinhmuc = (Company.GC.BLL.KDT.GC.DinhMuc)item.DataRow;
                    //Kiểm tra chọn trùng nhiều mã NPL khi chọn
                    foreach (DinhMuc it in DMCollectionCopy)
                    {
                        if (it.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                        {
                            if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                            {
                                it.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ GHỘP ĐỊNH MỨC CỦA MÃ NPL NÀY KHÔNG ?", true)=="Yes")
                            {
                                it.DinhMucSuDung = it.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = it.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else
                            {
                                isAdd = false;
                            }                            
                        }
                    }
                    if (isAdd)
                        DMCollectionCopy.Add(dinhmuc);
                }
                if (DMCollectionCopy.Count > 0)
                {
                     //Kiểm tra trùng mã NPL đã nhập định mức trước đó
                    foreach (DinhMuc ite in DMDangKy.DMCollection)
                    {
                        foreach (DinhMuc dinhmuc in DMCollectionCopy)
                        {
                            if (ite.MaSanPham == MaSanPham && ite.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                            {
                                if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                                {
                                    ite.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                    ite.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                    DMCollectionRemove.Add(dinhmuc);
                                }
                                else if ((ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN GHỘP ĐỊNH MỨC CỦA MÃ NPL KHÔNG ?", true) == "Yes"))
                                {
                                    ite.DinhMucSuDung = ite.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                    ite.TyLeHaoHut = ite.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                    DMCollectionRemove.Add(dinhmuc);
                                }
                            }   
                        }
                    }
                    foreach (DinhMuc item in DMCollectionRemove)
                    {
                        DMCollectionCopy.Remove(item);
                    }
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void SaoChepDinhMucGCForm_Load(object sender, EventArgs e)
        {
            BindData();
            this.Text = "SAO CHÉP ĐỊNH MỨC CHO MÃ SẢN PHẨM : " + MaSanPham;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DinhMucCollection DMTMPCollection = new DinhMucCollection();
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count < 0) return;
                if (ShowMessage(setText("BẠN CÓ MUỐN XÓA ĐỊNH MỨC CỦA SẢN PHẨM NÀY KHÔNG?", "Do you want to delete the selected row(s)?"), true) == "Yes")
                {
                    foreach (GridEXSelectedItem row in items)
                    {
                        if (row.RowType == RowType.Record)
                        {
                            Company.GC.BLL.KDT.GC.DinhMuc dm = (Company.GC.BLL.KDT.GC.DinhMuc)row.GetRow().DataRow;
                            DMTMPCollection.Add(dm);
                        }
                    }
                    foreach (DinhMuc dmRemove in DMTMPCollection)
                    {
                        DMCollectionTransfer.Remove(dmRemove);
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
