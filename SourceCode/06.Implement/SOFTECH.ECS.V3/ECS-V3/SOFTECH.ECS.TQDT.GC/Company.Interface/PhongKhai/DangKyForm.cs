﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Security.Cryptography;
using Company.GC.BLL.KDT;

namespace Company.Interface.PhongKhai
{
    public partial class DangKyForm : BaseForm
    {
        public DangKyForm()
        {
            InitializeComponent();
        }
        public bool IsRegister;
        private long t = 0;
        // private DataRowCollection dtb;
        private HeThongPhongKhai haiquanpkch = new HeThongPhongKhai();
        HeThongPhongKhai hqch = new HeThongPhongKhai();
        private string strPassConfig = "";
        private string _temp = "";

        private void Register(string strMa, string strTen, string strPass)
        {
            HeThongPhongKhai hqchpass = new HeThongPhongKhai();

            try
            {
                strMa = txtMaDoanhNghiep.Text.Trim();
                strTen = txtTenDoanhNghiep.Text.Trim();
                strPassConfig = txtPass.Text.Trim();
                strPass = this.GetMD5Value(txtPass.Text.Trim());
                _temp = hqchpass.SelectedPassWord(strMa, "CauHinh");
                // strPassConfig = strPass;
                //Trang thai Cau hinh :

                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                if (strPassConfig.Length > 0)
                    haiquanpkch.PassWord = strPass;
                else
                    haiquanpkch.PassWord = _temp;

                haiquanpkch.Key_Config = "CauHinh";
                haiquanpkch.Value_Config = "1";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();

                //Tên doanh Nghiệp
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "TEN_DON_VI";
                haiquanpkch.Value_Config = strTen;
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();

                //Mã Doanh  Nghiệp
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "MA_DON_VI";
                haiquanpkch.Value_Config = strMa;
                t = haiquanpkch.InsertUpdate();
                haiquanpkch.Role = 0;
                //Địa chỉ

                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "DIA_CHI";
                haiquanpkch.Value_Config = txtDiaChiDN.Text.Trim();
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Nước Xuất khẩu
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "NUOC";
                haiquanpkch.Value_Config = nuocHControl1.Ma;
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Tên đồi tác
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "TEN_DOI_TAC";
                haiquanpkch.Value_Config = "";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();

                //Điều kiện Giao Hàng mặc định
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "DKGH_MAC_DINH";
                haiquanpkch.Value_Config = cbDKGH.SelectedValue.ToString();
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Phương tiện thanh toán mặc định
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "PTTT_MAC_DINH";
                haiquanpkch.Value_Config = cbPTTT.SelectedValue.ToString();
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                // Phương tiện vận tải
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "PTVT_MAC_DINH";
                haiquanpkch.Value_Config = cbPTVT.SelectedValue.ToString();
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Nhóm loại hình
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "NHOM_LOAI_HINH";
                haiquanpkch.Value_Config = "NSX";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Loại hình
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "LOAI_HINH";
                haiquanpkch.Value_Config = "NSX01";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Địa điểm dỡ hàng
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "CUA_KHAU";
                haiquanpkch.Value_Config = diaDiemBocHangControl1.Ma;
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Nguyên tệ mặc định
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "NGUYEN_TE_MAC_DINH";
                haiquanpkch.Value_Config = nguyenTeControl1.Ma;
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Tỷ giá USD :                     
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "TY_GIA_USD";
                haiquanpkch.Value_Config = "16100";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Hạn thanh khoản
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "HanThanhKhoan";
                haiquanpkch.Value_Config = "275";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                // Thông báo thanh khoản
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "ThongBaoThanhKhoan";
                haiquanpkch.Value_Config = "10";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //CL_THN_THX
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "CL_THN_THX";
                haiquanpkch.Value_Config = "0";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                // Loại Web Service
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "LoaiWS";
                haiquanpkch.Value_Config = "1";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Nhóm loại hình khác
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "NHOM_LOAI_HINH_KHAC_NHAP";
                haiquanpkch.Value_Config = "N";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Ngày sao lưu
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "NgaySaoLuu";
                haiquanpkch.Value_Config = Convert.ToString(DateTime.Today.Date);
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();
                //Nhắc nhỡ sao lưu
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "NHAC_NHO_SAO_LUU";
                haiquanpkch.Value_Config = "7";
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();

                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "MaMID";
                haiquanpkch.Value_Config = txtMaMid.Text; //dngcustoms.gov.vn
                haiquanpkch.Role = 0;

                t = haiquanpkch.InsertUpdate();
                haiquanpkch.MaDoanhNghiep = strMa;
                haiquanpkch.TenDoanhNghiep = strTen;
                haiquanpkch.PassWord = strPass;
                haiquanpkch.Key_Config = "TuDongTinhThue";
                haiquanpkch.Value_Config = cbTuDongTinhThue.SelectedValue.ToString(); //dngcustoms.gov.vn
                haiquanpkch.Role = 0;
                t = haiquanpkch.InsertUpdate();

                //
                HeThongPhongKhai hqch = new HeThongPhongKhai();
                try
                {
                    //Thông tin Doanh Nhiệp
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_DON_VI", strMa);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DON_VI", hqch.SelectedSettings(strMa, "TEN_DON_VI"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DIA_CHI", hqch.SelectedSettings(strMa, "DIA_CHI"));

                    //Thông tin mặc định chung
                    //string temp = hqch.SelectedSettings(strMa, "TY_GIA_USD"));

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NUOC", hqch.SelectedSettings(strMa, "NUOC"));
                    //Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CL_THN_THX",  hqch.SelectedSettings(strMa, "CL_THN_THX"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CUA_KHAU", hqch.SelectedSettings(strMa, "CUA_KHAU"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DOI_TAC", hqch.SelectedSettings(strMa, "TEN_DOI_TAC"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DKGH_MAC_DINH", hqch.SelectedSettings(strMa, "DKGH_MAC_DINH"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PTTT_MAC_DINH", hqch.SelectedSettings(strMa, "PTTT_MAC_DINH"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PTVT_MAC_DINH", hqch.SelectedSettings(strMa, "PTVT_MAC_DINH"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("HanThanhKhoan", hqch.SelectedSettings(strMa, "HanThanhKhoan"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ThongBaoThanhKhoan", hqch.SelectedSettings(strMa, "ThongBaoThanhKhoan"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NGUYEN_TE_MAC_DINH", hqch.SelectedSettings(strMa, "NGUYEN_TE_MAC_DINH"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("LOAI_HINH", hqch.SelectedSettings(strMa, "LOAI_HINH"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHOM_LOAI_HINH", hqch.SelectedSettings(strMa, "NHOM_LOAI_HINH"));
                    //Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHOM_LOAI_HINH_KHAC_NHAP",  hqch.SelectedSettings(strMa, "NHOM_LOAI_HINH_KHAC_NHAP"));

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NGAYSAOLUU", hqch.SelectedSettings(strMa, "NgaySaoLuu"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", hqch.SelectedSettings(strMa, "NHAC_NHO_SAO_LUU"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TY_GIA_USD", 16100);// Convert.ToDecimal(hqch.SelectedSettings(strMa, "TY_GIA_USD")));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MaMID", hqch.SelectedSettings(strMa, "MaMID"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TuDongTinhThue", hqch.SelectedSettings(strMa, "TuDongTinhThue"));

                    GlobalSettings.RefreshKey();
                }
                catch (Exception ex1)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex1);
                    //ShowMessage("Lỗi :" + ex1.Message.ToString(), false);
                    //return;
                }

            }
            catch
            {
                ShowMessage("Phát hiện lỗi trong quá trình thiết lập ,vui lòng kiểm tra lại", false);
            }

        }
        //
        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }

        //
        private void ShowLoginForm()
        {
            LoginForm loginForm = new LoginForm();
            loginForm.ShowDialog();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string strMa = txtMaDoanhNghiep.Text.Trim();
            string strTen = txtTenDoanhNghiep.Text.Trim();
            string strPass = txtPass.Text.Trim();
            string strRePass = txtRePass.Text.Trim();
            if (strPass.Length > 0)
            {
                if (strPass != strRePass)
                {
                    ShowMessage("Mật khẩu chưa trùng khớp !", false);
                    //txtTenDoanhNghiep.Text = "";
                    txtPass.Text = "";
                    txtRePass.Text = "";
                    return;
                }
            }
            cvError.Validate();
            if (!cvError.IsValid) return;
            this.Cursor = Cursors.WaitCursor;
            this.Register(strMa, strTen, strPass);
            if (t > 0)
            {
                //if (clsBien.boolShow == false)
                //{
                //this.Hide();
                //this.ShowMainForm();
                //clsBien.boolStatusVarDangKy = false;
                //clsBien.boolStatusVarMain = true;
                //}
                //else
                //{
                this.Close();
                //}

            }
            else
            {
                ShowMessage("Thiết lập thông tin không thành công !", false);

            }
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }
        private void ShowMainForm()
        {
            MainForm mainForm = new MainForm();
            mainForm.ShowDialog();

        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void DangKyForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            //Dieu kien giao hang :
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.DisplayMember = "ID";
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            //cbDKGH.SelectedIndex = 0;

            //Phuong thuc thanh toan :
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;


            //Phuong tien van tai mac dinh :
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll();
            cbPTVT.DisplayMember = "Ten";
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            txtTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            //txtTyGiaTienTe.Text = GlobalSettings.TY_GIA_USD;

            txtDiaChiDN.Text = GlobalSettings.DIA_CHI;
            txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            txtMaMid.Text = GlobalSettings.MaMID;
            cbTuDongTinhThue.SelectedValue = GlobalSettings.TuDongTinhThue;
            //txtPass.Text = "";
            //txtRePass.Text = "";

            diaDiemBocHangControl1.ReadOnly = false;
            //diaDiemBocHangControl1.Ma = GlobalSettings.CUA_KHAU;
            nguyenTeControl1.ReadOnly = false;
            nuocHControl1.ReadOnly = false;
            //loaiHinhMauDichHControl1.ReadOnly = false;

        }

        private void DangKyForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.ShowLoginForm();
        }
    }
}

