using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanChiThiSuaDoiCuaHaiQuan : DevExpress.XtraReports.UI.XtraReport
    {
        public BanChiThiSuaDoiCuaHaiQuan()
        {
            InitializeComponent();
        }

        public void BindingReport(VAD1AC0 vad1ac)
        {
            lblSoToKhai.Text = vad1ac.ICN.GetValue().ToString().ToUpper();

            lblSoToKhaiDauTien.Text = vad1ac.FIC.GetValue().ToString().ToUpper();
            lblSoNhanhToKhaiChiaNho.Text = vad1ac.BNO.GetValue().ToString().ToUpper();
            lblTongSoToKhaiChiaNho.Text = vad1ac.DNO.GetValue().ToString().ToUpper();
            lblSoToKhaiTamNhapTaiXuat.Text = vad1ac.TDN.GetValue().ToString().ToUpper();
            lblMaPhanLoaiKiemTra.Text = vad1ac.A06.GetValue().ToString().ToUpper();
            lblMaLoaiHinh.Text = vad1ac.ICB.GetValue().ToString().ToUpper();
            lblMaPhanLoaiHangHoa.Text = vad1ac.CCC.GetValue().ToString().ToUpper();
            lblMaHieuPhuongThucVanChuyen.Text = vad1ac.MTC.GetValue().ToString().ToUpper();
            lblPhanLoaiCaNhanToChuc.Text = vad1ac.SKB.GetValue().ToString().ToUpper();
            lblMaSoHangHoaDaiDienToKhai.Text = vad1ac.A00.GetValue().ToString().ToUpper();
            lblTenCoQuanHaiQuanTiepNhanToKhai.Text = vad1ac.A07.GetValue().ToString().ToUpper();
            lblMaBoPhanXuLyToKhai.Text = vad1ac.CHB.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ac.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDangKy.Text = Convert.ToDateTime(vad1ac.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDangKy.Text = "";
            }
            if (vad1ac.AD1.GetValue().ToString().ToUpper() != "" && vad1ac.AD1.GetValue().ToString().ToUpper() != "0")
            {
                lblGioDangKy.Text = vad1ac.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ac.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayThayDoiDangKy.Text = Convert.ToDateTime(vad1ac.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayThayDoiDangKy.Text = "";
            }
            if (vad1ac.AD3.GetValue().ToString().ToUpper() != "" && vad1ac.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioThayDoiDangKy.Text = vad1ac.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioThayDoiDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ac.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(vad1ac.RED.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanTaiNhapTaiXuat.Text = "";
            }

            BindReportChiThiHQ(vad1ac);
        }
        public void BindReportChiThiHQ(VAD1AC0 vad1ac)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Ten", typeof(string));
                dt.Columns.Add("NoiDung", typeof(string));
                dt.Columns.Add("STT", typeof(string));
                int STT = 0;

                for (int i = 0; i < vad1ac.D__.listAttribute[0].ListValue.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    if (Convert.ToDateTime(vad1ac.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy") != ("01/01/1900"))
                    {
                        dr["Date"] = Convert.ToDateTime(vad1ac.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy");
                    }
                    dr["Ten"] = vad1ac.D__.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                    dr["STT"] = STT.ToString().ToUpper();
                    dr["NoiDung"] = vad1ac.D__.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 10)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["Date"] = "";
                    dr["Ten"] = "";
                    dr["NoiDung"] = "";
                    dr["STT"] = STT.ToString().ToUpper();
                    dt.Rows.Add(dr);
                }
                DetailReport.DataSource = dt;
                lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "Date");
                lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "Ten");
                lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "NoiDung");
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }



        public void BindingReportVAD8000(VAD8000 msg)
        {
            try
            {

                lblSoToKhai.Text = msg.ICN.GetValue().ToString().ToUpper();

                lblSoToKhaiDauTien.Text = msg.FIC.GetValue().ToString().ToUpper();
                if (lblSoToKhaiDauTien.Text == "0") 
                {
                    lblSoToKhaiDauTien.Text = "";
                }
                lblSoNhanhToKhaiChiaNho.Text = msg.BNO.GetValue().ToString().ToUpper();
                if (lblSoNhanhToKhaiChiaNho.Text == "0")
                    lblSoNhanhToKhaiChiaNho.Text = "";
                lblTongSoToKhaiChiaNho.Text = msg.DNO.GetValue().ToString().ToUpper();
                if (lblTongSoToKhaiChiaNho.Text == "0")
                    lblTongSoToKhaiChiaNho.Text = "";
                lblSoToKhaiTamNhapTaiXuat.Text = msg.TDN.GetValue().ToString().ToUpper();
                if (lblSoToKhaiTamNhapTaiXuat.Text == "0")
                    lblSoToKhaiTamNhapTaiXuat.Text = "";
                lblMaPhanLoaiKiemTra.Text = msg.A06.GetValue().ToString().ToUpper();
                lblMaLoaiHinh.Text = msg.A02.GetValue().ToString().ToUpper();
                lblMaPhanLoaiHangHoa.Text = msg.ADA.GetValue().ToString().ToUpper();
                lblMaHieuPhuongThucVanChuyen.Text = msg.ADB.GetValue().ToString().ToUpper();
                lblPhanLoaiCaNhanToChuc.Text = msg.A05.GetValue().ToString().ToUpper();
                lblMaSoHangHoaDaiDienToKhai.Text = msg.A00.GetValue().ToString().ToUpper();
                lblTenCoQuanHaiQuanTiepNhanToKhai.Text = msg.A07.GetValue().ToString().ToUpper();
                lblMaBoPhanXuLyToKhai.Text = msg.A08.GetValue().ToString().ToUpper();
                if (Convert.ToDateTime(msg.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayDangKy.Text = Convert.ToDateTime(msg.A09.GetValue()).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayDangKy.Text = "";
                }
                if (msg.AD1.GetValue().ToString().ToUpper() != "" && msg.AD1.GetValue().ToString().ToUpper() != "0")
                {
                    lblGioDangKy.Text = msg.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + msg.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + msg.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioDangKy.Text = "";
                }
                if (Convert.ToDateTime(msg.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayThayDoiDangKy.Text = Convert.ToDateTime(msg.AD2.GetValue()).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayThayDoiDangKy.Text = "";
                }
                if (msg.AD3.GetValue().ToString().ToUpper() != "" && msg.AD3.GetValue().ToString().ToUpper() != "0")
                {
                    lblGioThayDoiDangKy.Text = msg.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + msg.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + msg.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioThayDoiDangKy.Text = "";
                }
                if (Convert.ToDateTime(msg.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(msg.RED.GetValue()).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblThoiHanTaiNhapTaiXuat.Text = "";
                }
                lblBieuThiTHHetHan.Text = (msg.AAA.GetValue()).ToString();
                lblPhanLoaiChiThiHQ.Text = (msg.B99.GetValue()).ToString();
                //BindReportNoiDung(vae);

               
                DataTable dt = ProcessReport.ConvertListToTable(msg.CT, 10);
                DetailReport.DataSource = dt;
                lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "VAD8000_D01", "{0:dd/MM/yyyy}");
                lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "VAD8000_T01");
                lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "VAD8000_I01");
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            }
            catch (Exception e)
            {
                Logger.LocalLogger.Instance().WriteMessage(e);
            }

        }
        public void BindingReportVAE8000(VAE8000 msg)
        {
            try
            {

                lblSoToKhai.Text = msg.ECN.GetValue().ToString().ToUpper();
                
                lblSoToKhaiDauTien.Text = msg.FIC.GetValue().ToString().ToUpper();
                if (lblSoToKhaiDauTien.Text == "0")
                    lblSoToKhaiDauTien.Text = "";
                lblSoNhanhToKhaiChiaNho.Text = msg.BNO.GetValue().ToString().ToUpper();
                if (lblSoNhanhToKhaiChiaNho.Text == "0")
                    lblSoNhanhToKhaiChiaNho.Text = "";
                lblTongSoToKhaiChiaNho.Text = msg.DNO.GetValue().ToString().ToUpper();
                if (lblTongSoToKhaiChiaNho.Text == "0")
                    lblTongSoToKhaiChiaNho.Text = "";
                lblSoToKhaiTamNhapTaiXuat.Text = msg.TNO.GetValue().ToString().ToUpper();
                lblMaPhanLoaiKiemTra.Text = msg.K07.GetValue().ToString().ToUpper();
                lblMaLoaiHinh.Text = msg.K03.GetValue().ToString().ToUpper();
                lblMaPhanLoaiHangHoa.Text = msg.K05.GetValue().ToString().ToUpper();
                lblMaHieuPhuongThucVanChuyen.Text = msg.ADF.GetValue().ToString().ToUpper();
                lblPhanLoaiCaNhanToChuc.Text = "";// msg.A05.GetValue().ToString().ToUpper();
                lblMaSoHangHoaDaiDienToKhai.Text = msg.K01.GetValue().ToString().ToUpper();
                lblTenCoQuanHaiQuanTiepNhanToKhai.Text = msg.K08.GetValue().ToString().ToUpper();
                lblMaBoPhanXuLyToKhai.Text = msg.K09.GetValue().ToString().ToUpper();
                if (Convert.ToDateTime(msg.K10.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayDangKy.Text = Convert.ToDateTime(msg.K10.GetValue()).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayDangKy.Text = "";
                }
                if (msg.AD1.GetValue().ToString().ToUpper() != "" && msg.AD1.GetValue().ToString().ToUpper() != "0")
                {
                    lblGioDangKy.Text = msg.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + msg.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + msg.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioDangKy.Text = "";
                }
                if (Convert.ToDateTime(msg.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayThayDoiDangKy.Text = Convert.ToDateTime(msg.AD2.GetValue()).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayThayDoiDangKy.Text = "";
                }
                if (msg.AD3.GetValue().ToString().ToUpper() != "" && msg.AD3.GetValue().ToString().ToUpper() != "0")
                {
                    lblGioThayDoiDangKy.Text = msg.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + msg.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + msg.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioThayDoiDangKy.Text = "";
                }
                if (Convert.ToDateTime(msg.RID.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(msg.RID.GetValue()).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblThoiHanTaiNhapTaiXuat.Text = "";
                }
                lblBieuThiTHHetHan.Text = (msg.AAA.GetValue()).ToString();
                lblPhanLoaiChiThiHQ.Text = (msg.CCM.GetValue()).ToString();
                //BindReportNoiDung(vae);


                DataTable dt = ProcessReport.ConvertListToTable(msg.CT, 10);
                DetailReport.DataSource = dt;
                lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "VAE8000_D01", "{0:dd/MM/yyyy}");
                lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "VAE8000_T01");
                lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "VAE8000_I01");
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            }
            catch (Exception e)
            {
                Logger.LocalLogger.Instance().WriteMessage(e);
            }
        }
      

    }
}
