------------------------------------------------------------------------------------------------------------------------        
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_DinhMucChung]        
-- Database: ECS_TQ_SXXK        
-- Author: Ngo Thanh Tung        
-- Time created: Monday, March 22, 2010        
------------------------------------------------------------------------------------------------------------------------        
        
ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_DinhMucChung]        
 @WhereCondition nvarchar(500),        
 @OrderByExpression nvarchar(250) = NULL        
         
AS        
        
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED        
        
DECLARE @SQL nvarchar(3250)        
        
SET @SQL = ' SELECT DMDK.ID ,    
		DMDK.MaHaiQuan,  
		DMDK.SoTiepNhan,
        DMDK.NgayTiepNhan ,      
        DM.MaSanPham ,      
  CASE WHEN DM.MaSanPham IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_SXXK_SanPham WHERE DM.MaSanPham = Ma ) ELSE NULL END AS TenSanPham,      
    CASE WHEN DM.MaSanPham IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID= (SELECT TOP 1 DVT_ID FROM dbo.t_SXXK_SanPham WHERE DM.MaSanPham = Ma)) ELSE DM.DVT_ID END AS DVT_SP,      
        DM.MaNguyenPhuLieu AS MaNPL ,      
  CASE WHEN DM.MaNguyenPhuLieu IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_SXXK_NguyenPhuLieu WHERE DM.MaNguyenPhuLieu = Ma ) ELSE NULL END AS TenNPL,      
  CASE WHEN DM.MaNguyenPhuLieu IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID= (SELECT TOP 1 DVT_ID FROM dbo.t_SXXK_NguyenPhuLieu WHERE DM.MaNguyenPhuLieu = Ma)) ELSE DM.DVT_ID END AS DVT,      
        DM.DinhMucSuDung ,      
        DM.TyLeHaoHut ,      
  DM.DinhMucSuDung + DM.DinhMucSuDung*DM.TyLeHaoHut/100 AS DinhMucChung,      
               CASE WHEN IsFromVietNam = 1 THEN N''Có''      
      ELSE ''Không'' END AS MuaVN      
   FROM dbo.t_KDT_SXXK_DinhMucDangKy DMDK INNER JOIN t_kdt_SXXK_DinhMuc DM ON  DM.Master_ID = DMDK.ID WHERE        
'+ @WhereCondition        
        
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0        
BEGIN        
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression        
END        
        
EXEC sp_executesql @SQL        
    

GO

GO

------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamicVouchers]    
-- Database: ECS_TQDT_KD_VNACCS    
-- Author: Ngo Thanh Tung    
-- Time created: Thursday, January 23, 2014    
------------------------------------------------------------------------------------------------------------------------    
  
ALTER PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamicVouchers]    
 @WhereCondition NVARCHAR(MAX)  
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT TKMD.ID,  
  SoToKhai,  
  NgayDangKy,  
        TKMD.MaLoaiHinh,  
     SoHoaDon,    
  MaPhanLoaiKiemTra,  
  (SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_CertificateOfOrigin WHERE TKMD_ID =TKMD.ID ) AS CertificateOfOrigin,  
  (SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_BillOfLading WHERE TKMD_ID =TKMD.ID ) AS BillOfLading,  
  (SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_ContractDocument WHERE TKMD_ID =TKMD.ID ) AS ContractDocument,  
  (SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_CommercialInvoice WHERE TKMD_ID =TKMD.ID ) AS CommercialInvoice,  
  (SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_License WHERE TKMD_ID =TKMD.ID ) AS License,  
  (SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_Container_Details WHERE TKMD_ID =TKMD.ID ) AS Container,  
  (SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_AdditionalDocument WHERE TKMD_ID =TKMD.ID ) AS AdditionalDocument  
  FROM dbo.t_KDT_VNACC_ToKhaiMauDich TKMD   
LEFT JOIN t_KDT_VNACCS_CertificateOfOrigin CO ON CO.TKMD_ID = TKMD.ID  
LEFT JOIN t_KDT_VNACCS_BillOfLading BL ON BL.TKMD_ID = TKMD.ID  
LEFT JOIN t_KDT_VNACCS_ContractDocument CT ON CT.TKMD_ID = TKMD.ID  
LEFT JOIN t_KDT_VNACCS_CommercialInvoice CM ON CM.TKMD_ID = TKMD.ID  
LEFT JOIN t_KDT_VNACCS_License LC ON LC.TKMD_ID = TKMD.ID  
LEFT JOIN t_KDT_VNACCS_Container_Details CTD ON CTD.TKMD_ID = TKMD.ID  
LEFT JOIN t_KDT_VNACCS_AdditionalDocument AD ON AD.TKMD_ID = TKMD.ID       
WHERE  ' + @WhereCondition +  ' GROUP BY TKMD.ID,TKMD.SoToKhai,TKMD.NgayDangKy,TKMD.MaLoaiHinh,TKMD.SoHoaDon,TKMD.MaPhanLoaiKiemTra  '  
    
EXEC sp_executesql @SQL    

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '38.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('38.9',GETDATE(), N'CẬP NHẬT PROCDURE AUTO FEEDBACK CHỨNG TỪ ')
END    