-- Drop Existing Procedures

/****** Object:  Table [dbo].[t_KDT_ChungThuGiamDinh]    Script Date: 12/9/2012 11:02:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[t_KDT_ChungThuGiamDinh](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[NgayDangKy] [datetime] NULL,
	[SoTiepNhan] [bigint] NULL,
	[TrangThai] [int] NULL,
	[LoaiKB] [int] NULL,
	[GuidStr] [nvarchar](50) NULL,
	[DiaDiem] [nvarchar](255) NULL,
	[MaCoQuanGD] [nvarchar](35) NULL,
	[TenCoQuanGD] [nvarchar](50) NULL,
	[CanBoGD] [nvarchar](50) NULL,
	[NoiDung] [nvarchar](255) NULL,
	[KetQua] [nvarchar](255) NULL,
	[ThongTinKhac] [nvarchar](2000) NULL,
 CONSTRAINT [PK_t_KDT_ChungThuGiamDinh] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[t_KDT_ChungThuGiamDinh]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_ChungThuGiamDinh_t_KDT_ToKhaiMauDich] FOREIGN KEY([TKMD_ID])
REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_ChungThuGiamDinh] CHECK CONSTRAINT [FK_t_KDT_ChungThuGiamDinh_t_KDT_ToKhaiMauDich]
GO


IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_DeleteBy_TKMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_Insert]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_Insert]
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@SoTiepNhan bigint,
	@TrangThai int,
	@LoaiKB int,
	@GuidStr nvarchar(50),
	@DiaDiem nvarchar(255),
	@MaCoQuanGD nvarchar(35),
	@TenCoQuanGD nvarchar(50),
	@CanBoGD nvarchar(50),
	@NoiDung nvarchar(255),
	@KetQua nvarchar(255),
	@ThongTinKhac nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ChungThuGiamDinh]
(
	[TKMD_ID],
	[NgayDangKy],
	[SoTiepNhan],
	[TrangThai],
	[LoaiKB],
	[GuidStr],
	[DiaDiem],
	[MaCoQuanGD],
	[TenCoQuanGD],
	[CanBoGD],
	[NoiDung],
	[KetQua],
	[ThongTinKhac]
)
VALUES 
(
	@TKMD_ID,
	@NgayDangKy,
	@SoTiepNhan,
	@TrangThai,
	@LoaiKB,
	@GuidStr,
	@DiaDiem,
	@MaCoQuanGD,
	@TenCoQuanGD,
	@CanBoGD,
	@NoiDung,
	@KetQua,
	@ThongTinKhac
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_Update]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@SoTiepNhan bigint,
	@TrangThai int,
	@LoaiKB int,
	@GuidStr nvarchar(50),
	@DiaDiem nvarchar(255),
	@MaCoQuanGD nvarchar(35),
	@TenCoQuanGD nvarchar(50),
	@CanBoGD nvarchar(50),
	@NoiDung nvarchar(255),
	@KetQua nvarchar(255),
	@ThongTinKhac nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_ChungThuGiamDinh]
SET
	[TKMD_ID] = @TKMD_ID,
	[NgayDangKy] = @NgayDangKy,
	[SoTiepNhan] = @SoTiepNhan,
	[TrangThai] = @TrangThai,
	[LoaiKB] = @LoaiKB,
	[GuidStr] = @GuidStr,
	[DiaDiem] = @DiaDiem,
	[MaCoQuanGD] = @MaCoQuanGD,
	[TenCoQuanGD] = @TenCoQuanGD,
	[CanBoGD] = @CanBoGD,
	[NoiDung] = @NoiDung,
	[KetQua] = @KetQua,
	[ThongTinKhac] = @ThongTinKhac
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_InsertUpdate]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@SoTiepNhan bigint,
	@TrangThai int,
	@LoaiKB int,
	@GuidStr nvarchar(50),
	@DiaDiem nvarchar(255),
	@MaCoQuanGD nvarchar(35),
	@TenCoQuanGD nvarchar(50),
	@CanBoGD nvarchar(50),
	@NoiDung nvarchar(255),
	@KetQua nvarchar(255),
	@ThongTinKhac nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ChungThuGiamDinh] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ChungThuGiamDinh] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[NgayDangKy] = @NgayDangKy,
			[SoTiepNhan] = @SoTiepNhan,
			[TrangThai] = @TrangThai,
			[LoaiKB] = @LoaiKB,
			[GuidStr] = @GuidStr,
			[DiaDiem] = @DiaDiem,
			[MaCoQuanGD] = @MaCoQuanGD,
			[TenCoQuanGD] = @TenCoQuanGD,
			[CanBoGD] = @CanBoGD,
			[NoiDung] = @NoiDung,
			[KetQua] = @KetQua,
			[ThongTinKhac] = @ThongTinKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ChungThuGiamDinh]
		(
			[TKMD_ID],
			[NgayDangKy],
			[SoTiepNhan],
			[TrangThai],
			[LoaiKB],
			[GuidStr],
			[DiaDiem],
			[MaCoQuanGD],
			[TenCoQuanGD],
			[CanBoGD],
			[NoiDung],
			[KetQua],
			[ThongTinKhac]
		)
		VALUES 
		(
			@TKMD_ID,
			@NgayDangKy,
			@SoTiepNhan,
			@TrangThai,
			@LoaiKB,
			@GuidStr,
			@DiaDiem,
			@MaCoQuanGD,
			@TenCoQuanGD,
			@CanBoGD,
			@NoiDung,
			@KetQua,
			@ThongTinKhac
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_Delete]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ChungThuGiamDinh]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_ChungThuGiamDinh]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ChungThuGiamDinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_Load]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[SoTiepNhan],
	[TrangThai],
	[LoaiKB],
	[GuidStr],
	[DiaDiem],
	[MaCoQuanGD],
	[TenCoQuanGD],
	[CanBoGD],
	[NoiDung],
	[KetQua],
	[ThongTinKhac]
FROM
	[dbo].[t_KDT_ChungThuGiamDinh]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[SoTiepNhan],
	[TrangThai],
	[LoaiKB],
	[GuidStr],
	[DiaDiem],
	[MaCoQuanGD],
	[TenCoQuanGD],
	[CanBoGD],
	[NoiDung],
	[KetQua],
	[ThongTinKhac]
FROM
	[dbo].[t_KDT_ChungThuGiamDinh]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_SelectDynamic]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[SoTiepNhan],
	[TrangThai],
	[LoaiKB],
	[GuidStr],
	[DiaDiem],
	[MaCoQuanGD],
	[TenCoQuanGD],
	[CanBoGD],
	[NoiDung],
	[KetQua],
	[ThongTinKhac]
FROM [dbo].[t_KDT_ChungThuGiamDinh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_SelectAll]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[SoTiepNhan],
	[TrangThai],
	[LoaiKB],
	[GuidStr],
	[DiaDiem],
	[MaCoQuanGD],
	[TenCoQuanGD],
	[CanBoGD],
	[NoiDung],
	[KetQua],
	[ThongTinKhac]
FROM
	[dbo].[t_KDT_ChungThuGiamDinh]	

GO

