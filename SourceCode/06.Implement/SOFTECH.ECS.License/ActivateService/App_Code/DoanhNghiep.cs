﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Components
{
    public partial class DoanhNghiep
    {
        #region Private members.

        protected string _MaDoanhNghiep = string.Empty;
        protected string _TenDoanhNghiep = string.Empty;
        protected long _ID;
        protected string _DiaChi = string.Empty;
        protected string _LoaiHinh = string.Empty;
        protected string _GhiChu = string.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }

        public string TenDoanhNghiep
        {
            set { this._TenDoanhNghiep = value; }
            get { return this._TenDoanhNghiep; }
        }

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string DiaChi
        {
            set { this._DiaChi = value; }
            get { return this._DiaChi; }
        }

        public string LoaiHinh
        {
            set { this._LoaiHinh = value; }
            get { return this._LoaiHinh; }
        }

        public string GhiChu
        {
            set { this._GhiChu = value; }
            get { return this._GhiChu; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static DoanhNghiep Load(long iD)
        {
            const string spName = "[dbo].[p_DoanhNghiep_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            DoanhNghiep entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new DoanhNghiep();
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHinh"))) entity.LoaiHinh = reader.GetString(reader.GetOrdinal("LoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------
        public static List<DoanhNghiep> SelectCollectionAll()
        {
            List<DoanhNghiep> collection = new List<DoanhNghiep>();
            SqlDataReader reader = SelectReaderAll();
            while (reader.Read())
            {
                DoanhNghiep entity = new DoanhNghiep();

                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHinh"))) entity.LoaiHinh = reader.GetString(reader.GetOrdinal("LoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public static List<DoanhNghiep> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            List<DoanhNghiep> collection = new List<DoanhNghiep>();

            SqlDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                DoanhNghiep entity = new DoanhNghiep();

                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHinh"))) entity.LoaiHinh = reader.GetString(reader.GetOrdinal("LoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_DoanhNghiep_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_DoanhNghiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_DoanhNghiep_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_DoanhNghiep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertDoanhNghiep(string maDoanhNghiep, string tenDoanhNghiep, string diaChi, string loaiHinh, string ghiChu)
        {
            DoanhNghiep entity = new DoanhNghiep();
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.TenDoanhNghiep = tenDoanhNghiep;
            entity.DiaChi = diaChi;
            entity.LoaiHinh = loaiHinh;
            entity.GhiChu = ghiChu;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_DoanhNghiep_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
            db.AddInParameter(dbCommand, "@LoaiHinh", SqlDbType.VarChar, LoaiHinh);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@MaDoanhNghiep");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@MaDoanhNghiep");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<DoanhNghiep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (DoanhNghiep item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateDoanhNghiep(string maDoanhNghiep, string tenDoanhNghiep, long iD, string diaChi, string loaiHinh, string ghiChu)
        {
            DoanhNghiep entity = new DoanhNghiep();
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.TenDoanhNghiep = tenDoanhNghiep;
            entity.DiaChi = diaChi;
            entity.LoaiHinh = loaiHinh;
            entity.GhiChu = ghiChu;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_DoanhNghiep_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
            db.AddInParameter(dbCommand, "@LoaiHinh", SqlDbType.VarChar, LoaiHinh);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<DoanhNghiep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (DoanhNghiep item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateDoanhNghiep(string maDoanhNghiep, string tenDoanhNghiep, long iD, string diaChi, string loaiHinh, string ghiChu)
        {
            DoanhNghiep entity = new DoanhNghiep();
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.TenDoanhNghiep = tenDoanhNghiep;
            entity.ID = iD;
            entity.DiaChi = diaChi;
            entity.LoaiHinh = loaiHinh;
            entity.GhiChu = ghiChu;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_DoanhNghiep_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
            db.AddInParameter(dbCommand, "@LoaiHinh", SqlDbType.VarChar, LoaiHinh);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<DoanhNghiep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (DoanhNghiep item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteDoanhNghiep(long iD)
        {
            DoanhNghiep entity = new DoanhNghiep();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_DoanhNghiep_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<DoanhNghiep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (DoanhNghiep item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}