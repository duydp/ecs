/*
Run this script on:

        ecsteam.ECS_TQDT_KD_VERSION_1    -  This database will be modified

to synchronize it with:

        113.160.225.156.ECS_TQDT_KD

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 11/05/2011 2:59:15 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[t_KDT_Messages]'
GO
CREATE TABLE [dbo].[t_KDT_Messages]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ReferenceID] [uniqueidentifier] NULL,
[ItemID] [bigint] NULL,
[MessageFrom] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageType] [int] NULL,
[MessageFunction] [int] NULL,
[MessageContent] [xml] NULL,
[CreatedTime] [datetime] NULL,
[TieuDeThongBao] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiDungThongBao] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_ThongDiep] on [dbo].[t_KDT_Messages]'
GO
ALTER TABLE [dbo].[t_KDT_Messages] ADD CONSTRAINT [PK_t_KDT_ThongDiep] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Message_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_Update]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Message_Update]
	@ID int,
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@MessageFrom varchar(50),
	@MessageTo varchar(50),
	@MessageType int,
	@MessageFunction int,
	@MessageContent xml,
	@CreatedTime datetime,
	@TieuDeThongBao nvarchar(100),
	@NoiDungThongBao nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT_Messages]
SET
	[ReferenceID] = @ReferenceID,
	[ItemID] = @ItemID,
	[MessageFrom] = @MessageFrom,
	[MessageTo] = @MessageTo,
	[MessageType] = @MessageType,
	[MessageFunction] = @MessageFunction,
	[MessageContent] = @MessageContent,
	[CreatedTime] = @CreatedTime,
	[TieuDeThongBao] = @TieuDeThongBao,
	[NoiDungThongBao] = @NoiDungThongBao
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_Nuoc]'
GO
CREATE TABLE [dbo].[t_HaiQuan_Nuoc]
(
[ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_Nuoc] on [dbo].[t_HaiQuan_Nuoc]'
GO
ALTER TABLE [dbo].[t_HaiQuan_Nuoc] ADD CONSTRAINT [PK_t_HaiQuan_Nuoc] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_DonViTinh]'
GO
CREATE TABLE [dbo].[t_HaiQuan_DonViTinh]
(
[ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_DonViTinh] on [dbo].[t_HaiQuan_DonViTinh]'
GO
ALTER TABLE [dbo].[t_HaiQuan_DonViTinh] ADD CONSTRAINT [PK_t_HaiQuan_DonViTinh] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_NguyenTe]'
GO
CREATE TABLE [dbo].[t_HaiQuan_NguyenTe]
(
[ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TyGiaTinhThue] [money] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_NguyenTe] on [dbo].[t_HaiQuan_NguyenTe]'
GO
ALTER TABLE [dbo].[t_HaiQuan_NguyenTe] ADD CONSTRAINT [PK_t_HaiQuan_NguyenTe] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NguyenTe_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_SelectAll]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 13, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NguyenTe_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[TyGiaTinhThue]
FROM
	[dbo].[t_HaiQuan_NguyenTe]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_DoiTac]'
GO
CREATE TABLE [dbo].[t_HaiQuan_DoiTac]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[MaCongTy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenCongTy] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaChi] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DienThoai] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChu] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDoanhNghiep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_DoiTac] on [dbo].[t_HaiQuan_DoiTac]'
GO
ALTER TABLE [dbo].[t_HaiQuan_DoiTac] ADD CONSTRAINT [PK_t_HaiQuan_DoiTac] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DoiTac_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DoiTac_Insert]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 06, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DoiTac_Insert]
	@MaCongTy varchar(100),
	@TenCongTy varchar(500),
	@DiaChi nvarchar(500),
	@DienThoai varchar(50),
	@Email varchar(50),
	@Fax varchar(50),
	@GhiChu nvarchar(1000),
	@MaDoanhNghiep varchar(50),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_HaiQuan_DoiTac]
(
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep]
)
VALUES 
(
	@MaCongTy,
	@TenCongTy,
	@DiaChi,
	@DienThoai,
	@Email,
	@Fax,
	@GhiChu,
	@MaDoanhNghiep
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_MaHS]'
GO
CREATE TABLE [dbo].[t_HaiQuan_MaHS]
(
[Nhom] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PN1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PN2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pn3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mo_ta] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HS10SO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_MaHS] on [dbo].[t_HaiQuan_MaHS]'
GO
ALTER TABLE [dbo].[t_HaiQuan_MaHS] ADD CONSTRAINT [PK_t_HaiQuan_MaHS] PRIMARY KEY CLUSTERED  ([HS10SO])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaH_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaH_Delete]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 07, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaH_Delete]
	@HS10SO nvarchar(255)
AS
DELETE FROM 
	[dbo].[t_HaiQuan_MaHS]
WHERE
	[HS10SO] = @HS10SO

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DoiTac_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DoiTac_Update]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 06, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DoiTac_Update]
	@ID bigint,
	@MaCongTy varchar(100),
	@TenCongTy varchar(500),
	@DiaChi nvarchar(500),
	@DienThoai varchar(50),
	@Email varchar(50),
	@Fax varchar(50),
	@GhiChu nvarchar(1000),
	@MaDoanhNghiep varchar(50)
AS
UPDATE
	[dbo].[t_HaiQuan_DoiTac]
SET
	[MaCongTy] = @MaCongTy,
	[TenCongTy] = @TenCongTy,
	[DiaChi] = @DiaChi,
	[DienThoai] = @DienThoai,
	[Email] = @Email,
	[Fax] = @Fax,
	[GhiChu] = @GhiChu,
	[MaDoanhNghiep] = @MaDoanhNghiep
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_SXXK_ToKhaiMauDich]'
GO
CREATE TABLE [dbo].[t_SXXK_ToKhaiMauDich]
(
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoToKhai] [int] NOT NULL,
[MaLoaiHinh] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NamDangKy] [smallint] NOT NULL,
[NgayDangKy] [datetime] NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenDoanhNghiep] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDaiLyTTHQ] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDaiLyTTHQ] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonViDoiTac] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChiTietDonViDoiTac] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoGiayPhep] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayGiayPhep] [datetime] NULL,
[NgayHetHanGiayPhep] [datetime] NULL,
[SoHopDong] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHopDong] [datetime] NULL,
[NgayHetHanHopDong] [datetime] NULL,
[SoHoaDonThuongMai] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHoaDonThuongMai] [datetime] NULL,
[PTVT_ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoHieuPTVT] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayDenPTVT] [datetime] NULL,
[QuocTichPTVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiVanDon] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoVanDon] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayVanDon] [datetime] NULL,
[NuocXK_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NuocNK_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiaDiemXepHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CuaKhau_ID] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DKGH_ID] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NguyenTe_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TyGiaTinhThue] [money] NOT NULL,
[TyGiaUSD] [money] NOT NULL,
[PTTT_ID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoHang] [smallint] NOT NULL,
[SoLuongPLTK] [smallint] NULL,
[TenChuHang] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoContainer20] [numeric] (15, 0) NULL,
[SoContainer40] [numeric] (15, 0) NULL,
[SoKien] [numeric] (15, 0) NULL,
[TrongLuong] [numeric] (18, 2) NULL,
[TongTriGiaKhaiBao] [money] NOT NULL,
[TongTriGiaTinhThue] [money] NOT NULL,
[LoaiToKhaiGiaCong] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LePhiHaiQuan] [money] NULL,
[PhiBaoHiem] [money] NULL,
[PhiVanChuyen] [money] NULL,
[PhiXepDoHang] [money] NULL,
[PhiKhac] [money] NULL,
[Xuat_NPL_SP] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThanhLy] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NGAY_THN_THX] [datetime] NULL,
[MaDonViUT] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrangThaiThanhKhoan] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChungTu] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLuong] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHoanThanh] [datetime] NULL,
[TrangThai] [smallint] NULL,
[TrongLuongNet] [float] NULL,
[SoTienKhoan] [money] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_SXXK_ToKhaiMauDich] on [dbo].[t_SXXK_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_ToKhaiMauDich] ADD CONSTRAINT [PK_t_SXXK_ToKhaiMauDich] PRIMARY KEY CLUSTERED  ([MaHaiQuan], [SoToKhai], [MaLoaiHinh], [NamDangKy])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GROUPS]'
GO
CREATE TABLE [dbo].[GROUPS]
(
[MA_NHOM] [bigint] NOT NULL IDENTITY(1, 1),
[TEN_NHOM] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MO_TA] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SXXK_GROUPS] on [dbo].[GROUPS]'
GO
ALTER TABLE [dbo].[GROUPS] ADD CONSTRAINT [PK_SXXK_GROUPS] PRIMARY KEY CLUSTERED  ([MA_NHOM])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_SelectAll]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MA_NHOM],
	[TEN_NHOM],
	[MO_TA]
FROM
	[dbo].[GROUPS]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_VanDon]'
GO
CREATE TABLE [dbo].[t_KDT_VanDon]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoVanDon] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayVanDon] [datetime] NOT NULL,
[HangRoi] [bit] NULL,
[SoHieuPTVT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayDenPTVT] [datetime] NOT NULL,
[MaHangVT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHangVT] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenPTVT] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuocTichPTVT] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NuocXuat_ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNguoiNhanHang] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNguoiNhanHang] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNguoiGiaoHang] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNguoiGiaoHang] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CuaKhauNhap_ID] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CuaKhauXuat] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNguoiNhanHangTrungGian] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNguoiNhanHangTrungGian] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaCangXepHang] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenCangXepHang] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaCangDoHang] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenCangDoHang] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKMD_ID] [bigint] NOT NULL,
[DKGH_ID] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiaDiemGiaoHang] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiDi] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoHieuChuyenDi] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayKhoiHanh] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_VanDon] on [dbo].[t_KDT_VanDon]'
GO
ALTER TABLE [dbo].[t_KDT_VanDon] ADD CONSTRAINT [PK_t_KDT_VanDon] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_Container]'
GO
CREATE TABLE [dbo].[t_KDT_Container]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[VanDon_ID] [bigint] NOT NULL,
[SoHieu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoaiContainer] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Seal_No] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Trang_thai] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_Container] on [dbo].[t_KDT_Container]'
GO
ALTER TABLE [dbo].[t_KDT_Container] ADD CONSTRAINT [PK_t_KDT_Container] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_SXXK_ToKhaiTriGia]'
GO
CREATE TABLE [dbo].[t_KDT_SXXK_ToKhaiTriGia]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[ToSo] [bigint] NOT NULL,
[LoaiToKhaiTriGia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayXuatKhau] [datetime] NOT NULL,
[CapDoThuongMai] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[QuyenSuDung] [bit] NOT NULL,
[KhongXacDinh] [bit] NOT NULL,
[TraThem] [bit] NOT NULL,
[TienTra] [bit] NOT NULL,
[CoQuanHeDacBiet] [bit] NOT NULL,
[AnhHuongQuanHe] [bit] NOT NULL,
[GhiChep] [nvarchar] (240) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayKhaiBao] [datetime] NULL,
[NguoiKhaiBao] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChucDanhNguoiKhaiBao] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayTruyen] [datetime] NULL,
[KieuQuanHe] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_SXX_ToKhaiTriGia] on [dbo].[t_KDT_SXXK_ToKhaiTriGia]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_ToKhaiTriGia] ADD CONSTRAINT [PK_t_KDT_SXX_ToKhaiTriGia] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_Load]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[ToSo],
	[LoaiToKhaiTriGia],
	[NgayXuatKhau],
	[CapDoThuongMai],
	[QuyenSuDung],
	[KhongXacDinh],
	[TraThem],
	[TienTra],
	[CoQuanHeDacBiet],
	[AnhHuongQuanHe],
	[GhiChep],
	[NgayKhaiBao],
	[NguoiKhaiBao],
	[ChucDanhNguoiKhaiBao],
	[NgayTruyen],
	[KieuQuanHe]
FROM
	[dbo].[t_KDT_SXXK_ToKhaiTriGia]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_HopDongThuongMaiDetail]'
GO
CREATE TABLE [dbo].[t_KDT_HopDongThuongMaiDetail]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[HMD_ID] [bigint] NOT NULL,
[HopDongTM_ID] [bigint] NOT NULL,
[GhiChu] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoThuTuHang] [int] NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhu] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (252) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (18, 3) NULL,
[DonGiaKB] [float] NULL,
[TriGiaKB] [float] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_HopDong_Detail] on [dbo].[t_KDT_HopDongThuongMaiDetail]'
GO
ALTER TABLE [dbo].[t_KDT_HopDongThuongMaiDetail] ADD CONSTRAINT [PK_t_KDT_HopDong_Detail] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_Insert]'
GO





------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_Insert]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_Insert]
	@HMD_ID bigint,
	@HopDongTM_ID bigint,
	@GhiChu nvarchar(255),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@TriGiaKB float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HopDongThuongMaiDetail]
(
	[HMD_ID],
	[HopDongTM_ID],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
)
VALUES 
(
	@HMD_ID,
	@HopDongTM_ID,
	@GhiChu,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB
)

SET @ID = SCOPE_IDENTITY()



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DoiTac_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DoiTac_InsertUpdate]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 06, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DoiTac_InsertUpdate]
	@ID bigint,
	@MaCongTy varchar(100),
	@TenCongTy varchar(500),
	@DiaChi nvarchar(500),
	@DienThoai varchar(50),
	@Email varchar(50),
	@Fax varchar(50),
	@GhiChu nvarchar(1000),
	@MaDoanhNghiep varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DoiTac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DoiTac] 
		SET
			[MaCongTy] = @MaCongTy,
			[TenCongTy] = @TenCongTy,
			[DiaChi] = @DiaChi,
			[DienThoai] = @DienThoai,
			[Email] = @Email,
			[Fax] = @Fax,
			[GhiChu] = @GhiChu,
			[MaDoanhNghiep] = @MaDoanhNghiep
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_HaiQuan_DoiTac]
		(
			[MaCongTy],
			[TenCongTy],
			[DiaChi],
			[DienThoai],
			[Email],
			[Fax],
			[GhiChu],
			[MaDoanhNghiep]
		)
		VALUES 
		(
			@MaCongTy,
			@TenCongTy,
			@DiaChi,
			@DienThoai,
			@Email,
			@Fax,
			@GhiChu,
			@MaDoanhNghiep
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_KD_ToKhaiTriGiaPP23]'
GO
CREATE TABLE [dbo].[t_KDT_KD_ToKhaiTriGiaPP23]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NULL,
[MaToKhaiTriGia] [int] NULL,
[LyDo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STTHang] [int] NULL,
[NgayXuat] [datetime] NULL,
[STTHangTT] [int] NULL,
[TenHangTT] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayXuatTT] [datetime] NULL,
[SoTKHangTT] [bigint] NULL,
[NgayDangKyHangTT] [datetime] NULL,
[MaLoaiHinhHangTT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaHaiQuanHangTT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriGiaNguyenTeHangTT] [float] NULL,
[DieuChinhCongThuongMai] [float] NULL,
[DieuChinhCongSoLuong] [float] NULL,
[DieuChinhCongKhoanGiamGiaKhac] [float] NULL,
[DieuChinhCongChiPhiVanTai] [float] NULL,
[DieuChinhCongChiPhiBaoHiem] [float] NULL,
[TriGiaNguyenTeHang] [float] NULL,
[TriGiaTTHang] [float] NULL,
[ToSo] [int] NULL,
[DieuChinhTruCapDoThuongMai] [float] NULL,
[DieuChinhTruSoLuong] [float] NULL,
[DieuChinhTruKhoanGiamGiaKhac] [float] NULL,
[DieuChinhTruChiPhiVanTai] [float] NULL,
[DieuChinhTruChiPhiBaoHiem] [float] NULL,
[GiaiTrinh] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_KD_ToKhaiTriGiaPP23] on [dbo].[t_KDT_KD_ToKhaiTriGiaPP23]'
GO
ALTER TABLE [dbo].[t_KDT_KD_ToKhaiTriGiaPP23] ADD CONSTRAINT [PK_t_KDT_KD_ToKhaiTriGiaPP23] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Update]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaToKhaiTriGia int,
	@LyDo nvarchar(50),
	@TenHang nvarchar(80),
	@STTHang int,
	@NgayXuat datetime,
	@STTHangTT int,
	@TenHangTT nvarchar(80),
	@NgayXuatTT datetime,
	@SoTKHangTT bigint,
	@NgayDangKyHangTT datetime,
	@MaLoaiHinhHangTT varchar(50),
	@MaHaiQuanHangTT varchar(50),
	@TriGiaNguyenTeHangTT float,
	@DieuChinhCongThuongMai float,
	@DieuChinhCongSoLuong float,
	@DieuChinhCongKhoanGiamGiaKhac float,
	@DieuChinhCongChiPhiVanTai float,
	@DieuChinhCongChiPhiBaoHiem float,
	@TriGiaNguyenTeHang float,
	@TriGiaTTHang float,
	@ToSo int,
	@DieuChinhTruCapDoThuongMai float,
	@DieuChinhTruSoLuong float,
	@DieuChinhTruKhoanGiamGiaKhac float,
	@DieuChinhTruChiPhiVanTai float,
	@DieuChinhTruChiPhiBaoHiem float,
	@GiaiTrinh nvarchar(250)
AS
UPDATE
	[dbo].[t_KDT_KD_ToKhaiTriGiaPP23]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaToKhaiTriGia] = @MaToKhaiTriGia,
	[LyDo] = @LyDo,
	[TenHang] = @TenHang,
	[STTHang] = @STTHang,
	[NgayXuat] = @NgayXuat,
	[STTHangTT] = @STTHangTT,
	[TenHangTT] = @TenHangTT,
	[NgayXuatTT] = @NgayXuatTT,
	[SoTKHangTT] = @SoTKHangTT,
	[NgayDangKyHangTT] = @NgayDangKyHangTT,
	[MaLoaiHinhHangTT] = @MaLoaiHinhHangTT,
	[MaHaiQuanHangTT] = @MaHaiQuanHangTT,
	[TriGiaNguyenTeHangTT] = @TriGiaNguyenTeHangTT,
	[DieuChinhCongThuongMai] = @DieuChinhCongThuongMai,
	[DieuChinhCongSoLuong] = @DieuChinhCongSoLuong,
	[DieuChinhCongKhoanGiamGiaKhac] = @DieuChinhCongKhoanGiamGiaKhac,
	[DieuChinhCongChiPhiVanTai] = @DieuChinhCongChiPhiVanTai,
	[DieuChinhCongChiPhiBaoHiem] = @DieuChinhCongChiPhiBaoHiem,
	[TriGiaNguyenTeHang] = @TriGiaNguyenTeHang,
	[TriGiaTTHang] = @TriGiaTTHang,
	[ToSo] = @ToSo,
	[DieuChinhTruCapDoThuongMai] = @DieuChinhTruCapDoThuongMai,
	[DieuChinhTruSoLuong] = @DieuChinhTruSoLuong,
	[DieuChinhTruKhoanGiamGiaKhac] = @DieuChinhTruKhoanGiamGiaKhac,
	[DieuChinhTruChiPhiVanTai] = @DieuChinhTruChiPhiVanTai,
	[DieuChinhTruChiPhiBaoHiem] = @DieuChinhTruChiPhiBaoHiem,
	[GiaiTrinh] = @GiaiTrinh
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_HuyToKhai]'
GO
CREATE TABLE [dbo].[t_KDT_HuyToKhai]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoTiepNhan] [bigint] NULL,
[NgayTiepNhan] [datetime] NULL,
[NamTiepNhan] [int] NULL,
[TrangThai] [int] NULL,
[Guid] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LyDoHuy] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKMD_ID] [bigint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_HuyToKhai] on [dbo].[t_KDT_HuyToKhai]'
GO
ALTER TABLE [dbo].[t_KDT_HuyToKhai] ADD CONSTRAINT [PK_t_KDT_HuyToKhai] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_Insert]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@NamTiepNhan int,
	@TrangThai int,
	@Guid varchar(500),
	@LyDoHuy nvarchar(500),
	@TKMD_ID bigint,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HuyToKhai]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[NamTiepNhan],
	[TrangThai],
	[Guid],
	[LyDoHuy],
	[TKMD_ID]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@NamTiepNhan,
	@TrangThai,
	@Guid,
	@LyDoHuy,
	@TKMD_ID
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_InsertUpdate]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_InsertUpdate]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_InsertUpdate]
	@ID bigint,
	@HMD_ID bigint,
	@HopDongTM_ID bigint,
	@GhiChu nvarchar(255),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@TriGiaKB float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HopDongThuongMaiDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HopDongThuongMaiDetail] 
		SET
			[HMD_ID] = @HMD_ID,
			[HopDongTM_ID] = @HopDongTM_ID,
			[GhiChu] = @GhiChu,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HopDongThuongMaiDetail]
		(
			[HMD_ID],
			[HopDongTM_ID],
			[GhiChu],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB]
		)
		VALUES 
		(
			@HMD_ID,
			@HopDongTM_ID,
			@GhiChu,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB
		)		
	END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_CO]'
GO
CREATE TABLE [dbo].[t_KDT_CO]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoCO] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayCO] [datetime] NOT NULL,
[ToChucCap] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NuocCapCO] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNuocXKTrenCO] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNuocNKTrenCO] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenDiaChiNguoiXK] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDiaChiNguoiNK] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiCO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ThongTinMoTaChiTiet] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDoanhNghiep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NguoiKy] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoCo] [int] NULL,
[ThoiHanNop] [datetime] NULL,
[TKMD_ID] [bigint] NULL,
[GuidStr] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiKB] [int] NULL,
[SoTiepNhan] [bigint] NULL,
[NgayTiepNhan] [datetime] NULL,
[TrangThai] [int] NULL,
[NamTiepNhan] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_CO] on [dbo].[t_KDT_CO]'
GO
ALTER TABLE [dbo].[t_KDT_CO] ADD CONSTRAINT [PK_t_KDT_CO] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_SelectBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
FROM
	[dbo].[t_KDT_CO]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[USER_GROUP]'
GO
CREATE TABLE [dbo].[USER_GROUP]
(
[MA_NHOM] [bigint] NOT NULL,
[USER_ID] [bigint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_USER_GROUP] on [dbo].[USER_GROUP]'
GO
ALTER TABLE [dbo].[USER_GROUP] ADD CONSTRAINT [PK_USER_GROUP] PRIMARY KEY CLUSTERED  ([MA_NHOM], [USER_ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_USER_GROUP_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_USER_GROUP_Delete]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_USER_GROUP_Delete]
	@MA_NHOM bigint,
	@USER_ID bigint
AS
DELETE FROM 
	[dbo].[USER_GROUP]
WHERE
	[MA_NHOM] = @MA_NHOM
	AND [USER_ID] = @USER_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DoiTac_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DoiTac_Delete]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 06, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DoiTac_Delete]
	@ID bigint
AS
DELETE FROM 
	[dbo].[t_HaiQuan_DoiTac]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_Update]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@NamTiepNhan int,
	@TrangThai int,
	@Guid varchar(500),
	@LyDoHuy nvarchar(500),
	@TKMD_ID bigint
AS

UPDATE
	[dbo].[t_KDT_HuyToKhai]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[NamTiepNhan] = @NamTiepNhan,
	[TrangThai] = @TrangThai,
	[Guid] = @Guid,
	[LyDoHuy] = @LyDoHuy,
	[TKMD_ID] = @TKMD_ID
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_InsertUpdate]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@NamTiepNhan int,
	@TrangThai int,
	@Guid varchar(500),
	@LyDoHuy nvarchar(500),
	@TKMD_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HuyToKhai] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HuyToKhai] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[NamTiepNhan] = @NamTiepNhan,
			[TrangThai] = @TrangThai,
			[Guid] = @Guid,
			[LyDoHuy] = @LyDoHuy,
			[TKMD_ID] = @TKMD_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HuyToKhai]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[NamTiepNhan],
			[TrangThai],
			[Guid],
			[LyDoHuy],
			[TKMD_ID]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@NamTiepNhan,
			@TrangThai,
			@Guid,
			@LyDoHuy,
			@TKMD_ID
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuanPhongKhai_CauHinh]'
GO
CREATE TABLE [dbo].[t_HaiQuanPhongKhai_CauHinh]
(
[MaDoanhNghiep] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PassWord] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenDoanhNghiep] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Key_Config] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value_Config] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Role] [smallint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuanPhongKhai_CauHinh] on [dbo].[t_HaiQuanPhongKhai_CauHinh]'
GO
ALTER TABLE [dbo].[t_HaiQuanPhongKhai_CauHinh] ADD CONSTRAINT [PK_t_HaiQuanPhongKhai_CauHinh] PRIMARY KEY CLUSTERED  ([MaDoanhNghiep], [Key_Config])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuanPhongKhai_CauHinh_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_Update]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuanPhongKhai_CauHinh_Update]
	@MaDoanhNghiep char(15),
	@PassWord varchar(50),
	@TenDoanhNghiep nvarchar(50),
	@Key_Config varchar(30),
	@Value_Config nvarchar(100),
	@Role smallint
AS
UPDATE
	[dbo].[t_HaiQuanPhongKhai_CauHinh]
SET
	[PassWord] = @PassWord,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[Value_Config] = @Value_Config,
	[Role] = @Role
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Key_Config] = @Key_Config

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_SXXK_HangMauDich]'
GO
CREATE TABLE [dbo].[t_SXXK_HangMauDich]
(
[SoToKhai] [int] NOT NULL,
[MaLoaiHinh] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NamDangKy] [smallint] NOT NULL,
[SoThuTuHang] [smallint] NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaPhu] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenHang] [nvarchar] (252) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuong] [numeric] (18, 3) NOT NULL,
[DonGiaKB] [float] NULL,
[DonGiaTT] [float] NULL,
[TriGiaKB] [float] NOT NULL,
[TriGiaTT] [float] NOT NULL,
[TriGiaKB_VND] [float] NOT NULL,
[ThueSuatXNK] [float] NULL,
[ThueSuatTTDB] [float] NULL,
[ThueSuatGTGT] [float] NULL,
[ThueXNK] [float] NULL,
[ThueTTDB] [float] NULL,
[ThueGTGT] [float] NULL,
[PhuThu] [float] NULL,
[TyLeThuKhac] [float] NULL,
[TriGiaThuKhac] [float] NULL,
[MienThue] [float] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_SXXK_HangMauDich_1] on [dbo].[t_SXXK_HangMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_HangMauDich] ADD CONSTRAINT [PK_t_SXXK_HangMauDich_1] PRIMARY KEY CLUSTERED  ([SoToKhai], [MaLoaiHinh], [MaHaiQuan], [NamDangKy], [SoThuTuHang])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_SelectAll]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoToKhai],
	[MaLoaiHinh],
	[MaHaiQuan],
	[NamDangKy],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue]
FROM
	[dbo].[t_SXXK_HangMauDich]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_SXXK_HangTKTG]'
GO
CREATE TABLE [dbo].[t_KDT_SXXK_HangTKTG]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TenHang] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[STTHang] [int] NOT NULL,
[GiaTrenHoaDon] [float] NULL,
[KhoanThanhToanGianTiep] [float] NULL,
[TraTruoc] [float] NULL,
[TongCong1] [float] NULL,
[HoaHong] [float] NULL,
[ChiPhiBaoBi] [float] NULL,
[ChiPhiDongGoi] [float] NULL,
[TroGiup] [float] NULL,
[VatLieu] [float] NULL,
[CongCu] [float] NULL,
[NguyenLieu] [float] NULL,
[ThietKe] [float] NULL,
[BanQuyen] [float] NULL,
[TienTraSuDung] [float] NULL,
[ChiPhiVanChuyen] [float] NULL,
[CuocPhiXepHang] [float] NULL,
[CuocPhiBaoHiem] [float] NULL,
[TongCong2] [float] NULL,
[PhiBaoHiem] [float] NULL,
[ChiPhiPhatSinh] [float] NULL,
[TienLai] [float] NULL,
[TienThue] [float] NULL,
[GiamGia] [float] NULL,
[ChiPhiKhongTang] [float] NULL,
[TriGiaNguyenTe] [float] NULL,
[TriGiaVND] [float] NULL,
[NgayTruyen] [float] NOT NULL,
[TKTG_ID] [bigint] NOT NULL,
[ChietKhau] [float] NULL,
[TongCong3] [float] NULL,
[ChiPhiNoiDia] [float] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_SXXK_Hang] on [dbo].[t_KDT_SXXK_HangTKTG]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_HangTKTG] ADD CONSTRAINT [PK_t_KDT_SXXK_Hang] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_Insert]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_Insert]
	@TenHang nvarchar(80),
	@STTHang int,
	@GiaTrenHoaDon float,
	@KhoanThanhToanGianTiep float,
	@TraTruoc float,
	@TongCong1 float,
	@HoaHong float,
	@ChiPhiBaoBi float,
	@ChiPhiDongGoi float,
	@TroGiup float,
	@VatLieu float,
	@CongCu float,
	@NguyenLieu float,
	@ThietKe float,
	@BanQuyen float,
	@TienTraSuDung float,
	@ChiPhiVanChuyen float,
	@CuocPhiXepHang float,
	@CuocPhiBaoHiem float,
	@TongCong2 float,
	@PhiBaoHiem float,
	@ChiPhiPhatSinh float,
	@TienLai float,
	@TienThue float,
	@GiamGia float,
	@ChiPhiKhongTang float,
	@TriGiaNguyenTe float,
	@TriGiaVND float,
	@NgayTruyen float,
	@TKTG_ID bigint,
	@ChietKhau float,
	@TongCong3 float,
	@ChiPhiNoiDia float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_SXXK_HangTKTG]
(
	[TenHang],
	[STTHang],
	[GiaTrenHoaDon],
	[KhoanThanhToanGianTiep],
	[TraTruoc],
	[TongCong1],
	[HoaHong],
	[ChiPhiBaoBi],
	[ChiPhiDongGoi],
	[TroGiup],
	[VatLieu],
	[CongCu],
	[NguyenLieu],
	[ThietKe],
	[BanQuyen],
	[TienTraSuDung],
	[ChiPhiVanChuyen],
	[CuocPhiXepHang],
	[CuocPhiBaoHiem],
	[TongCong2],
	[PhiBaoHiem],
	[ChiPhiPhatSinh],
	[TienLai],
	[TienThue],
	[GiamGia],
	[ChiPhiKhongTang],
	[TriGiaNguyenTe],
	[TriGiaVND],
	[NgayTruyen],
	[TKTG_ID],
	[ChietKhau],
	[TongCong3],
	[ChiPhiNoiDia]
)
VALUES 
(
	@TenHang,
	@STTHang,
	@GiaTrenHoaDon,
	@KhoanThanhToanGianTiep,
	@TraTruoc,
	@TongCong1,
	@HoaHong,
	@ChiPhiBaoBi,
	@ChiPhiDongGoi,
	@TroGiup,
	@VatLieu,
	@CongCu,
	@NguyenLieu,
	@ThietKe,
	@BanQuyen,
	@TienTraSuDung,
	@ChiPhiVanChuyen,
	@CuocPhiXepHang,
	@CuocPhiBaoHiem,
	@TongCong2,
	@PhiBaoHiem,
	@ChiPhiPhatSinh,
	@TienLai,
	@TienThue,
	@GiamGia,
	@ChiPhiKhongTang,
	@TriGiaNguyenTe,
	@TriGiaVND,
	@NgayTruyen,
	@TKTG_ID,
	@ChietKhau,
	@TongCong3,
	@ChiPhiNoiDia
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Message_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_InsertUpdate]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Message_InsertUpdate]
	@ID int,
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@MessageFrom varchar(50),
	@MessageTo varchar(50),
	@MessageType int,
	@MessageFunction int,
	@MessageContent xml,
	@CreatedTime datetime,
	@TieuDeThongBao nvarchar(100),
	@NoiDungThongBao nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_Messages] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_Messages] 
		SET
			[ReferenceID] = @ReferenceID,
			[ItemID] = @ItemID,
			[MessageFrom] = @MessageFrom,
			[MessageTo] = @MessageTo,
			[MessageType] = @MessageType,
			[MessageFunction] = @MessageFunction,
			[MessageContent] = @MessageContent,
			[CreatedTime] = @CreatedTime,
			[TieuDeThongBao] = @TieuDeThongBao,
			[NoiDungThongBao] = @NoiDungThongBao
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_Messages]
		(
			[ReferenceID],
			[ItemID],
			[MessageFrom],
			[MessageTo],
			[MessageType],
			[MessageFunction],
			[MessageContent],
			[CreatedTime],
			[TieuDeThongBao],
			[NoiDungThongBao]
		)
		VALUES 
		(
			@ReferenceID,
			@ItemID,
			@MessageFrom,
			@MessageTo,
			@MessageType,
			@MessageFunction,
			@MessageContent,
			@CreatedTime,
			@TieuDeThongBao,
			@NoiDungThongBao
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_Delete]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HuyToKhai]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_Load]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_Load]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[HopDongTM_ID],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HopDongThuongMaiDetail]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_DeleteBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HuyToKhai]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DoiTac_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DoiTac_Load]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 06, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DoiTac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep]
FROM
	[dbo].[t_HaiQuan_DoiTac]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_SXXK_ChungTuKemTheo]'
GO
CREATE TABLE [dbo].[t_KDT_SXXK_ChungTuKemTheo]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TenChungTu] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoBanChinh] [smallint] NULL,
[SoBanSao] [smallint] NULL,
[FileUpLoad] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Master_ID] [bigint] NOT NULL,
[STTHang] [int] NOT NULL,
[LoaiCT] [int] NOT NULL,
[NoiDung] [image] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ChungTuKemTheo] on [dbo].[t_KDT_SXXK_ChungTuKemTheo]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_ChungTuKemTheo] ADD CONSTRAINT [PK_ChungTuKemTheo] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_Insert]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_Insert]
	@TenChungTu nvarchar(50),
	@SoBanChinh smallint,
	@SoBanSao smallint,
	@FileUpLoad varchar(500),
	@Master_ID bigint,
	@STTHang int,
	@LoaiCT int,
	@NoiDung image,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_SXXK_ChungTuKemTheo]
(
	[TenChungTu],
	[SoBanChinh],
	[SoBanSao],
	[FileUpLoad],
	[Master_ID],
	[STTHang],
	[LoaiCT],
	[NoiDung]
)
VALUES 
(
	@TenChungTu,
	@SoBanChinh,
	@SoBanSao,
	@FileUpLoad,
	@Master_ID,
	@STTHang,
	@LoaiCT,
	@NoiDung
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_SelectAll]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_SelectAll]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[HopDongTM_ID],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HopDongThuongMaiDetail]	



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_SelectAll]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai]
FROM
	[dbo].[t_KDT_Container]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_SelectBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_SelectBy_TKMD_ID]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[ToSo],
	[LoaiToKhaiTriGia],
	[NgayXuatKhau],
	[CapDoThuongMai],
	[QuyenSuDung],
	[KhongXacDinh],
	[TraThem],
	[TienTra],
	[CoQuanHeDacBiet],
	[AnhHuongQuanHe],
	[GhiChep],
	[NgayKhaiBao],
	[NguoiKhaiBao],
	[ChucDanhNguoiKhaiBao],
	[NgayTruyen],
	[KieuQuanHe]
FROM
	[dbo].[t_KDT_SXXK_ToKhaiTriGia]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DoiTac_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DoiTac_SelectAll]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 06, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DoiTac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep]
FROM
	[dbo].[t_HaiQuan_DoiTac]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_Nuoc_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Insert]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Nuoc_Insert]
	@ID char(3),
	@Ten nvarchar(50)
AS
INSERT INTO [dbo].[t_HaiQuan_Nuoc]
(
	[ID],
	[Ten]
)
VALUES
(
	@ID,
	@Ten
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_Update]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_Update]
	@ID bigint,
	@TenHang nvarchar(80),
	@STTHang int,
	@GiaTrenHoaDon float,
	@KhoanThanhToanGianTiep float,
	@TraTruoc float,
	@TongCong1 float,
	@HoaHong float,
	@ChiPhiBaoBi float,
	@ChiPhiDongGoi float,
	@TroGiup float,
	@VatLieu float,
	@CongCu float,
	@NguyenLieu float,
	@ThietKe float,
	@BanQuyen float,
	@TienTraSuDung float,
	@ChiPhiVanChuyen float,
	@CuocPhiXepHang float,
	@CuocPhiBaoHiem float,
	@TongCong2 float,
	@PhiBaoHiem float,
	@ChiPhiPhatSinh float,
	@TienLai float,
	@TienThue float,
	@GiamGia float,
	@ChiPhiKhongTang float,
	@TriGiaNguyenTe float,
	@TriGiaVND float,
	@NgayTruyen float,
	@TKTG_ID bigint,
	@ChietKhau float,
	@TongCong3 float,
	@ChiPhiNoiDia float
AS

UPDATE
	[dbo].[t_KDT_SXXK_HangTKTG]
SET
	[TenHang] = @TenHang,
	[STTHang] = @STTHang,
	[GiaTrenHoaDon] = @GiaTrenHoaDon,
	[KhoanThanhToanGianTiep] = @KhoanThanhToanGianTiep,
	[TraTruoc] = @TraTruoc,
	[TongCong1] = @TongCong1,
	[HoaHong] = @HoaHong,
	[ChiPhiBaoBi] = @ChiPhiBaoBi,
	[ChiPhiDongGoi] = @ChiPhiDongGoi,
	[TroGiup] = @TroGiup,
	[VatLieu] = @VatLieu,
	[CongCu] = @CongCu,
	[NguyenLieu] = @NguyenLieu,
	[ThietKe] = @ThietKe,
	[BanQuyen] = @BanQuyen,
	[TienTraSuDung] = @TienTraSuDung,
	[ChiPhiVanChuyen] = @ChiPhiVanChuyen,
	[CuocPhiXepHang] = @CuocPhiXepHang,
	[CuocPhiBaoHiem] = @CuocPhiBaoHiem,
	[TongCong2] = @TongCong2,
	[PhiBaoHiem] = @PhiBaoHiem,
	[ChiPhiPhatSinh] = @ChiPhiPhatSinh,
	[TienLai] = @TienLai,
	[TienThue] = @TienThue,
	[GiamGia] = @GiamGia,
	[ChiPhiKhongTang] = @ChiPhiKhongTang,
	[TriGiaNguyenTe] = @TriGiaNguyenTe,
	[TriGiaVND] = @TriGiaVND,
	[NgayTruyen] = @NgayTruyen,
	[TKTG_ID] = @TKTG_ID,
	[ChietKhau] = @ChietKhau,
	[TongCong3] = @TongCong3,
	[ChiPhiNoiDia] = @ChiPhiNoiDia
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_HoaDonThuongMaiDetail]'
GO
CREATE TABLE [dbo].[t_KDT_HoaDonThuongMaiDetail]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[HMD_ID] [bigint] NOT NULL,
[HoaDonTM_ID] [bigint] NOT NULL,
[GiaTriDieuChinhTang] [float] NOT NULL,
[GiaiTriDieuChinhGiam] [float] NULL,
[GhiChu] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoThuTuHang] [int] NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhu] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (252) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (18, 3) NULL,
[DonGiaKB] [float] NULL,
[TriGiaKB] [float] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_GiayPhep_Detail] on [dbo].[t_KDT_HoaDonThuongMaiDetail]'
GO
ALTER TABLE [dbo].[t_KDT_HoaDonThuongMaiDetail] ADD CONSTRAINT [PK_t_KDT_GiayPhep_Detail] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_Delete]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HoaDonThuongMaiDetail]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_SelectBy_HMD_ID]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_SelectBy_HMD_ID]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_SelectBy_HMD_ID]
	@HMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[HopDongTM_ID],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HopDongThuongMaiDetail]
WHERE
	[HMD_ID] = @HMD_ID



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_LoaiCO]'
GO
CREATE TABLE [dbo].[t_HaiQuan_LoaiCO]
(
[Ma] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SLOAI_CO] on [dbo].[t_HaiQuan_LoaiCO]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiCO] ADD CONSTRAINT [PK_SLOAI_CO] PRIMARY KEY CLUSTERED  ([Ma])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiCO_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiCO_Load]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiCO_Load]
	@Ma varchar(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Ma],
	[Ten]
FROM
	[dbo].[t_HaiQuan_LoaiCO]
WHERE
	[Ma] = @Ma
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SelectToKhai_HangMauDich_By]'
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_SXXK_SelectToKhai_HangMauDich_By]
	-- Add the parameters for the stored procedure here
	@FromDate DateTime,
	@ToDate DateTime,
	@NuocNK_ID nvarchar(500)=NULL
AS
IF @NuocNK_ID IS NOT NULL AND LEN(@NuocNK_ID) > 0
BEGIN
	SELECT Ten,TenHang, Sum(SoLuong)as TongSoLuong,DVT, Sum(TriGiaKB)as TongTriGia,NguyenTe_ID,NgayDangKy,DonGiaKB 
	FROM(
		SELECT a.SoToKhai, a.MaLoaiHinh, a.DVT_ID,b.NgayDangKy , a.MaHaiQuan,a.TenHang ,a.SoLuong, a.TriGiaKB,a.DonGiaKB
		,b.NguyenTe_ID,c.Ten,d.Ten AS DVT 
		FROM dbo.t_SXXK_HangMauDich a 
		INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
		ON a.SoToKhai = b.SoToKhai
		AND a.MaLoaiHinh = b.MaLoaiHinh
		AND a.NamDangKy = b.NamDangKy 
		AND a.MaHaiQuan = b.MaHaiQuan
		INNER JOIN dbo.t_HaiQuan_Nuoc c
		ON b.NuocNK_ID = c.ID
		INNER JOIN dbo.t_HaiQuan_DonViTinh d
		ON a.DVT_ID = d.ID
		WHERE a.MaLoaiHinh LIKE 'X%' AND NuocNK_ID = @NuocNK_ID)c
	WHERE (NgayDangKy BETWEEN @FromDate AND @ToDate) 
	GROUP BY Ten,TenHang,NguyenTe_ID,DVT,NgayDangKy,DonGiaKB
	ORDER BY Ten,TongTriGia DESC
END
ELSE
BEGIN
	SELECT Ten,TenHang, Sum(SoLuong)as TongSoLuong,DVT, Sum(TriGiaKB)as TongTriGia,NguyenTe_ID,NgayDangKy,DonGiaKB 
	FROM(
		SELECT a.SoToKhai, a.MaLoaiHinh, a.DVT_ID, a.MaHaiQuan,a.TenHang ,a.SoLuong,a.TriGiaKB,a.DonGiaKB,
		b.NguyenTe_ID,b.NgayDangKy,c.Ten,d.Ten AS DVT 
		FROM dbo.t_SXXK_HangMauDich a 
		INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
		ON a.SoToKhai = b.SoToKhai
		AND a.MaLoaiHinh = b.MaLoaiHinh
		AND a.NamDangKy = b.NamDangKy 
		AND a.MaHaiQuan = b.MaHaiQuan
		INNER JOIN dbo.t_HaiQuan_Nuoc c
		ON b.NuocNK_ID = c.ID
		INNER JOIN dbo.t_HaiQuan_DonViTinh d
		ON a.DVT_ID = d.ID
		WHERE a.MaLoaiHinh LIKE 'X%' )c
	WHERE NgayDangKy BETWEEN @FromDate AND @ToDate
	GROUP BY Ten,TenHang,NguyenTe_ID,DVT,NgayDangKy,DonGiaKB
	ORDER BY Ten,TongTriGia DESC
END
/* order BY Ten,TenSP*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_SelectBy_HopDongTM_ID]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_SelectBy_HopDongTM_ID]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_SelectBy_HopDongTM_ID]
	@HopDongTM_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[HopDongTM_ID],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HopDongThuongMaiDetail]
WHERE
	[HopDongTM_ID] = @HopDongTM_ID



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Message_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_Delete]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Message_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_Messages]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_Load]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[NamTiepNhan],
	[TrangThai],
	[Guid],
	[LyDoHuy],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_HuyToKhai]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_ChungTuKemChiTiet]'
GO
CREATE TABLE [dbo].[t_KDT_ChungTuKemChiTiet]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[ChungTuKemID] [bigint] NULL,
[FileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileSize] [numeric] (18, 0) NULL,
[NoiDung] [image] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DChungTuKem_CHI_TIET] on [dbo].[t_KDT_ChungTuKemChiTiet]'
GO
ALTER TABLE [dbo].[t_KDT_ChungTuKemChiTiet] ADD CONSTRAINT [PK_DChungTuKem_CHI_TIET] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_Insert]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_Insert]
	@ChungTuKemID bigint,
	@FileName varchar(255),
	@FileSize numeric(18, 0),
	@NoiDung image,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_ChungTuKemChiTiet]
(
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
)
VALUES 
(
	@ChungTuKemID,
	@FileName,
	@FileSize,
	@NoiDung
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_Nuoc_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Update]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Nuoc_Update]
	@ID char(3),
	@Ten nvarchar(50)
AS

UPDATE
	[dbo].[t_HaiQuan_Nuoc]
SET
	[Ten] = @Ten
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_Update]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_Update]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_Update]
	@ID bigint,
	@HMD_ID bigint,
	@HopDongTM_ID bigint,
	@GhiChu nvarchar(255),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@TriGiaKB float
AS

UPDATE
	[dbo].[t_KDT_HopDongThuongMaiDetail]
SET
	[HMD_ID] = @HMD_ID,
	[HopDongTM_ID] = @HopDongTM_ID,
	[GhiChu] = @GhiChu,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB
WHERE
	[ID] = @ID



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HoaDonTM_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HoaDonTM_ID]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HoaDonTM_ID]
	@HoaDonTM_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HoaDonThuongMaiDetail]
WHERE
	[HoaDonTM_ID] = @HoaDonTM_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_Update]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_Update]
	@ID bigint,
	@ChungTuKemID bigint,
	@FileName varchar(255),
	@FileSize numeric(18, 0),
	@NoiDung image
AS

UPDATE
	[dbo].[t_KDT_ChungTuKemChiTiet]
SET
	[ChungTuKemID] = @ChungTuKemID,
	[FileName] = @FileName,
	[FileSize] = @FileSize,
	[NoiDung] = @NoiDung
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_ToKhaiMauDich_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_InsertUpdate]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_InsertUpdate]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint,
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(100),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@Xuat_NPL_SP char(1),
	@ThanhLy char(1),
	@NGAY_THN_THX datetime,
	@MaDonViUT varchar(14),
	@TrangThaiThanhKhoan char(1),
	@ChungTu nvarchar(40),
	@PhanLuong varchar(2),
	@NgayHoanThanh datetime,
	@TrangThai smallint,
	@TrongLuongNet float,
	@SoTienKhoan money
AS
IF EXISTS(SELECT [MaHaiQuan], [SoToKhai], [MaLoaiHinh], [NamDangKy] FROM [dbo].[t_SXXK_ToKhaiMauDich] WHERE [MaHaiQuan] = @MaHaiQuan AND [SoToKhai] = @SoToKhai AND [MaLoaiHinh] = @MaLoaiHinh AND [NamDangKy] = @NamDangKy)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_ToKhaiMauDich] 
		SET
			[NgayDangKy] = @NgayDangKy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
			[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
			[TenDonViDoiTac] = @TenDonViDoiTac,
			[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHanHopDong] = @NgayHetHanHopDong,
			[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
			[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
			[PTVT_ID] = @PTVT_ID,
			[SoHieuPTVT] = @SoHieuPTVT,
			[NgayDenPTVT] = @NgayDenPTVT,
			[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
			[LoaiVanDon] = @LoaiVanDon,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[NuocXK_ID] = @NuocXK_ID,
			[NuocNK_ID] = @NuocNK_ID,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[CuaKhau_ID] = @CuaKhau_ID,
			[DKGH_ID] = @DKGH_ID,
			[NguyenTe_ID] = @NguyenTe_ID,
			[TyGiaTinhThue] = @TyGiaTinhThue,
			[TyGiaUSD] = @TyGiaUSD,
			[PTTT_ID] = @PTTT_ID,
			[SoHang] = @SoHang,
			[SoLuongPLTK] = @SoLuongPLTK,
			[TenChuHang] = @TenChuHang,
			[SoContainer20] = @SoContainer20,
			[SoContainer40] = @SoContainer40,
			[SoKien] = @SoKien,
			[TrongLuong] = @TrongLuong,
			[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
			[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
			[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
			[LePhiHaiQuan] = @LePhiHaiQuan,
			[PhiBaoHiem] = @PhiBaoHiem,
			[PhiVanChuyen] = @PhiVanChuyen,
			[PhiXepDoHang] = @PhiXepDoHang,
			[PhiKhac] = @PhiKhac,
			[Xuat_NPL_SP] = @Xuat_NPL_SP,
			[ThanhLy] = @ThanhLy,
			[NGAY_THN_THX] = @NGAY_THN_THX,
			[MaDonViUT] = @MaDonViUT,
			[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
			[ChungTu] = @ChungTu,
			[PhanLuong] = @PhanLuong,
			[NgayHoanThanh] = @NgayHoanThanh,
			[TrangThai] = @TrangThai,
			[TrongLuongNet] = @TrongLuongNet,
			[SoTienKhoan] = @SoTienKhoan
		WHERE
			[MaHaiQuan] = @MaHaiQuan
			AND [SoToKhai] = @SoToKhai
			AND [MaLoaiHinh] = @MaLoaiHinh
			AND [NamDangKy] = @NamDangKy
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_ToKhaiMauDich]
	(
			[MaHaiQuan],
			[SoToKhai],
			[MaLoaiHinh],
			[NamDangKy],
			[NgayDangKy],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaDaiLyTTHQ],
			[TenDaiLyTTHQ],
			[TenDonViDoiTac],
			[ChiTietDonViDoiTac],
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHanGiayPhep],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHanHopDong],
			[SoHoaDonThuongMai],
			[NgayHoaDonThuongMai],
			[PTVT_ID],
			[SoHieuPTVT],
			[NgayDenPTVT],
			[QuocTichPTVT_ID],
			[LoaiVanDon],
			[SoVanDon],
			[NgayVanDon],
			[NuocXK_ID],
			[NuocNK_ID],
			[DiaDiemXepHang],
			[CuaKhau_ID],
			[DKGH_ID],
			[NguyenTe_ID],
			[TyGiaTinhThue],
			[TyGiaUSD],
			[PTTT_ID],
			[SoHang],
			[SoLuongPLTK],
			[TenChuHang],
			[SoContainer20],
			[SoContainer40],
			[SoKien],
			[TrongLuong],
			[TongTriGiaKhaiBao],
			[TongTriGiaTinhThue],
			[LoaiToKhaiGiaCong],
			[LePhiHaiQuan],
			[PhiBaoHiem],
			[PhiVanChuyen],
			[PhiXepDoHang],
			[PhiKhac],
			[Xuat_NPL_SP],
			[ThanhLy],
			[NGAY_THN_THX],
			[MaDonViUT],
			[TrangThaiThanhKhoan],
			[ChungTu],
			[PhanLuong],
			[NgayHoanThanh],
			[TrangThai],
			[TrongLuongNet],
			[SoTienKhoan]
	)
	VALUES
	(
			@MaHaiQuan,
			@SoToKhai,
			@MaLoaiHinh,
			@NamDangKy,
			@NgayDangKy,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaDaiLyTTHQ,
			@TenDaiLyTTHQ,
			@TenDonViDoiTac,
			@ChiTietDonViDoiTac,
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHanGiayPhep,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHanHopDong,
			@SoHoaDonThuongMai,
			@NgayHoaDonThuongMai,
			@PTVT_ID,
			@SoHieuPTVT,
			@NgayDenPTVT,
			@QuocTichPTVT_ID,
			@LoaiVanDon,
			@SoVanDon,
			@NgayVanDon,
			@NuocXK_ID,
			@NuocNK_ID,
			@DiaDiemXepHang,
			@CuaKhau_ID,
			@DKGH_ID,
			@NguyenTe_ID,
			@TyGiaTinhThue,
			@TyGiaUSD,
			@PTTT_ID,
			@SoHang,
			@SoLuongPLTK,
			@TenChuHang,
			@SoContainer20,
			@SoContainer40,
			@SoKien,
			@TrongLuong,
			@TongTriGiaKhaiBao,
			@TongTriGiaTinhThue,
			@LoaiToKhaiGiaCong,
			@LePhiHaiQuan,
			@PhiBaoHiem,
			@PhiVanChuyen,
			@PhiXepDoHang,
			@PhiKhac,
			@Xuat_NPL_SP,
			@ThanhLy,
			@NGAY_THN_THX,
			@MaDonViUT,
			@TrangThaiThanhKhoan,
			@ChungTu,
			@PhanLuong,
			@NgayHoanThanh,
			@TrangThai,
			@TrongLuongNet,
			@SoTienKhoan
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_SelectBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[NamTiepNhan],
	[TrangThai],
	[Guid],
	[LyDoHuy],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_HuyToKhai]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_SelectAll]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
FROM
	[dbo].[t_KDT_CO]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_Nuoc_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_InsertUpdate]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Nuoc_InsertUpdate]
	@ID char(3),
	@Ten nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_Nuoc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_Nuoc] 
		SET
			[Ten] = @Ten
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_Nuoc]
	(
			[ID],
			[Ten]
	)
	VALUES
	(
			@ID,
			@Ten
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HMD_ID]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HMD_ID]
	@HMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HoaDonThuongMaiDetail]
WHERE
	[HMD_ID] = @HMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_Insert]'
GO





------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_Insert]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_Insert]
	@HMD_ID bigint,
	@HoaDonTM_ID bigint,
	@GiaTriDieuChinhTang float,
	@GiaiTriDieuChinhGiam float,
	@GhiChu nvarchar(100),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@TriGiaKB float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HoaDonThuongMaiDetail]
(
	[HMD_ID],
	[HoaDonTM_ID],
	[GiaTriDieuChinhTang],
	[GiaiTriDieuChinhGiam],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
)
VALUES 
(
	@HMD_ID,
	@HoaDonTM_ID,
	@GiaTriDieuChinhTang,
	@GiaiTriDieuChinhGiam,
	@GhiChu,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB
)

SET @ID = SCOPE_IDENTITY()



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SelectToKhai_HangMauDich_GroupByDoiTac]'
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_SXXK_SelectToKhai_HangMauDich_GroupByDoiTac]
	-- Add the parameters for the stored procedure here
	@FromDate DateTime,
	@ToDate DateTime,
	@NuocNK_ID nvarchar(500)=NULL
AS
IF @NuocNK_ID IS NOT NULL AND LEN(@NuocNK_ID) > 0
BEGIN
	SELECT Ten,TenDonViDoiTac, Sum(SoLuong)as TongSoLuong,DVT, Sum(TriGiaKB)as TongTriGia,NguyenTe_ID,NgayDangKy
	FROM(
		SELECT a.SoToKhai, a.MaLoaiHinh, a.DVT_ID,b.NgayDangKy , a.MaHaiQuan,a.TenHang ,a.SoLuong, a.TriGiaKB,a.DonGiaKB
		,b.NguyenTe_ID,b.TenDonViDoiTac,c.Ten,d.Ten AS DVT 
		FROM dbo.t_SXXK_HangMauDich a 
		INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
		ON a.SoToKhai = b.SoToKhai
		AND a.MaLoaiHinh = b.MaLoaiHinh
		AND a.NamDangKy = b.NamDangKy 
		AND a.MaHaiQuan = b.MaHaiQuan
		INNER JOIN dbo.t_HaiQuan_Nuoc c
		ON b.NuocNK_ID = c.ID
		INNER JOIN dbo.t_HaiQuan_DonViTinh d
		ON a.DVT_ID = d.ID
		WHERE a.MaLoaiHinh LIKE 'X%' AND NuocNK_ID = @NuocNK_ID)c
	WHERE (NgayDangKy BETWEEN @FromDate AND @ToDate) 
	GROUP BY Ten,TenDonViDoiTac,NguyenTe_ID,DVT,NgayDangKy
	ORDER BY Ten,TongTriGia DESC
END
ELSE
BEGIN
	SELECT Ten,TenDonViDoiTac, Sum(SoLuong)as TongSoLuong,DVT, Sum(TriGiaKB)as TongTriGia,NguyenTe_ID,NgayDangKy 
	FROM(
		SELECT a.SoToKhai, a.MaLoaiHinh, a.DVT_ID, a.MaHaiQuan,a.TenHang ,a.SoLuong,a.TriGiaKB,a.DonGiaKB,
		b.NguyenTe_ID,b.NgayDangKy,b.TenDonViDoiTac,c.Ten,d.Ten AS DVT 
		FROM dbo.t_SXXK_HangMauDich a 
		INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
		ON a.SoToKhai = b.SoToKhai
		AND a.MaLoaiHinh = b.MaLoaiHinh
		AND a.NamDangKy = b.NamDangKy 
		AND a.MaHaiQuan = b.MaHaiQuan
		INNER JOIN dbo.t_HaiQuan_Nuoc c
		ON b.NuocNK_ID = c.ID
		INNER JOIN dbo.t_HaiQuan_DonViTinh d
		ON a.DVT_ID = d.ID
		WHERE a.MaLoaiHinh LIKE 'X%' )c
	WHERE NgayDangKy BETWEEN @FromDate AND @ToDate
	GROUP BY Ten,TenDonViDoiTac,NguyenTe_ID,DVT,NgayDangKy
	ORDER BY Ten,TongTriGia DESC
END
/* order BY Ten,TenSP*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_InsertUpdate]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_InsertUpdate]
	@ID bigint,
	@ChungTuKemID bigint,
	@FileName varchar(255),
	@FileSize numeric(18, 0),
	@NoiDung image
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ChungTuKemChiTiet] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ChungTuKemChiTiet] 
		SET
			[ChungTuKemID] = @ChungTuKemID,
			[FileName] = @FileName,
			[FileSize] = @FileSize,
			[NoiDung] = @NoiDung
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ChungTuKemChiTiet]
		(
			[ChungTuKemID],
			[FileName],
			[FileSize],
			[NoiDung]
		)
		VALUES 
		(
			@ChungTuKemID,
			@FileName,
			@FileSize,
			@NoiDung
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GROUP_ROLE]'
GO
CREATE TABLE [dbo].[GROUP_ROLE]
(
[GROUP_ID] [bigint] NOT NULL,
[ID_ROLE] [bigint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_GROUP_FUNC] on [dbo].[GROUP_ROLE]'
GO
ALTER TABLE [dbo].[GROUP_ROLE] ADD CONSTRAINT [PK_GROUP_FUNC] PRIMARY KEY CLUSTERED  ([GROUP_ID], [ID_ROLE])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_ROLE_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_ROLE_SelectAll]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_ROLE_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[GROUP_ID],
	[ID_ROLE]
FROM
	[dbo].[GROUP_ROLE]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_Nuoc_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Delete]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Nuoc_Delete]
	@ID char(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_Nuoc]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_ROLE_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_ROLE_Load]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_ROLE_Load]
	@GROUP_ID bigint,
	@ID_ROLE bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[GROUP_ID],
	[ID_ROLE]
FROM
	[dbo].[GROUP_ROLE]
WHERE
	[GROUP_ID] = @GROUP_ID
	AND [ID_ROLE] = @ID_ROLE

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Message_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_Load]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Message_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceID],
	[ItemID],
	[MessageFrom],
	[MessageTo],
	[MessageType],
	[MessageFunction],
	[MessageContent],
	[CreatedTime],
	[TieuDeThongBao],
	[NoiDungThongBao]
FROM
	[dbo].[t_KDT_Messages]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_SelectAll]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[NamTiepNhan],
	[TrangThai],
	[Guid],
	[LyDoHuy],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_HuyToKhai]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_Nuoc_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Load]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Nuoc_Load]
	@ID char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten]
FROM
	[dbo].[t_HaiQuan_Nuoc]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_Nuoc_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_SelectAll]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Nuoc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten]
FROM
	[dbo].[t_HaiQuan_Nuoc]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_HoaDonThuongMai]'
GO
CREATE TABLE [dbo].[t_KDT_HoaDonThuongMai]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoHoaDon] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayHoaDon] [datetime] NOT NULL,
[NguyenTe_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PTTT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DKGH_ID] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDonViMua] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonViMua] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDonViBan] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonViBan] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThongTinKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKMD_ID] [bigint] NOT NULL,
[GuidStr] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiKB] [int] NULL,
[SoTiepNhan] [bigint] NULL,
[NgayTiepNhan] [datetime] NULL,
[TrangThai] [int] NULL,
[NamTiepNhan] [int] NULL,
[MaDoanhNghiep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_HoaDonThuongMai] on [dbo].[t_KDT_HoaDonThuongMai]'
GO
ALTER TABLE [dbo].[t_KDT_HoaDonThuongMai] ADD CONSTRAINT [PK_t_KDT_HoaDonThuongMai] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_Insert]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_Insert]
	@SoHoaDon varchar(100),
	@NgayHoaDon datetime,
	@NguyenTe_ID char(3),
	@PTTT_ID char(3),
	@DKGH_ID varchar(7),
	@MaDonViMua varchar(50),
	@TenDonViMua nvarchar(100),
	@MaDonViBan varchar(50),
	@TenDonViBan nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@MaDoanhNghiep varchar(50),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HoaDonThuongMai]
(
	[SoHoaDon],
	[NgayHoaDon],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
)
VALUES 
(
	@SoHoaDon,
	@NgayHoaDon,
	@NguyenTe_ID,
	@PTTT_ID,
	@DKGH_ID,
	@MaDonViMua,
	@TenDonViMua,
	@MaDonViBan,
	@TenDonViBan,
	@ThongTinKhac,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan,
	@MaDoanhNghiep
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaH_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaH_InsertUpdate]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 07, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaH_InsertUpdate]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255)
AS
IF EXISTS(SELECT [HS10SO] FROM [dbo].[t_HaiQuan_MaHS] WHERE [HS10SO] = @HS10SO)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_MaHS] 
		SET
			[Nhom] = @Nhom,
			[PN1] = @PN1,
			[PN2] = @PN2,
			[Pn3] = @Pn3,
			[Mo_ta] = @Mo_ta
		WHERE
			[HS10SO] = @HS10SO
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_MaHS]
	(
			[Nhom],
			[PN1],
			[PN2],
			[Pn3],
			[Mo_ta],
			[HS10SO]
	)
	VALUES
	(
			@Nhom,
			@PN1,
			@PN2,
			@Pn3,
			@Mo_ta,
			@HS10SO
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_Delete]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_Delete]
	@MA_NHOM bigint
AS
DELETE FROM 
	[dbo].[GROUPS]
WHERE
	[MA_NHOM] = @MA_NHOM

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_HangGiayPhepDetail]'
GO
CREATE TABLE [dbo].[t_KDT_HangGiayPhepDetail]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[GiayPhep_ID] [bigint] NOT NULL,
[MaNguyenTe] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaChuyenNganh] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChu] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HMD_ID] [bigint] NULL,
[SoThuTuHang] [int] NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhu] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (252) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (18, 3) NULL,
[DonGiaKB] [float] NULL,
[TriGiaKB] [float] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_GiayPhep_Detail_1] on [dbo].[t_KDT_HangGiayPhepDetail]'
GO
ALTER TABLE [dbo].[t_KDT_HangGiayPhepDetail] ADD CONSTRAINT [PK_t_KDT_GiayPhep_Detail_1] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_DeleteBy_HMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_DeleteBy_HMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_DeleteBy_HMD_ID]
	@HMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HangGiayPhepDetail]
WHERE
	[HMD_ID] = @HMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_SelectAll]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[ToSo],
	[LoaiToKhaiTriGia],
	[NgayXuatKhau],
	[CapDoThuongMai],
	[QuyenSuDung],
	[KhongXacDinh],
	[TraThem],
	[TienTra],
	[CoQuanHeDacBiet],
	[AnhHuongQuanHe],
	[GhiChep],
	[NgayKhaiBao],
	[NguoiKhaiBao],
	[ChucDanhNguoiKhaiBao],
	[NgayTruyen],
	[KieuQuanHe]
FROM
	[dbo].[t_KDT_SXXK_ToKhaiTriGia]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_InsertUpdate]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_InsertUpdate]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_InsertUpdate]
	@ID bigint,
	@HMD_ID bigint,
	@HoaDonTM_ID bigint,
	@GiaTriDieuChinhTang float,
	@GiaiTriDieuChinhGiam float,
	@GhiChu nvarchar(100),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@TriGiaKB float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HoaDonThuongMaiDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HoaDonThuongMaiDetail] 
		SET
			[HMD_ID] = @HMD_ID,
			[HoaDonTM_ID] = @HoaDonTM_ID,
			[GiaTriDieuChinhTang] = @GiaTriDieuChinhTang,
			[GiaiTriDieuChinhGiam] = @GiaiTriDieuChinhGiam,
			[GhiChu] = @GhiChu,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HoaDonThuongMaiDetail]
		(
			[HMD_ID],
			[HoaDonTM_ID],
			[GiaTriDieuChinhTang],
			[GiaiTriDieuChinhGiam],
			[GhiChu],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB]
		)
		VALUES 
		(
			@HMD_ID,
			@HoaDonTM_ID,
			@GiaTriDieuChinhTang,
			@GiaiTriDieuChinhGiam,
			@GhiChu,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB
		)		
	END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_SXXK_MsgSend]'
GO
CREATE TABLE [dbo].[t_KDT_SXXK_MsgSend]
(
[master_id] [bigint] NOT NULL,
[func] [int] NOT NULL,
[LoaiHS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[msg] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_SXXK_MsgSend_1] on [dbo].[t_KDT_SXXK_MsgSend]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_MsgSend] ADD CONSTRAINT [PK_t_KDT_SXXK_MsgSend_1] PRIMARY KEY CLUSTERED  ([master_id], [LoaiHS])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_MsgSend_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_InsertUpdate]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 17, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_InsertUpdate]
	@master_id bigint,
	@func int,
	@LoaiHS varchar(50),
	@msg ntext
AS
IF EXISTS(SELECT [master_id], [LoaiHS] FROM [dbo].[t_KDT_SXXK_MsgSend] WHERE [master_id] = @master_id AND [LoaiHS] = @LoaiHS)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_MsgSend] 
		SET
			[func] = @func,
			[msg] = @msg
		WHERE
			[master_id] = @master_id
			AND [LoaiHS] = @LoaiHS
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_SXXK_MsgSend]
	(
			[master_id],
			[func],
			[LoaiHS],
			[msg]
	)
	VALUES
	(
			@master_id,
			@func,
			@LoaiHS,
			@msg
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_Delete]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ChungTuKemChiTiet]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_MsgSend_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_Insert]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 17, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Insert]
	@master_id bigint,
	@func int,
	@LoaiHS varchar(50),
	@msg ntext
AS
INSERT INTO [dbo].[t_KDT_SXXK_MsgSend]
(
	[master_id],
	[func],
	[LoaiHS],
	[msg]
)
VALUES
(
	@master_id,
	@func,
	@LoaiHS,
	@msg
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_InsertUpdate]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_InsertUpdate]
	@ID bigint,
	@TenHang nvarchar(80),
	@STTHang int,
	@GiaTrenHoaDon float,
	@KhoanThanhToanGianTiep float,
	@TraTruoc float,
	@TongCong1 float,
	@HoaHong float,
	@ChiPhiBaoBi float,
	@ChiPhiDongGoi float,
	@TroGiup float,
	@VatLieu float,
	@CongCu float,
	@NguyenLieu float,
	@ThietKe float,
	@BanQuyen float,
	@TienTraSuDung float,
	@ChiPhiVanChuyen float,
	@CuocPhiXepHang float,
	@CuocPhiBaoHiem float,
	@TongCong2 float,
	@PhiBaoHiem float,
	@ChiPhiPhatSinh float,
	@TienLai float,
	@TienThue float,
	@GiamGia float,
	@ChiPhiKhongTang float,
	@TriGiaNguyenTe float,
	@TriGiaVND float,
	@NgayTruyen float,
	@TKTG_ID bigint,
	@ChietKhau float,
	@TongCong3 float,
	@ChiPhiNoiDia float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_HangTKTG] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_HangTKTG] 
		SET
			[TenHang] = @TenHang,
			[STTHang] = @STTHang,
			[GiaTrenHoaDon] = @GiaTrenHoaDon,
			[KhoanThanhToanGianTiep] = @KhoanThanhToanGianTiep,
			[TraTruoc] = @TraTruoc,
			[TongCong1] = @TongCong1,
			[HoaHong] = @HoaHong,
			[ChiPhiBaoBi] = @ChiPhiBaoBi,
			[ChiPhiDongGoi] = @ChiPhiDongGoi,
			[TroGiup] = @TroGiup,
			[VatLieu] = @VatLieu,
			[CongCu] = @CongCu,
			[NguyenLieu] = @NguyenLieu,
			[ThietKe] = @ThietKe,
			[BanQuyen] = @BanQuyen,
			[TienTraSuDung] = @TienTraSuDung,
			[ChiPhiVanChuyen] = @ChiPhiVanChuyen,
			[CuocPhiXepHang] = @CuocPhiXepHang,
			[CuocPhiBaoHiem] = @CuocPhiBaoHiem,
			[TongCong2] = @TongCong2,
			[PhiBaoHiem] = @PhiBaoHiem,
			[ChiPhiPhatSinh] = @ChiPhiPhatSinh,
			[TienLai] = @TienLai,
			[TienThue] = @TienThue,
			[GiamGia] = @GiamGia,
			[ChiPhiKhongTang] = @ChiPhiKhongTang,
			[TriGiaNguyenTe] = @TriGiaNguyenTe,
			[TriGiaVND] = @TriGiaVND,
			[NgayTruyen] = @NgayTruyen,
			[TKTG_ID] = @TKTG_ID,
			[ChietKhau] = @ChietKhau,
			[TongCong3] = @TongCong3,
			[ChiPhiNoiDia] = @ChiPhiNoiDia
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_HangTKTG]
		(
			[TenHang],
			[STTHang],
			[GiaTrenHoaDon],
			[KhoanThanhToanGianTiep],
			[TraTruoc],
			[TongCong1],
			[HoaHong],
			[ChiPhiBaoBi],
			[ChiPhiDongGoi],
			[TroGiup],
			[VatLieu],
			[CongCu],
			[NguyenLieu],
			[ThietKe],
			[BanQuyen],
			[TienTraSuDung],
			[ChiPhiVanChuyen],
			[CuocPhiXepHang],
			[CuocPhiBaoHiem],
			[TongCong2],
			[PhiBaoHiem],
			[ChiPhiPhatSinh],
			[TienLai],
			[TienThue],
			[GiamGia],
			[ChiPhiKhongTang],
			[TriGiaNguyenTe],
			[TriGiaVND],
			[NgayTruyen],
			[TKTG_ID],
			[ChietKhau],
			[TongCong3],
			[ChiPhiNoiDia]
		)
		VALUES 
		(
			@TenHang,
			@STTHang,
			@GiaTrenHoaDon,
			@KhoanThanhToanGianTiep,
			@TraTruoc,
			@TongCong1,
			@HoaHong,
			@ChiPhiBaoBi,
			@ChiPhiDongGoi,
			@TroGiup,
			@VatLieu,
			@CongCu,
			@NguyenLieu,
			@ThietKe,
			@BanQuyen,
			@TienTraSuDung,
			@ChiPhiVanChuyen,
			@CuocPhiXepHang,
			@CuocPhiBaoHiem,
			@TongCong2,
			@PhiBaoHiem,
			@ChiPhiPhatSinh,
			@TienLai,
			@TienThue,
			@GiamGia,
			@ChiPhiKhongTang,
			@TriGiaNguyenTe,
			@TriGiaVND,
			@NgayTruyen,
			@TKTG_ID,
			@ChietKhau,
			@TongCong3,
			@ChiPhiNoiDia
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_InsertUpdate]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaToKhaiTriGia int,
	@LyDo nvarchar(50),
	@TenHang nvarchar(80),
	@STTHang int,
	@NgayXuat datetime,
	@STTHangTT int,
	@TenHangTT nvarchar(80),
	@NgayXuatTT datetime,
	@SoTKHangTT bigint,
	@NgayDangKyHangTT datetime,
	@MaLoaiHinhHangTT varchar(50),
	@MaHaiQuanHangTT varchar(50),
	@TriGiaNguyenTeHangTT float,
	@DieuChinhCongThuongMai float,
	@DieuChinhCongSoLuong float,
	@DieuChinhCongKhoanGiamGiaKhac float,
	@DieuChinhCongChiPhiVanTai float,
	@DieuChinhCongChiPhiBaoHiem float,
	@TriGiaNguyenTeHang float,
	@TriGiaTTHang float,
	@ToSo int,
	@DieuChinhTruCapDoThuongMai float,
	@DieuChinhTruSoLuong float,
	@DieuChinhTruKhoanGiamGiaKhac float,
	@DieuChinhTruChiPhiVanTai float,
	@DieuChinhTruChiPhiBaoHiem float,
	@GiaiTrinh nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_KD_ToKhaiTriGiaPP23] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_KD_ToKhaiTriGiaPP23] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaToKhaiTriGia] = @MaToKhaiTriGia,
			[LyDo] = @LyDo,
			[TenHang] = @TenHang,
			[STTHang] = @STTHang,
			[NgayXuat] = @NgayXuat,
			[STTHangTT] = @STTHangTT,
			[TenHangTT] = @TenHangTT,
			[NgayXuatTT] = @NgayXuatTT,
			[SoTKHangTT] = @SoTKHangTT,
			[NgayDangKyHangTT] = @NgayDangKyHangTT,
			[MaLoaiHinhHangTT] = @MaLoaiHinhHangTT,
			[MaHaiQuanHangTT] = @MaHaiQuanHangTT,
			[TriGiaNguyenTeHangTT] = @TriGiaNguyenTeHangTT,
			[DieuChinhCongThuongMai] = @DieuChinhCongThuongMai,
			[DieuChinhCongSoLuong] = @DieuChinhCongSoLuong,
			[DieuChinhCongKhoanGiamGiaKhac] = @DieuChinhCongKhoanGiamGiaKhac,
			[DieuChinhCongChiPhiVanTai] = @DieuChinhCongChiPhiVanTai,
			[DieuChinhCongChiPhiBaoHiem] = @DieuChinhCongChiPhiBaoHiem,
			[TriGiaNguyenTeHang] = @TriGiaNguyenTeHang,
			[TriGiaTTHang] = @TriGiaTTHang,
			[ToSo] = @ToSo,
			[DieuChinhTruCapDoThuongMai] = @DieuChinhTruCapDoThuongMai,
			[DieuChinhTruSoLuong] = @DieuChinhTruSoLuong,
			[DieuChinhTruKhoanGiamGiaKhac] = @DieuChinhTruKhoanGiamGiaKhac,
			[DieuChinhTruChiPhiVanTai] = @DieuChinhTruChiPhiVanTai,
			[DieuChinhTruChiPhiBaoHiem] = @DieuChinhTruChiPhiBaoHiem,
			[GiaiTrinh] = @GiaiTrinh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_KD_ToKhaiTriGiaPP23]
		(
			[TKMD_ID],
			[MaToKhaiTriGia],
			[LyDo],
			[TenHang],
			[STTHang],
			[NgayXuat],
			[STTHangTT],
			[TenHangTT],
			[NgayXuatTT],
			[SoTKHangTT],
			[NgayDangKyHangTT],
			[MaLoaiHinhHangTT],
			[MaHaiQuanHangTT],
			[TriGiaNguyenTeHangTT],
			[DieuChinhCongThuongMai],
			[DieuChinhCongSoLuong],
			[DieuChinhCongKhoanGiamGiaKhac],
			[DieuChinhCongChiPhiVanTai],
			[DieuChinhCongChiPhiBaoHiem],
			[TriGiaNguyenTeHang],
			[TriGiaTTHang],
			[ToSo],
			[DieuChinhTruCapDoThuongMai],
			[DieuChinhTruSoLuong],
			[DieuChinhTruKhoanGiamGiaKhac],
			[DieuChinhTruChiPhiVanTai],
			[DieuChinhTruChiPhiBaoHiem],
			[GiaiTrinh]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaToKhaiTriGia,
			@LyDo,
			@TenHang,
			@STTHang,
			@NgayXuat,
			@STTHangTT,
			@TenHangTT,
			@NgayXuatTT,
			@SoTKHangTT,
			@NgayDangKyHangTT,
			@MaLoaiHinhHangTT,
			@MaHaiQuanHangTT,
			@TriGiaNguyenTeHangTT,
			@DieuChinhCongThuongMai,
			@DieuChinhCongSoLuong,
			@DieuChinhCongKhoanGiamGiaKhac,
			@DieuChinhCongChiPhiVanTai,
			@DieuChinhCongChiPhiBaoHiem,
			@TriGiaNguyenTeHang,
			@TriGiaTTHang,
			@ToSo,
			@DieuChinhTruCapDoThuongMai,
			@DieuChinhTruSoLuong,
			@DieuChinhTruKhoanGiamGiaKhac,
			@DieuChinhTruChiPhiVanTai,
			@DieuChinhTruChiPhiBaoHiem,
			@GiaiTrinh
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_Update]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_Update]
	@ID bigint,
	@SoHoaDon varchar(100),
	@NgayHoaDon datetime,
	@NguyenTe_ID char(3),
	@PTTT_ID char(3),
	@DKGH_ID varchar(7),
	@MaDonViMua varchar(50),
	@TenDonViMua nvarchar(100),
	@MaDonViBan varchar(50),
	@TenDonViBan nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@MaDoanhNghiep varchar(50)
AS

UPDATE
	[dbo].[t_KDT_HoaDonThuongMai]
SET
	[SoHoaDon] = @SoHoaDon,
	[NgayHoaDon] = @NgayHoaDon,
	[NguyenTe_ID] = @NguyenTe_ID,
	[PTTT_ID] = @PTTT_ID,
	[DKGH_ID] = @DKGH_ID,
	[MaDonViMua] = @MaDonViMua,
	[TenDonViMua] = @TenDonViMua,
	[MaDonViBan] = @MaDonViBan,
	[TenDonViBan] = @TenDonViBan,
	[ThongTinKhac] = @ThongTinKhac,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan,
	[MaDoanhNghiep] = @MaDoanhNghiep
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuanPhongKhai_CauHinh_DeleteALL]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_Delete]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuanPhongKhai_CauHinh_DeleteALL]
	@MaDoanhNghiep char(15)
	
AS
DELETE FROM 
	[dbo].[t_HaiQuanPhongKhai_CauHinh]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_NoiDungToKhai]'
GO
CREATE TABLE [dbo].[t_HaiQuan_NoiDungToKhai]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Ma] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaLoaiHinh] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GhiChu] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_NoiDungToKhai] on [dbo].[t_HaiQuan_NoiDungToKhai]'
GO
ALTER TABLE [dbo].[t_HaiQuan_NoiDungToKhai] ADD CONSTRAINT [PK_t_HaiQuan_NoiDungToKhai] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NoiDungToKhai_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NoiDungToKhai_Insert]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 26 Tháng Giêng 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NoiDungToKhai_Insert]
	@Ma varchar(50),
	@Ten nvarchar(max),
	@MaLoaiHinh varchar(5),
	@GhiChu nvarchar(max),
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_HaiQuan_NoiDungToKhai]
(
	[Ma],
	[Ten],
	[MaLoaiHinh],
	[GhiChu]
)
VALUES 
(
	@Ma,
	@Ten,
	@MaLoaiHinh,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_Load]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_Load]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[HoaDonTM_ID],
	[GiaTriDieuChinhTang],
	[GiaiTriDieuChinhGiam],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HoaDonThuongMaiDetail]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Message_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_SelectAll]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Message_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceID],
	[ItemID],
	[MessageFrom],
	[MessageTo],
	[MessageType],
	[MessageFunction],
	[MessageContent],
	[CreatedTime],
	[TieuDeThongBao],
	[NoiDungThongBao]
FROM
	[dbo].[t_KDT_Messages]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NoiDungToKhai_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NoiDungToKhai_Update]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 26 Tháng Giêng 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NoiDungToKhai_Update]
	@ID int,
	@Ma varchar(50),
	@Ten nvarchar(max),
	@MaLoaiHinh varchar(5),
	@GhiChu nvarchar(max)
AS

UPDATE
	[dbo].[t_HaiQuan_NoiDungToKhai]
SET
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaLoaiHinh] = @MaLoaiHinh,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_DeleteBy_ChungTuKemID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_DeleteBy_ChungTuKemID]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_DeleteBy_ChungTuKemID]
	@ChungTuKemID bigint
AS

DELETE FROM [dbo].[t_KDT_ChungTuKemChiTiet]
WHERE
	[ChungTuKemID] = @ChungTuKemID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaH_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaH_Insert]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 07, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaH_Insert]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255)
AS
INSERT INTO [dbo].[t_HaiQuan_MaHS]
(
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO]
)
VALUES
(
	@Nhom,
	@PN1,
	@PN2,
	@Pn3,
	@Mo_ta,
	@HS10SO
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_InsertUpdate]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_InsertUpdate]
	@ID bigint,
	@SoHoaDon varchar(100),
	@NgayHoaDon datetime,
	@NguyenTe_ID char(3),
	@PTTT_ID char(3),
	@DKGH_ID varchar(7),
	@MaDonViMua varchar(50),
	@TenDonViMua nvarchar(100),
	@MaDonViBan varchar(50),
	@TenDonViBan nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@MaDoanhNghiep varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HoaDonThuongMai] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HoaDonThuongMai] 
		SET
			[SoHoaDon] = @SoHoaDon,
			[NgayHoaDon] = @NgayHoaDon,
			[NguyenTe_ID] = @NguyenTe_ID,
			[PTTT_ID] = @PTTT_ID,
			[DKGH_ID] = @DKGH_ID,
			[MaDonViMua] = @MaDonViMua,
			[TenDonViMua] = @TenDonViMua,
			[MaDonViBan] = @MaDonViBan,
			[TenDonViBan] = @TenDonViBan,
			[ThongTinKhac] = @ThongTinKhac,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan,
			[MaDoanhNghiep] = @MaDoanhNghiep
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HoaDonThuongMai]
		(
			[SoHoaDon],
			[NgayHoaDon],
			[NguyenTe_ID],
			[PTTT_ID],
			[DKGH_ID],
			[MaDonViMua],
			[TenDonViMua],
			[MaDonViBan],
			[TenDonViBan],
			[ThongTinKhac],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan],
			[MaDoanhNghiep]
		)
		VALUES 
		(
			@SoHoaDon,
			@NgayHoaDon,
			@NguyenTe_ID,
			@PTTT_ID,
			@DKGH_ID,
			@MaDonViMua,
			@TenDonViMua,
			@MaDonViBan,
			@TenDonViBan,
			@ThongTinKhac,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan,
			@MaDoanhNghiep
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NoiDungToKhai_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NoiDungToKhai_InsertUpdate]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 26 Tháng Giêng 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NoiDungToKhai_InsertUpdate]
	@ID int,
	@Ma varchar(50),
	@Ten nvarchar(max),
	@MaLoaiHinh varchar(5),
	@GhiChu nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_NoiDungToKhai] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_NoiDungToKhai] 
		SET
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaLoaiHinh] = @MaLoaiHinh,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_HaiQuan_NoiDungToKhai]
		(
			[Ma],
			[Ten],
			[MaLoaiHinh],
			[GhiChu]
		)
		VALUES 
		(
			@Ma,
			@Ten,
			@MaLoaiHinh,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectAll]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectAll]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[HoaDonTM_ID],
	[GiaTriDieuChinhTang],
	[GiaiTriDieuChinhGiam],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HoaDonThuongMaiDetail]	



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_ChungTuKem]'
GO
CREATE TABLE [dbo].[t_KDT_ChungTuKem]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SO_CT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NGAY_CT] [datetime] NULL,
[MA_LOAI_CT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DIENGIAI] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiKB] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrangThaiXuLy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tempt] [numeric] (18, 0) NULL,
[MessageID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUIDSTR] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KDT_WAITING] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KDT_LASTINFO] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOTN] [numeric] (18, 0) NULL,
[NGAYTN] [datetime] NULL,
[TotalSize] [numeric] (18, 0) NULL,
[Phanluong] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HuongDan] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKMDID] [bigint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DChungTuKem] on [dbo].[t_KDT_ChungTuKem]'
GO
ALTER TABLE [dbo].[t_KDT_ChungTuKem] ADD CONSTRAINT [PK_DChungTuKem] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_Insert]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_Insert]
	@SO_CT varchar(50),
	@NGAY_CT datetime,
	@MA_LOAI_CT varchar(50),
	@DIENGIAI varchar(2000),
	@LoaiKB varchar(4),
	@TrangThaiXuLy varchar(50),
	@Tempt numeric(18, 0),
	@MessageID varchar(100),
	@GUIDSTR varchar(100),
	@KDT_WAITING varchar(50),
	@KDT_LASTINFO varchar(500),
	@SOTN numeric(18, 0),
	@NGAYTN datetime,
	@TotalSize numeric(18, 0),
	@Phanluong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMDID bigint,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_ChungTuKem]
(
	[SO_CT],
	[NGAY_CT],
	[MA_LOAI_CT],
	[DIENGIAI],
	[LoaiKB],
	[TrangThaiXuLy],
	[Tempt],
	[MessageID],
	[GUIDSTR],
	[KDT_WAITING],
	[KDT_LASTINFO],
	[SOTN],
	[NGAYTN],
	[TotalSize],
	[Phanluong],
	[HuongDan],
	[TKMDID]
)
VALUES 
(
	@SO_CT,
	@NGAY_CT,
	@MA_LOAI_CT,
	@DIENGIAI,
	@LoaiKB,
	@TrangThaiXuLy,
	@Tempt,
	@MessageID,
	@GUIDSTR,
	@KDT_WAITING,
	@KDT_LASTINFO,
	@SOTN,
	@NGAYTN,
	@TotalSize,
	@Phanluong,
	@HuongDan,
	@TKMDID
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NoiDungToKhai_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NoiDungToKhai_Delete]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 26 Tháng Giêng 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NoiDungToKhai_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_HaiQuan_NoiDungToKhai]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_Delete]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_HangTKTG]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_SXXK_SanPham]'
GO
CREATE TABLE [dbo].[t_SXXK_SanPham]
(
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ma] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_SXXK_SanPham] on [dbo].[t_SXXK_SanPham]'
GO
ALTER TABLE [dbo].[t_SXXK_SanPham] ADD CONSTRAINT [PK_t_SXXK_SanPham] PRIMARY KEY CLUSTERED  ([MaDoanhNghiep], [Ma], [MaHaiQuan])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SanPham_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_InsertUpdate]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_InsertUpdate]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@Ten nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(3)
AS
IF EXISTS(SELECT [MaDoanhNghiep], [Ma], [MaHaiQuan] FROM [dbo].[t_SXXK_SanPham] WHERE [MaDoanhNghiep] = @MaDoanhNghiep AND [Ma] = @Ma AND [MaHaiQuan] = @MaHaiQuan)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_SanPham] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID
		WHERE
			[MaDoanhNghiep] = @MaDoanhNghiep
			AND [Ma] = @Ma
			AND [MaHaiQuan] = @MaHaiQuan
	END
ELSE
	BEGIN
		INSERT INTO [dbo].[t_SXXK_SanPham]
		(
			[MaHaiQuan],
			[MaDoanhNghiep],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID]
		) 
		VALUES
		(
			@MaHaiQuan,
			@MaDoanhNghiep,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID
		)
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_Delete]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HoaDonThuongMai]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NoiDungToKhai_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NoiDungToKhai_Load]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 26 Tháng Giêng 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NoiDungToKhai_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaLoaiHinh],
	[GhiChu]
FROM
	[dbo].[t_HaiQuan_NoiDungToKhai]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectBy_HMD_ID]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectBy_HMD_ID]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectBy_HMD_ID]
	@HMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[HoaDonTM_ID],
	[GiaTriDieuChinhTang],
	[GiaiTriDieuChinhGiam],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HoaDonThuongMaiDetail]
WHERE
	[HMD_ID] = @HMD_ID



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_SXXK_NguyenPhuLieu]'
GO
CREATE TABLE [dbo].[t_SXXK_NguyenPhuLieu]
(
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ma] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHS] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_SXXK_NguyenPhuLieu] on [dbo].[t_SXXK_NguyenPhuLieu]'
GO
ALTER TABLE [dbo].[t_SXXK_NguyenPhuLieu] ADD CONSTRAINT [PK_t_SXXK_NguyenPhuLieu] PRIMARY KEY CLUSTERED  ([MaHaiQuan], [MaDoanhNghiep], [Ma])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_NguyenPhuLieu_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_SelectAll]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 23, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM
	[dbo].[t_SXXK_NguyenPhuLieu]
ORDER BY Ma


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NoiDungToKhai_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NoiDungToKhai_SelectAll]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 26 Tháng Giêng 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NoiDungToKhai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaLoaiHinh],
	[GhiChu]
FROM
	[dbo].[t_HaiQuan_NoiDungToKhai]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_Load]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM
	[dbo].[t_KDT_ChungTuKemChiTiet]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_MsgSend_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_Update]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 17, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Update]
	@master_id bigint,
	@func int,
	@LoaiHS varchar(50),
	@msg ntext
AS
UPDATE
	[dbo].[t_KDT_SXXK_MsgSend]
SET
	[func] = @func,
	[msg] = @msg
WHERE
	[master_id] = @master_id
	AND [LoaiHS] = @LoaiHS

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT]'
GO
CREATE TABLE [dbo].[t_KDT]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoLuong] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrangThai] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT] on [dbo].[t_KDT]'
GO
ALTER TABLE [dbo].[t_KDT] ADD CONSTRAINT [PK_t_KDT] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_Edit_SL_DN]'
GO
create proc [dbo].[p_Edit_SL_DN]
(
@Soluong int,
@databaseName varchar(30)
)
as
begin
update dbo.[t_kdt]
set soluong =  @Soluong
where id = 1
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_DeleteBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HoaDonThuongMai]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectBy_HoaDonTM_ID]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectBy_HoaDonTM_ID]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectBy_HoaDonTM_ID]
	@HoaDonTM_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[HoaDonTM_ID],
	[GiaTriDieuChinhTang],
	[GiaiTriDieuChinhGiam],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HoaDonThuongMaiDetail]
WHERE
	[HoaDonTM_ID] = @HoaDonTM_ID



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_SelectBy_ChungTuKemID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_SelectBy_ChungTuKemID]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_SelectBy_ChungTuKemID]
	@ChungTuKemID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM
	[dbo].[t_KDT_ChungTuKemChiTiet]
WHERE
	[ChungTuKemID] = @ChungTuKemID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_Insert]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_Insert]
	@GiayPhep_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@TriGiaKB float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HangGiayPhepDetail]
(
	[GiayPhep_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
)
VALUES 
(
	@GiayPhep_ID,
	@MaNguyenTe,
	@MaChuyenNganh,
	@GhiChu,
	@HMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB
)

SET @ID = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_DeNghiChuyenCuaKhau]'
GO
CREATE TABLE [dbo].[t_KDT_DeNghiChuyenCuaKhau]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[ThongTinKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDoanhNghiep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TKMD_ID] [bigint] NULL,
[GuidStr] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiKB] [int] NULL,
[SoTiepNhan] [bigint] NULL,
[NgayTiepNhan] [datetime] NULL,
[TrangThai] [int] NULL,
[NamTiepNhan] [int] NULL,
[SoVanDon] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayVanDon] [datetime] NULL,
[ThoiGianDen] [datetime] NULL,
[DiaDiemKiemTra] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TuyenDuong] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_DeNghiChuyenCuaKhau] on [dbo].[t_KDT_DeNghiChuyenCuaKhau]'
GO
ALTER TABLE [dbo].[t_KDT_DeNghiChuyenCuaKhau] ADD CONSTRAINT [PK_t_KDT_DeNghiChuyenCuaKhau] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@SoVanDon varchar(500),
	@NgayVanDon datetime,
	@ThoiGianDen datetime,
	@DiaDiemKiemTra nvarchar(500),
	@TuyenDuong nvarchar(500),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_DeNghiChuyenCuaKhau]
(
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong]
)
VALUES 
(
	@ThongTinKhac,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan,
	@SoVanDon,
	@NgayVanDon,
	@ThoiGianDen,
	@DiaDiemKiemTra,
	@TuyenDuong
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_Update]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_Update]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_Update]
	@ID bigint,
	@HMD_ID bigint,
	@HoaDonTM_ID bigint,
	@GiaTriDieuChinhTang float,
	@GiaiTriDieuChinhGiam float,
	@GhiChu nvarchar(100),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@TriGiaKB float
AS

UPDATE
	[dbo].[t_KDT_HoaDonThuongMaiDetail]
SET
	[HMD_ID] = @HMD_ID,
	[HoaDonTM_ID] = @HoaDonTM_ID,
	[GiaTriDieuChinhTang] = @GiaTriDieuChinhTang,
	[GiaiTriDieuChinhGiam] = @GiaiTriDieuChinhGiam,
	[GhiChu] = @GhiChu,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB
WHERE
	[ID] = @ID



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_Update]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_Update]
	@ID bigint,
	@SO_CT varchar(50),
	@NGAY_CT datetime,
	@MA_LOAI_CT varchar(50),
	@DIENGIAI varchar(2000),
	@LoaiKB varchar(4),
	@TrangThaiXuLy varchar(50),
	@Tempt numeric(18, 0),
	@MessageID varchar(100),
	@GUIDSTR varchar(100),
	@KDT_WAITING varchar(50),
	@KDT_LASTINFO varchar(500),
	@SOTN numeric(18, 0),
	@NGAYTN datetime,
	@TotalSize numeric(18, 0),
	@Phanluong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMDID bigint
AS

UPDATE
	[dbo].[t_KDT_ChungTuKem]
SET
	[SO_CT] = @SO_CT,
	[NGAY_CT] = @NGAY_CT,
	[MA_LOAI_CT] = @MA_LOAI_CT,
	[DIENGIAI] = @DIENGIAI,
	[LoaiKB] = @LoaiKB,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[Tempt] = @Tempt,
	[MessageID] = @MessageID,
	[GUIDSTR] = @GUIDSTR,
	[KDT_WAITING] = @KDT_WAITING,
	[KDT_LASTINFO] = @KDT_LASTINFO,
	[SOTN] = @SOTN,
	[NGAYTN] = @NGAYTN,
	[TotalSize] = @TotalSize,
	[Phanluong] = @Phanluong,
	[HuongDan] = @HuongDan,
	[TKMDID] = @TKMDID
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_Insert]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_Insert]
	@VanDon_ID bigint,
	@SoHieu varchar(50),
	@LoaiContainer varchar(1),
	@Seal_No varchar(50),
	@Trang_thai int,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_Container]
(
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai]
)
VALUES 
(
	@VanDon_ID,
	@SoHieu,
	@LoaiContainer,
	@Seal_No,
	@Trang_thai
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_Load]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHoaDon],
	[NgayHoaDon],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
FROM
	[dbo].[t_KDT_HoaDonThuongMai]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Delete]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Delete]
	@ID bigint
AS
DELETE FROM 
	[dbo].[t_KDT_KD_ToKhaiTriGiaPP23]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Update]
	@ID bigint,
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@SoVanDon varchar(500),
	@NgayVanDon datetime,
	@ThoiGianDen datetime,
	@DiaDiemKiemTra nvarchar(500),
	@TuyenDuong nvarchar(500)
AS

UPDATE
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
SET
	[ThongTinKhac] = @ThongTinKhac,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[ThoiGianDen] = @ThoiGianDen,
	[DiaDiemKiemTra] = @DiaDiemKiemTra,
	[TuyenDuong] = @TuyenDuong
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_SelectAll]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM
	[dbo].[t_KDT_ChungTuKemChiTiet]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_Update]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_Update]
	@ID bigint,
	@GiayPhep_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@TriGiaKB float
AS

UPDATE
	[dbo].[t_KDT_HangGiayPhepDetail]
SET
	[GiayPhep_ID] = @GiayPhep_ID,
	[MaNguyenTe] = @MaNguyenTe,
	[MaChuyenNganh] = @MaChuyenNganh,
	[GhiChu] = @GhiChu,
	[HMD_ID] = @HMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_Delete]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HopDongThuongMaiDetail]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaH_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaH_Load]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 07, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaH_Load]
	@HS10SO nvarchar(255)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO]
FROM
	[dbo].[t_HaiQuan_MaHS]
WHERE
	[HS10SO] = @HS10SO

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuanPhongKhai_CauHinh_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_Insert]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuanPhongKhai_CauHinh_Insert]
	@MaDoanhNghiep char(15),
	@PassWord varchar(50),
	@TenDoanhNghiep nvarchar(50),
	@Key_Config varchar(30),
	@Value_Config nvarchar(100),
	@Role smallint
AS
INSERT INTO [dbo].[t_HaiQuanPhongKhai_CauHinh]
(
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
)
VALUES
(
	@MaDoanhNghiep,
	@PassWord,
	@TenDoanhNghiep,
	@Key_Config,
	@Value_Config,
	@Role
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_Load]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenChungTu],
	[SoBanChinh],
	[SoBanSao],
	[FileUpLoad],
	[Master_ID],
	[STTHang],
	[LoaiCT],
	[NoiDung]
FROM
	[dbo].[t_KDT_SXXK_ChungTuKemTheo]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_SelectBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHoaDon],
	[NgayHoaDon],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
FROM
	[dbo].[t_KDT_HoaDonThuongMai]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_InsertUpdate]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_InsertUpdate]
	@ID bigint,
	@SO_CT varchar(50),
	@NGAY_CT datetime,
	@MA_LOAI_CT varchar(50),
	@DIENGIAI varchar(2000),
	@LoaiKB varchar(4),
	@TrangThaiXuLy varchar(50),
	@Tempt numeric(18, 0),
	@MessageID varchar(100),
	@GUIDSTR varchar(100),
	@KDT_WAITING varchar(50),
	@KDT_LASTINFO varchar(500),
	@SOTN numeric(18, 0),
	@NGAYTN datetime,
	@TotalSize numeric(18, 0),
	@Phanluong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMDID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ChungTuKem] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ChungTuKem] 
		SET
			[SO_CT] = @SO_CT,
			[NGAY_CT] = @NGAY_CT,
			[MA_LOAI_CT] = @MA_LOAI_CT,
			[DIENGIAI] = @DIENGIAI,
			[LoaiKB] = @LoaiKB,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[Tempt] = @Tempt,
			[MessageID] = @MessageID,
			[GUIDSTR] = @GUIDSTR,
			[KDT_WAITING] = @KDT_WAITING,
			[KDT_LASTINFO] = @KDT_LASTINFO,
			[SOTN] = @SOTN,
			[NGAYTN] = @NGAYTN,
			[TotalSize] = @TotalSize,
			[Phanluong] = @Phanluong,
			[HuongDan] = @HuongDan,
			[TKMDID] = @TKMDID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ChungTuKem]
		(
			[SO_CT],
			[NGAY_CT],
			[MA_LOAI_CT],
			[DIENGIAI],
			[LoaiKB],
			[TrangThaiXuLy],
			[Tempt],
			[MessageID],
			[GUIDSTR],
			[KDT_WAITING],
			[KDT_LASTINFO],
			[SOTN],
			[NGAYTN],
			[TotalSize],
			[Phanluong],
			[HuongDan],
			[TKMDID]
		)
		VALUES 
		(
			@SO_CT,
			@NGAY_CT,
			@MA_LOAI_CT,
			@DIENGIAI,
			@LoaiKB,
			@TrangThaiXuLy,
			@Tempt,
			@MessageID,
			@GUIDSTR,
			@KDT_WAITING,
			@KDT_LASTINFO,
			@SOTN,
			@NGAYTN,
			@TotalSize,
			@Phanluong,
			@HuongDan,
			@TKMDID
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_InsertUpdate]
	@ID bigint,
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@SoVanDon varchar(500),
	@NgayVanDon datetime,
	@ThoiGianDen datetime,
	@DiaDiemKiemTra nvarchar(500),
	@TuyenDuong nvarchar(500)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_DeNghiChuyenCuaKhau] 
		SET
			[ThongTinKhac] = @ThongTinKhac,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[ThoiGianDen] = @ThoiGianDen,
			[DiaDiemKiemTra] = @DiaDiemKiemTra,
			[TuyenDuong] = @TuyenDuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_DeNghiChuyenCuaKhau]
		(
			[ThongTinKhac],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan],
			[SoVanDon],
			[NgayVanDon],
			[ThoiGianDen],
			[DiaDiemKiemTra],
			[TuyenDuong]
		)
		VALUES 
		(
			@ThongTinKhac,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan,
			@SoVanDon,
			@NgayVanDon,
			@ThoiGianDen,
			@DiaDiemKiemTra,
			@TuyenDuong
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_DeleteBy_TKTG_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_DeleteBy_TKTG_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_DeleteBy_TKTG_ID]
	@TKTG_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_HangTKTG]
WHERE
	[TKTG_ID] = @TKTG_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_DeleteBy_HMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_DeleteBy_HMD_ID]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_DeleteBy_HMD_ID]
	@HMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HopDongThuongMaiDetail]
WHERE
	[HMD_ID] = @HMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_SelectAll]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHoaDon],
	[NgayHoaDon],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
FROM
	[dbo].[t_KDT_HoaDonThuongMai]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_ROLE_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_ROLE_Insert]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_ROLE_Insert]
	@GROUP_ID bigint,
	@ID_ROLE bigint
AS
INSERT INTO [dbo].[GROUP_ROLE]
(
	[GROUP_ID],
	[ID_ROLE]
)
VALUES
(
	@GROUP_ID,
	@ID_ROLE
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_Update]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_Update]
	@ID bigint,
	@TenChungTu nvarchar(50),
	@SoBanChinh smallint,
	@SoBanSao smallint,
	@FileUpLoad varchar(500),
	@Master_ID bigint,
	@STTHang int,
	@LoaiCT int,
	@NoiDung image
AS
UPDATE
	[dbo].[t_KDT_SXXK_ChungTuKemTheo]
SET
	[TenChungTu] = @TenChungTu,
	[SoBanChinh] = @SoBanChinh,
	[SoBanSao] = @SoBanSao,
	[FileUpLoad] = @FileUpLoad,
	[Master_ID] = @Master_ID,
	[STTHang] = @STTHang,
	[LoaiCT] = @LoaiCT,
	[NoiDung] = @NoiDung
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_DeleteBy_HopDongTM_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_DeleteBy_HopDongTM_ID]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_DeleteBy_HopDongTM_ID]
	@HopDongTM_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HopDongThuongMaiDetail]
WHERE
	[HopDongTM_ID] = @HopDongTM_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiCO_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiCO_SelectAll]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiCO_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Ma],
	[Ten]
FROM
	[dbo].[t_HaiQuan_LoaiCO]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_InsertUpdate]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_InsertUpdate]
	@ID bigint,
	@GiayPhep_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@TriGiaKB float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangGiayPhepDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangGiayPhepDetail] 
		SET
			[GiayPhep_ID] = @GiayPhep_ID,
			[MaNguyenTe] = @MaNguyenTe,
			[MaChuyenNganh] = @MaChuyenNganh,
			[GhiChu] = @GhiChu,
			[HMD_ID] = @HMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangGiayPhepDetail]
		(
			[GiayPhep_ID],
			[MaNguyenTe],
			[MaChuyenNganh],
			[GhiChu],
			[HMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB]
		)
		VALUES 
		(
			@GiayPhep_ID,
			@MaNguyenTe,
			@MaChuyenNganh,
			@GhiChu,
			@HMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_HopDongThuongMai]'
GO
CREATE TABLE [dbo].[t_KDT_HopDongThuongMai]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoHopDongTM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayHopDongTM] [datetime] NOT NULL,
[ThoiHanThanhToan] [datetime] NOT NULL,
[NguyenTe_ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PTTT_ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DKGH_ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiaDiemGiaoHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDonViMua] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonViMua] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDonViBan] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonViBan] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TongTriGia] [money] NOT NULL,
[ThongTinKhac] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TKMD_ID] [bigint] NOT NULL,
[GuidStr] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiKB] [int] NULL,
[SoTiepNhan] [bigint] NULL,
[NgayTiepNhan] [datetime] NULL,
[TrangThai] [int] NULL,
[NamTiepNhan] [int] NULL,
[MaDoanhNghiep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_HopDong] on [dbo].[t_KDT_HopDongThuongMai]'
GO
ALTER TABLE [dbo].[t_KDT_HopDongThuongMai] ADD CONSTRAINT [PK_t_KDT_HopDong] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_Insert]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_Insert]
	@SoHopDongTM varchar(50),
	@NgayHopDongTM datetime,
	@ThoiHanThanhToan datetime,
	@NguyenTe_ID varchar(3),
	@PTTT_ID varchar(3),
	@DKGH_ID varchar(3),
	@DiaDiemGiaoHang nvarchar(255),
	@MaDonViMua varchar(50),
	@TenDonViMua nvarchar(100),
	@MaDonViBan varchar(50),
	@TenDonViBan nvarchar(100),
	@TongTriGia money,
	@ThongTinKhac nvarchar(255),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@MaDoanhNghiep varchar(50),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HopDongThuongMai]
(
	[SoHopDongTM],
	[NgayHopDongTM],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
)
VALUES 
(
	@SoHopDongTM,
	@NgayHopDongTM,
	@ThoiHanThanhToan,
	@NguyenTe_ID,
	@PTTT_ID,
	@DKGH_ID,
	@DiaDiemGiaoHang,
	@MaDonViMua,
	@TenDonViMua,
	@MaDonViBan,
	@TenDonViBan,
	@TongTriGia,
	@ThongTinKhac,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan,
	@MaDoanhNghiep
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HeThongPhongKhai]'
GO
CREATE TABLE [dbo].[t_HeThongPhongKhai]
(
[MaDoanhNghiep] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PassWord] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDoanhNghiep] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Key_Config] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value_Config] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Role] [smallint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_t_HeThongPhongKhai] on [dbo].[t_HeThongPhongKhai]'
GO
ALTER TABLE [dbo].[t_HeThongPhongKhai] ADD CONSTRAINT [PK_t_t_HeThongPhongKhai] PRIMARY KEY CLUSTERED  ([MaDoanhNghiep], [Key_Config])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HeThongPhongKhai_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_Insert]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 11, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_Insert]
	@MaDoanhNghiep char(15),
	@PassWord varchar(50),
	@TenDoanhNghiep nvarchar(500),
	@Key_Config varchar(30),
	@Value_Config nvarchar(1000),
	@Role smallint
AS
INSERT INTO [dbo].[t_HeThongPhongKhai]
(
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
)
VALUES
(
	@MaDoanhNghiep,
	@PassWord,
	@TenDoanhNghiep,
	@Key_Config,
	@Value_Config,
	@Role
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_Update]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_Update]
	@ID bigint,
	@VanDon_ID bigint,
	@SoHieu varchar(50),
	@LoaiContainer varchar(1),
	@Seal_No varchar(50),
	@Trang_thai int
AS

UPDATE
	[dbo].[t_KDT_Container]
SET
	[VanDon_ID] = @VanDon_ID,
	[SoHieu] = @SoHieu,
	[LoaiContainer] = @LoaiContainer,
	[Seal_No] = @Seal_No,
	[Trang_thai] = @Trang_thai
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HuyToKhai]'
GO
CREATE TABLE [dbo].[t_HuyToKhai]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[LyDoHuy] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoTN] [int] NULL,
[NgayTN] [datetime] NULL,
[NamTN] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HuyToKhai] on [dbo].[t_HuyToKhai]'
GO
ALTER TABLE [dbo].[t_HuyToKhai] ADD CONSTRAINT [PK_t_HuyToKhai] PRIMARY KEY CLUSTERED  ([TKMD_ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HuyToKhai_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HuyToKhai_Insert]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, July 14, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HuyToKhai_Insert]
	@ID int,
	@TKMD_ID bigint,
	@LyDoHuy nvarchar(500),
	@SoTN int,
	@NgayTN datetime,
	@NamTN int
AS
INSERT INTO [dbo].[t_HuyToKhai]
(
	[ID],
	[TKMD_ID],
	[LyDoHuy],
	[SoTN],
	[NgayTN],
	[NamTN]
)
VALUES
(
	@ID,
	@TKMD_ID,
	@LyDoHuy,
	@SoTN,
	@NgayTN,
	@NamTN
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_NoiDungDieuChinhTK]'
GO
CREATE TABLE [dbo].[t_KDT_NoiDungDieuChinhTK]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[SoTK] [int] NULL,
[NgayDK] [datetime] NULL,
[MaLoaiHinh] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgaySua] [datetime] NULL,
[SoDieuChinh] [int] NULL,
[TrangThai] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_NoiDungDieuChinhTK] on [dbo].[t_KDT_NoiDungDieuChinhTK]'
GO
ALTER TABLE [dbo].[t_KDT_NoiDungDieuChinhTK] ADD CONSTRAINT [PK_t_KDT_NoiDungDieuChinhTK] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_Delete]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_NoiDungDieuChinhTK]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuanPhongKhai_CauHinh_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_InsertUpdate]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuanPhongKhai_CauHinh_InsertUpdate]
	@MaDoanhNghiep char(15),
	@PassWord varchar(50),
	@TenDoanhNghiep nvarchar(50),
	@Key_Config varchar(30),
	@Value_Config nvarchar(100),
	@Role smallint
AS
IF EXISTS(SELECT [MaDoanhNghiep], [Key_Config] FROM [dbo].[t_HaiQuanPhongKhai_CauHinh] WHERE [MaDoanhNghiep] = @MaDoanhNghiep AND [Key_Config] = @Key_Config)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuanPhongKhai_CauHinh] 
		SET
			[PassWord] = @PassWord,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[Value_Config] = @Value_Config,
			[Role] = @Role
		WHERE
			[MaDoanhNghiep] = @MaDoanhNghiep
			AND [Key_Config] = @Key_Config
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuanPhongKhai_CauHinh]
	(
			[MaDoanhNghiep],
			[PassWord],
			[TenDoanhNghiep],
			[Key_Config],
			[Value_Config],
			[Role]
	)
	VALUES
	(
			@MaDoanhNghiep,
			@PassWord,
			@TenDoanhNghiep,
			@Key_Config,
			@Value_Config,
			@Role
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HeThongPhongKhai_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_Update]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 11, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_Update]
	@MaDoanhNghiep char(15),
	@PassWord varchar(50),
	@TenDoanhNghiep nvarchar(500),
	@Key_Config varchar(30),
	@Value_Config nvarchar(1000),
	@Role smallint
AS
UPDATE
	[dbo].[t_HeThongPhongKhai]
SET
	[PassWord] = @PassWord,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[Value_Config] = @Value_Config,
	[Role] = @Role
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Key_Config] = @Key_Config

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_Load]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenHang],
	[STTHang],
	[GiaTrenHoaDon],
	[KhoanThanhToanGianTiep],
	[TraTruoc],
	[TongCong1],
	[HoaHong],
	[ChiPhiBaoBi],
	[ChiPhiDongGoi],
	[TroGiup],
	[VatLieu],
	[CongCu],
	[NguyenLieu],
	[ThietKe],
	[BanQuyen],
	[TienTraSuDung],
	[ChiPhiVanChuyen],
	[CuocPhiXepHang],
	[CuocPhiBaoHiem],
	[TongCong2],
	[PhiBaoHiem],
	[ChiPhiPhatSinh],
	[TienLai],
	[TienThue],
	[GiamGia],
	[ChiPhiKhongTang],
	[TriGiaNguyenTe],
	[TriGiaVND],
	[NgayTruyen],
	[TKTG_ID],
	[ChietKhau],
	[TongCong3],
	[ChiPhiNoiDia]
FROM
	[dbo].[t_KDT_SXXK_HangTKTG]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_Delete]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ChungTuKem]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_Insert]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_Insert]
	@TEN_NHOM nvarchar(1000),
	@MO_TA nvarchar(1000),
	@MA_NHOM bigint OUTPUT
AS
INSERT INTO [dbo].[GROUPS]
(
	[TEN_NHOM],
	[MO_TA]
)
VALUES 
(
	@TEN_NHOM,
	@MO_TA
)

SET @MA_NHOM = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_Update]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_Update]
	@ID bigint,
	@SoHopDongTM varchar(50),
	@NgayHopDongTM datetime,
	@ThoiHanThanhToan datetime,
	@NguyenTe_ID varchar(3),
	@PTTT_ID varchar(3),
	@DKGH_ID varchar(3),
	@DiaDiemGiaoHang nvarchar(255),
	@MaDonViMua varchar(50),
	@TenDonViMua nvarchar(100),
	@MaDonViBan varchar(50),
	@TenDonViBan nvarchar(100),
	@TongTriGia money,
	@ThongTinKhac nvarchar(255),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@MaDoanhNghiep varchar(50)
AS

UPDATE
	[dbo].[t_KDT_HopDongThuongMai]
SET
	[SoHopDongTM] = @SoHopDongTM,
	[NgayHopDongTM] = @NgayHopDongTM,
	[ThoiHanThanhToan] = @ThoiHanThanhToan,
	[NguyenTe_ID] = @NguyenTe_ID,
	[PTTT_ID] = @PTTT_ID,
	[DKGH_ID] = @DKGH_ID,
	[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
	[MaDonViMua] = @MaDonViMua,
	[TenDonViMua] = @TenDonViMua,
	[MaDonViBan] = @MaDonViBan,
	[TenDonViBan] = @TenDonViBan,
	[TongTriGia] = @TongTriGia,
	[ThongTinKhac] = @ThongTinKhac,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan,
	[MaDoanhNghiep] = @MaDoanhNghiep
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_MsgSend_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_Delete]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 17, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Delete]
	@master_id bigint,
	@LoaiHS varchar(50)
AS
DELETE FROM 
	[dbo].[t_KDT_SXXK_MsgSend]
WHERE
	[master_id] = @master_id
	AND [LoaiHS] = @LoaiHS

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HeThongPhongKhai_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_InsertUpdate]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 11, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_InsertUpdate]
	@MaDoanhNghiep char(15),
	@PassWord varchar(50),
	@TenDoanhNghiep nvarchar(500),
	@Key_Config varchar(30),
	@Value_Config nvarchar(1000),
	@Role smallint
AS
IF EXISTS(SELECT [MaDoanhNghiep], [Key_Config] FROM [dbo].[t_HeThongPhongKhai] WHERE [MaDoanhNghiep] = @MaDoanhNghiep AND [Key_Config] = @Key_Config)
	BEGIN
		UPDATE
			[dbo].[t_HeThongPhongKhai] 
		SET
			[PassWord] = @PassWord,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[Value_Config] = @Value_Config,
			[Role] = @Role
		WHERE
			[MaDoanhNghiep] = @MaDoanhNghiep
			AND [Key_Config] = @Key_Config
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HeThongPhongKhai]
	(
			[MaDoanhNghiep],
			[PassWord],
			[TenDoanhNghiep],
			[Key_Config],
			[Value_Config],
			[Role]
	)
	VALUES
	(
			@MaDoanhNghiep,
			@PassWord,
			@TenDoanhNghiep,
			@Key_Config,
			@Value_Config,
			@Role
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_InsertUpdate]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_InsertUpdate]
	@ID bigint,
	@SoHopDongTM varchar(50),
	@NgayHopDongTM datetime,
	@ThoiHanThanhToan datetime,
	@NguyenTe_ID varchar(3),
	@PTTT_ID varchar(3),
	@DKGH_ID varchar(3),
	@DiaDiemGiaoHang nvarchar(255),
	@MaDonViMua varchar(50),
	@TenDonViMua nvarchar(100),
	@MaDonViBan varchar(50),
	@TenDonViBan nvarchar(100),
	@TongTriGia money,
	@ThongTinKhac nvarchar(255),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@MaDoanhNghiep varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HopDongThuongMai] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HopDongThuongMai] 
		SET
			[SoHopDongTM] = @SoHopDongTM,
			[NgayHopDongTM] = @NgayHopDongTM,
			[ThoiHanThanhToan] = @ThoiHanThanhToan,
			[NguyenTe_ID] = @NguyenTe_ID,
			[PTTT_ID] = @PTTT_ID,
			[DKGH_ID] = @DKGH_ID,
			[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
			[MaDonViMua] = @MaDonViMua,
			[TenDonViMua] = @TenDonViMua,
			[MaDonViBan] = @MaDonViBan,
			[TenDonViBan] = @TenDonViBan,
			[TongTriGia] = @TongTriGia,
			[ThongTinKhac] = @ThongTinKhac,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan,
			[MaDoanhNghiep] = @MaDoanhNghiep
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HopDongThuongMai]
		(
			[SoHopDongTM],
			[NgayHopDongTM],
			[ThoiHanThanhToan],
			[NguyenTe_ID],
			[PTTT_ID],
			[DKGH_ID],
			[DiaDiemGiaoHang],
			[MaDonViMua],
			[TenDonViMua],
			[MaDonViBan],
			[TenDonViBan],
			[TongTriGia],
			[ThongTinKhac],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan],
			[MaDoanhNghiep]
		)
		VALUES 
		(
			@SoHopDongTM,
			@NgayHopDongTM,
			@ThoiHanThanhToan,
			@NguyenTe_ID,
			@PTTT_ID,
			@DKGH_ID,
			@DiaDiemGiaoHang,
			@MaDonViMua,
			@TenDonViMua,
			@MaDonViBan,
			@TenDonViBan,
			@TongTriGia,
			@ThongTinKhac,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan,
			@MaDoanhNghiep
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_Delete]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HangGiayPhepDetail]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_InsertUpdate]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_InsertUpdate]
	@ID bigint,
	@VanDon_ID bigint,
	@SoHieu varchar(50),
	@LoaiContainer varchar(1),
	@Seal_No varchar(50),
	@Trang_thai int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_Container] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_Container] 
		SET
			[VanDon_ID] = @VanDon_ID,
			[SoHieu] = @SoHieu,
			[LoaiContainer] = @LoaiContainer,
			[Seal_No] = @Seal_No,
			[Trang_thai] = @Trang_thai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_Container]
		(
			[VanDon_ID],
			[SoHieu],
			[LoaiContainer],
			[Seal_No],
			[Trang_thai]
		)
		VALUES 
		(
			@VanDon_ID,
			@SoHieu,
			@LoaiContainer,
			@Seal_No,
			@Trang_thai
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_Insert]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_Insert]
	@TKMD_ID bigint,
	@SoTK int,
	@NgayDK datetime,
	@MaLoaiHinh varchar(50),
	@NgaySua datetime,
	@SoDieuChinh int,
	@TrangThai int,
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_NoiDungDieuChinhTK]
(
	[TKMD_ID],
	[SoTK],
	[NgayDK],
	[MaLoaiHinh],
	[NgaySua],
	[SoDieuChinh],
	[TrangThai]
)
VALUES 
(
	@TKMD_ID,
	@SoTK,
	@NgayDK,
	@MaLoaiHinh,
	@NgaySua,
	@SoDieuChinh,
	@TrangThai
)

SET @ID = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HuyToKhai_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HuyToKhai_Delete]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, July 14, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HuyToKhai_Delete]
	@TKMD_ID bigint
AS

DELETE FROM 
	[dbo].[t_HuyToKhai]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong]
FROM
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_MsgSend_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_SelectAll]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 17, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[master_id],
	[func],
	[LoaiHS],
	[msg]
FROM
	[dbo].[t_KDT_SXXK_MsgSend]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_DeleteBy_TKMDID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_DeleteBy_TKMDID]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_DeleteBy_TKMDID]
	@TKMDID bigint
AS

DELETE FROM [dbo].[t_KDT_ChungTuKem]
WHERE
	[TKMDID] = @TKMDID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiCO_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiCO_Insert]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiCO_Insert]
	@Ma varchar(3),
	@Ten nvarchar(256)
AS
INSERT INTO [dbo].[t_HaiQuan_LoaiCO]
(
	[Ma],
	[Ten]
)
VALUES
(
	@Ma,
	@Ten
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_Delete]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HopDongThuongMai]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaH_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaH_SelectAll]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 07, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaH_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO]
FROM
	[dbo].[t_HaiQuan_MaHS]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HeThongPhongKhai_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_Delete]
-- Database: HaiQuanPhongKhaiKD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 11, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_Delete]
	@MaDoanhNghiep char(15),
	@Key_Config varchar(30)
AS
DELETE FROM 
	[dbo].[t_HeThongPhongKhai]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Key_Config] = @Key_Config

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_InsertUpdate]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_InsertUpdate]
	@ID int,
	@TKMD_ID bigint,
	@SoTK int,
	@NgayDK datetime,
	@MaLoaiHinh varchar(50),
	@NgaySua datetime,
	@SoDieuChinh int,
	@TrangThai int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_NoiDungDieuChinhTK] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_NoiDungDieuChinhTK] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTK] = @SoTK,
			[NgayDK] = @NgayDK,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgaySua] = @NgaySua,
			[SoDieuChinh] = @SoDieuChinh,
			[TrangThai] = @TrangThai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_NoiDungDieuChinhTK]
		(
			[TKMD_ID],
			[SoTK],
			[NgayDK],
			[MaLoaiHinh],
			[NgaySua],
			[SoDieuChinh],
			[TrangThai]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTK,
			@NgayDK,
			@MaLoaiHinh,
			@NgaySua,
			@SoDieuChinh,
			@TrangThai
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_DeleteBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_DeleteBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON

DELETE FROM [dbo].[t_KDT_KD_ToKhaiTriGiaPP23]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaH_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaH_Update]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 07, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaH_Update]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255)
AS
UPDATE
	[dbo].[t_HaiQuan_MaHS]
SET
	[Nhom] = @Nhom,
	[PN1] = @PN1,
	[PN2] = @PN2,
	[Pn3] = @Pn3,
	[Mo_ta] = @Mo_ta
WHERE
	[HS10SO] = @HS10SO

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_SelectBy_TKTG_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_SelectBy_TKTG_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_SelectBy_TKTG_ID]
	@TKTG_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenHang],
	[STTHang],
	[GiaTrenHoaDon],
	[KhoanThanhToanGianTiep],
	[TraTruoc],
	[TongCong1],
	[HoaHong],
	[ChiPhiBaoBi],
	[ChiPhiDongGoi],
	[TroGiup],
	[VatLieu],
	[CongCu],
	[NguyenLieu],
	[ThietKe],
	[BanQuyen],
	[TienTraSuDung],
	[ChiPhiVanChuyen],
	[CuocPhiXepHang],
	[CuocPhiBaoHiem],
	[TongCong2],
	[PhiBaoHiem],
	[ChiPhiPhatSinh],
	[TienLai],
	[TienThue],
	[GiamGia],
	[ChiPhiKhongTang],
	[TriGiaNguyenTe],
	[TriGiaVND],
	[NgayTruyen],
	[TKTG_ID],
	[ChietKhau],
	[TongCong3],
	[ChiPhiNoiDia]
FROM
	[dbo].[t_KDT_SXXK_HangTKTG]
WHERE
	[TKTG_ID] = @TKTG_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_DeleteBy_GiayPhep_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_DeleteBy_GiayPhep_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_DeleteBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HangGiayPhepDetail]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong]
FROM
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_DeleteBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HopDongThuongMai]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Load]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaToKhaiTriGia],
	[LyDo],
	[TenHang],
	[STTHang],
	[NgayXuat],
	[STTHangTT],
	[TenHangTT],
	[NgayXuatTT],
	[SoTKHangTT],
	[NgayDangKyHangTT],
	[MaLoaiHinhHangTT],
	[MaHaiQuanHangTT],
	[TriGiaNguyenTeHangTT],
	[DieuChinhCongThuongMai],
	[DieuChinhCongSoLuong],
	[DieuChinhCongKhoanGiamGiaKhac],
	[DieuChinhCongChiPhiVanTai],
	[DieuChinhCongChiPhiBaoHiem],
	[TriGiaNguyenTeHang],
	[TriGiaTTHang],
	[ToSo],
	[DieuChinhTruCapDoThuongMai],
	[DieuChinhTruSoLuong],
	[DieuChinhTruKhoanGiamGiaKhac],
	[DieuChinhTruChiPhiVanTai],
	[DieuChinhTruChiPhiBaoHiem],
	[GiaiTrinh]
FROM
	[dbo].[t_KDT_KD_ToKhaiTriGiaPP23]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HeThongPhongKhai_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_Load]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 11, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_Load]
	@MaDoanhNghiep char(15),
	@Key_Config varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
FROM
	[dbo].[t_HeThongPhongKhai]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Key_Config] = @Key_Config

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_Load]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTK],
	[NgayDK],
	[MaLoaiHinh],
	[NgaySua],
	[SoDieuChinh],
	[TrangThai]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTK]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HuyToKhai_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HuyToKhai_Load]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, July 14, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HuyToKhai_Load]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[LyDoHuy],
	[SoTN],
	[NgayTN],
	[NamTN]
FROM
	[dbo].[t_HuyToKhai]
WHERE
	[TKMD_ID] = @TKMD_ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HeThongPhongKhai_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_SelectAll]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 11, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
FROM
	[dbo].[t_HeThongPhongKhai]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_SelectAll]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTK],
	[NgayDK],
	[MaLoaiHinh],
	[NgaySua],
	[SoDieuChinh],
	[TrangThai]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTK]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_Delete]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_Container]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_FixDb]'
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_FixDb]
	@MaDoanhNghiep varchar(14)
AS
BEGIN
	DECLARE @SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint,
	@MaHaiQuan char(6),
	@MA_NPL_SP varchar(30),
	@Luong numeric(18,8),
	@Thue_XNK float,
	@Thu_TTDB float,
	@Thue_VAT float,
	@Phu_Thu float,
	@TRIGIA_THUKHAC float
	DECLARE @cur cursor
	SET @cur = cursor FOR 
		SELECT a.SoToKhai, a.MaLoaiHinh, a.NamDangKy, a.MaHaiQuan, 
			   a.Ma_NPL_SP, a.Luong, a.Thue_XNK, a.Thue_TTDB, a.Thue_VAT, a.Phu_Thu, TRIGIA_THUKHAC
		FROM v_SXXK_HangMauDichDieuChinhMoiNhat a
		INNER JOIN t_SXXK_ToKhaiMauDich b 
		ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh
			AND a.NamDangKy = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan
		LEFT JOIN t_SXXK_ThanhLy_NPLNhapTon c
		ON a.SoToKhai = c.SoToKhai AND a.MaLoaiHinh = c.MaLoaiHinh
			AND a.NamDangKy = c.NamDangKy AND a.MaHaiQuan = c.MaHaiQuan AND a.Ma_NPL_SP = c.MaNPL
		WHERE a.MaLoaiHinh LIKE 'NSX%' AND b.NgayDangKy >= '09/05/2007' AND c.Luong is NULL
		ORDER BY b.NgayDangKy
		OPEN @cur
		FETCH NEXT FROM @cur INTO @SoToKhai, @MaLoaiHinh, @NamDangKy, @MaHaiQuan, @MA_NPL_SP,
								  @Luong, @Thue_XNK, @Thu_TTDB, @Thue_VAT, @Phu_Thu, @TRIGIA_THUKHAC
	WHILE @@FETCH_STATUS = 0
    BEGIN
		INSERT INTO t_SXXK_ThanhLy_NPLNhapTon VALUES
		(@SoToKhai,@MaLoaiHinh, @NamDangKy,@MaHaiQuan,@Ma_NPL_SP, @MaDoanhNghiep , @Luong, @Luong, @Thue_XNK, @Thu_TTDB, @Thue_VAT, @Phu_Thu, @TRIGIA_THUKHAC,@Thue_XNK)

		FETCH NEXT FROM @cur INTO @SoToKhai, @MaLoaiHinh, @NamDangKy, @MaHaiQuan, @MA_NPL_SP,
								  @Luong, @Thue_XNK, @Thu_TTDB, @Thue_VAT, @Phu_Thu, @TRIGIA_THUKHAC
    END
            CLOSE @cur
            DEALLOCATE @cur

END






GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong]
FROM
	[dbo].[t_KDT_DeNghiChuyenCuaKhau]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_MsgSend_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_Load]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 17, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Load]
	@master_id bigint,
	@LoaiHS varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[master_id],
	[func],
	[LoaiHS],
	[msg]
FROM
	[dbo].[t_KDT_SXXK_MsgSend]
WHERE
	[master_id] = @master_id
	AND [LoaiHS] = @LoaiHS

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Insert]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 24, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Insert]
	@SoLuong nvarchar(max),
	@TrangThai nvarchar(max),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT]
(
	[SoLuong],
	[TrangThai]
)
VALUES 
(
	@SoLuong,
	@TrangThai
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HeThongPhongKhai_DeleteALL]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_Delete]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_DeleteALL]
	@MaDoanhNghiep char(15)
	
AS
DELETE FROM 
	[dbo].[t_HeThongPhongKhai]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_Load]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHopDongTM],
	[NgayHopDongTM],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
FROM
	[dbo].[t_KDT_HopDongThuongMai]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_Load]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SO_CT],
	[NGAY_CT],
	[MA_LOAI_CT],
	[DIENGIAI],
	[LoaiKB],
	[TrangThaiXuLy],
	[Tempt],
	[MessageID],
	[GUIDSTR],
	[KDT_WAITING],
	[KDT_LASTINFO],
	[SOTN],
	[NGAYTN],
	[TotalSize],
	[Phanluong],
	[HuongDan],
	[TKMDID]
FROM
	[dbo].[t_KDT_ChungTuKem]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_Delete]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_Delete]
	@ID bigint
AS
DELETE FROM 
	[dbo].[t_KDT_SXXK_ChungTuKemTheo]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_SelectBy_TKMD_ID]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_SelectBy_TKMD_ID]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTK],
	[NgayDK],
	[MaLoaiHinh],
	[NgaySua],
	[SoDieuChinh],
	[TrangThai]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTK]
WHERE
	[TKMD_ID] = @TKMD_ID



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiCO_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiCO_Update]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiCO_Update]
	@Ma varchar(3),
	@Ten nvarchar(256)
AS

UPDATE
	[dbo].[t_HaiQuan_LoaiCO]
SET
	[Ten] = @Ten
WHERE
	[Ma] = @Ma

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_SelectAll]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenHang],
	[STTHang],
	[GiaTrenHoaDon],
	[KhoanThanhToanGianTiep],
	[TraTruoc],
	[TongCong1],
	[HoaHong],
	[ChiPhiBaoBi],
	[ChiPhiDongGoi],
	[TroGiup],
	[VatLieu],
	[CongCu],
	[NguyenLieu],
	[ThietKe],
	[BanQuyen],
	[TienTraSuDung],
	[ChiPhiVanChuyen],
	[CuocPhiXepHang],
	[CuocPhiBaoHiem],
	[TongCong2],
	[PhiBaoHiem],
	[ChiPhiPhatSinh],
	[TienLai],
	[TienThue],
	[GiamGia],
	[ChiPhiKhongTang],
	[TriGiaNguyenTe],
	[TriGiaVND],
	[NgayTruyen],
	[TKTG_ID],
	[ChietKhau],
	[TongCong3],
	[ChiPhiNoiDia]
FROM
	[dbo].[t_KDT_SXXK_HangTKTG]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuanPhongKhai_CauHinh_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_Delete]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuanPhongKhai_CauHinh_Delete]
	@MaDoanhNghiep char(15),
	@Key_Config varchar(30)
AS
DELETE FROM 
	[dbo].[t_HaiQuanPhongKhai_CauHinh]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Key_Config] = @Key_Config

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_SelectBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHopDongTM],
	[NgayHopDongTM],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
FROM
	[dbo].[t_KDT_HopDongThuongMai]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SanPham_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_Load]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_Load]
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@MaHaiQuan char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM
	[dbo].[t_SXXK_SanPham]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma
	AND [MaHaiQuan] = @MaHaiQuan

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_DeleteBy_VanDon_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_DeleteBy_VanDon_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_DeleteBy_VanDon_ID]
	@VanDon_ID bigint
AS

DELETE FROM [dbo].[t_KDT_Container]
WHERE
	[VanDon_ID] = @VanDon_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_SelectMax_Id_DieuChinh]'
GO






------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_SelectBy_TKMD_ID]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_SelectMax_Id_DieuChinh]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	Max(ID) as 'ID'
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTK]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_Load]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HangGiayPhepDetail]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Insert]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Insert]
	@SoCO varchar(100),
	@NgayCO datetime,
	@ToChucCap nvarchar(500),
	@NuocCapCO varchar(3),
	@MaNuocXKTrenCO varchar(3),
	@MaNuocNKTrenCO varchar(3),
	@TenDiaChiNguoiXK nvarchar(500),
	@TenDiaChiNguoiNK nvarchar(500),
	@LoaiCO varchar(50),
	@ThongTinMoTaChiTiet nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@NguoiKy nvarchar(500),
	@NoCo int,
	@ThoiHanNop datetime,
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_CO]
(
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
)
VALUES 
(
	@SoCO,
	@NgayCO,
	@ToChucCap,
	@NuocCapCO,
	@MaNuocXKTrenCO,
	@MaNuocNKTrenCO,
	@TenDiaChiNguoiXK,
	@TenDiaChiNguoiNK,
	@LoaiCO,
	@ThongTinMoTaChiTiet,
	@MaDoanhNghiep,
	@NguoiKy,
	@NoCo,
	@ThoiHanNop,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_InsertUpdate]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_InsertUpdate]
	@ID bigint,
	@TenChungTu nvarchar(50),
	@SoBanChinh smallint,
	@SoBanSao smallint,
	@FileUpLoad varchar(500),
	@Master_ID bigint,
	@STTHang int,
	@LoaiCT int,
	@NoiDung image
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_ChungTuKemTheo] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_ChungTuKemTheo] 
		SET
			[TenChungTu] = @TenChungTu,
			[SoBanChinh] = @SoBanChinh,
			[SoBanSao] = @SoBanSao,
			[FileUpLoad] = @FileUpLoad,
			[Master_ID] = @Master_ID,
			[STTHang] = @STTHang,
			[LoaiCT] = @LoaiCT,
			[NoiDung] = @NoiDung
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_ChungTuKemTheo]
		(
			[TenChungTu],
			[SoBanChinh],
			[SoBanSao],
			[FileUpLoad],
			[Master_ID],
			[STTHang],
			[LoaiCT],
			[NoiDung]
		)
		VALUES 
		(
			@TenChungTu,
			@SoBanChinh,
			@SoBanSao,
			@FileUpLoad,
			@Master_ID,
			@STTHang,
			@LoaiCT,
			@NoiDung
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Update]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 24, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Update]
	@ID bigint,
	@SoLuong nvarchar(max),
	@TrangThai nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT]
SET
	[SoLuong] = @SoLuong,
	[TrangThai] = @TrangThai
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_NguyenPhuLieu_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_Update]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 23, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Update]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@Ten nvarchar(80),
	@MaHS varchar(10),
	@DVT_ID char(3)
AS
UPDATE
	[dbo].[t_SXXK_NguyenPhuLieu]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_SelectAll]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHopDongTM],
	[NgayHopDongTM],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
FROM
	[dbo].[t_KDT_HopDongThuongMai]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_SelectBy_TKMDID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_SelectBy_TKMDID]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_SelectBy_TKMDID]
	@TKMDID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SO_CT],
	[NGAY_CT],
	[MA_LOAI_CT],
	[DIENGIAI],
	[LoaiKB],
	[TrangThaiXuLy],
	[Tempt],
	[MessageID],
	[GUIDSTR],
	[KDT_WAITING],
	[KDT_LASTINFO],
	[SOTN],
	[NGAYTN],
	[TotalSize],
	[Phanluong],
	[HuongDan],
	[TKMDID]
FROM
	[dbo].[t_KDT_ChungTuKem]
WHERE
	[TKMDID] = @TKMDID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Update]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Update]
	@ID bigint,
	@SoCO varchar(100),
	@NgayCO datetime,
	@ToChucCap nvarchar(500),
	@NuocCapCO varchar(3),
	@MaNuocXKTrenCO varchar(3),
	@MaNuocNKTrenCO varchar(3),
	@TenDiaChiNguoiXK nvarchar(500),
	@TenDiaChiNguoiNK nvarchar(500),
	@LoaiCO varchar(50),
	@ThongTinMoTaChiTiet nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@NguoiKy nvarchar(500),
	@NoCo int,
	@ThoiHanNop datetime,
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int
AS

UPDATE
	[dbo].[t_KDT_CO]
SET
	[SoCO] = @SoCO,
	[NgayCO] = @NgayCO,
	[ToChucCap] = @ToChucCap,
	[NuocCapCO] = @NuocCapCO,
	[MaNuocXKTrenCO] = @MaNuocXKTrenCO,
	[MaNuocNKTrenCO] = @MaNuocNKTrenCO,
	[TenDiaChiNguoiXK] = @TenDiaChiNguoiXK,
	[TenDiaChiNguoiNK] = @TenDiaChiNguoiNK,
	[LoaiCO] = @LoaiCO,
	[ThongTinMoTaChiTiet] = @ThongTinMoTaChiTiet,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[NguoiKy] = @NguoiKy,
	[NoCo] = @NoCo,
	[ThoiHanNop] = @ThoiHanNop,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh]'
GO





------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_SelectBy_TKMD_ID]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoDieuChinh]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTK]
WHERE
	[TKMD_ID] = @TKMD_ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_SelectBy_GiayPhep_ID]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_SelectBy_GiayPhep_ID]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_SelectBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HangGiayPhepDetail]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_ToKhaiMauDich_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_Insert]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_Insert]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint,
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(100),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@Xuat_NPL_SP char(1),
	@ThanhLy char(1),
	@NGAY_THN_THX datetime,
	@MaDonViUT varchar(14),
	@TrangThaiThanhKhoan char(1),
	@ChungTu nvarchar(40),
	@PhanLuong varchar(2),
	@NgayHoanThanh datetime,
	@TrangThai smallint,
	@TrongLuongNet float,
	@SoTienKhoan money
AS
INSERT INTO [dbo].[t_SXXK_ToKhaiMauDich]
(
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NamDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[Xuat_NPL_SP],
	[ThanhLy],
	[NGAY_THN_THX],
	[MaDonViUT],
	[TrangThaiThanhKhoan],
	[ChungTu],
	[PhanLuong],
	[NgayHoanThanh],
	[TrangThai],
	[TrongLuongNet],
	[SoTienKhoan]
)
VALUES
(
	@MaHaiQuan,
	@SoToKhai,
	@MaLoaiHinh,
	@NamDangKy,
	@NgayDangKy,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaDaiLyTTHQ,
	@TenDaiLyTTHQ,
	@TenDonViDoiTac,
	@ChiTietDonViDoiTac,
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHanGiayPhep,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHanHopDong,
	@SoHoaDonThuongMai,
	@NgayHoaDonThuongMai,
	@PTVT_ID,
	@SoHieuPTVT,
	@NgayDenPTVT,
	@QuocTichPTVT_ID,
	@LoaiVanDon,
	@SoVanDon,
	@NgayVanDon,
	@NuocXK_ID,
	@NuocNK_ID,
	@DiaDiemXepHang,
	@CuaKhau_ID,
	@DKGH_ID,
	@NguyenTe_ID,
	@TyGiaTinhThue,
	@TyGiaUSD,
	@PTTT_ID,
	@SoHang,
	@SoLuongPLTK,
	@TenChuHang,
	@SoContainer20,
	@SoContainer40,
	@SoKien,
	@TrongLuong,
	@TongTriGiaKhaiBao,
	@TongTriGiaTinhThue,
	@LoaiToKhaiGiaCong,
	@LePhiHaiQuan,
	@PhiBaoHiem,
	@PhiVanChuyen,
	@PhiXepDoHang,
	@PhiKhac,
	@Xuat_NPL_SP,
	@ThanhLy,
	@NGAY_THN_THX,
	@MaDonViUT,
	@TrangThaiThanhKhoan,
	@ChungTu,
	@PhanLuong,
	@NgayHoanThanh,
	@TrangThai,
	@TrongLuongNet,
	@SoTienKhoan
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SanPham_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_Insert]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_Insert]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@Ten nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(3)
AS
INSERT INTO [dbo].[t_SXXK_SanPham]
(
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
)
VALUES
(
	@MaHaiQuan,
	@MaDoanhNghiep,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HuyToKhai_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HuyToKhai_SelectAll]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, July 14, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HuyToKhai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[LyDoHuy],
	[SoTN],
	[NgayTN],
	[NamTN]
FROM
	[dbo].[t_HuyToKhai]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiCO_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiCO_InsertUpdate]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiCO_InsertUpdate]
	@Ma varchar(3),
	@Ten nvarchar(256)
AS
IF EXISTS(SELECT [Ma] FROM [dbo].[t_HaiQuan_LoaiCO] WHERE [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_LoaiCO] 
		SET
			[Ten] = @Ten
		WHERE
			[Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_LoaiCO]
	(
			[Ma],
			[Ten]
	)
	VALUES
	(
			@Ma,
			@Ten
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_Update]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_Update]
	@ID int,
	--@TKMD_ID bigint,
	--@SoTK int,
	--@NgayDK datetime,
	--@MaLoaiHinh varchar(50),
	--@NgaySua datetime,
	--@SoDieuChinh int,
	@TrangThai int
AS

UPDATE
	[dbo].[t_KDT_NoiDungDieuChinhTK]
SET
	--[TKMD_ID] = @TKMD_ID,
	--[SoTK] = @SoTK,
	--[NgayDK] = @NgayDK,
	--[MaLoaiHinh] = @MaLoaiHinh,
	--[NgaySua] = @NgaySua,
	--[SoDieuChinh] = @SoDieuChinh,
	[TrangThai] = @TrangThai
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_InsertUpdate]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 24, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_InsertUpdate]
	@ID bigint,
	@SoLuong nvarchar(max),
	@TrangThai nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT] 
		SET
			[SoLuong] = @SoLuong,
			[TrangThai] = @TrangThai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT]
		(
			[SoLuong],
			[TrangThai]
		)
		VALUES 
		(
			@SoLuong,
			@TrangThai
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_InsertUpdate]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_InsertUpdate]
	@ID bigint,
	@SoCO varchar(100),
	@NgayCO datetime,
	@ToChucCap nvarchar(500),
	@NuocCapCO varchar(3),
	@MaNuocXKTrenCO varchar(3),
	@MaNuocNKTrenCO varchar(3),
	@TenDiaChiNguoiXK nvarchar(500),
	@TenDiaChiNguoiNK nvarchar(500),
	@LoaiCO varchar(50),
	@ThongTinMoTaChiTiet nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@NguoiKy nvarchar(500),
	@NoCo int,
	@ThoiHanNop datetime,
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_CO] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_CO] 
		SET
			[SoCO] = @SoCO,
			[NgayCO] = @NgayCO,
			[ToChucCap] = @ToChucCap,
			[NuocCapCO] = @NuocCapCO,
			[MaNuocXKTrenCO] = @MaNuocXKTrenCO,
			[MaNuocNKTrenCO] = @MaNuocNKTrenCO,
			[TenDiaChiNguoiXK] = @TenDiaChiNguoiXK,
			[TenDiaChiNguoiNK] = @TenDiaChiNguoiNK,
			[LoaiCO] = @LoaiCO,
			[ThongTinMoTaChiTiet] = @ThongTinMoTaChiTiet,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[NguoiKy] = @NguoiKy,
			[NoCo] = @NoCo,
			[ThoiHanNop] = @ThoiHanNop,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_CO]
		(
			[SoCO],
			[NgayCO],
			[ToChucCap],
			[NuocCapCO],
			[MaNuocXKTrenCO],
			[MaNuocNKTrenCO],
			[TenDiaChiNguoiXK],
			[TenDiaChiNguoiNK],
			[LoaiCO],
			[ThongTinMoTaChiTiet],
			[MaDoanhNghiep],
			[NguoiKy],
			[NoCo],
			[ThoiHanNop],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan]
		)
		VALUES 
		(
			@SoCO,
			@NgayCO,
			@ToChucCap,
			@NuocCapCO,
			@MaNuocXKTrenCO,
			@MaNuocNKTrenCO,
			@TenDiaChiNguoiXK,
			@TenDiaChiNguoiNK,
			@LoaiCO,
			@ThongTinMoTaChiTiet,
			@MaDoanhNghiep,
			@NguoiKy,
			@NoCo,
			@ThoiHanNop,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SelectTriGiaHangDPToKhaiXK]'
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_SXXK_SelectTriGiaHangDPToKhaiXK]
	-- Add the parameters for the stored procedure here
	@FromDate DateTime,
	@ToDate DateTime
AS
SELECT Ten,Sum(SoLuong)as TongSoLuongDP,DVT, Sum(TriGiaKB)as TongTriGiaDP,NguyenTe_ID
FROM( 
		SELECT  a.SoToKhai, a.MaLoaiHinh, a.DVT_ID,b.NgayDangKy,b.NuocNK_ID, a.MaHaiQuan,a.TenHang,a.SoLuong, a.TriGiaKB,b.NguyenTe_ID,c.Ten,d.Ten AS DVT 	
		FROM dbo.t_SXXK_HangMauDich a 
		INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
		ON a.SoToKhai = b.SoToKhai
		AND a.MaLoaiHinh = b.MaLoaiHinh
		AND a.NamDangKy = b.NamDangKy 
		AND a.MaHaiQuan = b.MaHaiQuan
		INNER JOIN dbo.t_HaiQuan_Nuoc c
		ON b.NuocNK_ID = c.ID
		INNER JOIN dbo.t_HaiQuan_DonViTinh d
		ON a.DVT_ID = d.ID		
		WHERE a.MaLoaiHinh LIKE 'X%'AND NuocXX_ID ='VN')e /*?????????
*/
		
WHERE NgayDangKy BETWEEN @FromDate AND @ToDate
Group BY Ten,NguyenTe_ID,DVT
/* order BY Ten,TenSP*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_SelectBy_Master_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_SelectBy_Master_ID]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenChungTu],
	[SoBanChinh],
	[SoBanSao],
	[FileUpLoad],
	[Master_ID],
	[STTHang],
	[LoaiCT],
	[NoiDung]
FROM
	[dbo].[t_KDT_SXXK_ChungTuKemTheo]
WHERE
	[Master_ID] = @Master_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_Load]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai]
FROM
	[dbo].[t_KDT_Container]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_NguyenPhuLieu_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_Delete]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Delete]
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@MaHaiQuan char(6)
AS
DELETE FROM 
	[dbo].[t_SXXK_NguyenPhuLieu]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma
	AND [MaHaiQuan] = @MaHaiQuan

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiCO_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiCO_Delete]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiCO_Delete]
	@Ma varchar(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_LoaiCO]
WHERE
	[Ma] = @Ma

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Delete]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 24, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Delete]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_CO]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_NoiDungDieuChinhTKDetail]'
GO
CREATE TABLE [dbo].[t_KDT_NoiDungDieuChinhTKDetail]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[NoiDungTKChinh] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiDungTKSua] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Id_DieuChinh] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_NoiDungDieuChinhTKDetail] on [dbo].[t_KDT_NoiDungDieuChinhTKDetail]'
GO
ALTER TABLE [dbo].[t_KDT_NoiDungDieuChinhTKDetail] ADD CONSTRAINT [PK_t_KDT_NoiDungDieuChinhTKDetail] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Delete]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_SelectAll]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SO_CT],
	[NGAY_CT],
	[MA_LOAI_CT],
	[DIENGIAI],
	[LoaiKB],
	[TrangThaiXuLy],
	[Tempt],
	[MessageID],
	[GUIDSTR],
	[KDT_WAITING],
	[KDT_LASTINFO],
	[SOTN],
	[NGAYTN],
	[TotalSize],
	[Phanluong],
	[HuongDan],
	[TKMDID]
FROM
	[dbo].[t_KDT_ChungTuKem]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_ROLE_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_ROLE_Delete]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_ROLE_Delete]
	@GROUP_ID bigint,
	@ID_ROLE bigint
AS
DELETE FROM 
	[dbo].[GROUP_ROLE]
WHERE
	[GROUP_ID] = @GROUP_ID
	AND [ID_ROLE] = @ID_ROLE

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteBy_Id_DieuChinh]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteBy_Id_DieuChinh]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: 01 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteBy_Id_DieuChinh]
	@Id_DieuChinh int
AS

DELETE FROM [dbo].[t_KDT_NoiDungDieuChinhTKDetail]
WHERE
	[Id_DieuChinh] = @Id_DieuChinh



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_ToKhaiMauDich_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_Update]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_Update]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint,
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(100),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@Xuat_NPL_SP char(1),
	@ThanhLy char(1),
	@NGAY_THN_THX datetime,
	@MaDonViUT varchar(14),
	@TrangThaiThanhKhoan char(1),
	@ChungTu nvarchar(40),
	@PhanLuong varchar(2),
	@NgayHoanThanh datetime,
	@TrangThai smallint,
	@TrongLuongNet float,
	@SoTienKhoan money
AS
UPDATE
	[dbo].[t_SXXK_ToKhaiMauDich]
SET
	[NgayDangKy] = @NgayDangKy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
	[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
	[TenDonViDoiTac] = @TenDonViDoiTac,
	[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHanHopDong] = @NgayHetHanHopDong,
	[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
	[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
	[PTVT_ID] = @PTVT_ID,
	[SoHieuPTVT] = @SoHieuPTVT,
	[NgayDenPTVT] = @NgayDenPTVT,
	[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
	[LoaiVanDon] = @LoaiVanDon,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[NuocXK_ID] = @NuocXK_ID,
	[NuocNK_ID] = @NuocNK_ID,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[CuaKhau_ID] = @CuaKhau_ID,
	[DKGH_ID] = @DKGH_ID,
	[NguyenTe_ID] = @NguyenTe_ID,
	[TyGiaTinhThue] = @TyGiaTinhThue,
	[TyGiaUSD] = @TyGiaUSD,
	[PTTT_ID] = @PTTT_ID,
	[SoHang] = @SoHang,
	[SoLuongPLTK] = @SoLuongPLTK,
	[TenChuHang] = @TenChuHang,
	[SoContainer20] = @SoContainer20,
	[SoContainer40] = @SoContainer40,
	[SoKien] = @SoKien,
	[TrongLuong] = @TrongLuong,
	[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
	[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
	[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
	[LePhiHaiQuan] = @LePhiHaiQuan,
	[PhiBaoHiem] = @PhiBaoHiem,
	[PhiVanChuyen] = @PhiVanChuyen,
	[PhiXepDoHang] = @PhiXepDoHang,
	[PhiKhac] = @PhiKhac,
	[Xuat_NPL_SP] = @Xuat_NPL_SP,
	[ThanhLy] = @ThanhLy,
	[NGAY_THN_THX] = @NGAY_THN_THX,
	[MaDonViUT] = @MaDonViUT,
	[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
	[ChungTu] = @ChungTu,
	[PhanLuong] = @PhanLuong,
	[NgayHoanThanh] = @NgayHoanThanh,
	[TrangThai] = @TrangThai,
	[TrongLuongNet] = @TrongLuongNet,
	[SoTienKhoan] = @SoTienKhoan
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [NamDangKy] = @NamDangKy


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_DeleteBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_CO]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_Insert]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_Insert]
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@MaHaiQuan char(6),
	@NamDangKy smallint,
	@SoThuTuHang smallint,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue float
AS
INSERT INTO [dbo].[t_SXXK_HangMauDich]
(
	[SoToKhai],
	[MaLoaiHinh],
	[MaHaiQuan],
	[NamDangKy],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue]
)
VALUES
(
	@SoToKhai,
	@MaLoaiHinh,
	@MaHaiQuan,
	@NamDangKy,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaKB,
	@TriGiaTT,
	@TriGiaKB_VND,
	@ThueSuatXNK,
	@ThueSuatTTDB,
	@ThueSuatGTGT,
	@ThueXNK,
	@ThueTTDB,
	@ThueGTGT,
	@PhuThu,
	@TyLeThuKhac,
	@TriGiaThuKhac,
	@MienThue
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_SelectAll]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM
	[dbo].[t_KDT_HangGiayPhepDetail]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuanPhongKhai_CauHinh_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_Load]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuanPhongKhai_CauHinh_Load]
	@MaDoanhNghiep char(15),
	@Key_Config varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
FROM
	[dbo].[t_HaiQuanPhongKhai_CauHinh]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Key_Config] = @Key_Config

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Load]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 24, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoLuong],
	[TrangThai]
FROM
	[dbo].[t_KDT]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_SelectBy_VanDon_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_SelectBy_VanDon_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_SelectBy_VanDon_ID]
	@VanDon_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai]
FROM
	[dbo].[t_KDT_Container]
WHERE
	[VanDon_ID] = @VanDon_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_SelectBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_SelectBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaToKhaiTriGia],
	[LyDo],
	[TenHang],
	[STTHang],
	[NgayXuat],
	[STTHangTT],
	[TenHangTT],
	[NgayXuatTT],
	[SoTKHangTT],
	[NgayDangKyHangTT],
	[MaLoaiHinhHangTT],
	[MaHaiQuanHangTT],
	[TriGiaNguyenTeHangTT],
	[DieuChinhCongThuongMai],
	[DieuChinhCongSoLuong],
	[DieuChinhCongKhoanGiamGiaKhac],
	[DieuChinhCongChiPhiVanTai],
	[DieuChinhCongChiPhiBaoHiem],
	[TriGiaNguyenTeHang],
	[TriGiaTTHang],
	[ToSo],
	[DieuChinhTruCapDoThuongMai],
	[DieuChinhTruSoLuong],
	[DieuChinhTruKhoanGiamGiaKhac],
	[DieuChinhTruChiPhiVanTai],
	[DieuChinhTruChiPhiBaoHiem],
	[GiaiTrinh]
FROM
	[dbo].[t_KDT_KD_ToKhaiTriGiaPP23]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuanPhongKhai_CauHinh_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_SelectAll]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuanPhongKhai_CauHinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
FROM
	[dbo].[t_HaiQuanPhongKhai_CauHinh]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GiayPhep]'
GO
CREATE TABLE [dbo].[t_KDT_GiayPhep]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoGiayPhep] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayGiayPhep] [datetime] NOT NULL,
[NgayHetHan] [datetime] NOT NULL,
[NguoiCap] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiCap] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDonViDuocCap] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonViDuocCap] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaCoQuanCap] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenQuanCap] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThongTinKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDoanhNghiep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TKMD_ID] [bigint] NULL,
[GuidStr] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiKB] [int] NULL,
[SoTiepNhan] [bigint] NULL,
[NgayTiepNhan] [datetime] NULL,
[TrangThai] [int] NULL,
[NamTiepNhan] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_GiayPhep] on [dbo].[t_KDT_GiayPhep]'
GO
ALTER TABLE [dbo].[t_KDT_GiayPhep] ADD CONSTRAINT [PK_t_KDT_GiayPhep] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Insert]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_Insert]
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_GiayPhep]
(
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
)
VALUES 
(
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHan,
	@NguoiCap,
	@NoiCap,
	@MaDonViDuocCap,
	@TenDonViDuocCap,
	@MaCoQuanCap,
	@TenQuanCap,
	@ThongTinKhac,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Message_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_Insert]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Message_Insert]
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@MessageFrom varchar(50),
	@MessageTo varchar(50),
	@MessageType int,
	@MessageFunction int,
	@MessageContent xml,
	@CreatedTime datetime,
	@TieuDeThongBao nvarchar(100),
	@NoiDungThongBao nvarchar(max),
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_Messages]
(
	[ReferenceID],
	[ItemID],
	[MessageFrom],
	[MessageTo],
	[MessageType],
	[MessageFunction],
	[MessageContent],
	[CreatedTime],
	[TieuDeThongBao],
	[NoiDungThongBao]
)
VALUES 
(
	@ReferenceID,
	@ItemID,
	@MessageFrom,
	@MessageTo,
	@MessageType,
	@MessageFunction,
	@MessageContent,
	@CreatedTime,
	@TieuDeThongBao,
	@NoiDungThongBao
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_Update]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_Update]
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@MaHaiQuan char(6),
	@NamDangKy smallint,
	@SoThuTuHang smallint,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue float
AS
UPDATE
	[dbo].[t_SXXK_HangMauDich]
SET
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaKB] = @TriGiaKB,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaKB_VND] = @TriGiaKB_VND,
	[ThueSuatXNK] = @ThueSuatXNK,
	[ThueSuatTTDB] = @ThueSuatTTDB,
	[ThueSuatGTGT] = @ThueSuatGTGT,
	[ThueXNK] = @ThueXNK,
	[ThueTTDB] = @ThueTTDB,
	[ThueGTGT] = @ThueGTGT,
	[PhuThu] = @PhuThu,
	[TyLeThuKhac] = @TyLeThuKhac,
	[TriGiaThuKhac] = @TriGiaThuKhac,
	[MienThue] = @MienThue
WHERE
	[SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [MaHaiQuan] = @MaHaiQuan
	AND [NamDangKy] = @NamDangKy
	AND [SoThuTuHang] = @SoThuTuHang

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SanPham_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_Update]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_Update]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@Ten nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(3)
AS
UPDATE
	[dbo].[t_SXXK_SanPham]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma
	AND [MaHaiQuan] = @MaHaiQuan

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]
	@NoiDungTKChinh nvarchar(max),
	@NoiDungTKSua nvarchar(max),
	@Id_DieuChinh int,
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_NoiDungDieuChinhTKDetail]
(
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh]
)
VALUES 
(
	@NoiDungTKChinh,
	@NoiDungTKSua,
	@Id_DieuChinh
)

SET @ID = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_Insert]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_Insert]
	@SoVanDon varchar(50),
	@NgayVanDon datetime,
	@HangRoi bit,
	@SoHieuPTVT varchar(50),
	@NgayDenPTVT datetime,
	@MaHangVT varchar(50),
	@TenHangVT nvarchar(100),
	@TenPTVT nvarchar(100),
	@QuocTichPTVT varchar(3),
	@NuocXuat_ID varchar(3),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang nvarchar(100),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang nvarchar(100),
	@CuaKhauNhap_ID varchar(4),
	@CuaKhauXuat nvarchar(100),
	@MaNguoiNhanHangTrungGian varchar(50),
	@TenNguoiNhanHangTrungGian nvarchar(100),
	@MaCangXepHang varchar(50),
	@TenCangXepHang varchar(100),
	@MaCangDoHang varchar(50),
	@TenCangDoHang varchar(100),
	@TKMD_ID bigint,
	@DKGH_ID varchar(7),
	@DiaDiemGiaoHang nvarchar(100),
	@NoiDi nvarchar(500),
	@SoHieuChuyenDi nvarchar(500),
	@NgayKhoiHanh datetime,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_VanDon]
(
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh]
)
VALUES 
(
	@SoVanDon,
	@NgayVanDon,
	@HangRoi,
	@SoHieuPTVT,
	@NgayDenPTVT,
	@MaHangVT,
	@TenHangVT,
	@TenPTVT,
	@QuocTichPTVT,
	@NuocXuat_ID,
	@MaNguoiNhanHang,
	@TenNguoiNhanHang,
	@MaNguoiGiaoHang,
	@TenNguoiGiaoHang,
	@CuaKhauNhap_ID,
	@CuaKhauXuat,
	@MaNguoiNhanHangTrungGian,
	@TenNguoiNhanHangTrungGian,
	@MaCangXepHang,
	@TenCangXepHang,
	@MaCangDoHang,
	@TenCangDoHang,
	@TKMD_ID,
	@DKGH_ID,
	@DiaDiemGiaoHang,
	@NoiDi,
	@SoHieuChuyenDi,
	@NgayKhoiHanh
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Insert]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_Insert]
	@TKMD_ID bigint,
	@MaToKhaiTriGia int,
	@LyDo nvarchar(50),
	@TenHang nvarchar(80),
	@STTHang int,
	@NgayXuat datetime,
	@STTHangTT int,
	@TenHangTT nvarchar(80),
	@NgayXuatTT datetime,
	@SoTKHangTT bigint,
	@NgayDangKyHangTT datetime,
	@MaLoaiHinhHangTT varchar(50),
	@MaHaiQuanHangTT varchar(50),
	@TriGiaNguyenTeHangTT float,
	@DieuChinhCongThuongMai float,
	@DieuChinhCongSoLuong float,
	@DieuChinhCongKhoanGiamGiaKhac float,
	@DieuChinhCongChiPhiVanTai float,
	@DieuChinhCongChiPhiBaoHiem float,
	@TriGiaNguyenTeHang float,
	@TriGiaTTHang float,
	@ToSo int,
	@DieuChinhTruCapDoThuongMai float,
	@DieuChinhTruSoLuong float,
	@DieuChinhTruKhoanGiamGiaKhac float,
	@DieuChinhTruChiPhiVanTai float,
	@DieuChinhTruChiPhiBaoHiem float,
	@GiaiTrinh nvarchar(250),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_KD_ToKhaiTriGiaPP23]
(
	[TKMD_ID],
	[MaToKhaiTriGia],
	[LyDo],
	[TenHang],
	[STTHang],
	[NgayXuat],
	[STTHangTT],
	[TenHangTT],
	[NgayXuatTT],
	[SoTKHangTT],
	[NgayDangKyHangTT],
	[MaLoaiHinhHangTT],
	[MaHaiQuanHangTT],
	[TriGiaNguyenTeHangTT],
	[DieuChinhCongThuongMai],
	[DieuChinhCongSoLuong],
	[DieuChinhCongKhoanGiamGiaKhac],
	[DieuChinhCongChiPhiVanTai],
	[DieuChinhCongChiPhiBaoHiem],
	[TriGiaNguyenTeHang],
	[TriGiaTTHang],
	[ToSo],
	[DieuChinhTruCapDoThuongMai],
	[DieuChinhTruSoLuong],
	[DieuChinhTruKhoanGiamGiaKhac],
	[DieuChinhTruChiPhiVanTai],
	[DieuChinhTruChiPhiBaoHiem],
	[GiaiTrinh]
)
VALUES 
(
	@TKMD_ID,
	@MaToKhaiTriGia,
	@LyDo,
	@TenHang,
	@STTHang,
	@NgayXuat,
	@STTHangTT,
	@TenHangTT,
	@NgayXuatTT,
	@SoTKHangTT,
	@NgayDangKyHangTT,
	@MaLoaiHinhHangTT,
	@MaHaiQuanHangTT,
	@TriGiaNguyenTeHangTT,
	@DieuChinhCongThuongMai,
	@DieuChinhCongSoLuong,
	@DieuChinhCongKhoanGiamGiaKhac,
	@DieuChinhCongChiPhiVanTai,
	@DieuChinhCongChiPhiBaoHiem,
	@TriGiaNguyenTeHang,
	@TriGiaTTHang,
	@ToSo,
	@DieuChinhTruCapDoThuongMai,
	@DieuChinhTruSoLuong,
	@DieuChinhTruKhoanGiamGiaKhac,
	@DieuChinhTruChiPhiVanTai,
	@DieuChinhTruChiPhiBaoHiem,
	@GiaiTrinh
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Update]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_Update]
	@ID bigint,
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int
AS

UPDATE
	[dbo].[t_KDT_GiayPhep]
SET
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHan] = @NgayHetHan,
	[NguoiCap] = @NguoiCap,
	[NoiCap] = @NoiCap,
	[MaDonViDuocCap] = @MaDonViDuocCap,
	[TenDonViDuocCap] = @TenDonViDuocCap,
	[MaCoQuanCap] = @MaCoQuanCap,
	[TenQuanCap] = @TenQuanCap,
	[ThongTinKhac] = @ThongTinKhac,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SelectAll]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 24, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoLuong],
	[TrangThai]
FROM
	[dbo].[t_KDT]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate]
	@ID int,
	@NoiDungTKChinh nvarchar(max),
	@NoiDungTKSua nvarchar(max),
	@Id_DieuChinh int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_NoiDungDieuChinhTKDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_NoiDungDieuChinhTKDetail] 
		SET
			[NoiDungTKChinh] = @NoiDungTKChinh,
			[NoiDungTKSua] = @NoiDungTKSua,
			[Id_DieuChinh] = @Id_DieuChinh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_NoiDungDieuChinhTKDetail]
		(
			[NoiDungTKChinh],
			[NoiDungTKSua],
			[Id_DieuChinh]
		)
		VALUES 
		(
			@NoiDungTKChinh,
			@NoiDungTKSua,
			@Id_DieuChinh
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_InsertUpdate]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_InsertUpdate]
	@ID bigint,
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GiayPhep] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GiayPhep] 
		SET
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHan] = @NgayHetHan,
			[NguoiCap] = @NguoiCap,
			[NoiCap] = @NoiCap,
			[MaDonViDuocCap] = @MaDonViDuocCap,
			[TenDonViDuocCap] = @TenDonViDuocCap,
			[MaCoQuanCap] = @MaCoQuanCap,
			[TenQuanCap] = @TenQuanCap,
			[ThongTinKhac] = @ThongTinKhac,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GiayPhep]
		(
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHan],
			[NguoiCap],
			[NoiCap],
			[MaDonViDuocCap],
			[TenDonViDuocCap],
			[MaCoQuanCap],
			[TenQuanCap],
			[ThongTinKhac],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan]
		)
		VALUES 
		(
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHan,
			@NguoiCap,
			@NoiCap,
			@MaDonViDuocCap,
			@TenDonViDuocCap,
			@MaCoQuanCap,
			@TenQuanCap,
			@ThongTinKhac,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Load]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_Insert]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_Insert]
	@ChungTuKemID bigint,
	@FileName varchar(255),
	@FileSize numeric(18, 0),
	@NoiDung image,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_ChungTuKemChiTiet]
(
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
)
VALUES 
(
	@ChungTuKemID,
	@FileName,
	@FileSize,
	@NoiDung
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Delete]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GiayPhep]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_Update]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_Update]
	@ID bigint,
	@SoVanDon varchar(50),
	@NgayVanDon datetime,
	@HangRoi bit,
	@SoHieuPTVT varchar(50),
	@NgayDenPTVT datetime,
	@MaHangVT varchar(50),
	@TenHangVT nvarchar(100),
	@TenPTVT nvarchar(100),
	@QuocTichPTVT varchar(3),
	@NuocXuat_ID varchar(3),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang nvarchar(100),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang nvarchar(100),
	@CuaKhauNhap_ID varchar(4),
	@CuaKhauXuat nvarchar(100),
	@MaNguoiNhanHangTrungGian varchar(50),
	@TenNguoiNhanHangTrungGian nvarchar(100),
	@MaCangXepHang varchar(50),
	@TenCangXepHang varchar(100),
	@MaCangDoHang varchar(50),
	@TenCangDoHang varchar(100),
	@TKMD_ID bigint,
	@DKGH_ID varchar(7),
	@DiaDiemGiaoHang nvarchar(100),
	@NoiDi nvarchar(500),
	@SoHieuChuyenDi nvarchar(500),
	@NgayKhoiHanh datetime
AS

UPDATE
	[dbo].[t_KDT_VanDon]
SET
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[HangRoi] = @HangRoi,
	[SoHieuPTVT] = @SoHieuPTVT,
	[NgayDenPTVT] = @NgayDenPTVT,
	[MaHangVT] = @MaHangVT,
	[TenHangVT] = @TenHangVT,
	[TenPTVT] = @TenPTVT,
	[QuocTichPTVT] = @QuocTichPTVT,
	[NuocXuat_ID] = @NuocXuat_ID,
	[MaNguoiNhanHang] = @MaNguoiNhanHang,
	[TenNguoiNhanHang] = @TenNguoiNhanHang,
	[MaNguoiGiaoHang] = @MaNguoiGiaoHang,
	[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
	[CuaKhauNhap_ID] = @CuaKhauNhap_ID,
	[CuaKhauXuat] = @CuaKhauXuat,
	[MaNguoiNhanHangTrungGian] = @MaNguoiNhanHangTrungGian,
	[TenNguoiNhanHangTrungGian] = @TenNguoiNhanHangTrungGian,
	[MaCangXepHang] = @MaCangXepHang,
	[TenCangXepHang] = @TenCangXepHang,
	[MaCangDoHang] = @MaCangDoHang,
	[TenCangDoHang] = @TenCangDoHang,
	[TKMD_ID] = @TKMD_ID,
	[DKGH_ID] = @DKGH_ID,
	[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
	[NoiDi] = @NoiDi,
	[SoHieuChuyenDi] = @SoHieuChuyenDi,
	[NgayKhoiHanh] = @NgayKhoiHanh
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_Load]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_Load]
	@MA_NHOM bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MA_NHOM],
	[TEN_NHOM],
	[MO_TA]
FROM
	[dbo].[GROUPS]
WHERE
	[MA_NHOM] = @MA_NHOM

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_Update]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_Update]
	@ID bigint,
	@ChungTuKemID bigint,
	@FileName varchar(255),
	@FileSize numeric(18, 0),
	@NoiDung image
AS

UPDATE
	[dbo].[t_KDT_ChungTuKemChiTiet]
SET
	[ChungTuKemID] = @ChungTuKemID,
	[FileName] = @FileName,
	[FileSize] = @FileSize,
	[NoiDung] = @NoiDung
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SanPham_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_Delete]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_Delete]
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@MaHaiQuan char(6)
AS
DELETE FROM 
	[dbo].[t_SXXK_SanPham]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma
	AND [MaHaiQuan] = @MaHaiQuan

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_DeleteBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GiayPhep]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_InsertUpdate]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_InsertUpdate]
	@ID bigint,
	@SoVanDon varchar(50),
	@NgayVanDon datetime,
	@HangRoi bit,
	@SoHieuPTVT varchar(50),
	@NgayDenPTVT datetime,
	@MaHangVT varchar(50),
	@TenHangVT nvarchar(100),
	@TenPTVT nvarchar(100),
	@QuocTichPTVT varchar(3),
	@NuocXuat_ID varchar(3),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang nvarchar(100),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang nvarchar(100),
	@CuaKhauNhap_ID varchar(4),
	@CuaKhauXuat nvarchar(100),
	@MaNguoiNhanHangTrungGian varchar(50),
	@TenNguoiNhanHangTrungGian nvarchar(100),
	@MaCangXepHang varchar(50),
	@TenCangXepHang varchar(100),
	@MaCangDoHang varchar(50),
	@TenCangDoHang varchar(100),
	@TKMD_ID bigint,
	@DKGH_ID varchar(7),
	@DiaDiemGiaoHang nvarchar(100),
	@NoiDi nvarchar(500),
	@SoHieuChuyenDi nvarchar(500),
	@NgayKhoiHanh datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VanDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VanDon] 
		SET
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[HangRoi] = @HangRoi,
			[SoHieuPTVT] = @SoHieuPTVT,
			[NgayDenPTVT] = @NgayDenPTVT,
			[MaHangVT] = @MaHangVT,
			[TenHangVT] = @TenHangVT,
			[TenPTVT] = @TenPTVT,
			[QuocTichPTVT] = @QuocTichPTVT,
			[NuocXuat_ID] = @NuocXuat_ID,
			[MaNguoiNhanHang] = @MaNguoiNhanHang,
			[TenNguoiNhanHang] = @TenNguoiNhanHang,
			[MaNguoiGiaoHang] = @MaNguoiGiaoHang,
			[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
			[CuaKhauNhap_ID] = @CuaKhauNhap_ID,
			[CuaKhauXuat] = @CuaKhauXuat,
			[MaNguoiNhanHangTrungGian] = @MaNguoiNhanHangTrungGian,
			[TenNguoiNhanHangTrungGian] = @TenNguoiNhanHangTrungGian,
			[MaCangXepHang] = @MaCangXepHang,
			[TenCangXepHang] = @TenCangXepHang,
			[MaCangDoHang] = @MaCangDoHang,
			[TenCangDoHang] = @TenCangDoHang,
			[TKMD_ID] = @TKMD_ID,
			[DKGH_ID] = @DKGH_ID,
			[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
			[NoiDi] = @NoiDi,
			[SoHieuChuyenDi] = @SoHieuChuyenDi,
			[NgayKhoiHanh] = @NgayKhoiHanh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VanDon]
		(
			[SoVanDon],
			[NgayVanDon],
			[HangRoi],
			[SoHieuPTVT],
			[NgayDenPTVT],
			[MaHangVT],
			[TenHangVT],
			[TenPTVT],
			[QuocTichPTVT],
			[NuocXuat_ID],
			[MaNguoiNhanHang],
			[TenNguoiNhanHang],
			[MaNguoiGiaoHang],
			[TenNguoiGiaoHang],
			[CuaKhauNhap_ID],
			[CuaKhauXuat],
			[MaNguoiNhanHangTrungGian],
			[TenNguoiNhanHangTrungGian],
			[MaCangXepHang],
			[TenCangXepHang],
			[MaCangDoHang],
			[TenCangDoHang],
			[TKMD_ID],
			[DKGH_ID],
			[DiaDiemGiaoHang],
			[NoiDi],
			[SoHieuChuyenDi],
			[NgayKhoiHanh]
		)
		VALUES 
		(
			@SoVanDon,
			@NgayVanDon,
			@HangRoi,
			@SoHieuPTVT,
			@NgayDenPTVT,
			@MaHangVT,
			@TenHangVT,
			@TenPTVT,
			@QuocTichPTVT,
			@NuocXuat_ID,
			@MaNguoiNhanHang,
			@TenNguoiNhanHang,
			@MaNguoiGiaoHang,
			@TenNguoiGiaoHang,
			@CuaKhauNhap_ID,
			@CuaKhauXuat,
			@MaNguoiNhanHangTrungGian,
			@TenNguoiNhanHangTrungGian,
			@MaCangXepHang,
			@TenCangXepHang,
			@MaCangDoHang,
			@TenCangDoHang,
			@TKMD_ID,
			@DKGH_ID,
			@DiaDiemGiaoHang,
			@NoiDi,
			@SoHieuChuyenDi,
			@NgayKhoiHanh
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_InsertUpdate]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_InsertUpdate]
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@MaHaiQuan char(6),
	@NamDangKy smallint,
	@SoThuTuHang smallint,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue float
AS
IF EXISTS(SELECT [SoToKhai], [MaLoaiHinh], [MaHaiQuan], [NamDangKy], [SoThuTuHang] FROM [dbo].[t_SXXK_HangMauDich] WHERE [SoToKhai] = @SoToKhai AND [MaLoaiHinh] = @MaLoaiHinh AND [MaHaiQuan] = @MaHaiQuan AND [NamDangKy] = @NamDangKy AND [SoThuTuHang] = @SoThuTuHang)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_HangMauDich] 
		SET
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue
		WHERE
			[SoToKhai] = @SoToKhai
			AND [MaLoaiHinh] = @MaLoaiHinh
			AND [MaHaiQuan] = @MaHaiQuan
			AND [NamDangKy] = @NamDangKy
			AND [SoThuTuHang] = @SoThuTuHang
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_HangMauDich]
	(
			[SoToKhai],
			[MaLoaiHinh],
			[MaHaiQuan],
			[NamDangKy],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue]
	)
	VALUES
	(
			@SoToKhai,
			@MaLoaiHinh,
			@MaHaiQuan,
			@NamDangKy,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_InsertUpdate]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_InsertUpdate]
	@ID bigint,
	@ChungTuKemID bigint,
	@FileName varchar(255),
	@FileSize numeric(18, 0),
	@NoiDung image
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ChungTuKemChiTiet] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ChungTuKemChiTiet] 
		SET
			[ChungTuKemID] = @ChungTuKemID,
			[FileName] = @FileName,
			[FileSize] = @FileSize,
			[NoiDung] = @NoiDung
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ChungTuKemChiTiet]
		(
			[ChungTuKemID],
			[FileName],
			[FileSize],
			[NoiDung]
		)
		VALUES 
		(
			@ChungTuKemID,
			@FileName,
			@FileSize,
			@NoiDung
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: 01 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]
	@Id_DieuChinh int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]
WHERE
	[Id_DieuChinh] = @Id_DieuChinh



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SanPham_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_SelectAll]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM
	[dbo].[t_SXXK_SanPham]
ORDER BY Ma


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_Update]'
GO
CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Update]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255)
AS
UPDATE
	[dbo].[t_HaiQuan_MaHS]
SET
	[Nhom] = @Nhom,
	[PN1] = @PN1,
	[PN2] = @PN2,
	[Pn3] = @Pn3,
	[Mo_ta] = @Mo_ta
WHERE
	[HS10SO] = @HS10SO
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_Delete]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VanDon]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Load]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
FROM
	[dbo].[t_KDT_GiayPhep]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_Delete]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ChungTuKemChiTiet]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_Delete]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_Delete]
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@MaHaiQuan char(6),
	@NamDangKy smallint,
	@SoThuTuHang smallint
AS
DELETE FROM 
	[dbo].[t_SXXK_HangMauDich]
WHERE
	[SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [MaHaiQuan] = @MaHaiQuan
	AND [NamDangKy] = @NamDangKy
	AND [SoThuTuHang] = @SoThuTuHang

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_NguyenPhuLieu_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_Insert]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 23, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Insert]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@Ten nvarchar(80),
	@MaHS varchar(10),
	@DVT_ID char(3)
AS
INSERT INTO [dbo].[t_SXXK_NguyenPhuLieu]
(
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
)
VALUES
(
	@MaHaiQuan,
	@MaDoanhNghiep,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Update]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Update]
	@ID int,
	@NoiDungTKChinh nvarchar(max),
	@NoiDungTKSua nvarchar(max),
	@Id_DieuChinh int
AS

UPDATE
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]
SET
	[NoiDungTKChinh] = @NoiDungTKChinh,
	[NoiDungTKSua] = @NoiDungTKSua,
	[Id_DieuChinh] = @Id_DieuChinh
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_SelectAll]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaToKhaiTriGia],
	[LyDo],
	[TenHang],
	[STTHang],
	[NgayXuat],
	[STTHangTT],
	[TenHangTT],
	[NgayXuatTT],
	[SoTKHangTT],
	[NgayDangKyHangTT],
	[MaLoaiHinhHangTT],
	[MaHaiQuanHangTT],
	[TriGiaNguyenTeHangTT],
	[DieuChinhCongThuongMai],
	[DieuChinhCongSoLuong],
	[DieuChinhCongKhoanGiamGiaKhac],
	[DieuChinhCongChiPhiVanTai],
	[DieuChinhCongChiPhiBaoHiem],
	[TriGiaNguyenTeHang],
	[TriGiaTTHang],
	[ToSo],
	[DieuChinhTruCapDoThuongMai],
	[DieuChinhTruSoLuong],
	[DieuChinhTruKhoanGiamGiaKhac],
	[DieuChinhTruChiPhiVanTai],
	[DieuChinhTruChiPhiBaoHiem],
	[GiaiTrinh]
FROM
	[dbo].[t_KDT_KD_ToKhaiTriGiaPP23]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
FROM
	[dbo].[t_KDT_GiayPhep]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VanDon]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SelectTriGiaHangToKhaiXK]'
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_SXXK_SelectTriGiaHangToKhaiXK]
	-- Add the parameters for the stored procedure here
	@FromDate DateTime,
	@ToDate DateTime
AS
SELECT Ten,Sum(SoLuong)as TongSoLuong,DVT, Sum(TriGiaKB)as TongTriGia,NguyenTe_ID
FROM( 
		SELECT  a.SoToKhai, a.MaLoaiHinh, a.DVT_ID,b.NgayDangKy,b.NuocNK_ID, a.MaHaiQuan,a.TenHang,a.SoLuong, a.TriGiaKB,b.NguyenTe_ID,c.Ten,d.Ten AS DVT 	
		FROM dbo.t_SXXK_HangMauDich a 
		INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
		ON a.SoToKhai = b.SoToKhai
		AND a.MaLoaiHinh = b.MaLoaiHinh
		AND a.NamDangKy = b.NamDangKy 
		AND a.MaHaiQuan = b.MaHaiQuan
		INNER JOIN dbo.t_HaiQuan_Nuoc c
		ON b.NuocNK_ID = c.ID
		INNER JOIN dbo.t_HaiQuan_DonViTinh d
		ON a.DVT_ID = d.ID		
		WHERE a.MaLoaiHinh LIKE 'X%')e /*?????????*/		
WHERE NgayDangKy BETWEEN @FromDate AND @ToDate
Group BY Ten,NguyenTe_ID,DVT
/* order BY Ten,TenSP*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_DeleteBy_ChungTuKemID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_DeleteBy_ChungTuKemID]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_DeleteBy_ChungTuKemID]
	@ChungTuKemID bigint
AS

DELETE FROM [dbo].[t_KDT_ChungTuKemChiTiet]
WHERE
	[ChungTuKemID] = @ChungTuKemID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_ToKhaiMauDich_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_Delete]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_Delete]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint
AS
DELETE FROM 
	[dbo].[t_SXXK_ToKhaiMauDich]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [NamDangKy] = @NamDangKy

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_DeleteBy_MaHaiQuan_And_SoToKhai_And_MaLoaiHinh_And_NamDangKy]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_DeleteBy_MaHaiQuan_And_SoToKhai_And_MaLoaiHinh_And_NamDangKy]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_DeleteBy_MaHaiQuan_And_SoToKhai_And_MaLoaiHinh_And_NamDangKy]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint
AS

SET NOCOUNT ON

DELETE FROM [dbo].[t_SXXK_HangMauDich]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [NamDangKy] = @NamDangKy

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectAll]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
FROM
	[dbo].[t_KDT_GiayPhep]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_USER_GROUP_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_USER_GROUP_SelectAll]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_USER_GROUP_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MA_NHOM],
	[USER_ID]
FROM
	[dbo].[USER_GROUP]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_ToKhaiMauDich_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_SelectAll]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NamDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[Xuat_NPL_SP],
	[ThanhLy],
	[NGAY_THN_THX],
	[MaDonViUT],
	[TrangThaiThanhKhoan],
	[ChungTu],
	[PhanLuong],
	[NgayHoanThanh],
	[TrangThai],
	[TrongLuongNet],
	[SoTienKhoan]
FROM
	[dbo].[t_SXXK_ToKhaiMauDich]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_Load]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh]
FROM
	[dbo].[t_KDT_VanDon]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[view_HangXuatKhau]'
GO
CREATE VIEW dbo.view_HangXuatKhau
AS
SELECT     dbo.t_HaiQuan_Nuoc.Ten, SUM(dbo.t_SXXK_HangMauDich.SoLuong) AS TongSoLuong, SUM(dbo.t_SXXK_HangMauDich.TriGiaKB) AS TongTriGia, 
                      dbo.t_SXXK_ToKhaiMauDich.NguyenTe_ID
FROM         dbo.t_SXXK_ToKhaiMauDich INNER JOIN
                      dbo.t_SXXK_HangMauDich ON dbo.t_SXXK_ToKhaiMauDich.MaHaiQuan = dbo.t_SXXK_HangMauDich.MaHaiQuan AND 
                      dbo.t_SXXK_ToKhaiMauDich.SoToKhai = dbo.t_SXXK_HangMauDich.SoToKhai AND 
                      dbo.t_SXXK_ToKhaiMauDich.MaLoaiHinh = dbo.t_SXXK_HangMauDich.MaLoaiHinh AND 
                      dbo.t_SXXK_ToKhaiMauDich.NamDangKy = dbo.t_SXXK_HangMauDich.NamDangKy INNER JOIN
                      dbo.t_HaiQuan_Nuoc ON dbo.t_SXXK_ToKhaiMauDich.NuocNK_ID = dbo.t_HaiQuan_Nuoc.ID
WHERE     (dbo.t_SXXK_HangMauDich.MaLoaiHinh LIKE 'X%')
GROUP BY dbo.t_HaiQuan_Nuoc.Ten, dbo.t_SXXK_ToKhaiMauDich.NguyenTe_ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_Load]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM
	[dbo].[t_KDT_ChungTuKemChiTiet]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_DeleteBy_Master_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_DeleteBy_Master_ID]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_DeleteBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON

DELETE FROM [dbo].[t_KDT_SXXK_ChungTuKemTheo]
WHERE
	[Master_ID] = @Master_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_SelectAll]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenChungTu],
	[SoBanChinh],
	[SoBanSao],
	[FileUpLoad],
	[Master_ID],
	[STTHang],
	[LoaiCT],
	[NoiDung]
FROM
	[dbo].[t_KDT_SXXK_ChungTuKemTheo]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_Insert]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_Insert]
	@TKMD_ID bigint,
	@ToSo bigint,
	@LoaiToKhaiTriGia nvarchar(50),
	@NgayXuatKhau datetime,
	@CapDoThuongMai char(10),
	@QuyenSuDung bit,
	@KhongXacDinh bit,
	@TraThem bit,
	@TienTra bit,
	@CoQuanHeDacBiet bit,
	@AnhHuongQuanHe bit,
	@GhiChep nvarchar(240),
	@NgayKhaiBao datetime,
	@NguoiKhaiBao nvarchar(30),
	@ChucDanhNguoiKhaiBao nvarchar(100),
	@NgayTruyen datetime,
	@KieuQuanHe nvarchar(256),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_SXXK_ToKhaiTriGia]
(
	[TKMD_ID],
	[ToSo],
	[LoaiToKhaiTriGia],
	[NgayXuatKhau],
	[CapDoThuongMai],
	[QuyenSuDung],
	[KhongXacDinh],
	[TraThem],
	[TienTra],
	[CoQuanHeDacBiet],
	[AnhHuongQuanHe],
	[GhiChep],
	[NgayKhaiBao],
	[NguoiKhaiBao],
	[ChucDanhNguoiKhaiBao],
	[NgayTruyen],
	[KieuQuanHe]
)
VALUES 
(
	@TKMD_ID,
	@ToSo,
	@LoaiToKhaiTriGia,
	@NgayXuatKhau,
	@CapDoThuongMai,
	@QuyenSuDung,
	@KhongXacDinh,
	@TraThem,
	@TienTra,
	@CoQuanHeDacBiet,
	@AnhHuongQuanHe,
	@GhiChep,
	@NgayKhaiBao,
	@NguoiKhaiBao,
	@ChucDanhNguoiKhaiBao,
	@NgayTruyen,
	@KieuQuanHe
)

SET @ID = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh]
FROM
	[dbo].[t_KDT_VanDon]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_Update]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_Update]
	@MA_NHOM bigint,
	@TEN_NHOM nvarchar(1000),
	@MO_TA nvarchar(1000)
AS
UPDATE
	[dbo].[GROUPS]
SET
	[TEN_NHOM] = @TEN_NHOM,
	[MO_TA] = @MO_TA
WHERE
	[MA_NHOM] = @MA_NHOM

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_SelectBy_ChungTuKemID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_SelectBy_ChungTuKemID]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_SelectBy_ChungTuKemID]
	@ChungTuKemID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM
	[dbo].[t_KDT_ChungTuKemChiTiet]
WHERE
	[ChungTuKemID] = @ChungTuKemID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_Update]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@ToSo bigint,
	@LoaiToKhaiTriGia nvarchar(50),
	@NgayXuatKhau datetime,
	@CapDoThuongMai char(10),
	@QuyenSuDung bit,
	@KhongXacDinh bit,
	@TraThem bit,
	@TienTra bit,
	@CoQuanHeDacBiet bit,
	@AnhHuongQuanHe bit,
	@GhiChep nvarchar(240),
	@NgayKhaiBao datetime,
	@NguoiKhaiBao nvarchar(30),
	@ChucDanhNguoiKhaiBao nvarchar(100),
	@NgayTruyen datetime,
	@KieuQuanHe nvarchar(256)
AS
UPDATE
	[dbo].[t_KDT_SXXK_ToKhaiTriGia]
SET
	[TKMD_ID] = @TKMD_ID,
	[ToSo] = @ToSo,
	[LoaiToKhaiTriGia] = @LoaiToKhaiTriGia,
	[NgayXuatKhau] = @NgayXuatKhau,
	[CapDoThuongMai] = @CapDoThuongMai,
	[QuyenSuDung] = @QuyenSuDung,
	[KhongXacDinh] = @KhongXacDinh,
	[TraThem] = @TraThem,
	[TienTra] = @TienTra,
	[CoQuanHeDacBiet] = @CoQuanHeDacBiet,
	[AnhHuongQuanHe] = @AnhHuongQuanHe,
	[GhiChep] = @GhiChep,
	[NgayKhaiBao] = @NgayKhaiBao,
	[NguoiKhaiBao] = @NguoiKhaiBao,
	[ChucDanhNguoiKhaiBao] = @ChucDanhNguoiKhaiBao,
	[NgayTruyen] = @NgayTruyen,
	[KieuQuanHe] = @KieuQuanHe
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_InsertUpdate]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_InsertUpdate]
	@MA_NHOM bigint,
	@TEN_NHOM nvarchar(1000),
	@MO_TA nvarchar(1000)
AS
IF EXISTS(SELECT [MA_NHOM] FROM [dbo].[GROUPS] WHERE [MA_NHOM] = @MA_NHOM)
	BEGIN
		UPDATE
			[dbo].[GROUPS] 
		SET
			[TEN_NHOM] = @TEN_NHOM,
			[MO_TA] = @MO_TA
		WHERE
			[MA_NHOM] = @MA_NHOM
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[GROUPS]
		(
			[TEN_NHOM],
			[MO_TA]
		)
		VALUES 
		(
			@TEN_NHOM,
			@MO_TA
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[view_HangDPXuatKhau]'
GO
CREATE VIEW dbo.view_HangDPXuatKhau
AS
SELECT     SUM(dbo.t_SXXK_HangMauDich.SoLuong) AS TongSoLuongDP, SUM(dbo.t_SXXK_HangMauDich.TriGiaKB) AS TongTriGiaDP, 
                      dbo.t_HaiQuan_Nuoc.Ten, dbo.t_SXXK_ToKhaiMauDich.NgayDangKy, dbo.t_SXXK_HangMauDich.TenHang
FROM         dbo.t_SXXK_HangMauDich INNER JOIN
                      dbo.t_SXXK_ToKhaiMauDich ON dbo.t_SXXK_HangMauDich.MaHaiQuan = dbo.t_SXXK_ToKhaiMauDich.MaHaiQuan AND 
                      dbo.t_SXXK_HangMauDich.SoToKhai = dbo.t_SXXK_ToKhaiMauDich.SoToKhai AND 
                      dbo.t_SXXK_HangMauDich.MaLoaiHinh = dbo.t_SXXK_ToKhaiMauDich.MaLoaiHinh AND 
                      dbo.t_SXXK_HangMauDich.NamDangKy = dbo.t_SXXK_ToKhaiMauDich.NamDangKy INNER JOIN
                      dbo.t_HaiQuan_Nuoc ON dbo.t_SXXK_ToKhaiMauDich.NuocNK_ID = dbo.t_HaiQuan_Nuoc.ID
WHERE     (dbo.t_SXXK_HangMauDich.MaLoaiHinh LIKE 'X%') AND (dbo.t_SXXK_ToKhaiMauDich.NuocNK_ID = 'JP')
GROUP BY dbo.t_HaiQuan_Nuoc.Ten, dbo.t_SXXK_ToKhaiMauDich.NgayDangKy, dbo.t_SXXK_HangMauDich.TenHang
HAVING      (dbo.t_SXXK_ToKhaiMauDich.NgayDangKy BETWEEN CONVERT(DATETIME, '2008-12-08 00:00:00', 102) AND CONVERT(DATETIME, 
                      '2008-12-15 00:00:00', 102))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SelectTriGiaHang/NuocToKhaiXK]'
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_SXXK_SelectTriGiaHang/NuocToKhaiXK]
	-- Add the parameters for the stored procedure here
	@FromDate DateTime,
	@ToDate DateTime
AS
SELECT Ten,TenSP,MaLoaiHinh, Sum(SoLuong)as TongSoLuong,DVT, Sum(TriGiaKB)as TongTriGia,NguyenTe_ID FROM(
	SELECT a.SoToKhai, a.MaLoaiHinh, a.DVT_ID,b.NgayDangKy, a.MaHaiQuan,a.TenHang AS TenSP,a.SoLuong, a.TriGiaKB,b.NguyenTe_ID,c.Ten,d.Ten AS DVT 
	FROM dbo.t_SXXK_HangMauDich a 
	INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
	ON a.SoToKhai = b.SoToKhai
	AND a.MaLoaiHinh = b.MaLoaiHinh
	AND a.NamDangKy = b.NamDangKy 
	AND a.MaHaiQuan = b.MaHaiQuan
	INNER JOIN dbo.t_HaiQuan_Nuoc c
	ON b.NuocNK_ID = c.ID
	INNER JOIN dbo.t_HaiQuan_DonViTinh d
	ON a.DVT_ID = d.ID
	WHERE a.MaLoaiHinh LIKE 'X%')c
WHERE NgayDangKy BETWEEN @FromDate AND @ToDate
GROUP BY MaLoaiHinh,Ten,TenSP,NguyenTe_ID,DVT
/* order BY Ten,TenSP*/
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NguyenTe_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Insert]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 13, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Insert]
	@ID char(3),
	@Ten nvarchar(50),
	@TyGiaTinhThue money
AS
INSERT INTO [dbo].[t_HaiQuan_NguyenTe]
(
	[ID],
	[Ten],
	[TyGiaTinhThue]
)
VALUES
(
	@ID,
	@Ten,
	@TyGiaTinhThue
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NguyenTe_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Update]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 13, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Update]
	@ID char(3),
	@Ten nvarchar(50),
	@TyGiaTinhThue money
AS

UPDATE
	[dbo].[t_HaiQuan_NguyenTe]
SET
	[Ten] = @Ten,
	[TyGiaTinhThue] = @TyGiaTinhThue
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_Load]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_Load]
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@MaHaiQuan char(6),
	@NamDangKy smallint,
	@SoThuTuHang smallint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoToKhai],
	[MaLoaiHinh],
	[MaHaiQuan],
	[NamDangKy],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue]
FROM
	[dbo].[t_SXXK_HangMauDich]
WHERE
	[SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [MaHaiQuan] = @MaHaiQuan
	AND [NamDangKy] = @NamDangKy
	AND [SoThuTuHang] = @SoThuTuHang

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NguyenTe_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_InsertUpdate]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 13, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NguyenTe_InsertUpdate]
	@ID char(3),
	@Ten nvarchar(50),
	@TyGiaTinhThue money
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_NguyenTe] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_NguyenTe] 
		SET
			[Ten] = @Ten,
			[TyGiaTinhThue] = @TyGiaTinhThue
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_NguyenTe]
	(
			[ID],
			[Ten],
			[TyGiaTinhThue]
	)
	VALUES
	(
			@ID,
			@Ten,
			@TyGiaTinhThue
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_SelectAll]'
GO
CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO]
FROM
	[dbo].[t_HaiQuan_MaHS]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_InsertUpdate]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@ToSo bigint,
	@LoaiToKhaiTriGia nvarchar(50),
	@NgayXuatKhau datetime,
	@CapDoThuongMai char(10),
	@QuyenSuDung bit,
	@KhongXacDinh bit,
	@TraThem bit,
	@TienTra bit,
	@CoQuanHeDacBiet bit,
	@AnhHuongQuanHe bit,
	@GhiChep nvarchar(240),
	@NgayKhaiBao datetime,
	@NguoiKhaiBao nvarchar(30),
	@ChucDanhNguoiKhaiBao nvarchar(100),
	@NgayTruyen datetime,
	@KieuQuanHe nvarchar(256)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_ToKhaiTriGia] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_ToKhaiTriGia] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[ToSo] = @ToSo,
			[LoaiToKhaiTriGia] = @LoaiToKhaiTriGia,
			[NgayXuatKhau] = @NgayXuatKhau,
			[CapDoThuongMai] = @CapDoThuongMai,
			[QuyenSuDung] = @QuyenSuDung,
			[KhongXacDinh] = @KhongXacDinh,
			[TraThem] = @TraThem,
			[TienTra] = @TienTra,
			[CoQuanHeDacBiet] = @CoQuanHeDacBiet,
			[AnhHuongQuanHe] = @AnhHuongQuanHe,
			[GhiChep] = @GhiChep,
			[NgayKhaiBao] = @NgayKhaiBao,
			[NguoiKhaiBao] = @NguoiKhaiBao,
			[ChucDanhNguoiKhaiBao] = @ChucDanhNguoiKhaiBao,
			[NgayTruyen] = @NgayTruyen,
			[KieuQuanHe] = @KieuQuanHe
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_ToKhaiTriGia]
		(
			[TKMD_ID],
			[ToSo],
			[LoaiToKhaiTriGia],
			[NgayXuatKhau],
			[CapDoThuongMai],
			[QuyenSuDung],
			[KhongXacDinh],
			[TraThem],
			[TienTra],
			[CoQuanHeDacBiet],
			[AnhHuongQuanHe],
			[GhiChep],
			[NgayKhaiBao],
			[NguoiKhaiBao],
			[ChucDanhNguoiKhaiBao],
			[NgayTruyen],
			[KieuQuanHe]
		)
		VALUES 
		(
			@TKMD_ID,
			@ToSo,
			@LoaiToKhaiTriGia,
			@NgayXuatKhau,
			@CapDoThuongMai,
			@QuyenSuDung,
			@KhongXacDinh,
			@TraThem,
			@TienTra,
			@CoQuanHeDacBiet,
			@AnhHuongQuanHe,
			@GhiChep,
			@NgayKhaiBao,
			@NguoiKhaiBao,
			@ChucDanhNguoiKhaiBao,
			@NgayTruyen,
			@KieuQuanHe
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_NguyenPhuLieu_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_Load]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 23, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Load]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM
	[dbo].[t_SXXK_NguyenPhuLieu]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NguyenTe_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Delete]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 13, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Delete]
	@ID char(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_NguyenTe]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_SelectAll]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh]
FROM
	[dbo].[t_KDT_VanDon]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_ToKhaiMauDich_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_Load]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_Load]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NamDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[Xuat_NPL_SP],
	[ThanhLy],
	[NGAY_THN_THX],
	[MaDonViUT],
	[TrangThaiThanhKhoan],
	[ChungTu],
	[PhanLuong],
	[NgayHoanThanh],
	[TrangThai],
	[TrongLuongNet],
	[SoTienKhoan]
FROM
	[dbo].[t_SXXK_ToKhaiMauDich]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [NamDangKy] = @NamDangKy

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_SelectAll]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM
	[dbo].[t_KDT_ChungTuKemChiTiet]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SelectTriGiaHangDP/NuocToKhaiXK]'
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_SXXK_SelectTriGiaHangDP/NuocToKhaiXK]
	-- Add the parameters for the stored procedure here
	@FromDate DateTime,
	@ToDate DateTime
AS
SELECT Ten,TenSP,MaLoaiHinh, Sum(SoLuong)as TongSoLuongDP,DVT, Sum(TriGiaKB)as TongTriGiaDP,NguyenTe_ID FROM(
	SELECT a.SoToKhai, a.MaLoaiHinh, a.DVT_ID,b.NgayDangKy, a.MaHaiQuan,a.TenHang AS TenSP,a.SoLuong, a.TriGiaKB,b.NguyenTe_ID,c.Ten,d.Ten AS DVT 
	FROM dbo.t_SXXK_HangMauDich a 
	INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
	ON a.SoToKhai = b.SoToKhai
	AND a.MaLoaiHinh = b.MaLoaiHinh
	AND a.NamDangKy = b.NamDangKy 
	AND a.MaHaiQuan = b.MaHaiQuan
	INNER JOIN dbo.t_HaiQuan_Nuoc c
	ON b.NuocNK_ID = c.ID
	INNER JOIN dbo.t_HaiQuan_DonViTinh d
	ON a.DVT_ID = d.ID
	WHERE a.MaLoaiHinh LIKE 'X%'AND NuocXX_ID='VN')c
WHERE NgayDangKy BETWEEN @FromDate AND @ToDate
GROUP BY MaLoaiHinh,Ten,TenSP,NguyenTe_ID,DVT
/* order BY Ten,TenSP*/

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_USER_GROUP_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_USER_GROUP_Insert]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_USER_GROUP_Insert]
	@MA_NHOM bigint,
	@USER_ID bigint
AS
INSERT INTO [dbo].[USER_GROUP]
(
	[MA_NHOM],
	[USER_ID]
)
VALUES
(
	@MA_NHOM,
	@USER_ID
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_Delete]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_Delete]
	@ID bigint
AS
DELETE FROM 
	[dbo].[t_KDT_SXXK_ToKhaiTriGia]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_NguyenPhuLieu_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_InsertUpdate]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 23, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_InsertUpdate]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma varchar(30),
	@Ten nvarchar(80),
	@MaHS varchar(10),
	@DVT_ID char(3)
AS
IF EXISTS(SELECT [MaHaiQuan], [MaDoanhNghiep], [Ma] FROM [dbo].[t_SXXK_NguyenPhuLieu] WHERE [MaHaiQuan] = @MaHaiQuan AND [MaDoanhNghiep] = @MaDoanhNghiep AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_NguyenPhuLieu] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID
		WHERE
			[MaHaiQuan] = @MaHaiQuan
			AND [MaDoanhNghiep] = @MaDoanhNghiep
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_NguyenPhuLieu]
	(
			[MaHaiQuan],
			[MaDoanhNghiep],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID]
	)
	VALUES
	(
			@MaHaiQuan,
			@MaDoanhNghiep,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID
	)
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Load]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
FROM
	[dbo].[t_KDT_CO]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NguyenTe_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Load]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 13, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Load]
	@ID char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[TyGiaTinhThue]
FROM
	[dbo].[t_HaiQuan_NguyenTe]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_USER_GROUP_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_USER_GROUP_Load]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_USER_GROUP_Load]
	@MA_NHOM bigint,
	@USER_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MA_NHOM],
	[USER_ID]
FROM
	[dbo].[USER_GROUP]
WHERE
	[MA_NHOM] = @MA_NHOM
	AND [USER_ID] = @USER_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_SelectBy_MaHaiQuan_And_SoToKhai_And_MaLoaiHinh_And_NamDangKy]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_SelectBy_MaHaiQuan_And_SoToKhai_And_MaLoaiHinh_And_NamDangKy]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_SelectBy_MaHaiQuan_And_SoToKhai_And_MaLoaiHinh_And_NamDangKy]
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NamDangKy smallint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[SoToKhai],
	[MaLoaiHinh],
	[MaHaiQuan],
	[NamDangKy],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue]
FROM
	[dbo].[t_SXXK_HangMauDich]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [NamDangKy] = @NamDangKy

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_DeleteBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_DeleteBy_TKMD_ID]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON

DELETE FROM [dbo].[t_KDT_SXXK_ToKhaiTriGia]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_SXXK_ThongTinDieuChinh]'
GO
CREATE TABLE [dbo].[t_SXXK_ThongTinDieuChinh]
(
[SoToKhai] [int] NOT NULL,
[MaLoaiHinh] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NamDangKy] [smallint] NOT NULL,
[LanDieuChinh] [int] NOT NULL,
[SoChungTu] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NGAY_HL] [datetime] NULL,
[SNAHAN] [int] NULL,
[THANH_LY] [int] NOT NULL CONSTRAINT [DF_DDIEUCHINH_THANH_LY] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThongTinDieuChinh] on [dbo].[t_SXXK_ThongTinDieuChinh]'
GO
ALTER TABLE [dbo].[t_SXXK_ThongTinDieuChinh] ADD CONSTRAINT [PK_ThongTinDieuChinh] PRIMARY KEY CLUSTERED  ([SoToKhai], [MaLoaiHinh], [MaHaiQuan], [NamDangKy], [LanDieuChinh])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_HangMauDichChungTu]'
GO
CREATE TABLE [dbo].[t_KDT_HangMauDichChungTu]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[HMD_ID] [bigint] NULL,
[ChungTu_ID] [bigint] NULL,
[LoaiChungTu] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_t_KDT_HangMauDichChungTu_LoaiChungTu] DEFAULT ('HopDong'),
[SoThuTuHang] [int] NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhu] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (252) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (18, 3) NULL,
[DonGiaKB] [float] NULL,
[TriGiaKB] [float] NULL,
[GiaTriDieuChinhTang] [float] NULL,
[GiaTriDieuChinhGiam] [float] NULL,
[GhiChu] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_HangMauDichChungTu] on [dbo].[t_KDT_HangMauDichChungTu]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDichChungTu] ADD CONSTRAINT [PK_t_KDT_HangMauDichChungTu] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_CuaKhau]'
GO
CREATE TABLE [dbo].[t_HaiQuan_CuaKhau]
(
[ID] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UuTien] [int] NOT NULL CONSTRAINT [DF_t_HaiQuan_CuaKhau_UuTien] DEFAULT ((100))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_CuaKhau] on [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ADD CONSTRAINT [PK_t_HaiQuan_CuaKhau] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_CuongChe]'
GO
CREATE TABLE [dbo].[t_KDT_CuongChe]
(
[DCUONGCHEID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[NgayNhanTB] [datetime] NOT NULL,
[SoTKCC] [numeric] (18, 0) NULL,
[TongTienThueCC] [numeric] (18, 0) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DCUONGCHE] on [dbo].[t_KDT_CuongChe]'
GO
ALTER TABLE [dbo].[t_KDT_CuongChe] ADD CONSTRAINT [PK_DCUONGCHE] PRIMARY KEY CLUSTERED  ([DCUONGCHEID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_HangMauDich]'
GO
CREATE TABLE [dbo].[t_KDT_HangMauDich]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[SoThuTuHang] [int] NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaPhu] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (252) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (18, 3) NULL,
[TrongLuong] [numeric] (18, 3) NULL,
[DonGiaKB] [float] NULL,
[DonGiaTT] [float] NULL,
[TriGiaKB] [float] NULL,
[TriGiaTT] [float] NULL,
[TriGiaKB_VND] [float] NULL,
[ThueSuatXNK] [float] NULL,
[ThueSuatTTDB] [float] NULL,
[ThueSuatGTGT] [float] NULL,
[ThueXNK] [float] NULL,
[ThueTTDB] [float] NULL,
[ThueGTGT] [float] NULL,
[PhuThu] [float] NULL,
[TyLeThuKhac] [float] NULL,
[TriGiaThuKhac] [float] NULL,
[MienThue] [tinyint] NULL,
[Ma_HTS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_HTS] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong_HTS] [numeric] (18, 3) NULL,
[FOC] [bit] NOT NULL CONSTRAINT [DF_t_KDT_HangMauDich_FOC] DEFAULT ((0)),
[ThueTuyetDoi] [bit] NULL,
[ThueSuatXNKGiam] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThueSuatTTDBGiam] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThueSuatVATGiam] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DonGiaTuyetDoi] [float] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HangMauDich] on [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ADD CONSTRAINT [PK_t_HangMauDich] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[User]'
GO
CREATE TABLE [dbo].[User]
(
[USER_NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PASSWORD] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HO_TEN] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MO_TA] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isAdmin] [bit] NOT NULL CONSTRAINT [DF__User__KIEU_NSD__75CE2CD5] DEFAULT ((0)),
[ID] [bigint] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_User] on [dbo].[User]'
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_SelectAll]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[USER_NAME],
	[PASSWORD],
	[HO_TEN],
	[MO_TA],
	[isAdmin],
	[ID]
FROM
	[dbo].[User]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ROLE]'
GO
CREATE TABLE [dbo].[ROLE]
(
[ID] [bigint] NOT NULL,
[RoleName] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MO_TA] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_SXXK_FUNCS_MO_TA] DEFAULT (''),
[ID_MODULE] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SXXK_FUNCS] on [dbo].[ROLE]'
GO
ALTER TABLE [dbo].[ROLE] ADD CONSTRAINT [PK_SXXK_FUNCS] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ROLE_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ROLE_Insert]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ROLE_Insert]
	@RoleName nvarchar(1000),
	@MO_TA nvarchar(1000),
	@ID_MODULE int,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[ROLE]
(
	[RoleName],
	[MO_TA],
	[ID_MODULE]
)
VALUES 
(
	@RoleName,
	@MO_TA,
	@ID_MODULE
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SelectTenNuocToKhaiXK]'
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_SXXK_SelectTenNuocToKhaiXK]
AS
SELECT Distinct Ten,NuocNK_ID
FROM dbo.t_SXXK_ToKhaiMauDich a 
INNER JOIN dbo.t_HaiQuan_Nuoc b
ON a.NuocNK_ID = b.ID
where MaLoaiHinh Like 'X%'
order BY Ten
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_ToKhaiMauDich]'
GO
CREATE TABLE [dbo].[t_KDT_ToKhaiMauDich]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoTiepNhan] [bigint] NOT NULL CONSTRAINT [DF_t_ToKhaiMauDich_SoTiepNhan] DEFAULT ((0)),
[NgayTiepNhan] [datetime] NOT NULL CONSTRAINT [DF_t_ToKhaiMauDich_NgayTiepNhan] DEFAULT (getdate()),
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoToKhai] [int] NULL CONSTRAINT [DF_t_ToKhaiMauDich_SoToKhai] DEFAULT ((0)),
[MaLoaiHinh] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayDangKy] [datetime] NULL,
[MaDoanhNghiep] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenDoanhNghiep] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDaiLyTTHQ] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDaiLyTTHQ] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonViDoiTac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChiTietDonViDoiTac] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoGiayPhep] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayGiayPhep] [datetime] NULL,
[NgayHetHanGiayPhep] [datetime] NULL,
[SoHopDong] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHopDong] [datetime] NULL,
[NgayHetHanHopDong] [datetime] NULL,
[SoHoaDonThuongMai] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHoaDonThuongMai] [datetime] NULL,
[PTVT_ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoHieuPTVT] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayDenPTVT] [datetime] NULL,
[QuocTichPTVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiVanDon] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoVanDon] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayVanDon] [datetime] NULL,
[NuocXK_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NuocNK_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiaDiemXepHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CuaKhau_ID] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DKGH_ID] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NguyenTe_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TyGiaTinhThue] [money] NOT NULL,
[TyGiaUSD] [money] NOT NULL,
[PTTT_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoHang] [smallint] NOT NULL,
[SoLuongPLTK] [smallint] NULL,
[TenChuHang] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoContainer20] [numeric] (15, 0) NULL,
[SoContainer40] [numeric] (15, 0) NULL,
[SoKien] [numeric] (15, 0) NULL,
[TrongLuong] [numeric] (18, 2) NULL,
[TongTriGiaKhaiBao] [money] NOT NULL,
[TongTriGiaTinhThue] [money] NULL,
[LoaiToKhaiGiaCong] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LePhiHaiQuan] [money] NULL,
[PhiBaoHiem] [money] NULL,
[PhiVanChuyen] [money] NULL,
[PhiXepDoHang] [money] NULL,
[PhiKhac] [money] NULL,
[CanBoDangKy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuanLyMay] [bit] NULL CONSTRAINT [DF_t_ToKhaiMauDich_QuanLyMay] DEFAULT ((1)),
[TrangThaiXuLy] [int] NULL,
[LoaiHangHoa] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GiayTo] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanLuong] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDonViUT] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDonViUT] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrongLuongNet] [float] NULL,
[SoTienKhoan] [money] NULL,
[GUIDSTR] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeXuatKhac] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LyDoSua] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionStatus] [smallint] NULL,
[GuidReference] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamDK] [int] NULL,
[HUONGDAN] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_ToKhaiMauDich] on [dbo].[t_KDT_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_ToKhaiMauDich] ADD CONSTRAINT [PK_t_ToKhaiMauDich] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ROLE_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ROLE_Delete]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ROLE_Delete]
	@ID bigint
AS
DELETE FROM 
	[dbo].[ROLE]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_InsertUpdate]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_InsertUpdate]
	@USER_NAME varchar(50),
	@PASSWORD varchar(200),
	@HO_TEN nvarchar(1000),
	@MO_TA nvarchar(1000),
	@isAdmin bit,
	@ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[User] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[User] 
		SET
			[USER_NAME] = @USER_NAME,
			[PASSWORD] = @PASSWORD,
			[HO_TEN] = @HO_TEN,
			[MO_TA] = @MO_TA,
			[isAdmin] = @isAdmin
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[User]
		(
			[USER_NAME],
			[PASSWORD],
			[HO_TEN],
			[MO_TA],
			[isAdmin]
		)
		VALUES 
		(
			@USER_NAME,
			@PASSWORD,
			@HO_TEN,
			@MO_TA,
			@isAdmin
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_Delete]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_Delete]
	@ID bigint
AS
DELETE FROM 
	[dbo].[User]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_KetQuaXuLy]'
GO
CREATE TABLE [dbo].[t_KDT_KetQuaXuLy]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ReferenceID] [uniqueidentifier] NULL,
[ItemID] [bigint] NULL,
[LoaiChungTu] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_t_KDT_KetQuaXuLy_LoaiChungTu] DEFAULT ('TK'),
[LoaiThongDiep] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiDung] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Ngay] [datetime] NULL CONSTRAINT [DF_t_KDT_KetQuaXuLy_CreatedTime] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_KetQuaXuLy] on [dbo].[t_KDT_KetQuaXuLy]'
GO
ALTER TABLE [dbo].[t_KDT_KetQuaXuLy] ADD CONSTRAINT [PK_t_KDT_KetQuaXuLy] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KetQuaXuLy_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_Insert]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_Insert]
	@ReferenceID uniqueidentifier,
	@LoaiChungTu varchar(10),
	@LoaiThongDiep nvarchar(100),
	@NoiDung nvarchar(500),
	@Ngay datetime,
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_KetQuaXuLy]
(
	[ReferenceID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay]
)
VALUES 
(
	@ReferenceID,
	@LoaiChungTu,
	@LoaiThongDiep,
	@NoiDung,
	@Ngay
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_Insert]'
GO







------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Insert]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_Insert]
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@TrongLuong numeric(18, 3),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@FOC BIT,
	@ThueTuyetDoi BIT,
	@ThueSuatXNKGiam  VARCHAR(50),
	@ThueSuatTTDBGiam VARCHAR(50),
	@ThueSuatVATGiam VARCHAR(50),
	@DonGiaTuyetDoi FLOAT,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HangMauDich]
(
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	ThueSuatXNKGiam,ThueSuatTTDBGiam,ThueSuatVATGiam,ThueTuyetDoi,DonGiaTuyetDoi
)
VALUES 
(
	@TKMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@TrongLuong,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaKB,
	@TriGiaTT,
	@TriGiaKB_VND,
	@ThueSuatXNK,
	@ThueSuatTTDB,
	@ThueSuatGTGT,
	@ThueXNK,
	@ThueTTDB,
	@ThueGTGT,
	@PhuThu,
	@TyLeThuKhac,
	@TriGiaThuKhac,
	@MienThue,
	@Ma_HTS,
	@DVT_HTS,
	@SoLuong_HTS,
	@FOC,@ThueSuatXNKGiam,@ThueSuatTTDBGiam,@ThueSuatVATGiam,@ThueTuyetDoi,@DonGiaTuyetDoi
)

SET @ID = SCOPE_IDENTITY()







GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KetQuaXuLy_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_Update]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_Update]
	@ID int,
	@ReferenceID uniqueidentifier,
	@LoaiChungTu varchar(10),
	@LoaiThongDiep nvarchar(100),
	@NoiDung nvarchar(500),
	@Ngay datetime
AS

UPDATE
	[dbo].[t_KDT_KetQuaXuLy]
SET
	[ReferenceID] = @ReferenceID,
	[LoaiChungTu] = @LoaiChungTu,
	[LoaiThongDiep] = @LoaiThongDiep,
	[NoiDung] = @NoiDung,
	[Ngay] = @Ngay
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KetQuaXuLy_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_InsertUpdate]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_InsertUpdate]
	@ID int,
	@ReferenceID uniqueidentifier,
	@LoaiChungTu varchar(10),
	@LoaiThongDiep nvarchar(100),
	@NoiDung nvarchar(500),
	@Ngay datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_KetQuaXuLy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_KetQuaXuLy] 
		SET
			[ReferenceID] = @ReferenceID,
			[LoaiChungTu] = @LoaiChungTu,
			[LoaiThongDiep] = @LoaiThongDiep,
			[NoiDung] = @NoiDung,
			[Ngay] = @Ngay
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_KetQuaXuLy]
		(
			[ReferenceID],
			[LoaiChungTu],
			[LoaiThongDiep],
			[NoiDung],
			[Ngay]
		)
		VALUES 
		(
			@ReferenceID,
			@LoaiChungTu,
			@LoaiThongDiep,
			@NoiDung,
			@Ngay
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KetQuaXuLy_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_Delete]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_KetQuaXuLy]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_Update]'
GO





------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Update]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@TrongLuong numeric(18, 3),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@FOC BIT,
	@ThueTuyetDoi BIT,
	@ThueSuatXNKGiam  VARCHAR(50),
	@ThueSuatTTDBGiam VARCHAR(50),
	@ThueSuatVATGiam VARCHAR(50),
	@DonGiaTuyetDoi FLOAT
AS
UPDATE
	[dbo].[t_KDT_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[TrongLuong] = @TrongLuong,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaKB] = @TriGiaKB,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaKB_VND] = @TriGiaKB_VND,
	[ThueSuatXNK] = @ThueSuatXNK,
	[ThueSuatTTDB] = @ThueSuatTTDB,
	[ThueSuatGTGT] = @ThueSuatGTGT,
	[ThueXNK] = @ThueXNK,
	[ThueTTDB] = @ThueTTDB,
	[ThueGTGT] = @ThueGTGT,
	[PhuThu] = @PhuThu,
	[TyLeThuKhac] = @TyLeThuKhac,
	[TriGiaThuKhac] = @TriGiaThuKhac,
	[MienThue] = @MienThue,
	[Ma_HTS] = @Ma_HTS,
	[DVT_HTS] = @DVT_HTS,
	[SoLuong_HTS] = @SoLuong_HTS,
	[FOC] = @FOC,
	ThueSuatXNKGiam =@ThueSuatXNKGiam ,
			ThueSuatTTDBGiam =@ThueSuatTTDBGiam ,
			ThueSuatVATGiam =@ThueSuatVATGiam ,
			ThueTuyetDoi =@ThueTuyetDoi,
			DonGiaTuyetDoi=@DonGiaTuyetDoi
WHERE
	[ID] = @ID






GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KetQuaXuLy_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_Load]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay]
FROM
	[dbo].[t_KDT_KetQuaXuLy]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KetQuaXuLy_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_SelectAll]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay]
FROM
	[dbo].[t_KDT_KetQuaXuLy]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_InsertUpdate]'
GO





------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 3),
	@TrongLuong numeric(18, 3),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@ThueSuatXNKGiam  VARCHAR(50),
	@ThueSuatTTDBGiam VARCHAR(50),
	@ThueSuatVATGiam VARCHAR(50),
	@DonGiaTuyetDoi FLOAT,
	@FOC BIT,
	@ThueTuyetDoi BIT
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue,
			[Ma_HTS] = @Ma_HTS,
			[DVT_HTS] = @DVT_HTS,
			[SoLuong_HTS] = @SoLuong_HTS,
			[FOC] = @FOC,
			ThueSuatXNKGiam =@ThueSuatXNKGiam ,
			ThueSuatTTDBGiam =@ThueSuatTTDBGiam ,
			ThueSuatVATGiam =@ThueSuatVATGiam ,
			ThueTuyetDoi=@ThueTuyetDoi,
			DonGiaTuyetDoi = @DonGiaTuyetDoi
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangMauDich]
		(
			[TKMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[TrongLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue],
			[Ma_HTS],
			[DVT_HTS],
			[SoLuong_HTS],
			[FOC],ThueSuatXNKGiam,ThueSuatTTDBGiam,ThueSuatVATGiam,ThueTuyetDoi,DonGiaTuyetDoi
		)
		VALUES 
		(
			@TKMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@TrongLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue,
			@Ma_HTS,
			@DVT_HTS,
			@SoLuong_HTS,
			@FOC,@ThueSuatXNKGiam,@ThueSuatTTDBGiam,@ThueSuatVATGiam,@ThueTuyetDoi,@DonGiaTuyetDoi
		)		
	END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Delete]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_Delete]
	@ID bigint
AS
DELETE FROM 
	[dbo].[t_KDT_HangMauDich]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON

DELETE FROM [dbo].[t_KDT_HangMauDich]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Load]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
*
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	[ID] = @ID



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	*
	
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	[TKMD_ID] = @TKMD_ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_SXXK_LogKhaiBao]'
GO
CREATE TABLE [dbo].[t_KDT_SXXK_LogKhaiBao]
(
[IDLog] [bigint] NOT NULL IDENTITY(1, 1),
[LoaiKhaiBao] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID_DK] [bigint] NOT NULL,
[GUIDSTR_DK] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserNameKhaiBao] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayKhaiBao] [datetime] NOT NULL,
[UserNameSuaDoi] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgaySuaDoi] [datetime] NOT NULL,
[GhiChu] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDelete] [bit] NULL CONSTRAINT [DF_t_KDT_SXXK_LogKhaiBao_IsDelete] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_SXXK_LogNPLDK] on [dbo].[t_KDT_SXXK_LogKhaiBao]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_LogKhaiBao] ADD CONSTRAINT [PK_t_KDT_SXXK_LogNPLDK] PRIMARY KEY CLUSTERED  ([IDLog])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, June 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDLog],
	[LoaiKhaiBao],
	[ID_DK],
	[GUIDSTR_DK],
	[UserNameKhaiBao],
	[NgayKhaiBao],
	[UserNameSuaDoi],
	[NgaySuaDoi],
	[GhiChu],
	[IsDelete]
FROM
	[dbo].[t_KDT_SXXK_LogKhaiBao]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectAll]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	*
FROM
	[dbo].[t_KDT_HangMauDich]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_LogKhaiBao_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_Load]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, June 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Load]
	@IDLog bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDLog],
	[LoaiKhaiBao],
	[ID_DK],
	[GUIDSTR_DK],
	[UserNameKhaiBao],
	[NgayKhaiBao],
	[UserNameSuaDoi],
	[NgaySuaDoi],
	[GhiChu],
	[IsDelete]
FROM
	[dbo].[t_KDT_SXXK_LogKhaiBao]
WHERE
	[IDLog] = @IDLog

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_LogKhaiBao_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_Delete]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, June 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Delete]
	@IDLog bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_LogKhaiBao]
WHERE
	[IDLog] = @IDLog


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_LogKhaiBao_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_InsertUpdate]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, June 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_InsertUpdate]
	@IDLog bigint,
	@LoaiKhaiBao varchar(50),
	@ID_DK bigint,
	@GUIDSTR_DK nvarchar(255),
	@UserNameKhaiBao nvarchar(50),
	@NgayKhaiBao datetime,
	@UserNameSuaDoi nvarchar(50),
	@NgaySuaDoi datetime,
	@GhiChu nvarchar(255),
	@IsDelete bit
AS
IF EXISTS(SELECT [IDLog] FROM [dbo].[t_KDT_SXXK_LogKhaiBao] WHERE [IDLog] = @IDLog)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_LogKhaiBao] 
		SET
			[LoaiKhaiBao] = @LoaiKhaiBao,
			[ID_DK] = @ID_DK,
			[GUIDSTR_DK] = @GUIDSTR_DK,
			[UserNameKhaiBao] = @UserNameKhaiBao,
			[NgayKhaiBao] = @NgayKhaiBao,
			[UserNameSuaDoi] = @UserNameSuaDoi,
			[NgaySuaDoi] = @NgaySuaDoi,
			[GhiChu] = @GhiChu,
			[IsDelete] = @IsDelete
		WHERE
			[IDLog] = @IDLog
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_LogKhaiBao]
		(
			[LoaiKhaiBao],
			[ID_DK],
			[GUIDSTR_DK],
			[UserNameKhaiBao],
			[NgayKhaiBao],
			[UserNameSuaDoi],
			[NgaySuaDoi],
			[GhiChu],
			[IsDelete]
		)
		VALUES 
		(
			@LoaiKhaiBao,
			@ID_DK,
			@GUIDSTR_DK,
			@UserNameKhaiBao,
			@NgayKhaiBao,
			@UserNameSuaDoi,
			@NgaySuaDoi,
			@GhiChu,
			@IsDelete
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_LogKhaiBao_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_Update]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, June 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Update]
	@IDLog bigint,
	@LoaiKhaiBao varchar(50),
	@ID_DK bigint,
	@GUIDSTR_DK nvarchar(255),
	@UserNameKhaiBao nvarchar(50),
	@NgayKhaiBao datetime,
	@UserNameSuaDoi nvarchar(50),
	@NgaySuaDoi datetime,
	@GhiChu nvarchar(255),
	@IsDelete bit
AS

UPDATE
	[dbo].[t_KDT_SXXK_LogKhaiBao]
SET
	[LoaiKhaiBao] = @LoaiKhaiBao,
	[ID_DK] = @ID_DK,
	[GUIDSTR_DK] = @GUIDSTR_DK,
	[UserNameKhaiBao] = @UserNameKhaiBao,
	[NgayKhaiBao] = @NgayKhaiBao,
	[UserNameSuaDoi] = @UserNameSuaDoi,
	[NgaySuaDoi] = @NgaySuaDoi,
	[GhiChu] = @GhiChu,
	[IsDelete] = @IsDelete
WHERE
	[IDLog] = @IDLog


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_LogKhaiBao_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_Insert]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, June 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_Insert]
	@LoaiKhaiBao varchar(50),
	@ID_DK bigint,
	@GUIDSTR_DK nvarchar(255),
	@UserNameKhaiBao nvarchar(50),
	@NgayKhaiBao datetime,
	@UserNameSuaDoi nvarchar(50),
	@NgaySuaDoi datetime,
	@GhiChu nvarchar(255),
	@IsDelete bit,
	@IDLog bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_SXXK_LogKhaiBao]
(
	[LoaiKhaiBao],
	[ID_DK],
	[GUIDSTR_DK],
	[UserNameKhaiBao],
	[NgayKhaiBao],
	[UserNameSuaDoi],
	[NgaySuaDoi],
	[GhiChu],
	[IsDelete]
)
VALUES 
(
	@LoaiKhaiBao,
	@ID_DK,
	@GUIDSTR_DK,
	@UserNameKhaiBao,
	@NgayKhaiBao,
	@UserNameSuaDoi,
	@NgaySuaDoi,
	@GhiChu,
	@IsDelete
)

SET @IDLog = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_SaoChep]'
GO
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SaoChep]
	@TKMD_ID BIGINT,
	@TKMD_ID_New BIGINT OUTPUT
AS
BEGIN
	BEGIN TRANSACTION
		INSERT INTO dbo.t_KDT_ToKhaiMauDich
		SELECT 
	        SoTiepNhan = 0 ,
	        NgayTiepNhan = '1900-1-1' ,
	        MaHaiQuan ,
	        SoToKhai = 0,
	        MaLoaiHinh ,
	        NgayDangKy = '1900-1-1',
	        MaDoanhNghiep ,
	        TenDoanhNghiep ,
	        MaDaiLyTTHQ ,
	        TenDaiLyTTHQ ,
	        TenDonViDoiTac ,
	        ChiTietDonViDoiTac ,
	        SoGiayPhep ,
	        NgayGiayPhep ,
	        NgayHetHanGiayPhep ,
	        SoHopDong ,
	        NgayHopDong ,
	        NgayHetHanHopDong ,
	        SoHoaDonThuongMai ,
	        NgayHoaDonThuongMai ,
	        PTVT_ID ,
	        SoHieuPTVT ,
	        NgayDenPTVT ,
	        QuocTichPTVT_ID ,
	        LoaiVanDon ,
	        SoVanDon ,
	        NgayVanDon ,
	        NuocXK_ID ,
	        NuocNK_ID ,
	        DiaDiemXepHang ,
	        CuaKhau_ID ,
	        DKGH_ID ,
	        NguyenTe_ID ,
	        TyGiaTinhThue ,
	        TyGiaUSD ,
	        PTTT_ID ,
	        SoHang ,
	        SoLuongPLTK ,
	        TenChuHang ,
	        SoContainer20 ,
	        SoContainer40 ,
	        SoKien ,
	        TrongLuong ,
	        TongTriGiaKhaiBao ,
	        TongTriGiaTinhThue ,
	        LoaiToKhaiGiaCong ,
	        LePhiHaiQuan ,
	        PhiBaoHiem ,
	        PhiVanChuyen ,
	        PhiXepDoHang ,
	        PhiKhac ,
	        CanBoDangKy ,
	        QuanLyMay ,
	        TrangThaiXuLy = -1,
	        LoaiHangHoa ,
	        GiayTo ,
	        PhanLuong = '',
	        MaDonViUT ,
	        TenDonViUT ,
	        TrongLuongNet ,
	        SoTienKhoan ,
	        GUIDSTR = NULL,
	        DeXuatKhac = '',
	        LyDoSua ,
	        ActionStatus = 0,
	        GuidReference = NULL,
	        NamDK = NULL,
	        HUONGDAN = '' 
	    FROM dbo.t_KDT_ToKhaiMauDich WHERE ID = @TKMD_ID	     
		
		SET @TKMD_ID_New = @@IDENTITY
		INSERT INTO dbo.t_KDT_HangMauDich
		SELECT 
	        @TKMD_ID_New ,
	        SoThuTuHang ,
	        MaHS ,
	        MaPhu ,
	        TenHang ,
	        NuocXX_ID ,
	        DVT_ID ,
	        SoLuong ,
	        TrongLuong ,
	        DonGiaKB ,
	        DonGiaTT ,
	        TriGiaKB ,
	        TriGiaTT ,
	        TriGiaKB_VND ,
	        ThueSuatXNK ,
	        ThueSuatTTDB ,
	        ThueSuatGTGT ,
	        ThueXNK ,
	        ThueTTDB ,
	        ThueGTGT ,
	        PhuThu ,
	        TyLeThuKhac ,
	        TriGiaThuKhac ,
	        MienThue ,
	        Ma_HTS ,
	        DVT_HTS ,
	        SoLuong_HTS ,
	        FOC ,
	        ThueTuyetDoi ,
	        ThueSuatXNKGiam ,
	        ThueSuatTTDBGiam ,
	        ThueSuatVATGiam ,
	        DonGiaTuyetDoi
		FROM 
			dbo.t_KDT_HangMauDich		
		WHERE 
			TKMD_ID = @TKMD_ID_New        
		
	COMMIT TRANSACTION
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Insert]
-- Database: ECS_KD_TQDT_LMMN
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 29, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@GUIDSTR varchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference varchar(50),
	@NamDK int,
	@HUONGDAN nvarchar(max),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaDaiLyTTHQ,
	@TenDaiLyTTHQ,
	@TenDonViDoiTac,
	@ChiTietDonViDoiTac,
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHanGiayPhep,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHanHopDong,
	@SoHoaDonThuongMai,
	@NgayHoaDonThuongMai,
	@PTVT_ID,
	@SoHieuPTVT,
	@NgayDenPTVT,
	@QuocTichPTVT_ID,
	@LoaiVanDon,
	@SoVanDon,
	@NgayVanDon,
	@NuocXK_ID,
	@NuocNK_ID,
	@DiaDiemXepHang,
	@CuaKhau_ID,
	@DKGH_ID,
	@NguyenTe_ID,
	@TyGiaTinhThue,
	@TyGiaUSD,
	@PTTT_ID,
	@SoHang,
	@SoLuongPLTK,
	@TenChuHang,
	@SoContainer20,
	@SoContainer40,
	@SoKien,
	@TrongLuong,
	@TongTriGiaKhaiBao,
	@TongTriGiaTinhThue,
	@LoaiToKhaiGiaCong,
	@LePhiHaiQuan,
	@PhiBaoHiem,
	@PhiVanChuyen,
	@PhiXepDoHang,
	@PhiKhac,
	@CanBoDangKy,
	@QuanLyMay,
	@TrangThaiXuLy,
	@LoaiHangHoa,
	@GiayTo,
	@PhanLuong,
	@MaDonViUT,
	@TenDonViUT,
	@TrongLuongNet,
	@SoTienKhoan,
	@GUIDSTR,
	@DeXuatKhac,
	@LyDoSua,
	@ActionStatus,
	@GuidReference,
	@NamDK,
	@HUONGDAN
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Update]
-- Database: ECS_KD_TQDT_LMMN
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 29, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@GUIDSTR varchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference varchar(50),
	@NamDK int,
	@HUONGDAN nvarchar(max)
AS
UPDATE
	[dbo].[t_KDT_ToKhaiMauDich]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
	[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
	[TenDonViDoiTac] = @TenDonViDoiTac,
	[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHanHopDong] = @NgayHetHanHopDong,
	[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
	[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
	[PTVT_ID] = @PTVT_ID,
	[SoHieuPTVT] = @SoHieuPTVT,
	[NgayDenPTVT] = @NgayDenPTVT,
	[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
	[LoaiVanDon] = @LoaiVanDon,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[NuocXK_ID] = @NuocXK_ID,
	[NuocNK_ID] = @NuocNK_ID,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[CuaKhau_ID] = @CuaKhau_ID,
	[DKGH_ID] = @DKGH_ID,
	[NguyenTe_ID] = @NguyenTe_ID,
	[TyGiaTinhThue] = @TyGiaTinhThue,
	[TyGiaUSD] = @TyGiaUSD,
	[PTTT_ID] = @PTTT_ID,
	[SoHang] = @SoHang,
	[SoLuongPLTK] = @SoLuongPLTK,
	[TenChuHang] = @TenChuHang,
	[SoContainer20] = @SoContainer20,
	[SoContainer40] = @SoContainer40,
	[SoKien] = @SoKien,
	[TrongLuong] = @TrongLuong,
	[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
	[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
	[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
	[LePhiHaiQuan] = @LePhiHaiQuan,
	[PhiBaoHiem] = @PhiBaoHiem,
	[PhiVanChuyen] = @PhiVanChuyen,
	[PhiXepDoHang] = @PhiXepDoHang,
	[PhiKhac] = @PhiKhac,
	[CanBoDangKy] = @CanBoDangKy,
	[QuanLyMay] = @QuanLyMay,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[LoaiHangHoa] = @LoaiHangHoa,
	[GiayTo] = @GiayTo,
	[PhanLuong] = @PhanLuong,
	[MaDonViUT] = @MaDonViUT,
	[TenDonViUT] = @TenDonViUT,
	[TrongLuongNet] = @TrongLuongNet,
	[SoTienKhoan] = @SoTienKhoan,
	[GUIDSTR] = @GUIDSTR,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ActionStatus] = @ActionStatus,
	[GuidReference] = @GuidReference,
	[NamDK] = @NamDK,
	[HUONGDAN] = @HUONGDAN
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
-- Database: ECS_KD_TQDT_LMMN
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 29, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(500),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@GUIDSTR varchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference varchar(50),
	@NamDK int,
	@HUONGDAN nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDich] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
			[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
			[TenDonViDoiTac] = @TenDonViDoiTac,
			[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHanHopDong] = @NgayHetHanHopDong,
			[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
			[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
			[PTVT_ID] = @PTVT_ID,
			[SoHieuPTVT] = @SoHieuPTVT,
			[NgayDenPTVT] = @NgayDenPTVT,
			[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
			[LoaiVanDon] = @LoaiVanDon,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[NuocXK_ID] = @NuocXK_ID,
			[NuocNK_ID] = @NuocNK_ID,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[CuaKhau_ID] = @CuaKhau_ID,
			[DKGH_ID] = @DKGH_ID,
			[NguyenTe_ID] = @NguyenTe_ID,
			[TyGiaTinhThue] = @TyGiaTinhThue,
			[TyGiaUSD] = @TyGiaUSD,
			[PTTT_ID] = @PTTT_ID,
			[SoHang] = @SoHang,
			[SoLuongPLTK] = @SoLuongPLTK,
			[TenChuHang] = @TenChuHang,
			[SoContainer20] = @SoContainer20,
			[SoContainer40] = @SoContainer40,
			[SoKien] = @SoKien,
			[TrongLuong] = @TrongLuong,
			[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
			[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
			[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
			[LePhiHaiQuan] = @LePhiHaiQuan,
			[PhiBaoHiem] = @PhiBaoHiem,
			[PhiVanChuyen] = @PhiVanChuyen,
			[PhiXepDoHang] = @PhiXepDoHang,
			[PhiKhac] = @PhiKhac,
			[CanBoDangKy] = @CanBoDangKy,
			[QuanLyMay] = @QuanLyMay,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[LoaiHangHoa] = @LoaiHangHoa,
			[GiayTo] = @GiayTo,
			[PhanLuong] = @PhanLuong,
			[MaDonViUT] = @MaDonViUT,
			[TenDonViUT] = @TenDonViUT,
			[TrongLuongNet] = @TrongLuongNet,
			[SoTienKhoan] = @SoTienKhoan,
			[GUIDSTR] = @GUIDSTR,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ActionStatus] = @ActionStatus,
			[GuidReference] = @GuidReference,
			[NamDK] = @NamDK,
			[HUONGDAN] = @HUONGDAN
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaDaiLyTTHQ],
			[TenDaiLyTTHQ],
			[TenDonViDoiTac],
			[ChiTietDonViDoiTac],
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHanGiayPhep],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHanHopDong],
			[SoHoaDonThuongMai],
			[NgayHoaDonThuongMai],
			[PTVT_ID],
			[SoHieuPTVT],
			[NgayDenPTVT],
			[QuocTichPTVT_ID],
			[LoaiVanDon],
			[SoVanDon],
			[NgayVanDon],
			[NuocXK_ID],
			[NuocNK_ID],
			[DiaDiemXepHang],
			[CuaKhau_ID],
			[DKGH_ID],
			[NguyenTe_ID],
			[TyGiaTinhThue],
			[TyGiaUSD],
			[PTTT_ID],
			[SoHang],
			[SoLuongPLTK],
			[TenChuHang],
			[SoContainer20],
			[SoContainer40],
			[SoKien],
			[TrongLuong],
			[TongTriGiaKhaiBao],
			[TongTriGiaTinhThue],
			[LoaiToKhaiGiaCong],
			[LePhiHaiQuan],
			[PhiBaoHiem],
			[PhiVanChuyen],
			[PhiXepDoHang],
			[PhiKhac],
			[CanBoDangKy],
			[QuanLyMay],
			[TrangThaiXuLy],
			[LoaiHangHoa],
			[GiayTo],
			[PhanLuong],
			[MaDonViUT],
			[TenDonViUT],
			[TrongLuongNet],
			[SoTienKhoan],
			[GUIDSTR],
			[DeXuatKhac],
			[LyDoSua],
			[ActionStatus],
			[GuidReference],
			[NamDK],
			[HUONGDAN]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaDaiLyTTHQ,
			@TenDaiLyTTHQ,
			@TenDonViDoiTac,
			@ChiTietDonViDoiTac,
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHanGiayPhep,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHanHopDong,
			@SoHoaDonThuongMai,
			@NgayHoaDonThuongMai,
			@PTVT_ID,
			@SoHieuPTVT,
			@NgayDenPTVT,
			@QuocTichPTVT_ID,
			@LoaiVanDon,
			@SoVanDon,
			@NgayVanDon,
			@NuocXK_ID,
			@NuocNK_ID,
			@DiaDiemXepHang,
			@CuaKhau_ID,
			@DKGH_ID,
			@NguyenTe_ID,
			@TyGiaTinhThue,
			@TyGiaUSD,
			@PTTT_ID,
			@SoHang,
			@SoLuongPLTK,
			@TenChuHang,
			@SoContainer20,
			@SoContainer40,
			@SoKien,
			@TrongLuong,
			@TongTriGiaKhaiBao,
			@TongTriGiaTinhThue,
			@LoaiToKhaiGiaCong,
			@LePhiHaiQuan,
			@PhiBaoHiem,
			@PhiVanChuyen,
			@PhiXepDoHang,
			@PhiKhac,
			@CanBoDangKy,
			@QuanLyMay,
			@TrangThaiXuLy,
			@LoaiHangHoa,
			@GiayTo,
			@PhanLuong,
			@MaDonViUT,
			@TenDonViUT,
			@TrongLuongNet,
			@SoTienKhoan,
			@GUIDSTR,
			@DeXuatKhac,
			@LyDoSua,
			@ActionStatus,
			@GuidReference,
			@NamDK,
			@HUONGDAN
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Delete]
-- Database: ECS_KD_TQDT_LMMN
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 29, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Delete]
	@ID bigint
AS
DELETE FROM 
	[dbo].[t_KDT_ToKhaiMauDich]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Load]
-- Database: ECS_KD_TQDT_LMMN
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 29, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
FROM
	[dbo].[t_KDT_ToKhaiMauDich]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectAll]
-- Database: ECS_KD_TQDT_LMMN
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 29, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
FROM
	[dbo].[t_KDT_ToKhaiMauDich]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ROLE_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ROLE_SelectAll]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ROLE_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[RoleName],
	[MO_TA],
	[ID_MODULE]
FROM
	[dbo].[ROLE]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ROLE_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ROLE_Load]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ROLE_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[RoleName],
	[MO_TA],
	[ID_MODULE]
FROM
	[dbo].[ROLE]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ROLE_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ROLE_Update]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ROLE_Update]
	@ID bigint,
	@RoleName nvarchar(1000),
	@MO_TA nvarchar(1000),
	@ID_MODULE int
AS
UPDATE
	[dbo].[ROLE]
SET
	[RoleName] = @RoleName,
	[MO_TA] = @MO_TA,
	[ID_MODULE] = @ID_MODULE
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_Insert]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_Insert]
	@USER_NAME varchar(50),
	@PASSWORD varchar(200),
	@HO_TEN nvarchar(1000),
	@MO_TA nvarchar(1000),
	@isAdmin bit,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[User]
(
	[USER_NAME],
	[PASSWORD],
	[HO_TEN],
	[MO_TA],
	[isAdmin]
)
VALUES 
(
	@USER_NAME,
	@PASSWORD,
	@HO_TEN,
	@MO_TA,
	@isAdmin
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ROLE_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ROLE_InsertUpdate]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ROLE_InsertUpdate]
	@ID bigint,
	@RoleName nvarchar(1000),
	@MO_TA nvarchar(1000),
	@ID_MODULE int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[ROLE] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[ROLE] 
		SET
			[RoleName] = @RoleName,
			[MO_TA] = @MO_TA,
			[ID_MODULE] = @ID_MODULE
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[ROLE]
		(
			[RoleName],
			[MO_TA],
			[ID_MODULE]
		)
		VALUES 
		(
			@RoleName,
			@MO_TA,
			@ID_MODULE
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_Update]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_Update]
	@USER_NAME varchar(50),
	@PASSWORD varchar(200),
	@HO_TEN nvarchar(1000),
	@MO_TA nvarchar(1000),
	@isAdmin bit,
	@ID bigint
AS
UPDATE
	[dbo].[User]
SET
	[USER_NAME] = @USER_NAME,
	[PASSWORD] = @PASSWORD,
	[HO_TEN] = @HO_TEN,
	[MO_TA] = @MO_TA,
	[isAdmin] = @isAdmin
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_Load]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[USER_NAME],
	[PASSWORD],
	[HO_TEN],
	[MO_TA],
	[isAdmin],
	[ID]
FROM
	[dbo].[User]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_CuongCheChiTiet]'
GO
CREATE TABLE [dbo].[t_KDT_CuongCheChiTiet]
(
[DCUONGCHE_TKID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[DCUONGCHEID] [numeric] (18, 0) NOT NULL,
[SoTK] [numeric] (18, 0) NOT NULL,
[Ma_LH] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ma_HQ] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nam_DK] [numeric] (18, 0) NOT NULL,
[Ngay_DK] [datetime] NOT NULL,
[Ma_DN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[reference] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[T_XNK] [numeric] (18, 0) NOT NULL CONSTRAINT [DF_DCUONGCHE_TK_T_XNK] DEFAULT ((0)),
[T_VAT] [numeric] (18, 0) NOT NULL CONSTRAINT [DF_DCUONGCHE_TK_T_VAT] DEFAULT ((0)),
[T_TTDB] [numeric] (18, 0) NOT NULL CONSTRAINT [DF_DCUONGCHE_TK_T_TTDB] DEFAULT ((0)),
[T_CLGIA] [numeric] (18, 0) NOT NULL CONSTRAINT [DF_DCUONGCHE_TK_T_CLGIA] DEFAULT ((0)),
[T_ThuKhac] [numeric] (18, 0) NOT NULL,
[T_Phat] [numeric] (18, 0) NOT NULL CONSTRAINT [DF_DCUONGCHE_TK_T_Phat] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DCUONGCHE_TK] on [dbo].[t_KDT_CuongCheChiTiet]'
GO
ALTER TABLE [dbo].[t_KDT_CuongCheChiTiet] ADD CONSTRAINT [PK_DCUONGCHE_TK] PRIMARY KEY CLUSTERED  ([DCUONGCHE_TKID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID_Report]'
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID_Report]
	@TKMD_ID bigint
AS
BEGIN
	SELECT HMD.[SoThuTuHang],
		HMD.[MaHS],
		HMD.[TenHang],
		HMD.[SoLuong],
		HMD.[DonGiaTT],
		HMD.[TriGiaTT],
		DVT.[Ten] DonViTinh,
		Nuoc.[Ten] XuatXu
		
	FROM
		[dbo].[t_KDT_HangMauDich] HMD,
		[dbo].[t_HaiQuan_DonViTinh] DVT,
		[dbo].[t_HaiQuan_Nuoc] Nuoc
	WHERE
		HMD.[TKMD_ID] = @TKMD_ID
		AND HMD.[NuocXX_ID]=Nuoc.[ID]
		AND HMD.[DVT_ID]=DVT.[ID]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_View_BangKeConatainer]'
GO
CREATE VIEW dbo.t_View_BangKeConatainer
AS
SELECT     dbo.t_KDT_Container.SoHieu, dbo.t_KDT_Container.LoaiContainer, dbo.t_KDT_Container.Seal_No, dbo.t_KDT_ToKhaiMauDich.NgayDangKy, 
                      dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep, dbo.t_KDT_ToKhaiMauDich.SoToKhai, 
                      dbo.t_KDT_VanDon.NgayVanDon, dbo.t_KDT_VanDon.SoHieuChuyenDi
FROM         dbo.t_KDT_Container INNER JOIN
                      dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_Container.ID = dbo.t_KDT_ToKhaiMauDich.ID INNER JOIN
                      dbo.t_KDT_VanDon ON dbo.t_KDT_Container.VanDon_ID = dbo.t_KDT_VanDon.ID AND dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_VanDon.TKMD_ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_Cuc]'
GO
CREATE TABLE [dbo].[t_HaiQuan_Cuc]
(
[ID] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_DieuKienGiaoHang]'
GO
CREATE TABLE [dbo].[t_HaiQuan_DieuKienGiaoHang]
(
[ID] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MoTa] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_DieuKienGiaoHang] on [dbo].[t_HaiQuan_DieuKienGiaoHang]'
GO
ALTER TABLE [dbo].[t_HaiQuan_DieuKienGiaoHang] ADD CONSTRAINT [PK_t_HaiQuan_DieuKienGiaoHang] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_DonViHaiQuan]'
GO
CREATE TABLE [dbo].[t_HaiQuan_DonViHaiQuan]
(
[ID] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_DonViHaiQuan] on [dbo].[t_HaiQuan_DonViHaiQuan]'
GO
ALTER TABLE [dbo].[t_HaiQuan_DonViHaiQuan] ADD CONSTRAINT [PK_t_HaiQuan_DonViHaiQuan] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_LoaiChungTu]'
GO
CREATE TABLE [dbo].[t_HaiQuan_LoaiChungTu]
(
[ID] [int] NOT NULL,
[Ten] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_LoaiChungTu] on [dbo].[t_HaiQuan_LoaiChungTu]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiChungTu] ADD CONSTRAINT [PK_t_HaiQuan_LoaiChungTu] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_LoaiHinhMauDich]'
GO
CREATE TABLE [dbo].[t_HaiQuan_LoaiHinhMauDich]
(
[ID] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten_VT] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_LoaiHinhMauDich] on [dbo].[t_HaiQuan_LoaiHinhMauDich]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiHinhMauDich] ADD CONSTRAINT [PK_t_HaiQuan_LoaiHinhMauDich] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_PhuongThucThanhToan]'
GO
CREATE TABLE [dbo].[t_HaiQuan_PhuongThucThanhToan]
(
[ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GhiChu] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_PhuongThucThanhToan] on [dbo].[t_HaiQuan_PhuongThucThanhToan]'
GO
ALTER TABLE [dbo].[t_HaiQuan_PhuongThucThanhToan] ADD CONSTRAINT [PK_t_HaiQuan_PhuongThucThanhToan] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_PhuongThucVanTai]'
GO
CREATE TABLE [dbo].[t_HaiQuan_PhuongThucVanTai]
(
[ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_PhuongThucVanTai] on [dbo].[t_HaiQuan_PhuongThucVanTai]'
GO
ALTER TABLE [dbo].[t_HaiQuan_PhuongThucVanTai] ADD CONSTRAINT [PK_t_HaiQuan_PhuongThucVanTai] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_Version]'
GO
CREATE TABLE [dbo].[t_HaiQuan_Version]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Version] [decimal] (5, 0) NOT NULL,
[Date] [datetime] NOT NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_Version] on [dbo].[t_HaiQuan_Version]'
GO
ALTER TABLE [dbo].[t_HaiQuan_Version] ADD CONSTRAINT [PK_t_HaiQuan_Version] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_Chuong]'
GO
CREATE TABLE [dbo].[t_KDT_Chuong]
(
[SoChuong] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MoTa] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChuGiai] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_DanhSachChungTu]'
GO
CREATE TABLE [dbo].[t_KDT_DanhSachChungTu]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TenDanhSachChungTu] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_MessageReturn]'
GO
CREATE TABLE [dbo].[t_KDT_MessageReturn]
(
[STT] [bigint] NOT NULL,
[LOAI] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NGAY] [datetime] NULL,
[GIO] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_MessageReturn] on [dbo].[t_KDT_MessageReturn]'
GO
ALTER TABLE [dbo].[t_KDT_MessageReturn] ADD CONSTRAINT [PK_t_KDT_MessageReturn] PRIMARY KEY CLUSTERED  ([STT])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_SXXK_Temp]'
GO
CREATE TABLE [dbo].[t_SXXK_Temp]
(
[SoToKhai] [int] NOT NULL,
[MaLoaiHinh] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NamDangKy] [smallint] NOT NULL,
[SoThuTuHang] [smallint] NOT NULL,
[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaPhu] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenHang] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuong] [numeric] (18, 3) NOT NULL,
[DonGiaKB] [float] NULL,
[DonGiaTT] [float] NULL,
[TriGiaKB] [float] NOT NULL,
[TriGiaTT] [float] NOT NULL,
[TriGiaKB_VND] [float] NOT NULL,
[ThueSuatXNK] [float] NULL,
[ThueSuatTTDB] [float] NULL,
[ThueSuatGTGT] [float] NULL,
[ThueXNK] [float] NULL,
[ThueTTDB] [float] NULL,
[ThueGTGT] [float] NULL,
[PhuThu] [float] NULL,
[TyLeThuKhac] [float] NULL,
[TriGiaThuKhac] [float] NULL,
[MienThue] [float] NOT NULL,
[ID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_SXXK_Temp] on [dbo].[t_SXXK_Temp]'
GO
ALTER TABLE [dbo].[t_SXXK_Temp] ADD CONSTRAINT [PK_t_SXXK_Temp] PRIMARY KEY CLUSTERED  ([SoToKhai], [MaLoaiHinh], [MaHaiQuan], [NamDangKy], [SoThuTuHang])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
FROM [dbo].[t_KDT_GiayPhep] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_Insert]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_Insert]
	@HMD_ID bigint,
	@TKMD_ID bigint,
	@CO_ID bigint,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_CODetail]
(
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
)
VALUES 
(
	@HMD_ID,
	@TKMD_ID,
	@CO_ID
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_Update]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_Update]
	@ID bigint,
	@HMD_ID bigint,
	@TKMD_ID bigint,
	@CO_ID bigint
AS

UPDATE
	[dbo].[t_KDT_CODetail]
SET
	[HMD_ID] = @HMD_ID,
	[TKMD_ID] = @TKMD_ID,
	[CO_ID] = @CO_ID
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_DeleteDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GiayPhepDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_InsertUpdate]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_InsertUpdate]
	@ID bigint,
	@HMD_ID bigint,
	@TKMD_ID bigint,
	@CO_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_CODetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_CODetail] 
		SET
			[HMD_ID] = @HMD_ID,
			[TKMD_ID] = @TKMD_ID,
			[CO_ID] = @CO_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_CODetail]
		(
			[HMD_ID],
			[TKMD_ID],
			[CO_ID]
		)
		VALUES 
		(
			@HMD_ID,
			@TKMD_ID,
			@CO_ID
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NguyenTe_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_DeleteDynamic]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 13, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NguyenTe_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_NguyenTe] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_SelectDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhepID],
	[TKMD_ID]
FROM [dbo].[t_KDT_GiayPhepDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NguyenTe_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_SelectDynamic]
-- Database: ECS_TQDT_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 13, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NguyenTe_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[TyGiaTinhThue]
FROM [dbo].[t_HaiQuan_NguyenTe] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DoiTac_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DoiTac_DeleteDynamic]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 06, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DoiTac_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DoiTac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DoiTac_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DoiTac_SelectDynamic]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 06, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DoiTac_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[ID],
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep]
 FROM [dbo].[t_HaiQuan_DoiTac] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_Delete]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_CODetail]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_Insert]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_Insert]
	@SoHopDong varbinary,
	@NgayHopDong datetime,
	@ThoiHanThanhToan datetime,
	@NguyenTe_ID varchar(3),
	@PTTT_ID varchar(3),
	@DKGH_ID varchar(3),
	@DiaDiemGiaoHang nvarchar(255),
	@MaDonViMua varchar(50),
	@TenDonViMua nvarchar(100),
	@MaDonViBan varchar(50),
	@TenDonViBan nvarchar(100),
	@TongTriGia money,
	@ThongTinKhac nvarchar(255),
	@TKMD_ID bigint,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HopDong]
(
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID]
)
VALUES 
(
	@SoHopDong,
	@NgayHopDong,
	@ThoiHanThanhToan,
	@NguyenTe_ID,
	@PTTT_ID,
	@DKGH_ID,
	@DiaDiemGiaoHang,
	@MaDonViMua,
	@TenDonViMua,
	@MaDonViBan,
	@TenDonViBan,
	@TongTriGia,
	@ThongTinKhac,
	@TKMD_ID
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_Update]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_Update]
	@ID bigint,
	@SoHopDong varbinary,
	@NgayHopDong datetime,
	@ThoiHanThanhToan datetime,
	@NguyenTe_ID varchar(3),
	@PTTT_ID varchar(3),
	@DKGH_ID varchar(3),
	@DiaDiemGiaoHang nvarchar(255),
	@MaDonViMua varchar(50),
	@TenDonViMua nvarchar(100),
	@MaDonViBan varchar(50),
	@TenDonViBan nvarchar(100),
	@TongTriGia money,
	@ThongTinKhac nvarchar(255),
	@TKMD_ID bigint
AS

UPDATE
	[dbo].[t_KDT_HopDong]
SET
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[ThoiHanThanhToan] = @ThoiHanThanhToan,
	[NguyenTe_ID] = @NguyenTe_ID,
	[PTTT_ID] = @PTTT_ID,
	[DKGH_ID] = @DKGH_ID,
	[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
	[MaDonViMua] = @MaDonViMua,
	[TenDonViMua] = @TenDonViMua,
	[MaDonViBan] = @MaDonViBan,
	[TenDonViBan] = @TenDonViBan,
	[TongTriGia] = @TongTriGia,
	[ThongTinKhac] = @ThongTinKhac,
	[TKMD_ID] = @TKMD_ID
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_InsertUpdate]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_InsertUpdate]
	@ID bigint,
	@SoHopDong varbinary,
	@NgayHopDong datetime,
	@ThoiHanThanhToan datetime,
	@NguyenTe_ID varchar(3),
	@PTTT_ID varchar(3),
	@DKGH_ID varchar(3),
	@DiaDiemGiaoHang nvarchar(255),
	@MaDonViMua varchar(50),
	@TenDonViMua nvarchar(100),
	@MaDonViBan varchar(50),
	@TenDonViBan nvarchar(100),
	@TongTriGia money,
	@ThongTinKhac nvarchar(255),
	@TKMD_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HopDong] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HopDong] 
		SET
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[ThoiHanThanhToan] = @ThoiHanThanhToan,
			[NguyenTe_ID] = @NguyenTe_ID,
			[PTTT_ID] = @PTTT_ID,
			[DKGH_ID] = @DKGH_ID,
			[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
			[MaDonViMua] = @MaDonViMua,
			[TenDonViMua] = @TenDonViMua,
			[MaDonViBan] = @MaDonViBan,
			[TenDonViBan] = @TenDonViBan,
			[TongTriGia] = @TongTriGia,
			[ThongTinKhac] = @ThongTinKhac,
			[TKMD_ID] = @TKMD_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HopDong]
		(
			[SoHopDong],
			[NgayHopDong],
			[ThoiHanThanhToan],
			[NguyenTe_ID],
			[PTTT_ID],
			[DKGH_ID],
			[DiaDiemGiaoHang],
			[MaDonViMua],
			[TenDonViMua],
			[MaDonViBan],
			[TenDonViBan],
			[TongTriGia],
			[ThongTinKhac],
			[TKMD_ID]
		)
		VALUES 
		(
			@SoHopDong,
			@NgayHopDong,
			@ThoiHanThanhToan,
			@NguyenTe_ID,
			@PTTT_ID,
			@DKGH_ID,
			@DiaDiemGiaoHang,
			@MaDonViMua,
			@TenDonViMua,
			@MaDonViBan,
			@TenDonViBan,
			@TongTriGia,
			@ThongTinKhac,
			@TKMD_ID
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_Delete]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HopDong]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_DeleteBy_CO_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_DeleteBy_CO_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_DeleteBy_CO_ID]
	@CO_ID bigint
AS

DELETE FROM [dbo].[t_KDT_CODetail]
WHERE
	[CO_ID] = @CO_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_DeleteBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HopDong]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_DeleteDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HopDong] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_DeleteBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_DeleteBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_CODetail]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_Load]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_HopDong]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_DeleteDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HuyToKhai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_SelectBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_HopDong]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_SelectDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID]
FROM [dbo].[t_KDT_HopDong] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDong_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDong_SelectAll]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDong_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoHopDong],
	[NgayHopDong],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_HopDong]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HuyToKhai_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HuyToKhai_SelectDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 14, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HuyToKhai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[NamTiepNhan],
	[TrangThai],
	[Guid],
	[LyDoHuy],
	[TKMD_ID]
FROM [dbo].[t_KDT_HuyToKhai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteDynamic]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HoaDonThuongMaiDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KetQuaXuLy_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_DeleteDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_KetQuaXuLy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_Load]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM
	[dbo].[t_KDT_CODetail]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectDynamic]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMaiDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HMD_ID],
	[HoaDonTM_ID],
	[GiaTriDieuChinhTang],
	[GiaiTriDieuChinhGiam],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM [dbo].[t_KDT_HoaDonThuongMaiDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KetQuaXuLy_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_SelectDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay]
FROM [dbo].[t_KDT_KetQuaXuLy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_ToKhaiMauDich_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_DeleteDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_ToKhaiMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_SelectBy_CO_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_SelectBy_CO_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_SelectBy_CO_ID]
	@CO_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM
	[dbo].[t_KDT_CODetail]
WHERE
	[CO_ID] = @CO_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_Nuoc_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_DeleteDynamic]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Nuoc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_Nuoc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_Nuoc_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_SelectDynamic]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Nuoc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten]
FROM [dbo].[t_HaiQuan_Nuoc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_SelectBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_SelectBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM
	[dbo].[t_KDT_CODetail]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_ToKhaiMauDich_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ToKhaiMauDich_SelectDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ToKhaiMauDich_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NamDangKy],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[Xuat_NPL_SP],
	[ThanhLy],
	[NGAY_THN_THX],
	[MaDonViUT],
	[TrangThaiThanhKhoan],
	[ChungTu],
	[PhanLuong],
	[NgayHoanThanh],
	[TrangThai],
	[TrongLuongNet],
	[SoTienKhoan]
 FROM [dbo].[t_SXXK_ToKhaiMauDich] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_MsgSend_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_DeleteDynamic]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 17, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_MsgSend] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_MsgSend_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_SelectDynamic]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 17, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[master_id],
	[func],
	[LoaiHS],
	[msg]
 FROM [dbo].[t_KDT_SXXK_MsgSend] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_SelectAll]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM
	[dbo].[t_KDT_CODetail]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_NguyenPhuLieu_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_SelectDynamic]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 23, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
 FROM [dbo].[t_SXXK_NguyenPhuLieu] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_ROLE_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_ROLE_DeleteDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_ROLE_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[GROUP_ROLE] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_DeleteDynamic]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_ToKhaiTriGia] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_ROLE_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_ROLE_SelectDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_ROLE_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[GROUP_ID],
	[ID_ROLE]
 FROM [dbo].[GROUP_ROLE] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NoiDungToKhai_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NoiDungToKhai_DeleteDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 26 Tháng Giêng 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NoiDungToKhai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_NoiDungToKhai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NoiDungToKhai_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NoiDungToKhai_SelectDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 26 Tháng Giêng 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NoiDungToKhai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ma],
	[Ten],
	[MaLoaiHinh],
	[GhiChu]
FROM [dbo].[t_HaiQuan_NoiDungToKhai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ToKhaiTriGia_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ToKhaiTriGia_SelectDynamic]
-- Database: ECSKD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 21, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ToKhaiTriGia_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[ID],
	[TKMD_ID],
	[ToSo],
	[LoaiToKhaiTriGia],
	[NgayXuatKhau],
	[CapDoThuongMai],
	[QuyenSuDung],
	[KhongXacDinh],
	[TraThem],
	[TienTra],
	[CoQuanHeDacBiet],
	[AnhHuongQuanHe],
	[GhiChep],
	[NgayKhaiBao],
	[NguoiKhaiBao],
	[ChucDanhNguoiKhaiBao],
	[NgayTruyen],
	[KieuQuanHe]
 FROM [dbo].[t_KDT_SXXK_ToKhaiTriGia] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_DeleteDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[GROUPS] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GROUP_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GROUP_SelectDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GROUP_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[MA_NHOM],
	[TEN_NHOM],
	[MO_TA]
 FROM [dbo].[GROUPS] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_DeleteDynamic]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HopDongThuongMaiDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ROLE_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ROLE_DeleteDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ROLE_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[ROLE] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ROLE_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ROLE_SelectDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ROLE_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[ID],
	[RoleName],
	[MO_TA],
	[ID_MODULE]
 FROM [dbo].[ROLE] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, June 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[IDLog],
	[LoaiKhaiBao],
	[ID_DK],
	[GUIDSTR_DK],
	[UserNameKhaiBao],
	[NgayKhaiBao],
	[UserNameSuaDoi],
	[NgaySuaDoi],
	[GhiChu],
	[IsDelete]
FROM [dbo].[t_KDT_SXXK_LogKhaiBao] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SanPham_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_DeleteDynamic]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_SanPham] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 15, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[SoVanDon],
	[NgayVanDon],
	[ThoiGianDen],
	[DiaDiemKiemTra],
	[TuyenDuong]
FROM [dbo].[t_KDT_DeNghiChuyenCuaKhau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMaiDetail_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_SelectDynamic]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HMD_ID],
	[HopDongTM_ID],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM [dbo].[t_KDT_HopDongThuongMaiDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SanPham_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_SelectDynamic]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
 FROM [dbo].[t_SXXK_SanPham] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_LogKhaiBao_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_LogKhaiBao_DeleteDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Monday, June 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_LogKhaiBao_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_LogKhaiBao] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DeleteDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[User] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_SelectDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[USER_NAME],
	[PASSWORD],
	[HO_TEN],
	[MO_TA],
	[isAdmin],
	[ID]
 FROM [dbo].[User] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_USER_GROUP_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_USER_GROUP_DeleteDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_USER_GROUP_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[USER_GROUP] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaH_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaH_DeleteDynamic]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 07, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaH_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_MaHS] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_USER_GROUP_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_USER_GROUP_SelectDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 27, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_USER_GROUP_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[MA_NHOM],
	[USER_ID]
 FROM [dbo].[USER_GROUP] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_DeleteDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_CO] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuanPhongKhai_CauHinh_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_SelectDynamic]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuanPhongKhai_CauHinh_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
 FROM [dbo].[t_HaiQuanPhongKhai_CauHinh] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaH_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaH_SelectDynamic]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Monday, July 07, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaH_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO]
 FROM [dbo].[t_HaiQuan_MaHS] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_SelectDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan]
FROM [dbo].[t_KDT_CO] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HeThongPhongKhai_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_DeleteDynamic]
-- Database: HaiQuanPhongKhaiKD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 11, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_HeThongPhongKhai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_DeleteDynamic]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_ChungTuKemTheo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HeThongPhongKhai_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HeThongPhongKhai_SelectDynamic]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 11, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HeThongPhongKhai_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[MaDoanhNghiep],
	[PassWord],
	[TenDoanhNghiep],
	[Key_Config],
	[Value_Config],
	[Role]
 FROM [dbo].[t_HeThongPhongKhai] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Message_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_DeleteDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Message_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_Messages] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_ChungTuKemTheo_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_ChungTuKemTheo_SelectDynamic]
-- Database: Haiquan
-- Author: Ngo Thanh Tung
-- Time created: Friday, May 30, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_ChungTuKemTheo_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[ID],
	[TenChungTu],
	[SoBanChinh],
	[SoBanSao],
	[FileUpLoad],
	[Master_ID],
	[STTHang],
	[LoaiCT],
	[NoiDung]
 FROM [dbo].[t_KDT_SXXK_ChungTuKemTheo] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiCO_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiCO_DeleteDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiCO_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_LoaiCO] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Message_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_SelectDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Message_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceID],
	[ItemID],
	[MessageFrom],
	[MessageTo],
	[MessageType],
	[MessageFunction],
	[MessageContent],
	[CreatedTime],
	[TieuDeThongBao],
	[NoiDungThongBao]
FROM [dbo].[t_KDT_Messages] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiCO_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiCO_SelectDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiCO_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[Ma],
	[Ten]
FROM [dbo].[t_HaiQuan_LoaiCO] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_Insert]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_Insert]
	@GiayPhepID bigint,
	@TKMD_ID bigint,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_GiayPhepDetail]
(
	[GiayPhepID],
	[TKMD_ID]
)
VALUES 
(
	@GiayPhepID,
	@TKMD_ID
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_DeleteDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_KD_ToKhaiTriGiaPP23] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuanPhongKhai_CauHinh_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuanPhongKhai_CauHinh_DeleteDynamic]
-- Database: HaiQuanPhongKhaiSQL2005
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, August 26, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuanPhongKhai_CauHinh_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuanPhongKhai_CauHinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_Insert]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_Insert]
	@HMD_ID bigint,
	@TKMD_ID bigint,
	@CO_ID bigint,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_CO_Detail]
(
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
)
VALUES 
(
	@HMD_ID,
	@TKMD_ID,
	@CO_ID
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_Update]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_Update]
	@ID bigint,
	@GiayPhepID bigint,
	@TKMD_ID bigint
AS

UPDATE
	[dbo].[t_KDT_GiayPhepDetail]
SET
	[GiayPhepID] = @GiayPhepID,
	[TKMD_ID] = @TKMD_ID
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_NguyenPhuLieu_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_DeleteDynamic]
-- Database: eDeclaration
-- Author: Ngo Thanh Tung
-- Time created: Monday, November 20, 2006
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_NguyenPhuLieu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_Update]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_Update]
	@ID bigint,
	@HMD_ID bigint,
	@TKMD_ID bigint,
	@CO_ID bigint
AS

UPDATE
	[dbo].[t_KDT_CO_Detail]
SET
	[HMD_ID] = @HMD_ID,
	[TKMD_ID] = @TKMD_ID,
	[CO_ID] = @CO_ID
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_InsertUpdate]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_InsertUpdate]
	@ID bigint,
	@GiayPhepID bigint,
	@TKMD_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GiayPhepDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GiayPhepDetail] 
		SET
			[GiayPhepID] = @GiayPhepID,
			[TKMD_ID] = @TKMD_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GiayPhepDetail]
		(
			[GiayPhepID],
			[TKMD_ID]
		)
		VALUES 
		(
			@GiayPhepID,
			@TKMD_ID
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_SelectDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 05, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KD_ToKhaiTriGiaPP23_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[ID],
	[TKMD_ID],
	[MaToKhaiTriGia],
	[LyDo],
	[TenHang],
	[STTHang],
	[NgayXuat],
	[STTHangTT],
	[TenHangTT],
	[NgayXuatTT],
	[SoTKHangTT],
	[NgayDangKyHangTT],
	[MaLoaiHinhHangTT],
	[MaHaiQuanHangTT],
	[TriGiaNguyenTeHangTT],
	[DieuChinhCongThuongMai],
	[DieuChinhCongSoLuong],
	[DieuChinhCongKhoanGiamGiaKhac],
	[DieuChinhCongChiPhiVanTai],
	[DieuChinhCongChiPhiBaoHiem],
	[TriGiaNguyenTeHang],
	[TriGiaTTHang],
	[ToSo],
	[DieuChinhTruCapDoThuongMai],
	[DieuChinhTruSoLuong],
	[DieuChinhTruKhoanGiamGiaKhac],
	[DieuChinhTruChiPhiVanTai],
	[DieuChinhTruChiPhiBaoHiem],
	[GiaiTrinh]
 FROM [dbo].[t_KDT_KD_ToKhaiTriGiaPP23] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HuyToKhai_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HuyToKhai_DeleteDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, July 14, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HuyToKhai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HuyToKhai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_InsertUpdate]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_InsertUpdate]
	@ID bigint,
	@HMD_ID bigint,
	@TKMD_ID bigint,
	@CO_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_CO_Detail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_CO_Detail] 
		SET
			[HMD_ID] = @HMD_ID,
			[TKMD_ID] = @TKMD_ID,
			[CO_ID] = @CO_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_CO_Detail]
		(
			[HMD_ID],
			[TKMD_ID],
			[CO_ID]
		)
		VALUES 
		(
			@HMD_ID,
			@TKMD_ID,
			@CO_ID
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_DeleteDynamic]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ChungTuKem] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_Delete]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GiayPhepDetail]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_DeleteDynamic]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HangGiayPhepDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_Delete]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_CO_Detail]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HuyToKhai_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HuyToKhai_SelectDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, July 14, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HuyToKhai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[LyDoHuy],
	[SoTN],
	[NgayTN],
	[NamTN]
FROM [dbo].[t_HuyToKhai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_DeleteBy_CO_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_DeleteBy_CO_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_DeleteBy_CO_ID]
	@CO_ID bigint
AS

DELETE FROM [dbo].[t_KDT_CO_Detail]
WHERE
	[CO_ID] = @CO_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_DeleteBy_GiayPhepID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_DeleteBy_GiayPhepID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_DeleteBy_GiayPhepID]
	@GiayPhepID bigint
AS

DELETE FROM [dbo].[t_KDT_GiayPhepDetail]
WHERE
	[GiayPhepID] = @GiayPhepID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_DeleteBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_CO_Detail]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKem_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKem_SelectDynamic]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKem_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[SO_CT],
	[NGAY_CT],
	[MA_LOAI_CT],
	[DIENGIAI],
	[LoaiKB],
	[TrangThaiXuLy],
	[Tempt],
	[MessageID],
	[GUIDSTR],
	[KDT_WAITING],
	[KDT_LASTINFO],
	[SOTN],
	[NGAYTN],
	[TotalSize],
	[Phanluong],
	[HuongDan],
	[TKMDID]
FROM [dbo].[t_KDT_ChungTuKem] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangGiayPhepDetail_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangGiayPhepDetail_SelectDynamic]
-- Database: ECS.TQDT.KD.TEMP
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangGiayPhepDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhep_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
FROM [dbo].[t_KDT_HangGiayPhepDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_DeleteBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_DeleteBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GiayPhepDetail]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_DeleteDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_CO_Detail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_Load]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhepID],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_GiayPhepDetail]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_SelectBy_GiayPhepID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_SelectBy_GiayPhepID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_SelectBy_GiayPhepID]
	@GiayPhepID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhepID],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_GiayPhepDetail]
WHERE
	[GiayPhepID] = @GiayPhepID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DeleteDynamic]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 24, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_Load]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM
	[dbo].[t_KDT_CO_Detail]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SelectDynamic]
-- Database: ECS_DAILY_DTKD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 24, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[SoLuong],
	[TrangThai]
FROM [dbo].[t_KDT] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_SelectBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_SelectBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhepID],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_GiayPhepDetail]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_SelectBy_CO_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_SelectBy_CO_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_SelectBy_CO_ID]
	@CO_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM
	[dbo].[t_KDT_CO_Detail]
WHERE
	[CO_ID] = @CO_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_DeleteDynamic]
-- Database: ECS_KD_TQDT_LMMN
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 29, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ToKhaiMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_DeleteDynamic]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VanDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_SelectBy_TKMD_ID]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM
	[dbo].[t_KDT_CO_Detail]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_DeleteDynamic]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ChungTuKemChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhepDetail_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhepDetail_SelectAll]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 17, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhepDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhepID],
	[TKMD_ID]
FROM
	[dbo].[t_KDT_GiayPhepDetail]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]
-- Database: ECS_KD_TQDT_LMMN
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, December 29, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
 FROM [dbo].[t_KDT_ToKhaiMauDich] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_SelectDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM [dbo].[t_KDT_CO_Detail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_VanDon_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_SelectDynamic]
-- Database: ECS_TQ_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: 10 Tháng Mười Hai 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh]
FROM [dbo].[t_KDT_VanDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ChungTuKemChiTiet_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuKemChiTiet_SelectDynamic]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuKemChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM [dbo].[t_KDT_ChungTuKemChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CO_Detail_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Detail_SelectAll]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Detail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM
	[dbo].[t_KDT_CO_Detail]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_DeleteDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_HangTKTG] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_HangTKTG_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HangTKTG_SelectDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 08, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HangTKTG_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 
'SELECT 
	[ID],
	[TenHang],
	[STTHang],
	[GiaTrenHoaDon],
	[KhoanThanhToanGianTiep],
	[TraTruoc],
	[TongCong1],
	[HoaHong],
	[ChiPhiBaoBi],
	[ChiPhiDongGoi],
	[TroGiup],
	[VatLieu],
	[CongCu],
	[NguyenLieu],
	[ThietKe],
	[BanQuyen],
	[TienTraSuDung],
	[ChiPhiVanChuyen],
	[CuocPhiXepHang],
	[CuocPhiBaoHiem],
	[TongCong2],
	[PhiBaoHiem],
	[ChiPhiPhatSinh],
	[TienLai],
	[TienThue],
	[GiamGia],
	[ChiPhiKhongTang],
	[TriGiaNguyenTe],
	[TriGiaVND],
	[NgayTruyen],
	[TKTG_ID],
	[ChietKhau],
	[TongCong3],
	[ChiPhiNoiDia]
FROM [dbo].[t_KDT_SXXK_HangTKTG] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_DeleteDynamic]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_HangMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_HangMauDich_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_SelectDynamic]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_HangMauDich_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[SoToKhai],
	[MaLoaiHinh],
	[MaHaiQuan],
	[NamDangKy],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue]
 FROM [dbo].[t_SXXK_HangMauDich] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_DeleteDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_CODetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_DeleteDynamic]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ChungTuKemChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_DeleteDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HoaDonThuongMai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_ChungTuKemChiTiet_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_ChungTuKemChiTiet_SelectDynamic]
-- Database: ECS_TQ_KD
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 24, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ChungTuKemChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[ChungTuKemID],
	[FileName],
	[FileSize],
	[NoiDung]
FROM [dbo].[t_KDT_ChungTuKemChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HoaDonThuongMai_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HoaDonThuongMai_SelectDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HoaDonThuongMai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[SoHoaDon],
	[NgayHoaDon],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
FROM [dbo].[t_KDT_HoaDonThuongMai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_CODetail_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CODetail_SelectDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CODetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[HMD_ID],
	[TKMD_ID],
	[CO_ID]
FROM [dbo].[t_KDT_CODetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_DeleteDynamic]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteDynamic]
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HangMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_DeleteDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_NoiDungDieuChinhTK] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HangMauDich_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectDynamic]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HangMauDich_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	*
 FROM [dbo].[t_KDT_HangMauDich] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_DeleteDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HopDongThuongMai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTK_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTK_SelectDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTK_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTK],
	[NgayDK],
	[MaLoaiHinh],
	[NgaySua],
	[SoDieuChinh],
	[TrangThai]
FROM [dbo].[t_KDT_NoiDungDieuChinhTK] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_HopDongThuongMai_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMai_SelectDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Friday, December 11, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_HopDongThuongMai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[SoHopDongTM],
	[NgayHopDongTM],
	[ThoiHanThanhToan],
	[NguyenTe_ID],
	[PTTT_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[MaDonViMua],
	[TenDonViMua],
	[MaDonViBan],
	[TenDonViBan],
	[TongTriGia],
	[ThongTinKhac],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[MaDoanhNghiep]
FROM [dbo].[t_KDT_HopDongThuongMai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_DeleteDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_Container] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_Container_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Container_SelectDynamic]
-- Database: ECS_KD
-- Author: Ngo Thanh Tung
-- Time created: Thursday, July 16, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_Container_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 
'SELECT 
	[ID],
	[VanDon_ID],
	[SoHieu],
	[LoaiContainer],
	[Seal_No],
	[Trang_thai]
FROM [dbo].[t_KDT_Container] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_NoiDungDieuChinhTKDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GiayPhep_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_DeleteDynamic]
-- Database: ECS_KD1
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 10, 2009
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GiayPhep_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(3250)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GiayPhep] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: 04 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh]
FROM [dbo].[t_KDT_NoiDungDieuChinhTKDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_CuongCheChiTiet]'
GO
ALTER TABLE [dbo].[t_KDT_CuongCheChiTiet] WITH NOCHECK ADD
CONSTRAINT [FK_DCUONGCHE_TK_DCUONGCHE] FOREIGN KEY ([DCUONGCHEID]) REFERENCES [dbo].[t_KDT_CuongChe] ([DCUONGCHEID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[GROUP_ROLE]'
GO
ALTER TABLE [dbo].[GROUP_ROLE] ADD
CONSTRAINT [FK_GROUP_ROLE_GROUPS] FOREIGN KEY ([GROUP_ID]) REFERENCES [dbo].[GROUPS] ([MA_NHOM]) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT [FK_GROUP_ROLE_ROLE] FOREIGN KEY ([ID_ROLE]) REFERENCES [dbo].[ROLE] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[USER_GROUP]'
GO
ALTER TABLE [dbo].[USER_GROUP] ADD
CONSTRAINT [FK_USER_GROUP_SXXK_GROUPS1] FOREIGN KEY ([MA_NHOM]) REFERENCES [dbo].[GROUPS] ([MA_NHOM]) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT [FK_USER_GROUP_User] FOREIGN KEY ([USER_ID]) REFERENCES [dbo].[User] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_ChungTuKemChiTiet]'
GO
ALTER TABLE [dbo].[t_KDT_ChungTuKemChiTiet] ADD
CONSTRAINT [FK_t_KDT_ChungTuKemChiTiet_t_KDT_ChungTuKem] FOREIGN KEY ([ChungTuKemID]) REFERENCES [dbo].[t_KDT_ChungTuKem] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_ChungTuKem]'
GO
ALTER TABLE [dbo].[t_KDT_ChungTuKem] ADD
CONSTRAINT [FK_t_KDT_ChungTuKem_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMDID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_CO]'
GO
ALTER TABLE [dbo].[t_KDT_CO] ADD
CONSTRAINT [FK_t_KDT_CO_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_Container]'
GO
ALTER TABLE [dbo].[t_KDT_Container] ADD
CONSTRAINT [FK_t_KDT_Container_t_KDT_VanDon] FOREIGN KEY ([VanDon_ID]) REFERENCES [dbo].[t_KDT_VanDon] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_DeNghiChuyenCuaKhau]'
GO
ALTER TABLE [dbo].[t_KDT_DeNghiChuyenCuaKhau] ADD
CONSTRAINT [FK_t_KDT_DeNghiChuyenCuaKhau_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HangGiayPhepDetail]'
GO
ALTER TABLE [dbo].[t_KDT_HangGiayPhepDetail] ADD
CONSTRAINT [FK_t_KDT_HangGiayPhepDetail_t_KDT_GiayPhep] FOREIGN KEY ([GiayPhep_ID]) REFERENCES [dbo].[t_KDT_GiayPhep] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_GiayPhep]'
GO
ALTER TABLE [dbo].[t_KDT_GiayPhep] ADD
CONSTRAINT [FK_t_KDT_GiayPhep_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HoaDonThuongMaiDetail]'
GO
ALTER TABLE [dbo].[t_KDT_HoaDonThuongMaiDetail] ADD
CONSTRAINT [FK_t_KDT_HoaDonThuongMai_Detail_t_KDT_HangMauDich] FOREIGN KEY ([HMD_ID]) REFERENCES [dbo].[t_KDT_HangMauDich] ([ID]),
CONSTRAINT [FK_t_KDT_HoaDonThuongMai_Detail_t_KDT_HoaDonThuongMai1] FOREIGN KEY ([HoaDonTM_ID]) REFERENCES [dbo].[t_KDT_HoaDonThuongMai] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HopDongThuongMaiDetail]'
GO
ALTER TABLE [dbo].[t_KDT_HopDongThuongMaiDetail] ADD
CONSTRAINT [FK_t_KDT_HopDong_Detail_t_KDT_HangMauDich] FOREIGN KEY ([HMD_ID]) REFERENCES [dbo].[t_KDT_HangMauDich] ([ID]),
CONSTRAINT [FK_t_KDT_HopDong_Detail_t_KDT_HopDong1] FOREIGN KEY ([HopDongTM_ID]) REFERENCES [dbo].[t_KDT_HopDongThuongMai] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ADD
CONSTRAINT [FK_t_HangMauDich_t_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HoaDonThuongMai]'
GO
ALTER TABLE [dbo].[t_KDT_HoaDonThuongMai] ADD
CONSTRAINT [FK_t_KDT_HoaDonThuongMai_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HopDongThuongMai]'
GO
ALTER TABLE [dbo].[t_KDT_HopDongThuongMai] ADD
CONSTRAINT [FK_t_KDT_HopDong_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HuyToKhai]'
GO
ALTER TABLE [dbo].[t_KDT_HuyToKhai] ADD
CONSTRAINT [FK_t_KDT_HuyToKhai_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_KD_ToKhaiTriGiaPP23]'
GO
ALTER TABLE [dbo].[t_KDT_KD_ToKhaiTriGiaPP23] ADD
CONSTRAINT [FK_t_KDT_KD_ToKhaiTriGiaPP23_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_SXXK_ChungTuKemTheo]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_ChungTuKemTheo] ADD
CONSTRAINT [FK_t_KDT_SXXK_ChungTuKemTheo_t_KDT_ToKhaiMauDich] FOREIGN KEY ([Master_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_SXXK_HangTKTG]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_HangTKTG] ADD
CONSTRAINT [FK_t_KDT_SXXK_HangTKTG_t_KDT_SXXK_ToKhaiTriGia] FOREIGN KEY ([TKTG_ID]) REFERENCES [dbo].[t_KDT_SXXK_ToKhaiTriGia] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_SXXK_ToKhaiTriGia]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_ToKhaiTriGia] ADD
CONSTRAINT [FK_t_KDT_SXX_ToKhaiTriGia_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_VanDon]'
GO
ALTER TABLE [dbo].[t_KDT_VanDon] ADD
CONSTRAINT [FK_t_KDT_VanDon_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_SXXK_HangMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_HangMauDich] ADD
CONSTRAINT [FK_t_SXXK_HangMauDich_t_SXXK_ToKhaiMauDich] FOREIGN KEY ([MaHaiQuan], [SoToKhai], [MaLoaiHinh], [NamDangKy]) REFERENCES [dbo].[t_SXXK_ToKhaiMauDich] ([MaHaiQuan], [SoToKhai], [MaLoaiHinh], [NamDangKy]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_SXXK_ThongTinDieuChinh]'
GO
ALTER TABLE [dbo].[t_SXXK_ThongTinDieuChinh] ADD
CONSTRAINT [FK_t_SXXK_ThongTinDieuChinh_t_SXXK_ToKhaiMauDich] FOREIGN KEY ([MaHaiQuan], [SoToKhai], [MaLoaiHinh], [NamDangKy]) REFERENCES [dbo].[t_SXXK_ToKhaiMauDich] ([MaHaiQuan], [SoToKhai], [MaLoaiHinh], [NamDangKy]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

INSERT INTO t_Haiquan_Version VALUES(1, GETDATE(), NULL)