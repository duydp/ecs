﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Category
{
    public partial class VNACC_Category_CustomsOffice : BaseForm
    {
        public Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsOffice customsOffice;
        public string where = "";
        public VNACC_Category_CustomsOffice()
        {
            InitializeComponent();
        }

        private void VNACC_Category_CustomsOffice_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsOffice.SelectCollectionDynamic(GetSearchWhere(), null);
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ClearText()
        {
            //txtCode.Text = String.Empty;
            txtName.Text = String.Empty;
            txtName_EN.Text = String.Empty;
            txtNotes.Text = String.Empty;
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtCode, errorProvider, "Mã hải quan  ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtName, errorProvider, "Mã CCHQ ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtName_EN, errorProvider, "Tên tiếng việt ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNotes, errorProvider, "Ghi chú ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void Get()
        {
            try
            {
                customsOffice.CustomsCode = txtCode.Text.Trim().ToUpper();
                customsOffice.CustomsOfficeName = txtName.Text.Trim().ToUpper();
                customsOffice.CustomsOfficeNameInVietnamese = txtName_EN.Text.Trim().ToUpper();
                customsOffice.Notes = txtNotes.Text.Trim().ToUpper();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void Set()
        {
            try
            {
                txtCode.Text = customsOffice.CustomsCode;
                txtName.Text = customsOffice.CustomsOfficeName;
                txtName_EN.Text = customsOffice.CustomsOfficeNameInVietnamese;
                txtNotes.Text = customsOffice.Notes;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtCode.Text))
                    where += " AND CustomsCode = '" + txtCode.Text.Trim() + "'";
                if (!String.IsNullOrEmpty(txtName.Text))
                    where += " AND CustomsOfficeName LIKE N'%" + txtName.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtName_EN.Text))
                    where += " AND CustomsOfficeNameInVietnamese LIKE N'%" + txtName_EN.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtNotes.Text))
                    where += " AND Notes LIKE N'%" + txtNotes.Text.Trim() + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return where;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                Get();
                customsOffice.TableID = "A038A";
                customsOffice.InsertUpdate();
                ShowMessage("Lưu thành công ", false);
                customsOffice = new Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsOffice();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Get();
                if (ShowMessage("Bạn có muốn xóa dòng này không ? ", true) == "Yes")
                {
                    customsOffice.Delete();
                }
                ShowMessage("Xóa thành công ", false);
                customsOffice = new Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsOffice();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {

                if (e.Row.RowType == RowType.Record)
                {
                    customsOffice = new Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsOffice();
                    customsOffice = (Company.KDT.SHARE.VNACCS.VNACC_Category_CustomsOffice)e.Row.DataRow;
                    Set();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
