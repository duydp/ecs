using System;
using System.Data;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.KD.BLL.SXXK;
using Company.KD.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.Report.SXXK;

namespace Company.Interface.SXXK
{
    public partial class SanPhamRegistedForm : BaseForm
    {
        public HangHoaXuat SanPhamSelected = new HangHoaXuat();
        public HangHoaXuatCollection SPCollection = new HangHoaXuatCollection();       
        public bool isBowrer=false;
        public SanPhamRegistedForm()
        {
            InitializeComponent();
            
        }

        public void BindData()
        {
            string where = string.Format("maDoanhNghiep='{0}'",GlobalSettings.MA_DON_VI);

            this.SPCollection = new HangHoaXuat().SelectCollectionDynamic(where, "Ma");
            dgList.DataSource = this.SPCollection;
            
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];          
        }

        //-----------------------------------------------------------------------------------------

        private void SanPhamRegistedForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            // Sản phẩm đã đăng ký.
            this.BindData();
            // Doanh nghiệp / Đại lý TTHQ.
            dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;
            //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.CapNhatDuLieu)))
            //{
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            //    uiButton3.Visible = false;
            //}      

        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {
           
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBowrer)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string maNPL = e.Row.Cells["Ma"].Text;
                    this.SanPhamSelected.Ma = maNPL;
                    this.SanPhamSelected.MaHaiQuan = "";
                    this.SanPhamSelected.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    SanPhamSelected.Load();
                    this.Hide();
                }
            }
            else
            {
                SanPhamChangeForm f = new SanPhamChangeForm();
                f.HangXuat = new HangHoaXuat();
                f.HangXuat.Ma = e.Row.Cells["Ma"].Text;
                f.HangXuat.Ten= e.Row.Cells["Ten"].Text;
                f.HangXuat.MaHS = e.Row.Cells["MaHS"].Text;
                f.HangXuat.DVT_ID= e.Row.Cells["DVT_ID"].Value.ToString();
                f.ShowDialog();
                this.BindData();
            }
        }

      
        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            //ImportSPForm f = new ImportSPForm();
            //f.SPCollection = this.SPCollection;
            //f.ShowDialog();
            //BindData();
            //dgList.Refetch();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            uiButton1.Enabled = false;
            this.BindData();
            dgList.Refetch();
            uiButton1.Enabled = true;
            this.Cursor = Cursors.Default;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MLMessages("Bạn có muốn xóa các thông tin đã chọn không?","MSG_CAN01","", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangHoaXuat hmd = (HangHoaXuat)i.GetRow().DataRow;
                        //if (hmd.CheckTKMDUserMa(GlobalSettings.MA_DON_VI))
                        //{
                        //    ShowMessage("Mặt hàng này đang được sử dụng để khai tờ khai nên không xóa được.", false);
                        //    e.Cancel = true;
                        //    return;
                        //}
                        //else
                            hmd.Delete();
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            SanPhamEditForm f = new SanPhamEditForm();
            f.ShowDialog();
            BindData();
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            //SelectSanPhamForm f = new SelectSanPhamForm();
            //f.ShowDialog();
            //ReportSanPham f2 = new ReportSanPham();
            //if (f.spCollectionSelect.Count > 0)
            //{
            //    f2.SPCollection = f.spCollectionSelect;
            //    f2.BindReportDinhMucDaDangKy();
            //    f2.ShowPreview();
            //}

        }


        private void uiButton2_Click_2(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------
    }
}