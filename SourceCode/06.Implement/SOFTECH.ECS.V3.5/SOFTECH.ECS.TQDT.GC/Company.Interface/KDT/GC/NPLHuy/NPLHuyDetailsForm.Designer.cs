﻿namespace Company.Interface.KDT.GC
{
    partial class NPLHuyDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NPLHuyDetailsForm));
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucCategoryAllowEmpty1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(838, 138);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.Location = new System.Drawing.Point(676, 10);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(757, 10);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ucCategoryAllowEmpty1);
            this.uiGroupBox1.Controls.Add(this.txtSoLuong);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtTenNPL);
            this.uiGroupBox1.Controls.Add(this.txtMaNPL);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(838, 98);
            this.uiGroupBox1.TabIndex = 29;
            this.uiGroupBox1.Text = "Nguyên phụ liệu";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ucCategoryAllowEmpty1
            // 
            this.ucCategoryAllowEmpty1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucCategoryAllowEmpty1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.ucCategoryAllowEmpty1.Appearance.Options.UseBackColor = true;
            this.ucCategoryAllowEmpty1.Appearance.Options.UseBorderColor = false;
            this.ucCategoryAllowEmpty1.Appearance.Options.UseFont = false;
            this.ucCategoryAllowEmpty1.Appearance.Options.UseForeColor = false;
            this.ucCategoryAllowEmpty1.Appearance.Options.UseImage = false;
            this.ucCategoryAllowEmpty1.Appearance.Options.UseTextOptions = false;
            this.ucCategoryAllowEmpty1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.ucCategoryAllowEmpty1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Default;
            this.ucCategoryAllowEmpty1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucCategoryAllowEmpty1.Code = "";
            this.ucCategoryAllowEmpty1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryAllowEmpty1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryAllowEmpty1.IsOnlyWarning = false;
            this.ucCategoryAllowEmpty1.IsValidate = true;
            this.ucCategoryAllowEmpty1.Location = new System.Drawing.Point(84, 64);
            this.ucCategoryAllowEmpty1.Name = "ucCategoryAllowEmpty1";
            this.ucCategoryAllowEmpty1.Name_VN = "";
            this.ucCategoryAllowEmpty1.SetOnlyWarning = false;
            this.ucCategoryAllowEmpty1.SetValidate = false;
            this.ucCategoryAllowEmpty1.ShowColumnCode = true;
            this.ucCategoryAllowEmpty1.ShowColumnName = true;
            this.ucCategoryAllowEmpty1.Size = new System.Drawing.Size(167, 21);
            this.ucCategoryAllowEmpty1.TabIndex = 5;
            this.ucCategoryAllowEmpty1.TagCode = "";
            this.ucCategoryAllowEmpty1.TagName = "";
            this.ucCategoryAllowEmpty1.Where = null;
            this.ucCategoryAllowEmpty1.WhereCondition = "";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 4;
            this.txtSoLuong.Location = new System.Drawing.Point(337, 64);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.ReadOnly = true;
            this.txtSoLuong.Size = new System.Drawing.Size(101, 21);
            this.txtSoLuong.TabIndex = 4;
            this.txtSoLuong.Text = "0.0000";
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtSoLuong.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Đơn vị tính";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(257, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Tổng NPL Hủy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(257, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên NPL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã NPL";
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.Location = new System.Drawing.Point(309, 25);
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.Size = new System.Drawing.Size(461, 21);
            this.txtTenNPL.TabIndex = 0;
            this.txtTenNPL.VisualStyleManager = this.vsmMain;
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.Location = new System.Drawing.Point(84, 25);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(167, 21);
            this.txtMaNPL.TabIndex = 0;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 98);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(838, 40);
            this.uiGroupBox2.TabIndex = 30;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.Location = new System.Drawing.Point(595, 10);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(75, 23);
            this.btnLuu.TabIndex = 3;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.Controls.Add(this.btnLuu);
            this.uiGroupBox3.Controls.Add(this.btnXoa);
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 3);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(838, 37);
            this.uiGroupBox3.TabIndex = 4;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // NPLHuyDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 138);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "NPLHuyDetailsForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Khai báo NPL cung ứng";
            this.Load += new System.EventHandler(this.CungUngForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnLuu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucCategoryAllowEmpty1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
    }
}