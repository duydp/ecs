using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangHoaDon : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HoaDon_ID { set; get; }
		public string MaHang { set; get; }
		public string MaSoHang { set; get; }
		public string TenHang { set; get; }
		public string MaNuocXX { set; get; }
		public string TenNuocXX { set; get; }
		public string SoKienHang { set; get; }
		public decimal SoLuong1 { set; get; }
		public string MaDVT_SoLuong1 { set; get; }
		public decimal SoLuong2 { set; get; }
		public string MaDVT_SoLuong2 { set; get; }
		public decimal DonGiaHoaDon { set; get; }
		public string MaTT_DonGia { set; get; }
		public string MaDVT_DonGia { set; get; }
		public decimal TriGiaHoaDon { set; get; }
		public string MaTT_GiaTien { set; get; }
		public string LoaiKhauTru { set; get; }
		public decimal SoTienKhauTru { set; get; }
		public string MaTT_TienKhauTru { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HangHoaDon> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HangHoaDon> collection = new List<KDT_VNACC_HangHoaDon>();
			while (reader.Read())
			{
				KDT_VNACC_HangHoaDon entity = new KDT_VNACC_HangHoaDon();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoaDon_ID"))) entity.HoaDon_ID = reader.GetInt64(reader.GetOrdinal("HoaDon_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoHang"))) entity.MaSoHang = reader.GetString(reader.GetOrdinal("MaSoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXX"))) entity.MaNuocXX = reader.GetString(reader.GetOrdinal("MaNuocXX"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNuocXX"))) entity.TenNuocXX = reader.GetString(reader.GetOrdinal("TenNuocXX"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKienHang"))) entity.SoKienHang = reader.GetString(reader.GetOrdinal("SoKienHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong1"))) entity.SoLuong1 = reader.GetDecimal(reader.GetOrdinal("SoLuong1"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT_SoLuong1"))) entity.MaDVT_SoLuong1 = reader.GetString(reader.GetOrdinal("MaDVT_SoLuong1"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong2"))) entity.SoLuong2 = reader.GetDecimal(reader.GetOrdinal("SoLuong2"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT_SoLuong2"))) entity.MaDVT_SoLuong2 = reader.GetString(reader.GetOrdinal("MaDVT_SoLuong2"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaHoaDon"))) entity.DonGiaHoaDon = reader.GetDecimal(reader.GetOrdinal("DonGiaHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_DonGia"))) entity.MaTT_DonGia = reader.GetString(reader.GetOrdinal("MaTT_DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT_DonGia"))) entity.MaDVT_DonGia = reader.GetString(reader.GetOrdinal("MaDVT_DonGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHoaDon"))) entity.TriGiaHoaDon = reader.GetDecimal(reader.GetOrdinal("TriGiaHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_GiaTien"))) entity.MaTT_GiaTien = reader.GetString(reader.GetOrdinal("MaTT_GiaTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKhauTru"))) entity.LoaiKhauTru = reader.GetString(reader.GetOrdinal("LoaiKhauTru"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhauTru"))) entity.SoTienKhauTru = reader.GetDecimal(reader.GetOrdinal("SoTienKhauTru"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_TienKhauTru"))) entity.MaTT_TienKhauTru = reader.GetString(reader.GetOrdinal("MaTT_TienKhauTru"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HangHoaDon> collection, long id)
        {
            foreach (KDT_VNACC_HangHoaDon item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangHoaDon VALUES(@HoaDon_ID, @MaHang, @MaSoHang, @TenHang, @MaNuocXX, @TenNuocXX, @SoKienHang, @SoLuong1, @MaDVT_SoLuong1, @SoLuong2, @MaDVT_SoLuong2, @DonGiaHoaDon, @MaTT_DonGia, @MaDVT_DonGia, @TriGiaHoaDon, @MaTT_GiaTien, @LoaiKhauTru, @SoTienKhauTru, @MaTT_TienKhauTru, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HangHoaDon SET HoaDon_ID = @HoaDon_ID, MaHang = @MaHang, MaSoHang = @MaSoHang, TenHang = @TenHang, MaNuocXX = @MaNuocXX, TenNuocXX = @TenNuocXX, SoKienHang = @SoKienHang, SoLuong1 = @SoLuong1, MaDVT_SoLuong1 = @MaDVT_SoLuong1, SoLuong2 = @SoLuong2, MaDVT_SoLuong2 = @MaDVT_SoLuong2, DonGiaHoaDon = @DonGiaHoaDon, MaTT_DonGia = @MaTT_DonGia, MaDVT_DonGia = @MaDVT_DonGia, TriGiaHoaDon = @TriGiaHoaDon, MaTT_GiaTien = @MaTT_GiaTien, LoaiKhauTru = @LoaiKhauTru, SoTienKhauTru = @SoTienKhauTru, MaTT_TienKhauTru = @MaTT_TienKhauTru, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangHoaDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoaDon_ID", SqlDbType.BigInt, "HoaDon_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocXX", SqlDbType.VarChar, "MaNuocXX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNuocXX", SqlDbType.VarChar, "TenNuocXX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoKienHang", SqlDbType.VarChar, "SoKienHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_SoLuong1", SqlDbType.VarChar, "MaDVT_SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong2", SqlDbType.Decimal, "SoLuong2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_SoLuong2", SqlDbType.VarChar, "MaDVT_SoLuong2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_DonGia", SqlDbType.VarChar, "MaTT_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_DonGia", SqlDbType.VarChar, "MaDVT_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_GiaTien", SqlDbType.VarChar, "MaTT_GiaTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKhauTru", SqlDbType.VarChar, "LoaiKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienKhauTru", SqlDbType.Decimal, "SoTienKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, "MaTT_TienKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoaDon_ID", SqlDbType.BigInt, "HoaDon_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocXX", SqlDbType.VarChar, "MaNuocXX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNuocXX", SqlDbType.VarChar, "TenNuocXX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoKienHang", SqlDbType.VarChar, "SoKienHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_SoLuong1", SqlDbType.VarChar, "MaDVT_SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong2", SqlDbType.Decimal, "SoLuong2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_SoLuong2", SqlDbType.VarChar, "MaDVT_SoLuong2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_DonGia", SqlDbType.VarChar, "MaTT_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_DonGia", SqlDbType.VarChar, "MaDVT_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_GiaTien", SqlDbType.VarChar, "MaTT_GiaTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKhauTru", SqlDbType.VarChar, "LoaiKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienKhauTru", SqlDbType.Decimal, "SoTienKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, "MaTT_TienKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangHoaDon VALUES(@HoaDon_ID, @MaHang, @MaSoHang, @TenHang, @MaNuocXX, @TenNuocXX, @SoKienHang, @SoLuong1, @MaDVT_SoLuong1, @SoLuong2, @MaDVT_SoLuong2, @DonGiaHoaDon, @MaTT_DonGia, @MaDVT_DonGia, @TriGiaHoaDon, @MaTT_GiaTien, @LoaiKhauTru, @SoTienKhauTru, @MaTT_TienKhauTru, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HangHoaDon SET HoaDon_ID = @HoaDon_ID, MaHang = @MaHang, MaSoHang = @MaSoHang, TenHang = @TenHang, MaNuocXX = @MaNuocXX, TenNuocXX = @TenNuocXX, SoKienHang = @SoKienHang, SoLuong1 = @SoLuong1, MaDVT_SoLuong1 = @MaDVT_SoLuong1, SoLuong2 = @SoLuong2, MaDVT_SoLuong2 = @MaDVT_SoLuong2, DonGiaHoaDon = @DonGiaHoaDon, MaTT_DonGia = @MaTT_DonGia, MaDVT_DonGia = @MaDVT_DonGia, TriGiaHoaDon = @TriGiaHoaDon, MaTT_GiaTien = @MaTT_GiaTien, LoaiKhauTru = @LoaiKhauTru, SoTienKhauTru = @SoTienKhauTru, MaTT_TienKhauTru = @MaTT_TienKhauTru, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangHoaDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoaDon_ID", SqlDbType.BigInt, "HoaDon_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocXX", SqlDbType.VarChar, "MaNuocXX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNuocXX", SqlDbType.VarChar, "TenNuocXX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoKienHang", SqlDbType.VarChar, "SoKienHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_SoLuong1", SqlDbType.VarChar, "MaDVT_SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong2", SqlDbType.Decimal, "SoLuong2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_SoLuong2", SqlDbType.VarChar, "MaDVT_SoLuong2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_DonGia", SqlDbType.VarChar, "MaTT_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_DonGia", SqlDbType.VarChar, "MaDVT_DonGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_GiaTien", SqlDbType.VarChar, "MaTT_GiaTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKhauTru", SqlDbType.VarChar, "LoaiKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienKhauTru", SqlDbType.Decimal, "SoTienKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, "MaTT_TienKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoaDon_ID", SqlDbType.BigInt, "HoaDon_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocXX", SqlDbType.VarChar, "MaNuocXX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNuocXX", SqlDbType.VarChar, "TenNuocXX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoKienHang", SqlDbType.VarChar, "SoKienHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_SoLuong1", SqlDbType.VarChar, "MaDVT_SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong2", SqlDbType.Decimal, "SoLuong2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_SoLuong2", SqlDbType.VarChar, "MaDVT_SoLuong2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_DonGia", SqlDbType.VarChar, "MaTT_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_DonGia", SqlDbType.VarChar, "MaDVT_DonGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_GiaTien", SqlDbType.VarChar, "MaTT_GiaTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKhauTru", SqlDbType.VarChar, "LoaiKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienKhauTru", SqlDbType.Decimal, "SoTienKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, "MaTT_TienKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HangHoaDon Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangHoaDon_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HangHoaDon> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HangHoaDon> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HangHoaDon> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HangHoaDon(long hoaDon_ID, string maHang, string maSoHang, string tenHang, string maNuocXX, string tenNuocXX, string soKienHang, decimal soLuong1, string maDVT_SoLuong1, decimal soLuong2, string maDVT_SoLuong2, decimal donGiaHoaDon, string maTT_DonGia, string maDVT_DonGia, decimal triGiaHoaDon, string maTT_GiaTien, string loaiKhauTru, decimal soTienKhauTru, string maTT_TienKhauTru, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangHoaDon entity = new KDT_VNACC_HangHoaDon();	
			entity.HoaDon_ID = hoaDon_ID;
			entity.MaHang = maHang;
			entity.MaSoHang = maSoHang;
			entity.TenHang = tenHang;
			entity.MaNuocXX = maNuocXX;
			entity.TenNuocXX = tenNuocXX;
			entity.SoKienHang = soKienHang;
			entity.SoLuong1 = soLuong1;
			entity.MaDVT_SoLuong1 = maDVT_SoLuong1;
			entity.SoLuong2 = soLuong2;
			entity.MaDVT_SoLuong2 = maDVT_SoLuong2;
			entity.DonGiaHoaDon = donGiaHoaDon;
			entity.MaTT_DonGia = maTT_DonGia;
			entity.MaDVT_DonGia = maDVT_DonGia;
			entity.TriGiaHoaDon = triGiaHoaDon;
			entity.MaTT_GiaTien = maTT_GiaTien;
			entity.LoaiKhauTru = loaiKhauTru;
			entity.SoTienKhauTru = soTienKhauTru;
			entity.MaTT_TienKhauTru = maTT_TienKhauTru;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HangHoaDon_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HoaDon_ID", SqlDbType.BigInt, HoaDon_ID);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaNuocXX", SqlDbType.VarChar, MaNuocXX);
			db.AddInParameter(dbCommand, "@TenNuocXX", SqlDbType.VarChar, TenNuocXX);
			db.AddInParameter(dbCommand, "@SoKienHang", SqlDbType.VarChar, SoKienHang);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@MaDVT_SoLuong1", SqlDbType.VarChar, MaDVT_SoLuong1);
			db.AddInParameter(dbCommand, "@SoLuong2", SqlDbType.Decimal, SoLuong2);
			db.AddInParameter(dbCommand, "@MaDVT_SoLuong2", SqlDbType.VarChar, MaDVT_SoLuong2);
			db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTT_DonGia", SqlDbType.VarChar, MaTT_DonGia);
			db.AddInParameter(dbCommand, "@MaDVT_DonGia", SqlDbType.VarChar, MaDVT_DonGia);
			db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTT_GiaTien", SqlDbType.VarChar, MaTT_GiaTien);
			db.AddInParameter(dbCommand, "@LoaiKhauTru", SqlDbType.VarChar, LoaiKhauTru);
			db.AddInParameter(dbCommand, "@SoTienKhauTru", SqlDbType.Decimal, SoTienKhauTru);
			db.AddInParameter(dbCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, MaTT_TienKhauTru);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HangHoaDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangHoaDon item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HangHoaDon(long id, long hoaDon_ID, string maHang, string maSoHang, string tenHang, string maNuocXX, string tenNuocXX, string soKienHang, decimal soLuong1, string maDVT_SoLuong1, decimal soLuong2, string maDVT_SoLuong2, decimal donGiaHoaDon, string maTT_DonGia, string maDVT_DonGia, decimal triGiaHoaDon, string maTT_GiaTien, string loaiKhauTru, decimal soTienKhauTru, string maTT_TienKhauTru, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangHoaDon entity = new KDT_VNACC_HangHoaDon();			
			entity.ID = id;
			entity.HoaDon_ID = hoaDon_ID;
			entity.MaHang = maHang;
			entity.MaSoHang = maSoHang;
			entity.TenHang = tenHang;
			entity.MaNuocXX = maNuocXX;
			entity.TenNuocXX = tenNuocXX;
			entity.SoKienHang = soKienHang;
			entity.SoLuong1 = soLuong1;
			entity.MaDVT_SoLuong1 = maDVT_SoLuong1;
			entity.SoLuong2 = soLuong2;
			entity.MaDVT_SoLuong2 = maDVT_SoLuong2;
			entity.DonGiaHoaDon = donGiaHoaDon;
			entity.MaTT_DonGia = maTT_DonGia;
			entity.MaDVT_DonGia = maDVT_DonGia;
			entity.TriGiaHoaDon = triGiaHoaDon;
			entity.MaTT_GiaTien = maTT_GiaTien;
			entity.LoaiKhauTru = loaiKhauTru;
			entity.SoTienKhauTru = soTienKhauTru;
			entity.MaTT_TienKhauTru = maTT_TienKhauTru;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HangHoaDon_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HoaDon_ID", SqlDbType.BigInt, HoaDon_ID);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaNuocXX", SqlDbType.VarChar, MaNuocXX);
			db.AddInParameter(dbCommand, "@TenNuocXX", SqlDbType.VarChar, TenNuocXX);
			db.AddInParameter(dbCommand, "@SoKienHang", SqlDbType.VarChar, SoKienHang);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@MaDVT_SoLuong1", SqlDbType.VarChar, MaDVT_SoLuong1);
			db.AddInParameter(dbCommand, "@SoLuong2", SqlDbType.Decimal, SoLuong2);
			db.AddInParameter(dbCommand, "@MaDVT_SoLuong2", SqlDbType.VarChar, MaDVT_SoLuong2);
			db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTT_DonGia", SqlDbType.VarChar, MaTT_DonGia);
			db.AddInParameter(dbCommand, "@MaDVT_DonGia", SqlDbType.VarChar, MaDVT_DonGia);
			db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTT_GiaTien", SqlDbType.VarChar, MaTT_GiaTien);
			db.AddInParameter(dbCommand, "@LoaiKhauTru", SqlDbType.VarChar, LoaiKhauTru);
			db.AddInParameter(dbCommand, "@SoTienKhauTru", SqlDbType.Decimal, SoTienKhauTru);
			db.AddInParameter(dbCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, MaTT_TienKhauTru);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HangHoaDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangHoaDon item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HangHoaDon(long id, long hoaDon_ID, string maHang, string maSoHang, string tenHang, string maNuocXX, string tenNuocXX, string soKienHang, decimal soLuong1, string maDVT_SoLuong1, decimal soLuong2, string maDVT_SoLuong2, decimal donGiaHoaDon, string maTT_DonGia, string maDVT_DonGia, decimal triGiaHoaDon, string maTT_GiaTien, string loaiKhauTru, decimal soTienKhauTru, string maTT_TienKhauTru, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangHoaDon entity = new KDT_VNACC_HangHoaDon();			
			entity.ID = id;
			entity.HoaDon_ID = hoaDon_ID;
			entity.MaHang = maHang;
			entity.MaSoHang = maSoHang;
			entity.TenHang = tenHang;
			entity.MaNuocXX = maNuocXX;
			entity.TenNuocXX = tenNuocXX;
			entity.SoKienHang = soKienHang;
			entity.SoLuong1 = soLuong1;
			entity.MaDVT_SoLuong1 = maDVT_SoLuong1;
			entity.SoLuong2 = soLuong2;
			entity.MaDVT_SoLuong2 = maDVT_SoLuong2;
			entity.DonGiaHoaDon = donGiaHoaDon;
			entity.MaTT_DonGia = maTT_DonGia;
			entity.MaDVT_DonGia = maDVT_DonGia;
			entity.TriGiaHoaDon = triGiaHoaDon;
			entity.MaTT_GiaTien = maTT_GiaTien;
			entity.LoaiKhauTru = loaiKhauTru;
			entity.SoTienKhauTru = soTienKhauTru;
			entity.MaTT_TienKhauTru = maTT_TienKhauTru;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangHoaDon_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HoaDon_ID", SqlDbType.BigInt, HoaDon_ID);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaNuocXX", SqlDbType.VarChar, MaNuocXX);
			db.AddInParameter(dbCommand, "@TenNuocXX", SqlDbType.VarChar, TenNuocXX);
			db.AddInParameter(dbCommand, "@SoKienHang", SqlDbType.VarChar, SoKienHang);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@MaDVT_SoLuong1", SqlDbType.VarChar, MaDVT_SoLuong1);
			db.AddInParameter(dbCommand, "@SoLuong2", SqlDbType.Decimal, SoLuong2);
			db.AddInParameter(dbCommand, "@MaDVT_SoLuong2", SqlDbType.VarChar, MaDVT_SoLuong2);
			db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTT_DonGia", SqlDbType.VarChar, MaTT_DonGia);
			db.AddInParameter(dbCommand, "@MaDVT_DonGia", SqlDbType.VarChar, MaDVT_DonGia);
			db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTT_GiaTien", SqlDbType.VarChar, MaTT_GiaTien);
			db.AddInParameter(dbCommand, "@LoaiKhauTru", SqlDbType.VarChar, LoaiKhauTru);
			db.AddInParameter(dbCommand, "@SoTienKhauTru", SqlDbType.Decimal, SoTienKhauTru);
			db.AddInParameter(dbCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, MaTT_TienKhauTru);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HangHoaDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangHoaDon item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HangHoaDon(long id)
		{
			KDT_VNACC_HangHoaDon entity = new KDT_VNACC_HangHoaDon();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangHoaDon_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HangHoaDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangHoaDon item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}